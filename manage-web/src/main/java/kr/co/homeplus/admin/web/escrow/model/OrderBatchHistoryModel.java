package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 이력 model")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderBatchHistoryModel {
    @ApiModelProperty(value = "이력번호(Seq)", position = 1)
    @RealGridColumnInfo(headText = "이력번호", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String historyNo;

    @ApiModelProperty(value = "배치번호(seq)", position = 2)
    @RealGridColumnInfo(headText = "배치번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchNo;

    @ApiModelProperty(value = "배치시작일시", position = 3)
    @RealGridColumnInfo(headText = "배치시작일시", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchStartDt;

    @ApiModelProperty(value = "배치종료일시", position = 4)
    @RealGridColumnInfo(headText = "배치종료일시", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchEndDt;

    @ApiModelProperty(value = "배치진행시간", position = 5)
    @RealGridColumnInfo(headText = "배치진행시간", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchTime;

    @ApiModelProperty(value = "실패건수", position = 6)
    @RealGridColumnInfo(headText = "실패건수", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String failCount;

    @ApiModelProperty(value = "성공건수", position = 7)
    @RealGridColumnInfo(headText = "성공건수", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String successCount;

    @ApiModelProperty(value = "전체건수", position = 8)
    @RealGridColumnInfo(headText = "전체건수", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String totalCount;

    @ApiModelProperty(value = "오류메시지", position = 9)
    @RealGridColumnInfo(headText = "오류메시지", width = 300, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String errorMessage;

    @ApiModelProperty(value = "메타필드1", position = 10)
    private String metaField1;

    @ApiModelProperty(value = "메타필드2", position = 11)
    private String metaField2;

    @ApiModelProperty(value = "메타필드3", position = 12)
    private String metaField3;

    @ApiModelProperty(value = "메타필드4", position = 13)
    private String metaField4;

    @ApiModelProperty(value = "메타필드5", position = 14)
    private String metaField5;
}
