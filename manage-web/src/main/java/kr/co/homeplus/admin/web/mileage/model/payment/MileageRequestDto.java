package kr.co.homeplus.admin.web.mileage.model.payment;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageRequestDto { //MileageRequestDto
    //마일리지유형
    private String mileageTypeNo;

    //마일리지유형명
    private String mileageTypeName;

    //요청유형(01:지급, 02:회수)
    private String requestType;

    //요청사유
    private String requestReason;

    //고객노출문구
    private String displayMessage;

    //요청상세사유
    private String requestMessage;

    //실행요청일
    private String processDt;

    //실행여부
    private String useYn;

    //즉시실행여부
    private String directlyYn;

    //요청자
    private String regId;

    //거래번호(없을시에 미입력)
    private String tradeNo;
}
