package kr.co.homeplus.admin.web.delivery.model.shipMonitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "발송지연조회 요청")
public class ShipMonitoringDelaySetDto {
    @ApiModelProperty(notes = "배송일자 타입")
    private String schShipDtType;

    @ApiModelProperty(notes = "배송일(Start)")
    private String schShipStartDt;

    @ApiModelProperty(notes = "배송일(End)")
    private String schShipEndDt;

    @ApiModelProperty(notes = "점포유형")
    private String schStoreType;

    @ApiModelProperty(notes = "점포ID")
    private String schStoreId;

    @ApiModelProperty(notes = "배송방법")
    private String schShipMethod;

    @ApiModelProperty(notes = "배송상태")
    private String schShipStatus;

    @ApiModelProperty(notes = "발송지연안내")
    private String schShipDelay;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "지연타입")
    private String schDelayType;

    @ApiModelProperty(notes = "지연일수")
    private String schDelayDay;

    @ApiModelProperty(notes = "마켓유형")
    private String schMarketType;
}
