package kr.co.homeplus.admin.web.partner.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerGetDto;
import kr.co.homeplus.admin.web.partner.model.shipPolicy.PartnerSellerListDto;
import kr.co.homeplus.admin.web.partner.model.shipPolicy.PartnerSellerListParamDto;
import kr.co.homeplus.admin.web.partner.model.shipPolicy.ShipPolicyListDto;
import kr.co.homeplus.admin.web.partner.model.shipPolicy.ShipPolicyParamDto;
import kr.co.homeplus.admin.web.partner.model.shipPolicy.ShipPolicySetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class ShipPolicyController {

    final private ResourceClient resourceClient;
    final private LoginCookieService cookieService;
    final private CodeService codeService;

    public ShipPolicyController(ResourceClient resourceClient,
                                LoginCookieService cookieService,
                                CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }
    /**
     * 업체관리 > 판매업체관리 > 배송정책관리 > Main
     */
    @RequestMapping(value = "shipPolicyMain", method = RequestMethod.GET)
    public String ShipPolicyMain(Model model) throws Exception {

        //팝업창 같이 사용 추가되면 shipPolicyMainPop 해당 메소드에도 추가 해줘야함 
        codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                , "partner_period_type" // 조회기간설정
                , "operator_type"       // 사업자유형
                , "partner_status"      // 회원상태
                , "release_time"        // 출하시간
                , "release_day"         // 출하일
                , "ship_method"         // 배송방법
                , "ship_area"           // 배송가능지역
                , "ship_kind"           // 배송비종류
                , "store_type"          // 점포유형
                , "ship_type"           // 배송유형
                , "holiday_except_yn"   // 휴일여부
                , "safe_number_use_yn"  // 안심번호 사용여부
                , "prepayment_yn"       // 선결제여부
                , "disp_yn"             // 배송비노출여부
        );

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("sellerListGridBaseInfo", PartnerSellerListDto.class));

        model.addAllAttributes(
            RealGridHelper.create("shipPolicyListGridBaseInfo", ShipPolicyListDto.class));

        return "/partner/shipPolicyMain";

    }

    @RequestMapping(value = "/popup/shipPolicyMainPop", method = RequestMethod.GET)
    public String shipPolicyMainPop(Model model, PartnerSellerListParamDto partnerSellerListParamDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String partnerSeller = "";

        codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                , "partner_period_type" // 조회기간설정
                , "operator_type"       // 사업자유형
                , "partner_grade"       // 회원등급
                , "partner_status"      // 회원상태
                , "release_time"        // 출하시간
                , "release_day"         // 출하일
                , "ship_method"         // 배송방법
                , "ship_area"           // 배송가능지역
                , "ship_kind"           // 배송비종류
                , "store_type"          // 점포유형
                , "ship_type"           // 배송유형
                , "holiday_except_yn"   // 휴일여부
                , "safe_number_use_yn"  // 안심번호 사용여부
                , "prepayment_yn"       // 선결제여부
                , "disp_yn"             // 배송비노출여부
        );

        List<PartnerSellerListDto> partnerSellerList = this.getPartnerList(partnerSellerListParamDto);

        if (partnerSellerList.size() > 0) {
            partnerSeller = mapper.writeValueAsString(partnerSellerList.get(0));
        }

        //코드정보
        model.addAttribute("popYn", "Y");
        model.addAttribute("partnerSeller", partnerSeller);

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("sellerListGridBaseInfo", PartnerSellerListDto.class));

        model.addAllAttributes(
            RealGridHelper.create("shipPolicyListGridBaseInfo", ShipPolicyListDto.class));
        return "/partner/shipPolicyMain";
    }

    /**
     * 업체관리 > 판매업체관리 > 배송정책관리 > 판매자리스트 조회
     * @param partnerSellerListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getPartnerList.json"}, method = RequestMethod.POST)
    public List<PartnerSellerListDto> getPartnerList(@RequestBody PartnerSellerListParamDto partnerSellerListParamDto) {
        String apiUrl = "/partner/shipPolicy/getSellerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PartnerSellerListDto>>>(){};
        return (List<PartnerSellerListDto>)resourceClient.postForResponseObject(ResourceRouteName.ITEM
            , partnerSellerListParamDto, apiUrl, typeReference).getData();
    }

    /**
     * 업체관리 > 판매업체관리 > 배송정책관리 > 배송정책 리스트 조회
     * @param shipPolicyParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getShipPolicyList.json"}, method = RequestMethod.GET)
    public List<ShipPolicyListDto> getShipPolicyList(ShipPolicyParamDto shipPolicyParamDto) {
        String apiUrl = "/partner/shipPolicy/getShipPolicyList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ShipPolicyListDto>>>(){};
        return (List<ShipPolicyListDto>)resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUrl, ShipPolicyParamDto.class, shipPolicyParamDto), typeReference).getData();
    }

    /**
     * 업체관리 > 판매업체관리 > 배송정책관리 > 판매자 주소조회
     * @param partnerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getPartnerAddr.json"}, method = RequestMethod.GET)
    public PartnerSellerGetDto getSeller(@RequestParam(name = "partnerId") String partnerId) {
        String apiUri = "/partner/getSeller";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PartnerSellerGetDto>>() {};
        return (PartnerSellerGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?partnerId=" + partnerId , typeReference).getData();
    }

    /**
     * 배송정책 등록/수정
     * @param shipPolicySetDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setShipPolicy.json"}, method = RequestMethod.POST)
    public ResponseResult setShipPolicy(@RequestBody ShipPolicySetDto shipPolicySetDto) {
        String apiUrl = "/partner/shipPolicy/setShipPolicy";

        shipPolicySetDto.setUserId(cookieService.getUserInfo().getEmpId());
        shipPolicySetDto.setDefaultYn(StringUtil.nvl(shipPolicySetDto.getDefaultYn(), "N"));
        shipPolicySetDto
            .setHolidayExceptYn(StringUtil.nvl(shipPolicySetDto.getHolidayExceptYn(),"N"));

        return resourceClient.postForResponseObject(ResourceRouteName.ITEM,  shipPolicySetDto
                , apiUrl, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }
}


