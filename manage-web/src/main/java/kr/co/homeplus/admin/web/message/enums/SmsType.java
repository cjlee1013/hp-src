package kr.co.homeplus.admin.web.message.enums;

import lombok.Getter;

public enum SmsType {

    SMS(0, 80, "S"),
    LMS(100, 2000, "L"),
    MMS(100, 2000, "M");

    @Getter
    private int subjectLength;

    @Getter
    private int bodyLength;

    /**
     * LG U+로 전달되는 MSG_TYPE
     */
    @Getter
    private String typeName;

    SmsType(int subjectLength, int bodyLength, String typeName) {
        this.subjectLength = subjectLength;
        this.bodyLength = bodyLength;
        this.typeName = typeName;
    }
}
