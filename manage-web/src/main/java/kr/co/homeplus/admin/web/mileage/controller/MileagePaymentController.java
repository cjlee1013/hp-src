package kr.co.homeplus.admin.web.mileage.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageReqExcelUploadDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageRequestType;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageCategory;
import kr.co.homeplus.admin.web.mileage.model.type.MileageSaveType;
import kr.co.homeplus.admin.web.mileage.service.MileagePaymentService;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSearchGetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSearchSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentModifySetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageRequestDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageReqDetailDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/mileage/payment")
public class MileagePaymentController {

    private final MileagePaymentService mileagePaymentService;

    public MileagePaymentController(MileagePaymentService mileagePaymentService) {
        this.mileagePaymentService = mileagePaymentService;
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 (SETTLE-6)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/mileageRequestList", method = RequestMethod.GET)
    public String mileageRequestMain(Model model) throws Exception {
        model.addAttribute("schRequestType", Arrays.asList(MileageSaveType.values()));
        model.addAttribute("requestType", Arrays.asList(MileageRequestType.values()));
        model.addAttribute("mileageKind", Arrays.asList(MileageSaveType.values()));
        model.addAllAttributes(RealGridHelper.create("mileageListBaseInfo", MileagePaymentSearchGetDto.class));
        model.addAllAttributes(RealGridHelper.create("mileageDetailBaseInfo", MileageReqDetailDto.class));

        return "/mileage/mileageRequestList";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 메인 리스트 조회
     * @return MileageRequestGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageRequestList.json", method = RequestMethod.GET)
    public List<MileagePaymentSearchGetDto> getMileageRequestList(MileagePaymentSearchSetDto listParamDto) throws Exception {
        return mileagePaymentService.getMileageRequestList(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 요청 등록
     * @return MileageRequestGetDto
     */
    @ResponseBody
    @PostMapping(value = "/setMileageRequest.json")
    public ResponseResult setMileageRequest(@RequestBody MileagePaymentSetDto listParamDto) throws Exception {
        return mileagePaymentService.setMileageRequest(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 요청 수정
     * @return MileageRequestGetDto
     */
    @ResponseBody
    @PostMapping(value = "/modifyMileageRequest.json")
    public ResponseResult modifyMileageRequest(@RequestBody MileagePaymentModifySetDto listParamDto) throws Exception {
        return mileagePaymentService.modifyMileageRequest(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 기본 정보 조회
     * @return MileageRequestDetailSetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageRequestBasic.json", method = RequestMethod.GET)
    public MileageRequestDto getMileageRequestBasic(MileagePaymentModifySetDto listParamDto) throws Exception {
        return mileagePaymentService.getMileageRequestBasic(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 대상 회원 조회
     * @return MileageRequestDetailSetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageRequestDetail.json", method = RequestMethod.GET)
    public List<MileageReqDetailDto> getMileageRequestDetail(MileagePaymentModifySetDto listParamDto) throws Exception {
        return mileagePaymentService.getMileageRequestDetail(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 엑셀 업로드
     * @return String
     */
    @ResponseBody
    @PostMapping(value = "/uploadExcel.json")
    public ResponseObject<Map<String, Object>> uploadExcel(HttpServletRequest httpServletRequest) throws Exception {
        String requestType = httpServletRequest.getParameter("requestType");
        return mileagePaymentService.uploadExcel(httpServletRequest, requestType);
    }
}