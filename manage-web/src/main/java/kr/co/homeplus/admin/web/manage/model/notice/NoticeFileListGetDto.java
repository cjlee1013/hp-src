package kr.co.homeplus.admin.web.manage.model.notice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("공지사항 첨부파일")
public class NoticeFileListGetDto {
    @ApiModelProperty(value = "공지사항 번호")
    private String noticeNo;

    @ApiModelProperty(value = "파일시퀀스")
    private String fileSeq;

    @ApiModelProperty(value = "파일명")
    private String fileName;

    @ApiModelProperty(value = "파일URL")
    private String fileId;

    @ApiModelProperty(value = "사용여부")
    private String useYn;
}
