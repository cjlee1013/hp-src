package kr.co.homeplus.admin.web.voc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 1:1문의 리스트 응답 DTO")
public class OneOnOneInquiryDetailGetDto {
  @ApiModelProperty(notes = "문의번호")
  private String inqryNo;

  @ApiModelProperty(notes = "문의종류")
  private String inqryKind;

  @ApiModelProperty(notes = "처리상태코드")
  private String inqryStatus;

  @ApiModelProperty(notes = "처리상태")
  private String inqryStatusNm;

  @ApiModelProperty(notes = "구분코드")
  private String inqryCategory;

  @ApiModelProperty(notes = "구분")
  private String inqryCategoryNm;

  @ApiModelProperty(notes = "문의구분코드")
  private String inqryType;

  @ApiModelProperty(notes = "문의구분")
  private String inqryTypeNm;

  @ApiModelProperty(notes = "문의항목코드")
  private String inqryDetailType;

  @ApiModelProperty(notes = "문의항목")
  private String inqryDetailTypeNm;

  @ApiModelProperty(notes = "사이트코드")
  private String siteType;

  @ApiModelProperty(notes = "사이트")
  private String siteTypeNm;

  @ApiModelProperty(notes = "점포유형")
  private String storeType;

  @ApiModelProperty(notes = "판매자ID/점포코드")
  private String storeId;

  @ApiModelProperty(notes = "판매자")
  private String storeNm;

  @ApiModelProperty(notes = "회원번호")
  private String userNo;

  @ApiModelProperty(notes = "작성자")
  private String userNm;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "제목")
  private String inqryTitle;

  @ApiModelProperty(notes = "문의일")
  private String inqryDt;

  @ApiModelProperty(notes = "파일첨부여부")
  private String inqryImgYn;

  @ApiModelProperty(notes = "파일첨부1")
  private String inqryImg1;

  @ApiModelProperty(notes = "파일첨부2")
  private String inqryImg2;

  @ApiModelProperty(notes = "파일첨부3")
  private String inqryImg3;

  @ApiModelProperty(notes = "SMS발송여부")
  private String smsAlamYn;

  @ApiModelProperty(notes = "문의내용")
  private String inqryCntnt;

  @ApiModelProperty(notes = "답변내용")
  private String inqryAnswrCntnt;

  @ApiModelProperty(notes = "답변작성일")
  private String inqryAnswrDt;

  @ApiModelProperty(notes = "답변작성자ID")
  private String inqryAnswrRegId;

  @ApiModelProperty(notes = "답변작성자")
  private String inqryAnswrRegNm;

  @ApiModelProperty(notes = "재문의내용")
  private String reInqryCntnt;

  @ApiModelProperty(notes = "재문의답변")
  private String reInqryAnswrCntnt;

  @ApiModelProperty(notes = "재문의답변작성일")
  private String reInqryAnswrDt;

  @ApiModelProperty(notes = "재문의답변자ID")
  private String reInqryAnswrRegId;

  @ApiModelProperty(notes = "재문의답변자")
  private String reInqryAnswrRegNm;

  @ApiModelProperty(notes = "직원메모")
  private String empMemo;

  @ApiModelProperty(notes = "직원메모작성일")
  private String empMemoDt;

  @ApiModelProperty(notes = "직원메모작성자ID")
  private String empMemoId;

  @ApiModelProperty(notes = "직원메모작성자")
  private String empMemoNm;

  @ApiModelProperty(notes = "sms발송시간")
  private String smsDt;

  @ApiModelProperty(notes = "재문의sms발송시간")
  private String reSmsDt;
}