package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;


import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class CategoryListSelectDto {
    @RealGridColumnInfo(headText = "카테고리ID", sortable = true, width = 120)
    private Long scateCd;

    @RealGridColumnInfo(headText = "1depth", sortable = true, width = 150)
    private String lcateNm;

    @RealGridColumnInfo(headText = "2depth", sortable = true, width = 150)
    private String mcateNm;

    @RealGridColumnInfo(headText = "3depth", sortable = true, width = 150)
    private String scateNm;

    @RealGridColumnInfo(headText = "등록여부", sortable = true, width = 150)
    private String resistYn;
}
