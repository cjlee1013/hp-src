package kr.co.homeplus.admin.web.core.interceptor;

import kr.co.homeplus.admin.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.admin.web.core.certification.SessionService;
import kr.co.homeplus.admin.web.core.config.AuthorizationProperties;
import kr.co.homeplus.admin.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.NotAllowedAccessIpException;
import kr.co.homeplus.admin.web.core.exception.SessionNotFoundException;
import kr.co.homeplus.admin.web.core.utility.RemoteUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * 현재 접근하는 사용자의 접근권한을 체크하는 인터셉터 입니다. 체크하는 내용은 아래와 같습니다.<p>
 *     <pre>
 *         1. 현재 접근하는 메뉴의 접근권한을 가지고 있는지 체크하여, 메뉴 접근권한이 없을 경우 'HasNotRoleException'(403 forbidden)을 발생시킵니다.
 *         2. 망분리 메뉴인지 체크하여, 허용되지 않은 아이피로 접근할 경우 'NotAllowedAccessIpException'(403 forbidden)을 발생시킵니다.
 *     </pre>
 *
 * interceptor 순서는 certificationInterceptor 다음으로 실행합니다.
 */
@Slf4j
public class AuthorizationCheckInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthorizationProperties properties;

    private SessionService sessionService;
    public AuthorizationCheckInterceptor (SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * ajax 호출 시 사용하는 url의 suffix 입니다.
     */
    private static final String REST_URI_SUFFIX = ".json";

    /**
     * 접근 권한이 없을 경우 노출할 에러메시지 입니다.
     */
    private static final String HAS_ROLE_EXCEPTION_MESSAGE = "해당 메뉴의 접근권한이 없습니다.";

    /**
     * 망분리 메뉴 접근시 허용되지 않은 아이피로 접근하는 경우 노출할 에러메시지 입니다.
     */
    private static final String NOT_ALLOWED_ACCESS_IP_EXCEPTION_MESSAGE = "이 화면은 VDESK 환경 또는 고객센터망에서만 접속 가능합니다.\n (VDESK 환경은 그룹웨어에서 접근 신청 가능합니다.)";

    /** 업무용 VDESK Ip 대역대 matcher **/
//    private IpAddressMatcher vdiAddressMatcher;

    /** 고객센터 Ip 대역대 matcher **/
//    private IpAddressMatcher customerAddressMatcher;

    /**
     * 생성자 주입을 통해 vdi 아이피 대역대와 고객만족센터 아이피 대역대 정보를 받아 IpAddressMatcher를 생성합니다.
     *
     * @param allowVdiIp 업무용 VDESK 아이피 대역대
     * @param allowCustomerIp 고객센터 아이피 대역대
     */
    public AuthorizationCheckInterceptor(String allowVdiIp, String allowCustomerIp) {
//        vdiAddressMatcher = new IpAddressMatcher(allowVdiIp);
//        customerAddressMatcher = new IpAddressMatcher(allowCustomerIp);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //메뉴 접근권한 체크여부
        if (handler instanceof HandlerMethod && properties.isInterceptorCheckEnable()) {
            userAuthorizationCheck(request, (HandlerMethod) handler);
        }

        return super.preHandle(request, response, handler);
    }

    /**
     * 현재 접근하는 사용자의 접근권한을 체크합니다. 체크하는 내용은 아래와 같습니다.<p>
     *     <pre>
     *         1. 현재 접근하는 메뉴의 접근권한을 가지고 있는지 체크하여, 메뉴 접근권한이 없을 경우 'HasNotRoleException'(403 forbidden)을 발생시킵니다.
     *         2. 망분리 메뉴인지 체크하여, 허용되지 않은 아이피로 접근할 경우 'NotAllowedAccessIpException'(403 forbidden)을 발생시킵니다.
     *     </pre>
     *
     * @param request request
     * @param handler HandlerMethod
     */
    private void userAuthorizationCheck(HttpServletRequest request, HandlerMethod handler) throws NoHandlerFoundException {
        AntPathMatcher antPathMatcher = new AntPathMatcher();

        if (isNotAjaxUri(request.getRequestURI()) && isAuthCheckUri(request.getRequestURI(), antPathMatcher, handler)) {
            // 1.현재 접근하는 url과 매칭되는 권한정보를 추출
            String uriPath = getUriPath(request.getRequestURL().toString());
            AuthMenuListDto authMenuListDto = getAccessMatchedRightsMenuList(this.getUserRightsMenuList(), uriPath);

            if(authMenuListDto != null) {
                // 2.망분리 메뉴 접근시 아이피 체크
                if (properties.isNetworkSeparateAccessCheckEnable()) {
                    //접속자 아이피 추출
                    String clientIp = RemoteUtil.getClientIP(request);

                    if (!isAllowVdiIp(authMenuListDto.getIsAccessVdi(), clientIp) && !isAllowCustomerIp(authMenuListDto.getIsAccessCustomerCenter(), clientIp)) {
                        log.error("Access Denied - Request not allowed from IP address : <{}>, uri : <{}>", clientIp, uriPath);
                        throw new NotAllowedAccessIpException(NOT_ALLOWED_ACCESS_IP_EXCEPTION_MESSAGE);
                    }
                }

                // 3.메뉴 접근권한 체크
                if (authMenuListDto.getIsHold().equals("n")) {
                    log.error("Access Denied - you do not have permission to access this uri : <{}>", uriPath);
                    throw new HasNotRoleException(HAS_ROLE_EXCEPTION_MESSAGE);
                }
            }
        }
    }

    /**
     * request URL이 웹의 메뉴를 접근하는지, ajax 호출을 하는지 체크합니다.<p>
     *
     * requestd의 suffix가 .json 이면 false, 아니면 true 리턴합니다.<br>
     *
     * @param requestUrl request url
     * @return boolean suffix가 .json 이면 false, 아니면 true 리턴합니다.
     */
    private boolean isNotAjaxUri(String requestUrl) {
        return !StringUtils.endsWith(requestUrl, REST_URI_SUFFIX);
    }

    /**
     * 현재 접근하는 URI가 권한체크 대상 URI인지 확인합니다.
     *
     * @param requestUri request URI
     * @param handler
     * @return 권한체크 대상이면 true, 권한체크 무시 대상이면 false를 리턴합니다.
     */
    private boolean isAuthCheckUri(String requestUri, AntPathMatcher antPathMatcher, HandlerMethod handler) {
        // CertificationNeedLess annotation 이 클래스나 메서드에 붙어 있을 경우, 권한체크 무시
        if (handler.getBeanType().isAnnotationPresent(CertificationNeedLess.class) || handler.getMethod().isAnnotationPresent(CertificationNeedLess.class)) {
            return false;
        }

        // 제외대상 uri인지 체크
        for (String ignoreUrl : properties.getIgnoreAuthCheckUris()) {
            if (antPathMatcher.match(ignoreUrl, requestUri)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 접근하는 사용자의 세션에 존재하는 권한정보가 담긴 메뉴 리스트를 리턴한다.
     *
     * @return 사용자의 권한정보가 담긴 메뉴 리스트
     */
    private List<AuthMenuListDto> getUserRightsMenuList() {
        try {
            List<AuthMenuListDto> authMenuListDtos = sessionService.getAmsMenuList();

            if (CollectionUtils.isEmpty(authMenuListDtos)) {
                throw new SessionNotFoundException();
            }

            return authMenuListDtos;
        }
        catch (Exception e) {
            throw new SessionNotFoundException(e);
        }
    }

    /**
     * 망분리 된 메뉴의 경우 접속자의 아이피를 확인하여 허용 된 대역대일 경우 true, 아니면 false를 리턴합니다.
     *
     * @param isAccessVdi 사용자의 권한정보가 담긴 메뉴리스트
     * @param clientIp 접속자 아이피
     * @return 허용 된 대역대일 경우 true, 아니면 false
     */
    private boolean isAllowVdiIp(String isAccessVdi, String clientIp) {
        // VDESK 접속허용 메뉴인지(y)
//        if (isAccessVdi.equals("y")) {
//            log.info("Access Client IP address : <{}>", clientIp);
//            return vdiAddressMatcher.matches(clientIp);
//        }

        return true;
    }

    /**
     * 망분리 된 메뉴의 경우 접속자의 아이피를 확인하여 허용 된 대역대일 경우 true, 아니면 false를 리턴합니다.
     *
     * @param isAccessCustomerCenter 사용자의 권한정보가 담긴 메뉴리스트
     * @param clientIp 접속자 아이피
     * @return 허용 된 대역대일 경우 true, 아니면 false
     */
    private boolean isAllowCustomerIp(String isAccessCustomerCenter, String clientIp) {
        // 고객센터 접속허용 메뉴인지(y)
//        if (isAccessCustomerCenter.equals("y")) {
//            log.info("Access Client IP address : <{}>", clientIp);
//            return customerAddressMatcher.matches(clientIp);
//        }

        return true;
    }

    /**
     * request url에서 uri를 추출하여 리턴합니다.<p>
     * 불필요한 경로 등을(여러개의 슬래시가 들어올 경우 등) 정규화 처리(ex. "//notice/prodNotice///notice")
     *
     * @param requestUrl request url
     * @return uri
     */
    private String getUriPath(String requestUrl) {
        URI uri;
        try {
            uri = new URI(requestUrl);
        }
        catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        //불필요한 경로 등을(여러개의 슬래시가 들어올 경우 등) 정규화 처리(ex. "//notice/prodNotice///notice")
        uri = uri.normalize();
        String uriPath = uri.getPath();

        return uriPath;
    }

    /**
     * 접근하는 URI와 사용자의 권한정보가 담긴 메뉴 리스트 내에 url을 비교하여, 매칭되는 사용자의 권한정보가 담긴 메뉴정보를 추출하여 리턴합니다.<p>
     *
     * 매칭되는 정보가 없을경우 null을 리턴합니다.
     *
     * @param menuList 사용자의 세션에 존재하는 권한정보가 담긴 메뉴 리스트
     * @param uri 접근하는 request URI
     * @return 사용자의 권한정보가 담긴 메뉴정보를 리턴합니다. 매칭되는 정보가 없을 경우 null을 리턴합니다.
     */
    private AuthMenuListDto getAccessMatchedRightsMenuList(List<AuthMenuListDto> menuList, String uri) {
        return menuList.stream().filter(depth1Menus -> !CollectionUtils.isEmpty(depth1Menus.getChildMenuList())).
                flatMap(depth1Menus -> depth1Menus.getChildMenuList().stream()).filter(depth2Menus -> !CollectionUtils.isEmpty(depth2Menus.getChildMenuList())).
                flatMap(depth2Menus -> depth2Menus.getChildMenuList().stream()).filter(depth3Menu -> depth3Menu.getMenuUrl().equals(uri)).findFirst().orElse(null);
    }


}