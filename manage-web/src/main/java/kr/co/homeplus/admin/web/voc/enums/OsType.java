package kr.co.homeplus.admin.web.voc.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum OsType implements RealGridLookUpSupport {

    ANDROID("Android"),
    IOS("iOS");

    private static final Map<String, String> CASE_NAME_MAP =
        Stream.of(values()).collect(Collectors.toMap(OsType::name, OsType::getCaseName));

    @Getter
    private final String caseName;

    OsType(String caseName) {
        this.caseName = caseName;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return CASE_NAME_MAP;
    }
}
