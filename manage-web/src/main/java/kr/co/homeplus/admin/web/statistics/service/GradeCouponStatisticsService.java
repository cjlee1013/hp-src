package kr.co.homeplus.admin.web.statistics.service;


import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.GradeCouponStatisticsListGetDto;
import kr.co.homeplus.admin.web.statistics.model.GradeCouponStatisticsListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class GradeCouponStatisticsService {

    private final ResourceClient resourceClient;

    /**
     * 등급별 쿠폰 통계 리스트 조회
     * **/
    public List<GradeCouponStatisticsListGetDto> getGradeCouponStatisticsList(
        GradeCouponStatisticsListSetDto gradeCouponStatisticsListSetDto) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            gradeCouponStatisticsListSetDto,
            "/statistics/promotion/getGradeCouponStatisticsList",
            new ParameterizedTypeReference<ResponseObject<List<GradeCouponStatisticsListGetDto>>>() {}
        ).getData();
    }

}
