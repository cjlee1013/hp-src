package kr.co.homeplus.admin.web.item.model.item;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
//셀러상품 등록/수정 Get Entry
public class ItemListGetDto {

    //상품번호
    @RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNo;

    //상품명
    @RealGridColumnInfo(headText = "상품명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true, width = 300)
    private String itemNm;

    //parent id
    @RealGridColumnInfo(headText = "parent id", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true, sortable = true)
    private String itemParent;

    //판매가격
    @RealGridColumnInfo(headText = "판매가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, sortable = true)
    private String salePrice;

    //할인가격
    @RealGridColumnInfo(headText = "할인가격", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.PRICE, sortable = true)
    private String dcPrice;

    //연관번호
    @RealGridColumnInfo(headText = "연관번호", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.BASIC, sortable = true)
    private String relationNo;

    //업체명
    @RealGridColumnInfo(headText = "판매업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String businessNm;

    //판매자 상품코드
    @RealGridColumnInfo(headText = "판매자 상품코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String sellerItemCd;

    //판매시작일
    @RealGridColumnInfo(headText = "판매시작일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String saleStartDt;

    //판매종료일
    @RealGridColumnInfo(headText = "판매종료일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String saleEndDt;

    //상품구분
    @RealGridColumnInfo(headText = "상품구분", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemDivision;

    //상품유형명
    @RealGridColumnInfo(headText = "상품유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemTypeNm;

    //상품상태명
    @RealGridColumnInfo(headText = "상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemStatusNm;

    //상품상태
    @RealGridColumnInfo(headText = "상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String itemStatus;

    //노출여부
    @RealGridColumnInfo(headText = "노출여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String dispYnNm;

    //대분류 카테고리명
    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String lcateNm;

    //중분류 카테고리명
    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String mcateNm;

    //소분류 카테고리명
    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String scateNm;

    //세분류 카테고리명
    @RealGridColumnInfo(headText = "세분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String dcateNm;

    //배송속성명`
    private String shipAttrNm;

    //등록자
    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String regNm;

    //등록일
    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String regDt;

    //수정자
    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    //수정일
    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgDt;
}
