package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.ZipcodeOrderStatistics;
import kr.co.homeplus.admin.web.statistics.model.ZipcodeOrderStatisticsSelect;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ZipcodeOrderStatisticsService {

    private final ResourceClient resourceClient;

    /**
     * 우편번호별 주문현황 리스트 조회
     *
     * @param zipcodeOrderStatisticsSelect
     * @return
     */
    public List<ZipcodeOrderStatistics> getZipcodeOrderStatisticsList(ZipcodeOrderStatisticsSelect zipcodeOrderStatisticsSelect) {
        var typeReference = new ParameterizedTypeReference<ResponseObject<List<ZipcodeOrderStatistics>>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, zipcodeOrderStatisticsSelect, EscrowConstants.ESCROW_GET_ZIPCODE_ORDER_STATISTICS_LIST, typeReference).getData();
    }

    /**
     * 우편번호별 주문현황 엑셀다운
     * - 단순 history 적재용
     */
    public void saveExtractHistory(ExtractCommonSetDto extractCommonSetDto) {
        resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, extractCommonSetDto, EscrowConstants.EXTRACT_SAVE_EXTRACT_HISTORY);
    }
}
