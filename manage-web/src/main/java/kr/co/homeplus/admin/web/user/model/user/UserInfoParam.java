package kr.co.homeplus.admin.web.user.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.user.enums.UserInfoSearchType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@ApiModel(description = "회원정보 관리 조회 파라미터")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoParam {
    @ApiModelProperty(value = "검색조건")
    private UserInfoSearchType searchType;

    @ApiModelProperty(value = "검색어")
    private String keyword;
}
