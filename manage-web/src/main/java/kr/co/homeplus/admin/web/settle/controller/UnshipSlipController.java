package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.admin.web.settle.service.UnshipSlipService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/unshipSlip")
public class UnshipSlipController {

    @Autowired
    private UnshipSlipService unshipSlipService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * TD미배송 전표조회 Main Page
     */
    @RequestMapping(value = "/tdUnshipSlipMain", method = RequestMethod.GET)
    public String tdUnshipSlipMain(Model model) throws Exception{
        model.addAttribute("storeType", settleCommonService.getCode("store_type"));
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("tdUnshipSlipGridBaseInfo", TdUnshipSlipListGetDto.class));

        return "/settle/tdUnshipSlipMain";
    }

    /**
     * TD미배송 전표조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTdUnshipSlipList.json", method = RequestMethod.GET)
    public List<TdUnshipSlipListGetDto> getTdUnshipSlipList(TdUnshipSlipListSetDto tdUnshipSlipListSetDto) throws Exception{
        return unshipSlipService.getTdUnshipSlipList(tdUnshipSlipListSetDto);
    }

    /**
     * 전표 생성 가능 여부
     */
    @ResponseBody
    @RequestMapping(value = "/getTdUnshipSlipCnt.json", method = RequestMethod.GET)
    public int getTdUnshipSlipCnt(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception{
        return unshipSlipService.getTdUnshipSlipCnt(tdUnshipSlipCreateSetDto);
    }

    /**
     * 전표 생성
     */
    @ResponseBody
    @PostMapping(value = "/setTdUnshipSlip.json")
    public ResponseObject<Object> setTdUnshipSlip(@RequestBody TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception {
        return unshipSlipService.setTdUnshipSlip(tdUnshipSlipCreateSetDto);
    }

}