package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.recipe.RecipeDetailGetDto;
import kr.co.homeplus.admin.web.item.model.recipe.RecipeItemGetDto;
import kr.co.homeplus.admin.web.item.model.recipe.RecipeListGetDto;
import kr.co.homeplus.admin.web.item.model.recipe.RecipeListParamDto;
import kr.co.homeplus.admin.web.item.model.recipe.RecipeSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/recipe")
public class RecipeController {
    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 상품관리 > 점포상품관리 > 레시피관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/recipeMain", method = RequestMethod.GET)
    public String recipesMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "recipe_sch_type"        // 레시피검색어타임
            ,       "recipe_disp_yn" //레시피노출여부
            ,       "recipe_grade"  //레시피 난이도
        );
        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("searchType", code.get("recipe_sch_type"));
        model.addAttribute("dipsYn", code.get("recipe_disp_yn"));
        model.addAttribute("grade", code.get("recipe_grade"));

        model.addAllAttributes(RealGridHelper.create("recipeListGridBaseInfo", RecipeListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("recipeItemListGridBaseInfo", RecipeItemGetDto.class));

        return "/item/recipeMain";
    }
    /**
     * 상품관리 > 점포상품관리 > 레시피리스트
     *
     * @param listParamDto
     * @return NoticeListParamDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getRecipeList.json"}, method = RequestMethod.GET)
    public List<RecipeListGetDto> getRecipeList(RecipeListParamDto listParamDto) {

        String apiUri = "/item/recipe/getRecipeList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RecipeListGetDto>>>() {};


        return  (List<RecipeListGetDto>) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM, StringUtil
                .getRequestString(apiUri, RecipeListParamDto.class, listParamDto), typeReference).getData();
    }
    /**
     * 상품관리 > 점포상품관리 > 레시피 상세
     *
     * @param recipeNo
     * @return NoticeListParamDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getRecipeDetail.json"}, method = RequestMethod.GET)
    public RecipeDetailGetDto getRecipeDetail(String recipeNo) {

        String apiUri = "/item/recipe/getRecipeDetail?recipeNo=";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<RecipeDetailGetDto>>() {};
        return (RecipeDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM,apiUri+recipeNo, typeReference).getData();
    }
    /**
     * 상품관리 > 점포상품관리 > 레시피 등록/수정
     * @param recipeSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setRecipe.json"}, method = RequestMethod.POST)
    public ResponseResult setRecipe(@RequestBody RecipeSetParamDto recipeSetParamDto) throws Exception {

        String apiUri = "/item/recipe/setRecipe";
        recipeSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, recipeSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }
}
