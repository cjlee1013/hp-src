package kr.co.homeplus.admin.web.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.admin.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.config.AdminProperties;
import kr.co.homeplus.admin.web.core.config.AuthorizationProperties;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.exception.LoginCookieServiceException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.impl.MaintenanceException;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 인증 체크를 위한 인터셉터
 */
@Slf4j
@RequiredArgsConstructor
public class CertificationInterceptor extends HandlerInterceptorAdapter {

    private final LoginCookieService cookieService;
    private final UserMenuService userMenuService;

    private final AdminProperties adminProperties;
    private final AuthorizationProperties authorizationProperties;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
        final Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return super.preHandle(request, response, handler);
        }

        isMaintenancePageDisplay(request.getRequestURI());

        if (isCertificationNeedLess((HandlerMethod) handler)) {
            //로그인 상태에서 로그인 페이지로 접근 시 home 으로 리다이렉트 처리.
            if (isAccessLoginPage(request.getRequestURI())) {
                log.debug("access login page!, sendRedirect start page.");
                response.sendRedirect(adminProperties.getStartUrl());
            }
            return super.preHandle(request, response, handler);
        }

        //인증 체크
        if (isNotLogin()){
            log.warn("loginCookie not found! sendRedirect login page.<uri:{}>", request.getRequestURI());
            throw new LoginCookieServiceException(ExceptionCode.SYS_ERROR_CODE_9207.getDesc());
        }

        // view 에서 사용할 정보 세팅.
        setUserInfoToRequest(request);

        return super.preHandle(request, response, handler);
    }

    /**
     * 로그인 상태에서 로그인 페이지 접근여부를 체크합니다.
     * @param requestUri request URI
     * @return 로그인 상태에서 로그인 페이지 접근할 경우 true, 아닐 경우 false를 리턴합니다.
     */
    private boolean isAccessLoginPage(final String requestUri) {
        return cookieService.isLogin() && adminProperties.getCertificationRedirectUrl()
            .equals(requestUri);
    }

    /**
     * 로그인 상태가 아닌지 체크합니다.
     * @return 로그인 상태가 아닐 경우 true, 로그인 상태의 경우 fasle를 리턴 합니다.
     */
    private boolean isNotLogin() {
        return !cookieService.isLogin();
    }

    /**
     * 점검중 페이지 노출여부
     *
     * @param requestUri request URI
     * @exception MaintenanceException  점검중 페이지 Exception
     */
    private void isMaintenancePageDisplay(final String requestUri) {
        if (adminProperties.isMaintenancePageDisplayEnable()) {
            //lbStatusCheck url을 제외한 나머지 URI 접근시 점검중 페이지로 노출합니다.
            AntPathMatcher antPathMatcher = new AntPathMatcher();
            if (!antPathMatcher.match("/lbStatusCheck", requestUri)) {
                throw new MaintenanceException();
            }
        }
    }

    /**
     * <p>
     * {@link CertificationNeedLess} annotation 태깅 여부를 체크합니다.
     * </p>
     * 해당 어노테이션이 클래스나 메서드에 붙어 있을 경우 로그인 인증을 체크하지 않습니다.
     *
     * @param handlerMethod HandlerMethod
     * @return boolean
     */
    private boolean isCertificationNeedLess(final HandlerMethod handlerMethod) {
        return handlerMethod.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || handlerMethod.getMethod().isAnnotationPresent(CertificationNeedLess.class);
    }

    /**
     * 공통으로 사용할 정보를 request 에 추가.
     *
     * @param request HttpServletRequest
     */
    private void setUserInfoToRequest(final HttpServletRequest request) {
        // 유저 기본 정보.
        UserInfo userInfo = cookieService.getUserInfo();
        request.setAttribute("userInfo", userInfo);

        //사용자 인가 된 메뉴정보 목록
        request.setAttribute("menuList", userMenuService.getAuthorizedMenus(userInfo.getEmpId()));

        //로그인 만료시간 정보
        request.setAttribute("loginExpirationMin", adminProperties.getLoginExpirationMin());

        //메뉴 권한체크 활성화 여부
        request.setAttribute("checkEnable", authorizationProperties.isMenuCheckEnable());

        //'상세 탭 페이지' URL 목록, LNB 노출을 막기 위함
        request.setAttribute("lnbHideMenuList", userMenuService.getDetailTabPageList());
    }
}