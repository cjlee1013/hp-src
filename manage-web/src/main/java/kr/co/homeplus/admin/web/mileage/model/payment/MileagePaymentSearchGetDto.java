package kr.co.homeplus.admin.web.mileage.model.payment;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileagePaymentSearchGetDto {
    @ApiModelProperty(value = "번호", position = 1)
    @RealGridColumnInfo(headText = "번호", width = 100)
    private String mileageReqNo;

    @ApiModelProperty(value = "구분", position = 2)
    @RealGridColumnInfo(headText = "구분", width = 100, hidden = true)
    private String requestType;

    @ApiModelProperty(value = "구분(NM)", position = 2)
    @RealGridColumnInfo(headText = "구분", width = 120)
    private String requestTypeNm;

    @ApiModelProperty(value = "사유", position = 3)
    @RealGridColumnInfo(headText = "사유", width = 150)
    private String requestReason;

    @ApiModelProperty(value = "마일리지유형", position = 4)
    @RealGridColumnInfo(headText = "마일리지유형", width = 120)
    private String mileageTypeName;

    @ApiModelProperty(value = "대상수", position = 5)
    @RealGridColumnInfo(headText = "대상수", width = 90, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String requestCnt;

    @ApiModelProperty(value = "대상금액", position = 6)
    @RealGridColumnInfo(headText = "대상금액", width = 110, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String requestAmt;

    @ApiModelProperty(value = "상태", position = 7)
    @RealGridColumnInfo(headText = "상태", width = 100)
    private String requestStatus;

    @ApiModelProperty(value = "실행예정일", position = 8)
    @RealGridColumnInfo(headText = "실행예정일", width = 100)
    private String processDt;

    @ApiModelProperty(value = "완료일", position = 9)
    @RealGridColumnInfo(headText = "완료일", width = 100)
    private String completeDt;

    @ApiModelProperty(value = "완료건수", position = 10)
    @RealGridColumnInfo(headText = "완료건수", width = 90, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeCnt;

    @ApiModelProperty(value = "등록일", position = 11)
    @RealGridColumnInfo(headText = "등록일", width = 120)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 12)
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 13)
    @RealGridColumnInfo(headText = "수정일", width = 120)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 14)
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgId;
}
