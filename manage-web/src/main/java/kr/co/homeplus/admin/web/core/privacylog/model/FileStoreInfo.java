package kr.co.homeplus.admin.web.core.privacylog.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class FileStoreInfo {
    private String eTag;
    private String fileName;
    private String fileId;
    private Integer width;
    private Integer height;
}
