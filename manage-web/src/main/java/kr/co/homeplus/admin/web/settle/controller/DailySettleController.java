package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import java.util.Arrays;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.SettleMallType;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleSumGetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleInfoGetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleInfoSetDto;
import kr.co.homeplus.admin.web.settle.service.DailySettleService;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.dailySettle.DailySettleListSetDto;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/dailySettle")
public class DailySettleController {

    private final DailySettleService dailySettleService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public DailySettleController(DailySettleService dailySettleService, SettleCommonService settleCommonService, CodeService codeService) {
        this.dailySettleService = dailySettleService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산관리 > 정산관리 > 매출조회 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dailySettleList", method = RequestMethod.GET)
    public String dailySettleList(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mallType", Arrays.asList(SettleMallType.values()));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("dailySettleListBaseInfo", DailySettleListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dailySettleDetailBaseInfo", DailySettleInfoGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dailySettleSumBaseInfo", DailySettleSumGetDto.class));

        return "/settle/dailySettleList";
    }

    /**
     * 정산관리 > 정산관리 > 매출조회 > 메인 리스트 조회
     * @return DailySettleListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettleList.json", method = RequestMethod.GET)
    public List<DailySettleListGetDto> getDailySettleList(DailySettleListSetDto listParamDto) throws Exception {
        return dailySettleService.getDailySettleList(listParamDto);
    }

    /**
     * 정산관리 > 정산관리 > 매출조회 > 파트너 상세 매출 조회
     * @return DailySettleInfoGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettleInfo.json", method = RequestMethod.GET)
    public List<DailySettleInfoGetDto> getDailySettleInfo(DailySettleInfoSetDto listParamDto) throws Exception {
        return dailySettleService.getDailySettleInfo(listParamDto);
    }

    /**
     * 정산관리 > 정산관리 > 매출조회 > 메인 리스트 조회
     * @return DailySettleListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettleSum.json", method = RequestMethod.GET)
    public List<DailySettleSumGetDto> getDailySettleSum(DailySettleListSetDto listParamDto) throws Exception {
        return dailySettleService.getDailySettleSum(listParamDto);
    }
}