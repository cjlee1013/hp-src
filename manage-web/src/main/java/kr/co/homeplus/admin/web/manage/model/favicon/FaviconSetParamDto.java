package kr.co.homeplus.admin.web.manage.model.favicon;

import java.util.List;
import lombok.Data;

@Data
public class FaviconSetParamDto {

    //파비콘 관리 고유번호
    private Long faviconNo;

    //전시위치
    private String siteType;

    //전시위치(PC,IOS,AOS)
    private String device;

    //사용여부
    private String useYn;

    //전시기간시작
    private String dispStartDt;

    //전시기간종료
    private String dispEndDt;

    //배너명ƒf
    private String bannerNm;

    //등록/수정자
    private String userId;

    //파비콘이미지
    List<FaviconImgSetParamDto> faviconImg;
}

