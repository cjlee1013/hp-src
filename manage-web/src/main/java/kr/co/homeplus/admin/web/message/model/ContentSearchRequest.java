package kr.co.homeplus.admin.web.message.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class ContentSearchRequest {

    private String schKeyword;

    private String storeType;

    private String affiliationType;

    private String workType;

    public void setSchKeyword(String schKeyword) {
        this.schKeyword = StringUtils.trim(schKeyword);
    }

    public void setStoreType(String storeType) {
        this.storeType = StringUtils.isEmpty(storeType) ? null : storeType;
    }

    public void setAffiliationType(String affiliationType) {
        this.affiliationType = StringUtils.isEmpty(affiliationType) ? null : affiliationType;
    }

    public void setWorkType(String workType) {
        this.workType = StringUtils.isEmpty(workType) ? null : workType;
    }
}
