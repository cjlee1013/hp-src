package kr.co.homeplus.admin.web.item.model.onedayItem;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnedayItemExcelDto {

    @EqualsAndHashCode.Include
    private String dispDt;
    @EqualsAndHashCode.Include
    private String siteType;
    @EqualsAndHashCode.Include
    private String itemStoreType;
    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String itemNm;
    @EqualsAndHashCode.Include
    private String dispYn;
    @EqualsAndHashCode.Include
    private String dispYnTxt;

}
