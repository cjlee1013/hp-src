package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbListSetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbStoreListGetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbDailyListGetDto;
import kr.co.homeplus.admin.web.settle.model.ocb.PgOcbTidListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgOcbPayService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 지점별 실적조회
     */
    public List<PgOcbStoreListGetDto> getStorePgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/ocb/getStorePgOcbPay",
            new ParameterizedTypeReference<ResponseObject<List<PgOcbStoreListGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 일별 실적조회
     */
    public List<PgOcbDailyListGetDto> getDailyPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/ocb/getDailyPgOcbPay",
            new ParameterizedTypeReference<ResponseObject<List<PgOcbDailyListGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 운영관리 > OCB실적조회 > 건별 실적조회
     */
    public List<PgOcbTidListGetDto> getTidPgOcbPay(PgOcbListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/ocb/getTidPgOcbPay",
            new ParameterizedTypeReference<ResponseObject<List<PgOcbTidListGetDto>>>(){}).getData();
    }

}
