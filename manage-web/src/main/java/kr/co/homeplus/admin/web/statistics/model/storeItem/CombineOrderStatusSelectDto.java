package kr.co.homeplus.admin.web.statistics.model.storeItem;

import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import lombok.Data;
import org.springframework.boot.autoconfigure.session.StoreType;

@Data
public class CombineOrderStatusSelectDto {
    String schOrderStartDt;
    String schOrderEndDt;
    String schShipReqStartDt;
    String schShipReqEndDt;
    String schStoreType;
    int schStoreId;

    /* 추출 히스토리 저장용 DTO */
    ExtractCommonSetDto extractCommonSetDto;
}
