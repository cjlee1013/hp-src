package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class PromoItemGetDto {

    @RealGridColumnInfo(headText = "상품번호", order = 1, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 300, columnType = RealGridColumnType.NAME)
    private String itemNm;

    @RealGridColumnInfo(headText = "상품상태")
    private String itemStatus;

    @RealGridColumnInfo(headText = "대분류")
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류")
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류")
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류")
    private String dcateNm;

    @RealGridColumnInfo(headText = "우선순위", hidden = true)
    private Integer priority;
}