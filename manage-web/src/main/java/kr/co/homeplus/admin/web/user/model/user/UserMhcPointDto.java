package kr.co.homeplus.admin.web.user.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(builderClassName = "UserMhcPointDtoBuilder", toBuilder = true)
@JsonDeserialize(builder = UserMhcPointDto.UserMhcPointDtoBuilder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserMhcPointDto {
    private String returnCode;
    private String returnMessage;
    /**
     * MHC 보유 포인트
     */
    private long totOwnPoint;
    /**
     * MHC 사용가능 포인트
     */
    private long totAvlPoint;

    @JsonPOJOBuilder(withPrefix = "")
    public static class UserMhcPointDtoBuilder {
    }
}
