package kr.co.homeplus.admin.web.promotion.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class EventSelectBoxInfoGet {
    private String eventCode;
    private String eventName;
}
