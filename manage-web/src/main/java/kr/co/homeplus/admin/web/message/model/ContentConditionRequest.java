package kr.co.homeplus.admin.web.message.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.message.enums.AffiliationType;
import kr.co.homeplus.admin.web.message.enums.StoreType;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ContentConditionRequest {

    @ApiModelProperty(value = "발송주체", required = true)
    private AffiliationType affiliationType;

    @ApiModelProperty(value = "점포유형", required = true)
    private StoreType storeType;

    @ApiModelProperty(value = "채널타입", required = true)
    private WorkType workType;

    @ApiModelProperty(value = "기타컬럼 검색1")
    private String etc1;

    @ApiModelProperty(value = "기타컬럼 검색2")
    private String etc2;

    @ApiModelProperty(value = "기타컬럼 검색3")
    private String etc3;

    @ApiModelProperty(value = "기타컬럼 검색4")
    private String etc4;

    @ApiModelProperty(value = "기타컬럼 검색5")
    private String etc5;
}