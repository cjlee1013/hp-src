package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoItemSetDto {

    // 상품번호
    private String itemNo;

    // 우선순위
    private Integer priority;
}