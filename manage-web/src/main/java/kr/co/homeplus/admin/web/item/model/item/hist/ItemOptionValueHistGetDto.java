package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.Optional;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemOptionValueHistGetDto {
	// 옵션번호
	private String optNo;
	// 옵션1값
	private String opt1Val;
	// 옵션2값
	private String opt2Val;
	// 우선순위
	private String priority;
	// 노출여부 (Y:노출, N:비노출)
	private String dispYn;
	// 노출여부명
	private String dispYnNm;

    @Override
    public String toString() {
        return Optional.of(
        		  this.optNo
        		+ this.opt1Val
				+ this.opt2Val
				+ this.dispYn
			  	+ this.dispYnNm
				+ this.priority
		).toString();
    }
}
