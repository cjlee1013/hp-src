package kr.co.homeplus.admin.web.item.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("상품 연동 히스토리")
public class MarketItemStatusDetailGetDto {

	@ApiModelProperty("상품번호")
	private String itemNo;

	@ApiModelProperty("상품명")
	private String itemNm;

	@ApiModelProperty("홈플러스 상품상태")
	private String itemStatusNm;

	@ApiModelProperty("홈플러스 상품상태")
	private String itemStatus;

	@ApiModelProperty("연동사이트")
	private String marketNm;

	@ApiModelProperty("연동사이트 상품번호")
	private String marketItemNo;

	@ApiModelProperty("연동사이트 상품상태")
	private String marketItemStatusNm;

	@ApiModelProperty("연동사이트 상품상태")
	private String marketItemStatus;

	@ApiModelProperty("연동여부")
	private String sendYnNm;

	@ApiModelProperty("연동여부")
	private String sendYn;

	@ApiModelProperty("마켓연동ID")
	private String partnerId;

	@ApiModelProperty("대분류")
	private String lcateNm;

	@ApiModelProperty("중분류")
	private String mcateNm;

	@ApiModelProperty("소분류")
	private String scateNm;

	@ApiModelProperty("세분류")
	private String dcateNm;

	@ApiModelProperty("세카테고리번호")
	private String dcateCd;

	@ApiModelProperty("마켓연동카테고리")
	private String marketCateCd;

	@ApiModelProperty("마켓연동 대카테고리")
	private String marketLcateNm;

	@ApiModelProperty("마켓연동 중카테고리")
	private String marketMcateNm;

	@ApiModelProperty("마켓연동 소카테고리")
	private String marketScateNm;

	@ApiModelProperty("마켓연동 세카테고리")
	private String marketDcateNm;

	@ApiModelProperty("연동결과")
	private String sendMsg;

	@ApiModelProperty(value = "판매가격")
	private long salePrice;

	@ApiModelProperty(value = "심플적용가")
	private long simplePrice;

	@ApiModelProperty(value = "쿠폰적용가(심플포함)")
	private long couponPrice;

	@ApiModelProperty("마스터 상품 전시여부")
	private String masterDispYn;

	@ApiModelProperty("마스터 상품 프로모션 대상 여부")
	private String masterPromoYn;

	@ApiModelProperty("마스터 상품 쿠폰 대상 여부")
	private String masterCouponYn;

	@ApiModelProperty("행사코드")
	private String customCd;

	@ApiModelProperty("상태메시지")
	private String msg;

	@ApiModelProperty("지점별 가격전송 정보")
	private List<MarketItemSaleStatusGetDto> storeList;



}