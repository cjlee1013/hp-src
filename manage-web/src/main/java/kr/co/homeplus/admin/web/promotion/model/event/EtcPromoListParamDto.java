package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EtcPromoListParamDto {
    private String schDateType;

    private String schStartDt;

    private String schEndDt;

    private String schStoreType;

    private String schUseYn;

    private String schPromoType;

    private String schType;

    private String schValue;
}