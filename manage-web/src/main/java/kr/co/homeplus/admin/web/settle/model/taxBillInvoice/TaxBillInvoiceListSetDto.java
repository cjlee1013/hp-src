package kr.co.homeplus.admin.web.settle.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
public class TaxBillInvoiceListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "담당자명")
    private String mngNm;

    @ApiModelProperty(value = "담당자이메일")
    private String mngEmail;

    @ApiModelProperty(value = "세금계산서발행타입")
    private String taxBillReceiveType;
}
