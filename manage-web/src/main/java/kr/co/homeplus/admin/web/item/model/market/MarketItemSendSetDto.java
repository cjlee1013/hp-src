package kr.co.homeplus.admin.web.item.model.market;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketItemSendSetDto {

	//상품번호
	private List<String> itemList;

}
