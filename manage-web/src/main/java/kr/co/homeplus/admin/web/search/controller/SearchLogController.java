package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import kr.co.homeplus.admin.web.search.model.SearchLogGridDto;
import kr.co.homeplus.admin.web.search.model.SearchLogResult;
import kr.co.homeplus.admin.web.search.service.SearchLogService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/statistics")
@RequiredArgsConstructor
public class SearchLogController {

    private final SearchLogService searchLogService;

    @GetMapping(value = "/successLog")
    public String SuccessLogView(Model model) {
        model.addAttribute("logType", "success");
        model.addAllAttributes(RealGridHelper.create("searchLogGridBaseInfo", SearchLogGridDto.SearchLog.class));
        return "/search/searchLog";
    }

    @GetMapping(value = "/successExpLog")
    public String SuccessExpLogView(Model model) {
        model.addAttribute("logType", "success");
        model.addAllAttributes(RealGridHelper.create("searchLogGridBaseInfo", SearchLogGridDto.SearchLog.class));
        return "/search/searchExpLog";
    }

    @GetMapping(value = "/failureLog")
    public String FailureLogView(Model model) {
        model.addAttribute("logType", "failure");
        model.addAllAttributes(RealGridHelper.create("searchLogGridBaseInfo", SearchLogGridDto.SearchLog.class));
        return "/search/searchLog";
    }

    @ResponseBody
    @GetMapping(value = "/failureLogRelatedWord.json")
    public List<String> FailureLogRelatedWordList(@RequestParam String failureKeyword) {
        return searchLogService.getFailureRelatedWord(failureKeyword);
    }

    @GetMapping(value = "/failureExpLog")
    public String FailureExpLogView(Model model) {
        model.addAttribute("logType", "failure");
        model.addAllAttributes(RealGridHelper.create("searchLogGridBaseInfo", SearchLogGridDto.SearchLog.class));
        return "/search/searchExpLog";
    }

    @ResponseBody
    @GetMapping(value= {"/getSearchlog.json"})
    @SuppressWarnings("unchecked")
    public List<SearchLogResult> getSearchLog(@RequestParam String logType, @RequestParam String startDt, @RequestParam String endDt, @RequestParam String siteType, @RequestParam String searchKeyword) {
        return (List<SearchLogResult>)searchLogService.getSearchLog(logType, startDt, endDt, siteType, searchKeyword).getData();
    }
}