package kr.co.homeplus.admin.web.statistics.controller;

import static kr.co.homeplus.admin.web.item.enums.StoreType.EXP;
import static kr.co.homeplus.admin.web.item.enums.StoreType.HYPER;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.statistics.model.OrderSubstitutionStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.OrderSubstitutionStatisticsSetDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.admin.web.statistics.service.OrderSubstitutionStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/promotion")
@RequiredArgsConstructor
@Slf4j
public class OrderSubstitutionStatisticsController {
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;
    private final OrderSubstitutionStatisticsService orderSubstitutionStatisticsService;

    @GetMapping("/orderSubstitutionStatisticsMain")
    public String orderSubstitutionStatisticsMain(Model model) throws Exception {
        List<MngCodeGetDto> storeTypeList = new ArrayList<>();
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();
        StoreBasicInfoDto storeBasicInfoDto = new StoreBasicInfoDto();

        // 로그인 사용자 점포 ID 있을 경우 점포 기본정보 조회
        String userStoreId = loginCookieService.getUserInfo().getStoreId();
        if (StringUtil.isNotEmpty(userStoreId)) {
            storeBasicInfoDto = orderSubstitutionStatisticsService.getStoreBasicInfo(userStoreId);
        }
        // 조회한 점포 기본정보가 있을 경우 해당 점포만 세팅, 없을 경우 점포유형 HYPER/EXP 세팅
        if (StringUtil.isNotEmpty(storeBasicInfoDto) && StringUtil.isNotEmpty(storeBasicInfoDto.getStoreId())) {
            mngCodeGetDto.setMcCd(storeBasicInfoDto.getStoreType());
            mngCodeGetDto.setMcNm(storeBasicInfoDto.getStoreType());
            storeTypeList.add(mngCodeGetDto);
            model.addAttribute("userStoreId", storeBasicInfoDto.getStoreId());
            model.addAttribute("userStoreNm", storeBasicInfoDto.getStoreNm());
        } else {
            Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");
            storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> HYPER.getType().equals(st.getMcCd()) || EXP.getType().equals(st.getMcCd()))
                .collect(Collectors.toList());
        }

        model.addAttribute("storeType", storeTypeList);
        model.addAllAttributes(RealGridHelper.createForGroup("orderSubstitutionStatisticsGridBaseInfo", OrderSubstitutionStatisticsGetDto.class));
        return "/statistics/orderSubstitutionStatisticsMain";
    }

    @ResponseBody
    @PostMapping("/getOrderSubstitutionInfo.json")
    public List<OrderSubstitutionStatisticsGetDto> getOrderSubstitutionInfo(@RequestBody OrderSubstitutionStatisticsSetDto setDto) {
        return orderSubstitutionStatisticsService.getOrderSubstitutionInfo(setDto);
    }
}
