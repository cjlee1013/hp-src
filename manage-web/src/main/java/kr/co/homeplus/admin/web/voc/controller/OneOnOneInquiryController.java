package kr.co.homeplus.admin.web.voc.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryListGetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryListSetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryMemberMemoListGetDto;
import kr.co.homeplus.admin.web.voc.service.OneOnOneInquiryService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 회원관리 > 고객문의관리 > 1:1문의
 */
@Controller
@RequestMapping("/voc")
public class OneOnOneInquiryController {
    @Autowired
    private OneOnOneInquiryService oneOnOneInquiryService;
    @Autowired
    private ShipCommonService shipCommonService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    public OneOnOneInquiryController() {
    }

    /**
     * 1:1문의 Main Page
     */
    @GetMapping("/oneOnOneInquiry/oneOnOneInquiryMain")
    public String oneOnOneInquiryMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("siteType", shipCommonService.getCode("site_type"));
        model.addAttribute("inqryCategory", shipCommonService.getCode("inqry_category"));
        model.addAttribute("inqryType", shipCommonService.getCode("inqry_type"));
        model.addAttribute("inqryDetailType", shipCommonService.getCode("inqry_detail_type"));
        model.addAttribute("inqryStatus", shipCommonService.getCode("inqry_status"));
        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAllAttributes(RealGridHelper.create("oneOnOneInquiryGridBaseInfo", OneOnOneInquiryListGetDto.class));

        return "/voc/oneOnOneInquiryMain";
    }

    /**
     * 1:1문의 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/oneOnOneInquiry/getOneOnOneInquiryList.json", method = RequestMethod.GET)
    public List<OneOnOneInquiryListGetDto> getOneOnOneInquiryList(OneOnOneInquiryListSetDto oneOnOneInquiryListSetDto) {
        return oneOnOneInquiryService.getOneOnOneInquiryList(oneOnOneInquiryListSetDto);
    }

    /**
     * 1:1문의 상세 팝업 Page
     */
    @GetMapping("/popup/oneOnOneInquiryDetailPop")
    public String oneOnOneInquiryDetailPop(Model model
            , @RequestParam(value = "callback", required = true, defaultValue = "") String callBackScript
            , @RequestParam(value = "inqryNo", required = true, defaultValue = "") String inqryNo
            , @RequestParam(value = "inqryKind", required = true, defaultValue = "") String inqryKind) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("inqryNo", XssPreventer.escape(inqryNo));
        model.addAttribute("inqryKind", XssPreventer.escape(inqryKind));
        model.addAttribute("siteType", shipCommonService.getCode("site_type"));
        model.addAttribute("inqryCategory", shipCommonService.getCode("inqry_category"));
        model.addAttribute("inqryType", shipCommonService.getCode("inqry_type"));
        model.addAttribute("inqryDetailType", shipCommonService.getCode("inqry_detail_type"));
        model.addAttribute("inqryStatus", shipCommonService.getCode("inqry_status"));
        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("callBackScript", callBackScript);

        return "/voc/pop/oneOnOneInquiryDetailPop";
    }

    /**
     * 1:1문의 상세 조회
     */
    @ResponseBody
    @PostMapping("/oneOnOneInquiry/getOneOnOneInquiryDetail.json")
    public OneOnOneInquiryDetailGetDto getOneOnOneInquiryDetail(final HttpServletRequest request, @RequestBody OneOnOneInquiryDetailSetDto oneOnOneInquiryDetailSetDto) {
        return oneOnOneInquiryService.getOneOnOneInquiryDetail(request, oneOnOneInquiryDetailSetDto.getInqryNo());
    }

    /**
     * 1:1문의 회원 메모 팝업 Page
     */
    @GetMapping("/popup/oneOnOneInquiryMemberMemoPop")
    public String oneOnOneInquiryMemberMemoPop(Model model
        , @RequestParam(value = "userNo", required = true, defaultValue = "") String userNo
        , @RequestParam(value = "type", required = true, defaultValue = "") String type) throws Exception {
        model.addAttribute("userNo", userNo);
        model.addAttribute("type", type);

        model.addAllAttributes(RealGridHelper.create("oneOnOneInquiryMemberMemoGridBaseInfo", OneOnOneInquiryMemberMemoListGetDto.class));

        return "/voc/pop/oneOnOneInquiryMemberMemoPop";
    }

    /**
     * 1:1문의 회원 메모 조회
     */
    @ResponseBody
    @RequestMapping(value = "/oneOnOneInquiry/getInquiryMemberMemoList.json", method = RequestMethod.GET)
    public List<OneOnOneInquiryMemberMemoListGetDto> getInquiryMemberMemoList(final HttpServletRequest request, @RequestParam(value = "userNo") String userNo) {
        return oneOnOneInquiryService.getInquiryMemberMemoList(request, userNo);
    }

    /**
     * 1:1문의 답변 등록
     */
    @ResponseBody
    @PostMapping(value = "/oneOnOneInquiry/setInquiryAnswer.json")
    public ResponseObject<Object> setInquiryAnswer(@RequestBody OneOnOneInquiryDetailSetDto oneOnOneInquiryDetailSetDto) throws Exception {
        return oneOnOneInquiryService.setInquiryAnswer(oneOnOneInquiryDetailSetDto);
    }

}
