package kr.co.homeplus.admin.web.item.model.priceChangeHist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceChangeHistSetParamDto {

    private String itemNo;
    private int salePrice;
    private String storeId;
    private int originPrice;
    private String regId;

}
