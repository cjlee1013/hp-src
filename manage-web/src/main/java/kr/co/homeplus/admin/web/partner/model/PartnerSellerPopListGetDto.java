package kr.co.homeplus.admin.web.partner.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
//판매업체 팝업 그리드 (판매업체)
public class PartnerSellerPopListGetDto {

    //판매업체ID
    @RealGridColumnInfo(headText = "판매업체ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerId;

    //업체명
    @RealGridColumnInfo(headText = "판매업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String businessNm;

    //상호
    @RealGridColumnInfo(headText = "상호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerNm;

    //사업자유형명
    @RealGridColumnInfo(headText = "사업자유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String operatorTypeNm;

    //사업자등록번호
    @RealGridColumnInfo(headText = "사업자등록번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerNo;

    //회원상태명
    @RealGridColumnInfo(headText = "회원상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerStatusNm;

}
