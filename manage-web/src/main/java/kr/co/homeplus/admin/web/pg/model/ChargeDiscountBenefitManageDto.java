package kr.co.homeplus.admin.web.pg.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class ChargeDiscountBenefitManageDto {

    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long chargeDiscountBenefitMngNo;
    @RealGridColumnInfo(headText = "사이트명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;
    @RealGridColumnInfo(headText = "청구할인명", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chargeDiscountNm;
    @RealGridColumnInfo(headText = "적용시작일자", sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String applyStartDt;
    @RealGridColumnInfo(headText = "적용종료일자", sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String applyEndDt;
    @RealGridColumnInfo(headText = "결제수단코드", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodCd;
    @RealGridColumnInfo(headText = "카드사", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodNm;
    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;
    @RealGridColumnInfo(headText = "등록자", width = 80, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;
    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME)
    private String regDt;
    @RealGridColumnInfo(headText = "수정자", width = 80, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;
    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME)
    private String chgDt;
    @RealGridColumnInfo(headText = "할인방법", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String discountType;
    @RealGridColumnInfo(headText = "최소구매금액", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private int purchaseMinPrice;
    @RealGridColumnInfo(headText = "최대할인금액", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private int discountMaxPrice;
    @RealGridColumnInfo(headText = "할인율", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private int discountRate;
    @RealGridColumnInfo(headText = "할인금액", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private int discountPrice;

}
