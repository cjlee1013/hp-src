package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletItemInfoSetParamDto {

    @JsonProperty("itemList")
    private List<DspLeafletItemSetParamDto> itemList;

}
