package kr.co.homeplus.admin.web.core.utility.realgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Deprecated(forRemoval = true)
@RequiredArgsConstructor
public enum RealGridDisplayStyle {
    NONE("none"),
    EVEN("even"),
    EVEN_FILL("even_fill"),
    FILL("fill");

    @Getter
    private final String style;

}
