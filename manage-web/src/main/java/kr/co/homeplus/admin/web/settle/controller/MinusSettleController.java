package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleCompleteSetDto;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleListSetDto;
import kr.co.homeplus.admin.web.settle.service.MinusSettleService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/minusSettle")
public class MinusSettleController {

    @Autowired
    private MinusSettleService minusSettleService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * DS역정산 관리 Main Page
     */
    @RequestMapping(value = "/minusSettleMain", method = RequestMethod.GET)
    public String minusSettleMain(Model model) throws Exception {
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("minusSettleGridBaseInfo", MinusSettleListGetDto.class));

        return "/settle/minusSettleMain";
    }

    /**
     * DS역정산 관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getMinusSettleList.json", method = RequestMethod.GET)
    public List<MinusSettleListGetDto> getMinusSettleList(MinusSettleListSetDto minusSettleListSetDto) throws Exception {
        return minusSettleService.getMinusSettleList(minusSettleListSetDto);
    }

    /**
     * 수기마감
     */
    @ResponseBody
    @PostMapping(value = "/setMinusSettleComplete.json")
    public ResponseObject<Object> setMinusSettleComplete(@RequestBody MinusSettleCompleteSetDto minusSettleCompleteSetDto) throws Exception {
        return minusSettleService.setMinusSettleComplete(minusSettleCompleteSetDto);
    }
}