package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoCateGetDto {
    // 카테고리코드
    private int cateCd;

    // 카테고리명
    private String cateNm;
}