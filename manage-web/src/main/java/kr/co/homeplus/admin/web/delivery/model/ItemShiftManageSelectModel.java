package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 상품별 Shift 관리 SELECT DTO")
public class ItemShiftManageSelectModel {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 1, required = true)
    private String schStoreType;
    @ApiModelProperty(notes = "점포ID", position = 2, required = true)
    private String schStoreId;
    @ApiModelProperty(notes = "검색어타입(ITEM_NO:상품번호,ITEM_NM:상품명)", position = 3)
    private String schKeywordType;
    @ApiModelProperty(notes = "검색어(상품번호 또는 상품명)", position = 4)
    private String schKeyword;
    @ApiModelProperty(notes = "대분류", position = 5)
    private String schLcateCd;
    @ApiModelProperty(notes = "중분류", position = 6)
    private String schMcateCd;
    @ApiModelProperty(notes = "소분류", position = 7)
    private String schScateCd;
    @ApiModelProperty(notes = "세분류", position = 8)
    private String schDcateCd;
}
