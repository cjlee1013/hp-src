package kr.co.homeplus.admin.web.item.model.mngAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MngAttributeGroupSetParamDto {
    private Long gattrNo;
    private String gattrType;
    private String gattrNm;
    private String gattrMngNm;
    private String multiYn = "N";
    private String gattrUseYn;
    private String gattrDispType;
    private String regId;
}
