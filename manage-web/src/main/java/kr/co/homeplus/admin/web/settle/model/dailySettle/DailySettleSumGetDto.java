package kr.co.homeplus.admin.web.settle.model.dailySettle;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailySettleSumGetDto {
    @ApiModelProperty(value = "상품구분")
    @RealGridColumnInfo(headText = "상품구분", sortable = true, width = 100)
    private String mallType;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
    private String storeNm;

    @ApiModelProperty(value = "매출(In VAT)")
    @RealGridColumnInfo(headText = "매출(In VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeAmt;

    @ApiModelProperty(value = "매출(Ex VAT)")
    @RealGridColumnInfo(headText = "매출(Ex VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long completeExAmt;

    @ApiModelProperty(value = "정산(In VAT)")
    @RealGridColumnInfo(headText = "정산(In VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;

    @ApiModelProperty(value = "정산(Ex VAT)")
    @RealGridColumnInfo(headText = "정산(Ex VAT)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleExAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimReturnShipAmt;

    @ApiModelProperty(value = "파트너부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "상품할인(업체)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "홈플러스부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "상품할인(자사)", sortable = true, width = 120,fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponHomeChargeAmt;

    @ApiModelProperty(value = "카드부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "상품할인(카드사)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponCardChargeAmt;

    @ApiModelProperty(value = "홈플러스부담 카드할인금액")
    @RealGridColumnInfo(headText = "카드할인(자사)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponHomeAmt;

    @ApiModelProperty(value = "카드부담 카드할인금액")
    @RealGridColumnInfo(headText = "카드할인(카드사)", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponCardAmt;

    @ApiModelProperty(value = "배송비할인")
    @RealGridColumnInfo(headText = "배송비할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    @RealGridColumnInfo(headText = "임직원할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long empDiscountAmt;

    @ApiModelProperty(value = "행사할인")
    @RealGridColumnInfo(headText = "행사할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long promoDiscountAmt;

    @ApiModelProperty(value = "장바구니할인")
    @RealGridColumnInfo(headText = "장바구니할인", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cartCouponAmt;

    @ApiModelProperty(value = "매출(E-KPI)")
    @RealGridColumnInfo(headText = "매출(E-KPI)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ekpiAmt;

    @ApiModelProperty(value = "총결제금액")
    @RealGridColumnInfo(headText = "결제금액 합계", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totalPaidAmt;

    @ApiModelProperty(value = "PG결제금액")
    @RealGridColumnInfo(headText = "PG", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgAmt;

    @ApiModelProperty(value = "DGV금액")
    @RealGridColumnInfo(headText = "DGV", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long dgvAmt;

    @ApiModelProperty(value = "MHC금액")
    @RealGridColumnInfo(headText = "MHC", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long mhcAmt;

    @ApiModelProperty(value = "OCB금액")
    @RealGridColumnInfo(headText = "OCB", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ocbAmt;

    @ApiModelProperty(value = "마일리지금액")
    @RealGridColumnInfo(headText = "마일리지", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long mileageAmt;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", hidden = true)
    private String storeType;

    @ApiModelProperty(value = "사이트타입")
    @RealGridColumnInfo(headText = "사이트타입", hidden = true)
    private String siteType;

}
