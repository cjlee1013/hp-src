package kr.co.homeplus.admin.web.mileage.model.statistic;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DailyMileageAmtSumSetDto {
    //검색날짜타입 D:일별, M:월별
    private String dateType;

    //조회 시작일
    private String StartDt;

    //조회 종료일
    private String EndDt;

    //마일리지 종류
    private String mileageKind;
}
