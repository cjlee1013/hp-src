package kr.co.homeplus.admin.web.promotion.model.coupon;

import io.swagger.annotations.ApiModel;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "CmsCouponGetDto", description = "쿠폰 조회 결과 Dto")
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class CmsCouponGetDto {
    @RealGridColumnInfo(headText = "쿠폰바코드", sortable = true)
    private String couponCd;    //쿠폰 코드

    @RealGridColumnInfo(headText = "쿠폰명", columnType = RealGridColumnType.NAME, width = 200, sortable = true)
    private String couponNm;    //쿠폰명

    @RealGridColumnInfo(headText = "쿠폰종류", columnType = RealGridColumnType.NAME, width = 300, sortable = true)
    private String couponTypeNm;    //쿠폰 유형명

    @RealGridColumnInfo(headText = "할인", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, sortable = true)
    private String couponValue;     //쿠폰값(할인)

    @RealGridColumnInfo(headText = "쿠폰 일련번호", sortable = true)
    private String couponSeq;   //쿠폰 일련번호

    @RealGridColumnInfo(headText = "타켓 일련번호", hidden = true)
    private String trgtSeq;     //타켓 일련번호

    @RealGridColumnInfo(headText = "귀속부서", sortable = true)
    private String incUnitCd;       //귀속부서코드

    @RealGridColumnInfo(headText = "쿠폰 시작일", sortable = true)
    private String couponSdate;     //쿠폰시작

    @RealGridColumnInfo(headText = "쿠폰 종료일", sortable = true)
    private String couponEdate;     //쿠폰종료

    @RealGridColumnInfo(headText = "제한", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, sortable = true)
    private String couponLimit;     //쿠폰제한

    @RealGridColumnInfo(headText = "자동할인", hidden = true)
    private String adFlag;          //자동 할인 여부

    @RealGridColumnInfo(headText = "타켓군명", columnType = RealGridColumnType.NAME, width = 200, sortable = true)
    private String trgtNm;         //타켓명

    private String couponCondition; //쿠폰조건
    private String couponAsdate;    //실제시작일
    private String couponAedate;    //실제종료일
    private String publicPart;      //발행부서

    private String couponType;      //상품수
    private String couponTypeGb;    //상품유형구분(1:정률, 2:정액)
    private String couponGb;        // 행사구분(1:장바구니, 3:상품쿠폰)
    private String couponGbNm;      // 행사구분명(장바구니, 상품쿠폰)
}
