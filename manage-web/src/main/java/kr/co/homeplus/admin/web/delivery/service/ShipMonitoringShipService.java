package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringShipGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringShipSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipMonitoringShipService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 배송모니터링 리스트 조회
   */
  public List<ShipMonitoringShipGetDto> getShipMonitoringShipList(ShipMonitoringShipSetDto shipMonitoringShipSetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipMonitoringShipSetDto.setSchStoreId(shipCommonService.getStoreId());
      shipMonitoringShipSetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipMonitoringShipSetDto,
            "/admin/monitoring/getMonitoringShipList",
            new ParameterizedTypeReference<ResponseObject<List<ShipMonitoringShipGetDto>>>(){}).getData();
  }

}
