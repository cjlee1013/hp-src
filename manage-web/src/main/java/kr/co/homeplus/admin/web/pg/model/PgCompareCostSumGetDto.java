package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class PgCompareCostSumGetDto {
    @ApiModelProperty(notes = "PG사", position = 1)
    @RealGridColumnInfo(headText = "PG사", sortable = true, width = 100)
    private String pgKind;

    @ApiModelProperty(notes = "결제금액", position = 2)
    @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long hmpPaymentAmt;

    @ApiModelProperty(notes = "수수료", position = 3)
    @RealGridColumnInfo(headText = "수수료", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long hmpCommissionAmt;

    @ApiModelProperty(notes = "정산금액", position = 4)
    @RealGridColumnInfo(headText = "정산금액", sortable = true, width = 100, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long hmpSettleAmt;

    @ApiModelProperty(notes = "결제금액", position = 5)
    @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgPaymentAmt;

    @ApiModelProperty(notes = "수수료", position = 6)
    @RealGridColumnInfo(headText = "수수료", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgCommissionAmt;

    @ApiModelProperty(notes = "정산금액", position = 7)
    @RealGridColumnInfo(headText = "정산금액", sortable = true, width = 100, groupName="PG", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgSettleAmt;

    @ApiModelProperty(notes = "결제금액", position = 8)
    @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffPaymentAmt;

    @ApiModelProperty(notes = "수수료", position = 9)
    @RealGridColumnInfo(headText = "수수료", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffCommissionAmt;

    @ApiModelProperty(notes = "정산금액", position = 10)
    @RealGridColumnInfo(headText = "정산금액", sortable = true, width = 100, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffSettleAmt;
}
