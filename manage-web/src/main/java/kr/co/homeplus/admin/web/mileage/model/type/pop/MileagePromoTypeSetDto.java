package kr.co.homeplus.admin.web.mileage.model.type.pop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

@Data
@ApiModel(description = "마일리지 행사 관련 타입 조회 DTO")
public class MileagePromoTypeSetDto {
    @NonNull
    @ApiModelProperty(value = "점포유형", position = 1)
    private String storeType;
    @NonNull
    @ApiModelProperty(value = "검색유형", position = 2)
    private String schTypeCd;
    @ApiModelProperty(value = "검색어", position = 3)
    private String schTypeContents;
}
