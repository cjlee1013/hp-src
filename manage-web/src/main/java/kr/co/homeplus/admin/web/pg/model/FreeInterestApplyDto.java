package kr.co.homeplus.admin.web.pg.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class FreeInterestApplyDto {
    @RealGridColumnInfo(headText = "무이자관리순번", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private Long freeInterestMngSeq;
    @RealGridColumnInfo(headText = "적용순번", hidden = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private Long applySeq;
    @RealGridColumnInfo(headText = "카드사", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodCd;
    @RealGridColumnInfo(headText = "결제금액(만원)", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer paymentAmt;
    @RealGridColumnInfo(headText = "무이자유형", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String freeInterestType;
    @RealGridColumnInfo(headText = "할부개월", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String installmentTerm;
    @RealGridColumnInfo(headText = "등록자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;
    @RealGridColumnInfo(headText = "등록일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;
}