package kr.co.homeplus.admin.web.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "주문배송 처리 DTO")
public class OrderProcessDto {

    @ApiModelProperty(value = "배송번호")
    private String bundleNo;

    private List<String> shipNo;

}
