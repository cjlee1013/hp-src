package kr.co.homeplus.admin.web.item.model.onedayItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnedayItemSetParamDto {

    private String dispDt;
    private String itemNo;
    private String dispYn;
    private String siteType;
    private String itemStoreType;
    private int priority;
    private String regId;

}
