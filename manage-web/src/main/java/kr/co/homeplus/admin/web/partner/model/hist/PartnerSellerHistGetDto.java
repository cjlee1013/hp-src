package kr.co.homeplus.admin.web.partner.model.hist;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class PartnerSellerHistGetDto {

    @RealGridColumnInfo(headText = "히스토리번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String histSeq;

    @RealGridColumnInfo(headText = "변경일시", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgDt = "-";

    @RealGridColumnInfo(headText = "판매업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgBusinessNm = "-";

    @RealGridColumnInfo(headText = "대표자명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgPartnerOwner = "-";

    @RealGridColumnInfo(headText = "통신판매업신고번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgCommunityNotiNo = "-";

    @RealGridColumnInfo(headText = "업태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgBusinessConditions = "-";

    @RealGridColumnInfo(headText = "종목", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgBizCateNm = "-";

    @RealGridColumnInfo(headText = "은행계좌", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgBankAccountNo = "-";

    @RealGridColumnInfo(headText = "예금주", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgDepositor = "-";

    @RealGridColumnInfo(headText = "지급여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgSettlePaymentYnNm = "-";

    @RealGridColumnInfo(headText = "판매업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String businessNm = "-";

    @RealGridColumnInfo(headText = "대표자명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String partnerOwner = "-";

    @RealGridColumnInfo(headText = "통신판매업신고번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String communityNotiNo = "-";

    @RealGridColumnInfo(headText = "업태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String businessConditions = "-";

    @RealGridColumnInfo(headText = "종목", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String bizCateNm = "-";

    @RealGridColumnInfo(headText = "은행계좌", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String bankAccountNo = "-";

    @RealGridColumnInfo(headText = "예금주", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String depositor = "-";

    @RealGridColumnInfo(headText = "지급여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true, width = 150)
    private String settlePaymentYnNm = "-";

    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgNm = "-";


    private String partnerId;

}
