package kr.co.homeplus.admin.web.item.model.auroraBanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraBannerLinkGetDto {

    private Long bannerId;
    private Long linkNo;
    private String linkType;
    private String linkInfo;
    private String linkInfoNm;
    private String linkOptions;
    private String textYn;
    private String title;
    private String subTitle;
    private String imgUrl;
    private int imgWidht;
    private int imgHeight;
    private String useYn;
    private String regId;
    private String regNm;
    private String regDt;
    private String chgId;
    private String chgNm;
    private String chgDt;

}
