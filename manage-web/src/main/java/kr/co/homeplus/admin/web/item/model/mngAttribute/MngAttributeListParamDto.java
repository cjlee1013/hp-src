package kr.co.homeplus.admin.web.item.model.mngAttribute;

import lombok.Data;

@Data
public class MngAttributeListParamDto {
   private String gattrNo;
}
