package kr.co.homeplus.admin.web.item.model.banner;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemBannerDetailListSelectDto {

    @ApiModelProperty(value = "일련번호")
    @RealGridColumnInfo(headText = "일련번호", width = 0, hidden = true)
    private String detailSeq;

    @ApiModelProperty(value = "공지번호")
    @RealGridColumnInfo(headText = "공지번호", width = 0, hidden = true)
    private Long bannerNo;

    @ApiModelProperty(value = "노출순서")
    @RealGridColumnInfo(headText = "노출순서", width = 80)
    private Integer priority;

    @ApiModelProperty(value = "이미지")
    @RealGridColumnInfo(headText = "이미지", width = 100)
    private String imgNm;

    @ApiModelProperty(value = "이미지URL")
    @RealGridColumnInfo(headText = "이미지URL", width = 0, hidden = true)
    private String imgUrl;

    @ApiModelProperty(value = "이미지가로")
    @RealGridColumnInfo(headText = "이미지가로", width = 0, hidden = true)
    private Integer imgWidth;

    @ApiModelProperty(value = "이미지세로")
    @RealGridColumnInfo(headText = "이미지세로", width = 0, hidden = true)
    private Integer imgHeight;

    @ApiModelProperty(value = "링크타입")
    @RealGridColumnInfo(headText = "링크타입", width = 0, hidden = true)
    private String linkType;

    @ApiModelProperty(value = "링크타입")
    @RealGridColumnInfo(headText = "링크타입", width = 100, sortable = true)
    private String linkTypeNm;

    @ApiModelProperty(value = "링크타입")
    @RealGridColumnInfo(headText = "링크타입", width = 100)
    private String linkInfo;

    @ApiModelProperty(value = "기획전/이벤트명")
    @RealGridColumnInfo(headText = "기획전/이벤트명", width = 0, hidden = true)
    private String promoNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true)
    private  String useYn;

}
