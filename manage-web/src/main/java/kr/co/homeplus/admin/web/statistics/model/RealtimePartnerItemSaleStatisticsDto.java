package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "실시간 판매업체 상품 판매현황 DTO")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class RealtimePartnerItemSaleStatisticsDto {
    @ApiModelProperty(value = "랭킹번호", position = 1)
    @RealGridColumnInfo(headText = "No", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER)
    private int rankingNo;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP, AURORA, DS)", position = 2)
    @RealGridColumnInfo(headText = "점포유형", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(value = "상품번호(상품 일때만 입력)", position = 3)
    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String itemNo;

    @ApiModelProperty(value = "상품명1", position = 4)
    @RealGridColumnInfo(headText = "상품명", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String itemName;

    @ApiModelProperty(value = "상품수량 (SUM(상품옵션수량))", position = 5)
    @RealGridColumnInfo(headText = "판매수량", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemQty;

    @ApiModelProperty(value = "클레임 상품수량 (SUM(상품옵션수량))", position = 6)
    @RealGridColumnInfo(headText = "취소수량", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimQty;

    @ApiModelProperty(value = "주문금액 ((상품가격+옵션가격N)*옵션수량N) + (추가상품가격*추가상품수량)", position = 7)
    @RealGridColumnInfo(headText = "판매금액", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderPrice;

    @ApiModelProperty(value = "클레임 주문금액(상품금액*클레임건수)", position = 8)
    @RealGridColumnInfo(headText = "취소금액", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimPrice;

    @ApiModelProperty(value = "순판매수량(주문-클레임)", position = 9)
    @RealGridColumnInfo(headText = "순판매수량", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totQty;

    @ApiModelProperty(value = "순판매금액(주문-클레임)", position = 10)
    @RealGridColumnInfo(headText = "순판매금액", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totPrice;

    @ApiModelProperty(value = "대카테고리명", position = 11)
    @RealGridColumnInfo(headText = "대분류", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String lcateNm;

    @ApiModelProperty(value = "중카테고리명", position = 12)
    @RealGridColumnInfo(headText = "중분류", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String mcateNm;

    @ApiModelProperty(value = "소카테고리명", position = 13)
    @RealGridColumnInfo(headText = "소분류", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String scateNm;

    @ApiModelProperty(value = "세카테고리명", position = 14)
    @RealGridColumnInfo(headText = "세분류", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String dcateNm;

    @ApiModelProperty(value = "세금여부(Y:과세,N:면세)", position = 15)
    @RealGridColumnInfo(headText = "과면세", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String taxYn;

    @ApiModelProperty(value = "수수료율", position = 16)
    @RealGridColumnInfo(headText = "수수료율", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.COMMISSION)
    private BigDecimal commissionRate;

    @ApiModelProperty(value = "상품금액(상품 판매가)", position = 17)
    @RealGridColumnInfo(headText = "상품금액", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long itemPrice;

    @ApiModelProperty(value = "매출금액(in vat)", position = 18)
    @RealGridColumnInfo(headText = "매출(In VAT)", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentInvat;

    @ApiModelProperty(value = "매출금액(Ex vat)", position = 19)
    @RealGridColumnInfo(headText = "매출(Ex VAT)", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentExvat;

    @ApiModelProperty(value = "정산(In vat)", position = 20)
    @RealGridColumnInfo(headText = "정산(In VAT)", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleInvat;

    @ApiModelProperty(value = "정산(Ex vat)", position = 21)
    @RealGridColumnInfo(headText = "정산(Ex VAT)", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleExvat;

    @ApiModelProperty(value = "판매수수료", position = 22)
    @RealGridColumnInfo(headText = "판매수수료", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private int commissionAmt;

    @ApiModelProperty(value = "총할인금액", position = 23)
    @RealGridColumnInfo(headText = "총 할인금액", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long discountAmt;

    @ApiModelProperty(value = "결제금액 합계", position = 24)
    @RealGridColumnInfo(headText = "결제금액 합계", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentAmt;
}