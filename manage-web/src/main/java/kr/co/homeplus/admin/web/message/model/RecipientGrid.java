package kr.co.homeplus.admin.web.message.model;

import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class RecipientGrid {

    @RealGridColumnInfo(hidden = true)
    private long workSeq;
    @RealGridColumnInfo(hidden = true)
    private long listSeq;
    @RealGridColumnInfo(hidden = true)
    private long historySeq;
    @RealGridColumnInfo(hidden = true)
    private WorkType workType;
    @RealGridColumnInfo(headText = "템플릿코드", columnType = RealGridColumnType.NAME)
    private String templateCode;
    @RealGridColumnInfo(headText = "발송일자", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private String sendDt;
    @RealGridColumnInfo(headText = "수신번호")
    private String recipient;
    @RealGridColumnInfo(headText = "1차 알림톡", columnType = RealGridColumnType.LOOKUP)
    private WorkStatus alimtalkStatus;
    @RealGridColumnInfo(headText = "2차 SMS", columnType = RealGridColumnType.LOOKUP)
    private WorkStatus smsStatus;
}
