package kr.co.homeplus.admin.web.settle.model.ocb;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PgOcbTidListGetDto {
    @ApiModelProperty(value = "기준일")
    @RealGridColumnInfo(headText = "기준일", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "가맹점번호")
    @RealGridColumnInfo(headText = "가맹점번호", sortable = true, width = 100)
    private String ocbStoreNo;

    @ApiModelProperty(value = "결제번호")
    @RealGridColumnInfo(headText = "결제번호", sortable = true, width = 100)
    private String paymentNo;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "OCB 승인번호")
    @RealGridColumnInfo(headText = "OCB 승인번호", sortable = true, width = 100)
    private String approvalNo;

    @ApiModelProperty(value = "취소구분")
    @RealGridColumnInfo(headText = "취소구분", sortable = true, width = 100)
    private String orderType;

    @ApiModelProperty(value = "거래구분")
    @RealGridColumnInfo(headText = "거래구분", sortable = true, width = 100)
    private String tradeType;

    @ApiModelProperty(value = "승인구분")
    @RealGridColumnInfo(headText = "승인구분", sortable = true, width = 100)
    private String approvalType;

    @ApiModelProperty(value = "매출액")
    @RealGridColumnInfo(headText = "매출액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long purchaseAmt;

    @ApiModelProperty(value = "결제금액")
    @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentAmt;

    @ApiModelProperty(value = "OCB 포인트")
    @RealGridColumnInfo(headText = "OCB 포인트", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ocbAmt;

    @ApiModelProperty(value = "원거래포인트")
    @RealGridColumnInfo(headText = "원거래포인트", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ocbOAmt;

}
