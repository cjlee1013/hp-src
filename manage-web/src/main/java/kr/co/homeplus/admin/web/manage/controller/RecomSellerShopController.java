package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.recomSellerShop.RecomSellerShopItemSelectDto;
import kr.co.homeplus.admin.web.manage.model.recomSellerShop.RecomSellerShopListParamDto;
import kr.co.homeplus.admin.web.manage.model.recomSellerShop.RecomSellerShopSelectDto;
import kr.co.homeplus.admin.web.manage.model.recomSellerShop.RecomSellerShopSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class RecomSellerShopController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 사이트관리 > 전문관 관리 > 추천 셀러샵 전시관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/recomSellerShopMain", method = RequestMethod.GET)
    public String recomSellerShopMain(Model model) {

        codeService.getCodeModel(model,
                "recomshop_sch_type"    // 셀러샵전시관리 검색어 타입
            ,   "recomshop_type"        // 셀러샵전시관리 노출타입
            ,   "recomshop_use_yn"      // 셀러샵전시관리 노출타입
        );


        model.addAllAttributes(RealGridHelper.create("recomSellerShopListGridBaseInfo", RecomSellerShopSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("recomSellerShopItemListGridBaseInfo", RecomSellerShopItemSelectDto.class));

        return "/manage/recomSellerShopMain";
    }

    /**
     * 사이트관리 > 전문관 관리 > 추천 셀러샵 전시관리 > 추천셀러샵 리스트 조회
     *
     * @param recomSellerShopListParamDto
     * @return RecomSellerShopSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getRecomSellerShopList.json"}, method = RequestMethod.GET)
    public List<RecomSellerShopSelectDto> getRecomSellerShopList(RecomSellerShopListParamDto recomSellerShopListParamDto) {
        String apiUri = "/manage/getRecommendSellerShopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RecomSellerShopSelectDto>>>() {};
        return (List<RecomSellerShopSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, RecomSellerShopListParamDto.class, recomSellerShopListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관 관리 > 추천 셀러샵 전시관리 > 추천셀러샵 아이템 리스트 조회
     *
     * @param partnerId
     * @return RecomSellerShopItemSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getRecomSellerShopItemList.json"}, method = RequestMethod.GET)
    public List<RecomSellerShopItemSelectDto> getRecomSellerShopItemList(String partnerId) {
        String apiUri = "/manage/getRecommendSellerShopItemList?partnerId=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RecomSellerShopItemSelectDto>>>() {};
        return (List<RecomSellerShopItemSelectDto> ) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri+partnerId, typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관 관리 > 추천 셀러샵 전시관리 > 추천셀러샵 등록/수정
     *
     * @param setParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setRecomSellerShopItem.json"}, method = RequestMethod.POST)
    public ResponseResult setRecomSellerShopItem(@RequestBody RecomSellerShopSetParamDto setParamDto) {
        String apiUri = "/manage/setRecommendSellerShop";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}