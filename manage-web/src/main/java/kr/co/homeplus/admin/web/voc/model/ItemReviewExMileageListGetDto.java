package kr.co.homeplus.admin.web.voc.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Setter
@Getter
public class ItemReviewExMileageListGetDto {

    @RealGridColumnInfo(headText = "NO", width = 30)
    private Long seq;

    @RealGridColumnInfo(headText = "구분", width = 50, order = 1)
    private String excNm;

    @RealGridColumnInfo(headText = "대분류", columnType = RealGridColumnType.NAME, width = 80, order = 2)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", columnType = RealGridColumnType.NAME, width = 80, order = 3)
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류", columnType = RealGridColumnType.NAME, width = 80, order = 4)
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류", columnType = RealGridColumnType.NAME, width = 80, order = 5)
    private String dcateNm;

//    요청에 의한 제거
//    @RealGridColumnInfo(headText = "판매업체명", order = 6)
//    private String partnerNm;

    @RealGridColumnInfo(headText = "상품번호", width = 50, order = 7)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", order = 8)
    private String itemNm1;

    @RealGridColumnInfo(headText = "등록자", width = 70, order = 9)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, order = 10)
    private String regDt;


    private String excKind;
    private String cateKind;
    private Integer cateCd;
    private String partnerId;
    private String useYn;
    private String useYnTxt;
    private String chgNm;
    private String chgDt;

}
