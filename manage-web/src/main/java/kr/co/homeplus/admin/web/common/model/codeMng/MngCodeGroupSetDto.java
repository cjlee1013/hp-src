package kr.co.homeplus.admin.web.common.model.codeMng;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MngCodeGroupSetDto {
	private String gmcCd;
	private String gmcNm;
	private String lgcUpcodeCd;
	private String userId;

}
