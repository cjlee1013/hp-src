package kr.co.homeplus.admin.web.delivery.model;

import lombok.Data;

@Data
public class StoreShiftManageSelectModel {

    private String schStartDt;

    private String schEndDt;

    private String schStoreType;

    private int schStoreId;
}
