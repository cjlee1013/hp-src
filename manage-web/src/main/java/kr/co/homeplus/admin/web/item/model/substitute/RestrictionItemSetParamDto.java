package kr.co.homeplus.admin.web.item.model.substitute;


import lombok.Data;

@Data
public class RestrictionItemSetParamDto {

    private String itemNo;

    private String useYn;

    private String regId;
}
