package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 엑셀 리스트 응답 DTO")
public class CallCenterExcelListGetDto {
  @ApiModelProperty(notes = "문의번호")
  @RealGridColumnInfo(headText = "문의번호", width = 120)
  @JsonProperty("KEY")
  private String KEY;

  @ApiModelProperty(notes = "접수일")
  @RealGridColumnInfo(headText = "접수일", width = 120)
  @JsonProperty("start_time")
  private String start_time;

  @ApiModelProperty(notes = "구분")
  @RealGridColumnInfo(headText = "구분", width = 120)
  @JsonProperty("voc_yn_nm")
  private String voc_yn_nm;

  @ApiModelProperty(notes = "유형")
  @RealGridColumnInfo(headText = "유형", width = 120)
  @JsonProperty("h_channel_nm")
  private String h_channel_nm;

  @ApiModelProperty(notes = "접수구분")
  @RealGridColumnInfo(headText = "접수구분", width = 120)
  @JsonProperty("channel_nm")
  private String channel_nm;

  @ApiModelProperty(notes = "상담구분")
  @RealGridColumnInfo(headText = "상담구분", width = 120)
  @JsonProperty("advisor_type_nm1")
  private String advisor_type_nm1;

  @ApiModelProperty(notes = "상담항목")
  @RealGridColumnInfo(headText = "상담항목", width = 120)
  @JsonProperty("advisor_type_nm2")
  private String advisor_type_nm2;

  @ApiModelProperty(notes = "처리부서")
  @RealGridColumnInfo(headText = "처리부서", width = 120)
  @JsonProperty("h_branch_nm")
  private String h_branch_nm;

  @ApiModelProperty(notes = "처리상태")
  @RealGridColumnInfo(headText = "처리상태", width = 120)
  @JsonProperty("advisor_ing_nm")
  private String advisor_ing_nm;

  @ApiModelProperty(notes = "처리완료시간")
  @RealGridColumnInfo(headText = "처리완료시간", width = 120)
  @JsonProperty("complet_dt")
  private String complet_dt;

  @ApiModelProperty(notes = "회원번호")
  @RealGridColumnInfo(headText = "회원번호", width = 120)
  @JsonProperty("cust_id")
  private String cust_id;

  @ApiModelProperty(notes = "판매자코드")
  @RealGridColumnInfo(headText = "판매자코드", width = 120)
  @JsonProperty("store_id")
  private String store_id;

  @ApiModelProperty(notes = "판매자")
  @RealGridColumnInfo(headText = "판매자", width = 120)
  @JsonProperty("store_nm")
  private String store_nm;

  @ApiModelProperty(notes = "마켓연동")
  @RealGridColumnInfo(headText = "마켓연동", width = 120)
  @JsonProperty("market_type_nm")
  private String market_type_nm;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 120)
  @JsonProperty("h_order_no")
  private String h_order_no;

  @ApiModelProperty(notes = "배송요청일")
  @RealGridColumnInfo(headText = "배송요청일", width = 120)
  @JsonProperty("deli_req_date")
  private String deli_req_date;

  @ApiModelProperty(notes = "배송요청시간")
  @RealGridColumnInfo(headText = "배송요청시간", width = 120)
  @JsonProperty("deli_req_time")
  private String deli_req_time;

  @ApiModelProperty(notes = "배송기사명")
  @RealGridColumnInfo(headText = "배송기사명", width = 120)
  @JsonProperty("delivery_nm")
  private String delivery_nm;

  @ApiModelProperty(notes = "상담주문상품")
  @RealGridColumnInfo(headText = "상담주문상품", width = 120)
  @JsonProperty("h_item_no")
  private String h_item_no;

  @ApiModelProperty(notes = "처리자")
  @RealGridColumnInfo(headText = "처리자", width = 120)
  @JsonProperty("complet_account_nm")
  private String complet_account_nm;

  @ApiModelProperty(notes = "처리사유")
  @RealGridColumnInfo(headText = "처리사유", width = 120)
  @JsonProperty("complet_reason_nm")
  private String complet_reason_nm;

  @ApiModelProperty(notes = "발생자")
  @RealGridColumnInfo(headText = "발생자", width = 120)
  @JsonProperty("issue_generator")
  private String issue_generator;

  @ApiModelProperty(notes = "발생원인")
  @RealGridColumnInfo(headText = "발생원인", width = 120)
  @JsonProperty("issue_reason")
  private String issue_reason;

  @ApiModelProperty(notes = "처리내용")
  @RealGridColumnInfo(headText = "처리내용", width = 120)
  @JsonProperty("complet_txt")
  private String complet_txt;

  @ApiModelProperty(notes = "제목")
  @RealGridColumnInfo(headText = "제목", width = 120)
  @JsonProperty("history_title")
  private String history_title;

  @ApiModelProperty(notes = "접수내용1")
  @RealGridColumnInfo(headText = "접수내용1", width = 120)
  @JsonProperty("rmks1")
  private String rmks1;

  @ApiModelProperty(notes = "접수일1")
  @RealGridColumnInfo(headText = "접수일1", width = 120)
  @JsonProperty("reg_dt1")
  private String reg_dt1;

  @ApiModelProperty(notes = "접수자1")
  @RealGridColumnInfo(headText = "접수자1", width = 120)
  @JsonProperty("account_nm1")
  private String account_nm1;

  @ApiModelProperty(notes = "접수내용2")
  @RealGridColumnInfo(headText = "접수내용2", width = 120)
  @JsonProperty("rmks2")
  private String rmks2;

  @ApiModelProperty(notes = "접수일2")
  @RealGridColumnInfo(headText = "접수일2", width = 120)
  @JsonProperty("reg_dt2")
  private String reg_dt2;

  @ApiModelProperty(notes = "접수자2")
  @RealGridColumnInfo(headText = "접수자2", width = 120)
  @JsonProperty("account_nm2")
  private String account_nm2;

  @ApiModelProperty(notes = "접수내용3")
  @RealGridColumnInfo(headText = "접수내용3", width = 120)
  @JsonProperty("rmks3")
  private String rmks3;

  @ApiModelProperty(notes = "접수일3")
  @RealGridColumnInfo(headText = "접수일3", width = 120)
  @JsonProperty("reg_dt3")
  private String reg_dt3;

  @ApiModelProperty(notes = "접수자3")
  @RealGridColumnInfo(headText = "접수자3", width = 120)
  @JsonProperty("account_nm3")
  private String account_nm3;

  @ApiModelProperty(notes = "접수내용4")
  @RealGridColumnInfo(headText = "접수내용4", width = 120)
  @JsonProperty("rmks4")
  private String rmks4;

  @ApiModelProperty(notes = "접수일4")
  @RealGridColumnInfo(headText = "접수일4", width = 120)
  @JsonProperty("reg_dt4")
  private String reg_dt4;

  @ApiModelProperty(notes = "접수자4")
  @RealGridColumnInfo(headText = "접수자4", width = 120)
  @JsonProperty("account_nm4")
  private String account_nm4;

  @ApiModelProperty(notes = "접수내용5")
  @RealGridColumnInfo(headText = "접수내용5", width = 120)
  @JsonProperty("rmks5")
  private String rmks5;

  @ApiModelProperty(notes = "접수일5")
  @RealGridColumnInfo(headText = "접수일5", width = 120)
  @JsonProperty("reg_dt5")
  private String reg_dt5;

  @ApiModelProperty(notes = "접수자5")
  @RealGridColumnInfo(headText = "접수자5", width = 120)
  @JsonProperty("account_nm5")
  private String account_nm5;

  @ApiModelProperty(notes = "접수내용6")
  @RealGridColumnInfo(headText = "접수내용6", width = 120)
  @JsonProperty("rmks6")
  private String rmks6;

  @ApiModelProperty(notes = "접수일6")
  @RealGridColumnInfo(headText = "접수일6", width = 120)
  @JsonProperty("reg_dt6")
  private String reg_dt6;

  @ApiModelProperty(notes = "접수자6")
  @RealGridColumnInfo(headText = "접수자6", width = 120)
  @JsonProperty("account_nm6")
  private String account_nm6;

  @ApiModelProperty(notes = "접수내용7")
  @RealGridColumnInfo(headText = "접수내용7", width = 120)
  @JsonProperty("rmks7")
  private String rmks7;

  @ApiModelProperty(notes = "접수일7")
  @RealGridColumnInfo(headText = "접수일7", width = 120)
  @JsonProperty("reg_dt7")
  private String reg_dt7;

  @ApiModelProperty(notes = "접수자7")
  @RealGridColumnInfo(headText = "접수자7", width = 120)
  @JsonProperty("account_nm7")
  private String account_nm7;

  @ApiModelProperty(notes = "접수내용8")
  @RealGridColumnInfo(headText = "접수내용8", width = 120)
  @JsonProperty("rmks8")
  private String rmks8;

  @ApiModelProperty(notes = "접수일8")
  @RealGridColumnInfo(headText = "접수일8", width = 120)
  @JsonProperty("reg_dt8")
  private String reg_dt8;

  @ApiModelProperty(notes = "접수자8")
  @RealGridColumnInfo(headText = "접수자8", width = 120)
  @JsonProperty("account_nm8")
  private String account_nm8;

  @ApiModelProperty(notes = "접수내용9")
  @RealGridColumnInfo(headText = "접수내용9", width = 120)
  @JsonProperty("rmks9")
  private String rmks9;

  @ApiModelProperty(notes = "접수일9")
  @RealGridColumnInfo(headText = "접수일9", width = 120)
  @JsonProperty("reg_dt9")
  private String reg_dt9;

  @ApiModelProperty(notes = "접수자9")
  @RealGridColumnInfo(headText = "접수자9", width = 120)
  @JsonProperty("account_nm9")
  private String account_nm9;

  @ApiModelProperty(notes = "접수내용10")
  @RealGridColumnInfo(headText = "접수내용10", width = 120)
  @JsonProperty("rmks10")
  private String rmks10;

  @ApiModelProperty(notes = "접수일10")
  @RealGridColumnInfo(headText = "접수일10", width = 120)
  @JsonProperty("reg_dt10")
  private String reg_dt10;

  @ApiModelProperty(notes = "접수자10")
  @RealGridColumnInfo(headText = "접수자10", width = 120)
  @JsonProperty("account_nm10")
  private String account_nm10;

  @ApiModelProperty(notes = "접수내용11")
  @RealGridColumnInfo(headText = "접수내용11", width = 120)
  @JsonProperty("rmks11")
  private String rmks11;

  @ApiModelProperty(notes = "접수일11")
  @RealGridColumnInfo(headText = "접수일11", width = 120)
  @JsonProperty("reg_dt11")
  private String reg_dt11;

  @ApiModelProperty(notes = "접수자11")
  @RealGridColumnInfo(headText = "접수자11", width = 120)
  @JsonProperty("account_nm11")
  private String account_nm11;

  @ApiModelProperty(notes = "접수내용12")
  @RealGridColumnInfo(headText = "접수내용12", width = 120)
  @JsonProperty("rmks12")
  private String rmks12;

  @ApiModelProperty(notes = "접수일12")
  @RealGridColumnInfo(headText = "접수일12", width = 120)
  @JsonProperty("reg_dt12")
  private String reg_dt12;

  @ApiModelProperty(notes = "접수자12")
  @RealGridColumnInfo(headText = "접수자12", width = 120)
  @JsonProperty("account_nm12")
  private String account_nm12;

  @ApiModelProperty(notes = "접수내용13")
  @RealGridColumnInfo(headText = "접수내용13", width = 120)
  @JsonProperty("rmks13")
  private String rmks13;

  @ApiModelProperty(notes = "접수일13")
  @RealGridColumnInfo(headText = "접수일13", width = 120)
  @JsonProperty("reg_dt13")
  private String reg_dt13;

  @ApiModelProperty(notes = "접수자13")
  @RealGridColumnInfo(headText = "접수자13", width = 120)
  @JsonProperty("account_nm13")
  private String account_nm13;

  @ApiModelProperty(notes = "접수내용14")
  @RealGridColumnInfo(headText = "접수내용14", width = 120)
  @JsonProperty("rmks14")
  private String rmks14;

  @ApiModelProperty(notes = "접수일14")
  @RealGridColumnInfo(headText = "접수일14", width = 120)
  @JsonProperty("reg_dt14")
  private String reg_dt14;

  @ApiModelProperty(notes = "접수자14")
  @RealGridColumnInfo(headText = "접수자14", width = 120)
  @JsonProperty("account_nm14")
  private String account_nm14;

  @ApiModelProperty(notes = "접수내용15")
  @RealGridColumnInfo(headText = "접수내용15", width = 120)
  @JsonProperty("rmks15")
  private String rmks15;

  @ApiModelProperty(notes = "접수일15")
  @RealGridColumnInfo(headText = "접수일15", width = 120)
  @JsonProperty("reg_dt15")
  private String reg_dt15;

  @ApiModelProperty(notes = "접수자15")
  @RealGridColumnInfo(headText = "접수자15", width = 120)
  @JsonProperty("account_nm15")
  private String account_nm15;

  @ApiModelProperty(notes = "접수내용16")
  @RealGridColumnInfo(headText = "접수내용16", width = 120)
  @JsonProperty("rmks16")
  private String rmks16;

  @ApiModelProperty(notes = "접수일16")
  @RealGridColumnInfo(headText = "관리번호", width = 120)
  @JsonProperty("reg_dt16")
  private String reg_dt16;

  @ApiModelProperty(notes = "접수자16")
  @RealGridColumnInfo(headText = "접수자16", width = 120)
  @JsonProperty("account_nm16")
  private String account_nm16;

  @ApiModelProperty(notes = "접수내용17")
  @RealGridColumnInfo(headText = "접수내용17", width = 120)
  @JsonProperty("rmks17")
  private String rmks17;

  @ApiModelProperty(notes = "접수일17")
  @RealGridColumnInfo(headText = "접수일17", width = 120)
  @JsonProperty("reg_dt17")
  private String reg_dt17;

  @ApiModelProperty(notes = "접수자17")
  @RealGridColumnInfo(headText = "접수자17", width = 120)
  @JsonProperty("account_nm17")
  private String account_nm17;

  @ApiModelProperty(notes = "접수내용18")
  @RealGridColumnInfo(headText = "접수내용18", width = 120)
  @JsonProperty("rmks18")
  private String rmks18;

  @ApiModelProperty(notes = "접수일18")
  @RealGridColumnInfo(headText = "접수일18", width = 120)
  @JsonProperty("reg_dt18")
  private String reg_dt18;

  @ApiModelProperty(notes = "접수자18")
  @RealGridColumnInfo(headText = "접수자18", width = 120)
  @JsonProperty("account_nm18")
  private String account_nm18;

  @ApiModelProperty(notes = "접수내용19")
  @RealGridColumnInfo(headText = "접수내용19", width = 120)
  @JsonProperty("rmks19")
  private String rmks19;

  @ApiModelProperty(notes = "접수일19")
  @RealGridColumnInfo(headText = "접수일19", width = 120)
  @JsonProperty("reg_dt19")
  private String reg_dt19;

  @ApiModelProperty(notes = "접수자19")
  @RealGridColumnInfo(headText = "접수자19", width = 120)
  @JsonProperty("account_nm19")
  private String account_nm19;

  @ApiModelProperty(notes = "접수내용20")
  @RealGridColumnInfo(headText = "접수내용20", width = 120)
  @JsonProperty("rmks20")
  private String rmks20;

  @ApiModelProperty(notes = "접수일20")
  @RealGridColumnInfo(headText = "접수일20", width = 120)
  @JsonProperty("reg_dt20")
  private String reg_dt20;

  @ApiModelProperty(notes = "접수자20")
  @RealGridColumnInfo(headText = "접수자20", width = 120)
  @JsonProperty("account_nm20")
  private String account_nm20;
}