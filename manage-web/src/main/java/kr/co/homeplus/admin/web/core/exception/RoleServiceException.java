package kr.co.homeplus.admin.web.core.exception;

public class RoleServiceException extends RuntimeException {

    public RoleServiceException(String message) {
        super(message);
    }

    public RoleServiceException(Exception e) {
        super(e);
    }
}
