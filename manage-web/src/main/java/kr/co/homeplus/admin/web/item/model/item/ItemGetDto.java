package kr.co.homeplus.admin.web.item.model.item;

import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 기본정보
 */
@Getter
@Setter
public class ItemGetDto {
	private String itemNo;
	private String itemNm;
	private String itemDivision;
	private String itemType;
	private String itemTypeNm;
	private String itemStatus;
	private String StatusDesc;
	private String sellerItemCd;
	private String partnerId;
	private String businessNm;
	private String mallType;
	private String mallTypeNm;
	private String saleStatusDt;
	private String adultType;
	private String pickYn;
	private String imgDispYn;
	private String dispYn;
	private String dispYnNm;
	private Long brandNo;
	private Long makerNo;
	private String optSelUseYn;
	private String optTxtUseYn;
	private String listSplitDispYn;
	private String regId;
	private String regDt;
	private String chgId;
	private String chgDt;

	private String storeId;
	private String storeNm;
	private String storeType;
	private String storeTypeNm;

	private int dcateCd;
	private int scateCd;
	private int mcateCd;
	private int lcateCd;
	private String dcateNm;
	private String scateNm;
	private String mcateNm;
	private String lcateNm;

	private String regNm;
	private String chgNm;
	private String transTypeNm;
	private String itemStatusNm;
	private String saleStatusNm;
	private String brandNm;
	private String makerNm;
	private String makeDt;
	private String expDt;

	/**
	 * 옵션정보
	 */
	List<ItemOptionValueGetDto> optList;

	/**
	 * 셀링포인트
	 */
	List<ItemPrGetDto> prList;

	/**
	 * 판매정보
	 */
	private String taxYn;
	private Long originPrice;
	private long purchasePrice;
	private long salePrice;
	private long dcPrice;
	private String saleStartDt;
	private String saleEndDt;
	private String commissionType;
	private BigDecimal commissionRate;
	private int commissionPrice;
	private String dcYn;
	private String dcPeriodYn;
	private String dcStartDt;
	private String dcEndDt;
	private String dcCommissionType;
	private BigDecimal dcCommissionRate;
	private int    dcCommissionPrice;
	private String salePeriodYn;
	private String rsvYn;
	private Long saleRsvNo;
	private Long rsvSaleSeq;
	private String rsvSaleNm;
	private String rsvStartDt;
	private String rsvEndDt;
	private String shipStartDt;
	//예약 구매제한 개수 여부
	private String rsvPurchaseLimitYn;
	//예약 구매제한 개수(1회) - ?개
	private Integer rsvPurchaseLimitQty;

	private int stockQty;
	private int remainStockQty;
	private int saleUnit;
	private int purchaseMinQty;
	private String purchaseLimitYn;
	private String purchaseLimitDuration;
	private int purchaseLimitDay;
	private int purchaseLimitQty;
	private String cartLimitYn;

	//판매유형(N:일반, D:직매입)
	private String saleType;

	/**
	 * 이미지 정보
	 */
	private List<ItemImgGetDto> imgList;

	/**
	 * 점포정보
	 */
	List<ItemStoreListGetDto> storeList;

    private long shipPolicyNo;
    private Integer releaseDay;
    private Integer releaseTime;
    private String holidayExceptYn;
    private Integer claimShipFee;
    private String releaseZipcode;
    private String releaseAddr1;
    private String releaseAddr2;
    private String returnZipcode;
    private String returnAddr1;
    private String returnAddr2;
    private String safeNumberUseYn;

    private String shipPolicyNm;
    private String shipKindNm;
    private String shipMethodNm;
    private String shipTypeNm;
    private int shipFee;
    private String prepaymentYnNm;
    private String shipDispYnNm;
    private String diffYn;
    private String diffQty;

	/**
	 * 상품 이미지정보
	 */
	private String itemDesc;
	private ItemVideoGetDto videoFile;

	/**
	 * 옵션 타이틀 정보
	 */
	private int    optTitleDepth;
	private String opt1Title;
	private String opt2Title;

	/**
	 * 텍스트옵션 정보
	 */
	private int    optTxtDepth;
	private String opt1Text;
	private String opt2Text;

	/**
	 * 부가정보
	 */
	private String epYn;
	private String linkageYn;
	private String srchKeyword;
	private String globalAgcyYn;
	private Long gnoticeNo;
	private List<ItemNoticeGetDto> noticeList;
	private Integer rentalFee;
	private Integer rentalPeriod;
	private Integer rentalRegFee;
	private String giftYn;
	private List<ItemProofListGetDto> proofFileList;
	private List<ItemCertGetDto> certList;
	private String isbn;

	/**
	 * 점포 기본정보 추가 필드
	 */
	//대표 셀링포인트 유형명
	private String prTypeNm;

	//대표 셀링포인트 내용
	private String prContents;

	//총 단위량 ( 유닛 당 펜스를 계산하는 데 사용되는 총용량 )
	private String totalUnitQty;

	//단위 수량
	private String unitQty;

	//측정 단위
	private String unitMeasure;

	//프론트 노출여부
	private String unitDispYn;

	//대표판매정보 리스트
	private List<ItemSaleDFListGetDto> saleDFList;

	//대표셀링포인트")
	List<ItemPrDFGetDto> prDFList;

	//lgc 카테고리명
	private String lgcCateNm;

	//거래형태명
	private String dealTypeNm;

	//마켓연동 리스트
	private List<ItemProviderGetDto> providerList;

	/**
	 * 점포 기본정보 추가 필드
	 */

	//오프라인 parent id
	private String itemParent;

	//오프라인 상품명
	private String lgcItemNm;

	//대표상품여부 (Y: 대표상품, N:옵션상품, NULL: 일반상품)
	private String representYn;
	
	//팩상품여부 (Y: 팩상품, N:낱개상품, NULL: 해당없음)
	private String packYn;

	//팩상품여부명 (Y: 팩상품, N:낱개상품, NULL: 해당없음)
	private String packYnNm;

	//팩상품 또는 낱개상품 리스트
	private List<String> itemPackList;

	//원산지
	private String originTxt;

	/**
	 * 점포 상세정보 추가 필드
	 */
	//연관상품 번호
	private Integer relationNo;

	//온라인재고설정 -설정함/설정안함(Y/N)
	private String onlineStockYn;

	//온라인재고설정시작일
	private String onlineStockStartDt;

	//온라인재고설정종료일
	private String onlineStockEndDt;

	//재고관리유형
	private String stockType;
	//재고관리유형명
	private String stockTypeNm;

	//장바구니제한여부
	private String cartYn;
	//성인카테고리여부
	private String adultLimitYn;
	//도서카테고리여부
	private String isbnYn;

}
