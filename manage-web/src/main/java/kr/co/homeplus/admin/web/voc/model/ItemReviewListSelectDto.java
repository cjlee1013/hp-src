package kr.co.homeplus.admin.web.voc.model;

import java.math.BigDecimal;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ItemReviewListSelectDto {

    @RealGridColumnInfo(headText = "상품평번호", width = 80)
    private Long reviewNo;

    @RealGridColumnInfo(headText = "점포유형", width = 60, order = 1)
    private String storeType;

    @RealGridColumnInfo(headText = "결제번호", hidden = true, order = 2)
    private Long paymentNo;

    @RealGridColumnInfo(headText = "번들번호", hidden = true, order = 3)
    private Long bundleNo;

    @RealGridColumnInfo(headText = "주문번호", width = 120, order = 4)
    private Long purchaseOrderNo;

    @RealGridColumnInfo(headText = "대분류", width = 120, order = 5)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", width = 120, order = 6)
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류", width = 120, order = 7)
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류", width = 120, order = 8)
    private String dcateNm;

    @RealGridColumnInfo(headText = "주문상품번호", hidden = true, order = 9)
    private Long orderItemNo;

    @RealGridColumnInfo(headText = "상품번호", order = 10, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "회원번호", hidden = true, order = 11)
    private Long userNo;

    @RealGridColumnInfo(headText = "작성자", width = 80, order = 12)
    private String userNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 120, order = 13, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "상품명", width = 250, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, order = 14)
    private String itemNm1;

    @RealGridColumnInfo(headText = "옵션명", hidden = true, order = 15)
    private String selOpt1Title;

    @RealGridColumnInfo(headText = "회원아이디", hidden = true, order = 16)
    private String userId;

    @RealGridColumnInfo(headText = "상품평내용", width = 350, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, order = 17)
    private String contents;

//    @RealGridColumnInfo(headText = "상품평 이미지", hidden = true, order = 12)
//    private String imgList;

    @RealGridColumnInfo(headText = "만족도", width = 50, order = 18, sortable = true)
    private BigDecimal grade;

    @RealGridColumnInfo(headText = "만족도", hidden = true, order = 19)
    private int gradeStatus;

    @RealGridColumnInfo(headText = "만족도(상품상태)", hidden = true, order = 20)
    private String gradeStatusNm;

    @RealGridColumnInfo(headText = "만족도(상품정보일치여부)", hidden = true, order = 21)
    private int gradeAccuracy;

    @RealGridColumnInfo(headText = "만족도(상품정보일치여부)", hidden = true, order = 22)
    private String gradeAccuracyNm;

    @RealGridColumnInfo(headText = "만족도(가격)", hidden = true, order = 23)
    private int gradePrice;

    @RealGridColumnInfo(headText = "만족도(가격)", hidden = true, order = 24)
    private String gradePriceNm;

    @RealGridColumnInfo(headText = "판매업체명", width = 120, order = 25)
    private String businessNm;

    @RealGridColumnInfo(headText = "판매업체ID", width = 120, hidden = true, order = 26)
    private String partnerId;

    @RealGridColumnInfo(headText = "적립여부", width = 60, order = 27)
    private String reserveYnNm;

    @RealGridColumnInfo(headText = "적립일", hidden = true, order = 28)
    private String reserveDt;

    @RealGridColumnInfo(headText = "노출여부", width = 60, order = 29)
    private String blockYnNm;

    @RealGridColumnInfo(headText = "미노출사유", width = 150, order = 30)
    private String blockContents;

    @RealGridColumnInfo(headText = "점포명", width = 70, order = 31, sortable = true)
    private String storeNm;

    @RealGridColumnInfo(headText = "수정자", width = 70, order = 32)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 120, order = 33)
    private String chgDt;

    @RealGridColumnInfo(headText = "적립여부", hidden = true, order = 34)
    private String reserveYn;

    @RealGridColumnInfo(headText = "노출여부", hidden = true, order = 35)
    private String blockYn;


    private int storeId;
    private Long optNo;
    private String selOpt1Val;
    private String attachFilePath;
    private String blockDt;
    private String regId;
    private String regNm;
    private String chgId;

}
