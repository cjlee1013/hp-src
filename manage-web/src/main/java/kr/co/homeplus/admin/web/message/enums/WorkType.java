package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum WorkType implements RealGridLookUpSupport {
    ALL("", "전체"),
    MAIL("mail", "메일"),
    SMS("sms", "SMS"),
    APP_PUSH("push", "푸시"),
    ALIMTALK("alimtalk", "알림톡");

    @Getter
    private final String workType;

    @Getter
    private final String korName;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(WorkType::name, WorkType::getKorName));

    WorkType(final String workType, final String korName) {
        this.workType = workType;
        this.korName = korName;
    }

    public static Map<String, String> getMap() {
        return LOOK_UP_MAP;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
