package kr.co.homeplus.admin.web.search.controller;

import javax.validation.Valid;
import kr.co.homeplus.admin.web.search.model.ReplacedKeywordGetParamDto;
import kr.co.homeplus.admin.web.search.model.ReplacedKeywordGridDto;
import kr.co.homeplus.admin.web.search.model.ReplacedKeywordSetParamDto;
import kr.co.homeplus.admin.web.search.service.ReplacedKeywordService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/replaced")
@RequiredArgsConstructor
public class ReplacedKeywordController {

    private final ReplacedKeywordService replacedKeywordService;

    @GetMapping(value = "/keywordList")
    public String getReplacedKeyword(Model model) {
        model.addAllAttributes(RealGridHelper.create("replacedKeywordGridBaseInfo", ReplacedKeywordGridDto.ReplacedKeyword.class));
        return "/search/replacedKeyword";
    }

    @ResponseBody
    @PostMapping(value = {"/insertKeyword.json"})
    public Integer insertReplacedKeyword(@RequestBody @Valid ReplacedKeywordSetParamDto param) {
        return (Integer)replacedKeywordService.insertReplacedKeyword(param).getData();
    }
    @ResponseBody
    @PostMapping(value = {"/{replacedId}/updateKeyword.json"})
    public Integer updateReplacedKeyword(@RequestBody @Valid ReplacedKeywordSetParamDto param, @PathVariable(name = "replacedId", required = false) Long replacedId) {
        return (Integer)replacedKeywordService.updateReplacedKeyword(param, replacedId).getData();
    }
    @ResponseBody
    @GetMapping(value = {"/{replacedId}/deleteKeyword.json"})
    public Integer deleteReplacedKeyword(@PathVariable(name = "replacedId") Long replacedId) {
        return (Integer)replacedKeywordService.deleteReplacedKeyword(replacedId).getData();
    }
    @ResponseBody
    @PostMapping(value = {"/keywordList.json"})
    public Object getReplacedKeyword(@RequestBody @Valid ReplacedKeywordGetParamDto param) {
        return replacedKeywordService.getReplacedKeyword(param).getData();
    }
}
