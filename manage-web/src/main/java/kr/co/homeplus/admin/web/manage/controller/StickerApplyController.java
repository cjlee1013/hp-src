package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.ItemNmListGetDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.ItemNmListParamDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.StickerApplyGetDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.StickerApplyItemGetDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.StickerApplyParamDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.StickerApplySetDto;
import kr.co.homeplus.admin.web.manage.model.stickerApplyItem.StickerItemExcelDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/manage")
@Slf4j
public class StickerApplyController {

    private final CodeService codeService;
    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final ExcelUploadService excelUploadService;

    public StickerApplyController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService,
        ExcelUploadService excelUploadService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.excelUploadService = excelUploadService;
    }

    @RequestMapping(value = "/sticker/stickerApplyMain", method = RequestMethod.GET)
    public String stickerManageMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"    // 사용여부
            , "sticker_period_type"
        );
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("stickerPeriodType", code.get("sticker_period_type"));

        model.addAllAttributes(
            RealGridHelper.create("stickerApplyListGridBaseInfo", StickerApplyGetDto.class));
        model.addAllAttributes(RealGridHelper
            .create("stickerApplyItemListGridBaseInfo", StickerApplyItemGetDto.class));
        return "/manage/stickerApplyMain";
    }

    @ResponseBody
    @RequestMapping(value = {"/sticker/getStickerApplyList.json"}, method = RequestMethod.GET)
    public List<StickerApplyGetDto> getStickerList(StickerApplyParamDto stickerApplyParamDto)
        throws Exception {
        String apiUri = "/manage/stickerApply/getStickerApplyList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StickerApplyGetDto>>>() {
        };
        return (List<StickerApplyGetDto>) resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE, StringUtil
                    .getRequestString(apiUri, StickerApplyParamDto.class, stickerApplyParamDto),
                typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/sticker/getStickerApplyItemList.json"}, method = RequestMethod.GET)
    public List<StickerApplyItemGetDto> getStickerApplyItemList(
        @RequestParam(value = "stickerApplyNo") Long stickerApplyNo) throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , "/manage/stickerApply/getStickerApplyItemList?stickerApplyNo=" + stickerApplyNo
            , new ParameterizedTypeReference<ResponseObject<List<StickerApplyItemGetDto>>>() {
            }).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/sticker/setStickerApply.json"}, method = RequestMethod.POST)
    public ResponseResult setStickerApply(@RequestBody StickerApplySetDto stickerApplySetDto)
        throws Exception {
        String apiUri = "/manage/stickerApply/setStickerApply";
        stickerApplySetDto.setUserId(cookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, stickerApplySetDto,
            apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
            }).getData();
    }

    /**
     * 일괄등록 팝업
     */
    @RequestMapping(value = "/popup/uploadStickerApplyPop", method = RequestMethod.GET)
    public String uploadStickerApplyPop(Model model
        , @RequestParam(value = "callBackScript", defaultValue = "") String callBackScript)
        throws Exception {
        model.addAttribute("callBackScript", callBackScript);

        return "/manage/pop/uploadStickerApplyPop";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 일괄등록
     */
    @ResponseBody
    @PostMapping("/sticker/stickerApplyItemExcel.json")
    public List<ItemNmListGetDto> stickerApplyItemExcel(
        MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {

        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<StickerItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품번호", "itemNo", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<StickerItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(
            StickerItemExcelDto.class, headers, 1, 0)
            .maxRows(1000)
            .build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3-1. 최대개수 확인
        if (excelData.size() <= 0) {
            //등록 요청 상품이 없습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1012);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<String> itemNoList = new ArrayList<>();

        for (StickerItemExcelDto itemInfo : excelData) {
            if (org.springframework.util.StringUtils.isEmpty(itemInfo.getItemNo())) {
                continue;
            }
            itemNoList.add(itemInfo.getItemNo());
        }

        // 3-3. 상품정보 조회
        ItemNmListParamDto itemNmListParamDto = new ItemNmListParamDto();
        itemNmListParamDto.setItemNoList(itemNoList);

        String apiUri = "/common/item/getItemNmList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemNmListGetDto>>>() {
        };

        return(List<ItemNmListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemNmListParamDto,  apiUri, typeReference).getData();
    }
}