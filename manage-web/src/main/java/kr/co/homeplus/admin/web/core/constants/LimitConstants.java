package kr.co.homeplus.admin.web.core.constants;

/**
 * 제한 관련 공통 상수
 */
public class LimitConstants {
    // 조회 개수 제한
    public static final int LIST_LIMIT = 5000;

    // 조회 개수 제한(리얼 그리드 적용 3만)
    public static final int LIST_LIMIT_REAL_GRID = 30000;

    // 조회 개수 제한 메시지
    public static final String LIST_LIMIT_MSG = "검색 결과 수가 많아 모두 출력할 수가 없습니다.\n검색 조건을 추가하여 다시 검색해주세요.";
}
