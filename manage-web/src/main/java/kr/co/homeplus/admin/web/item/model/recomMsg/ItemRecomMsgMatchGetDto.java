package kr.co.homeplus.admin.web.item.model.recomMsg;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ItemRecomMsgMatchGetDto {

    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 300, columnType = RealGridColumnType.NAME)
    private String itemNm;

    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME)
    private String regDt;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;
}