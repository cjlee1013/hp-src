package kr.co.homeplus.admin.web.claim.model.outOfStock;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutOfStockItemListSetDto {


    @ApiModelProperty(value = "클레임 검색기간", position = 1)
    private String schDateType;

    @ApiModelProperty(value = "조회시작일", position = 2)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 3)
    private String schEndDt;

    @ApiModelProperty(value = "점포유형", position = 4)
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 5)
    private String schStoreId;

    @ApiModelProperty(value = "판매업체ID", position = 6)
    private String schShiftId;

    @ApiModelProperty(value = "대체여부", position = 7)
    private String schSubstitutionYn;

    @ApiModelProperty(value = "대체상태", position = 8)
    private String schSubStatus;

    @ApiModelProperty(value = "취소상태", position = 9)
    private String schRefundStatus;

    @ApiModelProperty(value = "상세검색타입", position = 10)
    private String schSearchType;

    @ApiModelProperty(value = "상세검색", position = 11)
    private String schSearch;

    @ApiModelProperty(value = "조회 단축번호 시작", position = 11)
    private int schStartSordNo;

    @ApiModelProperty(value = "조회 단축번호 종료", position = 12)
    private int schEndSordNo;
}
