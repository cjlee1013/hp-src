package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import java.util.Map;

public class DataManagementCategorySearchListGetDto {
    private Object query;
    private long totalCount;
    private List<Map<String, Object>> dataList;

    public Object getQuery() {
        return query;
    }

    public void setQuery(Object query) {
        this.query = query;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<Map<String, Object>> getDataList() {
        return dataList;
    }

    public void setDataList(List<Map<String, Object>> dataList) {
        this.dataList = dataList;
    }
}
