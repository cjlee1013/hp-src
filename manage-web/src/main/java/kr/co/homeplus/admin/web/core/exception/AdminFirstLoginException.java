package kr.co.homeplus.admin.web.core.exception;

import kr.co.homeplus.admin.web.core.dto.UserInfo;

public class AdminFirstLoginException extends RuntimeException {

    private UserInfo userInfo;

    public AdminFirstLoginException(UserInfo userInfo, String message) {
        super(message);
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
