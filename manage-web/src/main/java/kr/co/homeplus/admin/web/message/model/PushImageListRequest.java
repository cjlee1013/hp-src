package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class PushImageListRequest {

    /**
     * 검색시작일
     */
    private String schStartDate;
    /**
     * 검색종료일
     */
    private String schEndDate;
    /**
     * 이미지 이름
     */
    private String imageName;
}
