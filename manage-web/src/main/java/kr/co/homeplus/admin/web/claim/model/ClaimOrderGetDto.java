package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimOrderGetDto {

    @ApiModelProperty(value = "클레임구분")
    @RealGridColumnInfo(headText = "클레임구분", width = 80, sortable = true)
    private String claimType;

    @ApiModelProperty(value = "처리상태")
    @RealGridColumnInfo(headText = "처리상태", width = 60, sortable = true)
    private String claimStatus;

    @ApiModelProperty(value = "신청일시")
    @RealGridColumnInfo(headText = "신청일시", columnType = RealGridColumnType.DATETIME, width = 150, sortable = true)
    private String requestDt;

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시", columnType = RealGridColumnType.DATETIME, width = 150, sortable = true)
    private String processDt;

    @ApiModelProperty(value = "그룹클레임번호")
    @RealGridColumnInfo(headText = "그룹클레임번호", columnType = RealGridColumnType.SERIAL, sortable = true)
    private String claimNo;

    @ApiModelProperty(value = "클레임번호")
    @RealGridColumnInfo(headText = "클레임번호", columnType = RealGridColumnType.SERIAL, width = 80, sortable = true)
    private String claimBundleNo;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", columnType = RealGridColumnType.SERIAL, width = 80, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", columnType = RealGridColumnType.SERIAL, width = 80, sortable = true)
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    @RealGridColumnInfo(headText = "상품 주문번호", columnType = RealGridColumnType.SERIAL, width = 130, sortable = true)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.SERIAL, width = 130, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 240, columnType = RealGridColumnType.NONE, sortable = true)
    private String itemName;

    @ApiModelProperty(value = "신청수량")
    @RealGridColumnInfo(headText = "신청수량", columnType = RealGridColumnType.SERIAL, width = 60, sortable = true)
    private String claimItemQty;

    @ApiModelProperty(value = "신청채널")
    @RealGridColumnInfo(headText = "신청채널", width = 80, sortable = true)
    private String requestId;

    @ApiModelProperty(value = "신청사유")
    @RealGridColumnInfo(headText = "신청사유", width = 180, sortable = true)
    private String claimReasonType;

    @ApiModelProperty(value = "보류일")
    @RealGridColumnInfo(headText = "보류일", columnType = RealGridColumnType.DATETIME, width = 150, sortable = true)
    private String pendingDt;

    @ApiModelProperty(value = "보류사유")
    @RealGridColumnInfo(headText = "보류사유", width = 80, sortable = true)
    private String pendingReasonType;

    @ApiModelProperty(value = "거부사유")
    @RealGridColumnInfo(headText = "거부사유", width = 80, sortable = true)
    private String rejectReasonType;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 60, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 80, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 80, columnType = RealGridColumnType.NONE, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "마켓연동")
    @RealGridColumnInfo(headText = "마켓연동", width = 80, sortable = true)
    private String marketType;

    @ApiModelProperty(value = "마켓주문번호")
    @RealGridColumnInfo(headText = "마켓주문번호", width = 80, sortable = true)
    private String marketOrderNo;

    @ApiModelProperty(value = "구매자회원번호")
    @RealGridColumnInfo(headText = "구매자회원번호", columnType = RealGridColumnType.SERIAL, sortable = true)
    private String userNo;

    @ApiModelProperty(value = "수령인이름")
    @RealGridColumnInfo(headText = "수령인이름", width = 80, sortable = true)
    private String buyerNm;

    @ApiModelProperty(value = "수령인연락처")
    @RealGridColumnInfo(headText = "수령인연락처", width = 120, sortable = true)
    private String buyerMobileNo;

    @ApiModelProperty(value = "클레임타입코드", position = 31)
    @RealGridColumnInfo(headText = "클레임타입코드", hidden = true, sortable = true)
    private String claimTypeCode;

}
