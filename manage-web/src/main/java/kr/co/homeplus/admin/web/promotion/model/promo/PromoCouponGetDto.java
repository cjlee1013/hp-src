package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridType;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true, realGridType = RealGridType.SINGLE_ROW_SELECT)
@Getter
@Setter
public class PromoCouponGetDto {
    @RealGridColumnInfo(headText = "쿠폰 번호")
    private Long couponNo;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @RealGridColumnInfo(headText = "관리쿠폰명", width = 300, columnType = RealGridColumnType.NAME)
    private String manageCouponNm;

    @RealGridColumnInfo(headText = "전시쿠폰명", width = 300, columnType = RealGridColumnType.NAME)
    private String displayCouponNm;

    @RealGridColumnInfo(headText = "적용시스템", width = 200)
    private String applySystem;

    @RealGridColumnInfo(headText = "쿠폰종류")
    private String couponType;

    @RealGridColumnInfo(headText = "쿠폰상태")
    private String status;

    @RealGridColumnInfo(headText = "할인액/율")
    private String discount;

    @RealGridColumnInfo(headText = "우선순위", hidden = true)
    private Integer priority;
}