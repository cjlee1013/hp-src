package kr.co.homeplus.admin.web.claim.model.storeMultiCancel;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreMultiCancelListSetDto {

    @ApiModelProperty(value = "클레임 검색기간", position = 1)
    private String schDateType;

    @ApiModelProperty(value = "조회시작일", position = 2)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 3)
    private String schEndDt;

    @ApiModelProperty(value = "점포유형", position = 4)
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 5)
    private String schStoreId;

    @ApiModelProperty(value = "배송shift", position = 6)
    private String schShiftId;

    @ApiModelProperty(value = "배송상태", position = 8)
    private String schShipStatus;

    @ApiModelProperty(value = "상세검색타입", position = 9)
    private String schSearchType;

    @ApiModelProperty(value = "상세검색", position = 10)
    private String schSearch;

    @ApiModelProperty(value = "점포유형", position = 11)
    private String schPartnerCd;
}
