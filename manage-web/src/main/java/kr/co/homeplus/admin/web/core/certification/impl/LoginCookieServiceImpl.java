package kr.co.homeplus.admin.web.core.certification.impl;

import static kr.co.homeplus.plus.util.WebCookieUtils.addCookie;
import static kr.co.homeplus.plus.util.WebCookieUtils.deleteCookie;
import static kr.co.homeplus.plus.util.WebCookieUtils.getCookie;
import static kr.co.homeplus.plus.util.WebCookieUtils.getCookieValue;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import java.util.Optional;
import javax.servlet.http.Cookie;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.dto.LoginInitInfo;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.exception.LoginCookieServiceException;
import kr.co.homeplus.admin.web.core.exception.LoginIdSaveCookieServiceException;
import kr.co.homeplus.plus.cookie.model.WebCookieConstants;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.crypto.exception.CryptoClientException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginCookieServiceImpl implements LoginCookieService {

    private final RefitCryptoService refitCryptoService;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    /** 로그인 쿠키 만료시간 **/
    private static final int ADMIN_LOGIN_COOKIE_MAX_AGE = -1;
    /** 어드민 아이디 저장 쿠키 만료시간 **/
    private static final int ADMIN_LOGIN_ID_SAVE_COOKIE_MAX_AGE = -1;
    /** 운영환경 profile**/
    private static final String ACTIVE_PROFILES_PRD = "prd";

    @Override
    public void createLoginCookie(final UserInfo userInfo) {
        String userInfoToJson;
        try {
            userInfoToJson = refitCryptoService.encrypt(new Gson().toJson(userInfo));
        } catch (CryptoClientException | JsonIOException e) {
            log.error("createLoginCookie encrypt failed!:<{}>", userInfo.getUserId());
            throw new LoginCookieServiceException(e.getMessage(), e);
        }
        addCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME, userInfoToJson,
            ADMIN_LOGIN_COOKIE_MAX_AGE);

        //상담원 PC에서 접속하는 DEV환경 도메인이 co.kr로 되어있어 co.kr의 도메인도 동시 생성처리 함
        if (!profilesActive.equals(ACTIVE_PROFILES_PRD)) {
            addCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME, userInfoToJson,
                ADMIN_LOGIN_COOKIE_MAX_AGE, WebCookieConstants.DOMAIN_PRD);
        }
    }

    @Override
    public UserInfo getUserInfo() {
        Cookie cookie = Optional
            .ofNullable(getCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME))
            .orElseThrow(() -> new LoginCookieServiceException("loginCookie not found Exception"));

        if (StringUtils.isEmpty(cookie.getValue())) {
            removeLoginCookie();
            throw new LoginCookieServiceException("loginCookie value isEmpty");
        }

        String decryptUserInfo;
        try {
            decryptUserInfo = refitCryptoService.decrypt(cookie.getValue());
        } catch (CryptoClientException e) {
            removeLoginCookie();
            throw new LoginCookieServiceException("An error occurred while decrypting a loginCookie.", e);
        }
        return new Gson().fromJson(decryptUserInfo, UserInfo.class);
    }

    @Override
    public void removeLoginCookie() {
        if (isLogin()) {
            deleteCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME);

            //상담원 PC에서 로그인 처리한 쿠키 제거
            if (!profilesActive.equals(ACTIVE_PROFILES_PRD)) {
                deleteCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME, WebCookieConstants.DOMAIN_PRD);
            }
        }
    }

    @Override
    public boolean isLogin() {
        return null != getCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME);
    }

    @Override
    public void createIdSaveCookie(final String userId) {
        String encryptUserId;
        try {
            encryptUserId = refitCryptoService.encrypt(userId);
        } catch (CryptoClientException e) {
            log.error("createIdSaveCookie encrypt failed!:<{}>", userId);
            throw new LoginIdSaveCookieServiceException(e.getMessage(), e);
        }

        addCookie(AdminConstants.ADMIN_LOGIN_ID_SAVE_COOKIE_NAME, encryptUserId,
            ADMIN_LOGIN_ID_SAVE_COOKIE_MAX_AGE);

        //상담원 PC에서 접속하는 DEV환경 도메인이 co.kr로 되어있어 co.kr의 도메인도 동시 생성처리 함
        if (!profilesActive.equals(ACTIVE_PROFILES_PRD)) {
            addCookie(AdminConstants.ADMIN_LOGIN_ID_SAVE_COOKIE_NAME, encryptUserId,
                ADMIN_LOGIN_ID_SAVE_COOKIE_MAX_AGE, WebCookieConstants.DOMAIN_PRD);
        }
    }

    @Override
    public void removeIdSaveCookie() {
        deleteCookie(AdminConstants.ADMIN_LOGIN_ID_SAVE_COOKIE_NAME);

        //상담원 PC에서 로그인 처리한 쿠키 제거
        if (!profilesActive.equals(ACTIVE_PROFILES_PRD)) {
            deleteCookie(AdminConstants.ADMIN_LOGIN_ID_SAVE_COOKIE_NAME, WebCookieConstants.DOMAIN_PRD);
        }
    }

    @Override
    public LoginInitInfo getLoginInitInfo() {
        final String encryptUserId = getCookieValue(AdminConstants.ADMIN_LOGIN_ID_SAVE_COOKIE_NAME);

        if (StringUtils.isNotEmpty(encryptUserId)) {
            String decryptUserId;
            try {
                decryptUserId = refitCryptoService.decrypt(encryptUserId);
            } catch (CryptoClientException e) {
                removeIdSaveCookie();
                throw new LoginIdSaveCookieServiceException(
                    "An error occurred while decrypting a idSaveCookie.", e);
            }
            return new LoginInitInfo(true, decryptUserId);
        }

        return new LoginInitInfo();
    }
}
