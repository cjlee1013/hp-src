package kr.co.homeplus.admin.web.item.model.safetyStock;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SafetyStockListParamDto {

    private String schType;
    private String schKeyword;
    private String searchCateCd1;
    private String searchCateCd2;
    private String searchCateCd3;
    private String searchCateCd4;
    private String schUseYn;

}
