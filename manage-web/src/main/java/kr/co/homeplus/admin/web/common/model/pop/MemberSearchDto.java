package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.admin.web.user.enums.UserStatus;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MemberSearchDto {
    @RealGridColumnInfo(headText = "회원번호")
    private long userNo;
    @RealGridColumnInfo(headText = "회원명")
    private String userNm;
    @RealGridColumnInfo(headText = "회원ID")
    private String userId;
    @RealGridColumnInfo(headText = "회원상태", columnType = RealGridColumnType.LOOKUP)
    private UserStatus userStatus;
    @RealGridColumnInfo(headText = "휴대폰")
    private String mobile;
    @RealGridColumnInfo(headText = "이메일")
    private String email;
    @RealGridColumnInfo(headText = "MHC ucid", hidden = true)
    private String mhcUcid;
    @RealGridColumnInfo(headText = "마스킹 처리되지 않은 회원명", hidden = true)
    private String originUserNm;
    @RealGridColumnInfo(headText = "마스킹 처리되지 않은 회원ID", hidden = true)
    private String originUserId;
}
