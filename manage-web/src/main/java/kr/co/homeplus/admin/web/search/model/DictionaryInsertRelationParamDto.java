package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 관련어 구성 정보")
public class DictionaryInsertRelationParamDto {
    @ApiModelProperty(notes = "관련유형", position = 1, required = true)
    private String relType;
    @ApiModelProperty(notes = "구성리스트", position = 2, required = true)
    private List<RelationDto> list;
}
