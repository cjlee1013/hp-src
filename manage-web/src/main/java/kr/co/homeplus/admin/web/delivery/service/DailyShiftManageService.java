package kr.co.homeplus.admin.web.delivery.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_CHECK_DUPLICATE_DAILY_SHIFT_MANAGE;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_DELETE_DAILY_SHIFT_MANAGE;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_DAILY_SHIFT_MANAGE_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_DAILY_SHIFT_STORE_MANAGE_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_SAVE_DAILY_SHIFT_MANAGE;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_SAVE_DAILY_SHIFT_STORE_MANAGE_LIST;

import io.swagger.models.auth.In;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftDuplicateCheckModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftManageSelectModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftStoreManageModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailyShiftManageService {
    private final ResourceClient resourceClient;

    /**
     * 일자별 Shift 정보 조회
     * @param dailyShiftManageSelectModel
     * @return
     * @throws Exception
     */
    public List<DailyShiftManageModel> getDailyShiftManageList(DailyShiftManageSelectModel dailyShiftManageSelectModel) throws Exception {
        String apiUri = ESCROW_GET_DAILY_SHIFT_MANAGE_LIST;
        ResourceClientRequest<List<DailyShiftManageModel>> request = ResourceClientRequest.<List<DailyShiftManageModel>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(dailyShiftManageSelectModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 일자별 Shift 점포 정보 조회
     * @param dailyShiftMngSeq
     * @return
     * @throws Exception
     */
    public List<DailyShiftStoreManageModel> getDailyShiftStoreManageList(long dailyShiftMngSeq) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("dailyShiftMngSeq", dailyShiftMngSeq));
        String apiUri = ESCROW_GET_DAILY_SHIFT_STORE_MANAGE_LIST + StringUtil.getParameter(setParameterList);
        ResourceClientRequest<List<DailyShiftStoreManageModel>> request = ResourceClientRequest.<List<DailyShiftStoreManageModel>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 일자별 Shift 관리 중복체크
     * @param dailyShiftDuplicateCheckModel
     * @return
     * @throws Exception
     */
    public Integer checkDuplicateDailyShiftManage(DailyShiftDuplicateCheckModel dailyShiftDuplicateCheckModel) throws Exception {
        String apiUri = ESCROW_CHECK_DUPLICATE_DAILY_SHIFT_MANAGE;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(dailyShiftDuplicateCheckModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 일자별 Shift 정보 저장
     * @param dailyShiftManageModel
     * @return
     * @throws Exception
     */
    public DailyShiftManageModel saveDailyShiftManage(DailyShiftManageModel dailyShiftManageModel) throws Exception {
        String apiUri = ESCROW_SAVE_DAILY_SHIFT_MANAGE;
        ResourceClientRequest<DailyShiftManageModel> request = ResourceClientRequest.<DailyShiftManageModel>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(dailyShiftManageModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 일자별 Shift 점포 정보 저장
     * @param dailyShiftStoreManageModelList
     * @return
     * @throws Exception
     */
    public Integer saveDailyShiftStoreManageList(List<DailyShiftStoreManageModel> dailyShiftStoreManageModelList) throws Exception {
        String apiUri = ESCROW_SAVE_DAILY_SHIFT_STORE_MANAGE_LIST;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(dailyShiftStoreManageModelList)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 일자별 Shift 정보 삭제
     * @param dailyShiftManageModelList
     * @return
     * @throws Exception
     */
    public Integer deleteDailyShiftManage(List<DailyShiftManageModel> dailyShiftManageModelList) throws Exception {
        String apiUri = ESCROW_DELETE_DAILY_SHIFT_MANAGE;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(dailyShiftManageModelList)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }
}
