package kr.co.homeplus.admin.web.message.model;

public enum ReturnCode {

    /**
     * 요청 단계
     */
    READY,

    /**
     * 검증 오류
     */
    INVALID,

    /**
     * 발송 에러
     */
    ERROR,

    /**
     * 발송 진행 (pinpoint 및 tran 요청)
     */
    SENDING,

    /**
     * 발송 요청 취소 (수동 및 예약발송)
     */
    CANCELED,

    /**
     * 발송 차단
     */
    BLOCKED,

    /**
     * 발송 결과 업데이트 완료
     */
    COMPLETED,

    /**
     * 알 수 없는 상태, 발송 업데이트 유효기간이 지나는 등의 상태 마킹
     */
    UNKNOWN,

    /**
     * 기타 상태
     */
    ETC
}
