package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreLockerModel;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreManageModel;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PickupStoreManageService {
    private final ResourceClient resourceClient;

    /**
     * 픽업점포관리 리스트 조회
     * @param pickupStoreManageSelectModel
     * @return
     * @throws Exception
     */
    public List<PickupStoreManageModel> getPickupStoreManageList(PickupStoreManageSelectModel pickupStoreManageSelectModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_PICKUP_STORE_MANAGE_LIST;
        ResourceClientRequest<List<PickupStoreManageModel>> request = ResourceClientRequest.<List<PickupStoreManageModel>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(pickupStoreManageSelectModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 픽업점포 락커 리스트 조회
     * @param pickupStoreLockerModel
     * @return
     * @throws Exception
     */
    public List<PickupStoreLockerModel> getPickupStoreLockerList(PickupStoreLockerModel pickupStoreLockerModel) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PICKUP_STORE_LOCKER_LIST, PickupStoreLockerModel.class, pickupStoreLockerModel);
        ResourceClientRequest<List<PickupStoreLockerModel>> request = ResourceClientRequest.<List<PickupStoreLockerModel>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }
}
