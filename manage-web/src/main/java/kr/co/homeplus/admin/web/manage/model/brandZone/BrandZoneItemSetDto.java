package kr.co.homeplus.admin.web.manage.model.brandZone;

import lombok.Data;

@Data
public class BrandZoneItemSetDto {

    private Long brandzoneNo;

    private String priority;

    private String itemNo;

    private Long optNo;

    private String useYn;
}
