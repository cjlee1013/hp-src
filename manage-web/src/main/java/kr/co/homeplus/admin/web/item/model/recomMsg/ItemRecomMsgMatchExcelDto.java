package kr.co.homeplus.admin.web.item.model.recomMsg;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class ItemRecomMsgMatchExcelDto {
    @EqualsAndHashCode.Include
    private String itemNo;
}