package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.StoreZipcodeModel;
import kr.co.homeplus.admin.web.delivery.model.StoreZipcodeSelectModel;
import kr.co.homeplus.admin.web.delivery.service.StoreZipcodeService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 점포배송정보 > 점포별 우편번호 조회
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class StoreZipcodeController {

    private final AuthorityService authorityService;
    @Autowired
    private CodeService codeService;
    @Autowired
    private StoreZipcodeService storeZipcodeService;

    public StoreZipcodeController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    /**
     * 점포별 우편번호 조회 Main Page
     */
    @GetMapping("/storeZipcodeMain")
    public String storeZipcodeMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // 점포유형 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"
        );
        model.addAttribute("storeType", code.get("store_type"));
        // 시도명 리스트 가져와서 model 에 담기
        model.addAttribute("sidoList", storeZipcodeService.getSidoList());
        model.addAllAttributes(RealGridHelper.create("storeZipcodeGridBaseInfo", StoreZipcodeModel.class));
        return "/delivery/storeZipcodeMain";
    }

    /**
     * 점포별 우편번호 리스트 조회
     * @param storeZipcodeSelectModel
     * @return
     */
    @ResponseBody
    @GetMapping("/getStoreZipcodeList.json")
    public List<StoreZipcodeModel> getStoreZipcodeList(@ModelAttribute StoreZipcodeSelectModel storeZipcodeSelectModel) {
        return storeZipcodeService.getStoreZipcodeList(storeZipcodeSelectModel);
    }
}
