package kr.co.homeplus.admin.web.item.model.expStore;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class ExpStoreHistTotalGetDto {

    @RealGridColumnInfo(headText = "코드", sortable = true, width = 50, columnType = RealGridColumnType.NUMBER)
    private Integer storeId;

    @RealGridColumnInfo(headText = "점포명",columnType= RealGridColumnType.DATETIME,  width = 100 , sortable = true)
    private String storeNm;

    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100 , sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "주문가능여부", width = 80 , sortable = true)
    private String orderAvailYnNm;

    @RealGridColumnInfo(headText = "변경사유", width = 100 , sortable = true)
    private String stopReasonCdNm;

    @RealGridColumnInfo(headText = "주문가능시작시간", width = 80 , sortable = true)
    private String orderStartTime;

    @RealGridColumnInfo(headText = "주문가능종료시간", width = 80 , sortable = true)
    private String orderEndTime;

    @RealGridColumnInfo(headText = "배달소요시간", width = 80, sortable = true)
    private String deliveryTime;

    @RealGridColumnInfo(headText = "수정자", width = 100 , sortable = true)
    private String chgNm;


}
