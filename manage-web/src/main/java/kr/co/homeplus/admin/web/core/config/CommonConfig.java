package kr.co.homeplus.admin.web.core.config;

import ch.qos.logback.classic.helpers.MDCInsertingServletFilter;
import javax.servlet.Filter;
import kr.co.homeplus.admin.web.core.filter.HomeplusSpecialCharacterFilter;
import kr.co.homeplus.admin.web.core.filter.HomeplusXssFilter;
import kr.co.homeplus.admin.web.core.filter.TraceLogFilter;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * 공통 Bean을 관리하는 config<p>
 *
 * 어드민 내 공통으로 사용되는 Bean을 등록 및 관리한다.
 *
 * @see org.springframework.web.multipart.commons.CommonsMultipartResolver
 */
@Configuration
public class CommonConfig {

    private final Environment env;
    private final RefitCryptoService refitCryptoService;

    public CommonConfig (Environment env, RefitCryptoService refitCryptoService) {
        this.env = env;
        this.refitCryptoService = refitCryptoService;
    }

    /** 문자열 인코딩 필터 순서 **/
    private static final int FILTER_ORDER_CHARACTER_ENCODING = 0;
    private static final int FILTER_ORDER_TRACE_LOG = 1;
    /** xss 필터 순서 **/
    private static final int FILTER_ORDER_XSS = 2;
    /** 특수문자 치환 필터 순서 **/
    private static final int FILTER_ORDER_SPECIAL_CHARACTER = 3;

    /**
     * 파일 업로드를 위한 MultipartResolver Bean 설정<p>
     *
     * CommonsMultipartResolver -> StandardServletMultipartResolver로 변경<br>
     * * 문제 : spring-boot로 전환 되면서 기존의 Controller에서 multipart를 받지 못하는 현상으로 수정<br>
     * * 원인 : spring-boot 1.2.0.RELEASE 부터 서블릿 3.1이 적용되어 StandardServletMultipartResolver로 변경<br>
     * * 기타 : 업로드 파일사이즈 등의 설정은 aplication.yml에서 설정 가능
     *
     * @return multipartResolver StandardServletMultipartResolver
     * @see StandardServletMultipartResolver
     */
    @Bean(name = "filterMultipartResolver")
    public StandardServletMultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    public FilterRegistrationBean registerCharacterEncodingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(new CharacterEncodingFilter());
        registrationBean.setOrder(FILTER_ORDER_CHARACTER_ENCODING);
        registrationBean.addUrlPatterns("/*");
        registrationBean.addInitParameter("encoding", "UTF-8");
        registrationBean.addInitParameter("forceEncoding", "true");

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean traceLogFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new TraceLogFilter(refitCryptoService));
        registrationBean.setOrder(FILTER_ORDER_TRACE_LOG);
        registrationBean.addUrlPatterns("/*");

        return registrationBean;
    }

    /**
     * XSS Filter 등록
     *
     * @return {@link FilterRegistrationBean}
     */
    @Bean
    public FilterRegistrationBean hmpXssFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new HomeplusXssFilter());
        registrationBean.setOrder(FILTER_ORDER_XSS);
        registrationBean.addUrlPatterns("/*");

        return registrationBean;
    }

    /**
     * SpecialCharacter(4 Byte 이상 특수문자 치환) Filter 등록
     *
     * @return {@link FilterRegistrationBean}
     */
    @Bean
    public FilterRegistrationBean hmpSpecialCharacterFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new HomeplusSpecialCharacterFilter());
        registrationBean.setOrder(FILTER_ORDER_SPECIAL_CHARACTER);
        registrationBean.addUrlPatterns("/*");

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<Filter> mdcInsertingServletFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new MDCInsertingServletFilter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    /**
     * 세션 리스너를 설정한다.<p>
     * application.yml 파일에서 읽어온 설정을 이용하여 세션 만료시간을 설정한다.
     * <code>
     * admin:
     *   config:
     *     login-expiration-min: 90
     * </code>
     *
     * @return {@link SessionListener}
     */
    @Bean
    public SessionListener sessionListener() {
        //세션 만료시간 설정(분 단위) : expirationMin * 60 * 60
        final int expirationMin = env.getRequiredProperty("admin.config.login-expiration-min", int.class);
        return new SessionListener(expirationMin * 60 * 60);
    }
}