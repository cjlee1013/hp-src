package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class ClaimHistoryListDto {

    @ApiModelProperty(value = "구분", position = 1)
    public String historyReason;

    @ApiModelProperty(value = "처리일시", position = 2)
    public String regDt;

    @ApiModelProperty(value = "상세내역", position = 3)
    public String historyDetail;

    @ApiModelProperty(value = "처리자", position = 4)
    public String regId;

}
