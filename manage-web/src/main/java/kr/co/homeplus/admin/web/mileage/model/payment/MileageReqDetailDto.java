package kr.co.homeplus.admin.web.mileage.model.payment;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageReqDetailDto {
    @ApiModelProperty(value = "마일리지요청상세번호", position = 1)
    @RealGridColumnInfo(headText = "마일리지요청상세번호", width = 100, hidden = true)
    private String mileageReqDetailNo;

    @ApiModelProperty(value = "마일리지요청번호", position = 2)
    @RealGridColumnInfo(headText = "일자", width = 100, hidden = true)
    private String mileageReqNo;

    @ApiModelProperty(value = "회원번호", position = 3)
    @RealGridColumnInfo(headText = "회원번호", width = 200, fieldType = RealGridFieldType.TEXT)
    @NotNull(message = "회원번호가 없습니다.")
    private String maskingUserNo;

    //@ApiModelProperty(value = "회원이름")
    //@RealGridColumnInfo(headText = "회원이름", width = 100, fieldType = RealGridFieldType.TEXT)
    //private String userNm;

    @ApiModelProperty(value = "마일리지금액", position = 4)
    @RealGridColumnInfo(headText = "금액", width = 200, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    @Max(value = 10000000, message = "1,000만 마일리지를 넘어갈수 없습니다.")
    private long mileageAmt;

    @ApiModelProperty(value = "마일리지상세상태", position = 5)
    @RealGridColumnInfo(headText = "상태", width = 100)
    private String mileageDetailStatus;

    @ApiModelProperty(value = "마일리지상세메시지(오류)", position = 7)
    @RealGridColumnInfo(headText = "비고", width = 300)
    private String detailMessage;

    @ApiModelProperty(value = "사용유무", position = 8)
    @RealGridColumnInfo(headText = "사용유무", width = 80, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "등록아이디", position = 9)
    @RealGridColumnInfo(headText = "등록아이디", width = 100, hidden = true)
    private String regId;

    @ApiModelProperty(value = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 100, fieldType = RealGridFieldType.TEXT, hidden = true)
    private String userNo;
}