package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentAccountSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentStateSetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerPayment.PartnerPaymentListSetDto;
import kr.co.homeplus.admin.web.settle.service.PartnerPaymentService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/settle/partnerPayment")
public class PartnerPaymentMngController {

    private final PartnerPaymentService partnerPaymentService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;

    /**
     * 정산관리 > 지급관리 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/partnerPaymentMng", method = RequestMethod.GET)
    public String partnerPaymentMng(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "settle_pay_state"
        );
        model.addAttribute("settlePayState", code.get("settle_pay_state"));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("partnerPaymentMngBaseInfo", PartnerPaymentListGetDto.class));

        return "/settle/partnerPaymentList";
    }

    /**
     * 정산관리 > 지급관리 > 메인 리스트 조회
     * @return PartnerPaymentListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerPaymentList.json", method = RequestMethod.GET)
    public List<PartnerPaymentListGetDto> getPartnerPaymentList(PartnerPaymentListSetDto listParamDto) throws Exception {
        return partnerPaymentService.getPartnerPaymentList(listParamDto);
    }

    /**
     * 정산관리 > 지급예정일 변경 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/popup/setSettlePreDt", method = RequestMethod.GET)
    public String setSettlePreDt(Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("partnerId", request.getParameter("partnerId"));
        model.addAttribute("partnerNm", request.getParameter("partnerNm"));
        model.addAttribute("settlePreDt", request.getParameter("settlePreDt"));
        model.addAttribute("settleSrl", request.getParameter("settleSrl"));

        return "/settle/pop/setSettlePreDt";
    }

    /**
     * 정산관리 > 지급상태 변경
     * @return Integer
     */
    @ResponseBody
    @RequestMapping(value = "/setPartnerPaymentState.json", method = RequestMethod.GET)
    public int setPartnerPaymentState(PartnerPaymentStateSetDto partnerPaymentStateSetDto) throws Exception {
        partnerPaymentStateSetDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return partnerPaymentService.setPartnerPaymentState(partnerPaymentStateSetDto);
    }

    /**
     * 정산관리 > 지급계좌 변경
     * @return Integer
     */
    @ResponseBody
    @RequestMapping(value = "/setPartnerPaymentAccount.json", method = RequestMethod.GET)
    public int setPartnerPaymentAccount(PartnerPaymentAccountSetDto partnerPaymentAccountSetDto) throws Exception {
        partnerPaymentAccountSetDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return partnerPaymentService.setPartnerPaymentAccount(partnerPaymentAccountSetDto);
    }
}