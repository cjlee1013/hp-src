package kr.co.homeplus.admin.web.search.controller;

import kr.co.homeplus.admin.web.search.model.SearchLogIncreaseGridDto;
import kr.co.homeplus.admin.web.search.service.SearchLogIncreaseService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/statistics")
@RequiredArgsConstructor
public class SearchLogIncreaseController {

    private final SearchLogIncreaseService searchLogIncreaseService;

    @GetMapping(value = { "/increaseRate" })
    public String getIncreaseRate(Model model) {
        model.addAllAttributes(RealGridHelper.create("increaseRateGridBaseInfo", SearchLogIncreaseGridDto.SearchLogIncrease.class));
        return "/search/searchLogIncreaseRate";
    }

    @GetMapping(value = { "/increaseRateExp" })
    public String getIncreaseRateExp(Model model) {
        model.addAllAttributes(RealGridHelper.create("increaseRateGridBaseInfo", SearchLogIncreaseGridDto.SearchLogIncrease.class));
        return "/search/searchExpLogIncreaseRate";
    }

    @ResponseBody
    @GetMapping(value= {"/getIncreaseSearchLogList.json"})
    public Object getIncreaseList(
          @RequestParam String dateType
        , @RequestParam String siteType
        , @RequestParam String startDt
        , @RequestParam String endDt
    ) {
        return searchLogIncreaseService.getIncreaseList(dateType, siteType, startDt, endDt).getData();
    }

    @ResponseBody
    @GetMapping(value= {"/getSearchLogCount.json"})
    public ResponseObject getSearchLogCount(
            @RequestParam String siteType,
            @RequestParam String startDt,
            @RequestParam String endDt
    ){
        return searchLogIncreaseService.getSearchLogCount(siteType, startDt, endDt);
    }
}
