package kr.co.homeplus.admin.web.item.controller;

import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.stopDeal.StopDealSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.admin.web.item.model.stopDeal.StopDealParamDto;
import kr.co.homeplus.admin.web.item.model.stopDeal.StopDealGetDto;
import kr.co.homeplus.admin.web.item.model.stopDeal.StopDealListSetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/item/stopDeal")
public class StopDealController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;
    private final ExcelUploadService excelUploadService;

    public StopDealController(ResourceClient resourceClient, LoginCookieService cookieService, CodeService codeService, ExcelUploadService excelUploadService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.excelUploadService = excelUploadService;
    }

    /**
     * 상품관리 > 점포상품관리 > 점포별 취급중지 일괄설정 > Main
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/stopDealMain", method = RequestMethod.GET)
    public String stopDealMain(Model model) throws Exception{

        // 코드정보
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");    // 점포유형

        model.addAttribute("storeType", code.get("store_type"));

        // 그리드
        model.addAllAttributes(RealGridHelper.create("stopDealGridBaseInfo", StopDealGetDto.class));

        return "/item/stopDealMain";
    }

    /*
    * 상품관리 > 점포상품관리 > 점포별 취급중지 상품번호 조회
    * */
    @ResponseBody
    @RequestMapping(value = "/getItemList.json", method = RequestMethod.GET)
    public List<StopDealGetDto> getItemList(@Valid StopDealParamDto stopDealParamDto ) throws Exception {

        String apiUri = "/item/stopDeal/getItemList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StopDealGetDto>>>() {};

        return (List<StopDealGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, StopDealParamDto.class, stopDealParamDto), typeReference).getData();

    }

    /*
     * 상품관리 > 점포상품관리 > 점포별 취급중지 Excel 데이터 전송
     * */
    @ResponseBody
    @RequestMapping(value = "/setStopDealList.json", method = RequestMethod.POST)
    public ResponseResult setStopDealList(@Valid @RequestBody StopDealListSetDto stopDealListSetDto ) throws Exception {

        String apiUri = "/item/stopDeal/setStopDealList";

        stopDealListSetDto.setRegId(cookieService.getUserInfo().getEmpId());
        ResponseResult responseResult = resourceClient.postForResponseObject(
                ResourceRouteName.ITEM, stopDealListSetDto, apiUri
                , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {})
                .getData();

        return responseResult;
    }
    /**
     * 점포 취급중지 엑셀 일괄등록 읽기
     */
    @ResponseBody
    @PostMapping("/stopDealExcel.json")
    public HashMap<String, Object>  stopDealExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");

        HashMap<String, Object> excelResult = new HashMap<>();

        List<StopDealSetDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
                .header(0, "상품번호", "itemNo", true)
                .header(1, "점포ID", "storeId", true)
                .header(2, "취급중지여부", "stopDealYn", true)
                .header(3, "중지시작일", "stopStartDt", true)
                .header(4, "중지종료일", "stopEndDt", true)
                .header(5, "중지사유", "stopReason", true)
                .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<StopDealSetDto> excelUploadOption = new ExcelUploadOption.Builder<>(StopDealSetDto.class, headers, 3, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 5000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 5,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1001, 5000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<StopDealSetDto> resultExcelData = new ArrayList<>();

        for (StopDealSetDto itemInfo : excelData) {

            // 빈 값 체크(상품번호)
            if (org.springframework.util.StringUtils.isEmpty(itemInfo.getItemNo())) {
                continue;
            }

            // 상품번호 자리수 체크
            if (itemInfo.getItemNo().length() != 9) {
                continue;
            }

            // 빈 값 체크(점포ID)
            if (org.springframework.util.StringUtils.isEmpty(itemInfo.getStoreId())) {
                continue;
            }

            // 취급 중지 설정인 경우
            if (itemInfo.getStopDealYn().equals("Y")) {

                // 중지 시작일자 체크
                if ( checkDate(itemInfo.getStopStartDt()) == false) {
                    continue;
                }

                // 중지 종료일자 체크
                if ( checkDate(itemInfo.getStopEndDt()) == false) {
                    continue;
                }

                // 중지 사유 필수 체크
                if ( org.springframework.util.StringUtils.isEmpty( itemInfo.getStopReason() ) ) {
                    continue;
                }
            } else if (itemInfo.getStopDealYn().equals("N")) {

            } else {
                // Y, N이 아닌 경우
                continue;
            }

            resultExcelData.add(itemInfo);
        }

        // 엑셀 데이터 사이즈와 전송이 필요한 데이터만 추린다..
        excelResult.put("ExcelDataSize",excelData.size() );
        excelResult.put("data",resultExcelData );

        // 3-3. 상품정보 조회
        return excelResult;
    }
    /*
     * 상품관리 > 점포상품관리 > 점포별 취급중지 작업단위 조회
     * */
    @ResponseBody
    @RequestMapping(value = "/getStopDealList.json", method = RequestMethod.GET)
    public List<StopDealGetDto> getStopDealList(@RequestParam(name = "workId") String workId ) throws Exception {


        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("workId", workId));
        getParameterList.add(SetParameter.create("regId", cookieService.getUserInfo().getEmpId()));

        String apiUri = "/item/stopDeal/getStopDealList"+ StringUtil.getParameter(getParameterList);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StopDealGetDto>>>() {};

        return (List<StopDealGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();

    }

    /**
     * 날짜 유효성 체크
     */
    private static boolean checkDate(String checkDate) {
        try {
            SimpleDateFormat dateFormatParser = new SimpleDateFormat("yyyy-MM-dd"); //검증할 날짜 포맷 설정
            dateFormatParser.setLenient(false); //false일경우 처리시 입력한 값이 잘못된 형식일 시 오류가 발생
            dateFormatParser.parse(checkDate); //대상 값 포맷에 적용되는지 확인
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}

