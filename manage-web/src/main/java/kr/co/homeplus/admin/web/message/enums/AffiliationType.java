package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

/**
 * Content 에서 사용하는 발송 주체 (본사/점포)
 */
public enum AffiliationType implements RealGridLookUpSupport {

    HEAD("본사"),
    STORE("점포");

    @Getter
    private final String korName;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(AffiliationType::name, AffiliationType::getKorName));

    AffiliationType(final String korName) {
        this.korName = korName;
    }

    public static Map<String, String> getMap() {
        return LOOK_UP_MAP;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
