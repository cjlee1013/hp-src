package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포 애니타임 SHIFT 관리 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StoreAnytimeShiftManageDto {
    @ApiModelProperty(value = "점포ID", position = 1)
    private int storeId;

    @ApiModelProperty(value = "SHIFTID", position = 2)
    private String shiftId;

    @ApiModelProperty(value = "SHIFT애니타임사용여부", position = 3)
    private String shiftAnytimeUseYn;

    @ApiModelProperty(value = "등록자ID", position = 4)
    private String regId;

    @ApiModelProperty(value = "수정자ID", position = 5)
    private String chgId;
}
