package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataManagementTotalSearchItemGetDto {
    private String docId;
    private Admin admin;
    private List<String> searchItemNmTokenList;
    private List<String> dcateNmTokenList;
    private List<String> searchKeyword;
    private List<String> itemOptionNmTokenList;

    private String categorySearchKeyword;
    private List<CategoryBoost> categoryBoostList;
    private List<CategoryBoost> categoryExclusiveList;
    private String brandNmKor;
    private String brandNmEng;
    private String weightCalc;
    private List<String> eventKeywordList;
    private Object physicalCategory;
    private String buyerCnt;
    private String shopNm;

    @Getter
    @Setter
    public static class Admin
    {
        private KeywordAnalyze keywordAnalyze;
        @Getter
        @Setter
        public static class KeywordAnalyze {
            private String keyword;
            private String correctedType;
            private String correctedText;
            private List<String> filtered;
            private List<Map<String,Object>> result;
            private List<String> generals;
            private List<Map<String,Object>> originalResult;
        }
    }

    @Getter
    @Setter
    public static class CategoryBoost
    {
        private String token;
        private List<Rank> ranks;

        @Getter
        @Setter
        public static class Rank
        {
            private String categoryId;
            private String categoryNm;
            private String scorer;
            private boolean exclusive;
            private String boostType;
        }
    }
}
