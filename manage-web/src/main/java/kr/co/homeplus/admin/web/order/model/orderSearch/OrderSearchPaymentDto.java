package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 결제정보 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderSearchPaymentDto {

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 50)
    private String paymentCategory;

    @ApiModelProperty(value = "처리수단")
    @RealGridColumnInfo(headText = "처리수단", width = 80)
    private String paymentType;

    @ApiModelProperty(value = "상세내역")
    @RealGridColumnInfo(headText = "상세내역", width = 120)
    private String paymentDetail;

    @ApiModelProperty(value = "번호")
    @RealGridColumnInfo(headText = "번호")
    private String cardNo;

    @ApiModelProperty(value = "금액")
    @RealGridColumnInfo(headText = "금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C, width = 80)
    private int amt;

    @ApiModelProperty(value = "연동 주문번호")
    @RealGridColumnInfo(headText = "연동 주문번호")
    private String pgOid;

    @ApiModelProperty(value = "PG사")
    @RealGridColumnInfo(headText = "PG사")
    private String pgKind;

    @ApiModelProperty(value = "상점ID")
    @RealGridColumnInfo(headText = "상점ID")
    private String pgMid;

    @ApiModelProperty(value = "t_id")
    @RealGridColumnInfo(headText = "t_id", width = 200)
    private String pgTid;

    @ApiModelProperty(value = "IP")
    @RealGridColumnInfo(headText = "IP")
    private String reqIp;

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시")
    private String paymentFshDt;

    @ApiModelProperty(value = "비고")
    @RealGridColumnInfo(headText = "비고")
    private String explanatoryNote;
}
