package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.auroraBanner.AuroraBannerGetDto;
import kr.co.homeplus.admin.web.item.model.auroraBanner.AuroraBannerLinkGetDto;
import kr.co.homeplus.admin.web.item.model.auroraBanner.AuroraBannerListParamDto;
import kr.co.homeplus.admin.web.item.model.auroraBanner.AuroraBannerSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/auroraBanner")
public class AuroraBannerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 배너 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/auroraBannerMain", method = RequestMethod.GET)
    public String auroraBannerMain(Model model) throws Exception {

        codeService.getCodeModel(model
            , "use_yn"			// 사용여부
            , "dsk_device_gubun"            // 노출 구분
            , "aurora_banner_type"          // 배너 설정
            , "aurora_banner_title"         // 배너 타이틀 사용여부
            , "dsp_main_link_type"          // 링크 타입
        );

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "aurora_banner_type"          // 배너 설정
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        ObjectMapper om = new ObjectMapper();
        model.addAttribute("auroraBannerTypeJson", om.writeValueAsString(code.get("aurora_banner_type")));

        model.addAllAttributes(RealGridHelper.create("auroraBannerGridBaseInfo", AuroraBannerGetDto.class));

        return "/item/auroraBannerMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 배너 관리 리스트
     *
     * @param auroraBannerListParamDto
     * @return List<AuroraBannerGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getAuroraBanner.json"}, method = RequestMethod.GET)
    public List<AuroraBannerGetDto> getAuroraBanner(@Valid AuroraBannerListParamDto auroraBannerListParamDto) {
        String apiUri = "/item/auroraBanner/getAuroraBanner";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AuroraBannerGetDto>>>() {};
        return (List<AuroraBannerGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, AuroraBannerListParamDto.class, auroraBannerListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 배너 등록/수정
     *
     * @param auroraBannerSetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/setAuroraBanner.json"})
    public ResponseResult setAuroraBanner(@Valid @RequestBody AuroraBannerSetParamDto auroraBannerSetParamDto) {
        String apiUri = "/item/auroraBanner/setAuroraBanner";
        auroraBannerSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, auroraBannerSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 배너 관리 리스트
     *
     * @param bannerId
     * @return List<AuroraBannerGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getAuroraBannerLink.json"}, method = RequestMethod.GET)
    public List<AuroraBannerLinkGetDto> getAuroraBannerLink(@RequestParam("bannerId") Long bannerId) {
        String apiUri = "/item/auroraBanner/getAuroraBannerLink?bannerId=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AuroraBannerLinkGetDto>>>() {};
        return (List<AuroraBannerLinkGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + bannerId, typeReference).getData();
    }

}
