package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MileageTypeDto {
    //마일리지 적립 타입 번호
    private String mileageTypeNo;

    //점포 유형
    private String storeType;

    //마일리지 유형명
    private String mileageTypeName;

    //마일리지 유형
    private String mileageCategory;

    //마일리지 종류 (행사적립)
    private String mileageKind;

    //사용 여부
    private String useYn;

    //등록자
    private String regId;

    //항목 수정시 필요한 것들
    //마일리지타입 설명
    private String mileageTypeExplain;

    //고객 노출 문구
    private String displayMessage;

    //등록시작일
    private String regStartDt;

    //등록종료일
    private String regEndDt;

    //유효기간 설정 유형 (TR = 등록일로부터)
    private String expireType = "TR";

    //유효기간 설정일
    private int expireDayInp;
}
