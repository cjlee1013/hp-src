package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class DspLeafletItemCategoryListGetDto {

    @RealGridColumnInfo(headText = "분류번호", columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER, width = 50, sortable = true)
    private Long cateNo;

    @RealGridColumnInfo(headText = "카테고리명", width = 200, order = 1, sortable = true)
    private String cateNm;

    @RealGridColumnInfo(headText = "사용여부", order = 2, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 3, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 4, sortable = true)
    private String chgNm;


    private String regId;
    private String regNm;
    private String regDt;
    private String chgId;
    private String chgDt;
    private Long leafletNo;
    private int priority;

}
