package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.onedayItem.OnedayItemExcelDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainTheme.DspMainThemeListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod.DspMainThemePeriodGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod.DspMainThemePeriodItemExcelDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod.DspMainThemePeriodItemGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod.DspMainThemePeriodListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod.DspMainThemePeriodSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class DspMainThemePeriodController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;
    private final ExcelUploadService excelUploadService;

    /**
     * 사이트관리 > 전시관리 > 메인 테마 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspMainThemePeriod/dspMainThemePeriodMain", method = RequestMethod.GET)
    public String dspMainThemePeriodMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"		            // 사용여부
            , "dsp_main_period_type"    // 조회기간 기준
            , "dsp_main_loc_type"       // 전시위치
            , "dsp_main_store_type"     // 스토어 타입
            , "dsp_main_link_type"    // 링크 타입
        );

        model.addAllAttributes(RealGridHelper.create("dspMainThemePeriodGridBaseInfo", DspMainThemePeriodGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dspMainThemePeriodItemGridBaseInfo", DspMainThemePeriodItemGetDto.class));

        return "/manage/dspMainThemePeriodMain";
    }

    /**
     * 사이트관리 > 전시관리 > 메인 테마 조회
     *
     * @param dspMainThemePeriodListParamDto
     * @return DspMainThemePeriodGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMainThemePeriod/getDspMainThemePeriodList.json"}, method = RequestMethod.GET)
    public List<DspMainThemePeriodGetDto> getDspMainThemePeriodList(DspMainThemePeriodListParamDto dspMainThemePeriodListParamDto) {
        String apiUri = "/manage/dspMainThemePeriod/getDspMainThemePeriodList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspMainThemePeriodGetDto>>>() {};
        return (List<DspMainThemePeriodGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspMainThemePeriodListParamDto.class, dspMainThemePeriodListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 메인 테마 등록/수정
     *
     * @param dspMainThemePeriodSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMainThemePeriod/setDspMainThemePeriod.json"}, method = RequestMethod.POST)
    public ResponseResult setDspMainThemePeriod(@RequestBody DspMainThemePeriodSetParamDto dspMainThemePeriodSetParamDto) {
        String apiUri = "/manage/dspMainThemePeriod/setDspMainThemePeriod";
        dspMainThemePeriodSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspMainThemePeriodSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 메인 테마 상품 조회
     * @param
     * @return DspMainThemePeriodItemGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMainThemePeriod/getDspMainThemePeriodItemList.json"}, method = RequestMethod.GET)
    public List<DspMainThemePeriodItemGetDto> getDspMainThemePeriodItemList(@RequestParam(value = "themeId") Long themeId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("themeId", themeId));

        String apiUriRequest = "/manage/dspMainThemePeriod/getDspMainThemePeriodItemList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspMainThemePeriodItemGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadMainThemePeriodItemPop")
    public String uploadMainThemePeriodItemPop(Model model, @RequestParam(value = "callback") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        return "/manage/pop/uploadMainThemePeriodItemPop";
    }

    /**
     * 메인 테마 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/dspMainThemePeriod/dspMainThemePeriodItemExcel.json")
    public List<DspMainThemePeriodItemExcelDto> dspMainThemePeriodItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<DspMainThemePeriodItemExcelDto> excelData;
        List<DspLeafletItemGetDto> arrResData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품유형", "itemStoreType", true)
            .header(1, "상품번호", "itemNo", true)
            .header(2, "사용여부", "dispYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<DspMainThemePeriodItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(DspMainThemePeriodItemExcelDto.class, headers, 4, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 100;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 100개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 100);
        }

        List<String> itemNoList = excelData.stream().map(DspMainThemePeriodItemExcelDto::getItemNo).distinct().collect(
            Collectors.toList());

        // 3-2. 일괄등록 요청상품 itemNo로 검증
        try {
            String apiUri = "/manage/dspLeaflet/getDspLeafletItemListByExcel";
            arrResData = resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemNoList, apiUri, new ParameterizedTypeReference<ResponseObject<List<DspLeafletItemGetDto>>>() {}).getData();
        } catch (Exception e) {
            //상품 정보 조회 중 오류가 발생하였습니다. 관리자에게 문의해 주세요.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1008);
        }

        // 3-3. 데이터 검증하여 실제 insert 할 model list 생성
        String errorItemNo = "";
        try {
            for(DspMainThemePeriodItemExcelDto item : excelData) {
                if(item.getDispYn().equals("Y")) {
                    item.setDispYnTxt("전시");
                } else {
                    item.setDispYnTxt("전시안함");
                }

                DspLeafletItemGetDto setItemData =
                    arrResData
                        .stream()
                        .filter(data -> item.getItemNo().equals(data.getItemNo()))
                        .findAny().orElse(null);

                if(ObjectUtils.isNotEmpty(setItemData)) {
                    item.setItemNm(setItemData.getItemNm1());
                } else {
                    errorItemNo = item.getItemNo();
                    throw new BusinessLogicException(null);
                }
            }
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1009, excelData.size(), errorItemNo);
        }

        return excelData;
    }

}