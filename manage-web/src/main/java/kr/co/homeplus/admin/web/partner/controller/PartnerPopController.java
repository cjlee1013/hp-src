package kr.co.homeplus.admin.web.partner.controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.PartnerAffiManagerGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerApprovalStatusGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerManagerGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerPickValidCreditGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerDeliveryGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerStatusGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerStatusHistGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerTempPwSetDto;
import kr.co.homeplus.admin.web.partner.model.hist.PartnerSellerHistGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner/popup")
public class PartnerPopController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    public PartnerPopController(ResourceClient resourceClient,
            LoginCookieService cookieService,
            CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;

    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 회원상태 변경 팝업창
     * @param model
     * @param partnerId
     * @param callback
     * @return
     * @throws Exception
     */
    @GetMapping("/getPartnerStatusPop")
    public String partnerStatusMngView(Model model,
            @RequestParam(value = "partnerId") String partnerId,
            @RequestParam(value = "callback") String callback) throws Exception {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));
        setParameterList.add(SetParameter.create("callback", callback));

        ResponseObject<PartnerStatusGetDto> responseObject  = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
                "/partner/getPartnerStatus" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<>() {});

        PartnerStatusGetDto responseData = responseObject.getData();

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "partner_status"			// 파트너 회원상태
        );

        model.addAttribute("partnerStatusCodes", code.get("partner_status"));
        model.addAttribute("partnerId", partnerId);
        model.addAttribute("partnerNm", responseData.getPartnerNm());
        model.addAttribute("partnerStatus", responseData.getPartnerStatus());
        model.addAttribute("partnerStatusNm", responseData.getPartnerStatusNm());
        model.addAttribute("partnerStatusHistList",  new Gson().toJson(responseData.getPartnerStatusHistGetDtoList()));

        model.addAllAttributes(RealGridHelper.createForGroup("partnerStatusHistGridBaseInfo", PartnerStatusHistGetDto.class));

        return "/partner/pop/partnerStatusPop";
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 승인상태변경 팝업창
     * @param model
     * @param partnerId
     * @param callback
     * @return
     * @throws Exception
     */
    @GetMapping("/getPartnerApprovalStatusPop")
    public String getPartnerApprovalStatusPop(Model model,
            @RequestParam(value = "partnerId") String partnerId,
            @RequestParam(value = "callback") String callback) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));
        setParameterList.add(SetParameter.create("callback", callback));

        ResponseObject<PartnerApprovalStatusGetDto> responseObject  = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
                "/partner/getPartnerApprovalStatus" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<>() {});

        PartnerApprovalStatusGetDto responseData = responseObject.getData();

        codeService.getCodeModel(model,
                "approval_status"			// 파트너 회원상태
        );

        model.addAttribute("partnerId", partnerId);
        model.addAttribute("partnerNm", responseData.getPartnerNm());
        model.addAttribute("partnerStatus", responseData.getPartnerApprovalStatus());
        model.addAttribute("partnerStatusNm", responseData.getPartnerApprovalStatusNm());

        return "/partner/pop/partnerApprovalStatusPop";
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 사업자등록번호 조회 팝업창
     * @return
     */
    @GetMapping("/getPartnerNoPop")
    public String getPartnerNoPop() {
        return "/partner/pop/partnerNoPop";
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > PW 관리 팝업창
     * @return
     */
    @GetMapping("/getPartnerPwPop")
    public String getPartnerPwPop(Model model
            , @RequestParam(value = "partnerId") String partnerId
            , @RequestParam(value = "businessNm") String businessNm
    ) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        PartnerAffiManagerGetDto responseData  = resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getPartnerAffiManager" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<PartnerAffiManagerGetDto>>() {}).getData();

        model.addAttribute("partnerId", partnerId);
        model.addAttribute("loginStatusNm", responseData.getLoginStatusNm());
        model.addAttribute("loginRegReason", responseData.getLoginRegReason());
        model.addAttribute("businessNm", responseData.getMngNm());
        model.addAttribute("affiliateEmail", responseData.getMngEmail());
        model.addAttribute("affiliateMobile", responseData.getMngMobile());

        return "/partner/pop/partnerPwPop";
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 임시패스워스 발급
     * @param partnerTempPwSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setPartnerTempPw.json"})
    public ResponseResult setPartnerTempPw(PartnerTempPwSetDto partnerTempPwSetDto) {
        partnerTempPwSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResponseObject<ResponseResult> responseObject  = resourceClient.postForResponseObject(ResourceRouteName.ITEM
                , partnerTempPwSetDto
                , "/partner/setPartnerTempPw"
                , new ParameterizedTypeReference<>() {});

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }

    /**
     * 반품 택배사 팝업창
     * @param model
     * @param request
     * @return
     */
    @PostMapping(value = "/getReturnDeliveryPop")
    public String getReturnDeliveryPop(Model model, HttpServletRequest request) {
        List<PartnerSellerDeliveryGetDto> response  = resourceClient.getForResponseObject(ResourceRouteName.SHIPPING
                , "/admin/shipManage/getReturnDlvCompanyList"
                , new ParameterizedTypeReference<ResponseObject<List<PartnerSellerDeliveryGetDto>>>() {}).getData();

        model.addAttribute("deliveryList",  new Gson().toJson(response));
        model.addAttribute("callback",  request.getParameter("callback"));
        model.addAttribute("deliveryInfo",  request.getParameter("deliveryInfo"));
        model.addAttribute("partnerId",  request.getParameter("partnerId"));
        model.addAttribute("partnerNm",  request.getParameter("partnerNm"));

        return "/partner/pop/deliveryPop";
    }

    /**
     * 파트너관리 > 판매자관리 > 택배사 신용코드 유효성 검사
     * @param dlvCd
     * @param creditCd
     * @return
     */
    @ResponseBody
    @GetMapping("/getCompanyCreditValidInfo.json")
    public List<PartnerPickValidCreditGetDto> getCompanyCreditValidInfo(@RequestParam(value = "dlvCd") String dlvCd, @RequestParam(value = "creditCd") String creditCd) {
        List<PartnerSellerDeliveryGetDto> partnerSellerDeliveryGetDtoList = new ArrayList<>();

        PartnerSellerDeliveryGetDto partnerSellerDeliveryGetDto = new PartnerSellerDeliveryGetDto();
        partnerSellerDeliveryGetDto.setDlvCd(dlvCd);
        partnerSellerDeliveryGetDto.setCreditCd(creditCd);
        partnerSellerDeliveryGetDtoList.add(partnerSellerDeliveryGetDto);

        return resourceClient.postForResponseObject(ResourceRouteName.SHIPPING
                ,   partnerSellerDeliveryGetDtoList
                , "/tracking/pick/getInvalidCredit"
                , new ParameterizedTypeReference<ResponseObject<List<PartnerPickValidCreditGetDto>>>() {}).getData();
    }

    /**
     * 판매업체 관리 > 변경이력 팝업창
     * @param model
     * @param partnerId
     * @return
     */
    @RequestMapping(value = "/partnerSellerHistPop", method = RequestMethod.GET)
    public String partnerSellerHistPop(Model model, @RequestParam String partnerId) {
        model.addAttribute("partnerId", partnerId);
        model.addAllAttributes(RealGridHelper.createForGroup("partnerSellerHistGridBaseInfo", PartnerSellerHistGetDto.class));

        return "/partner/pop/partnerSellerHistPop";
    }

}
