package kr.co.homeplus.admin.web.settle.model.minusSettle;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MinusSettleCompleteSetDto {
  @ApiModelProperty(value = "지급SRL")
  private String settleSrl;

  @ApiModelProperty(value = "수정자")
  private String chgId;
}
