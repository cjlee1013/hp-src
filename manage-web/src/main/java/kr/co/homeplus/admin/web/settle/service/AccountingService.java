package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.accounting.APBalanceSheetGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.APInvoiceListSetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.APInvoiceListGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.AccountSubjectListGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.AccountSubjectListSetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service
public class AccountingService {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public AccountingService(ResourceClient resourceClient, LoginCookieService loginCookieService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
    }

    //계정과목별 조회
    public List<AccountSubjectListGetDto> getAccountSubjectList(AccountSubjectListSetDto listParamDto) throws Exception{
        List<AccountSubjectListGetDto> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE
            , listParamDto
            , "/admin/accounting/getAccountSubjectList",
            new ParameterizedTypeReference<ResponseObject<List<AccountSubjectListGetDto>>>(){}).getData();

        return responseData;
    }

    //AP전표 조회
    public List<APInvoiceListGetDto> getAPInvoiceList(APInvoiceListSetDto listParamDto) throws Exception{
        List<APInvoiceListGetDto> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE
            , listParamDto
            , "/admin/accounting/getAPInvoiceList",
            new ParameterizedTypeReference<ResponseObject<List<APInvoiceListGetDto>>>(){}).getData();

        return responseData;
    }

    //AP전표 상세데이터 조회
    public List<APBalanceSheetGetDto> getAPBalanceSheet(APInvoiceListSetDto listParamDto) throws Exception{
        List<APBalanceSheetGetDto> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE
            , listParamDto
            , "/admin/accounting/getAPBalanceSheet",
            new ParameterizedTypeReference<ResponseObject<List<APBalanceSheetGetDto>>>(){}).getData();

        return responseData;
    }

    public ResponseObject<String> setAPInvoiceConfirm(@ModelAttribute List<APInvoiceListSetDto> listParamDto) throws Exception{
        for (APInvoiceListSetDto dto : listParamDto) {
            dto.setRegId(loginCookieService.getUserInfo().getEmpId());
        }

        ResponseObject<String> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE
            , listParamDto
            , "/admin/accounting/setAPInvoiceConfirm",
            new ParameterizedTypeReference<ResponseObject<String>>() {});

        return responseData;
    }

    //AP전표목록 조회
    public List<BalanceSheetSlipTypeGetDto> getAPSlipTypeList() throws Exception{
        List<BalanceSheetSlipTypeGetDto> responseData = resourceClient.getForResponseObject(
            ResourceRouteName.SETTLE
            , "/admin/accounting/getAPSlipTypeList",
            new ParameterizedTypeReference<ResponseObject<List<BalanceSheetSlipTypeGetDto>>>() {}).getData();

        return responseData;
    }
}
