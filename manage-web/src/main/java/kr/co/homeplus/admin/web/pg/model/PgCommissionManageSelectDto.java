package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제관리 > 결제수단 관리 > PG 수수료 관리 search DTO")
@Getter
@Setter
@EqualsAndHashCode
public class PgCommissionManageSelectDto {
    private String schFromDt;
    private String schEndDt;
    private String schPgKind;
    private String schParentMethodCd;
    private String schUseYn;
    private String schKeyword;
}
