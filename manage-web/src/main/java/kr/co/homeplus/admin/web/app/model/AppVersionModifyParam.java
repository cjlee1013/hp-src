package kr.co.homeplus.admin.web.app.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AppVersionModifyParam {

    @ApiModelProperty(value = "일련번호")
    @NotNull
    @Min(1)
    private int seq;

    @ApiModelProperty(value = "사용여부")
    @NotNull
    private Boolean isUsed;

    @ApiModelProperty(value = "수정자사번")
    private String modEmpId;

    @ApiModelProperty(value = "수정자명")
    private String modEmpNm;

}
