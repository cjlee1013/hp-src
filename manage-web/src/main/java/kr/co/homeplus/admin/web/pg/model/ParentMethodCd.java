package kr.co.homeplus.admin.web.pg.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum ParentMethodCd implements RealGridLookUpSupport {
    CARD("CARD", "신용카드"),
    VBANK("VBANK", "무통장"),
    RBANK("RBANK", "실시간계좌이체"),
    PHONE("PHONE", "휴대폰"),
    KAKAOPAY("KAKAOPAY", "카카오카드"),
    KAKAOMONEY("KAKAOMONEY", "카카오머니"),
    NAVERPAY("NAVERPAY", "네이버페이"),
    SAMSUNGPAY("SAMSUNGPAY", "삼성페이"),
    TOSS("TOSS", "토스"),
    PAYCO("PAYCO", "페이코"),
    MILEAGE("MILEAGE", "포인트 100%"),
    MHC("MHC", "포인트 100%"),
    DGV("DGV", "홈플러스상품권"),
    OCB("OCB", "포인트 100%"),
    FREE("FREE", "0원결제"),
    NONE("NONE", "결제수단없음"),
    MKTNAVER("MKTNAVER", "마켓연동네이버");

    private static Map<String, ParentMethodCd> MAP = Stream
        .of(values()).collect(Collectors.toMap(Enum::name, Function.identity()));

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(Enum::name, ParentMethodCd::getMethodNm));

    private static List<ParentMethodCd> EASY_PAY_LIST = Arrays.asList(
        KAKAOPAY, KAKAOMONEY, NAVERPAY, SAMSUNGPAY, TOSS, PAYCO);

    private static List<ParentMethodCd> BASIC_PARENT_METHOD_LIST = Arrays.asList(
        CARD, RBANK, PHONE, KAKAOPAY, KAKAOMONEY, NAVERPAY, SAMSUNGPAY, TOSS, PAYCO);

    private static List<ParentMethodCd> BENEFIT_PARENT_METHOD_LIST = Arrays.asList(
        CARD, KAKAOPAY, KAKAOMONEY, NAVERPAY, SAMSUNGPAY, PAYCO);

    private String methodCd;
    private String methodNm;

    public static ParentMethodCd getPaymentMethod(String methodCd) {
        for (ParentMethodCd method : ParentMethodCd.values()) {
            if (method.methodCd.equals(methodCd)) {
                return method;
            }
        }
        return null;
    }

    // 간편결제 결제수단 추출
    public static List<ParentMethodCd> getEasyPayMethodList() {
        return EASY_PAY_LIST;
    }

    // 기본 결제수단 리턴 (무통장 제외)
    public static List<ParentMethodCd> getBasicParentMethodList() {
        return BASIC_PARENT_METHOD_LIST;
    }

    // 혜택문구 적용 할 결제수단
    public static List<ParentMethodCd> getBenefitParentMethodList() {
        return BENEFIT_PARENT_METHOD_LIST;
    }

    @JsonCreator // This is the factory method and must be static
    public static ParentMethodCd fromString(String string) {
        return Optional
            .ofNullable(MAP.get(string))
            .orElseThrow(() -> new IllegalArgumentException(string));
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
