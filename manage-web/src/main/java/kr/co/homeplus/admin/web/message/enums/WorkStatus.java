package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum WorkStatus implements RealGridLookUpSupport {

    READY("발송 요청"),
    /**
     * 발송 진행 (pinpoint 및 tran 요청)
     */
    SENDING("발송 진행"),
    ERROR("발송 실패"),
    /**
     * 발송 요청 취소 (수동 및 예약발송)
     */
    CANCELED("발송 요청 취소"),
    /**
     * 발송 결과 업데이트 완료
     */
    COMPLETED("발송 완료"),

    /**
     * 발송시간에 걸려 차단된 상태
     */
    BLOCKED("발송 차단"),
    /**
     * 업로드 상태 (수동발송)
     */
    UPLOADING("대상자 업로드 중"),
    ETC("기타");

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(WorkStatus::name, WorkStatus::getStatusName));

    @Getter
    private final String statusName;

    WorkStatus(final String statusName) {
        this.statusName = statusName;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
