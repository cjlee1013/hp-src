package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineOrderDetailListGetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineOrderDetailListSetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineReceivePayListGetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineReceivePayListSetDto;
import kr.co.homeplus.admin.web.settle.service.DcOnlineReceivePayService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/dcOnlineReceivePay")
public class DcOnlineReceivePayController {

    @Autowired
    private DcOnlineReceivePayService dcOnlineReceivePayService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * DC온라인수불 조회 Main Page
     */
    @RequestMapping(value = "/dcOnlineReceivePayMain", method = RequestMethod.GET)
    public String dcOnlineReceivePayMain(Model model) throws Exception{
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("dcOnlineReceivePayGridBaseInfo", DcOnlineReceivePayListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dcOnlineOrderDetailGridBaseInfo", DcOnlineOrderDetailListGetDto.class));

        return "/settle/dcOnlineReceivePayMain";
    }

    /**
     * DC온라인수불 조회 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getDcOnlineReceivePayList.json")
    public List<DcOnlineReceivePayListGetDto> getDcOnlineReceivePayList(@RequestBody DcOnlineReceivePayListSetDto dcOnlineReceivePayListSetDto) {
        return dcOnlineReceivePayService.getDcOnlineReceivePayList(dcOnlineReceivePayListSetDto);
    }

    /**
     * DC온라인수불 조회 주문상세 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getDcOnlineOrderDetailList.json")
    public List<DcOnlineOrderDetailListGetDto> getDcOnlineOrderDetailList(@RequestBody DcOnlineOrderDetailListSetDto dcOnlineOrderDetailListSetDto) {
        return dcOnlineReceivePayService.getDcOnlineOrderDetailList(dcOnlineOrderDetailListSetDto);
    }
}