package kr.co.homeplus.admin.web.item.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//외부연동 설정관리 리스트 조회
public class MarketItemSettingListParamDto {

    //검색 날짜유형
    private String schDate;

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //세분류 카테고리
    private String schDcateCd;

    //싱상품유형 (B:반품(리세일), N:새상품, R:리퍼, U:중고)
    private String schItemType;

    //외부연동 사이트 ( 네이버, 11번가 )
    private String schPartnerId;

    //외부연동 사이트 등록여부
    private String schSetYn;

    //중지기간설정 여부
    private String schStopPeriodYn;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

    //상품상태
    private String schItemStatus;

}
