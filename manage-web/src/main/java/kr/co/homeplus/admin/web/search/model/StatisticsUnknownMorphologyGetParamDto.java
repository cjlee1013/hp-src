package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticsUnknownMorphologyGetParamDto {
    private String schKeyword;
    private int offset;
    private int limit;

    public int getLimit() {
        return this.limit == 0 ? 100 : this.limit;
    }
}
