package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class GradeStatChangeParam {

    @ApiModelProperty(value = "조회할 회원등급 기준실적월")
    private String gradeApplyYm;

    @ApiModelProperty(value = "Black To Black 변경건만 조회 여부")
    private Boolean isBlackType = false;

}
