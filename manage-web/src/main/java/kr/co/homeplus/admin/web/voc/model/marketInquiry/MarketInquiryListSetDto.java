package kr.co.homeplus.admin.web.voc.model.marketInquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 제휴사 고객문의 리스트 요청 DTO")
public class MarketInquiryListSetDto {
  @ApiModelProperty(notes = "일자타입")
  private String schDtType;

  @ApiModelProperty(notes = "시작일")
  private String schStartDt;

  @ApiModelProperty(notes = "종료일")
  private String schEndDt;

  @ApiModelProperty(notes = "점포ID")
  private String schStoreId;

  @ApiModelProperty(notes = "검색어")
  private String schKeyword;

  @ApiModelProperty(notes = "검색어조건")
  private String schKeywordType;

  @ApiModelProperty(notes = "마켓연동")
  private String schMarketType;

  @ApiModelProperty(notes = "처리상태")
  private String schIsresponseStatus;

  @ApiModelProperty(notes = "등록구분")
  private String schMessageType;

  @ApiModelProperty(notes = "문의타입")
  private String schContactType;
}