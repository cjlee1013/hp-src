package kr.co.homeplus.admin.web.item.model.recipe;

import java.util.List;
import lombok.Data;

@Data
public class RecipeDetailGetDto {

    private Long recipeNo;

    private String title;

    private String cookingQuantity;

    private String cookingTime;

    private String grade;

    private String contentsImgUrl;

    private String titleImgUrl;

    private String cookingTip;

    private String contents;

    private String dispYn;

    private List<RecipeStuffGetDto> stuffList;

    private List<RecipeItemGetDto> itemList;
}