package kr.co.homeplus.admin.web.user.model.consumer;

import kr.co.homeplus.admin.web.user.enums.ConsumerType;
import kr.co.homeplus.admin.web.user.enums.ConsumerReasonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Setter
@Getter
public class ConsumerGrid {

    @RealGridColumnInfo(hidden = true, headText = "일련번호")
    private long seq;
    @RealGridColumnInfo(headText = "회원번호")
    private long userNo;
    @RealGridColumnInfo(headText = "회원명")
    private String userNm;
    @RealGridColumnInfo(headText = "회원ID")
    private String userId;
    @RealGridColumnInfo(headText = "휴대폰번호")
    private String mobile;
    @RealGridColumnInfo(headText = "사유", columnType = RealGridColumnType.LOOKUP)
    private ConsumerReasonType reasonType;
    @RealGridColumnInfo(headText = "권한정지 범위", columnType = RealGridColumnType.LOOKUP)
    private ConsumerType consumerType;
}
