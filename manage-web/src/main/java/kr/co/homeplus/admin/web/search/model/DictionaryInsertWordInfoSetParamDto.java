package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DictionaryInsertWordInfoSetParamDto {
    @ApiModelProperty(notes = "단어명", required = true, position = 1)
    private String wordName;
    @ApiModelProperty(notes = "의미태그",  position = 2)
    private String senseTag;
    @ApiModelProperty(notes = "사전", required = true, position = 3)
    private ArrayList<String> dicType;
    @ApiModelProperty(notes = "품사",  required = true, position = 4)
    private String pos;
    @ApiModelProperty(notes = "명사 문법정보",  position = 5)
    private ArrayList<String> nounAttr;
    @ApiModelProperty(notes = "동사 문법정보",required = true, position = 6)
    private String verbAttr;
    @ApiModelProperty(notes = "개체명 유형",  position = 7)
    private ArrayList<String> neType;
    @ApiModelProperty(notes = "분야",  position = 8)
    private ArrayList<String> domain;
    @ApiModelProperty(notes = "언어",  position = 9)
    private ArrayList<String> language;
    @ApiModelProperty(notes = "간략설명",  position = 10)
    private String note;
    @ApiModelProperty(notes = "뜻풀이",  position = 11)
    private String description;
    @ApiModelProperty(notes = "예시",  position = 12)
    private String example;
    @ApiModelProperty(notes = "범용여부",  position = 13)
    private String isGeneral;
    @ApiModelProperty(notes = "사용여부",  position = 14)
    private boolean useFlag;
    @ApiModelProperty(notes = "금칙어",  position = 15)
    private boolean stopFlag;
    @ApiModelProperty(notes = "등록자",  position = 15)
    private String userId;
}
