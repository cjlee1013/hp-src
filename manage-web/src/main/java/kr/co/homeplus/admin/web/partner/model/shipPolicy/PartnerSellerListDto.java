package kr.co.homeplus.admin.web.partner.model.shipPolicy;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)

public class PartnerSellerListDto {

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 120, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체명")
    @RealGridColumnInfo(headText = "업체명", width = 150, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호", width = 150, sortable = true)
    private String businessNm;

    @ApiModelProperty(value = "사업자등록번호")
    @RealGridColumnInfo(headText = "사업자등록번호", width = 100, sortable = true)
    private String partnerNo;

    @ApiModelProperty(value = "대표자")
    @RealGridColumnInfo(headText = "대표자", width = 100, sortable = true)
    private String partnerOwner;

    @ApiModelProperty(value = "회원상태")
    @RealGridColumnInfo(headText = "회원상태", width = 100, sortable = true)
    private String partnerStatusNm;

    @ApiModelProperty(value = "점포ID")
    @RealGridColumnInfo(headText = "점포ID", columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private String storeId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", sortable = true)
    private String storeNm;

}
