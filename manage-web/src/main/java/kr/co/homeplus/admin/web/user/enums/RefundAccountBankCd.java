package kr.co.homeplus.admin.web.user.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RefundAccountBankCd implements RealGridLookUpSupport {
    KB("04", "국민은행"),
    SHINHAN("88", "신한은행"),
    HANA("81", "하나은행"),
    WOORI("20", "우리은행"),
    IBK("03", "기업은행"),
    ;

    private static final Map<String, String> LOOK_UP_MAP = Stream.of(values()).collect(
        Collectors.toMap(RefundAccountBankCd::name, RefundAccountBankCd::getDesc));

    @Getter
    private final String code;
    @Getter
    private final String desc;

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
