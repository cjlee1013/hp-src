package kr.co.homeplus.admin.web.settle.model.ocb;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PgOcbDailyListGetDto {
    @ApiModelProperty(value = "기준일")
    @RealGridColumnInfo(headText = "기준일", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 100)
    private String storeType;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
    private String storeNm;

    @ApiModelProperty(value = "매출금액")
    @RealGridColumnInfo(headText = "매출액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long purchaseAmt;

    @ApiModelProperty(value = "결제금액")
    @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentAmt;

    @ApiModelProperty(value = "OCB 포인트")
    @RealGridColumnInfo(headText = "OCB 포인트", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long ocbAmt;

    @ApiModelProperty(value = "공급가")
    @RealGridColumnInfo(headText = "공급가", sortable = true, width = 100, groupName = "사용수수료", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long fee;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", sortable = true, width = 100, groupName = "사용수수료", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long feeVat;

    @ApiModelProperty(value = "합계")
    @RealGridColumnInfo(headText = "합계", sortable = true, width = 100, groupName = "사용수수료", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totFee;

    @ApiModelProperty(value = "정산액")
    @RealGridColumnInfo(headText = "정산액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;

}
