package kr.co.homeplus.admin.web.delivery.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
public class StoreSlotManageModel extends StoreShiftManageModel {

    private String slotId;

    private String useYn;

    private String slotShipStartTime;

    private String slotShipEndTime;

    private String slotShipCloseTime;

    private Long orderFixCnt;

    private Long orderCnt;

    private Long orderChgCnt;

    private Long pickupOrderFixCnt;

    private Long pickupOrderCnt;

    private Long pickupOrderChgCnt;

    private Long lockerOrderFixCnt;

    private Long lockerOrderCnt;

    private Long lockerOrderChgCnt;

    private Long reserveOrderFixCnt;

    private Long reserveOrderChgCnt;

    private String chgId;

    private String chgDt;

}
