package kr.co.homeplus.admin.web.core.interceptor;

import static kr.co.homeplus.admin.web.core.constants.AdminConstants.REST_URI_SUFFIX;

import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.admin.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.config.AuthorizationProperties;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.ValidateAuthorizedMenuException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 현재 접근하는 메뉴가 인가 된 메뉴인지을 체크하는 인터셉터 입니다. 체크하는 내용은 아래와 같습니다.<p>
 * 현재 접근하는 메뉴의 접근권한을 가지고 있는지 체크하여, 메뉴 접근권한이 없을 경우 'HasNotRoleException'(403 forbidden)을 발생시킵니다.
 *
 * interceptor 순서는 certificationInterceptor 다음으로 실행합니다.
 */
@Slf4j
public class ValidateAuthorizedMenuInterceptor extends HandlerInterceptorAdapter {

    private UserMenuService userMenuService;
    private AuthorizationProperties properties;
    private LoginCookieService loginCookieService;

    public ValidateAuthorizedMenuInterceptor(final AuthorizationProperties properties,
        final UserMenuService userMenuService, final LoginCookieService loginCookieService) {
        this.properties = properties;
        this.userMenuService = userMenuService;
        this.loginCookieService = loginCookieService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //메뉴 접근권한 체크여부
        if (handler instanceof HandlerMethod && properties.isInterceptorCheckEnable()) {
            isPermissionMenu(request, (HandlerMethod) handler);
        }
        return super.preHandle(request, response, handler);
    }

    /**
     * requestUri 기준으로 현재 접근하는 메뉴에 사용자의 접근권한을 검증합니다.<p>
     *     <pre>
     *         1. 현재 접근하는 메뉴의 접근권한을 가지고 있는지 체크하여, 메뉴 접근권한이 없을 경우
     *         'HasNotRoleException'(403 forbidden)을 발생시킵니다.
     *     </pre>
     *
     * @param request request
     * @param handler HandlerMethod
     */
    private void isPermissionMenu(final HttpServletRequest request, final HandlerMethod handler) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();

        if (isNotValidateUri(request.getRequestURI(), handler, antPathMatcher)) {
            // 현재 접근하는 url과 매칭되는 권한정보를 추출
            final String uriPath = getUriPath(request.getRequestURL().toString());

            // auth-api를 통해 접근여부 체크
            if (isNotPermissionMenuFromAuth(uriPath)) {
                log.error("Access Denied:you do not have permission to access this uri:<{}>", uriPath);
                throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
            }
        }
    }

    /**
     * 현재 접근한 URI가 검증 제외대상인지 체크합니다.
     * @param requestUri request URI
     * @param handler  HandlerMethod
     * @param antPathMatcher AntPathMatcher
     * @return Boolean
     */
    private boolean isNotValidateUri(final String requestUri, final HandlerMethod handler,
        final AntPathMatcher antPathMatcher) {
        return isNotAjaxUri(requestUri) && isAuthCheckUri(requestUri, antPathMatcher, handler);
    }

    /**
     * request URL이 웹의 메뉴를 접근하는지, ajax 호출을 하는지 체크합니다.<p>
     *
     * requestd의 suffix가 .json 이면 false, 아니면 true 리턴합니다.<br>
     *
     * @param requestUrl request url
     * @return boolean suffix가 .json 이면 false, 아니면 true 리턴합니다.
     */
    private boolean isNotAjaxUri(final String requestUrl) {
        return !StringUtils.endsWith(requestUrl, REST_URI_SUFFIX);
    }

    /**
     * 현재 접근하는 URI가 권한체크 대상 URI인지 확인합니다.
     *
     * @param requestUri request URI
     * @param handler HandlerMethod
     * @return 권한체크 대상이면 true, 권한체크 무시 대상이면 false를 리턴합니다.
     */
    private boolean isAuthCheckUri(final String requestUri, final AntPathMatcher antPathMatcher,
        final HandlerMethod handler) {
        // CertificationNeedLess annotation 이 클래스나 메서드에 붙어 있을 경우, 권한체크 무시
        if (handler.getBeanType().isAnnotationPresent(CertificationNeedLess.class)
            || handler.getMethod().isAnnotationPresent(CertificationNeedLess.class)) {
            return false;
        }

        // 제외대상 uri가 아닌지 체크
        return properties.getIgnoreAuthCheckUris().stream()
            .noneMatch(ignoreUrl -> antPathMatcher.match(ignoreUrl, requestUri));
    }

    /**
     * 접근하는 메뉴에 사용자가 권한이 없는지 체크합니다.
     *
     * @param requestUri Request URI
     * @return 권한이 없을 경우 true, 있을 경우 false를 리턴 합니다.
     * @throws ValidateAuthorizedMenuException 인가 된 메뉴인지 검증 시 발생하는 Exception
     */
    private boolean isNotPermissionMenuFromAuth(final String requestUri) {
        boolean result;
        try {
            result = userMenuService
                .isNotPermissionMenu(loginCookieService.getUserInfo().getEmpId(), requestUri);
        } catch (Exception e) {
            throw new ValidateAuthorizedMenuException(e);
        }
        return result;
    }

    /**
     * request url에서 uri를 추출하여 리턴 합니다.<p>
     * 추가로 불필요한 경로 등을(여러개의 슬래시가 들어올 경우 등) 정규화 처리를 수행 합니다.<p>
     * <pre>ex) "//notice/prodNotice///notice -> /notice/prodNotice/notice"</pre>
     *
     * @param requestUrl request url
     * @return uri
     */
    private String getUriPath(final String requestUrl) {
        URI uri;
        try {
            uri = new URI(requestUrl);
        }
        catch (URISyntaxException e) {
            throw new ValidateAuthorizedMenuException(e);
        }

        //불필요한 경로 등을(여러개의 슬래시가 들어올 경우 등) 정규화 처리(ex. "//notice/prodNotice///notice")
        uri = uri.normalize();
        final String uriPath = uri.getPath();

        return uriPath;
    }
}