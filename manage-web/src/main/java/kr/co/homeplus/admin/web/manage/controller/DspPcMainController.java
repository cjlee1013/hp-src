package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspPcMain.PcMainGetDto;
import kr.co.homeplus.admin.web.manage.model.dspPcMain.PcMainListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspPcMain.PcMainListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspPcMain.PcMainSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspMain")
public class DspPcMainController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 전시관리 > pc 메인관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspPcMain", method = RequestMethod.GET)
    public String dspPcMain(Model model) {

        codeService.getCodeModel(model,
                        "display_yn"            // 전시여부
            ,           "dsp_main_sch_type"     // 검색어 구분
            ,           "dsp_main_site_type"    // 사이트 구분 (HOME,CLUB)
            ,           "dsp_main_period_type"  // 조회기간 기준
            ,           "dsp_main_link_type"    // 메인관리 링크타입
            ,           "dsp_main_loc"          // 전시위치
            ,           "dsp_main_store_type"   // 스토어 타입
            ,           "dsp_main_division"     // 메인관리 분할 설정
            ,           "dsp_main_close_type"   // 최상단 배너 닫기버튼
            ,           "dsp_disp_store_type"   // 메인관리 링크타입
            ,           "dsp_main_template"     // 메인관리 배너 단 설정
            ,           "dsp_main_template_2"   // 메인관리 배너 2단 상세
            ,           "dsp_main_template_3"   // 메인관리 배너 3단 상세
            ,           "dsp_main_template_5"   // 메인관리 배너 5단 상세
        );

        //택배점 리스트 조회
        String apiUri = "/item/store/getStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject< List<StoreListSelectDto>>>() {};
        List<StoreListSelectDto> dlvStoreList =  (List<StoreListSelectDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, StoreListParamDto.builder().searchStoreKind("DLV").searchUseYn("Y").build(), apiUri, typeReference).getData();

        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("dlvStoreList", dlvStoreList);

        model.addAllAttributes(RealGridHelper.create("dspPcMainListGridBaseInfo", PcMainListSelectDto.class));

        return "/manage/dspPcMain";
    }

    /**
     * 사이트관 > 전시관리 > pc 메인관리
     *
     * @param listParamDto
     * @return PcMainListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getPcMainList.json"}, method = RequestMethod.GET)
    public List<PcMainListSelectDto> getPcMainList(PcMainListParamDto listParamDto) {
        String apiUri = "/manage/dspMain/getPcMainList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PcMainListSelectDto>>>() {};
        return (List<PcMainListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, PcMainListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > pc 메인관리 상세 영역
     *
     * @param mainNo
     * @return PcMainGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getPcMainDetail.json"}, method = RequestMethod.GET)
    public PcMainGetDto getPcMainDetail(@RequestParam(name = "mainNo") String mainNo) {
        String apiUri = "/manage/dspMain/getPcMainDetail?mainNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<PcMainGetDto>>() {};
        return (PcMainGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri+mainNo, typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > pc 메인관리 등록/수정
     *
     * @param pcMainSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setPcMain.json"}, method = RequestMethod.POST)
    public ResponseResult setPcMain(@RequestBody PcMainSetParamDto pcMainSetParamDto) {
        String apiUri = "/manage/dspMain/setPcMain";
        pcMainSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, pcMainSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
