package kr.co.homeplus.admin.web.partner.model.affiliate;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.admin.web.partner.model.PartnerManagerGetDto;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AffiliateGetDto {

    // 제휴업체ID
    private String partnerId;
    // 제휴업체명
    private String affiliateNm;
    // 사업자유형
    private String operatorType;
    // 제휴유형
    private String affiliateType;
    // 회원상태
    private String partnerStatus;
    // 상호
    private String partnerNm;
    // 대표자명
    private String partnerOwner;
    // 사업자등록번호
    private String partnerNo;
    // 업태
    private String businessConditions;
    // 종목
    private String bizCateCd;
    // 종목명
    private String bizCateCdNm;
    // 우편번호
    private String zipcode;
    // 주소1
    private String addr1;
    // 주소2
    private String addr2;

    private String bankCd;

    private String bankCdNm;

    private String bankAccountNo;

    private String depositor;

    // 담당자정보
    List<PartnerManagerGetDto> managerList;

}
