package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.List;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionPaymentDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionStoreDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DiscountDetailSetDto {
    private long discountNo;
    private String discountNm;
    private String displayDiscountNm;
    private String storeType;
    private String discountKind;
    private String issueStartDt;
    private String issueEndDt;
    private char useYn;
    private String empNo;
    private char chgYn;

    private List<PromotionStoreDto> storeList;
    private List<PromotionPaymentDto> paymentList;

    private List<DiscountApplySetDto> applyScopeList;
    private List<DiscountApplySetDto> createApplyScopeList;
    private List<DiscountApplySetDto> deleteApplyScopeList;
}
