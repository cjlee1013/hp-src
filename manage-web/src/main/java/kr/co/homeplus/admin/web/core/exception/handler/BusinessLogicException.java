package kr.co.homeplus.admin.web.core.exception.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 비즈니스 로직 Exception
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BusinessLogicException extends Exception {

	private String errorCode;
	private String description  = "";
	private String errorMsg     = "";
	private String errorMsg2    = "";

	public BusinessLogicException(BusinessExceptionCode exceptionCode) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
	}

	public BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
		this.errorMsg       = errorMsg;
	}

	public BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg, String errorMsg2) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
		this.errorMsg       = errorMsg;
		this.errorMsg2		= errorMsg2;
	}

	public <T extends Number> BusinessLogicException(BusinessExceptionCode exceptionCode, T errorMsg) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
		this.errorMsg       = String.valueOf(errorMsg);
	}

	public <T extends Number> BusinessLogicException(BusinessExceptionCode exceptionCode, String errorMsg, T errorMsg2) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
		this.errorMsg       = errorMsg;
		this.errorMsg2		= String.valueOf(errorMsg2);
	}

	public <T extends Number> BusinessLogicException(BusinessExceptionCode exceptionCode, T errorMsg, String errorMsg2) {
		this.errorCode      = exceptionCode.getCode();
		this.description    = exceptionCode.getDesc();
		this.errorMsg       = String.valueOf(errorMsg);
		this.errorMsg2		= errorMsg2;
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
