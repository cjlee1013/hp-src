package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PopularKeywordSetDto {
    private Integer rank;
    private String keyword;
    private Integer count;
    private Integer variation;
}
