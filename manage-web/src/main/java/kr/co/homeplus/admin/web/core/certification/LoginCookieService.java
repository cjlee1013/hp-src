package kr.co.homeplus.admin.web.core.certification;

import kr.co.homeplus.admin.web.core.dto.LoginInitInfo;
import kr.co.homeplus.admin.web.core.dto.UserInfo;

/**
 * 어드민 로그인 쿠키 관련 서비스
 */
public interface LoginCookieService {

    void createLoginCookie(final UserInfo userInfo);

    UserInfo getUserInfo();

    void removeLoginCookie();

    boolean isLogin();

    /**
     * 아이디 저장 쿠키를 생성합니다.
     * @param userId 사용자 아이디
     */
    void createIdSaveCookie(String userId);

    /**
     * 아이디 저장 쿠키를 삭제합니다.
     */
    void removeIdSaveCookie();

    /**
     * 로그인 페이지에서 초기정보(아이디 저장여부, 저장 아이디)를 반환합니다.
     * @return {@link LoginInitInfo} 로그인 페이지 초기정보
     */
    LoginInitInfo getLoginInitInfo();
}
