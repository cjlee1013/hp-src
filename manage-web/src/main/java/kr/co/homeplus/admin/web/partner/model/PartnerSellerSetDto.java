package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSellerSetDto implements Serializable {

	private String businessNm;
	private String infoCenter;
	private String email;
	private String phone1;
	private String phone2;
	private String companyInfo;
	private String homepage;
	private Integer categoryCd;
	private String cultureDeductionYn;
	private String cultureDeductionNo;
	private String returnDeliveryYn;
	private String saleType;
	private String itemNo;

}
