package kr.co.homeplus.admin.web.voc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.voc.model.CsQnaDispStatusSetParamDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaListParamDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaListSelectDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaReplyDispStatusSetParamDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaReplyListParamDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaReplyListSelectDto;
import kr.co.homeplus.admin.web.voc.model.CsQnaReplySetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/voc/csQna")
public class CsQnaController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

     /**
     * Q&A관리 메인
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/csQnaMain", method = RequestMethod.GET)
    public String csQnaMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "qna_search_type"        //code_name
            ,"qna_status"
            ,"qna_disp_status"
            ,"qna_type"
        );

        model.addAttribute("qnaSearchType", code.get("qna_search_type"));
        model.addAttribute("qnaStatus", code.get("qna_status"));
        model.addAttribute("qnaDispStatus", code.get("qna_disp_status"));
        model.addAttribute("qnaType", code.get("qna_type"));

        model.addAllAttributes(RealGridHelper.create("csQnaListGridBaseInfo", CsQnaListSelectDto.class));

        return "/voc/csQnaMain";
    }

    /**
     * Q&A관리 >  Q&A 리스트
     *
     * @param listParamDto
     * @return NoticeListParamDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getQnaList.json"}, method = RequestMethod.GET)
    public List<CsQnaListSelectDto> getQnaList(CsQnaListParamDto listParamDto) {

        String apiUri = "/voc/csQna/getQnaList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CsQnaListSelectDto>>>() {};
        return (List<CsQnaListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, CsQnaListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * Q&A관리 >  Q&A 답변 리스트
     *
     * @param listParamDto
     * @return NoticeListParamDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getQnaReplyList.json"}, method = RequestMethod.GET)
    public List<CsQnaReplyListSelectDto> getQnaReplyList(CsQnaReplyListParamDto listParamDto) {

        String apiUri = "/voc/csQna/getQnaReplyList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CsQnaReplyListSelectDto>>>() {};
        return (List<CsQnaReplyListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, CsQnaReplyListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * Q&A관리 >  Q&A 답변등록
     * @param csQnaReplySetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setQnaReply.json"}, method = RequestMethod.POST)
    public ResponseResult setQnaReply(@RequestBody CsQnaReplySetParamDto csQnaReplySetParamDto) {

        String apiUri = "/voc/csQna/setQnaReply";
        csQnaReplySetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, csQnaReplySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * Q&A관리 >  Q&A 전시상태변경
     * @param csQnaDispStatusSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setQnaDisplayStatus.json"}, method = RequestMethod.POST)
    public ResponseResult setQnaDisplayStatus(@RequestBody CsQnaDispStatusSetParamDto csQnaDispStatusSetParamDto) {

        String apiUri = "/voc/csQna/setQnaDisplayStatus";
        csQnaDispStatusSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, csQnaDispStatusSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * Q&A관리 >  Q&A 답변 전시상태변경
     * @param csQnaReplyDispStatusSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setQnaReplyDisplayStatus.json"}, method = RequestMethod.POST)
    public ResponseResult setQnaReplyDisplayStatus(@RequestBody CsQnaReplyDispStatusSetParamDto csQnaReplyDispStatusSetParamDto) {

        String apiUri = "/voc/csQna/setQnaReplyDisplayStatus";
        csQnaReplyDispStatusSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, csQnaReplyDispStatusSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 파트너 업체 상호명 조회
     * @param partnerId
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getPartnerNm.json"}, method = RequestMethod.GET)
    public ResponseResult getPartnerNm(@RequestParam(name = "partnerId") String partnerId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUriRequest = "/voc/csQna/getPartnerNm" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {})
            .getData();
    }

}
