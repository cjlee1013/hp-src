package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.Advance.AdvanceListGetDto;
import kr.co.homeplus.admin.web.settle.model.Advance.AdvanceListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdvanceService {

    private final ResourceClient resourceClient;
    /**
     * 정산관리 > 회계자료조회 > 선수금조회
     */
    public List<AdvanceListGetDto> getList(AdvanceListSetDto advanceListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            advanceListSetDto,
            "/admin/Advance/getList",
            new ParameterizedTypeReference<ResponseObject<List<AdvanceListGetDto>>>(){}).getData();
    }

    public List<AdvanceListGetDto> getZeroList(AdvanceListSetDto advanceListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            advanceListSetDto,
            "/admin/Advance/getZeroList",
            new ParameterizedTypeReference<ResponseObject<List<AdvanceListGetDto>>>(){}).getData();
    }
}
