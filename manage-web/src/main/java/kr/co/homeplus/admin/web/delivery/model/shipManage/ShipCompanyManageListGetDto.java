package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 배송관리 리스트 응답 DTO")
public class ShipCompanyManageListGetDto {
    @ApiModelProperty(notes = "택배사명")
    private String dlvNm;

    @ApiModelProperty(notes = "내부코드")
    private String dlvCd;

    @ApiModelProperty(notes = "연동업체공통코드")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "연동업체")
    private String dlvLinkCompanyNm;

    @ApiModelProperty(notes = "연동업체코드")
    private String dlvLinkCompanyCd;

    @ApiModelProperty(notes = "자동반품접수")
    private String autoReturnYn;

    @ApiModelProperty(notes = "자동반품지연")
    private String autoReturnDelayYn;

    @ApiModelProperty(notes = "노출여부")
    private String displayYn;

    @ApiModelProperty(notes = "고객센터 전화번호")
    private String csPhone;

    @ApiModelProperty(notes = "홈페이지")
    private String homePage;

    @ApiModelProperty(notes = "비고")
    private String note;

    @ApiModelProperty(notes = "등록자")
    private String regId;

    @ApiModelProperty(notes = "등록일")
    private String regDt;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "수정일")
    private String chgDt;
}