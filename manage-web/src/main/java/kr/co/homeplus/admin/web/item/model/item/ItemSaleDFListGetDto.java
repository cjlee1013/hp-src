package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 대표판매정보
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class ItemSaleDFListGetDto {

	//점포ID
	@RealGridColumnInfo(headText = "점포ID", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL, sortable = true)
	private int storeId;

	//점포명
	@RealGridColumnInfo(headText = "점포명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true)
	private String storeNm;

	//점포유형
	@RealGridColumnInfo(headText = "점포유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String storeType;

	//판매가격
	@RealGridColumnInfo(headText = "판매가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE)
	private String 	salePrice;

	//행사가격
	@RealGridColumnInfo(headText = "행사가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, hidden = true)
	private String 	dcPrice;

	//판매기간 여부명
	@RealGridColumnInfo(headText = "판매기간설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String salePeriodYnNm;

	//판매일
	@RealGridColumnInfo(headText = "판매기간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 200)
	private String saleDt;

	//예약여부명
	@RealGridColumnInfo(headText = "예약판매설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String rsvYnNm;

	//판매예약일
	@RealGridColumnInfo(headText = "예약판매기간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 200)
	private String saleRsvDt;

	@RealGridColumnInfo(headText = "취급중지", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String stopDealYnNm;

	@RealGridColumnInfo(headText = "중지기간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 200)
	private String stopDt;

	//온라인취급여부명
	@RealGridColumnInfo(headText = "온라인취급상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String pfrYnNm;

	//재고관리유형명
	@RealGridColumnInfo(headText = "재고관리유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String stockTypeNm;

	//재고수량
	@RealGridColumnInfo(headText = "재고수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, sortable = true)
	private String stockQty;

	//단위수량명
	@RealGridColumnInfo(headText = "판매단위수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String unitQtyNm;

	//최소구매수량
	@RealGridColumnInfo(headText = "최소구매수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String purchaseMinQty;

	//구매제한개수명
	@RealGridColumnInfo(headText = "구매수량제한", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String purchaseLimitQtyNm;

}
