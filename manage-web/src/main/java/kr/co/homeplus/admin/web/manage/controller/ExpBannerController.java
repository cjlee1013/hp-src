package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.banner.BannerListParamDto;
import kr.co.homeplus.admin.web.manage.model.expBanner.ExpBannerGetDto;
import kr.co.homeplus.admin.web.manage.model.expBanner.ExpBannerLinkGetDto;
import kr.co.homeplus.admin.web.manage.model.expBanner.ExpBannerListParamDto;
import kr.co.homeplus.admin.web.manage.model.expBanner.ExpBannerSetParamDto;
import kr.co.homeplus.admin.web.manage.model.expBanner.ExpBannerStoreGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class ExpBannerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    /**
     * 사이트관리 > 전시관리 > EXPRESS 배너관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/expBanner/expBannerMain", method = RequestMethod.GET)
    public String expBannerMain(Model model) {

        codeService.getCodeModel(model
            ,   "disp_yn"                 // 전시여부
            ,   "exp_banner_loc"          // 전시위치 1뎁스
            ,   "dsk_device_gubun"        // 디바이스 구분
            ,   "dsp_main_link_type"      // 링크 타입
            ,   "dsp_main_store_type"     // 스토어 타입
            ,   "dsp_main_gnb_type"       // 메인관리 GNB 디바이스 타입
            ,   "dsp_disp_store_type"     // 메인관리 링크타입
            ,   "dsp_main_template"       // 메인관리 배너 단 설정
            ,   "dsp_main_template_2"     // 메인관리 배너 2단 상세
            ,   "dsp_main_template_3"     // 메인관리 배너 3단 상세
            ,   "dsp_main_template_5"     // 메인관리 배너 5단 상세
        );

        model.addAttribute("homeImgUrl", this.homeImgUrl);

        model.addAllAttributes(RealGridHelper.create("expBannerListGridBaseInfo", ExpBannerGetDto.class));

        return "/manage/expBannerMain";
    }

    /**
     * 사이트관리 > 전시관리 > EXPRESS 배너관리 리스트 조회
     *
     * @param expBannerListParamDto
     * @return List<ExpBannerGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/expBanner/getExpBannerList.json"}, method = RequestMethod.GET)
    public List<ExpBannerGetDto> getExpBannerList(@Valid ExpBannerListParamDto expBannerListParamDto) {
        String apiUri = "/manage/expBanner/getExpBannerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExpBannerGetDto>>>() {};
        return (List<ExpBannerGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil
            .getRequestString(apiUri, ExpBannerListParamDto.class, expBannerListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > EXPRESS 배너관리 리스트 조회
     *
     * @param bannerNo
     * @return List<ExpBannerLinkGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/expBanner/getExpBannerLinkList.json"}, method = RequestMethod.GET)
    public List<ExpBannerLinkGetDto> getExpBannerLinkList(@RequestParam("bannerNo") Long bannerNo) {
        String apiUri = "/manage/expBanner/getExpBannerLinkList?bannerNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExpBannerLinkGetDto>>>() {};
        return (List<ExpBannerLinkGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + bannerNo, typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > EXPRESS 배너관리 등록/수정
     *
     * @param expBannerSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/expBanner/setExpBanner.json"}, method = RequestMethod.POST)
    public ResponseResult setExpBanner(@RequestBody ExpBannerSetParamDto expBannerSetParamDto) {
        String apiUri = "/manage/expBanner/setExpBanner";
        expBannerSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, expBannerSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > EXPRESS 노출 점포 리스트 조회
     *
     * @param bannerNo
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/expBanner/getExpBannerStoreList.json"}, method = RequestMethod.GET)
    public List<ExpBannerStoreGetDto> getExpBannerStoreList(@RequestParam("bannerNo") Long bannerNo) {
        String apiUri = "/manage/expBanner/getExpBannerStore?bannerNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExpBannerStoreGetDto>>>() {};
        return (List<ExpBannerStoreGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + bannerNo, typeReference).getData();
    }

}
