package kr.co.homeplus.admin.web.settle.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "제휴정산일마감")
public class MarketSettleListGetDto {
  @ApiModelProperty(notes = "결제일")
  @RealGridColumnInfo(headText = "결제일", width = 100)
  private String paymentCompleteDt;

  @ApiModelProperty(notes = "매출일")
  @RealGridColumnInfo(headText = "매출일", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "입금일")
  @RealGridColumnInfo(headText = "입금일", width = 100)
  private String payScheduleDt;

  @ApiModelProperty(notes = "제휴사")
  @RealGridColumnInfo(headText = "제휴사", width = 100)
  private String siteType;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 150)
  private String marketOrderNo;

  @ApiModelProperty(notes = "상품주문번호")
  @RealGridColumnInfo(headText = "상품주문번호", width = 150)
  private String marketOrderItemNo;

  @ApiModelProperty(notes = "정산구분")
  @RealGridColumnInfo(headText = "정산구분", width = 100)
  private String orderType;

  @ApiModelProperty(notes = "정산상태")
  @RealGridColumnInfo(headText = "정산상태", width = 100)
  private String gubun;

  @ApiModelProperty(notes = "상품번호")
  @RealGridColumnInfo(headText = "상품번호", width = 100)
  private String itemNo;

  @ApiModelProperty(notes = "정산대상금액")
  @RealGridColumnInfo(headText = "정산대상금액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settleAmt;

  @ApiModelProperty(notes = "매출")
  @RealGridColumnInfo(headText = "매출", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "자사할인")
  @RealGridColumnInfo(headText = "자사할인", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketDiscountHomeAmt;

  @ApiModelProperty(notes = "제휴사할인")
  @RealGridColumnInfo(headText = "제휴사할인", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketDiscountMarketAmt;

  @ApiModelProperty(notes = "수수료합계")
  @RealGridColumnInfo(headText = "수수료합계", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String commissionSumAmt;

  @ApiModelProperty(notes = "판매수수료")
  @RealGridColumnInfo(headText = "판매수수료", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String saleAgencyFee;

  @ApiModelProperty(notes = "결제수수료")
  @RealGridColumnInfo(headText = "결제수수료", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String commissionAmt;

  @ApiModelProperty(notes = "기타수수료")
  @RealGridColumnInfo(headText = "기타수수료", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String etcAmt;

  @ApiModelProperty(notes = "공제금액")
  @RealGridColumnInfo(headText = "공제금액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String deductAmt;
}
