package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "PG 수수료 관리")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PgCommissionManageDto {
    @ApiModelProperty(value = "PG수수료관리순번", position = 1)
    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long pgCommissionMngSeq;

    @ApiModelProperty(value = "PG종류(INICIS,TOSSPG)", position = 2)
    @RealGridColumnInfo(headText = "PG", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgKind;

    @ApiModelProperty(value = "상위결제수단코드", position = 3)
    @RealGridColumnInfo(headText = "결제수단", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodCd;

    @ApiModelProperty(value = "수수료관리명", position = 4)
    @RealGridColumnInfo(headText = "제목", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String commissionMngNm;

    @ApiModelProperty(value = "시작일시(YYYY-MM-DD 00:00:00)", position = 5)
    @RealGridColumnInfo(headText = "시작일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String startDt;

    @ApiModelProperty(value = "종료일시(YYYY-MM-DD 23:59:59)", position = 6)
    @RealGridColumnInfo(headText = "종료일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String endDt;

    @ApiModelProperty(value = "정산주기(DAY:일정산,WEEK:주정산,EARLY_MONTH:익월초,END_MONTH:익월말,END_NEXT_MONTH:익익월말)", position = 7)
    @RealGridColumnInfo(headText = "정산주기", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String costPeriod;

    @ApiModelProperty(value = "정산일(정산주기 DAY 경우만 입력됨)", position = 8)
    @RealGridColumnInfo(headText = "정산일", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer costDay;

    @ApiModelProperty(value = "소수점처리(UP:올림,DOWN,버림,ROUND:반올림)", position = 9)
    @RealGridColumnInfo(headText = "소수점처리", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String decpntProcess;

    @ApiModelProperty(value = "부가세여부(Y:포함,N:포함안함)", position = 10)
    @RealGridColumnInfo(headText = "부가세여부", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String vatYn;

    @ApiModelProperty(value = "사용여부", position = 11)
    @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(value = "등록자ID", position = 12)
    @RealGridColumnInfo(headText = "등록자", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "등록일자", position = 13)
    @RealGridColumnInfo(headText = "등록일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 14)
    @RealGridColumnInfo(headText = "수정자", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "수정일자", position = 15)
    @RealGridColumnInfo(headText = "수정일", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;

    @ApiModelProperty(value = "PG수수료대상목록", position = 16)
    private List<PgCommissionTargetDto> pgCommissionTargetDtoList;
}
