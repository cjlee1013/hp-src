package kr.co.homeplus.admin.web.partner.model.affiliate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessInfoGetDto {

    private String statusCode;
    private String compName;
    private String repName;

}
