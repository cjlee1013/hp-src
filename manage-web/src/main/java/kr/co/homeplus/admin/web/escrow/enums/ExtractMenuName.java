package kr.co.homeplus.admin.web.escrow.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExtractMenuName {

    CMBN_ORDER_SALES("합배송 주문 매출")
    , COMBINE_ORDER_STATUS("합배송 주문 현황")
    , ZIPCODE_ORDER_STAT("우편번호별 주문현황")
    , CARD_DISCOUNT_STATISTICS("카드할인통계")
    , CARD_DISCOUNT_STATISTICS_DETAIL("카드할인통계상세")
    , COUPON_STATISTICS("쿠폰통계메인")
    , ITEM_COUPON_STATISTICS("상품쿠폰통계")
    , CART_COUPON_STATISTICS("장바구니쿠폰통계")
    , REALTIME_STORE_ORDER_STATISTICS("실시간 점포별 판매현황")
    , REALTIME_PARTNER_ORDER_STATISTICS("실시간 판매업체별 판매현황")
    , REALTIME_STORE_ITEM_SALE_STATISTICS("실시간 점포상품 판매현황")
    , REALTIME_PARTNER_ITEM_SALE_STATISTICS("실시간 판매업체상품 판매현황")
    , PAYMENT_INFO_EXTRACT("지불 수단별 판매현황")
    , CART_COUPON_USE_ORDER_EXTRACT("장바구니 쿠폰 사용 조회")
    , PICKUP_ORDER_EXTRACT("온라인 픽업 주문 현황")
    , ADD_COUPON_USE_ORDER_EXTRACT("중복 쿠폰 사용 조회")
    ;

    private String menuNm;
}
