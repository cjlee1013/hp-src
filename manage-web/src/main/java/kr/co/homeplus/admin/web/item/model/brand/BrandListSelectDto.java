package kr.co.homeplus.admin.web.item.model.brand;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class BrandListSelectDto {

    @RealGridColumnInfo(headText = "브랜드 코드", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL, sortable = true)
    private Long brandNo;

    @RealGridColumnInfo(headText = "한글 브랜드명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 250)
    private String brandNm;

    @RealGridColumnInfo(headText = "영문 브랜드명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 250)
    private String brandNmEng;

    @RealGridColumnInfo(headText = "사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String chgDt;


    @RealGridColumnInfo(headText = "브랜드 이미지", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String imgUrl;

    @RealGridColumnInfo(headText = "브랜드 소개", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String brandDesc;

    @RealGridColumnInfo(headText = "사용여부 코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "상품명 노출여부 코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String itemDispYn;

    @RealGridColumnInfo(headText = "홈페이지 Url", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String siteUrl;

    @RealGridColumnInfo(headText = "dispNm", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String dispNm;

    @RealGridColumnInfo(headText = "initial", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, hidden = true)
    private String initial;

}
