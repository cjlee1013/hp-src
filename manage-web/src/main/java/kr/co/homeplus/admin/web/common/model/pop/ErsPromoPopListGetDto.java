package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ErsPromoPopListGetDto {
    @RealGridColumnInfo(headText = "행사번호", sortable = true)
    private Long eventCd;

    @RealGridColumnInfo(headText = "행사명", width = 200)
    private String eventNm;

    @RealGridColumnInfo(headText = "구매기준유형")
    private String eventTargetTypeNm;

    @RealGridColumnInfo(headText = "지급기준유형")
    private String eventGiveTypeNm;

    @RealGridColumnInfo(headText = "시작일자", columnType = RealGridColumnType.DATE, width = 130)
    private String eventStDate;

    @RealGridColumnInfo(headText = "종료일자", columnType = RealGridColumnType.DATE, width = 130)
    private String eventEdDate;;

    @RealGridColumnInfo(headText = "온라인사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "확정여부")
    private String confirmFlagNm;
}