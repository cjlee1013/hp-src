package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;



import lombok.Data;

@Data
public class CategoryListParamDto {

    private Long searchCateCd1;

    private Long searchCateCd2;

    private Long searchCateCd3;

    private String searchResistYn;

}
