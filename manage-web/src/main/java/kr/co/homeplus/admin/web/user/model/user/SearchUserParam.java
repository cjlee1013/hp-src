package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchUserParam {
    @ApiModelProperty(value = "일반/휴면 조건 (1=일반, 6=휴면)")
    private String searchUserStatus;

    @ApiModelProperty(value = "사용자 분류 - 10 : 개인회원, 15 : 사업자구매회원, 20 : md, 30 : 판매자")
    private String searchUserClassify;

    @ApiModelProperty(value = "검색조건 (userId=ID, userEmail=이메일, userPhone=휴대폰, userName=이름, companyName=상호명, companyNo=사업자등록번호)")
    private String searchType;

    @ApiModelProperty(value = "검색어")
    private String keyword;

    @ApiModelProperty(value = "개인정보 마스킹 여부 (true = 개인정보 마스킹)")
    private Boolean isMasking;
}
