package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ItemPopupListParamDto {
	private String schLcateCd;
	private String schMcateCd;
	private String schScateCd;
	private String schDcateCd;
	private String schMallType;
	private String schStoreType;
	private String schPartnerType;
	private String schPartnerValue;
	private String schType;
	private String schValue;
}
