package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.PgMidManageDto;
import kr.co.homeplus.admin.web.pg.model.PgMidManageSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgMidManageService {

    private final ResourceClient resourceClient;

    /**
     * PG상점ID 검색
     * @param pgMidManageSelectDto
     * @return
     */
    public List<PgMidManageDto> getPgMidManageList(PgMidManageSelectDto pgMidManageSelectDto) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("pgKind", pgMidManageSelectDto.getSchPgKind()));
        setParameterList.add(SetParameter.create("useYn", pgMidManageSelectDto.getSchUseYn()));

        String apiUri = EscrowConstants.ESCROW_GET_PG_MID_MANAGE + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgMidManageDto>>>() {};
        return (List<PgMidManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * PG상점ID 등록
     * @param pgMidManageDto
     * @return
     */
    public ResponseObject<Object> savePgMidManage(PgMidManageDto pgMidManageDto) {
        String apiUri = EscrowConstants.ESCROW_SAVE_PG_MID_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, pgMidManageDto, apiUri, typeReference);
    }
}
