package kr.co.homeplus.admin.web.claim.enums;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClaimReasonTypeCode {

    CANCEL_REASON_TYPE_01("C001","구매의사없음","C","B", "O"),
    CANCEL_REASON_TYPE_02("C002","사이즈/생상 옵션 변경","C","B", "O"),
    CANCEL_REASON_TYPE_03("C003","재주문","C","B", "O"),
    CANCEL_REASON_TYPE_04("C004","기타(구매자 책임 사유)","C","B", "O"),
    CANCEL_REASON_TYPE_05("C005","기타(판매자 책임 사유)","C","S", "O"),
    RETURN_REASON_TYPE_01("R001","고객변심","R","B", "R"),
    RETURN_REASON_TYPE_02("R002","배송지연","R","B", "R"),
    RETURN_REASON_TYPE_03("R003","포장 및 상품 불량","R","S", "R"),
    RETURN_REASON_TYPE_04("R004","서비스불만족","R","S", "R"),
    RETURN_REASON_TYPE_05("R005","품절","R","S", "R"),
    RETURN_REASON_TYPE_06("R006","기타(구매자 책임 사유)","R","B", "R"),
    RETURN_REASON_TYPE_07("R007","기타(판매자 책임 사유)","R","S", "R"),
    EXCHANGE_REASON_TYPE_01("X001", "기타(구매자 책임 사유)","X","B", "R"),
    EXCHANGE_REASON_TYPE_02("X002","기타(판매자 책임 사유)","X","S", "R"),
    PENDING_REASON_TYPE_01("P001","상품 미수거","P","B", ""),
    PENDING_REASON_TYPE_02("P002","배송비 청구","P","B", ""),
    PENDING_REASON_TYPE_03("P003","기타비용청구","P","B", ""),
    PENDING_REASON_TYPE_04("P004","기타(구매자 책임 사유)", "P","B", ""),
    PENDING_REASON_TYPE_05("P005","기타(판매자 책임 사유)", "P","S", ""),
    EMPTY("XXXX","등록된 코드 없음", "","", "");

    private final String responseCode;
    private final String responseMessage;
    private final String claimType;
    private final String whoReason;
    private final String cancelType;

    public static ClaimReasonTypeCode getClaimReasonTypeCode (String code){
        return Arrays.stream(ClaimReasonTypeCode.values())
            .filter(ClaimReasonTypeCode -> ClaimReasonTypeCode.getResponseCode().equalsIgnoreCase(code))
            .findFirst()
            .orElse(EMPTY);
    }
}
