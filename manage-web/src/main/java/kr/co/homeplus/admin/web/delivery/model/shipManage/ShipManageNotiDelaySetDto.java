package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShipManageNotiDelaySetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "발송지연사유코드")
    private String delayShipCd;

    @ApiModelProperty(notes = "발송지연사유메시지")
    private String delayShipMsg;

    @ApiModelProperty(notes = "2차발송기한")
    private String delayShipDt;
}
