package kr.co.homeplus.admin.web.core.privacylog.model;

import lombok.Builder;
import lombok.Getter;

/**
 * 개인정보 접근이력 파라미터
 */
@Getter
@Builder
public class PrivacyLogInfo {

    /** 거래 발생 시간", required = true, example = "2020-01-01 12:34:56.789" **/
    private String txTime;

    /**  "접속 IP", required = true, example = "IPv4, 12.34.56.78" **/
    private String accessIp;

    /**  "사용자 ID", required = true, example = "사용자 ID" **/
    private String userId;

    /**  "거래 코드 URL, 화면 URL", required = true, example = "/example/Main" **/
    private String txCodeUrl;

    /**  "업무행위", required = true, example = "Insert, Update 등" **/
    private String txMethod;

    /**  "거래 코드 이름 (화면이름)", required = true, example = "화면 명" **/
    private String txCodeName;

    /**  "처리 주체", required = true, example = "처리한 정보주체(userNo)" **/
    private String processor;

    /**  "처리한 정보주체를 조회하기 위해 수행한 업무 SQL 문 (프로시저 파라미터 포함)", required = true, example = "수행한 쿼리 또는 프로시저 문, 파라미터 포함" **/
    private String processorTaskSql;
}
