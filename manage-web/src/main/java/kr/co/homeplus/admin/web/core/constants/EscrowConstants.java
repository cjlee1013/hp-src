package kr.co.homeplus.admin.web.core.constants;

/**
 * Escrow 어드민 상수 선언
 */
public class EscrowConstants {

    /* ApiName 정의*/
    /** 신용카드 결제수단 조회 (Site_type 중복제거) **/
    public static final String ESCROW_GET_DISTINCT_METHOD_CD = "/pg/paymentMethod/getDistinctMethodCd";
    /** 카드BIN 조회 **/
    public static final String ESCROW_GET_CARD_BIN_MANAGE = "/pg/cardBin/getCardBinManageList";
    /** 카드BIN 저장 **/
    public static final String ESCROW_SAVE_CARD_BIN_MANAGE = "/pg/cardBin/saveCardBinManage";
    /** PG상점ID 조회*/
    public static final String ESCROW_GET_PG_MID_MANAGE = "/pg/paymentMethod/getPgMidManage";
    /** PG상점ID 등록 */
    public static final String ESCROW_SAVE_PG_MID_MANAGE = "/pg/paymentMethod/savePgMidManage";
    /** 결제수단 조회 **/
    public static final String ESCROW_GET_PAYMENT_METHOD = "/pg/paymentMethod/getPaymentMethod";
    /** 결제수단 조회(유형별) **/
    public static final String ESCROW_GET_PAYMENT_METHOD_BY_TYPE = "/pg/paymentMethod/getPaymentMethodByType";
    /** 결제수단 저장 **/
    public static final String ESCROW_SAVE_PAYMENT_METHOD = "/pg/paymentMethod/savePaymentMethod";
    /** 결제수단코드 중복체크 **/
    public static final String ESCROW_CHECK_DUPLICATE_CODE = "/pg/paymentMethod/checkDuplicateCode";
    /** 결제수단 Tree 구조 조회 **/
    public static final String ESCROW_GET_PAYMENT_METHOD_TREE = "/pg/paymentMethod/getPaymentMethodTree";
    /** 결제수단 조회 - 파라미터 사용 **/
    public static final String ESCROW_GET_METHOD_CD_BY_SITE_TYPE = "/pg/paymentMethod/getMethodCdBySiteType";
    /** 기본배분율 조회 **/
    public static final String ESCROW_GET_PG_DIVIDE_RATE_LIST = "/pg/pgDivide/getPgDivideRateList";
    /** 기본배분율 저장 **/
    public static final String ESCROW_SAVE_PG_DIVIDE_RATE = "/pg/pgDivide/savePgDivideRate";
    /** 결제정책 마스터 조회 **/
    public static final String ESCROW_GET_PAYMENT_POLICY_MNG_LIST = "/pg/pgDivide/getPaymentPolicyMngList";
    /** 결제정책 결제수단 Tree 구조 조회 **/
    public static final String ESCROW_GET_PAYMENT_POLICY_METHOD_TREE = "/pg/pgDivide/getPaymentPolicyMethodTree";
    /** 결제정책 저장 **/
    public static final String ESCROW_SAVE_PAYMENT_POLICY_MNG = "/pg/pgDivide/savePaymentPolicyMng";
    /** 결제정책 PG배분율 조회 **/
    public static final String ESCROW_GET_PAYMENT_POLICY_PG_DIVIDE_RATE_LIST = "/pg/pgDivide/getPaymentPolicyPgDivideRateList";
    /** 결제정책 적용대상 조회 **/
    public static final String ESCROW_GET_PAYMENT_POLICY_APPLY_LIST = "/pg/pgDivide/getPaymentPolicyApplyList";
    /** 결제정책 적용대상 저장 **/
    public static final String ESCROW_SAVE_PAYMENT_POLICY_APPLY = "/pg/pgDivide/savePaymentPolicyApply";
    /** 결제정책 히스토리 조회 **/
    public static final String ESCROW_GET_PAYMENT_POLICY_HISTORY = "/pg/pgDivide/getPaymentPolicyHistory";
    /** 상품 유효성 체크 - 결제정책 상품일괄등록 대상 조회 **/
    public static final String ESCROW_GET_ITEM_VALID_CHECK = "/pg/pgDivide/getItemValidCheck";
    /** 청구할인 혜택 조회 **/
    public static final String ESCROW_GET_CHARGE_DISCOUNT_BENEFIT_MANAGE = "/pg/paymentMethod/getChargeDiscountBenefitManage";
    /** 청구할인 혜택 등록  **/
    public static final String ESCROW_SAVE_CHARGE_DISCOUNT_BENEFIT_MANAGE = "/pg/paymentMethod/saveChargeDiscountBenefitManage";

    /** 무이자혜택 마스터 조회 **/
    public static final String ESCROW_GET_FREE_INTEREST_MNG_LIST = "/pg/paymentMethod/getFreeInterestMngList";
    /** 무이자혜택 적용대상 조회 **/
    public static final String ESCROW_GET_FREE_INTEREST_APPLY_LIST = "/pg/paymentMethod/getFreeInterestApplyList";
    /** 무이자혜택 저장 **/
    public static final String ESCROW_SAVE_FREE_INTEREST = "/pg/paymentMethod/saveFreeInterest";
    /** 무이자혜택 카드정보 저장 **/
    public static final String ESCROW_SAVE_FREE_INTEREST_APPLY = "/pg/paymentMethod/saveFreeInterestApply";
    /** 무이자혜택 카드정보 복사 저장 **/
    public static final String ESCROW_SAVE_COPY_FREE_INTEREST = "/pg/paymentMethod/saveCopyFreeInterest";
    /** 무이자혜택 카드정보 삭제 **/
    public static final String ESCROW_DELETE_FREE_INTEREST = "/pg/paymentMethod/deleteFreeInterestApply";

    /** PG 승인대사 조회 */
    public static final String ESCROW_GET_PG_COMPARE_APPROVAL_SUM = "/pg/pgCompareApproval/getSum";
    public static final String ESCROW_GET_PG_COMPARE_APPROVAL_DIFF = "/pg/pgCompareApproval/getDiff";

    /** 도로명 시도명 리스트 조회 */
    public static final String ESCROW_GET_ZIPCODE_SIDO_LIST = "/escrow/zipcode/getZipcodeSidoList";
    /** 도로명 시군구명 리스트 조회 */
    public static final String ESCROW_GET_ZIPCODE_SIGUNGU_LIST = "/escrow/zipcode/getZipcodeSigunguList";
    /** 우편번호 검색 */
    public static final String ESCROW_GET_ZIPCODE_MANAGE = "/escrow/zipcode/getZipcodeManageList";
    /** 도서산간여부(제주/도서산간/해당없음) 수정 */
    public static final String ESCROW_UPDATE_ZIPCODE_ISLAND_TYPE = "/escrow/zipcode/updateZipcodeIslandType";

    /** 영업일 리스트 조회 */
    public static final String ESCROW_GET_BUSINESS_DAY_LIST = "/escrow/businessDay/getBusinessDayList";
    /** 영업일 등록 */
    public static final String ESCROW_SAVE_BUSINESS_DAY = "/escrow/businessDay/saveBusinessDay";
    /** 영업일 수정 */
    public static final String ESCROW_UPDATE_BUSINESS_DAY = "/escrow/businessDay/updateBusinessDay";

    /** 점포별 우편번호 리스트 조회 */
    public static final String ESCROW_GET_STORE_ZIPCODE_List = "/escrow/storeDelivery/getStoreZipcodeList";

    /** 점포별 휴일 리스트 조회 */
    public static final String ESCROW_GET_STORE_CLOSE_DAY_LIST = "/escrow/storeDelivery/getStoreCloseDayList";
    /** 휴일 이미지 리스트 조회 */
    public static final String ESCROW_GET_CLOSE_DAY_IMAGE_LIST = "/escrow/storeDelivery/getCloseDayImageList";
    /** 휴일 이미지 저장 */
    public static final String ESCROW_SAVE_CLOSE_DAY_IMAGE_LIST = "/escrow/storeDelivery/saveCloseDayImageList";

    /** 점포 shift 조회 */
    public static final String ESCROW_GET_STORE_SHIFT_MANAGE_LIST = "/escrow/storeDelivery/getShiftManageList";
    /** 점포 slot 조회 */
    public static final String ESCROW_GET_STORE_SLOT_MANAGE_LIST = "/escrow/storeDelivery/getSlotManageList";
    /** 점포 slot 사용여부 저장 */
    public static final String ESCROW_SAVE_SLOT_MANAGE = "/escrow/storeDelivery/saveSlotManage";
    /** 원거리 배송 조회 */
    public static final String ESCROW_GET_REMOTE_SHIP_LIST = "/escrow/storeDelivery/getRemoteShipList";

    /** 상품별 Shift 관리 리스트 조회 */
    public static final String ESCROW_GET_ITEM_SHIFT_MANAGE_LIST = "/escrow/storeDelivery/getItemShiftManageList";
    /** 단일상품 Shift 등록 (다중점포) */
    public static final String ESCROW_SAVE_ITEM_SHIFT_LIST = "/escrow/storeDelivery/saveItemShiftList";
    /** 단일상품 Shift 등록 (단일점포) */
    public static final String ESCROW_SAVE_ITEM_SHIFT = "/escrow/storeDelivery/saveItemShift";
    /** 상품별 Shift 관리 리스트 삭제 */
    public static final String ESCROW_DELETE_ITEM_SHIFT_LIST = "/escrow/storeDelivery/deleteItemShiftList";

    /** 픽업점포 관리 리스트 조회 */
    public static final String ESCROW_GET_PICKUP_STORE_MANAGE_LIST = "/escrow/storeDelivery/getPickupStoreManageList";
    /** 픽업점포 락커 리스트 조회 */
    public static final String ESCROW_GET_PICKUP_STORE_LOCKER_LIST = "/escrow/storeDelivery/getPickupStoreLockerList";

    /** 선물세트 발송지 리스트 조회 */
    public static final String ESCROW_GET_RESERVE_SHIP_PLACE_MNG_LIST = "/escrow/shipManage/getReserveShipPlaceMngList";
    /** 선물세트 발송지 저장 */
    public static final String ESCROW_SAVE_RESERVE_SHIP_PLACE = "/escrow/shipManage/saveReserveShipPlace";
    /** 선물세트 발송지 리스트 저장 (다중건) */
    public static final String ESCROW_SAVE_RESERVE_SHIP_PLACE_LIST = "/escrow/shipManage/saveReserveShipPlaceList";
    /** 선물세트 발송지 리스트 삭제 */
    public static final String ESCROW_DELETE_RESERVE_SHIP_PLACE_LIST = "/escrow/shipManage/deleteReserveShipPlaceList";

    /** 주문배치관리 목록 조회 */
    public static final String ESCROW_GET_ORDER_BATCH_MANAGE_LIST = "/escrow/orderBatch/getOrderBatchManageList";
    /** 주문배치관리 마스터 저장 */
    public static final String ESCROW_SAVE_ORDER_BATCH_MANAGE = "/escrow/orderBatch/saveOrderBatchManage";
    /** 주문배치관리 마스터 삭제 */
    public static final String ESCROW_DELETE_ORDER_BATCH_MANAGE = "/escrow/orderBatch/deleteOrderBatchManage";
    /** 주문배치히스토리 목록 조회 */
    public static final String ESCROW_GET_ORDER_BATCH_HISTORY_LIST = "/escrow/orderBatch/getOrderBatchHistoryList";
    /** LGW 배치이력 목록조회 */
    public static final String ESCROW_GET_LGW_BATCH_HISTORY_LIST = "/escrow/orderBatch/getLgwBatchHistoryList";

    /** 통계주문정보 랭킹 리스트 조회 */
    public static final String ESCROW_GET_STATISTICS_ORDER_INFO_RANKING_LIST = "/escrow/statistics/getStatisticsOrderInfoRankingList";
    /** 통계주문정보 점포상품 랭킹 리스트 조회 */
    public static final String ESCROW_GET_STATISTICS_ORDER_INFO_STORE_ITEM_RANKING_LIST = "/escrow/statistics/getStatisticsOrderInfoStoreItemRankingList";
    /** 통계주문정보 점포별 리스트 조회 */
    public static final String ESCROW_GET_STATISTICS_ORDER_INFO_LIST_BY_STORE = "/escrow/statistics/getStatisticsOrderInfoListByStore";
    /** 통계주문정보 판매업체별 리스트 조회 */
    public static final String ESCROW_GET_STATISTICS_ORDER_INFO_LIST_BY_PARTNER = "/escrow/statistics/getStatisticsOrderInfoListByPartner";
    /** 점포 기본정보 조회 */
    public static final String ESCROW_GET_STORE_BASIC_INFO = "/escrow/statistics/getStoreBasicInfo";
    /** 실시간 특정 상품 판매현황 리스트 조회 */
    public static final String ESCROW_GET_ONE_ITEM_SALE_LIST = "/escrow/statistics/getRealTimeOneItemSaleList";

    /** 카드 PREFIX 조회 */
    public static final String ESCROW_GET_CARD_PREFIX_MANAGE = "/pg/paymentMethod/getCardPrefixManageList";
    /** 카드 PREFIX 저장 */
    public static final String ESCROW_SAVE_CARD_PREFIX_MANAGE = "/pg/paymentMethod/saveCardPrefixManage";
    /** 카드 PREFIX 결제수단 일괄변경 */
    public static final String ESCROW_SAVE_BUNDLE_CARD_PREFIX_METHOD = "/pg/paymentMethod/saveBundleCardPrefixMethod";
    /** PG 수수료 마스터 조회 */
    public static final String ESCROW_GET_PG_COMMISSION_MNG_LIST = "/pg/paymentMethod/getPgCommissionMngList";
    /** PG 수수료 대상 조회 */
    public static final String ESCROW_GET_PG_COMMISSION_TARGET_LIST = "/pg/paymentMethod/getPgCommissionTargetList";
    /** PG 수수료 저장 */
    public static final String ESCROW_SAVE_PG_COMMISSION = "/pg/paymentMethod/savePgCommission";
    /** 중복제거 전체 결제수단 조회 */
    public static final String ESCROW_GET_ALL_DISTINCT_METHOD_CD = "/pg/paymentMethod/getAllDistinctMethodCd";
    /** 중복 수수료 정책 조회 */
    public static final String ESCROW_CHECK_DUPLICATE_COMMISSION = "/pg/paymentMethod/checkDuplicateCommission";

    /** 결제수단별 혜택 조회 */
    public static final String ESCROW_GET_PAYMENT_METHOD_BENEFIT_MANAGE = "/pg/paymentMethod/getPaymentMethodBenefitManage";
    /** 결제수단별 혜택 등록 */
    public static final String ESCROW_SAVE_PAYMENT_METHOD_BENEFIT_MANAGE = "/pg/paymentMethod/savePaymentMethodBenefitManage";
    /** 기획전별 판매현황 - 기획전 */
    public static final String ESCROW_GET_EXHIBITION_SALE_STATISTICS_EXH = "/statistics/getExhibitionSaleStatisticsExh";
    /** 기획전별 판매현황 - 기획전 > 상품 */
    public static final String ESCROW_GET_EXHIBITION_SALE_STATISTICS_ITEM = "/statistics/getExhibitionSaleStatisticsItem";

    /** 카드할인통계 조회 */
    public static final String ESCROW_GET_CARD_DISCOUNT_STATISTICS_LIST = "/escrow/statistics/getCardDiscountStatisticsList";

    /** 카드할인상세 통계 조회 */
    public static final String ESCROW_GET_CARD_DISCOUNT_STATISTICS_DETAIL_LIST = "/escrow/statistics/getCardDiscountStatisticsDetailList";

    /** 쿠폰통계 조회 */
    public static final String ESCROW_GET_COUPON_STATISTICS_LIST = "/escrow/statistics/getCouponStatisticsList";

    /** 상품쿠폰통계 상세 조회 */
    public static final String ESCROW_GET_ITEM_COUPON_STATISTICS_DETAIL_LIST = "/escrow/statistics/getItemCouponStatisticsDetailList";

    /** 장바구니쿠폰 배송비쿠폰 상세 조회*/
    public static final String ESCROW_GET_CART_COUPON_STATISTICS_DETAIL_LIST = "/escrow/statistics/getCartCouponStatisticsDetailList";


    /** 우편번호별 주문현황 통계 조회 */
    public static final String ESCROW_GET_ZIPCODE_ORDER_STATISTICS_LIST = "/statistics/getZipcodeOrderStatisticsList";

    /* 공통코드명 정의 */
    public static final String COMMON_CARD_BIN_KIND = "card_bin_kind";
    public static final String PG_MID_USE_PURPOSE = "pg_mid_use_purpose";
    public static final String SITE_TYPE = "site_type";
    public static final String COMMON_CARD_DETAIL_KIND = "card_detail_kind";

    /* 권한체크용 역할코드 */
    /** 결제혜택 관리 (무이자 혜택 관리, 청구할인 혜택 관리, 결제수단별 혜택 관리, 카드 Prefix 관리) */
    public static final String ROLE_CODE_PAY_COMMON = "pay_common";
    /** 결제수단 관리 (PG 상점 ID 관리, 결제수단 관리, PG 수수료 관리, 기본 배분율 관리, 결제정책 관리) */
    public static final String ROLE_CODE_PAY_MNG = "pay_mng";
    /** 결제정책 상품 관리 (결제정책 상품 관리) */
    public static final String ROLE_CODE_PAY_PRODUCT_COMMON = "pay_product_common";
    /** 예약상품발송 관리 (선물세트 발송지 관리) */
    public static final String ROLE_CODE_REGERVEPRD_SRV_COMMON = "regerveprd_srv_common";
    /** 점포배송정보관리 (점포별 우편번호 조회, 점포별 휴일 조회, Shift 관리, Slot 관리, 원거리배송 관리, 상품별 Shift 관리, 픽업점포관리) */
    public static final String ROLE_CODE_TRANSMNG_COMMON = "transmng_common";
    /** 시스템 관리자 (우편번호관리, 영업일관리) */
    public static final String ROLE_CODE_SYSTEM_DEV = "system_dev";
    /** 결제정책시스템 관리자 (주문 배치 관리) */
    public static final String ROLE_CODE_SYSTEM_ESCROW = "system_escrow";
    /** 대사관리 (PG 승인대사 조회) */
    public static final String ROLE_CODE_PAY_DIFF = "pay_diff";

    /** 장바구니쿠폰통계(점포별) 조회 */
    public static final String STATISTICS_GET_CART_COUPON_STATISTICS = "/statistics/cartcoupon/getCartCouponStatistics";
    /** 장바구니쿠폰통계(일자별) 조회 */
    public static final String STATISTICS_GET_CART_COUPON_STATISTICS_BY_STOREID = "/statistics/cartcoupon/getCartCouponStatisticsByStoreId";

    /** 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회 */
    public static final String ESCROW_GET_CART_COUPON_USE_ORDER_LIST = "/escrow/extract/getCartCouponUseOrderList";
    /** 픽업주문 리스트 조회 */
    public static final String ESCROW_GET_PICKUP_ORDER_LIST = "/escrow/extract/getPickupOrderList";
    /** 결제수단별 결제정보 리스트 조회 */
    public static final String ESCROW_GET_PAYMENT_INFO_LIST_BY_METHOD = "/escrow/extract/getPaymentInfoListByMethod";
    /** 보조결제수단 결제정보 리스트 조회 */
    public static final String ESCROW_GET_ASSISTANCE_PAYMENT_INFO_LIST = "/escrow/extract/getAssistancePaymentInfoList";
    /** 중복쿠폰 사용주문 리스트 조회 */
    public static final String ESCROW_GET_ADD_COUPON_USE_ORDER_LIST = "/escrow/extract/getAddCouponUseOrderList";

    /** 시스템관리 > 합배송 주문 매출 */
    public static final String EXTRACT_GET_CMBN_ORDER_SALES = "/extract/getCmbnOrderSales";
    /** 통계 > 점포상품통계 > 합배송 주문 현황 */
    public static final String STATISTICS_GET_COMBINE_ORDER_STATUS = "/statistics/storeItem/getCombineOrderStatus";
    /** 추출 히스토리 저장 */
    public static final String EXTRACT_SAVE_EXTRACT_HISTORY = "/extract/saveExtractHistory";

    /** 점포 애니타임 정보 조회 */
    public static final String ESCROW_GET_STORE_ANYTIME_INFO = "/escrow/anytime/getStoreAnytimeInfo";
    /** 점포 애니타임 Shift 정보 조회 */
    public static final String ESCROW_GET_STORE_ANYTIME_SHIFT_INFO = "/escrow/anytime/getStoreAnytimeShiftInfo";
    /** 점포 애니타임 사용여부 저장 */
    public static final String ESCROW_SAVE_STORE_ANYTIME_USE_YN = "/escrow/anytime/saveStoreAnytimeUseYn";
    /** 점포 애니타임 Shift 사용여부 저장 */
    public static final String ESCROW_SAVE_STORE_ANYTIME_SHIFT_USE_YN = "/escrow/anytime/saveStoreAnytimeShiftUseYn";

    /** 일자별 Shift 정보 조회 */
    public static final String ESCROW_GET_DAILY_SHIFT_MANAGE_LIST = "/escrow/storeDelivery/getDailyShiftManageList";
    /** 일자별 Shift 점포 정보 조회 */
    public static final String ESCROW_GET_DAILY_SHIFT_STORE_MANAGE_LIST = "/escrow/storeDelivery/getDailyShiftStoreManageList";
    /** 일자별 Shift 관리 중복체크 */
    public static final String ESCROW_CHECK_DUPLICATE_DAILY_SHIFT_MANAGE = "/escrow/storeDelivery/checkDuplicateDailyShiftManage";
    /** 일자별 Shift 정보 저장 */
    public static final String ESCROW_SAVE_DAILY_SHIFT_MANAGE = "/escrow/storeDelivery/saveDailyShiftManage";
    /** 일자별 Shift 점포 정보 저장 */
    public static final String ESCROW_SAVE_DAILY_SHIFT_STORE_MANAGE_LIST = "/escrow/storeDelivery/saveDailyShiftStoreManageList";
    /** 일자별 Shift 정보 삭제 */
    public static final String ESCROW_DELETE_DAILY_SHIFT_MANAGE = "/escrow/storeDelivery/deleteDailyShiftManage";
}
