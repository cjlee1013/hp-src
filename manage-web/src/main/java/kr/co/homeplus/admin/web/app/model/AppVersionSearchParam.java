package kr.co.homeplus.admin.web.app.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppVersionSearchParam {

    @ApiModelProperty(value = "일련번호")
    private int seq;

    @ApiModelProperty(value = "사이트구분")
    private String siteType;

    @ApiModelProperty(value = "디바이스구분")
    private String devicePlatform;

    @ApiModelProperty(value = "사용여부")
    private String isUsed;

}
