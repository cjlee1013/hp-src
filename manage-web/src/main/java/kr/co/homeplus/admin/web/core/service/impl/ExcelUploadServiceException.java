package kr.co.homeplus.admin.web.core.service.impl;

import org.springframework.http.HttpStatus;

/**
 * 엑셀업로드 서비스시 예외사항 처리
 *
 * @see RuntimeException
 */
public class ExcelUploadServiceException extends RuntimeException {
    private HttpStatus httpStatus;
    private String errorMessage;

    public ExcelUploadServiceException(String errorMessage) {
        super(errorMessage);
    }

    public ExcelUploadServiceException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

    public ExcelUploadServiceException(String errorMessage, HttpStatus httpStatus) {
        super(errorMessage);
        this.httpStatus = httpStatus;
    }

    public ExcelUploadServiceException(String errorMessage, HttpStatus httpStatus, Exception e) {
        super(errorMessage, e);
        this.httpStatus = httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
