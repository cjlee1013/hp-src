package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardCouponResultStatisticsDetailGetDto {
    @ApiModelProperty(value = "쿠폰번호")
    @RealGridColumnInfo(headText = "쿠폰번호", width = 80, sortable = true, hidden = true)
    private String couponNo;

    @ApiModelProperty(value = "쿠폰명")
    @RealGridColumnInfo(headText = "쿠폰명", width = 80, sortable = true, hidden = true)
    private String couponNm;

    @ApiModelProperty(value = "결제일자")
    @RealGridColumnInfo(headText = "결제일자", width = 80, sortable = true)
    private String paymentFshDt;

    @ApiModelProperty(value = "승인번호")
    @RealGridColumnInfo(headText = "승인번호", width = 80, sortable = true)
    private String approvalNo;

    @ApiModelProperty(value = "카드결제금액")
    @RealGridColumnInfo(headText = "카드결제금액", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String paymentAmt;

    @ApiModelProperty(value = "원매출금액")
    @RealGridColumnInfo(headText = "원매출금액", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String totItemPrice;

    @ApiModelProperty(value = "장바구니쿠폰금액")
    @RealGridColumnInfo(headText = "장바구니쿠폰금액", width = 90, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String cartDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰금액")
    @RealGridColumnInfo(headText = "중복쿠폰금액", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 80, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "PG")
    @RealGridColumnInfo(headText = "PG", width = 80, sortable = true)
    private String pgKind;

    @ApiModelProperty(value = "MID")
    @RealGridColumnInfo(headText = "MID", width = 80, sortable = true)
    private String pgMid;

    @ApiModelProperty(value = "카드사명")
    @RealGridColumnInfo(headText = "카드사명", width = 80, sortable = true)
    private String methodNm;

    @ApiModelProperty(value = "카드프리픽스")
    @RealGridColumnInfo(headText = "카드프리픽스", width = 80, sortable = true)
    private String cardPrefixNo;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 80, sortable = true)
    private String purchaseOrderNo;
}
