package kr.co.homeplus.admin.web.common.exception;

public class UrlDownloadFileFailedException extends RuntimeException {

    public UrlDownloadFileFailedException(String message) {
        super(message);
    }

    public UrlDownloadFileFailedException(Exception e) {
        super(e);
    }

    public UrlDownloadFileFailedException(String message, Exception e) {
        super(message, e);
    }
}
