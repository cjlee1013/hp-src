package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesListSetDto;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesUploadSetDto;
import kr.co.homeplus.admin.web.settle.service.ResaSalesService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/resaSales")
public class ResaSalesController {

    private final ResaSalesService ResaSalesService;
    private final SettleCommonService settleCommonService;
    private final LoginCookieService loginCookieService;

    @RequestMapping(value = "/List", method = RequestMethod.GET)
    public String ResaSales(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.createForGroup("resaSalesListBaseInfo", ResaSalesListGetDto.class));
        return "/settle/resaSalesList";
    }

    @ResponseBody
    @RequestMapping(value = "/getList.json", method = RequestMethod.GET)
    public List<ResaSalesListGetDto> getList(ResaSalesListSetDto resaSalesListSetDto) throws Exception {
        return ResaSalesService.getList(resaSalesListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getExist.json", method = RequestMethod.GET)
    public int getExist(String basicDt) throws Exception {
        return ResaSalesService.getExist(basicDt);
    }

    @ResponseBody
    @RequestMapping(value = "/setUpload.json", method = RequestMethod.POST)
    public int setUpload(@RequestBody List<ResaSalesUploadSetDto> resaSalesUploadSetDto) throws Exception {
        return ResaSalesService.setUpload(resaSalesUploadSetDto,loginCookieService.getUserInfo().getEmpId());
    }
}