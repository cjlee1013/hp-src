package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspExpMainListGetDto {
    @RealGridColumnInfo(headText = "전시번호", width = 60, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private Long dispId;

    @RealGridColumnInfo(headText = "전시구분", width = 50)
    private String dispTypeTxt;

    @RealGridColumnInfo(headText = "관리명", width = 150, order = 1)
    private String manageNm;

    @RealGridColumnInfo(headText = "대상상품 수", width = 50, columnType = RealGridColumnType.NUMBER_CENTER, order = 2, sortable = true)
    private int itemCnt;

    @RealGridColumnInfo(headText = "전시시작", width = 120, columnType = RealGridColumnType.DATETIME, order = 3, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료", width = 120, columnType = RealGridColumnType.DATETIME, order = 4, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 5, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 6, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 10, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "dispType", order = 12, hidden = true)
    private String dispType;
    @RealGridColumnInfo(headText = "themeNm", order = 13, hidden = true)
    private String themeNm;
    @RealGridColumnInfo(headText = "themeSubNm", order = 14, hidden = true)
    private String themeSubNm;
    @RealGridColumnInfo(headText = "imgUrl", order = 15, hidden = true)
    private String imgUrl;
    @RealGridColumnInfo(headText = "mimgUrl", order = 16, hidden = true)
    private String mimgUrl;
    @RealGridColumnInfo(headText = "tabNm", order = 17, hidden = true)
    private String tabNm;
    @RealGridColumnInfo(headText = "useYn", order = 18, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "regId", order = 19, hidden = true)
    private String regId;
    @RealGridColumnInfo(headText = "chgId", order = 20, hidden = true)
    private String chgId;

}
