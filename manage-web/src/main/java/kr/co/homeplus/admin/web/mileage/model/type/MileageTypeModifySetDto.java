package kr.co.homeplus.admin.web.mileage.model.type;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageTypeModifySetDto {
    //마일리지 번호
    private String mileageTypeNo;

    //점포 유형
    private String storeType;

    //마일리지 유형명
    private String mileageTypeName;

    //마일리지 유형
    private String mileageCategory;

    //마일리지 종류
    private String mileageKind;

    //사용 여부
    private String useYn;

    //등록자
    private String regId;

    //항목 수정시 필요한 것들
    //마일리지타입 설명
    private String mileageTypeExplain;

    //고객 노출 문구
    private String displayMessage;

    //등록시작일
    private String regStartDt;

    //등록종료일
    private String regEndDt;

    //유효기간 설정 유형
    private String expireType;

    //유효기간 설정일
    private int expireDayInp;

    //유효기간 시작일
    private String useStartDt;

    //유효기간 종료일
    private String useEndDt;

    private String mileageCode;
}
