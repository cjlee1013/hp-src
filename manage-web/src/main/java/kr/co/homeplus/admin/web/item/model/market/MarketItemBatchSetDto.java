package kr.co.homeplus.admin.web.item.model.market;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
//마켓연동 배치 등록
public class MarketItemBatchSetDto {


    //상품번호
    private String itemNo;

    //전송타입(0:정기,1: 수시, 2:긴급)
    private Integer sendType;

    private String userId;

}
