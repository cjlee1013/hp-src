package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업점포관리 model")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PickupStoreManageModel {
    @ApiModelProperty(value = "점포유형", position = 1)
    @RealGridColumnInfo(headText = "storeType", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 2)
    @RealGridColumnInfo(headText = "storeId", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeId;

    @ApiModelProperty(value = "점포명", position = 3)
    @RealGridColumnInfo(headText = "점포명", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @ApiModelProperty(value = "장소번호", position = 4)
    @RealGridColumnInfo(headText = "placeNo", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String placeNo;

    @ApiModelProperty(value = "장소사용여부", position = 5)
    @RealGridColumnInfo(headText = "장소사용여부", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String placeUseYn;

    @ApiModelProperty(value = "픽업장소", position = 6)
    @RealGridColumnInfo(headText = "placeNm", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String placeNm;

    @ApiModelProperty(value = "장소이미지", position = 7)
    @RealGridColumnInfo(headText = "imgUrl", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String imgUrl;

    @ApiModelProperty(value = "전화번호", position = 8)
    @RealGridColumnInfo(headText = "telNo", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String telNo;

    @ApiModelProperty(value = "장소정보", position = 9)
    @RealGridColumnInfo(headText = "장소정보", width = 250, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String placeAddr;

    @ApiModelProperty(value = "픽업장소상세", position = 10)
    @RealGridColumnInfo(headText = "placeExpln", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String placeExpln;

    @ApiModelProperty(value = "운영시간", position = 11)
    @RealGridColumnInfo(headText = "운영시간", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pickupTime;

    @ApiModelProperty(value = "락커사용여부", position = 12)
    @RealGridColumnInfo(headText = "락커운영여부", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String lockerUseYn;

    @ApiModelProperty(value = "사용락커개수", position = 13)
    @RealGridColumnInfo(headText = "락커개수", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private int lockerCount;
}





