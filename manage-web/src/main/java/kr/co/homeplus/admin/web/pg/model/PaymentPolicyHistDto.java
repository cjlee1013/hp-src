package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 히스토리 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentPolicyHistDto {
    @ApiModelProperty(value = "적용코드(상품코드,카테고리번호,제휴코드)")
    @RealGridColumnInfo(headText = "카테고리코드", width = 80, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyCd;

    @ApiModelProperty(value = "적용대상명(상품명,카테고리명,채널명)")
    @RealGridColumnInfo(headText = "카테고리명", width = 200, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyTargetNm;

    @ApiModelProperty(value = "등록일자(apply/method)")
    @RealGridColumnInfo(headText = "등록일", width = 120, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;

    @ApiModelProperty(value = "등록자id(apply/method)")
    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "결제정책이력순번")
    private Long paymentPolicyHistSeq;
    @ApiModelProperty(value = "적용범위(APPLY:적용대상,METHOD:결제수단)")
    private String applyScope;
    @ApiModelProperty(value = "정책번호")
    private Long policyNo;
    @ApiModelProperty(value = "적용우선순위(1:채널,2:상품,3:카테고리(subString...),4:그이외...)")
    private Integer applyPriority;
    @ApiModelProperty(value = "결제수단코드")
    private String methodCd;
}
