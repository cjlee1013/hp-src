package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateDetailSetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateSalesGetDto;
import kr.co.homeplus.admin.web.settle.model.affiliateSales.AffiliateSalesSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AffiliateSalesService {

  private final ResourceClient resourceClient;
  /**
   * 업체관리 > 제휴업체관리 > 제휴채널실적
   */
  public List<AffiliateSalesGetDto> getAffiliateSales(AffiliateSalesSetDto affiliateSalesSetDto) throws Exception {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SETTLE,
        affiliateSalesSetDto,
        "/admin/affiliate/getAffiliateSales",
        new ParameterizedTypeReference<ResponseObject<List<AffiliateSalesGetDto>>>(){}).getData();
  }
  public List<AffiliateDetailGetDto> getAffiliateDetail(
      AffiliateDetailSetDto affiliateDetailSetDto) throws Exception {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SETTLE,
        affiliateDetailSetDto,
        "/admin/affiliate/getAffiliateDetail",
        new ParameterizedTypeReference<ResponseObject<List<AffiliateDetailGetDto>>>(){}).getData();
  }
}
