package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.pop.StoreGetDto;
import lombok.Data;

/**
 * 셀링포인트
 */
@Data
public class ItemPrGetDto {
	//PR번호
	private Integer prNo;

	//유형-말머리(HEAD),구매가이드(GUIDE)
	private String 	prType;

	//유형명
	private String 	prTypeNm;

	//내용
	private String 	prContents;

	//필드명이랑 다름 (dispStartDt)
	//노출시작기간
	private String 	prDispStartDt;

	//필드명이랑 다름 (dispEndDt)
	//노출종료기간
	private String 	prDispEndDt;

	//기간설정
	private String 	prPeriodYn;

	//적용점포수
	private Integer storeCount;

	private String copyStorePrNo;

	//적용점포 리스트
	private List<StoreGetDto> prStoreList;
}
