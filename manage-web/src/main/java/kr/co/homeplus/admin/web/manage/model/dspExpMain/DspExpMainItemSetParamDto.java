package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("익스프레스 상품 등록")
public class DspExpMainItemSetParamDto {

    private Long dispId;
    private String itemNo;
    private int priority;
    private String dispYn;
    private String regId;

}
