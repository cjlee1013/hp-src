package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 상세 요청 DTO")
public class CallCenterDetailSetDto {
  @ApiModelProperty(notes = "마스터 일련번호")
  @JsonProperty("MASTER_SEQ")
  private String MASTER_SEQ;

  @ApiModelProperty(notes = "채널종류")
  @JsonProperty("CHANNEL_TYPE")
  private String CHANNEL_TYPE;
}