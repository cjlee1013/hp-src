package kr.co.homeplus.admin.web.message.service;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.ImageFileInfoDto;
import kr.co.homeplus.admin.web.core.dto.UploadImageFileDto;
import kr.co.homeplus.admin.web.core.dto.imageFile.UploadFileDto;
import kr.co.homeplus.admin.web.core.service.UploadService;
import kr.co.homeplus.admin.web.message.model.PushImageSetRequest;
import kr.co.homeplus.admin.web.message.model.PushImageSetResult;
import kr.co.homeplus.admin.web.message.model.ReturnCode;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class PushImageService {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imageFrontDomainPrefix;

    private final ResourceClient resourceClient;
    private final UploadService uploadService;
    private final LoginCookieService loginCookieService;

    public PushImageSetResult setPushImage(String imageName,
        final HttpServletRequest request) {
        String imageFileFieldName = "imageFile";

        UploadImageFileDto uploadImageFileDto = new UploadImageFileDto("PushImage", null, null, "FILE");
        uploadImageFileDto.setImageFieldName(imageFileFieldName);

        Map<String, List<ImageFileInfoDto>> uploadResult;
        try {
            uploadResult = uploadService
                .getUploadImageFileInfo(request, Arrays.asList(uploadImageFileDto));
        } catch (Exception e) {
            PushImageSetResult pushImageSetResult = new PushImageSetResult();
            pushImageSetResult.setReturnCode(ReturnCode.ERROR);
            pushImageSetResult.setMessage("파일 업로드 중 예외가 발생 하였습니다. cuase: " + e.getMessage());
            return pushImageSetResult;
        }

        List<ImageFileInfoDto> uploaded = uploadResult.get("fileArr");

        ImageFileInfoDto imageFileInfoDto;
        if (uploaded == null || uploaded.isEmpty()) {
            PushImageSetResult pushImageSetResult = new PushImageSetResult();
            pushImageSetResult.setReturnCode(ReturnCode.ERROR);
            pushImageSetResult.setMessage("파일 업로드 결과가 없습니다.");
            return pushImageSetResult;
        } else {
            imageFileInfoDto = uploaded.get(0);

            if (StringUtils.isNotBlank(imageFileInfoDto.getError())) {
                PushImageSetResult pushImageSetResult = new PushImageSetResult();
                pushImageSetResult.setReturnCode(ReturnCode.ERROR);
                pushImageSetResult.setMessage("파일 업로드가 실패 하였습니다. cause: " + imageFileInfoDto.getError());
                return pushImageSetResult;
            }
        }

        UploadFileDto fileStoreInfo = imageFileInfoDto.getFileStoreInfo();
        String fileId = fileStoreInfo.getFileId();
        String fileName = Normalizer.normalize(StringUtils.defaultString(
            fileStoreInfo.getFileName()), Normalizer.Form.NFC);

        PushImageSetRequest pushImageSetRequest = new PushImageSetRequest();
        pushImageSetRequest.setImageName(imageName);
        pushImageSetRequest.setOriginName(fileName);
        pushImageSetRequest.setUrl(imageFrontDomainPrefix + fileId);
        pushImageSetRequest.setRegId(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<PushImageSetResult> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, pushImageSetRequest,
                "/admin/pushimage/setPushImage", new ParameterizedTypeReference<>() {
                });

        return responseObject.getData();
    }
}
