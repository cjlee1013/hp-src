package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class RelatedKeywordGridDto {

    @Getter
    @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class RelatedKeywordInclude {
        @RealGridColumnInfo(headText = "번호", width = 0, hidden = true, columnType = RealGridColumnType.NUMBER)
        private Long relationId;
        @RealGridColumnInfo(headText = "사이트", width = 0, hidden = true, columnType = RealGridColumnType.BASIC)
        private String siteType;
        @RealGridColumnInfo(headText = "검색어", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "연관검색어", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String relationKeyword;
        @RealGridColumnInfo(headText = "운영기간 사용여부", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String operationPeriodYn;
        @RealGridColumnInfo(headText = "운영시작일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "운영종료일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
        @RealGridColumnInfo(headText = "사용여부", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String useYn;
        @RealGridColumnInfo(headText = "등록자", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String creator;
        @RealGridColumnInfo(headText = "수정자", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String modifier;
        @RealGridColumnInfo(headText = "등록일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String createdDt;
        @RealGridColumnInfo(headText = "수정일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String modifyDt;
        @RealGridColumnInfo(headText = "운영상태", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String status;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class RelatedKeywordExclude {
        @RealGridColumnInfo(headText = "번호", width = 0, hidden = true, columnType = RealGridColumnType.NUMBER)
        private Long relationId;
        @RealGridColumnInfo(headText = "사이트", width = 0, hidden = true, columnType = RealGridColumnType.BASIC)
        private String siteType;
        @RealGridColumnInfo(headText = "비노출 검색어", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "비노출기간 사용여부", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String operationPeriodYn;
        @RealGridColumnInfo(headText = "비노출 시작일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "비노출 종료일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
        @RealGridColumnInfo(headText = "사용여부", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String useYn;
        @RealGridColumnInfo(headText = "등록자", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String creator;
        @RealGridColumnInfo(headText = "수정자", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String modifier;
        @RealGridColumnInfo(headText = "등록일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String createdDt;
        @RealGridColumnInfo(headText = "수정일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String modifyDt;
        @RealGridColumnInfo(headText = "운영상태", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String status;
    }
}
