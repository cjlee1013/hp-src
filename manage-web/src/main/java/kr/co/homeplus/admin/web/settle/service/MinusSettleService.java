package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleCompleteSetDto;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.minusSettle.MinusSettleListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MinusSettleService {

    private final ResourceClient resourceClient;
    private final SettleCommonService settleCommonService;

    /**
     * DS역정산 관리 리스트 조회
     */
    public List<MinusSettleListGetDto> getMinusSettleList(MinusSettleListSetDto minusSettleListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            minusSettleListSetDto,
            "/admin/minusSettle/getMinusSettleList",
            new ParameterizedTypeReference<ResponseObject<List<MinusSettleListGetDto>>>(){}).getData();
    }

    /**
     * 수기마감
     */
    public ResponseObject<Object> setMinusSettleComplete(MinusSettleCompleteSetDto minusSettleCompleteSetDto) throws Exception{
        minusSettleCompleteSetDto.setChgId(settleCommonService.getUserCd());

        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            minusSettleCompleteSetDto,
            "/admin/minusSettle/setMinusSettleComplete",
            new ParameterizedTypeReference<ResponseObject<Object>>() {});
    }

}
