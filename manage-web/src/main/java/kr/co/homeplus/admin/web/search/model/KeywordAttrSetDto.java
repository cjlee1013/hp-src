package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeywordAttrSetDto {
    private Long keywordAttrNo;
    private String keyword;
    private String useYn;
    private String regId;
    private String regDt;
    private String chgId;
    private String chgDt;
    private List<KeywordGattr> keywordAttrMngList;

    @Getter
    @Setter
    static class KeywordGattr {
        //private Long keywordAttrMngNo;
        private Long keywordAttrNo;
        private Long gattrNo;
        private Long priority;
        private Long attrNo;
    }
}
