package kr.co.homeplus.admin.web.common.model.cnotice;

import lombok.Data;

@Data
public class AdminNoticeListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchType;

    private String searchKeyword;

}
