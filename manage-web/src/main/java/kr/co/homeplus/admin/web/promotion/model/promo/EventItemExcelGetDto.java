package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventItemExcelGetDto {
    private String itemNo;
    private String itemNm;
}