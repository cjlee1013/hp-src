package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "대체공제 쿠폰 통계 리스트 조회 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderSubstitutionStatisticsGetDto {

    @ApiModelProperty(value = "점포유형", position = 1)
    @RealGridColumnInfo(headText = "점포유형", width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(value = "점포명", position = 2)
    @RealGridColumnInfo(headText = "점포명", width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @ApiModelProperty(value = "매출금액", position = 3)
    @RealGridColumnInfo(headText = "매출금액", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long salesPrice;

    @ApiModelProperty(value = "매출상품수", position = 4)
    @RealGridColumnInfo(headText = "매출상품수", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long salesCnt;

    @ApiModelProperty(value = "대체수락 건", position = 5)
    @RealGridColumnInfo(headText = "대체수락 건", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totSubstitutionCnt;

    @ApiModelProperty(value = "대체수락중결품건", position = 6)
    @RealGridColumnInfo(headText = "대체 수락 중 결품 건", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totSubstitutionOosCnt;

    @ApiModelProperty(value = "대체수락완료건", position = 7)
    @RealGridColumnInfo(headText = "대체 수락 완료 건", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totCompleteSubstitutionCnt;

    @ApiModelProperty(value = "대체수락완료건원매출금액", position = 8)
    @RealGridColumnInfo(headText = "대체 수락 완료건 원매출금액", width = 200, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totOriginSalesPrice;

    @ApiModelProperty(value = "쿠폰발생금액", position = 9)
    @RealGridColumnInfo(headText = "쿠폰발생금액", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long totCouponPrice;

    @ApiModelProperty(value = "상점아이디", position = 10)
    @RealGridColumnInfo(headText = "상점아이디", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String originStoreId;
}
