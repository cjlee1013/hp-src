package kr.co.homeplus.admin.web.settle.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentStoreGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentStoreGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoSumGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListSetDto;
import kr.co.homeplus.admin.web.settle.service.ManagementSettleService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/management")
public class managementSettleController {

    private final ManagementSettleService managementSettleService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    @RequestMapping(value = "/paymentInfoList", method = RequestMethod.GET)
    public String paymentInfoList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("paymentInfoSumBaseInfo", PaymentInfoSumGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("paymentInfoListBaseInfo", PaymentInfoListGetDto.class));

        return "/settle/paymentInfoList";
    }

    @ResponseBody
    @RequestMapping(value = "/getPaymentInfoSum.json", method = RequestMethod.GET)
    public List<PaymentInfoSumGetDto> getPaymentInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        return managementSettleService.getPaymentInfoSum(paymentInfoListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getPaymentInfoList.json", method = RequestMethod.GET)
    public List<PaymentInfoListGetDto> getPaymentInfoList(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        return managementSettleService.getPaymentInfoList(paymentInfoListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getSettleInfoSum.json", method = RequestMethod.GET)
    public List<PaymentInfoSumGetDto> getSettleInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        return managementSettleService.getSettleInfoSum(paymentInfoListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getSettleInfoList.json", method = RequestMethod.GET)
    public List<PaymentInfoListGetDto> getSettleInfoList(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        return managementSettleService.getSettleInfoList(paymentInfoListSetDto);
    }


    @RequestMapping(value = "/PgPaymentInfoList", method = RequestMethod.GET)
    public String PgPaymentInfoList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("pgPaymentInfoListBaseInfo", PgPaymentInfoListGetDto.class));

        return "/settle/pgPaymentInfoList";
    }

    @ResponseBody
    @RequestMapping(value = "/getPgPaymentInfoList.json", method = RequestMethod.GET)
    public List<PgPaymentInfoListGetDto> getPgPaymentInfoList(
        PgPaymentInfoListSetDto pgPaymentInfoListSetDto) throws Exception {
        return managementSettleService.getPgPaymentInfoList(pgPaymentInfoListSetDto);
    }


    @RequestMapping(value = "/dgvPayment", method = RequestMethod.GET)
    public String dgvPayment(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentStoreBaseInfo", DgvPaymentStoreGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentDailyBaseInfo", DgvPaymentDailyGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentListBaseInfo", DgvPaymentListGetDto.class));

        List<MngCodeGetDto> code = codeService.getCode(
            "store_type"              // 점포유형
        ).get("store_type");
        List<MngCodeGetDto> storeCode = new ArrayList<>();
        for (MngCodeGetDto dto : code) {
            if ("REAL".equals(dto.getRef3())){
                storeCode.add(dto);
            }
        }
        model.addAttribute("storeType", storeCode);
        model.addAttribute("getKind","DGV");
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        return "/settle/etcPayment";
    }

    @ResponseBody
    @RequestMapping(value = "/getDGVPaymentStore.json", method = RequestMethod.GET)
    public List<DgvPaymentStoreGetDto> getDgvPaymentStore(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        return managementSettleService.getDgvPaymentStore(dgvPaymentSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getDGVPaymentDaily.json", method = RequestMethod.GET)
    public List<DgvPaymentDailyGetDto> getDgvPaymentDaily(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        return managementSettleService.getDgvPaymentDaily(dgvPaymentSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getDGVPaymentList.json", method = RequestMethod.GET)
    public List<DgvPaymentListGetDto> getDgvPaymentList(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        return managementSettleService.getDgvPaymentList(dgvPaymentSetDto);
    }


    @RequestMapping(value = "/mhcPayment", method = RequestMethod.GET)
    public String mhcPayment(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentStoreBaseInfo", MhcPaymentStoreGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentDailyBaseInfo", MhcPaymentDailyGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("etcPaymentListBaseInfo", MhcPaymentListGetDto.class));

        List<MngCodeGetDto> code = codeService.getCode(
            "store_type"              // 점포유형
        ).get("store_type");
        List<MngCodeGetDto> storeCode = new ArrayList<>();
        for (MngCodeGetDto dto : code) {
            if ("REAL".equals(dto.getRef3())){
                storeCode.add(dto);
            }
        }
        model.addAttribute("storeType", storeCode);
        model.addAttribute("getKind","MHC");
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        return "/settle/etcPayment";
    }

    @ResponseBody
    @RequestMapping(value = "/getMHCPaymentStore.json", method = RequestMethod.GET)
    public List<MhcPaymentStoreGetDto> getMhcPaymentStore(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        return managementSettleService.getMhcPaymentStore(mhcPaymentSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getMHCPaymentDaily.json", method = RequestMethod.GET)
    public List<MhcPaymentDailyGetDto> getMhcPaymentDaily(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        return managementSettleService.getMhcPaymentDaily(mhcPaymentSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getMHCPaymentList.json", method = RequestMethod.GET)
    public List<MhcPaymentListGetDto> getMhcPaymentList(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        return managementSettleService.getMhcPaymentList(mhcPaymentSetDto);
    }
}