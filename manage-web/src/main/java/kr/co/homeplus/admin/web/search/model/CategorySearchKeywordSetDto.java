package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategorySearchKeywordSetDto {
    private Long dcateSearchKeywordNo;
    private int dcateCd;
    private String keyword;
    private String useYn;
    private String regId;
    private String chgId;
    private String confirmId;
}
