package kr.co.homeplus.admin.web.core.exception;

/**
 * 로그인 세션이 없을경우 반환하는 Exception
 */
@Deprecated
public class SessionNotFoundException extends RuntimeException {
    public SessionNotFoundException() {
    }

    public SessionNotFoundException(String message) {
        super(message);
    }

    public SessionNotFoundException(Exception e) {
        super(e);
    }
}
