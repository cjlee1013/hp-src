package kr.co.homeplus.admin.web.manage.model.dspMainThemeItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemeItemSetParamDto {

    private Long itemSeq;
    private Long themeId;
    private String themeItemType;
    private String itemNo;
    private String dispDt;
    private int priority;
    private String dispYn;
    private String itemStoreType;
    private String imgUrl;
    private Integer imgWidth;
    private Integer imgHeight;
    private String linkType;
    private String linkInfo;
    private String regId;

}
