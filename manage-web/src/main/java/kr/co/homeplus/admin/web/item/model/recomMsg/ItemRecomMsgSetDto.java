package kr.co.homeplus.admin.web.item.model.recomMsg;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품 추천문구 저장")
public class ItemRecomMsgSetDto {
    // 추천문구 번호
    private Long recomNo;

    // 관리명
    private String recomMng;

    // 추천문구
    private String recomMsg;

    // 점포유형(HYPER|EXPRESS|DS)
    private String storeType;

    // 전시시작일시
    private String dispStartDt;

    // 전시종료일시
    private String dispEndDt;

    // 사용여부
    private String useYn;

    // 등록자
    private String empNo;

    // 상품리스트
    private List<String> itemNoList;
}
