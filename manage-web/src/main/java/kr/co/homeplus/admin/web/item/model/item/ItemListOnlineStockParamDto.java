package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemListOnlineStockParamDto {

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;
}
