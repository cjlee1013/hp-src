package kr.co.homeplus.admin.web.manage.model.dspMobileMain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MobileMainStoreSetDto {
    private String storeId;
    private String storeNm;
}