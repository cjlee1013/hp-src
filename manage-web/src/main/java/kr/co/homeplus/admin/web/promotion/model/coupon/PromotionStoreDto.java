package kr.co.homeplus.admin.web.promotion.model.coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromotionStoreDto {
    private String storeId;
    private String storeNm;
    private String useYn;
}