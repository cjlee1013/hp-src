package kr.co.homeplus.admin.web.settle.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class BalanceSheetListSetDto {
  @ApiModelProperty(notes = "시작일")
  private String startDt;

  @ApiModelProperty(notes = "종료일")
  private String endDt;

  @ApiModelProperty(notes = "전표유형")
  private String slipType;
}
