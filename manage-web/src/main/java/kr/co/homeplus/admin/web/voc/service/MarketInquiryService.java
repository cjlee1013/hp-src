package kr.co.homeplus.admin.web.voc.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryListGetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryListSetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.NaverInquiryAnswerSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MarketInquiryService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;
  private final LoginCookieService cookieService;

  /**
   * 제휴사 고객문의 리스트 조회
   */
  public List<MarketInquiryListGetDto> getMarketInquiryList(
      MarketInquiryListSetDto marketInquiryListSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        marketInquiryListSetDto,
        "/market/getMarketInquiryList",
        new ParameterizedTypeReference<ResponseObject<List<MarketInquiryListGetDto>>>(){}).getData();
  }

  /**
   * 제휴사 고객문의 상세 조회
   */
  public MarketInquiryDetailGetDto getMarketInquiryDetail(
      MarketInquiryDetailSetDto marketInquiryDetailSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        marketInquiryDetailSetDto,
        "/market/getMarketInquiryDetail",
        new ParameterizedTypeReference<ResponseObject<MarketInquiryDetailGetDto>>(){}).getData();
  }

  /**
   * 네이버 고객문의 답변등록
   */
  public ResponseObject<Object> setNaverInquiryAnswer(NaverInquiryAnswerSetDto naverInquiryAnswerSetDto) throws Exception {
    naverInquiryAnswerSetDto.setIsresponseId(shipCommonService.getUserId());
    naverInquiryAnswerSetDto.setIsresponseNm(shipCommonService.getUserNm());

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        naverInquiryAnswerSetDto,
        "/market/setNaverInquiryAnswer",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

}
