package kr.co.homeplus.admin.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserInfo {

    // 사용자 사번
    private String empId;

    // 사용자 아이디
    private String userId;

    // 사용자 이름
    private String userNm;

    // 사용자 부서코드
    private String deptCd;

    // 사용자 부서명
    private String deptNm;

    //점포코드
    private String storeId;

    //최초 로그인 여부
    private boolean isFirstLogin;

    //동일 비밀번호 90일 초과여부
    private boolean isUserPw90DaysExceeded;

    //7회이상 로그인 실패시 계정잠금 여부
    private boolean isLockUser;
}
