package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "사은 행사정보 팝업 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = false)
public class OrderGiftInfoDto {
    @ApiModelProperty(value = "점포", position = 1)
    @RealGridColumnInfo(headText = "점포", width = 120)
    private String storeNm;
    @ApiModelProperty(value = "상품번호", position = 2)
    @RealGridColumnInfo(headText = "상품번호", width = 120)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    @RealGridColumnInfo(headText = "상품명", width = 170)
    private String itemNm1;
    @ApiModelProperty(value = "행사구분", position = 4)
    @RealGridColumnInfo(headText = "행사구분")
    private String giftType;
    @ApiModelProperty(value = "행사번호", position = 5)
    @RealGridColumnInfo(headText = "행사번호", width = 120)
    private String giftNo;
    @ApiModelProperty(value = "행사명", position = 6)
    @RealGridColumnInfo(headText = "행사명", width = 200)
    private String giftMsg;
}
