package kr.co.homeplus.admin.web.manage.model.dspCacheManage;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspCacheManageListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long manageSeq;

    @RealGridColumnInfo(headText = "캐쉬명", width = 170, order = 1, sortable = true)
    private String cacheNm;

    @RealGridColumnInfo(headText = "캐쉬 URL", width = 250, order = 2, sortable = true)
    private String cacheUrl;

    @RealGridColumnInfo(headText = "강제생성적용일", columnType = RealGridColumnType.DATETIME, width = 120, order = 3, sortable = true)
    private String latestDt;

    @RealGridColumnInfo(headText = "사용여부", order = 4, hidden = true, sortable = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 5, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 6, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 7, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 8, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 9, sortable = true)
    private String chgDt;


    private Integer manageDepth;
    private Integer parentsManageSeq;
    private String priority;
    private String cacheKey;
    private int cacheCount;
    private String regId;
    private String chgId;

}
