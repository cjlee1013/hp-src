package kr.co.homeplus.admin.web.pg.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodBenefitManageDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodBenefitManageSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodBenefitManageService {
    private final ResourceClient resourceClient;

    /**
     * 결제수단별 혜택 조회
     *
     * @param paymentMethodBenefitManageSelectDto
     * @return
     */
    public List<PaymentMethodBenefitManageDto> getPaymentMethodBenefitManageList(PaymentMethodBenefitManageSelectDto paymentMethodBenefitManageSelectDto) {
        String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_METHOD_BENEFIT_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentMethodBenefitManageDto>>>() {};
        return (List<PaymentMethodBenefitManageDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, paymentMethodBenefitManageSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 결제수단별 혜택 등록
     *
     * @param paymentMethodBenefitManageDto
     * @return
     */
    public ResponseObject<Object> savePaymentMethodBenefitManage(PaymentMethodBenefitManageDto paymentMethodBenefitManageDto) {
        String apiUri = EscrowConstants.ESCROW_SAVE_PAYMENT_METHOD_BENEFIT_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, paymentMethodBenefitManageDto, apiUri, typeReference);
    }
}
