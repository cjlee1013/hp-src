package kr.co.homeplus.admin.web.statistics.model;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AverageOrderItemQtyListGetDto {

    @ApiModelProperty(value = "점포")
    @RealGridColumnInfo(headText = "점포")
    private String storeNm;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드")
    private String storeId;

    @ApiModelProperty(value = "평균 주문 상품수")
    @RealGridColumnInfo(headText = "평균 주문 상품수", fieldType = RealGridFieldType.NUMBER)
    private String averageItemCnt;

    @ApiModelProperty(value = "평균 주문 상품 ITEM수")
    @RealGridColumnInfo(headText = "평균 주문 ITEM수", fieldType = RealGridFieldType.NUMBER)
    private String averageItemQty;

}
