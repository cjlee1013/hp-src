package kr.co.homeplus.admin.web.settle.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.settle.model.PartnerSettleState;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.SettlePayMethod;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettlePreDtSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleStateSetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.service.PartnerSettleService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/partnerSettle")
public class PartnerSettleMngController {

    private final PartnerSettleService partnerSettleService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public PartnerSettleMngController(PartnerSettleService partnerSettleService, SettleCommonService settleCommonService, CodeService codeService) {
        this.partnerSettleService = partnerSettleService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산관리 > 정산관리 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/partnerSettleMng", method = RequestMethod.GET)
    public String partnerSettleMng(Model model) throws Exception {
        model.addAttribute("settleState", Arrays.asList(PartnerSettleState.values()));
        model.addAttribute("payMethodNm", Arrays.asList(SettlePayMethod.values()));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("partnerSettleMngBaseInfo", PartnerSettleListGetDto.class));

        return "/settle/partnerSettleMng";
    }

    /**
     * 정산관리 > 정산관리 > 메인 리스트 조회
     * @return PartnerSettleListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerSettleList.json", method = RequestMethod.GET)
    public List<PartnerSettleListGetDto> getPartnerSettleList(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getPartnerSettleList(listParamDto);
    }

    /**
     * 정산관리 > 상품별 상세 조회
     * @return PartnerSettleItemGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettleProd.json", method = RequestMethod.GET)
    public List<PartnerSettleItemGetDto> getDailySettleProd(PartnerSettleItemSetDto listParamDto) throws Exception {
        return partnerSettleService.getPartnerSettleItem(listParamDto);
    }

    /**
     * 정산관리 > 정산완료
     * @return ResponseObject<String>
     */
    @ResponseBody
    @RequestMapping(value = "/setSettleComplete.json", method = RequestMethod.GET)
    public ResponseObject<String> setSettleComplete(PartnerSettleStateSetDto listParamDto) throws Exception {
        return partnerSettleService.setSettleComplete(listParamDto);
    }

    /**
     * 정산관리 > 졍산보류해제
     * @return ResponseObject<String>
     */
    @ResponseBody
    @RequestMapping(value = "/setSettlePostpone.json", method = RequestMethod.GET)
    public ResponseObject<String> setSettlePostpone(PartnerSettleStateSetDto listParamDto) throws Exception {
        return partnerSettleService.setSettlePostpone(listParamDto);
    }

    /**
     * 정산관리 > 졍산보류해제
     * @return ResponseObject<String>
     */
    @ResponseBody
    @RequestMapping(value = "/setPostponeCancel.json", method = RequestMethod.GET)
    public ResponseObject<String> setPostponeCancel(PartnerSettleStateSetDto listParamDto) throws Exception {
        return partnerSettleService.setPostponeCancel(listParamDto);
    }

    /**
     * 정산관리 > 지급예정일 변경
     * @return ResponseObject<String>
     */
    @ResponseBody
    @RequestMapping(value = "/setSettlePreDt.json", method = RequestMethod.GET)
    public ResponseObject<String> setSettlePreDt(PartnerSettlePreDtSetDto listParamDto) throws Exception {
        //listParamDto.setSettleSrl(Long.parseLong(request.getParameter("settleSrl")));
        return partnerSettleService.setSettlePreDt(listParamDto);
    }
}