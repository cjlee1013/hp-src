package kr.co.homeplus.admin.web.mileage.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListDto;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListGetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.mileage.model.history.MileageListSetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageCategory;
import kr.co.homeplus.admin.web.mileage.service.MileageHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/mileage/history")
@RequiredArgsConstructor
public class MileageHistoryController {

    private final  MileageHistoryService mileageHistoryService;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 내역 조회 > Main (SETTLE-6)
     * @param model
     * @return String
     */
    @GetMapping(value = "/mileageListMain")
    public String mileageListMain(Model model) throws Exception {
        model.addAttribute("mileageCategory", Arrays.asList(MileageCategory.values()));
        model.addAllAttributes(RealGridHelper.create("mileageListBaseInfo", MileageListDto.class));
        return "/mileage/mileageListMain";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 내역 조회 JSON
     * @return MileageListDto
     */
    @ResponseBody
    @GetMapping(value = "/getMileageList.json")
    public MileageListGetDto getMileageList(@ModelAttribute MileageListSetDto listParamDto) throws Exception {
        return mileageHistoryService.getMileageList(listParamDto);
    }
}