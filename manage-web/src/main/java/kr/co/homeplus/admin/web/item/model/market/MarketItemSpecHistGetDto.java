package kr.co.homeplus.admin.web.item.model.market;

import io.swagger.annotations.ApiModel;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 상세정보 Get Entry
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@ApiModel("상품 연동 이력 History")
public class MarketItemSpecHistGetDto {

	@RealGridColumnInfo(headText = "전송일시", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String marketItemHistDt;

	@RealGridColumnInfo(headText = "호출API명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String methodTxt;

	@RealGridColumnInfo(headText = "비고", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String desc;

	//@RealGridColumnInfo(headText = "지점번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	//private String itemNo;

	//@RealGridColumnInfo(headText = "지점번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	//private String partnerId;

	// 히스토리번호
	@RealGridColumnInfo(headText = "id", fieldType = RealGridFieldType.TEXT, hidden = true)
	private String id;
	// 히스토리 등록일

	@RealGridColumnInfo(headText = "요청내용", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String request;

	@RealGridColumnInfo(headText = "응답내용", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String response;

	@RealGridColumnInfo(headText = "전문", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String spec;


}