package kr.co.homeplus.admin.web.claim.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.ClaimHistoryListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimImageInfoDto;
import kr.co.homeplus.admin.web.claim.model.ClaimProcessListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimReasonInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimDetailInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimReasonInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimRegisterPreRefundInfoDto;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.order.utils.OrderLogUtil;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDetailDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimMainService {

    private final ResourceClient resourceClient;

    private final LoginCookieService loginCookieService;

    private final PrivacyLogService privacyLogService;
    /**
     * 클레임 메인화면 리스트 조회
     * **/
    public List<ClaimOrderGetDto> getClaimList(ClaimOrderSetDto claimOrderSetDto) {

        //점포코드 추출
        //데이터가 있으면 점포사용자 없으면 본사 사용자로 구분
        claimOrderSetDto.setStoreId(loginCookieService.getUserInfo().getStoreId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimOrderSetDto,
            "/claim/getClaimList",
            new ParameterizedTypeReference<ResponseObject<List<ClaimOrderGetDto>>>() {}
        ).getData();
    }

     /**
     * 클레임 상세정보 조회
     *
     * @param claimDetailInfoSetDto 클레임 상세정보 조회 dto
     * @return ClaimDetailInfoGetDto 클레임 상세정보
     */
    public ClaimDetailInfoGetDto getClaimDetailInfo(HttpServletRequest request, ClaimDetailInfoSetDto claimDetailInfoSetDto) {

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = loginCookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("클레임 상세내역 조회")
            .processor(String.valueOf(loginCookieService.getUserInfo().getEmpId()))
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        ClaimDetailInfoGetDto claimInfo = resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimDetailInfoSetDto,
            "/claim/getClaimDetailInfo",
            new ParameterizedTypeReference<ResponseObject<ClaimDetailInfoGetDto>>() {}
        ).getData();

        if(claimInfo.getNomemOrderYn().equals("N")){
            try {
                ResponseObject<UserInfoDetailDto> userInfo =
                    resourceClient.getForResponseObject(
                        ResourceRouteName.USERMNG,
                        "/external/api/search/userInfo?userNo=" + claimInfo.getUserNo(),
                        OrderLogUtil.createPrivacySendAndHeader(PersonalLogMethod.SELECT,
                            claimInfo.getUserNo(), "클레임관리-클레임상세>회원정보조회"),
                        new ParameterizedTypeReference<>() {
                        }
                    );
                if ("0000,SUCCESS".contains(userInfo.getReturnCode())) {
                    claimInfo.setBuyerEmail(userInfo.getData().getUserId());
                }
            } catch (Exception e){
                claimInfo.setUserNo(claimInfo.getUserNo().concat("(탈퇴)"));
            }
        } else {
            claimInfo.setUserNo(claimInfo.getUserNo().concat("(비회원)"));
        }
        return claimInfo;
    }



    /**
    * 클레임 처리내역 리스트 조회
    *
    * @param claimNo
    * @return ClaimProcessListDto
    */
    public List<ClaimProcessListDto> getClaimProcessList(long claimNo, String claimType) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            null,
            "/claim/getClaimProcessList?claimNo="+claimNo + "&claimType=" + claimType,
            new ParameterizedTypeReference<ResponseObject<List<ClaimProcessListDto>>>() {}
        ).getData();
    }

    /**
    * 클레임 히스토리 리스트 조회
    *
    * @param claimNo
    * @return ClaimProcessListDto
    */
    public List<ClaimHistoryListDto> getClaimHistoryList(long claimNo) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            null,
            "/claim/getClaimHistoryList?claimNo="+claimNo,
            new ParameterizedTypeReference<ResponseObject<List<ClaimHistoryListDto>>>() {}
        ).getData();
    }

     /**
     * 클레임 상세정보 리스트 조회
     *
     * @param claimDetailInfoSetDto 클레임 상세정보 조회 dto
     * @return ClaimDetailInfoGetDto 클레임 상세정보
     */
    public List<ClaimDetailInfoListDto> getClaimDetailInfoList(
        ClaimDetailInfoSetDto claimDetailInfoSetDto) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimDetailInfoSetDto,
            "/claim/getClaimDetailInfoList",
            new ParameterizedTypeReference<ResponseObject<List<ClaimDetailInfoListDto>>>() {}
        ).getData();
    }

    /**
    * 클레임 사유 조회
    *
    * @param claimReasonInfoSetDto 클레임 상세정보 조회 dto
    * @return CancelReasonInfoGetDto 클레임 취소사유
    */
    public ClaimReasonInfoGetDto getClaimReasonInfo(
        ClaimReasonInfoSetDto claimReasonInfoSetDto) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimReasonInfoSetDto,
            "/claim/getClaimReasonInfo",
            new ParameterizedTypeReference<ResponseObject<ClaimReasonInfoGetDto>>() {}
        ).getData();
    }

    /**
    * 클레임 환불예정금액 조회
    *
    * @param claimNo
    * @return ClaimRegisterPreRefundInfoDto
    */
    public ClaimRegisterPreRefundInfoDto getRegisterPreRefund(long claimNo) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            null,
            "/claim/getRegisterPreRefund?claimNo="+claimNo,
            new ParameterizedTypeReference<ResponseObject<ClaimRegisterPreRefundInfoDto>>() {}
        ).getData();
    }

    /**
    * 등록된 이미지 정보 조회
    *
    * @param fileId
    * @return ClaimImageInfoDto
    */
    public ClaimImageInfoDto getClaimImageInfo(String fileId) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.UPLOAD,
            "/provide/info?processKey=ClaimAddImage&fileId="+fileId,
            new ParameterizedTypeReference<ResponseObject<ClaimImageInfoDto>>() {}
        ).getData();
    }


    /**
     * StoreNm(점포명) 조회
     */
    public String getStoreNm(String storeId) {
      return resourceClient.getForResponseObject(
          ResourceRouteName.SHIPPING,
          "/admin/shipManage/getStoreNm?storeId=" + storeId,
          new ParameterizedTypeReference<ResponseObject<String>>() {}).getData();
    }

    /**
     * StoreNm(점포명) 조회
     */
    public StoreInfoGetDto getStoreInfo(String storeId) {
      return resourceClient.getForResponseObject(
          ResourceRouteName.ESCROWMNG,
          "/claim/getStoreInfo?storeId=" + storeId,
          new ParameterizedTypeReference<ResponseObject<StoreInfoGetDto>>() {}).getData();
    }

}
