package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 판매업체 상태 히스토리 Get Entry
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class PartnerStatusHistGetDto {

	private String partnerStatus;

	@RealGridColumnInfo(headText = "회원상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
	private String partnerStatusNm;

	@RealGridColumnInfo(headText = "변경사유", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true)
	private String regReason;

	@RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String regNm;

	@RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
	private String regDt;
}
