package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnalysisToken {
    private String term;
    private String type;
    private int startOffset;
    private int endOffset;
    private int position;
}