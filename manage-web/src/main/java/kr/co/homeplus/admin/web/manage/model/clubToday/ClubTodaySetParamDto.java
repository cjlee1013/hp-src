package kr.co.homeplus.admin.web.manage.model.clubToday;

import java.util.List;
import lombok.Data;

@Data
public class ClubTodaySetParamDto {

    private Integer todayNo;

    private String todayNm;

    private String dispStartDt;

    private String dispEndDt;

    private Integer priority;

    private String todayTitle;

    private String pcImgUrl;

    private String mobileImgUrl;

    private String dispYn;

    private List<ClubTodayItemListSelectDto> itemList;

    private String regId;

}
