package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimePartnerOrderStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.admin.web.statistics.service.RealtimeOrderStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 주문결제통계 > 실시간 판매업체별 판매 현황
 */
@Controller
@RequestMapping("/escrow/statistics")
public class RealtimePartnerOrderStatisticsController {
    private final RealtimeOrderStatisticsService realtimeOrderStatisticsService;
    private final LoginCookieService loginCookieService;

    public RealtimePartnerOrderStatisticsController(RealtimeOrderStatisticsService realtimeOrderStatisticsService,
            LoginCookieService loginCookieService) {
        this.realtimeOrderStatisticsService = realtimeOrderStatisticsService;
        this.loginCookieService = loginCookieService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/realtimePartnerOrderStatisticsMain")
    public String realtimePartnerOrderStatisticsMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("realtimePartnerOrderStatisticsGridBaseInfo", RealtimePartnerOrderStatisticsDto.class));

        return "/statistics/realtimePartnerOrderStatisticsMain";
    }

    /**
     * 실시간 판매업체별 판매현황 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getRealtimePartnerOrderStatisticsList.json")
    public List<RealtimePartnerOrderStatisticsDto> getRealtimePartnerOrderStatisticsList(
            @ModelAttribute StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_PARTNER_ORDER_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        statisticsOrderInfoSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return realtimeOrderStatisticsService.getRealtimePartnerOrderStatisticsList(statisticsOrderInfoSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/saveRealtimePartnerOrderExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_PARTNER_ORDER_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        realtimeOrderStatisticsService.saveExtractHistory(extractCommonSetDto);
    }
}
