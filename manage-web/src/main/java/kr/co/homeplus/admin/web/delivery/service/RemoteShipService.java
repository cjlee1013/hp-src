package kr.co.homeplus.admin.web.delivery.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.RemoteShipManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RemoteShipService {

    private final ResourceClient resourceClient;

    public HashMap<String, Object> getRemoteShipList(RemoteShipManageSelectModel remoteShipManageSelectModel) throws Exception {

        List<SetParameter> setParameterList = new ArrayList<>();

        setParameterList.add(SetParameter.create("schStoreType", remoteShipManageSelectModel.getSchStoreType()));
        setParameterList.add(SetParameter.create("schStoreId", remoteShipManageSelectModel.getSchStoreId()));
        setParameterList.add(SetParameter.create("schZipcode", remoteShipManageSelectModel.getSchZipcode()));
        setParameterList.add(SetParameter.create("schSidoType", remoteShipManageSelectModel.getSchSidoType()));
        setParameterList.add(SetParameter.create("schSigunguType", remoteShipManageSelectModel.getSchSigunguType()));
        setParameterList.add(SetParameter.create("schAddrKeyword", remoteShipManageSelectModel.getSchAddrKeyword()));
        setParameterList.add(SetParameter.create("schBuildNo", remoteShipManageSelectModel.getSchBuildNo()));

        //get 방식 param List 설정 : shipWeekdayList=1&shipWeekdayList=2 ....
        for(String weekday : remoteShipManageSelectModel.getShipWeekdayList()) {
            setParameterList.add(SetParameter.create("shipWeekdayList", weekday));
        }

        String apiUri = EscrowConstants.ESCROW_GET_REMOTE_SHIP_LIST + StringUtil.getParameter(setParameterList);

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<HashMap<String, Object>>>() {};
        HashMap<String, Object> remoteShipMap = (HashMap<String, Object>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri,
            typeReference).getData();

        return remoteShipMap;
    }
}
