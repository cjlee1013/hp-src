package kr.co.homeplus.admin.web.common.exception;

import kr.co.homeplus.admin.web.common.exception.code.MemberSearchReturnCode;

/**
 * 어드민 회원조회 팝업에서 조회시 발생되는 Exception
 */
public class MemberSearchFailException extends RuntimeException {

    private MemberSearchReturnCode returnCode;

    public MemberSearchFailException(MemberSearchReturnCode returnCode, String message) {
        super(message);
        this.returnCode = returnCode;
    }

    public MemberSearchReturnCode getReturnCode() {
        return returnCode;
    }
}
