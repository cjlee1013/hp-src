package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 예약배송관리 리스트 요청 DTO")
public class ShipReserveManageListSetDto {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)")
    private String schStoreType;

    @ApiModelProperty(notes = "점포ID")
    private String schStoreId;

    @ApiModelProperty(notes = "배송상태")
    private String schShipStatus;

    @ApiModelProperty(notes = "배송일자 타입")
    private String schShipDtType;

    @ApiModelProperty(notes = "배송일(Start)")
    private String schShipStartDt;

    @ApiModelProperty(notes = "배송일(End)")
    private String schShipEndDt;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "비회원 주문여부")
    private String schNomemOrderYn;

    @ApiModelProperty(notes = "비회원 조회 타입")
    private String schNomemOrderType;

    @ApiModelProperty(notes = "회원번호")
    private String schUserNo;

    @ApiModelProperty(notes = "비회원 검색어")
    private String schNomemOrderWord;

    @ApiModelProperty(notes = "배송유형")
    private String schShipType;
}
