package kr.co.homeplus.admin.web.manage.model.brandZone;

import java.util.List;
import kr.co.homeplus.admin.web.manage.model.sticker.StickerImgSetDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandZoneSetDto {

    //브랜드존 등록번호
    private Long brandzoneNo;
    //브랜드번호
    private Long brandNo;
    //사이트타입
    private String siteType;
    //우선순위
    private String priority;
    //자동추천여부
    private String recommendYn;
    //사용여부
    private String useYn;
    //pc 로고 이미지 url
    private String pcImgUrl;
    //pc 이미지 가로
    private Integer pcImgWidth;
    //pc 이미지 세로
    private Integer pcImgHeight;
    //모바일 로고 이미지 url
    private String mobileImgUrl;
    //모바일 이미지 가로
    private Integer mobileImgWidth;
    //모바일 이미지 세로
    private Integer mobileImgHeight;
    //등록,수정자
    private String userId;

    //이미지정보
    List<BrandZoneItemSetDto> brandZoneItemList;
}
