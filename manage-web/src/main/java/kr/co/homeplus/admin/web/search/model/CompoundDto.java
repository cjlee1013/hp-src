package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 복합명사 구성 정보")
public class CompoundDto {
    @ApiModelProperty(notes = "복합어번호", required = true, position = 1)
    private Integer compId;
    @ApiModelProperty(notes = "복합어순서", required = true, position = 2)
    private int compOrder;
    @ApiModelProperty(notes = "단어가중치", required = true, position = 3)
    private int compWeight;
    @ApiModelProperty(notes = "사용여부", position = 5, hidden = true)
    private boolean useFlag;
    @ApiModelProperty(notes = "복합어명", position = 6, hidden = true)
    private String compName;
    @ApiModelProperty(notes = "의미태그", position = 7, hidden = true)
    private String senseTag;
    @ApiModelProperty(notes = "사전유형", position = 8, hidden = true)
    private String dicType;
    @ApiModelProperty(notes = "품사코드", position = 9, hidden = true)
    private String pos;
    @ApiModelProperty(notes = "품사명", position = 10, hidden = true)
    private String neType;
    @ApiModelProperty(notes = "분야", position = 11, hidden = true)
    private String domain;
    @ApiModelProperty(notes = "설명", position = 12, hidden = true)
    private String description;
    @ApiModelProperty(notes = "예시", position = 13, hidden = true)
    private String example;
}
