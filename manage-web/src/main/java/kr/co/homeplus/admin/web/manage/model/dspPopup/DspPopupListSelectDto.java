package kr.co.homeplus.admin.web.manage.model.dspPopup;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspPopupListSelectDto {

    @RealGridColumnInfo(headText = "popupNo", hidden = true)
    private Long popupNo;

    @RealGridColumnInfo(headText = "popupType", order = 1, hidden = true)
    private String popupType;

    @RealGridColumnInfo(headText = "locCd", order = 2, hidden = true)
    private String locCd;

    @RealGridColumnInfo(headText = "팝업명", width = 180, order = 3)
    private String popupNm;

    @RealGridColumnInfo(headText = "dispStartDt", order = 4, hidden = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "dispEndDt", order = 5, hidden = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "전시기간", width = 180, order = 6)
    private String periodDate;

    @RealGridColumnInfo(headText = "useYn", order = 7, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 80, order = 8, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "closeBtnPeriod", order = 9, hidden = true)
    private Integer closeBtnPeriod;
    @RealGridColumnInfo(headText = "imgNm", order = 10, hidden = true)
    private String imgNm;
    @RealGridColumnInfo(headText = "imgUrl", order = 11, hidden = true)
    private String imgUrl;
    @RealGridColumnInfo(headText = "imgWidth", order = 12, hidden = true)
    private Integer imgWidth;
    @RealGridColumnInfo(headText = "imgHeight", order = 13, hidden = true)
    private Integer imgHeight;
    @RealGridColumnInfo(headText = "linkType", order = 14, hidden = true)
    private String linkType;
    @RealGridColumnInfo(headText = "linkInfo", order = 15, hidden = true)
    private String linkInfo;
    @RealGridColumnInfo(headText = "linkItmNm", order = 16, hidden = true)
    private String linkItmNm;
    @RealGridColumnInfo(headText = "contentType", order = 17, hidden = true)
    private String contentType;
    @RealGridColumnInfo(headText = "htmlContent", order = 18, hidden = true)
    private String htmlContent;
    @RealGridColumnInfo(headText = "dispTarget", order = 19, hidden = true)
    private String dispTarget;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 20)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 21, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 22)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 23, sortable = true)
    private String chgDt;


    private String regId;
    private String chgId;

}
