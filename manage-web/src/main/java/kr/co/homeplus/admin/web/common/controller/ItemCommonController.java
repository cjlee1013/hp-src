package kr.co.homeplus.admin.web.common.controller;

import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 공통 상품
 */
@Slf4j
@Controller
@RequestMapping(value = "/common/item")
public class ItemCommonController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;

    public ItemCommonController(ResourceClient resourceClient, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.codeService=codeService;
    }
    /**
     * 상품명검색
     * @param itemNo
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemNm.json"}, method = RequestMethod.GET)
    public ResponseResult getItemNm(@RequestParam(name = "itemNo") String itemNo) {
        String apiUri = "/common/item/getItemNm";
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?itemNo=" + itemNo ,  new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }
}
