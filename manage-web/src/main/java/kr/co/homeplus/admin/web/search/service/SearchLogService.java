package kr.co.homeplus.admin.web.search.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SearchLogService {

    private final ResourceClient resourceClient;

    public ResponseObject getSearchLog(String logType, String startDt, String endDt, String siteType, String searchKeyword) {
        String apiUri;
        if (logType.equals("success")) {
            apiUri = "/searchlog/getSuccessSearchLogList";
        } else {
            apiUri = "/searchlog/getFailureSearchLogList";
        }
        apiUri += "?startDt="+startDt+"&endDt="+endDt+"&siteType="+siteType;
        if (StringUtils.isNotEmpty(searchKeyword)) {
            apiUri += "&searchKeyword=" + URLEncoder.encode(searchKeyword, StandardCharsets.UTF_8);
        }
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    public List<String> getFailureRelatedWord(String failureKeyword){
        String apiUri = "/searchlog/getFailureRelatedWord?keyword="+URLEncoder.encode(failureKeyword, StandardCharsets.UTF_8);

        ResponseObject<Map<String,Object>> responseObject = getResponseObject(apiUri);
        return (List<String>)responseObject.getData().get("relationKeywordList");
    }
}
