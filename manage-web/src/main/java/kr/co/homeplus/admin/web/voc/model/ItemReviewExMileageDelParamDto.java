package kr.co.homeplus.admin.web.voc.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemReviewExMileageDelParamDto {

    private String[] arrSeq;
    private String regId;

}
