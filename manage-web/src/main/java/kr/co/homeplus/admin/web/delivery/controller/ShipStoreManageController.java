package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreManageListSetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipStoreManageService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송관리 > 점포배송관리
 */
@Controller
@RequestMapping("/ship")
public class ShipStoreManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    private final ShipStoreManageService shipStoreManageService;
    private final ShipCommonService shipCommonService;
    private final AuthorityService authorityService;

    public ShipStoreManageController(ShipStoreManageService shipStoreManageService
        , ShipCommonService shipCommonService
        , AuthorityService authorityService) {
        this.shipStoreManageService = shipStoreManageService;
        this.shipCommonService = shipCommonService;
        this.authorityService = authorityService;
    }

    /**
     * 점포배송관리 Main Page
     */
    @GetMapping("/shipManage/shipStoreManageMain")
    public String shipStoreManageMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("packageExcel", authorityService.hasRole("package_excel"));
        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);

        model.addAllAttributes(RealGridHelper.create("shipStoreManageGridBaseInfo", ShipStoreManageListGetDto.class));

        return "/delivery/shipStoreManageMain";
    }

    /**
     * 점포배송관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipStoreManageList.json", method = RequestMethod.GET)
    public List<ShipStoreManageListGetDto> getShipStoreManageList(ShipStoreManageListSetDto shipStoreManageListSetDto) {
        return shipStoreManageService.getShipStoreManageList(shipStoreManageListSetDto);
    }

    /**
     * 배송Shift 리스트 조회
     */
    @ResponseBody
    @GetMapping("/shipManage/getStoreShiftList.json")
    public List<String> getStoreShiftList(@RequestParam(value = "storeId") String storeId
        ,@RequestParam(value = "storeType") String storeType) {
        return shipCommonService.getStoreShiftList(storeId, storeType);
    }
}
