package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.HYPER;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreLockerModel;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreManageModel;
import kr.co.homeplus.admin.web.delivery.model.PickupStoreManageSelectModel;
import kr.co.homeplus.admin.web.delivery.service.PickupStoreManageService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 배송관리 > 점포배송정보 > 픽업점포관리
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class PickupStoreManageController {
    private final AuthorityService authorityService;
    private final CodeService codeService;
    private final PickupStoreManageService pickupStoreManageService;

    public PickupStoreManageController(AuthorityService authorityService, CodeService codeService, PickupStoreManageService pickupStoreManageService) {
        this.authorityService = authorityService;
        this.codeService = codeService;
        this.pickupStoreManageService = pickupStoreManageService;
    }

    /**
     * 픽업점포관리 메인페이지 호출
     */
    @GetMapping("/pickupStoreManageMain")
    public String pickupStoreManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");
        List<MngCodeGetDto> storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> HYPER.getCode()
//                        [ESCROW-343, CLUB 노출제외]
//                        .equals(st.getMcCd()) || CLUB.getCode()
                        .equals(st.getMcCd()))
                .collect(Collectors.toList());
        model.addAttribute("storeType", storeTypeList);
        model.addAllAttributes(RealGridHelper.create("pickupStoreManageGridBaseInfo", PickupStoreManageModel.class));
        return "/delivery/pickupStoreManageMain";
    }

    /**
     * 픽업점포관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getPickupStoreManageList.json")
    public List<PickupStoreManageModel> getPickupStoreManageList(@ModelAttribute PickupStoreManageSelectModel pickupStoreManageSelectModel) throws Exception {
        return pickupStoreManageService.getPickupStoreManageList(pickupStoreManageSelectModel);
    }

    /**
     * 픽업점포 락커 리스트 조회
     */
    @ResponseBody
    @GetMapping("/getPickupStoreLockerList.json")
    public List<PickupStoreLockerModel> getPickupStoreLockerList(@RequestParam(value = "storeId") String storeId) throws Exception {
        PickupStoreLockerModel pickupStoreLockerModel = new PickupStoreLockerModel();
        pickupStoreLockerModel.setStoreId(storeId);
        return pickupStoreManageService.getPickupStoreLockerList(pickupStoreLockerModel);
    }
}

