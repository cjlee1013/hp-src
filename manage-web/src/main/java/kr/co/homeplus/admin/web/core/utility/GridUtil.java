package kr.co.homeplus.admin.web.core.utility;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 그리드 유틸
 */
public class GridUtil {
	private static final String TREE_ROOT       		= "1";          // 트리 root dtpeh
	private static final String TREE_KEY        		= "id";         // dhtmlx tree grid 트리 key
	private static final String TREE_CHILD_KEY  		= "item";       // dhtmlx tree 트리 child key
	private static final String TREE_ALL_CHECKED_NM  	= "전체";   		// dhtmlx tree 전체 전체 이름

	/**
	 * 그리드 생성
	 * @param gridName      그리드 이름
	 * @param headers       헤더세팅 정보
	 * @param events        추가할 이벤트 정보
	 * @param options       추가할 옵션 정보
	 * @return
	 * @throws Exception
	 */
	public static String makeGrid(final String gridName,
								  final List headers,
								  final List<String> events,
								  final List<String> options) throws Exception {
		return GridUtil.dhtmlxMakeGrid(gridName, headers, events, options);
	}

	public static String makeGrid(final String gridName,
								  final List headers,
								  final List<String> events) throws Exception {
		return GridUtil.dhtmlxMakeGrid(gridName, (List<DhtmlxGridHeader>) headers, events, Arrays.asList());
	}

	public static String makeGrid(final String gridName,
								  final List headers) throws Exception {
		return GridUtil.dhtmlxMakeGrid(gridName, headers, Arrays.asList(), Arrays.asList());
	}

	/**
	 * 트리 생성
	 * @param treeName		트리 이름
	 * @param loadUrl		트리에 사용될 데이터 URL
	 * @param tsChecked		three state checked 사용 여부 (상위선택시 하위모든 node 선택 기능)
	 * @return
	 * @throws Exception
	 */
	public static String makeTree(final String treeName,
								  final String loadUrl,
								  final boolean tsChecked) throws Exception {
		return GridUtil.dhtmlxMakeTree(treeName, loadUrl, tsChecked);
	}


	/**
	 * 그리드 > 데이터 변환
	 * @param data          데이터
	 * @param rowCount      데이터 개수
	 * @return
	 */
	public static Map<String, Object> convertGridData(final Object data,
													  final int rowCount) throws Exception {
		return GridUtil.dhtmlxConvertGridData(data, rowCount);
	}

	/**
	 * 트리 그리드 > 데이터 변환
	 * @param dataList      트리 데이터
	 * @param treeName      트리로 표현될 컬럼 이름
	 * @param treeKey       트리 아이디 컬럼 이름
	 * @param parentKey     트리 부모 아이디 컬럼 이름
	 * @param depthKey      트리 뎁스 컬럼이름
	 * @return
	 * @throws Exception
	 */
	public static List<Object> convertTreeGridData(final List<Map<String, Object>> dataList,
												   final String treeName,
												   final String treeKey,
												   final String parentKey,
												   final String depthKey) throws Exception {
		return GridUtil.convertTreeGridDataMaker(dataList, treeName, treeKey, parentKey, depthKey);
	}

	/**
	 * 트리 데이터 변환
	 * @param dataList		트리 데이터
	 * @param treeKey		트리 기준 키
	 * @param parentKey		부모키
	 * @param depthKey		뎁스 키
	 * @param itemNm		트리에 표시될 컬럼 이름
	 * @return
	 * @throws Exception
	 */
	public static String convertTreeData(final List<Map<String, Object>> dataList,
										 final String treeKey,
										 final String parentKey,
										 final String depthKey,
										 final String itemNm,
										 final boolean allChecked,
										 final String allCheckedNm) throws Exception {
		return GridUtil.convertTreeDataMaker(dataList, treeKey, parentKey, depthKey, itemNm, allChecked, allCheckedNm);
	}

	private static String dhtmlxMakeGrid(final String gridName,
										 final List<DhtmlxGridHeader> headers,
										 final List<String> events,
										 final List<String> options) throws Exception {
		List<String> headerList = new ArrayList();
		List<String> values     = new ArrayList();
		List<String> colNames   = new ArrayList();
		List<String> widths     = new ArrayList();
		List<String> types      = new ArrayList();
		List<String> aligns     = new ArrayList();
		List<String> sorts      = new ArrayList();
		List<Boolean> hiddens   = new ArrayList();
		List<Boolean> decimal   = new ArrayList();

		for (DhtmlxGridHeader header : headers) {
			values.add(header.getValue());
			colNames.add(header.getColName());
			widths.add(header.getWidth());
			types.add(header.getType());
			aligns.add(header.getAlign());
			sorts.add(header.getSort());
			hiddens.add(header.isHidden());
			decimal.add(header.isDecimal());
		}

		headerList.add(GridUtil.getAddHeadSetStyle("setHeader", values));
		headerList.add(GridUtil.getAddHead("setColumnIds", colNames));
		headerList.add(GridUtil.getAddHead("setInitWidths", widths));
		headerList.add(GridUtil.getAddHead("setColTypes", types));
		headerList.add(GridUtil.getAddHead("setColAlign", aligns));
		headerList.add(GridUtil.getAddHead("setColSorting", sorts));
		headerList.addAll(GridUtil.getAddHeadHidden("setColumnHidden", hiddens));
		headerList.addAll(GridUtil.getAddHeadNumberFormat("setNumberFormat", decimal, types));

		return new DhtmlxGridBaseObject(gridName, headerList, events, options).toString();
	}

	private static String getAddHead(final String func,
									 final List<String> data) {
		StringBuffer addHead = new StringBuffer();
		if (!data.isEmpty()) {
			addHead.append(func).append("(\"").append(data.stream().collect(Collectors.joining(","))).append("\");");
		}
		return addHead.toString();
	}

	private static String getAddHeadSetStyle(final String func,
											 final List<String> data) {
		StringBuffer addHead = new StringBuffer();
		if (!data.isEmpty()) {
			List<String> styleList = new ArrayList<>();
			for (String str : data) {
				styleList.add("'text-align:center;font-weight:bold;'");
			}

			addHead.append(func).append("(\"").append(data.stream().collect(Collectors.joining(","))).append("\", null, [").append(styleList.stream().collect(Collectors.joining(","))).append("]);");
		}
		return addHead.toString();
	}



	private static List<String> getAddHeadHidden(final String func,
												 final List<Boolean> hiddenList) {
		List<String> hiddens = new ArrayList();
		StringBuffer addHead;

		if (!hiddenList.isEmpty()) {
			int i = 0;

			for (boolean isHidden : hiddenList) {
				if (isHidden) {
					addHead = new StringBuffer();
					addHead.append(func).append("(").append(i).append(",").append("true").append(");");

					hiddens.add(addHead.toString());
				}
				i++;
			}
		}
		return hiddens;
	}

	private static List<String> getAddHeadNumberFormat(final String func,
													   final List<Boolean> decimalList,
													   final List<String> typesList) {
		List<String> decimals = new ArrayList();
		StringBuffer addHead;

		if (!decimalList.isEmpty()) {
			int i = 0;

			for (String type : typesList) {
				if (type.equals("ron")) {
//					for (boolean decimal : decimalList) {
                        addHead = new StringBuffer();
						if(decimalList.get(i)) {
							addHead.append(func).append("('0,000.00',").append(i).append(");");
						}
						else {
							addHead.append(func).append("('0,000',").append(i).append(");");
						}

                        decimals.add(addHead.toString());
//					}
				}

				i++;
			}
		}
		return decimals;
	}

	/**
	 * 트리 생성 스크립트
	 * @param treeName		트리 이름
	 * @param loadUrl		트리에 사용될 데이터 URL
	 * @param tsChecked		three state checked 사용 여부 (상위선택시 하위모든 node 선택 기능)
	 * @return
	 * @throws Exception
	 */
	private static String dhtmlxMakeTree(final String treeName,
										 final String loadUrl,
										 final boolean tsChecked) throws Exception {
		StringBuffer returnStr = new StringBuffer();
		returnStr.append("var " + treeName + ";\n");
		returnStr.append("function " + treeName + "Load() { \n");
		returnStr.append(treeName + " = new dhtmlXTreeObject('" + treeName + "','100%','100%',0);  \n");
		returnStr.append(treeName + ".setImagePath('/static/jslib/dhtmlx5/skins/web/imgs/dhxtree_web/');  \n");
		returnStr.append(treeName + ".enableCheckBoxes(true); \n");
		returnStr.append(treeName + ".enableThreeStateCheckboxes("+ tsChecked +"); \n");
		if(!loadUrl.equals("")) returnStr.append(treeName + ".load('"+ loadUrl +"','json'); \n");
		returnStr.append("} \n");

		return returnStr.toString();
	}

	private static Map<String, Object> dhtmlxConvertGridData(final Object data,
															 final int rowCount) throws Exception {
		Map<String, Object> dhtmlxGridData = new HashMap();
		dhtmlxGridData.put("data", data);
		dhtmlxGridData.put("rowCount", rowCount);
		return dhtmlxGridData;
	}

	private static List<Object> convertTreeGridDataMaker(final List<Map<String, Object>> dataList,
														 final String treeName,
														 final String treeKey,
														 final String parentKey,
														 final String depthKey) throws Exception {
		List<Object> treeList   = new ArrayList();
		List<String> depth1List = new ArrayList();
		DhtmlxTreeNode node     = new DhtmlxTreeNode();
		String parentId         = "";

		if (!dataList.isEmpty()) {
			for (Map<String, Object> dataMap : dataList) {
				dataMap.put(TREE_KEY, dataMap.get(treeKey));

				if (dataMap.get(parentKey) != null)
					parentId = dataMap.get(parentKey).toString();

				if (dataMap.get(depthKey).equals(TREE_ROOT))
					depth1List.add(dataMap.get(treeKey).toString());

				node.addNode(dataMap, treeName, dataMap.get(treeKey).toString(), parentId);
			}
		}

		treeList.addAll(depth1List.stream().map(depth1Id -> node.getNodes().get(depth1Id)).collect(Collectors.toList()));

		return treeList;
	}

	/**
	 * 트리데이터 변환
	 * @param dataList
	 * @param treeKey
	 * @param parentKey
	 * @param depthKey
	 * @param itemNm
	 * @return
	 * @throws Exception
	 */
	private static String convertTreeDataMaker(final List<Map<String, Object>> dataList,
											   final String treeKey,
											   final String parentKey,
											   final String depthKey,
											   final String itemNm,
											   final boolean allChecked,
											   String allCheckedNm) throws Exception {
		List<Object> treeList   = new ArrayList();
		List<String> depth1List = new ArrayList();
		DhtmlxTreeNode node     = new DhtmlxTreeNode();
		String parentId         = "";

		if (!dataList.isEmpty()) {
			for (Map<String, Object> dataMap : dataList) {
				dataMap.put(TREE_KEY, dataMap.get(treeKey));

				if (dataMap.get(parentKey) != null) {
					parentId = dataMap.get(parentKey).toString();
				}

				if (dataMap.get(depthKey).equals(TREE_ROOT)) {
					depth1List.add(dataMap.get(treeKey).toString());
				}

				node.addNodeTree(dataMap, dataMap.get(treeKey).toString(), parentId, itemNm);
			}
		}

		treeList.addAll(depth1List.stream().map(depth1Id -> node.getTreeNodes().get(depth1Id)).collect(Collectors.toList()));

		ObjectMapper om = new ObjectMapper();
		StringBuffer jsonStr = new StringBuffer();

        //dhtmlx tree 에 맞는 json 형태로 가공
		jsonStr.append("{\""+ TREE_KEY +"\":\"0\", \""+ TREE_CHILD_KEY +"\":");

        if(allChecked) {
            if(allCheckedNm == null || allCheckedNm == "") {
                allCheckedNm = TREE_ALL_CHECKED_NM;
            }

            jsonStr.append("[{\"" + TREE_KEY + "\":\"All\", \"text\":\""+ allCheckedNm +"\", \"open\":\"1\", \"" + TREE_CHILD_KEY + "\":");
        }

        jsonStr.append(om.writeValueAsString(treeList));

        if(allChecked) {
            jsonStr.append("}]");
        }

	    jsonStr.append("}");

		return jsonStr.toString();
	}

}
