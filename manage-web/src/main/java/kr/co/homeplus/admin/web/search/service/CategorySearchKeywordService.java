package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.CategorySearchKeywordSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategorySearchKeywordService {

    private final ResourceClient resourceClient;

    public ResponseObject getCategorySearchKeyword(String keyword, String searchType, String schUseYn, String schResetYn) {
        String apiUri = "/categorySearchKeyword/getList?keyword="+keyword+"&searchType="+searchType+"&schUseYn="+schUseYn+"&schResetYn="+schResetYn;
        return getResponseObject(apiUri);
    }
    public ResponseObject putCategorySearchKeyword(CategorySearchKeywordSetDto param) {
        String apiUri = "";
        if (param.getDcateSearchKeywordNo() == null) {
            apiUri = "/categorySearchKeyword/insert";
        } else {
            apiUri = "/categorySearchKeyword/update";
        }
        return getResponseObject(apiUri, param);
    }
    public ResponseObject confirmCategorySearchKeyword(CategorySearchKeywordSetDto param) {
        String apiUri = "/categorySearchKeyword/confirm";
        return getResponseObject(apiUri, param);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }
    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
