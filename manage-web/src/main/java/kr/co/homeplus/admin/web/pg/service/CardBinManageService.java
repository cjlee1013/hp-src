package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.CardBinManageDto;
import kr.co.homeplus.admin.web.pg.model.CardBinManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CardBinManageService {

    private static final String PARENT_METHOD_CD_CARD = "CARD";

    private final ResourceClient resourceClient;

    /**
     * 신용카드 결제수단을 조회
     * Site_type중 중복된 신용카드 결제수단을 제거한 후 반환
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getDistinctMethodCd() throws Exception {

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("parentMethodCd",PARENT_METHOD_CD_CARD));

        String apiUri = EscrowConstants.ESCROW_GET_DISTINCT_METHOD_CD + StringUtil.getParameter(setParameterList);

        ResponseObject<List<PaymentMethodDto>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri,
            new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {}
        );
        return responseObject.getData();
    }

    /**
     * 카드BIN 리스트 조회
     * @param cardBinManageSelectDto
     * @return
     * @throws Exception
     */
    public List<CardBinManageDto> getCardBinManageList(CardBinManageSelectDto cardBinManageSelectDto) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_CARD_BIN_MANAGE,CardBinManageSelectDto.class,cardBinManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CardBinManageDto>>>() {};
        return (List<CardBinManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri ,  typeReference).getData();

    }

    /**
     * 카드BIN 저장/수정
     * @param cardBinManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveCardBinManage(CardBinManageDto cardBinManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_CARD_BIN_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, cardBinManageDto, apiUri, typeReference);
    }

}
