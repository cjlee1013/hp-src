package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.item.model.issuePrice.IssuePriceListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/issuePrice")
public class IssuePriceController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;

    public IssuePriceController(ResourceClient resourceClient, LoginCookieService cookieService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 점포상품관리 > 이상매가조회 > Main
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/issuePriceMain", method = RequestMethod.GET)
    public String issuePriceMain(Model model) throws Exception{

        // 코드정보
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");    // 점포유형

        model.addAttribute("storeType", code.get("store_type"));

        // 그리드
        model.addAllAttributes(RealGridHelper.create("issuePriceGridBaseInfo", IssuePriceListGetDto.class));

        return "/item/issuePriceMain";
    }

    @ResponseBody
    @GetMapping(value = {"/getIssuePriceList.json"})
    public List<IssuePriceListGetDto> getIssuePriceList(@RequestParam(value = "schStoreType") String schStoreType) throws Exception {
        String apiUri = "/item/issuePrice/getIssuePriceList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<IssuePriceListGetDto>>>() {};

        return (List<IssuePriceListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?schStoreType=" + schStoreType , typeReference).getData();
    }
}

