package kr.co.homeplus.admin.web.partner.model.sellerShop;

import lombok.Data;

@Data
public class SellerShopListParamDto {

    private String searchStartDt;
    private String searchEndDt;
    private String searchKeyword;
    private String searchUseYn;
    private String searchType;

}
