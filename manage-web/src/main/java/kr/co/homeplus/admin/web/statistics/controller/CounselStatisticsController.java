package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.statistics.model.CounselStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CounselStatisticsSetDto;
import kr.co.homeplus.admin.web.statistics.service.CounselStatisticService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics")
public class CounselStatisticsController {

    @Autowired
    private CounselStatisticService counselStatisticService;
    @Autowired
    private ShipCommonService shipCommonService;

    /**
     * 상담통계 Main Page
     */
    @RequestMapping(value = "/counselStatisticsMain", method = RequestMethod.GET)
    public String counselStatisticsMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.createForGroup("counselStatisticsGridBaseInfo", CounselStatisticsGetDto.class));

        model.addAttribute("cs_h_channel", shipCommonService.getCode("cs_h_channel"));
        model.addAttribute("cs_advisor_type_1", shipCommonService.getCode("cs_advisor_type_1"));
        model.addAttribute("cs_channel_cd", shipCommonService.getCode("cs_channel_cd"));

        return "/statistics/counselStatisticsMain";
    }

    /**
     * 상담통계 메인 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getCounselStatisticsList.json", method = RequestMethod.GET)
    public List<CounselStatisticsGetDto> getCounselStatisticsList(CounselStatisticsSetDto counselStatisticsSetDto) {
        return counselStatisticService.getCounselStatisticsList(counselStatisticsSetDto);
    }
}