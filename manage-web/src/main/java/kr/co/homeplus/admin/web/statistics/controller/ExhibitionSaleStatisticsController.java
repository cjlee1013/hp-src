package kr.co.homeplus.admin.web.statistics.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsExhDto;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsItemDto;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsSelectDto;
import kr.co.homeplus.admin.web.statistics.service.ExhibitionSaleStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 상품 주문통계 > 기획전별 판매현황
 */
@Controller
@RequestMapping("/statistics")
public class ExhibitionSaleStatisticsController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final ExhibitionSaleStatisticsService exhibitionSaleStatisticsService;

    public ExhibitionSaleStatisticsController(LoginCookieService loginCookieService, AuthorityService authorityService, ExhibitionSaleStatisticsService exhibitionSaleStatisticsService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.exhibitionSaleStatisticsService = exhibitionSaleStatisticsService;
    }

    @GetMapping("/exhibitionSaleStatisticsMain")
    public String exhibitionSaleStatisticsMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("exhibitionSaleStatisticsExhBaseInfo", ExhibitionSaleStatisticsExhDto.class));
        model.addAllAttributes(RealGridHelper.create("exhibitionSaleStatisticsItemBaseInfo", ExhibitionSaleStatisticsItemDto.class));
        return "/statistics/exhibitionSaleStatisticsMain";
    }

    /**
     * 기획전별 판매현황 > 기획전 조회
     */
    @ResponseBody
    @GetMapping("/getExhibitionSaleStatisticsExh.json")
    public List<ExhibitionSaleStatisticsExhDto> getExhibitionSaleStatisticsExh(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) throws Exception {
        return exhibitionSaleStatisticsService.getExhibitionSaleStatisticsExh(exhibitionSaleStatisticsSelectDto);
    }

    /**
     * 기획전별 판매현황 > 기획전내 상품 조회
     */
    @ResponseBody
    @PostMapping("/getExhibitionSaleStatisticsItem.json")
    public List<ExhibitionSaleStatisticsItemDto> getExhibitionSaleStatisticsItem(@RequestBody ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) throws Exception {
        return exhibitionSaleStatisticsService.getExhibitionSaleStatisticsItem(exhibitionSaleStatisticsSelectDto);
    }
}
