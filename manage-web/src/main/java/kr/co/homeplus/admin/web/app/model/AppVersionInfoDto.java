package kr.co.homeplus.admin.web.app.model;

import kr.co.homeplus.admin.web.voc.enums.SiteType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.mobile.device.DevicePlatform;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class AppVersionInfoDto {

    @RealGridColumnInfo(headText = "앱관리번호", hidden = true)
    private int seq;

    @RealGridColumnInfo(headText = "앱구분", columnType = RealGridColumnType.LOOKUP)
    private SiteType siteType;
    @RealGridColumnInfo(headText = "OS구분")
    private DevicePlatform devicePlatform;
    @RealGridColumnInfo(headText = "버전")
    private String version;

    @RealGridColumnInfo(headText = "강제업데이트 여부")
    private String forceUpdate;
    @RealGridColumnInfo(headText = "업데이트추천 여부")
    private String updateNotification;
    @RealGridColumnInfo(headText = "버전업데이트일")
    private String updateApplyDt;
    @RealGridColumnInfo(headText = "사용여부")
    private Boolean isUsed;
    @RealGridColumnInfo(headText = "등록일")
    private String regDt;
    @RealGridColumnInfo(headText = "등록자")
    private String regEmpNm;

    // grid에서 사용하지 않는 항목
    private String memo;
    private Boolean isUsedHandler; //사용여부 항목 화면 제어 구분값

}
