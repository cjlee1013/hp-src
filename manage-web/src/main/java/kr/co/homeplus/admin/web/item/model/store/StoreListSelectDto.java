package kr.co.homeplus.admin.web.item.model.store;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoreListSelectDto {

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", columnType= RealGridColumnType.NUMBER_CENTER, width = 80, sortable = true)
    private String storeId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 80, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "점포명(영문)")
    @RealGridColumnInfo(headText = "점포명(영문)", width =80)
    private String storeNm2;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 0, hidden = true)
    private String storeType;

    @ApiModelProperty(value = "점포유형명")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeTypeNm;

    @ApiModelProperty(value = "ECOM CC")
    @RealGridColumnInfo(headText = "ECOM CC", width = 80, sortable = true)
    private String onlineCostCenter;

    @ApiModelProperty(value = "점포 CC")
    @RealGridColumnInfo(headText = "점포 CC", width = 80, sortable = true)
    private String storeCostCenter;

    @ApiModelProperty(value = "소재지역")
    @RealGridColumnInfo(headText = "소재지역", width = 0, hidden = true)
    private String region;

    @ApiModelProperty(value = "소재지역")
    @RealGridColumnInfo(headText = "소재지역", width = 80, sortable = true)
    private String regionNm;

    @ApiModelProperty(value = "점포구분")
    @RealGridColumnInfo(headText = "점포구분", width = 80, hidden = true)
    private String storeKind;

    @ApiModelProperty(value = "점포구분")
    @RealGridColumnInfo(headText = "점포구분", width = 80, sortable = true)
    private String storeKindNm;

    @ApiModelProperty(value = "Fulfillment Center")
    @RealGridColumnInfo(headText = "Fulfillment Center", width = 100, sortable = true)
    private Integer fcStoreId;

    @ApiModelProperty(value = "Fulfillment Center")
    @RealGridColumnInfo(headText = "Fulfillment Center", width = 0, hidden = true)
    private String fcStoreNm;

    @ApiModelProperty(value = "기준점포")
    @RealGridColumnInfo(headText = "기준점포", width = 80)
    private Integer originStoreId;

    @ApiModelProperty(value = "기준점포")
    @RealGridColumnInfo(headText = "기준점포", width = 0, hidden = true)
    private String originStoreNm;

    @ApiModelProperty(value = "구븐")
    @RealGridColumnInfo(headText = "구분", width = 0, hidden = true)
    private String storeAttr;

    @ApiModelProperty(value = "구븐")
    @RealGridColumnInfo(headText = "구분", width = 100)
    private String storeAttrNm;

    @ApiModelProperty(value = "픽업서비스")
    @RealGridColumnInfo(headText = "픽업서비스", width = 0, hidden = true)
    private String pickupYn;

    @ApiModelProperty(value = "픽업서비스")
    @RealGridColumnInfo(headText = "픽업서비스", width = 80)
    private String pickupYnNm;

    @ApiModelProperty(value = "휴무안내")
    @RealGridColumnInfo(headText = "휴무안내", width = 120)
    private String closedInfo;

    @ApiModelProperty(value = "온라인여부")
    @RealGridColumnInfo(headText = "온라인여부", width = 0, hidden = true)
    private String onlineYn;

    @ApiModelProperty(value = "온라인여부")
    @RealGridColumnInfo(headText = "온라인여부", width = 80)
    private String onlineYnNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 70)
    private String useYnNm;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regId;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgId;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;

    @ApiModelProperty(value = "대표자명")
    @RealGridColumnInfo(headText = "대표자명", width = 0, hidden = true)
    private String mgrNm;

    @ApiModelProperty(value = "우편번호")
    @RealGridColumnInfo(headText = "우편번호", width = 0, hidden = true)
    private String zipcode;

    @ApiModelProperty(value = "주소1")
    @RealGridColumnInfo(headText = "주소1", width = 0, hidden = true)
    private String addr1;

    @ApiModelProperty(value = "주소2")
    @RealGridColumnInfo(headText = "주소2", width = 0, hidden = true)
    private String addr2;

    @ApiModelProperty(value = "전화번호")
    @RealGridColumnInfo(headText = "전화번호", width = 0, hidden = true)
    private String phone;

    @ApiModelProperty(value = "팩스")
    @RealGridColumnInfo(headText = "팩스", width = 0, hidden = true)
    private String fax;

    @ApiModelProperty(value = "운영시간")
    @RealGridColumnInfo(headText = "운영시간", width = 0, hidden = true)
    private String oprTime;


}
