package kr.co.homeplus.admin.web.core.controller;

import io.swagger.annotations.ApiParam;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.admin.web.core.certification.CertificationService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.config.AdminProperties;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.AdminCertUserParam;
import kr.co.homeplus.admin.web.core.dto.AdminCertUserResponse;
import kr.co.homeplus.admin.web.core.dto.AdminUpdatePwParam;
import kr.co.homeplus.admin.web.core.dto.LoginInitInfo;
import kr.co.homeplus.admin.web.core.dto.LoginParam;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 로그인 처리를 위한 컨트롤러
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class LoginController {

    private final CertificationService certificationService;
    private final LoginCookieService loginCookieService;
    private final UserMenuService userMenuService;
    private final AdminProperties adminProperties;
    private final ResourceClient resourceClient;

    /**
     * 로그인 화면으로 이동합니다.
     * @return 로그인 화면 이동
     */
    @CertificationNeedLess
    @GetMapping(value = "/login")
    public String webServiceLoginStartNoFrame(final Model model) {
        final LoginInitInfo info = loginCookieService.getLoginInitInfo();

        model.addAttribute("saveUserId", info.getSaveUserId());
        model.addAttribute("chkSaveUserId", info.isChkSaveUserId());
        return "/login/login";
    }

    /**
     * 로그인 인증 처리를 수행합니다.
     * @param request {@link HttpServletRequest}
     * @param loginParam 로그인 파라미터
     * @param bindingResult {@link BindingResult}
     * @return 로그인 인증이 정상으로 처리될 경우 startUrl 을 리턴합니다.
     */
    @ResponseBody
    @CertificationNeedLess
    @PostMapping(value = "/login.json")
    public ResponseObject<String> loginProcess(final HttpServletRequest request,
        @Valid final LoginParam loginParam, final BindingResult bindingResult) {

        certificationService.getUserCertificationService(request, loginParam, bindingResult);

        final String goUrl = adminProperties.getStartUrl();

        return ResponseObject.Builder.<String>builder()
            .status(ResponseObject.STATUS_SUCCESS)
            .data(goUrl)
            .returnCode(ResponseObject.RETURN_CODE_SUCCESS)
            .build();
    }

    /**
     * 로그아웃 처리 후 로그아웃 페이지로 이동합니다.
     * @param model {@link Model}
     * @return 로그아웃 처리 후 로그아웃 페이지로 이동합니다.
     */
    @GetMapping(value = "/logout")
    public String webServiceLogoutAction(final Model model) {
        //메뉴 캐시 제거
        userMenuService.removeAuthorizedMenusCache(loginCookieService.getUserInfo().getEmpId());
        //로그인 쿠키 제거
        loginCookieService.removeLoginCookie();

        model.addAttribute("certificationRedirectParam",
            adminProperties.getCertificationRedirectUrl());

        // 로그아웃 페이지로 이동
        return "/login/logout";
    }

    /**
     * 로그인 팝업을 여는 컨트롤러<p>
     * <p>
     * 로그인 쿠키 인증 실패 시 노출됩니다.
     *
     * @return loginPop
     */
    @CertificationNeedLess
    @GetMapping(value = "/login/popup")
    public String webServiceLoginStartNoFramePopup(final Model model,
        final HttpServletRequest request) {

        model.addAttribute("returnUrl", adminProperties.getCertificationRedirectUrl());
        return "/login/loginPop";
    }

    /**
     * 계정확인 팝업
     *
     * @return 계정확인 팝업 이동
     */
    @CertificationNeedLess
    @GetMapping(value = "/login/userCertificatePopup")
    public String loginUserCertificatePopup() {
        return "/login/userCertificatePop";
    }

    /**
     * 계정확인 API 호출
     *
     * @param param 계정확인 파라미터
     * @return 계정확인 결과
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/admin/user/certificate.json")
    public ResponseObject<AdminCertUserResponse> adminCertificateUser(
        @RequestBody @Valid final AdminCertUserParam param) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.AUTHORITY, param, "/auth/admin/user/certificate",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 비밀번호 변경 팝업
     *
     * @return 비밀번호 변경 팝업 이동
     */
    @CertificationNeedLess
    @GetMapping(value = "/login/userUpdatePasswordPopup")
    public String userUpdatePasswordPopup(final Model model,
        @RequestParam @ApiParam("사번") final String empId,
        @RequestParam @ApiParam("로그인id") final String userId,
        @RequestParam(required = false, defaultValue = "") @ApiParam("로그인 유형") final String loginType) {
        log.info("userUpdatePasswordPopup! <empId:{}>,<userId:{}>,<messageId:{}>", empId, userId,
            loginType);

        model.addAttribute("empId", empId);
        model.addAttribute("userId", userId);
        model.addAttribute("loginType", loginType);
        return "/login/userUpdatePasswordPop";
    }

    /**
     * 비밀번호 변경 API 호출
     *
     * @param param 비밀번호 변경 파라미터
     * @return 비밀번호 변경 결과
     */
    @CertificationNeedLess
    @ResponseBody
    @PostMapping(value = "/admin/user/update/password.json")
    public ResponseObject<Boolean> adminUpdateUserPw(
        @RequestBody @Valid final AdminUpdatePwParam param) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.AUTHORITY, param, "/auth/admin/user/update/password",
            new ParameterizedTypeReference<>() {
            });
    }
}