package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ItemPopupListGetDto {
	@RealGridColumnInfo(headText = "상품번호", sortable = true)
	private String itemNo;
	@RealGridColumnInfo(headText = "상품명", sortable = true)
	private String itemNm1;
	@RealGridColumnInfo(headText = "parent ID", sortable = true)
	private String itemParent;
	@RealGridColumnInfo(headText = "판매업체명", sortable = true)
	private String businessNm;
	@RealGridColumnInfo(headText = "판매업체ID", sortable = true)
	private String partnerId;
	@RealGridColumnInfo(headText = "대분류", sortable = true)
	private String lcateNm;
	@RealGridColumnInfo(headText = "중분류", sortable = true)
	private String mcateNm;
	@RealGridColumnInfo(headText = "소분류", sortable = true)
	private String scateNm;
	@RealGridColumnInfo(headText = "세분류", sortable = true)
	private String dcateNm;
	@RealGridColumnInfo(headText = "등록자", sortable = true)
	private String regNm;
	@RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
	private String regDt;
}
