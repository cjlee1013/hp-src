package kr.co.homeplus.admin.web.core.usermenu;

import java.util.List;
import kr.co.homeplus.admin.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.admin.web.core.dto.AuthorizedMenus;
import kr.co.homeplus.admin.web.core.dto.UserInfo;

public interface UserMenuService {

    @Deprecated
    List<AuthMenuListDto> getUserPermitMenuList(UserInfo userInfo);

    @Deprecated
    List<AuthMenuListDto> getUserPermitMenuList(String userCd, String systemScope);

    /**
     * 어드민 접근메뉴 조회
     * @param empId 사번
     * @return
     */
    List<AuthorizedMenus> getAuthorizedMenus(String empId);

    /**
     * 접근 메뉴에 권한이 있는지 조회
     * @param empId 사번
     * @param requestUri request URI
     * @return
     */
    boolean isNotPermissionMenu(String empId, String requestUri);

    /**
     * 어드민 메뉴 1차캐시 제거
     * @param empId 사번
     */
    void removeAuthorizedMenusCache(String empId);

    /**
     * 상세 탭페이지 URI목록 조회<br>
     * 해당 메소드는 임시로 LNB 에 탭 페이지 노출을 막기위해 생성 된 메소드 입니다.
     * <pre>
     *     - 회원정보상세
     *     - 주문정보상세
     *     - 클레임정보상세
     *     - 판매자정산정보
     * </pre>
     * @return 상세 탭페이지 URI목록 조회
     */
    List<String> getDetailTabPageList();
}
