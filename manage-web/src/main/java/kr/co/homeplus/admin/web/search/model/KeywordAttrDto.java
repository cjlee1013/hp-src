package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeywordAttrDto {
    private Long keywordAttrNo;
    private String keyword;
    private String useYn;
    private List<KeywordAttrMngDto> keywordAttrMngList;
    private String regId;
    private String regDt;
    private String chgId;
    private String chgDt;
}
