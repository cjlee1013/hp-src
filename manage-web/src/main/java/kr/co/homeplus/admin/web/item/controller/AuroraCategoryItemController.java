package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.auroraCategoryItem.AuroraCategoryItemExcelDto;
import kr.co.homeplus.admin.web.item.model.auroraCategoryItem.AuroraCategoryItemGetDto;
import kr.co.homeplus.admin.web.item.model.auroraCategoryItem.AuroraCategoryItemListParamDto;
import kr.co.homeplus.admin.web.item.model.auroraCategoryItem.AuroraCategoryItemSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class AuroraCategoryItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;
    private final ExcelUploadService excelUploadService;


    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/auroraCategoryItem/auroraCategoryItemMain", method = RequestMethod.GET)
    public String auroraCategoryItemMain(Model model) throws Exception {

        codeService.getCodeModel(model
            , "disp_yn"			// 전시여부
        );

        model.addAllAttributes(RealGridHelper.create("auroraCategoryItemGridBaseInfo", AuroraCategoryItemGetDto.class));

        return "/item/auroraCategoryItemMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 리스트
     *
     * @param auroraCategoryItemListParamDto
     * @return List<AuroraCategoryItemGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/auroraCategoryItem/getAuroraCategoryItem.json"}, method = RequestMethod.GET)
    public List<AuroraCategoryItemGetDto> getCategory(@Valid AuroraCategoryItemListParamDto auroraCategoryItemListParamDto) {
        String apiUri = "/item/auroraCategoryItem/getAuroraCategoryItem";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AuroraCategoryItemGetDto>>>() {};
        return (List<AuroraCategoryItemGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, AuroraCategoryItemListParamDto.class, auroraCategoryItemListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 등록/수정
     *
     * @param auroraCategoryItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/auroraCategoryItem/setAuroraCategoryItem.json"})
    public ResponseResult setAuroraCategoryItem(@Valid @RequestBody AuroraCategoryItemSetParamDto auroraCategoryItemSetParamDto) {
        String apiUri = "/item/auroraCategoryItem/setAuroraCategoryItem";
        auroraCategoryItemSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, auroraCategoryItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadAuroraCategoryItemPop")
    public String uploadAuroraCategoryItemPop(Model model, @RequestParam(value = "callback") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        return "/item/pop/uploadAuroraCategoryItemPop";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 일괄등록
     */
    @ResponseBody
    @PostMapping("/auroraCategoryItem/setAuroraCategoryItemExcel.json")
    public void setAuroraCategoryItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<AuroraCategoryItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "중분류 카테고리ID", "mCateCd", true)
            .header(1, "상품번호", "itemNo", true)
            .header(2, "사용여부", "useYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<AuroraCategoryItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(AuroraCategoryItemExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<AuroraCategoryItemSetParamDto> arrSetParamDto = new ArrayList<>();
        String regId = loginCookieService.getUserInfo().getEmpId();
        excelData
            .stream()
            .forEach(item -> {
                if(!StringUtils.isEmpty(item.getMCateCd()) || !StringUtils.isEmpty(item.getItemNo()) || !StringUtils.isEmpty(item.getUseYn())) {
                    AuroraCategoryItemSetParamDto setDto = new AuroraCategoryItemSetParamDto();

                    setDto.setMCateCd(Integer.parseInt(item.getMCateCd()));
                    setDto.setItemNo(item.getItemNo());
                    setDto.setDispYn(item.getUseYn());
                    setDto.setRegId(regId);

                    arrSetParamDto.add(setDto);
                }
            });

        resourceClient.postForResponseObject(ResourceRouteName.ITEM, arrSetParamDto, "/item/auroraCategoryItem/setAuroraCategoryItemExcel", new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

}
