package kr.co.homeplus.admin.web.search.controller;

import java.util.HashMap;
import kr.co.homeplus.admin.web.search.model.AnalysisGetDto;
import kr.co.homeplus.admin.web.search.model.AnalysisGridDto;
import kr.co.homeplus.admin.web.search.service.AnalysisService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/analysis")
@RequiredArgsConstructor
public class AnalysisController {

    private final AnalysisService analysisService;

    @GetMapping(value = { "/result" })
    public String getAnalysis(Model model) {
        model.addAllAttributes(RealGridHelper.create("analysisSearchGridBaseInfo", AnalysisGridDto.AnalysisPlugin.class));
        model.addAllAttributes(RealGridHelper.create("analysisIndexGridBaseInfo", AnalysisGridDto.AnalysisPlugin.class));
        return "/search/analysis";
    }

    @ResponseBody
    @PostMapping(value = {"/result.json"})
    public AnalysisGetDto getAnalysis(@RequestBody HashMap<String,Object> requestBody) {
        AnalysisGetDto response = new AnalysisGetDto();
        response.setSearchPlugin(
            analysisService.getAnalysis(
                  (String)requestBody.get("keyword")
                , (String)requestBody.get("search_analyzer_name")
                , (String)requestBody.get("service")
            )
        );
        response.setIndexPlugin(
            analysisService.getAnalysis(
                  (String)requestBody.get("keyword")
                , (String)requestBody.get("index_analyzer_name")
                , (String)requestBody.get("service")
            )
        );
        return response;
    }
}
