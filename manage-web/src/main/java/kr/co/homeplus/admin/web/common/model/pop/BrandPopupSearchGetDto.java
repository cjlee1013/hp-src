package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 브랜드 정보 Get_entry
 */

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class BrandPopupSearchGetDto {
    @RealGridColumnInfo(headText = "브랜드코드", sortable = true, width = 50)
    private String 	brandNo;

    @RealGridColumnInfo(headText = "브랜드명", sortable = true, width = 50)
    private String 	brandNm;

    @RealGridColumnInfo(headText = "브랜드명(영문)", sortable = true, width = 50)
    private String  brandNmEng;

    @RealGridColumnInfo(headText = "전시명", hidden = true)
    private String  dispNm;

    @RealGridColumnInfo(headText = "등록자", sortable = true, width = 50)
    private String 	regNm;
}
