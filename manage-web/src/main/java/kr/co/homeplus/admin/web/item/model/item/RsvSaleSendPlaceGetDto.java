package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RsvSaleSendPlaceGetDto {
    private long rsvSaleSeq;
    private String sendPlace;
    private String sendPlaceNm;
    private String sendStartDt;
    private String sendEndDt;
    private char monYn;
    private char tueYn;
    private char wedYn;
    private char thuYn;
    private char friYn;
    private char satYn;
    private char sunYn;
}
