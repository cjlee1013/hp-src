package kr.co.homeplus.admin.web.manage.model.dspSearchBar;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspSearchBarListSelectDto {

    @RealGridColumnInfo(headText = "일련번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long keywordNo;

    @RealGridColumnInfo(headText = "siteType", order = 1, hidden = true)
    private String siteType;
    @RealGridColumnInfo(headText = "locCd", order = 2, hidden = true)
    private String locCd;

    @RealGridColumnInfo(headText = "사이트구분", width = 80, order = 3, hidden = true)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "pcUseYn", order = 4, hidden = true)
    private String pcUseYn;
    @RealGridColumnInfo(headText = "mobileUseYn", order = 5, hidden = true)
    private String mobileUseYn;

    @RealGridColumnInfo(headText = "키워드", width = 250, order = 6)
    private String keyword;

    @RealGridColumnInfo(headText = "전시 시작일시", columnType = RealGridColumnType.DATETIME, width = 120, order = 7, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일시", columnType = RealGridColumnType.DATETIME, width = 120, order = 8, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "노출위치", columnType = RealGridColumnType.BASIC, order = 9)
    private String locCdNm;

    @RealGridColumnInfo(headText = "가중치/우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 80, order = 10, sortable = true)
    private short weightOrPriority;

    @RealGridColumnInfo(headText = "useYn", order = 11, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 12, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "디바이스", width = 80, order = 13)
    private String deviceTypeNm;

    @RealGridColumnInfo(headText = "linkType", order = 14, hidden = true)
    private String linkType;
    @RealGridColumnInfo(headText = "linkInfo", order = 15, hidden = true)
    private String linkInfo;
    @RealGridColumnInfo(headText = "linkOptions", order = 16, hidden = true)
    private String linkOptions;
    @RealGridColumnInfo(headText = "linkInfoNm", order = 17, hidden = true)
    private String linkInfoNm;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 18)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 19, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 20)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 21, sortable = true)
    private String chgDt;


    private String regId;
    private String chgId;
    private short priority;
    private String weight;

}