package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ObjectUtils;

@Getter
@Setter
public class ItemHistGetDto {
    // 히스토리번호
    private String id;
    // 히스토리 등록일
    private String itemHistDt;
    // 히스토리 등록자ID
    private String itemHistUserId;
    // 히스토리 등록자명
    private String itemHistUserNm;

    /**
     * 기본정보
     */
    // 상품번호
    private String itemNo;
    // 상품명
    private String itemNm;
    // 상품유형 (B:반품(리세일), N:새상품, O:위메프 사외창고, R:리퍼, U:중고)
    private String itemType;
    // 판매유형 (NOR:일반,HDN:히든프라이스,BIZ:비즈)
    private String mallType;
    // 상품상태 (T: 임시저장, B:진행대기, E:수정대기, A:판매중, S:판매중지)
    private String itemStatus;
    // 판매상태 (A:판매가능, S:품절)
    private String saleStatus;
    // 파트너ID
    private String partnerId;
    // 판매중지처리일시
    private String saleStatusDt;
    // 성인상품유형명
    private String adultTypeNm;
    // 노출정보
    private String dispYn;
    // 노출정보명
    private String dispYnNm;
    // 상세설명타입 (IMG:이미지, HTML:html)
    private String detailType;
    // 브랜드번호
    private String brandNo;
    // 제조사번호
    private String makerNo;
    // 선택형옵션사용여부 (사용:Y, 미사용:N)
    private String optSelUseYn;
    // 텍스트옵션사용여부 (사용:Y, 미사용:N)
    private String optTxtUseYn;
    // 등록자
    private String regId;
    // 등록일시
    private String regDt;
    // 수정자
    private String chgId;
    // 수정일시
    private String chgDt;

    // 대카테고리
    private String lcateCd;
    // 대카테고리명
    private String lcateNm;
    // 중카테고리
    private String mcateCd;
    // 중카테고리명
    private String mcateNm;
    // 소카테고리명
    private String scateNm;
    // 소카테고리
    private String scateCd;
    // 세카테고리
    private String dcateCd;
    // 세카테고리명
    private String dcateNm;

    // 등록자 이름
    private String regNm;
    // 수정자 이름
    private String chgNm;
    // 상품유형명
    private String itemTypeNm;
    // 판매유형명
    private String mallTypeNm;
    // 상품상태명
    private String itemStatusNm;
    // 판매상태명
    private String saleUnit;
    // 선택형옵션사용여부명
    private String optSelUseYnNm;
    // 텍스트옵션사용여부명
    private String optTxtUseYnNm;
    // 파트너명
    private String partnerBusinessNm;
    // 파트너 회원상태 (NORMAL:정상, WITHDRAW:탈퇴)
    private String partnerStatus;
    // 브랜드명
    private String brandNm;
    // 제조사명
    private String makerNm;

    /**
     * 노출상태
     */
    // 상품노출상태
    private String itemShowStatus;
    // 정보변경요청상태
    private String itemInfoChgStatus;
    // 정보변경요청상태명
    private String itemInfoChgStatusNm;

    /**
     * 부가정보
     */
    // 판매자 상품코드
    private String sellerItemCd;
    // 부가정보 > 가격비교 등록여부 (Y,N)
    private String epYn;
    // 이미지 노출여부
	private String imgDispYn;
    // 선물하기사용여부 (Y: 사용, N: 사용안함)
    private String giftYn;
    // 검색 키워드
    private String srchKeyword;
    // 해외배송 대행여부
    private String globalAgcyYn;
    // 외부연동 사이트 등록여부
    private String linkageYn;
    // 인증 리스트
    private Map<String, ItemCertHistGetDto> certListMap;
    private List<ItemCertHistGetDto> certList;

    /**
     * 판매정보
     */
    // 판매기간시작
    private String saleStartDt;
    // 판매기간종료
    private String saleEndDt;
    // 정상가격
    private String originPrice;
    // 판매가격
    private String salePrice;
    //매입가
    private String purchasePrice;
    // 부가세 여부 (Y:과세, N:비과세)
    private String taxYn;
    //할인기간여부
    private String dcYn;
    //할인가격
    private String dcPrice;
    //특정할인기간여부
    private String dcPeriodYn;
    //할인시작일시
    private String dcStartDt;
    //할인종료일시
    private String dcEndDt;
    //할인수수료율
    private String dcCommissionRate;
    //할인수수료액
    private String dcCommissionPrice;
    // 장바구니 담기 제한 (Y:제한설정, N:제한없음)
    private String cartLimitYn;
    // 구매제한 - 최소구매수량
    private String purchaseMinQty;
    // 1인당 구매제한 사용여부 (Y:사용, N:미사용)
    private String purchaseLimitYn;
    // 구매제한 타입 (O:1회, P:기간제한)
    private String purchaseLimitDuration;
    // 구매제한 타입명 (O:1회, P:기간제한)
    private String purchaseLimitDurationNm;
    // 구매제한 일자 - ?일
    private String purchaseLimitDay;
    // 구매제한 개수 - ?개
    private String purchaseLimitQty;
    // 재고수량
    private String stockQty;
    // 판매정보 > 예약판매설정여
    private String rsvYn;
    // 판매정보 > 예약판매시작일시
    private String rsvStartDt;
    // 판매정보 > 예약판매종료일시
    private String rsvEndDt;
    // 판매정보 > 예약상품출하시작일시
    private String shipStartDt;
    // 수수료 부과 기준 (정률:FR, 정액:FA) - 판매가격, 카테고리, 판매자가 변할때 수수료에 영향을 줌")
    private String commissionType;
    // 수수료(%)
    private String commissionRate;
    // 수수료(원)
    private String commissionPrice;

    /**
     * 배송정보
     */
    // 배송정보 > 배송 정책 번호
    private String shipPolicyNo;
    // 배송정보 > 출고 기한
    private String releaseDay;
    // 배송정보 > 출고기한 1일 인 경우 출고시간(1~24)
    private String releaseTime;
    // 배송정보 > 휴일제외여부
    private String holidayExceptYn;
    // 배송정보 > 반품/교환 배송비 (편도)
    private String claimShipFee;
    // 배송정보 > 출고지-우편번호
    private String releaseZipcode;
    // 배송정보 > 출고지-기본주소(지번)
    private String releaseAddr1;
    // 배송정보 > 출고지-상세주소(지번)
    private String releaseAddr2;
    // 배송정보 > 회수지-우편번호
    private String returnZipcode;
    // 배송정보 > 회수지-기본주소(지번)
    private String returnAddr1;
    // 배송정보 > 회수지-상세주소(지번)
    private String returnAddr2;
    //배송정보명
    private String shipPolicyNm;
    //배송유형(ITEM:상품별/BUNDLE:묶음배송)
    private String shipKindNm;
    //배송방법 (DS/TD_DLV/DRCT/POST/PICK)
    private String shipMethodNm;
    //배송비종류 ( FREE : 무료 / FIXED : 유료 / COND : 조건부 무료 )
    private String shipTypeNm;
    //배송비
    private String shipFee;
    //배송비 결제방식
    private String prepaymentYnNm;
    //배송비 노출여부
    private String shipDispYnNm;

    // 배송정보 > 등록자
    private String shipRegId;
    // 배송정보 > 등록일시
    private String shipRegDt;
    // 배송정보 > 수정자
    private String shipChgId;
    // 배송정보 > 수정일시
    private String shipChgDt;


    /**
     * 상품 상세정보
     */
    // 상품상세정보
    private String itemDesc;

    /**
     * 옵션타이틀 정보
     */
    // 옵션타이틀 > type
    private String optTitleType;
    // 옵션타이틀 > depth
    private String optTitleDepth;
    // 옵션타이틀 > 제목1
    private String opt1Title;
    // 옵션타이틀 > 제목2
    private String opt2Title;
    //옵션정보 리스트
    Map<String, ItemOptionValueHistGetDto> optListMap;
    List<ItemOptionValueHistGetDto> optList;

    /**
     * 텍스트옵션 정보
     */
    // 텍스트옵션 > type
    private String optTextType;
    // 텍스트옵션 > depth
    private String optTextDepth;
    // 텍스트옵션 > 제목1
    private String opt1Text;
    // 텍스트옵션 > 제목2
    private String opt2Text;
    // 텍스트옵션 > 등록자
    private String optTextRegId;
    // 텍스트옵션 > 등록일시
    private String optTextRegDt;
    // 텍스트옵션 > 수정자
    private String optTextChgId;
    // 텍스트옵션 > 수정일시
    private String optTextChgDt;

    /**
     * 상품별 고시정보
     */
    //상품고시정보 정책코드명
    private String gnoticeNm;

    private Map<String, ItemNoticeHistGetDto> noticeListMap;

    private List<ItemNoticeHistGetDto> noticeList;

    /**
     * 이미지정보
     */
    private Map<String, ItemImgHistGetDto> imgListMap;

    private List<ItemImgHistGetDto> imgList;

    private String videoFileUrl;

    /**
     * 파일정보
     */
    private Map<String, ItemFileHistGetDto> proofFileListMap;

    private List<ItemFileHistGetDto> proofFileList;

    /**
     * 속성정보
     */
    private String unitDispYn;

    private String originTxt;

    /**
     * 점포상품 상세 필드
     */
    private String rsvPurchaseLimitYn;
    private String onlineStockYn;
    private String onlineStockStartDt;
    private String onlineStockEndDt;

    private <T> String getMapToString(Map<String, T> obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return "";
        }

        StringBuilder toChangeStr = new StringBuilder();

        for (Map.Entry<String, T> entry : obj.entrySet()) {
            toChangeStr.append(entry.toString());
        }

        return toChangeStr.toString();
    }

    private <T> String getListToString(List<T> obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return "";
        }

        StringBuilder toChangeStr = new StringBuilder();

        for (T entry : obj) {
            toChangeStr.append(entry.toString());
        }

        return toChangeStr.toString();
    }


    // 기본정보
    public Optional<String> toChangeBasicString() {
        return Optional.of(
                  this.itemShowStatus
                + this.itemStatusNm
                + this.itemNm
                + this.itemType
                + this.mallType
                + this.lcateNm
                + this.mcateNm
                + this.scateNm
                + this.dcateNm
                + this.dcateCd
                + this.partnerId
                + this.brandNo
                + this.brandNm
                + this.makerNo
                + this.makerNm
                + this.dispYn
                + this.dispYnNm
                + this.sellerItemCd
                + this.itemInfoChgStatus
        );
    }

    // 판매정보
    public Optional<String> toChangeSaleString() {
        return Optional.of(
                  this.taxYn
                + this.originPrice
                + this.salePrice
                + this.purchasePrice
                + this.commissionType
                + this.commissionRate
                + this.commissionPrice
                + this.dcYn
                + this.dcPrice
                + this.dcPeriodYn
                + this.dcStartDt
                + this.dcEndDt
                + this.dcCommissionRate
                + this.dcCommissionPrice
                + this.stockQty
                + this.saleStartDt
                + this.saleEndDt
                + this.purchaseMinQty
                + this.saleStatus
                + this.saleUnit
                + this.purchaseLimitYn
                + this.purchaseLimitDuration
                + this.purchaseLimitDurationNm
                + this.purchaseLimitDay
                + this.purchaseLimitQty
                + this.cartLimitYn
                + this.rsvYn
                + this.rsvStartDt
                + this.rsvEndDt
                + this.shipStartDt
        );
    }

    // 배송정보
    public Optional<String> toChangeShipString() {
        return Optional.of(
                  this.shipPolicyNo
                + this.releaseDay
                + this.releaseTime
                + this.holidayExceptYn
                + this.claimShipFee
                + this.releaseZipcode
                + this.releaseAddr1
                + this.releaseAddr2
                + this.returnZipcode
                + this.returnAddr1
                + this.returnAddr2
                + this.shipPolicyNm
                + this.shipKindNm
                + this.shipMethodNm
                + this.shipTypeNm
                + this.shipFee
                + this.prepaymentYnNm
                + this.shipDispYnNm
        );
    }

    // 옵션 정보
    public Optional<String> toChangeOptItemString() {
        return Optional.of(
                  this.optSelUseYn
                + this.optSelUseYnNm
                + this.optTitleDepth
                + this.opt1Title
                + this.opt2Title
                + this.optTxtUseYn
                + this.optTxtUseYnNm
                + this.optTextDepth
                + this.opt1Text
                + this.opt2Text
                + this.getListToString(this.optList)
        );
    }

    // 이미지정보
    public Optional<String> toChangeImgString() {
        return Optional.of(
                  this.getListToString(this.imgList)
        );
    }

    // 고시정보
    public Optional<String> toChangeNoticeString() {
        return Optional.of(
                  this.getListToString(this.noticeList)
        );
    }

    // 부가정보
    public Optional<String> toChangeEtcString() {
        return Optional.of(
                  this.getListToString(this.proofFileList)
                + this.getListToString(this.certList)
                + this.adultTypeNm
                + this.imgDispYn
                + this.srchKeyword
                + this.globalAgcyYn
                + this.epYn
        );
    }

    // group data
    public ItemHistGroupGetDto setItemHistGroupData(final ItemHistGroupStringGetDto prevItemHistGroupStringGetDto) {
        ItemHistGroupGetDto itemHistGroupGetDto = new ItemHistGroupGetDto();
        final String CHANGE_MSG = "수정";

        itemHistGroupGetDto.setId(this.id);
        itemHistGroupGetDto.setChgDt(this.itemHistDt);
        itemHistGroupGetDto.setChgNm(this.itemHistUserNm);

        if (prevItemHistGroupStringGetDto.getBasic().isPresent()
                && !prevItemHistGroupStringGetDto.getBasic().equals(this.toChangeBasicString())) {
            itemHistGroupGetDto.setChangeBasic(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getSale().isPresent()
                && !prevItemHistGroupStringGetDto.getSale().equals(this.toChangeSaleString())) {
            itemHistGroupGetDto.setChangeSale(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getShip().isPresent()
                && !prevItemHistGroupStringGetDto.getShip().equals(this.toChangeShipString())) {
            itemHistGroupGetDto.setChangeShip(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getOptItem().isPresent()
                && !prevItemHistGroupStringGetDto.getOptItem().equals(this.toChangeOptItemString())) {
            itemHistGroupGetDto.setChangeOptItem(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getImg().isPresent()
                && !prevItemHistGroupStringGetDto.getImg().equals(this.toChangeImgString())) {
            itemHistGroupGetDto.setChangeImg(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getNotice().isPresent()
                && !prevItemHistGroupStringGetDto.getNotice().equals(this.toChangeNoticeString())) {
            itemHistGroupGetDto.setChangeNotice(CHANGE_MSG);
        }

        if (prevItemHistGroupStringGetDto.getEtc().isPresent()
                && !prevItemHistGroupStringGetDto.getEtc().equals(this.toChangeEtcString())) {
            itemHistGroupGetDto.setChangeEtc(CHANGE_MSG);
        }

        return itemHistGroupGetDto;
    }

}
