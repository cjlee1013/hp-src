package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.EXP;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridGroupColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.StoreSlotManageModel;
import kr.co.homeplus.admin.web.delivery.model.StoreSlotManageSelectModel;
import kr.co.homeplus.admin.web.delivery.service.StoreSlotService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 점포배송정보 > slot 관리
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class StoreSlotController {

    private static final List<RealGridGroupColumn> STORE_SLOT_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption STORE_SLOT_GRID_OPTION = new RealGridOption("fill", false, true, false);

    private final StoreSlotService storeSlotService;
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;

    public StoreSlotController(StoreSlotService storeSlotService, CodeService codeService, LoginCookieService loginCookieService,
            AuthorityService authorityService) {

        this.storeSlotService = storeSlotService;
        this.codeService = codeService;
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;

        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("storeNm", "storeNm", "점포", RealGridColumnType.BASIC, 80, true,true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("shipDt", "shipDt", "일자", RealGridColumnType.DATE, 80, true,true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("shipWeekday", "shipWeekday", "요일", RealGridColumnType.BASIC, 60,true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("shiftId", "shiftId", "Shift번호", RealGridColumnType.BASIC, 60, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("vanNm", "vanNm", "VAN조", RealGridColumnType.BASIC, 60, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("useYn", "useYn", "사용여부", RealGridColumnType.BASIC, 80, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("slotId", "slotId", "Slot번호", RealGridColumnType.BASIC, 100, true, true, "Slot입력"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("slotShipStartTime", "slotShipStartTime", "From", RealGridColumnType.BASIC, 100, true, true, "Slot입력"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("slotShipEndTime", "slotShipEndTime", "To", RealGridColumnType.BASIC, 100, true, true, "Slot입력"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("orderFixCnt", "orderFixCnt", "고정", RealGridColumnType.BASIC, 100, true, true, "일반"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("orderCnt", "orderCnt", "주문", RealGridColumnType.BASIC, 100, true, true, "일반"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("orderChgCnt", "orderChgCnt", "변경", RealGridColumnType.BASIC, 100, true, true, "일반"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("pickupOrderFixCnt", "pickupOrderFixCnt", "고정", RealGridColumnType.BASIC, 100, true, true, "C&C"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("pickupOrderCnt", "pickupOrderCnt", "주문", RealGridColumnType.BASIC, 100, true, true, "C&C"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("pickupOrderChgCnt", "pickupOrderChgCnt", "변경", RealGridColumnType.BASIC, 100, true, true, "C&C"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("lockerOrderFixCnt", "lockerOrderFixCnt", "고정", RealGridColumnType.BASIC, 100, true, true, "락커"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("lockerOrderCnt", "lockerOrderCnt", "주문", RealGridColumnType.BASIC, 100, true, true, "락커"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("lockerOrderChgCnt", "lockerOrderChgCnt", "변경", RealGridColumnType.BASIC, 100, true, true, "락커"));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("reserveOrderFixCnt", "reserveOrderFixCnt", "예약고정건수", RealGridColumnType.BASIC, 100, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("reserveOrderChgCnt", "reserveOrderChgCnt", "예약변경건수", RealGridColumnType.BASIC, 100, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("chgId", "chgId", "수정자", RealGridColumnType.BASIC, 100, true, true, ""));
        STORE_SLOT_GRID_HEAD.add(new RealGridGroupColumn("chgDt", "chgDt", "수정일시", RealGridColumnType.DATETIME, 120, true, true, ""));
    }

    /**
     * SLOT 관리 Main Page
     */
    @GetMapping("/slotManageMain")
    public String slotManageMain(Model model) throws Exception{
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type",   //점포유형
            "weekday"               //배송요일
        );
        List<MngCodeGetDto> storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> !EXP.getCode()
                        .equals(st.getMcCd()))
                .collect(Collectors.toList());
        model.addAttribute("storeType", storeTypeList);
        model.addAttribute("weekdayList", code.get("weekday"));
        ObjectMapper om = new ObjectMapper();
        model.addAttribute("weekdayJson", om.writeValueAsString(code.get("weekday")));
        model.addAttribute("storeSlotGridBaseInfo", new RealGridBaseInfo("storeSlotGridBaseInfo", STORE_SLOT_GRID_OPTION, STORE_SLOT_GRID_HEAD).toStringGroup());

        return "/delivery/storeSlotManageMain";
    }

    /**
     * slot 조회
     * @param storeSlotManageSelectModel
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getSlotManageList.json")
    public List<StoreSlotManageModel> getSlotManageList(@ModelAttribute StoreSlotManageSelectModel storeSlotManageSelectModel) throws Exception {
        return storeSlotService.getStoreSlotManageList(storeSlotManageSelectModel);
    }

    /**
     * slot 사용여부 저장
     * @param storeSlotManageModelList
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveSlotManage.json")
    public ResponseObject<Object> saveCardBinManage(@RequestBody List<StoreSlotManageModel> storeSlotManageModelList) throws Exception {

        String userCd = loginCookieService.getUserInfo().getEmpId();
        return storeSlotService.saveSlotManage(storeSlotManageModelList, userCd);
    }
}
