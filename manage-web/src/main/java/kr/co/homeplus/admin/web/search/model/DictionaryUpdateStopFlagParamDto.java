package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "오탈자 > 사용여부")
public class DictionaryUpdateStopFlagParamDto {
    @ApiModelProperty(notes = "단어번호", position = 1, required = true)
    private Integer wordId;
    @ApiModelProperty(notes = "사용여부", position = 2, required = true)
    private boolean flag;
}
