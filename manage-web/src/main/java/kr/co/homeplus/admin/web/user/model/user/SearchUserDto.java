package kr.co.homeplus.admin.web.user.model.user;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchUserDto {
  @ApiModelProperty(value = "회원 아이디")
  private String userId;

  @ApiModelProperty(value = "회원 명")
  private String userName;

  @ApiModelProperty(value = "사용자 분류 - 10 : 개인회원, 15 : 사업자구매회원, 20 : md, 30 : 판매자")
  private Integer userClassify; // 사용자 분류

  @ApiModelProperty(value = "사용자 분류 - 10 : 개인, 15 : 사업자구매회원, 20 : md, 30 : 판매자")
  private String userClassifyNm; // 사용자 분류 명

  @ApiModelProperty(value = "회원 Email")
  private String userEmail;

  @ApiModelProperty(value = "회원 phone")
  private String userPhone;

  @ApiModelProperty(value = "상호")
  private String companyNm;

  @ApiModelProperty(value = "사업자번호")
  private String companyNo;

  @ApiModelProperty(value = "유료회원 구분 (STANDARD = 기본, PREMIUM = 프리미엄)")
  private String clubGrade;
}
