package kr.co.homeplus.admin.web.escrow.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_ZIPCODE_SIDO_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_ZIPCODE_SIGUNGU_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_UPDATE_ZIPCODE_ISLAND_TYPE;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.model.ZipcodeManageDto;
import kr.co.homeplus.admin.web.escrow.model.ZipcodeManageSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ZipcodeManageService {

    private final ResourceClient resourceClient;

    /**
     * 시도명 리스트 조회
     * @return
     */
    public List<String> getZipcodeSidoList() {
        String apiUri = ESCROW_GET_ZIPCODE_SIDO_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (List<String>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 시군구 리스트 조회
     * @param sidoNm
     * @return
     */
    public List<String> getZipcodeSigunguList(String sidoNm) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("sidoNm", sidoNm));

        String apiUri = ESCROW_GET_ZIPCODE_SIGUNGU_LIST + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (List<String>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 우편번호/도로명/지번 검색
     * @param zipcodeManageSelectDto
     * @return
     */
    public List<ZipcodeManageDto> getZipcodeManageList(ZipcodeManageSelectDto zipcodeManageSelectDto) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schIslandType", zipcodeManageSelectDto.getSchIslandType()));
        setParameterList.add(SetParameter.create("schKeywordType", zipcodeManageSelectDto.getSchKeywordType()));
        setParameterList.add(SetParameter.create("schSidoType", zipcodeManageSelectDto.getSchSidoType()));
        setParameterList.add(SetParameter.create("schSigunguType", zipcodeManageSelectDto.getSchSigunguType()));
        setParameterList.add(SetParameter.create("schKeyword", zipcodeManageSelectDto.getSchKeyword()));
        setParameterList.add(SetParameter.create("schBuildNo", zipcodeManageSelectDto.getSchBuildNo()));

        String apiUri = EscrowConstants.ESCROW_GET_ZIPCODE_MANAGE + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ZipcodeManageDto>>>() {};
        return (List<ZipcodeManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 도서산간여부(제주/도서산간/해당없음) 수정
     * @param zipcodeManageDto
     * @return
     */
    public Integer updateZipcodeIslandType(ZipcodeManageDto zipcodeManageDto) {
        String apiUri = ESCROW_UPDATE_ZIPCODE_ISLAND_TYPE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (Integer) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, zipcodeManageDto, apiUri, typeReference).getData();
    }
}
