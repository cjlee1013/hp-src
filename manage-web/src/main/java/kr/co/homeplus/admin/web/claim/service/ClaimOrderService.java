package kr.co.homeplus.admin.web.claim.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderDetailInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderDetailInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimRegSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.admin.web.claim.model.MultiShipClaimItemListGetDto;
import kr.co.homeplus.admin.web.claim.model.MultiShipClaimItemListSetDto;
import kr.co.homeplus.admin.web.claim.model.MultiShipInfoGetDto;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimOrderService {

    private final ResourceClient resourceClient;

    private final LoginCookieService loginCookieService;

    private final CodeService codeService;

    public List<ClaimOrderDetailInfoGetDto> getClaimOrderDetail(ClaimOrderDetailInfoSetDto claimOrderDetailInfoSetDto){

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("purchaseOrderNo", claimOrderDetailInfoSetDto.getPurchaseOrderNo()));
        setParameterList.add(SetParameter.create("bundleNo", claimOrderDetailInfoSetDto.getBundleNo()));
        setParameterList.add(SetParameter.create("claimType", claimOrderDetailInfoSetDto.getClaimType()));

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            null,
            "/claim/orderCancel" + StringUtil.getParameter(setParameterList),
            new ParameterizedTypeReference<ResponseObject<List<ClaimOrderDetailInfoGetDto>>>() {}
        ).getData();
    }

    public ClaimPreRefundGetDto getPreRefundPrice(ClaimPreRefundSetDto claimPreRefundSetDto) throws Exception {

        String url = claimPreRefundSetDto.getMultiShipYn().equalsIgnoreCase("Y") ? "/claim/multiShipPreRefundPrice" : "/claim/preRefundPrice";
        claimPreRefundSetDto.setIsMultiOrder(claimPreRefundSetDto.getMultiShipYn().equalsIgnoreCase("Y") ? true : false);

        //다중배송 전체 취소 시 배송지만큼 multiBundleNo 값을 0으로 넣어줌
        if(claimPreRefundSetDto.getMultiShipYn().equalsIgnoreCase("Y") && StringUtil.isEmpty(claimPreRefundSetDto.getMultiBundleNo())){
            claimPreRefundSetDto.setMultiBundleNo("0");
        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_reason_type"            // 클레임 사유 코드
        );
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();
        for(MngCodeGetDto dto : code.get("claim_reason_type")){
            if(dto.getMcCd().equals(claimPreRefundSetDto.getClaimReasonType())){
                mngCodeGetDto = dto;
            }
        }
        //클레임 사유 코드 조회
        claimPreRefundSetDto.setWhoReason(mngCodeGetDto.getRef2());
        claimPreRefundSetDto.setCancelType(mngCodeGetDto.getRef3());

        //판매자 귀책 시 반품비용 결제 방법 - 환불금차감으로 설정 2020.12.22
        if(mngCodeGetDto.getRef2().equalsIgnoreCase("S")){
            claimPreRefundSetDto.setEncloseYn("N");
        }

        ResponseObject<ClaimPreRefundGetDto> responseGetDto = resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimPreRefundSetDto,
            url,
            new ParameterizedTypeReference<ResponseObject<ClaimPreRefundGetDto>>() {}
        );

        if("SUCCESS,0000".contains(responseGetDto.getReturnCode())){
            responseGetDto.getData().setWhoReason(mngCodeGetDto.getRef2());
            responseGetDto.getData().setClaimType(mngCodeGetDto.getRef3());
            return responseGetDto.getData();
        } else {
            ClaimPreRefundGetDto responseDto = new ClaimPreRefundGetDto();
            responseDto.setReturnValue(Integer.parseInt(responseGetDto.getReturnCode()));
            responseDto.setReturnMsg(responseGetDto.getReturnMessage());
            return responseDto;
        }
    }

    public ResponseObject<String> setClaimRegister(ClaimRegSetDto claimRegSetDto) throws Exception {

        String url = claimRegSetDto.getMultiShipYn().equalsIgnoreCase("Y") ? "/claim/claimMultiCancelReg" : "/claim/claimCancelReg";
        claimRegSetDto.setIsMultiOrder(claimRegSetDto.getMultiShipYn().equalsIgnoreCase("Y") ? true : false);

        //다중배송 전체 취소 시 배송지만큼 multiBundleNo 값을 0으로 넣어줌
        if(claimRegSetDto.getMultiShipYn().equalsIgnoreCase("Y") && StringUtil.isEmpty(claimRegSetDto.getMultiBundleNo())){
            claimRegSetDto.setMultiBundleNo("0");
        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_reason_type"            // 클레임 사유 코드
        );
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();
        for(MngCodeGetDto dto : code.get("claim_reason_type")){
            if(dto.getMcCd().equals(claimRegSetDto.getClaimReasonType())){
                mngCodeGetDto = dto;
            }
        }

        // 이후 공통코드 테이블에 데이터 적재 후 해당 코드를 사용할 예정.
        // 클레임 사유 타입 코드
        claimRegSetDto.setClaimReasonType(claimRegSetDto.getClaimReasonType());
        claimRegSetDto.setClaimType(mngCodeGetDto.getRef1());
        claimRegSetDto.setWhoReason(mngCodeGetDto.getRef2());
        claimRegSetDto.setCancelType(mngCodeGetDto.getRef3());

        // 등록자 추가 ( 세션서비스에서 유저코드 사용)
        claimRegSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        log.debug("claimRegister {}", claimRegSetDto);

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimRegSetDto,
            url,
            new ParameterizedTypeReference<ResponseObject<String>>() {
            }
        );
    }

    /**
     * 배송 상태 조회
     * **/
    public ClaimShipInfoDto  getClaimShipInfo(long bundleNo) {
        String apiUri = "/claim/getClaimShipInfo";
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + "?bundleNo=" + bundleNo,
            new ParameterizedTypeReference<ResponseObject<ClaimShipInfoDto>>() {}).getData();
    }

    /**
     * 배송 상태 조회
     * **/
    public ClaimShipInfoDto  getMultiShipInfo(long bundleNo, long multiBundleNo) {
        String apiUri = "/claim/getMultiShipInfo";
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + "?bundleNo=" + bundleNo +"&multiBundleNo=" + multiBundleNo,
            new ParameterizedTypeReference<ResponseObject<ClaimShipInfoDto>>() {}).getData();
    }

    /**
     * 다중배송 배송지 리스트조회
     * **/
    public List<MultiShipInfoGetDto>  getMultiShipAddrList(long purchaseOrderNo,long bundleNo) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
        setParameterList.add(SetParameter.create("bundleNo", bundleNo));

        String apiUri = "/claim/getMultiShipList";
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri + StringUtil.getParameter(setParameterList),
            new ParameterizedTypeReference<ResponseObject<List<MultiShipInfoGetDto>>>() {}).getData();
    }

    public List<MultiShipClaimItemListGetDto> getMultiShipItemList(MultiShipClaimItemListSetDto claimOrderDetailInfoSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            claimOrderDetailInfoSetDto,
            "/claim/getMultiShipItemList",
            new ParameterizedTypeReference<ResponseObject<List<MultiShipClaimItemListGetDto>>>() {}
        ).getData();
    }


}
