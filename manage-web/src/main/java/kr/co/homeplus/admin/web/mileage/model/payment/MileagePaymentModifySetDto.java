package kr.co.homeplus.admin.web.mileage.model.payment;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileagePaymentModifySetDto {
    //마일리지요청번호
    private String mileageReqNo;

    //마일리지 지급/회수 기본 정보
    private MileageRequestDto mileageRequestDto;

    //마일리지 지급/회수 대상회원
    private List<MileageReqDetailDto> mileageReqDetailList;

}
