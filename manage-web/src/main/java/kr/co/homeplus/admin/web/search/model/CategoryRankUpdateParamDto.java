package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "카테고리랭킹 > 수정")
public class CategoryRankUpdateParamDto {
    @ApiModelProperty(notes = "매핑번호", position = 1)
    private Long rankingId;
    @ApiModelProperty(notes = "매핑번호", position = 2)
    private Long relationId;
    @ApiModelProperty(notes = "순위", position = 3)
    private String boostType;
    @ApiModelProperty(notes = "사용여부", position = 3)
    private String useFlag;

    private Integer categoryId;
    private String categoryNm;

    private String administratorId;
}
