package kr.co.homeplus.admin.web.core.utility;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 접속된 클라이언트의 아이피를 가져오는 메소드
 */
public class RemoteUtil {
    /**
     * 현재 접속된 클라이언트의 아이피를 가져온다.<br>
     * RequestContextHolder를 통해 현재 HttpServletRequest를 획득하여 아이피주소를 리턴한다.
     *
     * @return ip주소(String)
     */
    public static String getClientIP() {
        return getClientIP(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());
    }

    /**
     * 접속된 클라이언트의 아이피를 가져온다.
     *
     * @param request 현재 접속 한 Request
     * @return ip주소(String)
     */
    public static String getClientIP(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");

        if (ip == null || ip.length() == 0) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0) {
            ip = request.getHeader("WL-Proxy-Client-IP");  // 웹로직일 경우
        }

        if (ip == null || ip.length() == 0) {
            ip = request.getRemoteAddr() ;
        }

        return ip;

    }
}
