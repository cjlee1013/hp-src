package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.accounting.APBalanceSheetGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.APInvoiceListSetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.APInvoiceListGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.AccountSubjectListGetDto;
import kr.co.homeplus.admin.web.settle.model.accounting.AccountSubjectListSetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.admin.web.settle.service.AccountingService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/accounting")
public class AccountingController {

    private final AccountingService accountingService;
    private final SettleCommonService settleCommonService;

    public AccountingController(AccountingService accountingService, SettleCommonService settleCommonService) {
        this.accountingService = accountingService;
        this.settleCommonService = settleCommonService;
    }

    /**
     * 계정과목별 데이터 조회 페이지
     */
    @RequestMapping(value = "/accountSubjectMain", method = RequestMethod.GET)
    public String accountSubjectMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("accountSubjectListBaseInfo", AccountSubjectListGetDto.class));

        return "/settle/accountSubjectList";
    }

    //계정과목별 데이터 조회 JSON
    @RequestMapping(value = "/getAccountSubjectList.json", method = RequestMethod.GET)
    @ResponseBody
    public List<AccountSubjectListGetDto> getAccountSubjectList(@ModelAttribute AccountSubjectListSetDto listParam) throws Exception{
        return accountingService.getAccountSubjectList(listParam);
    }

    /**
     * AP 전표조회
     */
    @RequestMapping(value = "/apInvoiceMain", method = RequestMethod.GET)
    public String APInvoiceMain(Model model) throws Exception{
        model.addAllAttributes(RealGridHelper.create("apInvoiceListGridBaseInfo", APInvoiceListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("apBalanceSheetGridBaseInfo", APBalanceSheetGetDto.class));

        return "/settle/apInvoiceList";
    }

    //계정과목별 데이터 조회 JSON
    @RequestMapping(value = "/getAPInvoiceList.json", method = RequestMethod.GET)
    @ResponseBody
    public List<APInvoiceListGetDto> getAPInvoiceList(@ModelAttribute APInvoiceListSetDto listParam) throws Exception{
        return accountingService.getAPInvoiceList(listParam);
    }

    //계정과목별 데이터 조회 JSON
    @RequestMapping(value = "/getAPBalanceSheet.json", method = RequestMethod.GET)
    @ResponseBody
    public List<APBalanceSheetGetDto> getAPBalanceSheet(@ModelAttribute APInvoiceListSetDto listParam) throws Exception{
        return accountingService.getAPBalanceSheet(listParam);
    }

    //계정과목별 데이터 조회 JSON
    @ResponseBody
    @PostMapping(value = "/setAPInvoiceConfirm.json")
    public ResponseObject<String> setAPInvoiceConfirm(@RequestBody List<APInvoiceListSetDto> listParam) throws Exception{
        return accountingService.setAPInvoiceConfirm(listParam);
    }

    @ResponseBody
    @RequestMapping(value = "/getAPSlipTypeList.json", method = RequestMethod.GET)
    public List<BalanceSheetSlipTypeGetDto> getAPSlipTypeList() throws Exception {
        return accountingService.getAPSlipTypeList();
    }
}