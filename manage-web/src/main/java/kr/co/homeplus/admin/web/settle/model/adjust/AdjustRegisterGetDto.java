package kr.co.homeplus.admin.web.settle.model.adjust;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산 관리 > 임의조정금액관리 > 입력값")
public class AdjustRegisterGetDto {
    @RealGridColumnInfo(headText = "계산서 대상여부", hidden = true)
    @ApiModelProperty(notes = "계산서 대상여부")
    private String taxType;

    @RealGridColumnInfo(headText = "업체코드", sortable = true, width = 250)
    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;

    @RealGridColumnInfo(headText = "상품번호", width = 250)
    @ApiModelProperty(notes = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "유형", width = 250)
    @ApiModelProperty(notes = "유형")
    private String adjustType;

    @RealGridColumnInfo(headText = "구분", hidden = true)
    @ApiModelProperty(notes = "구분")
    private String gubun;

    @RealGridColumnInfo(headText = "조정금액", width = 250)
    @ApiModelProperty(notes = "조정금액")
    private String adjustAmt;

    @RealGridColumnInfo(headText = "조정수수료", width = 250)
    @ApiModelProperty(notes = "조정수수료")
    private String adjustFee;

    @RealGridColumnInfo(headText = "사유", width = 400)
    @ApiModelProperty(notes = "사유")
    private String comment;

    @RealGridColumnInfo(headText = "별도정산", width = 250)
    @ApiModelProperty(notes = "별도정산")
    private String separateYn;

    @RealGridColumnInfo(headText = "등록자", hidden = true)
    @ApiModelProperty(notes = "등록자")
    private String regId;

    @RealGridColumnInfo(headText = "업로드결과", width = 400)
    @ApiModelProperty(notes = "업로드결과")
    private String reason;
}
