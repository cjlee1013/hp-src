package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoListParamDto {
    private String schDateType;

    private String schStartDt;

    private String schEndDt;

    private String schSiteType;

    private String schDevice;

    private String schStoreType;

    private String schCouponUseYn;

    private String schDispYn;

    private String schUseYn;

    private String schType;

    private String schValue;

    private String schPromoType;
}