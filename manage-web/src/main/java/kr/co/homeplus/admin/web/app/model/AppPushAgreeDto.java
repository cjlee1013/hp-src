package kr.co.homeplus.admin.web.app.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class AppPushAgreeDto {

    @RealGridColumnInfo(headText = "일련번호", hidden = true)
    private long seq;
    @RealGridColumnInfo(headText = "추출일자")
    private String statDt;
    @RealGridColumnInfo(headText = "전체건수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER)
    private long totalCount;

    @RealGridColumnInfo(headText = "동의건수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER)
    private long agreeCount;
    @RealGridColumnInfo(headText = "거부건수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER)
    private long disagreeCount;
    @RealGridColumnInfo(headText = "등록일자")
    private String regDt;
}
