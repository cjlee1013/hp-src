package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배사코드관리 상세정보 저장 요청 DTO")
public class ShipCompanyManageInfoSetDto {
    @ApiModelProperty(notes = "택배사")
    private String dlvCd;

    @ApiModelProperty(notes = "연동업체")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "연동업체코드")
    private String dlvLinkCompanyCd;

    @ApiModelProperty(notes = "자동반품접수")
    private String autoReturnYn;

    @ApiModelProperty(notes = "자동반품지연")
    private String autoReturnDelayYn;

    @ApiModelProperty(notes = "노출여부")
    private String displayYn;

    @ApiModelProperty(notes = "고객센터 전화번호")
    private String csPhone;

    @ApiModelProperty(notes = "홈페이지")
    private String homePage;

    @ApiModelProperty(notes = "비고")
    private String note;

    @ApiModelProperty(notes = "등록자(수정자)")
    private String regId;
}
