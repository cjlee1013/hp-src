package kr.co.homeplus.admin.web.mileage.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum MileageRequestType {
    MILEAGE_REQUEST_TYPE_PUBLISH("01", "지급"),
    MILEAGE_REQUEST_TYPE_RETURN("02", "회수");

    private String code;
    private String name;
}
