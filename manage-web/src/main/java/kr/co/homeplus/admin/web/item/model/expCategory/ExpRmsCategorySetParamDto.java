package kr.co.homeplus.admin.web.item.model.expCategory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpRmsCategorySetParamDto {

    private String division;
    private String groupNo;
    private String dept;
    private String classCd;
    private String subclass;

}