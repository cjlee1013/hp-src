package kr.co.homeplus.admin.web.mileage.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.mileage.model.type.MileageSaveType;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageAmtSumGetDto;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageAmtSumSetDto;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageTypeSumGetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageCategory;
import kr.co.homeplus.admin.web.mileage.service.MileageStatisticService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/mileage/stat")
public class MileageStatisticController {

    final private MileageStatisticService mileageStatService;
    private final SettleCommonService settleCommonService;

    public MileageStatisticController(MileageStatisticService mileageStatService, SettleCommonService settleCommonService) {
        this.mileageStatService = mileageStatService;
        this.settleCommonService = settleCommonService;
    }
    /**
     * 결제관리 > 마일리지 관리 > 마일리지 통계 (SETTLE-6)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dailyMileageSum", method = RequestMethod.GET)
    public String dailyMileageSumMain(Model model) throws Exception {
        model.addAttribute("mileageKind", Arrays.asList(MileageSaveType.values()));

        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.createForGroup("mileageListBaseInfo", DailyMileageAmtSumGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("mileageDetailBaseInfo", DailyMileageTypeSumGetDto.class));

        return "/mileage/dailyMileageSum";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 통계 > 일/월별 마일리지 잔액 조회
     * @return DailyMileageAmtSumSetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyMileageSummary.json", method = RequestMethod.GET)
    public List<DailyMileageAmtSumGetDto> getDailyMileageSummary(DailyMileageAmtSumSetDto listParamDto) throws Exception {
        return mileageStatService.getDailyMileageSummary(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 마일리지 상세내역 조회
     * @return DailyMileageAmtSumSetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getDailyMileageTypeSummary.json", method = RequestMethod.GET)
    public List<DailyMileageTypeSumGetDto> getDailyMileageTypeSummary(DailyMileageAmtSumSetDto listParamDto) throws Exception {
        return mileageStatService.getDailyMileageTypeSummary(listParamDto);
    }

}