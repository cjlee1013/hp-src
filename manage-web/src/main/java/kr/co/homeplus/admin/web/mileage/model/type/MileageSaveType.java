package kr.co.homeplus.admin.web.mileage.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum MileageSaveType {
    //마일리지 적립 종류 mileage_kind
//    MILEAGE_KIND_SAVE_01("01", "주문적립"),
    MILEAGE_KIND_SAVE_02("02", "이벤트적립"),
    MILEAGE_KIND_SAVE_03("03", "고객보상"),
    MILEAGE_KIND_SAVE_04("04", "페이백"),
    MILEAGE_KIND_SAVE_05("05", "주문취소환원"),
//    MILEAGE_KIND_SAVE_11("11", "일반리뷰적립"),
//    MILEAGE_KIND_SAVE_12("12", "포토리뷰적립"),
    MILEAGE_KIND_SAVE_90("90", "행사적립"),
    MILEAGE_KIND_SAVE_99("99", "테스트");

    private String code;
    private String name;
}
