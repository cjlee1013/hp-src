package kr.co.homeplus.admin.web.escrow.model;

import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExtractCommonSetDto {
    // 추출자 사번
    String empId;
    // 메뉴명
    String menuNm;
    // 구분(조회/엑셀저장)
    String extractType;
    // 추출일시
    String extractDt;

    @Builder
    public ExtractCommonSetDto(String empId, String menuNm, String extractType) {
        this.empId = empId;
        this.menuNm = menuNm;
        this.extractType = extractType;
        this.extractDt = DateTimeUtil.getNowYmdHis();
    }
}
