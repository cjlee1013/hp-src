package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractAssistancePaymentInfoDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractPaymentInfoDto;
import kr.co.homeplus.admin.web.escrow.model.extract.OrderDataExtractSelectDto;
import kr.co.homeplus.admin.web.escrow.service.OrderDataExtractService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 주문결제통계 > 지불 수단별 판매현황
 */
@Controller
@RequestMapping("/escrow/extract")
public class PaymentInfoExtractController {
    private final LoginCookieService loginCookieService;
    private final OrderDataExtractService orderDataExtractService;

    public PaymentInfoExtractController(LoginCookieService loginCookieService, OrderDataExtractService orderDataExtractService) {
        this.loginCookieService = loginCookieService;
        this.orderDataExtractService = orderDataExtractService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/paymentInfoExtractMain")
    public String paymentInfoExtractMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("paymentInfoExtractGridBaseInfo", ExtractPaymentInfoDto.class));
        model.addAllAttributes(RealGridHelper.create("assistancePaymentInfoExtractGridBaseInfo", ExtractAssistancePaymentInfoDto.class));
        return "/escrow/paymentInfoExtractMain";
    }

    /**
     * 결제수단별 결제정보 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getPaymentInfoListByMethod.json")
    public List<ExtractPaymentInfoDto> getPaymentInfoListByMethod(@ModelAttribute OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.PAYMENT_INFO_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return orderDataExtractService.getPaymentInfoListByMethod(orderDataExtractSelectDto);
    }

    /**
     * 보조결제수단 결제정보 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getAssistancePaymentInfoList.json")
    public List<ExtractAssistancePaymentInfoDto> getAssistancePaymentInfoList(@ModelAttribute OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        return orderDataExtractService.getAssistancePaymentInfoList(orderDataExtractSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/savePaymentInfoExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.PAYMENT_INFO_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractService.saveExtractHistory(extractCommonSetDto);
    }
}
