package kr.co.homeplus.admin.web.search.controller;

import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementBestGetParam;
import kr.co.homeplus.admin.web.search.model.DataManagementBestGridDto;
import kr.co.homeplus.admin.web.search.service.DataManagementBestService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/best")
@RequiredArgsConstructor
public class DataManagementBestController {


    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final DataManagementBestService bestService;

    @GetMapping(value = {"/main"})
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("bestGridBaseInfo", DataManagementBestGridDto.BestSearchList.class));
        return "/search/dataManagementBestMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementBestGetParam param) {
        return bestService.getList(param);
    }
}
