package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListSetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgReceivableGetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleSetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleSumGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PgSettleService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 정산관리 > PG정산관리
     */
    public List<PgSettleSumGetDto> getPgSettleSum(PgSettleSetDto pgSettleSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgSettleSetDto,
            "/admin/pgSettle/getPgSettleSum",
            new ParameterizedTypeReference<ResponseObject<List<PgSettleSumGetDto>>>(){}).getData();
    }

    public List<PgSettleDetailGetDto> getPgSettleDetail(PgSettleSetDto pgSettleSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgSettleSetDto,
            "/admin/pgSettle/getPgSettleDetail",
            new ParameterizedTypeReference<ResponseObject<List<PgSettleDetailGetDto>>>(){}).getData();
    }

    public List<PgReceivableGetDto> getPgReceivable(PgSettleSetDto pgSettleSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgSettleSetDto,
            "/admin/pgSettle/getPgReceivable",
            new ParameterizedTypeReference<ResponseObject<List<PgReceivableGetDto>>>(){}).getData();
    }
}
