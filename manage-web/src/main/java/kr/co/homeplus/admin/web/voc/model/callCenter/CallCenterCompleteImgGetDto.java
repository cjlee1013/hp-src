package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 상담정보이미지 DTO")
public class CallCenterCompleteImgGetDto {
  @ApiModelProperty(notes = "파일명")
  @JsonProperty("fileName")
  private String fileName;

  @ApiModelProperty(notes = "파일경로")
  @JsonProperty("fileId")
  private String fileId;
}