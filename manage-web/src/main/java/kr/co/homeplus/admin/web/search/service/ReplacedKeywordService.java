package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.ReplacedKeywordGetParamDto;
import kr.co.homeplus.admin.web.search.model.ReplacedKeywordSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReplacedKeywordService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject insertReplacedKeyword(ReplacedKeywordSetParamDto param) {
        String apiUri = "/replaced/insertKeyword";
        param.setAdministratorId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }
    public ResponseObject updateReplacedKeyword(ReplacedKeywordSetParamDto param,Long replacedId) {
        String apiUri = "/replaced/updateKeyword/" + replacedId;
        param.setAdministratorId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }
    public ResponseObject deleteReplacedKeyword(Long replacedId) {
        String apiUri = "/replaced/deleteKeyword/" + replacedId;
        return getResponseObject(apiUri);
    }
    public ResponseObject getReplacedKeyword(ReplacedKeywordGetParamDto param) {
        String apiUri = "/replaced/getKeyword";
        return getResponseObject(apiUri, param);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
