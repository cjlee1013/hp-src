package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlyAdjustListGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATListGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATListSetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATOrderGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATOrderSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MonthlySalesVATService {

    private final ResourceClient resourceClient;

    /**
     * 세금계산서 관리 > 부가세신고내역 조회 > 메인 리스트 조회
     */
    public List<MonthlySalesVATListGetDto> getMonthlySalesVATList(MonthlySalesVATListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/monthlySalesVAT/getMonthlySalesVATList",
            new ParameterizedTypeReference<ResponseObject<List<MonthlySalesVATListGetDto>>>(){}).getData();
    }

    /**
     * 부가세신고내역 > 주문 상세내역
     */
    public List<MonthlySalesVATOrderGetDto> getMonthlySalesVATOrder(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/monthlySalesVAT/getMonthlySalesVATOrder",
            new ParameterizedTypeReference<ResponseObject<List<MonthlySalesVATOrderGetDto>>>(){}).getData();
    }

    /**
     * 부가세신고내역 > 정산조정 상세내역
     */
    public List<MonthlyAdjustListGetDto> getMonthlyAdjustList(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/monthlySalesVAT/getMonthlyAdjustList",
            new ParameterizedTypeReference<ResponseObject<List<MonthlyAdjustListGetDto>>>(){}).getData();
    }
}
