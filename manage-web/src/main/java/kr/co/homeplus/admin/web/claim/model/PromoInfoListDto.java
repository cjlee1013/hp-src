package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "클레임상세정보 - 차감 행사정보 팝업")
@RealGridOptionInfo(checkBar = false)
public class PromoInfoListDto {
    @ApiModelProperty(value = "상품번호", position = 1)
    @RealGridColumnInfo(headText = "상품번호", width = 130)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    @RealGridColumnInfo(headText = "상품명", width = 300)
    private String itemName;

    @ApiModelProperty(value = "행사종류", position = 3)
    @RealGridColumnInfo(headText = "행사종류", width = 300)
    private String promoType;

    @ApiModelProperty(value = "행사번호", position = 4)
    @RealGridColumnInfo(headText = "행사번호",  width = 150)
    private String promoNo;

    @ApiModelProperty(value = "행사할인금액", position = 5)
    @RealGridColumnInfo(headText = "행사할인금액", columnType = RealGridColumnType.PRICE, width = 130, fieldType = RealGridFieldType.NUMBER)
    private String promoDiscountAmt;
}
