package kr.co.homeplus.admin.web.item.model.market;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//외부연동 설정관리 상품 상세조회
public class MarketItemSettingGetDto {

	//상품번호
	private String itemNo;

	//상품명
	private String itemNm;

	//설정정보 리스트
	private List<MarketItemProviderGetDto> providerList;

}
