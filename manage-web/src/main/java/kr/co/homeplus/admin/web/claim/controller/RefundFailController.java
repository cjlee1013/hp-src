package kr.co.homeplus.admin.web.claim.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.admin.web.claim.service.RefundFailService;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/refundFail")
@RequiredArgsConstructor
public class RefundFailController {

    private final CodeService codeService;

    private final RefundFailService refundFailService;

    /**
     * 환불실패대상 조회 Main Page
     */
    @GetMapping("/refundFailMain")
    public String claimMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("refundFailListGridBaseInfo", RefundFailListGetDto.class));
        return "/claim/refundFailMain";
    }

    /**
     * 환불실패대상 리스트 조회
     */
    @GetMapping("/getRefundFailList.json")
    @ResponseBody
    public List<RefundFailListGetDto> getRefundFailList(@ModelAttribute RefundFailListSetDto refundFailListSetDto) throws Exception {
        return refundFailService.getRefundFailList(refundFailListSetDto);
    }

    /**
     * 환불완료 요청
     * @param requestRefundCompleteSetDto
     */
    @PostMapping("/reqRefundComplete.json")
    @ResponseBody
    public ResponseObject<RequestRefundCompleteGetDto> reqRefundComplete(@RequestBody RequestRefundCompleteSetDto requestRefundCompleteSetDto) throws Exception {
        return refundFailService.reqRefundComplete(requestRefundCompleteSetDto);
    }

    /**
     * 환불완료 팝업창
     */
    @GetMapping("/popup/getRefundCompletePop")
    public String refundCompletePop(Model model, HttpServletRequest request) throws Exception {
        return "/claim/pop/refundCompletePop";
    }


}
