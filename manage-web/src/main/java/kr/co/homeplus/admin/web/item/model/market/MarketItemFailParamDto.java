package kr.co.homeplus.admin.web.item.model.market;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("외부연동현황관리 parameter")
public class MarketItemFailParamDto {

    //외부연동 사이트
    private String schPartner;

    //검색타입
    private String schType;

    //검색키워드
    private String schKeyword;

    //검색 시작일
    private String searchStartDt;

    //검색 종료일
    private String searchEndDt;

    //실패사유
    private String failReason;

}