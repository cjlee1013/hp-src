package kr.co.homeplus.admin.web.statistics.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.DiscountStatistics;
import kr.co.homeplus.admin.web.statistics.model.DiscountStatisticsSelect;
import kr.co.homeplus.admin.web.statistics.service.CardDiscountService;
import kr.co.homeplus.admin.web.statistics.service.StatisticsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 프로모션통계 > 카드즉시할인통계
 */
@Controller
@RequestMapping("/escrow/statistics")
public class CardDiscountController {

    private final AuthorityService authorityService;

    @Autowired
    private CodeService codeService;

    @Autowired
    private CardDiscountService cardDiscountService;

    @Autowired
    private LoginCookieService loginCookieService;

    @Autowired
    private StatisticsCommonService statisticsCommonService;

    private static final List<RealGridColumn> CARD_DISCOUNT_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption CARD_DISCOUNT_GRID_OPTION = new RealGridOption("fill", false, false, false);

    private static final List<RealGridColumn> CARD_DISCOUNT_DETAIL_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption CARD_DISCOUNT_DETAIL_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public CardDiscountController(AuthorityService authorityService) {
        this.authorityService = authorityService;

        CARD_DISCOUNT_GRID_HEAD.add(new RealGridColumn("storeType", "storeType", "점포유형", RealGridColumnType.BASIC, 50, true, true));
        CARD_DISCOUNT_GRID_HEAD.add(new RealGridColumn("discountNo", "discountNo", "카드할인번호", RealGridColumnType.BASIC, 50, true, true));
        CARD_DISCOUNT_GRID_HEAD.add(new RealGridColumn("mallType", "mallType", "상품유형", RealGridColumnType.BASIC, 50, true, true));
        CARD_DISCOUNT_GRID_HEAD.add(new RealGridColumn("discountNm", "discountNm", "카드할인명", RealGridColumnType.NAME, 50, true, true));

        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("discountNo", "discountNo", "카드할인번호", RealGridColumnType.BASIC, 50, false, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("mallType", "mallType", "상품유형", RealGridColumnType.BASIC, 50, false, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("discountNm", "discountNm", "카드할인명", RealGridColumnType.BASIC, 50, false, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("orderDt", "orderDt", "일자", RealGridColumnType.DATE, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("orderCnt", "orderCnt", "주문건수", RealGridColumnType.NUMBER_C, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("payAmt", "payAmt", "주문금액", RealGridColumnType.NUMBER_C, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("discountAmt", "discountAmt", "할인금액", RealGridColumnType.NUMBER_C, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("claimCnt", "claimCnt", "취소건수", RealGridColumnType.NUMBER_C, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("claimAmt", "claimAmt", "취소금액", RealGridColumnType.NUMBER_C, 50, true, true));
        CARD_DISCOUNT_DETAIL_GRID_HEAD.add(new RealGridColumn("claimDiscountAmt", "claimDiscountAmt", "할인취소금액", RealGridColumnType.NUMBER_C, 50, true, true));

    }

    @GetMapping("/cardDiscountMain")
    public String cardDiscountMain(Model model) throws Exception {

        model.addAttribute("cardDiscountGridBaseInfo", new RealGridBaseInfo("cardDiscountGridBaseInfo", CARD_DISCOUNT_GRID_HEAD, CARD_DISCOUNT_GRID_OPTION).toString());
        model.addAttribute("cardDiscountDetailGridBaseInfo", new RealGridBaseInfo("cardDiscountDetailGridBaseInfo", CARD_DISCOUNT_DETAIL_GRID_HEAD, CARD_DISCOUNT_DETAIL_GRID_OPTION).toString());

        return "/statistics/cardDiscountMain";
    }

    @ResponseBody
    @GetMapping("/getCardDiscountList.json")
    public List<DiscountStatistics> getCardDiscountList(@ModelAttribute DiscountStatisticsSelect discountStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CARD_DISCOUNT_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        discountStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        return cardDiscountService.getCardDiscountList(discountStatisticsSelect);
    }

    @ResponseBody
    @GetMapping("/getCardDiscountDetailList.json")
    public List<DiscountStatistics> getCardDiscountDetailList(@ModelAttribute DiscountStatisticsSelect discountStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CARD_DISCOUNT_STATISTICS_DETAIL.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        discountStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        return cardDiscountService.getCardDiscountDetailList(discountStatisticsSelect);
    }

    /**
     * 엑셀 다운로드
     */
    @ResponseBody
    @GetMapping("/cardDiscount/saveExtractHistoryForExcelDown.json")
    public void saveExtractHistoryForExcelDown() throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CARD_DISCOUNT_STATISTICS_DETAIL.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());

        statisticsCommonService.saveExtractHistory(extractCommonSetDto);
    }
}
