package kr.co.homeplus.admin.web.manage.model.notice;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class NoticePartnerListSelectDto {

    @RealGridColumnInfo(headText = "판매자 ID", width = 50)
    private String partnerId;

    @RealGridColumnInfo(headText = "상호", columnType = RealGridColumnType.NAME, order = 1)
    private String partnerNm;

    @RealGridColumnInfo(headText = "업체명", columnType = RealGridColumnType.NAME, order = 2)
    private String businessNm;

    @RealGridColumnInfo(headText = "회원상태", width = 50, order = 3)
    private String partnerStatusNm;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 4)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 5, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "fileList", order = 6, hidden = true)
    private String fileList;
    @RealGridColumnInfo(headText = "noticeNo", order = 7, hidden = true)
    private String noticeNo;
    @RealGridColumnInfo(headText = "useYn", order = 8, hidden = true)
    private String useYn;


    private String partnerStatus;
    private String regId;

}
