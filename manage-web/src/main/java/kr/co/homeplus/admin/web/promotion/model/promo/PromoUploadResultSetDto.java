package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoUploadResultSetDto {
    private String successYn;
    private long uploadCount;
    private long successCount;
    private long failCount;

    List<PromoUploadSetDto> dataList;
}
