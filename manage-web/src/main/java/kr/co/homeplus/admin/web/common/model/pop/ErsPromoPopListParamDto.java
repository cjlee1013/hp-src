package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ErsPromoPopListParamDto {
	private String schDateType;

	private String schStartDt;

	private String schEndDt;

	private String schEventTargetType;

	private String schUseYn;

	private String schConfirmYn;

	private String schType;

	private String schValue;
}
