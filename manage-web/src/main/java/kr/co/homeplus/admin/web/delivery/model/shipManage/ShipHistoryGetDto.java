package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송 히스토리 정보")
public class ShipHistoryGetDto {
    @ApiModelProperty(notes = "처리일시")
    private String shipTranDt;

    @ApiModelProperty(notes = "배송상세")
    private String shipDetail;

    @ApiModelProperty(notes = "택배위치")
    private String shipWhere;

    @ApiModelProperty(notes = "배송기사명")
    private String shipMan;

    @ApiModelProperty(notes = "배송기사 연락처")
    private String shipManPhone;

    @ApiModelProperty(notes = "배송예정시간")
    private String shipEstmateDt;

    @ApiModelProperty(notes = "택배사")
    private String dlvNm;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;
}
