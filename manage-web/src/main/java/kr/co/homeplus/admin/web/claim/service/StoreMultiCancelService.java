package kr.co.homeplus.admin.web.claim.service;

import java.util.List;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.RequestStoreMultiCancelGetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.RequestStoreMultiCancelSetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StoreMultiCancelService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 결품/대체상품 리스트 조회
     * **/
    public List<StoreMultiCancelListGetDto> getStoreMultiCancelList(
        StoreMultiCancelListSetDto storeMultiCancelListSetDto) {

        //점포코드 추출
        //데이터가 있으면 점포사용자 없으면 본사 사용자로 구분
        if(!StringUtil.isEmpty(loginCookieService.getUserInfo().getStoreId())){
            storeMultiCancelListSetDto.setSchStoreId(loginCookieService.getUserInfo().getStoreId());
        }

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            storeMultiCancelListSetDto,
            "/claim/storeMultiCancel/getStoreMultiCancelList",
            new ParameterizedTypeReference<ResponseObject<List<StoreMultiCancelListGetDto>>>() {}
        ).getData();
    }

    /**
     * 점포일괄취소 요청
     * **/
    public ResponseObject<RequestStoreMultiCancelGetDto> reqStoreMultiCancel (RequestStoreMultiCancelSetDto requestStoreMultiCancelSetDto){
        requestStoreMultiCancelSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            requestStoreMultiCancelSetDto,
            "/claim/claimAllCancelReq",
            new ParameterizedTypeReference<ResponseObject<RequestStoreMultiCancelGetDto>>() {
            }
        );
    }
}
