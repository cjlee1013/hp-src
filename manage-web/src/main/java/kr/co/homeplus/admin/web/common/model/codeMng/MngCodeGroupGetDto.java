package kr.co.homeplus.admin.web.common.model.codeMng;

import com.fasterxml.jackson.annotation.JsonInclude;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class MngCodeGroupGetDto {
	@RealGridColumnInfo(headText = "유형코드", width = 120, sortable = true)
	private String gmcCd;
	@RealGridColumnInfo(headText = "이전시스템 상위코드", width = 120, hidden = true, sortable = true)
	private String lgcUpcodeCd;
	@RealGridColumnInfo(headText = "유형명", width = 120, sortable = true)
	private String gmcNm;
	private String regDt;
	private String chgDt;
	private String regId;
	private String chgId;
	private String regNm;
	private String chgNm;
}
