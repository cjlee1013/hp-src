package kr.co.homeplus.admin.web.message.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.enums.AffiliationType;
import kr.co.homeplus.admin.web.message.enums.ButtonType;
import kr.co.homeplus.admin.web.message.enums.StoreType;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.model.ContentConditionRequest;
import kr.co.homeplus.admin.web.message.model.ContentInfo;
import kr.co.homeplus.admin.web.message.model.ContentInfoGrid;
import kr.co.homeplus.admin.web.message.model.ContentSearchRequest;
import kr.co.homeplus.admin.web.message.service.ContentSupportService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/message/admin/content")
@RequiredArgsConstructor
@Slf4j
public class ContentAdminController {

    private final CodeService codeService;

    private final ResourceClient resourceClient;

    private final ContentSupportService contentSupportService;

    private final Map<String, Object> modelMap = RealGridHelper.create(
        "contentInfoGridBaseInfo", ContentInfoGrid.class);

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgUrl;

    @GetMapping("/main")
    public String main(final Model model) {
        codeService.getCodeModel(model, "message_work_status");
        Map<String, String> buttonInfo = ButtonType.getMap();
        buttonInfo.put("", "없음");

        model.addAllAttributes(modelMap);
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("buttonInfo", buttonInfo);
        model.addAttribute("storeType", StoreType.getMap());
        model.addAttribute("affiliationType", AffiliationType.getMap());
        model.addAttribute("workType", WorkType.getMap());
        return "/message/contentMain";
    }

    @PostMapping("/getAllContents.json")
    @ResponseBody
    public ResponseObject<List<ContentInfo>> selectAllContentInfo() {
        ResourceClientRequest<List<ContentInfo>> request = ResourceClientRequest.<List<ContentInfo>>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/list/all")
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @PostMapping("/getWorkContents.json")
    @ResponseBody
    public ResponseObject<List<ContentInfo>> selectWorkContentInfo(
        @RequestBody @Valid final ContentConditionRequest contentConditionRequest) {
        ResourceClientRequest<List<ContentInfo>> request = ResourceClientRequest.<List<ContentInfo>>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/list/work")
            .postObject(contentConditionRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @PostMapping("/getContent.json")
    @ResponseBody
    public ResponseObject<ContentInfo> selectContentInfoOnlyId(
        @RequestParam("contentSeq") @Valid final Long contentSeq) {
        ResourceClientRequest<ContentInfo> request = ResourceClientRequest.<ContentInfo>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/id/" + contentSeq)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @GetMapping("/getContentTemplateCode.json")
    @ResponseBody
    public ResponseObject<ContentInfo> selectContentInfoTemplateCode(
        @RequestParam("templateCode") @Valid final String templateCode) {
        ResourceClientRequest<ContentInfo> request = ResourceClientRequest.<ContentInfo>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/code/" + templateCode)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @PostMapping("/searchContents.json")
    @ResponseBody
    public ResponseObject<List<ContentInfo>> searchContentInfo(
        @RequestBody @Valid final ContentSearchRequest contentSearchRequest) {
        ResourceClientRequest<List<ContentInfo>> request = ResourceClientRequest.<List<ContentInfo>>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/list/search")
            .postObject(contentSearchRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @PostMapping("/setContent.json")
    @ResponseBody
    public ResponseObject<ContentInfo> insertContentInfo(
        @RequestBody @Valid final ContentInfo contentInfo) {
        return contentSupportService.insertContentInfo(contentInfo);
    }

    @PostMapping("/editContent.json")
    @ResponseBody
    public ResponseObject<Long> updateContentInfo(
        @RequestBody @Valid final ContentInfo contentInfo) {
        return contentSupportService.updateContentInfo(contentInfo);
    }

    @PostMapping("/deleteContent.json")
    @ResponseBody
    public ResponseObject<Long> deleteContentInfo(
        @RequestParam("contentSeq") @Valid final Long contentSeq) {
        ResourceClientRequest<Long> request = ResourceClientRequest.<Long>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/content/delete/" + contentSeq)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }

    @PostMapping("/deleteAttachment.json")
    @ResponseBody
    public ResponseObject<String> deleteAttachment(
        @RequestParam("contentSeq") @Valid final Long contentSeq) {
        return contentSupportService.deleteAttachment(contentSeq);
    }

    @PostMapping("/setAttachment.json")
    @ResponseBody
    public ResponseObject<String> setAttachment(@ModelAttribute("file") final MultipartFile file)
        throws IOException {
        return contentSupportService.setAttachment(file);
    }
}
