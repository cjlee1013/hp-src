package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipPolicyGetDto {
    private int shipPolicyNo;
    private String shipPolicyNm;
    private String useYn;
}
