package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DictionaryRegGetDto {
    private Info info;

    @Getter @Setter
    public class Info {
        private Integer wordId;
        private String wordName;
        private String senseTag;
        private boolean useFlag;
        private boolean stopFlag;
        private String note;
        private String dicType;
        public List<String> getDicType() {
            return Arrays.asList(dicType.split(","));
        }
        private String pos;
        private String nounAttr;
        public List<String> getNounAttr() {
            return Arrays.asList(nounAttr.split(","));
        }
        private String verbAttr;
        private String neType;
        public List<String> getNeType() {
            return Arrays.asList(neType.split(","));
        }
        private String domain;
        public List<String> getDomain() {
            return Arrays.asList(domain.split(","));
        }
        private String language;
        public List<String> getLanguage() {
            return Arrays.asList(language.split(","));
        }
        private boolean dontSplitComp;
        private String description;
        private String example;
        private String isGeneral;
    }
}
