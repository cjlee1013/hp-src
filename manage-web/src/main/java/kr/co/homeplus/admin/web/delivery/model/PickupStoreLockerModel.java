package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "픽업점포락커 model")
@Getter
@Setter
@EqualsAndHashCode
public class PickupStoreLockerModel {

    @ApiModelProperty(value = "점포ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "장소번호", position = 2)
    private String placeNo;

    @ApiModelProperty(value = "락커번호", position = 3)
    private String lockerNo;

    @ApiModelProperty(value = "사용여부", position = 4)
    private String useYn;
}





