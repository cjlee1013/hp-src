package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 기분석 구성 정보")
public class DictionaryInsertPreAnalysisSetParamDto {
    @ApiModelProperty(notes = "매칭속성", position = 1, required = true)
    private PreAnalysisAttrDto attr;
    @ApiModelProperty(notes = "구성리스트", position = 2, required = true)
    private List<PreAnalysisDto> list;
    private String userId;
}
