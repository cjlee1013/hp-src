package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@ApiModel(description = "마일리지 난수번호 관리 - 유저정보")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class MileageCouponUserInfoDto {
    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호")
    private String userMaskingNo;
    @ApiModelProperty(notes = "회원ID")
    @RealGridColumnInfo(headText = "회원ID")
    private String userId;
    @ApiModelProperty(notes = "이름")
    @RealGridColumnInfo(headText = "이름")
    private String userNm;
    @ApiModelProperty(notes = "난수번호")
    @RealGridColumnInfo(headText = "난수번호", width = 200)
    private String couponNo;
    @ApiModelProperty(notes = "발급일시")
    @RealGridColumnInfo(headText = "발급일시", width = 200)
    private String issueDt;
    @ApiModelProperty(notes = "사용일시")
    @RealGridColumnInfo(headText = "사용일시", width = 200)
    private String useDt;
    @ApiModelProperty(notes = "회수")
    @RealGridColumnInfo(headText = "회수", width = 80)
    private String recoveryYn;
    @ApiModelProperty(notes = "발급주체")
    @RealGridColumnInfo(headText = "발급주체")
    private String regId;
    @ApiModelProperty(notes = "회수자")
    @RealGridColumnInfo(headText = "회수자")
    private String recoveryId;
    @ApiModelProperty(notes = "회수일")
    @RealGridColumnInfo(headText = "회수일", width = 200)
    private String recoveryDt;
    @ApiModelProperty(notes = "쿠폰정보Seq")
    @RealGridColumnInfo(headText = "쿠폰정보Seq", hidden = true)
    private long couponInfoSeq;
    @ApiModelProperty(notes = "회원번호")
    @RealGridColumnInfo(headText = "회원번호NON", hidden = true)
    private String userNo;

    public void setUserNo(String userNo){
        if(StringUtils.isEmpty(userNo)){
            this.userNo = "-";
        } else {
            this.userNo = userNo;
        }
    }

    public void setUserId(String userId){
        if(StringUtils.isEmpty(userId)){
            this.userId = "-";
        } else {
            this.userId = userId;
        }
    }

    public void setUserNm(String userNm){
        if(StringUtils.isEmpty(userNm)){
            this.userNm = "-";
        } else {
            this.userNm = userNm;
        }
    }

    public void setUserMaskingNo(String userMaskingNo){
        if(StringUtils.isEmpty(userMaskingNo)){
            this.userMaskingNo = "-";
        } else {
            this.userMaskingNo = userMaskingNo;
        }
    }
}
