package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > 결제정책관리 search DTO")
public class PaymentPolicyManageSelectDto {
    private String schPeriod;
    private String schFromDt;
    private String schEndDt;
    private String schSiteType;
    private String schUseYn;
    private String schLcateCd;
    private String schMcateCd;
    private String schScateCd;
    private String schDcateCd;
    private String schCateCd;
    private String schType;
    private String schKeyword;
}