package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > BSD연동조회 전송 요청 DTO")
public class BsdLinkListSetDto {
    @ApiModelProperty(notes = "주문일(START)")
    private String schStartDt;

    @ApiModelProperty(notes = "주문일(END)")
    private String schEndDt;

    @ApiModelProperty(notes = "배송상태")
    private String schShipStatus;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "배송유형")
    private String schSendPlace;
}