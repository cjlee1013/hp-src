package kr.co.homeplus.admin.web.core.exception.handler;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.admin.web.common.exception.MemberSearchFailException;
import kr.co.homeplus.admin.web.common.exception.NotAllowedUrlDownloadFileException;
import kr.co.homeplus.admin.web.common.exception.UrlDownloadFileFailedException;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.config.AdminProperties;
import kr.co.homeplus.admin.web.core.constants.LimitConstants;
import kr.co.homeplus.admin.web.core.exception.AdminFirstLoginException;
import kr.co.homeplus.admin.web.core.exception.AdminLoginLockedException;
import kr.co.homeplus.admin.web.core.exception.CertificationException;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.LoginCookieServiceException;
import kr.co.homeplus.admin.web.core.exception.LoginIdSaveCookieServiceException;
import kr.co.homeplus.admin.web.core.exception.NotAllowedAccessIpException;
import kr.co.homeplus.admin.web.core.exception.RoleServiceException;
import kr.co.homeplus.admin.web.core.exception.SessionNotFoundException;
import kr.co.homeplus.admin.web.core.exception.ValidateAuthorizedMenuException;
import kr.co.homeplus.admin.web.core.service.impl.ExcelUploadServiceException;
import kr.co.homeplus.admin.web.core.service.impl.ListLimitServiceException;
import kr.co.homeplus.admin.web.core.service.impl.MaintenanceException;
import kr.co.homeplus.admin.web.user.exception.ConsumerManageException;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.exception.ResourceTimeoutException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 어드민 내 글로벌로 예외 헨들러를 관리하는 클래스
 */
@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionControllerAdvice {

    private final AdminProperties adminProperties;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    /**
     * 어드민 내 {@link Throwable}로 발생되는 예외를 처리하는 메소드로 명시적이지 않는 모든 예외를 처리한다.<p>
     * <p>
     * 응답 타입은 요청한 url의 suffix가 '.json'이면 json 타입으로 리턴, 아닐 경우 error Page로 이동한다.<br> 응답 내용은 에러내용을 담은
     * {@link ResponseObject}와 {@link HttpStatus}를 전달한다.<p>
     * <p>
     *
     * @param req HttpServletRequest
     * @param ex  Exception
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     * @see ResponseObject
     */
    @ExceptionHandler(Throwable.class)
    public Object handleServerError(final HttpServletRequest req, final Exception ex) {
        log.error("Throwable Error! <uri:{}>, <trace:{}>", req.getRequestURI(),
            ExceptionUtils.getStackTrace(ex));

        //응답할 ResponseObject 설정
        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9001.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9001.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), ex, responseObject,
            HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Resttemplate 사용 시 resource api 호출 이거나, gateway 를 통한 호출 시 발생하는 예외를 처리하는 핸들러<p> <p/> 전달되는
     * {@link Exception}의 type에 따라 리턴되는 값이 변경된다. 세부내용은 하단의 내용을 참조한다. <p/>
     * <pre>
     * 1. {@link RestClientResponseException} 이 발생 할 경우
     *  - 리턴 : status, ResponseBody 그대로 전달
     *
     * 2. 기타 그외의 exception
     *  - 리턴 : status만 변경(500), ResponseObject를 전달
     *   * unknownHostException, {@link java.io.IOException} 등의 기타 {@link Exception} 처리
     *   * 에러 내용을 {@link ResponseObject}로 담아 전달
     * </pre>
     *
     * @param req HttpServletRequest
     * @param e  ResourceClientException
     * @return responseEntity
     * @see RestClientResponseException
     * @see ResourceClientException
     */
    @ExceptionHandler(ResourceClientException.class)
    public Object handleResourceClientException(final HttpServletRequest req,
        final HttpServletResponse res, final ResourceClientException e) {
        final Throwable cause = e.getCause();

        /*
         * 1. RestClientResponseException 예외가 발생 할 경우
         *  1.1 url이 .json 일 경우 : status, ResponseBody 그대로 전달
         *  1.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        if (cause instanceof RestClientResponseException) {
            res.setStatus(e.getHttpStatus());
            final String responseBodyAsString = ((RestClientResponseException) cause)
                .getResponseBodyAsString();

            log.error(
                "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseBody:{}>",
                req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseBodyAsString);

            return returnResponse(req.getRequestURI(), e, e.getResponseBody(), HttpStatus.valueOf(e.getHttpStatus()));
        }
        /*
         * 2. 기타 그외의 exception
         *  2.1 url이 .json 일 경우 : status 지정(500), 에러 내용은 {@link ResponseObject}로 담아 전달
         *  2.2 url이 .json이 아닐 경우 : 에러 페이지로 이동
         */
        else {
            res.setStatus(500);
            ResponseObject<String> responseObject = setResponseForNotRestClientResponseException(e
            );

            log.error(
                "ResourceClientException Error! <uri:{}>,<resourceUrl:{}>,<httpStatus:{}>,<responseBody:{}> <trace:{}>",
                req.getRequestURI(), e.getResourceUrl(), e.getHttpStatus(), responseObject,
                ExceptionUtils.getStackTrace(e));
            return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * RestClientResponseException 이 아닌 Exception의 유형에 따라 에러코드와 문구가 담긴 ResponseObject 를 반환합니다.
     * @param e {@link ResourceClientException}
     * @return 에러코드, 문구가 담긴 ResponseObject 를 반환합니다.
     */
    private ResponseObject<String> setResponseForNotRestClientResponseException(
        final ResourceClientException e) {

        String returnCode;
        String returnMessage;

        if ( e.getCause() instanceof ResourceAccessException) {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9103.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9103.getDesc();
        } else {
            returnCode = ExceptionCode.SYS_ERROR_CODE_9102.getCode();
            returnMessage = ExceptionCode.SYS_ERROR_CODE_9102.getDesc();
        }
        return ResourceConverter
            .toResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR, returnCode, returnMessage);
    }

    /**
     * {@link ResourceTimeoutException} 발생 시 예외를 처리하는 핸들러 입니다.<p>
     * <p>
     * {@link ResourceTimeoutException}은 SocketTimeoutException, ConnectTimeoutException 발생 할 경우
     * throw 하는 {@link Exception} 입니다.<br>
     * <pre>
     *  1. url이 .json 일 경우 : status 지정(408), 에러 내용은 {@link ResponseObject}로 담아 전달
     *  2. url이 .json이 아닐 경우 : 에러 페이지로 이동
     * </pre>
     *
     * @param req HttpServletRequest
     * @param ex  ResourceTimeoutException
     * @return responseEntity
     */
    @ExceptionHandler(ResourceTimeoutException.class)
    public Object handleResourceTimeoutException(final HttpServletRequest req,
        final ResourceTimeoutException ex) {
        log.error("ResourceTimeoutException Error! <uri:{}>,<httpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.REQUEST_TIMEOUT, ExceptionUtils.getStackTrace(ex));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.REQUEST_TIMEOUT)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9104.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9104.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), ex, responseObject, HttpStatus.REQUEST_TIMEOUT);
    }

    /**
     * 로그인 세션 없을 경우 예외 처리.
     *
     * @param req HttpServletRequest
     * @param ex  SessionNotFoundException
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @Deprecated
    @ExceptionHandler(SessionNotFoundException.class)
    public Object sessionNotFoundException(final HttpServletRequest req,
        final SessionNotFoundException ex) {
        log.error(
            "SessionNotFoundException Error! <uri:{}>,<HttpStatus:{}>,<Exception:{}>,<message:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ex.getClass().toString(),
            ex.getMessage());

        ResponseObject<String> responseObject = setResponseObjectForException(ex,
            HttpStatus.UNAUTHORIZED);
        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setStatus(HttpStatus.UNAUTHORIZED);
            mav.addObject("certificationRedirectParam",
                adminProperties.getCertificationRedirectUrl());
            mav.setViewName("/core/error/sessionError");
            return mav;
        }
    }

    /**
     * 롤 권한이 없을 경우 예외처리
     *
     * @param req HttpServletRequest
     * @param ex  HasNotRoleException
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(HasNotRoleException.class)
    public Object hasNotRoleException(final HttpServletRequest req, final HasNotRoleException ex) {
        log.error("hasNotRoleException Error! <Exception:{}>,<message:{}>",
            ex.getClass().toString(),
            ex.getMessage());

        ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.FORBIDDEN)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9204.getCode())
            .returnMessage(StringUtils
                .defaultString(ex.getMessage(), ExceptionCode.SYS_ERROR_CODE_9204.getDesc()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.FORBIDDEN);
        } else {
            ModelAndView mav = new ModelAndView();
            setErrorModelAndView(mav, HttpStatus.FORBIDDEN, ExceptionUtils.getStackTrace(ex),
                responseObject);
            mav.addObject("certificationRedirectParam",
                adminProperties.getCertificationRedirectUrl());
            mav.setViewName("/core/error/error403");
            return mav;
        }
    }

    /**
     * 404 error not found 예외처리<p>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  NoHandlerFoundException
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object noHandlerFoundException(final HttpServletRequest req,
        final NoHandlerFoundException ex) {
        log.error("404 Not Found Error! <uri:{}>,<httpMethod:{}>,<HttpStatus:{}>",
            req.getRequestURI(), req.getMethod(), HttpStatus.NOT_FOUND.value());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .data(null)
            .status(HttpStatus.NOT_FOUND)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9105.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9105.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), ex, responseObject, HttpStatus.NOT_FOUND,
            "/core/error/error404");
    }

    /**
     * {@link ExcelUploadServiceException} 예외처리<p> 엑셀 업로드시 발생된 {@link Exception} 처리
     *
     * @param req {@link HttpServletRequest}
     * @param ex  ExcelUploadServiceException
     * @return 'httpStatus'와 json 타입으로 return
     */
    @ExceptionHandler(ExcelUploadServiceException.class)
    public ResponseEntity<ResponseObject<String>> excelUploadServiceException(
        final HttpServletRequest req,
        final ExcelUploadServiceException ex) {
        log.error(
            "ExcelUploadServiceException ERROR! <uri:{}>,<httpMethod:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), req.getMethod(), ex.getHttpStatus(), ExceptionUtils.getStackTrace(ex));

        ResponseObject<String> responseObject = setResponseObjectForException(ex,
            ex.getHttpStatus());
        HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseObject, httpHeaders, ex.getHttpStatus());
    }

    /**
     * multipart 관련 에러를 처리하는 핸들러입니다.<p> 전달되는 {@link Exception}의 type에 따라 리턴되는 값이 변경됩니다. 분류내용은 하단의
     * 내용을 참조하시길 바랍니다. <p/>
     * <pre>
     * 1. 업로드 한 multipart의 사이즈가 application.yml에 설정 된 multipart 사이즈 크기를 초과할 경우
     *   - 리턴 : status : 413(PAYLOAD_TOO_LARGE), ResponseObject : 에러 정보
     *
     * 2. 그 외의 {@link Exception}
     *   - 리턴 : status : 500(INTERNAL_SERVER_ERROR), ResponseObject : 에러 정보
     * </pre>
     *
     * @param req {@link HttpServletRequest}
     * @param ex  {@link MultipartException}
     * @return Url의 suffix가 .json이면 'httpStatus'와 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(MultipartException.class)
    public Object multipartException(final HttpServletRequest req, final Exception ex) {
        final Throwable cause = ex.getCause();
        HttpStatus httpStatus;
        ResponseObject<String> responseObject;

        if (cause.getCause() instanceof FileUploadBase.SizeLimitExceededException) {
            httpStatus = HttpStatus.PAYLOAD_TOO_LARGE;
            log.error(
                "MultipartException ERROR - Payload Too Large!!! <uri:{}>,"
                    + "<httpMethod:{}>,<HttpStatus:{}>,<requestSize:{}>,<PermittedSize:{}>",
                req.getRequestURI(), req.getMethod(), httpStatus.value(),
                ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getActualSize(),
                ((FileUploadBase.SizeLimitExceededException) cause.getCause()).getPermittedSize());

            responseObject = ResponseObject.Builder.<String>builder()
                .status(httpStatus)
                .data(null)
                .returnCode(ExceptionCode.SYS_ERROR_CODE_9002.getCode())
                .returnMessage(ExceptionCode.SYS_ERROR_CODE_9002.getDesc())
                .build();
            return returnResponse(req.getRequestURI(), ex, responseObject, httpStatus);

        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            log.error(
                "MultipartException ERROR - Internal Server Error!!! <uri:{}>,<httpMethod:{}>,<HttpStatus:{}>",
                req.getRequestURI(), req.getMethod(), httpStatus.value());

            responseObject = ResponseObject.Builder.<String>builder()
                .status(httpStatus)
                .data(null)
                .returnCode(ExceptionCode.SYS_ERROR_CODE_9003.getCode())
                .returnMessage(ExceptionCode.SYS_ERROR_CODE_9003.getDesc())
                .build();
            return returnResponse(req.getRequestURI(), ex, responseObject, httpStatus);
        }
    }

    /**
     * 망분리 메뉴 접근시 허용되지 않은 아이피로 접근하는 경우 예외처리7
     *
     * @param req request
     * @param ex  exception
     * @return Url의 suffix가 .json이면 'httpStatus'와 json타입으로 return, 아닐 경우 망분리 에러 페이지로 이동
     */
    @ExceptionHandler(NotAllowedAccessIpException.class)
    public Object notAllowedAccessIpException(final HttpServletRequest req,
        final NotAllowedAccessIpException ex) {
        log.error("NotAllowedAccessIpException Error! <Exception:{}>,<message:{}>",
            ex.getClass().toString(), ex.getMessage());
        ResponseObject<String> responseObject = setResponseObjectForException(ex,
            HttpStatus.FORBIDDEN);

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.FORBIDDEN);
        } else {
            ModelAndView mav = new ModelAndView();
            setErrorModelAndView(mav, HttpStatus.FORBIDDEN, ExceptionUtils.getStackTrace(ex),
                responseObject);
            mav.addObject("certificationRedirectParam",
                adminProperties.getCertificationRedirectUrl());
            mav.setViewName("/core/error/errorNotAllowedAccessIp");
            return mav;
        }
    }

    /**
     * {@link ListLimitServiceException} 예외 처리<p> 리스트 검색 결과 제한 예외 처리
     */
    @ExceptionHandler(ListLimitServiceException.class)
    public ResponseEntity<ResponseObject<String>> listLimitServiceException(
        final HttpServletRequest req, final ListLimitServiceException ex) {
        log.error(
            "ListLimitServiceException ERROR! <uri:{}>,<httpMethod:{}>,<HttpStatus:{}>",
            req.getRequestURI(), req.getMethod(), ex.getHttpStatus());

        ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(ex.getHttpStatus()).data(LimitConstants.LIST_LIMIT_MSG).build();
        HttpHeaders httpHeaders = createHttpHeaders();

        return new ResponseEntity<>(responseObject, httpHeaders, ex.getHttpStatus());
    }

    @ExceptionHandler(MaintenanceException.class)
    public ModelAndView maintenanceExceptionError() {
        ModelAndView mav = new ModelAndView();
        mav.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        mav.setViewName("/core/error/error503");
        return mav;
    }

    /**
     * {@link org.springframework.web.bind.annotation.RequestParam}과 바인딩되는 파라미터가 null일 경우 예외처리
     * @param req HttpServletRequest
     * @param e MissingServletRequestParameterException
     * @see org.springframework.web.bind.annotation.RequestParam
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Object handleMissingServletRequestParameterException(final HttpServletRequest req,
        final MissingServletRequestParameterException e) {
        log.error("MissingServletRequestParameterException!<uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9301.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9301.getDesc())
            .build();

        HttpHeaders httpHeaders = createHttpHeaders();
        return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * {@link LoginCookieService} 에서 발생한 예외처리
     *
     * @param req HttpServletRequest
     * @param e   LoginCookieServiceException
     * @return ResponseEntity
     */
    @ExceptionHandler(LoginCookieServiceException.class)
    public Object cookieServiceException(final HttpServletRequest req,
        final LoginCookieServiceException e) {
        log.error("CookieServiceException! <uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(e.getMessage())
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9201.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9201.getDesc()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNAUTHORIZED);
        } else {
            return new ModelAndView(
                "redirect:" + adminProperties.getCertificationRedirectUrl());
        }
    }

    /**
     * {@link LoginIdSaveCookieServiceException} 예외처리
     *
     * @param req HttpServletRequest
     * @param e   LoginIdSaveCookieServiceException
     * @return ResponseEntity
     */
    @ExceptionHandler(LoginIdSaveCookieServiceException.class)
    public Object idSaveCookieServiceException(final HttpServletRequest req,
        final LoginIdSaveCookieServiceException e) {
        log.error("LoginIdSaveCookieServiceException! <uri:{}>,<trace:{}>",
            req.getRequestURI(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(e.getMessage())
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9209.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9209.getDesc()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            return new ResponseEntity<>(responseObject, createHttpHeaders(),
                HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("redirect:" + adminProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * 로그인 인증 실패 시 예외 처리
     *
     * @param req HttpServletRequest
     * @param e  CertificationException
     * @return Url의 suffix가 '.json'이면 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(CertificationException.class)
    public Object certificationException(final HttpServletRequest req,
        final CertificationException e, final RedirectAttributes redirectAttributes) {
        log.error("CertificationException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ExceptionUtils.getStackTrace(e));

        final Map<String, Object> data = Map.of("userId", StringUtils.defaultString(e.getLoginParam().getUserId()));

        final ResponseObject<Map<String, Object>> responseObject = ResponseObject.Builder.<Map<String, Object>>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(data)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9200.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9200.getDesc()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            redirectAttributes.addFlashAttribute("returnMessage", e.getMessage());
            redirectAttributes.addFlashAttribute("userId",
                StringUtils.defaultString(e.getLoginParam().getUserId()));
            mav.setViewName("redirect:" + adminProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * 인가 된 메뉴인지을 검증하는 인터셉터에서 발생 된 예외 처리
     *
     * @param req HttpServletRequest
     * @param e  ValidateAuthorizedMenuException
     * @return Url의 suffix가 '.json'이면 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(ValidateAuthorizedMenuException.class)
    public Object validateAuthorizedMenuException(final HttpServletRequest req,
        final ValidateAuthorizedMenuException e) {
        log.error("validateAuthorizedMenuException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR.value(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9203.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9203.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 컨슈머 관리 예외처리 핸들러
     * @param req HttpServletRequest
     * @param ex {@link ConsumerManageException}
     * @return
     */
    @ExceptionHandler(ConsumerManageException.class)
    public ResponseEntity<ResponseObject<String>> consumerManageException(
        final HttpServletRequest req, final ConsumerManageException ex) {
        log.error("ConsumerManageException ERROR! <uri:{}>,<message:{}>",
            req.getRequestURI(), ex.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .data(ex.getMessage())
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9304.getCode())
            .returnMessage(StringUtils
                .defaultString(ex.getMessage(), ex.getReturnCode().getMsg()))
            .build();
        final HttpHeaders httpHeaders = createHttpHeaders();

        return new ResponseEntity<>(responseObject, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * {@code @Valid}를 통해 유효성 검증시 발생되는 Validation Exception
     *
     * @param req HttpServletRequest
     * @param e MethodArgumentNotValidException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidExceptionHandler(final HttpServletRequest req
        , final MethodArgumentNotValidException e) {
        final List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        log.error("MethodArgumentNotValidException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9302.getCode())
            .returnMessage(StringUtils.defaultString(allErrors.get(0).getDefaultMessage(),
                ExceptionCode.SYS_ERROR_CODE_9302.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * 파라미터 바인딩시 발생 되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e NumberFormatException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler({BindException.class})
    public Object handleBindException(final HttpServletRequest req, final BindException e) {
        final List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        log.error("BindException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9302.getCode())
            .returnMessage(StringUtils.defaultString(allErrors.get(0).getDefaultMessage(),
                ExceptionCode.SYS_ERROR_CODE_9302.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * NumberFormatException 를 처리하는 handler
     *
     * @param req HttpServletRequest
     * @param e NumberFormatException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler({NumberFormatException.class})
    public Object handleNumberFormatException(final HttpServletRequest req,
        final NumberFormatException e) {
        log.error("NumberFormatException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9303.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9303.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * {@code @RequestBody}를 통해 바인딩 된 파라미터가 null 일 경우 발생 된 예외 처리
     *
     * @param req HttpServletRequest
     * @param e {@link HttpMessageNotReadableException}
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Object httpMessageNotReadableExceptionHandler(final HttpServletRequest req
        , final HttpMessageNotReadableException e) {
        log.error("HttpMessageNotReadableException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.BAD_REQUEST.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.BAD_REQUEST)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9303.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9303.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.BAD_REQUEST);
    }

    /**
     * {@code @RequestBody}를 통해 바인딩 된 파라미터가 null 일 경우 발생 된 예외 처리
     *
     * @param req HttpServletRequest
     * @param e {@link HttpMessageNotReadableException}
     * @return 에러 내용을 ResponseEntity 담아 리턴
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Object httpRequestMethodNotSupportedExceptionHandler(final HttpServletRequest req
        , final HttpRequestMethodNotSupportedException e) {
        log.error("HttpRequestMethodNotSupportedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.METHOD_NOT_ALLOWED.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.METHOD_NOT_ALLOWED)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9106.getCode())
            .returnMessage(ExceptionCode.SYS_ERROR_CODE_9106.getDesc())
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.METHOD_NOT_ALLOWED);
    }

    /**
     * 권한 체크 실패 시 예외 처리
     *
     * @param req HttpServletRequest
     * @param e  RoleServiceException
     * @return Url의 suffix가 '.json'이면 json 타입으로 return, 아닐 경우 error Page로 이동
     */
    @ExceptionHandler(RoleServiceException.class)
    public Object roleServiceExceptionHandler(final HttpServletRequest req,
        final RoleServiceException e) {
        log.error("RoleServiceException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR.value(), ExceptionUtils.getStackTrace(e));

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9205.getCode())
            .returnMessage(StringUtils
                .defaultString(e.getMessage(), ExceptionCode.SYS_ERROR_CODE_9205.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 어드민 로그인 계정잠금 예외처리 핸들러
     * @param req HttpServletRequest
     * @param e {@link AdminLoginLockedException}
     * @param redirectAttributes {@link RedirectAttributes}
     * @return
     */
    @ExceptionHandler(AdminLoginLockedException.class)
    public Object adminLoginLockedExceptionHandler(final HttpServletRequest req,
        final AdminLoginLockedException e, final RedirectAttributes redirectAttributes) {

        log.error("AdminLoginLockedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ExceptionUtils.getStackTrace(e));

        //response data
        final Map<String, Object> data = Map
            .of("userId", StringUtils.defaultString(e.getLoginParam().getUserId()));

        final ResponseObject<Map<String, Object>> responseObject = ResponseObject.Builder
            .<Map<String, Object>>builder()
            .status(HttpStatus.UNAUTHORIZED)
            .data(data)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9206.getCode())
            .returnMessage(StringUtils
                .defaultString(ExceptionCode.SYS_ERROR_CODE_9206.getDesc(), e.getMessage()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            return new ResponseEntity<>(responseObject, createHttpHeaders(), HttpStatus.UNAUTHORIZED);
        } else {
            ModelAndView mav = new ModelAndView();
            redirectAttributes.addFlashAttribute("userId",
                StringUtils.defaultString(e.getLoginParam().getUserId()));
            redirectAttributes.addFlashAttribute("isLoginLocked", true);
            redirectAttributes.addFlashAttribute("returnMessage", e.getMessage());
            mav.setViewName("redirect:" + adminProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * 어드민 최초 로그인 예외처리 핸들러
     * @param req HttpServletRequest
     * @param e {@link AdminFirstLoginException}
     * @param redirectAttributes {@link RedirectAttributes}
     * @return
     */
    @ExceptionHandler(AdminFirstLoginException.class)
    public Object adminFirstLoginExceptionHandler(final HttpServletRequest req,
        final AdminFirstLoginException e, final RedirectAttributes redirectAttributes) {
        log.error("AdminFirstLoginException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.FORBIDDEN.value(), ExceptionUtils.getStackTrace(e));

        final Map<String, String> data = Map
            .of("userId", StringUtils.defaultString(e.getUserInfo().getUserId()),
                "empId", StringUtils.defaultString(e.getUserInfo().getEmpId()));

        final ResponseObject<Map<String, String>> responseObject = ResponseObject
            .Builder.<Map<String, String>>builder()
            .status(HttpStatus.FORBIDDEN)
            .data(data)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9208.getCode())
            .returnMessage(StringUtils
                .defaultString(ExceptionCode.SYS_ERROR_CODE_9208.getDesc(), e.getMessage()))
            .build();

        if (isRestUri(req.getRequestURI())) {
            return new ResponseEntity<>(responseObject, createHttpHeaders(), HttpStatus.FORBIDDEN);
        } else {
            ModelAndView mav = new ModelAndView();
            redirectAttributes.addFlashAttribute("userId", e.getUserInfo().getUserId());
            redirectAttributes.addFlashAttribute("empId", e.getUserInfo().getEmpId());
            redirectAttributes.addFlashAttribute("isFirstLogin", true);
            redirectAttributes.addFlashAttribute("returnMessage", e.getMessage());
            mav.setViewName("redirect:" + adminProperties.getCertificationRedirectUrl());
            return mav;
        }
    }

    /**
     * {@link MemberSearchFailException} 예외 처리
     */
    @ExceptionHandler(MemberSearchFailException.class)
    public ResponseEntity<ResponseObject<String>> memberSearchFailExceptionHandler(
        final HttpServletRequest req, final MemberSearchFailException e) {
        log.error(
            "MemberSearchFailException ERROR! <uri:{}>,<httpMethod:{}>,<returnCode:{}>",
            req.getRequestURI(), req.getMethod(), e.getReturnCode());

        ResponseObject<String> responseObject = ResourceConverter.toResponseObject(null,
            HttpStatus.UNPROCESSABLE_ENTITY, e.getReturnCode().name(), e.getReturnCode().getMsg());

        return new ResponseEntity<>(responseObject, createHttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * 파일 다운로드 실패시 발생되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e UrlDownloadFileFailedException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     * @see kr.co.homeplus.admin.web.common.controller.UploadController
     */
    @ExceptionHandler({UrlDownloadFileFailedException.class})
    public Object handleUrlDownloadFileFailedException(final HttpServletRequest req,
        final UrlDownloadFileFailedException e) {
        log.error("UrlDownloadFileFailedException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9401.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9401.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 허용되지 않은 URL 주소로 파일 다운로드 시 발생되는 Exception handler
     *
     * @param req HttpServletRequest
     * @param e NotAllowedUrlDownloadFileException
     * @return 에러 내용을 ResponseEntity 담아 리턴
     * @see kr.co.homeplus.admin.web.common.controller.UploadController
     */
    @ExceptionHandler({NotAllowedUrlDownloadFileException.class})
    public Object handleNotAllowedUrlDownloadFileException(final HttpServletRequest req,
        final NotAllowedUrlDownloadFileException e) {
        log.error("NotAllowedUrlDownloadFileException! <uri:{}>,<HttpStatus:{}>,<trace:{}>",
            req.getRequestURI(), HttpStatus.FORBIDDEN.value(), e.getMessage());

        final ResponseObject<String> responseObject = ResponseObject.Builder.<String>builder()
            .status(HttpStatus.FORBIDDEN)
            .data(null)
            .returnCode(ExceptionCode.SYS_ERROR_CODE_9401.getCode())
            .returnMessage(StringUtils.defaultString(e.getMessage(),
                ExceptionCode.SYS_ERROR_CODE_9401.getDesc()))
            .build();

        return returnResponse(req.getRequestURI(), e, responseObject, HttpStatus.FORBIDDEN);
    }

    /**
     * 예외 처리후 requestURI 유형에 따라 {@link HttpEntity} 또는 {@link ModelAndView} 로 반환합니다.
     * @param requestUri request URI
     * @param e {@link Exception}
     * @param responseBody ResponseBody
     * @param status {@link HttpStatus}
     * @param <T> T
     * @return URI의 suffix 가 .json 일 경우 {@link HttpEntity}로 아닐경우 {@link ModelAndView} 반환합니다.
     */
    private <T> Object returnResponse(final String requestUri, final Exception e,
        final T responseBody, final HttpStatus status) {
        return returnResponse(requestUri, e, responseBody, status, "/core/error/error");
    }

    /**
     * 예외 처리후 requestURI 유형에 따라 {@link HttpEntity} 또는 {@link ModelAndView} 로 반환합니다.
     * @param uri request URI
     * @param e {@link Exception}
     * @param responseBody ResponseBody
     * @param status {@link HttpStatus}
     * @param viewName viewName
     * @return URI의 suffix 가 .json 일 경우 {@link HttpEntity}로 아닐경우 {@link ModelAndView} 반환합니다.
     */
    private <T> Object returnResponse(final String uri, final Exception e, final T responseBody,
        final HttpStatus status, final String viewName) {
        if (isRestUri(uri)) {
            final HttpHeaders httpHeaders = createHttpHeaders();
            return new ResponseEntity<>(responseBody, httpHeaders, status);
        } else {
            final ModelAndView mav = new ModelAndView();
            setErrorModelAndView(mav, status, ExceptionUtils.getStackTrace(e), responseBody);
            mav.setViewName(viewName);
            return mav;
        }
    }

    /**
     * true 이면 .json url 로 호출, false 이면 page 호출여부 체크
     *
     * @param requestUrl request url
     * @return boolean
     */
    private boolean isRestUri(String requestUrl) {
        return StringUtils.endsWith(requestUrl, ".json");
    }

    /**
     * 에러 페이지에 전달 할 {@link ModelAndView}를 설정 합니다.
     *
     * @param mav            {@link ModelAndView}
     * @param status         {@link HttpStatus}
     * @param stackTrace     {@link Exception} StackTrace
     * @param responseObject {@link ResponseObject}
     */
    private <T> void setErrorModelAndView(ModelAndView mav, HttpStatus status, String stackTrace,
        T responseObject) {
        mav.setStatus(status);
        mav.addObject("profilesActive", profilesActive);
        mav.addObject("responseObject", responseObject);
        mav.addObject("stackTrace", stackTrace);
    }

    /**
     * 에러 발생 시 리턴 할 {@link ResponseObject}를 spec에 맞춰 생성해주는 메소드
     *
     * @param ex         Exception
     * @param httpStatus ResponseObject 내에 입력할 HttpStatus
     * @return ResponseObject
     */
    private ResponseObject<String> setResponseObjectForException(final Exception ex,
        final HttpStatus httpStatus) {
        Exception processedException = new Exception(ex.getMessage());
        processedException.setStackTrace(Arrays.copyOf(ex.getStackTrace(), 3));

        return ResponseObject.Builder.<String>builder().status(httpStatus)
            .data(ExceptionUtils.getStackTrace(processedException)).returnCode("01")
            .returnMessage(ExceptionUtils.getMessage(processedException)).build();
    }

    /**
     * 에러 전달시 사용할 HttpHeaders 생성<p>
     * <p>
     * char : UTF-8<br> MediaType : application/json
     *
     * @return HttpHeaders
     */
    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        Charset utf8 = StandardCharsets.UTF_8;
        MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON_UTF8, utf8);
        httpHeaders.setContentType(mediaType);

        return httpHeaders;
    }
}