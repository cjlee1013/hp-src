package kr.co.homeplus.admin.web.item.model.banner;

import lombok.Data;

@Data
public class ItemBannerDetailSetParamDto {

    //상품공지 세부내용 일련번호
    private String detailSeq;

    //상품상세 공지번호
    private Long bannerNo;

    //링크타입
    private String linkType;

    //링크타입
    private String linkInfo;

    //이미지명
    private String imgNm;

    //이미지경로
    private String imgUrl;

    //이미지가로사이즈
    private Integer imgWidth;

    //이미지세로사이즈
    private Integer imgHeight;

    //우선순위
    private Integer priority;

    //사용여부
    private String useYn;

    //등록자
    private String userId;

}
