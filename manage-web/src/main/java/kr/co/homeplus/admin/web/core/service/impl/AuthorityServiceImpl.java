package kr.co.homeplus.admin.web.core.service.impl;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.LoginCookieServiceException;
import kr.co.homeplus.admin.web.core.exception.RoleServiceException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * 권한체크 서비스의 구현체
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    @Override
    public boolean hasRole(final String empId, final String roleCd) {
        UriComponentsBuilder uri = UriComponentsBuilder
            .fromUriString(AdminConstants.ADMIN_ROLE_NAME_PERMISSION_CHECK_URI);
        uri.queryParam("empId", empId)
            .queryParam("roleCd", roleCd)
            .build().encode();

        ResponseObject<Boolean> responseObject = getBooleanResponseFromAuth(uri);

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else {
            log.error("hasRole failed from auth-api:<empId:{},roleCd:{}>,<{}>", empId, roleCd,
                responseObject.getReturnMessage());
            throw new RoleServiceException(responseObject.getReturnMessage());
        }
    }

    @Override
    public boolean hasRolePolicy(final String empId, final String roleCd, final String policyCd) {
        final UriComponentsBuilder uri = UriComponentsBuilder
            .fromUriString(AdminConstants.ADMIN_ROLE_NAME_POLICY_NAME_PERMISSION_CHECK_URI);
        uri.queryParam("empId", empId)
            .queryParam("roleCd", roleCd)
            .queryParam("policyCd", policyCd)
            .build().encode();

        ResponseObject<Boolean> responseObject = getBooleanResponseFromAuth(uri);

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();
        } else {
            log.error("hasRole failed from auth-api:<empId:{},roleCd:{}>,<{}>", empId, roleCd,
                responseObject.getReturnMessage());
            throw new RoleServiceException(responseObject.getReturnMessage());
        }
    }

    @Override
    public boolean hasRolePolicy(final String roleCd, final String policyCd) {
        String empId;
        try {
            empId = loginCookieService.getUserInfo().getEmpId();
        } catch (Exception e) {
            log.error("hasRolePolicy failed from cookie:<roleCd:{},policyCd;{}>,<{}>", roleCd, policyCd,
                ExceptionUtils.getStackTrace(e));
            throw new LoginCookieServiceException(ExceptionCode.SYS_ERROR_CODE_9207.getDesc());
        }
        return hasRolePolicy(empId, roleCd, policyCd);
    }

    @Override
    public boolean hasNotRole(final String roleCd) {
        return !hasRole(roleCd);
    }

    private ResponseObject<Boolean> getBooleanResponseFromAuth(UriComponentsBuilder uri) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.AUTHORITY, uri.toUriString()
            , new ParameterizedTypeReference<>() {
            }
        );
    }

    @Override
    public boolean hasRole(final String roleCd) {
        String empId;
        try {
            empId = loginCookieService.getUserInfo().getEmpId();
        } catch (Exception e) {
            log.error("hasRole failed from cookie:<roleCd:{}>,<{}>", roleCd,
                ExceptionUtils.getStackTrace(e));
            throw new LoginCookieServiceException(ExceptionCode.SYS_ERROR_CODE_9207.getDesc());
        }
        return hasRole(empId, roleCd);
    }

    /**
     * ResponseObject의 결과가 정상인지 확인합니다.
     * @param returnCode ResponseObject의 returnCode
     * @return returnCode가 "SUCCESS"일 경우 true, 아닐 경우 false를 반환합니다.
     */
    private boolean isResponseSuccess(final String returnCode) {
        return returnCode.equals(ResponseObject.RETURN_CODE_SUCCESS);
    }
}