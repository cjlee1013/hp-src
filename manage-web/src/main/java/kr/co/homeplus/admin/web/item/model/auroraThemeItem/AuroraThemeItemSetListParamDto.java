package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraThemeItemSetListParamDto {

    List<AuroraThemeSetParamDto> auroraThemeList;

    List<AuroraThemeItemSetDto> itemList;

    private String regId;

}
