package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum DicTypeCd {
    TOTAL("TOTAL","기본"),
    EXTENSION("EXTENSION","확장"),
    COMPOUNDS("COMPOUNDS","복합명사"),
    PREANALYSIS("PREANALYSIS","기분석"),
    JOSA("JOSA","조사"),
    EOMI("EOMI","어미"),
    PREFIX("PREFIX","접두사"),
    SUFFIX("SUFFIX","접미사");

    private String dicTypeCd;
    private String dicTypeNm;

    public static DicTypeCd getDicType(String dicTypeCd) {
        for(DicTypeCd dicType : DicTypeCd.values()) {
            if(dicType.dicTypeCd.equals(dicTypeCd)) {
                return dicType;
            }
        }
        return null;
    }
}
