package kr.co.homeplus.admin.web.promotion.model.coupon;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponDetailGetDto {
    private Long couponNo;
    private String storeType;
    private String status;
    private String statusNm;
    private Character withdrawYn;
    private String manageCouponNm;
    private String offlineCouponNo;
    private String displayCouponNm;
    private String departCd;
    private String couponType;
    private String purpose;
    private String purchaseStdDt;
    private Character displayItYn;
    private Character displayCzYn;
    private String itemDuplYn;
    private String discountType;
    private Integer discountPrice;
    private Integer discountRate;
    private Integer discountMax;
    private Integer purchaseMin;
    private Character shareYn;
    private Integer shareHome;
    private Integer shareSeller;
    private Integer shareCard;
    private Character applyPcYn;
    private Character applyAppYn;
    private Character applyMwebYn;
    private String applyScope;
    private String issueStartDt;
    private String issueEndDt;
    private String issueLimitType;
    private Integer issueMaxCnt;
    private Integer limitPerCnt;
    private Character limitPerDayYn;
    private Integer limitPerDayCnt;
    private Character issueTimeYn;
    private String issueStartTime;
    private String issueEndTime;
    private String validType;
    private Integer validDay;
    private Integer validTime;
    private String validStartDt;
    private String validEndDt;
    private Character shipSlotYn;
    private String slotStartTime;
    private String slotEndTime;
    private Character validMon;
    private Character validTue;
    private Character validWed;
    private Character validThu;
    private Character validFri;
    private Character validSat;
    private Character validSun;
    private Integer storeCnt;
    private Character combineOrderAvailYn;
    private Character pickupAvailYn;
    private String barcodeCouponSeq;
    private String barcode;
    private String cartBarcode;
    private String randomNoIssueYn;
    private String randomNoType;
    private String randomNo;
    private String gradeCouponYn;
    private String gradeCouponMonth;
    private Integer userGradeSeq;
    private String paymentType;
    private String empNo;
    private Character randomListDownAvailYn;

    private List<PromotionStoreDto> storeList;
    private List<PromotionPaymentDto> paymentList;
    // 적용범위 리스트
    private List<CouponApplyGetDto> applyList;
    // 제외상품 리스트
    private List<CouponExceptItemDto> exceptItemList;
}
