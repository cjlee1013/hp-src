package kr.co.homeplus.admin.web.core.exception;

public class PersonalLogServiceException extends RuntimeException {

    public PersonalLogServiceException(String message) {
        super(message);
    }

    public PersonalLogServiceException(String message, Throwable e) {
        super(message, e);
    }

    public PersonalLogServiceException(Throwable throwable) {
        super(throwable);
    }
}
