package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;


import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class AttributeListSelectDto {
    @RealGridColumnInfo(headText = "분류번호", sortable = true, width = 70, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private Long attrNo;

    @RealGridColumnInfo(headText = "관리분류명", sortable = true, width = 190)
    private String gattrMngNm;

    @RealGridColumnInfo(headText = "우선순위", sortable = true, width = 50, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private int priority;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 50)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;

    @RealGridColumnInfo(headText = "카테고리ID", hidden = true)
    private Long scateCd;

    @RealGridColumnInfo(headText = "사용여부코드", hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "노출유형", hidden = true)
    private String dispTypeTxt;
}
