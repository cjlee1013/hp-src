package kr.co.homeplus.admin.web.partner.model.sellerShop;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SellerShopSetParamDto {

    private String partnerId;
    private String shopNm;
    private String shopUrl;
    private String shopProfileImg;
    private String shopLogoImg;
    private String shopLogoYn;
    private String shopInfo;
    private String csInfo;
    private String csUseYn;
    private String useYn;
    private String useYnTxt;
    private String businessNm;
    private String partnerNm;
    private String partnerNo;
    private String partnerStatus;
    private String partnerStatusNm;
    private String regId;

}
