package kr.co.homeplus.admin.web.partner.service;


import com.github.jknack.handlebars.internal.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.partner.model.hist.PartnerSellerHistGetDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Slf4j
@Component
public class PartnerSellerHistService {

    public List<PartnerSellerHistGetDto> getPartnerSellerHistGroup(List<PartnerSellerHistGetDto> partnerSellerHistGetDtoList) {

        List<PartnerSellerHistGetDto> res = new ArrayList<>();

        if (ObjectUtils.isEmpty(partnerSellerHistGetDtoList)) {
            return partnerSellerHistGetDtoList;
        }

        int nextIndex = 1;
        int size = partnerSellerHistGetDtoList.size();

        for (PartnerSellerHistGetDto dto : partnerSellerHistGetDtoList) {
            PartnerSellerHistGetDto newDto = new PartnerSellerHistGetDto();
            if (nextIndex != size) {
                BeanUtils.copyProperties(dto, newDto);

                if (!dto.getPartnerOwner().equals(partnerSellerHistGetDtoList.get(nextIndex).getPartnerOwner())) {
                    newDto.setPartnerOwner(this.getStringDiff(dto.getPartnerOwner(), partnerSellerHistGetDtoList.get(nextIndex).getPartnerOwner()));
                    newDto.setChgPartnerOwner("수정");
                }

                if (!dto.getBusinessNm().equals(partnerSellerHistGetDtoList.get(nextIndex).getBusinessNm())) {
                    newDto.setBusinessNm(this.getStringDiff(dto.getBusinessNm(), partnerSellerHistGetDtoList.get(nextIndex).getBusinessNm()));
                    newDto.setChgBusinessNm("수정");
                }

                if (!dto.getCommunityNotiNo().equals(partnerSellerHistGetDtoList.get(nextIndex).getCommunityNotiNo())) {
                    newDto.setCommunityNotiNo(this.getStringDiff(dto.getCommunityNotiNo(), partnerSellerHistGetDtoList.get(nextIndex).getCommunityNotiNo()));
                    newDto.setChgCommunityNotiNo("수정");
                }

                if (!dto.getBusinessConditions().equals(partnerSellerHistGetDtoList.get(nextIndex).getBusinessConditions())) {
                    newDto.setBusinessConditions(this.getStringDiff(dto.getBusinessConditions(), partnerSellerHistGetDtoList.get(nextIndex).getBusinessConditions()));
                    newDto.setChgBusinessConditions("수정");
                }

                if (!dto.getBizCateNm().equals(partnerSellerHistGetDtoList.get(nextIndex).getBizCateNm())) {
                    newDto.setBizCateNm(this.getStringDiff(dto.getBizCateNm(), partnerSellerHistGetDtoList.get(nextIndex).getBizCateNm()));
                    newDto.setChgBizCateNm("수정");
                }

                if (!dto.getBankAccountNo().equals(partnerSellerHistGetDtoList.get(nextIndex).getBankAccountNo())) {
                    newDto.setBankAccountNo(this.getStringDiff(dto.getBankAccountNo(), partnerSellerHistGetDtoList.get(nextIndex).getBankAccountNo()));
                    newDto.setChgBankAccountNo("수정");
                }

                if (!dto.getDepositor().equals(partnerSellerHistGetDtoList.get(nextIndex).getDepositor())) {
                    newDto.setDepositor(this.getStringDiff(dto.getDepositor(), partnerSellerHistGetDtoList.get(nextIndex).getDepositor()));
                    newDto.setChgDepositor("수정"); 
                }

                if (StringUtils.isNotEmpty(dto.getSettlePaymentYnNm()) && !dto.getSettlePaymentYnNm().equals(partnerSellerHistGetDtoList.get(nextIndex).getSettlePaymentYnNm())) {
                    newDto.setSettlePaymentYnNm(this.getStringDiff(dto.getSettlePaymentYnNm(), partnerSellerHistGetDtoList.get(nextIndex).getSettlePaymentYnNm()));
                    newDto.setChgSettlePaymentYnNm("수정");
                }

                res.add(newDto);

                nextIndex++;
            } else {
                res.add(dto);
            }
        }


        return res;
    }

    private String getStringDiff(final String nowStr, final String prevStr) {
        final String SPAN_RED_HTML_START = "<span style=\"color:red\">";
        final String SPAN_BLUE_HTML_START = "<span style=\"color:blue\">";
        final String SPAN_HTML_END = "</span>";

        if (nowStr.equals(prevStr)) {
            return nowStr;
        } else {
            return    SPAN_RED_HTML_START
                    + (StringUtils.isEmpty(prevStr) ? "-" : prevStr)
                    + " -> "
                    + SPAN_HTML_END
                    + SPAN_BLUE_HTML_START
                    + (StringUtils.isEmpty(nowStr) ? "-" : nowStr)
                    + SPAN_HTML_END;
        }
    }
}
