package kr.co.homeplus.admin.web.settle.model.Advance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "선수금조회 목록")
public class AdvanceListSetDto {
  @ApiModelProperty(notes = "시작일")
  private String startDt;

  @ApiModelProperty(notes = "종료일")
  private String endDt;

  @ApiModelProperty(notes = "0원주문포함")
  private String includeZero;
}
