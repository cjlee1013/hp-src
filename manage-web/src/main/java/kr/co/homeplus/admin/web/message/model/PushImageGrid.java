package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class PushImageGrid {

    @RealGridColumnInfo(hidden = true)
    private long seq;
    @RealGridColumnInfo(headText = "이미지 이름")
    private String imageName;
    @RealGridColumnInfo(headText = "이미지 원본 이름")
    private String originName;
    @RealGridColumnInfo(headText = "이미지 url", columnType = RealGridColumnType.NAME)
    private String url;
    @RealGridColumnInfo(headText = "작성자")
    private String regId;
    @RealGridColumnInfo(headText = "작성일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime regDt;
}
