package kr.co.homeplus.admin.web.item.model.harmful;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class HarmfulItemListSelectDto {

    @RealGridColumnInfo(headText = "cdInsptmachi", hidden = true)
    private String cdInsptmachi;
    @RealGridColumnInfo(headText = "noDocument", order = 1, hidden = true)
    private String noDocument;
    @RealGridColumnInfo(headText = "seq", order = 2, hidden = true)
    private String seq;

    @RealGridColumnInfo(headText = "유형", width = 120, sortable = true, order = 3)
    private String nmRpttype;

    @RealGridColumnInfo(headText = "제품명", width = 150, sortable = true, order = 4)
    private String nmProd;

    @RealGridColumnInfo(headText = "바코드", width = 120, order = 5)
    private String barcode;

    @RealGridColumnInfo(headText = "제조업소", width = 120, order = 6)
    private String nmManufacupso;

    @RealGridColumnInfo(headText = "판매업소", width = 120, order = 7)
    private String nmSalerupso;

    @RealGridColumnInfo(headText = "검사기관", order = 8)
    private String instituteTxt;

    @RealGridColumnInfo(headText = "처리상태", order = 9)
    private String statusResult;

    @RealGridColumnInfo(headText = "등록일", width = 120, order = 10)
    private String recvDt;

    @RealGridColumnInfo(headText = "수정자", order = 11)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 12, sortable = true)
    private String chgDt;
}
