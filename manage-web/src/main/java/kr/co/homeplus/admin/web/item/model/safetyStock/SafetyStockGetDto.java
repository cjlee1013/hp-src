package kr.co.homeplus.admin.web.item.model.safetyStock;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class SafetyStockGetDto {

    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, order = 1, sortable = true)
    private String itemNm;

    @RealGridColumnInfo(headText = "HYPER", width = 80, order = 2, sortable = true)
    private String hyperDispTxt;

    @RealGridColumnInfo(headText = "CLUB", width = 80, order = 3, hidden = true)
    private String clubDispTxt;

    @RealGridColumnInfo(headText = "EXPRESS", width = 80, order = 4, sortable = true)
    private String expressDispTxt;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 5)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 130, order = 6, sortable = true)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 130, order = 7, sortable = true)
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 130, order = 8, sortable = true)
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 130, order = 9, sortable = true)
    private String dcateNm;

    @RealGridColumnInfo(headText = "등록자", order = 10, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 11, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 12, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 13, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "수정이력", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, order = 14)
    private String histBtn = "이력보기";

    @RealGridColumnInfo(headText = "hyperUseYn", order = 15 ,hidden = true)
    private String hyperUseYn;
    @RealGridColumnInfo(headText = "hyperQty", order = 16 ,hidden = true)
    private int hyperQty;
    @RealGridColumnInfo(headText = "clubUseYn", order = 17 ,hidden = true)
    private String clubUseYn;
    @RealGridColumnInfo(headText = "clubQty", order = 18 ,hidden = true)
    private int clubQty;
    @RealGridColumnInfo(headText = "expressUseYn", order = 19, hidden = true)
    private String expressUseYn;
    @RealGridColumnInfo(headText = "expressQty", order = 20, hidden = true)
    private int expressQty;
    @RealGridColumnInfo(headText = "useYn", order = 21, hidden = true)
    private String useYn;

    private String regId;
    private String chgId;

}
