package kr.co.homeplus.admin.web.item.model.auroraCategoryItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class AuroraCategoryItemGetDto {

    @RealGridColumnInfo(headText = "카테고리 ID", width = 80)
    private String mcateCd;

    @RealGridColumnInfo(headText = "lcateCd", order = 1, hidden = true)
    private String lcateCd;

    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 150, order = 2)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 150, order = 3)
    private String mcateNm;

    @RealGridColumnInfo(headText = "상품번호", order = 4)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 300, order = 5)
    private String itemNm;

    @RealGridColumnInfo(headText = "dispYn", order = 6, hidden = true)
    private String dispYn;

    @RealGridColumnInfo(headText = "전시여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 70, order = 7)
    private String dispYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 8)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 9)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 10)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 11)
    private String chgDt;

    private String regId;
    private String chgId;

}
