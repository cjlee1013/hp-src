package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.*;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BalanceSheetService {

    private final ResourceClient resourceClient;
    /**
     * 정산관리 > 회계자료조 > 전표조회
     */
    public List<BalanceSheetListGetDto> getList(BalanceSheetListSetDto balanceSheetListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            balanceSheetListSetDto,
            "/admin/balanceSheet/getList",
            new ParameterizedTypeReference<ResponseObject<List<BalanceSheetListGetDto>>>(){}).getData();
    }
    public List<BalanceSheetDetailGetDto> getDetail(BalanceSheetDetailSetDto balanceSheetDetailSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            balanceSheetDetailSetDto,
            "/admin/balanceSheet/getDetail",
            new ParameterizedTypeReference<ResponseObject<List<BalanceSheetDetailGetDto>>>(){}).getData();
    }
    public List<BalanceSheetSlipTypeGetDto> getSlipTypeList() throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.SETTLE, "/admin/balanceSheet/getSlipTypeList",
            new ParameterizedTypeReference<ResponseObject<List<BalanceSheetSlipTypeGetDto>>>() {}).getData();

    }
    public BalanceSheetInterfaceGetDto setInterface(BalanceSheetInterfaceSetDto balanceSheetInterfaceSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            balanceSheetInterfaceSetDto,
            "/admin/balanceSheet/setInterface",
            new ParameterizedTypeReference<ResponseObject<BalanceSheetInterfaceGetDto>>(){}).getData();
    }
}
