package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.management.DgvPaymentStoreGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentSetDto;
import kr.co.homeplus.admin.web.settle.model.management.MhcPaymentStoreGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoListSetDto;
import kr.co.homeplus.admin.web.settle.model.management.PaymentInfoSumGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListGetDto;
import kr.co.homeplus.admin.web.settle.model.management.PgPaymentInfoListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagementSettleService {

    private final ResourceClient resourceClient;

    /**
     * 정산관리 > 운영관리 > 일마감조회
     */

    public List<PaymentInfoSumGetDto> getPaymentInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        List<PaymentInfoSumGetDto> paymentInfoSumGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            paymentInfoListSetDto,
            "/admin/management/getPaymentInfoSum",
            new ParameterizedTypeReference<ResponseObject<List<PaymentInfoSumGetDto>>>(){}).getData();
        return paymentInfoSumGetDtoList;
    }

    public List<PaymentInfoListGetDto> getPaymentInfoList(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        List<PaymentInfoListGetDto> paymentInfoListGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            paymentInfoListSetDto,
            "/admin/management/getPaymentInfoList",
            new ParameterizedTypeReference<ResponseObject<List<PaymentInfoListGetDto>>>(){}).getData();
        for (PaymentInfoListGetDto paymentInfoListGetDto : paymentInfoListGetDtoList) {
            paymentInfoListGetDto.setPartnerId(PrivacyMaskingUtils.maskingUserId(paymentInfoListGetDto.getPartnerId()));
        }
        return paymentInfoListGetDtoList;
    }

    public List<PaymentInfoSumGetDto> getSettleInfoSum(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        List<PaymentInfoSumGetDto> paymentInfoSumGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            paymentInfoListSetDto,
            "/admin/management/getSettleInfoSum",
            new ParameterizedTypeReference<ResponseObject<List<PaymentInfoSumGetDto>>>(){}).getData();
        return paymentInfoSumGetDtoList;
    }

    public List<PaymentInfoListGetDto> getSettleInfoList(PaymentInfoListSetDto paymentInfoListSetDto) throws Exception {
        List<PaymentInfoListGetDto> paymentInfoListGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            paymentInfoListSetDto,
            "/admin/management/getSettleInfoList",
            new ParameterizedTypeReference<ResponseObject<List<PaymentInfoListGetDto>>>(){}).getData();
        for (PaymentInfoListGetDto paymentInfoListGetDto : paymentInfoListGetDtoList) {
            paymentInfoListGetDto.setPartnerId(PrivacyMaskingUtils.maskingUserId(paymentInfoListGetDto.getPartnerId()));
        }
        return paymentInfoListGetDtoList;
    }

    /**
     * 정산관리 > 운영관리 > PG수수료 일마감조회
     */
    public List<PgPaymentInfoListGetDto> getPgPaymentInfoList(
        PgPaymentInfoListSetDto pgPaymentInfoListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            pgPaymentInfoListSetDto,
            "/admin/management/getPgPaymentInfoList",
            new ParameterizedTypeReference<ResponseObject<List<PgPaymentInfoListGetDto>>>(){}).getData();
    }

    /**
     * 정산관리 > 운영관리 > DGV 실적조회
     */

    public List<DgvPaymentStoreGetDto> getDgvPaymentStore(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        List<DgvPaymentStoreGetDto> dgvPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            dgvPaymentSetDto,
            "/admin/management/getDgvPaymentStore",
            new ParameterizedTypeReference<ResponseObject<List<DgvPaymentStoreGetDto>>>(){}).getData();
        return dgvPaymentGetDtoList;
    }
    public List<DgvPaymentDailyGetDto> getDgvPaymentDaily(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        List<DgvPaymentDailyGetDto> dgvPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            dgvPaymentSetDto,
            "/admin/management/getDgvPaymentDaily",
            new ParameterizedTypeReference<ResponseObject<List<DgvPaymentDailyGetDto>>>(){}).getData();
        return dgvPaymentGetDtoList;
    }
    public List<DgvPaymentListGetDto> getDgvPaymentList(DgvPaymentSetDto dgvPaymentSetDto) throws Exception {
        List<DgvPaymentListGetDto> dgvPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            dgvPaymentSetDto,
            "/admin/management/getDgvPaymentList",
            new ParameterizedTypeReference<ResponseObject<List<DgvPaymentListGetDto>>>(){}).getData();
        return dgvPaymentGetDtoList;
    }

    /**
     * 정산관리 > 운영관리 > MHC 실적조회
     */

    public List<MhcPaymentStoreGetDto> getMhcPaymentStore(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        List<MhcPaymentStoreGetDto> mhcPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            mhcPaymentSetDto,
            "/admin/management/getMhcPaymentStore",
            new ParameterizedTypeReference<ResponseObject<List<MhcPaymentStoreGetDto>>>(){}).getData();
        return mhcPaymentGetDtoList;
    }
    public List<MhcPaymentDailyGetDto> getMhcPaymentDaily(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        List<MhcPaymentDailyGetDto> mhcPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            mhcPaymentSetDto,
            "/admin/management/getMhcPaymentDaily",
            new ParameterizedTypeReference<ResponseObject<List<MhcPaymentDailyGetDto>>>(){}).getData();
        return mhcPaymentGetDtoList;
    }
    public List<MhcPaymentListGetDto> getMhcPaymentList(MhcPaymentSetDto mhcPaymentSetDto) throws Exception {
        List<MhcPaymentListGetDto> mhcPaymentGetDtoList = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            mhcPaymentSetDto,
            "/admin/management/getMhcPaymentList",
            new ParameterizedTypeReference<ResponseObject<List<MhcPaymentListGetDto>>>(){}).getData();
        return mhcPaymentGetDtoList;
    }
}
