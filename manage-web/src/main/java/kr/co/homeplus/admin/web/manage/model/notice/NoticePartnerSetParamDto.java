package kr.co.homeplus.admin.web.manage.model.notice;


import lombok.Data;


@Data
public class NoticePartnerSetParamDto {


    private String partnerId;

    private String useYn;

    private String regId;

}
