package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeManageSelectDto;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeShiftManageDto;
import kr.co.homeplus.admin.web.escrow.service.StoreAnytimeManageService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 상품관리 > 점포관리 > 점포운영관리
 */
@Controller
@RequestMapping("/escrow/anytime")
public class StoreAnytimeManageController {
    private final StoreAnytimeManageService storeAnytimeManageService;
    private final LoginCookieService loginCookieService;

    public StoreAnytimeManageController(StoreAnytimeManageService storeAnytimeManageService, LoginCookieService loginCookieService) {
        this.storeAnytimeManageService = storeAnytimeManageService;
        this.loginCookieService = loginCookieService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/storeAnytimeManageMain")
    public String storeAnytimeManageMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("storeAnytimeManageGridBaseInfo", StoreAnytimeManageDto.class));
        return "/escrow/storeAnytimeManageMain";
    }

    /**
     * 점포 애니타임 정보 조회
     */
    @ResponseBody
    @PostMapping("/getStoreAnytimeInfo.json")
    public List<StoreAnytimeManageDto> getStoreAnytimeInfo(@RequestBody StoreAnytimeManageSelectDto storeAnytimeManageSelectDto) throws Exception {
        return storeAnytimeManageService.getStoreAnytimeInfo(storeAnytimeManageSelectDto);
    }

    /**
     * 점포 애니타임 Shift 정보 조회
     */
    @ResponseBody
    @GetMapping("/getStoreAnytimeShiftInfo.json")
    public List<StoreAnytimeShiftManageDto> getStoreAnytimeShiftInfo(@RequestParam(value = "storeId") int storeId) throws Exception {
        return storeAnytimeManageService.getStoreAnytimeShiftInfo(storeId);
    }

    /**
     * 점포 애니타임 사용여부 저장
     */
    @ResponseBody
    @PostMapping("/saveStoreAnytimeUseYn.json")
    public Integer saveStoreAnytimeUseYn(@RequestBody StoreAnytimeManageDto storeAnytimeManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        storeAnytimeManageDto.setRegId(userCd);
        storeAnytimeManageDto.setChgId(userCd);
        return storeAnytimeManageService.saveStoreAnytimeUseYn(storeAnytimeManageDto);
    }

    /**
     * 점포 애니타임 Shift 사용여부 저장
     */
    @ResponseBody
    @PostMapping("/saveStoreAnytimeShiftUseYn.json")
    public Integer saveStoreAnytimeShiftUseYn(@RequestBody List<StoreAnytimeShiftManageDto> storeAnytimeShiftManageDtoList) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        for (StoreAnytimeShiftManageDto shift : storeAnytimeShiftManageDtoList) {
            shift.setRegId(userCd);
            shift.setChgId(userCd);
        }
        return storeAnytimeManageService.saveStoreAnytimeShiftUseYn(storeAnytimeShiftManageDtoList);
    }
}
