package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.search.model.KeywordRankGetDto;
import kr.co.homeplus.admin.web.search.model.KeywordRankGridDto;
import kr.co.homeplus.admin.web.search.model.KeywordRankSetDto;
import kr.co.homeplus.admin.web.search.service.KeywordRankService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/keywordRank")
@RequiredArgsConstructor
public class KeywordRankController {

    private final KeywordRankService keywordRankService;

    @GetMapping(value = { "/rankList" })
    public String getKeywordRankMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = keywordRankService.getServiceCode();
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("itemStatus", code.get("item_status"));
        model.addAllAttributes(RealGridHelper.create("itemListGridBaseInfo", KeywordRankGridDto.KeywordItemList.class));
        return "/search/keywordRank";
    }

    @ResponseBody
    @GetMapping(value= {"/{itemNo}.json"})
    public KeywordRankGetDto getKeywordRank(@PathVariable(name="itemNo") String itemNo) {
        return (KeywordRankGetDto)keywordRankService.getKeywordRank(itemNo).getData();
    }

    @ResponseBody
    @PostMapping(value= {"/save.json"})
    public Integer saveKeywordRank(@RequestBody @Valid KeywordRankSetDto param) {
        return (Integer)keywordRankService.saveKeywordRank(param).getData();
    }

    @ResponseBody
    @PostMapping(value = {"/{itemNo}/delete.json"})
    public Integer deleteKeywordRank(@PathVariable(name="itemNo") String itemNo) {
        return (Integer)keywordRankService.deleteKeywordRank(itemNo).getData();
    }
}
