package kr.co.homeplus.admin.web.settle.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
@RealGridOptionInfo(checkBar = true)
public class BalanceSheetListGetDto {
  @ApiModelProperty(notes = "기준일")
  @RealGridColumnInfo(headText = "기준일", width = 150)
  private String basicDt;

  @ApiModelProperty(notes = "전표유형코드")
  @RealGridColumnInfo(headText = "전표유형코드", width = 100, hidden = true)
  private String slipType;

  @ApiModelProperty(notes = "전표유형")
  @RealGridColumnInfo(headText = "전표유형", width = 200)
  private String slipName;

  @ApiModelProperty(notes = "차변")
  @RealGridColumnInfo(headText = "차변", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String drAmt;

  @ApiModelProperty(notes = "대변")
  @RealGridColumnInfo(headText = "대변", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String crAmt;

  @ApiModelProperty(notes = "상태")
  @RealGridColumnInfo(headText = "상태", width = 100)
  private String status;

  @ApiModelProperty(notes = "변경일시")
  @RealGridColumnInfo(headText = "변경일시", width = 150)
  private String chgDt;

  @ApiModelProperty(notes = "변경자")
  @RealGridColumnInfo(headText = "변경자", width = 100)
  private String chgId;

}
