package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringPickGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringPickSetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipMonitoringPickService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송모니터링 > 회수지연조회
 */
@Controller
@RequestMapping("/ship")
public class ShipMonitoringPickController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private ShipMonitoringPickService shipMonitoringPickService;
    @Autowired
    private ShipCommonService shipCommonService;

    /**
     * 회수지연조회 Main Page
     */
    @GetMapping("/shipMonitoring/shipMonitoringPickMain")
    public String shipMonitoringPickMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("claimType", shipCommonService.getCode("claim_type"));
        model.addAttribute("claimStatus", shipCommonService.getCode("claim_status"));
        model.addAttribute("pickupStatus", shipCommonService.getCode("pickup_status"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAllAttributes(RealGridHelper.create("shipMonitoringPickGridBaseInfo", ShipMonitoringPickGetDto.class));

        return "/delivery/shipMonitoringPickMain";
    }

    /**
     * 회수지연조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipMonitoring/getShipMonitoringPickList.json", method = RequestMethod.GET)
    public List<ShipMonitoringPickGetDto> getShipMonitoringPickList(ShipMonitoringPickSetDto shipMonitoringPickSetDto) {
        return shipMonitoringPickService.getShipMonitoringPickList(shipMonitoringPickSetDto);
    }
}
