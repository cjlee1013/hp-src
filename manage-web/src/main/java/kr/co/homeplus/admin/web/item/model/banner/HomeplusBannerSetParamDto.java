package kr.co.homeplus.admin.web.item.model.banner;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeplusBannerSetParamDto {

    //상품상세공지번호
    private Long bannerNo;

    //공지타입(HMP:홈플러스/SELLER:판매자별
    private String bannerType;

    //구분(ALL:전체,CATE:카테고리별,ITEM:상품별,SELLER:판매자별
    private String dispType;

    //공지제목
    private String bannerNm;

    //세카테고리 선택 값
    private String selectCateCd4;

    //점포유형
    private String storeType;

    //공지내용
    private String bannerDesc;

    //노출여부
    private String dispYn;

    //노출시작일시
    private String dispStartDt;

    //노출종료일시
    private String dispEndDt;

    //등록/수정자
    private String userId;

    //수정여부
    private String isMod;

}
