package kr.co.homeplus.admin.web.mileage.model.payment;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Max;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageReqExcelUploadDto {
    @ApiModelProperty(value = "회원번호")
    @RealGridColumnInfo(headText = "회원번호", width = 200, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액")
    @RealGridColumnInfo(headText = "금액", width = 200, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    @Max(value = 10000000, message = "1,000만 마일리지를 넘어갈수 없습니다.")
    private long mileageAmt;

    @ApiModelProperty(value = "마일리지상세메시지(오류)")
    @RealGridColumnInfo(headText = "비고", width = 300, hidden = true)
    private String detailMessage;

}