package kr.co.homeplus.admin.web.mileage.controller;

import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageReqExcelUploadDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeSearchPopDto;
import kr.co.homeplus.admin.web.mileage.model.type.pop.MileagePromoTypeGetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/mileage/popup")
public class MileagePopupController {
    /**
     * 결제관리 > 마일리지 관리 > 마일리지 유형 조회 팝업
     * @param model
     * @return String
     */
    @RequestMapping(value = "/mileageTypeNamePop", method = RequestMethod.GET)
    public String mileageTypeNamePop(Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("mileageTypeNo", request.getParameter("mileageTypeNo"));
        model.addAttribute("mileageTypeName", request.getParameter("mileageTypeName"));
        model.addAllAttributes(RealGridHelper.create("mileageListBaseInfo", MileageTypeSearchPopDto.class));

        return "/mileage/pop/mileageTypeNamePop";
    }

    /**
     * 마일리지 프로모션 유형 조회 팝업
     */
    @GetMapping(value = "/mileagePromoTypePop")
    public String mileageTypeNamePop(Model model, @RequestParam("storeType") String storeType,
        @RequestParam(value = "callBack", defaultValue = "") String callBackScript) throws Exception {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("storeType", storeType);
        model.addAllAttributes(RealGridHelper.create("mileageTypeGrid", MileagePromoTypeGetDto.class));
        return "/mileage/pop/mileagePromoTypePop";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 대상회원 엑셀 업로드 팝업
     * @param model
     * @return String
     */
    @RequestMapping(value = "/mileageRequestExcelPop", method = RequestMethod.GET)
    public String mileageRequestExcelPop(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("mileageDetailBaseInfo", MileageReqExcelUploadDto.class));
        return "/mileage/pop/mileageRequestExcelPop";
    }
}