package kr.co.homeplus.admin.web.statistics.model.storeItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class CombineOrderStatusDto {
    @RealGridColumnInfo(headText = "점포", width = 50)
    String storeNm;
    @RealGridColumnInfo(headText = "점포코드", width = 50, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    int storeId;

    @RealGridColumnInfo(headText = "건수", groupName = "주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgOrderCount;
    @RealGridColumnInfo(headText = "금액", groupName = "주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgOrderAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "취소", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgCancelCount;
    @RealGridColumnInfo(headText = "금액", groupName = "취소", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgCancelAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "반품", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgReturnCount;
    @RealGridColumnInfo(headText = "금액", groupName = "반품", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgReturnAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "대체주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgSubCount;
    @RealGridColumnInfo(headText = "금액", groupName = "대체주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long orgSubAmt;

    @RealGridColumnInfo(headText = "건수", groupName = "주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combOrderCount;
    @RealGridColumnInfo(headText = "금액", groupName = "주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combOrderAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "취소", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combCancelCount;
    @RealGridColumnInfo(headText = "금액", groupName = "취소", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combCancelAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "반품", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combReturnCount;
    @RealGridColumnInfo(headText = "금액", groupName = "반품", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combReturnAmt;
    @RealGridColumnInfo(headText = "건수", groupName = "대체주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combSubCount;
    @RealGridColumnInfo(headText = "금액", groupName = "대체주문", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    long combSubAmt;
}
