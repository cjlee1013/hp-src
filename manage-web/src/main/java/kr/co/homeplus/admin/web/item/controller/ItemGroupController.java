package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.itemGroup.ItemGroupDetailListGetDto;
import kr.co.homeplus.admin.web.item.model.itemGroup.ItemGroupListGetDto;
import kr.co.homeplus.admin.web.item.model.itemGroup.ItemGroupListParamDto;
import kr.co.homeplus.admin.web.item.model.itemGroup.ItemGroupSetParamDto;
import kr.co.homeplus.admin.web.item.model.itemGroup.ItemGroupValidGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/itemGroup")
public class ItemGroupController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    public ItemGroupController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 점포상품관리 > 묶음형 상품관리
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/itemGroupMain", method = RequestMethod.GET)
    public String itemGroupMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"       // 사용여부
        );

        model.addAttribute("useYn", code.get("use_yn"));

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("itemGroupListGridBaseInfo", ItemGroupListGetDto.class));

        model.addAllAttributes(
            RealGridHelper.create("itemGroupDetailListGridBaseInfo", ItemGroupDetailListGetDto.class));

        return "/item/itemGroupMain";
    }

    /**
     * 상품관리 > 점포상품관리 > 묶음형상품관리 조회
     * @param itemGroupListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemGroupList.json"}, method = RequestMethod.POST)
    public List<ItemGroupListGetDto> getItemGroupList(@RequestBody ItemGroupListParamDto itemGroupListParamDto) {
        String apiUri = "/itemGroup/getItemGroupList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemGroupListGetDto>>>() {};
        return (List<ItemGroupListGetDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, itemGroupListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 묶음형상품상세 조회
     * @param groupNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemGroupDetailList.json"})
    public List<ItemGroupDetailListGetDto> getItemGroupDetailList(@RequestParam(name = "groupNo") Long groupNo) {
        String apiUri = "/itemGroup/getItemGroupDetailList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemGroupDetailListGetDto>>>() {};
        return (List<ItemGroupDetailListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?groupNo=" + groupNo , typeReference).getData();
    }

    /**
     * 묶음형 상품관리 상품 추가
     * @param itemNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getOptSelUseYnNm.json"})
    public ItemGroupValidGetDto getItemDetail(@RequestParam(name = "itemNo") String itemNo) {
        String apiUri = "/itemGroup/getItemDetail";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemGroupValidGetDto>>() {};
        return (ItemGroupValidGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?itemNo=" + itemNo , typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 묶음형 상품관리
     * @param itemGroupSetParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemGroup.json"}, method = RequestMethod.POST)
    public ResponseResult setItemGroup (@RequestBody ItemGroupSetParamDto itemGroupSetParamDto) throws Exception {
        String apiUri = "/itemGroup/setItemGroup";

        itemGroupSetParamDto.setUserId(cookieService.getUserInfo().getEmpId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemGroupSetParamDto, apiUri, typeReference).getData();

    }

}
