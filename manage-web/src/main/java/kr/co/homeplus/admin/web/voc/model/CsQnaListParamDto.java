package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String schLcateCd;

    private String schMcateCd;

    private String schScateCd;

    private String schDcateCd;

    private String searchQnaStatus;

    private String searchQnaDispStatus;

    private String searchType;

    private String searchKeyword;

    private String searchPartnerId;


}
