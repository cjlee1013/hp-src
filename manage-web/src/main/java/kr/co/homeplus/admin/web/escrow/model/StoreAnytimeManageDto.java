package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포 애니타임 관리 DTO")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class StoreAnytimeManageDto {
    @ApiModelProperty(value = "점포유형", position = 1)
    @RealGridColumnInfo(headText = "점포유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(value = "점포ID", position = 2)
    @RealGridColumnInfo(headText = "점포코드", sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.BASIC)
    private int storeId;

    @ApiModelProperty(value = "점포명", position = 3)
    @RealGridColumnInfo(headText = "점포명", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @ApiModelProperty(value = "점포애니타임사용여부", position = 4)
    @RealGridColumnInfo(headText = "애니타임 사용여부", width = 150, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeAnytimeUseYn;

    @ApiModelProperty(value = "등록일시", position = 5)
    @RealGridColumnInfo(headText = "등록일", width = 120, sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String regDt;

    @ApiModelProperty(value = "등록자ID", position = 6)
    @RealGridColumnInfo(headText = "등록자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "수정일시", position = 7)
    @RealGridColumnInfo(headText = "수정일", width = 120, sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String chgDt;

    @ApiModelProperty(value = "수정자ID", position = 8)
    @RealGridColumnInfo(headText = "수정자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;
}
