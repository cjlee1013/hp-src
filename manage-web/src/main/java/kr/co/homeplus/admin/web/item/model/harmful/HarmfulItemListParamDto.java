package kr.co.homeplus.admin.web.item.model.harmful;

import lombok.Data;


@Data
public class HarmfulItemListParamDto {

    private String searchPeriodType;

    private String searchStartDt;

    private String searchEndDt;

    private String searchItemStatus;

    private String searchItemType;

    private String searchInstitute;

    private String searchType;

    private String searchKeyword;
}
