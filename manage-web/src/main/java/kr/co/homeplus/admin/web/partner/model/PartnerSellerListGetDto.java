package kr.co.homeplus.admin.web.partner.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
//판매업체 리스트
public class PartnerSellerListGetDto {

    @RealGridColumnInfo(headText = "판매업체ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerId;

    @RealGridColumnInfo(headText = "업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String businessNm;

    @RealGridColumnInfo(headText = "상호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String partnerNm;

    @RealGridColumnInfo(headText = "사업자유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String operatorTypeNm;

    @RealGridColumnInfo(headText = "사업자등록번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerNo;

    @RealGridColumnInfo(headText = "vendor Code", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String ofVendorCd;

    @RealGridColumnInfo(headText = "회원등급", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerGradeNm;

    @RealGridColumnInfo(headText = "회원상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerStatusNm;

    @RealGridColumnInfo(headText = "OF 승인상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String ofVendorStatusNm;

    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 150)
    private String chgDt;


    //회원등급
    private String partnerGrade;

    //회원상태
    private String partnerStatus;

    //판매유형(N:일반, D:직매입)
    private String saleType;

    //등록자 ID
    private String regId;

    //수정자 ID
    private String chgId;
}
