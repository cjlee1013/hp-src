package kr.co.homeplus.admin.web.manage.model.favicon;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class FaviconListSelectDto {

    @ApiModelProperty(value = "파비콘고유번호")
    @RealGridColumnInfo(headText = "파비콘고유번호", hidden = true)
    private Long faviconNo;

    @ApiModelProperty(value = "사이트구분")
    @RealGridColumnInfo(headText = "사이트구분", hidden = true)
    private String siteType;

    @ApiModelProperty(value = "사이트구분")
    @RealGridColumnInfo(headText = "사이트구분", width = 100)
    private String siteTypeNm;

    @ApiModelProperty(value = "전시위치")
    @RealGridColumnInfo(headText = "전시위치", width = 100, sortable = true)
    private String device;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true)
    private String useYnNm;

    @ApiModelProperty(value = "노출시작일")
    @RealGridColumnInfo(headText = "노출시작일", columnType = RealGridColumnType.DATETIME, width = 100, sortable = true)
    private String dispStartDt;

    @ApiModelProperty(value = "노출종료일")
    @RealGridColumnInfo(headText = "노출종료일", columnType = RealGridColumnType.DATETIME, width = 100, sortable = true)
    private String dispEndDt;

    @ApiModelProperty(value = "배너명")
    @RealGridColumnInfo(headText = "배너명", width = 100, sortable = true)
    private String bannerNm;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;


}
