package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 상태 Get Entry
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerStatusGetDto {

	private String partnerNm;
	private String partnerStatus;
	private String partnerStatusNm;
	private List<PartnerStatusHistGetDto> partnerStatusHistGetDtoList;
}
