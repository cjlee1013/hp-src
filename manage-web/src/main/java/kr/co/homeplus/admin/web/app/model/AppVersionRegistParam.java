package kr.co.homeplus.admin.web.app.model;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.voc.enums.SiteType;
import lombok.Data;
import org.springframework.mobile.device.DevicePlatform;

@Data
public class AppVersionRegistParam {

    @ApiModelProperty(value = "사이트구분")
    @NotNull
    private SiteType siteType = SiteType.HOME;

    @ApiModelProperty(value = "디바이스구분")
    @NotNull
    private DevicePlatform devicePlatform = DevicePlatform.ANDROID;

    @ApiModelProperty(value = "앱 버전")
    @NotEmpty
    private String version;

    @ApiModelProperty(value = "업데이트내용 메모")
    private String memo;

    @ApiModelProperty(value = "강제업데이트여부")
    @NotEmpty
    private String forceUpdate = "N";

    @ApiModelProperty(value = "업데이트추천알림여부")
    @NotEmpty
    private String updateNotification = "N";

    @ApiModelProperty(value = "버전 업데이트 적용일자")
    @NotEmpty
    private String updateApplyDt;

    @ApiModelProperty(value = "등록자사번")
    private String regEmpId;

    @ApiModelProperty(value = "등록자명")
    private String regEmpNm;

}
