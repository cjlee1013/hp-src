package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class CategorySearchKeywordGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class CategorySearchKeyword {
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Long dcateSearchKeywordNo;
        @RealGridColumnInfo(headText = "카테고리", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String cateNm;
        @RealGridColumnInfo(headText = "키워드", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String keyword;
        @RealGridColumnInfo(headText = "재설정대상", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String resetYn;
        @RealGridColumnInfo(headText = "사용여부", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String useYn;
        @RealGridColumnInfo(headText = "사용여부", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useYnNm;
        @RealGridColumnInfo(headText = "등록자", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String regNm;
        @RealGridColumnInfo(headText = "등록일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String chgNm;
        @RealGridColumnInfo(headText = "수정일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String chgDt;
        @RealGridColumnInfo(headText = "확인일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String confirmDt;
    }
}
