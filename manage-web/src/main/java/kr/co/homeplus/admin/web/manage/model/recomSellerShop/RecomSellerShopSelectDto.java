package kr.co.homeplus.admin.web.manage.model.recomSellerShop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class RecomSellerShopSelectDto {

    @RealGridColumnInfo(headText = "판매업체ID", width = 120)
    private String partnerId;

    @RealGridColumnInfo(headText = "셀러샵명", width = 120, order = 1)
    private String shopNm;

    @RealGridColumnInfo(headText = "사용여부", order = 2)
    private String recommendUseYnTxt;

    @RealGridColumnInfo(headText = "상품추천", order = 3)
    private String recommendTypeTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 4)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 5, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "shopInfo", order = 6, hidden = true)
    private String shopInfo;
    @RealGridColumnInfo(headText = "recommendUseYn", order = 7, hidden = true)
    private String recommendUseYn;
    @RealGridColumnInfo(headText = "recommendType", order = 8, hidden = true)
    private String recommendType;


    private Integer recommendNo;
    private String shopUseYnTxt;

}
