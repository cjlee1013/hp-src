package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyApplyDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyApplySelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.pg.service.PaymentPolicyManageService;
import kr.co.homeplus.admin.web.pg.service.PgDivideManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 결제관리 > PG 배분율 관리 > 결제정책 상품 관리
 */
@Controller
@RequestMapping("/pg/pgDivide")
public class PaymentPolicyItemManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final PaymentPolicyManageService paymentPolicyManageService;
    private final PgDivideManageService pgDivideManageService;

    public PaymentPolicyItemManageController(LoginCookieService loginCookieService, AuthorityService authorityService, PaymentPolicyManageService paymentPolicyManageService, PgDivideManageService pgDivideManageService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.paymentPolicyManageService = paymentPolicyManageService;
        this.pgDivideManageService = pgDivideManageService;
    }

    /**
     * 결제정책 상품 관리 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/paymentPolicyItemManageMain")
    public String paymentPolicyItemManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_PRODUCT_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("paymentPolicyItemGridBaseInfo", PaymentPolicyManageDto.class));
        model.addAllAttributes(RealGridHelper.create("applyItemGridBaseInfo", PaymentPolicyApplyDto.class));
        model.addAllAttributes(RealGridHelper.create("applyCategoryGridBaseInfo", PaymentPolicyApplyDto.class));
        model.addAllAttributes(RealGridHelper.create("applyAffiliateGridBaseInfo", PaymentPolicyApplyDto.class));
        model.addAllAttributes(RealGridHelper.create("exposMethodGridBaseInfo", PaymentMethodTreeDto.class));
        model.addAttribute("siteTypeList", Arrays.asList(SiteType.values()));
        model.addAttribute("parentMethodCdList", ParentMethodCd.getBasicParentMethodList());
        model.addAttribute("inicisCdList", pgDivideManageService.getPgMidManageList(PgKind.INICIS.getPgCd(), "Y"));
        model.addAttribute("tosspgCdList", pgDivideManageService.getPgMidManageList(PgKind.TOSSPG.getPgCd(), "Y"));

        return "/pg/paymentPolicyItemManageMain";
    }

    /**
     * 결제정책 적용대상 조회
     * @param policyNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getPaymentPolicyApplyList.json")
    public List<PaymentPolicyApplyDto> getPaymentPolicyApplyList(@RequestParam(value = "policyNo") Long policyNo) throws Exception {
        PaymentPolicyApplySelectDto paymentPolicyApplySelectDto = new PaymentPolicyApplySelectDto();
        paymentPolicyApplySelectDto.setPolicyNo(policyNo);
        return paymentPolicyManageService.getPaymentPolicyApplyList(paymentPolicyApplySelectDto);
    }

    /**
     * 결제정책 적용대상 저장
     * @param paymentPolicyApplyDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/savePaymentPolicyApply.json")
    public ResponseObject<Object> savePaymentPolicyApply(@RequestBody PaymentPolicyApplyDto paymentPolicyApplyDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        paymentPolicyApplyDto.setRegId(userCd);

        return paymentPolicyManageService.savePaymentPolicyApply(paymentPolicyApplyDto);
    }
}