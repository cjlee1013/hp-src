package kr.co.homeplus.admin.web.partner.model;

import lombok.Data;

@Data
//판매자관리 > 판매자 임시패스워드 발급 Set Entry
public class PartnerTempPwSetDto {

	//판매업체ID
	private String partnerId;

	//판매업체명
	private String businessNm;

	//이메일 선택
	private String emailCheck;

	//휴대폰 선택
	private String mobileCheck;

	//등록/수정자
	private String userId;

}
