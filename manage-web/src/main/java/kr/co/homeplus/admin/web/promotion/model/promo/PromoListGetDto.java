package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class PromoListGetDto {
    @RealGridColumnInfo(headText = "기획전 번호", sortable = true)
    private Long promoNo;

    @RealGridColumnInfo(headText = "관리기획전명", width = 400, columnType = RealGridColumnType.NAME)
    private String mngPromoNm;

    @RealGridColumnInfo(headText = "전시기획전명", width = 400, columnType = RealGridColumnType.NAME)
    private String dispPromoNm;

    @RealGridColumnInfo(headText = "적용시스템", width = 200)
    private String applySystem;

    @RealGridColumnInfo(headText = "사이트구분")
    private String siteType;

    @RealGridColumnInfo(headText = "상품 점포유형")
    private String storeTypes;

    @RealGridColumnInfo(headText = "혜택존 전시순서", sortable = true)
    private Integer mainPriority;

    @RealGridColumnInfo(headText = "쿠폰사용여부")
    private String couponUseYn;

    @RealGridColumnInfo(headText = "전시여부")
    private String dispYn;

    @RealGridColumnInfo(headText = "사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "전시시작일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}