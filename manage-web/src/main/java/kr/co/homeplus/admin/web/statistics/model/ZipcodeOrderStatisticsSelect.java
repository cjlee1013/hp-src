package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "우편번호별 주문현황 조회용 DTO")
@Getter
@Setter
public class ZipcodeOrderStatisticsSelect {
    @ApiModelProperty(value = "조회기간 유형(ORDER:주문기간,SHIP:배송기간)", position = 1)
    private String schDateType;

    @ApiModelProperty(value = "조회시작일자(YYYY-MM-DD)", position = 2)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일자(YYYY-MM-DD)", position = 3)
    private String schEndDt;

    @ApiModelProperty(value = "점포유형(HYPER,EXP)", position = 4)
    private String schStoreType;

    @ApiModelProperty(value = "점포ID", position = 5)
    private String schStoreId;

    /* 추출 히스토리 저장용 DTO */
    private ExtractCommonSetDto extractCommonSetDto;
}
