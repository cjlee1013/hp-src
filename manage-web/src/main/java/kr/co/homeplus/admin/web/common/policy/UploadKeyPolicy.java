package kr.co.homeplus.admin.web.common.policy;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 이미지 및 파일 업로드 key
 */
@RequiredArgsConstructor
public enum UploadKeyPolicy {

		PRODUCT_LIST_IMAGE("ProductListImageResizing", "상품 리스트 이미지", "product_no", false)
	,	PRODUCT_MAIN_IMAGE("ProductMainImageResizing", "상품 메인 이미지", "product_no", false)
	, 	PRODUCT_MAIN_ORIGIN_IMAGE("ProductMainOriginImage", "상품 메인 원본 이미지", "product_no", false)
	, 	PRODUCT_DETAIL_IMAGE("ProductDetailImageWidthResizing", "상품 상세 이미지", "product_no", true)
	, 	PAYMENT_METHOD_IMAGE("PaymentMethodDto", "결제 수단 이미지", "method_cd", false)
	;

	@Getter
	private final String uploadKey;

	@Getter
	private final String description;

	@Getter
	private final String baseKeyNm;

	@Getter
	private final boolean isMulti;

	// uploadKey로 데이터 검색
	public static UploadKeyPolicy valueForUploadKey(String key) throws Exception {
		for (UploadKeyPolicy value : values()) {
			if (value.uploadKey.equals(key)) {
				return value;
			}
		}
		return null;
	}
}
