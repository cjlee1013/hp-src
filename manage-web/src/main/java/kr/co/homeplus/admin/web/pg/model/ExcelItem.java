package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class ExcelItem {
    private String itemNo;
    private String itemNm;
    private int errorCount;
}
