package kr.co.homeplus.admin.web.promotion.model.coupon;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class CouponFileGetDto {
    @RealGridColumnInfo(headText = "쿠폰번호", sortable = true, hidden = true)
    private Long couponNo;

    @RealGridColumnInfo(headText = "쿠폰파일seq", hidden = true)
    private Long couponFileNo;

    @RealGridColumnInfo(headText = "첨부파일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 200)
    private String fileNm;

    @RealGridColumnInfo(headText = "등록일")
    private String regDt;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "배치상태", hidden = true)
    private String batchStatus;

    @RealGridColumnInfo(headText = "배치상태")
    private String batchStatusNm;

    @RealGridColumnInfo(headText = "취소처리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BUTTON, alwaysShowButton = false, buttonType = RealGridButtonType.ACTION)
    private String cancelProc;
}