package kr.co.homeplus.admin.web.settle.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "제휴정산일마감 조회")
public class MarketSettleListSetDto {
  @ApiModelProperty(notes = "시작일자")
  private String startDt;

  @ApiModelProperty(notes = "종료일자")
  private String endDt;

  @ApiModelProperty(notes = "검색기간")
  private String dateType;

  @ApiModelProperty(notes = "제휴사")
  private String siteType;

  @ApiModelProperty(notes = "주문번호")
  private String marketOrderNo;

  @ApiModelProperty(notes = "상품주문번호")
  private String marketOrderItemNo;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;
}
