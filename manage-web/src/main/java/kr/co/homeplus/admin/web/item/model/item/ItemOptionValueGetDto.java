package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 옵션 정보
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
public class ItemOptionValueGetDto {
	@RealGridColumnInfo(headText = "옵션번호", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL, sortable = true)
	private long optNo;
	private long itemNo;
	@RealGridColumnInfo(headText = "옵션명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true)
	private String opt1Val;
	@RealGridColumnInfo(headText = "옵션명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, hidden = true)
	private String opt2Val;
	@RealGridColumnInfo(headText = "옵션가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, hidden = true)
	private int optPrice;
	private int priority;
	@RealGridColumnInfo(headText = "전시여부코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String dispYn;
	private String hyperDispYn;
	private String clubDispYn;
	private String expDispYn;
	@RealGridColumnInfo(headText = "재고수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, hidden = true)
	private int stockQty;
	@RealGridColumnInfo(headText = "업체옵션코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String sellerOptCd;
	@RealGridColumnInfo(headText = "사용여부코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String useYn;
	private String syncStock;
	private String regId;
	private String regDt;
	private String chgId;
	private String chgDt;

	private String regNm;
	private String chgNm;
	@RealGridColumnInfo(headText = "사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String dispYnNm;
	@RealGridColumnInfo(headText = "업체옵션코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String useYnNm;

	/**
	 * 판매된 수량
	 */
	private int salesQty;
}
