package kr.co.homeplus.admin.web.statistics.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.statistics.model.OrderSubstitutionStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.OrderSubstitutionStatisticsSetDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderSubstitutionStatisticsService {

    private final ResourceClient resourceClient;

    public List<OrderSubstitutionStatisticsGetDto> getOrderSubstitutionInfo(OrderSubstitutionStatisticsSetDto setDto) {
        List<OrderSubstitutionStatisticsGetDto> responseList = resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            setDto,
            "/statistics/substitution/getOrderSubstitutionStatistics",
            new ParameterizedTypeReference<ResponseObject<List<OrderSubstitutionStatisticsGetDto>>>() {}).getData();
        if(responseList != null) {
            for(OrderSubstitutionStatisticsGetDto dto : responseList) {
                dto.setTotSubstitutionOosCnt(dto.getTotSubstitutionOosCnt() + dto.getTotCompleteSubstitutionCnt());
            }
        }

        return responseList;
    }

    /**
     * 점포 기본정보 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    public StoreBasicInfoDto getStoreBasicInfo(String storeId) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("storeId", storeId));
        String apiUri = EscrowConstants.ESCROW_GET_STORE_BASIC_INFO + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<StoreBasicInfoDto>>() {};
        return (StoreBasicInfoDto) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }
}
