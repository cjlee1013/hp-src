package kr.co.homeplus.admin.web.user.model.user;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * 어드민 > 회원상세 > 등급변경 > 콜센터 문의내역 조회 파라미터
 */
@Setter
@Getter
public class CallCenterInquiryParam {
    /** 회원번호 **/
    @NotNull(message = "회원번호를 입력하세요.")
    private Long userNo;

    /** 검색 시작일(not required) **/
    private String schStartDt;
    /** 검색 종료일(not required) **/
    private String schEndDt;
}
