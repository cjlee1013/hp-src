package kr.co.homeplus.admin.web.mileage.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 관리 > 마일리지 내역 조회 응답")
public class MileageListGetDto {
    @ApiModelProperty(value = "사용가능마일리지", position = 1)
    private long mileageAmt;
    @ApiModelProperty(value = "소멸예정마일리지", position = 2)
    private long expiredMileageAmt;
    @ApiModelProperty(value = "마일리지내역리스트", position = 3)
    private List<MileageListDto> mileageHistoryList;
}
