package kr.co.homeplus.admin.web.user.enums;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum UserShippingAddress {
    UNKNOWN(0, ""),
    BASIC(1, "기본 배송지")
    ;

    @Getter
    int code;
    @Getter
    String desc;

    public static UserShippingAddress getUserShippingAddressPolicy(int code) {
        return Arrays.stream(values())
            .filter(shippingAddressPolicy -> shippingAddressPolicy.getCode() == code)
            .findFirst()
            .orElse(null);
    }
}
