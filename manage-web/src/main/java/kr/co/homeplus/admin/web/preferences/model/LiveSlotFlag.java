package kr.co.homeplus.admin.web.preferences.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "고객별 첫배송 슬롯 보여주기 On/Off 정보")
public class LiveSlotFlag {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(notes = "고객별 첫배송 슬롯 보여주기 On 여부", required = true)
    private boolean liveSlotOn;
}
