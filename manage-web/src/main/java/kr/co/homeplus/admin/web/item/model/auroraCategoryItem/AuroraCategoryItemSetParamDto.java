package kr.co.homeplus.admin.web.item.model.auroraCategoryItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraCategoryItemSetParamDto {

    private Integer mCateCd;
    private String itemNo;
    private String dispYn;
    private String regId;

}
