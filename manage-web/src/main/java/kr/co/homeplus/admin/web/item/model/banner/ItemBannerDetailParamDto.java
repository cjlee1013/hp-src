package kr.co.homeplus.admin.web.item.model.banner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerDetailParamDto {

    //공지번호
    private Long bannerNo;

}
