package kr.co.homeplus.admin.web.settle.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum SettleCycleType {
    //정산주기
    SETTLE_CYCLE_TYPE_M("1", "월정산"),
    SETTLE_CYCLE_TYPE_W("2", "주정산"),
    SETTLE_CYCLE_TYPE_D("3", "일정산");

    private String code;
    private String name;
}
