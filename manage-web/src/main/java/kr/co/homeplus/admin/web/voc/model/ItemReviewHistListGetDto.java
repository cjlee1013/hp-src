package kr.co.homeplus.admin.web.voc.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ItemReviewHistListGetDto {

    @RealGridColumnInfo(headText = "노출여부", width = 50)
    private String blockYnNm;

    @RealGridColumnInfo(headText = "미노출 사유", width = 250, order = 1)
    private String blockContents;

    @RealGridColumnInfo(headText = "수정자", order = 2)
    private String regNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 120, order = 3)
    private String chgDt;

}