package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipReserveManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipReserveManageListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.admin.web.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipReserveManageService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 점포배송관리 리스트 조회
   */
  public List<ShipReserveManageListGetDto> getShipReserveManageList(ShipReserveManageListSetDto shipReserveManageListSetDto) {

    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipReserveManageListSetDto.setSchStoreId(shipCommonService.getStoreId());
      shipReserveManageListSetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipReserveManageListSetDto,
        "/admin/shipManage/getShipReserveManageList",
        new ParameterizedTypeReference<ResponseObject<List<ShipReserveManageListGetDto>>>(){}).getData();
  }

  /**
   * 슬롯시간 정보 조회
   */
  public FrontStoreSlot getShipSlotInfo(String storeId) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.ESCROW,
        "/slot/external/getStoreReserveSlots?storeId=" + storeId,
        new ParameterizedTypeReference<ResponseObject<FrontStoreSlot>>() {}).getData();
  }

  /**
   * 배송시간 일괄 수정
   */
  public int setShipSlotInfo(List<ShipSlotStockChangeDto> stockChangeDtoList) throws Exception {
    int successCnt = 0;

    for(ShipSlotStockChangeDto dto : stockChangeDtoList) {
      dto.setHistoryRegId(shipCommonService.getUserCd());
      dto.setHistoryRegChannel("ADMIN");

      ResponseObject<String> responseObject = resourceClient.postForResponseObject(
          ResourceRouteName.ESCROW,
          dto,
          "/slot/external/changeShipInfoAndOrderSlotStock",
          new ParameterizedTypeReference<ResponseObject<String>>(){});

      if("SUCCESS".equals(responseObject.getReturnCode())) {
        successCnt ++;
      }
    }

    return successCnt;
  }
}
