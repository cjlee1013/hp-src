package kr.co.homeplus.admin.web.manage.model.dspMainTheme;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspMainThemeGetDto {

    @RealGridColumnInfo(headText = "테마번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long themeId;

    @RealGridColumnInfo(headText = "사이트 구분", width = 70, order = 1)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "테마명",columnType = RealGridColumnType.NAME, fieldType = RealGridFieldType.TEXT, width = 200, order = 2)
    private String themeNm;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 3, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "전시 시작일", columnType = RealGridColumnType.DATE, width = 120, order = 4, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", columnType = RealGridColumnType.DATE, width = 120, order = 5, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 6, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 10, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "siteType", order = 11, hidden = true)
    private String siteType;
    @RealGridColumnInfo(headText = "useYn", order = 12, hidden = true)
    private String useYn;


    private String regId;
    private String chgId;

}
