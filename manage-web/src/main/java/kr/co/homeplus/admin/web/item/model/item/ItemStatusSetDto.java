package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 중지사유 등록
 */
@Getter
@Setter
public class ItemStatusSetDto {

	//상품번호
	private List<String> itemNo;

	//상품상태
	private String itemStatus;

	//중지사유
	private String stopReason;

	//거래유형 (DS : 업체상품 , TD : 매장상품)
	private String mallType;

	//등록|수정자
	private String userId;
}
