package kr.co.homeplus.admin.web.user.model.consumer;

import kr.co.homeplus.admin.web.user.enums.ConsumerReasonType;
import kr.co.homeplus.admin.web.user.enums.ConsumerType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Setter
@Getter
public class ConsumerListGrid {
    @RealGridColumnInfo(headText = "일련번호", hidden = true, width = 50)
    private long seq;
    @RealGridColumnInfo(headText = "회원번호", hidden = true)
    private long userNo;
    @RealGridColumnInfo(headText = "구분", width = 30, sortable = true)
    private String isRelease;
    @RealGridColumnInfo(headText = "기간")
    private String period;
    @RealGridColumnInfo(headText = "사유", columnType = RealGridColumnType.LOOKUP, width = 90)
    private ConsumerReasonType reasonType;

    @RealGridColumnInfo(headText = "권한정지 범위", columnType = RealGridColumnType.LOOKUP, width = 90)
    private ConsumerType consumerType;

    @RealGridColumnInfo(headText = "등록자", width = 50)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일시")
    private String regDt;

    @RealGridColumnInfo(headText = "해제사유")
    private String releaseDetail;

    @RealGridColumnInfo(headText = "해제일시")
    private String releaseDt;
}
