package kr.co.homeplus.admin.web.item.model.bannedWord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannedWordListParamDto {

    private String searchCateCd;
    private String searchKeyword;
    private String searchUseYn;
    private String searchType;
    private String searchBannedType;
    private String bannedType;

}
