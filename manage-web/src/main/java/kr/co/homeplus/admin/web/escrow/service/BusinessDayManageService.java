package kr.co.homeplus.admin.web.escrow.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.model.BusinessDayManageDto;
import kr.co.homeplus.admin.web.escrow.model.BusinessDayManageSelectDto;
import kr.co.homeplus.admin.web.escrow.model.CalendarDateDto;
import kr.co.homeplus.admin.web.escrow.model.CalendarDayDto;
import kr.co.homeplus.admin.web.escrow.utils.DateTimeUtils;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BusinessDayManageService {

    private final ResourceClient resourceClient;

    /**
     * 캘린더 출력용 데이터(일자) 조회
     * @param businessDayManageSelectDto
     * @return
     */
    public CalendarDateDto getCalendarDataList(BusinessDayManageSelectDto businessDayManageSelectDto) {
        CalendarDateDto calendarDateDto = new CalendarDateDto();

        // 오늘날짜, 조회 할 년도와 월 setting
        String today = DateTimeUtils.getCurrentYmd();
        int todayYear = Integer.parseInt(today.split("-")[0]);
        int todayMonth = Integer.parseInt(today.split("-")[1]);
        int schYear = StringUtil.isEmpty(businessDayManageSelectDto.getSchYear()) ? todayYear: businessDayManageSelectDto.getSchYear();
        int schMonth = StringUtil.isEmpty(businessDayManageSelectDto.getSchMonth()) ? todayMonth : businessDayManageSelectDto.getSchMonth();
        calendarDateDto.setToday(today);
        calendarDateDto.setYear(schYear);
        calendarDateDto.setMonth(schMonth);

        // 조회 할 달의 1일자 요일 setting
        int firstDayOfWeek = DateTimeUtils.getFirstDayOfWeek(schYear, schMonth);
        calendarDateDto.setFirstDayOfWeek(firstDayOfWeek);

        // 조회 할 달의 일자들 속성 List setting
        List<CalendarDayDto> calendarDayDtoList = new ArrayList<>();
        for (int curDay=1; curDay<=DateTimeUtils.getEndOfMonth(schYear, schMonth); curDay++) {
            CalendarDayDto calendarDayDto = new CalendarDayDto();
            calendarDayDto.setDay(curDay);
            calendarDayDto.setDate(DateTimeUtils.getDigitYmd(schYear, schMonth, curDay));
            calendarDayDto.setDayOfWeek(DateTimeUtils.getDayOfWeek(schYear, schMonth, curDay));
            calendarDayDtoList.add(calendarDayDto);
        }
        calendarDateDto.setDayList(calendarDayDtoList);

        return calendarDateDto;
    }

    /**
     * 영업일 리스트 조회
     * @param businessDayManageSelectDto
     * @return
     */
    public List<BusinessDayManageDto> getBusinessDayList(BusinessDayManageSelectDto businessDayManageSelectDto) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schYear", businessDayManageSelectDto.getSchYear()));
        setParameterList.add(SetParameter.create("schMonth", businessDayManageSelectDto.getSchMonth()));

        String apiUri = EscrowConstants.ESCROW_GET_BUSINESS_DAY_LIST + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BusinessDayManageDto>>>() {};
        return (List<BusinessDayManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 영업일 등록
     * @param businessDayManageDto
     * @return
     */
    public ResponseObject<Object> saveBusinessDay(BusinessDayManageDto businessDayManageDto) {
        String apiUri = EscrowConstants.ESCROW_SAVE_BUSINESS_DAY;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, businessDayManageDto, apiUri, typeReference);
    }

    /**
     * 영업일 수정
     * @param businessDayManageDto
     * @return
     */
    public ResponseObject<Object> updateBusinessDay(BusinessDayManageDto businessDayManageDto) {
        String apiUri = EscrowConstants.ESCROW_UPDATE_BUSINESS_DAY;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, businessDayManageDto, apiUri, typeReference);
    }
}
