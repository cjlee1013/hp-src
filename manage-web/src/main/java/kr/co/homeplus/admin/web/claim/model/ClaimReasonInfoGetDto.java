package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimReasonInfoGetDto {

    @ApiModelProperty(value = "신청사유", position = 1)
    public String claimReasonType;

    @ApiModelProperty(value = "신청사유코드", position = 2)
    public String claimReasonTypeCode;

    @ApiModelProperty(value = "신청사유상세", position = 3)
    public String claimReasonDetail;

    @ApiModelProperty(value="환불수단 리스트", position = 4)
    private List<RefundTypeList> refundTypeList;

    @ApiModelProperty(value="클레임 보류 사유코드", position = 5)
    public String pendingReasonType;

    @ApiModelProperty(value="클레임 보류 상세사유", position = 6)
    public String pendingReasonDetail;

    @ApiModelProperty(value="클레임 거절 사유코드", position = 7)
    public String rejectReasonType;

    @ApiModelProperty(value="클레임 거절 상세사유", position = 8)
    public String rejectReasonDetail;

    @ApiModelProperty(value="발송상태", position = 9)
    public String receiveStatus;

    @ApiModelProperty(value = "환불상태", position = 10)
    private String refundStatus;

    @ApiModelProperty(value = "첨부파일1", position = 11)
    private String uploadFileName;

    @ApiModelProperty(value = "첨부파일2", position = 12)
    private String uploadFileName2;

    @ApiModelProperty(value = "첨부파일3", position = 13)
    private String uploadFileName3;

    @ApiModelProperty(value = "귀책사유", position = 14)
    private String whoReason;

    @ApiModelProperty(value = "추가배송비", position = 15)
    private String totAddShipPrice;

    @ApiModelProperty(value = "추가도서산간배송비", position = 16)
    private String totAddIslandShipPrice;

    @ApiModelProperty(value="배송비 미차감 여부", position = 17)
    private String isNoDiscountDeduct;

    @ApiModelProperty(value="반품 배송 정보", position = 18)
    private ClaimPickShippingDto claimPickShipping;

    @ApiModelProperty(value="교환 배송 정보", position = 19)
    private ClaimExchShippingDto claimExchShipping;

    @ApiModelProperty(value="배송비 동봉 여부", position = 20)
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "클레임상테", position = 21)
    public String claimStatus;

    @ApiModelProperty(value = "클레임타입코드", position = 22)
    private String claimStatusCode;

    @ApiModelProperty(value = "번들번호", position = 23)
    private long bundleNo;

    @ApiModelProperty(value = "주문번호", position = 24)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "고객신청여부", position = 25)
    private String userRequestYn;

    @ApiModelProperty(value = "행사미차감여부", position = 26)
    private String deductPromoYn;

    @ApiModelProperty(value = "할인미차감여부", position = 27)
    private String deductDiscountYn;

    @ApiModelProperty(value = "배송비미차감여부", position = 38)
    private String deductShipYn;

    @ApiModelProperty(value = "마켓여부", position = 39)
    private String marketType;

    @Getter
    @Setter
    public static class RefundTypeList {
        @ApiModelProperty(value = "클레임 결제번호", position = 1)
        private String claimPaymentNo;

        @ApiModelProperty(value = "환불수단", position = 2)
        private String refundType;

        @ApiModelProperty(value = "환불수단", position = 3)
        private String refundTypeCode;

        @ApiModelProperty(value="결제수단", position = 4)
        private String paymentType;

        @ApiModelProperty(value="결제수단", position = 5)
        private String paymentTypeCode;

        @ApiModelProperty(value = "환불상태", position = 6)
        private String refundStatus;

        @ApiModelProperty(value = "원 환불요청 결제번호", position = 7)
        private String originClaimPaymentNo;

        @ApiModelProperty(value = "환불상태명", position = 8)
        private String refundStatusName;

        @ApiModelProperty(value = "결제명", position = 9)
        private String methodNm;

        public RefundTypeList(){
            this.claimPaymentNo = "";
            this.refundType = "";
            this.refundTypeCode = "";
            this.paymentType = "";
            this.paymentTypeCode = "";
            this.refundStatus = "";
            this.originClaimPaymentNo = "";
            this.refundStatusName = "";
            this.methodNm = "";
        }
    }


}