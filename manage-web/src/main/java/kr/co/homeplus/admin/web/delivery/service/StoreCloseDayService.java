package kr.co.homeplus.admin.web.delivery.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_CLOSE_DAY_IMAGE_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_STORE_CLOSE_DAY_LIST;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_SAVE_CLOSE_DAY_IMAGE_LIST;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDayImage;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDayModel;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDaySelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreCloseDayService {

    private final ResourceClient resourceClient;

    /**
     * 점포별 휴일 리스트 조회
     * @param storeCloseDaySelectModel
     * @return
     */
    public List<StoreCloseDayModel> getStoreCloseDayList(StoreCloseDaySelectModel storeCloseDaySelectModel) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schStoreType", storeCloseDaySelectModel.getSchStoreType()));
        setParameterList.add(SetParameter.create("schCloseDayType", storeCloseDaySelectModel.getSchCloseDayType()));
        setParameterList.add(SetParameter.create("schKeywordType", storeCloseDaySelectModel.getSchKeywordType()));
        setParameterList.add(SetParameter.create("schKeyword", storeCloseDaySelectModel.getSchKeyword()));
        setParameterList.add(SetParameter.create("schUseYn", storeCloseDaySelectModel.getSchUseYn()));
        setParameterList.add(SetParameter.create("schApplyStartDt", storeCloseDaySelectModel.getSchApplyStartDt()));
        setParameterList.add(SetParameter.create("schApplyEndDt", storeCloseDaySelectModel.getSchApplyEndDt()));

        String apiUri = ESCROW_GET_STORE_CLOSE_DAY_LIST + StringUtil.getParameter(setParameterList);

        ResourceClientRequest<List<StoreCloseDayModel>> request = ResourceClientRequest.<List<StoreCloseDayModel>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 휴무 이미지 리스트 조회
     * @return
     */
    public List<StoreCloseDayImage> getCloseDayImageList() {
        String apiUri = ESCROW_GET_CLOSE_DAY_IMAGE_LIST;

        ResourceClientRequest<List<StoreCloseDayImage>> request = ResourceClientRequest.<List<StoreCloseDayImage>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 휴일 이미지 저장
     */
    public ResponseObject<Object> saveCloseDayImageList(StoreCloseDayImage storeCloseDayImage) throws Exception {
        String apiUri = ESCROW_SAVE_CLOSE_DAY_IMAGE_LIST;

        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(storeCloseDayImage)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
