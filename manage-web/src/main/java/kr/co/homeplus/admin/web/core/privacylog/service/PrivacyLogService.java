package kr.co.homeplus.admin.web.core.privacylog.service;

import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;

public interface PrivacyLogService {
    /**
     * log-service 로 개인정보 접근이력을 전송합니다.
     * @param param 개인정보 접근이력 파라미터
     */
    void send(PrivacyLogInfo param);
}
