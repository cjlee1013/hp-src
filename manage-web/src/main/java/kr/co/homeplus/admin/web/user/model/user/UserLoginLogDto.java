package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class UserLoginLogDto {

    @RealGridColumnInfo(headText = "로그인일시")
    private String loginDt;
    @RealGridColumnInfo(headText = "사이트구분")
    private String siteType;
    @RealGridColumnInfo(headText = "로그인유형")
    private String loginType;
    @RealGridColumnInfo(headText = "아이피")
    private String clientIp;

}
