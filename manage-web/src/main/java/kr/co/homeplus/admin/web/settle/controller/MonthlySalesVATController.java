package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlyAdjustListGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATListGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATListSetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATOrderGetDto;
import kr.co.homeplus.admin.web.settle.model.monthlySalesVAT.MonthlySalesVATOrderSetDto;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.admin.web.settle.service.MonthlySalesVATService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/monthlySalesVAT")
public class MonthlySalesVATController {

    private final MonthlySalesVATService MonthlySalesVATService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public MonthlySalesVATController(MonthlySalesVATService MonthlySalesVATService, SettleCommonService settleCommonService, CodeService codeService) {
        this.MonthlySalesVATService = MonthlySalesVATService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 세금계산서관리 > 부가세신고내역 조회 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/monthlySalesVATMain", method = RequestMethod.GET)
    public String MonthlySalesVATMain(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        //년도 조회
        model.addAttribute("endYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("endMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.createForGroup("monthlySalesVATListBaseInfo", MonthlySalesVATListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("monthlySalesVATDetailBaseInfo", MonthlySalesVATOrderGetDto.class));
        model.addAllAttributes(RealGridHelper.create("adjustListBaseInfo", MonthlyAdjustListGetDto.class));

        return "/settle/monthlySalesVATList";
    }

    /**
     * 부가세신고내역 > 메인 리스트 조회
     * @return MonthlySalesVATListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMonthlySalesVATList.json", method = RequestMethod.GET)
    public List<MonthlySalesVATListGetDto> getMonthlySalesVATList(MonthlySalesVATListSetDto listParamDto) throws Exception {
        return MonthlySalesVATService.getMonthlySalesVATList(listParamDto);
    }

    /**
     * 부가세신고내역 > 주문 상세내역
     * @return MonthlySalesVATOrderGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMonthlySalesVATOrder.json", method = RequestMethod.GET)
    public List<MonthlySalesVATOrderGetDto> getMonthlySalesVATOrder(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return MonthlySalesVATService.getMonthlySalesVATOrder(listParamDto);
    }

    /**
     * 부가세신고내역 > 정산조정 상세내역
     * @return MonthlyAdjustListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMonthlyAdjustList.json", method = RequestMethod.GET)
    public List<MonthlyAdjustListGetDto> getMonthlyAdjustList(MonthlySalesVATOrderSetDto listParamDto) throws Exception {
        return MonthlySalesVATService.getMonthlyAdjustList(listParamDto);
    }
}