package kr.co.homeplus.admin.web.delivery.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_ITEM_SHIFT_MANAGE_LIST;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageInsertModel;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ItemShiftManageService {

    private final ResourceClient resourceClient;

    /**
     * 상품별 Shift 관리 리스트 조회
     * @param itemShiftManageSelectModel
     * @return
     */
    public List<ItemShiftManageModel> getItemShiftManageList(ItemShiftManageSelectModel itemShiftManageSelectModel) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("schStoreType", itemShiftManageSelectModel.getSchStoreType()));
        setParameterList.add(SetParameter.create("schStoreId", itemShiftManageSelectModel.getSchStoreId()));
        setParameterList.add(SetParameter.create("schKeywordType", itemShiftManageSelectModel.getSchKeywordType()));
        setParameterList.add(SetParameter.create("schKeyword", itemShiftManageSelectModel.getSchKeyword()));
        setParameterList.add(SetParameter.create("schLcateCd", itemShiftManageSelectModel.getSchLcateCd()));
        setParameterList.add(SetParameter.create("schMcateCd", itemShiftManageSelectModel.getSchMcateCd()));
        setParameterList.add(SetParameter.create("schScateCd", itemShiftManageSelectModel.getSchScateCd()));
        setParameterList.add(SetParameter.create("schDcateCd", itemShiftManageSelectModel.getSchDcateCd()));

        ResourceClientRequest<List<ItemShiftManageModel>> request = ResourceClientRequest.<List<ItemShiftManageModel>>getBuilder()
            .apiId(ResourceRouteName.ESCROWMNG)
            .uri(ESCROW_GET_ITEM_SHIFT_MANAGE_LIST + StringUtil.getParameter(setParameterList))
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 단일상품 Shift 등록 (다중점포)
     * @param itemShiftManageInsertModelList
     * @return
     */
    public ResponseObject<Integer> saveItemShiftList(List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
            .apiId(ResourceRouteName.ESCROWMNG)
            .uri(EscrowConstants.ESCROW_SAVE_ITEM_SHIFT_LIST)
            .postObject(itemShiftManageInsertModelList)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request,new TimeoutConfig()).getBody();
    }

    /**
     * 단일상품 Shift 등록 (단일점포)
     * @param itemShiftManageInsertModel
     * @return
     */
    public ResponseObject<Integer> saveItemShift(ItemShiftManageInsertModel itemShiftManageInsertModel) {
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
            .apiId(ResourceRouteName.ESCROWMNG)
            .uri(EscrowConstants.ESCROW_SAVE_ITEM_SHIFT)
            .postObject(itemShiftManageInsertModel)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request,new TimeoutConfig()).getBody();
    }

    /**
     * 상품별 Shift 관리 리스트 삭제
     * @param itemShiftManageInsertModelList
     */
    public void deleteItemShiftList(List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        ResourceClientRequest<Void> request = ResourceClientRequest.<Void>postBuilder()
            .apiId(ResourceRouteName.ESCROWMNG)
            .uri(EscrowConstants.ESCROW_DELETE_ITEM_SHIFT_LIST)
            .postObject(itemShiftManageInsertModelList)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        resourceClient.post(request,new TimeoutConfig());
    }
}
