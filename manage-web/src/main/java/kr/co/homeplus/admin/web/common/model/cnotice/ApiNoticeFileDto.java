package kr.co.homeplus.admin.web.common.model.cnotice;

import lombok.Data;

@Data
public class ApiNoticeFileDto {
    private String fileUrl;
    private String fileNm;
}
