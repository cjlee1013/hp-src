package kr.co.homeplus.admin.web.claim.model.storeMultiCancel;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class StoreMultiCancelListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    @RealGridColumnInfo(headText = "주문번호")
    public String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 2)
    @RealGridColumnInfo(headText = "배송번호", hidden = true)
    public String shipNo;

    @ApiModelProperty(value = "배송번호", position = 3)
    @RealGridColumnInfo(headText = "배송번호")
    public String bundleNo;

    @ApiModelProperty(value = "주문일시", position = 4)
    @RealGridColumnInfo(headText = "주문일시", width = 120)
    public String orderDt;

    @ApiModelProperty(value = "배송 요청일", position = 5)
    @RealGridColumnInfo(headText = "배송 요청일", width = 120)
    public String shipDt;

    @ApiModelProperty(value = "배송shift", position = 6)
    @RealGridColumnInfo(headText = "배송shift", width = 80)
    public String shiftId;

    @ApiModelProperty(value = "배송상태", position = 7)
    @RealGridColumnInfo(headText = "배송상태", width = 80)
    public String shipStatus;

    @ApiModelProperty(value = "구매자", position = 8)
    @RealGridColumnInfo(headText = "구매자", width = 80)
    public String buyerNm;

    @ApiModelProperty(value = "구매자연락처", position = 9)
    @RealGridColumnInfo(headText = "구매자연락처", width = 80)
    public String buyerMobileNo;

    @ApiModelProperty(value = "수령인", position = 10)
    @RealGridColumnInfo(headText = "수령인", width = 80)
    public String receiverNm;

    @ApiModelProperty(value = "수령인연락처", position = 11)
    @RealGridColumnInfo(headText = "수령인연락처", width = 80)
    public String shipMobileNo;

    @ApiModelProperty(value = "주문상품", position = 12)
    @RealGridColumnInfo(headText = "주문상품", width = 240, columnType = RealGridColumnType.NONE)
    public String itemNm;

    @ApiModelProperty(value = "결제금액", position = 13)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "결제금액", width = 80, columnType = RealGridColumnType.PRICE)
    public String itemPrice;

    @ApiModelProperty(value = "주소", position = 14)
    @RealGridColumnInfo(headText = "주소", width = 350)
    private String shipAddr;

    @ApiModelProperty(value = "마켓연동")
    @RealGridColumnInfo(headText = "마켓연동", width = 80, sortable = true)
    private String marketType;

    @ApiModelProperty(value = "마켓주문번호")
    @RealGridColumnInfo(headText = "마켓주문번호", width = 80, sortable = true)
    private String marketOrderNo;

}
