package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.admin.web.message.enums.ShipStatus;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class OrderSendListGrid {

    @RealGridColumnInfo(headText = "주문번호", hidden = true)
    private long purchaseOrderNo;
    @RealGridColumnInfo(headText = "배송번호")
    private String bundleNo;
    @RealGridColumnInfo(headText = "점포 id", hidden = true)
    private String storeId;
    @RealGridColumnInfo(headText = "점포명")
    private String storeNm;
    @RealGridColumnInfo(headText = "고객번호", hidden = true)
    private String userNo;
    @RealGridColumnInfo(headText = "주문자명")
    private String buyerNm;
    @RealGridColumnInfo(headText = "주문자휴대폰")
    private String buyerMobileNo;
    @RealGridColumnInfo(headText = "수취인명")
    private String receiverNm;
    @RealGridColumnInfo(headText = "수취인휴대폰")
    private String shipMobileNo;
    @RealGridColumnInfo(headText = "주문일시")
    private String orderDt;
    @RealGridColumnInfo(headText = "배송일")
    private String shipDt;
    @RealGridColumnInfo(headText = "Shift")
    private String shiftId;
    @RealGridColumnInfo(headText = "Slot")
    private String slotId;
    @RealGridColumnInfo(headText = "주문상태", columnType = RealGridColumnType.LOOKUP)
    private ShipStatus shipStatus;

    // 마스킹 처리가 안 된 데이터
    @RealGridColumnInfo(hidden = true)
    private String rawBuyerNm;
    @RealGridColumnInfo(hidden = true)
    private String rawBuyerMobileNo;
    @RealGridColumnInfo(hidden = true)
    private String rawReceiverNm;
    @RealGridColumnInfo(hidden = true)
    private String rawShipMobileNo;
}
