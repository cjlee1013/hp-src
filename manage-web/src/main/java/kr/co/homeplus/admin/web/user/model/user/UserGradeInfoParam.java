package kr.co.homeplus.admin.web.user.model.user;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

/**
 * 어드민 > 회원등급 변경 팝업 > 회원정보 조회 파라미터
 */
@Setter
@Getter
public class UserGradeInfoParam {
    /**
     * 회원번호
     */
    @NotNull(message = "회원번호를 입력하세요.")
    private Long userNo;

    public UserGradeInfoParam(final Long userNo) {
        this.userNo = userNo;
    }
}
