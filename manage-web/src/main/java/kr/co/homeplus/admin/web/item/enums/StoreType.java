package kr.co.homeplus.admin.web.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum StoreType {

        HYPER("HYPER", "Hyper")
    ,   EXP("EXP", "Express")
    ,   CLUB("CLUB", "Club")
    ,   DS("DS", "DS")
    ,   AURORA("AURORA", "AURORA");

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private static final ImmutableBiMap<StoreType, String> biMap;

    static {
        BiMap<StoreType, String> map = EnumHashBiMap.create(StoreType.class);

        for (StoreType policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static StoreType valueFor(final String type) {
        final StoreType policy =  biMap.inverse().get(type);

        return policy;
    }

}
