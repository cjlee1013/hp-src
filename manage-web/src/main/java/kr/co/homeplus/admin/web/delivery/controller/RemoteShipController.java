package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.EXP;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridGroupColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.RemoteShipManageSelectModel;
import kr.co.homeplus.admin.web.delivery.model.RemoteShipSlotModel;
import kr.co.homeplus.admin.web.delivery.service.RemoteShipService;
import kr.co.homeplus.admin.web.delivery.service.StoreZipcodeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 점포배송정보 > 원거리 배송 관리
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class RemoteShipController {

    private static final List<RealGridGroupColumn> REMOTE_SHIP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption REMOTE_SHIP_GRID_OPTION = new RealGridOption("none", false, false, false);

    private final AuthorityService authorityService;
    private final RemoteShipService remoteShipService;
    private final CodeService codeService;
    private final StoreZipcodeService storeZipcodeService;

    public RemoteShipController(AuthorityService authorityService, RemoteShipService remoteShipService, CodeService codeService, StoreZipcodeService storeZipcodeService) {
        this.authorityService = authorityService;
        this.remoteShipService = remoteShipService;
        this.codeService = codeService;
        this.storeZipcodeService = storeZipcodeService;

    }

    /**
     * 원거리 배송 관리 Main Page
     */
    @GetMapping("/remoteShipMain")
    public String remoteShipMain(Model model) throws Exception{
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type",   //점포유형
            "weekday"               //배송요일
        );
        List<MngCodeGetDto> storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> !EXP.getCode()
                        .equals(st.getMcCd()))
                .collect(Collectors.toList());
        model.addAttribute("storeType", storeTypeList);
        model.addAttribute("weekdayList", code.get("weekday"));
        // 시도명 리스트 가져와서 model 에 담기
        model.addAttribute("sidoList", storeZipcodeService.getSidoList());

        return "/delivery/remoteShipMain";
    }

    /**
     * 원거리 배송 조회
     * @param remoteShipManageSelectModel
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getRemoteShipList.json")
    public HashMap<String, Object> getRemoteShipList(@ModelAttribute RemoteShipManageSelectModel remoteShipManageSelectModel) throws Exception {

        HashMap<String, Object> resultMap = remoteShipService.getRemoteShipList(remoteShipManageSelectModel);
        HashMap<String, Object> viewResultMap = new HashMap<>();

        if (resultMap.get("returnStatus").equals("SUCCESS")) {
            ObjectMapper om = new ObjectMapper();
            List<RemoteShipSlotModel> remoteShipSlotModelList = om.convertValue(resultMap.get("storeSlotList"), new TypeReference<List<RemoteShipSlotModel>>() {
            });

            REMOTE_SHIP_GRID_HEAD.clear();
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("zipcode", "zipcode", "우편번호", RealGridColumnType.BASIC, 100, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("sidoNm", "sidoNm", "시", RealGridColumnType.BASIC, 100, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("sigunguNm", "sigunguNm", "구", RealGridColumnType.BASIC, 100, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("roadAddrNm", "roadAddrNm", "도로명", RealGridColumnType.BASIC, 200, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("buildBonNo", "buildBonNo", "건물번호", RealGridColumnType.BASIC, 100, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("sigunguBuildNm", "sigunguBuildNm", "건물명", RealGridColumnType.BASIC, 200, true,true, ""));
            REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn("storeNm", "storeNm", "점포명", RealGridColumnType.BASIC, 100, true,true, ""));

            for(RemoteShipSlotModel remoteShipSlotModel : remoteShipSlotModelList) {
                String name = remoteShipSlotModel.getSlotId();
                String headerText = "[" + remoteShipSlotModel.getSlotSeq() + "] " + remoteShipSlotModel.getSlotShipStartTime() + "-" + remoteShipSlotModel.getSlotShipEndTime() ;
                String groupName = remoteShipSlotModel.getShipDt() + "[" + remoteShipSlotModel.getShiftId() + "]";
                REMOTE_SHIP_GRID_HEAD.add(new RealGridGroupColumn(name, name, headerText, RealGridColumnType.BASIC, 100, true,true, groupName));
            }

            List<HashMap<String, Object>> remoteShipZipcodList = om.convertValue(resultMap.get("storeRemoteShipSlotList"), new TypeReference<List<HashMap<String, Object>>>() {
            });

            viewResultMap.put("returnStatus", "SUCCESS");
            viewResultMap.put("remoteShipGridBaseInfo", new RealGridBaseInfo("remoteShipGridBaseInfo", REMOTE_SHIP_GRID_OPTION, REMOTE_SHIP_GRID_HEAD).toStringGroup());
            viewResultMap.put("remoteShipGridData", remoteShipZipcodList);
        } else {
            viewResultMap.put("returnStatus", "FAIL");
        }

        return viewResultMap;
    }
}
