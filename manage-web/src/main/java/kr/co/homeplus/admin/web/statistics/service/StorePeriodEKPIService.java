package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.storePeriodEKPI.StorePeriodListGetDto;
import kr.co.homeplus.admin.web.statistics.model.storePeriodEKPI.StorePeriodListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StorePeriodEKPIService {

    private final ResourceClient resourceClient;

    /**
     * 통계 > 상품주문통계 > 기간별 점포/판매업체 매출 (E-KPI)
     */
    public List<StorePeriodListGetDto> getStoreList(StorePeriodListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/statStorePeriodEKPI/getStoreList",
            new ParameterizedTypeReference<ResponseObject<List<StorePeriodListGetDto>>>(){}).getData();
    }

}
