package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송정보 출력 응답 DTO")
public class ShipInfoPrintGetDto {
    @ApiModelProperty(notes = "점포명")
    private String storeNm;

    @ApiModelProperty(notes = "상품명")
    private String itemNm1;

    @ApiModelProperty(notes = "상품코드")
    private String itemNo;

    @ApiModelProperty(notes = "판매금액")
    private String orderPrice;

    @ApiModelProperty(notes = "포스번호")
    private String posNo;

    @ApiModelProperty(notes = "주문일시")
    private String orderDt;

    @ApiModelProperty(notes = "주문자")
    private String buyerNm;

    @ApiModelProperty(notes = "주문자연락처")
    private String buyerMobileNo;

    @ApiModelProperty(notes = "수취임")
    private String receiverNm;

    @ApiModelProperty(notes = "수취인연락처")
    private String shipMobileNo;

    @ApiModelProperty(notes = "주소")
    private String addr;

    @ApiModelProperty(notes = "수량")
    private String itemQty;

    @ApiModelProperty(notes = "영수증번호")
    private String tradeNo;
}
