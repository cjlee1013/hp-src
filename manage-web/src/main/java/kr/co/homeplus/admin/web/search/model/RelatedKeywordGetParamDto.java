package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@ApiModel(description = "연관검색어 > 리스트 조회")
public class RelatedKeywordGetParamDto {
    private int offset = 0;
    private int limit = 10000;
    private String schSiteType;
    private String schKeyword;
    private String schRelationType;
    private String schStartDt;
    private String schEndDt;
    private String schUseYn;
    private String schSearchType;
    private String schStatus;
    private String schDateType;
}
