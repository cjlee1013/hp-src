package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringDelayGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringDelaySetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipMonitoringDelayService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 발송지연조회 리스트 조회
   */
  public List<ShipMonitoringDelayGetDto> getShipMonitoringDelayList(ShipMonitoringDelaySetDto shipMonitoringDelaySetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipMonitoringDelaySetDto.setSchStoreId(shipCommonService.getStoreId());
      shipMonitoringDelaySetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipMonitoringDelaySetDto,
            "/admin/monitoring/getMonitoringDelayList",
            new ParameterizedTypeReference<ResponseObject<List<ShipMonitoringDelayGetDto>>>(){}).getData();
  }

}
