package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 결제수단 관리 search DTO")
public class PaymentMethodSelectDto {
    private String siteType;
    private String methodCd;
    private String parentMethodCd;
}