package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspMobileMain.MobileMainGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMobileMain.MobileMainListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspMobileMain.MobileMainListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspMobileMain.MobileMainSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspMain")
public class DspMobileMainController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 전시관리 > mobile 메인관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspMobileMain", method = RequestMethod.GET)
    public String dspMobileMain(Model model) {

        codeService.getCodeModel(model,
                        "display_yn"              // 전시여부
            ,           "dsp_main_sch_type"       // 검색어 구분
            ,           "dsp_main_site_type"      // 사이트 구분 (HOME,CLUB)
            ,           "dsp_main_period_type"    // 조회기간 기준
            ,           "dsp_main_link_type"      // 메인관리 링크타입
            ,           "dsp_main_loc"            // 전시위치
            ,           "dsp_main_store_type"     // 스토어 타입
            ,           "dsp_disp_store_type"     // 메인관리 링크타입
            ,           "dsp_main_gnb_type"       // 메인관리 GNB 디바이스 타입
            ,           "dsp_main_spl_type"       // 메인관리 스플래시 타입
            ,           "use_yn"                  // 전시구간 사용여부
        );

        //택배점 리스트 조회
        String apiUri = "/item/store/getStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject< List<StoreListSelectDto>>>() {};
        List<StoreListSelectDto> dlvStoreList =  (List<StoreListSelectDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, StoreListParamDto.builder().searchStoreKind("DLV").searchUseYn("Y").build(), apiUri, typeReference).getData();

        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("dlvStoreList", dlvStoreList);

        model.addAllAttributes(RealGridHelper.create("dspMobileMainListGridBaseInfo", MobileMainListSelectDto.class));

        return "/manage/dspMobileMain";
    }

    /**
     * 사이트관 > 전시관리 > mobile 메인관리
     *
     * @param listParamDto
     * @return MobileMainListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getMobileMainList.json"}, method = RequestMethod.GET)
    public List<MobileMainListSelectDto> getMobileMainList(MobileMainListParamDto listParamDto) {
        String apiUri = "/manage/dspMain/getMobileMainList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MobileMainListSelectDto>>>() {};
        return (List<MobileMainListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, MobileMainListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > mobile 메인관리 상세 영역
     *
     * @param mainNo
     * @return MobileMainGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getMobileMainDetail.json"}, method = RequestMethod.GET)
    public MobileMainGetDto getMobileMainDetail(@RequestParam(name = "mainNo") String mainNo) {
        String apiUri = "/manage/dspMain/getMobileMainDetail?mainNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<MobileMainGetDto>>() {};
        return (MobileMainGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri+mainNo, typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > mobile 메인관리 등록/수정
     *
     * @param mobileMainSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setMobileMain.json"}, method = RequestMethod.POST)
    public ResponseResult setMobileMain(@RequestBody MobileMainSetParamDto mobileMainSetParamDto) {
        String apiUri = "/manage/dspMain/setMobileMain";
        mobileMainSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, mobileMainSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}