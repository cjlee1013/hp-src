package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class GnbPopupListSelectDto {
    @RealGridColumnInfo(headText = "번호", sortable = true, width = 50, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private Integer gnbNo;

    @RealGridColumnInfo(headText = "사이트구분", sortable = true, width = 100)
    private String siteTypeTxt;

    @RealGridColumnInfo(headText = "GNB명", sortable = true, width = 100)
    private String dispNm;

    @RealGridColumnInfo(headText = "전시시작", width = 200)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료", width = 200)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "전시순서", width = 60, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private Integer priority;

    @RealGridColumnInfo(headText = "디바이스", sortable = true, width = 100)
    private String device;

}
