package kr.co.homeplus.admin.web.user.exception;

import kr.co.homeplus.admin.web.user.enums.ConsumerReturnCode;

public class ConsumerManageException extends RuntimeException {
    private final ConsumerReturnCode returnCode;

    public ConsumerManageException(ConsumerReturnCode returnCode) {
        this.returnCode = returnCode;
    }

    public ConsumerReturnCode getReturnCode() {
        return returnCode;
    }
}
