package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.promotion.model.promo.EventPromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoCouponGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoItemGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoItemParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoListParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoThemeDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequiredArgsConstructor
public class PromotionController {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    @ApiOperation(value = "기획전 관리 main 화면")
    @ApiResponse(code = 200, message = "기획전 관리 main 화면")
    @GetMapping("/exhibition/exhMain")
    public String exhMain(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "site_type"               // 사이트 구분
            , "promo_device"            // 적용시스템
            , "promo_store_type"        // 점포유형
            , "promo_top_type"          // 기획전 상단 유형
            , "display_yn"              // 전시여부
            , "exh_sort_kind"           // 정렬유형
            , "coupon_type"             // 쿠폰종류
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteType", code.get("site_type"));
        model.addAttribute("promoDevice", code.get("promo_device"));
        model.addAttribute("storeType", code.get("promo_store_type"));
        model.addAttribute("topType", code.get("promo_top_type"));
        model.addAttribute("dispYn", code.get("display_yn"));
        model.addAttribute("sortKind", code.get("exh_sort_kind"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("dispYnJson", om.writeValueAsString(code.get("display_yn")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("promo_store_type")));
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("siteTypeJson", om.writeValueAsString(code.get("site_type")));

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAllAttributes(RealGridHelper.create("exhGridBaseInfo", PromoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("exhCouponGridBaseInfo", PromoCouponGetDto.class));
        model.addAllAttributes(RealGridHelper.create("exhThemeGridBaseInfo", PromoThemeDto.class));
        model.addAllAttributes(RealGridHelper.create("exhItemGridBaseInfo", PromoItemGetDto.class));

        return "/promotion/exhPromoMain";
    }

    @ApiOperation(value = "이벤트 관리 main 화면")
    @ApiResponse(code = 200, message = "이벤트 관리 main 화면")
    @GetMapping("/event/eventMain")
    public String eventMain(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "site_type"               // 사이트 구분
            , "promo_device"            // 적용시스템
            , "display_yn"              // 전시여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteType", code.get("site_type"));
        model.addAttribute("promoDevice", code.get("promo_device"));
        model.addAttribute("dispYn", code.get("display_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("dispYnJson", om.writeValueAsString(code.get("display_yn")));
        model.addAttribute("siteTypeJson", om.writeValueAsString(code.get("site_type")));

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAllAttributes(RealGridHelper.create("eventGridBaseInfo", EventPromoListGetDto.class));

        return "/promotion/eventPromoMain";
    }

    /**
     * 기획전, 이벤트 조회
     * @param promoListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/promotion/getPromoList.json"})
    public List<PromoListGetDto> getExhList(PromoListParamDto promoListParamDto) {
        String apiUri = "/promotion/promo/getPromoList";

        ResourceClientRequest<List<PromoListGetDto>> requestClient = ResourceClientRequest.<List<PromoListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(promoListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<PromoListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 기획전, 이벤트 상세 조회
     * @param promoNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/promotion/getPromoDetail.json"})
    public PromoDetailGetDto getExhDetail(@RequestParam(name = "promoNo") Long promoNo) {
        String apiUri = "/promotion/promo/getPromoDetail?promoNo=" + promoNo;

        ResourceClientRequest<PromoDetailGetDto> requestClient = ResourceClientRequest.<PromoDetailGetDto>getBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<PromoDetailGetDto>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 상품 상세 정보 조회
     * @param itemNoList
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/promotion/getItemList.json"})
    public List<PromoItemGetDto> getItemList(@RequestBody PromoItemParamDto itemNoList) throws Exception {
        String apiUri = "/promotion/promo/getItemList";

        ResourceClientRequest<List<PromoItemGetDto>> requestClient = ResourceClientRequest.<List<PromoItemGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(itemNoList)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<PromoItemGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 기획전 등록/수정
     * @param promoDetailSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/promotion/setPromotion.json"})
    public Long setPromotion(@RequestBody PromoDetailSetDto promoDetailSetDto) throws Exception {
        promoDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/promo/setPromotion";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(promoDetailSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }
}
