package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridType;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true, realGridType = RealGridType.SINGLE_ROW_SELECT)
@Getter
@Setter
public class PromoCouponSetDto {
    @RealGridColumnInfo(headText = "쿠폰 번호")
    private Long couponNo;

    @RealGridColumnInfo(headText = "우선순위", hidden = true)
    private Integer priority;
}