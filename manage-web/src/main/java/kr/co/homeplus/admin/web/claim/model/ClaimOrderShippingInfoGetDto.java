package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimOrderShippingInfoGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "구매자", position = 2)
    private String buyerNm;

    @ApiModelProperty(value = "구매자 휴대폰번호", position = 3)
    private String buyerMobileNo;

    @ApiModelProperty(value = "배송방법", position = 4)
    private String shipMethod;

    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipcode;

    @ApiModelProperty(value = "도로명", position = 7)
    private String roadBaseAddr;

    @ApiModelProperty(value = "도로명상세", position = 8)
    private String roadDetailAddr;

    @ApiModelProperty(value = "지번", position = 9)
    private String baseAddr;

    @ApiModelProperty(value = "지번상세", position = 10)
    private String detailAddr;

    @ApiModelProperty(value = "점포ID", position = 11)
    private String storeId;

    @ApiModelProperty(value = "점포명", position = 12)
    private String storeNm;
}
