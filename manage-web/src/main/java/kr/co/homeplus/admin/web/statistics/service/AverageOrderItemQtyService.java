package kr.co.homeplus.admin.web.statistics.service;


import java.util.List;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderSetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListGetDto;
import kr.co.homeplus.admin.web.statistics.model.AverageOrderItemQtyListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AverageOrderItemQtyService {

    private final ResourceClient resourceClient;

    /**
     * 클레임 메인화면 리스트 조회
     * **/
    public List<AverageOrderItemQtyListGetDto> getAverageOrderItemQtyList(
        AverageOrderItemQtyListSetDto averageOrderItemQtyListSetDto) {
        averageOrderItemQtyListSetDto.setSchStartDt(averageOrderItemQtyListSetDto.getSchStartDt().replaceAll("-",""));
        averageOrderItemQtyListSetDto.setSchEndDt(averageOrderItemQtyListSetDto.getSchEndDt().replaceAll("-", ""));
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            averageOrderItemQtyListSetDto,
            "/statistics/order/getAverageOrderItemQtyList",
            new ParameterizedTypeReference<ResponseObject<List<AverageOrderItemQtyListGetDto>>>() {}
        ).getData();
    }

}
