package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class StatisticsUnknownMorphologyGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class UnknownMorphology {
        @RealGridColumnInfo(headText = "미등록어", columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "빈도", columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
        private Integer frequency;
        @RealGridColumnInfo(headText = "사전여부", columnType = RealGridColumnType.BASIC)
        private String usedDic;
        @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.BASIC)
        private String regDt;
    }
}
