package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 상담정보이미지 저장 요청 DTO")
public class CallCenterCompleteImgSetDto {
  @ApiModelProperty(notes = "부모상담일련번호")
  @JsonProperty("par_history_seq")
  private String par_history_seq;

  @ApiModelProperty(notes = "파일리스트")
  @JsonProperty("fileList")
  private List<CallCenterCompleteImgGetDto> fileList;
}