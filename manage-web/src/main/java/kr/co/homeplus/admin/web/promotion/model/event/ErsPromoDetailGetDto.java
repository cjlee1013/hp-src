package kr.co.homeplus.admin.web.promotion.model.event;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("사은행사 상세 정보")
public class ErsPromoDetailGetDto {
    @ApiModelProperty(value = "사은행사 번호")
    private String eventCd;

    @ApiModelProperty(value = "사은행사 명")
    private String eventNm;

    @ApiModelProperty(value = "구매기준유형")
    private String eventTargetType;

    @ApiModelProperty(value = "지급기준유형")
    private String eventGiveType;

    @ApiModelProperty(value = "영수증합산여부")
    private String receiptSumYn;

    @ApiModelProperty(value = "테넌트참여여부")
    private String tenantYn;

    @ApiModelProperty(value = "연속식적용여부")
    private String multipleYn;

    @ApiModelProperty(value = "상품권금액출력여부")
    private String ticketAmtPrtYn;

    @ApiModelProperty(value = "고객정보출력여부")
    private String customerInfoPrtYn;

    @ApiModelProperty(value = "지급구분")
    private String saleType;

    @ApiModelProperty(value = "행사시작일")
    private String eventStDate;

    @ApiModelProperty(value = "행사종료일")
    private String eventEdDate;

    @ApiModelProperty(value = "증정시작일")
    private String giveStDate;

    @ApiModelProperty(value = "증정종료일")
    private String giveEdDate;

    @ApiModelProperty(value = "반환시작일")
    private String returnStDate;

    @ApiModelProperty(value = "반환종료일")
    private String returnEdDate;

    @ApiModelProperty(value = "영수증문구")
    private String receiptPrtTitle;

    @ApiModelProperty(value = "확정여부")
    private String confirmYn;

    @ApiModelProperty(value = "확정일시")
    private String confirmDt;

    @ApiModelProperty(value = "확정사용자")
    private String confirmUser;

    @ApiModelProperty(value = "주관부서명")
    private String partName;

    @ApiModelProperty(value = "취소구")
    private String cancelFlag;

    @ApiModelProperty(value = "취소일시")
    private String cancelDt;

    @ApiModelProperty(value = "취소등록자아")
    private String cancelUser;

    @ApiModelProperty(value = "재확정여부")
    private String reconfirmYn;

    @ApiModelProperty(value = "펀딩비율")
    private String fundRate;

    @ApiModelProperty(value = "할인여부")
    private String dcYn;

    @ApiModelProperty(value = "비자 rf 행사 구분")
    private String visaRfEventFlag;

    @ApiModelProperty(value = "영수증출력메세")
    private String receiptMsg1;
    private String receiptMsg2;
    private String receiptMsg3;
    private String receiptMsg4;
    private String receiptMsg5;
    private String receiptMsg6;
    private String receiptMsg7;
    private String receiptMsg8;
    private String receiptMsg9;
    private String receiptMsg10;
    private String receiptMsg11;
    private String receiptMsg12;
    private String receiptMsg13;
    private String receiptMsg14;
    private String receiptMsg15;

    @ApiModelProperty(value = "온라인 사용여부")
    private String useYn;

    @ApiModelProperty(value = "구매 가이드")
    private String purchaseGuide;

    // 구간행사 리스트
    @ApiModelProperty(value = "구간행사 리스트")
    private List<ErsPromoIntervalGetDto> intervalList;

    // 점포 리스트
    @ApiModelProperty(value = "적용점포 리스트")
    private List<ErsPromoStoreGetDto> storeList;

    // 적용상품 리스트
    @ApiModelProperty(value = "상품 리스트")
    private List<ErsPromoItemGetDto> itemList;

    // 적용카드 리스트
    @ApiModelProperty(value = "적용카드 리스트")
    private List<ErsPromoCardGetDto> cardList;
}
