package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelationDto {
    private String wordName;
    private String relType;
    private Integer relId;
    private String senseTag;
    private String dicType;
    private String pos;
    private String neType;
    private String domain;
    private String language;
    private String description;
    private String example;
}
