package kr.co.homeplus.admin.web.search.controller;


import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListGetParam;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListGridDto;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListSetParam;
import kr.co.homeplus.admin.web.search.service.OperationSearchLogService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/search/operation/searchLog")
@RequiredArgsConstructor
public class OperationSearchLogController {

    private final OperationSearchLogService operationSearchLogService;

    @GetMapping(value = { "/main" })
    public String getMain(Model model) {
        model.addAllAttributes(RealGridHelper.create("blackListGridBaseInfo", OperationSearchLogBlackListGridDto.blackList.class));
        return "/search/operationSearchLogMain";
    }

    @ResponseBody
    @PostMapping(value = {"/blackList.json"})
    public List<OperationSearchLogBlackListGridDto> getList(@RequestBody @Valid OperationSearchLogBlackListGetParam param) {
        return (List<OperationSearchLogBlackListGridDto>)operationSearchLogService.getList(param).getData();
    }

    @ResponseBody
    @PostMapping(value = {"/registerBlackListIp.json"})
    public Integer registerBlackListIp(@RequestBody @Valid OperationSearchLogBlackListSetParam param) {
        return (Integer)operationSearchLogService.registerBlackListIp(param).getData();
    }

    @ResponseBody
    @PostMapping(value = {"/modifyBlacklistStatus.json"})
    public Integer modifyBlacklistStatus(@RequestBody @Valid OperationSearchLogBlackListSetParam param) {
        return (Integer)operationSearchLogService.modifyBlacklistStatus(param).getData();
    }
}
