package kr.co.homeplus.admin.web.app.service;

import kr.co.homeplus.admin.web.app.model.AppStoreReviewControlInfo;
import kr.co.homeplus.admin.web.app.model.AppStoreReviewControlParam;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppStoreReviewControlService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public AppStoreReviewControlInfo getAppStoreReviewControlInfo(int seq) {
        String apiUri = "/admin/app/mng/appstore/review/getAppleReviewControlInfo?seq=" + seq;

        ResponseObject<AppStoreReviewControlInfo> responseObject = resourceClient
            .getForResponseObject(ResourceRouteName.USERMNG, apiUri, new ParameterizedTypeReference<>() {
            });
        return responseObject.getData();
    }

    public boolean setAppStoreReviewControl(AppStoreReviewControlParam appStoreReviewControlParam) {
        UserInfo userInfo = loginCookieService.getUserInfo();
        appStoreReviewControlParam.setEmpId(userInfo.getEmpId());
        appStoreReviewControlParam.setEmpNm(userInfo.getUserNm());


        String apiUri = "/admin/app/mng/appstore/review/setAppStoreReviewControlData";

        ResponseObject<Boolean> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, appStoreReviewControlParam, apiUri,
                new ParameterizedTypeReference<>() {
                });
        return responseObject.getData();
    }
}
