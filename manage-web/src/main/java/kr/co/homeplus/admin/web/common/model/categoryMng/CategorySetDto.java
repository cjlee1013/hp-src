package kr.co.homeplus.admin.web.common.model.categoryMng;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategorySetDto {

	private int cateCd;
	private int lcateCd; //상위 뎁스코드는 DB에서 가져올거임
	private int mcateCd; //상위 뎁스코드는 DB에서 가져올거임
	private int scateCd; //상위 뎁스코드는 DB에서 가져올거임
	private int dcateCd; //상위 뎁스코드는 DB에서 가져올거임
	private String cateNm;
	private int depth;
	private int parentCateCd;
	private Integer priority;
	private String cartYn;
	private String adultLimitYn;
	private String dispYn;
	private String useYn;
	private String clubDispYn;
	private String dept;
	private String classCd;
	private String subclass;
	private String regId;
	private String isbnYn;
	private String reviewDisplay;
	private String apiUseYn;

	// 권한
	private boolean categoryWriteRole;	// 카테고리 등록/수정 권한
}
