package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Data;

@Data
public class GnbPopupListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchSiteType;

    private String searchDeviceType;

}
