package kr.co.homeplus.admin.web.item.model.categoryGroup;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupParamDto {

	private String schType;

	private String schKeyword;

	private String schUseYn;

}
