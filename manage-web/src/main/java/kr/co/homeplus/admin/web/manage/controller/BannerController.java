package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.manage.model.banner.BannerGetDto;
import kr.co.homeplus.admin.web.manage.model.banner.BannerLinkGetDto;
import kr.co.homeplus.admin.web.manage.model.banner.BannerListParamDto;
import kr.co.homeplus.admin.web.manage.model.banner.BannerSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/banner")
public class BannerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    /**
     * 사이트관리 > 전시관리 > 배너 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/bannerMain", method = RequestMethod.GET)
    public String bannerMain(Model model) {

        codeService.getCodeModel(model
            , "disp_yn"               // 전시여부
            , "site_type"             // 사이트 구분
            , "banner_loc_1dpeth"     // 전시위치 1뎁스
            , "dsk_device_gubun"      // 디바이스 구분
            , "dsp_main_link_type"    // 링크 타입
            , "dsp_main_store_type"   // 스토어 타입
            , "dsp_main_gnb_type"     // 메인관리 GNB 디바이스 타입
            , "use_yn"                // 전시구간 사용여부
        );

        //택배점 리스트 조회
        String apiUri = "/item/store/getStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject< List<StoreListSelectDto>>>() {};
        List<StoreListSelectDto> dlvStoreList =  (List<StoreListSelectDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, StoreListParamDto.builder().searchStoreKind("DLV").searchUseYn("Y").build(), apiUri, typeReference).getData();

        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("dlvStoreList", dlvStoreList);

        model.addAllAttributes(RealGridHelper.create("bannerListGridBaseInfo", BannerGetDto.class));

        return "/manage/bannerMain";
    }

    /**
     * 사이트관리 > 전시관리 > 배너 관리 리스트 조회
     *
     * @param bannerListParamDto
     * @return List<BannerGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getBannerList.json"}, method = RequestMethod.GET)
    public List<BannerGetDto> getBannerList(@Valid BannerListParamDto bannerListParamDto) {
        String apiUri = "/manage/banner/getBannerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BannerGetDto>>>() {};
        return (List<BannerGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, BannerListParamDto.class, bannerListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 배너 관리 리스트 조회
     *
     * @param bannerNo
     * @return List<BannerLinkGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getBannerLinkList.json"}, method = RequestMethod.GET)
    public List<BannerLinkGetDto> getBannerLinkList(@RequestParam("bannerNo") Long bannerNo) {
        String apiUri = "/manage/banner/getBannerLinkList?bannerNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BannerLinkGetDto>>>() {};
        return (List<BannerLinkGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + bannerNo, typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 배너 관리 등록/수정
     *
     * @param bannerSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setBanner.json"}, method = RequestMethod.POST)
    public ResponseResult setBanner(@RequestBody BannerSetParamDto bannerSetParamDto) {
        String apiUri = "/manage/banner/setBanner";
        bannerSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, bannerSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

}
