package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDetailInfoListDto {

    @ApiModelProperty(value = "처리상태",position = 1)
    @RealGridColumnInfo(headText = "처리상태", width = 60)
    private String claimStatus;

    @ApiModelProperty(value = "수거상태",position = 2)
    @RealGridColumnInfo(headText = "수거상태", width = 60)
    private String pickStatus;

    @ApiModelProperty(value = "배송상태",position = 3)
    @RealGridColumnInfo(headText = "배송상태", width = 60)
    private String exchStatus;

    @ApiModelProperty(value = "환불상태",position = 4)
    @RealGridColumnInfo(headText = "환불상태", width = 60)
    private String refundStatus;

    @ApiModelProperty(value="클레임번들번호", position = 5)
    @RealGridColumnInfo(headText = "클레임번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String claimBundleNo;

    @ApiModelProperty(value = "배송번호", position = 6)
    @RealGridColumnInfo(headText = "배송번호")
    private String bundleNo;

    @ApiModelProperty(value = "상품주문번호", position = 7)
    @RealGridColumnInfo(headText = "상품주문번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String orderItemNo;

    @ApiModelProperty(value = "주문번호", position = 8)
    @RealGridColumnInfo(headText = "주문번호", hidden = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품번호", position = 9)
    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.SERIAL, width = 120)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 10)
    @RealGridColumnInfo(headText = "상품명", width = 240, columnType = RealGridColumnType.NONE)
    private String itemName;

    @ApiModelProperty(value = "옵션명", position = 11)
    @RealGridColumnInfo(headText = "옵션명", width = 200, columnType = RealGridColumnType.NONE)
    private String optItemNm;

    @ApiModelProperty(value = "상품금액", position = 12)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", columnType = RealGridColumnType.PRICE)
    private String claimPrice;

    @ApiModelProperty(value = "신청수량", position = 13)
    @RealGridColumnInfo(headText = "신청수량", width = 60)
    private String claimQty;

    @ApiModelProperty(value = "파트너ID", position = 14)
    @RealGridColumnInfo(headText = "파트너ID", hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "매장타입", position = 15)
    @RealGridColumnInfo(headText = "매장타입", hidden = true)
    private String storeType;

    @ApiModelProperty(value = "사이트타입", position = 16)
    @RealGridColumnInfo(headText = "사이트타입", hidden = true)
    private String siteType;
}
