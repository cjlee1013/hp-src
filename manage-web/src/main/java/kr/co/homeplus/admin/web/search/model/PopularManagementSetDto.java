package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PopularManagementSetDto {
    private Integer managementNo;
    private String keyword;
    private Integer rank;
    private String type;
    private String startDt;
    private String endDt;
    private String regId;
    private String regDt;
    private String chgId;
    private String chgDt;
    private String siteType;
    private String useYn;
}
