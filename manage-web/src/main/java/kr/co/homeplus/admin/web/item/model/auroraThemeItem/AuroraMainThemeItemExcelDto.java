package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class AuroraMainThemeItemExcelDto {

    @EqualsAndHashCode.Include
    private String themeId;
    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String useYn;

}
