package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class TypoGetParamDto {
    private String schKeyword;
    @NotEmpty
    @NotNull(message = "schStartDt is null")
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "시작일")
    private String schStartDt;
    @NotEmpty
    @NotNull(message = "schEndDt is null")
    @Pattern(regexp = "(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)", message = "종료일")
    private String schEndDt;
    private String schOrder = "date";

    private int offset;
    private int limit;
    public int getLimit() {
        return this.limit == 0 ? 100 : this.limit;
    }
}
