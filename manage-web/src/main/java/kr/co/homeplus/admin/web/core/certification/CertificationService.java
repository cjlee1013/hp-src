package kr.co.homeplus.admin.web.core.certification;

import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.dto.LoginParam;
import org.springframework.validation.BindingResult;

/**
 * 인증 관련기능 서비스
 */
public interface CertificationService {
    /**
     * 쿠키를 가지고 유저의 로그인 처리 진행.
     * @return
     * @throws Exception
     */
    boolean getUserCertificationService(HttpServletRequest request,
        LoginParam loginParam, BindingResult bindingResult);
}
