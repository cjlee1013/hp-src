package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoIntervalGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoItemGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoListParamDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoMassOnlineUseYnSetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoSetDto;
import kr.co.homeplus.admin.web.promotion.model.event.ErsPromoStoreGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.EtcPromoDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.EtcPromoDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.event.EtcPromoItemGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.EtcPromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.EtcPromoListParamDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileageErsPromoGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileagePromoDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileagePromoDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileagePromoItemGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileagePromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.MileagePromoListParamDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoDetailParamDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoIntervalGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoItemGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoListParamDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoSetDto;
import kr.co.homeplus.admin.web.promotion.model.event.RmsPromoStoreGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/event")
public class EventController {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    public EventController(ResourceClient resourceClient, LoginCookieService loginCookieService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 프로모션관리 > 행사관리 > 점포행사관리
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/storeEvent")
    public String rmsPromotion(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "event_kind"              // 행사종류
            , "event_state"             // 행사상태
            , "change_type"             // 할인유형
            , "promo_store_type"        // 점포유형
            , "xout_apply_flag"              // 행사적용구분
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("eventKind", code.get("event_kind"));
        model.addAttribute("eventState", code.get("event_state"));
        model.addAttribute("changeType", code.get("change_type"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("eventKindJson", om.writeValueAsString(code.get("event_kind")));
        model.addAttribute("eventStateJson", om.writeValueAsString(code.get("event_state")));
        model.addAttribute("changeTypeJson", om.writeValueAsString(code.get("change_type")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("promo_store_type")));
        model.addAttribute("xoutApplyFlagJson", om.writeValueAsString(code.get("xout_apply_flag")));

        model.addAllAttributes(RealGridHelper.create("rmsPromoGridBaseInfo", RmsPromoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("rmsPromoIntervalGridBaseInfo", RmsPromoIntervalGetDto.class));
        model.addAllAttributes(RealGridHelper.create("rmsPromoStoreGridBaseInfo", RmsPromoStoreGetDto.class));
        model.addAllAttributes(RealGridHelper.create("rmsPromoItemGridBaseInfo", RmsPromoItemGetDto.class));
        model.addAllAttributes(RealGridHelper.create("rmsPromoDiscItemGridBaseInfo", RmsPromoItemGetDto.class));

        return "/promotion/rmsPromoMain";
    }

    /**
     * 점포행사 조회
     *
     * @param rmsPromoListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getRmsPromoList.json"})
    public List<RmsPromoListGetDto> getRmsPromoList(RmsPromoListParamDto rmsPromoListParamDto) {
        String apiUri = "/promotion/event/getRmsPromoList";

        ResourceClientRequest<List<RmsPromoListGetDto>> requestClient = ResourceClientRequest.<List<RmsPromoListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(rmsPromoListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<RmsPromoListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 점포행사 상세 정보 조회
     * @param rmsPromoDetailParamDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/getRmsPromoDetail.json"})
    public RmsPromoDetailGetDto getRmsPromoDetail(RmsPromoDetailParamDto rmsPromoDetailParamDto) throws Exception {
        String apiUri = "/promotion/event/getRmsPromoDetail";

        ResourceClientRequest<RmsPromoDetailGetDto> requestClient = ResourceClientRequest.<RmsPromoDetailGetDto>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(rmsPromoDetailParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<RmsPromoDetailGetDto>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 점포행사 사용여부 수정
     * @param rmsPromoSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setRmsPromo.json"})
    public Long setRmsPromo(@RequestBody RmsPromoSetDto rmsPromoSetDto) throws Exception {
        rmsPromoSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/event/setRmsPromo";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(rmsPromoSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 프로모션관리 > 행사관리 > 사은행사관리
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/freeGift")
    public String ersPromotion(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model,
            "use_yn"           // 사용여부
            , "event_target_type"
            , "event_give_type"         // 행사 지급기준 유형
            , "apply_flag"
            , "apply_yn"
            , "sale_type"
            , "confirm_yn"
            , "gift_give_type"          // 사은품 지급 유형
        );

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("eventTargetTypeJson", om.writeValueAsString(code.get("event_target_type")));
        model.addAttribute("eventGiveTypeJson", om.writeValueAsString(code.get("event_give_type")));
        model.addAttribute("applyFlagJson", om.writeValueAsString(code.get("apply_flag")));
        model.addAttribute("applyYnJson", om.writeValueAsString(code.get("apply_yn")));
        model.addAttribute("saleTypeJson", om.writeValueAsString(code.get("sale_type")));
        model.addAttribute("confirmYnJson", om.writeValueAsString(code.get("confirm_yn")));
        model.addAttribute("giftGiveTypeJson", om.writeValueAsString(code.get("gift_give_type")));
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));

        model.addAllAttributes(RealGridHelper.create("ersPromoGridBaseInfo", ErsPromoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("ersPromoIntervalGridBaseInfo", ErsPromoIntervalGetDto.class));
        model.addAllAttributes(RealGridHelper.create("ersPromoStoreGridBaseInfo", ErsPromoStoreGetDto.class));
        model.addAllAttributes(RealGridHelper.create("ersPromoItemGridBaseInfo", ErsPromoItemGetDto.class));

        return "/promotion/ersPromoMain";
    }

    /**
     * 사은행사 조회
     *
     * @param ersPromoListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getErsPromoList.json"})
    public List<ErsPromoListGetDto> getErsPromoList(ErsPromoListParamDto ersPromoListParamDto) {
        String apiUri = "/promotion/event/getErsPromoList";

        ResourceClientRequest<List<ErsPromoListGetDto>> requestClient = ResourceClientRequest.<List<ErsPromoListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(ersPromoListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<ErsPromoListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 사은행사 상세 정보 조회
     * @param eventCd
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getErsPromoDetail.json"})
    public ErsPromoDetailGetDto getErsPromoDetail(@RequestParam(name = "eventCd") String eventCd) throws Exception {
        String apiUri = "/promotion/event/getErsPromoDetail?eventCd=" + eventCd;

        ResourceClientRequest<ErsPromoDetailGetDto> requestClient = ResourceClientRequest.<ErsPromoDetailGetDto>getBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<ErsPromoDetailGetDto>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 사은행사 사용여부 수정
     * @param ersPromoSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setErsPromo.json"})
    public Long setErsPromo(@RequestBody ErsPromoSetDto ersPromoSetDto) throws Exception {
        ersPromoSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/event/setErsPromo";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(ersPromoSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 사은행사 온라인 사용여부 일괄 수정
     * @param massOnlineUseYnSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setErsPromoMassOnlineUseYn.json"})
    public Integer setErsPromoMassOnlineUseYn(@RequestBody ErsPromoMassOnlineUseYnSetDto massOnlineUseYnSetDto) throws Exception {
        massOnlineUseYnSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/event/setErsPromoMassOnlineUseYn";

        ResourceClientRequest<Integer> requestClient = ResourceClientRequest.<Integer>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(massOnlineUseYnSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Integer>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    @ApiOperation(value = "덤 행사 관리 main 화면")
    @ApiResponse(code = 200, message = "덤 행사 관리 main 화면")
    @GetMapping("/etcMain")
    public String addtMain(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "etc_promo_store_type"    // 점포유형
            , "evn_disp_store_type"     // 행사노출점포유형
            , "display_yn"              // 전시여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("storeType", code.get("etc_promo_store_type"));
        model.addAttribute("dispStoreType", code.get("evn_disp_store_type"));
        model.addAttribute("dispYn", code.get("display_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("dispStoreTypeJson", om.writeValueAsString(code.get("evn_disp_store_type")));

        model.addAllAttributes(RealGridHelper.create("etcPromoGridBaseInfo", EtcPromoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("etcPromoItemGridBaseInfo", EtcPromoItemGetDto.class));

        return "/promotion/etcPromoMain";
    }

    /**
     * 덤 행사 조회
     *
     * @param etcPromoListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getEtcPromoList.json"})
    public List<EtcPromoListGetDto> getEtcPromoList(EtcPromoListParamDto etcPromoListParamDto) {
        String apiUri = "/promotion/event/getEtcPromoList";

        ResourceClientRequest<List<EtcPromoListGetDto>> requestClient = ResourceClientRequest.<List<EtcPromoListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(etcPromoListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<EtcPromoListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 덤 행사 상세 정보 조회
     * @param etcNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getEtcPromoDetail.json"})
    public EtcPromoDetailGetDto getEtcPromoDetail(@RequestParam(name = "etcNo") Long etcNo) throws Exception {
        String apiUri = "/promotion/event/getEtcPromoDetail?etcNo=" + etcNo;

        ResourceClientRequest<EtcPromoDetailGetDto> requestClient = ResourceClientRequest.<EtcPromoDetailGetDto>getBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<EtcPromoDetailGetDto>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 덤 행사 등록/수정
     * @param etcPromoDetailSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/setEtcPromo.json"})
    public Long setEtcPromo(@RequestBody EtcPromoDetailSetDto etcPromoDetailSetDto) throws Exception {
        etcPromoDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/event/setEtcPromo";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(etcPromoDetailSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    @ApiOperation(value = "마일리지 행사 관리 main 화면")
    @ApiResponse(code = 200, message = "마일리지 행사 관리 main 화면")
    @GetMapping("/mileageEvent")
    public String mileageEvent(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "site_type"               // 사이트 구분
            , "earn_type"               // 마일리지 적립 유형
            , "mil_promo_store_type"    // 점포유형
            , "evn_disp_store_type"     // 행사노출점포유형
            , "mileage_department"      // 마일리지 주관부서
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteType", code.get("site_type"));
        model.addAttribute("earnType", code.get("earn_type"));
        model.addAttribute("dispStoreType", code.get("evn_disp_store_type"));
        model.addAttribute("storeType", code.get("mil_promo_store_type"));
        model.addAttribute("department", code.get("mileage_department"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("mil_promo_store_type")));

        model.addAllAttributes(RealGridHelper.create("mileagePromoGridBaseInfo", MileagePromoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("mileageItemGridBaseInfo", MileagePromoItemGetDto.class));

        return "/promotion/mileagePromoMain";
    }

    /**
     * 마일리지 행사 조회
     *
     * @param mileagePromoListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMileagePromoList.json"})
    public List<MileagePromoListGetDto> getMileagePromoList(MileagePromoListParamDto mileagePromoListParamDto) {
        String apiUri = "/promotion/event/getMileagePromoList";

        ResourceClientRequest<List<MileagePromoListGetDto>> requestClient = ResourceClientRequest.<List<MileagePromoListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(mileagePromoListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<MileagePromoListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 마일리지 행사 상세 정보 조회
     * @param mileageNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMileagePromoDetail.json"})
    public MileagePromoDetailGetDto getMileagePromoDetail(@RequestParam(name = "mileageNo") Long mileageNo) throws Exception {
        String apiUri = "/promotion/event/getMileagePromoDetail?mileageNo=" + mileageNo;

        ResourceClientRequest<MileagePromoDetailGetDto> requestClient = ResourceClientRequest.<MileagePromoDetailGetDto>getBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<MileagePromoDetailGetDto>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 마일리지 행사 등록/수정
     * @param mileagePromoDetailSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/setMileagePromo.json"})
    public Long setMileagePromo(@RequestBody MileagePromoDetailSetDto mileagePromoDetailSetDto) throws Exception {
        mileagePromoDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/event/setMileagePromo";

        ResourceClientRequest<Long> requestClient = ResourceClientRequest.<Long>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(mileagePromoDetailSetDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Long>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }

    /**
     * 마일리지 - 자동설정 사은행사 상세 정보 조회
     * @param eventCd
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMileageErsPromoDetail.json"})
    public MileageErsPromoGetDto getMileageErsPromoDetail(@RequestParam(name = "eventCd") String eventCd) throws Exception {
        String apiUri = "/promotion/event/getMileageErsPromoDetail?eventCd=" + eventCd;

        ResourceClientRequest<MileageErsPromoGetDto> requestClient = ResourceClientRequest.<MileageErsPromoGetDto>getBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<MileageErsPromoGetDto>> responseEntity = resourceClient.get(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }
}
