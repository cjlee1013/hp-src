package kr.co.homeplus.admin.web.item.model.expStore;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExpStoreListGetDto {

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", columnType= RealGridColumnType.NUMBER_CENTER, width = 80, sortable = true)
    private String storeId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 80, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "점포명(영문)")
    @RealGridColumnInfo(headText = "점포명(영문)", width =80 , sortable = true)
    private String storeNm2;
    
    @ApiModelProperty(value = "소재지역")
    @RealGridColumnInfo(headText = "소재지역", width = 80, sortable = true)
    private String regionNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 70, sortable = true)
    private String useYnNm;

    @ApiModelProperty(value = "주문가능여부")
    @RealGridColumnInfo(headText = "주문가능여부", width = 0, hidden = true)
    private String orderAvailYn;

    @ApiModelProperty(value = "주문가능여부")
    @RealGridColumnInfo(headText = "주문가능여부", width = 80 , sortable = true)
    private String orderAvailYnNm;

    @ApiModelProperty(value = "주문가능시작시간")
    @RealGridColumnInfo(headText = "주문가능시작시간", width = 80 , sortable = true)
    private String orderStartTime;

    @ApiModelProperty(value = "주문가능종료시간")
    @RealGridColumnInfo(headText = "주문가능종료시간", width = 80 , sortable = true)
    private String orderEndTime;

    @ApiModelProperty(value ="주문불가사유")
    @RealGridColumnInfo(headText = "주문불가사유", width = 100 , sortable = true)
    private String stopReasonCdNm;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100 , sortable = true)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100, sortable = true)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100 , sortable = true)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100 , sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "시작시간")
    @RealGridColumnInfo(headText = "시작시간", width = 0, hidden = true)
    private String orderStartHour;

    @ApiModelProperty(value = "시작시간")
    @RealGridColumnInfo(headText = "시작시간", width = 0, hidden = true)
    private String orderStartMinute;

    @ApiModelProperty(value = "종료시간")
    @RealGridColumnInfo(headText = "종료시간", width = 0, hidden = true)
    private String orderEndHour;

    @ApiModelProperty(value = "종료시간")
    @RealGridColumnInfo(headText = "종료시간", width = 0, hidden = true)
    private String orderEndMinute;

    @ApiModelProperty(value = "배달소요시간")
    @RealGridColumnInfo(headText = "배달소요시간", width = 0, hidden = true)
    private String deliveryTime;

    @ApiModelProperty(value = "주문불가사유코드")
    @RealGridColumnInfo(headText = "주문불가사유코드", width = 0, hidden = true)
    private String stopReasonCd;

    @ApiModelProperty(value = "주문불가사유 기타")
    @RealGridColumnInfo(headText = "주문불가사유 기타", width = 0, hidden = true)
    private String stopReasonTxt;

}
