package kr.co.homeplus.admin.web.search.service;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.search.model.DataManagementBestGetParam;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DataManagementBestService {
    private final ResourceClient resourceClient;

    @Value("${plus.resource-routes.imageFront.url}")
    private String imageFrontDomainPrefix;

    public ResponseObject<Map<String,Object>> getList(DataManagementBestGetParam param) {
        String apiUri = StringUtil.getRequestString("/dataManagement/getBest", DataManagementBestGetParam.class, param);
        ResponseObject<Map<String,Object>> response = getResponseObject(apiUri);
        DecimalFormat df = new DecimalFormat("#.##");
        for(Map<String, Object> en : (List<HashMap<String,Object>>)response.getData().get("dataList")) {
            en.put("img",imageFrontDomainPrefix + "/it/" + en.get("itemNo") + "s0080");

            en.put("lcateNm",en.get("lcateNm") + "(" + en.get("lcateCd") + ")");
            en.put("mcateNm",en.get("mcateNm") + "(" + en.get("mcateCd") + ")");
            en.put("scateNm",en.get("scateNm") + "(" + en.get("scateCd") + ")");
            en.put("dcateNm",en.get("dcateNm") + "(" + en.get("dcateCd") + ")");
            en.put("weight",df.format(en.get("weight")));

            ///성인여부
            en.put("adultType","ADULT".equals(en.get("adultType")) && "N".equals(en.get("imgDispYn")) ? "Y" : "N");

            Map<String,Object> weightDetail = (HashMap<String,Object>)en.get("weightCalcDetail");
            // 구매 가중치
            en.put("weightBuyer",weightDetail.get("buyerWeight").toString());
            // 24시간 구매자 수
            en.put("weightDay",en.get("buyerCnt").toString());
            // 2시간 구매자 수
            en.put("weightHour",weightDetail.get("buyerCntHour").toString());
            ///상세가중치 (가격)
            en.put("weightPrice",weightDetail.get("priceWeight").toString());
            ///상세가중치 (점포유형)
            en.put("weightStoreType",weightDetail.get("storeTypeWeight").toString());
            ///상세가중치 (오픈일)
            en.put("weightOpenDay",weightDetail.get("openDayWeight").toString());
            ///상세가중치 (배송유형)
            en.put("weightShipMethod",weightDetail.get("shipTypeWeight").toString());
            ///상세가중치 (행사)
            en.put("weightPromotion",weightDetail.get("eventWeight").toString());
            ///상세가중치 (해택)
            en.put("weightBenefit",weightDetail.get("benefitWeight").toString());

            en.put("weightBunddleSum",weightDetail.get("groupWeight") != null ? weightDetail.get("groupWeight").toString() : "");

            en.put("weightCalc",en.get("weightCalc").toString());

            ///셀러정보
            en.put("shopNm",(en.get("sellerInfo") == null) ? null : ((HashMap<String,Object>)en.get("sellerInfo")).get("shopNm"));

            ///행사상품여부
            en.put("eventItemYn",en.get("eventInfo") != null && ((Map<String,Object>)en.get("eventInfo")).get("eventKind") != null && ("BASIC".equals(((Map<String,Object>)en.get("eventInfo")).get("eventKind").toString().toUpperCase(
                Locale.KOREA))) ? "Y" : "N");

            ///골라담기여부
            en.put("pickYn",en.get("eventInfo") != null && ((Map<String,Object>)en.get("eventInfo")).get("eventKind") != null && "PICK".equals(((Map<String,Object>)en.get("eventInfo")).get("eventKind").toString().toUpperCase(Locale.KOREA)) ? "Y" : "N");

            ///함께할인여부
            en.put("togetherYn",en.get("eventInfo") != null && ((Map<String,Object>)en.get("eventInfo")).get("eventKind") != null && ("INTERVAL".equals(((Map<String,Object>)en.get("eventInfo")).get("eventKind").toString().toUpperCase(Locale.KOREA)) || "TOGETHER".equals(((Map<String,Object>)en.get("eventInfo")).get("eventKind").toString().toUpperCase(Locale.KOREA))) ? "Y" : "N");

            ///사은품여부
            en.put("giftYn",en.get("eventInfo") != null && ((Map<String,Object>)en.get("eventInfo")).get("eventKind") != null && "GIFT".equals(((Map<String,Object>)en.get("eventInfo")).get("eventKind").toString().toUpperCase(Locale.KOREA)) ? "Y" : "N");

            ///무료배송여부
            en.put("freeshipYn",(en.get("labelList") != null && ((List<String>)en.get("labelList")).size() > 0 && ((List<String>)en.get("labelList")).stream().filter(x-> x.equals("무료배송")).count() > 0) ? "Y" : "N");
        }
        return response;
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

}
