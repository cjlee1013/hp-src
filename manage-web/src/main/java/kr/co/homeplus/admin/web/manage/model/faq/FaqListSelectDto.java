package kr.co.homeplus.admin.web.manage.model.faq;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class FaqListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long faqNo;

    @RealGridColumnInfo(headText = "분류", order = 1)
    private String classCdTxt;

    @RealGridColumnInfo(headText = "제목", width = 200, order = 2)
    private String faqSubject;

    @RealGridColumnInfo(headText = "사이트구분", order = 3, sortable = true)
    private String dispTxt;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 4, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 6, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 10, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "classCd", order = 11, hidden = true)
    private String classCd;
    @RealGridColumnInfo(headText = "useYn", order = 12, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "faqBody", order = 13, hidden = true)
    private String faqBody;
    @RealGridColumnInfo(headText = "dispHomeYn", order = 14, hidden = true)
    private String dispHomeYn;
    @RealGridColumnInfo(headText = "dispClubYn", order = 15, hidden = true)
    private String dispClubYn;

}
