package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketParamDto {

    private String schUseYn;

}
