package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 우편번호 조회 SELECT DTO")
public class StoreZipcodeSelectModel {
    @ApiModelProperty(notes = "점포타입(HYPER,EXP,CLUB)", position = 1)
    private String schStoreType;
    @ApiModelProperty(notes = "점포ID", position = 2)
    private Integer schStoreId;
    @ApiModelProperty(notes = "원거리 배송 여부", position = 3)
    private String schRemoteShipYn;
    @ApiModelProperty(notes = "우편번호", position = 4)
    private String schZipcode;
    @ApiModelProperty(notes = "시/도", position = 5)
    private String schSidoType;
    @ApiModelProperty(notes = "시군구", position = 6)
    private String schSigunguType;
    @ApiModelProperty(notes = "검색어(도로명/건물명)", position = 7)
    private String schAddrKeyword;
    @ApiModelProperty(notes = "검색어(건물번호)", position = 8)
    private String schBuildNo;
}
