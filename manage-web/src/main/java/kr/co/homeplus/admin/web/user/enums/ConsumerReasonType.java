package kr.co.homeplus.admin.web.user.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ConsumerReasonType implements RealGridLookUpSupport {
    ABNORMAL_PURCHASE(1, "이상구매 및 상습구매 취소")
    , HABITUAL_CLAIMS(2, "상습적인 클레임")
    , BULK_PURCHASE(3, "대량구매")
    , PROFANITY_PROMOTING(4, "욕설 및 타 업체 홍보")
    , SHIPPING_RELATED(5, "배송관련")
    ;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(ConsumerReasonType::name, ConsumerReasonType::getDesc));

    @Getter
    private final int code;

    @Getter
    private final String desc;

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
