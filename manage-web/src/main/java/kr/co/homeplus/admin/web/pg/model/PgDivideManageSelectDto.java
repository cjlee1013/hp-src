package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class PgDivideManageSelectDto {
    private Long divideSeq;
    private String pgMngType;
    private String siteType;
    private String platform;
    private String parentMethodCd;
    private String useYn;
    private String isPopup;
    private Long policyNo;
}
