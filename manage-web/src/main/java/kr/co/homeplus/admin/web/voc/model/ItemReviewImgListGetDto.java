package kr.co.homeplus.admin.web.voc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemReviewImgListGetDto {

    private Long reviewNo;
    private String imgNm;
    private String imgUrl;
    private int imgWidth;
    private int imgHeight;
    private int priority;
    private String useYn;

}
