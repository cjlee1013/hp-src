package kr.co.homeplus.admin.web.user.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserMhcCardType {
    MY_HP_CREDIT_CARD("0021", "마이홈플러스(신용)"),
    MY_HP_CHECK_CARD("0022", "마이홈플러스(체크)"),
    MY_HP_EMP_CREDIT_CARD("0023", "마이홈플러스임직원(신용)"),
    MY_HP_EMP_CHECK_CARD("0024", "마이홈플러스임직원(체크)"),
    MY_HP_NOMAL_CARD("0030", "마이홈플러스(일반)"),
    SHINHAN_AFFILIATE_CARD("1000", "신한제휴카드"),
    SAMSUNG_AFFILIATE_CARD("2000", "삼성제휴카드"),
    KB_AFFILIATE_CARD("3000", "KB제휴카드"),
    ETC_AFFILIATE_CARD("9000", "기타제휴카드"),

    SAMSUNG_PAY_SHINHAN("1100", "삼성 Pay(신한)"),
    LG_PAY_SHINHAN("1200", "LG Pay(신한)"),
    JUST_TOUCH_SHINHAN("1300", "저스트터치(신한)"),

    SAMSUNG_PAY_SAMSUNG("2100", "삼성 Pay(삼성)"),
    LG_PAY_SAMSUNG("2200", "LG Pay(삼성)"),
    JUST_TOUCH_SAMSUNG("2300", "저스트터치(삼성)"),

    SAMSUNG_PAY_KB("3100", "삼성 Pay(KB)"),
    LG_PAY_KB("3200", "LG Pay(KB)"),
    JUST_TOUCH_KB("3300", "저스트터치(KB)"),

    SYRUP_AFFILIATE_CARD("4000", "Syrup제휴카드"),
    STORE_VIRTUAL_CARD("0180", "점포가상카드"),
    ;

    @Getter
    private final String code;
    @Getter
    private final String desc;
}
