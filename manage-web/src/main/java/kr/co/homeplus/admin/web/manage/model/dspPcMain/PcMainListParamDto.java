package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import lombok.Data;

@Data
public class PcMainListParamDto {


    private String searchPeriodType;

    private String searchStartDt;


    private String searchEndDt;


    private String searchSiteType;


    private String searchDispYn;


    private String searchLoc1Depth;


    private String searchLoc2Depth;


    private String searchType;


    private String searchKeyword;

}
