package kr.co.homeplus.admin.web.manage.model.dspMobileMain;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
public class MobileMainListParamDto {

    private String searchPeriodType;

    private String searchStartDt;

    private String searchEndDt;

    private String searchSiteType;

    private String searchDispYn;

    private String searchLoc1Depth;

    private String searchLoc2Depth;

    private String searchType;

    private String searchKeyword;

}
