package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodBenefitManageDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodBenefitManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.pg.service.PaymentMethodBenefitManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 결제관리 > 결제수단관리 > 결제수단별 혜택관리
 */
@Controller
@RequestMapping("/pg/paymentMethod")
public class PaymentMethodBenefitManageController {

    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    @Autowired
    private PaymentMethodBenefitManageService paymentMethodBenefitManageService;
    @Autowired
    private CodeService codeService;

    public PaymentMethodBenefitManageController(LoginCookieService loginCookieService, AuthorityService authorityService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
    }

    /**
     * 결제수단별 혜택관리 Main Page
     */
    @GetMapping("/paymentMethodBenefitManageMain")
    public String paymentMethodBenefitManage(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("use_yn");
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteTypeList", Arrays.asList(SiteType.values()));
        model.addAttribute("parentMethodCdList", ParentMethodCd.getBenefitParentMethodList());
        model.addAllAttributes(RealGridHelper.create("paymentMethodBenefitManageGridBaseInfo", PaymentMethodBenefitManageDto.class));
        return "/pg/paymentMethodBenefitManageMain";
    }

    /**
     * 결제수단별 혜택 조회
     */
    @ResponseBody
    @GetMapping("/getPaymentMethodBenefitManageList.json")
    public List<PaymentMethodBenefitManageDto> getPaymentMethodBenefitManageList(@ModelAttribute PaymentMethodBenefitManageSelectDto paymentMethodBenefitManageSelectDto) {
        return paymentMethodBenefitManageService.getPaymentMethodBenefitManageList(paymentMethodBenefitManageSelectDto);
    }

    /**
     * 결제수단별 혜택 등록
     */
    @ResponseBody
    @PostMapping("/savePaymentMethodBenefitManage.json")
    public ResponseObject<Object> savePaymentMethodBenefitManage(@RequestBody PaymentMethodBenefitManageDto paymentMethodBenefitManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        paymentMethodBenefitManageDto.setRegId(userCd);
        paymentMethodBenefitManageDto.setChgId(userCd);

        return paymentMethodBenefitManageService.savePaymentMethodBenefitManage(paymentMethodBenefitManageDto);
    }
}
