package kr.co.homeplus.admin.web.statistics.controller;

import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsDailyDto;
import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.CartCouponStatisticsSelectDto;
import kr.co.homeplus.admin.web.statistics.service.CartCouponStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 통계 > 프로모통계 > 장바구니쿠폰통계
 */
@Slf4j
@Controller
@RequestMapping("/statistics/cartcoupon")
public class CartCouponStatisticsController {

    private final CartCouponStatisticsService cartCouponStatisticsService;

    public CartCouponStatisticsController(CartCouponStatisticsService cartCouponStatisticsService) {
        this.cartCouponStatisticsService = cartCouponStatisticsService;
    }

    /**
     * 장바구니쿠폰통계 메인
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/cartCouponStatisticsMain")
    public String cartCouponStatisticsMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("cartCouponStatisticsBaseInfo", CartCouponStatisticsDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("cartCouponStatisticsOrdDtBaseInfo", CartCouponStatisticsDailyDto.class));

        return "/statistics/cartCouponStatisticsMain";
    }

    /**
     * 장바구니쿠폰통계(점포별) 조회
     * @param cartCouponStatisticsSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/getCartCouponStatisticsList.json")
    public List<CartCouponStatisticsDto> getCartCouponStatisticsList(@ModelAttribute CartCouponStatisticsSelectDto cartCouponStatisticsSelectDto) throws Exception {
        return cartCouponStatisticsService.getCartCouponStatistics(cartCouponStatisticsSelectDto);
    }

    /**
     * 장바구니쿠폰통계(일자별) 조회
     * @param cartCouponStatisticsSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/getCartCouponStatisticsListByStoreId.json")
    public List<CartCouponStatisticsDailyDto> getCartCouponStatisticsListByStoreId(@ModelAttribute CartCouponStatisticsSelectDto cartCouponStatisticsSelectDto) throws Exception {
        return cartCouponStatisticsService.getCartCouponStatisticsByStoreId(cartCouponStatisticsSelectDto);
    }
}
