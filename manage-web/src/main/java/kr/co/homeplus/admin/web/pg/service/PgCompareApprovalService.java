package kr.co.homeplus.admin.web.pg.service;

import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalDiffDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalSumDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PgCompareApprovalService {
    private final ResourceClient resourceClient;

    /**
     * PG 승인대사 합계내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCompareApprovalSumDto> getPgCompareApprovalSumList(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_COMPARE_APPROVAL_SUM, PgCompareApprovalSelectDto.class, pgCompareApprovalSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgCompareApprovalSumDto>>>() {};

        return (List<PgCompareApprovalSumDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * PG 승인대사 차이내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     * @throws Exception
     */
    public List<PgCompareApprovalDiffDto> getPgCompareApprovalDiffList(PgCompareApprovalSelectDto pgCompareApprovalSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_COMPARE_APPROVAL_DIFF, PgCompareApprovalSelectDto.class, pgCompareApprovalSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgCompareApprovalDiffDto>>>() {};

        return (List<PgCompareApprovalDiffDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

}
