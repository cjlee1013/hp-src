package kr.co.homeplus.admin.web.settle.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnshipOrderListGetDto {
    @ApiModelProperty(value = "마감일")
    @RealGridColumnInfo(headText = "마감일", width = 120, sortable = true)
    private String basicDt;

    @ApiModelProperty(value = "이월구분")
    @RealGridColumnInfo(headText = "이월구분", width = 120, sortable = true)
    private String carryDownType;

    @ApiModelProperty(value = "결제일")
    @RealGridColumnInfo(headText = "결제일", width = 120, sortable = true)
    private String paymentBasicDt;

    @ApiModelProperty(value = "배송완료일")
    @RealGridColumnInfo(headText = "배송완료일", width = 120, sortable = true)
    private String completeDt;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 120, sortable = true, hidden = true)
    private String storeType;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", width = 120, sortable = true)
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 150, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "상품구분")
    @RealGridColumnInfo(headText = "상품구분", width = 120, sortable = true)
    private String mallType;

    @ApiModelProperty(value = "주문구분")
    @RealGridColumnInfo(headText = "주문구분", width = 120, sortable = true)
    private String gubun;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 150, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 150, sortable = true)
    private String bundleNo;

    @ApiModelProperty(value = "클레임번호")
    @RealGridColumnInfo(headText = "클레임그룹번호", width = 150, sortable = true)
    private String claimNo;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 150, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 150, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 150, sortable = true)
    private String partnerName;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 120, sortable = true)
    private String orderType;

    @ApiModelProperty(value = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호", width = 150, sortable = true)
    private String orderItemNo;

    @ApiModelProperty(value = "카테고리그룹")
    @RealGridColumnInfo(headText = "카테고리그룹", width = 100, sortable = true)
    private String division;

    @ApiModelProperty(value = "카테고리ID")
    @RealGridColumnInfo(headText = "카테고리ID", width = 120, sortable = true)
    private String deptNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 150, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 350, sortable = true)
    private String itemNm;

    @ApiModelProperty(value = "과면세")
    @RealGridColumnInfo(headText = "과면세", width = 120, sortable = true)
    private String taxYn;

    @ApiModelProperty(value = "수수료율")
    @RealGridColumnInfo(headText = "수수료율", width = 150, sortable = true)
    private String commissionRate;

    @ApiModelProperty(value = "매입원가")
    @RealGridColumnInfo(headText = "매입원가", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orgPrice;

    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(headText = "상품금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String itemSaleAmt;

    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(headText = "수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeQty;

    @ApiModelProperty(value = "매출(IN VAT)")
    @RealGridColumnInfo(headText = "매출(IN VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeAmt;

    @ApiModelProperty(value = "매출(EX VAT)")
    @RealGridColumnInfo(headText = "매출(EX VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeAmtExVat;

    @ApiModelProperty(value = "정산(IN VAT)")
    @RealGridColumnInfo(headText = "정산(IN VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleAmt;

    @ApiModelProperty(value = "정산(EX VAT)")
    @RealGridColumnInfo(headText = "정산(EX VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleAmtExVat;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String claimReturnShipAmt;

    @ApiModelProperty(value = "할인")
    @RealGridColumnInfo(headText = "할인", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;

    @ApiModelProperty(value = "결제금액합계")
    @RealGridColumnInfo(headText = "결제금액합계", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orderAmt;

    @ApiModelProperty(value = "PG")
    @RealGridColumnInfo(headText = "PG", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String pgAmt;

    @ApiModelProperty(value = "DGV")
    @RealGridColumnInfo(headText = "DGV", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String dgvAmt;

    @ApiModelProperty(value = "MHC")
    @RealGridColumnInfo(headText = "MHC", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String mhcAmt;

    @ApiModelProperty(value = "OCB")
    @RealGridColumnInfo(headText = "OCB", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String ocbAmt;

    @ApiModelProperty(value = "마일리지")
    @RealGridColumnInfo(headText = "마일리지", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String mileageAmt;

    @ApiModelProperty(value = "배송상태")
    @RealGridColumnInfo(headText = "배송상태", width = 100, sortable = true)
    private String shipStatus;

    @ApiModelProperty(value = "미수취신고상태")
    @RealGridColumnInfo(headText = "미수취신고상태", width = 100, sortable = true)
    private String nonRcvType;
}
