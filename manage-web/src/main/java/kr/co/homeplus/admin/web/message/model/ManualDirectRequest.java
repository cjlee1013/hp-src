package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import java.util.List;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ManualDirectRequest {

    private long workSeq;

    private String workName;

    private String description;

    private String regId;
    
    private String subject;

    private String body;

    private String callback;

    private WorkType workType;

    private List<BasicRecipient> bodyArgument;

    private String delayYn;

    private LocalDateTime workDt;
}
