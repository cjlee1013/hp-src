package kr.co.homeplus.admin.web.core.utility;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
@Data
public class DhtmlxTreeNode {
	private static final String TREEGRID_CHILD_KEY      = "rows";           // dhtmlx tree grid 자식 노드 key
	private static final String TREEGRID_PARENT_IMG     = "folder.gif";     // dhtmlx tree grid 부모 노드 폴더 이미지

	private static final String TREE_CHILD_KEY      	= "item";           // dhtmlx tree grid 자식 노드 key
	private static final String TREE_VALUE_KEY      	= "text";           // dhtmlx tree grid 자식 노드 key

	private Map<String, Map<String, Object>> nodes  	= new HashMap<>();    // tree nodes
	private Map<String, Map<String, Object>> treeNodes  	= new HashMap<>();    // tree nodes

	public void addNode(Map<String, Object> data, final String treeName, final String id, final String parentId) {
		Map<String, Object> parentMap   = new HashMap<>();
		Map<String, Object> folderMap   = new HashMap<>();
		List<Map<String, Object>> rows  = new ArrayList();
		String treeNameStr              = "";

		data.put(TREEGRID_CHILD_KEY, new ArrayList());

		this.getNodes().put(id, data);

		if (!parentId.isEmpty()) {

			if (this.getNodes().get(parentId) != null) {
				parentMap = (Map<String, Object>) this.getNodes().get(parentId);
			}

			if (!StringUtils.isEmpty(treeName)) {
				if (parentMap.get(treeName) instanceof Map) {
					Map<String, String> treeNameMap = (Map<String, String>) parentMap.get(treeName);
					treeNameStr = treeNameMap.get("value");
				}
				else {
					treeNameStr = (String) parentMap.get(treeName);
				}

				folderMap.put("value", treeNameStr);
				folderMap.put("image", TREEGRID_PARENT_IMG);
				parentMap.put(treeName, folderMap);
			}

			if (parentMap.get(TREEGRID_CHILD_KEY) != null) {
				rows = (List<Map<String, Object>>) parentMap.get(TREEGRID_CHILD_KEY);
				rows.add(data);
				parentMap.put(TREEGRID_CHILD_KEY, rows);
			}
			
			this.getNodes().put(parentId, parentMap);
		}
	}

	public void addNodeTree(Map<String, Object> data, final String id, final String parentId, final String itemNm) {
        Map<String, Object> parentMap   = new HashMap<>();
        List<Map<String, Object>> items  = new ArrayList();

        data.put(TREE_VALUE_KEY, data.get(itemNm));

        this.getTreeNodes().put(id, data);

        if (!parentId.isEmpty()) {
            if (this.getTreeNodes().get(parentId) != null)
                parentMap = (Map<String, Object>) this.getTreeNodes().get(parentId);

            if (parentMap.get(TREE_CHILD_KEY) != null) {
                items = (List<Map<String, Object>>) parentMap.get(TREE_CHILD_KEY);
                items.add(data);
                parentMap.put(TREE_CHILD_KEY, items);
            }
            else {
                items.add(data);
                parentMap.put(TREE_CHILD_KEY, items);
            }

            this.getTreeNodes().put(parentId, parentMap);
        }
	}
}
