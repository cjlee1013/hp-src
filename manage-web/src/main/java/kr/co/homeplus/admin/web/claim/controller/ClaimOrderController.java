package kr.co.homeplus.admin.web.claim.controller;


import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderDetailInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimOrderDetailInfoSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundGetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimRegSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShipInfoDto;
import kr.co.homeplus.admin.web.claim.model.MultiShipClaimItemListGetDto;
import kr.co.homeplus.admin.web.claim.model.MultiShipClaimItemListSetDto;
import kr.co.homeplus.admin.web.claim.service.ClaimOrderService;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/order/claim")
@RequiredArgsConstructor
@Slf4j
public class ClaimOrderController {

    private final ClaimOrderService claimOrderService;
    private final CodeService codeService;

    /**
     * 주문관리 > 주문관리 > 주문취소 팝업
     *
     * @param purchaseOrderNo, bundleNo
     * @param claimPartYn 전체 취소 여부 Y:N
     * @param orderCancelYn 주문취소/취소신청 여부 Y:주문취소, N:취소신청
     * @return String
     */
    @GetMapping("/popup/cancelOrder")
    public String cancelOrder(Model model,
        @RequestParam(value = "purchaseOrderNo") long purchaseOrderNo,
        @RequestParam(value = "bundleNo", required = false, defaultValue = "0") long bundleNo,
        @RequestParam(value = "claimPartYn") String claimPartYn,
        @RequestParam(value="orderCancelYn") String orderCancelYn,
        @RequestParam(value = "multiShipYn", required = false, defaultValue = "N") String multiShipYn) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_reason_type"            // 클레임 사유 코드
        );
        model.addAttribute("claimReasonType", code.get("claim_reason_type"));
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        model.addAttribute("bundleNo", bundleNo);
        model.addAttribute("claimPartYn", claimPartYn);
        model.addAttribute("orderCancelYn", orderCancelYn);
        model.addAttribute("multiShipYn", multiShipYn);
        model.addAttribute("claimTypeName", "취소");

        if(multiShipYn.equalsIgnoreCase("Y")){
            //다중배송 리스트 조회
            model.addAttribute("multiShipList", claimOrderService.getMultiShipAddrList(purchaseOrderNo, bundleNo));
        }


        return "/order/pop/orderCancelPop";
    }

    /**
     * 주문관리 > 주문관리 > 주문반품 팝업
     *
     * @param purchaseOrderNo, bundleNo
     * @return String
     */
    @GetMapping("/popup/returnOrder")
    public String returnOrder(Model model,
        @RequestParam(value = "purchaseOrderNo") long purchaseOrderNo,
        @RequestParam(value = "bundleNo") long bundleNo,
        @RequestParam(value = "shipType") String shipType,
        @RequestParam(value = "multiShipYn", required = false, defaultValue = "N") String multiShipYn) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_reason_type",           // 클레임 사유 코드
            "dlv_cd"                                // 택배사 코드
        );
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimReasonType", code.get("claim_reason_type"));
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        model.addAttribute("bundleNo", bundleNo);
        model.addAttribute("shipType", shipType);
        model.addAttribute("multiShipYn", multiShipYn);
        model.addAttribute("claimTypeName", "반품");

        if(multiShipYn.equalsIgnoreCase("Y")){
            //다중배송 리스트 조회
            model.addAttribute("multiShipList", claimOrderService.getMultiShipAddrList(purchaseOrderNo, bundleNo));
        }

        return "/order/pop/orderReturnPop";
    }

    /**
     * 주문관리 > 주문관리 > 주문교환 팝업
     *
     * @param purchaseOrderNo, bundleNo
     * @return String
     */
    @GetMapping("/popup/exchangeOrder")
    public String refundOrder(Model model,
        @RequestParam(value = "purchaseOrderNo") long purchaseOrderNo,
        @RequestParam(value = "bundleNo") long bundleNo,
        @RequestParam("shipType") String shipType,
        @RequestParam(value = "multiShipYn", required = false, defaultValue = "N") String multiShipYn) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "claim_reason_type",            // 클레임 사유 코드
            "dlv_cd"                                // 택배사 코드
        );
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimReasonType", code.get("claim_reason_type"));
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        model.addAttribute("bundleNo", bundleNo);
        model.addAttribute("shipType", shipType);
        model.addAttribute("multiShipYn", multiShipYn);
        model.addAttribute("claimTypeName", "교환");

        if(multiShipYn.equalsIgnoreCase("Y")){
            //다중배송 리스트 조회
            model.addAttribute("multiShipList", claimOrderService.getMultiShipAddrList(purchaseOrderNo, bundleNo));
        }

        return "/order/pop/orderExchangePop";
    }

    /**
     * 주문관리 > 주문관리 > 취소/교환/환불 팝업 > 배송지 정보
     *
     * @param model, reqType
     * @return String
     */
    @GetMapping("/popup/pop/deliveryInfoPop")
    public String deliveryInfoPop(Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("reqType", request.getParameter("reqType"));
        model.addAttribute("name", request.getParameter("name"));
        model.addAttribute("phone", request.getParameter("phone"));
        model.addAttribute("zipCode", request.getParameter("zipCode"));
        model.addAttribute("addr", request.getParameter("addr"));
        model.addAttribute("addrDetail", request.getParameter("addrDetail"));
        return "/order/pop/deliveryInfoPop";
    }

    /**
     * 주문관리 > 주문관리 > 취소/교환/환불 팝업 > 클레임 리스트 조회
     *
     * @param claimOrderDetailInfoSetDto
     * @return List<ClaimOrderDetailInfoGetDto>
     */
    @ResponseBody
    @PostMapping("/getClaimDetailInfo.json")
    public List<ClaimOrderDetailInfoGetDto> getClaimOrderDetail(@RequestBody ClaimOrderDetailInfoSetDto claimOrderDetailInfoSetDto) {
        return claimOrderService.getClaimOrderDetail(claimOrderDetailInfoSetDto);
    }


    /**
     * 주문관리 > 주문관리 > 반품/교환 팝업 > 배송정보 조회
     *
     * @param bundleNo
     * @return List<ClaimOrderDetailInfoGetDto>
     */
    @ResponseBody
    @GetMapping("/getMultiShipInfo.json")
    public ClaimShipInfoDto getMultiShipInfo(@RequestParam(name = "bundleNo") long bundleNo, @RequestParam(name = "multiBundleNo") long multiBundleNo) {
        return claimOrderService.getMultiShipInfo(bundleNo, multiBundleNo);
    }

    /**
     * 주문관리 > 주문관리 > 반품/교환 팝업 > 배송정보 조회
     *
     * @param bundleNo
     * @return List<ClaimOrderDetailInfoGetDto>
     */
    @ResponseBody
    @GetMapping("/getClaimShipInfo.json")
    public ClaimShipInfoDto getClaimShipInfo(@RequestParam(name = "bundleNo") long bundleNo) {
        return claimOrderService.getClaimShipInfo(bundleNo);
    }

    /**
     * 주문관리 > 주문관리 > 취소/교환/환불 팝업 > 환불예정금액 조회
     *
     * @param claimPreRefundSetDto
     * @return List<ClaimOrderDetailInfoGetDto>
     */
    @ResponseBody
    @PostMapping("/preRefundPrice.json")
    public ClaimPreRefundGetDto getPreRefundPrice(@RequestBody ClaimPreRefundSetDto claimPreRefundSetDto) throws Exception {
        return claimOrderService.getPreRefundPrice(claimPreRefundSetDto);
    }

    @ResponseBody
    @PostMapping("/claimRegister.json")
    public ResponseObject<String> claimRegister(@RequestBody ClaimRegSetDto claimRegSetDto) throws Exception {
        return claimOrderService.setClaimRegister(claimRegSetDto);
    }

    /**
     * 주문관리 > 주문관리 > 다중배송 취소/교환/환불 팝업 > 클레임 리스트 조회
     *
     * @param multiShipClaimItemListSetDto
     * @return List<MultiShipClaimItemListGetDto>
     */
    @ResponseBody
    @PostMapping("/getMultiShipItemList.json")
    public List<MultiShipClaimItemListGetDto> getMultiShipItemList(@RequestBody MultiShipClaimItemListSetDto multiShipClaimItemListSetDto) {
        return claimOrderService.getMultiShipItemList(multiShipClaimItemListSetDto);
    }

}
