package kr.co.homeplus.admin.web.item.model.recomMsg;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRecomMsgItemParamDto {

    private List<String> itemNoList;
    private String storeType;
}