package kr.co.homeplus.admin.web.settle.model.partnerSettleDetail;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerSettleItemGetDto {
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", width = 100)
    private String basicDt;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 100)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 100)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 150)
    private String partnerNm;

    @ApiModelProperty(value = "구분") //1:상품, 2:배송비
    @RealGridColumnInfo(headText = "구분", width = 100)
    private String orderType;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 100)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 200)
    private String itemNm;

    @ApiModelProperty(value = "과면세구분")
    @RealGridColumnInfo(headText = "과면세구분", width = 100)
    private String taxYn;

    @ApiModelProperty(value = "매출집계수량")
    @RealGridColumnInfo(headText = "매출집계수량", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteQty;

    @ApiModelProperty(value = "매출집계금액")
    @RealGridColumnInfo(headText = "매출집계금액", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipCompleteAmt;

    @ApiModelProperty(value = "판매수수료")
    @RealGridColumnInfo(headText = "판매수수료", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long saleAgencyFee;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimReturnShipAmt;

    @ApiModelProperty(value = "쿠폰할인금액")
    @RealGridColumnInfo(headText = "합계", width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponChargeAmt;

    @ApiModelProperty(value = "파트너부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "업체", width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponSellerChargeAmt;

    @ApiModelProperty(value = "홈플러스부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "자사", width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponHomeChargeAmt;

    @ApiModelProperty(value = "카드부담 쿠폰할인금액")
    @RealGridColumnInfo(headText = "카드사", width = 120, groupName="상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long couponCardChargeAmt;

    @ApiModelProperty(value = "카드할인금액")
    @RealGridColumnInfo(headText = "합계", width = 120, groupName="카드할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponAmt;

    @ApiModelProperty(value = "홈플러스부담 카드할인금액")
    @RealGridColumnInfo(headText = "자사", width = 120, groupName="카드할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponHomeAmt;

    @ApiModelProperty(value = "카드부담 카드할인금액")
    @RealGridColumnInfo(headText = "카드사", width = 120, groupName="카드할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardCouponCardAmt;

    @ApiModelProperty(value = "배송비할인")
    @RealGridColumnInfo(headText = "배송비할인", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    @RealGridColumnInfo(headText = "임직원할인", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long empDiscountAmt;

    @ApiModelProperty(value = "조정금액")
    @RealGridColumnInfo(headText = "조정금액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustAmt;

    @ApiModelProperty(value = "조정수수료")
    @RealGridColumnInfo(headText = "조정수수료", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long adjustFee;
    
}
