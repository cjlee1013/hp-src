package kr.co.homeplus.admin.web.user.controller;

import com.google.gson.Gson;
import java.util.List;
import kr.co.homeplus.admin.web.user.model.user.GradeStatChangeDto;
import kr.co.homeplus.admin.web.user.model.user.GradeStatChangeParam;
import kr.co.homeplus.admin.web.user.service.GradeStatService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridBuilder;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumn;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDataProviderOption;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOption;
import kr.co.homeplus.plus.api.support.realgrid.RealGridType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user/grade")
@RequiredArgsConstructor
public class GradeStatController {

    private final GradeStatService gradeStatService;

    /**
     * 회원등급변화통계 Main Page
     */
    @GetMapping(value = "/stat")
    public String gradeStatMain(final Model model, GradeStatChangeParam gradeStatChangeParam) {

        model.addAttribute("gradeStatChageListJson", gradeStatService.getGradeStatListJson(gradeStatChangeParam));
        model.addAttribute("gradeApplyYmList", gradeStatService.getGradeApplyYmList());
        model.addAttribute("gradeStatGridBaseInfo", gradeStatService.getRealGridSchemeForGradeStat(gradeStatChangeParam));

        return "/user/gradeStatMain";
    }

}
