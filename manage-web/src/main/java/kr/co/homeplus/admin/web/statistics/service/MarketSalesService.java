package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.DailyOrderInfoPartnerGetDto;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.DailyOrderInfoPartnerSetDto;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.DailyOrderInfoStoreGetDto;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.DailyOrderInfoStoreSetDto;
import kr.co.homeplus.admin.web.statistics.model.market.MarketStatisticsSalesGetDto;
import kr.co.homeplus.admin.web.statistics.model.market.MarketStatisticsSalesSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * 통계 > 마켓통계
 */
@Service
@RequiredArgsConstructor
public class MarketSalesService {
    private final ResourceClient resourceClient;
    public List<MarketStatisticsSalesGetDto> getSales(MarketStatisticsSalesSetDto marketStatisticsSalesSetDto) throws Exception {
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SETTLE)
            .uri("/admin/market/getMarketStatisticsSales")
            .postObject(marketStatisticsSalesSetDto)
            .typeReference(new ParameterizedTypeReference<>(){})
            .build();

        ResponseObject<Object> responseObject = resourceClient.post(request,new TimeoutConfig()).getBody();
        if (responseObject != null) {
            return (List<MarketStatisticsSalesGetDto>)responseObject.getData();
        } else {
            return null;
        }
    }
}
