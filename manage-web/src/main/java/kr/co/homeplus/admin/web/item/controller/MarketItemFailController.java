package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.item.model.market.MarketItemFailGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemFailParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/market")
public class MarketItemFailController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;

    public MarketItemFailController(ResourceClient resourceClient, LoginCookieService cookieService, CodeService codeService) {

        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 상품관리 > 마켓연동상품관리 > Main
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/marketItemFailMain")
    public String StoreMain(Model model) throws Exception{
        //그리드
        model.addAllAttributes(
            RealGridHelper.create("marketItemFailListGridBaseInfo", MarketItemFailGetDto.class));

        return "/item/marketItemFailMain";
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 연동실패 조회
     * @param marketItemFailParamDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/getMarketItemFailList.json"})
    public List<MarketItemFailGetDto> getMarketItemFailList(@RequestBody @Valid MarketItemFailParamDto marketItemFailParamDto) {

        ResourceClientRequest<List<MarketItemFailGetDto>> request = ResourceClientRequest.<List<MarketItemFailGetDto>>postBuilder()
            .apiId(ResourceRouteName.ITEM)
            .uri("/item/market/getMarketItemFailList")
            .postObject(marketItemFailParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @GetMapping(value = "/popup/marketItemFailHelpPop")
    public String marketItemFailHelpPop(){
        return "/item/pop/marketItemFailHelpPop";
    }

}

