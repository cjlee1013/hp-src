package kr.co.homeplus.admin.web.settle.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum SettleMallType {
    TD("TD"),
    DS("DS"),
    DC("DC");

    private String code;
}
