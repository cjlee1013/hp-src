package kr.co.homeplus.admin.web.manage.model.dspSearchBar;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspSearchBarListParamDto {

    private String searchStartDt;
    private String searchEndDt;
    private String searchSiteType;
    private String searchPeriodType;
    private String searchLocCd;
    private String searchDevice;
    private String searchUseYn;
    private String searchType;
    private String searchKeyword;

}