package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 상품별 Shift 관리 INSERT 용 DTO")
public class ItemShiftManageInsertModel {
    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 1, required = true)
    private String storeType;
    @ApiModelProperty(notes = "점포ID", position = 2, required = true)
    private String storeId;
    @ApiModelProperty(notes = "상품번호", position = 3, required = true)
    private String itemNo;

    @ApiModelProperty(notes = "Shift1", position = 4)
    private String shift1;
    @ApiModelProperty(notes = "Shift2", position = 5)
    private String shift2;
    @ApiModelProperty(notes = "Shift3", position = 6)
    private String shift3;
    @ApiModelProperty(notes = "Shift4", position = 7)
    private String shift4;
    @ApiModelProperty(notes = "Shift5", position = 8)
    private String shift5;
    @ApiModelProperty(notes = "Shift6", position = 9)
    private String shift6;
    @ApiModelProperty(notes = "Shift7", position = 9)
    private String shift7;
    @ApiModelProperty(notes = "Shift8", position = 9)
    private String shift8;
    @ApiModelProperty(notes = "Shift9", position = 9)
    private String shift9;

    @ApiModelProperty(notes = "등록자", position = 10)
    private String regId;
    @ApiModelProperty(notes = "수정자", position = 11)
    private String chgId;
}
