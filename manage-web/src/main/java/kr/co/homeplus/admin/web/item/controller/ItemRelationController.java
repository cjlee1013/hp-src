package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.itemRelation.ItemRelationDetailGetDto;
import kr.co.homeplus.admin.web.item.model.itemRelation.ItemRelationListGetDto;
import kr.co.homeplus.admin.web.item.model.itemRelation.ItemRelationListParamDto;
import kr.co.homeplus.admin.web.item.model.itemRelation.ItemRelationSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item")
public class ItemRelationController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    public ItemRelationController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;

    }

    /**
     * 상품관리 > 셀러상품관리 > 연관 상품관리
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/itemRelationMain", method = RequestMethod.GET)
    public String itemRelationMain(Model model) throws Exception {

        codeService.getCodeModel(model,
            "disp_yn"              //노출여부
                ,   "set_yn"                //설정여부
                ,   "disp_pc_type"          //PC노출기준
                ,   "disp_mobile_type"      //모바일노출기준
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAllAttributes(RealGridHelper.createForGroup("itemRelationListGridBaseInfo", ItemRelationListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("itemRelationDetailListGridBaseInfo", ItemRelationDetailGetDto.class));

        return "/item/itemRelationMain";
    }

    /**
     * 상품관리 > 셀러상품관리 > 연관상품 조회
     * @param itemRelationListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemRelationList.json"})
    public List<ItemRelationListGetDto> getItemRelationList(ItemRelationListParamDto itemRelationListParamDto) {
        String apiUri = "/item/getItemRelationList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemRelationListGetDto>>>() {};
        return (List<ItemRelationListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil
            .getRequestString(apiUri, ItemRelationListParamDto.class, itemRelationListParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 셀러상품관리 > 연관상품상세 리스트 조회
     * @param relationNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemRelationDetailList.json"})
    public List<ItemRelationDetailGetDto> getItemRelationDetailList(@RequestParam(name = "relationNo") Long relationNo) {
        String apiUri = "/item/getItemRelationDetailList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemRelationDetailGetDto>>>() {};
        return (List<ItemRelationDetailGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?relationNo=" + relationNo , typeReference).getData();
    }

    /**
     * 상품관리 > 셀러상품관리 > 연관상품상세 조회
     * @param itemNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"getItemRelationDetail.json"})
    public ItemRelationDetailGetDto getItemRelationDetail(@RequestParam(name = "itemNo") String itemNo) {
        String apiUri = "/item/getItemRelationDetail";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ItemRelationDetailGetDto>>() {};
        return (ItemRelationDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?itemNo=" + itemNo , typeReference).getData();
    }

    /**
     * 상품관리 > 셀러상품관리 > 연관
     * @param itemRelationSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemRelation.json"}, method = RequestMethod.POST)
    public ResponseResult setItemGroup (@RequestBody ItemRelationSetDto itemRelationSetDto) {
        String apiUri = "/item/setItemRelation";

        itemRelationSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemRelationSetDto, apiUri, typeReference).getData();

    }

}
