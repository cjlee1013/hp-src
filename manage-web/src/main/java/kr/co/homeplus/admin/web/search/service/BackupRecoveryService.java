package kr.co.homeplus.admin.web.search.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class BackupRecoveryService {

    private final ResourceClient resourceClient;
    private final RestTemplate restTemplate;

    public ResponseObject backupTableList(String schServiceCd) {
        String apiUri = "/deploy/backupTableList/" + schServiceCd;
        return getResponseObject(apiUri);
    }

    public ResponseObject<Object> txtAsDB(String schServiceCd, List<MultipartFile> files, String passwd) {
        if(!passwd.trim().equals("homeplus1@")) {
            throw new ValidationException("비밀번호가 일치하지 않습니다.");
        }
        String apiUri = "/deploy/txtAsDB/" + schServiceCd.split(",")[0];
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        List<String> tempFileNames = new ArrayList<>();
        String tempFileName;
        FileOutputStream fo = null;

        ResponseObject responseEntity = null;
        try {
            for (MultipartFile file : files) {
                tempFileName = file.getOriginalFilename();
                tempFileNames.add(tempFileName);
                fo = new FileOutputStream(tempFileName);
                if(fo == null) {
                    throw new ValidationException("다운로드 파일 정보가 존재하지 않습니다.");
                }
                fo.write(file.getBytes());

                map.add("file", new FileSystemResource(tempFileName));
            }

            responseEntity = getResponseObjectMultiPart(apiUri, map);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fo != null) {
                try {
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    fo = null;
                }
            }
            for (String fileName : tempFileNames) {
                File f = new File(fileName);
                if (f != null)
                    f.delete();
            }
        }

        return responseEntity;
    }

    public void downloadsAsTxt(String schServiceCd, String tableName, HttpServletRequest request, HttpServletResponse response)
        throws IOException {
        String apiUri = "/deploy/downloadsAsTxt/" + schServiceCd + "?tableName=" + tableName;
        final ResponseEntity<Resource> responseFile = getResponseObjectResource(apiUri);

        response.setContentType("application/octet-stream; charset=utf-8");
        String browser = getBrowser(request);
        String disposition = getDisposition(tableName, browser);
        response.setHeader("Content-Disposition", disposition);
        response.setHeader("Content-Transfer-Encoding", "binary");
        OutputStream out = null;
        InputStream fis = null;

        if (responseFile.getBody() == null) {
            return;
        }

        try {
            out = response.getOutputStream();
            fis = responseFile.getBody().getInputStream();
            FileCopyUtils.copy(fis, out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1)
            return "MSIE";
        else if (header.indexOf("Chrome") > -1)
            return "Chrome";
        else if (header.indexOf("Opera") > -1)
            return "Opera";
        return "Firefox";
    }

    private String getDisposition(String filename, String browser)
        throws UnsupportedEncodingException {
        String dispositionPrefix = "attachment;filename=";
        String encodedFilename = null;
        if (browser.equals("MSIE")) {
            encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll(
                "\\+", "%20");
        } else if (browser.equals("Firefox")) {
            encodedFilename = "\""
                + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
        } else if (browser.equals("Opera")) {
            encodedFilename = "\""
                + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
        } else if (browser.equals("Chrome")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < filename.length(); i++) {
                char c = filename.charAt(i);
                if (c > '~') {
                    sb.append(URLEncoder.encode("" + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            encodedFilename = sb.toString();
        }
        return dispositionPrefix + encodedFilename;
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private ResponseObject getResponseObjectMultiPart(String apiUri, MultiValueMap<String,Object> multiValueMap){
        ResourceClientRequest<Object> request = ResourceClientRequest.postForFileUploadBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .multiValueMap(multiValueMap)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }

    private ResponseEntity<Resource> getResponseObjectResource(String apiUri){
        return restTemplate.exchange(
              resourceClient.getResourceUrl(ResourceRouteName.SEARCHMNG, apiUri)
            , HttpMethod.GET
            , new HttpEntity<>(createHttpHeaders())
            , Resource.class );
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }
}
