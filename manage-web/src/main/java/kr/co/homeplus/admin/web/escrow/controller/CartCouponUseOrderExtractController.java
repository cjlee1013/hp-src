package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractCartCouponUseOrderDto;
import kr.co.homeplus.admin.web.escrow.model.extract.OrderDataExtractSelectDto;
import kr.co.homeplus.admin.web.escrow.service.OrderDataExtractService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 프로모션관리 > 쿠폰 > 장바구니 쿠폰 사용 조회
 */
@Controller
@RequestMapping("/escrow/extract")
public class CartCouponUseOrderExtractController {
    private final OrderDataExtractService orderDataExtractService;
    private final AuthorityService authorityService;
    private final LoginCookieService loginCookieService;

    public CartCouponUseOrderExtractController(OrderDataExtractService orderDataExtractService, AuthorityService authorityService,
            LoginCookieService loginCookieService) {
        this.orderDataExtractService = orderDataExtractService;
        this.authorityService = authorityService;
        this.loginCookieService = loginCookieService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/cartCouponUseOrderExtractMain")
    public String cartCouponUseOrderExtractMain(Model model) throws Exception {
        // 엑셀 다운로드 제어를 위해 역할코드 보유여부 추가
        model.addAttribute("excelDownloadAuth", authorityService.hasRole(EscrowConstants.ROLE_CODE_SYSTEM_ESCROW) ? "Y" : "N");
        model.addAllAttributes(RealGridHelper.create("cartCouponUseOrderExtractGridBaseInfo", ExtractCartCouponUseOrderDto.class));

        return "/escrow/cartCouponUseOrderExtractMain";
    }

    /**
     * 장바구니쿠폰 사용주문 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getCartCouponUseOrderList.json")
    public List<ExtractCartCouponUseOrderDto> getCartCouponUseOrderList(@ModelAttribute OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CART_COUPON_USE_ORDER_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return orderDataExtractService.getCartCouponUseOrderList(orderDataExtractSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/saveCartCouponUseOrderExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CART_COUPON_USE_ORDER_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractService.saveExtractHistory(extractCommonSetDto);
    }
}
