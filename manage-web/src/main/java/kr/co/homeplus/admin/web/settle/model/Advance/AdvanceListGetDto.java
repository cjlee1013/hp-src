package kr.co.homeplus.admin.web.settle.model.Advance;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "선수금조회 목록")
public class AdvanceListGetDto {
  @ApiModelProperty(notes = "기준월")
  @RealGridColumnInfo(headText = "기준월", sortable = true, width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "결제일")
  @RealGridColumnInfo(headText = "결제일", sortable = true, width = 100)
  private String paymentBasicDt;

  @ApiModelProperty(notes = "입금일")
  @RealGridColumnInfo(headText = "입금일", sortable = true, width = 100)
  private String payScheduleDt;

  @ApiModelProperty(notes = "매출일")
  @RealGridColumnInfo(headText = "매출일", sortable = true, width = 100)
  private String settleDt;

  @ApiModelProperty(notes = "상품구분")
  @RealGridColumnInfo(headText = "상품구분", sortable = true, width = 100)
  private String mallType;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "배송번호")
  @RealGridColumnInfo(headText = "배송번호", sortable = true, width = 100)
  private String bundleNo;

  @ApiModelProperty(notes = "클레임그룹번호")
  @RealGridColumnInfo(headText = "클레임그룹번호", sortable = true, width = 100)
  private String claimNo;

  @ApiModelProperty(notes = "상품번호")
  @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 100)
  private String itemNo;

  @ApiModelProperty(notes = "구분")
  @RealGridColumnInfo(headText = "구분", sortable = true, width = 100)
  private String gubun;

  @ApiModelProperty(notes = "주문금액")
  @RealGridColumnInfo(headText = "주문금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "자사할인")
  @RealGridColumnInfo(headText = "자사할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String homeAmt;

  @ApiModelProperty(notes = "업체할인")
  @RealGridColumnInfo(headText = "업체할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String sellerAmt;

  @ApiModelProperty(notes = "판매수익")
  @RealGridColumnInfo(headText = "판매수익", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String profitAmt;

  @ApiModelProperty(notes = "PG예치금")
  @RealGridColumnInfo(headText = "PG예치금", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String pgBalance;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "PG결제금액")
  @RealGridColumnInfo(headText = "PG결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String pgAmt;

  @ApiModelProperty(notes = "이외수단")
  @RealGridColumnInfo(headText = "이외수단", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String etcAmt;

  @ApiModelProperty(notes = "이월 선수금")
  @RealGridColumnInfo(headText = "이월 선수금", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String receivedLast;

  @ApiModelProperty(notes = "당월 결제금액")
  @RealGridColumnInfo(headText = "당월 결제금액", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String monthOrder;

  @ApiModelProperty(notes = "당월 PG결제금액")
  @RealGridColumnInfo(headText = "당월 PG결제금액", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String monthPg;

  @ApiModelProperty(notes = "당월 이외수단")
  @RealGridColumnInfo(headText = "당월 이외수단", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String monthEtc;

  @ApiModelProperty(notes = "당월 PG입금액")
  @RealGridColumnInfo(headText = "당월 PG입금액", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String monthPgPay;

  @ApiModelProperty(notes = "구매확정")
  @RealGridColumnInfo(headText = "구매확정", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String decisionAmt;

  @ApiModelProperty(notes = "선수금 잔액")
  @RealGridColumnInfo(headText = "선수금 잔액", sortable = true, width = 100, groupName="선수금", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String receivedLeft;

  @ApiModelProperty(notes = "DS매출")
  @RealGridColumnInfo(headText = "DS매출", sortable = true, width = 100, groupName="선수수익", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String dsSales;

  @ApiModelProperty(notes = "세금계산서")
  @RealGridColumnInfo(headText = "세금계산서", sortable = true, width = 100, groupName="선수수익", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String taxInvoice;

  @ApiModelProperty(notes = "선수수익잔액")
  @RealGridColumnInfo(headText = "선수수익잔액", sortable = true, width = 100, groupName="선수수익", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String revenueLeft;

}
