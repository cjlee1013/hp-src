package kr.co.homeplus.admin.web.settle.model.unshipSlip;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipSlipListGetDto {
    @ApiModelProperty(value = "마감월")
    @RealGridColumnInfo(headText = "마감월", width = 120, sortable = true)
    private String basicDt;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 120, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 120, sortable = true)
    private String costCenterName;

    @ApiModelProperty(value = "코스트센터")
    @RealGridColumnInfo(headText = "코스트센터", width = 120, sortable = true)
    private String costCenter;

    @ApiModelProperty(value = "회사구분")
    @RealGridColumnInfo(headText = "회사구분", width = 120, sortable = true)
    private String company;

    @ApiModelProperty(value = "카테고리구분")
    @RealGridColumnInfo(headText = "카테고리구분", width = 120, sortable = true)
    private String category;

    @ApiModelProperty(value = "선수금일반")
    @RealGridColumnInfo(headText = "선수금일반", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt20800;

    @ApiModelProperty(value = "매출액차감_미배송")
    @RealGridColumnInfo(headText = "매출액차감_미배송", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt30376;

    @ApiModelProperty(value = "매출부가세")
    @RealGridColumnInfo(headText = "매출부가세", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt24290;

    @ApiModelProperty(value = "점출부가세")
    @RealGridColumnInfo(headText = "점출부가세", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt30200;

    @ApiModelProperty(value = "상품원가_직영/특정")
    @RealGridColumnInfo(headText = "상품원가_직영/특정", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt35000;

    @ApiModelProperty(value = "매장상품_직매입")
    @RealGridColumnInfo(headText = "매장상품_직매입", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String amt14160;
}
