package kr.co.homeplus.admin.web.voc.service;

import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class VocCommonService {

  private final ResourceClient resourceClient;
  private final LoginCookieService loginCookieService;
  private final CodeService codeService;

  /**
   * SO권한 여부
   */
  public String getHChannel(String storeType) {
    String hChannel = "";

    switch (storeType) {
      case "HYPER" : hChannel = "H_CHANNEL_1"; break;
      case "CLUB" : hChannel = "H_CHANNEL_2"; break;
      case "DS" : hChannel = "H_CHANNEL_3"; break;
      case "EXP" : hChannel = "H_CHANNEL_4"; break;
      default: hChannel = "";
    }

    return hChannel;
  }



}
