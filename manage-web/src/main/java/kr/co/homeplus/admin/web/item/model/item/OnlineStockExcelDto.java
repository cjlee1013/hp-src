package kr.co.homeplus.admin.web.item.model.item;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class OnlineStockExcelDto {

    @EqualsAndHashCode.Include
    private String storeId;

    private String ff;

    private String gr;

    private String gm;

    private String cl;

    private String electric;

    private String eleven;

    private String naver;

    private String userId;

}
