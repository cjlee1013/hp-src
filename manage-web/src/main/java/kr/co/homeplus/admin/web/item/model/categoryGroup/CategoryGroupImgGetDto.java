package kr.co.homeplus.admin.web.item.model.categoryGroup;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupImgGetDto {

	private Integer gcateCd;

	private String imgType;

	private Integer imgHeight;

	private Integer imgWidth;

	private String imgUrl;

	private String imgNm;

}
