package kr.co.homeplus.admin.web.message.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.model.ManualCancelRequest;
import kr.co.homeplus.admin.web.message.model.ManualEditRequest;
import kr.co.homeplus.admin.web.message.model.ManualHistoryDetailRequest;
import kr.co.homeplus.admin.web.message.model.ManualHistoryDetailResponse;
import kr.co.homeplus.admin.web.message.model.ManualHistoryListRequest;
import kr.co.homeplus.admin.web.message.model.ManualResponse;
import kr.co.homeplus.admin.web.message.model.ManualSetRequest;
import kr.co.homeplus.admin.web.message.model.ManualWorkGrid;
import kr.co.homeplus.admin.web.message.model.SendListGrid;
import kr.co.homeplus.admin.web.message.model.UploadResult;
import kr.co.homeplus.admin.web.message.service.ManualSupportService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/message/admin/manual")
@RequiredArgsConstructor
@Slf4j
public class ManualAdminController {

    private final CodeService codeService;

    private final ResourceClient resourceClient;

    private final ManualSupportService manualSupportService;

    private final Map<String, Object> manualWorkGrid = RealGridHelper
        .create("manualWorkGridBaseInfo", ManualWorkGrid.class);

    private final Map<String, Object> sendListGrid = RealGridHelper
        .create("sendListGridBaseInfo", SendListGrid.class);

    @GetMapping("/main")
    public String main(final Model model) throws JsonProcessingException {
        model.addAllAttributes(manualWorkGrid);
        model.addAllAttributes(sendListGrid);

        String workStatus = new ObjectMapper().writeValueAsString(WorkStatus.ETC.getLookUpMap());
        model.addAllAttributes(Map.of("workStatus", workStatus));
        model.addAllAttributes(Map.of("messageWorkStatus", getSelectBoxWorkStatusMap()));

        model.addAttribute("empId", manualSupportService.getEmpId());
        model.addAttribute("isHeadOffice", manualSupportService.isHeadOffice());
        return "/message/manualMain";
    }

    private Map<String, String> getSelectBoxWorkStatusMap() {
        Map<String, String> workStatusMap = WorkStatus.ETC.getLookUpMap();
        workStatusMap.remove(WorkStatus.CANCELED.name());
        workStatusMap.remove(WorkStatus.ETC.name());
        return workStatusMap;
    }

    @PostMapping("/history/getWorks.json")
    @ResponseBody
    public ResponseObject<List<ManualWorkGrid>> historyList(
        @RequestBody @Valid final ManualHistoryListRequest manualHistoryListRequest) {
        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, manualHistoryListRequest,
                "/admin/manual/history/list", new ParameterizedTypeReference<>() {
                });
    }

    @PostMapping("/history/getDetails.json")
    @ResponseBody
    public ResponseObject<ManualHistoryDetailResponse> historyDetail(
        @RequestBody @Valid final ManualHistoryDetailRequest manualHistoryDetailRequest) {
        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, manualHistoryDetailRequest,
                "/admin/manual/history/detail", new ParameterizedTypeReference<>() {
                });
    }

    @PostMapping("/setWorks.json")
    @ResponseBody
    public ResponseObject<ManualResponse> set(
        @RequestBody @Valid final ManualSetRequest manualSetRequest) {
        return manualSupportService.set(manualSetRequest);
    }

    @PostMapping("/editWorks.json")
    @ResponseBody
    public ResponseObject<ManualResponse> edit(
        @ModelAttribute("data") final ManualEditRequest manualEditRequest,
        @ModelAttribute("file") final MultipartFile file) {
        return manualSupportService.edit(manualEditRequest, file);
    }

    @PostMapping("/cancelWorks.json")
    @ResponseBody
    public ResponseObject<ManualResponse> cancel(
        @RequestBody @Valid final ManualCancelRequest manualCancelRequest) {
        return manualSupportService.cancel(manualCancelRequest);
    }

    @GetMapping("/isHeadOffice.json")
    @ResponseBody
    public ResponseObject<String> isHeadOffice() {
        String isHeadOffice = String.valueOf(manualSupportService.isHeadOffice());
        return ResourceConverter.toResponseObject(isHeadOffice);
    }

    @PostMapping("/extractExcel.json")
    @ResponseBody
    public UploadResult extractExcel(
        @RequestParam("excels") final MultipartFile multipartFile, HttpServletResponse res)
        throws IOException {
        return manualSupportService.extractExcel(multipartFile, res);
    }
}
