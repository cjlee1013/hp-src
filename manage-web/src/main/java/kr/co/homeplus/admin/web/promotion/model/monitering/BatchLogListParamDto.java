package kr.co.homeplus.admin.web.promotion.model.monitering;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("배치로그 조회 조건")
public class BatchLogListParamDto {
    private String schStartDt;

    private String schEndDt;

    private String schUseYn;

    private String schType;

    private String schValue;
}