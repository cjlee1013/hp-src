package kr.co.homeplus.admin.web.item.model.categoryGroup;

import java.util.List;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CategoryGroupSetParamDto {

	private Integer gcateCd;

	private String gcateNm;

	private Integer priority;

	private String useYn;

	private List<CategoryGroupImgGetDto> imgList;

	private List<CategoryGroupLcateGetDto> lcateList;

	private String regId;

}
