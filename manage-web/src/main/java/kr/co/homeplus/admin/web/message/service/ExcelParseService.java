package kr.co.homeplus.admin.web.message.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.message.model.BasicRecipient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 기존 엑셀 리더는 수동 발송에 적용하기 힘들어 새로 구현
 */
@Service
@Slf4j
public class ExcelParseService {

    private static final String NECESSARY_FIELD1 = "identifier";

    private static final String NECESSARY_FIELD2 = "toToken";

    public List<BasicRecipient> read(MultipartFile multipartFile) {
        if (multipartFile == null || multipartFile.isEmpty()) {
            return null;
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Workbook xssfWorkbook = new XSSFWorkbook(inputStream);
            Sheet sheet = xssfWorkbook.getSheetAt(0);
            int maxRow = sheet.getPhysicalNumberOfRows();

            if (maxRow <= 1) {
                //내용이 없거나 헤더밖에 없는경우
                return null;
            }
            //첫 행은 header가 된다. 이 필수 header를 제외한 나머지는 매핑의 key가 된다.
            int nsField1 = 0;
            int nsField2 = 0;
            Map<Integer, String> fields = new HashMap<>();

            Row headers = sheet.getRow(0);
            for (int i = 0; i < headers.getPhysicalNumberOfCells(); i++) {
                headers.getCell(i).setCellType(CellType.STRING);
                String header = headers.getCell(i).getStringCellValue();
                if (header.equalsIgnoreCase(NECESSARY_FIELD1)) {
                    nsField1 = i;
                } else if (header.equalsIgnoreCase(NECESSARY_FIELD2)) {
                    nsField2 = i;
                } else {
                    fields.put(i, header);
                }
            }

            List<BasicRecipient> list = new ArrayList<>();
            for (int i = 1; i < maxRow; i++) {
                Row row = sheet.getRow(i);
                BasicRecipient recipient = getRows(row, nsField1, nsField2, fields);
                if (recipient != null) {
                    list.add(recipient);
                }
            }

            log.debug("excel parse recipients size : {}", list.size());
            return list;
        } catch (Exception e) {
            log.error("excel parse exception", e);
            return null;
        }
    }

    private BasicRecipient getRows(Row row, int nsField1, int nsField2,
        Map<Integer, String> fields) {
        BasicRecipient recipient = new BasicRecipient();
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            row.getCell(i).setCellType(CellType.STRING);
            String data = row.getCell(i).getStringCellValue();
            if (!StringUtils.isEmpty(data)) {
                if (nsField1 == i) {
                    recipient.setIdentifier(data);
                } else if (nsField2 == i) {
                    recipient.setToToken(data);
                } else {
                    recipient.setMappingData(fields.get(i), data);
                }
            } else {
                return null;
            }
        }
        return recipient;
    }
}
