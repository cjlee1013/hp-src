package kr.co.homeplus.admin.web.settle.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "제휴 정산대사 차이내역")
public class MarketCompareListGetDto {
  @ApiModelProperty(notes = "구매확정일")
  @RealGridColumnInfo(headText = "구매확정일", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "입금일")
  @RealGridColumnInfo(headText = "입금일", width = 100)
  private String payScheduleDt;

  @ApiModelProperty(notes = "제휴사")
  @RealGridColumnInfo(headText = "제휴사", width = 100)
  private String siteType;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 150)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "제휴사 주문번호")
  @RealGridColumnInfo(headText = "제휴사 주문번호", width = 150)
  private String marketOrderNo;

  @ApiModelProperty(notes = "상품번호")
  @RealGridColumnInfo(headText = "상품번호", width = 100)
  private String itemNo;

  @ApiModelProperty(notes = "주문구분")
  @RealGridColumnInfo(headText = "주문구분", width = 100)
  private String gubun;

  @ApiModelProperty(notes = "차이구분")
  @RealGridColumnInfo(headText = "차이구분", width = 100)
  private String diffType;

  @ApiModelProperty(notes = "홈플러스")
  @RealGridColumnInfo(headText = "홈플러스", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String homeAmt;

  @ApiModelProperty(notes = "제휴사")
  @RealGridColumnInfo(headText = "제휴사", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketAmt;

  @ApiModelProperty(notes = "차액")
  @RealGridColumnInfo(headText = "차액", sortable = true, width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diffAmt;
}
