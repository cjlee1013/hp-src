package kr.co.homeplus.admin.web.manage.model.banner;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannerSetParamDto {

    private Long bannerNo;
    private String siteType;
    private String device;
    private String loc1Depth;
    private int loc2Depth;
    private int loc3Depth;
    private String dispYn;
    private int priority = 0;
    private String dispStartDt;
    private String dispEndDt;
    private Character dispTimeYn;
    private String dispStartTime;
    private String dispEndTime;
    private String bannerNm;
    private String template;
    private String regId;

    private List<BannerLinkSetParamDto> linkList;

}
