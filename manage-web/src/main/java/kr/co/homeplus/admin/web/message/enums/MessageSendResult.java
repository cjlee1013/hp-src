package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;

/**
 * API RESPONSE로 제공 하는 Return Code 정의
 */
public enum MessageSendResult implements RealGridLookUpSupport {

    READY("요청 단계"),
    ERROR("발송 실패"),
    SENDING("발송 진행"),
    CANCELED("발송 요청 취소"),
    COMPLETED("발송 완료"),
    ETC("기타");

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(MessageSendResult::name, MessageSendResult::getResultName));

    private final String resultName;

    MessageSendResult(final String resultName) {
        this.resultName = resultName;
    }

    public String getResultName() {
        return resultName;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
