package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemCategoryListGetDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemExcelDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemGetDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemListGetDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletListGetDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class DspLeafletController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;
    private final ExcelUploadService excelUploadService;

    /**
     * 사이트관리 > 전문관관리 > 마트전단 전시 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspLeaflet/dspLeafletMain", method = RequestMethod.GET)
    public String dspLeafletMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"            // 사용여부
            , "dsk_site_gubun"  // 사이트 구분
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("dskSiteGubun", code.get("dsk_site_gubun"));

        model.addAllAttributes(RealGridHelper.create("dspLeafletGridBaseInfo", DspLeafletListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dspLeafletItemCategoryListGridBaseInfo", DspLeafletItemCategoryListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("dspLeafletItemListGridBaseInfo", DspLeafletItemListGetDto.class));

        return "/manage/dspLeafletMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 마트전단 전시 관리 리스트 조회 ( 그리드 )
     * @param dspLeafletListParamDto
     * @return DspPopupPartnerListSelectDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspLeaflet/getDspLeafletList.json"}, method = RequestMethod.GET)
    public List<DspLeafletListGetDto> getDspLeafletList(@Valid DspLeafletListParamDto dspLeafletListParamDto) {
        String apiUri = "/manage/dspLeaflet/getDspLeafletList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspLeafletListGetDto>>>() {};
        return (List<DspLeafletListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspLeafletListParamDto.class, dspLeafletListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 마트전단 전시 관리 카테고리 리스트
     * @param leafletNo
     * @return DspLeafletItemCategoryListGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspLeaflet/getDspLeafletItemCategoryList.json"}, method = RequestMethod.GET)
    public List<DspLeafletItemCategoryListGetDto> getDspLeafletItemCategoryList(@RequestParam(name = "leafletNo", defaultValue = "0") Long leafletNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("leafletNo", leafletNo));

        String apiUriRequest = "/manage/dspLeaflet/getDspLeafletItemCategoryList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspLeafletItemCategoryListGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 마트전단 전시 관리 카테고리별 상품 리스트
     * @param leafletNo
     * @return DspLeafletItemCategoryListGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspLeaflet/getDspLeafletItemList.json"}, method = RequestMethod.GET)
    public List<DspLeafletItemListGetDto> getDspLeafletItemList(
        @RequestParam(name = "leafletNo", defaultValue = "0") Long leafletNo
        , @RequestParam(name = "cateNo", defaultValue = "0") Long cateNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("leafletNo", leafletNo));
        getParameterList.add(SetParameter.create("cateNo", cateNo));

        String apiUriRequest = "/manage/dspLeaflet/getDspLeafletItemList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspLeafletItemListGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 마트전단 전시 관리 등록/수정
     *
     * @param dspLeafletSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspLeaflet/setDspLeaflet.json"}, method = RequestMethod.POST)
    public ResponseResult setDspLeaflet(@RequestBody DspLeafletSetParamDto dspLeafletSetParamDto) {
        String apiUri = "/manage/dspLeaflet/setDspLeaflet";
        dspLeafletSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspLeafletSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 마트전단 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadLeafletItemPop")
    public String uploadLeafletItemPop(Model model, @RequestParam(value = "callback") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        return "/manage/pop/uploadLeafletItemPop";
    }

    /**
     * 마트전단 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/dspLeaflet/leafletItemExcel.json")
    public List<DspLeafletItemGetDto> leafletItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<DspLeafletItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품번호", "itemNo", true)
            .header(1, "사용여부", "useYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<DspLeafletItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(DspLeafletItemExcelDto.class, headers, 4, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        List<String> itemNoList = excelData.stream().map(DspLeafletItemExcelDto::getItemNo).distinct().collect(Collectors.toList());

        // 일괄등록 요청상품 itemNo로 검증
        String apiUri = "/manage/dspLeaflet/getDspLeafletItemListByExcel";
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemNoList, apiUri, new ParameterizedTypeReference<ResponseObject<List<DspLeafletItemGetDto>>>() {}).getData();
    }

}
