package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.search.model.DeployHistoryGetParam;
import kr.co.homeplus.admin.web.search.model.DeploySetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeployService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getServerList(String schServiceCd, String appType) {
        String apiUri = "/deploy/getServerList/" + schServiceCd + "?appType=" + appType;
        return getResponseObject(apiUri);
    }

    public ResponseObject makeDictionaryFromTxt(String schServiceCd, String deployDictType) {
        String apiUri = "/deploy/makeDictionaryFromTxt/" + schServiceCd + "?deployDictType=" + deployDictType + "&deployer=" + loginCookieService.getUserInfo().getEmpId();
        return getResponseObject(apiUri);
    }

    public ResponseObject uploadDictionary(String schServiceCd, String deployDictType) {
        String apiUri = "/deploy/uploadDictionary/" + schServiceCd + "?deployDictType=" + deployDictType;
        return getResponseObject(apiUri);
    }

    public ResponseObject deployDictionary(Long deployId, Long rowId, String deployDictType) {
        String apiUri = "/deploy/process/" + deployId + "?rowId=" + rowId + "&deployDictType=" + deployDictType + "&deployer=" + loginCookieService.getUserInfo().getEmpId();
        return getResponseObject(apiUri);
    }

    public ResponseObject setItem(DeploySetParamDto param) {
        String apiUri = "/deploy/setItem";
        return getResponseObject(apiUri, param);
    }

    public ResponseObject searchAnalyzerReload(String domain, String schServiceCd) {
        String apiUri = "/deploy/searchAnalyzerReload/" + schServiceCd + "/" + domain;
        return getResponseObject(apiUri);
    }

    public ResponseObject searchHistory(DeployHistoryGetParam param) {
        String apiUri = StringUtil.getRequestString("/deploy/history", DeployHistoryGetParam.class, param);
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri) {
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
                .apiId(ResourceRouteName.SEARCHMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {
                })
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements) {
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.SEARCHMNG)
                .uri(apiUri)
                .postObject(elements)
                .typeReference(new ParameterizedTypeReference<>() {
                })
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
