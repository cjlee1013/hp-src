//package kr.co.homeplus.admin.web.pg.controller;
//
//import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.COMMON_CARD_BIN_KIND;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
//import kr.co.homeplus.admin.web.common.service.CodeService;
//import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
//import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
//import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
//import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
//import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
//import kr.co.homeplus.admin.web.pg.model.CardBinManageDto;
//import kr.co.homeplus.admin.web.pg.model.CardBinManageSelectDto;
//import kr.co.homeplus.admin.web.pg.service.CardBinManageService;
//import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//@Controller
//@Slf4j
//@RequestMapping("/pg/paymentMethod")
//public class CardBinManageController {
//
//    private final LoginCookieService loginCookieService;
//    private final CardBinManageService cardBinManageService;
//    private final CodeService codeService;
//
//    private static final List<RealGridColumn> CARD_BIN_MANAGE_GRID_HEAD = new ArrayList();
//    private static final RealGridOption CARD_BIN_MANAGE_GRID_OPTION = new RealGridOption("fill", false, false, false);
//
//    public CardBinManageController(LoginCookieService loginCookieService, CardBinManageService cardBinManageService,
//        CodeService codeService) {
//        this.loginCookieService = loginCookieService;
//        this.cardBinManageService = cardBinManageService;
//        this.codeService = codeService;
//
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("cardBinNoSeq", "cardBinNoSeq", "cardBinNoSeq", RealGridColumnType.NUMBER, 0, false, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("cardBinNo", "cardBinNo", "카드BIN번호", RealGridColumnType.NUMBER, 70, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("cardBinKind", "cardBinKind", "카드구분", RealGridColumnType.BASIC, 120, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("methodCd", "methodCd", "카드사", RealGridColumnType.BASIC, 100, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("cardNm", "cardNm", "카드명", RealGridColumnType.BASIC, 200, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("useYn", "useYn", "사용여부", RealGridColumnType.BASIC, 80, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("platform", "regId", "등록자", RealGridColumnType.BASIC, 90, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("sortSeq", "regDt", "등록일", RealGridColumnType.DATETIME, 90, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("chgId", "chgId", "수정자", RealGridColumnType.BASIC, 90, true, true));
//        CARD_BIN_MANAGE_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.DATETIME, 90, true, true));
//    }
//
//
//    /**
//     * 카드BIN관리 메인 화면
//     * @param model
//     * @return
//     * @throws Exception
//     */
//    @GetMapping("/cardBinManageMain")
//    public String cardBinManageMain(Model model) throws Exception {
//        // pgMidUsePurpose 리스트 가져와서 model 에 담기
//        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
//            COMMON_CARD_BIN_KIND
//        );
//
//        model.addAttribute("cardBinManageGridBaseInfo", new RealGridBaseInfo("cardBinManageGridBaseInfo",CARD_BIN_MANAGE_GRID_HEAD, CARD_BIN_MANAGE_GRID_OPTION).toString());
//        model.addAttribute("paymentMethodList", cardBinManageService.getDistinctMethodCd());
//        model.addAttribute("commCardBinKindList", code.get(COMMON_CARD_BIN_KIND));
//
//        return "/pg/cardBinManageMain";
//    }
//
//    /**
//     * 카드BIN 리스트 조회
//     * @param cardBinManageSelectDto
//     * @return
//     * @throws Exception
//     */
//    @ResponseBody
//    @GetMapping("/getCardBinManageList.json")
//    public List<CardBinManageDto> getCardBinManageList(CardBinManageSelectDto cardBinManageSelectDto) throws Exception {
//
//        return cardBinManageService.getCardBinManageList(cardBinManageSelectDto);
//
//    }
//
//    /**
//     * 카드BIN저장 / 수정
//     * @param cardBinManageDto
//     * @return
//     * @throws Exception
//     */
//    @ResponseBody
//    @PostMapping("/saveCardBinManage.json")
//    public ResponseObject<Object> saveCardBinManage(@RequestBody CardBinManageDto cardBinManageDto) throws Exception {
//        String userCd = loginCookieService.getUserInfo().getUserCd();
//        cardBinManageDto.setRegId(userCd);
//        cardBinManageDto.setChgId(userCd);
//
//        return cardBinManageService.saveCardBinManage(cardBinManageDto);
//    }
//
//
//
//}
