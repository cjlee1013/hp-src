package kr.co.homeplus.admin.web.delivery.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipHistoryGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllExcelSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.utils.ShippigUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipManageService {

  private final ResourceClient resourceClient;
  private final ExcelUploadService excelUploadService;
  private final ShipCommonService shipCommonService;

  /**
   * 배송 관리 리스트 조회
   */
  public List<ShipManageListGetDto> getShipManageList(ShipManageListSetDto shipManageListSetDto) {

    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipManageListSetDto.setSchStoreId(shipCommonService.getStoreId());
      shipManageListSetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageListSetDto,
            "/admin/shipManage/getShipManageList",
            new ParameterizedTypeReference<ResponseObject<List<ShipManageListGetDto>>>(){}).getData();
  }

  /**
   * 주문확인
   */
  public ResponseObject<Object> setConfirmOrder(ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto) throws Exception {
    shipManageConfirmOrderSetDto.setChgId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageConfirmOrderSetDto,
            "/admin/shipManage/setConfirmOrder",
            new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 발송지연안내
   */
  public ResponseObject<Object> setNotiDelay(ShipManageNotiDelaySetDto shipManageNotiDelaySetDto) throws Exception {
    shipManageNotiDelaySetDto.setChgId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageNotiDelaySetDto,
            "/admin/shipManage/setNotiDelay",
            new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 일괄발송처리
   */
  public ResponseObject<Map<String, Object>> setShipAllExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
      List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList = new ArrayList<>();
      ResponseObject<Map<String, Object>> responseObject = new ResponseObject<>();
      Map<String, Object> dataMap = new HashMap<>();

      // 엑셀 파일에서 데이터 추출
      this.setShipAllDataFromExcel(multipartHttpServletRequest, shipManageShipAllExcelSetDtoList, dataMap);

      // 엑셀 추출 중 에러
      if ("-1".equals(dataMap.get("excelSuccessCnt").toString())) {
        responseObject.setReturnCode("FAIL");
        responseObject.setReturnMessage("파일 형식이 잘못되었습니다");
        return responseObject;
      }

      // 검증 완료 데이터 없음, 종료
      if ("0".equals(dataMap.get("excelSuccessCnt").toString())) {
        dataMap.put("failList", shipManageShipAllExcelSetDtoList);
        dataMap.put("successCnt", "0");

        responseObject.setReturnCode("SUCCESS");
        responseObject.setData(dataMap);

        return responseObject;
      }

      // 등록ID SET
      shipManageShipAllExcelSetDtoList.forEach(x -> x.setChgId(shipCommonService.getUserCd()));

      // 엑셀 데이터 업데이트
      responseObject = resourceClient.postForResponseObject(
                        ResourceRouteName.SHIPPING,
                        shipManageShipAllExcelSetDtoList,
                        "/admin/shipManage/setShipAllExcel",
                        new ParameterizedTypeReference<ResponseObject<Map<String, Object>>>(){});

      // API 처리 에러
      if (!"SUCCESS".equals(responseObject.getReturnCode())) {
        return responseObject;
      }

      // 실패 리스트 추출, 엑셀 export용
      ObjectMapper mapper = new ObjectMapper();
      shipManageShipAllExcelSetDtoList = mapper.convertValue(responseObject.getData().get("resultList"), new TypeReference<List<ShipManageShipAllExcelSetDto>>() {})
                                            .stream()
                                            .filter(x -> ObjectUtils.isNotEmpty(x.getFailMsg()))
                                            .collect(Collectors.toList());

      // RETURN DATA
      dataMap.put("failList", shipManageShipAllExcelSetDtoList);
      dataMap.put("successCnt", responseObject.getData().get("successCnt"));
      responseObject.setData(dataMap);

      return responseObject;
  }

  /**
   * 선택발송처리 팝업 리스트 조회
   */
  public List<ShipManageShipAllPopListGetDto> getShipAllPopList(ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto) {
    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageShipAllPopListSetDto,
            "/admin/shipManage/getShipAllPopList",
            new ParameterizedTypeReference<ResponseObject<List<ShipManageShipAllPopListGetDto>>>(){}).getData();
  }

  /**
   * 선택발송처리
   */
  public ResponseObject<Object> setShipAll(ShipManageShipAllSetDto shipManageShipAllSetDto) throws Exception {
    shipManageShipAllSetDto.setChgId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageShipAllSetDto,
            "/admin/shipManage/setShipAll",
            new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 배송완료
   */
  public ResponseObject<Object> setShipComplete(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) throws Exception {
    shipManageShipCompleteSetDto.setChgId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipManageShipCompleteSetDto,
            "/admin/shipManage/setShipComplete",
            new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 배송완료 팝업 리스트 조회
   */
  public List<ShipManageShipCompletePopListGetDto> getShipCompletePopList(ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        shipManageShipCompleteSetDto,
        "/admin/shipManage/getShipCompletePopList",
        new ParameterizedTypeReference<ResponseObject<List<ShipManageShipCompletePopListGetDto>>>(){}).getData();
  }

  /**
   * 배송 히스토리 조회
   */
  public List<ShipHistoryGetDto> getShipHistory(String dlvCd, String invoiceNo) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/admin/shipManage/getShipHistoryList?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo,
        new ParameterizedTypeReference<ResponseObject<List<ShipHistoryGetDto>>>() {}).getData();
  }

  /**
   * 일괄발송처리 엑셀 업로드
   */
  private void setShipAllDataFromExcel(MultipartHttpServletRequest multipartHttpServletRequest
                                  , List<ShipManageShipAllExcelSetDto> shipManageShipAllExcelSetDtoList
                                  , Map<String, Object> dataMap) {
    try {
      // 파일을 읽기위해 input type file 태그 name 작성
      MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");

      // 파라미터 추출
      String shipMethod = multipartHttpServletRequest.getParameter("shipMethod");
      String dlvCd = multipartHttpServletRequest.getParameter("dlvCd");

      ExcelHeaders headers;

      if (ShippigUtil.isDlv(shipMethod)) {
        headers = new ExcelHeaders.Builder()
            .header(0, "배송번호", "bundleNo", true)
            .header(1, "송장번호", "invoiceNo", true)
            .build();
      } else if(ShippigUtil.isDrct(shipMethod)){
        headers = new ExcelHeaders.Builder()
            .header(0, "배송번호", "bundleNo", true)
            .header(1, "배송예정일", "scheduleShipDt", true)
            .build();
      } else {
        throw new Exception("배송방법 에러 : " + shipMethod);
      }

      // 엑셀 메타정보 입력
      ExcelUploadOption<ShipManageShipAllExcelSetDto> excelUploadOption = new ExcelUploadOption.Builder<>(
          ShipManageShipAllExcelSetDto.class, headers, 1, 0).build();

      // 엑셀 조회. multipartFile과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력.
      List<ShipManageShipAllExcelSetDto> result = excelUploadService
          .readExcelFile(multipartFile, excelUploadOption);
      if (result.isEmpty()) {
        dataMap.put("excelTotalCnt", -1);
        dataMap.put("excelSuccessCnt", -1);
        dataMap.put("excelErrorCnt", -1);

        return;
      }

      // 데이터 검증 결과 data를 array에 담기
      int totalCnt = result.size();
      int successCnt = 0;
      int errorCnt = 0;

      for (ShipManageShipAllExcelSetDto shipManageShipAllExcelSetDto : result) {
        if (this.validExcel(shipManageShipAllExcelSetDto, shipMethod, dlvCd)) {
          successCnt++;
        } else {
          errorCnt++;
        }

        shipManageShipAllExcelSetDtoList.add(shipManageShipAllExcelSetDto);
      }

      dataMap.put("excelTotalCnt", totalCnt);
      dataMap.put("excelSuccessCnt", successCnt);
      dataMap.put("excelErrorCnt", errorCnt);
    } catch (Exception e) {
      log.error("일괄발송처리 엑셀 데이터 추출 에러 : {}", ExceptionUtils.getStackTrace(e));
      dataMap.put("excelTotalCnt", -1);
      dataMap.put("excelSuccessCnt", -1);
      dataMap.put("excelErrorCnt", -1);
    }
  }

  /**
   * 일괄발송처리 데이터 검증
   */
  private boolean validExcel(ShipManageShipAllExcelSetDto shipManageShipAllExcelSetDto, String shipMethod, String dlvCd) {

    /* 공통 */
    String bundleNo = shipManageShipAllExcelSetDto.getBundleNo();

    // 배송방법 SET
    shipManageShipAllExcelSetDto.setShipMethod(shipMethod);

    // 배송번호 - NULL, 공백, 숫자
    if (!StringUtils.isNumeric(bundleNo)) {
      shipManageShipAllExcelSetDto.setFailMsg("배송번호를 확인하세요");
      return false;
    }

    /* 택배배송 */
    if (ShippigUtil.isDlv(shipMethod)) {
      String invoiceNo = shipManageShipAllExcelSetDto.getInvoiceNo().trim();

      // 송장번호 - NULL, 공백, 30자이상, 영문+숫자조합
      if (StringUtils.isEmpty(invoiceNo)
          || ShippigUtil.spaceCheck(invoiceNo)
          || invoiceNo.length() > 30
          || !invoiceNo.matches("^[a-zA-Z0-9]*$") ) {
        shipManageShipAllExcelSetDto.setFailMsg("송장번호를 확인하세요");
        return false;
      }

      // 택배사 - NULL
      if (StringUtils.isEmpty(dlvCd)) {
        shipManageShipAllExcelSetDto.setFailMsg("택배사를 확인하세요");
        return false;
      }

      // 검증된 데이터 SET
      shipManageShipAllExcelSetDto.setDlvCd(dlvCd);
      shipManageShipAllExcelSetDto.setInvoiceNo(invoiceNo);
    }

    /* 직접배송 */
    if (ShippigUtil.isDrct(shipMethod)) {

      String scheduleShipDt = shipManageShipAllExcelSetDto.getScheduleShipDt();
      String scheduleShipDtRestFormat = "";

      // 엑셀 추출시 현재 공통모듈에서 yyyy-MM-dd HH:mm:ss 로 포맷 변경하고 있어 yyyy-mm-dd 까지 분리한 후 확인
      // 확인 후 형식이 잘못 됐으면 다시 합쳐놓는다. (처리 결과 엑셀 추출 시 원본데이터 유지해서 제공)
      if (scheduleShipDt.length() > 10) {
        scheduleShipDtRestFormat = scheduleShipDt.substring(10);
        scheduleShipDt = scheduleShipDt.substring(0,10);
      }

      // 배송예정일 - NULL, 공백, yyyy-mm-dd 형식
      if (StringUtils.isEmpty(scheduleShipDt)
          || ShippigUtil.spaceCheck(scheduleShipDt)
          || !scheduleShipDt.matches("\\d{4}-\\d{2}-\\d{2}")) {

        shipManageShipAllExcelSetDto.setScheduleShipDt(scheduleShipDt + scheduleShipDtRestFormat);
        shipManageShipAllExcelSetDto.setFailMsg("배송예정일을 확인하세요");
        return false;
      }

      // 검증된 데이터 SET
      shipManageShipAllExcelSetDto.setScheduleShipDt(scheduleShipDt);
    }

    return true;
  }

  /**
   * 배송정보출력 조회
   */
  public List<ShipInfoPrintGetDto> getShipInfoPrintList(String bundleNo) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/admin/shipManage/getShipInfoPrintList?bundleNo=" + bundleNo,
        new ParameterizedTypeReference<ResponseObject<List<ShipInfoPrintGetDto>>>() {}).getData();
  }

}
