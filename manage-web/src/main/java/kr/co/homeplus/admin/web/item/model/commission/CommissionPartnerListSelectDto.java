package kr.co.homeplus.admin.web.item.model.commission;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class CommissionPartnerListSelectDto {

    @RealGridColumnInfo(headText = "판매자명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String partnerId;

    @RealGridColumnInfo(headText = "판매자ID", order = 1)
    private String partnerNm;

    @RealGridColumnInfo(headText = "대표자명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, order = 2)
    private String partnerOwner;

}
