package kr.co.homeplus.admin.web.search.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.search.model.StatisticsUnknownMorphologyGetParamDto;
import kr.co.homeplus.admin.web.search.model.StatisticsUnknownMorphologyGridDto;
import kr.co.homeplus.admin.web.search.model.StatisticsUnknownMorphologySetDto;
import kr.co.homeplus.admin.web.search.service.StatisticsUnknownMorphologyService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/search/statistics/unkownMorphology")
@RequiredArgsConstructor
public class StatisticsUnknownMorphologyController {
    private final StatisticsUnknownMorphologyService statisticsUnknownMorphologyService;

    @GetMapping(value = "/main")
    public String Main(Model model) {
        model.addAllAttributes(RealGridHelper.create("unknownMorphologyGridBaseInfo", StatisticsUnknownMorphologyGridDto.UnknownMorphology.class));
        return "/search/statisticsUnknownMorphology";
    }

    @ResponseBody
    @PostMapping(value = {"/search.json"})
    @SuppressWarnings("unchecked")
    public ResponseObject<List<StatisticsUnknownMorphologySetDto>> getUnknownMorphologyList(@RequestBody @Valid StatisticsUnknownMorphologyGetParamDto param) {
        return (ResponseObject<List<StatisticsUnknownMorphologySetDto>>)statisticsUnknownMorphologyService.getUnknownMorphologyList(param);
    }
}
