package kr.co.homeplus.admin.web.item.controller;


import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.AttributeGroupListSelectDto;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.AttributeItemListSelectDto;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.AttributeListSelectDto;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.AttributeSetParamDto;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.CategoryListParamDto;
import kr.co.homeplus.admin.web.item.model.category.categoryAttribute.CategoryListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/attribute")
public class CategoryAttributeController {

    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;

    public CategoryAttributeController(ResourceClient resourceClient,
                                        LoginCookieService loginCookieService,
                                       CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 상품속성관리 > 상품속성관리 > 카테고리별 속성관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/categoryAttributeMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"        // 사용여부
                ,	"resist_yn"    // 등록 노출 여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("resistYn", code.get("resist_yn"));

        model.addAllAttributes(RealGridHelper.create("categoryListGridBaseInfo", CategoryListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("attributeListGridBaseInfo", AttributeListSelectDto.class));
        return "/item/categoryAttributeMain";
    }

    /**
     * 상품속성관리 > 카테고리별 속성관리 > 분류그룹리스트
     * @return AttributeGroupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getAttributeGroupList.json", method = RequestMethod.GET)
    public List<AttributeGroupListSelectDto> getAttributeGroupList() throws Exception {

        String apiUri = "/item/attribute/getAttributeGroupList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AttributeGroupListSelectDto>>>() {};
        return (List<AttributeGroupListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }

    /**
     * 상품속성관리 > 상품속성관리 > 카테고리별 리스트
     *
     * @param listParamDto
     * @return BrandListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getCategoryList.json"}, method = RequestMethod.GET)
    public List<CategoryListSelectDto> getCategoryList(CategoryListParamDto listParamDto) {

        String apiUri = "/item/attribute/getCategoryList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CategoryListSelectDto>>>() {};
        return (List<CategoryListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, CategoryListParamDto.class, listParamDto), typeReference).getData();
    }
    /**
     * 상품속성관리 > 상품속성관리 > 카테고리별 리스트
     * @param scateCd
     * @return BrandListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getAttributeList.json"}, method = RequestMethod.GET)
    public List<AttributeListSelectDto> getAttributeList(String scateCd) {

        String apiUri = "/item/attribute/getAttributeList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AttributeListSelectDto>>>() {};
        return (List<AttributeListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?scateCd=" + scateCd , typeReference).getData();
    }

    /**
     * 상품속성관리 > 상품속성관리 > 속성 등록/수정
     * @param attributeSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setAttribute.json"}, method = RequestMethod.POST)
    public ResponseResult setAttribute(AttributeSetParamDto attributeSetParamDto) throws Exception {

        String apiUri = "/item/attribute/setAttribute";
        attributeSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, attributeSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }

    @ResponseBody
    @RequestMapping(value = {"/getAttributeItemList.json"}, method = RequestMethod.GET)
    public List<AttributeItemListSelectDto> getAttributeList(
            @RequestParam String gattrType,
            @RequestParam(required = false, defaultValue = "") String itemNo,
            @RequestParam(required = false, defaultValue = "") String scateCd
    ) {

        String apiUri = "/item/attribute/getAttributeItemList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AttributeItemListSelectDto>>>() {};
        return (List<AttributeItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?gattrType=" + gattrType + "&itemNo=" + itemNo + "&scateCd=" + scateCd , typeReference).getData();
    }
}
