package kr.co.homeplus.admin.web.mileage.model.payment;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum MileageRequestCode {
    //mileage_common_code
    //마일리지 지급/회수 상태
    MILEAGE_REQUEST_DETAIL_STATUS_WAIT("mileage_detail_status", "D1", "대기"),
    MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE("mileage_detail_status","D3", "완료"),
    MILEAGE_REQUEST_DETAIL_STATUS_FAIL("mileage_detail_status","D4", "실패"),

    //마일리지 지급/회수 에러메시지
    MILEAGE_DETAIL_CODE_01("detail_code", "01","회원중복"),
    MILEAGE_DETAIL_CODE_02("detail_code", "02","회원없음"),
    MILEAGE_DETAIL_CODE_03("detail_code", "03","금액없음"),
    MILEAGE_DETAIL_CODE_04("detail_code", "04","1회지급가능액초과"),
    MILEAGE_DETAIL_CODE_05("detail_code", "04","1회회수가능액초과"),
    MILEAGE_DETAIL_CODE_06("detail_code", "05","회수금액부족");

    private String codeGroup;
    private String responseCode;
    private String responseMessage;
}