package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ApiModel(description = "수거완료 요청 파라미터")
public class ClaimShipCompleteSetDto {

    @NotNull(message = "번들번호")
    @ApiModelProperty(value = "번들번호", position = 1)
    private String bundleNo;

    @ApiModelProperty(value = "수정자 정보", position = 2)
    private String chgId;
}
