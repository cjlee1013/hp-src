package kr.co.homeplus.admin.web.promotion.model.coupon;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CouponIssueHistSearchSetDto {
    private String schType;
    private String schValue;
    private String histSchType;
    private String histSchValue;
    private String histSchStartDt;
    private String histSchEndDt;

    private long couponNo;
    private String schPageType;
    private String schAllYn;
}
