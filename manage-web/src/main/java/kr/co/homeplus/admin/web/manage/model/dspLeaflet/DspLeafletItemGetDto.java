package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletItemGetDto {

    private String itemNo;
    private String itemNm1;
    private String itemTypeNm;
    private String useYn;
    private String useYnTxt;
    private String chgNm;

}
