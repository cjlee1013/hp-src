package kr.co.homeplus.admin.web.user.model.consumer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.user.enums.ConsumerReasonType;
import kr.co.homeplus.admin.web.user.enums.ConsumerType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(Include.NON_NULL)
public class ConsumerAddParam {
    /** "회원번호" **/
    @NotNull(message = "회원번호가 없습니다.")
    private Long userNo;

    /**  "권한정지 범위유형 **/
    @NotNull(message = "권한정지범위를 선택해주세요.")
    private ConsumerType consumerType;

    public ConsumerType getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(String consumerType) {
        this.consumerType = ConsumerType.valueOf(consumerType);
    }

    /** 사유 유형 **/
    @NotNull(message = "사유를 선택해주세요.")
    private ConsumerReasonType reasonType;

    public ConsumerReasonType getReasonType() {
        return reasonType;
    }
    public void setReasonType(String reasonType) {
        this.reasonType = ConsumerReasonType.valueOf(reasonType);
    }

    /** 사유 상세 **/
    private String reasonDetail;

    /** 제한 시작일 **/
    private String startDt;

    /** 제한 종료일 **/
    private String endDt;

    /** 등록자 **/
    private String regId;

    /** 등록자명 **/
    private String regNm;
}
