package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true)
public class CouponApplyGetDto {
    @RealGridColumnInfo(headText = "상품번호", width = 80, sortable = true)
    private String applyCd;
    @RealGridColumnInfo(headText = "상품명", width = 300, columnType = RealGridColumnType.NAME, sortable = true)
    private String applyNm;
    @RealGridColumnInfo(headText = "CMS 정액/정률 구분", hidden = true, sortable = true)
    private String itemType;  //cms
    @RealGridColumnInfo(headText = "CMS 정액/정률의 값", hidden = true, sortable = true)
    private String couponValue; //cms
    private String differentCountYn; //cms 설정 상품수와 온라인 리핏의 상품이 다른 경우
    @RealGridColumnInfo(headText = "등록자", hidden = true, sortable = true)
    private String regNm;
    @RealGridColumnInfo(headText = "등록일", hidden = true, sortable = true)
    private String regDt;

    public CouponApplyGetDto (String applyCd, String applyNm, String itemType, String couponValue) {
        this.applyCd = applyCd;
        this.applyNm = applyNm;
        this.itemType = itemType;
        this.couponValue = couponValue;
    }
    public CouponApplyGetDto () {
    }
}