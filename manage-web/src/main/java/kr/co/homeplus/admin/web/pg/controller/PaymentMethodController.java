package kr.co.homeplus.admin.web.pg.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodSelectDto;
import kr.co.homeplus.admin.web.pg.service.PaymentMethodService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 결제관리 > 결제수단 관리 > 결제수단 관리
 */
@Controller
@RequestMapping("/pg/paymentMethod")
public class PaymentMethodController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final PaymentMethodService paymentMethodService;

    @Value("${plus.resource-routes.image.url}")
    private String apiImgUrl;

    public PaymentMethodController(LoginCookieService loginCookieService, AuthorityService authorityService, PaymentMethodService paymentMethodService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.paymentMethodService = paymentMethodService;
    }

    /**
     * 결제수단 관리 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/paymentMethodMain")
    public String paymentMethodMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_MNG)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("paymentMethodGridBaseInfo", PaymentMethodDto.class));
        model.addAttribute("parentMethodCdList", paymentMethodService.getPaymentTypeModelList());
        model.addAttribute("apiImgUrl", this.apiImgUrl);

        return "/pg/paymentMethodMain";
    }

    /**
     * 결제수단 조회
     * @param schParentMethodCd
     * @param schSiteType
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getPaymentMethodList.json")
    public List<PaymentMethodDto> getPaymentMethodList(@RequestParam(value = "schParentMethodCd") String schParentMethodCd,
                                                       @RequestParam(value = "schSiteType") String schSiteType) throws Exception {
        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setParentMethodCd(schParentMethodCd);
        paymentMethodSelectDto.setSiteType(schSiteType);
        return paymentMethodService.getPaymentMethod(paymentMethodSelectDto);
    }

    /**
     * 결제수단코드 중복체크
     * @param methodCd
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/checkDuplicateCode.json")
    public List<PaymentMethodDto> checkDuplicateCode(@RequestParam(value = "methodCd") String methodCd,
                                                     @RequestParam(value = "siteType") String siteType) throws Exception {
        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setMethodCd(methodCd);
        paymentMethodSelectDto.setSiteType(siteType);
        return paymentMethodService.checkDuplicateCode(paymentMethodSelectDto);
    }

    /**
     * 결제수단 저장
     * @param paymentMethodDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/savePaymentMethod.json")
    public ResponseObject<Object> savePaymentMethod(@RequestBody PaymentMethodDto paymentMethodDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        paymentMethodDto.setRegId(userCd);
        paymentMethodDto.setChgId(userCd);

        return paymentMethodService.savePaymentMethod(paymentMethodDto);
    }

    /**
     * 결제수단 Tree 구조 조회
     * @param siteType
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getPaymentMethodTree.json")
    public List<Object> getPaymentMethodTree(@RequestParam(value = "siteType") String siteType) throws Exception {
        PaymentMethodSelectDto paymentMethodSelectDto = new PaymentMethodSelectDto();
        paymentMethodSelectDto.setSiteType(siteType);
        return paymentMethodService.getPaymentMethodTree(paymentMethodSelectDto);
    }
}
