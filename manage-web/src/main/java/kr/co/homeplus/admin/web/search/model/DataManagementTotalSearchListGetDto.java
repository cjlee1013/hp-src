package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataManagementTotalSearchListGetDto {
    private Object query;
    private Admin admin;
    private long totalCount;
    private Search search;
    private List<Map<String, Object>> dataList;

    @Getter
    @Setter
    public static class Search {
        private String type;
        private String keyword;
        private String changeKeyword;
    }

    @Getter
    @Setter
    public static class Admin {
        private KeywordAnalyze keywordAnalyze;
        private List<SortCondition> sortCondition;

        @Getter
        @Setter
        public static class SortCondition {
            private String field;
            private String order;
        }

        @Getter
        @Setter
        public static class KeywordAnalyze {
            private String keyword;
            private List<Map<String,Object>> result;
            private String correctedType;
            private String correctedText;
            private String message;
            private List<CategoryRank> categoryRanks;
            private List<String> filtered;

/*            @Getter
            @Setter
            public static class Result {
                private String token;
                private String type;
                private int startOffset;
                private int endOffset;
                @JsonProperty("similarity_token")
                private List<Result> similarity_token;
            }*/

            @Getter
            @Setter
            public static class CategoryRank {
                private String token;
                private List<Rank> ranks;

                @Getter
                @Setter
                public static class Rank {
                    private String categoryId;
                    private String scorer;
                    private String boostType;
                    private boolean exclusive;
                }
            }
        }
    }
}
