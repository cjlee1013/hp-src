package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlimtalkWorkDetail extends AlimtalkWorkGrid {

    private String subject;
    private String body;
    private List<AlimtalkSendListGrid> sendList;
}
