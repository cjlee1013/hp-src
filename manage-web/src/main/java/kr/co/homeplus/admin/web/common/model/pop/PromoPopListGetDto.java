package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class PromoPopListGetDto {
    @RealGridColumnInfo(headText = "프로모션번호", sortable = true)
    private Long promoNo;

    @RealGridColumnInfo(headText = "관리프로모션명", width = 200)
    private String mngPromoNm;

    @RealGridColumnInfo(headText = "사이트구분")
    private String siteType;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeTypes;

    @RealGridColumnInfo(headText = "전시여부")
    private String dispYn;

    @RealGridColumnInfo(headText = "사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "전시시작일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170)
    private String dispEndDt;;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170)
    private String chgDt;
}