package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraThemeSetParamDto {

    private Long themeId;
    private String themeNm;
    private String themeSubNm;
    private String pcImgUrl;
    private String mobileImgUrl;
    private String useYn;
    private int priority;
    private String regId;

}
