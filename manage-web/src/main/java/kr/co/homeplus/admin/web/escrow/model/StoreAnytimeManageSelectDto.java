package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "점포 애니타임 관리 Select DTO")
@Data
public class StoreAnytimeManageSelectDto {
    @ApiModelProperty(value = "점포유형", position = 1)
    private String schStoreType;

    @ApiModelProperty(value = "검색유형", position = 2)
    private String schType;

    @ApiModelProperty(value = "검색어", position = 3)
    private String schKeyword;
}
