package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticsUnknownMorphologySetDto {
    private String keyword;
    private int frequency;
    private String usedDic;
    private String regDt;
}
