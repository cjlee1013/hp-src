package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineOrderDetailListGetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineOrderDetailListSetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineReceivePayListGetDto;
import kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay.DcOnlineReceivePayListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DcOnlineReceivePayService {

    private final ResourceClient resourceClient;

    /**
     * DC온라인수불 조회 리스트 조회
     */
    public List<DcOnlineReceivePayListGetDto> getDcOnlineReceivePayList(DcOnlineReceivePayListSetDto dcOnlineReceivePayListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            dcOnlineReceivePayListSetDto,
            "/admin/dcOnlineReceivePay/getDcOnlineReceivePayList",
            new ParameterizedTypeReference<ResponseObject<List<DcOnlineReceivePayListGetDto>>>(){}).getData();
    }

    /**
     * DC온라인수불 조회 주문상세 리스트 조회
     */
    public List<DcOnlineOrderDetailListGetDto> getDcOnlineOrderDetailList(DcOnlineOrderDetailListSetDto dcOnlineOrderDetailListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            dcOnlineOrderDetailListSetDto,
            "/admin/dcOnlineReceivePay/getDcOnlineOrderDetailList",
            new ParameterizedTypeReference<ResponseObject<List<DcOnlineOrderDetailListGetDto>>>(){}).getData();
    }
}
