package kr.co.homeplus.admin.web.item.model.category.categoryCommission;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryCommissionGetDto {
    private String lcateNm;
    private String mcateNm;
    private String scateNm;
    private String dcateNm;
    private String lcateCd;
    private String mcateCd;
    private String scateCd;
    private String dcateCd;
    private String useYn;
    private List<String> dispYn;
    private String commissionUseYn;
    private String commissionRate;
    private String regNm;
    private String regDt;
    private String chgNm;
    private String chgDt;

    private String businessNm;
    private String partnerId;

    private String strUseYn;

}
