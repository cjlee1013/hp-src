package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountApplySetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountCompareApplyScope;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountSearchSetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountUseGetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountUseInfoGetDto;
import kr.co.homeplus.admin.web.promotion.model.discount.DiscountUseSearchSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/discount")
public class DiscountController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    public DiscountController(ResourceClient resourceClient, LoginCookieService loginCookieService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 할인정보 저장 (카드상품할인)
     * @param discountDetailSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setDiscount.json")
    public DiscountCompareApplyScope setDiscount(@RequestBody DiscountDetailSetDto discountDetailSetDto) throws Exception {
        discountDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<DiscountCompareApplyScope> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , discountDetailSetDto
            , "/promotion/discount/setDiscount"
            , new ParameterizedTypeReference<ResponseObject<DiscountCompareApplyScope>>() {}
        );

        return responseObject.getData();
    }

    /**
     * 할인정보 조회 (카드상품할인)
     * @param discountSearchSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/getDiscountList.json")
    public List<DiscountDetailGetDto> getDiscountList(@ModelAttribute DiscountSearchSetDto discountSearchSetDto) throws Exception{
        ResponseObject<List<DiscountDetailGetDto>> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , discountSearchSetDto
            , "/promotion/discount/getDiscountList"
            , new ParameterizedTypeReference<ResponseObject<List<DiscountDetailGetDto>>>() {}
        );

        return responseObject.getData();
    }

    /**
     * 할인 상세정보 조회 (카드상품할인)
     * @param discountSearchSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/getDiscountDetail.json")
    public DiscountDetailGetDto getDiscountDetail(@ModelAttribute DiscountSearchSetDto discountSearchSetDto) throws Exception{
        ResponseObject<DiscountDetailGetDto> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , discountSearchSetDto
            , "/promotion/discount/getDiscountDetail"
            , new ParameterizedTypeReference<ResponseObject<DiscountDetailGetDto>>() {}
        );

        return responseObject.getData();
    }

    /**
     * 카드상품할인관리
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/cardDiscount")
    public String cardDiscount(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"                   //점포구분
            , "coupon_add_system"            //등록시스템
            , "coupon_discount_type"         //할인방법
            , "use_yn"                       //사용여부
            , "share_yn"                     //분담설정 여부
            , "coupon_purpose"               //행사목적
            , "disp_yn"                      //행사가 노출 여부
        );


        model.addAttribute("storeType",  code.get("store_type"));
        model.addAttribute("cardDiscountType", code.get("coupon_discount_type"));
        model.addAttribute("cardDiscountShareYn", code.get("share_yn"));
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("couponPurpose", code.get("coupon_purpose"));
        model.addAttribute("displayPromoAmtYn", code.get("disp_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("cardDiscountTypeJson", om.writeValueAsString(code.get("coupon_discount_type")));
        model.addAttribute("cardDiscountShareYnJson", om.writeValueAsString(code.get("share_yn")));
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));

        model.addAllAttributes(RealGridHelper.create("cardDiscountGridBaseInfo", DiscountDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.create("cardDiscountApplyGridBaseInfo", DiscountApplySetDto.class));

        return "/promotion/cardDiscount";
    }

    /**
     * 할인 사용 조회
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/discountHistory")
    public String discountHistory(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "discount_kind"		        //할인종류
            , "store_type"                   //점포구분
            , "use_yn"                       //사용여부
        );

        model.addAttribute("storeType", code.get("store_type"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("discountKindJson", om.writeValueAsString(code.get("discount_kind")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));

        model.addAllAttributes(RealGridHelper.create("discountHistoryDiscountGridBaseInfo", DiscountDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.create("discountHistoryGridBaseInfo", DiscountUseGetDto.class));

        return "/promotion/discountHistory";
    }

    /**
     * 할인 사용 조회 > 할인 사용 상세 내역 조회
     * @param discountUseSearchSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/getDiscountUseInfoList.json")
    public DiscountUseInfoGetDto getDiscountUseInfoList(@ModelAttribute DiscountUseSearchSetDto discountUseSearchSetDto) throws Exception {
        DiscountUseInfoGetDto discountUseInfoGetDto = new DiscountUseInfoGetDto();

        try {
            List<DiscountUseGetDto> useInfoList
                = resourceClient.postForResponseObject(
                ResourceRouteName.ESCROWMNG,
                discountUseSearchSetDto,
                "/external/discount/getDiscountUseInfoList",
                new ParameterizedTypeReference<ResponseObject<List<DiscountUseGetDto>>>() {
                }
            ).getData();

            for (DiscountUseGetDto dto : useInfoList) {
                dto.setUserNo(PrivacyMaskingUtils.maskingUserId(dto.getUserNo()));
            }

            discountUseInfoGetDto.setDiscountUseList(useInfoList);
            discountUseInfoGetDto.setDiscountCount(useInfoList.size());
            discountUseInfoGetDto.setSumDiscountAmt(useInfoList.stream().mapToLong(x -> x.getDiscountAmt()).sum());

        } catch (Exception e) {
            throw new Exception("통신에 실패 하였습니다.");
        }

        return discountUseInfoGetDto;
    }
}
