package kr.co.homeplus.admin.web.message.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.model.BasicRecipient;
import kr.co.homeplus.admin.web.message.model.ManualCancelRequest;
import kr.co.homeplus.admin.web.message.model.ManualDirectRequest;
import kr.co.homeplus.admin.web.message.model.ManualEditRequest;
import kr.co.homeplus.admin.web.message.model.ManualResponse;
import kr.co.homeplus.admin.web.message.model.ManualSetRequest;
import kr.co.homeplus.admin.web.message.model.ReturnCode;
import kr.co.homeplus.admin.web.message.model.UploadResult;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class ManualSupportService {

    private static final int MAX_SEND_LIST = 100_000;

    private final ResourceClient resourceClient;

    private final ExcelParseService excelParseService;

    private final ExcelUploadService excelUploadService;

    private final LoginCookieService loginCookieService;

    public ManualSupportService(
        ResourceClient resourceClient,
        ExcelParseService excelParseService,
        ExcelUploadService excelUploadService,
        LoginCookieService loginCookieService) {
        this.resourceClient = resourceClient;
        this.excelParseService = excelParseService;
        this.excelUploadService = excelUploadService;
        this.loginCookieService = loginCookieService;
    }

    public ResponseObject<ManualResponse> edit(final ManualEditRequest request,
        final MultipartFile file) {
        request.setChgId(getEmpId());
        request.setBodyArgument(excelParseService.read(file));
        String url = "/manual/update/" + request.getWorkType().getWorkType();

        ResourceClientRequest<ManualResponse> resourceClientRequest =
            ResourceClientRequest.<ManualResponse>postBuilder()
                .apiId(ResourceRouteName.MESSAGE)
                .uri(url)
                .postObject(request)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(resourceClientRequest,
            new TimeoutConfig(5000, 5000, 1000 * 60 * 5)).getBody();
    }

    public ResponseObject<ManualResponse> set(final ManualSetRequest request) {
        request.setRegId(getEmpId());
        boolean isDirect = StringUtils.isEmpty(request.getTemplateCode());
        boolean isAlimtalk = (request.getWorkType() == WorkType.ALIMTALK);
        String url = "/manual/send/" + request.getWorkType().getWorkType();

        ResourceClientRequest.Builder<ManualResponse> resourceClientRequestBuilder =
            ResourceClientRequest.<ManualResponse>postBuilder()
                .apiId(ResourceRouteName.MESSAGE);

        if (isDirect && isAlimtalk) {
            ManualResponse response = new ManualResponse();
            response.setManualSendSeq(-1L);
            response.setManualSendWorkSeq(-1L);
            response.setMessage("알림톡은 템플릿 기반 발송만 가능합니다.");
            response.setReturnCode(ReturnCode.ERROR);
            response.setSendCount(0);
            response.setWorkDt(request.getWorkDt());
            return ResourceConverter.toResponseObject(response);
        } else if (isDirect) {
            ManualDirectRequest manualDirectRequest = new ManualDirectRequest();
            manualDirectRequest.setBody(request.getContentBody());
            manualDirectRequest.setBodyArgument(request.getBodyArgument());
            manualDirectRequest.setCallback(request.getCallback());
            manualDirectRequest.setDelayYn(null);
            manualDirectRequest.setSubject(request.getContentSubject());
            manualDirectRequest.setWorkDt(request.getWorkDt());
            manualDirectRequest.setWorkType(request.getWorkType());
            manualDirectRequest.setWorkName(request.getCoupledWorkName());
            manualDirectRequest.setDescription(request.getDescription());
            manualDirectRequest.setRegId(request.getRegId());

            ResourceClientRequest<ManualResponse> resourceClientRequest = resourceClientRequestBuilder
                .uri(url + "/direct").postObject(manualDirectRequest).typeReference(new ParameterizedTypeReference<>() {}).build();

            ResponseEntity<ResponseObject<ManualResponse>> responseEntity = resourceClient
                .post(resourceClientRequest, new TimeoutConfig(5000, 5000, 1000 * 60 * 5));

            return responseEntity.getBody();
        } else {
            ResourceClientRequest<ManualResponse> resourceClientRequest = resourceClientRequestBuilder
                .uri(url).postObject(request).typeReference(new ParameterizedTypeReference<>() {}).build();

            ResponseEntity<ResponseObject<ManualResponse>> responseEntity = resourceClient
                .post(resourceClientRequest, new TimeoutConfig(5000, 5000, 1000 * 60 * 5));

            return responseEntity.getBody();
        }
    }

    public ResponseObject<ManualResponse> cancel(final ManualCancelRequest request) {
        request.setChgId(getEmpId());
        String url = "/manual/cancel/" + request.getWorkType().getWorkType();

        ResourceClientRequest<ManualResponse> resourceClientRequest =
            ResourceClientRequest.<ManualResponse>postBuilder()
                .apiId(ResourceRouteName.MESSAGE)
                .uri(url)
                .postObject(request)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient
            .post(resourceClientRequest, new TimeoutConfig(5000, 5000, 1000 * 60 * 5)).getBody();
    }

    /**
     * 메일 업로드 관련자는 모두 제거한다 (정책) 핸드폰 번호 형태만 걸러 내려준다.
     *
     * @param excel 엑셀파일
     * @return List BasicRecipient
     */
    public UploadResult extractExcel(final MultipartFile excel, HttpServletResponse res)
        throws IOException {
        ExcelHeaders excelHeaders = new ExcelHeaders.Builder()
            .header(0, BasicRecipient.IDENTIFIER, "identifier")
            .header(1, BasicRecipient.TO_TOKEN, "toToken")
            .build();

        ExcelUploadOption<BasicRecipient> excelUploadOption = new ExcelUploadOption.Builder<>(
            BasicRecipient.class, excelHeaders, 1, 0).build();

        try {
            return doFilter(excelUploadService.readExcelFile(excel, excelUploadOption));
        } catch (Exception e) {
            log.error("excel send list extract error", e);
            UploadResult uploadResult = new UploadResult();
            uploadResult.setList(new ArrayList<>());
            uploadResult.setMessage(e.getLocalizedMessage());
            return uploadResult;
        }
    }

    private UploadResult doFilter(List<BasicRecipient> rawList) {
        if(rawList.size() > MAX_SEND_LIST) {
            UploadResult uploadResult = new UploadResult();
            uploadResult.setList(new ArrayList<>());
            uploadResult.setMessage("대상자는 " + MAX_SEND_LIST + "건 미만이어야 합니다.");
            return uploadResult;
        }

        final Pattern phonePattern = Pattern.compile("^(\\d{3})-?(\\d{3,4})-?(\\d{4})$");

        int total = rawList.size();
        boolean matchFirst = false;
        if (total > 0) {
            BasicRecipient firstRecipient = rawList.get(0);
            matchFirst = phonePattern.matcher(firstRecipient.getToToken()).matches();
        }

        List<BasicRecipient> resultList = new ArrayList<>();

        int filtered = 0;

        boolean isMixedPattern = false;
        for (BasicRecipient recipient : rawList) {
            if (StringUtils.isEmpty(recipient.getToToken()) ||
                StringUtils.isEmpty(recipient.getIdentifier())) {
                filtered++;
            } else {
                boolean matchNext = phonePattern.matcher(recipient.getToToken()).matches();

                if (matchFirst == matchNext) {
                    resultList.add(recipient);
                } else {
                    isMixedPattern = true;
                    break;
                }
            }
        }

        UploadResult uploadResult = new UploadResult();
        if (isMixedPattern) {
            uploadResult.setMessage("연락처 종류가 섞여 있습니다.");
        } else {
            log.debug("total : {}, filtered : {}, result : {}", total, filtered, total - filtered);
            uploadResult.setList(resultList);
        }
        return uploadResult;
    }

    public boolean isHeadOffice() {
        return StringUtils.isEmpty(loginCookieService.getUserInfo().getStoreId());
    }

    public String getEmpId() {
        return loginCookieService.getUserInfo().getEmpId();
    }
}
