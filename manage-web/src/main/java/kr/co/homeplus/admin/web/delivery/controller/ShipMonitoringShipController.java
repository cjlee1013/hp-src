package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringShipGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringShipSetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipMonitoringShipService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송모니터링 > 배송모니터링
 */
@Controller
@RequestMapping("/ship")
public class ShipMonitoringShipController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    @Autowired
    private ShipMonitoringShipService shipMonitoringShipService;
    @Autowired
    private ShipCommonService shipCommonService;

    /**
     * 배송모니터링 Main Page
     */
    @GetMapping("/shipMonitoring/shipMonitoringShipMain")
    public String shipMonitoringShipMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAllAttributes(RealGridHelper.create("shipMonitoringShipGridBaseInfo", ShipMonitoringShipGetDto.class));

        return "/delivery/shipMonitoringShipMain";
    }

    /**
     * 배송모니터링 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipMonitoring/getShipMonitoringShipList.json", method = RequestMethod.GET)
    public List<ShipMonitoringShipGetDto> getShipMonitoringShipList(ShipMonitoringShipSetDto shipMonitoringShipSetDto) {
        return shipMonitoringShipService.getShipMonitoringShipList(shipMonitoringShipSetDto);
    }
}
