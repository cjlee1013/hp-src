package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.claim.model.ClaimPreRefundSetDto.ClaimPreRefundItemList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "클레임 조회 요청 DTO")
public class ClaimOrderSetDto {

    @ApiModelProperty(value = "클레임타입", position = 1)
    private String schClaimType;

    @ApiModelProperty(value = "처리상태", position = 2)
    private String schClaimStatus;

    @ApiModelProperty(value = "수거처리상태", position = 3)
    private String schPickupStatus;

    @ApiModelProperty(value = "마켓연동", position = 4)
    private String partnerCd;

    @ApiModelProperty(value = "클레임 검색기간", position = 5)
    private String claimDateType;

    @ApiModelProperty(value = "조회시작일", position = 6)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 7)
    private String schEndDt;

    @ApiModelProperty(value = "회원/비회원 여부", position = 8)
    private String schUserYn;

    @ApiModelProperty(value = "회원번호", position = 9)
    private String schUserSearch;

    @ApiModelProperty(value = "비회원번호 검색조건", position = 10)
    private String schBuyerInfo;

    @ApiModelProperty(value = "판매업체ID", position = 11)
    private String schPartnerId;

    @ApiModelProperty(value = "판매업체명", position = 12)
    private String schPartnerName;

    @ApiModelProperty(value = "검색어 타입", position = 13)
    private String claimSearchType;

    @ApiModelProperty(value = "검색어", position = 14)
    private String schClaimSearch;

    @ApiModelProperty(value = "신청채널", position = 15)
    private String schClaimChannel;

    @ApiModelProperty(value = "점포유형", position = 16)
    private String storeType;

    @ApiModelProperty(value = "배송처리상태", position = 17)
    private String schExchStatus;

    @ApiModelProperty(value = "점포코드", position = 18)
    private String storeId;

    @ApiModelProperty(value = "배송유형", position = 19)
    private String schReserveShipMethod;
}
