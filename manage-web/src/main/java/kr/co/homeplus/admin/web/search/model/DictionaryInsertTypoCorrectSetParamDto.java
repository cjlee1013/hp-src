package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DictionaryInsertTypoCorrectSetParamDto {
    @ApiModelProperty(notes = "오탈자정보", position = 1)
    private String correctedTerms;
}
