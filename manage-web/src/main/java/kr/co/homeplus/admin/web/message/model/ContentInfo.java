package kr.co.homeplus.admin.web.message.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.validation.constraints.Size;
import kr.co.homeplus.admin.web.message.enums.AffiliationType;
import kr.co.homeplus.admin.web.message.enums.StoreType;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ContentInfo {

    @ApiModelProperty(value = "row id")
    private long contentSeq;

    @ApiModelProperty(value = "제목")
    @Size(max = 100)
    private String name;

    @ApiModelProperty(value = "설명")
    @Size(max = 400)
    private String description;

    @ApiModelProperty(value = "점포유형 (HYPER/CLUB)")
    private StoreType storeType;

    @ApiModelProperty(value = "발송주체 (HEAD/STORE)")
    private AffiliationType affiliationType;

    @ApiModelProperty(value = "채널 타입")
    private WorkType workType;

    @ApiModelProperty(value = "발신자 정보")
    @Size(max = 100)
    private String callback;

    @ApiModelProperty(value = "컨텐츠 제목")
    @Size(max = 100)
    private String contentSubject;

    @ApiModelProperty(value = "컨텐츠 내용")
    @Size(max = 16_777_215)
    private String contentBody;

    @ApiModelProperty(value = "알림톡 전용 템플릿 코드 번호")
    @Size(max = 30)
    private String templateCode;

    @ApiModelProperty(value = "템플릿 발송 차단 여부")
    private String blockYn;

    @ApiModelProperty(value = "템플릿 발송 차단 시작 시간")
    private LocalTime blockStartTime;

    @ApiModelProperty(value = "템플릿 발송 차단 종료 시간")
    private LocalTime blockEndTime;

    @ApiModelProperty(value = "기타컬럼 1")
    @Size(max = 100)
    private String etc1;

    @ApiModelProperty(value = "기타컬럼 2")
    @Size(max = 100)
    private String etc2;

    @ApiModelProperty(value = "기타컬럼 3")
    @Size(max = 100)
    private String etc3;

    @ApiModelProperty(value = "기타컬럼 4")
    @Size(max = 100)
    private String etc4;

    @ApiModelProperty(value = "기타컬럼 5")
    @Size(max = 100)
    private String etc5;

    @ApiModelProperty(value = "첨부 컬럼")
    @Size(max = 4000)
    private String attachment;

    @ApiModelProperty(value = "등록자")
    @Size(max = 30)
    private String regId;

    @ApiModelProperty(value = "등록일시")
    private LocalDateTime regDt;

    @ApiModelProperty(value = "수정자")
    @Size(max = 30)
    private String chgId;

    @ApiModelProperty(value = "수정일시")
    private LocalDateTime chgDt;
}
