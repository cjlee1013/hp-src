package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.CounselStatisticsGetDto;
import kr.co.homeplus.admin.web.statistics.model.CounselStatisticsSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CounselStatisticService {

    private final ResourceClient resourceClient;

    /**
     * 상담통계 메인조회
     * @param
     * @return
     */
    public List<CounselStatisticsGetDto> getCounselStatisticsList(
        CounselStatisticsSetDto counselStatisticsSetDto) {

        return resourceClient.postForResponseObject(
            ResourceRouteName.USERMNG,
            counselStatisticsSetDto,
            "/admin/callCenter/getCounselStatisticsList",
            new ParameterizedTypeReference<ResponseObject<List<CounselStatisticsGetDto>>>(){}).getData();
    }
}
