package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailyDivisionEKPIListGetDto;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailyDivisionEKPIListSetDto;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailySettleMarketItemGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DailyDivisionEKPIService {

    private final ResourceClient resourceClient;

    /**
     * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
     */
    public List<DailyDivisionEKPIListGetDto> getDailyDivisionEKPIList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailyDivisionEKPI/getDailyDivisionEKPIList",
            new ParameterizedTypeReference<ResponseObject<List<DailyDivisionEKPIListGetDto>>>(){}).getData();
    }

    /**
     * 통계 > 마켓통계 > 기간별 마켓상품 판매현황 SETTLE-699 SETTLE-904
     */
    public List<DailySettleMarketItemGetDto> getDailySettleMarketItemList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            listParamDto,
            "/admin/dailyDivisionEKPI/getDailySettleMarketItemList",
            new ParameterizedTypeReference<ResponseObject<List<DailySettleMarketItemGetDto>>>(){}).getData();
    }
}
