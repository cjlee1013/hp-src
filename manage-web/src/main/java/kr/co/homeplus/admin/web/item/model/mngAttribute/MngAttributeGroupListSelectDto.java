package kr.co.homeplus.admin.web.item.model.mngAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class MngAttributeGroupListSelectDto {
    @RealGridColumnInfo(headText = "분류번호", sortable = true, width = 70, columnType = RealGridColumnType.NUMBER, fieldType = RealGridFieldType.NUMBER)
    private long gattrNo;

    @RealGridColumnInfo(headText = "노출분류명", sortable = true, width = 190)
    private String gattrNm;

    @RealGridColumnInfo(headText = "관리분류명", sortable = true, width = 190, hidden = true)
    private String gattrMngNm;

    @RealGridColumnInfo(headText = "구분코드", hidden = true)
    private String gattrType;

    @RealGridColumnInfo(headText = "구분", sortable = true, width = 70)
    private String gattrTypeTxt;

    @RealGridColumnInfo(headText = "선택방식코드", hidden = true)
    private String multiYn;

    @RealGridColumnInfo(headText = "선택방식", sortable = true, width = 70)
    private String multiYnTxt;

    @RealGridColumnInfo(headText = "사용여부코드", hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 70)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "노출유형", hidden = true)
    private String dispType;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}
