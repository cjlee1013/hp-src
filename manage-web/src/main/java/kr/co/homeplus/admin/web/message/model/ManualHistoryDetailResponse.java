package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class ManualHistoryDetailResponse {

    private long manualSendSeq;

    private long manualSendWorkSeq;

    private String workType;

    private String coupledWorkSeq;

    private String coupledWorkName;

    private String subject;

    private String body;

    private String callback;

    private String description;

    private String workStatus;

    private String delayYn;

    private String workDt;

    private String endDt;

    private String regId;

    private String regDt;

    private String chgId;

    private String chgDt;

    private String delYn;
}
