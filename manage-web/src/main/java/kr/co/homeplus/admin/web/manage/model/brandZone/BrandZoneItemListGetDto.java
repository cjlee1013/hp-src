package kr.co.homeplus.admin.web.manage.model.brandZone;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandZoneItemListGetDto{

    @ApiModelProperty(value = "브랜드존 관리번호")
    @RealGridColumnInfo(headText = "브랜드존 관리번호" , hidden = true)
    private Long brandzoneNo;

    @ApiModelProperty(value = "우선순위", position = 1)
    @RealGridColumnInfo(headText = "우선순위", width = 100, sortable = true)
    private String priority;

    @ApiModelProperty(value = "상품번호", position = 3 )
    @RealGridColumnInfo(headText = "상품번호", width = 100)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 4)
    @RealGridColumnInfo(headText = "상품명", width = 150)
    private String itemNm;

    @ApiModelProperty(value = "사용")
    @RealGridColumnInfo(headText = "사용", width = 100, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 100)
    private String useYnNm;

    @ApiModelProperty(value = "브랜드명")
    @RealGridColumnInfo(headText = "브랜드명", width = 100)
    private String brandNm;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 100)
    private String partnerNm;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 100)
    private String partnerId;

    @ApiModelProperty(value = "대분류" )
    @RealGridColumnInfo(headText = "대분류", width = 80)
    private String lcateNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류" , width = 80)
    private String mcateNm;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", width = 80)
    private String scateNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", width = 80)
    private String dcateNm;

    @RealGridColumnInfo(headText = "등록자", sortable = true)
    private String regNm;
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", sortable = true)
    private String chgNm;
    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 170, sortable = true)
    private String chgDt;
}
