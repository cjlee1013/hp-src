package kr.co.homeplus.admin.web.statistics.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.SettleMallType;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailyDivisionEKPIListGetDto;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailyDivisionEKPIListSetDto;
import kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI.DailySettleMarketItemGetDto;
import kr.co.homeplus.admin.web.statistics.service.DailyDivisionEKPIService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/dailyDivisionEKPI")
public class DailyDivisionEKPIController {

    private final DailyDivisionEKPIService dailyDivisionEKPIService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public DailyDivisionEKPIController(DailyDivisionEKPIService dailyDivisionEKPIService, SettleCommonService settleCommonService, CodeService codeService) {
        this.dailyDivisionEKPIService = dailyDivisionEKPIService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dailyDivisionEKPIList", method = RequestMethod.GET)
    public String dailyPaymentList(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mallType", Arrays.asList(SettleMallType.values()));
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("dailyDivisionEKPIListBaseInfo", DailyDivisionEKPIListGetDto.class));

        return "/statistics/dailyDivisionEKPIList";
    }

    /**
     * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
     */
    @ResponseBody
    @RequestMapping(value = "/dailyDivisionEKPIList.json", method = RequestMethod.GET)
    public List<DailyDivisionEKPIListGetDto> getDailyDivisionEKPIList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return dailyDivisionEKPIService.getDailyDivisionEKPIList(listParamDto);
    }

    /**
     * 통계 > 마켓통계 > 기간별 마켓상품 판매현황 SETTLE-699 SETTLE-904
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dailySettleMarketItemList", method = RequestMethod.GET)
    public String dailyMarketSettleList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("dailySettleMarketItemBaseInfo", DailySettleMarketItemGetDto.class));

        return "/statistics/dailySettleMarketItem";
    }

    /**
     * 통계 > 마켓통계 > 기간별 마켓상품 판매현황 SETTLE-699 SETTLE-904
     */
    @ResponseBody
    @RequestMapping(value = "/getDailySettleMarketItemList.json", method = RequestMethod.GET)
    public List<DailySettleMarketItemGetDto> getDailySettleMarketItemList(DailyDivisionEKPIListSetDto listParamDto) throws Exception {
        return dailyDivisionEKPIService.getDailySettleMarketItemList(listParamDto);
    }
}