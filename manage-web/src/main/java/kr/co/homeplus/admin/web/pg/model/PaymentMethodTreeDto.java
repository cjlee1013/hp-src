package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "결제수단 Tree DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentMethodTreeDto {
    @ApiModelProperty(notes = "결제수단명")
    @RealGridColumnInfo(headText = "methodName", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String methodName;

    @ApiModelProperty(notes = "결제수단코드")
    @RealGridColumnInfo(headText = "methodCode", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodCode;

    @ApiModelProperty(notes = "상위결제수단코드")
    @RealGridColumnInfo(headText = "parentCode", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentCode;

    @ApiModelProperty(notes = "TreeDepth")
    @RealGridColumnInfo(headText = "depth", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String depth;
}