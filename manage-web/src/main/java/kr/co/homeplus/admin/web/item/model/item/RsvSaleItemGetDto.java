package kr.co.homeplus.admin.web.item.model.item;

import java.time.LocalDateTime;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true   )
@Getter
@Setter
public class RsvSaleItemGetDto {

    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 250, columnType = RealGridColumnType.NAME, sortable = true)
    private String itemNm;

    @RealGridColumnInfo(headText = "발송지", width = 150, sortable = true)
    private String sendPlace;

    /*@RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME_ISO, width = 200, sortable = true)
    private LocalDateTime regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME_ISO, width = 200, sortable = true)
    private LocalDateTime chgDt;*/
}
