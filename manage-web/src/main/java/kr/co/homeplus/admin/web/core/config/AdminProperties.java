package kr.co.homeplus.admin.web.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * application.yml 설정을 바인딩한 프로퍼티<p>
 * {@link ConfigurationProperties} 사용으로 추후 spring cloud config 사용 가능하도록 변경.
 *
 * @see ConfigurationProperties
 */
@Data
@Component
@ConfigurationProperties(prefix = "admin.config")
public class AdminProperties {
    /**
     * 어드민 웹 scope : admin, 권한관리에 접근할 때 사용합니다.
     */
    private String scopeValue;

    /**
     * sso 로그인 후 시작 url
     */
    private String startUrl;

    /**
     * 인증 시 리다이렉트 되는 로그인 url
     */
    private String certificationRedirectUrl;

    /**
     * static 파일(.css, .js ...)의 파일버전
     */
    private String fileVersion;

    /**
     * static 파읠의 포맷설정
     */
    private String fileVersionFormat;

    /**
     * 로그인 만료시간
     */
    private int loginExpirationMin;

    /**
     * 로그아웃 url
     */
    private String logOutUrl;

    /**
     * 점검중 페이지 노출여부<p>
     *
     * 점검중 페이지가 노출 될 경우 내부페이지에 접근할 수 없습니다.<br>
     * true일 경우에는 노출, false면 노출하지 않음
     */
    private boolean maintenancePageDisplayEnable;
}
