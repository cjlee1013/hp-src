package kr.co.homeplus.admin.web.core.privacylog.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 개인정보 접근이력 전송결과
 */
@Setter
@Getter
public class FileStoreResult {
    private FileStoreInfo fileStoreInfo;
}
