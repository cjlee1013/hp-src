package kr.co.homeplus.admin.web.item.model.sameClass;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SameClassItemParamDto {


    private String itemNo;

    private Integer sameClassNo;
}
