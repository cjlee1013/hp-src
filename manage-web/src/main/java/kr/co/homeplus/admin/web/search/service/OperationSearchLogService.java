package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.admin.web.search.model.*;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class OperationSearchLogService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getList(OperationSearchLogBlackListGetParam param) {
        String apiUri = "/operation/getSearchLogBlackList";
        return getResponseObject(apiUri,param);
    }

    public ResponseObject registerBlackListIp(OperationSearchLogBlackListSetParam param) {
        param.setRegId(loginCookieService.getUserInfo().getEmpId());
        param.setChgId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/operation/registerBlackListIp";
        return getResponseObject(apiUri,param);
    }

    public ResponseObject modifyBlacklistStatus(OperationSearchLogBlackListSetParam param) {
        param.setChgId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/operation/modifyBlackListStatus";
        return getResponseObject(apiUri,param);
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.SEARCHMNG)
                .uri(apiUri)
                .postObject(elements)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
