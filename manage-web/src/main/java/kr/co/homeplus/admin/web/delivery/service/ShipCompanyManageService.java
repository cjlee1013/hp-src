package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipCompanyManageService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 택배사코드 관리 리스트 조회
   */
  public List<ShipCompanyManageListGetDto> getShipCompanyManageList(ShipCompanyManageListSetDto shipCompanyManageListSetDto) {
    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipCompanyManageListSetDto,
            "/admin/shipManage/getShipCompanyManageList",
            new ParameterizedTypeReference<ResponseObject<List<ShipCompanyManageListGetDto>>>(){}).getData();
  }

  /**
   * 택배사코드 관리 상세정보 저장
   */
  public ResponseObject<Object> setShipCompanyInfo(ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto) throws Exception {
    shipCompanyManageInfoSetDto.setRegId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipCompanyManageInfoSetDto,
            "/admin/shipManage/setShipCompanyInfo",
            new ParameterizedTypeReference<ResponseObject<Object>>() {});
  }

  /**
   * 연동업체코드 리스트 조회
   */
  public List<ShipLinkCompanyCodeListGetDto> getLinkCompanyCodeList(String company) {
    return resourceClient.getForResponseObject(
            ResourceRouteName.SHIPPING,
            "/admin/shipManage/getLinkCompanyCodeList?company=" +company,
            new ParameterizedTypeReference<ResponseObject<List<ShipLinkCompanyCodeListGetDto>>>() {}).getData();
  }

  /**
   * 연동업체설정 리스트 조회
   */
  public List<ShipLinkCompanySetListGetDto> getShipLinkCompanySetList() {
    return resourceClient.getForResponseObject(
            ResourceRouteName.SHIPPING,
            "/admin/shipManage/getShipLinkCompanySetList",
            new ParameterizedTypeReference<ResponseObject<List<ShipLinkCompanySetListGetDto>>>() {}).getData();
  }

  /**
   * 연동업체설정 저장
   */
  public ResponseObject<Object> setShipLinkCompanySet(ShipLinkCompanySetDto shipLinkCompanySetDto) throws Exception {
    shipLinkCompanySetDto.setRegId(shipCommonService.getUserCd());

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipLinkCompanySetDto,
            "/admin/shipManage/setShipLinkCompanySet",
            new ParameterizedTypeReference<ResponseObject<Object>>() {});
  }
}