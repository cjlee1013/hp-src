package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 TD정보 점포상품 기본정보 Set Entry
 */
@Getter
@Setter
public class ItemTDBasicSetDto {
	//유입채널 (등록/수정) : (ADMIN: 어드민, PARTNER: 파트너, INBOUND: 인바운드)
	private String channel;

	/**
	 * 기본정보
	 */
	//상품번호
	private String itemNo;

	//상품명
	private String itemNm;

	//상품유형 (B:반품(리세일), N:새상품, R:리퍼, U:중고)
	private String itemType;

	//판매자 상품코드
	private String sellerItemCd;

	//세카테고리
	private int    dcateCd;

	//거래유형 (DS : 업체상품 , TD : 매장상품)
	private String mallType;

	//노출여부
	private String dispYn;

	//배송속성 (직택배상품: DRCT_DLV, 직배전용상품: DRCT, 픽업전용상품: PICK, 퀵전용상품: QUICK)
	private String shipAttr;

	//등록/수정자
	private String userId;

	/**
	 * 판매정보
	 */
	//부가세 여부(Y:과세,N:비과세)
	private String taxYn;

	//점포ID
	private Integer storeId;

	//점포유형
	private String storeType;

	//장바구니 제한여부
	private String cartLimitYn;

	/**
	 * 이미지정보
	 */
	private List<ItemImgGetDto> imgList;

	/**
	 * 옵션정보
	 */
	//선택형 옵션단계
	private int    optTitleDepth;

	//선택형 옵션 1단계 타이틀
	private String opt1Title;

	//선택옵션사용여부 (사용:Y,미사용:N)
	private String optSelUseYn;

	//리스트 분할노출 여부
	private String listSplitDispYn;

	//옵션리스트
	List<ItemOptionValueGetDto> optList;

	/**
	 * 상품 이미지정보
	 */
	//상품상세정보
	private String itemDesc;

	//동영상정보
	private ItemVideoGetDto videoFile;

	/**
	 * 속성정보
	 */
	//브랜드번호
	private Long   brandNo;

	//제조사번호
	private Long   makerNo;

	//제조일자
	private String makeDt;

	//유효일자
	private String expDt;

	//프론트 노출여부
	private String unitDispYn;

	//원산지
	private String originTxt;

	/**
	 * 부가정보
	 */
	//속성번호 리스트
	private List<Integer> attrNoList;

	//성인상품유형 (NORMAL: 일반, ADULT: 성인, LOCAL_LIQUOR: 전통주)
	private String adultType;

	//픽업상품 여부 (Y: 픽업전용, N: 일반)
	private String pickYn;

	//이미지노출 여부 (Y:노출, N:노출안함)
	private String imgDispYn;

	//가격비교 등록여부
	private String epYn;

	//외부연동 사이트
	private String linkageYn;

	//검색 키워드
	private String srchKeyword;

	//선물하기사용여부 (Y: 사용, N: 사용안함)
	private String giftYn;

	//증빙서류 리스트
	private List<ItemProofListGetDto> proofFileList;

	//인증리스트
	private List<ItemCertGetDto> certList;

	//isbn
	private String isbn;

	//제휴사, 마켓연동 리스트
	private List<ItemProviderGetDto> providerList;
}
