package kr.co.homeplus.admin.web.search.service;

import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CrawlerTargetService {

    @Value("${plus.resource-routes.imageFront.url}")
    private String imageFrontDomainPrefix;

    private final ResourceClient resourceClient;

    public Map<String, Object> getCrawlerTarget(String itemNo, int storeId) {
        String apiUri = "/crawlerTarget/getCrawlerTarget?itemNo="+itemNo+"&storeId="+storeId;
        Map result = (Map)getResponseObject(apiUri).getData();
        result.put("imageFrontDomainPrefix", imageFrontDomainPrefix);
        return result;
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }
}
