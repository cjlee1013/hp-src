package kr.co.homeplus.admin.web.item.model.sameClass;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SameClassListSelectDto {

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "동류그룹번호")
    @RealGridColumnInfo(headText = "동류그룹번호", width = 0, hidden = true)
    private Integer sameClassNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNm1;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 0, hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 120, sortable = true)
    private String useYnTxt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 150)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 150)
    private String chgDt;

}
