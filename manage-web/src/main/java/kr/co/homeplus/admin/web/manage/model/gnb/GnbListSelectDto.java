package kr.co.homeplus.admin.web.manage.model.gnb;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class GnbListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Integer gnbNo;

    @RealGridColumnInfo(headText = "사이트구분", order = 1)
    private String siteTypeTxt;

    @RealGridColumnInfo(headText = "GNB명", order = 2)
    private String dispNm;

    @RealGridColumnInfo(headText = "전시 시작일", columnType = RealGridColumnType.DATE, width = 120, order = 3, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", columnType = RealGridColumnType.DATE, width = 120, order = 4, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "전시순서", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 5, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "디바이스", order = 6)
    private String device;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 7, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 8)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 9, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 10)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 11, sortable = true)
    private String chgDt;


    @RealGridColumnInfo(headText = "siteType", order = 12, hidden = true)
    private String siteType;
    @RealGridColumnInfo(headText = "linkType", order = 13, hidden = true)
    private String linkType;
    @RealGridColumnInfo(headText = "linkInfo", order = 14, hidden = true)
    private String linkInfo;
    @RealGridColumnInfo(headText = "useYn", order = 15, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "iconKind", order = 16, hidden = true)
    private String iconKind;

}
