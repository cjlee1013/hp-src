package kr.co.homeplus.admin.web.partner.model.affiliate;


import lombok.Data;

@Data
public class ChannelListParamDto {

    private String searchStartDt;
    private String searchEndDt;
    private String searchSiteType;
    private String searchUseYn;
    private String searchType;
    private String searchKeyword;

}
