package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaReplySetParamDto {

    private Integer qnaReplyNo;

    private Integer qnaNo;

    private String replyArea;

    private String qnaReply;

    private String displayStatus;

    private String regId;

}
