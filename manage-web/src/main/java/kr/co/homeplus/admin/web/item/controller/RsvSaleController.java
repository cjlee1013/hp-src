package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.item.RsvSaleGetDto;
import kr.co.homeplus.admin.web.item.model.item.RsvSaleItemGetDto;
import kr.co.homeplus.admin.web.item.model.item.RsvSaleListParamDto;
import kr.co.homeplus.admin.web.item.model.item.RsvSaleSendPlaceGetDto;
import kr.co.homeplus.admin.web.item.model.item.RsvSaleSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/rsvSale")
public class RsvSaleController {
    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    public RsvSaleController(ResourceClient resourceClient,
            LoginCookieService loginCookieService,
            CodeService codeService) throws Exception {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 배송관리 > 예약상품판매관리 메인
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/main")
    public String rsvItemSaleMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            	"use_yn"  // 사용여부
            ,	"store_type"     // 점포구분 - Hyper,Club,Exp,DS
            ,	"item_sale_type" // 판매유형
            ,	"send_place"     // 발송지
            ,	"sale_way"       // 판매방법
        );

        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("saleType", code.get("item_sale_type"));
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("sendPlace", code.get("send_place"));
        model.addAttribute("saleWay", code.get("sale_way"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("saleTypeJson", om.writeValueAsString(code.get("item_sale_type")));
        model.addAttribute("sendPlaceJson", om.writeValueAsString(code.get("send_place")));

        model.addAllAttributes(RealGridHelper.create("rsvSaleListGridBaseInfo", RsvSaleGetDto.class));
        model.addAllAttributes(RealGridHelper.create("itemListGridBaseInfo", RsvSaleItemGetDto.class));

        return "/item/rsvSaleMain";
    }

    /**
     * 배송관리 > 예약상품판매관리 > 리스트 조회
     * @param rsvSaleListParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getRsvSaleList.json")
    public List<RsvSaleGetDto> getRsvSaleList(RsvSaleListParamDto rsvSaleListParamDto) {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            StringUtil.getRequestString("/item/rsvSale/getRsvSaleList", RsvSaleListParamDto.class,
                rsvSaleListParamDto),
            new ParameterizedTypeReference<ResponseObject<List<RsvSaleGetDto>>>() { }).getData();
    }

    /**
     * 배송관리 > 예약상품판매관리 > 예약상품판매 단건 조회
     * @param rsvSaleSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getRsvSaleInfo.json")
    public RsvSaleGetDto getRsvSaleInfo(@RequestParam("rsvSaleSeq") long rsvSaleSeq) {
        String apiUri = "/item/rsvSale/getRsvSaleInfo?rsvSaleSeq=" + rsvSaleSeq;
        RsvSaleGetDto rsvSaleInfo = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<RsvSaleGetDto>>() {}).getData();

        return rsvSaleInfo;
    }

    /**
     *  배송관리 > 예약상품판매관리 > 대상 상품리스트 조회
     * @param rsvSaleSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getRsvSaleSendPlaceList.json")
    public List<RsvSaleSendPlaceGetDto> getRsvSaleSendPlaceList(@RequestParam("rsvSaleSeq") long rsvSaleSeq) {
        String apiUri = "/item/rsvSale/getRsvSaleSendPlaceList?rsvSaleSeq=" + rsvSaleSeq;
        List<RsvSaleSendPlaceGetDto> itemList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<List<RsvSaleSendPlaceGetDto>>>() {}).getData();

        return itemList;
    }

    /**
     *  배송관리 > 예약상품판매관리 > 대상 상품리스트 조회
     * @param rsvSaleSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getRsvSaleItemList.json")
    public List<RsvSaleItemGetDto> getRsvSaleItemList(@RequestParam("rsvSaleSeq") long rsvSaleSeq) {
        String apiUri = "/item/rsvSale/getRsvSaleItemList?rsvSaleSeq=" + rsvSaleSeq;
        List<RsvSaleItemGetDto> itemList = resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri,
            new ParameterizedTypeReference<ResponseObject<List<RsvSaleItemGetDto>>>() {}).getData();

        return itemList;
    }

    /**
     * 배송관리 > 예약상품판매관리 > 저장(입력/수정)
     * @param rsvSaleSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/setRsvSale.json")
    public Object setRsvSale(@RequestBody RsvSaleSetDto rsvSaleSetDto) {
        rsvSaleSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/rsvSale/setRsvSale";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, rsvSaleSetDto, apiUri, typeReference).getData();
    }
}
