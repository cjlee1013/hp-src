package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "일자별 SHIFT 중복체크 Model")
@Getter
@Setter
@EqualsAndHashCode
public class DailyShiftDuplicateCheckModel {
    @ApiModelProperty(value = "주문시작일시", position = 1)
    private String orderStartDt;

    @ApiModelProperty(value = "주문종료일시", position = 2)
    private String orderEndDt;

    @ApiModelProperty(value = "상품번호", position = 3)
    private String itemNo;

    @ApiModelProperty(value = "점포ID리스트", position = 4)
    private List<Integer> storeIdList;

    @ApiModelProperty(value = "일자별SHIFT관리순번", position = 5)
    private Integer dailyShiftMngSeq;
}
