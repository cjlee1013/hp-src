package kr.co.homeplus.admin.web.statistics.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 어드민 ESCROW 통계 팝업 컨트롤러
 */
@Controller
@RequestMapping("/statistics/popup")
public class EscrowStatisticsPopupController {
    /**
     * 실시간 점포 상품 판매 현황 도움말 팝업
     */
    @GetMapping("/realtimeStoreItemSaleStatisticsHelpPop")
    public String realtimeStoreItemSaleStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/realtimeStoreItemSaleStatisticsHelpPop";
    }

    /**
     * 실시간 판매업체 상품 판매 현황 도움말 팝업
     */
    @GetMapping("/realtimePartnerItemSaleStatisticsHelpPop")
    public String realtimePartnerItemSaleStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/realtimePartnerItemSaleStatisticsHelpPop";
    }

    /**
     * 기획전별 판매현황 도움말 팝업
     */
    @GetMapping("/exhibitionSaleStatisticsHelpPop")
    public String exhibitionSaleStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/exhibitionSaleStatisticsHelpPop";
    }

    /**
     * 실시간 점포별 판매 현황 도움말 팝업
     */
    @GetMapping("/realtimeStoreOrderStatisticsHelpPop")
    public String realtimeStoreOrderStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/realtimeStoreOrderStatisticsHelpPop";
    }

    /**
     * 실시간 판매업체별 판매 현황 도움말 팝업
     */
    @GetMapping("/realtimePartnerOrderStatisticsHelpPop")
    public String realtimePartnerOrderStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/realtimePartnerOrderStatisticsHelpPop";
    }

    /**
     * 지불 수단별 판매현황 도움말 팝업
     */
    @GetMapping("/paymentInfoExtractHelpPop")
    public String paymentInfoExtractHelpPop(Model model) throws Exception {
        return "/statistics/pop/paymentInfoExtractHelpPop";
    }

    /**
     * 온라인 픽업 주문 현황 도움말 팝업
     */
    @GetMapping("/pickupOrderExtractHelpPop")
    public String pickupOrderExtractHelpPop(Model model) throws Exception {
        return "/statistics/pop/pickupOrderExtractHelpPop";
    }

    /**
     * 우편번호별 주문현황 도움말 팝업
     */
    @GetMapping("/zipcodeOrderStatisticsHelpPop")
    public String zipcodeOrderStatisticsHelpPop(Model model) throws Exception {
        return "/statistics/pop/zipcodeOrderStatisticsHelpPop";
    }

    /**
     * 합배송 주문 현황 도움말 팝업
     */
    @GetMapping("/combineOrderStatusHelpPop")
    public String combineOrderStatusHelpPop(Model model) throws Exception {
        return "/statistics/pop/combineOrderStatusHelpPop";
    }
}
