package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountCompareApplyScope {
    private int discountNo;
    private List<String> applyCdList;
}
