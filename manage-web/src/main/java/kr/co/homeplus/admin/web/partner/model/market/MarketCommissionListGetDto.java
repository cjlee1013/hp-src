package kr.co.homeplus.admin.web.partner.model.market;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarketCommissionListGetDto {

    @ApiModelProperty(value = "수수료일련번호")
    @RealGridColumnInfo(headText = "수수료일련번호", hidden = true)
    private Long commissionSeq;

    @ApiModelProperty(value = "수수료율")
    @RealGridColumnInfo(headText = "수수료율", width = 100, columnType = RealGridColumnType.COMMISSION)
    private BigDecimal commissionRate;

    @ApiModelProperty(value = "적용시작일")
    @RealGridColumnInfo(headText = "적용시작일", width = 100, columnType = RealGridColumnType.DATE, sortable = true)
    private String applyStartDt;

    @ApiModelProperty(value = "적용종료일")
    @RealGridColumnInfo(headText = "적용종료일", width = 100, columnType = RealGridColumnType.DATE, sortable = true)
    private String applyEndDt;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "변경여부")
    @RealGridColumnInfo(headText = "변경여부", hidden = true)
    private String changeYn;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 100)
    private String chgDt;
}
