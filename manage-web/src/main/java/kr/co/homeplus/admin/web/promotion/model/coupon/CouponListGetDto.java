package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = false, indicator = true)
public class CouponListGetDto {
    @RealGridColumnInfo(headText = "쿠폰번호", width = 80, sortable = true)
    private Long couponNo;
    @RealGridColumnInfo(headText = "점포유형", width = 70, sortable = true)
    private String storeType;
    @RealGridColumnInfo(headText = "관리쿠폰명", width = 250, columnType = RealGridColumnType.NAME, sortable = true)
    private String manageCouponNm;
    @RealGridColumnInfo(headText = "쿠폰종류", width = 90, sortable = true)
    private String couponType;
    @RealGridColumnInfo(headText = "쿠폰상태", width = 60, sortable = true)
    private String status;
    @RealGridColumnInfo(headText = "할인액/율", width = 80, columnType = RealGridColumnType.NUMBER_STR,sortable = true)
    private String discount;
    @RealGridColumnInfo(headText = "회수여부", width = 60, sortable = true)
    private String withdrawYn;
    @RealGridColumnInfo(headText = "발급기간", width = 250, sortable = true)
    private String issuePeriod;
    @RealGridColumnInfo(headText = "적용기간", width = 250, sortable = true)
    private String validPeriod;
    @RealGridColumnInfo(headText = "최소구매금액", width = 80, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, sortable = true)
    private Integer purchaseMin;
    @RealGridColumnInfo(headText = "최대할인금액", width = 80, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, sortable = true)
    private Integer discountMax;
    @RealGridColumnInfo(headText = "적용시스템", width = 90, sortable = true)
    private String applySystem;
    @RealGridColumnInfo(headText = "등급쿠폰", width = 60, sortable = true)
    private String gradeCouponYn;
    @RealGridColumnInfo(headText = "행사목적", width = 90, sortable = true)
    private String purpose;
    @RealGridColumnInfo(headText = "분담여부", width = 70, sortable = true)
    private String shareYn;
    @RealGridColumnInfo(headText = "귀속부서", width = 90, sortable = true)
    private String departCd;

    private String issueLimitType;
    private String randomNoIssueYn;

    @RealGridColumnInfo(headText = "등록자", width = 70, sortable = true)
    private String regNm;
    @RealGridColumnInfo(headText = "등록일", width = 150, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, sortable = true)
    private String regDt;
    @RealGridColumnInfo(headText = "수정자", width = 70, sortable = true)
    private String chgNm;
    @RealGridColumnInfo(headText = "수정일", width = 150, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, sortable = true)
    private String chgDt;
}
