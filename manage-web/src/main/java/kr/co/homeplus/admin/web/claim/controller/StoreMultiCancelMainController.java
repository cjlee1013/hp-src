package kr.co.homeplus.admin.web.claim.controller;



import java.util.List;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.RequestStoreMultiCancelGetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.RequestStoreMultiCancelSetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.StoreMultiCancelListGetDto;
import kr.co.homeplus.admin.web.claim.model.storeMultiCancel.StoreMultiCancelListSetDto;
import kr.co.homeplus.admin.web.claim.service.ClaimMainService;
import kr.co.homeplus.admin.web.claim.service.StoreMultiCancelService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/storeMultiCancel")
@RequiredArgsConstructor
public class StoreMultiCancelMainController {

    private final StoreMultiCancelService storeMultiCancelService;
    private final ClaimMainService claimMainService;
    private final LoginCookieService loginCookieService;

    /**
     * 점포일괄취소 Main Page
     */
    @GetMapping("/multiCancelMain")
    public String multiCancelMain(Model model){
        model.addAllAttributes(RealGridHelper.create("storeMultiCancelListGridBaseInfo", StoreMultiCancelListGetDto.class));

        String storeId = loginCookieService.getUserInfo().getStoreId();

        if(StringUtils.isNotEmpty(storeId)){
            StoreInfoGetDto storeInfoGetDto = claimMainService.getStoreInfo(storeId);
            model.addAttribute("storeId", loginCookieService.getUserInfo().getStoreId());
            model.addAttribute("storeNm", storeInfoGetDto.getStoreNm());
            model.addAttribute("storeType", storeInfoGetDto.getStoreType());
        }
        return "/claim/storeMultiCancelMain";
    }


    /**
     * 점포일괄취소 리스트 조회
     */
    @PostMapping("/getStoreMultiCancelList.json")
    @ResponseBody
    public List<StoreMultiCancelListGetDto> getStoreMultiCancelList(@RequestBody StoreMultiCancelListSetDto storeMultiCancelListSetDto) throws Exception {
        return storeMultiCancelService.getStoreMultiCancelList(storeMultiCancelListSetDto);
    }

    /**
     * 점포 일괄취소 팝업창
     */
    @GetMapping("/popup/getStoreMultiCancelPop")
    public String getStoreMultiCancelPop(Model model, @RequestParam(value="callback") String callBackScript) throws Exception {
        model.addAttribute("callBackScript",callBackScript);
        return "/claim/pop/storeMultiCancelPop";
    }

    /**
     * 점포 일괄취소 요청
     */
    @PostMapping("/reqStoreMultiCancel.json")
    @ResponseBody
    public ResponseObject<RequestStoreMultiCancelGetDto> reqStoreMultiCancel(@RequestBody RequestStoreMultiCancelSetDto requestStoreMultiCancelSetDto) throws Exception {
        return storeMultiCancelService.reqStoreMultiCancel(requestStoreMultiCancelSetDto);
    }
}
