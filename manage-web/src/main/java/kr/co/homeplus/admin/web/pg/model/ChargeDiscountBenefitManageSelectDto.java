package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class ChargeDiscountBenefitManageSelectDto {

    private String schSiteType;
    private String schChargeDiscountNm;
    private String schApplyStartDt;
    private String schApplyEndDt;
    private String schUseYn;

}
