package kr.co.homeplus.admin.web.user.model.consumer;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.user.enums.ConsumerReasonType;
import kr.co.homeplus.admin.web.user.enums.ConsumerType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConsumerModifyParam {
    /** 일련번호 **/
    @NotNull(message = "블랙컨슈머 일련번호가 없습니다.")
    private Long seq;

    /** 회원번호 **/
    private Long userNo;

    /** 권한정지 범위유형(1:구매제한, 2:커뮤니티 이용제한, 3:블랙컨슈머) **/
    private ConsumerType consumerType;

    /** 사유 **/
    private ConsumerReasonType reasonType;

    /** 사유 상세 **/
    private String reasonDetail;

    /** 제한 시작일 **/
    private String startDt;

    /** 제한 종료일 **/
    private String endDt;

    /** 블랙컨슈머 등록해제여부(true:등록|false:해제) **/
    @JsonProperty("isRelease")
    private boolean isRelease;

    /** 블랙컨슈머 해제사유 **/
    private String releaseDetail;

    /** 블랙컨슈머 해제일자 **/
    private String releaseDt;

    /** 수정자 **/
    private String modId;

    /** 수정자명 **/
    private String modNm;
}
