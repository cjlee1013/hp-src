package kr.co.homeplus.admin.web.item.model.itemRelation;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class ItemRelationListGetDto {

    //연관번호
    @RealGridColumnInfo(headText = "연관번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String relationNo;

    //연관명
    @RealGridColumnInfo(headText = "연관명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, width = 150, sortable = true)
    private String relationNm;

    //상품개수
    @RealGridColumnInfo(headText = "상품개수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, sortable = true)
    private String itemCount;

    //판매자ID
    @RealGridColumnInfo(headText = "판매자ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerId;

    //노출여부
    @RealGridColumnInfo(headText = "노출여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String dispYn;

    //노출여부명
    @RealGridColumnInfo(headText = "노출여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String dispYnNm;

    //대분류 카테고리명
    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String lcateNm;

    //중분류 카테고리명
    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String mcateNm;

    //소분류 카테고리명
    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String scateNm;

    //소분류 카테고리
    @RealGridColumnInfo(headText = "소분류 카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String scateCd;

    //PC노출기준
    @RealGridColumnInfo(headText = "PC노출기준", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String dispPcType;

    //모바일노출기준
    @RealGridColumnInfo(headText = "모바일노출기준", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String dispMobileType;

    //인트로이미지URL
    @RealGridColumnInfo(headText = "인트로이미지URL", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String imgUrl;

    //인트로이미지명
    @RealGridColumnInfo(headText = "인트로이미지명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String imgNm;

    //사용여부
    @RealGridColumnInfo(headText = "사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String useYn;

    //사용여부명
    @RealGridColumnInfo(headText = "설정여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String useYnNm;

    //등록자
    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String regNm;

    //등록일
    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String regDt;

    //수정자
    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    //수정일
    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String chgDt;

}
