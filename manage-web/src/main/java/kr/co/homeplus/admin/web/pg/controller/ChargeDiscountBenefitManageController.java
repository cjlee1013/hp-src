package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ChargeDiscountBenefitManageDto;
import kr.co.homeplus.admin.web.pg.model.ChargeDiscountBenefitManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.pg.service.ChargeDiscountBenefitService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 결제관리 > 결제수단관리 > 청구할인혜택관리
 */
@Controller
@RequestMapping("/pg/paymentMethod")
public class ChargeDiscountBenefitManageController {

    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final ChargeDiscountBenefitService chargeDiscountBenefitService;

    public ChargeDiscountBenefitManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            ChargeDiscountBenefitService chargeDiscountBenefitService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.chargeDiscountBenefitService = chargeDiscountBenefitService;
    }

    /**
     * 청구할인 혜택 관리 메인 화면
     *
     * @param model
     * @return
     */
    @GetMapping("/chargeDiscountBenefitManageMain")
    public String chargeDiscountBenefitManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // 산용카드 결제수단 조회
        Map<String, List<PaymentMethodDto>> paymentMethodListMap = chargeDiscountBenefitService.getMethodCdBySiteType();

        List<PaymentMethodDto> hmpPaymentMethodList = paymentMethodListMap.get(SiteType.HOME.getCode());
//        [ESCROW-343, CLUB 노출제외]
//        List<PaymentMethodDto> clubPaymentMethodList = paymentMethodListMap.get(SiteType.CLUB.getCode());

        // 가져온 공통코드와 함께 model 에 담기
        model.addAttribute("siteTypeList", Arrays.asList(SiteType.values()));
        model.addAttribute("hmpPaymentMethodList", hmpPaymentMethodList);
//        [ESCROW-343, CLUB 노출제외]
//        model.addAttribute("clubPaymentMethodList", clubPaymentMethodList);
        model.addAllAttributes(RealGridHelper.create("chargeDiscountBenefitManageGridBaseInfo", ChargeDiscountBenefitManageDto.class));
        return "/pg/chargeDiscountBenefitManageMain";
    }

    /**
     * 청구할인 혜택 검색
     */
    @ResponseBody
    @GetMapping("/getChargeDiscountBenefitManage.json")
    public List<ChargeDiscountBenefitManageDto> getChargeDiscountBenefitManageList(ChargeDiscountBenefitManageSelectDto chargeDiscountBenefitManageSelectDto) throws Exception {
        return chargeDiscountBenefitService.getChargeDiscountBenefitManageList(chargeDiscountBenefitManageSelectDto);
    }

    /**
     * 청구할인 혜택 등록/수정
     *
     * @param chargeDiscountBenefitManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/saveChargeDiscountBenefitManage.json")
    public ResponseObject<Object> saveChargeDiscountBenefitManage(@RequestBody ChargeDiscountBenefitManageDto chargeDiscountBenefitManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        chargeDiscountBenefitManageDto.setRegId(userCd);
        chargeDiscountBenefitManageDto.setChgId(userCd);

        return chargeDiscountBenefitService.saveChargeDiscountBenefitManage(chargeDiscountBenefitManageDto);
    }
}
