package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.bannedWord.BannedWordListParamDto;
import kr.co.homeplus.admin.web.item.model.bannedWord.BannedWordListSelectDto;
import kr.co.homeplus.admin.web.item.model.bannedWord.BannedWordSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/bannedWord")
public class BannedWordController {
    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;

    /**
     * 시스템관리 > 기준정보관리 > 금칙어관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/bannedWordMain", method = RequestMethod.GET)
    public String bannedWordMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"            // 사용여부
                ,   "banned_type"   // 금칙어 유형
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("bannedType", code.get("banned_type"));

        model.addAllAttributes(RealGridHelper.create("bannedWordListGridBaseInfo", BannedWordListSelectDto.class));

        return "/item/bannedWordMain";
    }

    /**
     * 시스템관리 > 기준정보관리 > 금칙어관리 > 리스트
     *
     * @param listParamDto
     * @return BannedWordSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getBannedWordList.json"}, method = RequestMethod.GET)
    public List<BannedWordListSelectDto> getBannedWordList(BannedWordListParamDto listParamDto) {

        String apiUri = "/item/bannedWord/getBannedWordList";

        // 금칙어 처리를 위한 타 API 파라미터 수정을 피하기 위함.
        listParamDto.setBannedType(listParamDto.getSearchBannedType());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BannedWordListSelectDto>>>() {};
        return (List<BannedWordListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, BannedWordListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 시스템관리 > 기준정보관리 > 금칙어관리
     *
     * @param setParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setBannedWord.json"}, method = RequestMethod.POST)
    public ResponseResult setBannedWordList(@ModelAttribute BannedWordSetParamDto setParamDto) throws Exception {
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/bannedWord/setBannedWord";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference).getData();
    }

}
