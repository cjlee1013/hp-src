package kr.co.homeplus.admin.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

/**
 * 이미지 서버의 이미지 validation 기능 호출하기 위한 정보를 전달하는 Dto
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageFileValidationUploadDto {
    /**
     * image_key
     */
    private String imageKey;
    /**
     * mimeType
     */
    private String mimeType;
    /**
     * 파일 사이즈
     */
    private long size;
    /**
     * 가로 사이즈(너비)
     */
    private int width;
    /**
     * 세로 사이즈(높이)
     */
    private int height;
}
