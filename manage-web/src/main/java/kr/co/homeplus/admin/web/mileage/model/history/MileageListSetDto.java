package kr.co.homeplus.admin.web.mileage.model.history;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "마일리지 내역 조회 요청 DTO")
public class MileageListSetDto {
    @ApiModelProperty(value = "검색 작일", position = 1)
    private String schStartDt;
    @ApiModelProperty(value = "검색종료일", position = 1)
    private String schEndDt;
    @ApiModelProperty(value = "마일리지구분", position = 1)
    private String mileageCategory;
    @ApiModelProperty(value = "회원번호", position = 1)
    private String userNo;
}
