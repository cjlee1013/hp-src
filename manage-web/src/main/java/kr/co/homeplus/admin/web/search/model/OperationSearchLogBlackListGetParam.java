package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ApiModel(description = "검색로그관  > 블랙리스트 IP 조회")
public class OperationSearchLogBlackListGetParam {

    @ApiParam(value = "서비스 유형 ( 홈플러스 : HOMEPLUS, 익스프레스 : EXP  )")
    private String serviceType;
    @ApiParam(value = "블랙리스트 유형")
    private String blacklistType;
    @ApiParam(value = "블랙리스트 검색 ip")
    private String schKeyword;
    @ApiParam(value = "검색 날짜 유형")
    private String dateType;
    @ApiParam(value = "유효시작일")
    private String schStartDt;
    @ApiParam(value = "유효종료일")
    private String schEndDt;

}
