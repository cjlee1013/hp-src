package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class CategoryRankGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class CategoryRankList{
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private long rankingId;
        @RealGridColumnInfo(headText = "키워드", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String keyword;
        @RealGridColumnInfo(headText = "카테고리매핑", width = 93, columnType = RealGridColumnType.BASIC, sortable = true)
        private String categoryNm;
        @RealGridColumnInfo(headText = "재설정대상", width = 0, columnType = RealGridColumnType.BASIC, hidden = true, sortable = true)
        private char resetFlag;
        @RealGridColumnInfo(headText = "재설정대상", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String resetFlagNm;
        @RealGridColumnInfo(headText = "사용여부", width = 0, columnType = RealGridColumnType.BASIC, hidden = true, sortable = true)
        private char useFlag;
        @RealGridColumnInfo(headText = "사용여부", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useFlagNm;
        @RealGridColumnInfo(headText = "등록자", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String creator;
        @RealGridColumnInfo(headText = "등록일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String modifier;
        @RealGridColumnInfo(headText = "수정일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String updDt;
    }

    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
    public static class CategoryRankRelation{
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private long relationId;
        @RealGridColumnInfo(headText = "번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private long rankingId;
        @RealGridColumnInfo(headText = "키워드", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String keyword;
        @RealGridColumnInfo(headText = "점수", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String boostType;
        @RealGridColumnInfo(headText = "카테고리번호", width = 25, columnType = RealGridColumnType.BASIC, sortable = true)
        private int categoryId;
        @RealGridColumnInfo(headText = "카테고리명", width = 75, columnType = RealGridColumnType.NAME, sortable = true)
        private String categoryNm;
        @RealGridColumnInfo(headText = "재설정대상", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private char resetFlag;
        @RealGridColumnInfo(headText = "재설정대상", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String resetFlagNm;
        @RealGridColumnInfo(headText = "사용여부", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private char useFlag;
        @RealGridColumnInfo(headText = "사용여부", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useFlagNm;
        @RealGridColumnInfo(headText = "등록자", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String creator;
        @RealGridColumnInfo(headText = "등록일", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String modifier;
        @RealGridColumnInfo(headText = "수정일", width = 30, columnType = RealGridColumnType.BASIC, sortable = true)
        private String modDt;
    }
}