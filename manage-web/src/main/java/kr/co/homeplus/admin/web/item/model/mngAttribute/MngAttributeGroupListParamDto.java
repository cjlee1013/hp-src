package kr.co.homeplus.admin.web.item.model.mngAttribute;

import lombok.Data;

@Data
public class MngAttributeGroupListParamDto {
    private String searchGattrType;
    private String searchKeyword;
    private String searchUseYn;
    private String searchType;
}
