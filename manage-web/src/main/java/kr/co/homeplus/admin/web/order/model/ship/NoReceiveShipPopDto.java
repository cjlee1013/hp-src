package kr.co.homeplus.admin.web.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "미수취 신고 조회 요청 DTO")
public class NoReceiveShipPopDto {

    @ApiModelProperty(value = "배송번호", position = 1)
    private long bundleNo;

    @ApiModelProperty(value = "상품주문번호", position = 2)
    private long orderItemNo;

    @ApiModelProperty(value = "고객번호", position = 3)
    private long userNo;

    @ApiModelProperty(value = "상품주문번호", position = 4)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "상품주문번호", position = 5)
    private long shipNo;

    @ApiModelProperty(value = "미수취팝업타입", position = 6)
    private String noRcvPopType;

}
