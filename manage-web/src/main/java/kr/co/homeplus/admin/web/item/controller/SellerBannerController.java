package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListSelectDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerMatchSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/banner")
public class SellerBannerController {

    final private ResourceClient resourceClient;
    final private CodeService codeService;

    public SellerBannerController(ResourceClient resourceClient,
                                  CodeService codeService) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 상품상세 일괄공지 > 판매자공지
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/sellerBannerMain", method = RequestMethod.GET)
    public String SellerBannerMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "banner_period_type"       // 검색일자타입
                , "disp_yn" // 노출여부
                , "disp_type_seller"         // 배송방법
        );

        //코드정보
        model.addAttribute("bannerPeriodType", code.get("banner_period_type"));
        model.addAttribute("dispYn", code.get("disp_yn"));
        model.addAttribute("dispType", code.get("disp_type_seller"));

        model.addAllAttributes(
            RealGridHelper.create("sellerBannerListGridBaseInfo", ItemBannerListSelectDto.class));

        return "/item/sellerBannerMain";
    }

    /**
     * 상품관리> 상품상세 일괄등록 > 판매자공지 리스트
     *
     * @param itemBannerListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerBannerList.json"}, method = RequestMethod.POST)
    public List<ItemBannerListSelectDto> getSellerBannerList(@RequestBody ItemBannerListParamDto itemBannerListParamDto) {
        String apiUri = "/item/banner/getSellerBannerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerListSelectDto>>>() {};
        return (List<ItemBannerListSelectDto>) resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, itemBannerListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 상품상세 일괄 등록 > 판매자공지리스트 > 등록상품 조회
     *
     * @param bannerNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getItemMatchList.json"})
    public List<ItemBannerMatchSelectDto> getMatchItemList(@RequestParam(name = "bannerNo") Long bannerNo) {
        String apiUri = "/item/banner/getItemMatchList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerMatchSelectDto>>>() {};
        return (List<ItemBannerMatchSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?bannerNo=" + bannerNo, typeReference).getData();
    }
}

