package kr.co.homeplus.admin.web.statistics.controller;

import static kr.co.homeplus.admin.web.item.enums.StoreType.EXP;
import static kr.co.homeplus.admin.web.item.enums.StoreType.HYPER;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimeStoreOrderStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.admin.web.statistics.service.RealtimeOrderStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 주문결제통계 > 실시간 점포별 판매 현황
 */
@Controller
@RequestMapping("/escrow/statistics")
public class RealtimeStoreOrderStatisticsController {
    private final CodeService codeService;
    private final LoginCookieService loginCookieService;
    private final RealtimeOrderStatisticsService realtimeOrderStatisticsService;

    public RealtimeStoreOrderStatisticsController(CodeService codeService, LoginCookieService loginCookieService,
            RealtimeOrderStatisticsService realtimeOrderStatisticsService) {
        this.codeService = codeService;
        this.loginCookieService = loginCookieService;
        this.realtimeOrderStatisticsService = realtimeOrderStatisticsService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/realtimeStoreOrderStatisticsMain")
    public String realtimeStoreOrderStatisticsMain(Model model) throws Exception {
        List<MngCodeGetDto> storeTypeList = new ArrayList<>();
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();
        StoreBasicInfoDto storeBasicInfoDto = new StoreBasicInfoDto();

        // 로그인 사용자 점포 ID 있을 경우 점포 기본정보 조회
        String userStoreId = loginCookieService.getUserInfo().getStoreId();
        if (StringUtil.isNotEmpty(userStoreId)) {
            storeBasicInfoDto = realtimeOrderStatisticsService.getStoreBasicInfo(userStoreId);
        }
        // 조회한 점포 기본정보가 있을 경우 해당 점포만 세팅, 없을 경우 점포유형 HYPER/EXP 세팅
        if (StringUtil.isNotEmpty(storeBasicInfoDto) && StringUtil.isNotEmpty(storeBasicInfoDto.getStoreId())) {
            mngCodeGetDto.setMcCd(storeBasicInfoDto.getStoreType());
            mngCodeGetDto.setMcNm(storeBasicInfoDto.getStoreType());
            storeTypeList.add(mngCodeGetDto);
            model.addAttribute("userStoreId", storeBasicInfoDto.getStoreId());
            model.addAttribute("userStoreNm", storeBasicInfoDto.getStoreNm());
        } else {
            Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");
            storeTypeList = code.get("store_type")
                    .stream()
                    .filter(st -> HYPER.getType().equals(st.getMcCd()) || EXP.getType().equals(st.getMcCd()))
                    .collect(Collectors.toList());
        }

        model.addAttribute("storeType", storeTypeList);
        model.addAllAttributes(RealGridHelper.createForGroup("realtimeStoreOrderStatisticsGridBaseInfo", RealtimeStoreOrderStatisticsDto.class));

        return "/statistics/realtimeStoreOrderStatisticsMain";
    }

    /**
     * 실시간 점포별 판매현황 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getRealtimeStoreOrderStatisticsList.json")
    public List<RealtimeStoreOrderStatisticsDto> getRealtimeStoreOrderStatisticsList(
            @ModelAttribute StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_STORE_ORDER_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        statisticsOrderInfoSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return realtimeOrderStatisticsService.getRealtimeStoreOrderStatisticsList(statisticsOrderInfoSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/saveRealtimeStoreOrderExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_STORE_ORDER_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        realtimeOrderStatisticsService.saveExtractHistory(extractCommonSetDto);
    }
}
