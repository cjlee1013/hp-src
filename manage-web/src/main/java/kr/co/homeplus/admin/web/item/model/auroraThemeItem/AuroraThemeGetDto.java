package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class AuroraThemeGetDto {

    @RealGridColumnInfo(headText = "테마 ID", width = 80)
    private Long themeId;

    @RealGridColumnInfo(headText = "테마명(메인)", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 250, order = 1)
    private String themeNm;

    @RealGridColumnInfo(headText = "테마명(서브)", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 250, order = 2)
    private String themeSubNm;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 3)
    private String useYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 4)
    private String regNm;

    @RealGridColumnInfo(headText = "pcImgUrl", order = 5, hidden = true)
    private String pcImgUrl;
    @RealGridColumnInfo(headText = "mobileImgUrl", order = 6, hidden = true)
    private String mobileImgUrl;
    @RealGridColumnInfo(headText = "useYn", order = 7, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "priority", order = 8, hidden = true)
    private String priority;

    private String regDt;
    private String chgDt;
    private String regId;
    private String chgId;
    private String chgNm;

}
