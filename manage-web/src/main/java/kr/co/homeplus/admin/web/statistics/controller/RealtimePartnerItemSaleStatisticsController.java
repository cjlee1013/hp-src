package kr.co.homeplus.admin.web.statistics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.RealtimePartnerItemSaleStatisticsDto;
import kr.co.homeplus.admin.web.statistics.model.StatisticsOrderInfoSelectDto;
import kr.co.homeplus.admin.web.statistics.service.RealtimeOrderStatisticsService;
import kr.co.homeplus.admin.web.statistics.service.RealtimePartnerItemSaleStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 상품 주문통계 > 실시간 판매업체 상품 판매 현황
 */
@Controller
@RequestMapping("/escrow/statistics")
public class RealtimePartnerItemSaleStatisticsController {
    @Value("${front.homeplus.url}")
    private String frontUrl;

    private final CodeService codeService;
    private final RealtimeOrderStatisticsService realtimeOrderStatisticsService;
    private final LoginCookieService loginCookieService;
    private final RealtimePartnerItemSaleStatisticsService realtimePartnerItemSaleStatisticsService;

    public RealtimePartnerItemSaleStatisticsController(CodeService codeService, RealtimeOrderStatisticsService realtimeOrderStatisticsService,
            LoginCookieService loginCookieService, RealtimePartnerItemSaleStatisticsService realtimePartnerItemSaleStatisticsService) {
        this.codeService = codeService;
        this.realtimeOrderStatisticsService = realtimeOrderStatisticsService;
        this.loginCookieService = loginCookieService;
        this.realtimePartnerItemSaleStatisticsService = realtimePartnerItemSaleStatisticsService;
    }

    /**
     * 실시간 판매업체 상품 판매 현황 메인페이지 호출
     */
    @GetMapping("/realtimePartnerItemSaleStatisticsMain")
    public String realtimePartnerItemSaleStatisticsMain(Model model) throws Exception {
        ObjectMapper om = new ObjectMapper();
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("tax_yn");
        model.addAttribute("taxYnJson", om.writeValueAsString(code.get("tax_yn")));
        model.addAttribute("frontUrl", frontUrl);
        model.addAllAttributes(RealGridHelper.create("realtimePartnerItemSaleStatisticsGridBaseInfo", RealtimePartnerItemSaleStatisticsDto.class));

        return "/statistics/realtimePartnerItemSaleStatisticsMain";
    }

    /**
     * 실시간 판매업체 상품 판매현황 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getRealtimePartnerItemSaleStatisticsList.json")
    public List<RealtimePartnerItemSaleStatisticsDto> getRealtimePartnerItemSaleStatisticsList(
            @ModelAttribute StatisticsOrderInfoSelectDto statisticsOrderInfoSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_PARTNER_ITEM_SALE_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        statisticsOrderInfoSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return realtimePartnerItemSaleStatisticsService.getRealtimePartnerItemSaleStatisticsList(statisticsOrderInfoSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/saveRealtimePartnerItemSaleExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.REALTIME_PARTNER_ITEM_SALE_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        realtimeOrderStatisticsService.saveExtractHistory(extractCommonSetDto);
    }
}
