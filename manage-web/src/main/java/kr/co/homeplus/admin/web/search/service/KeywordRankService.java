package kr.co.homeplus.admin.web.search.service;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.KeywordRankSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KeywordRankService {

    private final ResourceClient resourceClient;
    final private CodeService codeService;

    public Map<String, List<MngCodeGetDto>> getServiceCode() throws Exception {
        return codeService.getCode(
            "use_yn"       // 사용여부
            ,	"item_type"         // 상품유형
            ,	"store_type"        // 점포구분 - Hyper,Club,Exp,DS
            ,   "item_status"       // 상품상태
            ,   "disp_yn"           // 노출여부
            ,   "set_yn"            // 설정여부
            ,   "limit_yn"          // 제한여부
            ,   "target_yn"         // 대상여부
            ,   "item_img_type"     // 상품이미지유형
            ,   "tax_yn"            // 과세여부
            ,   "commission_type"   // 수수료유형
            ,   "limit_duration"    // 기간제한
            ,   "holiday_except"    // 공휴일제외유형
            ,   "pr_type"           // 셀링포인트유형
            ,   "rsv_type"          // 예약유형
            ,   "reg_yn"            // 등록여부
            ,   "stop_limit_yn"     // 중지제한여부
        );
    }

    public ResponseObject getKeywordRank(String itemNo) {
        String apiUri = "/keywordRank/" + itemNo;
        return getResponseObject(apiUri);
    }

    public ResponseObject saveKeywordRank(KeywordRankSetDto param) {
        String apiUri = "/keywordRank/save";
        return getResponseObject(apiUri);
    }

    public ResponseObject deleteKeywordRank(String itemNo) {
        String apiUri = "/keywordRank/" + itemNo + "/delete";
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }
}
