package kr.co.homeplus.admin.web.message.model;

import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ManualCancelRequest {

    private long manualSendSeq;

    private long manualSendWorkSeq;

    private long coupledWorkSeq;

    private WorkStatus workStatus;

    private WorkType workType;

    private String chgId;
}
