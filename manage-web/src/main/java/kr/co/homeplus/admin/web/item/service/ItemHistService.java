package kr.co.homeplus.admin.web.item.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemCertHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemDetailHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemFileHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupStringBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupStringGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemImgHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemNoticeHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemOptionValueHistGetDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Slf4j
@Component
public class ItemHistService {

    public List<ItemHistGroupGetDto> getItemHistGroup(List<ItemHistGetDto> itemHistGetDtoList) {
        List<ItemHistGroupGetDto> itemHistGroupGetDtoList = Lists.newArrayList();
        ItemHistGroupStringGetDto prevItemHistGroupStringGetDto = new ItemHistGroupStringGetDto();

        if (ObjectUtils.isEmpty(itemHistGetDtoList)) {
            return itemHistGroupGetDtoList;
        }

        Collections.reverse(itemHistGetDtoList);

        for (final ItemHistGetDto itemHistGetDto : itemHistGetDtoList) {
            itemHistGroupGetDtoList.add(itemHistGetDto.setItemHistGroupData(prevItemHistGroupStringGetDto));

            prevItemHistGroupStringGetDto.setBasic(itemHistGetDto.toChangeBasicString());
            prevItemHistGroupStringGetDto.setSale(itemHistGetDto.toChangeSaleString());
            prevItemHistGroupStringGetDto.setShip(itemHistGetDto.toChangeShipString());
            prevItemHistGroupStringGetDto.setOptItem(itemHistGetDto.toChangeOptItemString());
            prevItemHistGroupStringGetDto.setImg(itemHistGetDto.toChangeImgString());
            prevItemHistGroupStringGetDto.setNotice(itemHistGetDto.toChangeNoticeString());
            prevItemHistGroupStringGetDto.setEtc(itemHistGetDto.toChangeEtcString());
        }

        Collections.reverse(itemHistGroupGetDtoList);

        return itemHistGroupGetDtoList;
    }

    public List<ItemHistGroupBasicGetDto> getItemHistBasicGroup(List<ItemHistBasicGetDto> itemHistGetDtoList) {
        List<ItemHistGroupBasicGetDto> itemHistGroupGetDtoList = Lists.newArrayList();
        ItemHistGroupStringBasicGetDto prevItemHistGroupStringGetDto = new ItemHistGroupStringBasicGetDto();

        if (ObjectUtils.isEmpty(itemHistGetDtoList)) {
            return itemHistGroupGetDtoList;
        }

        Collections.reverse(itemHistGetDtoList);

        for (final ItemHistBasicGetDto itemHistGetDto : itemHistGetDtoList) {
            itemHistGroupGetDtoList.add(itemHistGetDto.setItemHistGroupData(prevItemHistGroupStringGetDto));

            prevItemHistGroupStringGetDto.setBasic(itemHistGetDto.toChangeBasicString());
            prevItemHistGroupStringGetDto.setSale(itemHistGetDto.toChangeSaleString());
            prevItemHistGroupStringGetDto.setOptItem(itemHistGetDto.toChangeOptItemString());
            prevItemHistGroupStringGetDto.setImg(itemHistGetDto.toChangeImgString());
            prevItemHistGroupStringGetDto.setAttr(itemHistGetDto.toChangeAttrString());
            prevItemHistGroupStringGetDto.setEtc(itemHistGetDto.toChangeEtcString());
        }

        Collections.reverse(itemHistGroupGetDtoList);

        return itemHistGroupGetDtoList;
    }

    public List<ItemDetailHistGetDto> getItemHistDetailGroup(List<ItemDetailHistGetDto> itemDetailHistGetDtoList) throws Exception {

        List<ItemDetailHistGetDto> res = new ArrayList<>();

        if (ObjectUtils.isEmpty(itemDetailHistGetDtoList)) {
            return itemDetailHistGetDtoList;
        }

        int nextIndex = 1;
        int size = itemDetailHistGetDtoList.size();

        for (ItemDetailHistGetDto dto : itemDetailHistGetDtoList) {
            ItemDetailHistGetDto newDto = new ItemDetailHistGetDto();
            if (nextIndex != size) {
                BeanUtils.copyProperties(newDto, dto);
                //판매기간-시작시간
                if (!Optional.ofNullable(dto.getSaleStartDt()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getSaleStartDt()).orElse(""))) {
                    newDto.setSaleStartDt(this.getStringDiff(dto.getSaleStartDt(), itemDetailHistGetDtoList.get(nextIndex).getSaleStartDt()));
                    newDto.setChgSaleStartDt("수정");
                }
                //판매기간-종료시간
                if (!Optional.ofNullable(dto.getSaleEndDt()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getSaleEndDt()).orElse(""))) {
                    newDto.setSaleEndDt(this.getStringDiff(dto.getSaleEndDt(), itemDetailHistGetDtoList.get(nextIndex).getSaleEndDt()));
                    newDto.setChgSaleEndDt("수정");
                }
                //예약판매부가설정
                if (!Optional.ofNullable(dto.getRsvPurchaseLimitYn()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getRsvPurchaseLimitYn()).orElse(""))) {
                    newDto.setRsvPurchaseLimitYn(this.getStringDiff(dto.getRsvPurchaseLimitYn(), itemDetailHistGetDtoList.get(nextIndex).getRsvPurchaseLimitYn()));
                    newDto.setChgRsvPurchaseLimitYn("수정");
                }
                //온라인재고설정
                if (!Optional.ofNullable(dto.getOnlineStockYn()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getOnlineStockYn()).orElse(""))) {
                    newDto.setOnlineStockYn(this.getStringDiff(dto.getOnlineStockYn(), itemDetailHistGetDtoList.get(nextIndex).getOnlineStockYn()));
                    newDto.setChgOnlineStockYn("수정");
                }
                //온라인재고설정기간-시작시간
                if (!Optional.ofNullable(dto.getOnlineStockStartDt()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getOnlineStockStartDt()).orElse(""))) {
                    newDto.setOnlineStockStartDt(this.getStringDiff(dto.getOnlineStockStartDt(), itemDetailHistGetDtoList.get(nextIndex).getOnlineStockStartDt()));
                    newDto.setChgOnlineStockStartDt("수정");
                }
                //온라인재고설정기간-종료시간
                if (!Optional.ofNullable(dto.getOnlineStockEndDt()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getOnlineStockEndDt()).orElse(""))) {
                    newDto.setOnlineStockEndDt(this.getStringDiff(dto.getOnlineStockEndDt(), itemDetailHistGetDtoList.get(nextIndex).getOnlineStockEndDt()));
                    newDto.setChgOnlineStockEndDt("수정");
                }
                //판매단위수량
                if (!Optional.ofNullable(dto.getSaleUnit()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getSaleUnit()).orElse(""))) {
                    newDto.setSaleUnit(this.getStringDiff(dto.getSaleUnit(), itemDetailHistGetDtoList.get(nextIndex).getSaleUnit()));
                    newDto.setChgSaleUnit("수정");
                }
                //최소구매수량
                if (!Optional.ofNullable(dto.getPurchaseMinQty()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getPurchaseMinQty()).orElse(""))) {
                    newDto.setPurchaseMinQty(this.getStringDiff(dto.getPurchaseMinQty(), itemDetailHistGetDtoList.get(nextIndex).getPurchaseMinQty()));
                    newDto.setChgPurchaseMinQty("수정");
                }
                //구매수량제한여부
                if (!Optional.ofNullable(dto.getPurchaseLimitYn()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitYn()).orElse(""))) {
                    newDto.setPurchaseLimitYn(this.getStringDiff(dto.getPurchaseLimitYn(), itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitYn()));
                    newDto.setChgPurchaseLimitYn("수정");
                }
                //구매수량제한-일
                if (!Optional.ofNullable(dto.getPurchaseLimitDay()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitDay()).orElse(""))) {
                    newDto.setPurchaseLimitDay(this.getStringDiff(dto.getPurchaseLimitDay(), itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitDay()));
                    newDto.setChgPurchaseLimitDay("수정");
                }
                //구매수량제한-개수
                if (!Optional.ofNullable(dto.getPurchaseLimitQty()).orElse("").equals(Optional.ofNullable(itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitQty()).orElse(""))) {
                    newDto.setPurchaseLimitQty(this.getStringDiff(dto.getPurchaseLimitQty(), itemDetailHistGetDtoList.get(nextIndex).getPurchaseLimitQty()));
                    newDto.setChgPurchaseLimitQty("수정");
                }

                res.add(newDto);

                nextIndex++;
            } else {
                res.add(dto);
            }
        }


        return res;
    }

    public ItemHistGetDto getItemHistSelectDiff(final List<ItemHistGetDto> itemHistGetDtoList) {
        if (ObjectUtils.isEmpty(itemHistGetDtoList)) {
            new ItemHistGetDto();
        }

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> itemHistGetMap = null;
        Map<String, Object> prevItemHistGetMap = null;

        int idx = 0;
        for (final ItemHistGetDto itemHistGetDto : itemHistGetDtoList) {

            if (itemHistGetDto.getProofFileList() != null) {
                itemHistGetDto.setProofFileListMap(new HashMap<>());

                for(ItemFileHistGetDto dto : itemHistGetDto.getProofFileList()) {
                    itemHistGetDto.getProofFileListMap().put(dto.getFileSeq(), dto);
                }
            }

            if (itemHistGetDto.getOptList() != null) {
                itemHistGetDto.setOptListMap(new HashMap<>());

                for(ItemOptionValueHistGetDto dto : itemHistGetDto.getOptList()) {
                    itemHistGetDto.getOptListMap().put(dto.getOptNo(), dto);
                }
            }

            itemHistGetDto.setImgListMap(new HashMap<>());

            for(ItemImgHistGetDto dto : itemHistGetDto.getImgList()) {
                itemHistGetDto.getImgListMap().put(dto.getImgNo(), dto);
            }

            if (itemHistGetDto.getNoticeList() != null) {
                itemHistGetDto.setNoticeListMap(new HashMap<>());

                for(ItemNoticeHistGetDto dto : itemHistGetDto.getNoticeList()) {
                    itemHistGetDto.getNoticeListMap().put(dto.getNoticeNo(), dto);
                }
            }

            if (itemHistGetDto.getCertList() != null) {
                itemHistGetDto.setCertListMap(new HashMap<>());

                for(ItemCertHistGetDto dto : itemHistGetDto.getCertList()) {
                    itemHistGetDto.getCertListMap().put(dto.getItemCertSeq(), dto);
                }
            }

            if (idx == 0) {
                itemHistGetMap = oMapper.convertValue(itemHistGetDto, new TypeReference<Map<String, Object>>() {});
            } else if (idx == 1) {
                prevItemHistGetMap = oMapper.convertValue(itemHistGetDto, new TypeReference<Map<String, Object>>() {});
            }

            idx++;
        }

        Map<String, Object> res = this.getMultiDepthDiff(itemHistGetMap, prevItemHistGetMap);

        if (res.get("optListMap").equals("-")) {
            res.put("optList", null);
            res.put("optListMap", null);
        }

        if (res.get("proofFileListMap").equals("-")) {
            res.put("proofFileList", null);
            res.put("proofFileListMap", null);
        }

        if (res.get("certListMap").equals("-")) {
            res.put("certList", null);
            res.put("certListMap", null);
        }

        if (res.get("noticeListMap").equals("-")) {
            res.put("noticeList", null);
            res.put("noticeListMap", null);
        }

        return oMapper.convertValue(
                res,
                ItemHistGetDto.class);
    }

    // multi depth diff
    private Map<String, Object> getMultiDepthDiff(
            Map<String, Object> itemHistGetMap, final Map<String, Object> prevItemHistGetMap) {
        if (!ObjectUtils.isEmpty(itemHistGetMap) || !ObjectUtils.isEmpty(prevItemHistGetMap)) {
            for (final Map.Entry<String, Object> entry : itemHistGetMap.entrySet()) {
                if (entry.getValue() instanceof String) {
                    if (!ObjectUtils.isEmpty(itemHistGetMap) && ObjectUtils.isEmpty(prevItemHistGetMap)) {
                        // 추가
                        itemHistGetMap.put(
                                entry.getKey(),
                                this.getStringAdd(entry.getValue().toString())
                        );
                    } else {
                        if (ObjectUtils.isEmpty(prevItemHistGetMap) || ObjectUtils.isEmpty(prevItemHistGetMap.get(entry.getKey()))) {
                            // 추가
                            itemHistGetMap.put(
                                    entry.getKey(),
                                    this.getStringAdd(entry.getValue().toString())
                            );
                            continue;
                        }

                        // 수정
                        itemHistGetMap.put(
                                entry.getKey(),
                                this.getStringDiff(entry.getValue().toString(), prevItemHistGetMap.get(entry.getKey()).toString())
                        );
                    }

                } else if (entry.getValue() instanceof List) {
                    if (!ObjectUtils.isEmpty(prevItemHistGetMap)) {
                        itemHistGetMap.put(
                                entry.getKey(),
                                this.getMultiDepthDetailDiffList(entry.getValue(), prevItemHistGetMap.get(entry.getKey()))
                        );
                    }

                } else if (entry.getValue() instanceof Map) {
                    if (ObjectUtils.isEmpty(prevItemHistGetMap)) {
                        // 추가
                        itemHistGetMap.put(
                                entry.getKey(),
                                this.getMultiDepthDetailDiffMap(entry.getValue(), null)
                        );
                    } else {
                        // 수정
                        itemHistGetMap.put(
                                entry.getKey(),
                                this.getMultiDepthDetailDiffMap(entry.getValue(), prevItemHistGetMap.get(entry.getKey()))
                        );
                    }

                } else {
                    // null 공백처리
                    itemHistGetMap.put(entry.getKey(), "-");
                }
            }
        } else {
            itemHistGetMap = Maps.newHashMap();
        }

        return itemHistGetMap;
    }

    @SuppressWarnings("unchecked")
    private Object getMultiDepthDetailDiffMap(final Object nowObj, final Object prevObj) {
        Map<String, Object> retMap = Maps.newHashMap();

        if (!ObjectUtils.isEmpty(nowObj) || !ObjectUtils.isEmpty(prevObj)) {
            Map<String, Object> nowMap = (HashMap<String, Object>) nowObj;
            final Map<String, Object> prevMap = (HashMap<String, Object>) prevObj;

            for (final Map.Entry<String, Object> entry : nowMap.entrySet()) {
                if (ObjectUtils.isEmpty(prevMap)) {
                    // 추가
                    if (entry.getValue() instanceof Map) {
                        retMap.put(
                                entry.getKey(),
                                this.getMultiDepthDiff((HashMap<String, Object>) entry.getValue(), null)
                        );
                    } else {
                        retMap.put(entry.getKey(), entry.getValue());
                    }
                } else {
                    // 수정
                    final Object obj2 = prevMap.get(entry.getKey());

                    retMap.put(
                            entry.getKey(),
                            this.getMultiDepthDiff((HashMap<String, Object>) entry.getValue(), (HashMap<String, Object>) obj2)
                    );
                }
            }
        } else {
            if (!ObjectUtils.isEmpty(nowObj)) {
                retMap = (HashMap<String, Object>) nowObj;
            }
        }

        return retMap;
    }

    @SuppressWarnings("unchecked")
    private Object getMultiDepthDetailDiffList(final Object nowObj, final Object prevObj) {
        List<Object> retList = Lists.newArrayList();

        if (!ObjectUtils.isEmpty(nowObj) || !ObjectUtils.isEmpty(prevObj)) {
            List<Object> nowList = (List<Object>) nowObj;
            final List<Object> prevList = (List<Object>) prevObj;
            final int prevListSize = prevList.size();
            int idx = 0;

            for (final Object obj : nowList) {
                if (prevListSize > idx) {
                    final Object obj2 = prevList.get(idx);

                    retList.add(this.getMultiDepthDiff((Map<String, Object>) obj, (Map<String, Object>) obj2));
                } else {
                    retList.add(this.getMultiDepthDiff((Map<String, Object>) obj, null));
                }

                idx++;
            }
        } else {
            retList = (List<Object>) nowObj;
        }

        return retList;
    }

    private String getStringDiff(String nowStr, String prevStr) {
        final String SPAN_RED_HTML_START = "<span style=\"color:red\">";
        final String SPAN_BLUE_HTML_START = "<span style=\"color:blue\">";
        final String SPAN_HTML_END = "</span>";

        if (nowStr == null) {
            nowStr = "-";
        }

        if (prevStr == null) {
            prevStr = "-";
        }

        if (nowStr.equals(prevStr)) {
            return nowStr;
        } else {

            return    SPAN_RED_HTML_START
                    + prevStr
                    + " -> "
                    + SPAN_HTML_END
                    + SPAN_BLUE_HTML_START
                    + nowStr
                    + SPAN_HTML_END;
        }
    }

    private String getStringAdd(final String nowStr) {
        final String SPAN_HTML_START = "<span style=\"color:#00C853\">";
        final String SPAN_HTML_END = "</span>";

        return    SPAN_HTML_START
                + nowStr
                + SPAN_HTML_END;
    }
}
