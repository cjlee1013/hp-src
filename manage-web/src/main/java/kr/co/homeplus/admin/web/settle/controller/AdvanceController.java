package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.Advance.AdvanceListGetDto;
import kr.co.homeplus.admin.web.settle.model.Advance.AdvanceListSetDto;
import kr.co.homeplus.admin.web.settle.service.AdvanceService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/advance")
public class AdvanceController {

    private final AdvanceService advanceService;
    private final SettleCommonService settleCommonService;

    @RequestMapping(value = "/advance", method = RequestMethod.GET)
    public String advance(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.createForGroup("advanceListBaseInfo", AdvanceListGetDto.class));
        return "/settle/advanceList";
    }

    @ResponseBody
    @RequestMapping(value = "/getList.json", method = RequestMethod.GET)
    public List<AdvanceListGetDto> getList(AdvanceListSetDto advanceListSetDto) throws Exception {
        return advanceService.getList(advanceListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getZeroList.json", method = RequestMethod.GET)
    public List<AdvanceListGetDto> getZeroList(AdvanceListSetDto advanceListSetDto) throws Exception {
        return advanceService.getZeroList(advanceListSetDto);
    }
}