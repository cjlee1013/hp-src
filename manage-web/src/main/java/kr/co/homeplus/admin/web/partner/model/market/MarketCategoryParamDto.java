package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryParamDto {

    // 회원상태
    private String schPartnerStatus;
    // 사업자 유형
    private String schOperatorType;
    // 검색 조건
    private String schType;
    // 검색 키워드
    private String schKeyword;

}
