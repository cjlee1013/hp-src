package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.Optional;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ItemFileHistGetDto {
	// 첨부파일 일련번호
	private String fileSeq;
	// 구분
	private String fileType;
	// 구분명
	private String fileTypeNm;
	// 파일경로
	private String fileUrl;
	// 원본파일명
	private String fileNm;


    @Override
    public String toString() {
		return Optional.of(
				  this.fileSeq
				+ this.fileType
			  	+ this.fileTypeNm
			  	+ this.fileNm
				+ this.fileUrl
		).toString();
    }
}
