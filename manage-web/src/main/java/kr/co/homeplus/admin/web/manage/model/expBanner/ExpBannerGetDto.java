package kr.co.homeplus.admin.web.manage.model.expBanner;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class ExpBannerGetDto {

    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true)
    private Long bannerNo;

    @RealGridColumnInfo(headText = "디바이스", width = 80, order = 1)
    private String deviceTypeNm;

    @RealGridColumnInfo(headText = "전시위치", width = 120, order = 2)
    private String locNm;

    @RealGridColumnInfo(headText = "배너명", width = 180, order = 3)
    private String bannerNm;

    @RealGridColumnInfo(headText = "전시 시작일", width = 120, order = 4, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", width = 120, order = 5, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "전시순서", width = 50, columnType = RealGridColumnType.NUMBER_CENTER, order = 6, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "전시여부", width = 70, order = 7, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 8)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 9, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 10)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 11, sortable = true)
    private String chgDt;


    @RealGridColumnInfo(headText = "template", order = 12, hidden = true, sortable = true)
    private String template;
    @RealGridColumnInfo(headText = "templateDetail", order = 13, hidden = true, sortable = true)
    private String templateDetail;
    @RealGridColumnInfo(headText = "device", order = 14, hidden = true, sortable = true)
    private String device;
    @RealGridColumnInfo(headText = "loc", order = 15, hidden = true, sortable = true)
    private String loc;
    @RealGridColumnInfo(headText = "dispYn", order = 16, hidden = true, sortable = true)
    private String dispYn;


    private String regId;
    private String chgId;

}
