package kr.co.homeplus.admin.web.common.model.cnotice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiNoticeListSelectDto {
    private Long totalCount;
    private List<AdminNoticeListSelectDto> list;
}
