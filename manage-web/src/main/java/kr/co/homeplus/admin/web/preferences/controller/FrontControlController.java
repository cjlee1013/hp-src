package kr.co.homeplus.admin.web.preferences.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.preferences.model.LiveSlotFlag;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "프론트 구성요소 컨트롤러")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequestMapping("/preferences/front/control")
@RequiredArgsConstructor
public class FrontControlController {

    private final ResourceClient resourceClient;

    @ApiOperation(value = "알림톡 발송 main 화면")
    @ApiResponse(code = 200, message = "알림톡 발송 main 화면")
    @GetMapping("/main")
    public String frontControlMain() {
        return "/preferences/frontControlMain";
    }

    @ApiOperation(value = "사용자 첫배송 기능 설정값 조회")
    @ApiResponse(code = 200, message = "사용자 첫배송 기능 설정값")
    @GetMapping("/getLiveSlotOn.json")
    @ResponseBody
    public ResponseObject<LiveSlotFlag> getLiveSlotOn() {
        String apiUri = "/config/live/slot/get";

        ResourceClientRequest<LiveSlotFlag> request = ResourceClientRequest.<LiveSlotFlag>getBuilder()
            .apiId(ResourceRouteName.PREFERENCES)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig())
            .getBody();
    }

    @ApiOperation(value = "사용자 첫배송 기능 설정")
    @ApiResponse(code = 200, message = "사용자 첫배송 기능 설정 여부")
    @PostMapping("/setLiveSlotOn.json")
    @ResponseBody
    public ResponseObject<LiveSlotFlag> setLiveSlotOn(
        @ApiParam(value = "사용자 첫배송 기능 설정값", required = true) final boolean liveSlotOn) {
        String apiUri = "/config/live/slot/set";

        LiveSlotFlag liveSlotFlag = new LiveSlotFlag();
        liveSlotFlag.setLiveSlotOn(liveSlotOn);

        ResourceClientRequest<LiveSlotFlag> request = ResourceClientRequest.<LiveSlotFlag>postBuilder()
            .apiId(ResourceRouteName.PREFERENCES)
            .uri(apiUri)
            .postObject(liveSlotFlag)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig())
            .getBody();
    }
}
