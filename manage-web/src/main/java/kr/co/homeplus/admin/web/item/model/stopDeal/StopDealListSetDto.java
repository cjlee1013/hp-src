package kr.co.homeplus.admin.web.item.model.stopDeal;

import lombok.Data;
import java.util.List;

@Data
public class StopDealListSetDto {

    private String workId;

    private String regId;

    private String step;

    private List<StopDealSetDto> stopDealListSet;
}
