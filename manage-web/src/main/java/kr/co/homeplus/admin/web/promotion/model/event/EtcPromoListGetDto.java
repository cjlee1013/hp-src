package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class EtcPromoListGetDto {
    @RealGridColumnInfo(headText = "행사 번호", sortable = true)
    private Long etcNo;

    @RealGridColumnInfo(headText = "관리명", width = 400)
    private String etcMngNm;

    @RealGridColumnInfo(headText = "유형")
    private String storeType;

    @RealGridColumnInfo(headText = "행사시작일", columnType = RealGridColumnType.DATE, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String promoStartDt;

    @RealGridColumnInfo(headText = "행사종료일", columnType = RealGridColumnType.DATE, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String promoEndDt;

    @RealGridColumnInfo(headText = "사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}