package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcMainSetParamDto {

    private Integer mainNo;

    private String siteType;

    private String loc1Depth;

    private String loc2Depth;

    private String dispYn;

    private Integer priority;

    private String dispStartDt;

    private String dispEndDt;

    private String bannerNm;

    private String dispStoreType;

    private String bgColor;

    private String closeType;

    private String division;

    private String template;

    private String templateDetail;

    private List<PcMainLinkSetParamDto> linkList;

    private List<PcMainStoreSetDto> storeList;

    private String regId;

}
