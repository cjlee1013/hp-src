package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class RecipientSearchRequest {

    private String schStartDate;

    private String schEndDate;

    private String schKeyword;

    private String schType;

    private String workStatus;

    private String workType;
}
