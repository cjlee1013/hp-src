package kr.co.homeplus.admin.web.item.model.itemGroup;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemGroupListGetDto {

    @ApiModelProperty(value = "묶음번호")
    @RealGridColumnInfo(headText = "묶음번호", width = 100, sortable = true)
    private Long groupNo;

    @ApiModelProperty(value = "묶음상품명")
    @RealGridColumnInfo(headText = "묶음상품명", width = 150, sortable = true)
    private String groupItemNm;

    @ApiModelProperty(value = "상품수")
    @RealGridColumnInfo(headText = "상품수", width = 200, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private String itemCount;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true)
    private String useYnNm;

    @ApiModelProperty(value = "옵션단계")
    @RealGridColumnInfo(headText = "옵션단계", width = 0, hidden = true)
    private String depth;

    @ApiModelProperty(value = "1단계타이틀")
    @RealGridColumnInfo(headText = "1단계타이틀", width = 0, hidden = true)
    private String opt1Title;

    @ApiModelProperty(value = "2단계타이틀")
    @RealGridColumnInfo(headText = "2단계타이틀", width = 0, hidden = true)
    private String opt2Title;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;
}
