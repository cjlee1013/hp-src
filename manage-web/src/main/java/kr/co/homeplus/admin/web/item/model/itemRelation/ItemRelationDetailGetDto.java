package kr.co.homeplus.admin.web.item.model.itemRelation;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
public class ItemRelationDetailGetDto {

    //상품번호
    @RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNo;

    //상품명
    @RealGridColumnInfo(headText = "상품명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 250, sortable = true)
    private String itemNm;

    //상품상태명
    @RealGridColumnInfo(headText = "상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemStatusNm;
    
    //판매가격
    @RealGridColumnInfo(headText = "판매가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, sortable = true)
    private String salePrice;

    //판매업체ID
    @RealGridColumnInfo(headText = "판매업체ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String partnerId;

    //판매시작일
    @RealGridColumnInfo(headText = "판매시작일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String saleStartDt;

    //판매종료일
    @RealGridColumnInfo(headText = "판매종료일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String saleEndDt;

    //성인상품유형명
    @RealGridColumnInfo(headText = "성인상품유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String adultTypeNm;

    //장바구니 제한여부
    @RealGridColumnInfo(headText = "장바구니제한", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String cartLimitYnNm;

    //장바구니 제한여부
    @RealGridColumnInfo(headText = "장바구니제한", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String cartLimitYn;

    //텍스트 옵션 사용여부
    @RealGridColumnInfo(headText = "텍스트옵션", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String optTxtUseYn;

    //텍스트 옵션 사용여부
    @RealGridColumnInfo(headText = "텍스트옵션", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String optTxtUseYnNm;

    //성인상품유형
    @RealGridColumnInfo(headText = "성인상품유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String adultType;

    //대분류 카테고리명
    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String lcateNm;

    //중분류 카테고리명
    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String mcateNm;

    //소분류 카테고리명
    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String scateNm;

    //소분류 카테고리
    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String scateCd;

}
