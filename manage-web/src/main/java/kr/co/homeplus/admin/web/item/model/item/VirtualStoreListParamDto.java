package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualStoreListParamDto {
    private String schStoreType;
    private String schStoreKind;
    private String schType;
    private String schKeyword;
    private Character schUseYn;
}
