package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 배송관리 선택발송처리 팝업 리스트 요청 DTO")
public class ShipManageShipAllPopListSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;
}
