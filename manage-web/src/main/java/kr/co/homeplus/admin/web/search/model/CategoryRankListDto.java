package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryRankListDto {
    private long rankingId;
    private String keyword;
    private char useFlag;
    private String useFlagNm;
    private char resetFlag;
    private String resetFlagNm;
    private String regDt;
    private String categoryNm;
    private String creator;
    private String updDt;
    private String modifier;
}
