package kr.co.homeplus.admin.web.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel("어드민 계정확인 파라미터")
public class AdminCertUserParam {

    @ApiParam("로그인ID")
    @NotBlank(message = "로그인ID가 없습니다.")
    @Size(min = 3, max = 20, message = "아이디는 3에서 20자리 이내로 입력 가능합니다")
    private String userId;

    @ApiParam("운영자명")
    @NotBlank(message = "운영자명이 없습니다.")
    @Size(max = 30, message = "최대 30글자까지 입력 가능합니다")
    private String userNm;

    @ApiParam("사번")
    @NotBlank(message = "사번이 없습니다.")
    @Size(max = 10, message = "최대 10글자까지 입력 가능합니다")
    @Pattern(regexp = "^[0-9]+$", message = "사번은 숫자만 허용합니다.")
    private String empId;
}
