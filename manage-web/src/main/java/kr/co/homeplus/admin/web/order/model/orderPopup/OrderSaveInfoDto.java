package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 상세 > 적립내역")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderSaveInfoDto {
    @ApiModelProperty(value = "포인트타입", position = 1)
    @RealGridColumnInfo(headText = "포인트타입")
    private String pointKind;
    @ApiModelProperty(value = "마일리지", position = 2)
    @RealGridColumnInfo(headText = "마일리지", width = 150)
    private String mileageKind;
    @ApiModelProperty(value = "MHC거래번호", position = 3)
    @RealGridColumnInfo(headText = "MHC거래번호")
    private String approvalNo;
    @ApiModelProperty(value = "금액", position = 4)
    @RealGridColumnInfo(headText = "금액", columnType = RealGridColumnType.NUMBER_CENTER)
    private int resultPoint;
    @ApiModelProperty(value = "적립/회수상태", position = 5)
    @RealGridColumnInfo(headText = "적립/회수상태")
    private String pointStatus;
}