package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum  PreAnalysisAttrCd {
    ALWAYS("ALWAYS","*"),
    LIMITED("LIMITED","?"),
    UNUSED("UNUSED","없음");

    private String code;
    private String name;

    public static PreAnalysisAttrCd getType(String code) {
        for(PreAnalysisAttrCd cd : PreAnalysisAttrCd.values()) {
            if(cd.getCode().equals(code)) {
                return cd;
            }
        }
        return null;
    }
}
