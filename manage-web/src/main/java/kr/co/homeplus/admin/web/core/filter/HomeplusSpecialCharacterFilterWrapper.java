package kr.co.homeplus.admin.web.core.filter;

import java.util.Locale;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HomeplusSpecialCharacterFilterWrapper extends HttpServletRequestWrapper {

    private HttpServletRequest request;
    private byte[] rawData;
    private ResettableServletInputStream servletInputStream;

    public HomeplusSpecialCharacterFilterWrapper(ServletRequest request) throws IOException {
        super((HttpServletRequest) request);
        this.request = (HttpServletRequest) request;
        this.servletInputStream = new ResettableServletInputStream();

        resetInputStream();
    }

     /**
     * application/x-www-form-urlencoded 의 경우
     * getParameter, getParameterValues 메소드가 호출되며
     * applications/json 의 경우
     * 해당 메소드를 통해 inputStream 을 제어해주어야 한다.
     */
    private void resetInputStream() throws IOException {
        if (!isMultipartRequest()) {
            String requestBody = IOUtils.toString(getReader());
            if (StringUtils.isNotEmpty(requestBody)) {
                String contentType = request.getContentType();
                if (contentType.equals("application/json")) {
                    requestBody = replaceSpecialCharacter(requestBody);
                }
            }
            servletInputStream.inputStream = new ByteArrayInputStream(requestBody.getBytes("UTF-8"));
        }
    }

    /**
     * request 내 단일 파라미터를 확인해 특수문자를 치환한다.
     *
     * @return String
     */
    @Override
    public String getParameter(String param) {
        String originalData = super.getParameter(param);
        if (isMultipartRequest() || originalData == null) {
            return originalData;
        }

        return replaceSpecialCharacter(originalData);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (rawData == null) {
            rawData = IOUtils.toByteArray(this.request.getReader(), "UTF-8");
            servletInputStream.inputStream = new ByteArrayInputStream(rawData);
        }

        return servletInputStream;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        if (rawData == null) {
            rawData = IOUtils.toByteArray(this.request.getReader(), "UTF-8");
            servletInputStream.inputStream = new ByteArrayInputStream(rawData);
        }

        return new BufferedReader(new InputStreamReader(servletInputStream, "UTF-8"));
    }
    /**
     * request 내 복수개의 파라미터를 확인해 특수문자를 치환한다.
     *
     * @return String[]
     */
    @Override
    public String[] getParameterValues(String param) {
        String originalData[] = super.getParameterValues(param);
        if (isMultipartRequest() || originalData == null) {
            return originalData;
        }
        for (int seq = 0; seq < originalData.length; seq++) {
            originalData[seq] = replaceSpecialCharacter(originalData[seq]);
        }

        return originalData;
    }

    /**
     * Multipart Request 인지 판별하는 Method
     *
     * @return boolean
     */
    private boolean isMultipartRequest() {
        return "POST".equalsIgnoreCase(request.getMethod())
                && request.getContentType() != null
                && request.getContentType().toLowerCase(Locale.KOREAN).startsWith("multipart/");
    }

    /**
     * 4Byte 이상의 Emoji String을 ?? 로 치환하는 Method
     *
     * @return String
     */
    private static String replaceSpecialCharacter(String originalString) {
        Pattern specialCharacterPattern = Pattern.compile("[\\uD83C-\\uDBFF]+|[\\uDC00-\\uDFFF]+");
        Matcher specialCharacterMatcher = specialCharacterPattern.matcher(originalString);
        Matcher specialCharacterSecondMatcher = specialCharacterPattern.matcher(specialCharacterMatcher.replaceAll("?"));
        return specialCharacterSecondMatcher.replaceAll("?");
    }

    /**
     * 재사용 가능한 servlet input stream
     */
    private class ResettableServletInputStream extends ServletInputStream {
        private InputStream inputStream;

        @Override
        public int read() throws IOException {
            return inputStream.read();
        }

        /**
         * Returns true when all the data from the stream has been read else
         * it returns false.
         *
         * @return <code>true</code> when all data for this particular request
         * has been read, otherwise returns <code>false</code>.
         * @since Servlet 3.1
         */
        @Override
        public boolean isFinished() {
            return false;
        }

        /**
         * Returns true if data can be read without blocking else returns
         * false.
         *
         * @return <code>true</code> if data can be obtained without blocking,
         * otherwise returns <code>false</code>.
         * @since Servlet 3.1
         */
        @Override
        public boolean isReady() {
            return false;
        }

        /**
         * Instructs the <code>ServletInputStream</code> to invoke the provided
         * {@link ReadListener} when it is possible to read
         *
         * @param readListener the {@link ReadListener} that should be notified
         *                     when it's possible to read.
         * @throws IllegalStateException if one of the following conditions is true
         *                               <ul>
         *                               <li>the associated request is neither upgraded nor the async started
         *                               <li>setReadListener is called more than once within the scope of the same request.
         *                               </ul>
         * @throws NullPointerException  if readListener is null
         * @since Servlet 3.1
         */
        @Override
        public void setReadListener(ReadListener readListener) {

        }
    }
}