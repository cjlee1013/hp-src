package kr.co.homeplus.admin.web.order.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.constants.RoleConstants;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.order.constants.OrderTransfer;
import kr.co.homeplus.admin.web.order.model.orderPopup.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderCashReceiptDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderCouponDiscountDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderDelayShipInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderGiftInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderInvoiceRegGetDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderPartnerInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderPromoInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderSaveInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderShipAddrInfoDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchClaimDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchDetailDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchGetDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchHistoryDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchPaymentDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchPriceDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchProdDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchSetDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchShipDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchSubstitutionDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipRegGto;
import kr.co.homeplus.admin.web.order.model.ship.OrderInvoiceRegDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderProcessDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.admin.web.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.admin.web.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.admin.web.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.admin.web.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.admin.web.order.utils.OrderLogUtil;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryMemberMemoListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.util.ResponseObjectUtils;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.utils.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderManagementService {

    private final ResourceClient resourceClient;

    private final LoginCookieService loginCookieService;

    private final RefitCryptoService refitCryptoService;

    private final AuthorityService authorityService;

    public List<OrderSearchGetDto> getOrderMainSearch(OrderSearchSetDto searchSetDto) {
        log.debug("getOrderMainSearch ::: {}", searchSetDto);
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            searchSetDto,
            OrderTransfer.ORDER_MAIN_SEARCH,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchGetDto>>>() {}
        ).getData();
    }

    public OrderSearchProdDto getOrderDetailProdSearch(long purchaseOrderNo) {
        OrderSearchProdDto prodDto =
            resourceClient.getForResponseObject(
                ResourceRouteName.ESCROWMNG,
                OrderTransfer.ORDER_DETAIL_PRODUCT_SEARCH + purchaseOrderNo,
                new ParameterizedTypeReference<ResponseObject<OrderSearchProdDto>>() {}
                ).getData();

        if(prodDto.getNomemOrderYn().equals("N")){
            try {
                ResponseObject<UserInfoDto> userInfo =
                    resourceClient.getForResponseObject(
                        ResourceRouteName.USERMNG, "/external/api/search/userInfo?userNo=" + prodDto.getUserNo(),
                        OrderLogUtil.createPrivacySendAndHeader(PersonalLogMethod.SELECT, prodDto.getUserNo(), "주문관리-상세>회원정보조회"),
                        new ParameterizedTypeReference<>() {}
                    );
                if("0000,SUCCESS".contains(userInfo.getReturnCode())){
                    prodDto.setBuyerEmail(userInfo.getData().getUserId());
                }
            } catch (Exception e){
                prodDto.setUserNo(prodDto.getUserNo().concat("(탈퇴)"));
            }
        } else {
            prodDto.setUserNo(prodDto.getUserNo().concat("(비회원)"));
        }
        return prodDto;
    }

    public OrderSearchPriceDto getOrderDetailPriceSearch(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_PRICE_SEARCH + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<OrderSearchPriceDto>>() {}
        ).getData();
    }

    public List<OrderSearchDetailDto> getOrderDetailPurchaseSearch(long purchaseOrderNo) {
        List<OrderSearchDetailDto>  returnList =  resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_PURCHASE_SEARCH + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchDetailDto>>>() {}
        ).getData();
        return returnList;
    }

    public List<OrderSearchShipDto> getOrderDetailShipSearch(long purchaseOrderNo, String mallType) {
        List<SetParameter> parameterList = new ArrayList<>(){{
            add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
            add(SetParameter.create("mallType", mallType));
        }};

        List<OrderSearchShipDto> resultDto = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_SHIP_SEARCH +
                StringUtil.getParameter(parameterList), new ParameterizedTypeReference<ResponseObject<List<OrderSearchShipDto>>>() {}
            ).getData();

        resultDto.stream()
            .filter(dto -> org.apache.commons.lang3.StringUtils.isNotEmpty(dto.getDriverTelNo()))
            .forEach( dto -> {
                try {
                    dto.setDriverTelNo(refitCryptoService.decrypt(dto.getDriverTelNo()));
                } catch (Exception e){
                    dto.setDriverTelNo("");
                }
            });
        return resultDto;
    }

    public List<OrderSearchClaimDto> getOrderDetailClaimSearch(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_CLAIM_SEARCH + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchClaimDto>>>() {}
        ).getData();
    }

    public List<OrderSearchPaymentDto> getOrderDetailPaymentSearch(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_PAYMENT_SEARCH + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchPaymentDto>>>() {}
        ).getData();
    }

    public List<OrderShipAddrInfoDto> getOrderShipAddrInfo(long bundleNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_SHIP_ADDR_INFO_SEARCH.concat(String.valueOf(bundleNo)),
            new ParameterizedTypeReference<ResponseObject<List<OrderShipAddrInfoDto>>>() {}
        ).getData();
    }

    public List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(long multiBundleNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_MULTI_SHIP_ADDR_INFO_SEARCH.concat(String.valueOf(multiBundleNo)),
            new ParameterizedTypeReference<ResponseObject<List<MultiOrderShipAddrInfoDto>>>() {}
        ).getData();
    }

    public ResponseObject<String> setOrderProcess(long bundleNo, String shipStatus) throws Exception {
        List<OrderInvoiceRegGetDto> orderProcessList = getTransfer(bundleNo, shipStatus, OrderInvoiceRegGetDto.class);
        LinkedHashMap<String, Object> parameter = new LinkedHashMap<>();
        parameter.put("chgId", loginCookieService.getUserInfo().getEmpId());
        parameter.put("bundleNoList", new ArrayList<String>(){{ add(String.valueOf(bundleNo)); }});
        return getPostTransfer(ResourceRouteName.SHIPPING, OrderTransfer.ORDER_SHIP_CHECK, parameter, String.class);
    }

    public ResponseObject<String> setOrderInvoiceProcess(OrderInvoiceRegDto invoiceRegDto) throws Exception {
        // 처리자 ID 셋팅.
        invoiceRegDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return getPostTransfer(ResourceRouteName.SHIPPING, OrderTransfer.ORDER_INVOICE_REG_DLV, invoiceRegDto, String.class);
    }

    public ResponseObject<String> setOrderCompleteProcess(OrderProcessDto completeDto) throws Exception {
        LinkedHashMap<String, Object> parameter = new LinkedHashMap<>();
        parameter.put("chgId", loginCookieService.getUserInfo().getEmpId());
        parameter.put("bundleNoList", new ArrayList<>(){{ add(completeDto.getBundleNo()); }});
        return getPostTransfer(ResourceRouteName.SHIPPING, OrderTransfer.ORDER_INVOICE_COMPLETE, parameter, String.class);
    }

    public ResponseObject<String> setOrderShipChange(OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        shipAddrEditDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return getPostTransfer(ResourceRouteName.ESCROWMNG, OrderTransfer.ORDER_SHIP_ADDR_CHANGE, shipAddrEditDto, String.class);
    }

    public ResponseObject<String> setMultiOrderAddrChange(MultiOrderShipAddrInfoDto shipAddrEditDto) throws Exception {
        shipAddrEditDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return getPostTransfer(ResourceRouteName.ESCROWMNG, OrderTransfer.ORDER_MULTI_SHIP_ADDR_CHANGE, shipAddrEditDto, String.class);
    }

    public ResponseObject<String> setNoReceiveReg(NoRcvShipRegGto noRcvShipRegGto) throws Exception {
        noRcvShipRegGto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return getPostTransfer(ResourceRouteName.ESCROWMNG, OrderTransfer.ORDER_SHIP_NO_RCV_REG, noRcvShipRegGto, String.class);
    }

    public ResponseObject<String> setNoReceiveModify(NoRcvShipEditDto noRcvShipEditDto) throws Exception {
        noRcvShipEditDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return getPostTransfer(ResourceRouteName.ESCROWMNG, OrderTransfer.ORDER_SHIP_NO_RCV_MODIFY, noRcvShipEditDto, String.class);
    }

    public ResponseObject<NoRcvShipInfoGetDto> getNoReceiveInfo(NoRcvShipInfoSetDto noRcvShipInfoSetDto) throws Exception {
        return getPostTransfer(ResourceRouteName.ESCROWMNG, OrderTransfer.ORDER_SHIP_NO_RCV_INFO, noRcvShipInfoSetDto, NoRcvShipInfoGetDto.class);
    }

    public ResponseObject<String> setShipSlotChangeModify(ShipSlotStockChangeDto changeDto) throws Exception {
        changeDto.setHistoryRegId(loginCookieService.getUserInfo().getEmpId());
        changeDto.setHistoryRegChannel("ADMIN");
        return getPostTransfer(ResourceRouteName.ESCROW, OrderTransfer.ORDER_SHIP_SLOT_CHANGE_MODIFY, changeDto, String.class);
    }

    public ResponseObject<FrontStoreSlot> getShipSlotInfo(long purchaseOrderNo, long bundleNo) throws Exception {
        return getTransfer(
            ResourceRouteName.ESCROW,
            OrderTransfer.ORDER_SHIP_SLOT_CHANGE_INFO,
            new ArrayList<SetParameter>(){{
                add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
                add(SetParameter.create("bundleNo", bundleNo));
            }},
            FrontStoreSlot.class
        );
    }

    public OrderDelayShipInfoDto getOrderDelayShippingInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DELAY_SHIPPING_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<OrderDelayShipInfoDto>>() {}
        ).getData();
    }

    public OrderPartnerInfoDto getOrderPartnerInfo(String partnerId, long bundleNo) throws Exception {
        OrderPartnerInfoDto returnDto =
            getTransfer(
                ResourceRouteName.ESCROWMNG,
                OrderTransfer.ORDER_PARTNER_INFO,
                new ArrayList<SetParameter>(){{
                    add(SetParameter.create("partnerId", partnerId));
                    add(SetParameter.create("bundleNo", bundleNo));
                }},
                OrderPartnerInfoDto.class
            ).getData();
        if(returnDto.getMngPhone().length() > 14){
            returnDto.setMngPhone(refitCryptoService.decrypt(returnDto.getMngPhone()));
        }
        return returnDto;
    }

    public List<OrderPromoInfoDto> getOderPromoPopInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_PROMOTION_POP_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderPromoInfoDto>>>() {}
        ).getData();
    }

    public List<OrderCouponDiscountDto> getOrderCouponDiscountInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_COUPON_DISCOUNT_POP_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderCouponDiscountDto>>>() {}
        ).getData();
    }

    public List<OrderGiftInfoDto> getOderGiftPopInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_GIFT_POP_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderGiftInfoDto>>>() {}
        ).getData();
    }

    public List<OrderSearchSubstitutionDto> getOrderSubstitutionInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_DETAIL_SUBSTITUTION_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchSubstitutionDto>>>() {}
        ).getData();
    }

    public List<OrderSaveInfoDto> orderSaveInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_SAVE_POP_INFO + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSaveInfoDto>>>() {}
        ).getData();
    }

    public List<OneOnOneInquiryMemberMemoListGetDto> getUserMemo(String userNo) {
        ResponseObject<List<OneOnOneInquiryMemberMemoListGetDto>> listDto =
            resourceClient.getForResponseObject(
                ResourceRouteName.USERMNG,
                "/admin/oneOnOneInquiry/getInquiryMemberMemoList?userNo=" + userNo,
                OrderLogUtil.createPrivacySendAndHeader(PersonalLogMethod.SELECT, userNo, "회원메모 조회")
            );
        return listDto.getData();
    }

    public ResponseObject<Integer> getPresentReSend(long purchaseOrderNo){
        try {
            return resourceClient.getForResponseObject(
                ResourceRouteName.ESCROW,
                OrderTransfer.ORDER_PRESENT_RE_SEND.concat("?purchaseOrderNo=") + purchaseOrderNo,
                new ParameterizedTypeReference<ResponseObject<Integer>>() {}
            );
        } catch (ResourceClientException rce){
            ResponseObject responseObject = ResponseObjectUtils.parse(StringUtils.fromBytes(rce.getResponseBody(), StandardCharsets.UTF_8), ResponseObject.class);
            return ResponseObject.Builder.<Integer>builder().returnCode(responseObject.getReturnCode()).data(0).returnMessage(responseObject.getReturnMessage()).build();
        }
    }

    public ResponseObject<StoreShipMemoDto> getOrderStoreShipMemoInfo(long purchaseOrderNo, long bundleNo){
        List<SetParameter> parameter = new ArrayList<>();
        parameter.add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
        parameter.add(SetParameter.create("bundleNo", bundleNo));
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_STORE_SHIP_MEMO_INFO + StringUtil.getParameter(parameter),
            new ParameterizedTypeReference<ResponseObject<StoreShipMemoDto>>() {}
        );
    }

    public ResponseObject<StoreShipMsgDto> getOrderStoreShipMsgInfo(long purchaseOrderNo){
        List<SetParameter> parameter = new ArrayList<>();
        parameter.add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_STORE_SHIP_MSG_INFO + StringUtil.getParameter(parameter),
            new ParameterizedTypeReference<ResponseObject<StoreShipMsgDto>>() {}
        );
    }

    public ResponseObject<String> setOrderStoreShipMemoInfo(StoreShipMemoDto memoDto){
        memoDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            memoDto,
            OrderTransfer.ORDER_STORE_SHIP_MEMO_SET,
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );
    }

    public List<OrderSearchHistoryDto> getOrderHistoryList(long purchaseOrderNo){
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            "/order/orderSearch/getOrderHistoryList?purchaseOrderNo=" + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<List<OrderSearchHistoryDto>>>() {}
        ).getData();
    }

    public <T> List<T> getTransfer(long bundleNo, String shipStatus, Class<T> returnClass) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("bundleNo", bundleNo));
        setParameterList.add(SetParameter.create("shipStatus", shipStatus));
        return getTransfer(setParameterList, returnClass);
    }

    /**
     * StoreNm(점포명) 조회
     */
    public String getStoreNm(String storeId) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.SHIPPING,
            "/admin/shipManage/getStoreNm?storeId=" + storeId,
            new ParameterizedTypeReference<ResponseObject<String>>() {}).getData();
    }

    /**
     * 점포정보 조회
     */
    public StoreInfoGetDto getStoreInfo(String storeId) {
      return resourceClient.getForResponseObject(
          ResourceRouteName.ESCROWMNG,
          "/claim/getStoreInfo?storeId=" + storeId,
          new ParameterizedTypeReference<ResponseObject<StoreInfoGetDto>>() {}).getData();
    }

    public ResponseObject<OrderCashReceiptDto> getOrderCashReceiptInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_CASH_RECEIPT_POP_INFO + "?purchaseOrderNo=" + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<OrderCashReceiptDto>>() {}
        );
    }

    public ResponseObject<OrderShipOriginInfo> getOrderShipOriginInfo(long purchaseOrderNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_SHIP_ORIGIN_INFO + "?purchaseOrderNo=" + purchaseOrderNo,
            new ParameterizedTypeReference<ResponseObject<OrderShipOriginInfo>>() {}
        );
    }

    public Boolean getOrderTicketCancelRoleCheck() {
        return authorityService.hasRole(RoleConstants.ORDER_TICKET_CANCEL);
    }

    public ResponseObject<String> setMultiShipStatusChange(long purchaseOrderNo) throws Exception {
        log.info("{}", purchaseOrderNo);
        LinkedHashMap<String, Object> parameter = new LinkedHashMap<>();
        parameter.put("purchaseOrderNo", purchaseOrderNo);
        return getPostTransfer(ResourceRouteName.SHIPPING, OrderTransfer.ORDER_SHIP_STATUS_CHANGE, parameter, String.class);
    }

    private <T> List<T> getTransfer(List<SetParameter> parameter, Class<T> returnClass) throws Exception {
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        ParameterizedTypeReference<ResponseObject<List<T>>> typeReference = ParameterizedTypeReference.forType(resolvableType.getType());
        ResponseObject<List<T>> responseObject = null;
        return resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG,
            OrderTransfer.ORDER_SHIP_TOTAL_INFO_SEARCH + StringUtil.getParameter(parameter),
            typeReference
        ).getData();
    }

    private <T> ResponseObject<T> getPostTransfer(String apiId, String url, Object parameter, Class<T> returnClass) throws Exception {
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        ParameterizedTypeReference<ResponseObject<T>> typeReference = ParameterizedTypeReference.forType(resolvableType.getType());
        return resourceClient.postForResponseObject( apiId, parameter, url, typeReference );
    }

    @SuppressWarnings("unchecked")
    private <T> ResponseObject<T> getTransfer(String apiId, String url, Object parameter, Class<T> returnClass) throws Exception {
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        ParameterizedTypeReference<ResponseObject<T>> typeReference = ParameterizedTypeReference.forType(resolvableType.getType());
        return resourceClient.getForResponseObject( apiId, url + StringUtil.getParameter((List<SetParameter>) parameter), typeReference );
    }

    @SuppressWarnings("unchecked")
    private <T> T parsingResponseObject(ResponseObject<T> responseObject) throws Exception {
        // responseObject Null Check
        assert responseObject != null;
        if(responseObject.getReturnCode().equalsIgnoreCase("0000") || responseObject.getReturnCode().equalsIgnoreCase("SUCCESS")) {
            return responseObject.getData();
        } else {
            return (T) ResponseObject.Builder.builder().returnCode(responseObject.getReturnCode()).returnMessage(responseObject.getReturnMessage());
        }
    }

}
