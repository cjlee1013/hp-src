package kr.co.homeplus.admin.web.manage.model.dspSearchBar;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspSearchBarSetDto {

    private Long keywordNo;
    private String siteType = "HOME";
    private String locCd;
    private String pcUseYn = "N";
    private String mobileUseYn = "N";
    private String keyword;
    private short weight;
    private short priority;
    private String dispStartDt;
    private String dispEndDt;
    private String useYn;
    private String linkType;
    private String linkInfo;
    private String linkOptions;
    private String regId;

}