package kr.co.homeplus.admin.web.item.model.item.hist;


import java.util.Optional;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemHistGroupStringGetDto {
    // 기본정보 (심사/합의정보 포함)
    private Optional<String> basic = Optional.empty();
    // 판매정보
    private Optional<String> sale = Optional.empty();
    // 배송정보
    private Optional<String> ship = Optional.empty();
    // 옵션/자재 정보
    private Optional<String> optItem = Optional.empty();
    // 이미지정보
    private Optional<String> img = Optional.empty();
    // 고시정보
    private Optional<String> notice = Optional.empty();
    // 부가정보
    private Optional<String> etc = Optional.empty();
}
