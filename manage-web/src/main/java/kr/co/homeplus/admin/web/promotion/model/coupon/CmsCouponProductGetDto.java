package kr.co.homeplus.admin.web.promotion.model.coupon;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "CmsCouponProductGetDto", description = "쿠폰 상품 조회 결과 Dto")
@Getter
@Setter
public class CmsCouponProductGetDto {
    /*
    private String trgtSeq;         //타켓 일련번호
    private String trgtNm;          //타켓명
    private String couponNm;        //쿠폰명
    private String couponSeq;       //쿠폰 일련번호
    private String couponCd;        //쿠폰 코드
    private String couponTypeNm;
    private String couponValue;
    private String couponCondition;
    private String incUnitCd;
    private String couponTypeGb;

     */


    private String pluCd;           //단품코드
    private String pluNm;           //단품명

    private String trgtSeq;         //타켓 일련번호
    private String trgtNm;          //타켓명
    private String couponNm;        //쿠폰명
    private String couponSeq;       //쿠폰 일련번호
    private String couponCd;        //쿠폰 코드
    private String vendorCd;        //밴더 코드
    private String vendorNm;        //밴더 명
    private String div;             //div명
    private String dept;            //dept명
    private String sectionNm;       //section명
    private String classNm;         //class명
    private String subclassNm;      //subclass명


    private String couponValue;
    private String itemType;

}
