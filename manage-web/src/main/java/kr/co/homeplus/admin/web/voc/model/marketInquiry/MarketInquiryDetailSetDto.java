package kr.co.homeplus.admin.web.voc.model.marketInquiry;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 제휴사 고객문의 상세 요청 DTO")
public class MarketInquiryDetailSetDto {
  private Long messageNo; //문의 번호
  private String marketType; //제휴사코드
  private String messageType; //등록구분코드
}