package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DictionaryWordHistoryGetDto {
    private Long historyId;
    private Integer updateType;
    private String updateTypeNm;
    private String updateDate;
    private String updateUserid;
    private String note;
}
