package kr.co.homeplus.admin.web.core.config;

import brave.Tracer;
import kr.co.homeplus.admin.web.core.interceptor.SleuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

	@Autowired
	private Tracer tracer;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(sleuthInterceptor()).addPathPatterns("/**/*");
	}

	public SleuthInterceptor sleuthInterceptor() {
		return new SleuthInterceptor(tracer);
	}
}
