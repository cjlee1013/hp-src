package kr.co.homeplus.admin.web.mileage.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum MileageCategory {
    //마일리지 증감 구분 mileage_category
    MILEAGE_CATEGORY_SAVE("01", "적립"),
    MILEAGE_CATEGORY_USE("02", "사용"),
    MILEAGE_CATEGORY_CANCEL("03", "취소"),
    MILEAGE_CATEGORY_RETURN("04", "회수"),
    MILEAGE_CATEGORY_EXPIRE("05", "소멸");

    private String code;
    private String name;
}