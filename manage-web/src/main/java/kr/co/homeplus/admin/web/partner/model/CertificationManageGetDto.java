package kr.co.homeplus.admin.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CertificationManageGetDto {

    private String certificationSeq;
    private String certificationMngNm;
    private String certificationMngPhone;

}
