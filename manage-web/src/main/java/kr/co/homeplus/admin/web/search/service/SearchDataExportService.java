package kr.co.homeplus.admin.web.search.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.view.excel.ExcelBindInfo;
import kr.co.homeplus.admin.web.search.model.SearchDataExportParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SearchDataExportService {
    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final ResourceClient resourceClient;

    public ExcelBindInfo getExcelData(SearchDataExportParamDto param) {
        StringBuilder apiUri = new StringBuilder("/searchData/getCategorySearchListResult");
        apiUri.append("?storeType=" + param.getStoreType());
        apiUri.append("&storeId=" + param.getStoreId());
        apiUri.append("&sort=" + param.getSort());
        apiUri.append("&includeFields=" + param.getIncludeFields());
        String[] includeFields = param.getIncludeFields().split(",");

        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri.toString())
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        Map<String,Object> responseData = (Map<String, Object>) resourceClient.get(request, new TimeoutConfig()).getBody().getData();

        ExcelBindInfo excelBindInfo = new ExcelBindInfo();
        excelBindInfo.setDownloadExcelFileName(param.getStoreNm() + "_" + profilesActive + "_상품목록_" + LocalDateTime.now());
        excelBindInfo.setSheetName("상품목록");
        Map<Integer, Integer> columnWidths = new HashMap<>();
        String[] header = new String[includeFields.length];
        int fieldCnt = 0;
        for (String field : includeFields) {
            if (field.equals("itemNo")) {
                columnWidths.put(fieldCnt,5000);
                header[fieldCnt++] = "상품번호";
            } else if (field.equals("itemNm")) {
                columnWidths.put(fieldCnt,15000);
                header[fieldCnt++] = "상품명";
            } else if (field.equals("soldOutYn")) {
                columnWidths.put(fieldCnt,3000);
                header[fieldCnt++] = "품절여부";
            }
        }
        excelBindInfo.setCellHeaders(header);
        excelBindInfo.setColumnWidths(columnWidths);

        for (Map<String,Object> data : (List<Map<String,Object>>) responseData.get("dataList")) {
            String[] row = new String[includeFields.length];
            fieldCnt = 0;
            for (String field : includeFields) {
                row[fieldCnt++] = data.get(field).toString();
            }
            excelBindInfo.addRows(row);
        }
        return excelBindInfo;
    }
}
