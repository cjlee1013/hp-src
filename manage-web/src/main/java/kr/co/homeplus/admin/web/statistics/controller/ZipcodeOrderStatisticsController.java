package kr.co.homeplus.admin.web.statistics.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.admin.web.statistics.model.ZipcodeOrderStatistics;
import kr.co.homeplus.admin.web.statistics.model.ZipcodeOrderStatisticsSelect;
import kr.co.homeplus.admin.web.statistics.service.RealtimeOrderStatisticsService;
import kr.co.homeplus.admin.web.statistics.service.ZipcodeOrderStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 점포 상품 통계 > 우편번호별 주문현황
 */
@Controller
@RequestMapping("/statistics")
public class ZipcodeOrderStatisticsController {
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;
    private final ZipcodeOrderStatisticsService zipcodeOrderStatisticsService;
    private final RealtimeOrderStatisticsService realtimeOrderStatisticsService;

    public ZipcodeOrderStatisticsController(LoginCookieService loginCookieService, CodeService codeService,
            ZipcodeOrderStatisticsService zipcodeOrderStatisticsService, RealtimeOrderStatisticsService realtimeOrderStatisticsService) {
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
        this.zipcodeOrderStatisticsService = zipcodeOrderStatisticsService;
        this.realtimeOrderStatisticsService = realtimeOrderStatisticsService;
    }

    /**
     * 우편번호별 주문현황 리스트 메인
     */
    @GetMapping("/zipcodeOrderStatisticsMain")
    public String zipcodeOrderStatisticsMain(Model model) throws Exception {
        List<MngCodeGetDto> storeTypeList = new ArrayList<>();
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();
        StoreBasicInfoDto storeBasicInfoDto = new StoreBasicInfoDto();

        // 로그인 사용자 점포 ID 있을 경우 점포 기본정보 조회
        String userStoreId = loginCookieService.getUserInfo().getStoreId();
        if (StringUtil.isNotEmpty(userStoreId)) {
            storeBasicInfoDto = realtimeOrderStatisticsService.getStoreBasicInfo(userStoreId);
        }
        // 조회한 점포 기본정보가 있을 경우 해당 점포만 세팅, 없을 경우 점포유형 HYPER/EXP 세팅
        if (StringUtil.isNotEmpty(storeBasicInfoDto) && StringUtil.isNotEmpty(storeBasicInfoDto.getStoreId())) {
            mngCodeGetDto.setMcCd(storeBasicInfoDto.getStoreType());
            mngCodeGetDto.setMcNm(storeBasicInfoDto.getStoreType());
            mngCodeGetDto.setRef1("home");
            storeTypeList.add(mngCodeGetDto);
            model.addAttribute("userStoreId", storeBasicInfoDto.getStoreId());
            model.addAttribute("userStoreNm", storeBasicInfoDto.getStoreNm());
        } else {
            Map<String, List<MngCodeGetDto>> code = codeService.getCode("store_type");
            storeTypeList = code.get("store_type");
        }

        model.addAttribute("storeType", storeTypeList);
        model.addAllAttributes(RealGridHelper.create("zipcodeOrderStatisticsBaseInfo", ZipcodeOrderStatistics.class));
        return "/statistics/zipcodeOrderStatisticsMain";
    }

    /**
     * 우편번호별 주문현황 리스트 조회
     */
    @ResponseBody
    @PostMapping("/getZipcodeOrderStatisticsList.json")
    public List<ZipcodeOrderStatistics> getZipcodeOrderStatisticsList(@RequestBody ZipcodeOrderStatisticsSelect zipcodeOrderStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.ZIPCODE_ORDER_STAT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        zipcodeOrderStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        return zipcodeOrderStatisticsService.getZipcodeOrderStatisticsList(zipcodeOrderStatisticsSelect);
    }

    /**
     * 우편번호별 주문현황 엑셀다운
     * - 단순 history 적재용
     */
    @ResponseBody
    @PostMapping("/saveZipcodeOrderStatExtractHistoryForExcelDown.json")
    public void saveZipcodeOrderStatExtractHistoryForExcelDown() {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.ZIPCODE_ORDER_STAT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());

        zipcodeOrderStatisticsService.saveExtractHistory(extractCommonSetDto);
    }
}
