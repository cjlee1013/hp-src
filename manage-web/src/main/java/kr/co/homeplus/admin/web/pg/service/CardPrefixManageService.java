package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.CardPrefixManageDto;
import kr.co.homeplus.admin.web.pg.model.CardPrefixManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CardPrefixManageService {
    private static final String PARENT_METHOD_CD_CARD = "CARD";
    private final ResourceClient resourceClient;

    /**
     * 신용카드 결제수단을 조회
     * Site_type중 중복된 신용카드 결제수단을 제거한 후 반환
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getDistinctMethodCd() throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("parentMethodCd", PARENT_METHOD_CD_CARD));
        String apiUri = EscrowConstants.ESCROW_GET_DISTINCT_METHOD_CD + StringUtil.getParameter(setParameterList);

        ResponseObject<List<PaymentMethodDto>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<>() {}
        );
        return responseObject.getData();
    }

    /**
     * 카드 PREFIX 리스트 조회
     * @param cardPrefixManageSelectDto
     * @return
     * @throws Exception
     */
    public List<CardPrefixManageDto> getCardPrefixManageList(CardPrefixManageSelectDto cardPrefixManageSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_CARD_PREFIX_MANAGE, CardPrefixManageSelectDto.class, cardPrefixManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CardPrefixManageDto>>>() {};
        return (List<CardPrefixManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();

    }

    /**
     * 카드 PREFIX 저장/수정
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveCardPrefixManage(CardPrefixManageDto cardPrefixManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_CARD_PREFIX_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, cardPrefixManageDto, apiUri, typeReference);
    }

    /**
     * 카드 PREFIX 결제수단 일괄변경
     * @param cardPrefixManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveBundleCardPrefixMethod(CardPrefixManageDto cardPrefixManageDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_BUNDLE_CARD_PREFIX_METHOD;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, cardPrefixManageDto, apiUri, typeReference);
    }
}
