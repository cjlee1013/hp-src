package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class KeywordRankGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
    public static class KeywordItemList{
        @RealGridColumnInfo(headText = "상품번호", width = 120, columnType = RealGridColumnType.BASIC, sortable = true)
        private String itemNo;
        @RealGridColumnInfo(headText = "상품명", width = 250, columnType = RealGridColumnType.BASIC, sortable = true)
        private String itemNm;
        @RealGridColumnInfo(headText = "판매시작일", columnType = RealGridColumnType.DATE, sortable = true)
        private String saleStartDt;
        @RealGridColumnInfo(headText = "판매종료일", columnType = RealGridColumnType.DATE, sortable = true)
        private String saleEndDt;
        @RealGridColumnInfo(headText = "상품유형", columnType = RealGridColumnType.BASIC, sortable = true)
        private String itemTypeNm;
        @RealGridColumnInfo(headText = "상품상태", columnType = RealGridColumnType.BASIC, sortable = true)
        private String itemStatusNm;
        @RealGridColumnInfo(headText = "노출여부", columnType = RealGridColumnType.BASIC, sortable = true)
        private String dispYnNm;
        @RealGridColumnInfo(headText = "대분류", columnType = RealGridColumnType.BASIC, sortable = true)
        private String lcateNm;
        @RealGridColumnInfo(headText = "중분류", columnType = RealGridColumnType.BASIC, sortable = true)
        private String mcateNm;
        @RealGridColumnInfo(headText = "소분류", columnType = RealGridColumnType.BASIC, sortable = true)
        private String scateNm;
        @RealGridColumnInfo(headText = "세분류", columnType = RealGridColumnType.BASIC, sortable = true)
        private String dcateNm;
        @RealGridColumnInfo(headText = "등록자", columnType = RealGridColumnType.BASIC, sortable = true)
        private String regNm;
        @RealGridColumnInfo(headText = "등록일", width = 150, columnType = RealGridColumnType.DATETIME, sortable = true)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", columnType = RealGridColumnType.BASIC, sortable = true)
        private String chgNm;
        @RealGridColumnInfo(headText = "수정일", width = 150, columnType = RealGridColumnType.DATETIME, sortable = true)
        private String chgDt;
    }
}
