package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspExpMainListParamDto {

    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchUseYn;
    private String searchType;
    private String searchLoc;
    private String searchKeyword;

}
