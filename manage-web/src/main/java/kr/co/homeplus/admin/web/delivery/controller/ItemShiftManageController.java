package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.EXP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.Constants;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageInsertModel;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.ItemShiftManageSelectModel;
import kr.co.homeplus.admin.web.delivery.service.ItemShiftManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 배송관리 > 점포배송정보 > 상품별 Shift 관리
 */
@Controller
@RequestMapping("/escrow")
public class ItemShiftManageController {

    private final AuthorityService authorityService;
    @Autowired
    private LoginCookieService loginCookieService;;
    @Autowired
    private CodeService codeService;
    @Autowired
    private ItemShiftManageService itemShiftManageService;
    @Autowired
    private ExcelUploadService excelUploadService;

    private static final String PATTERN_SHIFT_YN = "(?i)(Y|N)";
    private static final String PATTERN_STORE_TYPE = "(?i)(HYPER|CLUB|EXPRESS)";
    private static final List<RealGridColumn> ITEM_SHIFT_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption ITEM_SHIFT_MANAGE_GRID_OPTION = new RealGridOption("fill", false, true, false);

    public ItemShiftManageController(AuthorityService authorityService) {
        this.authorityService = authorityService;

        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 80, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("itemNm", "itemNm", "상품명", RealGridColumnType.BASIC, 150, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("storeType", "storeType", "점포유형", RealGridColumnType.BASIC, 80, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("storeId", "storeId", "적용점포", RealGridColumnType.BASIC, 80, false, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("storeNm", "storeNm", "적용점포명", RealGridColumnType.BASIC, 80, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift1", "shift1", "Shift1", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift2", "shift2", "Shift2", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift3", "shift3", "Shift3", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift4", "shift4", "Shift4", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift5", "shift5", "Shift5", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift6", "shift6", "Shift6", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift7", "shift7", "Shift7", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift8", "shift8", "Shift8", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("shift9", "shift9", "Shift9", RealGridColumnType.BASIC, 50, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.BASIC, 100, true, true));
        ITEM_SHIFT_MANAGE_GRID_HEAD.add(new RealGridColumn("chgId", "chgId", "수정자", RealGridColumnType.BASIC, 80, true, true));
    }

    /**
     * 상품별 Shift 관리 Main Page
     */
    @GetMapping("/storeDelivery/itemShiftManageMain")
    public String itemShiftManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }

        // 점포유형 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"
        );
        List<MngCodeGetDto> storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> !EXP.getCode()
                        .equals(st.getMcCd()))
                .collect(Collectors.toList());
        model.addAttribute("storeType", storeTypeList);
        model.addAttribute("itemShiftManageGridBaseInfo", new RealGridBaseInfo("itemShiftManageGridBaseInfo", ITEM_SHIFT_MANAGE_GRID_HEAD, ITEM_SHIFT_MANAGE_GRID_OPTION).toString());
        return "/delivery/itemShiftManageMain";
    }

    /**
     * 상품별 Shift 관리 리스트 조회
     * @param itemShiftManageSelectModel
     * @return
     */
    @ResponseBody
    @GetMapping("/storeDelivery/getItemShiftManageList.json")
    public List<ItemShiftManageModel> getItemShiftManageList(@ModelAttribute ItemShiftManageSelectModel itemShiftManageSelectModel) {
        return itemShiftManageService.getItemShiftManageList(itemShiftManageSelectModel);
    }

    /**
     * 단일상품 Shift 등록 (다중점포)
     * @param itemShiftManageInsertModelList
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/storeDelivery/saveItemShiftList.json")
    public ResponseObject<Integer> saveItemShiftList(@RequestBody List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        for (ItemShiftManageInsertModel itemShiftManageInsertModel : itemShiftManageInsertModelList) {
            itemShiftManageInsertModel.setRegId(userCd);
            itemShiftManageInsertModel.setChgId(userCd);
        }

        return itemShiftManageService.saveItemShiftList(itemShiftManageInsertModelList);
    }

    /**
     * 단일상품 Shift 수정 (단일점포)
     * @param itemShiftManageInsertModel
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/storeDelivery/saveItemShift.json")
    public ResponseObject<Integer> saveItemShift(@RequestBody ItemShiftManageInsertModel itemShiftManageInsertModel) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        itemShiftManageInsertModel.setRegId(userCd);
        itemShiftManageInsertModel.setChgId(userCd);

        return itemShiftManageService.saveItemShift(itemShiftManageInsertModel);
    }

    /**
     * 상품별 Shift 관리 리스트 삭제
     * @param itemShiftManageInsertModelList
     * @return
     */
    @ResponseBody
    @PostMapping("/storeDelivery/deleteItemShiftList.json")
    public void deleteItemShiftList(@RequestBody List<ItemShiftManageInsertModel> itemShiftManageInsertModelList) {
        itemShiftManageService.deleteItemShiftList(itemShiftManageInsertModelList);
    }

    /**
     * 엑셀 일괄등록 팝업창
     * @param model
     * @param templateUrl
     * @param callBackScript
     * @return
     */
    @GetMapping("/popup/excelInsertPop")
    public String excelInsertPop(Model model
                                , @RequestParam(value = "templateUrl", defaultValue = "") String templateUrl
                                , @RequestParam(value = "callback", defaultValue = "") String callBackScript) {
        model.addAttribute("templateUrl", templateUrl);
        model.addAttribute("callback", callBackScript);
        return "/delivery/pop/excelInsertPop";
    }

    /**
     * 엑셀 일괄수정 팝업창
     * @param model
     * @param templateUrl
     * @param callBackScript
     * @return
     */
    @GetMapping("/popup/excelUpdatePop")
    public String excelUpdatePop(Model model
                                , @RequestParam(value = "templateUrl", defaultValue = "") String templateUrl
                                , @RequestParam(value = "callback", defaultValue = "") String callBackScript) {
        model.addAttribute("templateUrl", templateUrl);
        model.addAttribute("callback", callBackScript);
        return "/delivery/pop/excelUpdatePop";
    }

    /**
     * 엑셀 일괄등록, 일괄수정 적용
     */
    @ResponseBody
    @PostMapping("/storeDelivery/saveItemShiftListForExcel.json")
    public Map<String, Integer> saveItemShiftListForExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<ItemShiftManageInsertModel> result;

        /* 1. 엑셀 메타정보에 매핑정보 입력 */
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품번호", "itemNo", true)
            .header(1, "점포유형", "storeType", true)
            .header(2, "점포코드", "storeId", true)
            .header(3, "Shift1", "shift1", false)
            .header(4, "Shift2", "shift2", false)
            .header(5, "Shift3", "shift3", false)
            .header(6, "Shift4", "shift4", false)
            .header(7, "Shift5", "shift5", false)
            .header(8, "Shift6", "shift6", false)
            .header(9, "Shift7", "shift7", false)
            .header(10, "Shift8", "shift8", false)
            .header(11, "Shift9", "shift9", false)
            .build();
        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<ItemShiftManageInsertModel> excelUploadOption = new ExcelUploadOption.Builder<>(
            ItemShiftManageInsertModel.class, headers, 1, 0)
            .build();

        /* 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.) */
        try {
            result = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1013);
        }

        /* 3. 매핑된 데이터에 대한 검증 */
        Integer totalCnt = result.size();
        Integer successCnt = 0;
        Integer errorCnt = 0;
        //  3-1. 데이터 검증하여 실제 insert 할 model list 생성
        List<ItemShiftManageInsertModel> itemShiftManageInsertModelList = new ArrayList<>();
        for (ItemShiftManageInsertModel itemShiftManageInsertModel : result) {
            // 1. 상품번호 - 빈 값 체크
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getItemNo())) {
                errorCnt++; break;
            }
            // 2. 상품번호 - 자릿수 체크 (varchar 25)
            if (itemShiftManageInsertModel.getItemNo().length() > 25) {
                errorCnt++; break;
            }
            // 3. 점포유형 - 빈 값 체크
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getStoreType())) {
                errorCnt++; break;
            }
            // 4. 점포유형 - 패턴체크 (HYPER|CLUB|EXPRESS)
            if (!itemShiftManageInsertModel.getStoreType().matches(PATTERN_STORE_TYPE)) {
                errorCnt++; break;
            }
            // 5. 점포ID - 빈 값 체크
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getStoreId())) {
                errorCnt++; break;
            }
            // 6. 점포ID - 자릿수 체크 (varchar 10)
            if (itemShiftManageInsertModel.getStoreId().length() > 10) {
                errorCnt++; break;
            }
            // 7. Shift1~9 빈 값은 N 으로 변경
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift1())) { itemShiftManageInsertModel.setShift1(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift2())) { itemShiftManageInsertModel.setShift2(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift3())) { itemShiftManageInsertModel.setShift3(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift4())) { itemShiftManageInsertModel.setShift4(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift5())) { itemShiftManageInsertModel.setShift5(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift6())) { itemShiftManageInsertModel.setShift6(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift7())) { itemShiftManageInsertModel.setShift7(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift8())) { itemShiftManageInsertModel.setShift8(Constants.USE_N); }
            if (StringUtils.isEmpty(itemShiftManageInsertModel.getShift9())) { itemShiftManageInsertModel.setShift9(Constants.USE_N); }
            // 8. shift1~9 패턴체크 (Y|N)
            if (!itemShiftManageInsertModel.getShift1().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift2().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift3().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift4().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift5().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift6().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift7().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift8().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }
            if (!itemShiftManageInsertModel.getShift9().matches(PATTERN_SHIFT_YN)) { errorCnt++; break; }

            // 검증 모두 통과하면 data insert 할 array 에 담기
            successCnt++;
            itemShiftManageInsertModelList.add(itemShiftManageInsertModel);
        }

        /* 4. 정상 데이터 insert, update 진행 */
        this.saveItemShiftList(itemShiftManageInsertModelList);

        /* 5. return 객체 생성 */
        Map<String, Integer> rtnMap = new HashMap<>();
        rtnMap.put("totalCnt", totalCnt);
        rtnMap.put("successCnt", successCnt);
        rtnMap.put("errorCnt", errorCnt);

        return rtnMap;
    }
}
