package kr.co.homeplus.admin.web.settle.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 상세")
public class BalanceSheetDetailGetDto {
  @ApiModelProperty(notes = "기준일")
  @RealGridColumnInfo(headText = "기준일", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "전표유형")
  @RealGridColumnInfo(headText = "전표유형", width = 100)
  private String slipName;

  @ApiModelProperty(notes = "계정코드")
  @RealGridColumnInfo(headText = "계정코드", width = 100)
  private String accountCode;

  @ApiModelProperty(notes = "계정과목명")
  @RealGridColumnInfo(headText = "계정과목명", width = 150)
  private String accountName;

  @ApiModelProperty(notes = "코스트센터")
  @RealGridColumnInfo(headText = "코스트센터", width = 100)
  private String costCenter;

  @ApiModelProperty(notes = "코스트센터명")
  @RealGridColumnInfo(headText = "코스트센터명", width = 150)
  private String costCenterName;

  @ApiModelProperty(notes = "카테고리구분")
  @RealGridColumnInfo(headText = "카테고리구분", width = 100)
  private String category;

  @ApiModelProperty(notes = "차변")
  @RealGridColumnInfo(headText = "차변", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String drAmt;

  @ApiModelProperty(notes = "대변")
  @RealGridColumnInfo(headText = "대변", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String crAmt;

  @ApiModelProperty(notes = "적요")
  @RealGridColumnInfo(headText = "적요", width = 200)
  private String description;
}
