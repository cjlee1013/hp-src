package kr.co.homeplus.admin.web.manage.model.recomSellerShop;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecomSellerShopSetParamDto {

    private Integer recommendNo;

    private String partnerId;

    private String recommendType;

    private String recommendUseYn;

    private List<RecomSellerShopItemSelectDto> itemList;

    private String regId;

}
