package kr.co.homeplus.admin.web.item.model.categoryGroup;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryGroupLcateParamDto {

    private Integer gcateCd;

    private List<CategoryGroupLcateGetDto> lcateList;


}
