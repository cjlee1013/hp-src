package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/***
 * 개체명 유형 : 개체명 또는 단위 유형
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum NeTypeCd {
    PERSON("PERSON","인명"),
    LOCATION("LOCATION","지명"),
    ORGANIZATION("ORGANIZATION","조직명"),
    BRAND("BRAND","브랜드"),
    COUNT_UNIT("COUNT_UNIT","수량"),
    SIZE_UNIT("SIZE_UNIT","길이"),
    WEIGHT_UNIT("WEIGHT_UNIT","무게"),
    VOLUME_UNIT("VOLUME_UNIT","부피"),
    TERMPORAL_UNIT("TERMPORAL_UNIT","시간");

    private String code;
    private String name;

    public static NeTypeCd getType(String code) {
        for(NeTypeCd cd : NeTypeCd.values()) {
            if(cd.getCode().equals(code)) {
                return cd;
            }
        }
        return null;
    }
}
