package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "행사할인정보 팝업 DTO")
public class OrderPromoInfoDto {
    @ApiModelProperty(value = "점포", position = 1)
    @RealGridColumnInfo(headText = "점포", width = 120)
    private String storeNm;
    @ApiModelProperty(value = "상품번호", position = 2)
    @RealGridColumnInfo(headText = "상품번호", width = 120)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    @RealGridColumnInfo(headText = "상품명", width = 170)
    private String itemNm1;
    @ApiModelProperty(value = "행사종류", position = 4)
    @RealGridColumnInfo(headText = "행사종류", width = 130)
    private String promoNm1;
    @ApiModelProperty(value = "행사번호", position = 5)
    @RealGridColumnInfo(headText = "행사번호", width = 110)
    private String promoNo;
    @ApiModelProperty(value = "행사할인금액", position = 6)
    @RealGridColumnInfo(headText = "행사할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 120)
    private long discountAmt;
}
