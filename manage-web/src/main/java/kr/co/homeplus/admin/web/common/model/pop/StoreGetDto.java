package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 점포 정보 Get_entry
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class StoreGetDto {

	@RealGridColumnInfo(headText = "코드", sortable = true, width = 50, columnType = RealGridColumnType.NUMBER)
	private Integer storeId;

	@RealGridColumnInfo(headText = "제외 점포", sortable = true, columnType = RealGridColumnType.NAME)
	private String 	storeNm;

	@RealGridColumnInfo(headText = "구분", hidden = true)
	private String 	storeAttr;

	@RealGridColumnInfo(headText = "구분", sortable = true, width = 100, columnType = RealGridColumnType.BASIC)
	private String 	storeAttrNm;

}
