package kr.co.homeplus.admin.web.promotion.model.event;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("마일리지 행사 자동설정 조회 - 사은행사 정보")
public class MileageErsPromoGetDto {
    // 사은행사명
    private String eventNm;

    // 하한금액
    private String lowAmt;

    // 지금상품권금액
    private String ticketAmt;

    // 구매기준유형
    private String eventTargetType;

    // 지급기준유형
    private String giveType;

    // 시작일시
    private String eventStDate;

    // 종료일시
    private String eventEdDate;

    // 적용점포 리스트
    private List<ErsPromoStoreGetDto> storeList;

    // 적용상품 리스트
    private List<ErsPromoItemGetDto> itemList;
}