package kr.co.homeplus.admin.web.settle.model.resaSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "SALES 자료조회 검색조건")
public class ResaSalesListSetDto {
  @ApiModelProperty(notes = "시작일")
  private String startDt;

  @ApiModelProperty(notes = "종료일")
  private String endDt;

  @ApiModelProperty(notes = "검색조건")
  private String dateType;

  @ApiModelProperty(notes = "차이내역")
  private String diffOnly;
}
