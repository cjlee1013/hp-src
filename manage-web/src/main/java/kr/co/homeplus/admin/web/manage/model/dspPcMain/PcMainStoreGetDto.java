package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcMainStoreGetDto {
    private String storeId;
    private String storeNm;
}