package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class PcMainListSelectDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Integer mainNo;

    @RealGridColumnInfo(headText = "siteType", order = 1, hidden = true)
    private String siteType;

    @RealGridColumnInfo(headText = "사이트 구분", width = 60, order = 2, sortable = true)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "loc1depth", order = 3, hidden = true)
    private String loc1depth;

    @RealGridColumnInfo(headText = "전시위치(1depth)", width = 60, order = 4)
    private String loc1depthNm;

    @RealGridColumnInfo(headText = "siteType", order = 5, hidden = true)
    private String loc2depth;

    @RealGridColumnInfo(headText = "전시위치(2depth)", width = 60, order = 6)
    private String loc2depthNm;

    @RealGridColumnInfo(headText = "배너명", order = 7)
    private String bannerNm;

    @RealGridColumnInfo(headText = "전시 시작일", columnType = RealGridColumnType.DATETIME, width = 120, order = 8, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", columnType = RealGridColumnType.DATETIME, width = 120, order = 9, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 10, sortable = true)
    private Integer priority;

    @RealGridColumnInfo(headText = "dispYn", order = 11, hidden = true)
    private String dispYn;

    @RealGridColumnInfo(headText = "전시여부", width = 50, order = 12)
    private String dispYnNm;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 13)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 14, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 15)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 16, sortable = true)
    private String chgDt;

}
