package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.item.enums.ItemStatus;
import kr.co.homeplus.admin.web.item.enums.StoreType;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.OnlineStockMngGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemDetailHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupGetDto;
import kr.co.homeplus.admin.web.item.model.maker.MakerSchPopDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSpecHistGetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/item/popup")
public class ItemPopupController {

    /**
     * 영구/일시중지 사유입력 팝업창
     * @param itemStatus
     * @return
     */
    @RequestMapping(value = "/stopReasonPop", method = RequestMethod.POST)
    public String stopReasonPop(Model model, @RequestParam(name = "itemNo") String itemNo, @RequestParam(name = "itemStatus") String itemStatus, @RequestParam(name = "mallType") String mallType) {

        model.addAttribute("itemNo", itemNo);
        model.addAttribute("itemStatus", itemStatus);
        model.addAttribute("mallType", mallType);
        model.addAttribute("itemStatusNm", ItemStatus.valueDescFor(itemStatus));

        return "/item/pop/stopReasonPop";
    }

    /**
     * 상품 변경이력 팝업
     * @param model
     * @param itemNo
     * @return
     */
    @RequestMapping(value = "/itemHistPop", method = RequestMethod.GET)
    public String itemHistPop(Model model, @RequestParam String itemNo, @RequestParam(required = false) String storeType) {
        model.addAttribute("itemNo", itemNo);
        model.addAttribute("storeType", storeType);

        if (storeType.equals(StoreType.DS.getType())) {

            model.addAllAttributes(RealGridHelper.createForGroup("itemHistGridBaseInfo", ItemHistGroupGetDto.class));
            return "/item/pop/itemHistPop";
        } else if (StringUtils.isEmpty(storeType)) {

            model.addAllAttributes(RealGridHelper.createForGroup("itemHistGridBaseInfo", ItemHistGroupBasicGetDto.class));
            return "/item/pop/itemHistBasicPop";
        } else {

            model.addAllAttributes(RealGridHelper.createForGroup("itemHistGridBaseInfo", ItemDetailHistGetDto.class));
            return "/item/pop/itemHistDetailPop";
        }
    }

    /**
     * 상품관리 > 점포상품관리 > 온라인재고 일괄설정 > 온라인재고 기준설정
     * @param model
     * @return
     */
    @RequestMapping(value = "/onlineStockMngPop", method = RequestMethod.GET)
    public String onlineStockMngPop(Model model) {
        model.addAllAttributes(RealGridHelper.createForGroup("onlineStockGridBaseInfo", OnlineStockMngGetDto.class));
        return "/item/pop/onlineStockMngPop";
    }

    /**
     * 상품관리 > 점포상품관리 > 온라인재고 일괄설정 > 온라인재고 복수설정
     * @return
     */
    @RequestMapping(value = "/setOnlineStockPop", method = RequestMethod.GET)
    public String setOnlineStockPop() {
        return "/item/pop/setOnlineStockPop";
    }

    /**
     * 상품관리 > 상품관리 > 제조사관리 > 검색 팝업
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/maker/searchPopup", method = RequestMethod.GET)
    public String searchPopup(Model model) {

        model.addAllAttributes(RealGridHelper.createForGroup("makerSearchPopGridBaseInfo", MakerSchPopDto.class));

        return "/item/pop/makerSearch";
    }

    /**
     * 점포별 취급중지 엑셀 일괄등록 팝업창
     * @param model
     * @param templateUrl
     * @param callBackScript
     * @return
     */
    @GetMapping("/stopDealExcelReadPop")
    public String stopDealExcelReadPop(Model model
    , @RequestParam(value = "callback", defaultValue = "") String callBackScript) {

        model.addAttribute("callback", callBackScript);

        return "/item/pop/stopDealExcelReadPop";
    }

    /**
     * 외부 연동 상품 변경이력 팝업
     * @param model
     * @param itemNo
     * @param partnerId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/marketItemHistPop", method = RequestMethod.GET)
    public String marketItemHistPop(Model model, @RequestParam String itemNo, @RequestParam String partnerId) throws Exception {

        model.addAttribute("itemNo", itemNo);
        model.addAttribute("partnerId", partnerId);

        model.addAllAttributes(RealGridHelper.createForGroup("marketItemHistListPopGridBaseInfo", MarketItemSpecHistGetDto.class));

        return "/item/pop/marketItemHistPop";
    }
}

