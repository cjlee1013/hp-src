package kr.co.homeplus.admin.web.user.model;


import lombok.Getter;
import lombok.Setter;

/**
 * 회원등급 정의
 */
@Setter
@Getter
public class UserGradeDefinitionSelectBox {
    /** 회원등급번호 **/
    private int gradeSeq;
    /** 회원등급명 **/
    private String gradeNm;
}
