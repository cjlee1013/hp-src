package kr.co.homeplus.admin.web.mileage.model.type;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MileageTypeSearchPopDto {
    @ApiModelProperty(value = "마일리지 번호", position = 1)
    @RealGridColumnInfo(headText = "마일리지 번호", width = 120)
    private String mileageTypeNo;

    @ApiModelProperty(value = "마일리지 유형", position = 1)
    @RealGridColumnInfo(headText = "마일리지 유형", width = 150)
    private String mileageTypeName;

    @ApiModelProperty(value = "마일리지 종류", position = 1)
    @RealGridColumnInfo(headText = "마일리지 종류", width = 120, hidden = true)
    private String mileageKind;
}
