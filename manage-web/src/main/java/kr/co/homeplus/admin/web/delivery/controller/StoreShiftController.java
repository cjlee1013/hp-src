package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.EXP;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.StoreShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.StoreShiftManageSelectModel;
import kr.co.homeplus.admin.web.delivery.service.StoreShiftService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 점포배송정보 > shift 관리
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class StoreShiftController {

    private final AuthorityService authorityService;
    private final StoreShiftService storeShiftService;
    private final CodeService codeService;

    public StoreShiftController(AuthorityService authorityService, StoreShiftService storeShiftService, CodeService codeService) {
        this.authorityService = authorityService;
        this.storeShiftService = storeShiftService;
        this.codeService = codeService;
    }

    /**
     * shift 관리 Main Page
     */
    @GetMapping("/shiftManageMain")
    public String shiftManageMain(Model model) throws Exception{
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type",   //점포유형
            "weekday"               //배송요일
        );
        List<MngCodeGetDto> storeTypeList = code.get("store_type")
                .stream()
                .filter(st -> !EXP.getCode()
                        .equals(st.getMcCd()))
                .collect(Collectors.toList());
        model.addAttribute("storeType", storeTypeList);
        model.addAttribute("weekdayList", code.get("weekday"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("weekdayJson", om.writeValueAsString(code.get("weekday")));

        // Hyper(df) 점포 리스트 가져와서 model 에 담기
        model.addAllAttributes(RealGridHelper.create("storeShiftGridBaseInfo", StoreShiftManageModel.class));
        return "/delivery/storeShiftManageMain";
    }

    @ResponseBody
    @GetMapping("/getShiftManageList.json")
    public List<StoreShiftManageModel> getShiftManageList(@ModelAttribute StoreShiftManageSelectModel storeShiftManageSelectModel) throws Exception {
        return storeShiftService.getStoreShiftManageList(storeShiftManageSelectModel);
    }
}
