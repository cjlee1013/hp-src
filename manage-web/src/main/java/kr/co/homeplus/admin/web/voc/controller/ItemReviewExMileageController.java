package kr.co.homeplus.admin.web.voc.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.voc.model.ItemReviewExMileageDelParamDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewExMileageListGetDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewExMileageListParamDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewExMileageSetParamDto;
import kr.co.homeplus.admin.web.voc.model.ItemReviewListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/voc/itemReview")
public class ItemReviewExMileageController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     *  회원관리 > 고객문의관리 > 상품평 마일리지 지급 제외 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/itemReviewExMileageMain", method = RequestMethod.GET)
    public String itemReviewExMileageMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"   // 스토어 타입
            , "block_yn"            // 미노출여부
            , "reserve_yn"          // 적립여부
        );

        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("blockYn", code.get("block_yn"));
        model.addAttribute("reserveYn", code.get("reserve_yn"));

        model.addAllAttributes(RealGridHelper.create("itemReviewExMileageListGridBaseInfo", ItemReviewExMileageListGetDto.class));

        return "/voc/itemReviewExMileageMain";
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평 마일리지 지급 제외 관리 조회
     *
     * @param itemReviewExMileageListParamDto
     * @return List<ItemReviewExMileageListGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getItemReviewExMileageList.json"}, method = RequestMethod.GET)
    public List<ItemReviewExMileageListGetDto> getItemReviewExMileageList(ItemReviewExMileageListParamDto itemReviewExMileageListParamDto) {
        String apiUri = "/voc/itemReview/getItemReviewExMileageList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemReviewExMileageListGetDto>>>() {};
        return (List<ItemReviewExMileageListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, ItemReviewExMileageListParamDto.class, itemReviewExMileageListParamDto), typeReference).getData();
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평 마일리지 지급 제외 등록
     * @param itemReviewExMileageSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemReviewExMileage.json"}, method = RequestMethod.POST)
    public ResponseResult setItemReviewExMileage(@RequestBody ItemReviewExMileageSetParamDto itemReviewExMileageSetParamDto) throws Exception {
        String apiUri = "/voc/itemReview/setItemReviewExMileage";
        itemReviewExMileageSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemReviewExMileageSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 회원관리 > 고객문의관리 > 상품평 마일리지 지급 제외 삭제
     * @param arrSeq
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/delItemReviewExMileage.json"}, method = RequestMethod.POST)
    public ResponseResult delItemReviewExMileage(@RequestBody String[] arrSeq) throws Exception {
        String apiUri = "/voc/itemReview/delItemReviewExMileage";

        ItemReviewExMileageDelParamDto itemReviewExMileageDelParamDto = new ItemReviewExMileageDelParamDto();
        itemReviewExMileageDelParamDto.setArrSeq(arrSeq);
        itemReviewExMileageDelParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemReviewExMileageDelParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
