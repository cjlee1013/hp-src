package kr.co.homeplus.admin.web.voc.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.voc.enums.OsType;
import kr.co.homeplus.admin.web.voc.enums.PlatformType;
import kr.co.homeplus.admin.web.voc.enums.SiteType;
import kr.co.homeplus.admin.web.voc.model.CustomerSuggestGrid;
import kr.co.homeplus.admin.web.voc.model.CustomerSuggestInfoDto;
import kr.co.homeplus.admin.web.voc.model.CustomerSuggestSearchDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/voc/customerSuggest")
@RequiredArgsConstructor
@Slf4j
public class CustomerSuggestController {

    private final ResourceClient resourceClient;

    private final Map<String, Object> gridBaseInfo =
        RealGridHelper.create("customerSuggestGridBaseInfo", CustomerSuggestGrid.class);

    /**
     * 회원관리 > 고객문의관리 > 제안하기 메인
     *
     * @param model
     * @return
     */
    @GetMapping("/customerSuggestMain")
    public String main(final Model model) {
        model.addAllAttributes(gridBaseInfo);

        Map<Object, Object> osType = new LinkedHashMap<>();
        osType.put("", "전체");
        osType.put(OsType.ANDROID, OsType.ANDROID.getCaseName());
        osType.put(OsType.IOS, OsType.IOS.getCaseName());

        Map<Object, Object> siteType = new LinkedHashMap<>();
        siteType.put("", "전체");
        siteType.put(SiteType.HOME, SiteType.HOME.getKorSiteName());
        siteType.put(SiteType.CLUB, SiteType.CLUB.getKorSiteName());

        Map<Object, Object> platformType = new LinkedHashMap<>();
        platformType.put("", "전체");
        platformType.put(PlatformType.APP, PlatformType.APP.getCaseName());
        platformType.put(PlatformType.MWEB, PlatformType.MWEB.getCaseName());

        model.addAttribute("osType", osType);
        model.addAttribute("siteType", siteType);
        model.addAttribute("platformType", platformType);
        return "/voc/customerSuggestMain";
    }

    @GetMapping("/getCustomerSuggestInfos.json")
    @ResponseBody
    public ResponseObject<List<CustomerSuggestInfoDto>> getCustomerSuggestInfos(
        final CustomerSuggestSearchDto customerSuggestSearchDto) {
        String url = StringUtil.getRequestString("/suggest/get",
            CustomerSuggestSearchDto.class, customerSuggestSearchDto);
        return resourceClient.getForResponseObject(ResourceRouteName.USERMNG, url);
    }

    @GetMapping("/getCustomerSuggestInfo.json")
    @ResponseBody
    public ResponseObject<CustomerSuggestInfoDto> getCustomerSuggestInfo(@RequestParam("customerSuggestSeq") final long seq) {
        String url = "/suggest/get/" + seq;
        return resourceClient.getForResponseObject(ResourceRouteName.USERMNG, url);
    }
}
