package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 주문정보 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class OrderSearchDetailDto {

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80)
    private String storeType;

    @ApiModelProperty(value = "판매자아이디")
    @RealGridColumnInfo(headText = "판매자아이디", hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "판매업체")
    @RealGridColumnInfo(headText = "판매업체")
    private String partnerNm;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 80)
    private String bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    @RealGridColumnInfo(headText = "상품 주문번호", width = 80)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 110)
    private String itemNo;

    @ApiModelProperty(value = "박스번호")
    @RealGridColumnInfo(headText = "박스번호", width = 80, hidden = true)
    private String multiBundleNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 200, columnType = RealGridColumnType.NONE)
    private String itemNm1;

    @ApiModelProperty(value = "옵션번호")
    @RealGridColumnInfo(headText = "옵션번호", width = 80)
    private String orderOptNo;

    @ApiModelProperty(value = "옵션")
    @RealGridColumnInfo(headText = "옵션")
    private String optNm;

    @ApiModelProperty(value = "대체여부")
    @RealGridColumnInfo(headText = "대체여부", width = 80)
    private String substitutionYn;

    @ApiModelProperty(value = "최초주문수량")
    @RealGridColumnInfo(headText = "최초주문수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, width = 80)
    private String itemQty;

    @ApiModelProperty(value = "클레임수량")
    @RealGridColumnInfo(headText = "클레임수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, width = 80)
    private String claimQty;

    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(headText = "상품금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80)
    private String itemPrice;

    @ApiModelProperty(value = "결품수량")
    @RealGridColumnInfo(headText = "결품수량", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER, width = 80, sortable = true)
    private String oosQty;

    @ApiModelProperty(value = "대체상품")
    @RealGridColumnInfo(headText = "대체상품", sortable = true)
    private String changeItemNm;

    @ApiModelProperty(value = "택배비지급")
    @RealGridColumnInfo(headText = "택배비지급", sortable = true)
    private String prepaymentYn;

    @ApiModelProperty(value = "장바구니할인 여부")
    @RealGridColumnInfo(headText = "장바구니할인 여부", width = 80, sortable = true)
    private String discountCartYn;

    @ApiModelProperty(value = "총 상품금액")
    @RealGridColumnInfo(headText = "총 상품금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String orderPrice;

    @ApiModelProperty(value = "상품할인금액")
    @RealGridColumnInfo(headText = "상품할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String discountAmt;

    @ApiModelProperty(value = "카드상품할인")
    @RealGridColumnInfo(headText = "카드상품할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String cardDiscountAmt;

    @ApiModelProperty(value = "행사할인금액")
    @RealGridColumnInfo(headText = "행사할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String promoDiscountAmt;

    @ApiModelProperty(value = "중복쿠폰")
    @RealGridColumnInfo(headText = "중복쿠폰", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String addCouponAmt;

    @ApiModelProperty(value = "새벽배송 회수백비용")
    @RealGridColumnInfo(headText = "새벽배송 회수백비용", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, hidden = true)
    private String auroraReturnAmt;

    @ApiModelProperty(value = "최초 배송비")
    @RealGridColumnInfo(headText = "최초 배송비", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String shipPrice;

    @ApiModelProperty(value = "최초 도서산간 배송비")
    @RealGridColumnInfo(headText = "최초 도서산간 배송비", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String islandShipPrice;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80, sortable = true)
    private String claimShipPrice;

    @ApiModelProperty(value = "도서산간 배송비")
    @RealGridColumnInfo(headText = "도서산간 배송비", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE)
    private String claimAddShipPrice;

    @ApiModelProperty(value = "배송비할인금액")
    @RealGridColumnInfo(headText = "배송비할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80)
    private String shipDiscountPrice;

    @ApiModelProperty(value = "클레임배송비")
    @RealGridColumnInfo(headText = "클레임배송비", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80)
    private String claimPrice;

    @ApiModelProperty(value = "최초 상품별 장바구니할인금액")
    @RealGridColumnInfo(headText = "최초 상품별 장바구니할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 90)
    private String basicCartItemDiscountAmt;

    @ApiModelProperty(value = "장바구니할인금액")
    @RealGridColumnInfo(headText = "장바구니할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 90)
    private String totCartDiscountAmt;

    @ApiModelProperty(value = "상품별 장바구니할인금액")
    @RealGridColumnInfo(headText = "상품별 장바구니할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 90)
    private String cartItemDiscountAmt;

    @ApiModelProperty(value = "임직원할인")
    @RealGridColumnInfo(headText = "임직원할인", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80)
    private String totEmpDiscountAmt;

    @ApiModelProperty(value = "결제금액")
    @RealGridColumnInfo(headText = "결제금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 80)
    private String totAmt;

    @ApiModelProperty(value = "결제상태", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "결제상태")
    private String paymentStatus;

    @ApiModelProperty(value = "배송상태", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "배송상태")
    private String shipStatus;

    @ApiModelProperty(value = "클레임가능여부 ", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "클레임가능여부")
    private String claimYn;

    @ApiModelProperty(value = "배송타입", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "배송타입")
    private String shipType;

    @ApiModelProperty(value = "합배송클레임완료여부", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "합배송클레임완료여부")
    private String cmbnClaimYn;

    @ApiModelProperty(value = "사이트타입", hidden = true)
    @RealGridColumnInfo(hidden = true, headText = "사이트타입")
    private String siteType;

}