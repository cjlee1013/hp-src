package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.banner.HomeplusBannerSetParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListParamDto;
import kr.co.homeplus.admin.web.item.model.banner.ItemBannerListSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/banner")
public class HomeplusBannerController {

    final private ResourceClient resourceClient;
    final private LoginCookieService cookieService;
    final private CodeService codeService;

    public HomeplusBannerController(ResourceClient resourceClient,
                                    LoginCookieService cookieService,
                                    CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 상품상세 일괄공지 > 홈플러스공지
     * @param model
     * @return String
     */
    @RequestMapping(value = "/homeplusBannerMain", method = RequestMethod.GET)
    public String HomeplusBannerMain(Model model) throws Exception {

        //코드정보
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "banner_period_type"   // 검색일자타입
                , "disp_yn"                     // 노출여부
                , "disp_type_home"              // 배송방법
                , "banner_store_type"           // 상품공지 점포 타입
        );

        model.addAttribute("bannerPeriodType", code.get("banner_period_type"));
        model.addAttribute("dispYn", code.get("disp_yn"));
        model.addAttribute("dispType", code.get("disp_type_home"));
        model.addAttribute("bannerStoreType", code.get("banner_store_type"));

        //grid
        model.addAllAttributes(
            RealGridHelper.create("homeplusBannerListGridBaseInfo", ItemBannerListSelectDto.class));

        return "/item/homeplusBannerMain";
    }

    /**
     * 상품관리> 상품상세 일괄등록 > 홈플러스공지 리스트
     * @param itemBannerListParamDto
     * @return List<ItemBannerListSelectDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getHomeplusBannerList.json"}, method = RequestMethod.POST)
    public List<ItemBannerListSelectDto> getHomeplusBannerList(
        @RequestBody ItemBannerListParamDto itemBannerListParamDto) {
        String apiUri = "/item/banner/getHomeplusBannerList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemBannerListSelectDto>>>() {};
        return (List<ItemBannerListSelectDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemBannerListParamDto , apiUri, typeReference).getData();
    }

    /**
     * 상품관리> 상품상세 일괄등록 > 홈플러스공지 등록/수정
     * @param homeplusBannerSetParamDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setHomeplusBanner.json"}, method = RequestMethod.POST)
    public ResponseResult setHomeplusBanner (@ModelAttribute HomeplusBannerSetParamDto homeplusBannerSetParamDto) throws Exception {
        String apiUri = "/item/banner/setHomeplusBanner";
        homeplusBannerSetParamDto.setUserId(cookieService.getUserInfo().getEmpId());

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM,
            homeplusBannerSetParamDto, apiUri, typeReference).getData();
    }

}
