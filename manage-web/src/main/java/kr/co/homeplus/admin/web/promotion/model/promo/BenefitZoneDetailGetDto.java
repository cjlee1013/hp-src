package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BenefitZoneDetailGetDto {
    // 혜택존 번호
    private Long benefitNo;

    // 사이트 구분
    private String siteType;

    // 디바이스
    private String deviceType;

    // 전시위치
    private String dispLoc;

    // 전시순서
    private Integer priority;

    // 전시시작일
    private String dispStartDt;

    // 전시종료일
    private String dispEndDt;

    // 배너명
    private String bannerNm;

    // 전시여부
    private String dispYn;

    // 링크유형
    private String linkType;

    // 링크정보
    private String linkInfo;

    // 링크명
    private String linkNm;

    // 링크옵션
    private String linkOptions;

    // 점포유형
    private String storeType;

    // 택배점 설정여부
    private String dlvStoreYn;

    // 택배점포ID
    private String dlvStoreId;

    // 이미지 리스트
    private List<PromoImgDto> imgList;
}