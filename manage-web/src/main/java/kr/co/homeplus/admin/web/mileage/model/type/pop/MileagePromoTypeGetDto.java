package kr.co.homeplus.admin.web.mileage.model.type.pop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 행사 관련 조회 응답 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class MileagePromoTypeGetDto {
    @ApiModelProperty(value = "마일리지타입번호", position = 1)
    @RealGridColumnInfo(headText = "마일리지번호")
    private long mileageTypeNo;
    @ApiModelProperty(value = "마일리지타입명", position = 2)
    @RealGridColumnInfo(headText = "마일리지유형")
    private String mileageTypeName;
    @ApiModelProperty(value = "유효기간타입(TT:기간내/TR:등록일로부터)", position = 3)
    @RealGridColumnInfo(headText = "유효기간타입")
    private String expireType;
    @ApiModelProperty(value = "유효기간일", position = 4)
    @RealGridColumnInfo(headText = "유효기간일")
    private String expireDt;
    @ApiModelProperty(value = "고객노출문구", position = 5)
    @RealGridColumnInfo(headText = "고객노출문구", hidden = true)
    private String mileageTypeExplain;
}
