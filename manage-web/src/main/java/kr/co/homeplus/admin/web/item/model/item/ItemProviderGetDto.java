package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

//상품 제휴사,마켓연동 관련 정보
@Getter
@Setter
public class ItemProviderGetDto {

	//업체아이디,제휴채널 ID
	private String providerId;

	//업체아이디, 제휴채널 명
	private String providerNm;

	//단독:S, 기타:E
	private String providerType;

	//파트너 구분(AFFI:제휴사, COOP:마켓연동)-공통코드:partner_type
	private String partnerType;

	//사용여부
	private String useYn;

	//등록여부 (Y:등록, N:등록안함)
    private String regYnNm;

	//중지기간설정여부(Y:설정, N:설정안함)
	private String stopPeriodYn;

	//중지시작일
	private String stopStartDt;

	//중지종료일
	private String stopEndDt;
}
