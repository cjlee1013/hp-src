package kr.co.homeplus.admin.web.core.constants;

public class DateTimeFormat {
    public static final String TIME_ZONE_KST = "Asia/Seoul";
    public static final String DATE_FORMAT_DIGIT_YMD = "yyyyMMdd";
    public static final String DATE_FORMAT_DEFAULT_YMD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_DEFAULT_YMD_HIS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_MILL_YMD_HIS = "yyyy-MM-dd HH:mm:ss.SSS";
}
