package kr.co.homeplus.admin.web.statistics.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.CouponStatistics;
import kr.co.homeplus.admin.web.statistics.model.CouponStatisticsSelect;
import kr.co.homeplus.admin.web.statistics.service.CouponStatisticService;
import kr.co.homeplus.admin.web.statistics.service.StatisticsCommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 프로모션통계 > 쿠폰통계
 */
@Slf4j
@Controller
@RequestMapping("/escrow/statistics")
public class CouponStatisticsController {

    private final AuthorityService authorityService;
    private final LoginCookieService loginCookieService;

    @Autowired
    private CouponStatisticService couponStatisticService;

    @Autowired
    private StatisticsCommonService statisticsCommonService;

    private static final List<RealGridColumn> COUPON_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption COUPON_GRID_OPTION = new RealGridOption("fill", false, false, false);

    private static final List<RealGridColumn> COUPON_DETAIL_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption COUPON_DETAIL_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public CouponStatisticsController(AuthorityService authorityService, LoginCookieService loginCookieService) {
        this.authorityService = authorityService;
        this.loginCookieService = loginCookieService;

        COUPON_GRID_HEAD.add(new RealGridColumn("storeType", "storeType", "점포유형", RealGridColumnType.BASIC, 50, true, true));
        COUPON_GRID_HEAD.add(new RealGridColumn("couponNo", "couponNo", "쿠폰번호", RealGridColumnType.BASIC, 50, true, true));
        COUPON_GRID_HEAD.add(new RealGridColumn("couponKind", "couponKind", "쿠폰종류코드", RealGridColumnType.BASIC, 50, false, true));
        COUPON_GRID_HEAD.add(new RealGridColumn("couponKindNm", "couponKindNm", "쿠폰종류", RealGridColumnType.BASIC, 50, true, true));
        COUPON_GRID_HEAD.add(new RealGridColumn("couponNm", "couponNm", "쿠폰명", RealGridColumnType.NAME, 200, true, true));

        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("storeNm", "storeNm", "점포명", RealGridColumnType.BASIC, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("couponNm", "couponNm", "쿠폰행사명", RealGridColumnType.BASIC, 50, false, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("itemNo", "itemNo", "상품번호", RealGridColumnType.BASIC, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("lcateNm", "lcateNm", "대분류", RealGridColumnType.BASIC, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("mcateNm", "mcateNm", "중분류", RealGridColumnType.BASIC, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("orderCnt", "orderCnt", "주문건수", RealGridColumnType.NUMBER_C, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("orderAmt", "orderAmt", "주문금액", RealGridColumnType.NUMBER_C, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("couponUseCnt", "couponUseCnt", "사용건수", RealGridColumnType.NUMBER_C, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("discountAmt", "discountAmt", "사용금액", RealGridColumnType.NUMBER_C, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("claimCnt", "claimCnt", "취소건수", RealGridColumnType.NUMBER_C, 50, true, true));
        COUPON_DETAIL_GRID_HEAD.add(new RealGridColumn("claimDiscountAmt", "claimDiscountAmt", "취소금액", RealGridColumnType.NUMBER_C, 50, true, true));

    }

    /**
     * 쿠폰통계 메인
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/couponMain")
    public String couponMain(Model model) throws Exception {

        model.addAttribute("couponGridBaseInfo", new RealGridBaseInfo("couponGridBaseInfo", COUPON_GRID_HEAD, COUPON_GRID_OPTION).toString());
        model.addAttribute("couponDetailGridBaseInfo", new RealGridBaseInfo("couponDetailGridBaseInfo", COUPON_DETAIL_GRID_HEAD, COUPON_DETAIL_GRID_OPTION).toString());

        return "/statistics/couponMain";
    }

    /**
     * 쿠폰 조회 상단
     * @param couponStatisticsSelect
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getCouponStatisticsList.json")
    public List<CouponStatistics> getCouponList(@ModelAttribute CouponStatisticsSelect couponStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.COUPON_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        couponStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        List<CouponStatistics> couponStatisticList = couponStatisticService.getCouponStatisticList(couponStatisticsSelect);
        return couponStatisticList;
    }

    /**
     * 상품쿠폰, 상품할인 상세 조회
     * @param couponStatisticsSelect
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getItemCouponStatisticsDetailList.json")
    public List<CouponStatistics> getItemCouponDetailList(@ModelAttribute CouponStatisticsSelect couponStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.ITEM_COUPON_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        couponStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        List<CouponStatistics> couponStatisticList = couponStatisticService.getItemCouponStatisticDetailList(couponStatisticsSelect);
        return couponStatisticList;
    }

    /**
     * 장바구니쿠폰, 배송비쿠폰 상세 조회
     * @param couponStatisticsSelect
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getCartCouponStatisticsDetailList.json")
    public List<CouponStatistics> getCartCouponDetailList(@ModelAttribute CouponStatisticsSelect couponStatisticsSelect) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.CART_COUPON_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        couponStatisticsSelect.setExtractCommonSetDto(extractCommonSetDto);

        List<CouponStatistics> couponStatisticList = couponStatisticService.getCartCouponStatisticDetailList(couponStatisticsSelect);
        return couponStatisticList;
    }

    /**
     * 엑셀 다운로드
     */
    @ResponseBody
    @GetMapping("/coupon/saveExtractHistoryForExcelDown.json")
    public void saveExtractHistoryForExcelDown(String couponKind) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(couponKind.equals("item")?ExtractMenuName.ITEM_COUPON_STATISTICS.getMenuNm():ExtractMenuName.CART_COUPON_STATISTICS.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());

        statisticsCommonService.saveExtractHistory(extractCommonSetDto);
    }
}
