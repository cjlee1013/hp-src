package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspLeafletCategory.DspLeafletCategoryListGetDto;
import kr.co.homeplus.admin.web.manage.model.dspLeafletCategory.DspLeafletCategorySetParamDto;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/dspLeafletCategory")
public class DspLeafletCategoryController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 전문관관리 > 마트전단 카테고리 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspLeafletCategoryMain", method = RequestMethod.GET)
    public String dspLeafletCategoryMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"            // 사용여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("dspLeafletCategoryListGridBaseInfo", DspLeafletCategoryListGetDto.class));

        return "/manage/dspLeafletCategoryMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 마트전단 카테고리 관리 리스트 조회 ( 그리드 )
     * @param schKeyword
     * @return DspPopupPartnerListSelectDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getDspLeafletCategoryList.json"}, method = RequestMethod.GET)
    public List<DspLeafletCategoryListGetDto> getDspLeafletCategoryList(@RequestParam(name = "schKeyword") String schKeyword, @RequestParam(name = "searchUseYn") String searchUseYn) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("schKeyword", schKeyword));
        getParameterList.add(SetParameter.create("searchUseYn", searchUseYn));

        String apiUriRequest = "/manage/dspLeafletCategory/getDspLeafletCategory" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspLeafletCategoryListGetDto>>>() {})
            .getData();
    }

    /**
     * 시스템관리 > 모니터링 > Cache 강제 적용 후 정보 갱신
     * @param setParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setDspLeafletCategory.json"}, method = RequestMethod.POST)
    public ResponseResult setDspLeafletCategory(@RequestBody DspLeafletCategorySetParamDto setParamDto) {
        String apiUri = "/manage/dspLeafletCategory/setDspLeafletCategory";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
