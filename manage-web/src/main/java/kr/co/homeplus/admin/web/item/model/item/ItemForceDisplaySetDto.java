package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * 상품 강제전시 Eentry
 */
@Getter
@Setter
public class ItemForceDisplaySetDto {

	//상품번호
	private String itemNo;

	//점포타입
	private String storeType;

	//점포아이디
	private String storeIds;

	//사용자
	private String userId;
}
