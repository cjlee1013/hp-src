package kr.co.homeplus.admin.web.claim.model.refundFail;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@ApiModel(description = "환불완료 요청 DTO")
public class RequestRefundCompleteSetDto {

    @ApiModelProperty(value = "클레임결제번호 리스트", position = 1)
    List<claimPaymentNoDto> claimPaymentNoList;

    @ApiModelProperty(value = "수정요청자ID", position = 2)
    private String chgId;

    @Getter
    @Setter
    public static class claimPaymentNoDto {
        @ApiModelProperty(value = "클레임결제번호", position = 1)
        private String claimPaymentNo;

        @ApiModelProperty(value = "클레임 번호", position = 2)
        private long claimNo;
    }

}
