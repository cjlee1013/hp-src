package kr.co.homeplus.admin.web.item.model.priceChangeHist;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class SalePriceDetailListGetDto {

    @RealGridColumnInfo(headText = "점포ID", width = 70, sortable = true)
    private String storeId;

    @RealGridColumnInfo(headText = "점포명", sortable = true, order = 1)
    private String storeNm;

    @RealGridColumnInfo(headText = "점포유형", sortable = true, order = 2)
    private String storeType;

    @RealGridColumnInfo(headText = "판매가", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true, order = 3)
    private int salePrice;

}
