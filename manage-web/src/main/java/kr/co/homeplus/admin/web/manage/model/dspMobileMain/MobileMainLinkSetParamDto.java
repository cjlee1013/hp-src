package kr.co.homeplus.admin.web.manage.model.dspMobileMain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MobileMainLinkSetParamDto {

    private Integer linkNo;

    private String linkType;

    private String linkInfo;

    private String linkOptions;

    private String textYn;

    private String title;

    private String subTitle;

    private String subTitle2;

    private String gnbDeviceType;

    private String imgUrl;

    private int imgWidth;

    private int imgHeight;

    private String useYn;

    private String dlvYn;

    private int dlvStoreId;


}
