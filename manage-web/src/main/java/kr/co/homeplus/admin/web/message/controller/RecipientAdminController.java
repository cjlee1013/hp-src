package kr.co.homeplus.admin.web.message.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.model.RecipientDetailInfo;
import kr.co.homeplus.admin.web.message.model.RecipientGrid;
import kr.co.homeplus.admin.web.message.model.RecipientSearchRequest;
import kr.co.homeplus.admin.web.message.model.SmsResendRequest;
import kr.co.homeplus.admin.web.message.model.SwitchableRecipientView;
import kr.co.homeplus.admin.web.message.service.RecipientSupportService;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/message/admin/recipient")
@RequiredArgsConstructor
@Slf4j
public class RecipientAdminController {

    private final CodeService codeService;

    private final RecipientSupportService recipientSupportService;

    private final Map<String, Object> recipientGridBaseInfo = RealGridHelper.create(
        "recipientGridBaseInfo", RecipientGrid.class);

    @GetMapping("/main")
    public String main(final Model model) {
        codeService.getCodeModel(model, "message_work_status");

        model.addAllAttributes(recipientGridBaseInfo);
        return "/message/recipientMain";
    }

    @PostMapping("/search.json")
    @ResponseBody
    public List<SwitchableRecipientView> search(
        @RequestBody @Valid final RecipientSearchRequest recipientSearchRequest) {
        return recipientSupportService.search(recipientSearchRequest);
    }

    @GetMapping("/getDetail.json")
    @ResponseBody
    public RecipientDetailInfo getDetail(@RequestParam("listSeq") long listSeq,
        @RequestParam("historySeq") long historySeq, @RequestParam("workType") WorkType workType) {
        return recipientSupportService.getDetail(listSeq, historySeq, workType).getData()
            .getRecipientDetailInfo();
    }

    @GetMapping("/resend.json")
    @ResponseBody
    public ResponseObject<String> resend(@ModelAttribute SmsResendRequest smsResendRequest,
        @RequestParam("workType") WorkType workType) {
        return ResourceConverter.toResponseObject(
            recipientSupportService.resend(smsResendRequest, workType));
    }
}
