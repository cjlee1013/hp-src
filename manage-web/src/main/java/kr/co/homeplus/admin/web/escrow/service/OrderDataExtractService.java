package kr.co.homeplus.admin.web.escrow.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractAddCouponUseOrderDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractAssistancePaymentInfoDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractCartCouponUseOrderDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractPaymentInfoDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractPickupOrderDto;
import kr.co.homeplus.admin.web.escrow.model.extract.OrderDataExtractSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderDataExtractService {
    private final ResourceClient resourceClient;

    /**
     * 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     * @throws Exception
     */
    public List<ExtractCartCouponUseOrderDto> getCartCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_CART_COUPON_USE_ORDER_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExtractCartCouponUseOrderDto>>>() {};
        return (List<ExtractCartCouponUseOrderDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, orderDataExtractSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 픽업주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     * @throws Exception
     */
    public List<ExtractPickupOrderDto> getPickupOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_PICKUP_ORDER_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExtractPickupOrderDto>>>() {};
        return (List<ExtractPickupOrderDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, orderDataExtractSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 결제수단별 결제정보 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     * @throws Exception
     */
    public List<ExtractPaymentInfoDto> getPaymentInfoListByMethod(OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_INFO_LIST_BY_METHOD;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExtractPaymentInfoDto>>>() {};
        return (List<ExtractPaymentInfoDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, orderDataExtractSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 보조결제수단 결제정보 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     * @throws Exception
     */
    public List<ExtractAssistancePaymentInfoDto> getAssistancePaymentInfoList(OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ASSISTANCE_PAYMENT_INFO_LIST;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExtractAssistancePaymentInfoDto>>>() {};
        return (List<ExtractAssistancePaymentInfoDto>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, orderDataExtractSelectDto, apiUri, typeReference).getData();
    }

    /**
     * 추출이력 저장
     * @param extractCommonSetDto
     */
    public void saveExtractHistory(ExtractCommonSetDto extractCommonSetDto) {
        resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, extractCommonSetDto, EscrowConstants.EXTRACT_SAVE_EXTRACT_HISTORY);
    }

    /**
     * 중복쿠폰 사용주문 리스트 조회
     * @param orderDataExtractSelectDto
     * @return
     * @throws Exception
     */
    public List<ExtractAddCouponUseOrderDto> getAddCouponUseOrderList(OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ADD_COUPON_USE_ORDER_LIST;
        ResourceClientRequest<List<ExtractAddCouponUseOrderDto>> request = ResourceClientRequest.<List<ExtractAddCouponUseOrderDto>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderDataExtractSelectDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }
}
