package kr.co.homeplus.admin.web.voc.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.voc.enums.OsType;
import kr.co.homeplus.admin.web.voc.enums.PlatformType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class CustomerSuggestGrid {

    @RealGridColumnInfo(headText = "번호", width = 20)
    private long customerSuggestSeq;

    @RealGridColumnInfo(headText = "회원 번호", width = 25, hidden = true)
    private String userNo;

    @RealGridColumnInfo(headText = "앱 구분", width = 25, columnType = RealGridColumnType.LOOKUP)
    private PlatformType platformType;

    @RealGridColumnInfo(headText = "단말 OS", width = 25, columnType = RealGridColumnType.LOOKUP)
    private OsType osType;

    @RealGridColumnInfo(headText = "단말 모델명", width = 25)
    private String deviceModel;

    @RealGridColumnInfo(headText = "내용", width = 150)
    private String customerSuggestMessage;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME, width = 60)
    private LocalDateTime regDt;
}
