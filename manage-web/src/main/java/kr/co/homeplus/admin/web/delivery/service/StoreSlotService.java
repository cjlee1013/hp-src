package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.StoreSlotManageModel;
import kr.co.homeplus.admin.web.delivery.model.StoreSlotManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreSlotService {

    private final ResourceClient resourceClient;

    public List<StoreSlotManageModel> getStoreSlotManageList(StoreSlotManageSelectModel storeSlotManageSelectModel) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_STORE_SLOT_MANAGE_LIST, StoreSlotManageSelectModel.class, storeSlotManageSelectModel);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StoreSlotManageModel>>>() {};
        List<StoreSlotManageModel> storeSlotManageModelList = (List<StoreSlotManageModel>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri,
            typeReference).getData();

        return storeSlotManageModelList;
    }

    /**
     * 점포 slot 사용여부 변경
     * @param storeSlotManageModelList
     * @param userCd
     * @return
     */
    public ResponseObject<Object> saveSlotManage(List<StoreSlotManageModel> storeSlotManageModelList, String userCd) {

        storeSlotManageModelList.stream()
            .forEach(slot -> slot.setChgId(userCd));

        String apiUri = EscrowConstants.ESCROW_SAVE_SLOT_MANAGE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};
        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, storeSlotManageModelList, apiUri, typeReference);
    }
}
