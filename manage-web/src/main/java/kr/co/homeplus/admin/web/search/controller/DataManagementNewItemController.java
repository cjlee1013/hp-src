package kr.co.homeplus.admin.web.search.controller;

import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementNewItemGetParam;
import kr.co.homeplus.admin.web.search.model.DataManagementNewItemGridDto;
import kr.co.homeplus.admin.web.search.service.DataManagementNewItemService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/newItem")
@RequiredArgsConstructor
public class DataManagementNewItemController {

    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final DataManagementNewItemService newItemService;

    @GetMapping(value = {"/main"})
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("newItemGridBaseInfo", DataManagementNewItemGridDto.NewItemSearchList.class));
        return "/search/dataManagementNewItemMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementNewItemGetParam param) {
        return newItemService.getList(param);
    }
}
