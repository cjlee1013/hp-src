package kr.co.homeplus.admin.web.message.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.message.model.SmsHistoryDetailRequest;
import kr.co.homeplus.admin.web.message.model.SmsWorkDetail;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class MessageLogService {

    public static final String LOGGER_HEADER_KEY = "logger-data";

    private final ResourceClient resourceClient;

    private final ObjectMapper objectMapper;

    private final PrivacyLogService privacyLogService;

    private final LoginCookieService loginCookieService;

    private PrivacyLogInfo write(final HttpServletRequest request, final long workSeq) {
        PrivacyLogInfo personalLogWrapper = PrivacyLogInfo.builder()
            .userId(loginCookieService.getUserInfo().getUserId())
            .accessIp(ServletUtils.clientIP(request))
            .processor(String.valueOf(workSeq))
            .processorTaskSql("")
            .txCodeName("SMS 발송조회/상세정보")
            .txCodeUrl("/message/admin/sms/history/getDetail.json")
            .txMethod(PersonalLogMethod.SELECT.name())
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .build();
        privacyLogService.send(personalLogWrapper);
        return personalLogWrapper;
    }

    private HttpHeaders necessaryHeaders(@NonNull PrivacyLogInfo privacyLogInfo) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        try {
            // JSON 만들기 전 한글 인코딩 처리
            String txCodeName = privacyLogInfo.getTxCodeName();
            PrivacyLogInfo encoded = PrivacyLogInfo.builder()
                .userId(privacyLogInfo.getUserId())
                .accessIp(privacyLogInfo.getAccessIp())
                .processor(privacyLogInfo.getProcessor())
                .processorTaskSql(privacyLogInfo.getProcessorTaskSql())
                .txCodeName(URLEncoder.encode(txCodeName, StandardCharsets.UTF_8))
                .txCodeUrl(privacyLogInfo.getTxCodeUrl())
                .txMethod(privacyLogInfo.getTxMethod())
                .txTime(privacyLogInfo.getTxTime())
                .build();
            httpHeaders.set(LOGGER_HEADER_KEY, objectMapper.writeValueAsString(encoded));
        } catch (JsonProcessingException e) {
            log.info("log header create error", e);
        }
        return httpHeaders;
    }

    public ResponseObject<SmsWorkDetail> smsHistoryDetail(
        final HttpServletRequest request, final SmsHistoryDetailRequest smsHistoryDetailRequest) {
        PrivacyLogInfo privacyLogInfo = this.write(request, smsHistoryDetailRequest.getSmsWorkSeq());
        HttpHeaders httpHeaders = this.necessaryHeaders(privacyLogInfo);
        return resourceClient.postForResponseObject(ResourceRouteName.MESSAGE,
            smsHistoryDetailRequest, "/admin/sms/history/detail", httpHeaders,
            new ParameterizedTypeReference<>() {
            });

    }
}
