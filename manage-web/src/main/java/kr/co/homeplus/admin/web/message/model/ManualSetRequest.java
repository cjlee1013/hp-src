package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import java.util.List;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ManualSetRequest {

    private long workSeq;

    private String coupledWorkName;

    private String description;

    private String regId;

    private String templateCode;

    private WorkType workType;

    private List<BasicRecipient> bodyArgument;

    private String fromToken;

    private String delayYn;

    private String contentSubject;

    private String contentBody;

    private String callback;

    private LocalDateTime workDt;
}
