package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
@ApiModel(description = "쿠폰 1회성 정보 다운로드")
public class MileageCouponOneTimeDto {
    @ApiModelProperty(value = "쿠폰번호")
    @RealGridColumnInfo(headText = "쿠폰번호", width = 120)
    private String couponNo;
    @ApiModelProperty(value = "쿠폰사용여부")
    @RealGridColumnInfo(headText = "쿠폰사용여부", width = 50)
    private String couponUseYn;
}
