package kr.co.homeplus.admin.web.promotion.model.monitering;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class IdeBatchInfoListGetDto {
    @RealGridColumnInfo(headText = "일련번호", width = 60)
    private Long seq;

    @RealGridColumnInfo(headText = "배치명", width = 100)
    private String batchNm;

    @RealGridColumnInfo(headText = "배치관리명", width = 100)
    private String batchMngNm;

    @RealGridColumnInfo(headText = "배치정보", hidden = true)
    private String batchInfo;

    @RealGridColumnInfo(headText = "배치주기")
    private String batchSchedule;

    @RealGridColumnInfo(headText = "사용여부", width = 50)
    private String useYn;

    @RealGridColumnInfo(headText = "등록일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 150)
    private String regDt;

    @RealGridColumnInfo(headText = "등록자명")
    private String regNm;

    @RealGridColumnInfo(headText = "수정일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 150)
    private String chgDt;

    @RealGridColumnInfo(headText = "수정자명")
    private String chgNm;
}