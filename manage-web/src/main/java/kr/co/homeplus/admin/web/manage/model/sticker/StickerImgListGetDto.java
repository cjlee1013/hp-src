package kr.co.homeplus.admin.web.manage.model.sticker;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StickerImgListGetDto {

    //이미지번호
    private Long imgNo;

    //이미지 가로
    private int imgWidth;

    //이미지 세로
    private int imgHeight;

    //이미지 경로
    private String imgUrl;

    //디바이스
    private String device;

    //사용여부
    private String useYn;
}
