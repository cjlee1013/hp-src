package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaReplyDispStatusSetParamDto {

    private Integer qnaNo;
    private Integer qnaReplyNo;
    private String displayStatus;
    private String regId;

}
