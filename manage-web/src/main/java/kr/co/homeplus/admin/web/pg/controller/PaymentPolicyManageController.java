package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodTreeDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyManageDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentPolicyMethodSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.admin.web.pg.service.PaymentPolicyManageService;
import kr.co.homeplus.admin.web.pg.service.PgDivideManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 결제관리 > PG 배분율 관리 > 결제정책 관리
 */
@Controller
@RequestMapping("/pg/pgDivide")
public class PaymentPolicyManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final PaymentPolicyManageService paymentPolicyManageService;
    private final PgDivideManageService pgDivideManageService;

    public PaymentPolicyManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            PaymentPolicyManageService paymentPolicyManageService, PgDivideManageService pgDivideManageService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.paymentPolicyManageService = paymentPolicyManageService;
        this.pgDivideManageService = pgDivideManageService;
    }

    /**
     * 결제정책 관리 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/paymentPolicyManageMain")
    public String paymentPolicyManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_MNG)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("paymentPolicyGridBaseInfo", PaymentPolicyManageDto.class));
        model.addAttribute("siteTypeList", Arrays.asList(SiteType.values()));
        model.addAttribute("parentMethodCdList", ParentMethodCd.getBasicParentMethodList());
        model.addAttribute("inicisCdList", pgDivideManageService.getPgMidManageList(PgKind.INICIS.getPgCd(), "Y"));
        model.addAttribute("tosspgCdList", pgDivideManageService.getPgMidManageList(PgKind.TOSSPG.getPgCd(), "Y"));

        // 결제수단 tree 그리드
        model.addAllAttributes(RealGridHelper.create("methodTreeGridBaseInfo", PaymentMethodTreeDto.class));
        model.addAllAttributes(RealGridHelper.create("exposMethodGridBaseInfo", PaymentMethodTreeDto.class));

        return "/pg/paymentPolicyManageMain";
    }

    /**
     * 결제정책 마스터 조회
     * @param paymentPolicyManageSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/getPaymentPolicyMngList.json")
    public List<PaymentPolicyManageDto> getPaymentPolicyMngList(@ModelAttribute PaymentPolicyManageSelectDto paymentPolicyManageSelectDto) throws Exception {
        return paymentPolicyManageService.getPaymentPolicyMngList(paymentPolicyManageSelectDto);
    }

    /**
     * 결제정책 결제수단 Tree 구조 조회
     * @param siteType
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/getPaymentPolicyMethodTree.json")
    public List<Object> getPaymentPolicyMethodTree(@RequestParam(value = "siteType") String siteType,
                                                   @RequestParam(value = "policyNo") Long policyNo) throws Exception {
        PaymentPolicyMethodSelectDto paymentPolicyMethodSelectDto = new PaymentPolicyMethodSelectDto();
        paymentPolicyMethodSelectDto.setSiteType(siteType);
        paymentPolicyMethodSelectDto.setPolicyNo(policyNo);
        return paymentPolicyManageService.getPaymentPolicyMethodTree(paymentPolicyMethodSelectDto);
    }

    /**
     * 결제정책 저장
     * @param paymentPolicyManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/savePaymentPolicyMng.json")
    public ResponseObject<Object> savePaymentPolicyMng(@RequestBody PaymentPolicyManageDto paymentPolicyManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        paymentPolicyManageDto.setRegId(userCd);
        paymentPolicyManageDto.setChgId(userCd);

        return paymentPolicyManageService.savePaymentPolicyMng(paymentPolicyManageDto);
    }
}