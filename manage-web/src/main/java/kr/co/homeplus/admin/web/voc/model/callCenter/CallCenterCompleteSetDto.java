package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 처리완료 요청 DTO")
public class CallCenterCompleteSetDto {
  @ApiModelProperty(notes = "부모상담일련번호")
  @JsonProperty("par_history_seq")
  private String par_history_seq;

  @ApiModelProperty(notes = "처리사유")
  @JsonProperty("complet_reason")
  private String complet_reason;

  @ApiModelProperty(notes = "처리사유 상세")
  @JsonProperty("complet_detail")
  private String complet_detail;

  @ApiModelProperty(notes = "발생자")
  @JsonProperty("issue_generator")
  private String issue_generator;

  @ApiModelProperty(notes = "발생원인")
  @JsonProperty("issue_reason")
  private String issue_reason;

  @ApiModelProperty(notes = "처리자ID")
  @JsonProperty("complet_account_id")
  private String complet_account_id;

  @ApiModelProperty(notes = "처리자명")
  @JsonProperty("complet_account_nm")
  private String complet_account_nm;

  @ApiModelProperty(notes = "처리내용")
  @JsonProperty("complet_txt")
  private String complet_txt;
}