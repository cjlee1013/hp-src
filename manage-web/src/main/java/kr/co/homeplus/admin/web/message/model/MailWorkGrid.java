package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class MailWorkGrid {

    @RealGridColumnInfo(hidden = true)
    private long mailWorkSeq;
    @RealGridColumnInfo(headText = "작업명", columnType = RealGridColumnType.NAME)
    private String mailWorkName;
    @RealGridColumnInfo(headText = "설명", columnType = RealGridColumnType.NAME)
    private String description;
    @RealGridColumnInfo(headText = "상태", columnType = RealGridColumnType.LOOKUP)
    private WorkStatus workStatus;
    @RealGridColumnInfo(headText = "요청")
    private String regId;
    @RealGridColumnInfo(headText = "등록일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime regDt;
    @RealGridColumnInfo(headText = "수정자", hidden = true)
    private String chgId;
    @RealGridColumnInfo(headText = "수정일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME, hidden = true)
    private LocalDateTime chgDt;
    @RealGridColumnInfo(headText = "요청일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime workDt;
    @RealGridColumnInfo(headText = "예약발송여부", hidden = true)
    private String delayYn;
    @RealGridColumnInfo(headText = "완료일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime endDt;
}
