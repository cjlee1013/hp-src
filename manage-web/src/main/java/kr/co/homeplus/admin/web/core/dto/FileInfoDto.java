package kr.co.homeplus.admin.web.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.admin.web.core.dto.imageFile.UploadFileDto;
import lombok.Data;

/**
 * 파일 업로드 시에 받아오는 데이터. file info Dto.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileInfoDto {

    private String original_file;

    private UploadFileDto upload_file;

    /**
     * error 정보. null 일 경우 에러가 없음.
     */
    private String error;

    /**
     * errors 가 있는지 확인하여 true, false 반환.
     */
    public boolean hasError() {
        return this.error != null;
    }
}
