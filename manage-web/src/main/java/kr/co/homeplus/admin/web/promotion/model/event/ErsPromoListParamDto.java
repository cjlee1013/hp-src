package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErsPromoListParamDto {
    private String schDateType;

    private String schStartDt;

    private String schEndDt;

    private String schEventTargetType;

    private String schEventGiveType;

    private String schStoreId;

    private String schConfirmYn;

    private String schUseYn;

    private String schType;

    private String schValue;
}