package kr.co.homeplus.admin.web.partner.model.affiliate;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AffiliateSettleSetDto implements Serializable {

    private String bankCd;
    private String bankAccountNo;
    private String depositor;

}
