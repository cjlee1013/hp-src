package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class ManualHistoryDetailRequest {

    private String manualSendSeq;

    private String workType;
}
