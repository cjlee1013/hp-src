package kr.co.homeplus.admin.web.item.controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ChannelConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.enums.MallType;
import kr.co.homeplus.admin.web.item.enums.StoreType;
import kr.co.homeplus.admin.web.item.model.item.ItemCertParamDto;
import kr.co.homeplus.admin.web.item.model.item.ItemDispYnSetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemListGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemListOnlineStockGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemListOnlineStockParamDto;
import kr.co.homeplus.admin.web.item.model.item.ItemListParamDto;
import kr.co.homeplus.admin.web.item.model.item.ItemOptionValueGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemParamDto;
import kr.co.homeplus.admin.web.item.model.item.ItemProviderGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemSaleDFListGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemSetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemStatusSetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemStoreListGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemStoreStockMngListGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemTDBasicSetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemTDDetailSetDto;
import kr.co.homeplus.admin.web.item.model.item.MngNoticeGroupListGetDto;
import kr.co.homeplus.admin.web.item.model.item.MngNoticeListGetDto;
import kr.co.homeplus.admin.web.item.model.item.OnlineStockItemSetDto;
import kr.co.homeplus.admin.web.item.model.item.OnlineStockExcelDto;
import kr.co.homeplus.admin.web.item.model.item.OnlineStockMngGetDto;
import kr.co.homeplus.admin.web.item.model.item.ItemForceDisplaySetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemDetailHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupBasicGetDto;
import kr.co.homeplus.admin.web.item.model.item.hist.ItemHistGroupGetDto;
import kr.co.homeplus.admin.web.item.service.ItemHistService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import org.springframework.beans.BeanUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/item")
public class ItemController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    final private ItemHistService itemHistService;

    final private ExcelUploadService excelUploadService;

    @Value("${plus.resource-routes.upload.url}")
    private String upload;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    public ItemController(ResourceClient resourceClient,
            LoginCookieService cookieService,
            CodeService codeService,
            ItemHistService itemHistService,
            ExcelUploadService excelUploadService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.itemHistService = itemHistService;
        this.excelUploadService = excelUploadService;
    }

    /**
     * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/itemMain/{mallType}",
            "/itemMain/{mallType}/{menuType}"}, method = RequestMethod.GET
    )
    public String itemMain(Model model, @PathVariable String mallType,
            @PathVariable(required = false) String menuType) {

        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                ,	"item_type"         // 상품유형
                ,	"store_type"        // 점포구분 - Hyper,Club,Exp,DS
                ,   "item_status"       // 상품상태
                ,   "disp_yn"           // 노출여부
                ,   "set_yn"            // 설정여부
                ,   "limit_yn"          // 제한여부
                ,   "stop_limit_yn"     // 중지제한여부
                ,   "target_yn"         // 대상여부
                ,   "item_img_type"     // 상품이미지유형
                ,   "tax_yn"            // 과세여부
                ,   "commission_type"   // 수수료유형
                ,   "limit_duration"    // 기간제한
                ,   "holiday_except"    // 공휴일제외유형
                ,   "pr_type"           // 셀링포인트유형
                ,   "rsv_type"          // 예약유형
                ,   "reg_yn"            // 등록여부
                ,   "adult_type"        // 성인상품유형
                ,   "proof_file"        // 증빙서류
                ,   "item_is_cert"      // 상품인증여부
                ,   "item_cert_type"    // 상품읹증유형
                ,   "item_cert_group"   // 상품인증그룹
                ,   "item_exempt_type"  // 상품인증면제유형
                ,   "pick_yn"           // 픽업상품유형
                ,   "stop_deal_yn"      // 취급중지여부
                ,   "item_cert_et_cer"  // 안전인증 소비효율등급
                ,   "stock_type"        // 재고관리유형
                ,   "safe_number_use_yn"// 안심번호
        );

        MallType mallTypePolicy = MallType.valueFor(mallType);

        model.addAttribute("mallType", mallTypePolicy.getType());
        model.addAttribute("mallTypeNm", mallTypePolicy.getDescription());
        model.addAttribute("menuType", menuType);
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("storeIdDefault", "37"); //금천점 코드 37

        if (mallTypePolicy.equals(MallType.TD)) {
            model.addAllAttributes(RealGridHelper.createForGroup("saleDFListGridBaseInfo", ItemSaleDFListGetDto.class));
            model.addAllAttributes(RealGridHelper.createForGroup("storeListGridBaseInfo", ItemStoreListGetDto.class));

            model.addAttribute("itemCertTypeObj",  new Gson().toJson(code.get("item_cert_type")));
            model.addAttribute("itemCertGroupObj",  new Gson().toJson(code.get("item_cert_group")));
        }

        model.addAllAttributes(RealGridHelper.createForGroup("itemListGridBaseInfo", ItemListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("optListGridBaseInfo", ItemOptionValueGetDto.class));

        return "/item/itemMain";
    }

    @GetMapping("/onlineStockMng")
    public String onlineStockMng(Model model) {

        codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                ,   "disp_yn"           // 노출여부
                ,   "set_yn"            // 설정여부
        );

        MallType mallTypePolicy = MallType.valueFor("TD");

        model.addAttribute("mallType", mallTypePolicy.getType());
        model.addAttribute("menuType", "detail");

        model.addAllAttributes(RealGridHelper.createForGroup("storeListGridBaseInfo", ItemStoreStockMngListGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("itemListGridBaseInfo", ItemListOnlineStockGetDto.class));

        return "/item/onlineStockMng";
    }

    @ResponseBody
    @GetMapping("/getStockTemplate.json")
    public Map<String, Object> getStockTemplate() {
        ResourceClientRequest<Map<String, Object>> request = ResourceClientRequest.<Map<String, Object>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/getStockTemplate")
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();

    }

    @ResponseBody
    @GetMapping("/getOnlineStockList.json")
    public List<ItemListOnlineStockGetDto> onlineStockMngList(
            ItemListOnlineStockParamDto listParamDto) {
        String apiUri = "/item/getOnlineStockList";

        ResourceClientRequest<List<ItemListOnlineStockGetDto>> request = ResourceClientRequest.<List<ItemListOnlineStockGetDto>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(StringUtil.getRequestString(apiUri, ItemListOnlineStockParamDto.class, listParamDto))
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();

    }

    @ResponseBody
    @GetMapping("/getStockTemplateList.json")
    public List<OnlineStockMngGetDto> getStockTemplateList() {
        String apiUri = "/item/getStockTemplateList";

        ResourceClientRequest<List<OnlineStockMngGetDto>> request = ResourceClientRequest.<List<OnlineStockMngGetDto>>getBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @PostMapping("/setOnlineStockExcel.json")
    public ResponseResult setOnlineStockExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<OnlineStockExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
                .header(0, "점포코드", "storeId", true)
                .header(1, "FF", "ff", true)
                .header(2, "GR", "gr", true)
                .header(3, "GM", "gm", true)
                .header(4, "CL", "cl", true)
                .header(5, "Electric", "electric", true)
                .header(6, "11번가", "eleven", true)
                .header(7, "N마트", "naver", true)
                .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<OnlineStockExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(OnlineStockExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<OnlineStockExcelDto> arrSetParamDto = new ArrayList<>();
        String regId = cookieService.getUserInfo().getEmpId();

        excelData
                .stream()
                .forEach(item -> {
                    if(StringUtils.isNotEmpty(item.getStoreId())) {
                        String regex = "[^0-9]";
                        String storeId = item.getStoreId().replaceAll(regex,"");

                        if (StringUtils.isNotEmpty(storeId)) {
                            OnlineStockExcelDto setDto = new OnlineStockExcelDto();
                            item.setFf(item.getFf().replaceAll(regex,""));
                            item.setGr(item.getGr().replaceAll(regex,""));
                            item.setGm(item.getGm().replaceAll(regex,""));
                            item.setCl(item.getCl().replaceAll(regex,""));
                            item.setElectric(item.getElectric().replaceAll(regex,""));
                            item.setEleven(item.getEleven().replaceAll(regex,""));
                            item.setNaver(item.getNaver().replaceAll(regex,""));

                            BeanUtils.copyProperties(item, setDto);
                            setDto.setUserId(regId);

                            arrSetParamDto.add(setDto);
                        }
                    }
                });

        String apiUri = "/item/setOnlineStockExcel";

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri(apiUri)
                .postObject(arrSetParamDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 상품관리 > 상품 등록/수정 > 상품조회 리스트
     * @param listParamDto
     * @return
     */
    @ResponseBody
    @PostMapping("/getItemList.json")
    public List<ItemListGetDto> getItemList(@RequestBody ItemListParamDto listParamDto) {
        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM,
                listParamDto,
                "/item/getItemList",
                new ParameterizedTypeReference<ResponseObject<List<ItemListGetDto>>>() {
                }).getData();
    }

    /**
     * 상품관리 > 상품 등록/수정 > 상품조회(단일)
     * @param itemParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getDetail.json", method = RequestMethod.GET)
    public ItemGetDto getDetail(ItemParamDto itemParamDto) {
        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                StringUtil.getRequestString("/item/getDetail", ItemParamDto.class, itemParamDto),
                new ParameterizedTypeReference<ResponseObject<ItemGetDto>>() {
                }).getData();
    }

    /**
     * 상품관리 > 셀러상품 등록/수정 > 상품등록
     * @param itemSetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setItem.json"}, method = RequestMethod.POST)
    public Object setItem(@RequestBody ItemSetDto itemSetDto) {

        itemSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        itemSetDto.setMallType(MallType.DS.getType());
        itemSetDto.setChannel(ChannelConstants.ADMIN);

        String apiUri = "/item/setItem";

        if (itemSetDto.getTempYn().equals("Y")) {
            apiUri = "/item/setItemTemp";
        }

        ResponseObject<ResponseResult> responseObject = resourceClient
                .postForResponseObject(ResourceRouteName.ITEM, itemSetDto, apiUri,
                        new ParameterizedTypeReference<>() {
                        });

        return responseObject;
    }

    /**
     * 상품관리 > 점포상품관리 > 점포상품 기본정보 수정
     * @param itemTDBasicSetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemTDBasic.json"}, method = RequestMethod.POST)
    public Object setItemTDBasic(@RequestBody ItemTDBasicSetDto itemTDBasicSetDto) {

        itemTDBasicSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        itemTDBasicSetDto.setMallType(MallType.TD.getType());
        itemTDBasicSetDto.setChannel(ChannelConstants.ADMIN);

        ResponseObject<ResponseResult> responseObject = resourceClient
                .postForResponseObject(
                        ResourceRouteName.ITEM,
                        itemTDBasicSetDto,
                        "/item/setItemTDBasic",
                        new ParameterizedTypeReference<>() {
                        });

        return responseObject.getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 점포상품 상세정보 수정
     * @param itemTDDetailSetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setItemTDDetail.json"}, method = RequestMethod.POST)
    public Object setItemTDDetail(@RequestBody ItemTDDetailSetDto itemTDDetailSetDto) {

        itemTDDetailSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        itemTDDetailSetDto.setMallType(MallType.TD.getType());
        itemTDDetailSetDto.setChannel(ChannelConstants.ADMIN);

        ResponseObject<ResponseResult> responseObject = resourceClient
                .postForResponseObject(ResourceRouteName.ITEM,
                        itemTDDetailSetDto,
                        "/item/setItemTDDetail",
                        new ParameterizedTypeReference<>() {
                        });

        return responseObject.getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 온라인재고 일괄설정
     * @param itemTDDetailSetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setOnlineStock.json"}, method = RequestMethod.POST)
    public Object setOnlineStock(@RequestBody ItemTDDetailSetDto itemTDDetailSetDto) {

        itemTDDetailSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        itemTDDetailSetDto.setMallType(MallType.TD.getType());
        itemTDDetailSetDto.setStoreType(StoreType.HYPER.getType());

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/setOnlineStock")
                .postObject(itemTDDetailSetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/setOnlineStockItem.json"}, method = RequestMethod.POST)
    public Object setOnlineStockItem(@RequestBody OnlineStockItemSetDto onlinestockbyitemsetdto) {

        onlinestockbyitemsetdto.setUserId(cookieService.getUserInfo().getEmpId());

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/setOnlineStockItem")
                .postObject(onlinestockbyitemsetdto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 판매가능/영구/일시중지 사유 등록
     * @param itemStatusSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/setItemStatus.json")
    public Object setStopReason(@ModelAttribute ItemStatusSetDto itemStatusSetDto) {
        int limitCnt = 100;
        itemStatusSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        List<List<String>> itemNoList = ListUtils.partition(itemStatusSetDto.getItemNo(), limitCnt);

        for (List<String> item : itemNoList) {
            ItemStatusSetDto params = new ItemStatusSetDto();

            BeanUtils.copyProperties(itemStatusSetDto, params);
            params.setItemNo(item);


            ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                    .apiId(ResourceRouteName.ITEM)
                    .uri("/item/setItemStatus")
                    .postObject(params)
                    .typeReference(new ParameterizedTypeReference<>() {})
                    .build();

            resourceClient.post(request, new TimeoutConfig()).getBody().getData();
        }

        ResponseResult responseResult = new ResponseResult();
        responseResult.setReturnMsg("판매상태가 변경되었습니다.");

        responseResult.setReturnMsg("상품 " + itemStatusSetDto.getItemNo().size() + "건의 판매상태가 변경되었습니다.");

        return responseResult;
    }

    @ResponseBody
    @PostMapping("/setItemDispYn.json")
    public Object setItemDispYn(@ModelAttribute ItemDispYnSetDto itemDispYnSetDto) {

        itemDispYnSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResponseObject<ResponseResult> responseObject = resourceClient
                .postForResponseObject(
                        ResourceRouteName.ITEM,
                        itemDispYnSetDto,
                        "/item/setItemDispYn",
                        new ParameterizedTypeReference<>() {
                        });

        return responseObject.getData();
    }

    /**
     * 상품정보고시관리 그룹 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getMngNoticeGroupList.json", method = RequestMethod.GET)
    public List<MngNoticeGroupListGetDto> getMngNoticeGroupList() {
        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/item/getMngNoticeGroupList",
                new ParameterizedTypeReference<ResponseObject<List<MngNoticeGroupListGetDto>>>() {
                }).getData();
    }

    /**
     * 상품정보고시관리 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getMngNoticeList.json", method = RequestMethod.GET)
    public List<MngNoticeListGetDto> getMngNoticeList(
            @RequestParam(name = "gnoticeNo") Integer gnoticeNo
    ) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("gnoticeNo", gnoticeNo));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/item/getMngNoticeList" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<List<MngNoticeListGetDto>>>() {
                }).getData();
    }

    @ResponseBody
    @RequestMapping(value = "/getItemProviderList.json", method = RequestMethod.GET)
    public List<ItemProviderGetDto> getItemProviderList() {
        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/item/getItemProviderList",
                new ParameterizedTypeReference<ResponseObject<List<ItemProviderGetDto>>>() {
                }).getData();
    }

    /**
     * 상품 히스토리 그룹정보 (페이지 번호를 증가시켜 호출하면 됨)
     * @param itemNo
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getItemHistGroup.json", method = RequestMethod.GET)
    public List<ItemHistGroupGetDto> getItemHistGroup(@RequestParam String itemNo, @RequestParam int pageNo, @RequestParam(required = false) String storeType) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("itemNo", itemNo));
        setParameterList.add(SetParameter.create("pageNo", pageNo));
        setParameterList.add(SetParameter.create("storeType", storeType));

        return itemHistService
                .getItemHistGroup(
                        resourceClient.getForResponseObject(
                                ResourceRouteName.ITEM,
                                "/item/getItemHistList" + StringUtil.getParameter(setParameterList),
                                new ParameterizedTypeReference<ResponseObject<List<ItemHistGetDto>>>() {
                                }).getData()
                );
    }

    /**
     * 상품 히스토리 BASIC 그룹정보 (페이지 번호를 증가시켜 호출하면 됨)
     * @param itemNo
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getItemHistBasicGroup.json", method = RequestMethod.GET)
    public List<ItemHistGroupBasicGetDto> getItemHistBasicGroup(@RequestParam String itemNo, @RequestParam int pageNo, @RequestParam(required = false) String storeType) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("itemNo", itemNo));
        setParameterList.add(SetParameter.create("pageNo", pageNo));
        setParameterList.add(SetParameter.create("storeType", storeType));

        return itemHistService
                .getItemHistBasicGroup(
                        resourceClient.getForResponseObject(
                                ResourceRouteName.ITEM,
                                "/item/getItemHistList" + StringUtil.getParameter(setParameterList),
                                new ParameterizedTypeReference<ResponseObject<List<ItemHistBasicGetDto>>>() {
                                }).getData()
                );
    }

    /**
     * 상품 히스토리 DETAIL 그룹정보 (페이지 번호를 증가시켜 호출하면 됨)
     * @param itemNo
     * @param pageNo
     * @param storeType
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/getItemHistDetailGroup.json", method = RequestMethod.GET)
    public List<ItemDetailHistGetDto> getItemHistDetailGroup(@RequestParam String itemNo, @RequestParam int pageNo, @RequestParam(required = false) String storeType) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("itemNo", itemNo));
        setParameterList.add(SetParameter.create("pageNo", pageNo));
        setParameterList.add(SetParameter.create("storeType", storeType));

        return itemHistService
                .getItemHistDetailGroup(
                        resourceClient.getForResponseObject(
                                ResourceRouteName.ITEM,
                                "/item/getItemHistList" + StringUtil.getParameter(setParameterList),
                                new ParameterizedTypeReference<ResponseObject<List<ItemDetailHistGetDto>>>() {
                                }).getData()
                );
    }

    /**
     * 상품 히스토리 - 선택한 히스토리 비교
     * @param selectHistSeq
     * @param prevHistSeq
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getItemHistSelectDiff.json", method = RequestMethod.GET)
    public ItemHistGetDto getItemHistSelectDiff(@RequestParam String selectHistSeq, @RequestParam(required = false) String prevHistSeq) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("seqList", selectHistSeq + "," + prevHistSeq));

        if (ObjectUtils.isEmpty(prevHistSeq)) {
            setParameterList.add(SetParameter.create("seqList", selectHistSeq));
        }

        final List<ItemHistGetDto> prodHistGetDtoList = resourceClient
                .getForResponseObject(
                        ResourceRouteName.ITEM,
                        "/item/getItemHistTargetList" + StringUtil.getParameter(setParameterList),
                        new ParameterizedTypeReference<ResponseObject<List<ItemHistGetDto>>>() {
                        }).getData();

        return itemHistService.getItemHistSelectDiff(prodHistGetDtoList);
    }

    /**
     * 상품 인증 조회
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/checkCert.json", method = RequestMethod.GET)
    public ResponseObject checkItemCert(ItemCertParamDto itemCertParamDto){
        ResponseObject res = resourceClient.getForResponseObject(ResourceRouteName.ITEM,
               StringUtil.getRequestString("/item/checkItemCert", ItemCertParamDto.class, itemCertParamDto));
        return res;
    }

    /*
    * 점포상품 강제전
    * */
    @GetMapping("/itemForceDisplay")
    public String itemForceDisplay(Model model) {

        codeService.getCodeModel(model,
                "store_type"       // 점포 타입
        );

        return "/item/itemForceDisplay";
    }

    @ResponseBody
    @PostMapping("/setItemForceDisplay.json")
    public ResponseResult setItemForceDisplay(@RequestBody ItemForceDisplaySetDto itemForceDisplaySetDto) {

        itemForceDisplaySetDto.setUserId(cookieService.getUserInfo().getEmpId());

        ResourceClientRequest<ResponseResult> request = ResourceClientRequest.<ResponseResult>postBuilder()
                .apiId(ResourceRouteName.ITEM)
                .uri("/item/setItemForceDisplay")
                .postObject(itemForceDisplaySetDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }


}
