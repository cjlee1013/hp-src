package kr.co.homeplus.admin.web.core.dto.imageFile;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThumbnailFileDto {
    private Long file_size;
    private Integer width;
    private Integer height;
    private String mime_type;
    private String absolute_path;
    private String full_name;
    private String file_name;
    private String extension;
    private String directory;
    private String url;
}
