package kr.co.homeplus.admin.web.partner.model.sellerApiKey;

import java.util.List;
import lombok.Data;

/**
 * 셀러연동 API Key 관리 사용여부 수정
 */
@Data
public class SellerUseYnSetDto {

	//상품번호
	private List<String> plusapiSeqList;

	//노출여부
	private String useYn;

	//등록|수정자
	private String userId;
}