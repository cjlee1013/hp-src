package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EtcPromoDetailSetDto {
    private Long etcNo;

    private String promoType;

    private String storeType;

    private String etcMngNm;

    private String etcInfo;

    private String promoStartDt;

    private String promoEndDt;

    private String useYn;

    private String dispStoreType;

    private String empNo;

    // 노출 점포
    private List<Integer> storeIdList;

    // 등록 상품 리스트
    private List<EtcPromoItemSetDto> insertItemList;

    // 수정 상품 리스트
    private List<EtcPromoItemSetDto> updateItemList;
}