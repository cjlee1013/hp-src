package kr.co.homeplus.admin.web.item.controller;


import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.certification.SessionService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.item.model.category.categoryCommission.CategoryCommissionGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/commission")
public class CategoryCommissionController {
    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    public static final List<RealGridColumn> COMMISSION_GRID_HEAD = new ArrayList();
    public static final RealGridOption COMMISSION_GRID_OPTION = new RealGridOption("fill", false, false, false);


    public CategoryCommissionController(
            ResourceClient resourceClient,
            LoginCookieService cookieService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;

        COMMISSION_GRID_HEAD.add(new RealGridColumn("lcateNm", "lcateNm", "1depth", RealGridColumnType.NAME, 200, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("mcateNm", "mcateNm", "2depth", RealGridColumnType.NAME, 200, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("scateNm", "scateNm", "3depth", RealGridColumnType.NAME, 200, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("dcateNm", "dcateNm", "4depth", RealGridColumnType.NAME, 200, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("dcateCd", "dcateCd", "4depth 코드", RealGridColumnType.SERIAL, 100, false, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("commissionRate", "commissionRate", "기준수수료", RealGridColumnType.COMMISSION_C, 150, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("regNm", "regNm", "등록자", RealGridColumnType.BASIC, 100, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("regDt", "regDt", "등록일", RealGridColumnType.DATETIME, 150, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("chgNm", "chgNm", "수정자", RealGridColumnType.BASIC, 100, true, true));
        COMMISSION_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.DATETIME, 150, true, true));
    }

    /**
     * 카테고리관리 > 카테고리관리 > 카테고리 기준수수료
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = { "/categoryCommissionMain"}, method = RequestMethod.GET)
    public String categoryCommissionMain(Model model) {
        model.addAttribute("commissionGridBaseInfo", new RealGridBaseInfo("commissionGridBaseInfo", COMMISSION_GRID_HEAD, COMMISSION_GRID_OPTION).toString());
        return "/item/categoryCommissionMain";
    }

    /**
     * 카테고리관리 > 카테고리관리 > 카테고리 기준수수료 > 조회
     *
     * @param categoryCommissionGetDto
     * @return
     * @throws Exception
     */
    @ResponseBody @RequestMapping(value = {"/getCategoryCommission.json"}, method = RequestMethod.GET)
    public List<CategoryCommissionGetDto> getCategoryCommission(@ModelAttribute CategoryCommissionGetDto categoryCommissionGetDto) {
        return (List<CategoryCommissionGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, categoryCommissionGetDto, "/common/CategoryMng/getCategoryCommission"
                , new ParameterizedTypeReference<ResponseObject<List<CategoryCommissionGetDto>>>() {}).getData();
    }

}
