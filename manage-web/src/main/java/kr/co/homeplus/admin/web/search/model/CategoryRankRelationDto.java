package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryRankRelationDto {
    private String keyword;
    private long relationId;
    private long rankingId;
    private int categoryId;
    private String categoryNm;
    private String boostType;
    private String creator;
    private String modifier;
    private String regDt;
    private String modDt;
    private char useFlag;
    private String useFlagNm;
    private char resetFlag;
    private String resetFlagNm;
}
