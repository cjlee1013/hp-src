package kr.co.homeplus.admin.web.settle.model.margin;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class MarginDetailListGetDto {
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", width = 110, sortable = true)
    private String basicDt;

    @ApiModelProperty(value = "과면세")
    @RealGridColumnInfo(headText = "과면세", width = 80, sortable = true)
    private String taxYn;

    @ApiModelProperty(value = "카테고리ID")
    @RealGridColumnInfo(headText = "카테고리ID", width = 120, sortable = true)
    private String deptNo;

    @ApiModelProperty(value = "CLASS")
    @RealGridColumnInfo(headText = "CLASS", width = 120, sortable = true)
    private String classNo;

    @ApiModelProperty(value = "SUB CLASS")
    @RealGridColumnInfo(headText = "SUB CLASS", width = 120, sortable = true)
    private String subClassNo;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 130, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 120, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 120, sortable = true)
    private String partnerName;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 350, sortable = true, columnType = RealGridColumnType.NONE)
    private String itemName;

    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(headText = "상품금액", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String itemSaleAmt;

    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(headText = "수량", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeQty;

    @ApiModelProperty(value = "할인금액")
    @RealGridColumnInfo(headText = "할인금액", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;

    @ApiModelProperty(value = "매출(IN VAT)")
    @RealGridColumnInfo(headText = "매출(IN VAT)", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeInAmt;

    @ApiModelProperty(value = "매출(EX VAT)")
    @RealGridColumnInfo(headText = "매출(EX VAT)", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeExAmt;

    @ApiModelProperty(value = "원가(IN VAT)")
    @RealGridColumnInfo(headText = "원가(IN VAT)", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orgInAmt;

    @ApiModelProperty(value = "원가(EX VAT)")
    @RealGridColumnInfo(headText = "원가(EX VAT)", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String orgExAmt;

    @ApiModelProperty(value = "마진")
    @RealGridColumnInfo(headText = "마진", width = 120, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String margin;

    @ApiModelProperty(value = "마진율")
    @RealGridColumnInfo(headText = "마진율", width = 80, sortable = true, columnType =RealGridColumnType.NUMBER_C)
    private String marginRate;
}
