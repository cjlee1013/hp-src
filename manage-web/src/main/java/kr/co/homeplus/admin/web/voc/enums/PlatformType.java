package kr.co.homeplus.admin.web.voc.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum PlatformType implements RealGridLookUpSupport {

    APP("app"),
    MWEB("mweb");

    private static final Map<String, String> CASE_NAME_MAP =
        Stream.of(values()).collect(Collectors.toMap(PlatformType::name, PlatformType::getCaseName));

    @Getter
    private final String caseName;

    PlatformType(String caseName) {
        this.caseName = caseName;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return CASE_NAME_MAP;
    }
}
