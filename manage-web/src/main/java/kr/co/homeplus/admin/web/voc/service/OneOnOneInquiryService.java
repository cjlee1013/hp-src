package kr.co.homeplus.admin.web.voc.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryListGetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryListSetDto;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryMemberMemoListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OneOnOneInquiryService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;
  private final LoginCookieService cookieService;
  private final PrivacyLogService privacyLogService;

  /**
   * 1:1문의 리스트 조회
   */
  public List<OneOnOneInquiryListGetDto> getOneOnOneInquiryList(OneOnOneInquiryListSetDto oneOnOneInquiryListSetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      oneOnOneInquiryListSetDto.setSchStoreId(shipCommonService.getStoreId());
      oneOnOneInquiryListSetDto.setSchStoreType(storeInfo.getStoreType());
      oneOnOneInquiryListSetDto.setStoreOfficeYn("Y");
    }

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        oneOnOneInquiryListSetDto,
        "/admin/oneOnOneInquiry/getOneOnOneInquiryList",
        new ParameterizedTypeReference<ResponseObject<List<OneOnOneInquiryListGetDto>>>(){}).getData();
  }

  /**
   * 1:1문의 상세 조회
   */
  public OneOnOneInquiryDetailGetDto getOneOnOneInquiryDetail(HttpServletRequest request, String inqryNo) {
    String userNo = shipCommonService.getUserCd();

    final String privacyAccessIp = ServletUtils.clientIP(request);
    final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

    final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
        .txTime(DateTimeUtil.getNowMillYmdHis())
        .accessIp(privacyAccessIp)
        .userId(privacyAccessUserId)
        .txCodeUrl(request.getRequestURI())
        .txMethod(PersonalLogMethod.SELECT.name())
        .txCodeName("1:1문의관리 상세 조회")
        .processor(String.valueOf(userNo))
        .processorTaskSql("")
        .build();

    privacyLogService.send(logInfo);

    final HttpHeaders headers = new HttpHeaders();
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

    return resourceClient.getForResponseObject(
        ResourceRouteName.USERMNG,
        "/admin/oneOnOneInquiry/getOneOnOneInquiryDetail?inqryNo=" + inqryNo,
        headers,
        new ParameterizedTypeReference<ResponseObject<OneOnOneInquiryDetailGetDto>>() {}).getData();
  }

  /**
   * 1:1문의 회원 메모 조회
   */
  public List<OneOnOneInquiryMemberMemoListGetDto> getInquiryMemberMemoList(
      HttpServletRequest request, String userNo) {

    final String privacyAccessIp = ServletUtils.clientIP(request);
    final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

    final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
        .txTime(DateTimeUtil.getNowMillYmdHis())
        .accessIp(privacyAccessIp)
        .userId(privacyAccessUserId)
        .txCodeUrl(request.getRequestURI())
        .txMethod(PersonalLogMethod.SELECT.name())
        .txCodeName("1:1문의관리 회원메모 조회")
        .processor(String.valueOf(userNo))
        .processorTaskSql("")
        .build();

    privacyLogService.send(logInfo);

    final HttpHeaders headers = new HttpHeaders();
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

    return resourceClient.getForResponseObject(
        ResourceRouteName.USERMNG,
        "/admin/oneOnOneInquiry/getInquiryMemberMemoList?userNo=" + userNo,
        headers,
        new ParameterizedTypeReference<ResponseObject<List<OneOnOneInquiryMemberMemoListGetDto>>>() {}).getData();
  }

  /**
   * 1:1문의 답변등록
   */
  public ResponseObject<Object> setInquiryAnswer(OneOnOneInquiryDetailSetDto oneOnOneInquiryDetailSetDto) throws Exception {
    oneOnOneInquiryDetailSetDto.setRegId(shipCommonService.getUserId());
    oneOnOneInquiryDetailSetDto.setRegNm(shipCommonService.getUserNm());

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        oneOnOneInquiryDetailSetDto,
        "/admin/oneOnOneInquiry/setInquiryAnswer",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

}
