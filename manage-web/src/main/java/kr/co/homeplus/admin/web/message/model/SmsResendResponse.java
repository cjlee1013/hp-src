package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class SmsResendResponse {

    private long smsOriginalWorkSeq;

    private long smsResendWorkSeq;

    private String errorMessage;
}
