package kr.co.homeplus.admin.web.manage.model.brandZone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandZoneParamDto {

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

}

