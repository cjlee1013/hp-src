package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringPickGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipMonitoring.ShipMonitoringPickSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipMonitoringPickService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * 회수지연조회 리스트 조회
   */
  public List<ShipMonitoringPickGetDto> getShipMonitoringPickList(ShipMonitoringPickSetDto shipMonitoringPickSetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      shipMonitoringPickSetDto.setSchStoreId(shipCommonService.getStoreId());
      shipMonitoringPickSetDto.setSchStoreType(storeInfo.getStoreType());
    }

    return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            shipMonitoringPickSetDto,
            "/admin/monitoring/getMonitoringPickList",
            new ParameterizedTypeReference<ResponseObject<List<ShipMonitoringPickGetDto>>>(){}).getData();
  }

}
