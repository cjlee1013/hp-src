package kr.co.homeplus.admin.web.settle.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PartnerSettleState {
    //정신관리 > 파트너 정산상태 (검색리스트)
    SETTLE_STATUS_WAIT("1", "정산대기"),
    SETTLE_STATUS_COMPLETE_AUTO("20", "정산완료(자동)"),
    SETTLE_STATUS_COMPLETE_MANUAL("2", "정산완료(수동)"),
    SETTLE_STATUS_POSTPONE_AUTO("3", "정산보류(자동)"),
    SETTLE_STATUS_POSTPONE_MANUAL("4", "정산보류(수동)"),
    SETTLE_STATUS_POSTPONE_OFFSET("6", "정산보류(상계)"),
    SETTLE_STATUS_POSTPONE_CLOSE("5", "정산보류(마감)"),

    ;

    private String code;
    private String name;
}
