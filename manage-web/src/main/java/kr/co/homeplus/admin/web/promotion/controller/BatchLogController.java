package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.promotion.model.monitering.BatchLogListGetDto;
import kr.co.homeplus.admin.web.promotion.model.monitering.BatchLogListParamDto;
import kr.co.homeplus.admin.web.promotion.model.monitering.IdeBatchInfoListGetDto;
import kr.co.homeplus.admin.web.promotion.model.monitering.IdeBatchInfoListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("/monitering")
public class BatchLogController {
    private final LoginCookieService loginCookieService;
    private final ResourceClient resourceClient;
    private final CodeService codeService;

    public BatchLogController(LoginCookieService loginCookieService, ResourceClient resourceClient, CodeService codeService) {
        this.loginCookieService = loginCookieService;
        this.resourceClient = resourceClient;
        this.codeService = codeService;
    }

    @ApiOperation(value = "배치로그 모니터링 main 화면")
    @ApiResponse(code = 200, message = "배치로그 모니터링 main 화면")
    @GetMapping("/batchLog/main")
    public String exhMain(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "batch_status"           // 배치상태
            , "use_yn"           // 사용여부
        );

        model.addAttribute("batchStatus", code.get("batch_status"));
        model.addAttribute("useYn", code.get("use_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("batchStatusJson", om.writeValueAsString(code.get("batch_status")));

        model.addAllAttributes(RealGridHelper.create("ideBatchInfoGridBaseInfo", IdeBatchInfoListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("batchLogGridBaseInfo", BatchLogListGetDto.class));

        return "/promotion/batchLogMain";
    }

    /**
     * 배치로그 조회
     * @param batchLogListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/batchLog/getLogList.json"})
    public List<BatchLogListGetDto> getLogList(BatchLogListParamDto batchLogListParamDto) {
        String apiUri = "/monitering/getBatchLogList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BatchLogListGetDto>>>() {};

        return (List<BatchLogListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, batchLogListParamDto, apiUri, typeReference).getData();
    }

    /**
     * IDE 배치 정보 조회
     * @param batchLogListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/batchLog/getIdeBatchList.json"})
    public List<IdeBatchInfoListGetDto> getIdeBatchList(BatchLogListParamDto batchLogListParamDto) {
        String apiUri = "/monitering/getIdeBatchList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<IdeBatchInfoListGetDto>>>() {};

        return (List<IdeBatchInfoListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, batchLogListParamDto, apiUri, typeReference).getData();
    }

    /**
     * IDE 배치 정보 등록/수정
     * @param ideBatchInfoListSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/batchLog/setIdeBatchInfo.json"})
    public Long setIdeBatchInfo(@RequestBody IdeBatchInfoListSetDto ideBatchInfoListSetDto) throws Exception {
        ideBatchInfoListSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/monitering/setIdeBatchInfo";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, ideBatchInfoListSetDto, apiUri, typeReference).getData();
    }
}