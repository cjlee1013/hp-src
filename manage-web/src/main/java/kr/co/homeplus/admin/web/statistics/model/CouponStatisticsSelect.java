package kr.co.homeplus.admin.web.statistics.model;

import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponStatisticsSelect {

    private String schStartDt;

    private String schEndDt;

    private String schSiteTypeExtend;

    private String schCouponKind;

    private Long schCouponNo;

    private String schCouponNm;

    private Long schDetailCouponNo;

    private String schDetailCouponKind;

    /* 추출 히스토리 저장용 DTO */
    ExtractCommonSetDto extractCommonSetDto;

}
