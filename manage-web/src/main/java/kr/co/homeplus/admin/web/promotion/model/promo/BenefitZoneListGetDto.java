package kr.co.homeplus.admin.web.promotion.model.promo;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class BenefitZoneListGetDto {
    @RealGridColumnInfo(headText = "혜택존 번호", sortable = true)
    private Long benefitNo;

    @RealGridColumnInfo(headText = "전시위치")
    private String dispLoc;

    @RealGridColumnInfo(headText = "배너명", width = 300)
    private String bannerNm;

    @RealGridColumnInfo(headText = "전시시작일시", width = 200)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시", width = 200)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "디바이스")
    private String deviceType;

    @RealGridColumnInfo(headText = "전시순서")
    private Integer priority;

    @RealGridColumnInfo(headText = "전시여부")
    private String dispYn;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}