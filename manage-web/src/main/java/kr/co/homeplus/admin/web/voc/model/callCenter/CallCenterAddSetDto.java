package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 문의추가 요청 DTO")
public class CallCenterAddSetDto {
  @ApiModelProperty(notes = "부모상담일련번호")
  @JsonProperty("par_history_seq")
  private String par_history_seq;

  @ApiModelProperty(notes = "처리부서")
  @JsonProperty("h_branch")
  private String h_branch;

  @ApiModelProperty(notes = "채널종류")
  @JsonProperty("channel_type")
  private String channel_type;

  @ApiModelProperty(notes = "처리상태")
  @JsonProperty("advisor_ing_type")
  private String advisor_ing_type;

  @ApiModelProperty(notes = "접수자ID")
  @JsonProperty("reg_account_id")
  private String reg_account_id;

  @ApiModelProperty(notes = "접수자명")
  @JsonProperty("reg_account_nm")
  private String reg_account_nm;

  @ApiModelProperty(notes = "접수내용")
  @JsonProperty("rmks")
  private String rmks;
}