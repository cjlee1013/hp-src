package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareListGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareSetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketCompareDailyGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketSettleListGetDto;
import kr.co.homeplus.admin.web.settle.model.market.MarketSettleListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketSettleService {

    private final ResourceClient resourceClient;
    /**
     * 정산관리 > 제휴정산관리
     */
    public List<MarketSettleListGetDto> getMarketSettleList(MarketSettleListSetDto marketSettleListSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marketSettleListSetDto,
            "/admin/market/getMarketSettleList",
            new ParameterizedTypeReference<ResponseObject<List<MarketSettleListGetDto>>>(){}).getData();
    }
    public List<MarketCompareDailyGetDto> getMarketCompareSum(MarketCompareSetDto marketCompareSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marketCompareSetDto,
            "/admin/market/getMarketCompareSum",
            new ParameterizedTypeReference<ResponseObject<List<MarketCompareDailyGetDto>>>(){}).getData();
    }
    public List<MarketCompareListGetDto> getMarketCompareList(MarketCompareSetDto marketCompareSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marketCompareSetDto,
            "/admin/market/getMarketCompareList",
            new ParameterizedTypeReference<ResponseObject<List<MarketCompareListGetDto>>>(){}).getData();
    }
}
