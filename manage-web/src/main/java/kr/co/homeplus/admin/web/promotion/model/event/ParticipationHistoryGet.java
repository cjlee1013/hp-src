package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 이벤트 참여이력
 */
@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class ParticipationHistoryGet {
    @RealGridColumnInfo(headText = "참여이력 일련번호", sortable = true)
    private long seq;
    @RealGridColumnInfo(headText = "이벤트코드", sortable = true, hidden = true)
    private String eventCode;
    @RealGridColumnInfo(headText = "이벤트명", sortable = true)
    private String eventName;
    @RealGridColumnInfo(headText = "회원번호", sortable = true)
    private long userNo;
    @RealGridColumnInfo(headText = "참여일시")
    private String regDt;
}
