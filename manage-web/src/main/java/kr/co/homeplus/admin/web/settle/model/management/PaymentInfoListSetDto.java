package kr.co.homeplus.admin.web.settle.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PaymentInfoListSetDto {
  @ApiModelProperty(notes = "시작일자")
  private String startDt;

  @ApiModelProperty(notes = "종료일자")
  private String endDt;

  @ApiModelProperty(notes = "상품구분")
  private String mallType;

  @ApiModelProperty(notes = "판매업체ID")
  private String partnerId;

  @ApiModelProperty(notes = "업체코드")
  private String ofVendorCd;

  @ApiModelProperty(notes = "판매업체명")
  private String partnerName;

  @ApiModelProperty(notes = "상품번호")
  private String itemNo;

  @ApiModelProperty(notes = "상품명")
  private String itemNm;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "배송번호")
  private String bundleNo;

  @ApiModelProperty(notes = "클레임그룹번호")
  private String claimNo;

  @ApiModelProperty(notes = "상품주문번호")
  private String orderItemNo;

  @ApiModelProperty(notes = "점포유형")
  private String storeType;

  @ApiModelProperty(notes = "점포코드")
  private String originStoreId;
}
