package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.StoreBasicInfoDto;
import kr.co.homeplus.admin.web.statistics.model.storeItem.CombineOrderStatusDto;
import kr.co.homeplus.admin.web.statistics.model.storeItem.CombineOrderStatusSelectDto;
import kr.co.homeplus.admin.web.statistics.service.CombineOrderStatusService;
import kr.co.homeplus.admin.web.statistics.service.RealtimeOrderStatisticsService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 시스템관리 > 합주문 주문 매출
 */
@Controller
@RequestMapping("/statistics/orderPayment")
public class CombineOrderStatusController {
    private final LoginCookieService loginCookieService;
    private final CombineOrderStatusService combineOrderStatusService;
    private final RealtimeOrderStatisticsService realtimeOrderStatisticsService;

    public CombineOrderStatusController(LoginCookieService loginCookieService, CombineOrderStatusService combineOrderStatusService,
            RealtimeOrderStatisticsService realtimeOrderStatisticsService) {
        this.loginCookieService = loginCookieService;
        this.combineOrderStatusService = combineOrderStatusService;
        this.realtimeOrderStatisticsService = realtimeOrderStatisticsService;
    }

    @GetMapping("/combineOrderStatusMain")
    public String combineOrderStatusMain(Model model) throws Exception {
        StoreBasicInfoDto storeBasicInfoDto = new StoreBasicInfoDto();

        // 로그인 사용자 점포 ID 있을 경우 점포 기본정보 조회
        String userStoreId = loginCookieService.getUserInfo().getStoreId();
        if (StringUtil.isNotEmpty(userStoreId)) {
            storeBasicInfoDto = realtimeOrderStatisticsService.getStoreBasicInfo(userStoreId);
        }
        // 조회한 점포 기본정보가 있을 경우 해당 점포만 세팅
        if (StringUtil.isNotEmpty(storeBasicInfoDto) && StringUtil.isNotEmpty(storeBasicInfoDto.getStoreId())) {
            model.addAttribute("userStoreType", storeBasicInfoDto.getStoreType());
            model.addAttribute("userStoreId", storeBasicInfoDto.getStoreId());
            model.addAttribute("userStoreNm", storeBasicInfoDto.getStoreNm());
        }

        model.addAllAttributes(RealGridHelper.createForGroup("combineOrderStatusBaseInfo", CombineOrderStatusDto.class));
        return "/statistics/combineOrderStatusMain";
    }

    /**
     * 조회
     */
    @ResponseBody
    @PostMapping("/getCombineOrderStatus.json")
    public List<CombineOrderStatusDto> getCombineOrderStatus(@RequestBody CombineOrderStatusSelectDto combineOrderStatusSelectDto) throws Exception {
        /* 조회에 대한 history 저장을 위해 필요 */
        combineOrderStatusSelectDto.setExtractCommonSetDto(
            ExtractCommonSetDto.builder()
            .empId(loginCookieService.getUserInfo().getEmpId())
            .menuNm(ExtractMenuName.COMBINE_ORDER_STATUS.getMenuNm())
            .extractType(ExtractType.SEARCH.name())
            .build()
        );
        return combineOrderStatusService.getCombineOrderStatus(combineOrderStatusSelectDto);
    }

    /**
     * 엑셀 이력 저장
     */
    @ResponseBody
    @PostMapping("/saveExtractHistoryCombineOrderStatus.json")
    public void saveExtractHistoryForExcelDown() throws Exception {
        /* 엑셀 추출에 대한 history 저장을 위해 필요 */
        combineOrderStatusService.saveExtractHistory(
            ExtractCommonSetDto.builder()
            .empId(loginCookieService.getUserInfo().getEmpId())
            .menuNm(ExtractMenuName.COMBINE_ORDER_STATUS.getMenuNm())
            .extractType(ExtractType.EXCEL_DOWN.name())
            .build()
        );
    }
}
