package kr.co.homeplus.admin.web.common.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.internal.lang3.StringUtils;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.exception.MemberSearchFailException;
import kr.co.homeplus.admin.web.common.exception.code.MemberSearchReturnCode;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.pop.AddressParam;
import kr.co.homeplus.admin.web.common.model.pop.AddressPopupDto;
import kr.co.homeplus.admin.web.common.model.pop.AffiliateChannelPopListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.AffiliateChannelPopListSelectDto;
import kr.co.homeplus.admin.web.common.model.pop.BrandPopupListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.BrandPopupSearchGetDto;
import kr.co.homeplus.admin.web.common.model.pop.CouponPopListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.CouponPopListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.ErsPromoPopListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.ErsPromoPopListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.GnbPopupListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.GnbPopupListSelectDto;
import kr.co.homeplus.admin.web.common.model.pop.ItemPopupListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.ItemPopupListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.ItemPopupOptionListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.ItemPopupOptionListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.MemberSearchDto;
import kr.co.homeplus.admin.web.common.model.pop.MemberSearchParam;
import kr.co.homeplus.admin.web.common.model.pop.OrderSendListGrid;
import kr.co.homeplus.admin.web.common.model.pop.OrderSendListInfo;
import kr.co.homeplus.admin.web.common.model.pop.OrderSendListRequest;
import kr.co.homeplus.admin.web.common.model.pop.OrderSendListResponse;
import kr.co.homeplus.admin.web.common.model.pop.PromoPopListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.PromoPopListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.SellerShopPopupGetDto;
import kr.co.homeplus.admin.web.common.model.pop.SellerShopPopupListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.StoreGetDto;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupListGetDto;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupListParamDto;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupSearchGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.annotation.CertificationNeedLess;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.MobileUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.enums.StoreType;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreHistGetDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.partner.model.PartnerAffiPopListGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerPopListGetDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CmsCouponGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CmsCouponSearchParamDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionPaymentDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 신규어드민 팝업 컨트롤러
 */
@Slf4j
@Controller
public class PopupController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;
    private final LoginCookieService cookieService;
    private final PrivacyLogService privacyLogService;

    // 회원 조회 팝업 그리드
    private final Map<String, Object> MEMBER_SEARCH_GRID_INFO = RealGridHelper.create("memberSearchGridBaseInfo", MemberSearchDto.class);

    public PopupController(ResourceClient resourceClient, CodeService codeService,
                           LoginCookieService cookieService,
                           PrivacyLogService privacyLogService) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
        this.cookieService = cookieService;
        this.privacyLogService = privacyLogService;
    }

    /**
     * 우편번호 검색 시스템 팝업
     * <p>
     * 인자로 callBack 스크립트를 받음
     *
     * @param callBackScript callback script
     * @return 우편번호 검색 시스템 팝업
     */
    @GetMapping(value = "/common/popup/zipCodePop")
    public String zipCodePop(HttpServletRequest request, Model model,
                             @RequestParam(value = "callBack", defaultValue = "") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("title", "우편번호 검색");
        return "/common/pop/zipCodePop";
    }

    /**
     * 주소검색 데이터 조회
     */
    @ResponseBody
    @CertificationNeedLess
    @GetMapping(value = "/common/getAddressList.json")
    public ResponseObject<AddressPopupDto> getAddressList(@ModelAttribute AddressParam addressParam) {
        StringBuilder searchAddressUrl = new StringBuilder("/address/V1_0/search");
        searchAddressUrl.append("?page=").append(addressParam.getPage());
        searchAddressUrl.append("&perPage=").append(addressParam.getPerPage());
        searchAddressUrl.append("&query=").append(addressParam.getQuery());

        return resourceClient
            .getForResponseObject(ResourceRouteName.ADDRESS, searchAddressUrl.toString(),
                new ParameterizedTypeReference<>() {});
    }

    /**
     * 제주/도서산간지역 확인 팝업
     *
     * @return 제주/도서산간지역 확인 팝업
     */
    @GetMapping(value = "/common/popup/checkIslandPop")
    public String checkIslandPop() {
        return "/common/pop/checkIslandPop";
    }

    /**
     * 제주/도서산간지역 팝업용도 주소검색 데이터 조회
     */
    @ResponseBody
    @GetMapping(value = "/common/getAddressListByIsland.json")
    public ResponseObject<AddressPopupDto> getAddressListByIsland(
        @ModelAttribute final AddressParam param) {
        final StringBuilder addressUri = new StringBuilder("/address/V1_0/search");
        addressUri.append("?page=")
            .append(param.getPage());
        addressUri.append("&perPage=")
            .append(param.getPerPage());
        addressUri.append("&query=")
            .append(param.getQuery());

        return resourceClient.getForResponseObject(ResourceRouteName.ADDRESS, addressUri.toString(),
            new ParameterizedTypeReference<>() {
        });
    }

    /**
     * 상품조회 공통팝업
     */
    @RequestMapping(value = "/common/popup/itemPop", method = RequestMethod.GET)
    public String itemPop(Model model,
                          @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                          @RequestParam(value = "isMulti", defaultValue = "") String isMulti,
                          @RequestParam(value = "partnerId", defaultValue = "") String partnerId,
                          @RequestParam(value = "itemType", defaultValue = "") String itemType,
                          @RequestParam(value = "mallType", defaultValue = "") String mallType,
                          @RequestParam(value = "storeType", defaultValue = "") String storeType,
                          @RequestParam(value = "siteType", defaultValue = "HOME") String siteType)
        throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "mall_type"        // 거래유형
            , "promo_store_type"              // 점포유형
        );

        model.addAttribute("mallType", code.get("mall_type"));
        model.addAttribute("storeType", code.get("promo_store_type"));

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("partnerId", partnerId);
        model.addAttribute("itemType", itemType);
        model.addAttribute("itemMallType", mallType);
        model.addAttribute("itemSiteType", siteType);
        model.addAttribute("itemStoreType", storeType);

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("promo_store_type")));

        // 상품조회 그리드
        model.addAllAttributes(RealGridHelper.create("itemPopGridBaseInfo", ItemPopupListGetDto.class));

        return "/common/pop/itemPop";
    }

    /**
     * 상품조회(용도옵션포함) 공통팝업
     */
    @RequestMapping(value = "/common/popup/itemOptionPop", method = RequestMethod.GET)
    public String itemOptionPop(Model model,
                                @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                                @RequestParam(value = "isMulti", defaultValue = "") String isMulti,
                                @RequestParam(value = "mallType", defaultValue = "") String mallType,
                                @RequestParam(value = "limitSize", defaultValue = "0") String limitSize) {

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("mallType", mallType);
        model.addAttribute("limitSize", limitSize);
        // 상품조회 그리드
        model.addAllAttributes(RealGridHelper.create("itemPopOptionGridBaseInfo", ItemPopupOptionListGetDto.class));

        return "/common/pop/itemPopOption";
    }


    /**
     * 상품 정보 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getItemPopList.json"})
    public List<ItemPopupListGetDto> getItemPopList(ItemPopupListParamDto itemPopupListParamDto) {
        String apiUri = "/common/popup/getItemPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemPopupListGetDto>>>() {};

        return (List<ItemPopupListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemPopupListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품 정보(용도옵션포함) 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getItemOptionPopList.json"})
    public List<ItemPopupOptionListGetDto> getItemOptionPopList(
        ItemPopupOptionListParamDto itemPopupOptionListParamDto) {
        String apiUri = "/common/popup/getItemOptionPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ItemPopupOptionListGetDto>>>() {
        };

        return (List<ItemPopupOptionListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, itemPopupOptionListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 파트너검색 팝업
     */
    @GetMapping(value = "/common/popup/partnerPop")
    public String partnerPop(Model model,
                             @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                             @RequestParam(value = "partnerId", defaultValue = "") String partnerId,
                             @RequestParam(value = "partnerType", defaultValue = "") String partnerType,
                             @RequestParam(value = "isMulti", defaultValue = "") String isMulti)
        throws Exception {
        codeService.getCodeModel(model,
            "operator_type"         // 사업자유형
            , "partner_status"              // 회원상태
        );

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("partnerId", partnerId);
        model.addAttribute("partnerType", partnerType);
        model.addAttribute("isMulti", isMulti);

        if (partnerType.equals("AFFI")) {
            // 제휴업체 그리드
            model.addAllAttributes(RealGridHelper.createForGroup("partnerPopGridBaseInfo", PartnerAffiPopListGetDto.class));
        } else {
            // 판매업체 그리드
            model.addAllAttributes(RealGridHelper.createForGroup("partnerPopGridBaseInfo", PartnerSellerPopListGetDto.class));
        }

        return "/common/pop/partnerPop";
    }

    /**
     * 카테고리검색 공통팝업 (트리)
     */
    @RequestMapping(value = "/common/popup/treeCategoryPop", method = RequestMethod.GET)
    public String treeCategoryPop(Model model,
                                  @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                                  @RequestParam(value = "tsChecked", defaultValue = "false") boolean tsChecked,
                                  @RequestParam(value = "transDataParentNode", defaultValue = "false") boolean transDataParentNode,
                                  @RequestParam(value = "transDataChildNode", defaultValue = "false") boolean transDataChildNode,
                                  @RequestParam(value = "isAllCheck", defaultValue = "false") boolean isAllCheck,
                                  @RequestParam(value = "stepFlag", defaultValue = "") String stepFlag)
        throws Exception {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("tsChecked", tsChecked);
        model.addAttribute("transDataParentNode", transDataParentNode);
        model.addAttribute("transDataChildNode", transDataChildNode);
        model.addAttribute("isAllCheck", isAllCheck);
        model.addAttribute("stepFlag", stepFlag);

        // 상품조회 그리드
        String treeCategoryPopName = "treeCategoryPop";
        model.addAttribute("treeCategoryPopName", treeCategoryPopName);
        model.addAttribute("treeCategoryPopMake", GridUtil.makeTree(treeCategoryPopName, "", tsChecked));

        return "/common/pop/treeCategoryPop";
    }

    /**
     * 카테고리검색 공통팝업 조회 (트리)
     */
    @ResponseBody
    @RequestMapping(value = "/common/getTreeCategoryPop.json", method = RequestMethod.GET)
    public String getTreeCategoryPop(
        @RequestParam(value = "stepFlag", defaultValue = "") String stepFlag) throws Exception {
        ResponseObject<List<Map<String, Object>>> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.ITEM
            , ""
            , "/common/popup/getCategoryPop" + stepFlag
            , new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {});

        return GridUtil.convertTreeData(responseObject.getData(), "cateCd", "parentCateCd", "depth", "cateNm", true, "전체 카테고리");
    }


    /**
     * 점포검색 팝업 (점포 조회 후 단건 Return)
     */
    @RequestMapping(value = "/common/popup/storePop", method = RequestMethod.GET)
    public String storePop(Model model,
                           @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                           @RequestParam(value = "storeType", defaultValue = "") String storeType,
                           @RequestParam(value = "storeRange", defaultValue = "REAL") String storeRange

    ) throws Exception {

        codeService.getCodeModel(model, "store_type"         // 점포유형
        );

        model.addAttribute("storeTypeParam", storeType);
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("storeRange", storeRange);

        // 점포 리스트 그리드
        model.addAllAttributes(RealGridHelper.create("storePopGridBaseInfo", StorePopupSearchGetDto.class));

        return "/common/pop/storePop";
    }

    /**
     * 점포검색 팝업 (점포 조회 후 단건 Return)
     */
    @ResponseBody
    @GetMapping(value = {"/common/getStoreSearchPopList.json"})
    public List<StorePopupSearchGetDto> getStoreSearchPopList(
        @RequestParam(value = "searchStoreType") String searchStoreType,
        @RequestParam(value = "storeRange") String storeRange,
        @RequestParam(value = "searchType") String searchType,
        @RequestParam(value = "searchValue") String searchValue) {
        String apiUri = "/common/popup/getStorePopupSearchList?storeType=" + searchStoreType + "&storeRange=" + storeRange + "&searchType=" + searchType + "&searchValue=" + searchValue;

        return (List<StorePopupSearchGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<List<StorePopupSearchGetDto>>>() {}).getData();
    }

    /**
     * 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getStorePopList.json"})
    public List<StorePopupListGetDto> getStorePopList(
        StorePopupListParamDto storePopupListParamDto) {
        String apiUri = "/common/popup/getStorePopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StoreListSelectDto>>>() {
        };

        return (List<StorePopupListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, storePopupListParamDto, apiUri, typeReference).getData();
    }

    @ResponseBody
    @GetMapping(value = {"/common/getStoreSelectPopList.json"})
    public List<StoreGetDto> getStoreSelectPopList(
        @RequestParam(value = "storeType") String storeType) {
        return (List<StoreGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM,
            "/common/popup/getStoreSelectPopList?storeType=" + storeType, new ParameterizedTypeReference<ResponseObject<List<StoreGetDto>>>() {}).getData();
    }

    /**
     * 점포선택 팝업창
     */
    @RequestMapping(value = {"/common/popup/storeSelectPop"}, method = RequestMethod.POST)
    public String setStorePop(Model model, HttpServletRequest request) {
        codeService.getCodeModel(model, "store_type");

        List<StoreGetDto> allStoreList = (List<StoreGetDto>) resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , "/common/popup/getStoreSelectPopList?storeType=" + request.getParameter("storeTypePop")
            , new ParameterizedTypeReference<ResponseObject<List<StoreGetDto>>>() {}).getData();

        model.addAttribute("storeTypePop", request.getParameter("storeTypePop"));
        model.addAttribute("storeTypeTitle", StoreType.valueFor(request.getParameter("storeTypePop")).getDescription());
        model.addAttribute("storeTypeRadioYn", request.getParameter("storeTypeRadioYn"));
        model.addAttribute("callBackScript", request.getParameter("callBackScript"));
        model.addAttribute("storeList", request.getParameter("storeList"));
        model.addAttribute("setYn", StringUtils.defaultIfEmpty(request.getParameter("setYn"), "Y"));
        model.addAttribute("allStoreList", new Gson().toJson(allStoreList));

        model.addAllAttributes(RealGridHelper.create("storeListGridBaseInfo", StoreGetDto.class));
        model.addAllAttributes(RealGridHelper.create("storeListSetGridBaseInfo", StoreGetDto.class));

        return "/common/pop/storeSelectPop";
    }

    /**
     * 이미지 미리보기 팝업
     */
    @RequestMapping(value = {"/common/popup/previewImg"}, method = RequestMethod.GET)
    public String previewImg() {

        return "/common/pop/previewImgPop";
    }

    /**
     * 카드 팝업
     */
    @PostMapping(value = "/common/popup/cardPop")
    public String cardPop(Model model, HttpServletRequest request) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_METHOD_BY_TYPE + "?paymentMethodType=" + request.getParameter("paymentMethodType") + "&siteType=" + request.getParameter("siteType");

        List<PaymentMethodDto> allPaymentList = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {}).getData();

        model.addAttribute("callBackScript", request.getParameter("callBackScript"));
        model.addAttribute("paymentMethodType", request.getParameter("paymentMethodType"));
        model.addAttribute("setYn", request.getParameter("setYn"));
        model.addAttribute("multiYn", request.getParameter("multiYn"));
        model.addAttribute("groupCdNeedYn", request.getParameter("groupCdNeedYn"));
        model.addAttribute("paymentList", request.getParameter("paymentList"));
        model.addAttribute("allPaymentList", new Gson().toJson(allPaymentList));

        // 카드업체 그리드
        //model.addAttribute("cardPopGridBaseInfo", new RealGridBaseInfo("cardPopGridBaseInfo", POP_CARD_GRID_COLUMN, POP_CARD_GRID_OPTION).toString());
        model.addAllAttributes(RealGridHelper.create("cardPopGridBaseInfo", PromotionPaymentDto.class));

        return "/common/pop/cardPop";
    }

    /**
     * 결제수단 조회(유형별) - 카드 리스트 선택 팝업에서 사용
     */
    @GetMapping(value = "/common/cardPopList.json")
    public List<PaymentMethodDto> getPaymentMethodByType(
        @RequestParam(value = "schPaymentMethodCd") String schPaymentMethodCd,
        @RequestParam(value = "schSiteType") String schSiteType) throws Exception {

        String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_METHOD_BY_TYPE + "?paymentMethodType=" + schPaymentMethodCd + "&SiteType=" + schSiteType;

        List<PaymentMethodDto> paymentList = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {}).getData();

        return paymentList;
    }

    /**
     * 바코드 팝업
     */
    @PostMapping(value = "/common/popup/barcodePop")
    public String barcodePop(Model model, HttpServletRequest request) throws Exception {
        String apiUri = "/promotion/coupon/cms/getCouponList";

        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model, "coupon_cms_unit");

        CmsCouponSearchParamDto paramDto = new CmsCouponSearchParamDto();
        paramDto.setCoyn(request.getParameter("coyn"));
        paramDto.setCheckflag("1");

        List<CmsCouponGetDto> couponBarcodeList = resourceClient.postForResponseObject(ResourceRouteName.MANAGE, paramDto, apiUri, new ParameterizedTypeReference<ResponseObject<List<CmsCouponGetDto>>>() {}).getData();

        model.addAttribute("callBackScript", request.getParameter("callBackScript"));
        model.addAttribute("setYn", request.getParameter("setYn"));
        model.addAttribute("storeType", request.getParameter("storeType"));
        model.addAttribute("getBarcode", request.getParameter("getBarcode"));
        model.addAttribute("couponBarcodeList", new Gson().toJson(couponBarcodeList));
        model.addAttribute("incUnitCdJson", new Gson().toJson(code.get("coupon_cms_unit")));

        // 바코드 팝업 그리드
        model.addAllAttributes(RealGridHelper.create("barcodePopGridBaseInfo", CmsCouponGetDto.class));

        return "/common/pop/barcodePop";
    }

    /**
     * 회원조회 팝업
     *
     * @param callBackScript callback script
     */
    @GetMapping(value = "/common/popup/memberSearchPop")
    public String memberSearchPop(final Model model,
                                  @RequestParam(value = "callBack", defaultValue = "") final String callBackScript,
                                  @RequestParam(defaultValue = "false") final boolean isIncludeUnmasking) {

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isIncludeUnmasking", isIncludeUnmasking);
        model.addAllAttributes(MEMBER_SEARCH_GRID_INFO);
        return "/common/pop/memberSearchPop";
    }

    /**
     * 회원조회
     *
     * @param param 회원조회 파라미터
     */
    @ResponseBody
    @PostMapping(value = {"/common/search/members.json"})
    public ResponseObject<List<MemberSearchDto>> getMembers(final HttpServletRequest request,
                                                            @RequestBody final MemberSearchParam param) {

        invalidGetMembersParam(param);

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("팝업 회원조회")
            .processor(param.toString())
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        return resourceClient
            .postForResponseObject(ResourceRouteName.USERMNG, param, "/admin/search/members",
                headers,
                new ParameterizedTypeReference<>() {
                });
    }

    private void invalidGetMembersParam(final MemberSearchParam param) {
        //입력값 체크
        if (org.apache.commons.lang3.StringUtils.isBlank(param.getMobile())
            && org.apache.commons.lang3.StringUtils.isBlank(param.getUserId())
            && org.apache.commons.lang3.StringUtils.isBlank(param.getUserNm())
            && param.getUserNo() == null) {
            throw new MemberSearchFailException(MemberSearchReturnCode.MEMBER_SEARCH_AT_LEAST_ONE_SEARCH_VALUE, "Invalid getMembers() parameter. param=" + param.toString());
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(param.getUserNm()) && param.getUserNm().length() < 2) {
            throw new MemberSearchFailException(MemberSearchReturnCode.MEMBER_SEARCH_PARAMETER_AT_LEAST_TWO_LETTERS, "Invalid getMembers() parameter. param=" + param.toString());
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(param.getUserId()) && param.getUserId().length() < 2) {
            throw new MemberSearchFailException(MemberSearchReturnCode.MEMBER_SEARCH_PARAMETER_AT_LEAST_TWO_LETTERS, "Invalid getMembers() parameter. param=" + param.toString());
        }

        //휴대폰 번호 형식 체크
        if (org.apache.commons.lang3.StringUtils.isNotBlank(param.getMobile()) && MobileUtil.isNotMobileType(param.getMobile())) {
            throw new MemberSearchFailException(MemberSearchReturnCode.WRONG_MOBILE_PATTERN, "Invalid getMembers() parameter. param=" + param.toString());
        }
    }


    /**
     * 쿠폰조회 공통팝업
     */
    @RequestMapping(value = "/common/popup/couponPop", method = RequestMethod.GET)
    public String couponPop(Model model,
                            @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                            @RequestParam(value = "isMulti", defaultValue = "") String isMulti,
                            @RequestParam(value = "storeType", defaultValue = "") String storeType,
                            @RequestParam(value = "couponNo", defaultValue = "") Long couponNo)
        throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"
            , "coupon_status"           // 쿠폰상태
            , "coupon_type"             // 쿠폰종류
        );

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("storeType", storeType);
        model.addAttribute("couponNo", couponNo);

        model.addAttribute("storeTypeCd", code.get("store_type"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("couponStatusJson", om.writeValueAsString(code.get("coupon_status")));

        // 쿠폰조회 그리드
        model.addAllAttributes(RealGridHelper.create("couponPopGridBaseInfo", CouponPopListGetDto.class));

        return "/common/pop/couponPop";
    }

    @ResponseBody
    @GetMapping(value = {"/common/getCouponPopList.json"})
    public List<CouponPopListGetDto> getCouponPopList(CouponPopListParamDto couponPopListParamDto) {
        String apiUri = "/common/popup/getCouponPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponPopListGetDto>>>() {};

        return (List<CouponPopListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, couponPopListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 브랜드 조회 공통 팝업
     */
    @RequestMapping(value = {"/common/popup/brandPop"}, method = RequestMethod.GET)
    public String brandPop(Model model, @RequestParam(value = "callback", defaultValue = "") String callBackScript) throws Exception {

        model.addAttribute("callBackScript", callBackScript);
        // 점포 리스트 그리드
        model.addAllAttributes(RealGridHelper.create("brandPopGridBaseInfo", BrandPopupSearchGetDto.class));

        return "/common/pop/brandPop";
    }


    /**
     * 브랜드 팝업 > 리스트 조회
     *
     * @param brandPopupListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/common/getBrandSearchPopList.json"})
    public List<BrandPopupSearchGetDto> getBrandSearchPopList(
        BrandPopupListParamDto brandPopupListParamDto) {
        String apiUri = "/common/popup/getBrandSearchPopList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BrandPopupSearchGetDto>>>() {
        };
        return (List<BrandPopupSearchGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, BrandPopupListParamDto.class, brandPopupListParamDto), typeReference).getData();
    }

    /**
     * 프로모션 검색 팝업
     *
     * @param model
     * @param promoType
     * @param callBackScript
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/common/popup/promoPop")
    public String promoPop(Model model,
                           @RequestParam(value = "promoType", defaultValue = "EXH") String promoType,
                           @RequestParam(value = "promoNo", defaultValue = "") String promoNo,
                           @RequestParam(value = "isMulti", defaultValue = "N") String isMulti,
                           @RequestParam(value = "siteType", defaultValue = "") String siteType,
                           @RequestParam(value = "storeType", defaultValue = "") String storeType,
                           @RequestParam(value = "callback", defaultValue = "") String callBackScript)
        throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"
            , "display_yn"
            , "promo_store_type"
            , "site_type"               // 사이트 구분
        );
        model.addAttribute("storeType", code.get("promo_store_type"));
        model.addAttribute("siteType", code.get("site_type"));

        model.addAttribute("promoType", promoType);
        model.addAttribute("promoNo", promoNo);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("promositeType", siteType);
        model.addAttribute("promoStoreType", storeType);
        model.addAttribute("callBackScript", callBackScript);

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("dispYnJson", om.writeValueAsString(code.get("display_yn")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("promo_store_type")));
        model.addAttribute("siteTypeJson", om.writeValueAsString(code.get("site_type")));

        model.addAllAttributes(RealGridHelper.create("promoPopGridBaseInfo", PromoPopListGetDto.class));

        return "/common/pop/promoPop";
    }

    /**
     * 프로모션 팝업 데이터 조회
     *
     * @param promoPopListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/common/getPromoPopList.json", method = RequestMethod.GET)
    public List<PromoPopListGetDto> getPromotionPopList(
        @ModelAttribute PromoPopListParamDto promoPopListParamDto) {
        String apiUri = "/common/popup/getPromoPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PromoPopListGetDto>>>() {};

        return (List<PromoPopListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, promoPopListParamDto, apiUri, typeReference).getData();
    }

    /**
     * GNB 조회 팝업
     * <p>
     * 인자로 callBack 스크립트를 받음
     *
     * @param callBackScript callback script
     * @return GNB 조회 팝업
     */
    @GetMapping(value = "/common/popup/gnbPop")
    public String gnbPop(Model model,
                         @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                         @RequestParam(value = "isMulti", defaultValue = "") String isMulti,
                         @RequestParam(value = "deviceType", defaultValue = "") String deviceType,
                         @RequestParam(value = "siteType", defaultValue = "") String siteType)
        throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "gnb_device"        // 거래유형
            , "gnb_site_type"              // 점포유형
        );

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("siteType", siteType);
        model.addAttribute("deviceType", deviceType);
        model.addAttribute("gnbDevice", code.get("gnb_device"));
        model.addAttribute("gnbSiteType", code.get("gnb_site_type"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("gnbDeviceTypeJson", om.writeValueAsString(code.get("gnb_device")));

        // 상품조회 그리드
        model.addAllAttributes(RealGridHelper.create("gnbPopGridBaseInfo", GnbPopupListSelectDto.class));

        return "/common/pop/gnbPop";
    }

    /**
     * GNB 정보 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getGnbPopList.json"})
    public List<GnbPopupListSelectDto> getGnbPopList(GnbPopupListParamDto gnbPopupListParamDto) {
        String apiUri = "/common/popup/getGnbPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<GnbPopupListSelectDto>>>() {};

        return (List<GnbPopupListSelectDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, gnbPopupListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 셀러샵 조회 팝업
     * <p>
     * 인자로 callBack 스크립트를 받음
     *
     * @param callBackScript callback script
     * @return GNB 조회 팝업
     */
    @GetMapping(value = "/common/popup/sellerShopPop")
    public String sellerShopPop(Model model,
                                @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                                @RequestParam(value = "isMulti", defaultValue = "") String isMulti)
        throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model,
            "recomshop_sch_type"        // 거래유형
        );

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);

        // 상품조회 그리드
        model.addAllAttributes(RealGridHelper.create("sellerShopPopGridBaseInfo", SellerShopPopupGetDto.class));

        return "/common/pop/sellerShopPop";
    }

    /**
     * 셀러샵  정보 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getSellerShopPopList.json"})
    public List<SellerShopPopupGetDto> getSellerShopPopList(
        SellerShopPopupListParamDto sellerShopPopupListParamDto) {
        String apiUri = "/common/popup/getSellerShopPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SellerShopPopupGetDto>>>() {
        };

        return (List<SellerShopPopupGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, sellerShopPopupListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 제휴체널 조회 팝업
     * <p>
     * 인자로 callBack 스크립트를 받음
     *
     * @param callBackScript callback script
     * @return GNB 조회 팝업
     */
    @GetMapping(value = {"/common/popup/affiliateChannelPop"})
    public String affiliateChannelPop(Model model,
                                      @RequestParam(value = "callback", defaultValue = "") String callBackScript,
                                      @RequestParam(value = "isMulti", defaultValue = "") String isMulti)
        throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode("affli_channel_type"    // 제휴채널타입
            , "affli_search_type"     // 제휴채널검색타입
            , "dsk_site_gubun"        // 사이트 구분
        );

        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);
        model.addAttribute("dskSiteGugun", code.get("dsk_site_gubun"));
        model.addAttribute("affliSearchType", code.get("affli_search_type"));

        // 상품조회 그리드
        model.addAllAttributes(RealGridHelper.create("affiliateChannelPopGridBaseInfo", AffiliateChannelPopListSelectDto.class));

        return "/common/pop/affiliateChannelPop";
    }

    /**
     * 제휴체널  정보 조회
     */
    @ResponseBody
    @GetMapping(value = {"/common/getAffiliateChannelPopList.json"})
    public List<AffiliateChannelPopListSelectDto> getAffiliateChannelPopPopList(AffiliateChannelPopListParamDto affiliateChannelPopListParamDto) {
        String apiUri = "/common/popup/getAffiliateChannelPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AffiliateChannelPopListSelectDto>>>() {};

        return (List<AffiliateChannelPopListSelectDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM, affiliateChannelPopListParamDto, apiUri, typeReference).getData();
    }

    @GetMapping(value = "/common/popup/orderSendListPop")
    public String orderSendListPop(Model model,
        @RequestParam(value = "buyerCallback", defaultValue = "") String buyerCallback,
        @RequestParam(value = "shipCallback", defaultValue = "") String shipCallback) {
        model.addAttribute("buyerCallback", buyerCallback);
        model.addAttribute("shipCallback", shipCallback);
        model.addAllAttributes(RealGridHelper.create("orderSendListGridBaseInfo", OrderSendListGrid.class));
        return "/common/pop/orderSendListPop";
    }

    @ResponseBody
    @PostMapping(value = {"/common/getOrderSendList.json"})
    public OrderSendListResponse getOrderSendList(@RequestBody final OrderSendListRequest request) {
        String url = "/escrow/message/getOrderSendList";
        OrderSendListResponse response = resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG, request, url,
            new ParameterizedTypeReference<ResponseObject<OrderSendListResponse>>() {
            }).getData();
        if (response.getList().size() > 0) {
            response.getList().forEach(OrderSendListInfo::setMasking);
        }
        return response;
    }

    @GetMapping(value = "/common/popup/excelSendListPop")
    public String excelSendListPop(Model model,
                                   @RequestParam(value = "callback", defaultValue = "") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        model.addAllAttributes(RealGridHelper.create("orderSendListGridBaseInfo", OrderSendListGrid.class));
        return "/common/pop/excelSendListPop";
    }

    /**
     * 사은행사 검색 팝업
     *
     * @param model
     * @param callBackScript
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/common/popup/ersPromoPop")
    public String ersPromoPop(Model model, @RequestParam(value = "callBackScript", defaultValue = "") String callBackScript, @RequestParam(value = "isMulti", defaultValue = "") String isMulti)
        throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"
            , "event_target_type"   // 구매기준유형
            , "event_give_type"     // 지급기준유형
            , "confirm_yn"          // 확정여부
        );
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("eventTargetType", code.get("event_target_type"));
        model.addAttribute("confirmYn", code.get("confirm_yn"));
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("isMulti", isMulti);

        model.addAllAttributes(RealGridHelper.create("ersPromoPopGridBaseInfo", ErsPromoPopListGetDto.class));

        return "/common/pop/ersPromoPop";
    }

    /**
     * 사은행사 팝업 데이터 조회
     *
     * @param ersPromoPopListParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/common/getErsPromoPopList.json", method = RequestMethod.GET)
    public List<ErsPromoPopListGetDto> getErsPromoPopList(
        @ModelAttribute ErsPromoPopListParamDto ersPromoPopListParamDto) {
        String apiUri = "/common/popup/getErsPromoPopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ErsPromoPopListGetDto>>>() {};

        return (List<ErsPromoPopListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, ersPromoPopListParamDto, apiUri, typeReference).getData();
    }

    /**
     * Express 변경 이력 조회 팝업  
     * @param model
     * @param storeId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/common/popup/expStoreHistPop"}, method = RequestMethod.GET)
    public String expStoreHistPop(Model model, @RequestParam(value = "storeId") Integer storeId)
        throws Exception {

        List<ExpStoreHistGetDto> expStoreHistGetDto = resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , "/item/expStore/getExpStoreHistList?storeId=" + storeId
            , new ParameterizedTypeReference<ResponseObject<List<ExpStoreHistGetDto>>>() {}).getData();

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("expStoreHist", om.writeValueAsString(expStoreHistGetDto));

        model.addAllAttributes(RealGridHelper.create("expStoreHistListPopGridBaseInfo", ExpStoreHistGetDto.class));

        return "/common/pop/expStoreHistPop";
    }

}