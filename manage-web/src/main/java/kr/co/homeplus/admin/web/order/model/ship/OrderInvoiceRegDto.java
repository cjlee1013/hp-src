package kr.co.homeplus.admin.web.order.model.ship;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "송장등록 요청 DTO")
public class OrderInvoiceRegDto {

    @ApiModelProperty(value = "주문관리번호", position = 1)
    private String bundleNo;

    @ApiModelProperty(value = "송장번호", position = 2)
    private String invoiceNo;

    @ApiModelProperty(value = "택배사코드", position = 3)
    private String dlvCd;

    @ApiModelProperty(value = "배송방법", position = 4)
    private String shipMethod;

    @ApiModelProperty(value = "배송예정일", position = 5)
    private String scheduleShipDt;

    @ApiModelProperty(value = "수정자ID", position = 6)
    private String chgId;

}
