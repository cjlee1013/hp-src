package kr.co.homeplus.admin.web.item.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.model.pop.StoreGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.item.model.store.StoreSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/store")
public class StoreController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;

    public StoreController(ResourceClient resourceClient, LoginCookieService cookieService, CodeService codeService) {

        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 상품관리 > 점포관리 > Main
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/storeMain", method = RequestMethod.GET)
    public String StoreMain(Model model) throws Exception{

        //코드정보
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "td_store_type"    //점포유형
                , "store_kind"           //점포구분
                , "store_attr"           //점포속성
                , "online_yn"            //온라인여부
                , "pickup_yn"            //픽업서비스여부
                , "region"               //소재지역
                , "use_yn" );            //사용여부

        model.addAttribute("storeType", code.get("td_store_type"));
        model.addAttribute("storeKind", code.get("store_kind"));
        model.addAttribute("storeAttr", code.get("store_attr"));
        model.addAttribute("onlineYn", code.get("online_yn"));
        model.addAttribute("pickupYn", code.get("pickup_yn"));
        model.addAttribute("region", code.get("region"));
        model.addAttribute("useYn", code.get("use_yn"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("td_store_type")));
        model.addAttribute("storeKindJson", om.writeValueAsString(code.get("store_kind")));
        model.addAttribute("storeAttrJson", om.writeValueAsString(code.get("store_attr")));

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("storeListGridBaseInfo", StoreListSelectDto.class));

        return "/item/storeMain";
    }

     /**
     * 상품관리 > 상품관리 > 점포관리 > 리스트
     *
     * @param storeListParamDto
     * @return StoreListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getStoreList.json"}, method = RequestMethod.POST)
    public List<StoreListSelectDto> getStoreList(@RequestBody StoreListParamDto storeListParamDto) {
        String apiUri = "/item/store/getStoreList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StoreListSelectDto>>>() {};
        return (List<StoreListSelectDto>) resourceClient.postForResponseObject(ResourceRouteName.ITEM,
            storeListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품관리 > 상품관리 > 점포관리 > 점포등록/수정
     * @param storeSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setStore.json"}, method = RequestMethod.POST)
    public ResponseResult setStore(@RequestBody StoreSetParamDto storeSetParamDto) throws Exception {

        storeSetParamDto.setUserId(cookieService.getUserInfo().getEmpId());
        ResponseObject<ResponseResult> responseObject = resourceClient.postForResponseObject(ResourceRouteName.ITEM, storeSetParamDto
                , "/item/store/setStore",  new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseResult = responseObject.getData();

        return responseResult;
    }

    /**
     * 점포 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = {"/getStore.json"})
    public StoreGetDto getPartnerId(@RequestParam(value = "storeId") Integer storeId ) throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , "/item/store/getStore?storeId=" + storeId
            , new ParameterizedTypeReference<ResponseObject<StoreGetDto>>() {}).getData();
    }

}

