package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeGroupListParamDto;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeGroupListSelectDto;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeGroupSetParamDto;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeListParamDto;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeListSelectDto;
import kr.co.homeplus.admin.web.item.model.mngAttribute.MngAttributeSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/mngAttribute")
public class MngAttributeController {
    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    public MngAttributeController(ResourceClient resourceClient,
                                LoginCookieService loginCookieService,
                                CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 상품속성관리 > 상품속성관리 > 상품속성관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/mngAttributeMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"        // 사용여부
                ,	"gattr_type"        // 상품속성관리 구분
                ,   "gattr_disp_type"   //상품속성관리 노출유형
                ,   "gattr_color_type"  //상ㅍ무속선관리 색상타입
                ,   "multi_yn"          // 복수선택여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("gattrType", code.get("gattr_type"));
        model.addAttribute("gattrDispType", code.get("gattr_disp_type"));
        model.addAttribute("gattrColorType", code.get("gattr_color_type"));
        model.addAttribute("multiYn", code.get("multi_yn"));
        model.addAttribute("homeImgUrl", this.homeImgUrl);

        model.addAllAttributes(RealGridHelper.create("attributeGroupListGridBaseInfo", MngAttributeGroupListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("attributeListGridBaseInfo", MngAttributeListSelectDto.class));
        return "/item/mngAttributeMain";
    }

    /**
     * 상품속성관리 > 상품속성관리 > 상품속성관리 리스트
     * @return AttributeGroupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMngAttributeGroupList.json", method = RequestMethod.GET)
    public List<MngAttributeGroupListSelectDto> getMngAttributeGroupList(MngAttributeGroupListParamDto listParamDto) throws Exception {

        String apiUri = "/item/mngAttribute/getMngAttributeGroupList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MngAttributeGroupListSelectDto>>>() {};
        return (List<MngAttributeGroupListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, MngAttributeGroupListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 상품속성관리 > 상품속성관리 > 상품속성관리 상품속성분류 등록/수정
     *
     * @param setParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setMngAttributeGroup.json"}, method = RequestMethod.POST)
    public ResponseResult setMngAttributeGroup(@ModelAttribute MngAttributeGroupSetParamDto setParamDto) throws Exception {
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/mngAttribute/setMngAttributeGroup";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference).getData();
    }

    /**
     * 상품속성관리 > 상품속성관리 > 상품속성관리 리스트
     * @return AttributeGroupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMngAttributeList.json", method = RequestMethod.GET)
    public List<MngAttributeListSelectDto> getMngAttributeList(MngAttributeListParamDto listParamDto) throws Exception {

        String apiUri = "/item/mngAttribute/getMngAttributeList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MngAttributeListSelectDto>>>() {};
        return (List<MngAttributeListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, MngAttributeListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 상품속성관리 > 상품속성관리 > 상품속성관리 속성정보 등록/수정
     *
     * @param setParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setMngAttribute.json"}, method = RequestMethod.POST)
    public ResponseResult setMngAttribute(@ModelAttribute MngAttributeSetParamDto setParamDto) throws Exception {
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = "/item/mngAttribute/setMngAttribute";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference).getData();
    }

}
