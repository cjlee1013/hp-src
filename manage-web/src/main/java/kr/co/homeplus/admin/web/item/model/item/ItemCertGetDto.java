package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

/**
 * 인증 정보
 */
@Data
public class ItemCertGetDto {

	//상품인증 시퀀스
	private Integer itemCertSeq;

	//인증여부 (Y:인증대상, N:인증대상아님, E:면제대상)
	private String isCert;

	//인증그룹 (공통코드 : item_cert_group) - (어린이제품:KD,생활용품:LF,전기용품:ER,방송통신기자재:RP)
	private String	certGroup;

	//면제유형 (item_exempt_type) PI: 병행수입 면제, PA: 구매대행 면제
	private String 	certExemptType;

	//인증유형 (공통코드 : item_cert_type)
	private String 	certType;

	//인증번호
	private String 	certNo;

}
