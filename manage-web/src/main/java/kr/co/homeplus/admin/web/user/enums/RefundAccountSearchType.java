package kr.co.homeplus.admin.web.user.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RefundAccountSearchType {
    USER_NM(1, "회원 명")
    , USER_NO(2, "회원 번호")
    , USER_ID(3, "회원 아이디")
    , MOBILE(4, "휴대폰 번호")
    ;

    @Getter
    private final int code;
    @Getter
    private final String desc;
}
