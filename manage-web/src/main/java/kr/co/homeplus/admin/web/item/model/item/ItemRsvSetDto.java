package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

/**
 * 상품 예약 판매정보 Set Entry
 */
@Data
//상품 예약 판매정보 Set Entry
public class ItemRsvSetDto {

	//예약판매수량
	private int rsvStockQty;

	//예약 구매제한 개수 여부
	private String rsvPurchaseLimitYn;

	//예약 구매제한 개수(1회)
	private Integer rsvPurchaseLimitQty;

}
