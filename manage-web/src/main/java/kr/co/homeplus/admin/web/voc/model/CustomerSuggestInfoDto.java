package kr.co.homeplus.admin.web.voc.model;

import java.time.LocalDateTime;
import javax.validation.constraints.Size;
import kr.co.homeplus.admin.web.voc.enums.OsType;
import kr.co.homeplus.admin.web.voc.enums.PlatformType;
import kr.co.homeplus.admin.web.voc.enums.SiteType;
import lombok.Data;

@Data
public class CustomerSuggestInfoDto {

    private long customerSuggestSeq;

    @Size(max = 500)
    private String customerSuggestMessage;

    private String userNo;

    private SiteType siteType;

    private PlatformType platformType;

    private OsType osType;

    private String deviceModel;

    private LocalDateTime regDt;
}
