package kr.co.homeplus.admin.web.user.model.user;

import kr.co.homeplus.admin.web.user.enums.UserStatus;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 회원정보 관리 조회결과 DTO
 **/
@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class UserInfoDto {

    @RealGridColumnInfo(headText = "회원번호", hidden = true)
    private Long userNo;

    @RealGridColumnInfo(headText = "회원번호")
    private String maskingUserNo;

    @RealGridColumnInfo(headText = "회원명")
    private String userNm;

    @RealGridColumnInfo(headText = "회원ID")
    private String userId;

    @RealGridColumnInfo(headText = "회원상태", columnType = RealGridColumnType.LOOKUP)
    private UserStatus userStatus;

    @RealGridColumnInfo(headText = "휴대폰")
    private String mobile;

    @RealGridColumnInfo(headText = "이메일")
    private String email;
}
