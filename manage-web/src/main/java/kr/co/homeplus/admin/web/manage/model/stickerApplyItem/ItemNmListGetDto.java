package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("상품관리 Get Entry")
public class ItemNmListGetDto {

    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 1)
    private String itemNm;

}

