package kr.co.homeplus.admin.web.item.model.banner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBannerMatchSetParamDto {
    //배너번호
    private Long bannerNo;

    //상품번호
    private String itemNo;

    //사용여부
    private String useYn;

    //등록자
    private String userId;
}
