package kr.co.homeplus.admin.web.user.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 컨슈머 관리 Error Code 정의
 */
@RequiredArgsConstructor
public enum ConsumerReturnCode {
    CONSUMER_ADD_NOT_MODIFICATION_PERIOD("컨슈머 수정 기간이 아닙니다."),
    CONSUMER_ADD_NOT_RELEASE_DETAIL("등록 시 해제사유는 입력할 수 없습니다."),
    CONSUMER_NOT_MODIFIED_ALREADY_RELEASE("기존에 해제된 건은 수정할 수 없습니다."),
    ;

    @Getter
    private final String msg;
}
