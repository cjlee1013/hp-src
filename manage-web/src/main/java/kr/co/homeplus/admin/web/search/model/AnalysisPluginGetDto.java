package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnalysisPluginGetDto {
    private List<AnalysisToken> tokens;
}
