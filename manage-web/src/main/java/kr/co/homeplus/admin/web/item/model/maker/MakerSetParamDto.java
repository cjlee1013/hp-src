package kr.co.homeplus.admin.web.item.model.maker;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MakerSetParamDto {

    private Long makerNo;
    private String makerNm;
    private String siteUrl;
    private String useYn;
    private String regId;
}
