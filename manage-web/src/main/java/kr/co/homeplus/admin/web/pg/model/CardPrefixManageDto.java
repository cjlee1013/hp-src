package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "카드 PREFIX 관리")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class CardPrefixManageDto {
    @ApiModelProperty(value = "카드프리픽스", position = 1)
    @RealGridColumnInfo(headText = "카드프리픽스", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardPrefixNo;

    @ApiModelProperty(value = "제휴카드종류", position = 2)
    @RealGridColumnInfo(headText = "제휴종류", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String affiliateCardKind;

    @ApiModelProperty(value = "카드식별명", position = 3)
    @RealGridColumnInfo(headText = "카드식별명", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardIdenNm;

    @ApiModelProperty(value = "결제수단코드", position = 4)
    @RealGridColumnInfo(headText = "결제수단코드", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodCd;

    @ApiModelProperty(value = "결제수단코드명", position = 5)
    @RealGridColumnInfo(headText = "결제수단코드명", width = 150, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodNm;

    @ApiModelProperty(value = "카드상세종류코드(0:일반,1:법인,2:체크,3:임직원,4:제휴)", position = 6)
    @RealGridColumnInfo(headText = "카드상세종류코드", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardDetailKind;

    @ApiModelProperty(value = "카드상세종류코드명", position = 7)
    @RealGridColumnInfo(headText = "카드상세종류코드명", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardDetailKindNm;

    @ApiModelProperty(value = "카드종류(0:자사카드,1:MyHomeplus카드,4:신한카드,6:타사카드,8:상품권(모바일,디지털),9:직불카드)", position = 8)
    @RealGridColumnInfo(headText = "카드종류", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardKind;

    @ApiModelProperty(value = "카드회사코드", position = 9)
    @RealGridColumnInfo(headText = "카드회사코드", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardCompanyCd;

    @ApiModelProperty(value = "카드회사명", position = 10)
    @RealGridColumnInfo(headText = "카드회사명", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardCompanyNm;

    @ApiModelProperty(value = "카드식별코드", position = 11)
    @RealGridColumnInfo(headText = "카드식별코드", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cardIdenCd;

    private List<CardPrefixManageDto> cardPrefixManageDtoList;
}
