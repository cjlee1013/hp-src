package kr.co.homeplus.admin.web.voc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemReviewExMileageListParamDto {

    private String schStartDt;
    private String schEndDt;
    private String searchCateCd;
    private String schType;
    private String schKeyword;

}
