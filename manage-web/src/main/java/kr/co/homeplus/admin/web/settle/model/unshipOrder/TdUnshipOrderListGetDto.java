package kr.co.homeplus.admin.web.settle.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipOrderListGetDto {
    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 120, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "상품구분")
    @RealGridColumnInfo(headText = "상품구분", width = 120, sortable = true)
    private String mallType;

    @ApiModelProperty(value = "회사코드")
    @RealGridColumnInfo(headText = "회사코드", width = 120, sortable = true)
    private String companyCd;

    @ApiModelProperty(value = "회사명")
    @RealGridColumnInfo(headText = "회사명", width = 120, sortable = true)
    private String companyNm;

    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", width = 120, sortable = true)
    private String originStoreId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 150, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "카테고리그룹")
    @RealGridColumnInfo(headText = "카테고리그룹", width = 150, sortable = true)
    private String division;

    @ApiModelProperty(value = "카테고리ID")
    @RealGridColumnInfo(headText = "카테고리ID", width = 150, sortable = true)
    private String deptNo;

    @ApiModelProperty(value = "카테고리명")
    @RealGridColumnInfo(headText = "카테고리명", width = 150, sortable = true)
    private String categoryNm;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 150, sortable = true)
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 350, sortable = true)
    private String itemNm;

    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(headText = "수량", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeQty;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 150, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호", width = 150, sortable = true)
    private String orderItemNo;

    @ApiModelProperty(value = "주문금액")
    @RealGridColumnInfo(headText = "주문금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeAmt;

    @ApiModelProperty(value = "임직원할인")
    @RealGridColumnInfo(headText = "임직원할인", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String empDiscountAmt;

    @ApiModelProperty(value = "상품할인(합계)")
    @RealGridColumnInfo(headText = "상품할인(합계)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String couponChargeSumAmt;

    @ApiModelProperty(value = "상품할인(업체)")
    @RealGridColumnInfo(headText = "상품할인(업체)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String couponSellerChargeAmt;

    @ApiModelProperty(value = "상품할인(카드사)")
    @RealGridColumnInfo(headText = "상품할인(카드사)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String couponCardChargeAmt;

    @ApiModelProperty(value = "상품할인(자사)")
    @RealGridColumnInfo(headText = "상품할인(자사)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String couponHomeChargeAmt;

    @ApiModelProperty(value = "카드할인(합계)")
    @RealGridColumnInfo(headText = "카드할인(합계)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String cardCouponSumAmt;

    @ApiModelProperty(value = "카드할인(카드사)")
    @RealGridColumnInfo(headText = "카드할인(카드사)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String cardCouponCardAmt;

    @ApiModelProperty(value = "카드할인(자사)")
    @RealGridColumnInfo(headText = "카드할인(자사)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String cardCouponHomeAmt;

    @ApiModelProperty(value = "매출합계")
    @RealGridColumnInfo(headText = "매출 합계", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String salesAmt;

    @ApiModelProperty(value = "공급가액")
    @RealGridColumnInfo(headText = "공급가액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String supplyAmt;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String vatAmt;

    @ApiModelProperty(value = "원가")
    @RealGridColumnInfo(headText = "원가", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String originPrice;

    @ApiModelProperty(value = "결제일")
    @RealGridColumnInfo(headText = "결제일", width = 150, sortable = true)
    private String paymentBasicDt;

    @ApiModelProperty(value = "배송상태")
    @RealGridColumnInfo(headText = "배송상태", width = 150, sortable = true)
    private String shipStatus;

    @ApiModelProperty(value = "배송요청일")
    @RealGridColumnInfo(headText = "배송요청일", width = 150, sortable = true)
    private String shipDt;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 150, sortable = true, hidden = true)
    private String bundleNo;

    @ApiModelProperty(value = "배송방법")
    @RealGridColumnInfo(headText = "배송방법", width = 150, sortable = true, hidden = true)
    private String shipMethodNm;

    @ApiModelProperty(value = "발송처리일시")
    @RealGridColumnInfo(headText = "발송처리일시", width = 150, sortable = true, hidden = true)
    private String shippingDt;

    @ApiModelProperty(value = "현배송상태")
    @RealGridColumnInfo(headText = "현배송상태", width = 150, sortable = true, hidden = true)
    private String currentShipStatus;

}
