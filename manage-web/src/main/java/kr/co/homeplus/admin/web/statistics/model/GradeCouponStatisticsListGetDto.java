package kr.co.homeplus.admin.web.statistics.model;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GradeCouponStatisticsListGetDto {

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "등급")
    @RealGridColumnInfo(headText = "등급")
    private String gradeStr;

    @ApiModelProperty(value = "주문고객수")
    @RealGridColumnInfo(headText = "주문고객수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String userCnt;

    @ApiModelProperty(value = "주문건수")
    @RealGridColumnInfo(headText = "주문건수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String purchaseNumber;

    @ApiModelProperty(value = "매출")
    @RealGridColumnInfo(headText = "매출", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String totAmt;

    @ApiModelProperty(value = "주문횟수")
    @RealGridColumnInfo(headText = "주문횟수", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NUMBER_C)
    private double purchaseCnt;

    @ApiModelProperty(value = "객단가")
    @RealGridColumnInfo(headText = "객단가", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NUMBER_C)
    private double customerPrice;

    @ApiModelProperty(value = "장바구니쿠폰사용금액")
    @RealGridColumnInfo(headText = "장바구니쿠폰사용금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;

}
