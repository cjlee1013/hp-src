package kr.co.homeplus.admin.web.search.controller;


import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchGridDto;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchItemGetDto;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchItemGetParam;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchListGetParam;
import kr.co.homeplus.admin.web.search.service.DataManagementTotalSearchService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/totalSearch")
@RequiredArgsConstructor
public class DataManagementTotalSearchController {

    private final DataManagementTotalSearchService totalSearchService;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    @GetMapping(value = { "/main" })
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("totalsearchGridBaseInfo", DataManagementTotalSearchGridDto.TotalSearchList.class));
        return "/search/dataManagementTotalSearchMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementTotalSearchListGetParam param) {
        return totalSearchService.getList(param);
    }

    @ResponseBody
    @GetMapping(value = {"/item.json"})
    public DataManagementTotalSearchItemGetDto getItem(DataManagementTotalSearchItemGetParam param) {
        return totalSearchService.getItem(param).getData();
    }

    @ResponseBody
    @GetMapping(value = {"/filter.json"})
    public ResponseObject<Map<String,Object>> getFilter(DataManagementTotalSearchListGetParam param){
        return totalSearchService.getFilter(param);
    }
}
