package kr.co.homeplus.admin.web.promotion.model.promo;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpecialZoneDetailGetDto {
    // 전문관 번호
    private Long specialNo;

    // 사용여부
    private String useYn;

    // 우선순위
    private Integer priority;

    // 전시시작일
    private String dispStartDt;

    // 전시종료일
    private String dispEndDt;

    // 배너명
    private String bannerNm;

    // 기획전 번호
    private Long promoNo;

    // 기획전 명
    private String promoNm;

    // 이미지 리스트
    private List<PromoImgDto> imgList;
}