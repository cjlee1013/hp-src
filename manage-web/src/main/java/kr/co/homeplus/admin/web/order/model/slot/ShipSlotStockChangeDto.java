package kr.co.homeplus.admin.web.order.model.slot;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "슬롯재고 가차감")
public class ShipSlotStockChangeDto {

    @ApiModelProperty(value = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value = "고객번호")
    private String userNo;

    @ApiModelProperty(value = "변경할 Ship Dt", reference = "YYYY-MM-DD")
    private String chgShipDt;

    @ApiModelProperty(value = "변경할 Shift ID")
    private String chgShiftId;

    @ApiModelProperty(value = "변경할 Slot ID")
    private String chgSlotId;

    @ApiModelProperty(value = "사번(어드민) 또는 고객번호(마이페이지)")
    private String historyRegId;

    @ApiModelProperty(value = "ADMIN, MYPAGE")
    private String historyRegChannel;
}
