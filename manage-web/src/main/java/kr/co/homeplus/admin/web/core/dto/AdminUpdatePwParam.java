package kr.co.homeplus.admin.web.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Setter
@Getter
@ApiModel("비밀번호 업데이트 파라미터")
public class AdminUpdatePwParam {

    @ApiModelProperty("사번")
    @NotBlank(message = "사번이 없습니다.")
    private String empId;

    @ApiModelProperty("변경할 비밀번호")
    @NotBlank(message = "비밀번호를 입력해주세요")
    @Length(min = 6, max = 15, message = "비밀번호 생성 규칙을 확인하여 다시 입력해 주세요")
    private String userPw;

    @ApiModelProperty("확인할 비밀번호")
    @NotBlank(message = "확인 비밀번호를 입력해주세요")
    @Length(min = 6, max = 15, message = "비밀번호 생성 규칙을 확인하여 다시 입력해 주세요")
    private String userConfirmPw;
}

