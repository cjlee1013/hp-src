package kr.co.homeplus.admin.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ChannelConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.PartnerAffiManagerGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerManagerGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerTempPwSetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateGetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateListGetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateParamDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateSetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.BusinessInfoGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/partner")
public class AffiliateController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 업체관리 > 제휴업체관리 > 제휴업체 등록/수정
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/affiliateMain")
    public String affiliateMain(Model model) {

        codeService.getCodeModel(model,
            "use_yn"       // 사용여부
            , "affiliate_type"      // 제휴유형
            , "operator_type"       // 사업자유형
            , "partner_grade"       // 회원등급
            , "partner_status"      // 회원상태
            , "approval_status"     // 승인상태
            , "return_delivery_yn"  // 반품택배사여부
            , "bank_code"           // 은행코드
            , "settle_cycle_type"   // 정산주기
        );

        model.addAllAttributes(RealGridHelper.create("affiliateListGridBaseInfo", AffiliateListGetDto.class));

        return "/partner/affiliateMain";
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴업체 리스트 조회
     *
     * @param affiliateParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getAffiliateList.json"})
    public List<AffiliateListGetDto> getAffiliateList(AffiliateParamDto affiliateParamDto) {

        String apiUri = "/partner/getAffiliateList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AffiliateListGetDto>>>() {};
        return (List<AffiliateListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, AffiliateParamDto.class, affiliateParamDto), typeReference).getData();
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴업체 상세조회
     *
     * @param partnerId
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getAffiliate.json"})
    public AffiliateGetDto getAffiliate(@RequestParam(name = "partnerId") String partnerId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUri = "/partner/getAffiliate" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, new ParameterizedTypeReference<ResponseObject<AffiliateGetDto>>() {}).getData();
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴업체 등록/수정
     *
     * @param affiliateSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody @PostMapping(value = "/setAffiliate.json")
    public ResponseResult setAffiliate(@ModelAttribute AffiliateSetDto affiliateSetDto) {
        affiliateSetDto.setUserId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, affiliateSetDto, "/partner/setAffiliate", new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 아이디 중복 확인
     *
     * @param partnerId
     * @param partnerType
     * @return
     * @throws Exception
     */
    @ResponseBody @GetMapping(value = {"/getAffiliateIdCheck.json"})
    public ResponseResult getPartnerId(@RequestParam(value = "partnerId") String partnerId, @RequestParam(value = "partnerType") String partnerType) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));
        getParameterList.add(SetParameter.create("partnerType", partnerType));

        String apiUriRequest = "/partner/getAffiliateIdCheck" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {})
            .getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 사업자 번호 확인
     *
     * @param partnerNm
     * @param partnerOwner
     * @param businessId
     * @return ResponseResult
     * @throws
     */
    @ResponseBody @GetMapping(value = {"/getBusinessIdCheck.json"})
    public ResponseResult getBusinessIdCheck(@RequestParam(value = "partnerNm") String partnerNm
        , @RequestParam(value = "partnerOwner") String partnerOwner
        , @RequestParam(value = "businessId") String businessId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerNm", partnerNm));
        getParameterList.add(SetParameter.create("partnerOwner", partnerOwner));
        getParameterList.add(SetParameter.create("businessId", businessId));

        String apiUriRequest = "/partner/getBusinessIdCheck" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {})
            .getData();
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴업체 리스트 조회
     *
     * @param partnerNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/checkPartnerNo.json"})
    public BusinessInfoGetDto checkPartnerNo(@RequestParam(value = "partnerNo") String partnerNo, @RequestParam(value = "partnerType") String partnerType) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerNo", partnerNo));
        getParameterList.add(SetParameter.create("partnerType", partnerType));
        getParameterList.add(SetParameter.create("channel", ChannelConstants.ADMIN));

        String apiUriRequest = "/partner/checkPartnerNo" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<BusinessInfoGetDto>>() {})
            .getData();
    }

    /**
     * 업체관리 > 제휴업체 관리 > 제휴업체 등록/수정 > PW 관리 팝업창
     * @return
     */
    @GetMapping("/popup/getAffiliatePwPop")
    public String getAffiliatePwPop(Model model
        , @RequestParam(value = "partnerId") String partnerId
        , @RequestParam(value = "businessNm") String businessNm) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUriRequest = "/partner/getPartnerAffiManager" + StringUtil.getParameter(getParameterList);

        PartnerAffiManagerGetDto responseData = resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<PartnerAffiManagerGetDto>>() {})
            .getData();

        model.addAttribute("partnerId", partnerId);
        model.addAttribute("loginStatusNm", responseData.getLoginStatusNm());
        model.addAttribute("businessNm", businessNm);
        model.addAttribute("affiliateEmail", responseData.getMngEmail());
        model.addAttribute("affiliateMobile", responseData.getMngMobile());

        return "/partner/pop/affiliatePwPop";
    }

    /**
     * 업체관리 > 제휴업체 관리 > 제휴업체 등록/수정 > 임시패스워스 발급
     * @param partnerTempPwSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setAffiliateTempPw.json"})
    public ResponseResult setAffiliateTempPw(PartnerTempPwSetDto partnerTempPwSetDto) {
        partnerTempPwSetDto.setUserId(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<ResponseResult> responseObject  = resourceClient.postForResponseObject(ResourceRouteName.ITEM
            , partnerTempPwSetDto
            , "/partner/setAffiliateTempPw"
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {});

        ResponseResult responseData = responseObject.getData();

        return responseData;
    }

}
