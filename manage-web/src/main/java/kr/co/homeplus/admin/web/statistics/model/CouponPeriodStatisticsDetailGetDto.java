package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponPeriodStatisticsDetailGetDto {
    @ApiModelProperty(value = "점포코드")
    @RealGridColumnInfo(headText = "점포코드", width = 100, sortable = true)
    private String storeId;

    @ApiModelProperty(value = "점포명")
    @RealGridColumnInfo(headText = "점포명", width = 100, sortable = true)
    private String storeNm;

    @ApiModelProperty(value = "주문건수")
    @RealGridColumnInfo(headText = "주문건수", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String purchaseCnt;

    @ApiModelProperty(value = "주문금액")
    @RealGridColumnInfo(headText = "주문금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeAmt;

    @ApiModelProperty(value = "사용금액")
    @RealGridColumnInfo(headText = "사용금액", width = 100, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;
}
