package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class EventItemExcelDto {
    @EqualsAndHashCode.Include
    private String itemNo;

    private String itemNm;

    private String dispYn;
}