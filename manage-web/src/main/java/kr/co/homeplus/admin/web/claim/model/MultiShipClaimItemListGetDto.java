package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문관리 > 주문정보 상세 > 취소/교환/환불 응답")
public class MultiShipClaimItemListGetDto {

    @ApiModelProperty(value = "주문번호", hidden = true, position = 1)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품주문번호", hidden = true, position = 2)
    private String orderItemNo;

    @ApiModelProperty(value = "상품번호", position = 3)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 4)
    private String itemNm1;

    @ApiModelProperty(value = "옵션번호", position = 6)
    private String orderOptNo;

    @ApiModelProperty(value = "주문수량", position = 7)
    private String orderQty;

    @ApiModelProperty(value = "주문금액", position = 8)
    private String orderPrice;

    @ApiModelProperty(value = "상품별최소구매수량", position = 9)
    private String purchaseMinQty;

    @ApiModelProperty(value = "주문타입", hidden = true, position = 10)
    private String orderType;

    @ApiModelProperty(value = "주문일시", position = 11)
    private String orderDt;

    @ApiModelProperty(value = "클레임건수", position = 12)
    private String claimQty;

    @ApiModelProperty(value = "행사번호", position = 13)
    private String promoNo;

    @ApiModelProperty(value = "행사명", position = 14)
    private String promoNm1;

    @ApiModelProperty(value = "수량별 배송비 차등 정보", position = 15)
    private String diffQty;
}
