package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 어휘 조회")
public class DictionaryListGetParamDto {
    @ApiModelProperty(notes = "어휘명", position = 1, required = true)
    private String wordName;
    @ApiModelProperty(notes = "품사", position = 2)
    private String pos;
    @ApiModelProperty(notes = "사전유형", position = 3)
    private ArrayList<String> dicType;
    @ApiModelProperty(notes = "동의어 포함", position = 4)
    private String synonymInclude = "N";
    @ApiModelProperty(notes = "하위어 포함", position = 5)
    private String hyponymInclude = "N";
    @ApiModelProperty(notes = "연관어 포함", position = 6)
    private String relatednymInclude = "N";
    @ApiModelProperty(notes = "관계", position = 7)
    private String relation;
    @ApiModelProperty(notes = "날짜검색유형", position = 8)
    private String schDateType;
    @ApiModelProperty(notes = "시작일", position = 8)
    private Long sdate;
    @ApiModelProperty(notes = "종료일", position = 9)
    private Long edate;
    @ApiModelProperty(notes = "범용여부", position = 10)
    private String isGeneral;
    @ApiModelProperty(notes = "사용여부", position = 11)
    private String useFlag;
    @ApiModelProperty(notes = "금칙어여부", position = 12)
    private String stopFlag;
    @ApiModelProperty(notes = "검색유형", position = 13)
    private String searchType;


}
