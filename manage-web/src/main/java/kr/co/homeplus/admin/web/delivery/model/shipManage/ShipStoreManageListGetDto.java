package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 점포배송관리 리스트 응답 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = false)
public class ShipStoreManageListGetDto {
    @ApiModelProperty(notes = "몰유형")
    @RealGridColumnInfo(headText = "몰유형", width = 150, sortable = true, hidden = true)
    private String mallType;

    @ApiModelProperty(notes = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeType;

    @ApiModelProperty(notes = "점포")
    @RealGridColumnInfo(headText = "점포", width = 120, sortable = true)
    private String storeNm;

    @ApiModelProperty(notes = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 100, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 100, sortable = true)
    private String bundleNo;

    @ApiModelProperty(notes = "옴니주문번호")
    @RealGridColumnInfo(headText = "옴니주문번호", width = 100, sortable = true)
    private String purchaseStoreInfoNo;

    @ApiModelProperty(notes = "주문상태")
    @RealGridColumnInfo(headText = "주문상태", width = 100, sortable = true)
    private String shipStatusNm;

    @ApiModelProperty(notes = "주문상태코드")
    @RealGridColumnInfo(headText = "주문상태코드", width = 100, sortable = true, hidden = true)
    private String shipStatus;

    @ApiModelProperty(notes = "주문일시")
    @RealGridColumnInfo(headText = "주문일시", width = 150, sortable = true)
    private String orderDt;

    @ApiModelProperty(notes = "배송요청일")
    @RealGridColumnInfo(headText = "배송요청일", width = 100, sortable = true)
    private String reserveShipDt;

    @ApiModelProperty(notes = "배송Shift")
    @RealGridColumnInfo(headText = "배송Shift", width = 100, sortable = true)
    private String shiftId;

    @ApiModelProperty(notes = "단축번호")
    @RealGridColumnInfo(headText = "단축번호", width = 100, sortable = true)
    private String sordNo;

    @ApiModelProperty(notes = "배송시간")
    @RealGridColumnInfo(headText = "배송시간", width = 100, sortable = true)
    private String slotShipTime;

    @ApiModelProperty(notes = "구매자")
    @RealGridColumnInfo(headText = "구매자", width = 80, sortable = true)
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    @RealGridColumnInfo(headText = "구매자 연락처", width = 100, sortable = true)
    private String buyerMobileNo;

    @ApiModelProperty(notes = "마켓연동")
    @RealGridColumnInfo(headText = "마켓연동", width = 80, sortable = true)
    private String marketType;

    @ApiModelProperty(notes = "마켓주문번호")
    @RealGridColumnInfo(headText = "마켓주문번호", width = 120, sortable = true)
    private String marketOrderNo;

    @ApiModelProperty(notes = "법인")
    @RealGridColumnInfo(headText = "법인", width = 60, sortable = true)
    private String corpOrder;

    @ApiModelProperty(notes = "수령인")
    @RealGridColumnInfo(headText = "수령인", width = 80, sortable = true)
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    @RealGridColumnInfo(headText = "수령인 연락처", width = 100, sortable = true)
    private String shipMobileNo;

    @ApiModelProperty(notes = "배송방법")
    @RealGridColumnInfo(headText = "배송방법", width = 90, sortable = true)
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    @RealGridColumnInfo(headText = "배송방법코드", width = 150, sortable = true, hidden = true)
    private String shipMethod;

    @ApiModelProperty(notes = "우편번호")
    @RealGridColumnInfo(headText = "우편번호", width = 80, sortable = true)
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    @RealGridColumnInfo(headText = "주소", width = 350, columnType = RealGridColumnType.NONE, sortable = true)
    private String addr;

    @ApiModelProperty(notes = "배송메세지")
    @RealGridColumnInfo(headText = "배송메세지", width = 200, columnType = RealGridColumnType.NONE, sortable = true)
    private String dlvShipMsg;

    @ApiModelProperty(notes = "결제일시")
    @RealGridColumnInfo(headText = "결제일시", width = 150, sortable = true)
    private String paymentFshDt;

    @ApiModelProperty(notes = "번들수량")
    @RealGridColumnInfo(headText = "번들수량", width = 150, sortable = true, hidden = true)
    private String bundleQty;
}