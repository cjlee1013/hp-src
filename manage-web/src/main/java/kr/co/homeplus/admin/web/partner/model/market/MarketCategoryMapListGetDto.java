package kr.co.homeplus.admin.web.partner.model.market;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarketCategoryMapListGetDto {

    @ApiModelProperty(value = "대분류")
    @RealGridColumnInfo(headText = "대분류", width = 120, sortable = true)
    private String lcateNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류", width = 120, sortable = true)
    private String mcateNm;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", width = 120, sortable = true)
    private String scateNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", width = 120, sortable = true)
    private String dcateNm;

    @ApiModelProperty(value = "카테고리ID")
    @RealGridColumnInfo(headText = "카테고리ID", width = 100, sortable = true)
    private String dcateCd;

    /**
     * TODO: 이베이 주석 처리 21.11
     * 이베이 Refit 포함 시 다시 활성화
     */

  /*  @ApiModelProperty(value = "G마켓 연동코드")
    @RealGridColumnInfo(headText = "G마켓 연동코드", width = 100)
    private String gmarketCateCd;

    @ApiModelProperty(value = "G마켓")
    @RealGridColumnInfo(headText = "G마켓", width = 120)
    private String gmarketCateNm;

    @ApiModelProperty(value = "G마켓 연동여부")
    @RealGridColumnInfo(headText = "G마켓 연동여부", hidden = true)
    private String gmarketApplyYn;

    @ApiModelProperty(value = "G마켓 연동여부")
    @RealGridColumnInfo(headText = "G마켓 연동여부", width = 50)
    private String gmarketApplyYnNm;

    @ApiModelProperty(value = "G마켓 사용여부")
    @RealGridColumnInfo(headText = "G마켓 사용여부", width = 50)
    private String gmarketUseYn;

    @ApiModelProperty(value = "옥션 연동코드")
    @RealGridColumnInfo(headText = "옥션 연동코드", width = 100)
    private String auctionCateCd;

    @ApiModelProperty(value = "옥션")
    @RealGridColumnInfo(headText = "옥션", width = 120)
    private String auctionCateNm;

    @ApiModelProperty(value = "옥션 연동여부")
    @RealGridColumnInfo(headText = "옥션 연동여부",  hidden = true)
    private String auctionApplyYn;

    @ApiModelProperty(value = "옥션 연동여부")
    @RealGridColumnInfo(headText = "옥션 연동여부",  width = 50)
    private String auctionApplyYnNm;

    @ApiModelProperty(value = "옥션 사용여부")
    @RealGridColumnInfo(headText = "옥션 사용여부", width = 50)
    private String auctionUseYn;
   */

    @ApiModelProperty(value = "11번가 연동코드")
    @RealGridColumnInfo(headText = "11번가 연동코드", width = 100, sortable = true)
    private String elevenCateCd;

    @ApiModelProperty(value = "11번가")
    @RealGridColumnInfo(headText = "11번가", width = 120)
    private String elevenCateNm;

    @ApiModelProperty(value = "11번가 연동여부")
    @RealGridColumnInfo(headText = "11번가 연동여부", hidden = true)
    private String elevenApplyYn;

    @ApiModelProperty(value = "11번가 연동여부")
    @RealGridColumnInfo(headText = "11번가 연동여부", width = 50, sortable = true)
    private String elevenApplyYnNm;

    @ApiModelProperty(value = "11번가 사용여부")
    @RealGridColumnInfo(headText = "11번가 사용여부", width = 50, sortable = true)
    private String elevenUseYn;

    @ApiModelProperty(value = "N마트 연동코드")
    @RealGridColumnInfo(headText = "N마트 연동코드", width = 100, sortable = true)
    private String naverCateCd;

    @ApiModelProperty(value = "N마트")
    @RealGridColumnInfo(headText = "N마트", width = 120)
    private String naverCateNm;

    @ApiModelProperty(value = "N마트 연동여부")
    @RealGridColumnInfo(headText = "N마트 연동여부", hidden = true)
    private String naverApplyYn;

    @ApiModelProperty(value = "N마트 연동여부")
    @RealGridColumnInfo(headText = "N마트 연동여부", width = 50, sortable = true)
    private String naverApplyYnNm;

    @ApiModelProperty(value = "N마트 사용여부")
    @RealGridColumnInfo(headText = "N마트 사용여부", width = 50, sortable = true)
    private String naverUseYn;

    @ApiModelProperty(value = "등록여부")
    @RealGridColumnInfo(headText = "등록여부", width = 50, sortable = true)
    private String regYnNm;

}
