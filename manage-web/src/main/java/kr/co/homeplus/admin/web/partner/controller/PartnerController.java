package kr.co.homeplus.admin.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.constants.RoleConstants;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.PartnerApprovalStatusSetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerBankAccntNoCertifyParamGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerAddressDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerDeliveryGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerListGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerParamDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSellerShipListGetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerSetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerStatusSetDto;
import kr.co.homeplus.admin.web.partner.model.PartnerTypesSetDto;
import kr.co.homeplus.admin.web.partner.model.hist.PartnerSellerHistGetDto;
import kr.co.homeplus.admin.web.partner.service.PartnerSellerHistService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/partner")
public class PartnerController {

    private final PartnerSellerHistService partnerSellerHistService;

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    final private CodeService codeService;

    private final PrivacyLogService privacyLogService;

    final private AuthorityService authorityService;

    public PartnerController(
            PartnerSellerHistService partnerSellerHistService,
            ResourceClient resourceClient,
            LoginCookieService cookieService,
            CodeService codeService,
            PrivacyLogService privacyLogService,
            AuthorityService authorityService) {
        this.partnerSellerHistService = partnerSellerHistService;
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
        this.privacyLogService = privacyLogService;
        this.authorityService = authorityService;
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 관리
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/sellerMain")
    public String sellerMain(Model model) {

        codeService.getCodeModel(model,
                "use_yn"       // 사용여부
                , "operator_type"       // 사업자유형
                , "partner_grade"       // 회원등급
                , "partner_status"      // 회원상태
                , "of_vendor_status"    // OF 승인상태
                , "return_delivery_yn"  // 반품택배사여부
                , "seller_sale_type"    // 판매유형
                , "bank_code"           // 은행코드
                , "settle_cycle_type"   // 정산주기
                , "settle_payment_yn"   // 정산지급여부
                , "partner_file_type"   // 첨부서류
        );

        model.addAttribute("SELLER_SETTLE_PAY", authorityService.hasRole(RoleConstants.SELLER_SETTLE_PAY) ? "" : "disabled");
        model.addAllAttributes(RealGridHelper.createForGroup("sellerListGridBaseInfo", PartnerSellerListGetDto.class));

        return "/partner/sellerMain";
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체 리스트 조회
     *
     * @param partnerSellerParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getSellerList.json"})
    public List<PartnerSellerListGetDto> getSellerList(PartnerSellerParamDto partnerSellerParamDto) {

        String apiUri = "/partner/getSellerList";

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                StringUtil.getRequestString(apiUri, PartnerSellerParamDto.class, partnerSellerParamDto),
                new ParameterizedTypeReference<ResponseObject<List<PartnerSellerListGetDto>>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 상세조회
     *
     * @param partnerId
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getSeller.json"})
    public PartnerSellerGetDto getSeller(final HttpServletRequest request, @RequestParam(name = "partnerId") String partnerId) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
                .txTime(DateTimeUtil.getNowMillYmdHis())
                .accessIp(ServletUtils.clientIP(request))
                .userId(cookieService.getUserInfo().getUserId())
                .txCodeUrl(request.getRequestURI())
                .txMethod(PersonalLogMethod.SELECT.name())
                .txCodeName("판매업체 상세조회")
                .processor(partnerId)
                .processorTaskSql("")
                .build();

        privacyLogService.send(logInfo);

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getSeller" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<PartnerSellerGetDto>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 판매자 등록/수정
     *
     * @param partnerSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setPartner.json")
    public ResponseResult setPartnerMng(@RequestBody PartnerSetDto partnerSetDto) {
        partnerSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        if(authorityService.hasRole(RoleConstants.SELLER_SETTLE_PAY) == false) {
            partnerSetDto.getSettleInfo().setSettlePaymentYn(null);
        }

        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM,
                partnerSetDto,
                "/partner/setPartner",
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 아이디 중복 확인
     *
     * @param partnerId
     * @param partnerType
     * @return
     * @throws Exception
     */
    @ResponseBody @GetMapping(value = {"/getSellerIdCheck.json"})
    public ResponseResult getPartnerId(@RequestParam(value = "partnerId") String partnerId, @RequestParam(value = "partnerType") String partnerType) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));
        setParameterList.add(SetParameter.create("partnerType", partnerType));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getSellerIdCheck" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 회원상태 변경
     *
     * @param partnerStatusSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/setStatus.json")
    public ResponseResult partnerStatusMng(@ModelAttribute PartnerStatusSetDto partnerStatusSetDto) {
        partnerStatusSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM,
                partnerStatusSetDto,
                "/partner/setStatus",
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 승인상태 변경
     * @param partnerApprovalStatusSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/setApprovalStatus.json")
    public ResponseResult setApprovalStatus(@ModelAttribute PartnerApprovalStatusSetDto partnerApprovalStatusSetDto) {
        partnerApprovalStatusSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM,
                partnerApprovalStatusSetDto,
                "/partner/setApprovalStatus",
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 업체관리 > 파트너관리 > 판매업체관리 > 종목 조회
     *
     * @param codeNm
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/getBizCateCdList.json")
    public List<MngCodeGetDto> getBizCateCdList(@RequestParam(value = "codeNm") String codeNm) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("typeCode", "biz_cate_cd"));
        setParameterList.add(SetParameter.create("codeNm", codeNm));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/common/getCodeListByNm" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<List<MngCodeGetDto>>>() {
                }).getData();
    }

    /**
     * 배송정보 리스트
     *
     * @param partnerId
     * @param useYn
     * @return PartnerSellerShipListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSellerShipList.json", method = RequestMethod.GET)
    public List<PartnerSellerShipListGetDto> getSellerShipList(@RequestParam(value = "partnerId") String partnerId, @RequestParam(value = "useYn", defaultValue = "", required = false) String useYn) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        if(useYn != "") {
            setParameterList.add(SetParameter.create("useYn", useYn));
        }

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getSellerShipList" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<List<PartnerSellerShipListGetDto>>>() {
                }).getData();
    }

    /**
     * 판매자 주소 조회
     *
     * @param partnerId
     * @return PartnerSellerAddressDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSellerAddress.json", method = RequestMethod.GET)
    public PartnerSellerAddressDto getSellerAddress(@RequestParam(value = "partnerId") String partnerId) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getSellerAddress" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<PartnerSellerAddressDto>>() {
                }).getData();
    }

    /**
     * 파트너관리 > 통신판매업신고번호 조회
     * @param partnerNo
     * @return
     */
    @ResponseBody
    @GetMapping("/getCommuniNotiNo.json")
    public ResponseResult getCommuniNotiNo(@RequestParam String partnerNo) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerNo", partnerNo));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getCommuniNotiNo" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                }).getData();
    }

    /**
     * 파트너관리 > 판매자관리 > 판매자 상세 조회 (반품택배사 조회)
     * @param partnerId
     * @return
     */
    @ResponseBody
    @GetMapping("/getPartnerDeliveryList.json")
    public List<PartnerSellerDeliveryGetDto> getPartnerDeliveryList(@RequestParam(value = "partnerId") String partnerId) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getPartnerDeliveryList" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<List<PartnerSellerDeliveryGetDto>>>() {
                }).getData();
    }

    /**
     * 계좌인증 조회
     * @param partnerBankAccntNoCertifyParamGetDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getBankAccntNoCertify.json"}, method = RequestMethod.POST)
    public PartnerBankAccntNoCertifyParamGetDto getBankAccntNoCertify(PartnerBankAccntNoCertifyParamGetDto partnerBankAccntNoCertifyParamGetDto) {
        return resourceClient.postForResponseObject(
                ResourceRouteName.ITEM
                , partnerBankAccntNoCertifyParamGetDto
                , "/partner/getBankAccount"
                , new ParameterizedTypeReference<ResponseObject<PartnerBankAccntNoCertifyParamGetDto>>() {
                }).getData();
    }

    /**
     * 판매업체 상품유형 권한 조회
     * @param partnerId
     * @return
     */
    @ResponseBody
    @GetMapping("/getPartnerTypesList.json")
    public List<PartnerTypesSetDto> getPartnerTypesList(@RequestParam String partnerId) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));

        return resourceClient.getForResponseObject(
                ResourceRouteName.ITEM,
                "/partner/getPartnerTypesList" + StringUtil.getParameter(setParameterList),
                new ParameterizedTypeReference<ResponseObject<List<PartnerTypesSetDto>>>() {
                }).getData();
    }

    /**
     * 판매업체 히스토리 그룹정보 (페이지 번호를 증가시켜 호출하면 됨)
     * @param partnerId
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerSellerHistGroup.json", method = RequestMethod.GET)
    public List<PartnerSellerHistGetDto> getPartnerSellerHistGroup(@RequestParam String partnerId, @RequestParam int pageNo) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("partnerId", partnerId));
        setParameterList.add(SetParameter.create("pageNo", pageNo));

        return partnerSellerHistService.getPartnerSellerHistGroup(
                resourceClient.getForResponseObject(
                        ResourceRouteName.ITEM,
                        "/partner/getPartnerSellerHistGroup" + StringUtil.getParameter(setParameterList),
                        new ParameterizedTypeReference<ResponseObject<List<PartnerSellerHistGetDto>>>() {
                        }).getData()
        );
    }
}
