package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.List;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionPaymentDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.PromotionStoreDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DiscountDetailGetDto {
    @RealGridColumnInfo(headText = "할인번호", sortable = true, width = 100)
    private long discountNo;

    @RealGridColumnInfo(headText = "할인구분", sortable = true, hidden = true)
    private String discountKind;

    @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 100)
    private String storeType;

    @RealGridColumnInfo(headText = "관리할인명", sortable = true, columnType = RealGridColumnType.NAME, fieldType = RealGridFieldType.TEXT, width = 300)
    private String discountNm;

    @RealGridColumnInfo(headText = "사용여부", sortable = true, width = 100)
    private char useYn;

    @RealGridColumnInfo(headText = "적용기간", sortable = true, width = 300)
    private String issuePeriod;

    @RealGridColumnInfo(headText = "등록자", sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", sortable = true, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", sortable = true, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;

    private String displayDiscountNm;
    private String discountKindNm;
    private String issueStartDt;
    private String issueEndDt;
    private char chgYn;
    private String useNm;

    private List<PromotionStoreDto> storeList;
    private List<PromotionPaymentDto> paymentList;
    private List<DiscountApplySetDto> applyScopeList;
}
