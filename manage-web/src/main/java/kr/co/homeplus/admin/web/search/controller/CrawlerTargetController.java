package kr.co.homeplus.admin.web.search.controller;

import java.util.Map;
import kr.co.homeplus.admin.web.search.service.CrawlerTargetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/searchTargetItem")
@RequiredArgsConstructor
public class CrawlerTargetController {

    private final CrawlerTargetService crawlerTargetService;

    @GetMapping(value = { "/main" })
    public String getMain(){
        return "/search/searchTargetItem";
    }

    @ResponseBody
    @GetMapping(value= {"/getCrawlerTarget.json"})
    public Map<String, Object> getCrawlerTarget(
          @RequestParam String itemNo
        , @RequestParam int storeId
    ) {
        return crawlerTargetService.getCrawlerTarget(itemNo, storeId);
    }
}
