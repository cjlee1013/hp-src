package kr.co.homeplus.admin.web.item.model.expCategory;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ExpCategoryGetDto {

    @RealGridColumnInfo(headText = "카테고리 ID", width = 120)
    private String cateCd;

    @RealGridColumnInfo(headText = "카테고리명", columnType = RealGridColumnType.NAME, width = 250, order = 1)
    private String cateNm;

    @RealGridColumnInfo(headText = "우선순위", width = 70, order = 2)
    private String priority;

    @RealGridColumnInfo(headText = "사용여부", order = 3)
    private String useYnNm;

    @RealGridColumnInfo(headText = "노출범위", order = 4, hidden = true)
    private String dispYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 5)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 130, order = 6)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 7)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 130, order = 8)
    private String chgDt;

    @RealGridColumnInfo(hidden = true, order = 9)
    private String depth;

    @RealGridColumnInfo(hidden = true, order = 10)
    private String parentCateCd;

    @RealGridColumnInfo(hidden = true, order = 11)
    private String lCateCd;

    @RealGridColumnInfo(hidden = true, order = 12)
    private String mCateCd;

    @RealGridColumnInfo(hidden = true, order = 13)
    private String useYn;

    @RealGridColumnInfo(hidden = true, order = 14)
    private String dispYn;

    @RealGridColumnInfo(hidden = true, order = 15)
    private String dept;

    @RealGridColumnInfo(hidden = true, order = 16)
    private String onIconPcImgUrl;

    @RealGridColumnInfo(hidden = true, order = 17)
    private String offIconPcImgUrl;

    @RealGridColumnInfo(hidden = true, order = 18)
    private String onIconMobileImgUrl;

    @RealGridColumnInfo(hidden = true, order = 19)
    private String offIconMobileImgUrl;


    private String lCateNm;
    private String mCateNm;
    private String regId;
    private String chgId;

}
