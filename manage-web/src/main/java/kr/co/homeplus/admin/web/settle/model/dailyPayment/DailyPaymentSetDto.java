package kr.co.homeplus.admin.web.settle.model.dailyPayment;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailyPaymentSetDto {
    @ApiModelProperty(value = "검색날짜타입") //D:일별, M:월별
    private String dateType;

    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(value = "업체코드")
    private String vendorCd;

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm;

    @ApiModelProperty(value = "점포코드")
    private String originStoreId;

    @ApiModelProperty(value = "사이트타입")
    private String siteType;
}
