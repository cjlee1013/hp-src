package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class SellerShopPopupGetDto {
    @RealGridColumnInfo(headText = "판매업체 ID", sortable = true)
    private String partnerId;

    @RealGridColumnInfo(headText = "업체명", sortable = true)
    private String partnerNm;

    @RealGridColumnInfo(headText = "셀러샵 이름(닉네임)", sortable = true)
    private String shopNm;

    @RealGridColumnInfo(headText = "셀러샵 소개", hidden = true)
    private String shopInfo;

}
