package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "대체공제쿠폰통계 조회 파라미터 DTO")
public class OrderSubstitutionStatisticsSetDto {
    @ApiModelProperty(value = "조회시작일", required = true)
    private String schStartDt;
    @ApiModelProperty(value = "조회종료일", required = true)
    private String schEndDt;
    @ApiModelProperty(value = "조회상점타입")
    private String schStoreType;
    @ApiModelProperty(value = "상점아이디")
    private String schStoreId;
}
