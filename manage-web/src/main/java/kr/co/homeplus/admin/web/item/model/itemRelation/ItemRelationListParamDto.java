package kr.co.homeplus.admin.web.item.model.itemRelation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRelationListParamDto {

    //검색 날짜유형
    private String schDate;

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

    //검색 노출여부
    private String schDispYn ;

    //대분류 카테고리
    private String schLcateCd;

    //증분류 카테고리
    private String schMcateCd;

    //소분류 카테고리
    private String schScateCd;

    //판매업체ID
    private String schPartnerId;

    //설정여부
    private String schUseYn;
}
