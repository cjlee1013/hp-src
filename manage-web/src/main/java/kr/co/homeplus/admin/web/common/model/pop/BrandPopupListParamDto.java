package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandPopupListParamDto {
	private String schRegStartDate;
	private String schRegEndDate;
	private String searchType;
	private String searchValue;
}
