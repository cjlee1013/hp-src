package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel("회원관리 MHC포인트 조회 파라미터")
public class UserInfoMhcParam {
    @NotBlank(message = "MHC 대체카드번호가 없습니다.")
    @ApiModelProperty("MHC대체카드번호_Hmac값")
    private String mhcCardNoHmac;
}
