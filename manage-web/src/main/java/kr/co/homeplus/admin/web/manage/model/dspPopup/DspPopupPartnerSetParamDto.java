package kr.co.homeplus.admin.web.manage.model.dspPopup;

import lombok.Data;

@Data
public class DspPopupPartnerSetParamDto {
    private String partnerId;
    private String partnerNm;
    private String businessNm;
    private String partnerStatusNm;
}
