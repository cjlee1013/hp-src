package kr.co.homeplus.admin.web.settle.model.affiliateSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 목록")
public class AffiliateSalesGetDto {
  @ApiModelProperty(notes = "집계기간")
  @RealGridColumnInfo(headText = "집계기간", sortable = true, width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "제휴채널ID")
  @RealGridColumnInfo(headText = "제휴채널ID", sortable = true, width = 100)
  private String affiliateCd;

  @ApiModelProperty(notes = "제휴채널명")
  @RealGridColumnInfo(headText = "제휴채널명", sortable = true, width = 100)
  private String channelNm;

  @ApiModelProperty(notes = "제휴업체명")
  @RealGridColumnInfo(headText = "제휴업체명", sortable = true, width = 100)
  private String affiliateNm;

  @ApiModelProperty(notes = "사이트구분코드")
  @RealGridColumnInfo(headText = "사이트구분코드", hidden = true)
  private String siteType;

  @ApiModelProperty(notes = "사이트구분")
  @RealGridColumnInfo(headText = "사이트구분", sortable = true, width = 100)
  private String siteName;

  @ApiModelProperty(notes = "주문건 수")
  @RealGridColumnInfo(headText = "주문건 수", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderCnt;

  @ApiModelProperty(notes = "취소건 수")
  @RealGridColumnInfo(headText = "취소건 수", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cancelCnt;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "배송비")
  @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipPrice;

  @ApiModelProperty(notes = "부가세")
  @RealGridColumnInfo(headText = "부가세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String vatPrice;

  @ApiModelProperty(notes = "실적대상금액")
  @RealGridColumnInfo(headText = "실적대상금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String supplyAmt;

  @ApiModelProperty(notes = "수수료율")
  @RealGridColumnInfo(headText = "수수료율", sortable = true, width = 100)
  private String commissionRate;

  @ApiModelProperty(notes = "수수료액")
  @RealGridColumnInfo(headText = "수수료액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String commissionAmt;
}
