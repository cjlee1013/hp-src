package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.view.excel.ExcelBindInfo;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CmsCouponProductGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponApplyGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponExceptItemDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponFileGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponFileSetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponIssueHistGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponIssueHistListGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponIssueHistSearchSetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponIssueMemberSetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponListGetDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponListParamDto;
import kr.co.homeplus.admin.web.promotion.model.coupon.CrmCouponListGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoUploadSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("/coupon")
public class CouponController {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    public CouponController(ResourceClient resourceClient, LoginCookieService loginCookieService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 프로모션관리 > 쿠폰관리 > 쿠폰관리
     *
     * @param model
     * @return String
     */
    @GetMapping(value = "/couponMain")
    public String couponMain(Model model) throws Exception {
        List<PaymentMethodDto> couponMethodList = new ArrayList<>();
        try {
            String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_METHOD_BY_TYPE +"?paymentMethodType=CARD,EASYPAY&siteType=HOME";

            couponMethodList = resourceClient.getForResponseObject(
                ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {}
            ).getData();
        } catch (Exception e) {
            log.error(e.getStackTrace().toString());
        }

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"           // 사용여부
            , "coupon_status"           // 쿠폰상태
            , "store_type"              // 점포유형
            , "coupon_type"             // 쿠폰종류
            , "coupon_purpose"          // 쿠폰행사목적
            , "coupon_apply_type"       // 쿠폰적용방식
            , "coupon_dupl_yn"          // 쿠폰중복여부
            , "coupon_discount_type"    // 쿠폰할인방법
            , "coupon_valid_type"       // 쿠폰유효기간타입
            , "issue_limit_type"        // 쿠폰 총 발급수량 타입
            , "coupon_apply_scope"      // 쿠폰적용범위
            , "share_yn"                // 분담여부
            , "grp_discount_Type"       // 그룹상품할인 할인조건
            , "coupon_cms_unit"         // CMS귀속부서
            , "coupon_cart_barcode"     // 장바구니쿠폰 CMS바코드 번호
            , "avail_yn"                // 가능여부
            , "coupon_payment_type"     // 쿠폰결제수단
            , "coupon_random_type"      // 난수쿠폰타입
            , "user_grade_seq"          // 사용자 등급
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("couponType", code.get("coupon_type"));
        model.addAttribute("couponPurpose", code.get("coupon_purpose"));
        model.addAttribute("couponApplyType", code.get("coupon_apply_type"));
        model.addAttribute("couponDuplYn", code.get("coupon_dupl_yn"));
        model.addAttribute("couponDiscountType", code.get("coupon_discount_type"));
        model.addAttribute("couponValidType", code.get("coupon_valid_type"));
        model.addAttribute("issueLimitType", code.get("issue_limit_type"));
        model.addAttribute("couponApplyScope", code.get("coupon_apply_scope"));
        model.addAttribute("shareYn", code.get("share_yn"));
        model.addAttribute("couponCmsUnit", code.get("coupon_cms_unit"));
        model.addAttribute("couponCartBarcode", code.get("coupon_cart_barcode"));
        model.addAttribute("couponMethodList", couponMethodList);
        model.addAttribute("availYn", code.get("avail_yn"));
        model.addAttribute("couponPaymentType", code.get("coupon_payment_type"));
        model.addAttribute("couponRandomType", code.get("coupon_random_type"));
        model.addAttribute("userGradeSeq", code.get("user_grade_seq"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("useYnJson", om.writeValueAsString(code.get("use_yn")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("couponPurposeJson", om.writeValueAsString(code.get("coupon_purpose")));
        model.addAttribute("couponApplyTypeJson", om.writeValueAsString(code.get("coupon_apply_type")));
        model.addAttribute("couponDuplYnJson", om.writeValueAsString(code.get("coupon_dupl_yn")));
        model.addAttribute("couponDiscountTypeJson", om.writeValueAsString(code.get("coupon_discount_type")));
        model.addAttribute("couponValidTypeJson", om.writeValueAsString(code.get("coupon_valid_type")));
        model.addAttribute("issueLimitTypeJson", om.writeValueAsString(code.get("issue_limit_type")));
        model.addAttribute("couponApplyScopeJson", om.writeValueAsString(code.get("coupon_apply_scope")));
        model.addAttribute("couponStatusJson", om.writeValueAsString(code.get("coupon_status")));
        model.addAttribute("shareYnJson", om.writeValueAsString(code.get("share_yn")));
        model.addAttribute("couponCmsUnitJson", om.writeValueAsString(code.get("coupon_cms_unit")));
        model.addAttribute("couponCartBarcodeJson", om.writeValueAsString(code.get("coupon_cart_barcode")));
        model.addAttribute("couponMethodListJson", om.writeValueAsString(couponMethodList));
        model.addAttribute("availYnJson", om.writeValueAsString(code.get("avail_yn")));
        model.addAttribute("couponPaymentTypeJson", om.writeValueAsString(code.get("coupon_payment_type")));
        model.addAttribute("couponRandomTypeJson", om.writeValueAsString(code.get("coupon_random_type")));
        model.addAttribute("userGradeSeqJson", om.writeValueAsString(code.get("user_grade_seq")));

        /*
        model.addAttribute("couponListGridBaseInfo", new RealGridBaseInfo("couponListGridBaseInfo", COUPON_LIST_GRID_COLUMN, COUPON_LIST_GRID_OPTION).toString());
        model.addAttribute("applyGridBaseInfo", new RealGridBaseInfo("applyGridBaseInfo", COUPON_APPLY_GRID_COLUMN, COUPON_APPLY_GRID_OPTION).toString());
        model.addAttribute("exceptGridBaseInfo", new RealGridBaseInfo("exceptGridBaseInfo", COUPON_EXCEPT_GRID_COLUMN, COUPON_EXCEPT_GRID_OPTION).toString());
        */

        model.addAllAttributes(RealGridHelper.create("couponListGridBaseInfo", CouponListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("applyGridBaseInfo", CouponApplyGetDto.class));
        model.addAllAttributes(RealGridHelper.create("exceptGridBaseInfo", CouponExceptItemDto.class));

        return "/promotion/couponMain";
    }

    /**
     * 결제수단 정보 조회
     *
     * @param siteType
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getMethodList.json"})
    public List<PaymentMethodDto> getMethodList(@RequestParam(value="siteType") String siteType) {
        String apiUri = EscrowConstants.ESCROW_GET_PAYMENT_METHOD_BY_TYPE + "?paymentMethodType=CARD,EASYPAY&siteType=" + siteType;

        List<PaymentMethodDto> methodList = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri, new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {}
        ).getData();

        return methodList;
    }

    /**
     * 쿠폰 정보 조회
     *
     * @param couponListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getCouponList.json"})
    public List<CouponListGetDto> getCouponList(CouponListParamDto couponListParamDto) {
        String apiUri = "/promotion/coupon/getCouponList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponListGetDto>>>() {};

        return (List<CouponListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, couponListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 쿠폰 상세 정보 조회
     * @param couponNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getCouponDetail.json"})
    public CouponDetailGetDto getCouponDetail(@RequestParam(name = "couponNo") long couponNo,
        @RequestParam(name = "copyYn") Character copyYn) throws Exception {
        String apiUri = "/promotion/coupon/getCouponDetail";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<CouponDetailGetDto>>() {};

        return (CouponDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?couponNo=" + couponNo +"&copyYn=" + copyYn , typeReference).getData();
    }

    /**
     * 일회성 난수쿠폰 다운로드
     * @param couponNo
     * @return
     */
    @GetMapping(value = {"/downloadCouponRandom.json"})
    public String downloadCouponRandom(Model model, @RequestParam(name = "couponNo") long couponNo) throws Exception {
        String apiUri = "/promotion/coupon/getCouponRandomList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<Object>>>() {};
        List<String> dataObj = (List<String>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?couponNo=" + couponNo, typeReference).getData();

        ExcelBindInfo excelBindInfo = new ExcelBindInfo();
        excelBindInfo.setDownloadExcelFileName("randomList");
        excelBindInfo.setSheetName("일회성 난수번호");
        excelBindInfo.setCellHeaders("난수쿠폰번호");
        for (String data : dataObj) {
            excelBindInfo.addRows(data);
        }

        model.addAttribute("excelBindInfo", excelBindInfo);

        return "excelDataDownloadView";
    }

    /**
     * 쿠폰 상세 정보 조회
     * @param couponNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getCouponRandomList.json"})
    public CouponDetailGetDto getCouponRandomList(@RequestParam(name = "couponNo") long couponNo,
                                                 @RequestParam(name = "issueCount") long issueCount) throws Exception {
        String apiUri = "/promotion/coupon/getCouponRandomList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<CouponDetailGetDto>>() {};

        return (CouponDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?couponNo=" + couponNo +"&issueCount=" + issueCount , typeReference).getData();
    }

    /**
     * 쿠폰 상세 정보 저장
     * @param couponDetailSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = {"/setCoupon.json"})
    public Long setCoupon(@RequestBody CouponDetailSetDto couponDetailSetDto) throws Exception {
        couponDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());
        couponDetailSetDto.setEmpNm(loginCookieService.getUserInfo().getUserNm());

        String apiUri = "/promotion/coupon/setCoupon";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE,
            couponDetailSetDto, apiUri, typeReference).getData();
    }


    /**
     * 쿠폰 상태변경 (발급, 삭제, 중단, 회수)
     *
     * @param couponDetailSetDto
     * @returnsetCoupon
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setCouponStatus.json")
    public Long setCouponStatus(@RequestBody CouponDetailSetDto couponDetailSetDto) throws Exception {
        couponDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<CouponDetailGetDto> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , couponDetailSetDto
            , "/promotion/coupon/setCouponStatus"
            , new ParameterizedTypeReference<ResponseObject<CouponDetailGetDto>>() {
            }
        );

        return responseObject.getData().getCouponNo();
    }

    /**
     * 프로모션관리 > 쿠폰관리 > 쿠폰 발급관리
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/couponIssueMng")
    public String couponIssueMng(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "coupon_status"    //쿠폰상태
            , "store_type"              //점포유형
            , "coupon_purpose"          //쿠폰행사목적
            , "coupon_type"             //쿠폰종류
            , "coupon_apply_type"       //쿠폰적용방식
        );

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("couponApplyTypeJson", om.writeValueAsString(code.get("coupon_apply_type")));
        model.addAttribute("couponStatusJson", om.writeValueAsString(code.get("coupon_status")));
        model.addAttribute("couponPurposeJson", om.writeValueAsString(code.get("coupon_purpose")));

        model.addAttribute("couponStatus", code.get("coupon_status"));
        model.addAttribute("storeType", code.get("store_type"));

        model.addAllAttributes(RealGridHelper.create("couponIssueMngCouponGridBaseInfo", CouponListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("couponIssueMngGridBaseInfo", CouponIssueHistListGetDto.class));

        return "/promotion/couponIssueMng";
    }

    /**
     * 프로모션관리 > 쿠폰관리 > 쿠폰 사용조회
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/couponIssueHistory")
    public String couponIssueHistory(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "coupon_status"     //쿠폰상태
            , "store_type"              // 점포유형
            , "coupon_purpose"          // 쿠폰행사목적
            , "coupon_type"             // 쿠폰종류
            , "coupon_apply_type"       // 쿠폰적용방식
        );

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("store_type")));
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("couponApplyTypeJson", om.writeValueAsString(code.get("coupon_apply_type")));
        model.addAttribute("couponStatusJson", om.writeValueAsString(code.get("coupon_status")));
        model.addAttribute("couponPurposeJson", om.writeValueAsString(code.get("coupon_purpose")));

        model.addAttribute("couponStatus", code.get("coupon_status"));
        model.addAttribute("storeType", code.get("store_type"));

        model.addAllAttributes(RealGridHelper.create("couponListGridBaseInfo", CouponListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("couponUseHistoryGridBaseInfo", CouponIssueHistListGetDto.class));

        return "/promotion/couponIssueHistory";
    }

    /**
     * 회원에게 쿠폰 발급
     *
     * @param couponIssueMemberSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setIssueCoupon.json")
    public long setIssueCoupon(@ModelAttribute CouponIssueMemberSetDto couponIssueMemberSetDto) throws Exception {
        couponIssueMemberSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());
        ResponseObject<CouponIssueMemberSetDto> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , couponIssueMemberSetDto
            , "/promotion/coupon/setIssueCoupon"
            , new ParameterizedTypeReference<ResponseObject<CouponIssueMemberSetDto>>() {
            }
        );

        return responseObject.getData().getCouponNo();
    }

    /**
     * 쿠폰 발급 이력 조회
     *
     * @param couponIssueHistSearchSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getCouponIssueHistList.json")
    public CouponIssueHistGetDto getCouponIssueHistList(@ModelAttribute CouponIssueHistSearchSetDto couponIssueHistSearchSetDto) throws Exception {
        String apiUri = "/promotion/coupon/getCouponIssueHistList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<CouponIssueHistGetDto>>() {};

        return (CouponIssueHistGetDto) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, couponIssueHistSearchSetDto, apiUri, typeReference).getData();
    }

    /**
     * 프로모션관리 > 쿠폰관리 > 쿠폰발급관리 > 쿠폰 회원발급 일괄등록 팝업
     *
     * @param model
     * @param couponNo
     * @param callBackScript
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/popup/uploadIssueCouponPop")
    public String uploadIssueCouponPop(Model model,
        @RequestParam(value = "couponNo", defaultValue = "0") int couponNo,
        @RequestParam(value = "callback", defaultValue = "") String callBackScript) throws Exception {
        model.addAttribute("couponNo", couponNo);
        model.addAttribute("callBackScript", callBackScript);
        return "/common/pop/uploadIssueCouponPop";
    }

    /**
     * 회원에게 발급된 쿠폰 회수
     *
     * @param couponIssueMemberSetDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/setWithdrawCoupon.json")
    public long setWithdrawCoupon(@RequestBody CouponIssueMemberSetDto couponIssueMemberSetDto) throws Exception {
        couponIssueMemberSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<CouponIssueMemberSetDto> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.MANAGE
            , couponIssueMemberSetDto
            , "/promotion/coupon/setWithdrawCoupon"
            , new ParameterizedTypeReference<ResponseObject<CouponIssueMemberSetDto>>() {
            }
        );

        return responseObject.getData().getCouponNo();
    }

    /**
     * 프로모션관리 > 쿠폰관리 > CRM 쿠폰 대량발급
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/crmCouponIssue")
    public String crmCouponIssue(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "coupon_type"      // 쿠폰종류
            , "coupon_status"           // 쿠폰상태
            , "store_type"              // 점포유형
            , "coupon_cms_unit"         // CMS귀속부서
        );

        model.addAttribute("storeType", code.get("store_type"));

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("couponTypeJson", om.writeValueAsString(code.get("coupon_type")));
        model.addAttribute("couponStatusJson", om.writeValueAsString(code.get("coupon_status")));
        model.addAttribute("dpartCdJson", om.writeValueAsString(code.get("coupon_cms_unit")));

        model.addAllAttributes(RealGridHelper.create("crmCouponGridBaseInfo", CrmCouponListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("uploadListGridBaseInfo", CouponFileGetDto.class));

        return "/promotion/crmCouponIssue";
    }

    /**
     * CRM 쿠폰 파일 저장
     * @param couponFileSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/setCrmCouponFile.json"})
    public Long setCrmCouponFile(@RequestBody CouponFileSetDto couponFileSetDto) {
        couponFileSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/coupon/setCouponFile";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, couponFileSetDto, apiUri, typeReference).getData();
    }

    /**
     * CRM쿠폰 업로드 파일 중단 처리
     * @param couponFileSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/setCouponFileStop.json"})
    public Long setCouponFileStop(@RequestBody CouponFileSetDto couponFileSetDto) {
        couponFileSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/coupon/setCouponFileStop";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, couponFileSetDto, apiUri, typeReference).getData();
    }

    /**
     * CRM 쿠폰 파일 조회
     * @param couponNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getCrmCouponFile.json"})
    public CouponFileGetDto getCrmCouponFile(@RequestParam(name = "couponNo") Long couponNo) {
        String apiUri = "/promotion/coupon/getCouponFile";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<CouponFileGetDto>>() {};

        return (CouponFileGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?couponNo=" + couponNo , typeReference).getData();
    }

    /**
     * CRM 쿠폰 파일 조회
     * @param couponNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/getCrmCouponFileList.json"})
    public List<CouponFileGetDto> getCrmCouponFileList(@RequestParam(name = "couponNo") Long couponNo) {
        String apiUri = "/promotion/coupon/getCouponFileList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponFileGetDto>>>() {};

        return (List<CouponFileGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?couponNo=" + couponNo , typeReference).getData();
    }

    /**
     * CMS 쿠폰 상품 리스트 조회
     * @param couponSeq
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/getCmsCouponProdList.json")
    public List<CouponApplyGetDto> getCmsCouponProductList(@RequestParam(name = "couponSeq") Long couponSeq,
        @RequestParam(name = "coyn") String coyn) throws Exception {

        List<CouponApplyGetDto> resultList = new ArrayList<>();

        String apiUri = "/promotion/coupon/cms/getCouponProductList?couponSeq="+ couponSeq + "&coyn=" + coyn;
        List<CmsCouponProductGetDto> cmsCouponProdList = resourceClient.getForResponseObject(ResourceRouteName.MANAGE,  apiUri,
            new ParameterizedTypeReference<ResponseObject<List<CmsCouponProductGetDto>>>() {}).getData();

        List<CouponApplyGetDto> transApplyList = cmsCouponProdList.stream()
            .map(c -> new CouponApplyGetDto(c.getPluCd(), c.getPluNm(), c.getItemType(), c.getCouponValue()))
            .collect(Collectors.toList());

        if (ObjectUtils.isNotEmpty(transApplyList)) {
            List<String> itemNos = transApplyList.stream().map(i -> i.getApplyCd()).collect(Collectors.toList());

            ResponseObject<List<PromoUploadSetDto>> response = resourceClient.postForResponseObject(
                ResourceRouteName.MANAGE
                , itemNos
                , "/promotion/upload/getItemInfoList"
                , new ParameterizedTypeReference<ResponseObject<List<PromoUploadSetDto>>>() {
                }
            );

            List<PromoUploadSetDto> itemInfoList = response.getData();

            String itemType = transApplyList.get(0).getItemType();
            String couponValue = transApplyList.get(0).getCouponValue();
            String differentCountYn = (transApplyList.size() > itemInfoList.size() ? "Y" : "N");

            for (PromoUploadSetDto i : itemInfoList) {
                CouponApplyGetDto dto = new CouponApplyGetDto();
                dto.setApplyCd(i.getItemNo());
                dto.setApplyNm(i.getItemNm());
                dto.setItemType(itemType);
                dto.setCouponValue(couponValue);
                dto.setDifferentCountYn(differentCountYn);

                resultList.add(dto);
            }
        }

        return resultList;
    }
}