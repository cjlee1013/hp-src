package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "검색 데이터 추출")
public class SearchDataExportParamDto {
    @ApiModelProperty(value = "점포이름", required = true)
    @NotNull(message = "storeNm is null")
    private String storeNm;

    @ApiModelProperty(value = "점포타입", required = true)
    @NotNull(message = "storeType is null")
    private String storeType;

    @ApiModelProperty(value = "점포아이디", required = true)
    @NotNull(message = "storeId is null")
    private Long storeId;

    @ApiModelProperty(value = "정렬", required = true)
    @NotNull(message = "sort is null")
    private String sort;

    @ApiModelProperty(value = "노출필드", required = true)
    @NotNull(message = "includeFields is null")
    private String includeFields;
}
