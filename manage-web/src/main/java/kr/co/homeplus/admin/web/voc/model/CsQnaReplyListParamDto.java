package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaReplyListParamDto {

    private Integer qnaNo;

    private String displayStatus;
    
    private String partnerId;

}
