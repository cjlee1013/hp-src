package kr.co.homeplus.admin.web.user.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserStatus implements RealGridLookUpSupport {
    UNKNOWN_ZERO(0, "-")
    , NORMAL(4, "정상")
    ;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(UserStatus::name, UserStatus::getDesc));

    @Getter
    private final int code;
    @Getter
    private final String desc;

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
