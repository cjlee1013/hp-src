package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class PaymentTypeModel {
    String methodCd;
    String methodNm;
    String siteType;
    String siteNm;

    public static PaymentTypeModel create(ParentMethodCd parentMethodCd) {
        PaymentTypeModel paymentTypeModel = new PaymentTypeModel();
        paymentTypeModel.setMethodCd(parentMethodCd.getMethodCd());
        paymentTypeModel.setMethodNm(parentMethodCd.getMethodNm());
        return paymentTypeModel;
    }
}
