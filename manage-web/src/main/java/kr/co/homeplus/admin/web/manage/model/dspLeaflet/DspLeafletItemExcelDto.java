package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class DspLeafletItemExcelDto {

    @EqualsAndHashCode.Include
    private String storeType;
    @EqualsAndHashCode.Include
    private String cateNo;
    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String useYn;

}
