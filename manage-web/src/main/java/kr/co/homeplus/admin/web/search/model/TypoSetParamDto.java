package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@ApiModel(description = "오탈자정보")
public class TypoSetParamDto {
    @ApiModelProperty(notes = "일련번호", position = 0)
    private Long typoId;
    @ApiModelProperty(notes = "키워드", position = 1, required = true)
    private String keyword;
    @ApiModelProperty(notes = "오타키워드", position = 2, required = true)
    private String correctedTerms;
}
