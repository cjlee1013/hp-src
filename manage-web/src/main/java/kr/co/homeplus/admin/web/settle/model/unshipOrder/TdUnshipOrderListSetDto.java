package kr.co.homeplus.admin.web.settle.model.unshipOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TdUnshipOrderListSetDto {
    @ApiModelProperty(value = "검색기간(년)")
    private String schYear;

    @ApiModelProperty(value = "검색기간(월)")
    private String schMonth;

    @ApiModelProperty(value = "점포유형")
    private String schStoreType;

    @ApiModelProperty(value = "점포코드")
    private String schStoreId;

    @ApiModelProperty(value = "카테고리 division")
    private String division;

    @ApiModelProperty(value = "카테고리 groupNo")
    private String groupNo;

    @ApiModelProperty(value = "카테고리 deptNo")
    private String dept;

    @ApiModelProperty(value = "카테고리 classNo")
    private String classCd;

    @ApiModelProperty(value = "카테고리 subClassNo")
    private String subclass;

    @ApiModelProperty(value = "배송상태")
    private String schShipStatus;

    //SETTLE-942 마켓연동 제휴 추가 (상품구분: TD/제휴)
    @ApiModelProperty(value = "상품구분")
    private String marketType;
}
