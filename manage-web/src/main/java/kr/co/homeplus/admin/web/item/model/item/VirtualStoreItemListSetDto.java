package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualStoreItemListSetDto {
    private List<VirtualStoreItemSetDto> saveList;
    /*private List<VirtualStoreItemSetDto> createList;
    private List<VirtualStoreItemSetDto> updateList;*/
    private String regId;
}