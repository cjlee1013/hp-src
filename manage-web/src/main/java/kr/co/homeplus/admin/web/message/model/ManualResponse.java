package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class ManualResponse {

    private long manualSendSeq;

    private long manualSendWorkSeq;

    private LocalDateTime workDt;

    private ReturnCode returnCode;

    private String message;

    private Integer sendCount;
}
