package kr.co.homeplus.admin.web.claim.model;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.Data;

@Data
public class ClaimGiftItemListDto {

    @ApiModelProperty(value = "상품번호", position = 1)
    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 2)
    @RealGridColumnInfo(headText = "상품명", width = 200)
    private String itemNm;

    @ApiModelProperty(value = "행사구분", position = 3)
    @RealGridColumnInfo(headText = "행사구분")
    private String giftTargetType;

    @ApiModelProperty(value = "행사번호", position = 4)
    @RealGridColumnInfo(headText = "행사번호")
    private String giftNo;

    @ApiModelProperty(value = "행사명", position = 5)
    @RealGridColumnInfo(headText = "행사명", width = 300)
    private String giftMsg;
}
