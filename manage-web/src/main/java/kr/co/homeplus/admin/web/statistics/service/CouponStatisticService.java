package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.statistics.model.CouponStatistics;
import kr.co.homeplus.admin.web.statistics.model.CouponStatisticsSelect;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CouponStatisticService {

    private final ResourceClient resourceClient;

    /**
     * 쿠폰통계 메인조회
     * @param couponStatisticsSelect
     * @return
     */
    public List<CouponStatistics> getCouponStatisticList(CouponStatisticsSelect couponStatisticsSelect) {

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponStatistics>>>() {};
        List<CouponStatistics> couponStatisticsList = (List<CouponStatistics>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, couponStatisticsSelect,
            EscrowConstants.ESCROW_GET_COUPON_STATISTICS_LIST, typeReference).getData();

        return couponStatisticsList;
    }

    /**
     * 상품쿠폰, 상품할인 조회
     * @param couponStatisticsSelect
     * @return
     */
    public List<CouponStatistics> getItemCouponStatisticDetailList(CouponStatisticsSelect couponStatisticsSelect) {

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponStatistics>>>() {};
        List<CouponStatistics> couponStatisticsDetailList = (List<CouponStatistics>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, couponStatisticsSelect,
            EscrowConstants.ESCROW_GET_ITEM_COUPON_STATISTICS_DETAIL_LIST, typeReference).getData();

        return couponStatisticsDetailList;
    }

    /**
     * 장바구니쿠폰, 배송비쿠폰 조회
     * @param couponStatisticsSelect
     * @return
     */
    public List<CouponStatistics> getCartCouponStatisticDetailList(CouponStatisticsSelect couponStatisticsSelect) {
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<CouponStatistics>>>() {};
        List<CouponStatistics> couponStatisticsDetailList = (List<CouponStatistics>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, couponStatisticsSelect,
            EscrowConstants.ESCROW_GET_CART_COUPON_STATISTICS_DETAIL_LIST, typeReference).getData();

        return couponStatisticsDetailList;
    }
}
