package kr.co.homeplus.admin.web.core.exception;

import kr.co.homeplus.admin.web.core.interceptor.AuthorizationCheckInterceptor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 망분리 메뉴 접근시 허용되지 않은 아이피로 접근하는 경우 반환하는 예외입니다.
 *
 * @see AuthorizationCheckInterceptor
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class NotAllowedAccessIpException extends RuntimeException {
    private int roleSeq = 0;
    private String message;
    private String code;

    public NotAllowedAccessIpException(int roleSeq, String message){
        this.roleSeq = roleSeq;
        this.message = message;
    }

    public NotAllowedAccessIpException(int roleSeq, String message, String code){
        this.roleSeq = roleSeq;
        this.message = message;
        this.code = code;
    }

    public NotAllowedAccessIpException(int roleSeq){
        this.roleSeq = roleSeq;
    }

    public NotAllowedAccessIpException(String message){
        this.message = message;
    }
}
