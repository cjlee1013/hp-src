package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "우편번호(건물DB) DTO")
public class ZipcodeManageDto {

    /* ESCROW 우편번호(건물DB) 필드 정보 (building_zipcode) */
    @ApiModelProperty(notes = "건물관리번호", required = true)
    private String buildMngNo;

    @ApiModelProperty(notes = "법정동코드")
    private String courtDongCd;
    @ApiModelProperty(notes = "시/도명")
    private String sidoNm;
    @ApiModelProperty(notes = "시/군/구명")
    private String sigunguNm;
    @ApiModelProperty(notes = "법정읍면동명")
    private String courtEubmyndnNm;
    @ApiModelProperty(notes = "법정리명")
    private String courtRiNm;
    @ApiModelProperty(notes = "산숫자종류(0:대지,1:산)")
    private String mountainNumKind;
    @ApiModelProperty(notes = "지번본번(번지)")
    private Integer gibunBonNo;
    @ApiModelProperty(notes = "지번부번(호)")
    private Integer gibunBuNo;
    @ApiModelProperty(notes = "도로명코드(시군구코드(5)+도로명번호(7))")
    private String roadAddrCd;
    @ApiModelProperty(notes = "도로명")
    private String roadAddrNm;
    @ApiModelProperty(notes = "지하숫자종류(0:지상,1:지하,2:공중)")
    private String undgrdNumKind;
    @ApiModelProperty(notes = "건물본번")
    private Integer buildBonNo;
    @ApiModelProperty(notes = "건물부번")
    private Integer buildBuNo;
    @ApiModelProperty(notes = "상세건물명")
    private String detailBuildNm;
    @ApiModelProperty(notes = "행정동코드(참고용)")
    private String adminDongCd;
    @ApiModelProperty(notes = "행정동명(참고용)")
    private String adminDongNm;
    @ApiModelProperty(notes = "우편번호(2015.8.1이후 기초구역번호(신우편번호)제공)")
    private String zipcode;
    @ApiModelProperty(notes = "시군구용건물명")
    private String sigunguBuildNm;
    @ApiModelProperty(notes = "공동주택숫자종류(0:비공동주택,1:공동주택)")
    private String CmmHouseNumKind;
    @ApiModelProperty(notes = "기본구역번호")
    private String baseAreaNo;
    @ApiModelProperty(notes = "상세주소숫자종류(0:미부여,1:부여)")
    private String detailAddrNumKind;
    /* 필요해서 추가된 컬럼 (building_zipcode) */
    @ApiModelProperty(notes = "도서산간유형(ALL:전국,JEJU:제주,ISLAND:산간지역)")
    private String islandType;
    @ApiModelProperty(notes = "조합된 도로명주소(노출용)")
    private String roadAddrFullTxt;
    @ApiModelProperty(notes = "조합된 지번주소(노출용)")
    private String gibunAddrFullTxt;
    @ApiModelProperty(notes = "수정자")
    private String chgId;
    @ApiModelProperty(notes = "수정일")
    private String chgDt;

    /** 화면에서 필요함 */
    @ApiModelProperty(notes = "도서산간타입명")
    private String islandTypeTxt;
}
