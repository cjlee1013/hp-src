package kr.co.homeplus.admin.web.manage.model.dspMainThemeItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspMainThemeSelectDto {

    @RealGridColumnInfo(headText = "테마번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long themeId;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 1, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "테마명", width = 200, order = 2)
    private String themeNm;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 3, sortable = true)
    private String useYnTxt;

}
