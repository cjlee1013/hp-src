package kr.co.homeplus.admin.web.settle.model.dcOnlineReceivePay;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DcOnlineOrderDetailListGetDto {
    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 120)
    private String gubun;

    @ApiModelProperty(value = "배송완료일")
    @RealGridColumnInfo(headText = "배송완료일", width = 120)
    private String shipCompleteDt;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 120)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120)
    private String itemNo;

    @ApiModelProperty(value = "RMS상품번호")
    @RealGridColumnInfo(headText = "RMS상품번호", width = 120)
    private String rmsItemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", width = 450)
    private String itemName;

    @ApiModelProperty(value = "배송수량")
    @RealGridColumnInfo(headText = "배송수량", width = 120)
    private String completeQty;

    @ApiModelProperty(value = "매입원가")
    @RealGridColumnInfo(headText = "매입원가", width = 120)
    private String orgPrice;

    @ApiModelProperty(value = "판매가")
    @RealGridColumnInfo(headText = "판매가", width = 120)
    private String itemSaleAmt;

    @ApiModelProperty(value = "할인금액")
    @RealGridColumnInfo(headText = "할인금액", width = 120)
    private String discountAmt;

    @ApiModelProperty(value = "발주서/반품 번호")
    @RealGridColumnInfo(headText = "발주서/반품 번호", width = 120)
    private String poNo;

    @ApiModelProperty(value = "RMS처리일자")
    @RealGridColumnInfo(headText = "RMS처리일자", width = 120)
    private String rmsDt;
}
