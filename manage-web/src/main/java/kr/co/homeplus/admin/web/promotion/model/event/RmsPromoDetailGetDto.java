package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RmsPromoDetailGetDto {
    private Long rpmPromoCompDetailId;
    private Integer thresholdId;

    // 구간할인 리스트
    private List<RmsPromoIntervalGetDto> intervalList;

    // 점포 리스트
    private List<RmsPromoStoreGetDto> storeList;

    // 적용상품 리스트
    private List<RmsPromoItemGetDto> itemList;

    // 할인 대상 상품 리스트
    private List<RmsPromoItemGetDto> discItemList;
}
