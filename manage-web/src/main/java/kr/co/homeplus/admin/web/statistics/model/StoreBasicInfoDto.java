package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "점포 기본정보 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class StoreBasicInfoDto {
    @ApiModelProperty(value = "점포 ID", position = 1)
    private String storeId;

    @ApiModelProperty(value = "점포명", position = 2)
    private String storeNm;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, EXP)", position = 3)
    private String storeType;
}