package kr.co.homeplus.admin.web.delivery.model;

import java.util.List;
import lombok.Data;

@Data
public class RemoteShipManageSelectModel {

    private String schZipcode;
    private String schStoreType;
    private int schStoreId;
    private List<String> shipWeekdayList;
    private String schSidoType;     //시/도
    private String schSigunguType;  //시군구
    private String schAddrKeyword;      //검색어(도로명)
    private String schBuildNo;      //검색어(건물번호)

}
