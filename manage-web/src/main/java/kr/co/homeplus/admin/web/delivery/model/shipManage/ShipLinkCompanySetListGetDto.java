package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배사코드관리 연동업체설정 리스트 응답 DTO")
public class ShipLinkCompanySetListGetDto {
    @ApiModelProperty(notes = "연동업체공통코드")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "연동업체명")
    private String dlvLinkCompanyNm;

    @ApiModelProperty(notes = "연동여부")
    private String linkYn;

    @ApiModelProperty(notes = "연동중지시작일")
    private String stopStartDt;

    @ApiModelProperty(notes = "연동중지종료일")
    private String stopEndDt;

    @ApiModelProperty(notes = "연동중지기간")
    private String stopDt;

    @ApiModelProperty(notes = "등록자")
    private String regId;

    @ApiModelProperty(notes = "등록일")
    private String regDt;

    @ApiModelProperty(notes = "수정자")
    private String chgId;

    @ApiModelProperty(notes = "수정일")
    private String chgDt;
}