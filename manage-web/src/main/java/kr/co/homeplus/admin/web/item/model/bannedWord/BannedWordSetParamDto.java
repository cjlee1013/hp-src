package kr.co.homeplus.admin.web.item.model.bannedWord;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BannedWordSetParamDto {
    private Long bannedSeq;
    private String bannedType;
    private String cateCd;
    private String cateKind;
    private String keyword;
    private String keywordDesc;
    private String useYn;
    private String regId;
}
