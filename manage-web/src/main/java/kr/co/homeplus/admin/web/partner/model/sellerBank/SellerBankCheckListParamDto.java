package kr.co.homeplus.admin.web.partner.model.sellerBank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellerBankCheckListParamDto {

    private String schStartDt;
    private String schEndDt;
    private String schType;
    private String schKeyword;

}
