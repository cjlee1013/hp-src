package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.maker.MakerListParamDto;
import kr.co.homeplus.admin.web.item.model.maker.MakerListSelectDto;
import kr.co.homeplus.admin.web.item.model.maker.MakerSchPopDto;
import kr.co.homeplus.admin.web.item.model.maker.MakerSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/maker")
public class MakerController {

    final private ResourceClient resourceClient;

    final private LoginCookieService cookieService;

    public MakerController(ResourceClient resourceClient,
            LoginCookieService cookieService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;

    }

    /**
     * 상품관리 > 상품관리 > 제조사관리 > 메인페이지
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/makerMain", method = RequestMethod.GET)
    public String makerMain(Model model) {

        model.addAllAttributes(RealGridHelper.createForGroup("makerListGridBaseInfo", MakerListSelectDto.class));

        return "/item/makerMain";
    }

    /**
     * 상품관리 > 상품관리 > 제조사 > 리스트
     *
     * @param listParamDto
     * @return MakerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getMakerList.json"}, method = RequestMethod.GET)
    public List<MakerListSelectDto> getMakerList(MakerListParamDto listParamDto) {
        String apiUri = "/item/maker/getMakerList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MakerListSelectDto>>>() {};
        return (List<MakerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil
            .getRequestString(apiUri, MakerListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 제조사명으로 리스트 조회
     * @param makerNm
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMakerListByNm.json"}, method = RequestMethod.GET)
    public List<MakerListSelectDto> getMakerListByNm(@RequestParam(value = "makerNm") String makerNm) {

        String apiUri = "/item/maker/getMakerListByNm?makerNm=" + makerNm;

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<MakerListSelectDto>>>() {};
        return (List<MakerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri , typeReference).getData();
    }

    /**
     * 상품관리 > 상품관리 > 제조사관리
     *
     * @param setParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setMaker.json"}, method = RequestMethod.POST)
    public ResponseResult setMaker(@ModelAttribute MakerSetParamDto setParamDto) {
        setParamDto.setRegId(cookieService.getUserInfo().getEmpId());
        String apiUri = "/item/maker/setMaker";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {};
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, typeReference).getData();
    }

}
