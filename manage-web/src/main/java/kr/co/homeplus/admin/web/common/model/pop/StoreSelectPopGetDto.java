package kr.co.homeplus.admin.web.common.model.pop;

import java.util.List;
import lombok.Data;

/**
 * 점포선택 팝업창
 */
@Data
public class StoreSelectPopGetDto {

	//적용 점포리스트
	private List<StoreGetDto> storeList;

	//제외 점포리스트
	private List<StoreGetDto> exclStoreList;
}
