package kr.co.homeplus.admin.web.escrow.model.extract;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "보조결제수단 결제정보 추출용 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class ExtractAssistancePaymentInfoDto {
    @ApiModelProperty(value = "상위결제수단명", position = 1)
    @RealGridColumnInfo(headText = "결제수단", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String parentMethodNm;

    @ApiModelProperty(value = "카드명", position = 2)
    @RealGridColumnInfo(headText = "상세 수단", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String cardNm;

    @ApiModelProperty(value = "결제건수", position = 3)
    @RealGridColumnInfo(headText = "결제건수", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long payCnt;

    @ApiModelProperty(value = "결제금액", position = 4)
    @RealGridColumnInfo(headText = "결제금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long payAmt;

    @ApiModelProperty(value = "객단가", position = 5)
    @RealGridColumnInfo(headText = "객단가", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long userUnitPrice;
}
