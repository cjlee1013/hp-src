package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspPopup.DspPopupListParamDto;
import kr.co.homeplus.admin.web.manage.model.dspPopup.DspPopupListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspPopup.DspPopupPartnerListSelectDto;
import kr.co.homeplus.admin.web.manage.model.dspPopup.DspPopupSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class DspPopupController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 팝업 관리 > 파트너공지 팝업 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspPopup/dspPopupMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) {

        codeService.getCodeModel(model,
                "use_yn"                  // 사용여부
            ,   "dsp_popup_period_typ"    // 조회기간
            ,   "dsp_popup_link_type"     // 링크타입
            ,   "dsp_popup_btn_type"      // 버튼타입
            ,   "dsp_popup_btn_period"    // 숨김기능
            ,   "dsp_popup_target"        // 노출대상
            ,   "partner_pop_menu_id"     // 파트너 팝업 메뉴 ID
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("dspPopupListGridBaseInfo", DspPopupListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("dspPopupPartnerListGridBaseInfo", DspPopupPartnerListSelectDto.class));

        return "/manage/dspPopupMain";
    }

    /**
     * 사이트관리 > 팝업 관리 > 파트너공지 팝업 관리 등록/수정
     *
     * @param setParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspPopup/setDspPopup.json"}, method = RequestMethod.POST)
    public ResponseResult setDspPopup(@RequestBody DspPopupSetParamDto setParamDto) {
        String apiUri = "/manage/dspPopup/setDspPopup";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 팝업 관리 > 파트너 리스트 조회 ( 그리드 )
     *
     * @param popupNo
     * @return DspPopupPartnerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/dspPopup/getDspPopupPartnerList.json"}, method = RequestMethod.GET)
    public List<DspPopupPartnerListSelectDto> getDspPopupPartnerList(@RequestParam(name = "popupNo") String popupNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("popupNo", popupNo));

        String apiUriRequest = "/manage/dspPopup/getDspPopupPartnerList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspPopupPartnerListSelectDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 팝업 관리 > 파트너 리스트 조회
     *
     * @param partnerIdArr
     * @return DspPopupPartnerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/dspPopup/getPartnerList.json"}, method = RequestMethod.GET)
    public List<DspPopupPartnerListSelectDto> getPartnerList(@RequestParam(name = "partnerIdArr") String partnerIdArr) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerIdArr", partnerIdArr));

        String apiUriRequest = "/manage/dspPopup/getPartnerList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspPopupPartnerListSelectDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 팝업 관리 > 파트너공지 팝업 관리
     *
     * @param partnerCnt
     * @return String
     */
    @Deprecated
    @RequestMapping(value = {"/popup/temp/getPartnerListPop1"}, method = RequestMethod.GET)
    public String getPartnerListPop(Model model, @RequestParam(value = "partnerCnt") String partnerCnt) {
        model.addAttribute("partnerCnt" , partnerCnt);
        return "/manage/pop/dspPopupPartnerPop";
    }

    /**
     * 사이트관리 > 팝업 관리 > 파트너공지 팝업 관리 리스트
     *
     * @param listParamDto
     * @return DspPopupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/dspPopup/getDspPopupList.json", method = RequestMethod.GET)
    public List<DspPopupListSelectDto> getDspPopupList(DspPopupListParamDto listParamDto) {
        String apiUri = "/manage/dspPopup/getDspPopupList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DspPopupListSelectDto>>>() {};
        return (List<DspPopupListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, DspPopupListParamDto.class, listParamDto), typeReference).getData();
    }

}
