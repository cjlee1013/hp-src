package kr.co.homeplus.admin.web.search.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.AnalysisPluginGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryComponentGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertCompoundsSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertMorphExpansionSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertPreAnalysisSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertTypoCorrectSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryInsertWordInfoSetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryListGetParamDto;
import kr.co.homeplus.admin.web.search.model.DictionaryPreAnalysisGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryRegGetDto;
import kr.co.homeplus.admin.web.search.model.DictionaryTypoCorrectGetDto;
import kr.co.homeplus.admin.web.search.model.PreAnalysisDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequestRaw;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DictionaryService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getWordHistory(Integer wordId) {
        String apiUri = "/dictionary/getWordHistory/" + wordId;
        return getResponseObject(apiUri);
    }

    public ResponseObject getSrchWordInfo(
        DictionaryListGetParamDto param) {
        String apiUri = "/dictionary/getSrchWordInfo";
        return getResponseObject(apiUri, param);
    }

    public ResponseObject insertWordInfo(DictionaryInsertWordInfoSetParamDto param) {
        String apiUri = "/dictionary/insertWordInfo";
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject updateWordInfo(DictionaryInsertWordInfoSetParamDto param, Integer wordId) {
        String apiUri = "/dictionary/updateWordInfo/" + wordId;
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param, HttpMethod.PUT);
    }

    public ResponseObject deleteWordInfo(Integer wordId) {
        String apiUri = "/dictionary/deleteWordInfo/" + wordId;
        return getResponseObject(apiUri, null, HttpMethod.POST);
    }

    public DictionaryComponentGetDto getCompoundsInfo(Integer wordId) {
        String apiUri = "/dictionary/getCompounds/" + wordId;
        ResourceClientRequest<DictionaryComponentGetDto> request = ResourceClientRequest.<DictionaryComponentGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request,new TimeoutConfig()).getBody().getData();
    }

    public DictionaryPreAnalysisGetDto getPreAnalysisInfo(Integer wordId) {
        String apiUri = "/dictionary/getPreAnalysis/" + wordId;
        ResourceClientRequest<DictionaryPreAnalysisGetDto> request = ResourceClientRequest.<DictionaryPreAnalysisGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        DictionaryPreAnalysisGetDto response = resourceClient.get(request,new TimeoutConfig()).getBody().getData();
        DictionaryPreAnalysisGetDto result = new DictionaryPreAnalysisGetDto();
        if (response != null && response.getList() != null) {
            result.setAttr(response.getAttr());

            DictionaryComponentGetDto resultList = response.getList();
            List<PreAnalysisDto> list = new ArrayList<>();
            if (response.getList().getResultList() != null) {
                for (PreAnalysisDto en : response.getList().getResultList()) {
                    PreAnalysisDto re_en = new PreAnalysisDto();
                    re_en.setCompId(en.getCompId());
                    re_en.setCompName(en.getCompName());
                    re_en.setCompOrder(en.getCompOrder());
                    re_en.setCompWeight(en.getCompWeight());
                    re_en.setDescription(en.getDescription());
                    re_en.setDicType(en.getDicType());
                    re_en.setDoAnalysis(en.isDoAnalysis());
                    re_en.setDoAnalysisYn(en.isDoAnalysis() == true ? "Y" : "N");
                    re_en.setDomain(en.getDomain());
                    re_en.setExample(en.getExample());
                    re_en.setNeType(en.getNeType());
                    re_en.setPos(en.getPos());
                    re_en.setSenseTag(en.getSenseTag());
                    re_en.setUseFlag(en.isUseFlag());
                    re_en.setWordId(en.getWordId());
                    list.add(re_en);
                }
            }
            resultList.setResultList(list);
            result.setList(resultList);
        }
        return result;
    }

    public ResponseObject getRelationInfo(Integer wordId) {
        String apiUri = "/dictionary/getRelation/" + wordId;
        return getResponseObject(apiUri);
    }

    public ResponseObject insertCompoundsResult(DictionaryInsertCompoundsSetParamDto param, Integer wordId) {
        String apiUri = "/dictionary/insertCompoundsResult/" + wordId;
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject insertCompounds(DictionaryInsertCompoundsSetParamDto param, Integer wordId) {
        String apiUri = "/dictionary/insertCompounds/" + wordId;
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject insertCompoundsResult(
        DictionaryInsertPreAnalysisSetParamDto param, Integer wordId) {
        String apiUri = "/dictionary/insertPreAnalysisResult/" + wordId;
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject insertCompounds(DictionaryInsertPreAnalysisSetParamDto param,
        Integer wordId) {
        String apiUri = "/dictionary/insertPreAnalysis/" + wordId;
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri);
    }

    public ResponseObject insertMorphExpansion(
        DictionaryInsertMorphExpansionSetParamDto param) {
        String apiUri = "/dictionary/insertMorphExpansion";
        param.setUserId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    public ResponseObject deleteMorphExpansion(Integer wordId) {
        String apiUri = "/dictionary/deleteMorphExpansion?wordId=" + wordId + "&userId=" + loginCookieService
                .getUserInfo().getEmpId();
        return getResponseObject(apiUri);
    }

    public AnalysisPluginGetDto analysisResult(String keyword) {
        String apiUri = "/analysis/plugin?keyword=" + URLEncoder.encode(
            keyword.trim().replaceAll("&amp;", "&").replaceAll("&quot;", "\"")
                .replaceAll("&#39;", "'").replaceAll("&lt;", "<").replaceAll("&gt;", ">")
                .replaceAll(" +", " "), StandardCharsets.UTF_8)
            + "&analysisMode=index_analyzer&service=HOMEPLUS";
        ResourceClientRequestRaw<AnalysisPluginGetDto> request = ResourceClientRequestRaw.<AnalysisPluginGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    public DictionaryRegGetDto getRegWordInfo(Integer wordId) {
        String apiUri = "/dictionary/getRegWordInfo/" + wordId;
        ResourceClientRequest<DictionaryRegGetDto> request = ResourceClientRequest.<DictionaryRegGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }

    public <E> ResponseObject getResponseObject(String apiUri, E elements, HttpMethod method){
        HttpEntity<Object> httpEntity = new HttpEntity<>(elements, new HttpHeaders(){{
            setContentType(MediaType.APPLICATION_JSON);
            setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        }});
        String resourceUrl = resourceClient.getResourceUrl(ResourceRouteName.SEARCHMNG, apiUri);
        return resourceClient.exchange(resourceUrl, method, httpEntity, new ParameterizedTypeReference<>(){}, new TimeoutConfig()).getBody();
    }

    @Deprecated
    public DictionaryTypoCorrectGetDto getTypoCorrect(Integer wordId) {
        String apiUri = "/dictionary/getTypoCorrect/" + wordId;
        ResourceClientRequestRaw<DictionaryTypoCorrectGetDto> request = ResourceClientRequestRaw.<DictionaryTypoCorrectGetDto>getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    @Deprecated
    public ResponseObject insertTypoCorrect(DictionaryInsertTypoCorrectSetParamDto param, Integer wordId) {
        String apiUri = "/dictionary/insertTypoCorrect/" + wordId;
        return getResponseObject(apiUri, param);
    }
}