package kr.co.homeplus.admin.web.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel("계정확인 응답결과")
public class AdminCertUserResponse {
    @ApiModelProperty("계정확인여부")
    private boolean isCert;
    @ApiModelProperty("사번")
    private String empId;
    @ApiModelProperty("로그인아이디")
    private String userId;
}

