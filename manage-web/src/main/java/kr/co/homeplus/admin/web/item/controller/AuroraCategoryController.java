package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.auroraCategory.AuroraCategoryGetDto;
import kr.co.homeplus.admin.web.item.model.auroraCategory.AuroraCategorySetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/auroraCategory")
public class AuroraCategoryController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 카테고리 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/auroraCategoryMain", method = RequestMethod.GET)
    public String auroraCategoryMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"				    // 사용여부
            , "category_disp_yn"		// 노출범위
        );

        model.addAllAttributes(RealGridHelper.create("auroraCategoryGridBaseInfo", AuroraCategoryGetDto.class));

        return "/item/auroraCategoryMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 카테고리 리스트
     *
     * @param lCateCd
     * @return Object
     */
    @ResponseBody
    @RequestMapping(value = {"/getAuroraCategory.json"}, method = RequestMethod.GET)
    public List<Object> getAuroraCategory(@RequestParam(value = "lCateCd", required = false) String lCateCd) throws Exception {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("lCateCd", lCateCd));

        String apiUriRequest = "/item/auroraCategory/getAuroraCategory" + StringUtil.getParameter(getParameterList);

        List<Map<String, Object>> responseData = resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {})
            .getData();

        return GridUtil.convertTreeGridData(responseData, null, "cateCd", "parentCateCd", "depth");
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 카테고리 등록/수정
     *
     * @param auroraCategorySetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/setAuroraCategory.json"})
    public ResponseResult setAuroraCategory(@Valid @RequestBody AuroraCategorySetParamDto auroraCategorySetParamDto) {
        String apiUri = "/item/auroraCategory/setAuroraCategory";
        auroraCategorySetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, auroraCategorySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
