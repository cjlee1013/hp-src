package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.promotion.model.monitering.BatchLogListGetDto;
import kr.co.homeplus.admin.web.promotion.model.monitering.BatchLogListParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.EventItemExcelDto;
import kr.co.homeplus.admin.web.promotion.model.promo.EventItemExcelGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.EventItemExcelParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoItemExcelDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoUploadResultSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoUploadSetDto;
import kr.co.homeplus.admin.web.promotion.service.PromoUploadService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@Slf4j
@RequestMapping("/promotion/popup")
public class PromotionPopupController {
    private final ResourceClient resourceClient;
    private final PromoUploadService promoUploadService;
    private final ExcelUploadService excelUploadService;
    private final CodeService codeService;

    public PromotionPopupController(ResourceClient resourceClient, PromoUploadService promoUploadService, ExcelUploadService excelUploadService, CodeService codeService) {
        this.resourceClient = resourceClient;
        this.excelUploadService = excelUploadService;
        this.promoUploadService = promoUploadService;
        this.codeService = codeService;
    }

    /***
     * 적용정보 일괄등록 팝업
     * @param model
     * @param promotionType
     * @param couponNo
     * @param discountNo
     * @param applyScope
     * @param storeType
     * @param callBackScript
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/upload/uploadApplyScopePop", method = RequestMethod.GET)
    public String uploadApplyScopePop(Model model
        , @RequestParam(value = "promotionType", defaultValue = "") String promotionType
        , @RequestParam(value = "couponNo", defaultValue = "") String couponNo
        , @RequestParam(value = "discountNo", defaultValue = "") String discountNo
        , @RequestParam(value = "applyScope", defaultValue = "") String applyScope
        , @RequestParam(value = "exceptScope", defaultValue = "") String exceptScope
        , @RequestParam(value = "storeType", defaultValue = "") String storeType
        , @RequestParam(value = "callBackScript", defaultValue = "") String callBackScript) throws Exception {
        model.addAttribute("promotionType", promotionType);
        model.addAttribute("couponNo", couponNo);
        model.addAttribute("discountNo", discountNo);
        model.addAttribute("applyScope", applyScope);
        model.addAttribute("exceptScope", exceptScope);
        model.addAttribute("storeType", storeType);
        model.addAttribute("callBackScript", callBackScript);

        return "/common/pop/uploadApplyScopePop";
    }
    /**
     * 적용, 제외정보 일괄등록 데이터 표시 (엑셀 -> 그리드)
     * @param multipartFile
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/upload/uploadScopePop.json", method = RequestMethod.POST)
    public PromoUploadResultSetDto setUploadScopePop(@RequestParam(value = "uploadFile") MultipartFile multipartFile
        , @RequestParam(value = "promotionType", required = false) String promotionType
        , @RequestParam(value = "applyScope", required = false) String applyScope
        , @RequestParam(value = "exceptScope", required = false) String exceptScope
        , @RequestParam(value = "storeType", required = false) String storeType

    ) throws Exception {
        PromoUploadResultSetDto result =  new PromoUploadResultSetDto();

        final int APPLY_MAX_ROW  = 50000;
        final int EXCEPT_MAX_ROW = 15000;

        String scopeNm;
        int maxRow;

        if (!StringUtils.isEmpty(applyScope) && StringUtils.isEmpty(exceptScope)) {
            scopeNm = applyScope;
            maxRow = APPLY_MAX_ROW;
        } else {
            scopeNm = exceptScope;
            maxRow = EXCEPT_MAX_ROW;
        }

        List<PromoUploadSetDto> promoUploadSetDtoList = promoUploadService.getApplyScopeExcelData(multipartFile, promotionType, applyScope, exceptScope, maxRow);

        if (ObjectUtils.isEmpty(promoUploadSetDtoList)) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1006);
        }

        //카드즉시할인 중복상품 검증
        if (StringUtils.equals(promotionType, "CARD_DISCOUNT")) {
            Set<String> distinctItemNos = promoUploadSetDtoList.stream().map(x -> x.getItemNo()).collect(Collectors.toSet());

            if (promoUploadSetDtoList.size() > distinctItemNos.size()) {
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1014);
            }
        }

        List<PromoUploadSetDto> resultList = promoUploadService.getUploadScopeNm(promotionType, storeType, scopeNm, promoUploadSetDtoList);

        if (StringUtils.equals(promotionType, "CARD_DISCOUNT")) {
            promoUploadService.setMatchCardDiscountInfo(promoUploadSetDtoList, resultList, storeType);
        } else if (StringUtils.equals(scopeNm, "DRCT") || StringUtils.equals(scopeNm, "DRCT_NOR") || StringUtils.equals(scopeNm, "DRCT_DLV")) {
            promoUploadService.setMatchRsvSalePlaceInfo(promoUploadSetDtoList, resultList, scopeNm);
        }

        long uploadCount = promoUploadSetDtoList.size();
        long successCount = resultList.size();
        long failCount =  uploadCount - successCount;

        if (!ObjectUtils.isEmpty(promoUploadSetDtoList)) {
            if ((ObjectUtils.isEmpty(resultList) || promoUploadSetDtoList.size() != resultList.size())) {
                result.setSuccessYn("N");
            } else {
                result.setSuccessYn("Y");
            }

            result.setUploadCount(uploadCount);
            result.setSuccessCount(successCount);
            result.setFailCount(failCount);

            result.setDataList(resultList);
        }

        return result;
    }

    /***
     * 적용정보 일괄등록 결과 팝업
     * @param model
     * @param successYn
     * @param totalCount
     * @param successCount
     * @param failCount
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/upload/uploadResultPop", method = RequestMethod.GET)
    public String uploadResultPop(Model model
        , @RequestParam(value = "successYn", defaultValue = "") String successYn
        , @RequestParam(value = "uploadCount", defaultValue = "") String totalCount
        , @RequestParam(value = "successCount", defaultValue = "") String successCount
        , @RequestParam(value = "failCount", defaultValue = "") String failCount) throws Exception {
        model.addAttribute("successYn", successYn);
        model.addAttribute("uploadCount", totalCount);
        model.addAttribute("successCount", successCount);
        model.addAttribute("failCount", failCount);

        return "/common/pop/uploadResultPop";
    }

    /**
     * 쿠폰 일괄발급 처리
     * @param multipartFile
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/upload/uploadIssueCoupon.json", method = RequestMethod.POST)
    public PromoUploadResultSetDto uploadIssueCoupon(@RequestParam(value = "uploadFile") MultipartFile multipartFile
    , @RequestParam(value = "couponNo") String couponNo) throws Exception {
        PromoUploadResultSetDto result = promoUploadService.setUploadIssueCoupon(multipartFile, couponNo);

        return result;
    }

    /**
     * 기획전, 덤행사 엑셀 일괄등록 팝업창
     * @param model
     * @param templateUrl
     * @param callBackScript
     * @return
     */
    @GetMapping("/upload/excelReadPop")
    public String excelReadPop(Model model
        , @RequestParam(value = "fileType", defaultValue = "") String fileType
        , @RequestParam(value = "templateUrl", defaultValue = "") String templateUrl
        , @RequestParam(value = "callback", defaultValue = "") String callBackScript) {
        model.addAttribute("fileType", fileType);
        model.addAttribute("templateUrl", templateUrl);
        model.addAttribute("callback", callBackScript);
        return "/promotion/pop/promoExcelReadPop";
    }

    /**
     * 기획전 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/upload/promoItemExcel.json")
    public List<PromoItemExcelDto> promoItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<PromoItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder().header(0, "상품번호", "itemNo", true).build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<PromoItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(PromoItemExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 300;
        Integer successCnt = 0;

        //  3-1. 데이터 검증하여 실제 insert 할 model list 생성
        List<PromoItemExcelDto> itemNoList = new ArrayList<>();

        for (PromoItemExcelDto itemInfo : excelData) {
            // 1. 상품번호 - 빈 값 체크
            if (org.springframework.util.StringUtils.isEmpty(itemInfo.getItemNo())) {
                continue;
            }

            itemNoList.add(itemInfo);
            successCnt++;

            // 최대수량까지만
            if (successCnt > maxCnt) {
                break;
            }
        }

        return excelData;
    }

    /**
     * 덤행사 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/upload/eventItemExcel.json")
    public List<EventItemExcelDto> eventItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<EventItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품번호", "itemNo", true)
            .header(1, "전시여부", "dispYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<EventItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(EventItemExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<EventItemExcelDto> etcItemList = new ArrayList<>();
        List<String> itemNoList = new ArrayList<>();

        for (EventItemExcelDto itemInfo : excelData) {
            // 1. 상품번호 - 빈 값 체크
            if (org.springframework.util.StringUtils.isEmpty(itemInfo.getItemNo())) {
                continue;
            }

            itemNoList.add(itemInfo.getItemNo());
            etcItemList.add(itemInfo);
        }

        // 3-3. 상품정보 조회
        EventItemExcelParamDto eventExcelParam = new EventItemExcelParamDto();
        eventExcelParam.setItemNoList(itemNoList);

        List<EventItemExcelGetDto> resList = promoUploadService.getPromoItemInfoList(eventExcelParam);

        // 3-4. 조회해온 상품명 각 상품별 적용
        for (EventItemExcelDto item : etcItemList) {
            String itemNm = resList.stream().filter(i -> i.getItemNo().equals(item.getItemNo())).map(i -> i.getItemNm()).collect(Collectors.joining());

            item.setItemNm(itemNm);
        }

        return excelData;
    }

    /**
     * IDE 배치로그 상세 검색 팝업
     * @param model
     * @param batchNm
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/batchLog/detailPop")
    public String promoPop(Model model, @RequestParam(value="batchNm", defaultValue = "") String batchNm) throws Exception {
        model.addAttribute("batchNm", batchNm);

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "batch_status"           // 배치상태
        );

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("batchStatusJson", om.writeValueAsString(code.get("batch_status")));

        model.addAllAttributes(RealGridHelper.create("batchLogDetailGridBaseInfo", BatchLogListGetDto.class));

        return "/promotion/pop/batchLogDetailPop";
    }

    /**
     * IDE 배치로그 상세정보 조회
     * @param batchLogListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/batchLog/getBatchLogInfoList.json"})
    public List<BatchLogListGetDto> getBatchLogInfoList(BatchLogListParamDto batchLogListParamDto) {
        String apiUri = "/monitering/getBatchLogInfoList";

        ResourceClientRequest<List<BatchLogListGetDto>> requestClient = ResourceClientRequest.<List<BatchLogListGetDto>>postBuilder().apiId(ResourceRouteName.MANAGE)
            .uri(apiUri)
            .postObject(batchLogListParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<BatchLogListGetDto>>> responseEntity = resourceClient.post(requestClient, new TimeoutConfig());

        return responseEntity.getBody().getData();
    }
}