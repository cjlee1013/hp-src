package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerManagerSetDto {

    private String seq;
    private String mngCd;
    private String mngNm;
    private String mngMobile;
    private String mngPhone;
    private String mngEmail;
    private String useYn;

}
