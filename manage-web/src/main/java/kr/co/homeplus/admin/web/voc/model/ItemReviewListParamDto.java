package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class ItemReviewListParamDto {

    // 등록시작일
    private String schStartDt;

    // 등록종료일
    private String schEndDt;

    // 스토어타입
    private String schStoreType;

    // 적립여부
    private String schReserveYn;

    // 미노출여부
    private String schBlockYn;

    // 검색조건
    private String schType;

    // 검색어
    private String schKeyword;

    //만족도
    private String schGrade;

    // 대카테고리
    private String schLcateCd;

    // 중카테고리
    private String schMcateCd;

    // 소카테고리
    private String schScateCd;

    // 세카테고리
    private String schDcateCd;


}
