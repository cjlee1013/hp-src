package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.Optional;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//상품정보고시관리 Set Entry
public class ItemNoticeHistGetDto {

    //고시항목번호
    private String noticeNo;

    //고시항목명
    private String noticeNm;

    //정보고시항목명
    private String noticeDesc;

    //정보고시항목명 (TD용)
    private String noticeHtml;

    @Override
    public String toString() {
        return Optional.of(
                  this.noticeNo
                + this.noticeNm
                + this.noticeDesc
                + this.noticeHtml
        ).toString();
    }
}
