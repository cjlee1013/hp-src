package kr.co.homeplus.admin.web.delivery.model;

import lombok.Data;

@Data
public class RemoteShipSlotModel {
    private String shipDt;
    private String shiftId;
    private String slotId;
    private String slotSeq;
    private String slotShipStartTime;
    private String slotShipEndTime;
}
