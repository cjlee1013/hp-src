package kr.co.homeplus.admin.web.statistics.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/statistics/popup")
public class StatisticsPopupController {
    /**
     * http://jira.homeplusnet.co.kr/browse/SETTLE-542
     * 통계 메뉴 안내문구 팝업
    */

    //카테고리별 매출 (E-KPI)
    @RequestMapping(value = "/dailyDivisionEKPIPop", method = RequestMethod.GET)
    public String dailyDivisionEKPIPop(Model model) throws Exception {
        return "/statistics/pop/dailyDivisionEKPIPop";
    }

    //기간별 점포 판매 현황
    @RequestMapping(value = "/dailyStorePaymentPop", method = RequestMethod.GET)
    public String dailyStorePaymentPop(Model model) throws Exception {
        return "/statistics/pop/dailyStorePaymentPop";
    }

    //기간별 판매업체 판매 현황
    @RequestMapping(value = "/dailyPartnerPaymentPop", method = RequestMethod.GET)
    public String dailyPartnerPaymentPop(Model model) throws Exception {
        return "/statistics/pop/dailyPartnerPaymentPop";
    }

    //기간별 점포 판매 현황(E-KPI)
    @RequestMapping(value = "/storePeriodEKPIPop", method = RequestMethod.GET)
    public String storePeriodEKPIPop(Model model) throws Exception {
        return "/statistics/pop/storePeriodEKPIPop";
    }

    //기간별 판매업체 판매 현황(E-KPI)
    @RequestMapping(value = "/DSPeriodEKPIPop", method = RequestMethod.GET)
    public String DSPeriodEKPIPop(Model model) throws Exception {
        return "/statistics/pop/DSPeriodEKPIPop";
    }

    //주문당 평균 주문 상품수
    @RequestMapping(value = "/averageOrderItemQtyPop", method = RequestMethod.GET)
    public String averageOrderItemQtyPop(Model model) throws Exception {
        return "/statistics/pop/averageOrderItemQtyPop";
    }
}