package kr.co.homeplus.admin.web.user.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.user.model.refundAccount.RefundAccountDto;
import kr.co.homeplus.admin.web.user.model.refundAccount.RefundAccountGrid;
import kr.co.homeplus.admin.web.user.model.refundAccount.RefundAccountSearchParam;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 환불계좌관리 메뉴
 */
@Slf4j
@Controller
@RequestMapping("/user/refundAccount")
public class RefundAccountMainController {

    private final ResourceClient resourceClient;
    private final Map<String, Object> refundAccountGridBaseInfo = RealGridHelper
        .create("refundAccountGridBaseInfo", RefundAccountGrid.class);

    public RefundAccountMainController(ResourceClient resourceClient) {
        this.resourceClient = resourceClient;
    }

    /**
     * 환불계좌관리 Main Page
     */
    @GetMapping(value = "/main")
    public String searchUserMainController(final Model model) {
        model.addAllAttributes(refundAccountGridBaseInfo);
        return "/user/refundAccountMain";
    }

    /**
     * 환불계좌관리 목록 조회(검색)
     */
    @ResponseBody
    @GetMapping(value = "/getRefundAccountList.json")
    public ResponseObject<List<RefundAccountGrid>> getRefundAccountList(HttpServletRequest request,
        @ModelAttribute final RefundAccountSearchParam param) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.USERMNG,
            param,
            "/admin/refundAccount/getRefundAccountList",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 환불계좌관리 상세 조회
     */
    @Validated
    @ResponseBody
    @GetMapping(value = "/getRefundAccount.json")
    public ResponseObject<RefundAccountDto> getRefundAccount(HttpServletRequest request,
        @RequestParam @NotNull final Long seq) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/refundAccount/getRefundAccount?seq=" + seq,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 환불계좌관리 계좌 삭제
     */
    @Validated
    @ResponseBody
    @GetMapping(value = "/removeRefundAccount.json")
    public ResponseObject<Boolean> removeRefundAccount(HttpServletRequest request,
        @RequestParam @NotNull final Long seq) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/refundAccount/removeRefundAccount?seq=" + seq,
            new ParameterizedTypeReference<>() {
            });
    }
}
