package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ErsPromoIntervalGetDto {
    @RealGridColumnInfo(headText = "순번")
    private String lineNo;

    @RealGridColumnInfo(headText = "하한금액", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private Integer lowAmt;

    @RealGridColumnInfo(headText = "상한금액", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private Integer highAmt;

    @RealGridColumnInfo(headText = "지급유형")
    private String giveType;

    @RealGridColumnInfo(headText = "지급가능수량", columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER)
    private Integer giftQty;

    @RealGridColumnInfo(headText = "지급상품권금액", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private Integer ticketAmt;

    @RealGridColumnInfo(headText = "사은품명1", width = 100)
    private String itemNm1;

    @RealGridColumnInfo(headText = "사은품명2", width = 100)
    private String itemNm2;

    @RealGridColumnInfo(headText = "사은품명3", width = 100)
    private String itemNm3;
}