package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.gnb.GnbListParamDto;
import kr.co.homeplus.admin.web.manage.model.gnb.GnbListSelectDto;
import kr.co.homeplus.admin.web.manage.model.gnb.GnbSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/gnb")
public class GnbController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 사이트 관리 > GNB 관리 > GNB 관리
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/gnbMain", method = RequestMethod.GET)
    public String noticedMain(Model model ) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "gnb_device"        // GNB 디바이스타입
            ,	"gnb_search_type"   // GNB 검색어 구분
            ,   "gnb_site_type"     // GNB 사이트구분
            ,   "gnb_link_type"     // GNB 링크타입
            ,   "use_yn"            // 사용여부
        );

        model.addAttribute("searchDevice", code.get("gnb_device"));
        model.addAttribute("searchType", code.get("gnb_search_type"));
        model.addAttribute("searchSite", code.get("gnb_site_type"));
        model.addAttribute("searchLinkType", code.get("gnb_link_type"));
        model.addAttribute("searchUseYn", code.get("use_yn"));

        model.addAllAttributes(RealGridHelper.create("gnbListGridBaseInfo", GnbListSelectDto.class));

        return "/manage/gnbMain";
    }

    /**
     * 사이트 관리 > GNB 관리 >  GNB 리스트
     *
     * @param listParamDto
     * @return GnbListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getGnbList.json"}, method = RequestMethod.GET)
    public List<GnbListSelectDto> getGnbList(GnbListParamDto listParamDto) {
        String apiUri = "/manage/gnb/getGnbList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<GnbListSelectDto>>>() {};
        return (List<GnbListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, GnbListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 사이트관 > GNB관라 >  GNB 등록/수정
     *
     * @param gnbSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setGnb.json"}, method = RequestMethod.POST)
    public ResponseResult setGnb(@RequestBody GnbSetParamDto gnbSetParamDto) {
        String apiUri = "/manage/gnb/setGnb";
        gnbSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, gnbSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
