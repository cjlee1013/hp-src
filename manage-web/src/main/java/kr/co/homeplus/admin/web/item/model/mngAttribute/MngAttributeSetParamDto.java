package kr.co.homeplus.admin.web.item.model.mngAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MngAttributeSetParamDto {
    private Long attrNo;
    private Long mGattrNo;
    private String attrNm;
    private String dispType;
    private Integer priority;
    private String attrUseYn;
    private String imgUrl;
    private String imgNm;
    private String imgWidth;
    private String imgHeight;
    private String colorType;
    private String colorCode;
    private String regId;
}
