package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class RecipientDetailInfo {

    private String subject;

    private String body;

    private String bodyArgument;
}
