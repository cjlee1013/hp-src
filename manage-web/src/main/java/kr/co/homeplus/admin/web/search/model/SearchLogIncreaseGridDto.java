package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class SearchLogIncreaseGridDto {
    @Getter
    @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class SearchLogIncrease{
        @RealGridColumnInfo(headText = "기간", width = 49, columnType = RealGridColumnType.BASIC, sortable = true)
        private String range;
        @RealGridColumnInfo(headText = "지난범위", width = 67, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true, fieldType = RealGridFieldType.NUMBER)
        private Long pastCount;
        @RealGridColumnInfo(headText = "현재범위", width = 67, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true, fieldType = RealGridFieldType.NUMBER)
        private Long currentCount;
        @RealGridColumnInfo(headText = "증감률", width = 47, columnType = RealGridColumnType.BASIC, sortable = true)
        private String increaseRate;
    }
}
