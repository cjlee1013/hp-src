package kr.co.homeplus.admin.web.core.certification.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import kr.co.homeplus.admin.web.core.certification.SessionService;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 세션관련 기능을 제공하는 서비스의 구현체
 */
@Slf4j
@Service(value = "SessionService")
public class SessionServiceImpl implements SessionService {

    private final UserMenuService userMenuService;
    public SessionServiceImpl(UserMenuService userMenuService) {
        this.userMenuService = userMenuService;
    }

    @Override
    public boolean isCertification() throws Exception {
        HttpSession httpSession = this.getRequest().getSession();

        if (null != httpSession) {
            if (null != httpSession.getAttribute("admin.create") && null != httpSession.getAttribute("admin.expire")
                    && null != httpSession.getAttribute(AdminConstants.ADMIN_USER_TOKEN)) {
                Date current = new Date();
                Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String) httpSession.getAttribute("admin.create"));
                Date end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String) httpSession.getAttribute("admin.expire"));

                // 발행한 세션의 유효기간 확인
                if (current.after(start) && current.before(end)) {
                    this.confirmAmsMenuList((UserInfo) httpSession.getAttribute(AdminConstants.ADMIN_USER_TOKEN));
                    return true;
                }
            }

            return false;
        }

        return false;
    }


    private void setAmsMenuList(List<AuthMenuListDto> params) throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        httpSession.setAttribute("admin.ams.menu", params);
    }

    @Override
    public boolean setCertification(String rmsToken) throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        httpSession.setAttribute("admin.token", rmsToken);

        return true;
    }

    @Override
    public boolean setCertification(String rmsToken, String sessionKey) throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        httpSession.setAttribute(sessionKey, rmsToken);

        return true;
    }

    @Override
    public boolean setCertification(Object object, String sessionKey) throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        httpSession.setAttribute(sessionKey, object);

        return true;
    }

    @Override
    public String getCertification() {
        HttpSession httpSession = this.getRequest().getSession();
        String httpSessionResult = (String) httpSession.getAttribute("admin.token");
        return httpSessionResult;
    }

    @Override
    public List<AuthMenuListDto> getAmsMenuList() throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        List<AuthMenuListDto> menuList = (List<AuthMenuListDto>) httpSession.getAttribute("admin.ams.menu");

        return menuList;
    }

    @Override
    public UserInfo getUserInfo() throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        UserInfo userInfo = (UserInfo) httpSession.getAttribute(AdminConstants.ADMIN_USER_TOKEN);
        return userInfo;
    }

    @Override
    public boolean removeCertification() throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        httpSession.removeAttribute("admin.create");
        httpSession.removeAttribute("admin.expire");
        httpSession.removeAttribute("admin.ams.menu");
        httpSession.removeAttribute(AdminConstants.ADMIN_USER_TOKEN);

        return true;
    }

    @Override
    public void confirmAmsMenuList(UserInfo userInfo) throws Exception {
        HttpSession httpSession = this.getRequest().getSession();
        if (null == httpSession.getAttribute("admin.ams.menu")) {
            this.setAmsMenuList(userMenuService.getUserPermitMenuList(userInfo));
        }

    }

    /**
     * @return
     * @brief HttpServletRequest를 리턴
     * @details
     */
    private HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }
}
