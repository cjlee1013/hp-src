package kr.co.homeplus.admin.web.message.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.model.MailCancelRequest;
import kr.co.homeplus.admin.web.message.model.MailHistoryDetailRequest;
import kr.co.homeplus.admin.web.message.model.MailHistoryListRequest;
import kr.co.homeplus.admin.web.message.model.MailSendListGrid;
import kr.co.homeplus.admin.web.message.model.MailWorkDetail;
import kr.co.homeplus.admin.web.message.model.MailWorkGrid;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "메일 어드민용 컨트롤러")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequestMapping("/message/admin/mail")
@RequiredArgsConstructor
public class MailAdminController {

    private final CodeService codeService;
    private final ResourceClient resourceClient;

    private final Map<String, Object> modelMap = RealGridHelper
        .create("mailWorkGridBaseInfo", MailWorkGrid.class);
    private final Map<String, Object> mailSendListGridBaseInfoMap = RealGridHelper
        .create("mailSendListGridBaseInfo", MailSendListGrid.class);

    @ApiOperation(value = "메일 발송 main 화면")
    @ApiResponse(code = 200, message = "메일 발송 main 화면")
    @GetMapping("/history/main")
    public String main(final Model model) {
        codeService.getCodeModel(model, "message_work_status");
        model.addAllAttributes(modelMap);
        model.addAllAttributes(mailSendListGridBaseInfoMap);

        return "/message/mailMain";
    }

    @ApiOperation(value = "메일 발송 이력 목록 조회")
    @ApiResponse(code = 200, message = "메일 발송 이력 목록")
    @PostMapping("/history/getWorks.json")
    @ResponseBody
    public ResponseObject<List<MailWorkGrid>> historyList(
        @ApiParam(value = "메일 발송 이력 조회 조건", required = true) @Valid final MailHistoryListRequest mailHistoryListRequest) {

        ResourceClientRequest<List<MailWorkGrid>> resourceClientRequest = ResourceClientRequest.<List<MailWorkGrid>>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/admin/mail/history/list")
            .postObject(mailHistoryListRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<List<MailWorkGrid>>> responseEntity = resourceClient
            .post(resourceClientRequest, new TimeoutConfig());
        return responseEntity.getBody();
    }

    @ApiOperation(value = "메일 발송 상세 조회")
    @ApiResponse(code = 200, message = "메일 발송 상세")
    @GetMapping("/history/getDetail.json")
    @ResponseBody
    public ResponseObject<MailWorkDetail> historyDetail(
        @ApiParam(value = "메일 발송 상세 조회 조건", required = true) @Valid final MailHistoryDetailRequest mailHistoryDetailRequest) {

        ResourceClientRequest<MailWorkDetail> resourceClientRequest = ResourceClientRequest.<MailWorkDetail>postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/admin/mail/history/detail")
            .postObject(mailHistoryDetailRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<MailWorkDetail>> responseEntity = resourceClient
            .post(resourceClientRequest, new TimeoutConfig());
        return responseEntity.getBody();
    }

    @ApiOperation(value = "메일 발송 취소")
    @ApiResponse(code = 200, message = "메일 발송 취소 성공 여부")
    @PostMapping("/history/cancel.json")
    @ResponseBody
    public ResponseObject<Object> cancelOne(
        @ApiParam(value = "메일 발송 취소 조건", required = true) @Valid final MailCancelRequest mailCancelRequest) {


        ResourceClientRequest<Object> resourceClientRequest = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.MESSAGE)
            .uri("/admin/mail/history/cancel")
            .postObject(mailCancelRequest)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseEntity<ResponseObject<Object>> responseEntity = resourceClient
            .post(resourceClientRequest, new TimeoutConfig());

        return ResponseObject.Builder.<Object>builder().data(responseEntity.getBody()).build();
    }
}
