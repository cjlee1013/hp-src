package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "대체키워드")
public class ReplacedKeywordSetParamDto {
    @ApiModelProperty(notes = "일련번호", position = 0)
    private Long replacedId;
    @ApiModelProperty(notes = "원본키워드", position = 1, required = true)
    private String keyword;
    @ApiModelProperty(notes = "대체키워드", position = 2, required = true)
    private String replacedKeyword;
    @ApiModelProperty(notes = "사용여부", position = 3, required = true)
    private char useFlag;
    @ApiModelProperty(notes = "사용여부(한글)", position = 3, hidden = true)
    private String useFlagNm;
    @ApiModelProperty(notes = "시작일", position = 4, required = true)
    private String sdate;
    @ApiModelProperty(notes = "종료일", position = 5, required = true)
    private String edate;
    @ApiModelProperty(notes = "생성/수정일", position = 6, hidden = true)
    private String updateDt;
    @ApiModelProperty(notes = "사용기간 설정여부", position = 7, required = true)
    private String operationPeriodYn;

    private String regDt;
    private String creator;
    private String modifier;

    private String administratorId;
}
