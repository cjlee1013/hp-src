package kr.co.homeplus.admin.web.claim.service;

import java.util.List;
import kr.co.homeplus.admin.web.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.admin.web.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.admin.web.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OutOfStockItemService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 결품/대체상품 리스트 조회
     * **/
    public List<OutOfStockItemListGetDto> getOutOfStockItemList(
        OutOfStockItemListSetDto outOfStockItemListSetDto) {

        //점포코드 추출
        //데이터가 있으면 점포사용자 없으면 본사 사용자로 구분
        if(!StringUtil.isEmpty(loginCookieService.getUserInfo().getStoreId())){
            outOfStockItemListSetDto.setSchStoreId(loginCookieService.getUserInfo().getStoreId());
        }

        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            outOfStockItemListSetDto,
            "/claim/outOfStock/getOutOfStockItemList",
            new ParameterizedTypeReference<ResponseObject<List<OutOfStockItemListGetDto>>>() {}
        ).getData();
    }

    /**
     * 점포 shift 리스트 조회
     * **/
    public List<String> getStoreShiftList(StoreShiftListSetDto storeShiftListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            storeShiftListSetDto,
            "/claim/outOfStock/getStoreShiftList",
            new ParameterizedTypeReference<ResponseObject<List<String>>>() {}
        ).getData();
    }


}
