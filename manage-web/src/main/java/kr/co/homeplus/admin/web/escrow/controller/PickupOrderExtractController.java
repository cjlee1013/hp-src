package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.escrow.enums.ExtractMenuName;
import kr.co.homeplus.admin.web.escrow.enums.ExtractType;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.escrow.model.extract.ExtractPickupOrderDto;
import kr.co.homeplus.admin.web.escrow.model.extract.OrderDataExtractSelectDto;
import kr.co.homeplus.admin.web.escrow.service.OrderDataExtractService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 점포상품통계 > 온라인 픽업 주문 현황
 */
@Controller
@RequestMapping("/escrow/extract")
public class PickupOrderExtractController {
    private final OrderDataExtractService orderDataExtractService;
    private final LoginCookieService loginCookieService;

    public PickupOrderExtractController(OrderDataExtractService orderDataExtractService, LoginCookieService loginCookieService) {
        this.orderDataExtractService = orderDataExtractService;
        this.loginCookieService = loginCookieService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/pickupOrderExtractMain")
    public String pickupOrderExtractMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("pickupOrderExtractGridBaseInfo", ExtractPickupOrderDto.class));

        return "/escrow/pickupOrderExtractMain";
    }

    /**
     * 픽업주문 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getPickupOrderList.json")
    public List<ExtractPickupOrderDto> getPickupOrderList(@ModelAttribute OrderDataExtractSelectDto orderDataExtractSelectDto) throws Exception {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.PICKUP_ORDER_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.SEARCH.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractSelectDto.setExtractCommonSetDto(extractCommonSetDto);
        return orderDataExtractService.getPickupOrderList(orderDataExtractSelectDto);
    }

    /**
     * 추출이력 저장
     */
    @ResponseBody
    @RequestMapping("/savePickupOrderExtractHistory.json")
    public void saveExtractHistory() {
        ExtractCommonSetDto extractCommonSetDto = new ExtractCommonSetDto();
        extractCommonSetDto.setEmpId(loginCookieService.getUserInfo().getEmpId());
        extractCommonSetDto.setMenuNm(ExtractMenuName.PICKUP_ORDER_EXTRACT.getMenuNm());
        extractCommonSetDto.setExtractType(ExtractType.EXCEL_DOWN.name());
        extractCommonSetDto.setExtractDt(DateTimeUtil.getNowYmdHis());
        orderDataExtractService.saveExtractHistory(extractCommonSetDto);
    }
}
