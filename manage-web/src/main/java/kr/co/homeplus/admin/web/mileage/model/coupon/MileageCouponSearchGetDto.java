package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MileageCouponSearchGetDto {
    @ApiModelProperty(value = "번호", position = 1)
    @RealGridColumnInfo(headText = "번호", width = 50)
    private String couponManageNo;

    @ApiModelProperty(value = "제목", position = 2)
    @RealGridColumnInfo(headText = "제목", width = 200)
    private String couponNm;

    @ApiModelProperty(value = "난수 발급기간", position = 3)
    @RealGridColumnInfo(headText = "난수 발급기간", width = 250)
    private String issueDt;

    @ApiModelProperty(value = "실행", position = 4)
    @RealGridColumnInfo(headText = "실행")
    private String issueYn;

    @ApiModelProperty(value = "난수종류", position = 5)
    @RealGridColumnInfo(headText = "난수종류")
    private String couponType;

    @ApiModelProperty(value = "발급수량", position = 6)
    @RealGridColumnInfo(headText = "발급수량")
    private String issueQty;

    @ApiModelProperty(value = "지급마일리지", position = 7)
    @RealGridColumnInfo(headText = "지급마일리지", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE)
    private String mileageAmt;

    @ApiModelProperty(value = "마일리지 유형", position = 8)
    @RealGridColumnInfo(headText = "마일리지 유형", width = 150)
    private String mileageTypeName;

    @ApiModelProperty(value = "등록일", position = 9)
    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String regDt;

    @ApiModelProperty(value = "등록자", position = 10)
    @RealGridColumnInfo(headText = "등록자")
    private String regId;

    @ApiModelProperty(value = "수정일", position = 11)
    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 150)
    private String chgDt;

    @ApiModelProperty(value = "수정자", position = 12)
    @RealGridColumnInfo(headText = "수정자")
    private String chgId;
}
