package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "점포정보 응답 DTO")
public class ShipStoreInfoGetDto {
  @ApiModelProperty(notes = "점포명")
  private String storeNm;

  @ApiModelProperty(notes = "점포유형")
  private String storeType;
}