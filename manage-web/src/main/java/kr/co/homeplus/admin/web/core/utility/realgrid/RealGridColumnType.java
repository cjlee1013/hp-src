package kr.co.homeplus.admin.web.core.utility.realgrid;

@Deprecated(forRemoval = true)
public enum RealGridColumnType {
    NONE, // 없음
    BASIC, // 기본
    NAME, // 명칭
    NUMBER, // 숫자
    NUMBER_C, // 숫자(콤마)
    NUMBER_CENTER, // 숫자(콤마, 가운데정렬)
    NUMBER_STR, // 숫자형 문자
    SERIAL, // 일련번호형식(중앙정렬, 콤마없음)
    PRICE, // 금액
    COMMISSION, // 수수료
    COMMISSION_C, // 수수료(중앙정렬)
    DATE, // YYYY-MM-DD
    DATETIME, // YYYY-MM-DD HH:mm:ss
    IMAGE, // 이미지
    BUTTON, // 버튼
}
