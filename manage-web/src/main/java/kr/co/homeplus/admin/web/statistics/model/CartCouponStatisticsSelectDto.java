package kr.co.homeplus.admin.web.statistics.model;

import lombok.Data;

@Data
public class CartCouponStatisticsSelectDto {

    private String schFromDt;
    private String schEndDt;
    private String schStoreType;
    private String schCouponKind;
    private String schStoreId;
}
