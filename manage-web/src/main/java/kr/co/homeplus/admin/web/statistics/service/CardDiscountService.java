package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.statistics.model.DiscountStatistics;
import kr.co.homeplus.admin.web.statistics.model.DiscountStatisticsSelect;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardDiscountService {

    private final ResourceClient resourceClient;

    public List<DiscountStatistics> getCardDiscountList(DiscountStatisticsSelect discountStatisticsSelect) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_CARD_DISCOUNT_STATISTICS_LIST, DiscountStatisticsSelect.class, discountStatisticsSelect);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DiscountStatistics>>>() {};
        List<DiscountStatistics> discountStatisticsList = (List<DiscountStatistics>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG
            , discountStatisticsSelect
            , apiUri
            , typeReference).getData();

        return discountStatisticsList;
    }

    public List<DiscountStatistics> getCardDiscountDetailList(DiscountStatisticsSelect discountStatisticsSelect) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_CARD_DISCOUNT_STATISTICS_DETAIL_LIST, DiscountStatisticsSelect.class, discountStatisticsSelect);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<DiscountStatistics>>>() {};
        List<DiscountStatistics> discountStatisticsList = (List<DiscountStatistics>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG
            , discountStatisticsSelect
            , apiUri
            , typeReference).getData();

        return discountStatisticsList;
    }
}
