package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryDetailGetDto {

    //최하위카테고리
    private String marketCateCd;

    //대카테고리명
    private String marketLcateCd;

    //중카테고리명
    private String marketMcateCd;

    //소카테고리명
    private String marketScateCd;

    //세카테고리명
    private String marketDcateCd;

    //판매자 ID
    private String partnerId;


}
