package kr.co.homeplus.admin.web.statistics.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GradeCouponStatisticsListSetDto {

    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;
}
