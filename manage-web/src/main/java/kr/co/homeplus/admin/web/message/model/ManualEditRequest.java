package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import kr.co.homeplus.admin.web.message.enums.WorkStatus;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import lombok.Data;

@Data
public class ManualEditRequest {

    private long manualSendSeq;

    private long manualSendWorkSeq;

    private long coupledWorkSeq;

    private String coupledWorkName;

    private String description;

    private WorkStatus workStatus;

    private List<BasicRecipient> bodyArgument;

    private WorkType workType;

    private String chgId;

    private String workDt;
}
