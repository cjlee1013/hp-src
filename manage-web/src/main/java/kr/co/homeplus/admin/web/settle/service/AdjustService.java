package kr.co.homeplus.admin.web.settle.service;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustItemGetDto;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustCode;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustListSetDto;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustRegisterGetDto;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Service
public class AdjustService {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final ExcelUploadService excelUploadService;

    //사유항목 최대 길이
    private static final int COMMENT_MAX_LENGTH = 100;

    public AdjustService(ResourceClient resourceClient, LoginCookieService loginCookieService, ExcelUploadService excelUploadService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.excelUploadService = excelUploadService;
    }

    //임의조정 관리 조회
    public List<AdjustListGetDto> getAdjustList(AdjustListSetDto listParamDto) throws Exception{
        List<AdjustListGetDto> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE
            , listParamDto
            , "/admin/adjust/getAdjustList",
            new ParameterizedTypeReference<ResponseObject<List<AdjustListGetDto>>>(){}).getData();

        return responseData;
    }

    //아아템 조회
    public ResponseObject<AdjustItemGetDto> getAdjustItem(@RequestParam(name ="itemNo") String itemNo) throws Exception {
        ResponseObject<AdjustItemGetDto> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            "",
            "/admin/adjust/getAdjustItem?itemNo="+ itemNo,
            new ParameterizedTypeReference<ResponseObject<AdjustItemGetDto>>() {}
        );
        return responseData;
    }

    //임의조정 관리 금액등록
    public ResponseObject<String> insertAdjust(AdjustRegisterGetDto adjustRegisterGetDto) throws Exception{
        adjustRegisterGetDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<String> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            adjustRegisterGetDto,
            "/admin/adjust/insertAdjust",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );

        return responseData;
    }


    //임의조정 관리 엑셀업로드
    public List<AdjustRegisterGetDto> uploadExcel(HttpServletRequest httpServletRequest) throws Exception {
       List<AdjustRegisterGetDto> excelResultList; // 엑셀에 넣을 결과값 list
        String fileName = "uploadFile";
        final MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;
        final MultipartFile multipartFile = multipartHttpServletRequest.getFile(fileName);

        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "업체코드", "vendorCd", false) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
            .header(1, "상품번호", "itemNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
            .header(2, "유형","adjustType",  true)
            .header(3, "조정금액","adjustAmt",  true)
            .header(4, "조정수수료","adjustFee",  false)
            .header(5, "설명","comment",  false)
            .header(6, "별도정산","separateYn",  true)
            .build();

        ExcelUploadOption<AdjustRegisterGetDto> excelUploadOption = new ExcelUploadOption.Builder<>(
            AdjustRegisterGetDto.class, headers, 1, 0)
            .maxRows(1000)
            .build();

        boolean isValid = false;
        try {
            String regId = loginCookieService.getUserInfo().getEmpId();

            excelResultList = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
            HashMap<Integer,String> adjustTaxCode = AdjustCode.getAdjustTaxCode();

            for (AdjustRegisterGetDto adjust : excelResultList) {
                isValid = true;

                if (StringUtils.isBlank(adjust.getAdjustType())) {
                    adjust.setReason("조정유형을 입력하시기 바랍니다. ");
                    isValid = false;
                } else {
                    adjust.setAdjustType(adjust.getAdjustType().trim());
                    try {
                        if (!adjustTaxCode.containsKey(Integer.parseInt(adjust.getAdjustType()))) {
                            adjust.setReason("유형 코드값을 확인하시기 바랍니다. ");
                            isValid = false;
                        }

                        if (adjustTaxCode.containsKey(Integer.parseInt(adjust.getAdjustType()))) { //세금계산서 대상
                            if (StringUtils.isBlank(adjust.getAdjustFee())) {
                                adjust.setReason("조정수수료를 입력하시기 바랍니다. ");
                                isValid = false;
                            }

                        } else {
                            if (StringUtils.isBlank(adjust.getAdjustAmt())) {
                                adjust.setReason("조정금액을 입력하시기 바랍니다. ");
                                isValid = false;
                            }
                        }

                    } catch (Exception e) {
                        adjust.setReason("유형 코드값을 확인하시기 바랍니다. ");
                        isValid = false;
                    }
                }

                if (StringUtils.isBlank(adjust.getItemNo())) {
                    adjust.setReason("상품번호를 입력하시기 바랍니다. ");
                    isValid = false;
                }
                adjust.setItemNo(adjust.getItemNo().trim());

                if (StringUtils.isBlank(adjust.getComment())) {
                    adjust.setComment("");
                }

                if (adjust.getComment().length() > COMMENT_MAX_LENGTH) {
                    adjust.setReason("설명 글자수 100자 제한 오류");
                    isValid = false;
                }

                if (StringUtils.isBlank(adjust.getSeparateYn())) {
                    adjust.setReason("별도정산 여부를 입력해주세요 (Y/N)");
                    isValid = false;
                }
                adjust.setSeparateYn(adjust.getSeparateYn().trim());

                if (!StringUtils.isBlank(adjust.getVendorCd())) {
                    adjust.setVendorCd(adjust.getVendorCd().trim());
                }
                adjust.setVendorCd(adjust.getVendorCd().trim());

                if (isValid) {
                    adjust.setRegId(regId);

                    ResponseObject<String> responseData = insertAdjust(adjust);
                    adjust.setReason(responseData.getReturnMessage());
                }
            }
        } catch (Exception e) {
            excelResultList = new ArrayList<>();
            AdjustRegisterGetDto dto = new AdjustRegisterGetDto();
            if(!isValid) {
                dto.setReason("파일을 읽는중 오류가 발생했습니다. 양식에 맞는 파일을 사용 바랍니다.");
            }
            excelResultList.add(dto);
        }

        List<AdjustRegisterGetDto> excelList = new Gson().fromJson( new Gson().toJson(excelResultList), new TypeToken<List<AdjustRegisterGetDto>>(){}.getType() );

        return excelList;
    }

    //임의조정 관리 금액 삭제
    public ResponseObject<String> deleteAdjust(AdjustListGetDto adjustListDto) throws Exception{
        adjustListDto.setRegId(loginCookieService.getUserInfo().getEmpId());

        ResponseObject<String> responseData = resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            adjustListDto,
            "/admin/adjust/deleteAdjust",
            new ParameterizedTypeReference<ResponseObject<String>>() {}
        );

        return responseData;
    }
}
