package kr.co.homeplus.admin.web.item.model.expCategoryItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ExpCategoryItemGetDto {

    @RealGridColumnInfo(headText = "카테고리 ID", width = 120)
    private String mcateCd;

    @RealGridColumnInfo(headText = "1depth", width = 250, order = 1)
    private String lcateNm;

    @RealGridColumnInfo(headText = "2depth", width = 250, order = 2)
    private String mcateNm;

    @RealGridColumnInfo(headText = "상품번호", width = 120, order = 3)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 250, order = 4)
    private String itemNm;

    @RealGridColumnInfo(headText = "매핑여부", order = 5)
    private String statusNm;

    @RealGridColumnInfo(headText = "등록자", order = 6)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 130, order = 7)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 8)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 130, order = 9)
    private String chgDt;

    @RealGridColumnInfo(headText = "lcateCd", hidden = true)
    private String lcateCd;
    @RealGridColumnInfo(headText = "status", hidden = true)
    private String status;

    private String regId;
    private String chgId;

}
