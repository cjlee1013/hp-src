package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 히스토리 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderSearchHistoryDto {

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시", width = 80)
    private String historyDt;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 60)
    private long bundleNo;

    @ApiModelProperty(value = "처리구분")
    @RealGridColumnInfo(headText = "처리구분", width = 80)
    private String historyDetailType;

    @ApiModelProperty(value = "상세내역")
    @RealGridColumnInfo(headText = "상세내역", width = 200, columnType = RealGridColumnType.NONE)
    private String historyDetailDesc;

    @ApiModelProperty(value = "처리자")
    @RealGridColumnInfo(headText = "처리자", width = 80)
    private String regId;

}
