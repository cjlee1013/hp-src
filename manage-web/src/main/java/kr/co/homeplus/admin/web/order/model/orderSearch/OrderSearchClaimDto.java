package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 클레임정보 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderSearchClaimDto {

    @ApiModelProperty(value = "클레임구분코드")
    @RealGridColumnInfo(headText = "클레임구분코드", hidden = true)
    private String claimType;

    @ApiModelProperty(value = "클레임구분")
    @RealGridColumnInfo(headText = "클레임구분")
    private String claimTypeNm;

    @ApiModelProperty(value = "그룹 클레임번호")
    @RealGridColumnInfo(headText = "그룹 클레임번호")
    private long claimNo;

    @ApiModelProperty(value = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", hidden = true)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "클레임번호")
    @RealGridColumnInfo(headText = "클레임번호")
    private long claimBundleNo;

    @ApiModelProperty(value = "클레임상태")
    @RealGridColumnInfo(headText = "클레임상태")
    private String claimStatus;

    @ApiModelProperty(value = "배송번호")
    @RealGridColumnInfo(headText = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "상품번호")
    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    @RealGridColumnInfo(headText = "상품명", columnType = RealGridColumnType.NONE)
    private String itemName;

    @ApiModelProperty(value = "신청수량")
    @RealGridColumnInfo(headText = "신청수량", columnType = RealGridColumnType.NUMBER_CENTER)
    private int claimItemQty;

    @ApiModelProperty(value = "신청일시")
    @RealGridColumnInfo(headText = "신청일시")
    private String regDt;

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시")
    private String chgDt;

}
