package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@ApiModel(description = "결제관리 > 결제수단관리 > 결제수단별 혜택관리 DTO")
@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentMethodBenefitManageDto {
    @ApiModelProperty(notes = "혜택관리 순번", required = true)
    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long paymentMethodBenefitManageSeq;

    @ApiModelProperty(notes = "사이트유형", required = true)
    @RealGridColumnInfo(headText = "사이트", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;

    @ApiModelProperty(notes = "결제수단")
    @RealGridColumnInfo(headText = "결제수단", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodCd;

    @ApiModelProperty(notes = "제목", required = true)
    @RealGridColumnInfo(headText = "제목", width = 150, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String benefitTitle;

    @RealGridColumnInfo(headText = "노출기간", width = 150, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyDt = this.getApplyDt();

    @ApiModelProperty(notes = "노출 시작기간")
    @RealGridColumnInfo(headText = "노출 시작기간", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyStartDt;

    @ApiModelProperty(notes = "노출 종료기간")
    @RealGridColumnInfo(headText = "노출 종료기간", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyEndDt;

    @ApiModelProperty(notes = "혜택안내문구1")
    @RealGridColumnInfo(headText = "혜택안내문구1", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String benefitMessage1;

    @ApiModelProperty(notes = "혜택안내문구2")
    @RealGridColumnInfo(headText = "혜택안내문구2", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String benefitMessage2;

    @ApiModelProperty(notes = "혜택안내문구3")
    @RealGridColumnInfo(headText = "혜택안내문구3", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String benefitMessage3;

    @ApiModelProperty(notes = "사용여부(Y:사용,N:미사용)", required = true)
    @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(notes = "등록자ID", required = true)
    @RealGridColumnInfo(headText = "등록자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(notes = "등록일자", required = true, hidden = true)
    @RealGridColumnInfo(headText = "등록일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;

    @ApiModelProperty(notes = "수정자ID", required = true)
    @RealGridColumnInfo(headText = "수정자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(notes = "수정일자", required = true, hidden = true)
    @RealGridColumnInfo(headText = "수정일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;

    public String getApplyDt() {
        return this.applyStartDt + "~" + this.applyEndDt;
    }
}
