package kr.co.homeplus.admin.web.order.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderNoRcvCode {

    NO_RCV_PROCESS_TYPE_COMPLETE("C", "미수취신고철회요청"),
    NO_RCV_PROCESS_TYPE_WITHDRAW("W", "미수취신고철회"),

    NO_RCV_DECLARE_TYPE_DELIVERY("NA", "상품이 아직 도착하지 않았어요."),
    NO_RCV_DECLARE_TYPE_ROBBERY("NR", "도난이 의심됩니다."),
    NO_RCV_DECLARE_TYPE_TRACKING("NT", "배송 조회가 되지 않습니다."),
    NO_RCV_DECLARE_TYPE_ETC("NE", "기타");

    @Getter
    private final String code;

    @Getter
    private final String name;
}
