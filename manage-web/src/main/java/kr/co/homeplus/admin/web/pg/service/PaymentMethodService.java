package kr.co.homeplus.admin.web.pg.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.GridUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodSelectDto;
import kr.co.homeplus.admin.web.pg.model.PaymentTypeModel;
import kr.co.homeplus.admin.web.pg.model.SiteType;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentMethodService {
    private final ResourceClient resourceClient;

    /**
     * 결제수단 조회
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> getPaymentMethod(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_METHOD, PaymentMethodSelectDto.class, paymentMethodSelectDto);
        ResponseObject<List<PaymentMethodDto>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.ESCROWMNG, apiUri,
            new ParameterizedTypeReference<>() {
            }
        );

        List<PaymentMethodDto> newResults = new ArrayList<>();
        for(PaymentMethodDto paymentMethod : responseObject.getData()){
            paymentMethod.setParentMethodNm(ParentMethodCd.getPaymentMethod(paymentMethod.getParentMethodCd()).getMethodNm());
            newResults.add(paymentMethod);
        }
        return newResults;
    }

    /**
     * 결제수단코드 중복체크
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<PaymentMethodDto> checkDuplicateCode(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_CHECK_DUPLICATE_CODE, PaymentMethodSelectDto.class, paymentMethodSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PaymentMethodDto>>>() {};

        return (List<PaymentMethodDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 결제수단 저장
     * @param paymentMethodDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> savePaymentMethod(PaymentMethodDto paymentMethodDto) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_PAYMENT_METHOD;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};

        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, paymentMethodDto, apiUri, typeReference);
    }

    /**
     * 결제수단 Tree 구조 조회
     * @param paymentMethodSelectDto
     * @return
     * @throws Exception
     */
    public List<Object> getPaymentMethodTree(PaymentMethodSelectDto paymentMethodSelectDto) throws Exception {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_METHOD_TREE, PaymentMethodSelectDto.class, paymentMethodSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {};
        ResponseObject<List<Map<String, Object>>> responseObject = resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference);
        List<Map<String, Object>> responseData = responseObject.getData();

        return GridUtil.convertTreeGridData(responseData, null, "methodCode", "parentCode", "depth");
    }

    /**
     * 상위결제수단 조회
     * @return
     * @throws Exception
     */
    public List<PaymentTypeModel> getPaymentTypeModelList() throws Exception {
        // 공통코드에서 site_type 조회 -> ENUM 값으로 변경
        List<PaymentTypeModel> paymentTypeModelList = new ArrayList<>();
        for(SiteType siteType : SiteType.values()) {
            for(ParentMethodCd parentMethodCd : ParentMethodCd.getBasicParentMethodList()){
                PaymentTypeModel paymentTypeModel = PaymentTypeModel.create(parentMethodCd);
                paymentTypeModel.setSiteType(siteType.getCode());
                paymentTypeModel.setSiteNm(siteType.getName());
                paymentTypeModelList.add(paymentTypeModel);
            }
        }
        return paymentTypeModelList;
    }
}
