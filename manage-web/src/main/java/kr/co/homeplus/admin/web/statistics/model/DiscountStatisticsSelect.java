package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "할인통계조회 DTO")
@Getter
@Setter
public class DiscountStatisticsSelect {

    private String schStartDt;

    private String schEndDt;

    private String schSiteTypeExtend;

    private String schMallType;

    private Long schDiscountNo;

    private String schDiscountNm;

    private Long schDetailDiscountNo;

    /* 추출 히스토리 저장용 DTO */
    ExtractCommonSetDto extractCommonSetDto;
}
