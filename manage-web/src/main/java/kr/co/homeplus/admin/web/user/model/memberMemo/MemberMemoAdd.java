package kr.co.homeplus.admin.web.user.model.memberMemo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel("회원메모 등록 파라미터")
public class MemberMemoAdd {
    @NotNull(message = "회원일련번호가 없습니다.")
    @ApiModelProperty("회원일련번호")
    private Long userNo;
    @NotNull(message = "회원메모가 없습니다.")
    @ApiModelProperty("회원메모")
    private String content;

    @ApiModelProperty("등록자아이디")
    private String regEmpId;
    @ApiModelProperty("등록자명")
    private String regEmpNm;
}
