package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "어휘속성 > 복합명사 구성 정보")
public class DictionaryInsertCompoundsSetParamDto {
    @ApiModelProperty(notes = "구성리스트", position = 1, required = true)
    private List<CompoundDto> list;
    private String userId;
}