package kr.co.homeplus.admin.web.promotion.model.coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponFileSetDto {
    private Long couponNo;
    private Long couponFileNo;
    private String empNo;
    private String fileNm;
    private String fileUrl;
}