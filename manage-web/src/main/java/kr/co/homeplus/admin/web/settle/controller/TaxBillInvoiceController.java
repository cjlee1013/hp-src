package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillInvoiceListGetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillInvoiceListSetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveGetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveMarketGetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveSetDto;
import kr.co.homeplus.admin.web.settle.model.taxBillInvoice.TaxBillReceiveSupplierGetDto;
import kr.co.homeplus.admin.web.settle.service.TaxBillInvoiceService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/taxBillInvoice")
public class TaxBillInvoiceController {

    private final TaxBillInvoiceService TaxBillInvoiceService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public TaxBillInvoiceController(TaxBillInvoiceService TaxBillInvoiceService, SettleCommonService settleCommonService, CodeService codeService) {
        this.TaxBillInvoiceService = TaxBillInvoiceService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 세금계산서관리 > 발행내역 관리 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/taxBillInvoiceMain", method = RequestMethod.GET)
    public String taxBillInvoiceMain(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("taxBillInvoiceListBaseInfo", TaxBillInvoiceListGetDto.class));

        return "/settle/taxBillInvoiceList";
    }

    /**
     * 세금계산서관리 > 메인 리스트 조회
     * @return TaxBillInvoiceListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getTaxBillInvoiceList.json", method = RequestMethod.GET)
    public List<TaxBillInvoiceListGetDto> getTaxBillInvoiceList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.getTaxBillInvoiceList(listParamDto);
    }

    /**
     * 세금계산서관리 > 수취내역 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/taxBillReceiveList", method = RequestMethod.GET)
    public String taxBillReceiveList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("taxBillInvoiceListBaseInfo", TaxBillReceiveGetDto.class));

        return "/settle/taxBillReceiveList";
    }

    /**
     * 세금계산서관리 > 수취내역 관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTaxBillReceiveList.json", method = RequestMethod.GET)
    public List<TaxBillReceiveGetDto> getTaxBillReceiveList(TaxBillInvoiceListSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.getTaxBillReceiveList(listParamDto);
    }

    /**
     * 세금계산서관리 > 수취내역 등록
     */
    @ResponseBody
    @RequestMapping(value = "/setTaxBillReceiveList.json", method = RequestMethod.GET)
    public ResponseObject<String> setTaxBillReceiveList(TaxBillReceiveSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.setTaxBillReceiveList(listParamDto);
    }

    /**
     * 세금계산서관리 > 수취내역 수정
     */
    @ResponseBody
    @RequestMapping(value = "/updTaxBillReceive.json", method = RequestMethod.GET)
    public ResponseObject<String> updTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.updTaxBillReceive(listParamDto);
    }

    /**
     * 세금계산서관리 > 수취내역 확정
     */
    @ResponseBody
    @RequestMapping(value = "/setTaxBillReceiveConfirm.json", method = RequestMethod.GET)
    public ResponseObject<String> setTaxBillReceiveConfirm(TaxBillReceiveSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.setTaxBillReceiveConfirm(listParamDto);
    }

    /**
     * 세금계산서 수취내역 > 공급자 정보 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTaxBillSupplierList.json", method = RequestMethod.GET)
    public List<TaxBillReceiveSupplierGetDto> getTaxBillSupplierList(String taxBillReceiveType) throws Exception {
        return TaxBillInvoiceService.getTaxBillSupplierList(taxBillReceiveType);
    }

    /**
     * 세금계산서관리 > 수취내역 삭제
     */
    @ResponseBody
    @RequestMapping(value = "/deleteTaxBillReceive.json", method = RequestMethod.GET)
    public ResponseObject<String> deleteTaxBillReceive(TaxBillReceiveSetDto listParamDto) throws Exception {
        return TaxBillInvoiceService.deleteTaxBillReceive(listParamDto);
    }

    /**
     * 세금계산서관리 > 마켓연동 수취내역 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/taxBillReceiveMarket", method = RequestMethod.GET)
    public String taxBillReceiveMarket(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "taxbill_receive_type"              // 세금계산서 수취유형
        );
        model.addAttribute("schTaxBillReceiveType", code.get("taxbill_receive_type"));
        model.addAllAttributes(RealGridHelper.create("taxBillInvoiceListBaseInfo", TaxBillReceiveMarketGetDto.class));

        return "/settle/taxBillReceiveMarket";
    }
}