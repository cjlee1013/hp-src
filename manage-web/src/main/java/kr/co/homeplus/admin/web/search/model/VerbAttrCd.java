package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/***
 * 동사 문법정보 : 불규칙 활용 유형 (동사 한정)
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum VerbAttrCd {
    X("X","규칙"),
    B("B","ㅂ 불규칙"),
    H("H","ㅎ 불규칙"),
    L("L","르 불규칙"),
    U("U","ㄹ 불규칙"),
    S("S","ㅅ 불규칙"),
    D("D","ㄷ 불규칙"),
    R("R","러 불규칙");

    private String code;
    private String name;

    public static VerbAttrCd getType(String code) {
        for(VerbAttrCd cd : VerbAttrCd.values()) {
            if(cd.getCode().equals(code)) {
                return cd;
            }
        }
        return null;
    }
}
