package kr.co.homeplus.admin.web.partner.model.sellerApiKey;

import lombok.Data;

@Data
public class SellerApiKeyListParamDto {

    private String schDt;
    private String schStartDt;
    private String schEndDt;
    private String schPartnerId;
    private String schUseYn;
    private String schAgencyCd;

}
