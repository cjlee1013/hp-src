package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DictionaryComponentGetDto<T> {
    /***
     * 사전용
     */
    private List<T> dictionaryList;
    /***
     * 검색용
     */
    private List<T> resultList;
}
