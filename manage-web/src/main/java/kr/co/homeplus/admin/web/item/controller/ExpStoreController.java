package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreHistGetDto;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreHistTotalGetDto;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreListGetDto;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreListParamDto;
import kr.co.homeplus.admin.web.item.model.expStore.ExpStoreSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/store")
public class ExpStoreController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final CodeService codeService;

    public ExpStoreController(ResourceClient resourceClient, LoginCookieService cookieService,
        CodeService codeService) {

        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;
    }

    /**
     * EXPRESS 점포 주문시간 관리
     */
    @RequestMapping(value = "/expStoreMain", method = RequestMethod.GET)
    public String ExpStoreMain(Model model) throws Exception {

        //코드정보
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "order_avail_yn"   //주문가능여부
            , "exp_stop_reason"             //주문불가사유
            , "use_yn");               //사용여부

        model.addAttribute("orderAvailYn", code.get("order_avail_yn"));
        model.addAttribute("stopReasonCd", code.get("exp_stop_reason"));

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("expStoreListGridBaseInfo", ExpStoreListGetDto.class));

        model.addAllAttributes(RealGridHelper
            .create("expStoreHistTotalListPopGridBaseInfo", ExpStoreHistTotalGetDto.class));

        return "/item/expStoreMain";
    }

    @ResponseBody
    @GetMapping(value = {"/getExpStoreList.json"})
    public List<ExpStoreListGetDto> getExpStoreList(ExpStoreListParamDto expstoreListParamDto) {
        String apiUri = "/item/expStore/getExpStoreList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExpStoreListGetDto>>>() {
        };
        return (List<ExpStoreListGetDto>) resourceClient
            .getForResponseObject(ResourceRouteName.ITEM,
                StringUtil
                    .getRequestString(apiUri, ExpStoreListParamDto.class, expstoreListParamDto),
                typeReference).getData();
    }

    /**
     * 상품관리 > 점포관리 > EXPRESS 점포 주문시간 관리
     */
    @ResponseBody
    @PostMapping(value = "/setExpStore.json")
    public ResponseResult setExpStore(ExpStoreSetDto expStoreSetDto) throws Exception {

        expStoreSetDto.setUserId(cookieService.getUserInfo().getEmpId());
        ResponseObject<ResponseResult> responseObject = resourceClient
            .postForResponseObject(ResourceRouteName.ITEM, expStoreSetDto
                , "/item/expStore/setExpStore",
                new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {
                });

        ResponseResult responseResult = responseObject.getData();

        return responseResult;
    }


    @ResponseBody
    @GetMapping(value = {"/getExpStoreHistTotalList.json"})
    public List<ExpStoreHistTotalGetDto> getExpStoreList() {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , "/item/expStore/getExpStoreHistTotalList"
            , new ParameterizedTypeReference<ResponseObject<List<ExpStoreHistTotalGetDto>>>() {
            }).getData();

    }
}

