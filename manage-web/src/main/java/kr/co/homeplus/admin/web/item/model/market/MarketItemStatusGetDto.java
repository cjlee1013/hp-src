package kr.co.homeplus.admin.web.item.model.market;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarketItemStatusGetDto {

    @RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true, width = 300)
    private String itemNm;

    @RealGridColumnInfo(headText = "홈플러스 상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemStatusNm;

    @RealGridColumnInfo(headText = "홈플러스 상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String itemStatus;

    @RealGridColumnInfo(headText = "연동사이트", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String marketNm;

    @RealGridColumnInfo(headText = "연동사이트 상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 120)
    private String marketItemNo;

    @RealGridColumnInfo(headText = "연동사이트 상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 120)
    private String marketItemStatusNm;

    @RealGridColumnInfo(headText = "연동사이트 상품상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketItemStatus;

    @RealGridColumnInfo(headText = "연동여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 120)
    private String sendYnNm;

    @RealGridColumnInfo(headText = "연동여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String sendYn;

    @RealGridColumnInfo(headText = "마켓연동ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String partnerId;

    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String dcateNm;

    @RealGridColumnInfo(headText = "세카테고리번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String dcateCd;

    @RealGridColumnInfo(headText = "마켓연동카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketCateCd;

    @RealGridColumnInfo(headText = "마켓연동 대카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketLcateNm;

    @RealGridColumnInfo(headText = "마켓연동 중카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketMcateNm;

    @RealGridColumnInfo(headText = "마켓연동 소카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketScateNm;

    @RealGridColumnInfo(headText = "마켓연동 세카테고리", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String marketDcateNm;

    @RealGridColumnInfo(headText = "연동결과" , fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String sendMsg;

    @RealGridColumnInfo(headText = "최종연동자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "최종연동일시",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;


}
