package kr.co.homeplus.admin.web.settle.model.partnerPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PartnerPaymentListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "지급상태")
    private String settlePayState;

    @ApiModelProperty(value = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(value = "판매업체명")
    private String partnerName;

    @ApiModelProperty(value = "사업자번호")
    private String partnerNo;

    @ApiModelProperty(value = "계좌번호")
    private String bankAccountNo;

    @ApiModelProperty(value = "예금주")
    private String bankAccountHolder;

    @ApiModelProperty(value = "지급SRL")
    private String settleSrl;

    @ApiModelProperty(value = "지급방법")
    private String payMethod;
}
