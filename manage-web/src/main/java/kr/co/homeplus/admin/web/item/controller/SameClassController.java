package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassGroupListParamDto;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassGroupListSelectDto;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassGroupSetDto;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassItemParamDto;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassListSelectDto;
import kr.co.homeplus.admin.web.item.model.sameClass.SameClassSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/sameclass")
public class SameClassController {

    final private ResourceClient resourceClient;

    final private LoginCookieService loginCookieService;

    final private CodeService codeService;

    public SameClassController(ResourceClient resourceClient,
                                            LoginCookieService loginCookieService,
                                           CodeService codeService) {
        this.resourceClient = resourceClient;
        this.loginCookieService = loginCookieService;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 점포상품관리 > 동류그룹관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/sameClassMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"        // 사용여부
                ,       "same_class_sch_type" //동류그룹 검색 타입
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("searchType", code.get("same_class_sch_type"));

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("sameClassGroupListGridBaseInfo", SameClassGroupListSelectDto.class));

        model.addAllAttributes(
            RealGridHelper.create("sameClassListGridBaseInfo", SameClassListSelectDto.class));

        return "/item/sameClassMain";
    }
    /**
     * 상품관리 > 점포상품관리 >  동류그룹 리스트
     * @return SameClassGroupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSameClassGroupList.json", method = RequestMethod.GET)
    public List<SameClassGroupListSelectDto> getRestrictionItemList(SameClassGroupListParamDto listParamDto) throws Exception {
        String apiUri = "/item/sameclass/getSameClassGroupList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SameClassGroupListSelectDto>>>() {};
        return (List<SameClassGroupListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, SameClassGroupListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 동류그룹 정보
     * @return SameClassGroupListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSameClassGroup.json", method = RequestMethod.GET)
    public SameClassGroupListSelectDto getSameClassGroup(@RequestParam(value="sameClassNo") int sameClassNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("sameClassNo", sameClassNo));

        String apiUriRequest = "/item/sameclass/getSameClassGroup" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<SameClassGroupListSelectDto>>() {})
            .getData();
    }

    /**
     * 상품관리 > 점포상품관리 >  동류그룹 등록/수정
     * @param sameClassGroupSetDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setSameClassGroup.json"}, method = RequestMethod.POST)
    public ResponseResult setSameClassGroup(SameClassGroupSetDto sameClassGroupSetDto) throws Exception {

        String apiUri = "/item/sameclass/setSameClassGroup";
        sameClassGroupSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, sameClassGroupSetDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }

    /**
     * 상품관리 > 점포상품관리 >  동류상품 리스트
     * @return SameClassListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSameClassList.json", method = RequestMethod.GET)
    public List<SameClassListSelectDto> getSameClassList(@RequestParam(name = "sameClassNo") String sameClassNo) throws Exception {

        String apiUri = "/item/sameclass/getSameClassList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SameClassListSelectDto>>>() {};
        return (List<SameClassListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?sameClassNo=" + sameClassNo , typeReference).getData();

    }
    /**
     * 상품관리 > 점포상품관리 >  동류상품
     * @return SameClassListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getSameClassItem.json", method = RequestMethod.GET)
    public ResponseResult getSameClassItem(SameClassItemParamDto listParamDto) throws Exception {

        String apiUri = "/item/sameclass/getSameClassItem";
        return (ResponseResult) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, SameClassItemParamDto.class, listParamDto), new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 상품관리 > 점포상품관리 >  동류상품 등록/수정
     * @param sameClassSetDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setSameClass.json"}, method = RequestMethod.POST)
    public ResponseResult setSameClass(SameClassSetDto sameClassSetDto) throws Exception {

        String apiUri = "/item/sameclass/setSameClass";
        sameClassSetDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, sameClassSetDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }

}
