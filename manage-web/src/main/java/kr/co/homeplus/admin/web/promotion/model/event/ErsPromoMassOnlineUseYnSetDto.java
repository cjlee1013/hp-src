package kr.co.homeplus.admin.web.promotion.model.event;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErsPromoMassOnlineUseYnSetDto {
    private List<String> eventCdList;

    private String useYn;

    private String empNo;
}