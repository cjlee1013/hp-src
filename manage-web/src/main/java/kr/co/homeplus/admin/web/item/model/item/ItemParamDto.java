package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//셀러상품 상세 조회
public class ItemParamDto {

    //상품번호
    private String itemNo;

    //거래유형 (DS : 업체상품 , TD : 매장상품)
    private String mallType;

    //점포ID
    private String storeId;

    //점포유형
    private String storeType;

    //메뉴유형 (basic: 기본정보, detail: 상세정보)
    private String menuType;

    //온라인재고 일괄설정 메뉴 여부
    private String onlineStockMngYn;

}
