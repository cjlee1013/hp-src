package kr.co.homeplus.admin.web.search.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.co.homeplus.admin.web.search.service.BackupRecoveryService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/search/backupRecovery")
@RequiredArgsConstructor
public class BackupRecoveryController {

    private final BackupRecoveryService backupRecoveryService;

    @GetMapping(value = "/main")
    public String Main() {
        return "/search/backupRecovery";
    }

    @ResponseBody
    @GetMapping(value = {"/backupTableList.json"})
    public ResponseObject<Object> backupTableList(@RequestParam("schServiceCd") String schServiceCd) {
        return backupRecoveryService.backupTableList(schServiceCd);
    }

    @ResponseBody
    @PostMapping(value = {"/txtAsDB.json"})
    public ResponseObject<Object> txtAsDB(@RequestParam("schServiceCd") String schServiceCd, @RequestParam("uploadFile") List<MultipartFile> files, @RequestParam("passwd") String passwd) {
        return backupRecoveryService.txtAsDB(schServiceCd, files, passwd);
    }

    /***
     * 사전 백업
     */
    @GetMapping(value = {"/downloadsAsTxt.json"})
    public void downloadsAsTxt(@RequestParam("schServiceCd") String schServiceCd, @RequestParam("tableName") String tableName, HttpServletRequest request, HttpServletResponse response)
        throws IOException, URISyntaxException {
        backupRecoveryService.downloadsAsTxt(schServiceCd, tableName, request, response);
    }
}
