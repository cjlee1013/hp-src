package kr.co.homeplus.admin.web.mileage.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.mileage.model.type.pop.MileagePromoTypeGetDto;
import kr.co.homeplus.admin.web.mileage.model.type.pop.MileagePromoTypeSetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.admin.web.mileage.model.type.MileageSaveType;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeSearchPopDto;
import kr.co.homeplus.admin.web.mileage.service.MileageTypeMngService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeMngGetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeMngSetDto;
import kr.co.homeplus.admin.web.mileage.model.type.MileageTypeModifySetDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/mileage/type")
public class MileageTypeMngController {

    private final MileageTypeMngService mileageTypeMngService;
    private final CodeService codeService;

    public MileageTypeMngController(MileageTypeMngService mileageTypeMngService, CodeService codeService) {
        this.mileageTypeMngService = mileageTypeMngService;
        this.codeService = codeService;

    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 (SETTLE-6)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/mileageTypeMng", method = RequestMethod.GET)
    public String mileageTypeMngMain(Model model) throws Exception {
        // 점포유형 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mileageKind", Arrays.asList(MileageSaveType.values()));
        model.addAllAttributes(RealGridHelper.create("mileageListBaseInfo", MileageTypeMngGetDto.class));

        return "/mileage/mileageTypeMng";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 메인 리스트 조회
     * @return MileageTypeMngGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageTypeList.json", method = RequestMethod.GET)
    public List<MileageTypeMngGetDto> getMileageRequestList(MileageTypeMngSetDto listParamDto) throws Exception {
        return mileageTypeMngService.getMileageRequestList(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 타입 상세 정보 조회(Search by MileageTypeNo)
     * @return MileageRequestGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageTypeDetail.json", method = RequestMethod.GET)
    public MileageTypeModifySetDto getMileageTypeDetail(MileageTypeModifySetDto listParamDto) throws Exception {
        return mileageTypeMngService.getMileageTypeDetail(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 등록
     * @return MileageTypeMngGetDto
     */
    @ResponseBody
    @PostMapping("/addMileageType.json")
    public ResponseObject<String> addMileageType(@RequestBody MileageTypeModifySetDto listParamDto) throws Exception {
        return mileageTypeMngService.addMileageType(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 수정
     * @return MileageTypeMngGetDto
     */
    @ResponseBody
    @PostMapping("/modifyMileageType.json")
    public ResponseObject<String> modifyMileageType(@RequestBody MileageTypeModifySetDto listParamDto) throws Exception {
        return mileageTypeMngService.modifyMileageType(listParamDto);
    }

    /**
     * 마일리지 프로모션 유형 조회 팝업
     */
    @GetMapping(value = "/mileagePromoTypePop")
    public String mileageTypeNamePop(Model model, @RequestParam("storeType") String storeType,
        @RequestParam(value = "callBack", defaultValue = "") String callBackScript) throws Exception {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("storeType", storeType);
        model.addAllAttributes(RealGridHelper.create("mileageTypeGrid", MileagePromoTypeGetDto.class));
        return "/mileage/pop/mileagePromoTypePop";
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 유형 조회
     * @return MileageTypeSearchPopDto
     */
    @ResponseBody
    @RequestMapping(value = "/getMileageTypeByName.json", method = RequestMethod.GET)
    public List<MileageTypeSearchPopDto> getMileageTypeByName(MileageTypeSearchPopDto listParamDto) throws Exception {
        return mileageTypeMngService.getMileageTypeByName(listParamDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getMileagePromoTypePopInfo.json", method = RequestMethod.GET)
    public List<MileagePromoTypeGetDto> getMileagePromoTypePopInfo(MileagePromoTypeSetDto setDto) throws Exception {
        return mileageTypeMngService.getMileagePromoTypePopInfo(setDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 등록
     * @return MileageTypeMngGetDto
     */
    @ResponseBody
    @PostMapping("/addMileageEventType.json")
    public ResponseObject<String> addMileageEventType(@RequestBody MileageTypeModifySetDto listParamDto) throws Exception {
        return mileageTypeMngService.addMileageEventType(listParamDto);
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 적립 타입 관리 > 타입 수정
     * @return MileageTypeMngGetDto
     */
    @ResponseBody
    @PostMapping("/modifyMileageEventType.json")
    public ResponseObject<String> modifyMileageEventType(@RequestBody MileageTypeModifySetDto listParamDto) throws Exception {
        return mileageTypeMngService.modifyMileageEventType(listParamDto);
    }

}