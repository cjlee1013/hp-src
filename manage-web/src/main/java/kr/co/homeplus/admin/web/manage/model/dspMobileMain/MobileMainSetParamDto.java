package kr.co.homeplus.admin.web.manage.model.dspMobileMain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MobileMainSetParamDto {

    private Integer mainNo;

    private String siteType;

    private String loc1Depth;

    private String loc2Depth;

    private String dispYn;

    private Integer priority;

    private String splash;

    private String dispStartDt;

    private String dispEndDt;

    private Character dispTimeYn;

    private String dispStartTime;

    private String dispEndTime;

    private String bannerNm;

    private String dispStoreType;

    private String bgColor;

    private String closeType;

    private String division;

    private String template;

    private String templateDetail;

    private List<MobileMainLinkSetParamDto> linkList;

    private List<MobileMainStoreSetDto> storeList;

    private String regId;

}
