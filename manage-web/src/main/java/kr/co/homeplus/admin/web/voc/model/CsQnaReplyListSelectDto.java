package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaReplyListSelectDto {

    private String qnaReplyNo;

    private String qnaNo;

    private String displayStatus;

    private String displayStatusTxt;

    private String qnaDisplayStatus;

    private String replyArea;

    private String qnaReply;

    private String regNm;

    private String partnerNm;

    private String partnerId;

    private String regDt;
}
