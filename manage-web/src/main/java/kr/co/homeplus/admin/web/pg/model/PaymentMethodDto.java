package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridInfo(useOrder = true)
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PaymentMethodDto {
    @ApiModelProperty(value = "수단코드")
    @RealGridColumnInfo(headText = "상세결제수단코드", order = 3, width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodCd;

    @ApiModelProperty(value = "수단명")
    @RealGridColumnInfo(headText = "상세결제수단명", order = 4, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String methodNm;

    @ApiModelProperty(value = "상위수단코드(신용카드,휴대폰결제등)")
    @RealGridColumnInfo(headText = "parentMethodCd", order = 15, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodCd;

    @ApiModelProperty(value = "상위수단명")
    @RealGridColumnInfo(headText = "결제수단명", order = 2, width = 70, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodNm;

    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    @RealGridColumnInfo(headText = "siteType", order = 14, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;

    @ApiModelProperty(value = "사이트유형명")
    @RealGridColumnInfo(headText = "사이트", order = 1, width = 70, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteTypeNm;

    @ApiModelProperty(value = "PG인증종류(안심:ASIAM,ISP:ISP,V3D:V3D 등)")
    @RealGridColumnInfo(headText = "pgCertifyKind", order = 16, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgCertifyKind;

    @ApiModelProperty(value = "inicis코드(PG명 향후 변경가능)")
    @RealGridColumnInfo(headText = "INICIS", order = 5, width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String inicisCd;

    @ApiModelProperty(value = "tosspg코드(PG명 향후 변경가능)")
    @RealGridColumnInfo(headText = "TOSSPG", order = 6, width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String tosspgCd;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", order = 7, width = 70, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(value = "플랫폼(전체,PC,MOBILE)")
    @RealGridColumnInfo(headText = "노출범위", order = 8, width = 70, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String platform;

    @ApiModelProperty(value = "정렬순서")
    @RealGridColumnInfo(headText = "노출순서", order = 9, width = 70, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private int sortSeq;

    @ApiModelProperty(value = "이미지url")
    @RealGridColumnInfo(headText = "imgUrl", order = 17, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String imgUrl;

    @ApiModelProperty(value = "이미지url모바일")
    @RealGridColumnInfo(headText = "imgUrlMobile", order = 18, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String imgUrlMobile;

    @ApiModelProperty(value = "이미지넓이")
    @RealGridColumnInfo(headText = "imgWidth", order = 19, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private int imgWidth;

    @ApiModelProperty(value = "이미지높이")
    @RealGridColumnInfo(headText = "imgHight", order = 20, width = 10, hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private int imgHight;

    @ApiModelProperty(value = "등록자id")
    @RealGridColumnInfo(headText = "등록자", order = 11, width = 70, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "등록일자")
    @RealGridColumnInfo(headText = "등록일", order = 10, width = 140, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME)
    private String regDt;

    @ApiModelProperty(value = "수정자id")
    @RealGridColumnInfo(headText = "수정자", order = 13, width = 70, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "수정일자")
    @RealGridColumnInfo(headText = "수정일", order = 12, width = 140, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME)
    private String chgDt;
}
