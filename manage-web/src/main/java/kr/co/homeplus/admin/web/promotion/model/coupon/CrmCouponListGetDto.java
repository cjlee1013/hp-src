package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class CrmCouponListGetDto {
    @RealGridColumnInfo(headText = "쿠폰번호", sortable = true)
    private Long couponNo;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @RealGridColumnInfo(headText = "관리쿠폰명")
    private String manageCouponNm;

    @RealGridColumnInfo(headText = "쿠폰종류")
    private String couponType;

    @RealGridColumnInfo(headText = "쿠폰상태")
    private String status;

    @RealGridColumnInfo(headText = "할인액/율")
    private String discount;

    @RealGridColumnInfo(headText = "발급기간", width = 200)
    private String issuePeriod;

    @RealGridColumnInfo(headText = "적용기간", width = 200)
    private String validPeriod;

    @RealGridColumnInfo(headText = "귀속부서")
    private String departCd;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 200)
    private String chgDt;

    @RealGridColumnInfo(headText = "발급제한유형(쿠폰총발급수량유형:(UNLIMIT:무제한, DAY:일별, PERIOD:기간))", hidden = true)
    private String issueLimitType;
}