package kr.co.homeplus.admin.web.settle.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class TaxBillReceiveSetDto {
    @ApiModelProperty(value = "수취ID")
    @RealGridColumnInfo(headText = "수취ID", width = 100)
    private String taxReceiveNo;

    @ApiModelProperty(value = "발행일")
    @RealGridColumnInfo(headText = "발행일", width = 100)
    private String issueDt;

    @ApiModelProperty(value = "공급처")
    @RealGridColumnInfo(headText = "공급처", width = 100)
    private String supplier;

    @ApiModelProperty(value = "세금계산서발행타입")
    @RealGridColumnInfo(headText = "수취유형", width = 100)
    private String taxBillReceiveType;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 100)
    private String partnerId;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", width = 100)
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    @RealGridColumnInfo(headText = "사업자번호", width = 100)
    private String partnerNo;

    @ApiModelProperty(value = "대표자명")
    @RealGridColumnInfo(headText = "대표자명", width = 100)
    private String partnerOwner;

    @ApiModelProperty(value = "합계금액")
    @RealGridColumnInfo(headText = "합계금액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sumAmt;

    @ApiModelProperty(value = "공급가액")
    @RealGridColumnInfo(headText = "공급가액", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long supplyAmt;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long vatAmt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regId;

}
