package kr.co.homeplus.admin.web.settle.controller;

import java.util.Arrays;
import java.util.List;
import kr.co.homeplus.admin.web.settle.model.SettleMallType;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.TdUnshipOrderListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.TdUnshipOrderListSetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.UnshipOrderListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipOrder.UnshipOrderListSetDto;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.admin.web.settle.service.UnshipOrderService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/unshipOrder")
public class UnshipOrderController {

    @Autowired
    private UnshipOrderService unshipOrderService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * 미배송 주문조회 Main Page
     */
    @RequestMapping(value = "/unshipOrderMain", method = RequestMethod.GET)
    public String unshipOrderMain(Model model) throws Exception{
        model.addAttribute("storeType", settleCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", settleCommonService.getCode("ship_status"));
        model.addAttribute("mallType", Arrays.asList(SettleMallType.values()));
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.create("unshipOrderGridBaseInfo", UnshipOrderListGetDto.class));

        return "/settle/unshipOrderMain";
    }

    /**
     * TD미배송 주문조회 Main Page
     */
    @RequestMapping(value = "/tdUnshipOrderMain", method = RequestMethod.GET)
    public String tdUnshipOrderMain(Model model) throws Exception{
        model.addAttribute("storeType", settleCommonService.getCode("store_type"));
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAttribute("shipStatus", settleCommonService.getCode("ship_status"));
        model.addAllAttributes(RealGridHelper.createForGroup("tdUnshipOrderGridBaseInfo", TdUnshipOrderListGetDto.class));
        return "/settle/tdUnshipOrderMain";
    }

    /**
     * 미배송 주문조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getUnshipOrderList.json", method = RequestMethod.GET)
    public List<UnshipOrderListGetDto> getUnshipOrderList(UnshipOrderListSetDto unshipOrderListSetDto) throws Exception{
        return unshipOrderService.getUnshipOrderList(unshipOrderListSetDto);
    }

    /**
     * TD미배송 주문조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getTdUnshipOrderList.json", method = RequestMethod.GET)
    public List<TdUnshipOrderListGetDto> getTdUnshipOrderList(TdUnshipOrderListSetDto tdUnshipOrderListSetDto) throws Exception{
        return unshipOrderService.getTdUnshipOrderList(tdUnshipOrderListSetDto);
    }
}