package kr.co.homeplus.admin.web.settle.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "결제일마감")
public class PgPaymentInfoListGetDto {
  @ApiModelProperty(notes = "기준일")
  @RealGridColumnInfo(headText = "기준일", sortable = true, width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "PG사")
  @RealGridColumnInfo(headText = "PG사", sortable = true, width = 100)
  private String pgKind;

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
  private String originStoreId;

  @ApiModelProperty(notes = "점포명")
  @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
  private String originStoreNm;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "클레임그룹번호")
  @RealGridColumnInfo(headText = "클레임그룹번호", sortable = true, width = 100)
  private String claimNo;

  @ApiModelProperty(notes = "상품구분")
  @RealGridColumnInfo(headText = "상품구분", sortable = true, width = 100)
  private String mallType;

  @ApiModelProperty(notes = "주문구분")
  @RealGridColumnInfo(headText = "주문구분", sortable = true, width = 100)
  private String gubun;

  @ApiModelProperty(notes = "총 상품금액")
  @RealGridColumnInfo(headText = "총 상품금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "배송비")
  @RealGridColumnInfo(headText = "배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipAmt;

  @ApiModelProperty(notes = "반품배송비")
  @RealGridColumnInfo(headText = "반품배송비", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String claimReturnShipAmt;

  @ApiModelProperty(notes = "상품할인(업체)")
  @RealGridColumnInfo(headText = "상품할인(업체)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponSellerChargeAmt;

  @ApiModelProperty(notes = "상품할인(자사)")
  @RealGridColumnInfo(headText = "상품할인(자사)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponHomeChargeAmt;

  @ApiModelProperty(notes = "상품할인(카드)")
  @RealGridColumnInfo(headText = "상품할인(카드)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String couponCardChargeAmt;

  @ApiModelProperty(notes = "카드할인(자사)")
  @RealGridColumnInfo(headText = "카드할인(자사)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cardCouponHomeAmt;

  @ApiModelProperty(notes = "카드할인(카드)")
  @RealGridColumnInfo(headText = "카드할인(카드)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cardCouponCardAmt;

  @ApiModelProperty(notes = "배송비할인")
  @RealGridColumnInfo(headText = "배송비할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String shipDiscountAmt;

  @ApiModelProperty(notes = "임직원할인")
  @RealGridColumnInfo(headText = "임직원할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String empDiscountAmt;

  @ApiModelProperty(notes = "행사할인")
  @RealGridColumnInfo(headText = "행사할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String promoDiscountAmt;

  @ApiModelProperty(notes = "장바구니할인")
  @RealGridColumnInfo(headText = "장바구니할인", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cartCouponHomeAmt;

  @ApiModelProperty(notes = "매출(E-KPI)")
  @RealGridColumnInfo(headText = "매출(E-KPI)", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String ekpiAmt;

  @ApiModelProperty(notes = "결제수단/환불수단")
  @RealGridColumnInfo(headText = "결제수단/환불수단", sortable = true, width = 100)
  private String paymentTool;

  @ApiModelProperty(notes = "합계 결제금액")
  @RealGridColumnInfo(headText = "결제금액 합계", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "PG 결제금액")
  @RealGridColumnInfo(headText = "PG", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String pgAmt;

  @ApiModelProperty(notes = "DGV 결제금액")
  @RealGridColumnInfo(headText = "DGV", sortable = true, width = 100,  fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String dgvAmt;

  @ApiModelProperty(notes = "MHC 결제금액")
  @RealGridColumnInfo(headText = "MHC", sortable = true, width = 100,  fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mhcAmt;

  @ApiModelProperty(notes = "OCB 결제금액")
  @RealGridColumnInfo(headText = "OCB", sortable = true, width = 100,  fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String ocbAmt;

  @ApiModelProperty(notes = "마일리지 결제금액")
  @RealGridColumnInfo(headText = "마일리지", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String mileageAmt;

  @ApiModelProperty(notes = "판매수수료")
  @RealGridColumnInfo(headText = "판매수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String saleAgencyFee;

  @ApiModelProperty(notes = "정산대상금액")
  @RealGridColumnInfo(headText = "정산대상금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settleAmt;

  @ApiModelProperty(notes = "부가세조건")
  @RealGridColumnInfo(headText = "부가세조건", sortable = true, width = 100)
  private String vatCondition;

  @ApiModelProperty(notes = "PG수수료율")
  @RealGridColumnInfo(headText = "PG수수료율", sortable = true, width = 100)
  private String commissionRate;

  @ApiModelProperty(notes = "PG수수료")
  @RealGridColumnInfo(headText = "PG수수료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String commissionAmt;

  @ApiModelProperty(notes = "PG예치금")
  @RealGridColumnInfo(headText = "PG예치금", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String depositAmt;

  @ApiModelProperty(notes = "지급완료금액")
  @RealGridColumnInfo(headText = "지급완료금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String payCompleteAmt;

  @ApiModelProperty(notes = "예치금잔고")
  @RealGridColumnInfo(headText = "예치금잔고", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String depositBalance;

  @ApiModelProperty(notes = "입금주기")
  @RealGridColumnInfo(headText = "입금주기", sortable = true, width = 100)
  private String costPeriod;

  @ApiModelProperty(notes = "입금일")
  @RealGridColumnInfo(headText = "입금일", sortable = true, width = 100)
  private String payScheduleDt;

  @ApiModelProperty(notes = "입금예정금액")
  @RealGridColumnInfo(headText = "입금예정금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String payScheduleAmt;

  @ApiModelProperty(notes = "연동 주문번호")
  @RealGridColumnInfo(headText = "연동 주문번호", sortable = true, width = 100)
  private String pgOid;
}
