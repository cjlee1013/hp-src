package kr.co.homeplus.admin.web.partner.model;

import lombok.Data;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 계좌 실명 인증 파라미터
 */
@Data
public class PartnerBankAccntNoCertifyParamGetDto {
	//은행코드(H004:국민,H088:신한,H081:하나,H020:우리,H003:기업)
	private String bankCd;

	//은행계좌번호
	private String bankAccntNo;

	//예금주
	private String depositor;
}
