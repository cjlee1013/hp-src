package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "실시간 판매업체별 판매현황 DTO")
@Getter
@Setter
@EqualsAndHashCode
public class RealtimePartnerOrderStatisticsDto {
    @ApiModelProperty(value = "판매업체명", position = 1)
    @RealGridColumnInfo(headText = "판매업체명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String businessNm;

    @ApiModelProperty(value = "판매업체 ID", position = 2)
    @RealGridColumnInfo(headText = "판매업체ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String partnerId;

    @ApiModelProperty(value = "판매업체코드(OF 밴더코드)", position = 3)
    @RealGridColumnInfo(headText = "판매업체코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String ofVendorCd;

    @ApiModelProperty(value = "주문 건수", position = 4)
    @RealGridColumnInfo(headText = "건수", groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderQty;

    @ApiModelProperty(value = "주문 금액", position = 5)
    @RealGridColumnInfo(headText = "금액", groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long orderPrice;

    @ApiModelProperty(value = "취소 건수", position = 6)
    @RealGridColumnInfo(headText = "건수", groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cancelQty;

    @ApiModelProperty(value = "취소 금액", position = 7)
    @RealGridColumnInfo(headText = "금액", groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cancelPrice;

    @ApiModelProperty(value = "반품 건수", position = 8)
    @RealGridColumnInfo(headText = "건수", groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long returnQty;

    @ApiModelProperty(value = "반품 금액", position = 9)
    @RealGridColumnInfo(headText = "금액", groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long returnPrice;
}