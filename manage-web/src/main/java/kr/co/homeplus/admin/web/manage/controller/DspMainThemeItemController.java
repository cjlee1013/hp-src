package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.dspMainTheme.DspMainThemeGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemeItem.DspMainThemeItemCntGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemeItem.DspMainThemeItemExcelDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemeItem.DspMainThemeItemGetDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemeItem.DspMainThemeItemSetParamDto;
import kr.co.homeplus.admin.web.manage.model.dspMainThemeItem.DspMainThemeSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class DspMainThemeItemController {

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;
    private final ExcelUploadService excelUploadService;

    /**
     * 사이트관리 > 전시관리 > 홈테마 상품 관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/dspMain/dspMainThemeItemMain", method = RequestMethod.GET)
    public String dspMainThemeItemMain(Model model) {

        codeService.getCodeModel(model
            ,   "use_yn"		        // 사용여부
            ,   "dsk_site_gubun"        // 사이트 구분
            ,   "disp_yn"		        // 사용여부
            ,   "dsp_main_store_type"   // 스토어 타입
            ,   "dsp_main_link_type"    // 링크 타입
        );

        model.addAttribute("homeImgUrl", this.homeImgUrl);

        model.addAllAttributes(RealGridHelper.create("dspMainThemeGridBaseInfo", DspMainThemeSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("dspMainThemeItemGridBaseInfo", DspMainThemeItemGetDto.class));

        return "/manage/dspMainThemeItemMain";
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 상품 조회
     * @param
     * @return DspMainThemeGetDto
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/getDspMainThemeItemList.json"}, method = RequestMethod.GET)
    public List<DspMainThemeItemGetDto> getDspMainThemeItemList(@RequestParam(value = "themeId") Long themeId, @RequestParam(value = "dispDt") String dispDt) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("themeId", themeId));
        getParameterList.add(SetParameter.create("dispDt", dispDt));

        String apiUriRequest = "/manage/dspMain/getDspMainThemeItemList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspMainThemeItemGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 등록/수정
     *
     * @param dspMainThemeItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/setDspMainThemeItem.json"}, method = RequestMethod.POST)
    public ResponseResult setDspMainThemeItem(@Valid @RequestBody List<DspMainThemeItemSetParamDto> dspMainThemeItemSetParamDto) {
        String apiUri = "/manage/dspMain/setDspMainThemeItem";
        dspMainThemeItemSetParamDto.get(0).setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspMainThemeItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 전시여부 수정
     *
     * @param dspMainThemeItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/setDspMainItemThemeDisplay.json"}, method = RequestMethod.POST)
    public ResponseResult setDspMainItemThemeDisplay(@Valid @RequestBody List<DspMainThemeItemSetParamDto> dspMainThemeItemSetParamDto) {
        String apiUri = "/manage/dspMain/setDspMainItemThemeDisplay";
        dspMainThemeItemSetParamDto.get(0).setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspMainThemeItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 등록 ( 일괄_팝업 )
     *
     * @param dspMainThemeItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/setDspMainThemeItemByMulti.json"}, method = RequestMethod.POST)
    public ResponseResult setDspMainThemeItemByMulti(@Valid @RequestBody List<DspMainThemeItemSetParamDto> dspMainThemeItemSetParamDto) {
        String apiUri = "/manage/dspMain/setDspMainThemeItemByExcel";
        dspMainThemeItemSetParamDto.get(0).setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, dspMainThemeItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 상품 상품 수
     *
     * @param dispDate
     * @return List<DspMainThemeItemCntGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/getDspMainItemCnt.json"}, method = RequestMethod.GET)
    public List<DspMainThemeItemCntGetDto> getDspMainItemCnt(@RequestParam(value = "dispDate") String dispDate, @RequestParam(value = "siteType") String siteType) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("dispDate", dispDate));
        getParameterList.add(SetParameter.create("siteType", siteType));

        String apiUriRequest = "/manage/dspMain/getMainThemeItemCnt" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspMainThemeItemCntGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 ( 일별 )
     *
     * @param dispDt
     * @return List<DspMainThemeGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/dspMain/getDspMainTheme.json"}, method = RequestMethod.GET)
    public List<DspMainThemeGetDto> getDspMainTheme(@RequestParam(value = "dispDt") String dispDt, @RequestParam(value = "siteType") String siteType) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("dispDt", dispDt));
        getParameterList.add(SetParameter.create("siteType", siteType));

        String apiUriRequest = "/manage/dspMain/getDspMainTheme" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<DspMainThemeGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadMainThemeItemPop")
    public String uploadMainThemeItemPop(Model model, @RequestParam(value = "callback") String callBackScript, @RequestParam(value = "targetDisp") String targetDisp) {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("targetDisp", targetDisp);
        return "/manage/pop/uploadMainThemeItemPop";
    }

    /**
     * 기획전 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/dspMain/mainThemeItemExcel.json")
    public List<DspMainThemeItemExcelDto> mainThemeItemExcel(@RequestParam(value="targetDisp") String targetDisp, MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<DspMainThemeItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "테마번호", "themeId", true)
            .header(1, "상품유형", "storeType", true)
            .header(2, "상품번호", "itemNo", true)
            .header(3, "사용여부", "useYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<DspMainThemeItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(DspMainThemeItemExcelDto.class, headers, 4, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<DspMainThemeItemSetParamDto> arrSetParamDto = new ArrayList<>();
        String regId = loginCookieService.getUserInfo().getEmpId();
        excelData
            .stream()
            .forEach(item -> {
                // 값에 대한 체크
                if(!StringUtils.isEmpty(item.getThemeId()) || !StringUtils.isEmpty(item.getStoreType()) || !StringUtils.isEmpty(item.getItemNo()) || !StringUtils.isEmpty(item.getUseYn())) {
                    // 점포 유형 체크
                    if(item.getStoreType().equals("HYPER") || item.getStoreType().equals("CLUB") || item.getStoreType().equals("AURORA") || item.getStoreType().equals("EXP") || item.getStoreType().equals("DS")) {
                        DspMainThemeItemSetParamDto setDto = new DspMainThemeItemSetParamDto();

                        setDto.setDispDt(targetDisp);
                        setDto.setThemeItemType("ITEM");
                        setDto.setThemeId(Long.parseLong(item.getThemeId()));
                        setDto.setItemNo(item.getItemNo());
                        setDto.setImgUrl("");
                        setDto.setImgHeight(0);
                        setDto.setImgWidth(0);
                        setDto.setLinkInfo("");
                        setDto.setLinkType("");
                        setDto.setDispYn(item.getUseYn());
                        setDto.setItemStoreType(item.getStoreType());
                        setDto.setRegId(regId);

                        arrSetParamDto.add(setDto);
                    } else {
                        //점포유형이 잘못 되었습니다.
                        try {
                            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1010);
                        } catch (BusinessLogicException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    //빈 데이터가 존재 합니다.
                    try {
                        throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1011);
                    } catch (BusinessLogicException e) {
                        e.printStackTrace();
                    }
                }
            });

        resourceClient.postForResponseObject(ResourceRouteName.MANAGE, arrSetParamDto, "/manage/dspMain/setDspMainThemeItemByExcel", new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();

        return excelData;
    }

}
