package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimImageInfoDto {

    @ApiModelProperty(value = "", position = 1)
    public String acceptRange;

    @ApiModelProperty(value = "수정날짜", position = 2)
    public String lastModified;

    @ApiModelProperty(value = "파일타입", position = 3)
    public String contentType;

    @ApiModelProperty(value = "파일크기", position = 4)
    public String contentLength;

    @ApiModelProperty(value = "etag", position = 5)
    public String etag;


}
