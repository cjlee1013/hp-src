package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CouponPopListParamDto {
	private String schStartDt;

	private String schEndDt;

	private String schStoreType;

	private String schCouponType;

	private String schStatus;

	private String schType;

	private String schValue;
}
