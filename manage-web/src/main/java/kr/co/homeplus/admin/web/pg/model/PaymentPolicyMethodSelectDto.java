package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-결제수단 search DTO")
public class PaymentPolicyMethodSelectDto {
    private Long policyNo;
    private String siteType;
}
