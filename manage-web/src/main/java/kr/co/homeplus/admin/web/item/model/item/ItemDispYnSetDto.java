package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 노출여부 수정
 */
@Getter
@Setter
public class ItemDispYnSetDto {

	//상품번호
	private List<String> itemNo;

	//노출여부
	private String dispYn;

	//거래유형 (DS : 업체상품 , TD : 매장상품)
	private String mallType;

	//등록|수정자
	private String userId;
}
