package kr.co.homeplus.admin.web.manage.model.expBanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpBannerStoreSetParamDto {

    private Long bannerNo;
    private String storeId;
    private String regId;

}
