package kr.co.homeplus.admin.web.delivery.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupSearchGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShipCommonService {

  private final ResourceClient resourceClient;
  private final LoginCookieService loginCookieService;
  private final CodeService codeService;

  /**
   * UserCd 조회
   */
  public String getUserCd() {
    return loginCookieService.getUserInfo().getEmpId();
  }

  /**
   * UserNm 조회
   */
  public String getUserNm() {
    return loginCookieService.getUserInfo().getUserNm();
  }

  /**
   * UserId 조회
   */
  public String getUserId() {
    return loginCookieService.getUserInfo().getUserId();
  }

  /**
   * StoreId(점포코드) 조회
   */
  public String getStoreId() { return loginCookieService.getUserInfo().getStoreId(); }

  /**
   * SO권한 여부
   */
  public Boolean isStoreOffice() {
    if (StringUtils.isEmpty(this.getStoreId())) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Code 조회
   */
  public List<MngCodeGetDto> getCode(String gmcCd) {
    try {
      Map<String, List<MngCodeGetDto>> code = codeService.getCode(gmcCd);

      return code.get(gmcCd);
    } catch (Exception e) {
      return new ArrayList<>();
    }
  }

  /**
   * 점포정보 조회
   */
  public ShipStoreInfoGetDto getStoreInfo(String storeId) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/admin/shipManage/getStoreInfo?storeId=" + storeId,
        new ParameterizedTypeReference<ResponseObject<ShipStoreInfoGetDto>>() {}).getData();
  }

  /**
   * SO 점포정보 설정
   */
  public void setStoreInfo(Model model) {
    model.addAttribute("isSo", this.isStoreOffice());
    model.addAttribute("so_storeId", "");
    model.addAttribute("so_storeNm", "");
    model.addAttribute("so_storeType", "");

    if (this.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = this.getStoreInfo(this.getStoreId());

      if (ObjectUtils.isNotEmpty(storeInfo)) {
        model.addAttribute("so_storeId", this.getStoreId());
        model.addAttribute("so_storeNm", storeInfo.getStoreNm());
        model.addAttribute("so_storeType", storeInfo.getStoreType());
      }
    }
  }

  /**
   * 점포 리스트 조회
   */
  public List<StorePopupSearchGetDto> getStoreList(String storeType) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.ITEM,
        "/item/store/getStoreForSelectBox?storeType=" + storeType + "&storeRange=REAL",
        new ParameterizedTypeReference<ResponseObject<List<StorePopupSearchGetDto>>>() {}).getData();
  }

  /**
   * 배송Shift 리스트 조회
   */
  public List<String> getStoreShiftList(String storeId, String storeType) {
    return resourceClient.getForResponseObject(
        ResourceRouteName.SHIPPING,
        "/admin/shipManage/getStoreShiftList?storeId=" + storeId + "&storeType=" + storeType,
        new ParameterizedTypeReference<ResponseObject<List<String>>>() {}).getData();
  }

}
