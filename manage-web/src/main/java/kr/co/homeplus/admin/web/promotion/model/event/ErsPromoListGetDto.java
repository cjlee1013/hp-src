package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class ErsPromoListGetDto {
    @RealGridColumnInfo(headText = "사은행사 번호")
    private String eventCd;

    @RealGridColumnInfo(headText = "사은행사명", width = 200)
    private String eventNm;

    @RealGridColumnInfo(headText = "구매기준유형")
    private String eventTargetType;

    @RealGridColumnInfo(headText = "지급기준유형")
    private String eventGiveType;

    @RealGridColumnInfo(headText = "영수증 합산여부")
    private String receiptSumYn;

    @RealGridColumnInfo(headText = "연속식 적용여부")
    private String multipleYn;

    @RealGridColumnInfo(headText = "고객정보 출력여부")
    private String customerInfoPrtYn;

    @RealGridColumnInfo(headText = "지급구분")
    private String saleType;

    @RealGridColumnInfo(headText = "시작일시")
    private String eventStDate;

    @RealGridColumnInfo(headText = "종료일시")
    private String eventEdDate;

    @RealGridColumnInfo(headText = "확정여부")
    private String confirmFlag;

    @RealGridColumnInfo(headText = "취소여부")
    private String cancelFlag;

    @RealGridColumnInfo(headText = "온라인 사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일")
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일")
    private String chgDt;
}