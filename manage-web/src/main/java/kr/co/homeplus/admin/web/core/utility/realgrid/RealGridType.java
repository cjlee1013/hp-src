package kr.co.homeplus.admin.web.core.utility.realgrid;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Deprecated(forRemoval = true)
@Getter
@AllArgsConstructor
public enum RealGridType {
	MultiRowSelect("MultiRowSelect", "{ \"style\": \"block\" }","{ \"singleMode\": false, \"lookupDisplay\": true }","{\"rowFocusVisible\":true, \"showEmptyMessage\":true, \"emptyMessage\":\"검색 결과가 없습니다.\"}"),
	SingleRowSelect("SingleRowSelect", "{ \"style\": \"singleRow\" }","{ \"singleMode\": true, \"lookupDisplay\": true }","{\"showEmptyMessage\":true, \"emptyMessage\":\"검색 결과가 없습니다.\"}");

	private final String code;
	private final String option;
	private final String singleMode;
	private final String baseInfo;
}
