package kr.co.homeplus.admin.web.search.controller;

import kr.co.homeplus.admin.web.search.model.RelatedKeywordGetParamDto;
import kr.co.homeplus.admin.web.search.model.RelatedKeywordGridDto;
import kr.co.homeplus.admin.web.search.model.RelatedKeywordSetParamDto;
import kr.co.homeplus.admin.web.search.service.RelatedKeywordService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/related")
@RequiredArgsConstructor
public class RelatedKeywordController {

    private final RelatedKeywordService relatedKeywordService;

    @GetMapping(value = "/{relationType}")
    public String ExposureMain(Model model, @PathVariable("relationType") String relationType) {
        if(relationType.equalsIgnoreCase("INCLUDE")) {
            model.addAllAttributes(RealGridHelper.create("relatedKeywordListGridBaseInfo", RelatedKeywordGridDto.RelatedKeywordInclude.class));
        } else {
            model.addAllAttributes(RealGridHelper.create("relatedKeywordListGridBaseInfo", RelatedKeywordGridDto.RelatedKeywordExclude.class));
        }
        return "/search/related" + relationType.toLowerCase();
    }

    @ResponseBody
    @GetMapping(value = {"/getItemList.json"})
    public Object getExposureItemList(RelatedKeywordGetParamDto param) {
        return relatedKeywordService.getExposureItemList(param).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/setItem.json"}, method = RequestMethod.POST)
    public Object setExposureItem(RelatedKeywordSetParamDto param) {
        return relatedKeywordService.setExposureItem(param);
    }
}
