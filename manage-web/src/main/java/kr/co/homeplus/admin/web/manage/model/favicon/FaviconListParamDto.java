package kr.co.homeplus.admin.web.manage.model.favicon;

import lombok.Data;

@Data
public class FaviconListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchSiteType;

    private String searchDevice;

    private String searchDispSite;

    private String searchUseYn;

    private String schType;

    private String schKeyword;

}
