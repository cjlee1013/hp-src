package kr.co.homeplus.admin.web.delivery.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.ReserveShipPlaceManageModel;
import kr.co.homeplus.admin.web.delivery.model.ReserveShipPlaceManageSelectModel;
import kr.co.homeplus.admin.web.delivery.service.ReserveShipPlaceManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * Admin > 배송관리 > 배송관리 > 선물세트 발송지 관리
 */
@Controller
@RequestMapping("/escrow")
public class ReserveShipPlaceManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final ReserveShipPlaceManageService reserveShipPlaceManageService;
    private final ExcelUploadService excelUploadService;

    public ReserveShipPlaceManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            ReserveShipPlaceManageService reserveShipPlaceManageService, ExcelUploadService excelUploadService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.reserveShipPlaceManageService = reserveShipPlaceManageService;
        this.excelUploadService = excelUploadService;
    }

    /**
     * 선물세트 발송지 관리 메인페이지 호출
     */
    @GetMapping("/shipManage/reserveShipPlaceManageMain")
    public String reserveShipPlaceManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_REGERVEPRD_SRV_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("reserveShipPlaceManageGridBaseInfo", ReserveShipPlaceManageModel.class));
        return "/delivery/reserveShipPlaceManageMain";
    }

    /**
     * 선물세트 발송지 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/shipManage/getReserveShipPlaceMngList.json")
    public List<ReserveShipPlaceManageModel> getReserveShipPlaceMngList(@ModelAttribute ReserveShipPlaceManageSelectModel reserveShipPlaceManageSelectModel) throws Exception {
        return reserveShipPlaceManageService.getReserveShipPlaceMngList(reserveShipPlaceManageSelectModel);
    }

    /**
     * 선물세트 발송지 저장
     */
    @ResponseBody
    @PostMapping("/shipManage/saveReserveShipPlace.json")
    public ResponseObject<Object> saveReserveShipPlace(@RequestBody ReserveShipPlaceManageModel reserveShipPlaceManageModel) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        reserveShipPlaceManageModel.setRegId(userCd);
        reserveShipPlaceManageModel.setChgId(userCd);
        return reserveShipPlaceManageService.saveReserveShipPlace(reserveShipPlaceManageModel);
    }

    /**
     * 선물세트 발송지 리스트 저장 (다중건)
     */
    @ResponseBody
    @PostMapping("/shipManage/saveReserveShipPlaceList.json")
    private ResponseObject<Object> saveReserveShipPlaceList(@RequestBody List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        for (ReserveShipPlaceManageModel reserveShipPlaceManageModel : reserveShipPlaceManageModelList) {
            reserveShipPlaceManageModel.setRegId(userCd);
            reserveShipPlaceManageModel.setChgId(userCd);
        }
        return reserveShipPlaceManageService.saveReserveShipPlaceList(reserveShipPlaceManageModelList);
    }

    /**
     * 선물세트 발송지 리스트 삭제
     */
    @ResponseBody
    @PostMapping("/shipManage/deleteReserveShipPlaceList.json")
    public ResponseObject<Object> deleteReserveShipPlaceList(@RequestBody List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList) throws Exception {
        return reserveShipPlaceManageService.deleteReserveShipPlaceList(reserveShipPlaceManageModelList);
    }

    /**
     * 엑셀 업로드 팝업 호출
     */
    @GetMapping("/popup/reserveShipExcelUploadPop")
    public String excelUploadPop(Model model
            , @RequestParam(value = "callback", defaultValue = "") String callback
            , @RequestParam(value = "templateUrl", defaultValue = "") String templateUrl) {
        model.addAttribute("callback", callback);
        model.addAttribute("templateUrl", templateUrl);
        return "/delivery/pop/reserveShipExcelUploadPop";
    }

    /**
     * 선물세트 발송지 엑셀 리스트 저장
     */
    @ResponseBody
    @PostMapping("/shipManage/saveReserveShipPlaceExcelList.json")
    public Map<String, Integer> saveReserveShipPlaceExcelList(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<ReserveShipPlaceManageModel> result;

        // 엑셀 헤더정보 설정, headerName 이 다른 경우 read 에러 발생
        ExcelHeaders headers = new ExcelHeaders.Builder()
                .header(0, "발송일 (YYYY-MM-DD)", "shipDt", true)
                .header(1, "점포 (Y/N)", "shipPlaceStoreYn", true)
                .header(2, "중앙(안성) (Y/N)", "shipPlaceAnseongYn", true)
                .header(3, "중앙(함안) (Y/N)", "shipPlaceHamanYn", true)
                .build();
        // 엑셀 업로드 옵션 설정, 클래스정보, 헤더정보, read 시 시작 row/cell 지정
        ExcelUploadOption<ReserveShipPlaceManageModel> excelUploadOption = new ExcelUploadOption.Builder<>(
                ReserveShipPlaceManageModel.class, headers, 1, 0)
                .build();
        // 엑셀 파일 read
        try {
            result = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1013);
        }

        // read 한 데이터 검증 및 저장할 리스트 생성
        Integer totalCnt = result.size();
        Integer successCnt = 0;
        Integer errorCnt = 0;
        List<ReserveShipPlaceManageModel> reserveShipPlaceManageModelList = new ArrayList<>();
        for (ReserveShipPlaceManageModel reserveShipPlaceManageModel : result) {
            // 1. 발송일 - 빈 값 체크
            if (StringUtils.isEmpty(reserveShipPlaceManageModel.getShipDt())) {
                errorCnt++; break;
            }
            // 2. 발송일 - 패턴체크 (yyyy-mm-dd)
            if (!reserveShipPlaceManageModel.getShipDt().matches("(19[0-9]{2}|[0-9]{4})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)")) {
                errorCnt++; break;
            }
            // 3. 점포 - 빈 값 체크
            if (StringUtils.isEmpty(reserveShipPlaceManageModel.getShipPlaceStoreYn())) {
                errorCnt++; break;
            }
            // 4. 점포 - 패턴체크 (Y|N)
            if (!reserveShipPlaceManageModel.getShipPlaceStoreYn().matches("(?i)(Y|N)")) {
                errorCnt++; break;
            }
            // 5. 중앙(안성) - 빈 값 체크
            if (StringUtils.isEmpty(reserveShipPlaceManageModel.getShipPlaceAnseongYn())) {
                errorCnt++; break;
            }
            // 6. 중앙(안성) - 패턴체크 (Y|N)
            if (!reserveShipPlaceManageModel.getShipPlaceAnseongYn().matches("(?i)(Y|N)")) {
                errorCnt++; break;
            }
            // 7. 중앙(함안) - 빈 값 체크
            if (StringUtils.isEmpty(reserveShipPlaceManageModel.getShipPlaceHamanYn())) {
                errorCnt++; break;
            }
            // 8. 중앙(함안) - 패턴체크 (Y|N)
            if (!reserveShipPlaceManageModel.getShipPlaceHamanYn().matches("(?i)(Y|N)")) {
                errorCnt++; break;
            }
            // 9. 발송지 여부를 모두 대문자로 변경
            reserveShipPlaceManageModel.setShipPlaceStoreYn(reserveShipPlaceManageModel.getShipPlaceStoreYn().toUpperCase());
            reserveShipPlaceManageModel.setShipPlaceAnseongYn(reserveShipPlaceManageModel.getShipPlaceAnseongYn().toUpperCase());
            reserveShipPlaceManageModel.setShipPlaceHamanYn(reserveShipPlaceManageModel.getShipPlaceHamanYn().toUpperCase());

            // 검증 모두 통과하면 data insert 할 array 에 담기
            successCnt++;
            reserveShipPlaceManageModelList.add(reserveShipPlaceManageModel);
        }

        // 데이터 검증 실패건이 없는 경우 등록 수행 (기획서 정의)
        if (errorCnt == 0) {
            this.saveReserveShipPlaceList(reserveShipPlaceManageModelList);
        }

        // 처리결과 return
        Map<String, Integer> rtnMap = new HashMap<>();
        rtnMap.put("totalCnt", totalCnt);
        rtnMap.put("successCnt", successCnt);
        rtnMap.put("errorCnt", errorCnt);

        return rtnMap;
    }
}