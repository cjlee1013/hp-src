package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraMainThemeItemExcelDto;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraThemeGetDto;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraThemeItemGetDto;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraThemeItemSetExcelParamDto;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraThemeItemSetListParamDto;
import kr.co.homeplus.admin.web.item.model.auroraThemeItem.AuroraThemeSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class AuroraThemeItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;
    private final ExcelUploadService excelUploadService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 테마상품 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/auroraThemeItem/auroraThemeMain", method = RequestMethod.GET)
    public String auroraThemeMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"				    // 사용여부
            , "dsk_device_gubun"        // 노출 구분
            , "aurora_banner_type"      // 배너 설정
            , "aurora_banner_title"     // 배너 타이틀 사용여부
            , "dsp_popup_link_type"     // 링크타입
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        model.addAllAttributes(RealGridHelper.create("auroraThemeGridBaseInfo", AuroraThemeGetDto.class));
        model.addAllAttributes(RealGridHelper.create("auroraThemeItemGridBaseInfo", AuroraThemeItemGetDto.class));

        return "/item/auroraThemeMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 테마 리스트.
     *
     * @param
     * @return List<AuroraThemeGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/auroraThemeItem/getAuroraTheme.json"}, method = RequestMethod.GET)
    public List<AuroraThemeGetDto> getAuroraTheme() {
        String apiUri = "/item/auroraThemeItem/getAuroraTheme";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<AuroraThemeGetDto>>>() {};
        return (List<AuroraThemeGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 테마상품 리스트
     *
     * @param
     * @return List<AuroraThemeItemGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/auroraThemeItem/getAuroraThemeItem.json"}, method = RequestMethod.GET)
    public List<AuroraThemeItemGetDto> getAuroraThemeItem(@RequestParam(name = "themeId", defaultValue = "0") Long themeId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("themeId", themeId));

        String apiUriRequest = "/item/auroraThemeItem/getAuroraThemeItem" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<AuroraThemeItemGetDto>>>() {})
            .getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 테마 등록/수정
     *
     * @param auroraThemeSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @PostMapping(value = {"/auroraThemeItem/setAuroraTheme.json"})
    public ResponseResult setAuroraTheme(@Valid @RequestBody AuroraThemeSetParamDto auroraThemeSetParamDto) {
        String apiUri = "/item/auroraThemeItem/setAuroraTheme";
        auroraThemeSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, auroraThemeSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 테마 등록/수정
     *
     * @param auroraThemeItemSetListParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @PostMapping(value = {"/auroraThemeItem/setAuroraThemeItem.json"})
    public ResponseResult setAuroraThemeItem(@Valid @RequestBody AuroraThemeItemSetListParamDto auroraThemeItemSetListParamDto) {
        String apiUri = "/item/auroraThemeItem/setAuroraThemeItem";
        auroraThemeItemSetListParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, auroraThemeItemSetListParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadAuroraMainThemeItemPop")
    public String uploadAuroraMainThemeItemPop(Model model, @RequestParam(value = "callback") String callBackScript) {
        model.addAttribute("callBackScript", callBackScript);
        return "/item/pop/uploadAuroraMainThemeItemPop";
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 상품 일괄등록
     */
    @ResponseBody
    @PostMapping("/auroraThemeItem/setAuroraThemeItemExcel.json")
    public void setAuroraThemeItemExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<AuroraMainThemeItemExcelDto> excelData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "테마번호", "themeId", true)
            .header(1, "상품번호", "itemNo", true)
            .header(2, "사용여부", "useYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<AuroraMainThemeItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(AuroraMainThemeItemExcelDto.class, headers, 1, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            //파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 1000;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 1,000개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 1000);
        }

        // 3-2. 데이터 검증하여 실제 insert 할 model list 생성
        List<AuroraThemeItemSetExcelParamDto> arrSetParamDto = new ArrayList<>();
        String regId = loginCookieService.getUserInfo().getEmpId();
        excelData
            .stream()
            .forEach(item -> {
                if(!StringUtils.isEmpty(item.getThemeId()) || !StringUtils.isEmpty(item.getItemNo()) || !StringUtils.isEmpty(item.getUseYn())) {
                    AuroraThemeItemSetExcelParamDto setDto = new AuroraThemeItemSetExcelParamDto();

                    setDto.setThemeId(Long.parseLong(item.getThemeId()));
                    setDto.setItemNo(item.getItemNo());
                    setDto.setUseYn(item.getUseYn());
                    setDto.setPriority(0); // Last Index 처리.
                    setDto.setRegId(regId);

                    arrSetParamDto.add(setDto);
                }
            });

        resourceClient.postForResponseObject(ResourceRouteName.ITEM, arrSetParamDto, "/item/auroraThemeItem/setAuroraThemeItemByExcel", new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();
    }

}
