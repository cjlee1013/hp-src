package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeploySetParamDto {
    private Long seqNo;
    private String serviceCd;
    private String appType;
    private String useYn;
    private String privateDomain;
    private String authUsername;
    private String authPassword;
}
