package kr.co.homeplus.admin.web.pg.service;

import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageDto;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageHistoryDto;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgMidManageDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PgDivideManageService {
    private final ResourceClient resourceClient;

    /**
     * PG코드 조회
     * @param pgKind
     * @param useYn
     * @return
     */
    public List<PgMidManageDto> getPgMidManageList(String pgKind, String useYn) {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("pgKind", pgKind));
        setParameterList.add(SetParameter.create("useYn", useYn));
        String apiUri = EscrowConstants.ESCROW_GET_PG_MID_MANAGE + StringUtil.getParameter(setParameterList);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgMidManageDto>>>() {};

        return (List<PgMidManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 기본배분율 조회
     * @param pgDivideManageSelectDto
     * @return
     */
    public List<PgDivideManageDto> getPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) {
        pgDivideManageSelectDto.setPgMngType("BASIC");
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_DIVIDE_RATE_LIST, PgDivideManageSelectDto.class, pgDivideManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgDivideManageDto>>>() {};

        return (List<PgDivideManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 기본배분율 히스토리 조회
     * @param pgDivideManageSelectDto
     * @return
     */
    public List<PgDivideManageHistoryDto> getPgDivideRateHistoryList(PgDivideManageSelectDto pgDivideManageSelectDto) {
        pgDivideManageSelectDto.setPgMngType("BASIC");
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PG_DIVIDE_RATE_LIST, PgDivideManageSelectDto.class, pgDivideManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgDivideManageHistoryDto>>>() {};

        return (List<PgDivideManageHistoryDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    /**
     * 기본배분율 저장
     * @param pgDivideManageDto
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> savePgDivideRate(PgDivideManageDto pgDivideManageDto) throws Exception{
        String apiUri = EscrowConstants.ESCROW_SAVE_PG_DIVIDE_RATE;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Object>>() {};

        return (ResponseObject<Object>) resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, pgDivideManageDto, apiUri, typeReference);
    }

    /**
     * 결제정책 PG배분율 조회
     * @param pgDivideManageSelectDto
     * @return
     */
    public List<PgDivideManageDto> getPaymentPolicyPgDivideRateList(PgDivideManageSelectDto pgDivideManageSelectDto) {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_PAYMENT_POLICY_PG_DIVIDE_RATE_LIST, PgDivideManageSelectDto.class, pgDivideManageSelectDto);
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<PgDivideManageDto>>>() {};

        return (List<PgDivideManageDto>) resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }
}
