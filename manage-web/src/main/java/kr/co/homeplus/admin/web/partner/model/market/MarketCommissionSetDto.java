package kr.co.homeplus.admin.web.partner.model.market;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCommissionSetDto {

    //일련번호
    private Long commissionSeq;
    //수수료
    private BigDecimal commissionRate;
    //적용시작일
    private String applyStartDt;
    //적용종료일
    private String applyEndDt;
    //사용여부
    private String useYn;
    //변경여부
    private String changeYn;

}

