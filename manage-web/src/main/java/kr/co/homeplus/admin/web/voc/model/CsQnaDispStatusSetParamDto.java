package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CsQnaDispStatusSetParamDto {

    private Integer qnaNo;
    private String displayStatus;
    private String regId;

}
