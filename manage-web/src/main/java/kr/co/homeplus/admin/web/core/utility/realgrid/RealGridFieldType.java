package kr.co.homeplus.admin.web.core.utility.realgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Deprecated(forRemoval = true)
@RequiredArgsConstructor
public enum RealGridFieldType {
    TEXT("text"),
    NUMBER("number"),
    DATETIME("datetime"),
    BOOL("bool");

    @Getter
    private final String type;
}
