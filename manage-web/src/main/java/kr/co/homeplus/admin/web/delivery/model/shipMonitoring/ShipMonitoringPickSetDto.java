package kr.co.homeplus.admin.web.delivery.model.shipMonitoring;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회수지연조회 요청")
public class ShipMonitoringPickSetDto {
    @ApiModelProperty(notes = "일자 타입")
    private String schShipDtType;

    @ApiModelProperty(notes = "시작일(Start)")
    private String schShipStartDt;

    @ApiModelProperty(notes = "종료일(End)")
    private String schShipEndDt;

    @ApiModelProperty(notes = "점포유형")
    private String schStoreType;

    @ApiModelProperty(notes = "점포ID")
    private String schStoreId;

    @ApiModelProperty(notes = "수거방법")
    private String schPickShipType;

    @ApiModelProperty(notes = "클레임구분")
    private String schClaimType;

    @ApiModelProperty(notes = "수거상태")
    private String schPickStatus;

    @ApiModelProperty(notes = "검색조건 타입")
    private String schKeywordType;

    @ApiModelProperty(notes = "검색어")
    private String schKeyword;

    @ApiModelProperty(notes = "수거지연일")
    private String schPickDelayDay;

    @ApiModelProperty(notes = "마켓유형")
    private String schMarketType;
}