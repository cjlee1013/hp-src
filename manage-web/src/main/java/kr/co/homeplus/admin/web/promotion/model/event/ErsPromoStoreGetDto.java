package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ErsPromoStoreGetDto {
    @RealGridColumnInfo(headText = "점포ID")
    private Integer storeId;

    @RealGridColumnInfo(headText = "점포명")
    private String storeNm;

    @RealGridColumnInfo(headText = "취소구분")
    private String cancelFlag;

    @RealGridColumnInfo(headText = "취소일자")
    private String cancelDate;
}