package kr.co.homeplus.admin.web.user.model.user;

import java.util.List;
import lombok.Data;

@Data
public class GradeStatResponseDto {

    private List<GradeStatChangeDto> gradeStatChangeList;

}
