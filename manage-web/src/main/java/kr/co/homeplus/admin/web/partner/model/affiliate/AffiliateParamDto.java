package kr.co.homeplus.admin.web.partner.model.affiliate;

import lombok.Data;

@Data
public class AffiliateParamDto {

    // 등록 시작일
    private String schRegStartDate;
    // 등록 종료일
    private String schRegEndDate;
    // 제휴유형
    private String schAffiliateType;
    // 회원상태
    private String schPartnerStatus;
    // 사업자 유형
    private String schOperatorType;
    // 검색 조건
    private String schType;
    // 검색 키워드
    private String schKeyword;

}
