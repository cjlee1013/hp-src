package kr.co.homeplus.admin.web.promotion.model.coupon;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponIssueMemberSetDto {
    private List<String> issueNoArr;
    private long couponNo;
    private long userNo;
    private String userId;
    private String userNm;
    private String mhcUcid;
    private String empNo;
}
