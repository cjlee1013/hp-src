package kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemePeriodItemSetParamDto {

    private Long itemSeq;
    private Long themeId;
    private String itemNo;
    private int priority;
    private String dispYn;
    private String regId;

}
