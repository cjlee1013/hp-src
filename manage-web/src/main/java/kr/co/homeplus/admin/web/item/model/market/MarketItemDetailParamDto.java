package kr.co.homeplus.admin.web.item.model.market;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel("외부연동현황관리 parameter")
public class MarketItemDetailParamDto {

    //외부연동 사이트
    private String itemNo;

    //대카테고리
    private String partnerId;
}