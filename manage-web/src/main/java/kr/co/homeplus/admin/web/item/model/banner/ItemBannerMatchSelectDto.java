package kr.co.homeplus.admin.web.item.model.banner;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemBannerMatchSelectDto {
    //배너번호
    private Long bannerNo;

    //상품번호
    private String itemNo;

    //상품명
    private String itemNm;

    //사용여부
    private String useYn;

    //등록자
    private String regNm;

    //수정자
    private String chgNm;

}
