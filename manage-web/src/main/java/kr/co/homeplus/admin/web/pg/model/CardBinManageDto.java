package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class CardBinManageDto {
    private long cardBinNoSeq;
    private String cardBinNo;
    private String methodCd;
    private String cardBinKind;
    private String cardNm;
    private String useYn;
    private String regId;
    private String regDt;
    private String chgId;
    private String chgDt;
}
