package kr.co.homeplus.admin.web.promotion.model.promo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BenefitZoneListParamDto {
    private String schDateType;

    private String schStartDt;

    private String schEndDt;

    private String schDispYn;

    private String schType;

    private String schValue;
}