package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "대체주문 정보 DTO")
public class OrderSearchSubstitutionDto {
    @ApiModelProperty(value = "대체 주문번호")
    @RealGridColumnInfo(headText = "대체 주문번호")
    private String purchaseOrderNo;
    @ApiModelProperty(value = "대체 상품번호")
    @RealGridColumnInfo(headText = "대체 상품번호")
    private String itemNo;
    @ApiModelProperty(value = "대체 상품명")
    @RealGridColumnInfo(headText = "대체 상품명", width = 200, columnType = RealGridColumnType.NONE)
    private String itemNm1;
    @ApiModelProperty(value = "수량")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "수량", columnType = RealGridColumnType.NUMBER_C, width = 150)
    private String itemQty;
    @ApiModelProperty(value = "상품금액")
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", columnType = RealGridColumnType.PRICE, width = 150)
    private String orderPrice;
    @ApiModelProperty(value = "대체주문일시")
    @RealGridColumnInfo(headText = "대체주문일시", width = 150)
    private String orderDt;
}
