package kr.co.homeplus.admin.web.manage.model.clubToday;

import lombok.Data;

@Data
public class ClubTodayListParamDto {

    private String searchPeriodType;

    private String searchStartDt;

    private String searchEndDt;

    private String searchDispYn;

    private String searchType;

    private String searchKeyword;

}
