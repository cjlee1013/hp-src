package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

@Data
//상품정보고시관리 Set Entry
public class ItemNoticeGetDto {

    //고시항목번호
    private int noticeNo;

    //정보고시항목명
    private String noticeDesc;

    //정보고시항목명 (TD용)
    private String noticeHtml;

}
