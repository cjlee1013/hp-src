package kr.co.homeplus.admin.web.voc.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemReviewExMileageSetParamDto {

    private String excKind;
    private String cateKind;
    private Integer cateCd;
    private String itemNo;
    private String useYn = "Y";
    private String regId;

}
