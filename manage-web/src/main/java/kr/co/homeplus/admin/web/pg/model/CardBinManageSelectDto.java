package kr.co.homeplus.admin.web.pg.model;

import lombok.Data;

@Data
public class CardBinManageSelectDto {
    private String schCardBinNo;
    private String schMethodCd;
    private String schUseYn;
}
