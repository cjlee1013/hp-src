package kr.co.homeplus.admin.web.partner.model.market;

import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryMapSetDto {

    //등록|수정자
    private String userId;

    //마켓연동 카테고리 정보
    List<MarketCategoryMapValueSetDto> mapList;

}
