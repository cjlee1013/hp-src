package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirtualStoreItemSetDto {
    private int storeId;
    private String itemNo;
    private String dealYn;
    private long shipPolicyNo;
    private String regId;
}