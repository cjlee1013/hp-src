package kr.co.homeplus.admin.web.item.model.onedayItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnedayItemCntGetDto {

    private String dispDt;
    private int itemCnt;

}
