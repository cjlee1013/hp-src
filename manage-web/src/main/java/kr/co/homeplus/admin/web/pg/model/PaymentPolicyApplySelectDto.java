package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 배분율 관리 > 결제정책 관리-적용대상 search DTO")
public class PaymentPolicyApplySelectDto {
    private Long applySeq;
    private Long policyNo;
    private String applyCd;
    private Integer applyPriority;
    private String applyTargetNm;
}
