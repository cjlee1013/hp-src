package kr.co.homeplus.admin.web.settle.model.adjust;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "정산 관리 > 정산조정 > 정산조정 대상 아이템 조회")
public class AdjustItemGetDto {
    @ApiModelProperty(notes = "판매업체ID")
    private String partnerId;

    @ApiModelProperty(notes = "판매업체명")
    private String partnerNm;

    @ApiModelProperty(notes = "업체코드")
    private String vendorCd;

    @ApiModelProperty(notes = "상품명")
    private String itemNm;
}
