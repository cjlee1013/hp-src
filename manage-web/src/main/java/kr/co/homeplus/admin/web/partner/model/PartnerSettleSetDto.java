package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSettleSetDto implements Serializable {

	private String bankCd;
	private String bankAccountNo;
	private String depositor;
	private String settleCycleType;
	private String settlePaymentYn;

}
