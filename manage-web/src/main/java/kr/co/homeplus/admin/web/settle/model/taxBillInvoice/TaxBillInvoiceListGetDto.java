package kr.co.homeplus.admin.web.settle.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
    
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxBillInvoiceListGetDto {
    @ApiModelProperty(value = "발행SRL")
    @RealGridColumnInfo(headText = "발행SRL", sortable = true, width = 100)
    private String taxKeyNo;

    @ApiModelProperty(value = "발행일")
    @RealGridColumnInfo(headText = "발행일", sortable = true, width = 100)
    private String issueDt;

    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", sortable = true, width = 100)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", sortable = true, width = 100)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", sortable = true, width = 100)
    private String partnerNm;

    @ApiModelProperty(value = "매출유형")
    @RealGridColumnInfo(headText = "매출유형", sortable = true, width = 100)
    private String salesKind;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", sortable = true, width = 100)
    private String businessNm;

    @ApiModelProperty(value = "사업자번호")
    @RealGridColumnInfo(headText = "사업자번호", sortable = true, width = 100)
    private String partnerNo;

    @ApiModelProperty(value = "대표자명")
    @RealGridColumnInfo(headText = "대표자명", sortable = true, width = 100)
    private String partnerOwner;

    @ApiModelProperty(value = "담당자명")
    @RealGridColumnInfo(headText = "담당자명", sortable = true, width = 100)
    private String mngId;

    @ApiModelProperty(value = "담당자이메일")
    @RealGridColumnInfo(headText = "담당자이메일", sortable = true, width = 200)
    private String mngEmail;

    @ApiModelProperty(value = "합계금액")
    @RealGridColumnInfo(headText = "서비스 이용료", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sumAmt;

    @ApiModelProperty(value = "공급가액")
    @RealGridColumnInfo(headText = "공급가액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long supplyAmt;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long vatAmt;

    @ApiModelProperty(value = "등록일시")
    @RealGridColumnInfo(headText = "등록일시", sortable = true, width = 160)
    private String regDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", sortable = true, width = 150)
    private String regId;
}
