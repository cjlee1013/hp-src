package kr.co.homeplus.admin.web.app.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AppStoreReviewControlParam extends AppStoreReviewControlInfo {

    private int seq;
    private String empId;
    private String empNm;
}
