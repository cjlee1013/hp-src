package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PromoPopListParamDto {
	private String promoType;

	private String schSiteType;

	private String schStoreType;

	private String schDateType;

	private String schStartDt;

	private String schEndDt;

	private String schType;

	private String schValue;
}
