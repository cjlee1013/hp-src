package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.search.model.TypoGetParamDto;
import kr.co.homeplus.admin.web.search.model.TypoSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TypoService {
    private final ResourceClient resourceClient;

    public ResponseObject getTypoSearch(TypoGetParamDto param) {
        String apiUri = StringUtil.getRequestString("/dictionary/getTypoCorrectList", TypoGetParamDto.class, param);
        return getResponseObject(apiUri);
    }
    public ResponseObject getCandidateList() {
        return getResponseObject("/statistics/getTypoCandidateList");
    }
    public ResponseObject setTypoItem(TypoSetParamDto param) {
        String apiUri = "/dictionary/insertTypoCorrect";
        return getResponseObject(apiUri, param);
    }
    public ResponseObject setTypoItem(long typoId) {
        String apiUri = "/dictionary/deleteTypoCorrect/" + typoId;
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
