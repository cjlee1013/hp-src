package kr.co.homeplus.admin.web.claim.controller;


import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.claim.model.ClaimGiftItemListDto;
import kr.co.homeplus.admin.web.claim.model.ClaimProcessReqSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShipCompleteSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShippingSetDto;
import kr.co.homeplus.admin.web.claim.model.ClaimShippingStatusSetDto;
import kr.co.homeplus.admin.web.claim.model.DiscountInfoListDto;
import kr.co.homeplus.admin.web.claim.model.PromoInfoListDto;
import kr.co.homeplus.admin.web.claim.service.ClaimPopupService;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.order.utils.ObjectUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/popup")
@RequiredArgsConstructor
public class ClaimPopupController {

    private final CodeService codeService;

    private final ClaimPopupService claimPopupService;

    /**
     * 클레임 상태변경 팝업창 호출
     * processType 값으로 Approve : 승인, return : 반품, Exchange : 교환 팝업창이 호출된다.
     * @return
     */
    @GetMapping("/getClaimProcessReqPop")
    public String getClaimProcessReqPop(Model model, HttpServletRequest request) throws Exception {
        String claimTypeName = "";
        String url = "/claim/pop/claim"+request.getParameter("processType")+"Pop";
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        model.addAttribute("claimType", XssPreventer.escape(request.getParameter("claimType")));
        model.addAttribute("claimPickShippingNo", ObjectUtils.toLong(request.getParameter("claimPickShippingNo")));

        switch (request.getParameter("claimType")){
            case "C" :
                claimTypeName = "취소";
                break;
            case "R" :
                claimTypeName = "반품";
                break;
            case "X" :
                claimTypeName = "교환";
                break;
        }
        model.addAttribute("claimTypeName", claimTypeName);

        //보류, 거부인 경우 클레임 사유 코드 전달
        if(request.getParameter("processType").equalsIgnoreCase("Reject") || request.getParameter("processType").equalsIgnoreCase("Pending")){
            Map<String, List<MngCodeGetDto>> code = codeService.getCode("claim_reason_type");
            model.addAttribute("claimReasonType", code.get("claim_reason_type"));
        }
        return url;
    }

    /**
     * 수거상태변경 팝업창 호출
     * pickType Request : 요청, Complete : 완료
     * @return
     */
    @GetMapping("/getPickProcessReqPop")
    public String getPickProcessReqPop(Model model, HttpServletRequest request) throws Exception {
        String url = "/claim/pop/pick"+request.getParameter("pickType")+"Pop";
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("dlv_cd");
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimPickShippingNo", ObjectUtils.toLong(request.getParameter("claimPickShippingNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        return url;
    }

    /**
     * 환불수단변경 팝업창 호출
     * @return
     */
    @GetMapping("/getChangePaymentTypePop")
    public String getChangePaymentTypePop(Model model, HttpServletRequest request) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode("bank_code");
        model.addAttribute("bankCode", code.get("bank_code"));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        model.addAttribute("claimType", XssPreventer.escape(request.getParameter("claimType")));
        model.addAttribute("buyerNm", XssPreventer.escape(request.getParameter("buyerNm")));
        return "/claim/pop/changePaymentTypePop";
    }

    /**
     * 수거/배송 방법변경 팝업창 호출
     * @return
     */
    @GetMapping("/getChangeDeliveryTypePop")
    public String getChangeDeliveryTypePop(Model model, HttpServletRequest request) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "dlv_cd"                                // 택배사 코드
        );
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        model.addAttribute("claimType", XssPreventer.escape(request.getParameter("claimType")));
        model.addAttribute("reqType", XssPreventer.escape(request.getParameter("reqType")));
        return "/claim/pop/changeDeliveryTypePop";
    }


    /**
     * 교환배송 팝업창 호출
     * @return
     */
    @GetMapping("/exchShippingPop")
    public String exchShippingPop(Model model, HttpServletRequest request) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode("dlv_cd");
        model.addAttribute("dlvCd", code.get("dlv_cd"));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        return "/claim/pop/exchShippingPop";
    }

    /**
     * 교환완료 팝업창 호출
     * @return
     */
    @GetMapping("/exchCompletePop")
    public String exchCompletePop(Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("claimExchShippingNo", ObjectUtils.toLong(request.getParameter("claimExchShippingNo")));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAttribute("claimBundleNo", ObjectUtils.toLong(request.getParameter("claimBundleNo")));
        return "/claim/pop/exchCompletePop";
    }

    /**
     * 판매자정보 팝업창 호출
     * **/
    @GetMapping("/getSellerInfoPop")
    public String getSellerInfoPop(Model model, HttpServletRequest request){
        model.addAttribute("purchaseOrderNo", ObjectUtils.toLong(request.getParameter("purchaseOrderNo")));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        return "/claim/pop/sellerInfoPop";
    }

    /**
     * 차감할인정보 팝업창 호출
     * @return
     */
    @GetMapping("/discountInfoPop")
    public String discountInfoPop(Model model, HttpServletRequest request) {
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAllAttributes(RealGridHelper.create("discountInfoListGridBaseInfo", DiscountInfoListDto.class));
        return "/claim/pop/discountInfoPop";
    }

    /**
     * 차감 할인정보 조회
     * @return DiscountInfoListDto
     */
    @GetMapping("/getDiscountList.json")
    @ResponseBody
    public List<DiscountInfoListDto> getDiscountList(@RequestParam(value = "claimNo") long claimNo) {
        return claimPopupService.getDiscountInfoList(claimNo);
    }

    /**
     * 차감할인정보 팝업창 호출
     * @return
     */
    @GetMapping("/claimGiftInfoPop")
    public String claimGiftInfoPop(Model model, HttpServletRequest request) {
        model.addAttribute("purchaseOrderNo", ObjectUtils.toLong(request.getParameter("purchaseOrderNo")));
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAllAttributes(
            RealGridHelper.create("claimGiftItemGridBaseInfo", ClaimGiftItemListDto.class));
        return "/claim/pop/claimGiftInfoPop";
    }

    /**
     * 취소사은행사 조회
     * @return DiscountInfoListDto
     */
    @GetMapping("/claimGiftInfoList.json")
    @ResponseBody
    public List<ClaimGiftItemListDto> claimGiftInfoList(@RequestParam(value = "claimNo") String claimNo) {
        return claimPopupService.claimGiftInfoList(claimNo);
    }

    /**
     * 차감할인정보 팝업창 호출
     * @return
     */
    @GetMapping("/promoInfoPop")
    public String promoInfoPop(Model model, HttpServletRequest request) {
        model.addAttribute("claimNo", ObjectUtils.toLong(request.getParameter("claimNo")));
        model.addAllAttributes(RealGridHelper.create("promoInfoListGridBaseInfo", PromoInfoListDto.class));
        return "/claim/pop/promoInfoPop";
    }

    /**
     * 차감 행사정보 조회
     * @return PromoInfoListDto
     */
    @GetMapping("/getPromoInfoList.json")
    @ResponseBody
    public List<PromoInfoListDto> getPromoInfoList(@RequestParam(value = "claimNo") long claimNo) {
        return claimPopupService.getPromoInfoList(claimNo);
    }

    /**
     * 클레임 승인/철회/보류/거부 요청
     * @return ClaimProcessRegGetDto
     */
    @PostMapping("/reqClaimProcess.json")
    @ResponseBody
    public ResponseObject<String> reqClaimProcess(@RequestBody ClaimProcessReqSetDto claimProcessReqSetDto) throws Exception {
        return claimPopupService.reqClaimProcess(claimProcessReqSetDto);
    }

    /**
     * 수거상태 변경 요청
     * @param claimShippingStatusSetDto
     */
    @PostMapping("/reqChgPickStatus.json")
    @ResponseBody
    public ResponseObject<Boolean> reqChgPickStatus(@RequestBody ClaimShippingStatusSetDto claimShippingStatusSetDto) throws Exception {
        return claimPopupService.reqChgPickStatus(claimShippingStatusSetDto);
    }

    /**
     * 반품 수거완료 변경 요청
     * @param claimShipCompleteSetDto
     */
    @PostMapping("/reqPickShipComplete.json")
    @ResponseBody
    public ResponseObject<String> reqPickShipComplete(@RequestBody ClaimShipCompleteSetDto claimShipCompleteSetDto) throws Exception {
        return claimPopupService.reqPickShipComplete(claimShipCompleteSetDto);
    }


    /**
     * 수거/교환 정보 변경 요청
     * @param claimShippingSetDto
     */
    @ResponseBody
    @PostMapping("/chgClaimShipping.json")
    public ResponseObject<String> chgClaimShipping(@RequestBody ClaimShippingSetDto claimShippingSetDto) {
        return claimPopupService.chgClaimShipping(claimShippingSetDto);
    }


}
