package kr.co.homeplus.admin.web.settle.model.market;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "제휴 정산대사 합계")
public class MarketCompareDailyGetDto {
  @ApiModelProperty(notes = "일자")
  @RealGridColumnInfo(headText = "일자", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "제휴사")
  @RealGridColumnInfo(headText = "제휴사", width = 100)
  private String siteType;

  @ApiModelProperty(notes = "홈플러스 결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String homeShipOrderPrice;

  @ApiModelProperty(notes = "홈플러스 취소금액")
  @RealGridColumnInfo(headText = "취소금액", sortable = true, width = 150, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String homeShipOrderRefPrice;

  @ApiModelProperty(notes = "홈플러스 합계금액")
  @RealGridColumnInfo(headText = "합계금액", sortable = true, width = 150, groupName="홈플러스", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String homeSumAmt;

  @ApiModelProperty(notes = "제휴사 결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, groupName="제휴사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketShipOrderPrice;

  @ApiModelProperty(notes = "제휴사 취소금액")
  @RealGridColumnInfo(headText = "취소금액", sortable = true, width = 150, groupName="제휴사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketShipOrderRefPrice;

  @ApiModelProperty(notes = "제휴사 합계금액")
  @RealGridColumnInfo(headText = "합계금액", sortable = true, width = 150, groupName="제휴사", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String marketSumAmt;

  @ApiModelProperty(notes = "차액 결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 150, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diffShipOrderPrice;

  @ApiModelProperty(notes = "차액 취소금액")
  @RealGridColumnInfo(headText = "취소금액", sortable = true, width = 150, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diffShipOrderRefPrice;

  @ApiModelProperty(notes = "차액 합계금액")
  @RealGridColumnInfo(headText = "합계금액", sortable = true, width = 150, groupName="차액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diffSumAmt;
}
