package kr.co.homeplus.admin.web.pg.controller;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.PG_MID_USE_PURPOSE;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.model.PgMidManageDto;
import kr.co.homeplus.admin.web.pg.model.PgMidManageSelectDto;
import kr.co.homeplus.admin.web.pg.service.PgMidManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 결제관리 > 결제수단관리 > PG상점ID관리
 */
@Controller
@RequestMapping("/pg/paymentMethod")
public class PgMidManageController {

    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    @Autowired
    private PgMidManageService pgMidManageService;
    @Autowired
    private CodeService codeService;

    public PgMidManageController(LoginCookieService loginCookieService, AuthorityService authorityService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
    }

    /**
     * PG상점ID관리 Main Page
     */
    @GetMapping("/pgMidManageMain")
    public String pgMidManage(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_MNG)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // pgMidUsePurpose 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            PG_MID_USE_PURPOSE
        );

        // 가져온 공통코드와 함께 model 에 담기
        model.addAttribute("pgMidUsePurposeList", code.get(PG_MID_USE_PURPOSE));
        model.addAttribute("pgKindList", Arrays.asList(PgKind.values()));
        model.addAttribute("easyPayTargetList", ParentMethodCd.getEasyPayMethodList());
        model.addAllAttributes(RealGridHelper.create("pgMidManageGridBaseInfo", PgMidManageDto.class));
        return "/pg/pgMidManageMain";
    }

    /**
     * PG상점ID 검색
     */
    @ResponseBody
    @GetMapping("/getPgMidManageList.json")
    public List<PgMidManageDto> getPgMidManageList(@ModelAttribute PgMidManageSelectDto pgMidManageSelectDto) {
        return pgMidManageService.getPgMidManageList(pgMidManageSelectDto);
    }

    /**
     * PG상점ID 등록
     */
    @ResponseBody
    @PostMapping("/savePgMidManage.json")
    public ResponseObject<Object> savePgMidManage(@RequestBody PgMidManageDto pgMidManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        pgMidManageDto.setRegId(userCd);
        pgMidManageDto.setChgId(userCd);

        return pgMidManageService.savePgMidManage(pgMidManageDto);
    }
}
