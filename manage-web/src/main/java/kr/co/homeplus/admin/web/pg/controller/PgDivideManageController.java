package kr.co.homeplus.admin.web.pg.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.Constants;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageDto;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageHistoryDto;
import kr.co.homeplus.admin.web.pg.model.PgDivideManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.service.PgDivideManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 결제관리 > PG 배분율 관리 > 기본배분율 관리
 */
@Controller
@RequestMapping("/pg")
public class PgDivideManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final PgDivideManageService pgDivideManageService;

    public PgDivideManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            PgDivideManageService pgDivideManageService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.pgDivideManageService = pgDivideManageService;
    }

    /**
     * 기본배분율 관리 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/pgDivide/pgDivideManageMain")
    public String pgDivideManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_MNG)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("pgDivideManageGridBaseInfo", PgDivideManageDto.class));
        model.addAttribute("parentMethodCdList", ParentMethodCd.getBasicParentMethodList());
        model.addAttribute("inicisCdList", pgDivideManageService.getPgMidManageList(PgKind.INICIS.getPgCd(), "Y"));
        model.addAttribute("tosspgCdList", pgDivideManageService.getPgMidManageList(PgKind.TOSSPG.getPgCd(), "Y"));
        return "/pg/pgDivideManageMain";
    }

    /**
     * 기본배분율 변경이력 팝업 호출
     * @param model
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    @RequestMapping("/popup/pgDivideManageHistoryPop")
    public String popDivRateHistory(Model model, @RequestParam(value="parentMethodCd") String parentMethodCd,
                                                 @RequestParam(value="siteType") String siteType) throws Exception {
        model.addAllAttributes(RealGridHelper.create("pgDivideManageHistoryPopBaseInfo", PgDivideManageHistoryDto.class));
        model.addAttribute("parentMethodCd", parentMethodCd);
        model.addAttribute("siteType", siteType);
        return "/pg/pop/pgDivideManageHistoryPop";
    }

    /**
     * 기본배분율 조회
     * @param pgDivideManageSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/pgDivide/getPgDivideRateList.json")
    public List<PgDivideManageDto> getPgDivideRateList(@ModelAttribute PgDivideManageSelectDto pgDivideManageSelectDto) throws Exception {
        pgDivideManageSelectDto.setUseYn(Constants.USE_Y);
        pgDivideManageSelectDto.setIsPopup(Constants.ZERO);
        return pgDivideManageService.getPgDivideRateList(pgDivideManageSelectDto);
    }

    /**
     * 기본배분율 히스토리 조회
     * @param parentMethodCd
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/pgDivide/getPgDivideRateHistoryList.json")
    public List<PgDivideManageHistoryDto> getPgDivideRateHistoryList(@RequestParam(value="parentMethodCd") String parentMethodCd,
                                                              @RequestParam(value="siteType") String siteType) throws Exception {
        PgDivideManageSelectDto pgDivideManageSelectDto = new PgDivideManageSelectDto();
        pgDivideManageSelectDto.setUseYn(Constants.USE_N);
        pgDivideManageSelectDto.setIsPopup(Constants.ONE);
        pgDivideManageSelectDto.setParentMethodCd(parentMethodCd);
        pgDivideManageSelectDto.setSiteType(siteType);
        return pgDivideManageService.getPgDivideRateHistoryList(pgDivideManageSelectDto);
    }

    /**
     * 기본배분율 저장
     * @param pgDivideManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/pgDivide/savePgDivideRate.json")
    public ResponseObject<Object> savePgDivideRate(@RequestBody PgDivideManageDto pgDivideManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        pgDivideManageDto.setRegId(userCd);
        pgDivideManageDto.setChgId(userCd);
        return pgDivideManageService.savePgDivideRate(pgDivideManageDto);
    }

    /**
     * 결제정책 PG배분율 조회
     * @param policyNo
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("/pgDivide/getPaymentPolicyPgDivideRateList.json")
    public List<PgDivideManageDto> getPaymentPolicyPgDivideRateList(@RequestParam(value="policyNo") Long policyNo) throws Exception {
        PgDivideManageSelectDto pgDivideManageSelectDto = new PgDivideManageSelectDto();
        pgDivideManageSelectDto.setUseYn(Constants.USE_Y);
        pgDivideManageSelectDto.setPolicyNo(policyNo);
        return pgDivideManageService.getPaymentPolicyPgDivideRateList(pgDivideManageSelectDto);
    }
}