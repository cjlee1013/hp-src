package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Content 에서 사용하는 점포 유형 (HYPER/CLUB/EXPRESS)
 */
public enum StoreType {

    HYPER, CLUB, EXPRESS;

    private static final Map<String, String> MAP =
        Stream.of(values()).collect(Collectors.toMap(StoreType::name, StoreType::name));

    public static Map<String, String> getMap() {
        return MAP;
    }
}
