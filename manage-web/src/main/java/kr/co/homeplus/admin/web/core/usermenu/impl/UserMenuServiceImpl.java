package kr.co.homeplus.admin.web.core.usermenu.impl;

import java.util.List;
import kr.co.homeplus.admin.web.core.config.AdminProperties;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.AuthMenuListDto;
import kr.co.homeplus.admin.web.core.dto.AuthorizedMenus;
import kr.co.homeplus.admin.web.core.dto.PermitMenuParam;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserMenuServiceImpl implements UserMenuService {

    private final ResourceClient resourceClient;
    private final AdminProperties adminProperties;

    /**
     * 상세 탭페이지 URI목록
     * <pre>
     *     - 회원정보상세
     *     - 주문정보상세
     *     - 클레임정보상세
     *     - 판매자정산정보
     *     - 콜센터문의관리
     * </pre>
     */
    private static final List<String> DETAIL_TAB_PAGE_URI_LIST = List.of("/user/userInfo/detail",
        "/order/orderManageDetail",
        "/claim/info/getClaimDetailInfoTab",
        "/settle/partnerSettleDetail/partnerSettleDetail",
        "/callCenter/callCenterMain");

    @Override
    public List<AuthMenuListDto> getUserPermitMenuList(UserInfo userInfo) {

        //uri 설정 : "/permit/menus"
        StringBuilder uri = new StringBuilder(AdminConstants.AUTH_PERMIT_MENU_URI);
        uri.append("?userCd=").append(userInfo.getEmpId());
        uri.append("&systemScope=").append(adminProperties.getScopeValue());

        //사용자가 접근가능한 메뉴리스트 조회
        ResponseObject<List<AuthMenuListDto>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.AUTHORITY
            , uri.toString()
            , new ParameterizedTypeReference<>() {
            }
        );

        return responseObject.getData();
    }

    @Override
    public List<AuthMenuListDto> getUserPermitMenuList(final String userCd, final String systemScope) {

        StringBuilder uri = new StringBuilder(AdminConstants.AUTH_PERMIT_MENU_URI);
        uri.append("?userCd=").append(userCd);
        uri.append("&systemScope=").append(systemScope);

        //사용자가 접근가능한 메뉴리스트 조회
        ResponseObject<List<AuthMenuListDto>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.AUTHORITY
            , uri.toString()
            , new ParameterizedTypeReference<>() {
            }
        );
        return responseObject.getData();
    }

    @Override
    @Cacheable(value = "getAuthorizedMenusCache", key="#empId", condition = "#empId != null")
    public List<AuthorizedMenus> getAuthorizedMenus(final String empId) {
        log.debug("call getAuthorizedMenus:<{}>", empId);

        ResponseObject<List<AuthorizedMenus>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.AUTHORITY
            , AdminConstants.AUTH_AUTHORIZED_MENUS_URI + "?empId=" + empId
            , new ParameterizedTypeReference<>() {
            }
        );
        return responseObject.getData();
    }

    @Override
    @Cacheable(value = "isNotPermissionMenuCache", key="#empId + #requestUri", condition = "#empId != null")
    public boolean isNotPermissionMenu(final String empId, final String requestUri) {
        log.debug("call isNotPermissionMenu:<{}>,<{}>", empId, requestUri);

        PermitMenuParam param = new PermitMenuParam();
        param.setEmpId(empId);
        param.setRequestUri(requestUri);

        ResponseObject<Boolean> responseObject = resourceClient.postForResponseObject(
            ResourceRouteName.AUTHORITY, param
            , AdminConstants.AUTH_AUTHORIZED_MENUS_NOT_PERMITTED_URI
            , new ParameterizedTypeReference<>() {
            }
        );
        return responseObject.getData();
    }

    /**
     * 메뉴 캐시를 제거합니다.
     * @param empId 사번
     */
    @Override
    @CacheEvict(value = "getAuthorizedMenusCache", key = "#empId")
    public void removeAuthorizedMenusCache(final String empId) {
        //no-op
    }

    @Override
    public List<String> getDetailTabPageList() {
        return DETAIL_TAB_PAGE_URI_LIST;
    }
}
