package kr.co.homeplus.admin.web.item.model.auroraCategoryItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraCategoryItemListParamDto {

    private String searchType;
    private String searchKeyword;
    private String lcateCd;
    private String searchDispYn;

}
