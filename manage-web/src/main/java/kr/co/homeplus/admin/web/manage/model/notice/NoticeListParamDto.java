package kr.co.homeplus.admin.web.manage.model.notice;

import lombok.Data;

@Data
public class NoticeListParamDto {


    private String searchPeriodType;

    private String searchStartDt;

    private String searchEndDt;

    private String searchDispArea;

    private String searchClassCd;

    private String searchDispSite;

    private String searchUseYn;

    private String searchTopYn;

    private String searchType;

    private String searchKeyword;

}
