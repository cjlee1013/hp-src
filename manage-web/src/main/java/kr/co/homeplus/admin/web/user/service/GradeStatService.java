package kr.co.homeplus.admin.web.user.service;

import com.google.gson.Gson;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import kr.co.homeplus.admin.web.user.model.user.GradeStatChangeDto;
import kr.co.homeplus.admin.web.user.model.user.GradeStatChangeParam;
import kr.co.homeplus.admin.web.user.model.user.GradeStatResponseDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridBuilder;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumn;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDataProviderOption;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOption;
import kr.co.homeplus.plus.api.support.realgrid.RealGridType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class GradeStatService {

    private final ResourceClient resourceClient;

    public String getGradeStatListJson(GradeStatChangeParam gradeStatChangeParam) {

        Gson gson = new Gson();
        List<GradeStatChangeDto> gradeStatChangeList = new ArrayList<>();

        if (StringUtils.isEmpty(gradeStatChangeParam.getGradeApplyYm())) {
            gradeStatChangeList = Collections.emptyList();
            return gson.toJson(gradeStatChangeList);
        }

        try {

            String apiUrl = "/admin/gradeStat/getGradeStatList?gradeApplyYm=" + gradeStatChangeParam.getGradeApplyYm();
            apiUrl += gradeStatChangeParam.getIsBlackType() == true ? "&isBlackType=1" : "";

            ResponseObject<List<GradeStatChangeDto>> responseObject = resourceClient.getForResponseObject("usermng",
                apiUrl, new ParameterizedTypeReference<ResponseObject<List<GradeStatChangeDto>>>() {
                });

            gradeStatChangeList = responseObject.getData();

        } catch (Exception e) {
            log.error("Grade Stat Response error. trace : {}", ExceptionUtils.getStackTrace(e));
            gradeStatChangeList = Collections.emptyList();
        }

        return gson.toJson(gradeStatChangeList);
    }

    public String getRealGridSchemeForGradeStat(GradeStatChangeParam gradeStatChangeParam) {

        RealGridBuilder rb = RealGridHelper.createBuilder("gradeStatGrid", GradeStatChangeDto.class)
            .realGridOption(new RealGridOption.Builder(RealGridDisplayStyle.FILL, RealGridType.BLOCK_SELECT).build())
            .realGridDataProviderOption(new RealGridDataProviderOption.Builder().softDeleting(true)
                .build());
        // 컬럼은 갯수만큼 추가
        rb.add(
            new RealGridColumn.Builder().name("gradeChangeStr")
                .fieldName("gradeChangeStr")
                .headerText("회원등급변경구분")
                .realGridColumnType(RealGridColumnType.NAME)
                .width(30)
                .visible(true)
                .sortable(false)
                .buttonProperty(
                    new RealGridButtonProperty.Builder(RealGridButtonType.ACTION, true).build()
                )
                .build()
        );

        List<String> columnList = getGradeApplyYmRealGridColumnList(gradeStatChangeParam);
        int columnNum = 5;

        for (String gradeApplyColumn : columnList) {
            rb.add(
                new RealGridColumn.Builder().name("cols"+columnNum)
                    .fieldName("cols"+columnNum)
                    .headerText(gradeApplyColumn)
                    .realGridColumnType(RealGridColumnType.NUMBER_C)
                    .width(30)
                    .visible(true)
                    .sortable(false)
                    .buttonProperty(
                        new RealGridButtonProperty.Builder(RealGridButtonType.ACTION, true).build()
                    )
                    .build()
            );

            columnNum--;
        }

        return rb.build();
    }


    public List<String> getGradeApplyYmList() {
        String startGradeApplyYm = "202101";
        LocalDate start = LocalDate.parse(startGradeApplyYm + "01", DateTimeFormatter.ofPattern("yyyyMMdd"));
        LocalDate end = LocalDate.now();

        Long months = start.until(end, ChronoUnit.MONTHS);

        List<String> gradeApplyYmList = LongStream.rangeClosed(0, months)
            .mapToObj(monthsToAdd -> start.plusMonths(monthsToAdd)
                .format(DateTimeFormatter.ofPattern("yyyyMM")))
            .collect(Collectors.toList());
        log.debug("gradeApplyYmList : {}", gradeApplyYmList);

        return gradeApplyYmList;
    }

    private List<String> getGradeApplyYmRealGridColumnList(GradeStatChangeParam gradeStatChangeParam) {

        String gradeApplyYm = Optional.ofNullable(gradeStatChangeParam.getGradeApplyYm())
            .orElseGet(() -> LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMM")));

        LocalDate end = LocalDate.parse(gradeApplyYm + "01", DateTimeFormatter.ofPattern("yyyyMMdd"));
        LocalDate start = end.minusMonths(5);

        Long months = start.until(end, ChronoUnit.MONTHS);

        List<String> gradeApplyYmList = LongStream.rangeClosed(1, months)
            .mapToObj(monthsToAdd -> start.plusMonths(monthsToAdd)
                .format(DateTimeFormatter.ofPattern("yyyyMM")))
            .collect(Collectors.toList());
        log.debug("gradeApplyYmList : {}", gradeApplyYmList);

        return gradeApplyYmList;
    }

}
