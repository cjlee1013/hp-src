package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문 배치 관리 model")
@Getter
@Setter
@EqualsAndHashCode
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OrderBatchManageModel {
    @ApiModelProperty(value = "배치번호(seq)", position = 1)
    @RealGridColumnInfo(headText = "배치번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchNo;

    @ApiModelProperty(value = "배치타입", position = 2)
    @RealGridColumnInfo(headText = "배치타입", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchType;

    @ApiModelProperty(value = "배치명", position = 3)
    @RealGridColumnInfo(headText = "배치명", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchNm;

    @ApiModelProperty(value = "배치주기(스케줄러에 등록된 주기설정 정보)", position = 4)
    @RealGridColumnInfo(headText = "배치주기", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String batchTerm;

    @ApiModelProperty(value = "설명", position = 5)
    @RealGridColumnInfo(headText = "설명", width = 250, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String description;

    @ApiModelProperty(value = "사용여부", position = 6)
    @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(value = "메타필드1", position = 7)
    @RealGridColumnInfo(headText = "metaField1", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String metaField1;

    @ApiModelProperty(value = "메타필드2", position = 8)
    @RealGridColumnInfo(headText = "metaField2", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String metaField2;

    @ApiModelProperty(value = "메타필드3", position = 9)
    @RealGridColumnInfo(headText = "metaField3", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String metaField3;

    @ApiModelProperty(value = "메타필드4", position = 10)
    @RealGridColumnInfo(headText = "metaField4", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String metaField4;

    @ApiModelProperty(value = "메타필드5", position = 11)
    @RealGridColumnInfo(headText = "metaField5", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String metaField5;

    @ApiModelProperty(value = "등록자ID", position = 12)
    @RealGridColumnInfo(headText = "등록자명", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;

    @ApiModelProperty(value = "등록일시", position = 13)
    @RealGridColumnInfo(headText = "등록일시", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;

    @ApiModelProperty(value = "수정자ID", position = 14)
    @RealGridColumnInfo(headText = "수정자명", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "수정일시", position = 15)
    @RealGridColumnInfo(headText = "수정일시", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;
}
