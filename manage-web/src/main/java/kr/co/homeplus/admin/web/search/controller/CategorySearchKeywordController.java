package kr.co.homeplus.admin.web.search.controller;

import kr.co.homeplus.admin.web.search.model.CategorySearchKeywordGridDto;
import kr.co.homeplus.admin.web.search.model.CategorySearchKeywordSetDto;
import kr.co.homeplus.admin.web.search.service.CategorySearchKeywordService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/search/categorySearchKeyword")
public class CategorySearchKeywordController {

    private final CategorySearchKeywordService categorySearchKeywordService;

    @GetMapping(value = { "/main" })
    public String getMain(Model model){
        model.addAllAttributes(RealGridHelper.create("categorySearchKeywordGridBaseInfo", CategorySearchKeywordGridDto.CategorySearchKeyword.class));
        return "/search/categorySearchKeyword";
    }

    @ResponseBody
    @GetMapping(value= {"/getCategorySearchKeyword.json"})
    public Object getCategorySearchKeyword(
          @RequestParam String keyword
        , @RequestParam String searchType
        , @RequestParam String schUseYn
        , @RequestParam String schResetYn
    ) {
        return categorySearchKeywordService.getCategorySearchKeyword(keyword, searchType, schUseYn, schResetYn).getData();
    }

    @ResponseBody
    @PostMapping(value= {"/putCategorySearchKeyword.json"})
    public Integer putCategorySearchKeyword(@RequestBody CategorySearchKeywordSetDto categorySearchKeyword) {
        return (Integer)categorySearchKeywordService.putCategorySearchKeyword(categorySearchKeyword).getData();
    }

    @ResponseBody
    @PostMapping(value= {"/confirmCategorySearchKeyword.json"})
    public Integer confirmCategorySearchKeyword(@RequestBody CategorySearchKeywordSetDto categorySearchKeyword) {
        return (Integer)categorySearchKeywordService.confirmCategorySearchKeyword(categorySearchKeyword).getData();
    }
}