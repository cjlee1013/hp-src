package kr.co.homeplus.admin.web.core.exception.handler;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 */
@RequiredArgsConstructor
public enum ExceptionCode {
	//TODO 코드명, 코드값, 문구 고민이 필요합니다.
		SYS_ERROR_CODE_9001("-9001", "시스템 에러 : 관리자에게 문의해 주세요.")
	,   SYS_ERROR_CODE_9002("-9002", "시스템 에러 : 첨부파일 크기가 허용한도를 초과합니다.")
	,   SYS_ERROR_CODE_9003("-9003", "시스템 에러 : 첨부파일 등록 처리중 문제가 발생했습니다.")

	,	SYS_ERROR_CODE_9101("-9101", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9102("-9102", "통신 에러 : 내부 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9103("-9103", "통신 에러 : API 연결에 실패하였습니다.")
	,   SYS_ERROR_CODE_9104("-9104", "통신 에러 : API 요청 대기시간이 만료되었습니다.")
	,   SYS_ERROR_CODE_9105("-9105", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9106("-9106", "통신 에러 : 요청이 허용되지 않는 메소드입니다.")

	,   SYS_ERROR_CODE_9200("-9200", "인증 에러 : 로그인 인증에 실패하였습니다.")
	,   SYS_ERROR_CODE_9201("-9201", "인증 에러 : 로그인 쿠키에서 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9203("-9203", "인가 에러 : 메뉴 인가여부를 체크하는 도중 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9204("-9204", "인가 에러 : 해당 메뉴에 접근권한이 없습니다.")
	,   SYS_ERROR_CODE_9205("-9205", "인가 에러 : 권한 체크하는 도중 오류가 발생하였습니다.")
	,   SYS_ERROR_CODE_9206("-9206", "로그인 실패횟수 초과로 계정정보 확인 후 다시 로그인 하실 수 있습니다.")
	,   SYS_ERROR_CODE_9207("-9207", "로그인 정보가 없습니다. 로그인 상태를 확인 하시길 바랍니다.")
	,   SYS_ERROR_CODE_9208("-9208", "초기 비밀번호를 변경하신 후 로그인이 가능합니다.")
	,   SYS_ERROR_CODE_9209("-9209", "인증 에러 : 아이디 저장 쿠키에서 오류가 발생하였습니다.")

	,   SYS_ERROR_CODE_9301("-9301", "필수 파라미터가 누락되었습니다.")
	,   SYS_ERROR_CODE_9302("-9302", "파라미터 유효성 검사 오류입니다.")
	,   SYS_ERROR_CODE_9303("-9303", "입력값이 형식에 맞지 않습니다.")
	,   SYS_ERROR_CODE_9304("-9304", "입력값이 지원하지 않는 타입입니다.")

	,   SYS_ERROR_CODE_9401("-9401", "허용되지 않은 URL 주소로 파일 다운로드를 시도하였습니다.")
	,   SYS_ERROR_CODE_9402("-9402", "파일 다운로드에 실패하였습니다.")
	,   SYS_ERROR_CODE_9403("-9403", "파일 다운로드 URL이 입력되지 않았습니다.")
	;

	@Getter
	private final String code;
	@Getter
	private final String desc;

	@Override
	public String toString() {
		return code + ": " + desc;
	}
}
