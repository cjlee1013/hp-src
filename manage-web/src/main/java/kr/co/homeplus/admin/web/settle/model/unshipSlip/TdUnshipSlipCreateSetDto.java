package kr.co.homeplus.admin.web.settle.model.unshipSlip;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdUnshipSlipCreateSetDto {
    @ApiModelProperty(value = "검색기간(년)")
    private String schYear;

    @ApiModelProperty(value = "검색기간(월)")
    private String schMonth;
}
