package kr.co.homeplus.admin.web.core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class HomeplusSpecialCharacterFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // 1. ServletRequest 파라미터를 이용해 필터 작업 수행
        chain.doFilter(new HomeplusSpecialCharacterFilterWrapper(request), response);
    }

    @Override
    public void destroy() {
    }
}