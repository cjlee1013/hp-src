package kr.co.homeplus.admin.web.manage.model.dspMainThemeItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class DspMainThemeItemGetDto {

    @RealGridColumnInfo(headText = "상품번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품/배너명", columnType = RealGridColumnType.NUMBER_CENTER, width = 150, order = 1)
    private String dispNm;

    @RealGridColumnInfo(headText = "전시여부", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 2, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 3)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 4, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "itemSeq", order = 5, hidden = true)
    private Long itemSeq;
    @RealGridColumnInfo(headText = "themeId", order = 6, hidden = true)
    private Long themeId;
    @RealGridColumnInfo(headText = "dispYn", order = 7, hidden = true)
    private String dispYn;
    @RealGridColumnInfo(headText = "itemStoreType", order = 8, hidden = true)
    private String itemStoreType;
    @RealGridColumnInfo(headText = "mallType", order = 9, hidden = true)
    private String mallType;
    @RealGridColumnInfo(headText = "themeItemType", order = 10, hidden = true)
    private String themeItemType;
    @RealGridColumnInfo(headText = "linkInfo", order = 11, hidden = true)
    private String linkInfo;
    @RealGridColumnInfo(headText = "linkType", order = 12, hidden = true)
    private String linkType;
    @RealGridColumnInfo(headText = "imgUrl", order = 13, hidden = true)
    private String imgUrl;
    @RealGridColumnInfo(headText = "imgWidth", order = 14, hidden = true)
    private Integer imgWidth;
    @RealGridColumnInfo(headText = "imgHeight", order = 15, hidden = true)
    private Integer imgHeight;
    @RealGridColumnInfo(headText = "itemNm", order = 16, hidden = true)
    private String itemNm;


    private int priority;
    private String dispDt;
    private String linkInfoNm;
    private String regId;
    private String chgId;
    private String chgNm;
    private String chgDt;

}
