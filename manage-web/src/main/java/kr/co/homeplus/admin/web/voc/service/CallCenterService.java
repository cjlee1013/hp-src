package kr.co.homeplus.admin.web.voc.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterAddSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterCompleteImgSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterCompleteSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterExcelListGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterExcelListSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterListGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CallCenterService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;
  private final LoginCookieService cookieService;
  private final PrivacyLogService privacyLogService;
  private final VocCommonService vocCommonService;

  /**
   * 콜센터문의관리 리스트 조회
   */
  public List<CallCenterListGetDto> getCallCenterList(CallCenterListSetDto callCenterListSetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      callCenterListSetDto.setSTORE_ID(StringUtil.stringPadding(shipCommonService.getStoreId(), "LEFT", "0", 4));
      callCenterListSetDto.setH_CHANNEL(vocCommonService.getHChannel(storeInfo.getStoreType()));
    }

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterListSetDto,
        "/admin/callCenter/getCallCenterList",
        new ParameterizedTypeReference<ResponseObject<List<CallCenterListGetDto>>>(){}).getData();
  }

  /**
   * 콜센터문의관리 엑셀 리스트 조회
   */
  public List<CallCenterExcelListGetDto> getCallCenterExcelList(CallCenterExcelListSetDto callCenterExcelListSetDto) {
    if (shipCommonService.isStoreOffice()) {
      ShipStoreInfoGetDto storeInfo = shipCommonService.getStoreInfo(shipCommonService.getStoreId());

      callCenterExcelListSetDto.setSTORE_ID(StringUtil.stringPadding(shipCommonService.getStoreId(), "LEFT", "0", 4));
      callCenterExcelListSetDto.setH_CHANNEL(vocCommonService.getHChannel(storeInfo.getStoreType()));
    }

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterExcelListSetDto,
        "/admin/callCenter/getCallCenterExcelList",
        new ParameterizedTypeReference<ResponseObject<List<CallCenterExcelListGetDto>>>(){}).getData();
  }

  /**
   * 콜센터문의관리 상세 조회
   */
  public List<CallCenterDetailGetDto> getCallCenterDetail(HttpServletRequest request, CallCenterDetailSetDto callCenterDetailSetDto) {
    String userNo = shipCommonService.getUserCd();

    final String privacyAccessIp = ServletUtils.clientIP(request);
    final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

    final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
        .txTime(DateTimeUtil.getNowMillYmdHis())
        .accessIp(privacyAccessIp)
        .userId(privacyAccessUserId)
        .txCodeUrl(request.getRequestURI())
        .txMethod(PersonalLogMethod.SELECT.name())
        .txCodeName("콜센터문의관리 상세 조회")
        .processor(String.valueOf(userNo))
        .processorTaskSql("")
        .build();

    privacyLogService.send(logInfo);

    final HttpHeaders headers = new HttpHeaders();
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
    headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterDetailSetDto,
        "/admin/callCenter/getCallCenterDetail",
        headers,
        new ParameterizedTypeReference<ResponseObject<List<CallCenterDetailGetDto>>>(){}).getData();
  }

  /**
   * 콜센터문의관리 문의 추가
   */
  public ResponseObject<Object> setAddCounsel(CallCenterAddSetDto callCenterAddSetDto) throws Exception {
    callCenterAddSetDto.setReg_account_id(shipCommonService.getUserId());
    callCenterAddSetDto.setReg_account_nm(shipCommonService.getUserNm());

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterAddSetDto,
        "/admin/callCenter/setAddCounsel",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 콜센터문의관리 처리 완료
   */
  public ResponseObject<Object> setAddComplete(CallCenterCompleteSetDto callCenterCompleteSetDto) throws Exception {
    callCenterCompleteSetDto.setComplet_account_id(shipCommonService.getUserId());
    callCenterCompleteSetDto.setComplet_account_nm(shipCommonService.getUserNm());

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterCompleteSetDto,
        "/admin/callCenter/setAddComplete",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

  /**
   * 콜센터문의관리 상담정보이미지 저장
   */
  public ResponseObject<Object> setAddCompleteImg(CallCenterCompleteImgSetDto callCenterCompleteImgSetDto) throws Exception {

    return resourceClient.postForResponseObject(
        ResourceRouteName.USERMNG,
        callCenterCompleteImgSetDto,
        "/admin/callCenter/setAddCompleteImg",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }

}
