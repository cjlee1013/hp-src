package kr.co.homeplus.admin.web.settle.controller;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustCode;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustItemGetDto;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustRegisterGetDto;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustListSetDto;
import kr.co.homeplus.admin.web.settle.model.adjust.AdjustListGetDto;
import kr.co.homeplus.admin.web.settle.service.AdjustService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/settleAdjust")
public class AdjustController {

    private final AdjustService adjustService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public AdjustController(AdjustService adjustService, SettleCommonService settleCommonService, CodeService codeService) {
        this.adjustService = adjustService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산조정 관리 페이지
     */
    @RequestMapping(value = "/adjustList", method = RequestMethod.GET)
    public String taxAdjustList(Model model) throws Exception{
        HashMap<Integer,String> adjustCode = AdjustCode.getAdjustCodeAll();
        model.addAttribute("adjustType", adjustCode);
        model.addAttribute("schAdjustType", adjustCode);

        model.addAllAttributes(RealGridHelper.create("adjustListBaseInfo", AdjustListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("adjustUploadBaseInfo", AdjustRegisterGetDto.class));

        return "/settle/adjustList";
    }

    //정산조정 관리 조회 JSON
    @RequestMapping(value = "/getAdjustList.json", method = RequestMethod.GET)
    @ResponseBody
    public List<AdjustListGetDto> getAdjustList(@ModelAttribute AdjustListSetDto adjustListSetDto) throws Exception{
        return adjustService.getAdjustList(adjustListSetDto);
    }

    //정산조정 아이템 JSON
    @RequestMapping(value = "/getAdjustItem.json", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<AdjustItemGetDto> getAdjustItem(@RequestParam(name ="itemNo") String itemNo) throws Exception{
        return adjustService.getAdjustItem(itemNo);
    }

    //정산조정 입력 JSON
    @RequestMapping(value = "/insertAdjust.json", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> insertAdjust(@ModelAttribute AdjustRegisterGetDto adjustRegisterGetDto) throws Exception{
        return adjustService.insertAdjust(adjustRegisterGetDto);
    }

    //정산조정 관리 엑셀업로드
    @RequestMapping(value = "/uploadExcel.json", method = RequestMethod.POST)
    @ResponseBody
    public List<AdjustRegisterGetDto> uploadExcel(HttpServletRequest httpServletRequest) throws Exception {
        return adjustService.uploadExcel(httpServletRequest);
    }

    //정산조정 입력 JSON
    @RequestMapping(value = "/deleteAdjust.json", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> deleteAdjust(@ModelAttribute AdjustListGetDto listParams) throws Exception{
        return adjustService.deleteAdjust(listParams);
    }
}