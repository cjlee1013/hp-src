package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 리스트 응답 DTO")
public class CallCenterDetailGetDto {
  @ApiModelProperty(notes = "문의번호")
  @JsonProperty("KEY")
  private String KEY;

  @ApiModelProperty(notes = "처리상태")
  @JsonProperty("advisor_ing_nm")
  private String advisor_ing_nm;

  @ApiModelProperty(notes = "처리상태코드")
  @JsonProperty("advisor_ing_type")
  private String advisor_ing_type;

  @ApiModelProperty(notes = "접수채널")
  @JsonProperty("channel_nm")
  private String channel_nm;

  @ApiModelProperty(notes = "접수채널코드")
  @JsonProperty("channel_type")
  private String channel_type;

  @ApiModelProperty(notes = "점포유형")
  @JsonProperty("h_channel_nm")
  private String h_channel_nm;

  @ApiModelProperty(notes = "점포유형코드")
  @JsonProperty("h_channel")
  private String h_channel;

  @ApiModelProperty(notes = "상담구분")
  @JsonProperty("advisor_type_nm1")
  private String advisor_type_nm1;

  @ApiModelProperty(notes = "상담구분코드")
  @JsonProperty("advisor_type_1")
  private String advisor_type_1;

  @ApiModelProperty(notes = "상담항목")
  @JsonProperty("advisor_type_nm2")
  private String advisor_type_nm2;

  @ApiModelProperty(notes = "상담항목코드")
  @JsonProperty("advisor_type_2")
  private String advisor_type_2;

  @ApiModelProperty(notes = "판매업체ID")
  @JsonProperty("store_id")
  private String store_id;

  @ApiModelProperty(notes = "판매업체")
  @JsonProperty("store_nm")
  private String store_nm;

  @ApiModelProperty(notes = "회원번호")
  @JsonProperty("cust_id")
  private String cust_id;

  @ApiModelProperty(notes = "회원명")
  @JsonProperty("cust_nm")
  private String cust_nm;

  @ApiModelProperty(notes = "주문번호")
  @JsonProperty("h_order_no")
  private String h_order_no;

  @ApiModelProperty(notes = "상담구분")
  @JsonProperty("voc_yn_nm")
  private String voc_yn_nm;

  @ApiModelProperty(notes = "상담구분코드")
  @JsonProperty("voc_yn")
  private String voc_yn;

  @ApiModelProperty(notes = "상담주문상품")
  @JsonProperty("h_item_no")
  private String h_item_no;

  @ApiModelProperty(notes = "마켓연동")
  @JsonProperty("market_type_nm")
  private String market_type_nm;

  @ApiModelProperty(notes = "처리부서")
  @JsonProperty("h_branch_nm")
  private String h_branch_nm;

  @ApiModelProperty(notes = "처리부서코드")
  @JsonProperty("h_branch")
  private String h_branch;

  @ApiModelProperty(notes = "처리자ID")
  @JsonProperty("trans_advisor_id")
  private String trans_advisor_id;

  @ApiModelProperty(notes = "처리자")
  @JsonProperty("trans_advisor_nm")
  private String trans_advisor_nm;

  @ApiModelProperty(notes = "접수일")
  @JsonProperty("reg_dt")
  private String reg_dt;

  @ApiModelProperty(notes = "접수자")
  @JsonProperty("account_nm")
  private String account_nm;

  @ApiModelProperty(notes = "제목(중요도)")
  @JsonProperty("h_important_grade")
  private String h_important_grade;

  @ApiModelProperty(notes = "제목")
  @JsonProperty("history_title")
  private String history_title;

  @ApiModelProperty(notes = "접수내용")
  @JsonProperty("rmks")
  private String rmks;

  @ApiModelProperty(notes = "처리사유")
  @JsonProperty("complet_reason_nm")
  private String complet_reason_nm;

  @ApiModelProperty(notes = "처리사유코드")
  @JsonProperty("complet_reason")
  private String complet_reason;

  @ApiModelProperty(notes = "처리사유상세")
  @JsonProperty("complet_detail_nm")
  private String complet_detail_nm;

  @ApiModelProperty(notes = "처리사유상세코드")
  @JsonProperty("complet_detail")
  private String complet_detail;

  @ApiModelProperty(notes = "발생자")
  @JsonProperty("issue_generator")
  private String issue_generator;

  @ApiModelProperty(notes = "발생원인")
  @JsonProperty("issue_reason")
  private String issue_reason;

  @ApiModelProperty(notes = "처리내용")
  @JsonProperty("complet_txt")
  private String complet_txt;

  @ApiModelProperty(notes = "처리완료일시")
  @JsonProperty("complet_dt")
  private String complet_dt;

  @ApiModelProperty(notes = "처리자")
  @JsonProperty("complet_account_nm")
  private String complet_account_nm;

  @ApiModelProperty(notes = "처리자ID")
  @JsonProperty("complet_account_id")
  private String complet_account_id;

  @ApiModelProperty(notes = "부모상담일련번호")
  @JsonProperty("par_history_seq")
  private String par_history_seq;

  @ApiModelProperty(notes = "상담정보이미지리스트")
  @JsonProperty("fileList")
  private List<CallCenterCompleteImgGetDto> fileList;
}