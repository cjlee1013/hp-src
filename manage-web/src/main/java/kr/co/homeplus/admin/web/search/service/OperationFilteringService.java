package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListGetParam;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListRuleInfoSetParam;
import kr.co.homeplus.admin.web.search.model.OperationSearchLogBlackListSetParam;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OperationFilteringService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getSearchLogBlackListRuleInfo(OperationSearchLogBlackListRuleInfoSetParam param) {
        String apiUri = StringUtil.getRequestString("/operation/getSearchLogBlackListRuleInfo", OperationSearchLogBlackListGetParam.class, param);
        return getResponseObject(apiUri,param);
    }

    public ResponseObject modifyBlackListRuleInfo(OperationSearchLogBlackListRuleInfoSetParam param) {
        param.setChgId(loginCookieService.getUserInfo().getEmpId());
        String apiUri = StringUtil.getRequestString("/operation/modifyBlackListRuleInfo", OperationSearchLogBlackListGetParam.class, param);
        return getResponseObject(apiUri,param);
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.SEARCHMNG)
                .uri(apiUri)
                .postObject(elements)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
