package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class MailHistoryDetailRequest {

    private long mailWorkSeq;
}
