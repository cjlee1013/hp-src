package kr.co.homeplus.admin.web.partner.model.sellerShopItem;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SellerShopItemSetParamDto {

    private String partnerId;

    private String dispType;

    private List<SellerShopItemListSelectDto> itemList;

    private String regId;

}
