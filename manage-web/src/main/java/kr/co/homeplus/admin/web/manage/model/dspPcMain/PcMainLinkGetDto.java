package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcMainLinkGetDto {

    private String linkType;

    private String linkInfo;

    private String linkInfoNm;

    private String linkOptions;

    private String textYn;

    private String title;

    private String subTitle;

    private String imgUrl;

    private int imgWidth;

    private int imgHeight;

}