package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Data;

@Data
public class SellerShopPopupListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchType;

    private String searchKeyword;

}
