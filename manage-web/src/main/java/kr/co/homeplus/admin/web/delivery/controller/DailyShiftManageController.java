package kr.co.homeplus.admin.web.delivery.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftDuplicateCheckModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftManageSelectModel;
import kr.co.homeplus.admin.web.delivery.model.DailyShiftStoreManageModel;
import kr.co.homeplus.admin.web.delivery.service.DailyShiftManageService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 배송관리 > 점포배송정보 > 특정일 배송관리 (화면명 변경 : (구) 일자별 Shift 관리)
 */
@Controller
@RequestMapping("/escrow/storeDelivery")
public class DailyShiftManageController {
    private final DailyShiftManageService dailyShiftManageService;
    private final LoginCookieService loginCookieService;

    public DailyShiftManageController(DailyShiftManageService dailyShiftManageService, LoginCookieService loginCookieService) {
        this.dailyShiftManageService = dailyShiftManageService;
        this.loginCookieService = loginCookieService;
    }

    /**
     * 메인 페이지 호출
     */
    @GetMapping("/dailyShiftManageMain")
    public String dailyShiftManageMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("dailyShiftManageGridBaseInfo", DailyShiftManageModel.class));
        return "/delivery/dailyShiftManageMain";
    }

    /**
     * 일자별 Shift 정보 조회
     */
    @ResponseBody
    @PostMapping("/getDailyShiftManageList.json")
    public List<DailyShiftManageModel> getDailyShiftManageList(@RequestBody DailyShiftManageSelectModel dailyShiftManageSelectModel) throws Exception {
        return dailyShiftManageService.getDailyShiftManageList(dailyShiftManageSelectModel);
    }

    /**
     * 일자별 Shift 점포 정보 조회
     */
    @ResponseBody
    @GetMapping("/getDailyShiftStoreManageList.json")
    public List<DailyShiftStoreManageModel> getDailyShiftStoreManageList(@RequestParam(value = "dailyShiftMngSeq") long dailyShiftMngSeq) throws Exception {
        return dailyShiftManageService.getDailyShiftStoreManageList(dailyShiftMngSeq);
    }

    /**
     * 일자별 Shift 관리 중복체크
     */
    @ResponseBody
    @PostMapping("/checkDuplicateDailyShiftManage.json")
    public Integer checkDuplicateDailyShiftManage(@RequestBody DailyShiftDuplicateCheckModel dailyShiftDuplicateCheckModel) throws Exception {
        return dailyShiftManageService.checkDuplicateDailyShiftManage(dailyShiftDuplicateCheckModel);
    }

    /**
     * 일자별 Shift 정보 저장
     */
    @ResponseBody
    @PostMapping("/saveDailyShiftManage.json")
    public DailyShiftManageModel saveDailyShiftManage(@RequestBody DailyShiftManageModel dailyShiftManageModel) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        dailyShiftManageModel.setRegId(userCd);
        dailyShiftManageModel.setChgId(userCd);
        return dailyShiftManageService.saveDailyShiftManage(dailyShiftManageModel);
    }

    /**
     * 일자별 Shift 점포 정보 저장
     */
    @ResponseBody
    @PostMapping("/saveDailyShiftStoreManageList.json")
    public Integer saveDailyShiftStoreManageList(@RequestBody List<DailyShiftStoreManageModel> dailyShiftStoreManageModelList) throws Exception {
        return dailyShiftManageService.saveDailyShiftStoreManageList(dailyShiftStoreManageModelList);
    }

    /**
     * 일자별 Shift 정보 삭제
     */
    @ResponseBody
    @PostMapping("/deleteDailyShiftManage.json")
    public Integer deleteDailyShiftManage(@RequestBody List<DailyShiftManageModel> dailyShiftManageModelList) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        for (DailyShiftManageModel dailyShiftManageModel : dailyShiftManageModelList) {
            dailyShiftManageModel.setChgId(userCd);
        }
        return dailyShiftManageService.deleteDailyShiftManage(dailyShiftManageModelList);
    }
}
