package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class TypoGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class Typo {
        @RealGridColumnInfo(headText = "typoId", width = 0, hidden = true, columnType = RealGridColumnType.NUMBER)
        private Long typoId;
        @RealGridColumnInfo(headText = "키워드", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "오탈자", width = 70, sortable = true, columnType = RealGridColumnType.BASIC)
        private String correctedTerms;
        @RealGridColumnInfo(headText = "수정일", width = 20, sortable = true, columnType = RealGridColumnType.BASIC)
        private String updateDt;
    }
}
