package kr.co.homeplus.admin.web.partner.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MarketType {

        ELEVEN( "coopeleven", "11번가")
    ,   GMAREKT( "coopgmarket", "G마켓")
    ,   AUCTION( "coopauction", "옥션")
    ,   NAVER("coopnaver", "N마트");

    @Getter
    private final String id;

    @Getter
    private final String description;

}
