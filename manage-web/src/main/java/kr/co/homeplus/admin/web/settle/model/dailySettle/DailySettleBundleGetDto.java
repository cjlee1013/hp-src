package kr.co.homeplus.admin.web.settle.model.dailySettle;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailySettleBundleGetDto {
    @ApiModelProperty(value = "정산번호")
    @RealGridColumnInfo(headText = "정산번호", width = 100)
    private long settleNo;
    
    @ApiModelProperty(value = "정산주기")
    @RealGridColumnInfo(headText = "정산주기", width = 100)
    private String settleCycleType;
    
    @ApiModelProperty(value = "정산주기코드")
    @RealGridColumnInfo(headText = "정산주기코드", width = 100)
    private String settlePeriodCd;
    
    @ApiModelProperty(value = "매출일")
    @RealGridColumnInfo(headText = "매출일", width = 100)
    private String basicDt;
    
    @ApiModelProperty(value = "파트너ID")
    @RealGridColumnInfo(headText = "파트너ID", width = 100)
    private String partnerId;
    
    @ApiModelProperty(value = "배송주체")
    @RealGridColumnInfo(headText = "배송주체", width = 100)
    private String shipMng;
    
    @ApiModelProperty(value = "원배송비")
    @RealGridColumnInfo(headText = "원배송비", width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long shipAmt;
    
    @ApiModelProperty(value = "홈플러스부담 원배송비")
    @RealGridColumnInfo(headText = "홈플러스부담배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long homeChargeShipAmt;
    
    @ApiModelProperty(value = "파트너부담 원배송비")
    @RealGridColumnInfo(headText = "파트너부담 원배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sellerChargeShipAmt;
    
    @ApiModelProperty(value = "카드사부담 원배송비")
    @RealGridColumnInfo(headText = "카드사부담 원배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long cardChargeShipAmt;
    
    @ApiModelProperty(value = "고객부담 추가배송비")
    @RealGridColumnInfo(headText = "고객부담 추가배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long custChargeAddShipAmt;
    
    @ApiModelProperty(value = "홈플러스부담 추가배송비")
    @RealGridColumnInfo(headText = "홈플러스부담 추가배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long homeChargeAddShipAmt;
    
    @ApiModelProperty(value = "파트너부담 추가배송비")
    @RealGridColumnInfo(headText = "파트너부담 추가배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sellerChargeAddShipAmt;
    
    @ApiModelProperty(value = "고객부담 반품배송비")
    @RealGridColumnInfo(headText = "고객부담 반품배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long custChargeReturnShipAmt;
    
    @ApiModelProperty(value = "홈플러스부담 반품배송비")
    @RealGridColumnInfo(headText = "홈플러스부담 반품배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long homeChargeReturnShipAmt;
    
    @ApiModelProperty(value = "파트너부담 반품배송비")
    @RealGridColumnInfo(headText = "파트너부담 반품배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sellerChargeReturnShipAmt;
    
    @ApiModelProperty(value = "조건부 무료배송비")
    @RealGridColumnInfo(headText = "조건부 무료배송비", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long condShipAmt;
    
    @ApiModelProperty(value = "클레임시 원배송비 판매자부담금")
    @RealGridColumnInfo(headText = "클레임시 원배송비 판매자부담금", width = 150, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimChargeSeller;
    
    @ApiModelProperty(value = "클레임공제금액")
    @RealGridColumnInfo(headText = "클레임공제금액", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long claimDeductAmt;
    
    @ApiModelProperty(value = "정산대상금액")
    @RealGridColumnInfo(headText = "정산대상금액", width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long settleAmt;
    
    @ApiModelProperty(value = "팀")
    @RealGridColumnInfo(headText = "팀", width = 100)
    private String teamCd;
    
    @ApiModelProperty(value = "사업부")
    @RealGridColumnInfo(headText = "사업부", width = 100)
    private String deptCd;
    
    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", width = 100)
    private String regDt;
    
    @ApiModelProperty(value = "등록아이디")
    @RealGridColumnInfo(headText = "등록아이디", width = 100)
    private String regId;
    
    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일", width = 100)
    private String chgDt;
    
    @ApiModelProperty(value = "수정아이디")
    @RealGridColumnInfo(headText = "수정아이디", width = 100)
    private String chgId;
    
}
