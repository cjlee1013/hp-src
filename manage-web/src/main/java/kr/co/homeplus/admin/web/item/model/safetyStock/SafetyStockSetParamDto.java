package kr.co.homeplus.admin.web.item.model.safetyStock;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SafetyStockSetParamDto {

    private String itemNo;
    private String hyperUseYn;
    private int hyperQty;
    private String clubUseYn = "N";
    private int clubQty;
    private String expressUseYn;
    private int expressQty;
    private String useYn;
    private String regId;

}
