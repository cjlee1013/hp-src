package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;



import lombok.Data;


@Data
public class AttributeSetParamDto {

    private Long scateCd;

    private Long attrNo;

    private Long selectAttrNo;

    private int priority;

    private String useYn;

    private String regId;

}
