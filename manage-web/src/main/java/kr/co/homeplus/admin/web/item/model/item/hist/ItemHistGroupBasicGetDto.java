package kr.co.homeplus.admin.web.item.model.item.hist;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class ItemHistGroupBasicGetDto {
    // 히스토리번호
    @RealGridColumnInfo(headText = "히스토리번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String id;

    // 변경일시
    @RealGridColumnInfo(headText = "변경일시", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true)
    private String chgDt;

    // 기본정보 (심사/합의정보 포함)
    @RealGridColumnInfo(headText = "기본정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeBasic = "-";

    // 판매정보
    @RealGridColumnInfo(headText = "판매정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeSale = "-";

    // 옵션/자재 정보
    @RealGridColumnInfo(headText = "용도옵션정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeOptItem = "-";

    // 이미지정보
    @RealGridColumnInfo(headText = "이미지정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeImg = "-";

    // 속성정보
    @RealGridColumnInfo(headText = "속성정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeAttr = "-";

    // 부가정보
    @RealGridColumnInfo(headText = "부가정보", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String changeEtc = "-";

    // 등록/수정자 이름
    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

}
