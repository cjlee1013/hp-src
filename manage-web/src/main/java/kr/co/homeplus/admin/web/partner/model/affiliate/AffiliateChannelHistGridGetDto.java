package kr.co.homeplus.admin.web.partner.model.affiliate;

import java.math.BigDecimal;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class AffiliateChannelHistGridGetDto {

    @RealGridColumnInfo(headText = "수수료", width = 50)
    private BigDecimal commissionRate;

    @RealGridColumnInfo(headText = "수정자", width = 50, order = 1)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 50, order = 2)
    private String chgDt;

}
