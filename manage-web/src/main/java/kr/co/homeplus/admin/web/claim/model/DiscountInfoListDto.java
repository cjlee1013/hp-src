package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "클레임상세정보 - 차감 할인정보 팝업")
@RealGridOptionInfo(checkBar = false)
public class DiscountInfoListDto {


    @ApiModelProperty(value = "할인번호", position = 1)
    @RealGridColumnInfo(headText = "할인번호", width = 200)
    private String orderDiscountNo;

    @ApiModelProperty(value = "종류", position = 2)
    @RealGridColumnInfo(headText = "종류", width = 200)
    private String additionTypeDetail;

    @ApiModelProperty(value = "할인적용대상", position = 3)
    @RealGridColumnInfo(headText = "할인적용대상", width = 400)
    private String discountTarget;

    @ApiModelProperty(value = "할인금액", position = 4)
    @RealGridColumnInfo(headText = "할인금액", columnType = RealGridColumnType.PRICE, width = 200, fieldType = RealGridFieldType.NUMBER)
    private String discountAmt;
}
