package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspLeafletListGetDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long leafletNo;

    @RealGridColumnInfo(headText = "사이트 구분", order = 1, hidden = true)
    private String siteType;

    @RealGridColumnInfo(headText = "사이트 구분", width = 70, order = 2, sortable = true)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "마트전단명", width = 200, order = 3, sortable = true)
    private String martLeafletNm;

    @RealGridColumnInfo(headText = "전시 시작일", columnType = RealGridColumnType.DATE, width = 120, order = 4, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", columnType = RealGridColumnType.DATE, width = 120, order = 5, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "등록 상품 수", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 6, sortable = true)
    private int itemCnt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 7, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 9, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 10, sortable = true)
    private String chgDt;


    private String regId;
    private String chgId;

}
