package kr.co.homeplus.admin.web.item.model.item;

import lombok.Data;

/**
 * 상품 기본정보
 */
@Data
public class ItemRsvDetailGetDto {

	//예약판매번호
	private long saleRsvNo;

	//예약판매시작일시
	private String rsvStartDt;

	//예약판매종료일시
	private String rsvEndDt;

	//예약상품출하시작일시
	private String shipStartDt;

}
