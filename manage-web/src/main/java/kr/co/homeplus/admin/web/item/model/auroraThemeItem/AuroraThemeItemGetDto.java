package kr.co.homeplus.admin.web.item.model.auroraThemeItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class AuroraThemeItemGetDto {

    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 300, order = 1)
    private String itemNm;

    @RealGridColumnInfo(headText = "상품유형", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 70, order = 2)
    private String itemTypeNm;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 3)
    private String useYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 4)
    private String regNm;

    @RealGridColumnInfo(headText = "themeId", order = 5, hidden = true)
    private Long themeId;
    @RealGridColumnInfo(headText = "useYn", order = 6, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "priority", order = 7, hidden = true)
    private String priority;

    private String regDt;
    private String chgDt;
    private String regId;
    private String chgId;
    private String chgNm;

}
