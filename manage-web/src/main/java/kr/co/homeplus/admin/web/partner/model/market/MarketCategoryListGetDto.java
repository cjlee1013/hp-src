package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryListGetDto {

    // 마켓연동업체업체ID
    private String partnerId;
    // 마ㅔㅋㅅ연동업체명
    private String businessNm;
    // 사이트명
    private String siteNm;
    // 상호
    private String partnerNm;
    // 사업자등록번호
    private String partnerNo;
    // 회원상태
    private String partnerStatus;
    // 사업자유형명
    private String operatorTypeNm;
    // 회원상태명
    private String partnerStatusNm;
    //사용여부
    private String useYn;
    //사용여부명
    private String useYnNm;
    // 등록자 ID
    private String regId;
    // 등록자
    private String regNm;
    // 등록일
    private String regDt;
    // 수정자 ID
    private String chgId;
    // 수정자
    private String chgNm;
    // 수정일
    private String chgDt;

}
