package kr.co.homeplus.admin.web.item.model.itemGroup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemGroupDetailSetParamDto {

    //묶음상품관리번호
    private Long groupNo;

    //상풐번호
    private String itemNo;

    //사용
    private String useYn;

    //대표상품여부
    private String representYn;

    //1단계타이틀
    private String opt1Val;

    //2단계타이틀
    private String opt2Val;

    //등록/수정자
    private String userId;

}
