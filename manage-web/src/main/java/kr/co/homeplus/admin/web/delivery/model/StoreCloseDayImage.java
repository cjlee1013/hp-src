package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 휴일 조회 이미지 팝업 DTO")
public class StoreCloseDayImage {
    @ApiModelProperty(notes = "점포유형(HYPER,CLUB)", position = 1)
    private String storeType;
    @ApiModelProperty(notes = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타)", position = 2)
    private String closeDayType;
    @ApiModelProperty(notes = "이미지주소(PC)", position = 3)
    private String pcImgUrl;
    @ApiModelProperty(notes = "이미지주소(Mobile)", position = 4)
    private String mobileImgUrl;
    @ApiModelProperty(notes = "최종수정일", position = 5)
    private String chgDt;
    @ApiModelProperty(notes = "최종등록자", position = 6)
    private String chgId;
    private List<StoreCloseDayImage> imageList;
}