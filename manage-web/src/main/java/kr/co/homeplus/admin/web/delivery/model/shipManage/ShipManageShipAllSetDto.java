package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 배송관리 선택발송처리 요청 DTO")
public class ShipManageShipAllSetDto {
    @ApiModelProperty(notes = "배송번호")
    private String bundleNo;

    @ApiModelProperty(notes = "배송방법")
    private String shipMethod;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "배송예정일")
    private String scheduleShipDt;

    @ApiModelProperty(notes = "수정자")
    private String chgId;
}
