package kr.co.homeplus.admin.web.delivery.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageInfoSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipCompanyManageListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanyCodeListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanySetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipLinkCompanySetListGetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipCompanyManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 배송관리 > 택배사코드관리
 */
@Controller
@RequestMapping("/ship")
public class ShipCompanyManageController {

    @Autowired
    private ShipCompanyManageService shipCompanyManageService;
    @Autowired
    private ShipCommonService shipCommonService;

    // 택배사 코드 관리 그리드
    private static final List<RealGridColumn> SHIP_COMPANY_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_COMPANY_MANAGE_GRID_OPTION = new RealGridOption("fill", false, false, false);

    // 연동업체설정 팝업 그리드
    private static final List<RealGridColumn> SHIP_LINK_COMPANY_SET_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_LINK_COMPANY_SET_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public ShipCompanyManageController() {
        // 택배사 코드 관리 그리드
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvNm", "dlvNm", "택배사명", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvCd", "dlvCd", "내부코드", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvLinkCompany", "dlvLinkCompany", "연동업체공통코드", RealGridColumnType.BASIC, 80, false, false));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvLinkCompanyNm", "dlvLinkCompanyNm", "연동업체", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("dlvLinkCompanyCd", "dlvLinkCompanyCd", "연동업체코드", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("autoReturnYn", "autoReturnYn", "자동반품접수", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("autoReturnDelayYn", "autoReturnDelayYn", "자동반품지연", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("displayYn", "displayYn", "노출여부", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("csPhone", "csPhone", "고객센터 전화번호", RealGridColumnType.BASIC, 80, false, false));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("homePage", "homePage", "홈페이지", RealGridColumnType.BASIC, 80, false, false));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("note", "note", "비고", RealGridColumnType.BASIC, 80, false, false));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("regId", "regId", "등록자", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("regDt", "regDt", "등록일시", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("chgId", "chgId", "수정자", RealGridColumnType.BASIC, 80, true, true));
        SHIP_COMPANY_MANAGE_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일시", RealGridColumnType.BASIC, 80, true, true));

        // 연동업체설정 팝업 그리드
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("dlvLinkCompany", "dlvLinkCompany", "연동업체공통코드", RealGridColumnType.BASIC, 80, false, false));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("dlvLinkCompanyNm", "dlvLinkCompanyNm", "연동업체명", RealGridColumnType.BASIC, 100, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("linkYn", "linkYn", "연동여부", RealGridColumnType.BASIC, 60, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("stopStartDt", "stopStartDt", "연동중지시작일", RealGridColumnType.BASIC, 120, false, false));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("stopEndDt", "stopEndDt", "연동중지종료일", RealGridColumnType.BASIC, 120, false, false));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("stopDt", "stopDt", "연동중지기간", RealGridColumnType.BASIC, 160, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("regId", "regId", "등록자", RealGridColumnType.BASIC, 80, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("regDt", "regDt", "등록일시", RealGridColumnType.BASIC, 130, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("chgId", "chgId", "수정자", RealGridColumnType.BASIC, 80, true, true));
        SHIP_LINK_COMPANY_SET_POP_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일시", RealGridColumnType.BASIC, 130, true, true));
    }

    /**
     * 택배사코드관리 Main Page
     */
    @GetMapping("/shipManage/shipCompanyManageMain")
    public String shipCompanyManageMain(Model model) throws Exception {
        model.addAttribute("dlvLinkCompany", shipCommonService.getCode("dlv_link_company"));
        model.addAttribute("dlvCd", shipCommonService.getCode("dlv_cd"));
        model.addAttribute("shipCompanyManageGridBaseInfo", new RealGridBaseInfo("shipCompanyManageGridBaseInfo", SHIP_COMPANY_MANAGE_GRID_HEAD, SHIP_COMPANY_MANAGE_GRID_OPTION).toString());

        return "/delivery/shipCompanyManageMain";
    }

    /**
     * 택배사코드관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipCompanyManageList.json", method = RequestMethod.GET)
    public List<ShipCompanyManageListGetDto> getShipCompanyManageList(ShipCompanyManageListSetDto shipCompanyManageListSetDto) {
        return shipCompanyManageService.getShipCompanyManageList(shipCompanyManageListSetDto);
    }

    /**
     * 연동업체코드 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getLinkCompanyCodeList.json", method = RequestMethod.GET)
    public List<ShipLinkCompanyCodeListGetDto> getLinkCompanyCodeList(@RequestParam(value = "company") String company) {
        return shipCompanyManageService.getLinkCompanyCodeList(company);
    }

    /**
     * 택배사코드관리 상세정보 저장
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setShipCompanyInfo.json")
    public ResponseObject<Object> setShipCompanyInfo(@RequestBody ShipCompanyManageInfoSetDto shipCompanyManageInfoSetDto) throws Exception {
        return shipCompanyManageService.setShipCompanyInfo(shipCompanyManageInfoSetDto);
    }

    /**
     * 연동업체설정 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipLinkCompanySetList.json", method = RequestMethod.GET)
    public List<ShipLinkCompanySetListGetDto> getShipLinkCompanySetList() {
        return shipCompanyManageService.getShipLinkCompanySetList();
    }

    /**
     * 연동업체설정 저장
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setShipLinkCompanySet.json")
    public ResponseObject<Object> setShipLinkCompanySet(@RequestBody ShipLinkCompanySetDto shipLinkCompanySetDto) throws Exception {
        return shipCompanyManageService.setShipLinkCompanySet(shipLinkCompanySetDto);
    }

    /**
     * 연동업체설정 팝업
     */
    @GetMapping("/popup/getShipLinkCompanySetPop")
    public String getShipLinkCompanySetPop(Model model, @RequestParam(value="callback") String callBackScript) throws Exception {
        model.addAttribute("callBackScript",callBackScript);
        model.addAttribute("dlvLinkCompany", shipCommonService.getCode("dlv_link_company"));
        model.addAttribute("shipLinkCompanySetPopGridBaseInfo", new RealGridBaseInfo("shipLinkCompanySetPopGridBaseInfo", SHIP_LINK_COMPANY_SET_POP_GRID_HEAD, SHIP_LINK_COMPANY_SET_POP_GRID_OPTION).toString());

        return "/delivery/pop/shipLinkCompanySetPop";
    }
}
