package kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class DailyOrderInfoStoreGetDto {
  @ApiModelProperty(notes = "점포")
  @RealGridColumnInfo(headText = "점포", width = 150)
  private String storeNm;
  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", width = 100)
  private String storeId;
  @ApiModelProperty(notes = "주문건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderQty;
  @ApiModelProperty(notes = "주문금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;
  @ApiModelProperty(notes = "취소건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cancelQty;
  @ApiModelProperty(notes = "취소금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cancelPrice;
  @ApiModelProperty(notes = "반품건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String returnQty;
  @ApiModelProperty(notes = "반품금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String returnPrice;
  @ApiModelProperty(notes = "대체건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "대체", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String subQty;
  @ApiModelProperty(notes = "대체금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "대체", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String subPrice;
}
