package kr.co.homeplus.admin.web.order.utils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderLogUtil {

    private final LoginCookieService loginCookieService;
    private final PrivacyLogService privacyLogService;
    private static LoginCookieService cookieService;
    private static PrivacyLogService logService;

    @PostConstruct
    private void initialize() {
        cookieService = loginCookieService;
        logService = privacyLogService;
    }

    public static void privacyLogSend(PersonalLogMethod logMethod, String userNo, String actionNm){
        sendLog(createPrivacyLogInfo(logMethod, userNo, actionNm, ""));
    }

    public static void privacyLogSend(PersonalLogMethod logMethod, String userNo, String actionNm, String sql){
        sendLog(createPrivacyLogInfo(logMethod, userNo, actionNm, sql));
    }

    public static HttpHeaders createPrivacySendAndHeader(PersonalLogMethod logMethod, String userNo, String actionNm, String sql){
        privacyLogSend(logMethod, userNo, actionNm, sql);
        return getCreateHeader();
    }

    public static HttpHeaders createPrivacySendAndHeader(PersonalLogMethod logMethod, String userNo, String actionNm){
        privacyLogSend(logMethod, userNo, actionNm);
        return getCreateHeader();
    }

    public static HttpHeaders createPrivacyHeadder(){
        return getCreateHeader();
    }

    private static HttpHeaders getCreateHeader(){
        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, getRemoteIp());
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, getUserId());
        return headers;
    }

    private static PrivacyLogInfo createPrivacyLogInfo(PersonalLogMethod logMethod, String actionNm, String userNo, String sql) {
        return PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(getRemoteIp())
            .userId(getUserId())
            .txCodeUrl(getRequest().getRequestURI())
            .txMethod(logMethod.name())
            .txCodeName(actionNm)
            .processor(String.valueOf(userNo))
            .processorTaskSql(sql)
            .build();
    }

    private static HttpServletRequest getRequest(){
        return ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest();
    }

    private static String getRemoteIp(){
        return ServletUtils.clientIP(getRequest());
    }

    private static String getUserId(){
        return cookieService.getUserInfo().getUserId();
    }

    private static void sendLog(PrivacyLogInfo privacyLogInfo){
        logService.send(privacyLogInfo);
    }

}
