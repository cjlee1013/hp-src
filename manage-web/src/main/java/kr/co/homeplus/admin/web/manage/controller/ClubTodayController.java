package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.clubToday.ClubTodayItemListSelectDto;
import kr.co.homeplus.admin.web.manage.model.clubToday.ClubTodayListParamDto;
import kr.co.homeplus.admin.web.manage.model.clubToday.ClubTodayListSelectDto;
import kr.co.homeplus.admin.web.manage.model.clubToday.ClubTodaySetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/clubToday")
public class ClubTodayController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트 관리 >  전시관리 > 클럽투데이 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/clubTodayMain", method = RequestMethod.GET)
    public String clubTodayMain(Model model ) {

        codeService.getCodeModel(model
            ,   "dsp_club_disp_type"    //클럽 투데이 전시여부
            ,   "dsp_club_period_type"  // 클럽 투데이 조회기간 기준
            ,   "dsp_club_sch_type"     // 클럽 투데이 검색어 구분
        );

        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("clubTodayListGridBaseInfo", ClubTodayListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("clubTodayItemListGridBaseInfo", ClubTodayItemListSelectDto.class));

        return "/manage/clubTodayMain";
    }

    /**
     * 사이트 관리 >  전시관리 > 클럽투데이 리스트
     *
     * @param listParamDto
     * @return GnbListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getClubTodayList.json"}, method = RequestMethod.GET)
    public List<ClubTodayListSelectDto> getClubTodayList(ClubTodayListParamDto listParamDto) {
        String apiUri = "/manage/clubToday/getClubTodayList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ClubTodayListSelectDto>>>() {};
        return (List<ClubTodayListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil
            .getRequestString(apiUri, ClubTodayListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     *사이트 관리 >  전시관리 > 클럽투데이 아이템
     *
     * @param todayNo
     * @return NoticeListParamDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getClubTodayItemList.json"}, method = RequestMethod.GET)
    public List<ClubTodayItemListSelectDto> getClubTodayItemList(@RequestParam(value="todayNo") String todayNo) {
        String apiUri = "/manage/clubToday/getClubTodayItemList?todayNo="+todayNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ClubTodayItemListSelectDto>>>() {};
        return (List<ClubTodayItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri, typeReference).getData();
    }

    /**
     * 사이트 관리 >  전시관리 > 클럽투데이 등록/수정
     * @param clubTodaySetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setClubToday.json"}, method = RequestMethod.POST)
    public ResponseResult setClubToday(@RequestBody ClubTodaySetParamDto clubTodaySetParamDto) {
        String apiUri = "/manage/clubToday/setClubToday";
        clubTodaySetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, clubTodaySetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
