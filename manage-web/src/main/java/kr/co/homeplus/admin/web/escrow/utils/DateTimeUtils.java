package kr.co.homeplus.admin.web.escrow.utils;

import static java.time.temporal.ChronoField.ALIGNED_WEEK_OF_MONTH;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.time.DateUtils;

public class DateTimeUtils {
    // LocalDate
    public static final String DIGIT_YMD = "yyyyMMdd";

    public static final String DEFAULT_YMD = "yyyy-MM-dd";

    // LocalDateTime
    public static final String DIGIT_YMD_HIS = "yyyyMMddHHmmss";

    public static final String DEFAULT_YMD_HIS = "yyyy-MM-dd HH:mm:ss";

    public static final String MILL_YMD_HIS = "yyyy-MM-dd HH:mm:ss.SSS";

    ////// Formatter 가져오기 //////
    /**
     * format String to Formatter
     * parameter 로 받아온 format 문자열로 DateTimeFormatter return
     * @param formatStr Pattern String
     * @return formatter by pattern
     */
    private static DateTimeFormatter getFormatter(String formatStr) {
        return DateTimeFormatter.ofPattern(formatStr);
    }

    /**
     * Default DateTime Formatter 리턴
     * @return DateTimeFormatter - yyyy-MM-dd HH:mm:ss
     */
    private static DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern(DEFAULT_YMD_HIS);
    }


    ////// 현재 날짜,시간 가져오기 //////
    /**
     * 현재 날짜 가져오기
     * Default Date Format yyyy-MM-dd
     * @return dateTimeStr
     */
    public static String getCurrentYmd() {
        return LocalDate.now().format(getFormatter(DEFAULT_YMD));
    }

    /**
     * 현재 날짜 가져오기
     * 시간을 갖지 않는 날짜 format 으로 return 가능
     * @return dateTimeStr
     */
    public static String getCurrentYmd(String dateFormatStr) {
        return LocalDate.now().format(getFormatter(dateFormatStr));
    }

    /**
     * 오늘 날짜 가져오기
     * Default DateTime Format yyyy-MM-dd HH:mm:ss
     * @return dateTimeStr
     */
    public static String getToday() { //getNow, getCurrent
        return LocalDateTime.now().format(getFormatter(DEFAULT_YMD_HIS));
    }

    /**
     * 현재 날짜/시간 가져오기
     * Default DateTime Format yyyy-MM-dd HH:mm:ss
     * @return dateTimeStr
     */
    public static String getCurrentYmdHis() {
        return LocalDateTime.now().format(getFormatter(DEFAULT_YMD_HIS));
    }

    /**
     * 현재 날짜/시간 가져오기
     * 날짜, 시간을 가지는 format 으로 return 가능
     * @return dateTimeStr
     */
    public static String getCurrentYmdHis(String dateTimeFormatStr) {
        return LocalDateTime.now().format(getFormatter(dateTimeFormatStr));
    }


    ////// parsing utils - String -> Date, Date -> String, String -> String //////
    /**
     * LocalDateTime -> formatted String
     * Default DateTime Format yyyy-MM-dd HH:mm:ss
     * @param sourceDateTime LocalDateTime
     * @return String
     */
    public static String parseDateTimeToStr(LocalDateTime sourceDateTime) {
        return parseDateTimeToStr(sourceDateTime, DEFAULT_YMD_HIS);
    }

    /**
     * LocalDateTime -> formatted String
     * @param sourceDateTime LocalDateTime
     * @param dateTimeFormatStr 시간을 가지는 format String
     * @return formatted dateTimeString
     */
    public static String parseDateTimeToStr(LocalDateTime sourceDateTime, String dateTimeFormatStr) {
        return sourceDateTime.format(getFormatter(dateTimeFormatStr));
    }

    /**
     * date String -> date String
     * @param sourceDateStr 바꾸고 싶은 날짜 문자열
     * @param fromDateFormatStr 현재 문자열의 format
     * @param toDateFormatStr 바꾸고자 하는 날짜 문자열의 format
     */
    public static String changeDateStrFormat(String sourceDateStr, String fromDateFormatStr, String toDateFormatStr) {
        LocalDate date = LocalDate.parse(sourceDateStr, getFormatter(fromDateFormatStr));
        return date.format(getFormatter(toDateFormatStr));
    }

    /**
     * dateTime String -> dateTime String
     * @param sourceDateTimeStr 바꾸고 싶은 날짜 문자열
     * @param fromDateTimeFormatStr 현재 문자열의 format
     * @param toDateTimeFormatStr 바꾸고자 하는 날짜 문자열의 format
     */
    public static String changeDateTimeStrFormat(String sourceDateTimeStr, String fromDateTimeFormatStr, String toDateTimeFormatStr) {
        LocalDateTime dateTime = LocalDateTime.parse(sourceDateTimeStr, getFormatter(fromDateTimeFormatStr));
        return dateTime.format(getFormatter(toDateTimeFormatStr));
    }

    /**
     * date String -> dateTime String
     * @param sourceDateStr 바꾸고 싶은 날짜 문자열
     * @param fromDateFormatStr 현재 문자열의 format
     * @param toDateTimeFormatStr 바꾸고자 하는 날짜 문자열의 format
     * @param hour 붙여줄 시간
     * @param min 붙여줄 분
     * @param sec 붙여줄 초
     */
    public static String changeDateStrToTimeStr(String sourceDateStr, String fromDateFormatStr, String toDateTimeFormatStr, int hour, int min, int sec) {
        LocalDate date = LocalDate.parse(sourceDateStr, getFormatter(fromDateFormatStr));
        LocalTime time = LocalTime.of(hour, min, sec);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        return dateTime.format(getFormatter(toDateTimeFormatStr));
    }


    ////// 비교 utils //////
    /**
     * 현재 시간 기준 이전 날짜인지 확인
     * @param sourceDateTimeStr 체크할 시간 형식 string
     * Default DateTime Format yyyy-MM-dd HH:mm:ss 으로 parsing
     */
    public static boolean isBeforeNow(String sourceDateTimeStr) {
        return isBeforeNow(sourceDateTimeStr, DEFAULT_YMD_HIS);
    }

    /**
     * 현재 시간 기준 이전 날짜인지 확인
     * sourceDateTimeFormatStr 으로 parsing
     * @param sourceDateTimeStr 체크할 시간 형식 string
     * @param sourceDateTimeFormatStr 체크할 문자열의 format
     */
    public static boolean isBeforeNow(String sourceDateTimeStr, String sourceDateTimeFormatStr) {
        LocalDateTime localDateTime = LocalDateTime.parse(sourceDateTimeStr, getFormatter(sourceDateTimeFormatStr));
        return localDateTime.isBefore(LocalDateTime.now());
    }

    /**
     * 현재 시간 기준 이후 날짜인지 확인
     * @param sourceDateTimeStr 체크할 시간 형식 string
     * Default DateTime Format yyyy-MM-dd HH:mm:ss 으로 parsing
     */
    public static boolean isAfterNow(String sourceDateTimeStr) {
        return isAfterNow(sourceDateTimeStr, DEFAULT_YMD_HIS);
    }

    /**
     * 현재 시간 기준 이후 날짜인지 확인
     * sourceDateTimeFormatStr 으로 parsing
     * @param sourceDateTimeStr 체크할 시간 형식 string
     * @param sourceDateTimeFormatStr 체크할 문자열의 format
     */
    public static boolean isAfterNow(String sourceDateTimeStr, String sourceDateTimeFormatStr) {
        LocalDateTime localDateTime = LocalDateTime.parse(sourceDateTimeStr, getFormatter(sourceDateTimeFormatStr));
        return localDateTime.isAfter(LocalDateTime.now());
    }

    /**
     * DateTime String 비교
     * Default DateTime Format yyyy-MM-dd HH:mm:ss 으로 parsing
     * dateStr1 > dateStr2 : -n
     * dateStr1 = dateStr2 : 0
     * dateStr1 < dateStr2 : +n
     */
    public static int compareDateTimeStr(String dateStr1, String dateStr2) {
        LocalDateTime localDateTime1 = LocalDateTime.parse(dateStr1, getFormatter());
        LocalDateTime localDateTime2 = LocalDateTime.parse(dateStr2, getFormatter());
        return localDateTime1.compareTo(localDateTime2);
    }

    /**
     * DateTime String 비교
     * dateTimeFormatStr 로 parsing
     * dateStr1 > dateStr2 : -n
     * dateStr1 = dateStr2 : 0
     * dateStr1 < dateStr2 : +n
     */
    public static int compareDateTimeStr(String dateStr1, String dateStr2, String dateTimeFormatStr) {
        LocalDateTime localDateTime1 = LocalDateTime.parse(dateStr1, getFormatter(dateTimeFormatStr));
        LocalDateTime localDateTime2 = LocalDateTime.parse(dateStr2, getFormatter(dateTimeFormatStr));
        return localDateTime1.compareTo(localDateTime2);
    }


    ////// dateTime String 사용하여 날짜 계산 utils //////
    public static String plusMinutes(String dateTimeStr, long minutes) {
        return plusMinutes(dateTimeStr, minutes, DEFAULT_YMD_HIS);
    }
    public static String plusMinutes(String dateTimeStr, long minutes, String dateTimeFormatStr) {
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, getFormatter(dateTimeFormatStr));
        return dateTime.plusMinutes(minutes).format(getFormatter(dateTimeFormatStr));
    }

    public static String plusDays(String dateTimeStr, long days) {
        return plusDays(dateTimeStr, days, DEFAULT_YMD_HIS);
    }
    public static String plusDays(String dateTimeStr, long days, String dateTimeFormatStr) {
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, getFormatter(dateTimeFormatStr));
        return dateTime.plusDays(days).format(getFormatter(dateTimeFormatStr));
    }

    public static String minusMinutes(String dateTimeStr, long minutes) {
        return minusMinutes(dateTimeStr, minutes, DEFAULT_YMD_HIS);
    }
    public static String minusMinutes(String dateTimeStr, long minutes, String dateTimeFormatStr) {
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, getFormatter(dateTimeFormatStr));
        return dateTime.minusMinutes(minutes).format(getFormatter(dateTimeFormatStr));
    }

    public static String minusDays(String dateTimeStr, long days) {
        return minusDays(dateTimeStr, days, DEFAULT_YMD_HIS);
    }
    public static String minusDays(String dateTimeStr, long days, String dateTimeFormatStr) {
        LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, getFormatter(dateTimeFormatStr));
        return dateTime.minusDays(days).format(getFormatter(dateTimeFormatStr));
    }

    ////// check utils //////
    /**
     * 만료체크(분 단위)
     * @param checkDateTimeStr 만료체크 대상 Str
     * @param minusMinutes 만료기간
     */
    public static boolean isExpireCheckMin(String checkDateTimeStr, long minusMinutes) {
        LocalDateTime checkDateTime = LocalDateTime.parse(checkDateTimeStr, getFormatter());
        return checkDateTime.compareTo(LocalDateTime.now().minusMinutes(minusMinutes)) < 0;
    }

    /**
     * 만료체크(일 단위)
     * @param checkDateTimeStr 만료체크 대상 Str
     * @param minusDays 만료기간
     */
    public static boolean isExpireCheckDay(String checkDateTimeStr, long minusDays) {
        LocalDateTime checkDateTime = LocalDateTime.parse(checkDateTimeStr, getFormatter());
        return checkDateTime.compareTo(LocalDateTime.now().minusDays(minusDays)) < 0;
    }

    /**
     * DateTime 형 포맷만 사용 가능
     * format : 2020-01-01 00:00:00
     * 시작일/종료일 체크
     * 시작일,종료일 정상 : true (시작일 <= 종료일)
     * 시작일,종료일 비정상 : false (시작일 > 종료일)
     */
    public static boolean checkStartEndDt(String startDt, String endDt) {
        return checkStartEndDt(startDt, endDt, DEFAULT_YMD_HIS);
    }

    /**
     * DateTime 형 포맷만 사용 가능
     * format : 2020-01-01 00:00:00
     * 시작일/종료일 체크
     * 시작일,종료일 정상 : true (시작일 <= 종료일)
     * 시작일,종료일 비정상 : false (시작일 > 종료일)
     */
    public static boolean checkStartEndDt(String startDt, String endDt, String dateTimeFormatStr) {
        LocalDateTime ldtFrom = LocalDateTime.parse(startDt, getFormatter(dateTimeFormatStr));
        LocalDateTime ldtTo = LocalDateTime.parse(endDt, getFormatter(dateTimeFormatStr));
        return ldtFrom.compareTo(ldtTo) <= 0;
    }

    /**
     * 날짜 문자열이 지정한 날짜 패턴과 일치하는지 체크(엄격모드)
     * @param dateStr 날짜문자열
     * @param parsePattern 문자열 패턴
     * @return true: 패턴일치, false: 패턴일치하지 않음
     */
    public static boolean isValidParseDateStrictly(String dateStr, String parsePattern) {
        try {
            DateUtils.parseDateStrictly(dateStr, parsePattern);
            return true;
        } catch (Exception e) {}
        return false;
    }

    ////// Calendar 관련 utils //////
    /**
     * 달의 요일 가져오기 (1~7)
     */
    public static int getDayOfWeek(int year, int month, int dayOfMonth) {
        LocalDate date = LocalDate.of(year, month, dayOfMonth);
        return date.getDayOfWeek().getValue();
    }

    /**
     * 달의 1일자 요일 가져오기 (1~7)
     */
    public static int getFirstDayOfWeek(int year, int month) {
        int lastDayOfWeek = DateTimeUtils.getDayOfWeek(year, month, 1);
        return lastDayOfWeek;
    }

    /**
     * 달의 마지막 날 가져오기 (28, 29, 30, 31)
     */
    public static int getEndOfMonth(int year, int month) {
        LocalDate date = LocalDate.of(year, month, 1);
        LocalDate end = date.withDayOfMonth(date.lengthOfMonth());
        return end.getDayOfMonth();
    }

    /**
     * 달의 마지막 날 요일 가져오기 (1~7)
     */
    public static int getLastDayOfWeek(int year, int month) {
        int lastDay = DateTimeUtils.getEndOfMonth(year, month);
        int lastDayOfWeek = DateTimeUtils.getDayOfWeek(year, month, lastDay);
        return lastDayOfWeek;
    }

    /**
     * 해당하는 일자의 주차(week) 가져오기
     */
    public static int getDayWeekOfMonth(int year, int month, int dayOfMonth) {
        LocalDate date = LocalDate.of(year, month, dayOfMonth);
        return date.get(ALIGNED_WEEK_OF_MONTH);
    }

    /**
     * parameter 로 받아온 일자를 String 포멧으로 리턴 (YYYY-MM-DD)
     */
    public static String getDigitYmd(int year, int month, int dayOfMonth) {
        LocalDate date = LocalDate.of(year, month, dayOfMonth);
        return date.toString();
    }
}