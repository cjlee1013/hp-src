package kr.co.homeplus.admin.web.user.model.user;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
/** 어드민 > 회원등급 변경 팝업 > 회원정보 DTO **/
public class UserGradeInfo {
    /** 회원번호 **/
    private long userNo;
    /** 회원명 **/
    private String userNm;
    /** 회원아이디 **/
    private String userId;
    /** 성별 **/
    private String gender;
}
