package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.RelatedKeywordGetParamDto;
import kr.co.homeplus.admin.web.search.model.RelatedKeywordSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RelatedKeywordService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getExposureItemList(RelatedKeywordGetParamDto param) {
        String apiUri = "/relatedKeyword/getItemList";
        return getResponseObject(apiUri, param);
    }

    public ResponseObject setExposureItem(RelatedKeywordSetParamDto param) {
        String apiUri = "/relatedKeyword/setItem";
        param.setCreator(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
