package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErsPromoSetDto {
    private String eventCd;

    private String useYn;

    private String empNo;

    private String purchaseGuide;
}