package kr.co.homeplus.admin.web.core.utility.realgrid;

import lombok.Data;

@Deprecated(forRemoval = true)
@Data
public class RealGridButtonProperty {
    private RealGridButtonType button;
    private String popupMenu;
    private boolean alwaysShowButton;
}
