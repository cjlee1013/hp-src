package kr.co.homeplus.admin.web.settle.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 IF")
public class BalanceSheetInterfaceSetDto {
  @ApiModelProperty(notes = "기준일")
  private String basicDt;

  @ApiModelProperty(notes = "전표유형")
  private String slipType;

  @ApiModelProperty(notes = "수정자")
  private String chgId;

  public void setChgId(String chgId) {
    this.chgId = chgId;
  }
}
