package kr.co.homeplus.admin.web.message.service;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.io.IOException;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.enums.WorkType;
import kr.co.homeplus.admin.web.message.model.ContentInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@Service
@Slf4j
public class ContentSupportService {

    private final ResourceClient resourceClient;

    private final LoginCookieService loginCookieService;

    public ResponseObject<String> setAttachment(final MultipartFile file)
        throws IOException {
        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, file.getBytes(),
                "/content/attachment/byte");
    }

    public ResponseObject<String> deleteAttachment(final long contentSeq) {
        return resourceClient.postForResponseObject(ResourceRouteName.MESSAGE, contentSeq,
            "/content/attachment/delete");
    }

    public ResponseObject<ContentInfo> insertContentInfo(final ContentInfo contentInfo) {
        contentInfo.setRegId(getEmpId());
        contentInfo.setChgId(getEmpId());
        ContentInfo replacedContentInfo = replace(contentInfo);
        return resourceClient.postForResponseObject(
            ResourceRouteName.MESSAGE, replacedContentInfo, "/content/insert");
    }

    public ResponseObject<Long> updateContentInfo(final ContentInfo contentInfo) {
        contentInfo.setChgId(getEmpId());
        ContentInfo replacedContentInfo = replace(contentInfo);
        return resourceClient.postForResponseObject(
            ResourceRouteName.MESSAGE, replacedContentInfo, "/content/update");
    }

    private String getEmpId() {
        return loginCookieService.getUserInfo().getEmpId();
    }

    private ContentInfo replace(ContentInfo contentInfo) {
        contentInfo.setName(replace(contentInfo.getName()));
        contentInfo.setDescription(replace(contentInfo.getDescription()));
        contentInfo.setCallback(replace(contentInfo.getCallback()));
        contentInfo.setEtc1(replace(contentInfo.getEtc1()));
        contentInfo.setEtc2(replace(contentInfo.getEtc2()));
        contentInfo.setEtc3(replace(contentInfo.getEtc3()));
        contentInfo.setEtc4(replace(contentInfo.getEtc4()));
        contentInfo.setEtc5(replace(contentInfo.getEtc5()));
        contentInfo.setContentSubject(replace(contentInfo.getContentSubject()));
        if (WorkType.MAIL != contentInfo.getWorkType()) {
            contentInfo.setContentBody(replace(contentInfo.getContentBody()));
        }
        return contentInfo;
    }

    private String replace(String str) {
        String result = XssPreventer.unescape(str);
        // XssSaxFilter를 쓰는경우 특수문자 앞에 gt가 붙어들어가는 현상이 있어 제거
        return result.replaceFirst(">@", "@");
    }
}
