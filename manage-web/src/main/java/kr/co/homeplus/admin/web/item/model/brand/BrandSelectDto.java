package kr.co.homeplus.admin.web.item.model.brand;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandSelectDto {

    private Long brandNo;

    private String brandNm;

    private String brandNmEng;

    private String imgUrl;

    private String brandDesc;

    private String useYn;

    private String itemDispYn;

    private String siteUrl;

    private String dispNm;

    private String initial;

}
