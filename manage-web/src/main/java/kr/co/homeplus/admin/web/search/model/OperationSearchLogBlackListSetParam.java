package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@ApiModel(description = "검색로그관  > 블랙리스트 IP 등록 ")
public class OperationSearchLogBlackListSetParam {

    @ApiParam(value = "서비스 유형 ( 홈플러스 : HOMEPLUS, 익스프레스 : EXP  )", required = true)
    private String serviceType;
    @ApiParam(value = "블랙리스트 유형")
    private String blacklistType;
    @ApiParam(value = "블랙리스트 검색 ip")
    private String regKeyword;
    @ApiParam(value = "유효시작일")
    private String regStartDt;
    @ApiParam(value = "유효종료일")
    private String regEndDt;
    @ApiParam(value = "비고 (메모) ")
    private String regMemo;
    @ApiParam(value = "등록자 ")
    private String regId;
    @ApiParam(value = "수정자  ")
    private String chgId;
    @ApiParam(value="활성화 상태")
    private String enableYn;
    @ApiParam(value = "블랙리스트 IP No list")
    private Integer[] ips;
    @ApiParam(value = "액션 유형 (REG/MOD)")
    private String actionType;

}
