package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressParam {
    private String page;
    private int perPage;
    private String query;
}
