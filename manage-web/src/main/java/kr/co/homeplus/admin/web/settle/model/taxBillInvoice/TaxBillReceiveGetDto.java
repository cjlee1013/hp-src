package kr.co.homeplus.admin.web.settle.model.taxBillInvoice;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(checkBar = true)
public class TaxBillReceiveGetDto {
    @ApiModelProperty(value = "확정여부")
    @RealGridColumnInfo(headText = "확정여부", sortable = true, width = 100)
    private String confirmYn;

    @ApiModelProperty(value = "수취ID")
    @RealGridColumnInfo(headText = "수취ID", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_STR)
    private String taxReceiveNo;

    @ApiModelProperty(value = "발행일")
    @RealGridColumnInfo(headText = "발행일", sortable = true, width = 120)
    private String issueDt;

    @ApiModelProperty(value = "공급처")
    @RealGridColumnInfo(headText = "공급처", sortable = true, width = 100)
    private String supplier;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", sortable = true, width = 100)
    private String vendorCd;

    @ApiModelProperty(value = "세금계산서발행타입")
    @RealGridColumnInfo(headText = "수취유형", sortable = true, width = 110)
    private String taxBillReceiveType;

    @ApiModelProperty(value = "상호")
    @RealGridColumnInfo(headText = "상호명", sortable = true, width = 150)
    private String partnerNm;

    @ApiModelProperty(value = "사업자번호")
    @RealGridColumnInfo(headText = "사업자번호", sortable = true, width = 120)
    private String partnerNo;

    @ApiModelProperty(value = "대표자명")
    @RealGridColumnInfo(headText = "대표자명", sortable = true, width = 100)
    private String partnerOwner;

    @ApiModelProperty(value = "합계금액")
    @RealGridColumnInfo(headText = "합계금액", groupName = "결제기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long sumAmt;

    @ApiModelProperty(value = "공급가")
    @RealGridColumnInfo(headText = "공급가", groupName = "결제기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long supplyAmt;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", groupName = "결제기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long vatAmt;

    @ApiModelProperty(value = "합계금액")
    @RealGridColumnInfo(headText = "합계금액", groupName = "입금기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long commissionSumAmt;

    @ApiModelProperty(value = "공급가")
    @RealGridColumnInfo(headText = "공급가", groupName = "입금기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long commissionSupplyAmt;

    @ApiModelProperty(value = "부가세")
    @RealGridColumnInfo(headText = "부가세", groupName = "입금기준", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long commissionVatAmt;

    @ApiModelProperty(value = "차이금액")
    @RealGridColumnInfo(headText = "차이금액", sortable = true, width = 120, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffAmt;

    @ApiModelProperty(value = "등록일시")
    @RealGridColumnInfo(headText = "등록일시", sortable = true, width = 160)
    private String regDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", sortable = true, width = 150, columnType = RealGridColumnType.NUMBER_STR)
    private String regId;

    @ApiModelProperty(value = "변경일시")
    @RealGridColumnInfo(headText = "변경일시", sortable = true, width = 160)
    private String chgDt;

    @ApiModelProperty(value = "변경자")
    @RealGridColumnInfo(headText = "변경자", sortable = true, width = 150, columnType = RealGridColumnType.NUMBER_STR)
    private String chgId;

    @ApiModelProperty(value = "공급업체ID")
    @RealGridColumnInfo(headText = "공급업체ID", width = 100, hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "세금계산서발행타입")
    @RealGridColumnInfo(headText = "수취유형", width = 100, hidden = true)
    private String taxBillReceiveTypeCd;
}
