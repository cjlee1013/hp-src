package kr.co.homeplus.admin.web.item.model.expCategoryItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpCategoryItemListParamDto {

    private String searchType;
    private String searchKeyword;
    private String lcateCd;
    private String searchStatusYn;

}
