package kr.co.homeplus.admin.web.delivery.utils;

public class ShippigUtil {

    /*
     * 택배배송 코드값 확인
     */
    public static boolean isDlv(String shipMethod) {
        String[] shipMethodArr = shipMethod.split("_");
        String method = shipMethodArr.length == 1 ? shipMethodArr[0] : shipMethodArr[1];

        return "DLV".equals(method);
    }

    /*
     * 직접배송 코드값 확인
     */
    public static boolean isDrct(String shipMethod) {
        String[] shipMethodArr = shipMethod.split("_");
        String method = shipMethodArr.length == 1 ? shipMethodArr[0] : shipMethodArr[1];

        return "DRCT".equals(method);
    }

    /*
     * 문자열 안에 공백 있는지 확인
     */
    public static boolean spaceCheck(String spaceCheck) {
        for(int i = 0 ; i < spaceCheck.length() ; i++)
        {
            if(spaceCheck.charAt(i) == ' ')
                return true;
        }
        return false;
    }
}