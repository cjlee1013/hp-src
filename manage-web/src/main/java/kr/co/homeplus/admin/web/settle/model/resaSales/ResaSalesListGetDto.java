package kr.co.homeplus.admin.web.settle.model.resaSales;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "SALES 자료조회 목록")
public class ResaSalesListGetDto {
  @ApiModelProperty(notes = "일자")
  @RealGridColumnInfo(headText = "일자", width = 100)
  private String basicDt;

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", width = 100)
  private String storeId;

  @ApiModelProperty(notes = "점포명")
  @RealGridColumnInfo(headText = "점포명", width = 100)
  private String storeNm;

  @ApiModelProperty(notes = "코스트센터")
  @RealGridColumnInfo(headText = "코스트센터", width = 100)
  private String costCenter;

  @ApiModelProperty(notes = "ReSA_SALES")
  @RealGridColumnInfo(headText = "SALES", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resaSales;

  @ApiModelProperty(notes = "ReSA_15191")
  @RealGridColumnInfo(headText = "15191", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resa15191;

  @ApiModelProperty(notes = "ReSA_20720")
  @RealGridColumnInfo(headText = "20720", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resa20720;

  @ApiModelProperty(notes = "ReSA_64800")
  @RealGridColumnInfo(headText = "64800", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resa64800;

  @ApiModelProperty(notes = "ReSA_72905")
  @RealGridColumnInfo(headText = "72905", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resa72905;

  @ApiModelProperty(notes = "ReSA_20744")
  @RealGridColumnInfo(headText = "20744", sortable = true, width = 100, groupName="ReSA", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String resa20744;

  @ApiModelProperty(notes = "정산_Sales")
  @RealGridColumnInfo(headText = "SALES", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settleSales;

  @ApiModelProperty(notes = "정산_15191")
  @RealGridColumnInfo(headText = "15191", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle15191;

  @ApiModelProperty(notes = "정산_20720")
  @RealGridColumnInfo(headText = "20720", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle20720;

  @ApiModelProperty(notes = "정산_20720_DS_장바구니")
  @RealGridColumnInfo(headText = "20720_DS_장바구니", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle20720DsCart;

  @ApiModelProperty(notes = "정산_64800")
  @RealGridColumnInfo(headText = "64800", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle64800;

  @ApiModelProperty(notes = "정산_72905")
  @RealGridColumnInfo(headText = "72905", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle72905;

  @ApiModelProperty(notes = "정산_20744")
  @RealGridColumnInfo(headText = "20744", sortable = true, width = 100, groupName="정산", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String settle20744;

  @ApiModelProperty(notes = "차이내역_Sales")
  @RealGridColumnInfo(headText = "SALES", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diffSales;

  @ApiModelProperty(notes = "차이내역_15191")
  @RealGridColumnInfo(headText = "15191", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diff15191;

  @ApiModelProperty(notes = "차이내역_20720")
  @RealGridColumnInfo(headText = "20720", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diff20720;

  @ApiModelProperty(notes = "차이내역_64800")
  @RealGridColumnInfo(headText = "64800", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diff64800;

  @ApiModelProperty(notes = "차이내역_72905")
  @RealGridColumnInfo(headText = "72905", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diff72905;

  @ApiModelProperty(notes = "차이내역_20744")
  @RealGridColumnInfo(headText = "20744", sortable = true, width = 100, groupName="차이내역", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String diff20744;
}
