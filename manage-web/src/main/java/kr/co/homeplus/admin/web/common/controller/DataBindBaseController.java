package kr.co.homeplus.admin.web.common.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

@Controller
public class DataBindBaseController {
    //동적 리스트 바인딩의 문제로 인해 (사이즈
    //Spring Databinder에 설정된 DEFAULT_AUTO_GROW_COLLECTION_LIMIT=256 의 사이즈를 늘림
    @Value("${spring.application.autoGrowCollectionLimit}")
    private int autoGrowCollectionLimit;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.setAutoGrowCollectionLimit(autoGrowCollectionLimit);
    }
}
