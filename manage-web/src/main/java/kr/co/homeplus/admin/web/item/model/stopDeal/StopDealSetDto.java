package kr.co.homeplus.admin.web.item.model.stopDeal;

import lombok.Data;
@Data
public class StopDealSetDto {

    private String itemNo;

    private int storeId;

    private String stopDealYn;

    private String stopStartDt;

    private String stopEndDt;

    private String stopReason;
}
