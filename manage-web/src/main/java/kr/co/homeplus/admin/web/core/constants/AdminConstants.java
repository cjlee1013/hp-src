package kr.co.homeplus.admin.web.core.constants;

import kr.co.homeplus.plus.api.support.client.TimeoutConfig;

/**
 * 어드민 내 상수를 저장하는 클래스
 */
public class AdminConstants {
    public static final String ADMIN_USER_TOKEN = "userInfo";

    /** 상품 이미지 업로드시에 사용하는 uri */
    public static final String ADMIN_IMAGE_UPLOAD_URI = "/store/single/image";

    /** 상품 이미지 업로드시에 사용하는 uri (URL 업로드) */
    public static final String ADMIN_IMAGE_UPLOAD_URL_URI = "/store/url/image";

    /** 상품 이미지 업로드시에 사용하는 파일의 key name */
    public static final String ADMIN_IMAGE_UPLOAD_FIELD_NAME = "file";

    /** 상품 이미지 업로드시에 사용하는 server key name */
    public static final String ADMIN_IMAGE_UPLOAD_KEY = "processKey";

    /** 이미지 업로드 시 유효성 검사에 사용할 URI */
    public static final String ADMIN_IMAGE_UPLOAD_VALIDATOR = "/store/single/image";

    /** 이미지 업로드 시 유효성 검사에 사용되는 필수 값 'mode'에서 사용 */
    public static final String ADMIN_IMAGE_UPLOAD_VALIDATOR_MOD = "validate";

    /** 파일 업로드시에 사용하는 uri */
    public static final String ADMIN_FILE_UPLOAD_URI = "/store/single/file";

    /**
     * 권한 api에서 어드민 사용자 로그인 요청 URI
     */
    public static final String AUTH_ADMIN_LOGIN_URI = "/auth/user/login";

    /** 권한 api에서 사용자 접근 가능한 메뉴 조회 시 사용할 URI */
    public static final String AUTH_PERMIT_MENU_URI = "/menus";

    /** auth-api 를 통한 역할 인가 URI */
    public static final String ADMIN_ROLE_NAME_PERMISSION_CHECK_URI = "/admin/authorized/hasRole";
    /** auth-api 를 통한 역할 인가 URI */
    public static final String ADMIN_ROLE_NAME_POLICY_NAME_PERMISSION_CHECK_URI = "/admin/authorized/hasRolePolicy";
    /** 권한 api에서 사용자에게 접근 가능한 메뉴정보 조회 시 사용할 URI(신 버전) */
    public static final String AUTH_AUTHORIZED_MENUS_URI = "/admin/authorized/menus";
    /** 어드민 메뉴 접근시 권한 검증을 조회 **/
    public static final String AUTH_AUTHORIZED_MENUS_NOT_PERMITTED_URI = "/admin/authorized/menus/notPermitted";

    /** 어드민 로그인 로그 저장 URI(auth-api) **/
    public static final String ADMIN_LOG_INSERT_LOGIN_LOG = "/admin/log/insertLoginLog";

    /**
     * 어드민 ajax 호출 시 규약 된 url suffix
     */
    public static final String REST_URI_SUFFIX = ".json";
    /**
     * 어드민 로그인 쿠키 명
     */
    public static final String ADMIN_LOGIN_COOKIE_NAME = "homeplusAdmin";
    /**
     * 어드민 아이디 저장 쿠키명
     */
    public static final String ADMIN_LOGIN_ID_SAVE_COOKIE_NAME = "homeplusAdminExtra";

    /**
     * 어드민 기본 {@link TimeoutConfig}
     * <pre>
     * CONNECTION_REQUEST_TIMEOUT = 5 * 1000;
     * CONNECT_TIMEOUT = 5 * 1000;
     * READ_TIMEOUT = 60 * 1000;
     * </pre>
     */
    public static final TimeoutConfig ADMIN_DEFAULT_TIMEOUT_CONFIG = new TimeoutConfig();
}
