package kr.co.homeplus.admin.web.promotion.model.coupon;

import java.util.List;
import kr.co.homeplus.admin.web.user.model.user.UserSearchDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponBundleIssueSetDto {
    private long couponNo;
    private List<UserSearchDto> issueUserList;
    private String empNo;
}
