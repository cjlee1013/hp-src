package kr.co.homeplus.admin.web.mileage.service;

import java.util.List;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponInfoDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponModifyDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponOneTimeDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponRequestDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponReturnGetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponReturnSetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponUserInfoDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponSearchGetDto;
import kr.co.homeplus.admin.web.mileage.model.coupon.MileageCouponSearchSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileagePaymentSetDto;
import kr.co.homeplus.admin.web.mileage.model.payment.MileageReqDetailDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MileageCouponService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject<List<MileageCouponSearchGetDto>> getMileageCouponManageList(MileageCouponSearchSetDto searchSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            searchSetDto,
            "/office/coupon/mileageCouponManageList",
            new ParameterizedTypeReference<ResponseObject<List<MileageCouponSearchGetDto>>>() {}
        );
    }

    public ResponseObject<List<MileageCouponUserInfoDto>> getMileageCouponUserList(long couponManageNo) throws Exception {
        return resourceClient.getForResponseObject(
            ResourceRouteName.MILEAGE,
            "/office/coupon/mileageCouponUserInfo/" + String.valueOf(couponManageNo),
            new ParameterizedTypeReference<ResponseObject<List<MileageCouponUserInfoDto>>>() {}
        );
    }

    public ResponseObject<MileageCouponInfoDto> getMileageCouponManageInfo(long couponManageNo) throws Exception {
        return resourceClient.getForResponseObject(
            ResourceRouteName.MILEAGE,
            "/office/coupon/mileageManageInfo/" + String.valueOf(couponManageNo),
            new ParameterizedTypeReference<ResponseObject<MileageCouponInfoDto>>() {}
        );
    }

    public ResponseObject<String> setMileageCoupon(MileageCouponRequestDto requestDto) throws Exception {
        requestDto.setRequestId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            requestDto,
            "/office/coupon/requestMileageCouponReg",
            new ParameterizedTypeReference<ResponseObject<String>>(){});
    }

    public ResponseObject<String> modifyMileageCoupon(MileageCouponModifyDto modifyDto) throws Exception {
        modifyDto.setRequestId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            modifyDto,
            "/office/coupon/requestMileageCouponModify",
            new ParameterizedTypeReference<ResponseObject<String>>(){});
    }

    public ResponseObject<MileageCouponReturnGetDto> returnMileageCoupon(MileageCouponReturnSetDto returnSetDto) throws Exception {
        returnSetDto.setRequestId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            returnSetDto,
            "/office/coupon/requestMileageCouponReturn",
            new ParameterizedTypeReference<ResponseObject<MileageCouponReturnGetDto>>(){});
    }

    public ResponseObject<List<MileageCouponOneTimeDto>> getMileageCouponDownList(long couponManageNo) throws Exception {
        return resourceClient.getForResponseObject(
            ResourceRouteName.MILEAGE,
            "/office/coupon/mileageCouponInfo/down/" + String.valueOf(couponManageNo),
            new ParameterizedTypeReference<ResponseObject<List<MileageCouponOneTimeDto>>>() {}
        );
    }

}
