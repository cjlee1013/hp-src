package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.escrow.model.ExtractCommonSetDto;
import kr.co.homeplus.admin.web.statistics.model.storeItem.CombineOrderStatusDto;
import kr.co.homeplus.admin.web.statistics.model.storeItem.CombineOrderStatusSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CombineOrderStatusService {
    private final ResourceClient resourceClient;

    public List<CombineOrderStatusDto> getCombineOrderStatus(CombineOrderStatusSelectDto combineOrderStatusSelectDto) {
        var typeReference = new ParameterizedTypeReference<ResponseObject<List<CombineOrderStatusDto>>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, combineOrderStatusSelectDto, EscrowConstants.STATISTICS_GET_COMBINE_ORDER_STATUS, typeReference).getData();
    }

    public void saveExtractHistory(ExtractCommonSetDto extractCommonSetDto) {
        resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, extractCommonSetDto, EscrowConstants.EXTRACT_SAVE_EXTRACT_HISTORY);
    }
}
