package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > PG 대사 > PG 승인대사 조회 > PG 승인대사 차이내역 DTO")
public class PgCompareApprovalDiffDto {

    @ApiModelProperty(notes = "PG종류(TOSSPG,INICIS)", required = true, position = 1)
    @RealGridColumnInfo(headText = "PG사", width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgKind;

    @ApiModelProperty(notes = "PG상점ID", position = 2)
    @RealGridColumnInfo(headText = "MID", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgMid;

    @ApiModelProperty(notes = "결제일", position = 3)
    @RealGridColumnInfo(headText = "거래일자", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String paymentDt;

    @ApiModelProperty(notes = "주문번호", position = 4)
    @RealGridColumnInfo(headText = "주문번호", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private long purchaseOrderNo;

    @ApiModelProperty(notes = "거래구분", position = 5)
    @RealGridColumnInfo(headText = "거래구분", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String tradeTypeNm;

    @ApiModelProperty(notes = "결제금액(홈플러스)", position = 6)
    @RealGridColumnInfo(headText = "홈플러스 승인액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long paymentAmt;

    @ApiModelProperty(notes = "결제금액(PG)", position = 7)
    @RealGridColumnInfo(headText = "PG 승인액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgPaymentAmt;

    @ApiModelProperty(notes = "차이사유", position = 8)
    @RealGridColumnInfo(headText = "차이구분", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String diffReasonNm;

    @ApiModelProperty(notes = "PG거래ID", position = 9)
    @RealGridColumnInfo(headText = "PG주문번호", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgTid;

    @ApiModelProperty(notes = "PG결과코드", position = 10)
    @RealGridColumnInfo(headText = "PG처리코드", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgResultCd;

    @ApiModelProperty(notes = "결제수단(홈플러스)", position = 11)
    @RealGridColumnInfo(headText = "홈플러스 결제수단", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String paymentMethod;

    @ApiModelProperty(notes = "결제수단(PG)", position = 12)
    @RealGridColumnInfo(headText = "PG 결제수단", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String pgPaymentMethod;

    @ApiModelProperty(notes = "클레임번호", position = 13)
    @RealGridColumnInfo(headText = "그룹클레임번호", sortable = true, width = 100, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private long claimNo;

}
