package kr.co.homeplus.admin.web.item.model.substitute;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class RestrictionItemListSelectDto {

    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 150, order = 1)
    private String itemNm1;

    @RealGridColumnInfo(headText = "대분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 120, order = 2, sortable = true)
    private String lcateNm;

    @RealGridColumnInfo(headText = "중분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 120, order = 3, sortable = true)
    private String mcateNm;

    @RealGridColumnInfo(headText = "소분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 120, order = 4, sortable = true)
    private String scateNm;

    @RealGridColumnInfo(headText = "세분류", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 120, order = 5, sortable = true)
    private String dcateNm;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 6)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 8, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 10, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "useYn", order = 11, hidden = true)
    private String useYn;
}
