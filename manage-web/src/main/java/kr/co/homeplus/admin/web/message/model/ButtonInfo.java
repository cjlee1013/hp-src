package kr.co.homeplus.admin.web.message.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class ButtonInfo {
    /**
     * COMMON REQUIRED
     */
    private String type;
    private String name;

    /**
     * APP LINK
     */
    @Expose
    @SerializedName("scheme_android")
    private String schemeAndroid;
    @Expose
    @SerializedName("scheme_ios")
    private String schemeIos;

    /**
     * WEB LINK
     */
    @Expose
    @SerializedName("url_mobile")
    private String urlMobile;
    @Expose
    @SerializedName("url_pc")
    private String urlPc;

    /**
     * TALK
     */
    @Expose
    @SerializedName("chat_extra")
    private String chatExtra;

    /**
     * BOT EVENT
     */
    @Expose
    @SerializedName("chat_event")
    private String chatEvent;
}
