package kr.co.homeplus.admin.web.core.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(description = "어드민 접근메뉴 권한체크 파라미터")
public class PermitMenuParam {

    @ApiModelProperty("사원번호")
    @NotBlank(message = "사원번호가 없습니다.")
    @Size(min = 10, max = 10)
    private String empId;

    @ApiModelProperty("request URI")
    @NotBlank(message = "request URI가 없습니다.")
    private String requestUri;
}