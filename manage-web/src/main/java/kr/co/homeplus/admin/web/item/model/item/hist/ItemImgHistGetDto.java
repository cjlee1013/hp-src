package kr.co.homeplus.admin.web.item.model.item.hist;

import java.util.Optional;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemImgHistGetDto {
	// 이미지번호
	private String imgNo;
	// 상품번호
	private String itemNo;
	// 이미지타입 (MN:메인, LST:리스트)
	private String imgType;
    // 이미지타입명 (MN:메인, LST:리스트)
    private String imgTypeNm;
	// 우선순위
	private String priority;
	// 이미지명
	private String imgNm;
	// 이미지경로
	private String imgUrl;
	// 대표이미지 여부
	private String mainYn;


    @Override
    public String toString() {
		return Optional.of(
				  this.imgNo
				+ this.imgType
			  	+ this.imgTypeNm
				+ this.priority
				+ this.imgUrl
				+ this.mainYn
		).toString();
    }
}
