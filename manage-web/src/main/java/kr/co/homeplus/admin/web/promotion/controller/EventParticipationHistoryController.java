package kr.co.homeplus.admin.web.promotion.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.promotion.model.EventSelectBoxInfoGet;
import kr.co.homeplus.admin.web.promotion.model.event.ParticipateHistorySet;
import kr.co.homeplus.admin.web.promotion.model.event.ParticipationHistoryGet;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 이벤트 참여이력 관리
 */
@Slf4j
@Controller
@RequestMapping("/event")
@RequiredArgsConstructor
public class EventParticipationHistoryController {

    private final ResourceClient resourceClient;

    //grid
    private final Map<String, Object> participationHistoryGridBaseInfo = RealGridHelper
        .create("participationHistoryGridBaseInfo", ParticipationHistoryGet.class);

    /**
     * 이벤트 참여이력 Main Page
     */
    @GetMapping(value = "/participationHistory")
    public String ParticipationHistoryController(final Model model) {
        List<EventSelectBoxInfoGet> eventSelectBoxInfo = getEventSelectBoxInfo();

        model.addAttribute("eventSelectBoxInfo", eventSelectBoxInfo);
        model.addAllAttributes(participationHistoryGridBaseInfo);
        return "/promotion/participationHistoryMain";
    }

    /**
     * 이벤트 참여이력 조회
     */
    @ResponseBody
    @PostMapping(value = "/participation/getHistory.json")
    public ResponseObject<List<ParticipationHistoryGet>> getHistory(
        @RequestBody @Valid final ParticipateHistorySet historySet) {

        return resourceClient.postForResponseObject(
            ResourceRouteName.EVENT, historySet, "/admin/event/participation/history",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 이벤트 셀렉트박스 정보 조회
     */
    private List<EventSelectBoxInfoGet> getEventSelectBoxInfo() {

        ResponseObject<List<EventSelectBoxInfoGet>> responseObject = resourceClient.getForResponseObject(
            ResourceRouteName.EVENT, "/admin/event/selectBox/info",
            new ParameterizedTypeReference<>() {
            });

        return responseObject.getReturnCode().equals(ResponseObject.RETURN_CODE_SUCCESS)
            ? responseObject.getData() : Collections.singletonList(new EventSelectBoxInfoGet());
    }
}
