package kr.co.homeplus.admin.web.voc.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum SiteType implements RealGridLookUpSupport {

    HOME("home", "홈플러스"),
    CLUB("club", "더클럽");

    private static final Map<String, String> KOR_NAME_MAP =
        Stream.of(values()).collect(Collectors.toMap(SiteType::name, SiteType::getKorSiteName));

    private static final Map<String, String> ENG_NAME_MAP =
        Stream.of(values()).collect(Collectors.toMap(SiteType::name, SiteType::getEngSiteName));

    @Getter
    private final String engSiteName;

    @Getter
    private final String korSiteName;

    SiteType(String engSiteName, String korSiteName) {
        this.engSiteName = engSiteName;
        this.korSiteName = korSiteName;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return KOR_NAME_MAP;
    }
}
