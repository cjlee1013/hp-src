package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetDetailSetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetListGetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetListSetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetSlipTypeGetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetInterfaceGetDto;
import kr.co.homeplus.admin.web.settle.model.balanceSheet.BalanceSheetInterfaceSetDto;
import kr.co.homeplus.admin.web.settle.service.BalanceSheetService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/balanceSheet")
public class BalanceSheetController {

    private final BalanceSheetService balanceSheetService;
    private final SettleCommonService settleCommonService;
    private final LoginCookieService loginCookieService;

    @RequestMapping(value = "/balanceSheet", method = RequestMethod.GET)
    public String balanceSheet(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));

        model.addAllAttributes(RealGridHelper.create("balanceSheetListBaseInfo", BalanceSheetListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("balanceSheetDetailBaseInfo", BalanceSheetDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.create("balanceSheetDetailAllBaseInfo", BalanceSheetDetailGetDto.class));
        return "/settle/balanceSheetList";
    }

    @ResponseBody
    @RequestMapping(value = "/getList.json", method = RequestMethod.GET)
    public List<BalanceSheetListGetDto> getList(BalanceSheetListSetDto balanceSheetListSetDto) throws Exception {
        return balanceSheetService.getList(balanceSheetListSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getDetail.json", method = RequestMethod.GET)
    public List<BalanceSheetDetailGetDto> getDetail(BalanceSheetDetailSetDto balanceSheetDetailSetDto) throws Exception {
        return balanceSheetService.getDetail(balanceSheetDetailSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getSlipType.json", method = RequestMethod.GET)
    public List<BalanceSheetSlipTypeGetDto> getList() throws Exception {
        return balanceSheetService.getSlipTypeList();
    }

    @ResponseBody
    @RequestMapping(value = "/setInterface.json", method = RequestMethod.GET)
    public BalanceSheetInterfaceGetDto setInterface(BalanceSheetInterfaceSetDto balanceSheetInterfaceSetDto) throws Exception {
        balanceSheetInterfaceSetDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return balanceSheetService.setInterface(balanceSheetInterfaceSetDto);
    }
}