package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.substitute.RestrictionItemListParamDto;
import kr.co.homeplus.admin.web.item.model.substitute.RestrictionItemListSelectDto;
import kr.co.homeplus.admin.web.item.model.substitute.RestrictionItemSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/substitute")
public class SubstituteRestrictionController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 상품관리 > 점포상품관리 > 대체불가상품관리
     * @param model
     * @return String
     */
    @RequestMapping(value = "/substituteMain", method = RequestMethod.GET)
    public String categoryAttributeMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                "use_yn"        // 사용여부
                ,"sbst_search_type"  //상품검색타입
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("searchType", code.get("sbst_search_type"));

        model.addAllAttributes(RealGridHelper.create("restrictionListGridBaseInfo", RestrictionItemListSelectDto.class));

        return "/item/substituteMain";
    }

    /**
     * 상품관리 > 점포상품관리 >  대체불가상품 리스트
     * @return RestrictionListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getRestrictionItemList.json", method = RequestMethod.GET)
    public List<RestrictionItemListSelectDto> getRestrictionItemList(RestrictionItemListParamDto listParamDto) throws Exception {

        String apiUri = "/item/substitute/getRestrictionItemList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<RestrictionItemListSelectDto>>>() {};
        return (List<RestrictionItemListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, RestrictionItemListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 >  대체불가상품
     * @return RestrictionListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = "/getRestrictionItem.json", method = RequestMethod.GET)
    public RestrictionItemListSelectDto getRestrictionItem(@RequestParam(value="itemNo") String itemNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("itemNo", itemNo));

        String apiUriRequest = "/item/substitute/getRestrictionItem" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<RestrictionItemListSelectDto>>() {})
            .getData();
    }

     /**
     * 상품관리 > 점포상품관리 >  대체불가상품 등록/수정
     * @param restrictionItemSetParamDto
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setRestrictionItem.json"}, method = RequestMethod.POST)
    public ResponseResult setRestrictionItem(RestrictionItemSetParamDto restrictionItemSetParamDto) throws Exception {

        String apiUri = "/item/substitute/setRestrictionItem";
        restrictionItemSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, restrictionItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();

    }

}
