package kr.co.homeplus.admin.web.user.model.consumer;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.admin.web.user.enums.ConsumerType;
import kr.co.homeplus.admin.web.user.enums.ConsumerReasonType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConsumerDto {
    @ApiModelProperty(value = "일련번호")
    private Long seq;

    @ApiModelProperty(value = "회원번호")
    private Long userNo;

    @ApiModelProperty(value = "회원아이디")
    private String userId;

    @ApiModelProperty(value = "회원명")
    private String userNm;

    @ApiModelProperty(value = "휴대폰 번호")
    private String mobile;

    @ApiModelProperty(value = "권한정지 범위유형(1:구매제한, 2:커뮤니티 이용제한, 3:블랙컨슈머)")
    private ConsumerType consumerType;

    @ApiModelProperty(value = "사유 유형")
    private ConsumerReasonType reasonType;

    @ApiModelProperty(value = "해제 여부")
    private boolean isRelease;

    @ApiModelProperty(value = "해제일자")
    private String releaseDt;

    @ApiModelProperty(value = "사유 상세")
    private String reasonDetail;

    @ApiModelProperty(value = "해제 상세")
    private String releaseDetail;

    @ApiModelProperty(value = "제한 시작일")
    private String startDt;

    @ApiModelProperty(value = "제한 종료일")
    private String endDt;

    @ApiModelProperty(value = "등록일")
    private String regDt;

    @ApiModelProperty(value = "수정일")
    private String modDt;
}
