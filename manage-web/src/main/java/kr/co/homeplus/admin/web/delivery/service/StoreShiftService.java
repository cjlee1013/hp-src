package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.delivery.model.StoreShiftManageModel;
import kr.co.homeplus.admin.web.delivery.model.StoreShiftManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreShiftService {

    private final ResourceClient resourceClient;

    public List<StoreShiftManageModel> getStoreShiftManageList(StoreShiftManageSelectModel storeShiftManageSelectModel) throws Exception {

        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_STORE_SHIFT_MANAGE_LIST, StoreShiftManageSelectModel.class, storeShiftManageSelectModel);
        ResourceClientRequest<List<StoreShiftManageModel>> request = ResourceClientRequest.<List<StoreShiftManageModel>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

}
