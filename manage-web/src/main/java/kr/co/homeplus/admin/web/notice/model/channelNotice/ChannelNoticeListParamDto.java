package kr.co.homeplus.admin.web.notice.model.channelNotice;

import lombok.Data;

@Data
public class ChannelNoticeListParamDto {
    private String schDate;
    private String schStartDate;
    private String schEndDate;
    private String schType;
    private String schValue;
    private String schCnoticeType = "";
    private String schCnoticeKind = "";
    private String schTopYn = "";
    private String schUseYn = "";
}
