package kr.co.homeplus.admin.web.escrow.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchHistorySelectModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchManageSelectModel;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderBatchManageService {
    private final ResourceClient resourceClient;

    /**
     * 주문배치관리 마스터 리스트 조회
     * @param orderBatchManageSelectModel
     * @return
     * @throws Exception
     */
    public List<OrderBatchManageModel> getOrderBatchManageList(OrderBatchManageSelectModel orderBatchManageSelectModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ORDER_BATCH_MANAGE_LIST;
        ResourceClientRequest<List<OrderBatchManageModel>> request = ResourceClientRequest.<List<OrderBatchManageModel>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderBatchManageSelectModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 주문배치관리 마스터 저장
     * @param orderBatchManageModel
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> saveOrderBatchManage(OrderBatchManageModel orderBatchManageModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_SAVE_ORDER_BATCH_MANAGE;
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderBatchManageModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }

    /**
     * 주문배치관리 마스터 삭제
     * @param orderBatchManageModel
     * @return
     * @throws Exception
     */
    public ResponseObject<Object> deleteOrderBatchManage(OrderBatchManageModel orderBatchManageModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_DELETE_ORDER_BATCH_MANAGE;
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderBatchManageModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }

    /**
     * 주문배치 히스토리 리스트 조회
     * @param orderBatchHistorySelectModel
     * @return
     * @throws Exception
     */
    public List<OrderBatchHistoryModel> getOrderBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_ORDER_BATCH_HISTORY_LIST;
        ResourceClientRequest<List<OrderBatchHistoryModel>> request = ResourceClientRequest.<List<OrderBatchHistoryModel>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderBatchHistorySelectModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * LGW 배치이력 목록조회 (프로시저 이력조회)
     * @param orderBatchHistorySelectModel
     * @return
     * @throws Exception
     */
    public List<OrderBatchHistoryModel> getLgwBatchHistoryList(OrderBatchHistorySelectModel orderBatchHistorySelectModel) throws Exception {
        String apiUri = EscrowConstants.ESCROW_GET_LGW_BATCH_HISTORY_LIST;
        ResourceClientRequest<List<OrderBatchHistoryModel>> request = ResourceClientRequest.<List<OrderBatchHistoryModel>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(orderBatchHistorySelectModel)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }
}
