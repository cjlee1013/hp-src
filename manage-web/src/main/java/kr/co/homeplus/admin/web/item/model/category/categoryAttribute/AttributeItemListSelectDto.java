package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;

import lombok.Data;

@Data
//상품속성 리스트
public class AttributeItemListSelectDto {

    //그룹속성번호
    private Long gattrNo;

    //그룹노출속성명
    private String gattrNm;

    //그룹관리속성명
    private String gattrMngNm;

    //멀티선택여부
    private String multiYn;

    //속성번호
    private Long attrNo;

    //속성명
    private String attrNm;

    //사용여부
    private String useYn;

}
