package kr.co.homeplus.admin.web.harm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 위해상품 dto
 */
@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HarmProdDto {
    @RealGridColumnInfo(headText = "유형", width = 150)
    private String nmRpttype;

    @RealGridColumnInfo(headText = "검사기관", width = 150)
    private String cdInsptmachiName;

    @RealGridColumnInfo(headText = "제품명", width = 300)
    private String nmProd;

    @RealGridColumnInfo(headText = "제조업소", width = 250)
    private String nmManufacupso;

    @RealGridColumnInfo(headText = "판매업소", width = 250)
    private String nmSalerupso;

    @RealGridColumnInfo(headText = "등록일시", width = 150)
    private String dhRecv;

    @RealGridColumnInfo(headText = "검사기관코드", hidden = true)
    private String cdInsptmachi;

    @RealGridColumnInfo(headText = "문서번호", hidden = true)
    private String noDocument;

    @RealGridColumnInfo(headText = "차수", hidden = true)
    private String seq;

    @RealGridColumnInfo(headText = "처리상태코드", hidden = true)
    private String statusResultCode;

    @RealGridColumnInfo(hidden = true)
    private String processorName;
    @RealGridColumnInfo(hidden = true)
    private String biznoManufacupso;

    @RealGridColumnInfo(hidden = true)
    private String biznoSalerupso;
    @RealGridColumnInfo(hidden = true)
    private String barcode;
    @RealGridColumnInfo(hidden = true)
    private String dtRecv;
    @RealGridColumnInfo(hidden = true)
    private String tmRecv;

    @RealGridColumnInfo(hidden = true)
    private String statusResult;
    @RealGridColumnInfo(hidden = true)
    private String dtSendresult;
    @RealGridColumnInfo(hidden = true)
    private String tmSendresult;
    @RealGridColumnInfo(hidden = true)
    private String dhSendresult;
}
