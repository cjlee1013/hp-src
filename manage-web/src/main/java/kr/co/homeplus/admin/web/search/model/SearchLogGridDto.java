package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class SearchLogGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class SearchLog{
        @RealGridColumnInfo(headText = "검색어", width = 150, columnType = RealGridColumnType.BASIC, sortable = true)
        private String keyword;
        @RealGridColumnInfo(headText = "쿼리수", width = 80, columnType = RealGridColumnType.NUMBER_CENTER, fieldType = RealGridFieldType.NUMBER, sortable = true)
        private Long count;
    }
}
