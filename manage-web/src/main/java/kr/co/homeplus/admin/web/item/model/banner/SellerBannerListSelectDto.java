package kr.co.homeplus.admin.web.item.model.banner;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SellerBannerListSelectDto {

    @ApiModelProperty(value = "제목")
    @RealGridColumnInfo(headText = "제목", width = 200, sortable = true)
    private String bannerNm;

    @ApiModelProperty(value = "공지사항번호")
    @RealGridColumnInfo(headText = "공지사항번호", width = 0, hidden = true)
    private Long bannerNo;

    @ApiModelProperty(value = "공지내용")
    @RealGridColumnInfo(headText = "공지내용", width = 0, hidden = true)
    private String bannerDesc;

    //구분(ALL:전체,CATE:카테고리별,ITEM:상품별,SELLER:판매자별
    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 0, hidden = true)
    private String dispType;

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분", width = 90, sortable = true)
    private String dispTypeNm;

    @ApiModelProperty(value = "노출여부")
    @RealGridColumnInfo(headText = "노출여부", width = 0, hidden = true)
    private String dispYn;

    @ApiModelProperty(value = "노출여부")
    @RealGridColumnInfo(headText = "노출여부", width = 80, sortable = true)
    private String dispYnNm;

    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeType;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세뷴류", width = 0, hidden = true)
    private String dcateCd;

    @ApiModelProperty(value = "대분류")
    @RealGridColumnInfo(headText = "대분류", width = 120)
    private String lcateNm;

    @ApiModelProperty(value = "중분류")
    @RealGridColumnInfo(headText = "중분류", width = 120)
    private String mcateNm;

    @ApiModelProperty(value = "소분류")
    @RealGridColumnInfo(headText = "소분류", width = 120)
    private String scateNm;

    @ApiModelProperty(value = "세분류")
    @RealGridColumnInfo(headText = "세분류", width = 120)
    private String dcateNm;

    @ApiModelProperty(value = "노출시작일")
    @RealGridColumnInfo(headText = "노출시작일", columnType = RealGridColumnType.DATE, width = 80)
    private String dispStartDt;

    @ApiModelProperty(value = "노출종료일")
    @RealGridColumnInfo(headText = "노출종료일", columnType = RealGridColumnType.DATE, width = 80)
    private String dispEndDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", columnType= RealGridColumnType.DATETIME, width = 100)
    private String regDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일",columnType= RealGridColumnType.DATETIME,  width = 100)
    private String chgDt;

}
