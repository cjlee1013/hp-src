package kr.co.homeplus.admin.web.manage.model.dspPopup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DspPopupListParamDto {
    private String searchPeriodType;
    private String searchStartDt;
    private String searchEndDt;
    private String searchUseYn;
    private String searchType;
    private String searchKeyword;
}
