package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class OnlineStockMngGetDto {
    @RealGridColumnInfo(headText = "점포ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeId;

    @RealGridColumnInfo(headText = "점포명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @RealGridColumnInfo(headText = "FF", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String ff;

    @RealGridColumnInfo(headText = "GR", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String gr;

    @RealGridColumnInfo(headText = "GM", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String gm;

    @RealGridColumnInfo(headText = "CL", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String cl;

    @RealGridColumnInfo(headText = "Electric", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String electric;

    @RealGridColumnInfo(headText = "11번가", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String eleven;

    @RealGridColumnInfo(headText = "N마트", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String naver;
}
