package kr.co.homeplus.admin.web.manage.model.faq;

import lombok.Data;

@Data
public class FaqSetParamDto {

    private Long faqNo;
    private String classCd;
    private String dispArea;
    private String useYn;
    private String dispHomeYn;
    private String dispClubYn;
    private int priority;
    private String faqSubject;
    private String faqBody;
    private String regId;
}
