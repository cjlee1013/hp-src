package kr.co.homeplus.admin.web.message.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;

public enum ShipStatus implements RealGridLookUpSupport {

    NN("대기"),
    D1("결제완료"),
    D2("상품준비중"),
    D3("배송중"),
    D4("배송완료"),
    D5("구매확정");

    @Getter
    private final String status;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values())
        .collect(Collectors.toMap(ShipStatus::name, ShipStatus::getStatus));

    ShipStatus(final String status) {
        this.status = status;
    }

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}