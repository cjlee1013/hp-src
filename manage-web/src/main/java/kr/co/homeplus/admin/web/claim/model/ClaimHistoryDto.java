package kr.co.homeplus.admin.web.claim.model;


import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimHistoryDto {

    @ApiModelProperty(value = "구분")
    @RealGridColumnInfo(headText = "구분")
    private String historyReason;

    @ApiModelProperty(value = "처리일시")
    @RealGridColumnInfo(headText = "처리일시")
    public String regDt;

    @ApiModelProperty(value = "상세내역")
    @RealGridColumnInfo(headText = "상세내역")
    public String historyDetail;

    @ApiModelProperty(value = "처리자")
    @RealGridColumnInfo(headText = "처리자")
    public String regId;
}
