package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.sticker.StickerImgListGetDto;
import kr.co.homeplus.admin.web.manage.model.sticker.StickerListGetDto;
import kr.co.homeplus.admin.web.manage.model.sticker.StickerParamDto;
import kr.co.homeplus.admin.web.manage.model.sticker.StickerSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/manage/sticker")
@Slf4j
public class StickerController {

    private final CodeService codeService;
    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    public StickerController(ResourceClient resourceClient,
        LoginCookieService cookieService,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.cookieService = cookieService;
        this.codeService = codeService;

    }

    @RequestMapping(value = "/stickerMain", method = RequestMethod.GET)
    public String stickerMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"    // 사용여부
        );
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAllAttributes(RealGridHelper.create("stickerListGridBaseInfo", StickerListGetDto.class));
        return "/manage/stickerMain";
    }

    @ResponseBody
    @RequestMapping(value = {"/getStickerList.json"}, method = RequestMethod.GET)
    public List<StickerListGetDto> getStickerList(StickerParamDto stickerParamDto) throws Exception {
        String apiUri = "/manage/sticker/getStickerList";

        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<StickerListGetDto>>>() {};
        return (List<StickerListGetDto>) resourceClient
            .getForResponseObject(ResourceRouteName.MANAGE, StringUtil
                    .getRequestString(apiUri, StickerParamDto.class, stickerParamDto),
                typeReference).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/getStickerImgList.json"}, method = RequestMethod.GET)
    public List<StickerImgListGetDto> getStickerImgList(@RequestParam(value="stickerNo") String stickerNo) throws Exception {
        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , "/manage/sticker/getStickerImgList?stickerNo=" + stickerNo
            , new ParameterizedTypeReference<ResponseObject<List<StickerImgListGetDto>>>(){}).getData();
    }

    @ResponseBody
    @RequestMapping(value = {"/setSticker.json"}, method = RequestMethod.POST)
    public ResponseResult setSticker(@RequestBody StickerSetDto stickerSetDto) throws Exception {
        String apiUri = "/manage/sticker/setSticker";
        stickerSetDto.setUserId(cookieService.getUserInfo().getEmpId());

        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, stickerSetDto,
            apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}).getData();

    }

    @ResponseBody
    @RequestMapping(value = {"/getStickerForSelectBox.json"}, method = RequestMethod.GET)
    List<Map<String, Object>> getStickerForSelectBox() {

        String apiUri = "/manage/sticker/getStickerForSelectBox";
        List<Map<String, Object>> data = (List<Map<String, Object>>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE
            , apiUri, new ParameterizedTypeReference<ResponseObject<List<Map<String, Object>>>>() {
        }).getData();

        return data;
    }


}