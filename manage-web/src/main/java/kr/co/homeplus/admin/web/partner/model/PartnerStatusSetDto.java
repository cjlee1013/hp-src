package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 업체관리 > 파트너관리 > 판매업체관리 > 판매업체 상태 Set Entry
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerStatusSetDto {

	private String partnerId;
	private String partnerStatus;
	private String regReason;
	private String userId;
}
