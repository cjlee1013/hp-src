package kr.co.homeplus.admin.web.delivery.controller;

import static kr.co.homeplus.admin.web.delivery.model.StoreType.AURORA;
import static kr.co.homeplus.admin.web.delivery.model.StoreType.CLUB;
import static kr.co.homeplus.admin.web.delivery.model.StoreType.HYPER;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.delivery.model.CloseDayType;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDayImage;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDayModel;
import kr.co.homeplus.admin.web.delivery.model.StoreCloseDaySelectModel;
import kr.co.homeplus.admin.web.delivery.service.StoreCloseDayService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 배송관리 > 점포배송정보 > 점포별 휴일 조회
 */
@Controller
@RequestMapping("/escrow")
public class StoreCloseDayController {

    private final AuthorityService authorityService;
    @Autowired
    private CodeService codeService;
    @Autowired
    private StoreCloseDayService storeCloseDayService;
    @Autowired
    private LoginCookieService loginCookieService;
    @Value("${plus.resource-routes.image.url}")
    private String apiImgUrl;

    public StoreCloseDayController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    /**
     * 점포별 휴일 조회 Main Page
     */
    @GetMapping("/storeDelivery/storeCloseDayMain")
    public String storeCloseDayMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_TRANSMNG_COMMON)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // 점포유형 리스트 가져와서 model 에 담기
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"
        );
        model.addAttribute("storeType", code.get("store_type"));
        // 휴무유형 리스트 가져와서 model 에 담기
        model.addAttribute("closeDayTypeList", Arrays.asList(CloseDayType.values()));
        model.addAllAttributes(RealGridHelper.create("storeCloseDayGridBaseInfo", StoreCloseDayModel.class));
        return "/delivery/storeCloseDayMain";
    }

    /**
     * 점포별 휴일 리스트 조회
     * @param storeCloseDaySelectModel
     * @return
     */
    @ResponseBody
    @GetMapping("/storeDelivery/getStoreCloseDayList.json")
    public List<StoreCloseDayModel> getStoreCloseDayList(@ModelAttribute StoreCloseDaySelectModel storeCloseDaySelectModel) {
        return storeCloseDayService.getStoreCloseDayList(storeCloseDaySelectModel);
    }

    /**
     * 휴일 이미지 변경 팝업창
     */
    @GetMapping("/popup/closeDayImagePop")
    public String closeDayImagePop(Model model) throws Exception {
        ObjectMapper om = new ObjectMapper();
        List<StoreCloseDayImage> closeDayImageList = storeCloseDayService.getCloseDayImageList();
        List<StoreCloseDayImage> closeDayImageListByHyper = closeDayImageList.stream().filter(c -> HYPER.getCode().equals(c.getStoreType())).collect(Collectors.toList());
        List<StoreCloseDayImage> closeDayImageListByAurora = closeDayImageList.stream().filter(c -> AURORA.getCode().equals(c.getStoreType())).collect(Collectors.toList());
        List<StoreCloseDayImage> closeDayImageListByClub = closeDayImageList.stream().filter(c -> CLUB.getCode().equals(c.getStoreType())).collect(Collectors.toList());
        model.addAttribute("closeDayImageListByHyperJson", om.writeValueAsString(closeDayImageListByHyper));
        model.addAttribute("closeDayImageListByAuroraJson", om.writeValueAsString(closeDayImageListByAurora));
        model.addAttribute("closeDayImageListByClubJson", om.writeValueAsString(closeDayImageListByClub));
        model.addAttribute("apiImgUrl", this.apiImgUrl);
        return "/delivery/pop/closeDayImagePop";
    }

    /**
     * 휴일 이미지 저장
     * @param storeCloseDayImage
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/storeDelivery/saveCloseDayImageList.json")
    public ResponseObject<Object> saveCloseDayImageList(@RequestBody StoreCloseDayImage storeCloseDayImage) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        storeCloseDayImage.setChgId(userCd);
        return storeCloseDayService.saveCloseDayImageList(storeCloseDayImage);
    }
}
