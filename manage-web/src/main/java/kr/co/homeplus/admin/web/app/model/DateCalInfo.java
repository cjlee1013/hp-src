package kr.co.homeplus.admin.web.app.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DateCalInfo {
    private String optionValue;
    private String optionStr;
}
