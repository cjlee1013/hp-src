package kr.co.homeplus.admin.web.manage.model.expBanner;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpBannerSetParamDto {

    private Long bannerNo;
    private String siteType;
    private String device;
    private String loc;
    private String dispYn;
    private int priority = 0;
    private String dispStartDt;
    private String dispEndDt;
    private String bannerNm;
    private String template;
    private String templateDetail;
    private String regId;

    private List<ExpBannerLinkSetParamDto> linkList;

    private List<ExpBannerStoreSetParamDto> storeList;

}
