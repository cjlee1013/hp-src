package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerBizCateCdSetDto implements Serializable {

	private String partnerId;
	private String userId;
	private String categoryCd;
	private String useYn;
}
