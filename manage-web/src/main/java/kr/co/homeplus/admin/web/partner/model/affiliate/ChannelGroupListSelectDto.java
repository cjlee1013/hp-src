package kr.co.homeplus.admin.web.partner.model.affiliate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ChannelGroupListSelectDto {

    private String channelId;
    private String salesCd;
    private String useYn;
    private String regNm;
    private String regDt;
    private String chgNm;
    private String chgDt;

}
