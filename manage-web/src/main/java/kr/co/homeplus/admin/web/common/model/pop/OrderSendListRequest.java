package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Data;

/**
 * 수동발송 > 주문 검색 팝업 검색 조건
 */
@Data
public class OrderSendListRequest {

    private String orderType;

    private String orderDt;

    private String storeId;

    private String storeNm;

    private boolean isMember;

    private String userNo;

    private String buyerNm;

    private String shiftId;

    private String slotId;

    private String driverNm;
}
