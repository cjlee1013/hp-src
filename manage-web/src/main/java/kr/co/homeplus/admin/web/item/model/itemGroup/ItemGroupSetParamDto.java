package kr.co.homeplus.admin.web.item.model.itemGroup;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemGroupSetParamDto {

    //묶음상품관리번호
    private Long groupNo;

    //묶음상품명
    private String groupItemNm;

    //사용여부
    private String useYn;

    //옵션단계
    private String depth;

    //1단계타이틀
    private String opt1Title;

    //2단계타이틀
    private String opt2Title;

    //등록/수정자
    private String userId;

    List<ItemGroupDetailListGetDto> itemGroupDetailList;

}
