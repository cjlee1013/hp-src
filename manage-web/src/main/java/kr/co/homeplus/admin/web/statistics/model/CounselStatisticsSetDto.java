package kr.co.homeplus.admin.web.statistics.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CounselStatisticsSetDto {
    @ApiModelProperty(notes = "시작일")
    @JsonProperty("SCH_S_YMD")
    private String SCH_S_YMD;

    @ApiModelProperty(notes = "종료일")
    @JsonProperty("SCH_E_YMD")
    private String SCH_E_YMD;

    @ApiModelProperty(notes = "점포유형")
    @JsonProperty("H_CHANNEL")
    private String H_CHANNEL;

    @ApiModelProperty(notes = "판매자")
    @JsonProperty("STORE_ID")
    private String STORE_ID;

    @ApiModelProperty(notes = "상담구분")
    @JsonProperty("ADVISOR_TYPE_1")
    private String ADVISOR_TYPE_1;

    @ApiModelProperty(notes = "채널구분")
    @JsonProperty("CHANNEL_TYPE")
    private String CHANNEL_TYPE;
}
