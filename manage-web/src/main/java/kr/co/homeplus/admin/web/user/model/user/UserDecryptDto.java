package kr.co.homeplus.admin.web.user.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDecryptDto {
    @ApiModelProperty(value = "MHC 대체카드번호 - 마스킹처리")
    private String mhcCardno;

    @ApiModelProperty(value = "계좌번호 - 마스킹 처리")
    private String bankAccount;
}
