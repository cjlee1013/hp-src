package kr.co.homeplus.admin.web.core.service;

/**
 * 권한체크 서비스
 */
public interface AuthorityService {
    /**
     * 권한 역할체크
     * 사번, 역할코드를 인자로 넘기면, auth-api를 통해 권한허가 여부를 체크합니다.
     * @param roleCd 역할 코드
     * @return {boolean}
     */
    boolean hasRole(String roleCd);

    /**
     * 권한 역할체크
     * 사번, 역할코드를 인자로 넘기면, auth-api를 통해 권한허가 여부를 체크합니다.
     * @param empId 사번
     * @param roleCd 역할 코드
     * @return {boolean}
     */
    boolean hasRole(String empId, String roleCd);

    /**
     * 권한 역할-정책체크
     * 사번, 역할, 정책코드를 인자로 넘기면, auth-api를 통해 역할-정책(policy)허가 여부를 체크합니다.
     * @param empId 사번
     * @param roleCd 역할 코드
     * @param policyCd 정책 코드
     * @return {boolean}
     */
    boolean hasRolePolicy(String empId, String roleCd, String policyCd);

    /**
     * 권한 역할-정책체크
     * 역할, 정책코드를 인자로 넘기면, auth-api를 통해 역할 정책(policy)허가 여부를 체크합니다.
     * @param roleCd 역할 코드
     * @param policyCd 정책 코드
     * @return {boolean}
     */
    boolean hasRolePolicy(String roleCd, String policyCd);

    /**
     * 권한 역할이 없을 경우를 체크합니다.
     * 사번, 역할코드를 인자로 넘기면, auth-api를 통해 권한허가 여부를 체크합니다.
     * @param roleCd 역할 코드
     * @return {boolean}
     */
    boolean hasNotRole(String roleCd);
}
