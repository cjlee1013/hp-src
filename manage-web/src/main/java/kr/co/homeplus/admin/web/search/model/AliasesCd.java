package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum AliasesCd {
    HYPER("hyper-search"),
    DS("ds-search");

    private String aliasesNm;

    public static AliasesCd getAliases(String aliasesNm) {
        for(AliasesCd type : AliasesCd.values()) {
            if(type.getAliasesNm().equals(aliasesNm)) {
                return type;
            }
        }
        return null;
    }
}
