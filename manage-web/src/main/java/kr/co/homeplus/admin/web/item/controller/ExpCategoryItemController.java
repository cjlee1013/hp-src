package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.expCategory.ExpCategorySetParamDto;
import kr.co.homeplus.admin.web.item.model.expCategoryItem.ExpCategoryItemGetDto;
import kr.co.homeplus.admin.web.item.model.expCategoryItem.ExpCategoryItemListParamDto;
import kr.co.homeplus.admin.web.item.model.expCategoryItem.ExpCategoryItemSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item/expCategoryItem")
public class ExpCategoryItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 사이트관리 > 전문관관리 > 익스프레스 샵 카테고리 상품 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/expCategoryItemMain", method = RequestMethod.GET)
    public String expCategoryItemMain(Model model) {

        codeService.getCodeModel(model
            , "exp_status"			// 매핑여부
        );

        model.addAllAttributes(RealGridHelper.create("expCategoryItemGridBaseInfo", ExpCategoryItemGetDto.class));

        return "/item/expCategoryItemMain";
    }

    /**
     * 사이트관리 > 전문관관리 > 익스프레스 샵 카테고리 상품 리스트
     *
     * @param expCategoryItemListParamDto
     * @return List<ExpCategoryItemGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getExpCategoryItem.json"}, method = RequestMethod.GET)
    public List<ExpCategoryItemGetDto> getExpCategoryItem(@Valid ExpCategoryItemListParamDto expCategoryItemListParamDto) {
        String apiUri = "/item/expCategoryItem/getExpCategoryItem";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ExpCategoryItemGetDto>>>() {};
        return (List<ExpCategoryItemGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, ExpCategoryItemListParamDto.class, expCategoryItemListParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 전문관관리 > 새벽배송 상품 등록/수정
     *
     * @param expCategoryItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/setExpCategoryItem.json"})
    public ResponseResult setExpCategoryItem(@Valid @RequestBody ExpCategoryItemSetParamDto expCategoryItemSetParamDto) {
        String apiUri = "/item/expCategoryItem/setExpCategoryItem";
        expCategoryItemSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, expCategoryItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}
