package kr.co.homeplus.admin.web.mileage.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageAmtSumGetDto;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageAmtSumSetDto;
import kr.co.homeplus.admin.web.mileage.model.statistic.DailyMileageTypeSumGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MileageStatisticService {

    private final ResourceClient resourceClient;

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 통계 > 일/월별 마일리지 잔액 조회
     */
    public List<DailyMileageAmtSumGetDto> getDailyMileageSummary(DailyMileageAmtSumSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/stat/getDailyMileageSummary",
            new ParameterizedTypeReference<ResponseObject<List<DailyMileageAmtSumGetDto>>>(){}).getData();
    }

    /**
     * 결제관리 > 마일리지 관리 > 마일리지 지급/회수 관리 > 마일리지 상세내역 조회
     */
    public List<DailyMileageTypeSumGetDto> getDailyMileageTypeSummary(DailyMileageAmtSumSetDto listParamDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.MILEAGE,
            listParamDto,
            "/office/stat/getDailyMileageTypeSummary",
            new ParameterizedTypeReference<ResponseObject<List<DailyMileageTypeSumGetDto>>>(){}).getData();
    }
}
