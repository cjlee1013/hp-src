package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelatedKeywordSetParamDto {
    private Long relationId;
    private String siteType;
    private String keyword;
    private String relationKeyword;
    private String useYn;
    private String relationType;
    private String operationPeriodYn;
    private String startDt;
    private String endDt;
    private String creator;
}
