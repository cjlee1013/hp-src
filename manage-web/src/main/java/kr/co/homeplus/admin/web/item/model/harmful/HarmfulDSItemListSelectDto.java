package kr.co.homeplus.admin.web.item.model.harmful;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class HarmfulDSItemListSelectDto {

    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 150, order = 1)
    private String itemNm1;

    @RealGridColumnInfo(headText = "판매업체ID", order = 2)
    private String partnerId;

    @RealGridColumnInfo(headText = "판매업체명", width = 120, order = 3)
    private String partnerNm;

    @RealGridColumnInfo(headText = "상품상태", width = 120, order = 4, sortable = true)
    private String itemStatus;

    @RealGridColumnInfo(headText = "판매시작일", width = 120, order = 5, sortable = true)
    private String saleStartDt;

    @RealGridColumnInfo(headText = "판매종료일", width = 120, order = 6, sortable = true)
    private String saleEndDt;

}
