package kr.co.homeplus.admin.web.voc.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterAddSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterCompleteImgSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterCompleteSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterExcelListGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterExcelListSetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterListGetDto;
import kr.co.homeplus.admin.web.voc.model.callCenter.CallCenterListSetDto;
import kr.co.homeplus.admin.web.voc.service.CallCenterService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.PrivacyMaskingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 회원관리 > 고객문의관리 > 콜센터문의관리
 */
@Controller
@RequestMapping("/voc")
public class CallCenterController {
    @Autowired
    private CallCenterService callCenterService;
    @Autowired
    private ShipCommonService shipCommonService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    public CallCenterController() {
    }

    /**
     * 콜센터문의관리 Main Page
     */
    @GetMapping("/callCenter/callCenterMain")
    public String getCallCenterMain(Model model
        , @RequestParam(value = "userNo", required = false, defaultValue = "") String userNo
        , @RequestParam(value = "userNm", required = false, defaultValue = "") String userNm) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("cs_advisor_ing_type", shipCommonService.getCode("cs_advisor_ing_type"));
        model.addAttribute("cs_advisor_type_1", shipCommonService.getCode("cs_advisor_type_1"));
        model.addAttribute("cs_advisor_type_2", shipCommonService.getCode("cs_advisor_type_2"));
        model.addAttribute("cs_channel_cd", shipCommonService.getCode("cs_channel_cd"));
        model.addAttribute("cs_h_branch_cd", shipCommonService.getCode("cs_h_branch_cd"));
        model.addAttribute("cs_h_channel", shipCommonService.getCode("cs_h_channel"));
        model.addAttribute("cs_voc_yn", shipCommonService.getCode("cs_voc_yn"));
        model.addAttribute("userNo", userNo);
        model.addAttribute("userNm", PrivacyMaskingUtils.maskingUserName(userNm));

        model.addAllAttributes(RealGridHelper.create("callCenterGridBaseInfo", CallCenterListGetDto.class));
        model.addAllAttributes(RealGridHelper.create("callCenterExcelGridBaseInfo", CallCenterExcelListGetDto.class));

        return "/voc/callCenterMain";
    }

    /**
     * 콜센터문의관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/callCenter/getCallCenterList.json", method = RequestMethod.GET)
    public List<CallCenterListGetDto> getCallCenterList(CallCenterListSetDto callCenterListSetDto) {
        return callCenterService.getCallCenterList(callCenterListSetDto);
    }

    /**
     * 콜센터문의관리 엑셀 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/callCenter/getCallCenterExcelList.json", method = RequestMethod.GET)
    public List<CallCenterExcelListGetDto> getCallCenterExcelList(CallCenterExcelListSetDto callCenterExcelListSetDto) {
        return callCenterService.getCallCenterExcelList(callCenterExcelListSetDto);
    }

    /**
     * 콜센터문의관리 상세 팝업
     */
    @GetMapping("/popup/getCallCenterDetailPop")
    public String getCallCenterDetailPop(Model model
        , @RequestParam(value="callback") String callBackScript
        , @RequestParam(value="callbackLink") String callbackLinkScript
        , @RequestParam(value="master_seq") String master_seq
        , @RequestParam(value="channel_type") String channel_type) throws Exception {
        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("homeImgUrl", homeImgUrl);
        model.addAttribute("master_seq", XssPreventer.escape(master_seq));
        model.addAttribute("channel_type", XssPreventer.escape(channel_type));
        model.addAttribute("callBackScript",callBackScript);
        model.addAttribute("callbackLinkScript",callbackLinkScript);

        model.addAttribute("cs_h_branch_cd", shipCommonService.getCode("cs_h_branch_cd"));
        model.addAttribute("cs_advisor_ing_type", shipCommonService.getCode("cs_advisor_ing_type"));
        model.addAttribute("cs_complet_reason", shipCommonService.getCode("cs_complet_reason"));
        model.addAttribute("cs_reason_detail", shipCommonService.getCode("cs_reason_detail"));

        return "/voc/pop/callCenterDetailPop";
    }

    /**
     * 콜센터문의관리 상세 조회
     */
    @ResponseBody
    @PostMapping("/callCenter/getCallCenterDetail.json")
    public List<CallCenterDetailGetDto> getCallCenterDetail(final HttpServletRequest request, @RequestBody CallCenterDetailSetDto callCenterDetailSetDto) {
        return callCenterService.getCallCenterDetail(request, callCenterDetailSetDto);
    }

    /**
     * 콜센터문의관리 문의 추가
     */
    @ResponseBody
    @PostMapping(value = "/callCenter/setAddCounsel.json")
    public ResponseObject<Object> setAddCounsel(@RequestBody CallCenterAddSetDto callCenterAddSetDto) throws Exception {
        return callCenterService.setAddCounsel(callCenterAddSetDto);
    }

    /**
     * 콜센터문의관리 처리 완료
     */
    @ResponseBody
    @PostMapping(value = "/callCenter/setAddComplete.json")
    public ResponseObject<Object> setAddComplete(@RequestBody CallCenterCompleteSetDto callCenterCompleteSetDto) throws Exception {
        return callCenterService.setAddComplete(callCenterCompleteSetDto);
    }

    /**
     * 콜센터문의관리 파일정보 저장
     */
    @ResponseBody
    @PostMapping(value = "/callCenter/setAddCompleteImg.json")
    public ResponseObject<Object> setAddCompleteImg(@RequestBody CallCenterCompleteImgSetDto callCenterCompleteImgSetDto) throws Exception {
        return callCenterService.setAddCompleteImg(callCenterCompleteImgSetDto);
    }

}
