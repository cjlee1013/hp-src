package kr.co.homeplus.admin.web.core.exception;

import kr.co.homeplus.admin.web.core.dto.LoginParam;

public class AdminLoginLockedException extends RuntimeException {

    private LoginParam loginParam;

    public AdminLoginLockedException(String message) {
        super(message);
    }

    public AdminLoginLockedException(LoginParam loginParam, String message) {
        super(message);
        this.loginParam = loginParam;
    }

    public LoginParam getLoginParam() {
        return loginParam;
    }
}
