package kr.co.homeplus.admin.web.order.model.orderPopup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "쿠폰할인정보 DTO")
public class OrderCouponDiscountDto {
    @ApiModelProperty(value = "점포", position = 1)
    @RealGridColumnInfo(headText = "점포", width = 120)
    private String storeNm;
    @ApiModelProperty(value = "할인번호", position = 2)
    @RealGridColumnInfo(headText = "할인번호", width = 120)
    private String couponNo;
    @ApiModelProperty(value = "종류코드", position = 3, hidden = true)
    @RealGridColumnInfo(headText = "종류코드", hidden = true)
    private String discountKind;
    @ApiModelProperty(value = "종류명", position = 4)
    @RealGridColumnInfo(headText = "종류", width = 120)
    private String discountKindNm;
    @ApiModelProperty(value = "쿠폰명", position = 5)
    @RealGridColumnInfo(headText = "쿠폰명", width = 150)
    private String couponNm;
    @ApiModelProperty(value = "쿠폰정보", position = 6)
    @RealGridColumnInfo(headText = "쿠폰정보", width = 200)
    private String couponInfo;
    @ApiModelProperty(value = "할인적용대상", position = 7)
    @RealGridColumnInfo(headText = "할인적용대상", width = 170)
    private String discountApplyTarget;
    @ApiModelProperty(value = "할인금액", position = 8)
    @RealGridColumnInfo(headText = "할인금액", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.PRICE, width = 120)
    private long discountAmt;
    @ApiModelProperty(value = "사용가능기간", position = 9)
    @RealGridColumnInfo(headText = "사용가능기간", width = 250)
    private String discountCouponPeriod;
}
