package kr.co.homeplus.admin.web.claim.model.outOfStock;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class OutOfStockItemListGetDto {

    @ApiModelProperty(value = "점포명", position = 1)
    @RealGridColumnInfo(headText = "점포명", width = 80)
    public String storeNm;

    @ApiModelProperty(value = "주문번호", position = 2)
    @RealGridColumnInfo(headText = "주문번호")
    public String purchaseOrderNo;

    @ApiModelProperty(value = "대체여부", position = 3)
    @RealGridColumnInfo(headText = "대체여부", width = 80)
    public String substitutionYn;

    @ApiModelProperty(value = "대체상태", position = 4)
    @RealGridColumnInfo(headText = "대체상태", width = 80)
    public String subStatus;

    @ApiModelProperty(value = "취소상태", position = 5)
    @RealGridColumnInfo(headText = "취소상태", width = 80)
    public String refundStatus;

    @ApiModelProperty(value = "배송일", position = 6)
    @RealGridColumnInfo(headText = "배송일", width = 120)
    public String shipDt;

    @ApiModelProperty(value = "배송shift", position = 7)
    @RealGridColumnInfo(headText = "배송shift", width = 80)
    public String shiftId;

    @ApiModelProperty(value = "단축번호", position = 8)
    @RealGridColumnInfo(headText = "단축번호", width = 80)
    public String sordNo;

    @ApiModelProperty(value = "주문상품번호", position = 8)
    @RealGridColumnInfo(headText = "주문상품번호", width = 80)
    public String itemNo;

    @ApiModelProperty(value = "주문일", position = 9)
    @RealGridColumnInfo(headText = "주문일", width = 120)
    public String orderDt;

    @ApiModelProperty(value = "주문상품명", position = 10)
    @RealGridColumnInfo(headText = "주문상품명", width = 240, columnType = RealGridColumnType.NONE)
    public String itemNm;

    @ApiModelProperty(value = "주문수량", position = 11)
    @RealGridColumnInfo(headText = "주문수량", width = 80)
    public String itemQty;

    @ApiModelProperty(value = "결품수량", position = 12)
    @RealGridColumnInfo(headText = "결품수량", width = 80)
    public String oosQty;

    @ApiModelProperty(value = "상품금액", position = 13)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "상품금액", width = 80, columnType = RealGridColumnType.PRICE)
    public String itemPrice;

    @ApiModelProperty(value = "대체주문번호", position = 14)
    @RealGridColumnInfo(headText = "대체주문번호")
    public String subPurchaseOrderNo;

    @ApiModelProperty(value = "대체상품번호", position = 15)
    @RealGridColumnInfo(headText = "대체상품번호", width = 80)
    public String subItemNo;

    @ApiModelProperty(value = "대체상품명", position = 16)
    @RealGridColumnInfo(headText = "대체상품명", width = 240, columnType = RealGridColumnType.NONE)
    public String subItemNm;

    @ApiModelProperty(value = "대체수량", position = 17)
    @RealGridColumnInfo(headText = "대체수량", width = 80)
    public String subItemQty;

    @ApiModelProperty(value = "대체상품금액", position = 18)
    @RealGridColumnInfo(fieldType = RealGridFieldType.NUMBER, headText = "대체상품금액", width = 80, columnType = RealGridColumnType.PRICE)
    private String subItemPrice;

    @ApiModelProperty(value = "실패사유(사유코드)", position = 19)
    @RealGridColumnInfo(headText = "실패사유(사유코드)", width = 240)
    private String returnMessage;
}
