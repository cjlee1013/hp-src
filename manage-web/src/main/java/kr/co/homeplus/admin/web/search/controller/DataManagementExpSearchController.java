package kr.co.homeplus.admin.web.search.controller;


import java.util.Map;
import kr.co.homeplus.admin.web.search.model.DataManagementExpSearchGridDto;
import kr.co.homeplus.admin.web.search.model.DataManagementExpSearchListGetParam;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchItemGetDto;
import kr.co.homeplus.admin.web.search.model.DataManagementTotalSearchItemGetParam;
import kr.co.homeplus.admin.web.search.service.DataManagementExpSearchService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/search/dataManagement/expSearch")
@RequiredArgsConstructor
public class DataManagementExpSearchController {

    @Value("${spring.profiles.active}")
    private String profilesActive;

    private final DataManagementExpSearchService expSearchService;

    @GetMapping(value = { "/main" })
    public String getMain(Model model) {
        model.addAttribute("profilesActive",profilesActive);
        model.addAllAttributes(RealGridHelper.create("expsearchGridBaseInfo", DataManagementExpSearchGridDto.ExpSearchList.class));
        return "/search/dataManagementExpSearchMain";
    }

    @ResponseBody
    @GetMapping(value = {"/list.json"})
    public ResponseObject<Map<String,Object>> getList(DataManagementExpSearchListGetParam param) {
        return expSearchService.getList(param);
    }

    @ResponseBody
    @GetMapping(value = {"/item.json"})
    public DataManagementTotalSearchItemGetDto getItem(DataManagementTotalSearchItemGetParam param) {
        return expSearchService.getItem(param).getData();
    }

    @ResponseBody
    @GetMapping(value = {"/filter.json"})
    public ResponseObject<Map<String,Object>> getFilter(DataManagementExpSearchListGetParam param){
        return expSearchService.getFilter(param);
    }
}
