package kr.co.homeplus.admin.web.user.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.constants.RoleConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.privacylog.model.PersonalLogMethod;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyHeaderKey;
import kr.co.homeplus.admin.web.core.privacylog.model.PrivacyLogInfo;
import kr.co.homeplus.admin.web.core.privacylog.service.PrivacyLogService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.user.model.UserGradeDefinitionSelectBox;
import kr.co.homeplus.admin.web.user.model.user.UserGradeInfo;
import kr.co.homeplus.admin.web.user.model.user.CallCenterInquiryGet;
import kr.co.homeplus.admin.web.user.model.user.CallCenterInquiryParam;
import kr.co.homeplus.admin.web.user.model.user.ShippingAddressDto;
import kr.co.homeplus.admin.web.user.model.user.UserDecryptDto;
import kr.co.homeplus.admin.web.user.model.user.UserDecryptParam;
import kr.co.homeplus.admin.web.user.model.user.UserGradeHistoryDto;
import kr.co.homeplus.admin.web.user.model.user.UserGradeInfoParam;
import kr.co.homeplus.admin.web.user.model.user.UserGradeUpdateParam;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDetailAgreeInfo;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDetailDto;
import kr.co.homeplus.admin.web.user.model.user.UserInfoDto;
import kr.co.homeplus.admin.web.user.model.user.UserInfoMhcParam;
import kr.co.homeplus.admin.web.user.model.user.UserInfoParam;
import kr.co.homeplus.admin.web.user.model.user.UserLoginLogDto;
import kr.co.homeplus.admin.web.user.model.user.UserMhcPointDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * 회원관리 메뉴
 */
@Slf4j
@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserInfoMainController {

    private final ResourceClient resourceClient;
    private final LoginCookieService cookieService;
    private final PrivacyLogService privacyLogService;
    private final AuthorityService authorityService;

    @Value("${spring.profiles.active}")
    private String profilesActive;

    /** 운영환경 profile**/
    private static final String ACTIVE_PROFILES_PRD = "prd";

    private final Map<String, Object> userMainGridBaseInfo = RealGridHelper
        .create("userMainGridBaseInfo", UserInfoDto.class);
    //등급내역 그리드
    private final Map<String, Object> userGradeHistoryGridBaseInfo = RealGridHelper
        .create("userGradeHistoryGridBaseInfo", UserGradeHistoryDto.class);

    //로그인로그 그리드
    private final Map<String, Object> userLoginLogGridBaseInfo = RealGridHelper
        .create("userLoginLogGridBaseInfo", UserLoginLogDto.class);

    //배송지 목록 그리드
    private final Map<String, Object> shippingAddressGridBaseInfo = RealGridHelper
        .create("shippingAddressGridBaseInfo", ShippingAddressDto.class);

    //등급변경 팝업 그리드
    private final Map<String, Object> userGradeUpdateGridBaseInfo = RealGridHelper
        .create("userGradeUpdateGridBaseInfo", CallCenterInquiryGet.class);

    /**
     * 회원관리 Main Page
     */
    @GetMapping(value = "/userInfo/main")
    public String searchUserMainController(final Model model) {
        model.addAllAttributes(userMainGridBaseInfo);
        return "/user/userInfoMain";
    }

    /**
     * 회원관리 목록 조회(검색)
     */
    @ResponseBody
    @GetMapping(value = "/userInfo/getUserInfoList.json")
    public List<UserInfoDto> searchUserList(final HttpServletRequest request,
        @ModelAttribute final UserInfoParam param) {
        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logInfo = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원정보관리 목록조회")
            .processor(param.toString())
            .processorTaskSql("")
            .build();

        privacyLogService.send(logInfo);

        if (isPrd() && authorityService.hasNotRole(RoleConstants.MEMBER_COMMON)) {
            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
        }

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        return resourceClient.postForResponseObject(
            ResourceRouteName.USERMNG,
            param,
            "/admin/userInfo/getUserInfoList",
            headers,
            new ParameterizedTypeReference<ResponseObject<List<UserInfoDto>>>() {
            }).getData();
    }

    /**
     * profile이 prd인지 체크합니다.
     * @return prd일 경우 true, 아닐경우 false를 리턴합니다.
     */
    private boolean isPrd() {
        return profilesActive.equalsIgnoreCase(ACTIVE_PROFILES_PRD);
    }

    /**
     * 회원관리 상세 페이지
     * @param userNo 회원번혼
     * @return 회원관리 상세 페이지
     */
    @GetMapping(value = "/userInfo/detail")
    public String searchUserDetailController(final Model model,
        @RequestParam final Long userNo) {
        model.addAllAttributes(shippingAddressGridBaseInfo);
        //userNo
        model.addAttribute("userNo", userNo);
        model.addAttribute("MEMBER_GRADE",
            authorityService.hasRole(RoleConstants.MEMBER_GRADE));
        return "/user/userInfoDetail";
    }

    /**
     * 회원관리 상세 조회
     */
    @ResponseBody
    @GetMapping(value = "/userInfo/userInfoDetail.json")
    public ResponseObject<UserInfoDetailDto> searchUserDetail(final HttpServletRequest request,
        @RequestParam final Long userNo) {

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo param = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원정보관리 회원정보 상세")
            .processor(String.valueOf(userNo))
            .processorTaskSql("")
            .build();

        privacyLogService.send(param);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, "/admin/userInfo/getUserInfoDetail?userNo=" + userNo,
            headers,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 회원관리 상세 > 마케팅 정보 조회
     * @param userId 사용자 아이디
     * @param regDt 가입일시
     * @return 마케팅 정보 반환
     */
    @ResponseBody
    @GetMapping(value = "/userInfo/userAgreeInfo.json")
    public ResponseObject<UserInfoDetailAgreeInfo> getUserSsoAgreeInfo(@RequestParam final String userId,
        @RequestParam final String regDt) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/userInfo/getSsoAgreeInfo?userId=" + userId + "&regDt=" + regDt,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 회원관리 상세 > 대체카드번호, 계좌번호 복호화 및 마스킹 조회
     * @param param 회원정보 상세 복호화 및 마스킹 처리대상 파라미터
     * @return 복호화 된 대체카드번호, 계좌번호를 반환
     */
    @ResponseBody
    @PostMapping(value = "/userInfo/decryptUserInfo.json")
    public ResponseObject<UserDecryptDto> getDecryptUserInfo(@RequestBody final UserDecryptParam param) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.USERMNG, param,
            "/admin/userInfo/getDecryptUserInfo",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * MHC 포인트 조회
     * @param mhcCardnoHmac MHC 대체카드번호 HMAC
     * @return MHC 포인트(보유 포인트, 사용가능 포인트) 반환
     */
    @ResponseBody
    @PostMapping(value = "/userInfo/mhcPoint.json")
    public ResponseObject<UserMhcPointDto> getUserMhcPoint(
        @RequestBody @Valid final UserInfoMhcParam mhcCardnoHmac) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROW, mhcCardnoHmac, "/mhc/external/searchMhcPoint",
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 배송지 주소 삭제
     * @param seq 배송지 정보 일련 번호
     */
    @ResponseBody
    @GetMapping(value = "/userInfo/deleteShippingAddress.json")
    public ResponseObject<Boolean> deleteShippingAddress(@RequestParam final List<Long> seq) {
        UriComponents uri = UriComponentsBuilder
            .fromUriString("/admin/userInfo/deleteUserShippingAddress")
            .queryParam("seq", seq.toArray())
            .build();

        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, uri.toUriString(),
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 등급이력 팝업
     */
    @GetMapping(value = "/popup/gradeHistory")
    public String gradeHistoryController(final Model model, @RequestParam final long userNo) {
        model.addAttribute("userNo", userNo);
        model.addAllAttributes(userGradeHistoryGridBaseInfo);
        return "/user/pop/userGradeHistoryPop";
    }

    @ResponseBody
    @GetMapping(value = "/userInfo/gradeHistory.json")
    public ResponseObject<List<UserGradeHistoryDto>> getUserGradeHistory(
        @RequestParam final long userNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, "/admin/userInfo/gradeHistory?userNo=" + userNo,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 로그인로그 팝업
     */
    @GetMapping(value = "/popup/loginLog")
    public String loginLogController(final Model model, @RequestParam final long userNo) {
        model.addAttribute("userNo", userNo);
        model.addAllAttributes(userLoginLogGridBaseInfo);
        return "/user/pop/userLoginLogPop";
    }

    @ResponseBody
    @GetMapping(value = "/userInfo/loginLog.json")
    public ResponseObject<List<UserLoginLogDto>> getUserLoginLog(@RequestParam final long userNo) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, "/admin/loginLog/getLoginLogList?userNo=" + userNo,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 환불계좌관리 계좌 삭제
     */
    @Validated
    @ResponseBody
    @GetMapping(value = "/userInfo/removeRefundAccount.json")
    public ResponseObject<Boolean> removeRefundAccount(@RequestParam @NotNull final Long seq) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG,
            "/admin/userInfo/removeRefundAccount?seq=" + seq,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * 등급변경 팝업
     */
    @GetMapping(value = "/popup/userGradeUpdate")
    public String userGradeUpdatePopupController(final Model model, @RequestParam final Long userNo) {

        UserGradeInfo userGradeInfo = getUserGradeInfo(new UserGradeInfoParam(userNo));
        List<UserGradeDefinitionSelectBox> userGradeSelectBoxInfo = getUserGradeDefinitionSelectBox();

        model.addAttribute("userGradeInfo", userGradeInfo);
        model.addAttribute("userGradeSelectBoxInfo", userGradeSelectBoxInfo);

        model.addAllAttributes(userGradeUpdateGridBaseInfo);

        return "/user/pop/userGradeUpdate";
    }

    /**
     * 회원상세 > 등급변경 > 회원정보 조회
     * @param param
     */
    private UserGradeInfo getUserGradeInfo(final UserGradeInfoParam param) {
        final HttpServletRequest request = ServletUtils.currentRequest();
        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logParam = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원정보관리 회원정보상세 회원등급변경 팝업 회원정보 조회")
            .processor("")
            .processorTaskSql("")
            .build();

        privacyLogService.send(logParam);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        ResourceClientRequest<UserGradeInfo> clientRequest =
            ResourceClientRequest.<UserGradeInfo>getBuilder()
                .apiId(ResourceRouteName.USERMNG)
                .uri("/admin/userInfo/getUserGradeInfo")
                .postObject(param)
                .typeReference(new ParameterizedTypeReference<>() {})
                .httpHeaders(headers)
                .build();

        ResponseObject<UserGradeInfo> responseObject = resourceClient
            .post(clientRequest, AdminConstants.ADMIN_DEFAULT_TIMEOUT_CONFIG).getBody();

        return responseObject.getReturnCode().equals(ResponseObject.RETURN_CODE_SUCCESS)
            ? responseObject.getData() : new UserGradeInfo();
    }

    /**
     * 회원상세 > 등급변경 > 등급정의 셀렉트박스 정보 조회(Gray 등급제외)
     */
    private List<UserGradeDefinitionSelectBox> getUserGradeDefinitionSelectBox() {
        ResourceClientRequest<List<UserGradeDefinitionSelectBox>> request =
            ResourceClientRequest.<List<UserGradeDefinitionSelectBox>>getBuilder()
            .apiId(ResourceRouteName.USERMNG)
            .uri("/admin/userInfo/getGradeDefinitionExcludeGray")
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        ResponseObject<List<UserGradeDefinitionSelectBox>> responseObject = resourceClient
            .get(request, AdminConstants.ADMIN_DEFAULT_TIMEOUT_CONFIG).getBody();

        return responseObject.getReturnCode().equals(ResponseObject.RETURN_CODE_SUCCESS)
            ? responseObject.getData() : Collections.singletonList(new UserGradeDefinitionSelectBox());
    }

    /**
     * 회원상세 > 등급변경 > 콜센터 문의내역 조회
     * @param param 어드민 > 회원상세 > 등급변경 > 콜센터 문의내역 조회 파라미터
     * @return 콜센터 문의내역 조회목록
     */
    @ResponseBody
    @PostMapping(value = "/userInfo/getCallCenterInquiries.json")
    public ResponseObject<List<CallCenterInquiryGet>> getCallCenterInquiries(
        @RequestBody @Valid final CallCenterInquiryParam param) {

        ResourceClientRequest<List<CallCenterInquiryGet>> request =
            ResourceClientRequest.<List<CallCenterInquiryGet>>getBuilder()
                .apiId(ResourceRouteName.USERMNG)
                .uri("/admin/userInfo/getCallCenterInquiries")
                .postObject(param)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();

        return resourceClient.post(request, AdminConstants.ADMIN_DEFAULT_TIMEOUT_CONFIG).getBody();
    }

    /**
     * 회원상세 > 등급변경 > 회원 등급변경
     * @param param 회원상세 > 등급변경 > 회원 등급변경 파라미터
     */
    @ResponseBody
    @PostMapping(value = "/userInfo/updateUserGrade.json")
    public ResponseObject<Boolean> updateUserGrade(final HttpServletRequest request,
        @RequestBody @Valid final UserGradeUpdateParam param) {

        final String privacyAccessIp = ServletUtils.clientIP(request);
        final String privacyAccessUserId = cookieService.getUserInfo().getUserId();

        final PrivacyLogInfo logParam = PrivacyLogInfo.builder()
            .txTime(DateTimeUtil.getNowMillYmdHis())
            .accessIp(privacyAccessIp)
            .userId(privacyAccessUserId)
            .txCodeUrl(request.getRequestURI())
            .txMethod(PersonalLogMethod.SELECT.name())
            .txCodeName("회원정보관리 회원정보상세 회원등급변경")
            .processor(param.toString())
            .processorTaskSql("")
            .build();

        privacyLogService.send(logParam);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_IP, privacyAccessIp);
        headers.set(PrivacyHeaderKey.PRIVACY_ACCESS_USER_ID, privacyAccessUserId);

        param.setRegId(cookieService.getUserInfo().getEmpId());

        ResourceClientRequest<Boolean> clientRequest =
            ResourceClientRequest.<Boolean>getBuilder()
                .apiId(ResourceRouteName.USERMNG)
                .uri("/admin/userInfo/updateUserGrade")
                .postObject(param)
                .typeReference(new ParameterizedTypeReference<>() {})
                .httpHeaders(headers)
                .build();

        return resourceClient.post(clientRequest, AdminConstants.ADMIN_DEFAULT_TIMEOUT_CONFIG)
            .getBody();
    }
}
