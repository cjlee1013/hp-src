package kr.co.homeplus.admin.web.item.model.auroraCategory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraCategorySetParamDto {

    private Integer cateCd;

    private String cateNm;

    private Integer priority;

    private int depth;

    private int parentCateCd;

    private String dispYn;

    private String useYn;

    private String regId;

}
