package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PartnerPopupListParamDto {
	private String schPartnerType;
	private String schType;
	private String schValue;
}
