package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.market.MarketItemParamDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSaleStatusGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemSpecHistGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemStatusDetailGetDto;
import kr.co.homeplus.admin.web.item.model.market.MarketItemStatusGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/item/market")
public class MarketItemStatusController {

    private final ResourceClient resourceClient;
    private final CodeService codeService;

    public MarketItemStatusController(ResourceClient resourceClient,
        CodeService codeService) {
        this.resourceClient = resourceClient;
        this.codeService = codeService;
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 외부연동현황관리 NEW
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/marketItemMain", method = RequestMethod.GET)
    public String marketItemMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCodeModel(model,
            "use_yn"        // 사용여부
            ,   "item_status"        // 홈플러스 상품상태
            ,   "market_item_status" // 연동사이트 상품상태
            ,   "send_yn"            // 연동여부
        );

        //그리드
        model.addAllAttributes(
            RealGridHelper.create("marketItemListGridBaseInfo", MarketItemStatusGetDto.class));

        model.addAllAttributes(
            RealGridHelper.create("marketItemStorePriceGridBaseInfo", MarketItemSaleStatusGetDto.class));

        return "/item/marketItemMain";
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 외부연동현황관리 NEW 조회
     * @param marketItemParamDto
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMarketItemList.json"}, method = RequestMethod.POST)
    public List<MarketItemStatusGetDto> getMarketItemList(@RequestBody @Valid MarketItemParamDto marketItemParamDto) {

        ResourceClientRequest<List<MarketItemStatusGetDto>> request = ResourceClientRequest.<List<MarketItemStatusGetDto>>postBuilder()
            .apiId(ResourceRouteName.ITEM)
            .uri("/item/market/getMarketItemList")
            .postObject(marketItemParamDto)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 상품관리 > 마켓연동상품관리 > 외부연동현황관리 NEW 조회 > detail 조회
     * @param itemNo
     * @param partnerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMarketItemDetail.json"}, method = RequestMethod.GET)
    public MarketItemStatusDetailGetDto getMarketItemDetail(@RequestParam String itemNo, @RequestParam String partnerId) {
        String apiUri = "/item/market/getMarketItemHistDetail";

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("itemNo", itemNo));
        setParameterList.add(SetParameter.create("partnerId", partnerId));


        ResourceClientRequest<MarketItemStatusDetailGetDto> request = ResourceClientRequest.<MarketItemStatusDetailGetDto>getBuilder()
            .apiId(ResourceRouteName.ITEM)
            .uri(apiUri + StringUtil.getParameter(setParameterList))
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();

    }

    /**
     * 외부연동 상품 연동이력 조회 (mongo DB)
     * @param itemNo
     * @param partnerId
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getMarketItemSpecHistList.json"}, method = RequestMethod.GET)
    public List<MarketItemSpecHistGetDto> getMarketItemSpecHistList(@RequestParam String itemNo, @RequestParam String partnerId, @RequestParam int pageNo) {
        String apiUri = "/item/market/getMarketItemSpecHistList";

        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("itemNo", itemNo));
        setParameterList.add(SetParameter.create("partnerId", partnerId));
        setParameterList.add(SetParameter.create("pageNo", pageNo));

        ResourceClientRequest<List<MarketItemSpecHistGetDto>> request = ResourceClientRequest.<List<MarketItemSpecHistGetDto>>getBuilder()
            .apiId(ResourceRouteName.ITEM)
            .uri(apiUri + StringUtil.getParameter(setParameterList))
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();

        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();

    }



}

