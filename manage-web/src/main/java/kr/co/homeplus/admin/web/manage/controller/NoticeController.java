package kr.co.homeplus.admin.web.manage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.notice.NoticeFileListGetDto;
import kr.co.homeplus.admin.web.manage.model.notice.NoticeListParamDto;
import kr.co.homeplus.admin.web.manage.model.notice.NoticeListSelectDto;
import kr.co.homeplus.admin.web.manage.model.notice.NoticePartnerListSelectDto;
import kr.co.homeplus.admin.web.manage.model.notice.NoticeSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage")
public class NoticeController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > 공지사항관리
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/notice/noticeMain", method = RequestMethod.GET)
    public String noticedMain(Model model ) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                    "notice_disp_area"      // 공지사항호출영역
                ,	"notice_search_type"    // 공지사항 검색어 구분
                ,   "notice_use_yn"         // 공지사항 사용여부
                ,   "notice_top_yn"         // 공지사항 중요공지
                ,   "notice_period_type"
        );

        model.addAttribute("searchDispArea", code.get("notice_disp_area"));
        model.addAttribute("searchType", code.get("notice_search_type"));
        model.addAttribute("searchUseYn", code.get("notice_use_yn"));
        model.addAttribute("searchTopYn", code.get("notice_top_yn"));
        model.addAttribute("searchPeriodType", code.get("notice_period_type"));
        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("noticeListGridBaseInfo", NoticeListSelectDto.class));
        model.addAllAttributes(RealGridHelper.create("noticePartnerListGridBaseInfo", NoticePartnerListSelectDto.class));

        return "/manage/noticeMain";
    }

    /**
     * 사이트관리 > 공지사항관리 > 공지사항 리스트
     *
     * @param listParamDto
     * @return NoticeListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/getNoticeList.json"}, method = RequestMethod.GET)
    public List<NoticeListSelectDto> getFaqList(NoticeListParamDto listParamDto) {
        String apiUri = "/manage/notice/getNoticeList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<NoticeListSelectDto>>>() {};
        return (List<NoticeListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, NoticeListParamDto.class, listParamDto), typeReference).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 공지사항 리스트
     *
     * @param noticeNo
     * @return NoticeListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/getNotice.json"}, method = RequestMethod.GET)
    public NoticeListSelectDto getFaqList(@RequestParam(name = "noticeNo") int noticeNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("noticeNo", noticeNo));

        String apiUri = "/manage/notice/getNotice" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri, new ParameterizedTypeReference<ResponseObject<NoticeListSelectDto>>() {}).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 공지사항 파일 리스트
     *
     * @param noticeNo
     * @return NoticeFileListGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/getNoticeFileList.json"}, method = RequestMethod.GET)
    public List<NoticeFileListGetDto> getFaqList(String noticeNo) {
        String apiUri = "/manage/notice/getNoticeFileList?noticeNo=";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<NoticeFileListGetDto>>>() {};
        return (List<NoticeFileListGetDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE,apiUri+noticeNo, typeReference).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 공지사항 등록/수정
     *
     * @param noticeSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/setNotice.json"}, method = RequestMethod.POST)
    public ResponseResult setNotie(@RequestBody NoticeSetParamDto noticeSetParamDto) {
        String apiUri = "/manage/notice/setNotice";
        noticeSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, noticeSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 개별공지사항 파트너 리스트 조회
     *
     * @param noticeNo
     * @return NoticePartnerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/getNoticePartnerList.json"}, method = RequestMethod.GET)
    public List<NoticePartnerListSelectDto> getNoticePartnerList(@RequestParam(name = "noticeNo") String noticeNo) {
        String apiUri = "/manage/notice/getNoticePartnerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<NoticePartnerListSelectDto>>>() {};
        return (List<NoticePartnerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?noticeNo=" + noticeNo , typeReference).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 파트너 리스트 조회
     *
     * @param partnerIdArr
     * @return NoticePartnerListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/notice/getPartnerList.json"}, method = RequestMethod.GET)
    public List<NoticePartnerListSelectDto> getPartnerList(@RequestParam(name = "partnerIdArr") String partnerIdArr) {
        String apiUri = "/manage/notice/getPartnerList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<NoticePartnerListSelectDto>>>() {};
        return (List<NoticePartnerListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?partnerIdArr=" + partnerIdArr , typeReference).getData();
    }

    /**
     * 사이트관리 > 공지사항관리 > 파트너 등록 팝업
     *
     * @param partnerCnt
     * @return String
     */
    @Deprecated
    @RequestMapping(value = {"/popup/temp/getPartnerListPop"}, method = RequestMethod.GET)
    public String getPartnerListPop(Model model, @RequestParam(value = "partnerCnt") String partnerCnt) {
        model.addAttribute("partnerCnt", partnerCnt);
        return "/manage/pop/addPartnerPop";
    }

}
