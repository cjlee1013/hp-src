package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "결제관리 > 결제수단 관리 > PG 수수료 대상 search DTO")
@Getter
@Setter
@EqualsAndHashCode
public class PgCommissionTargetSelectDto {
    private Long pgCommissionMngSeq;
    private String methodCd;
}
