package kr.co.homeplus.admin.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.sellerShop.SellerShopListParamDto;
import kr.co.homeplus.admin.web.partner.model.sellerShop.SellerShopListSelectDto;
import kr.co.homeplus.admin.web.partner.model.sellerShop.SellerShopSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/partner")
public class SellerShopController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/sellerShopMain", method = RequestMethod.GET)
    public String PartnerSellerShopMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "seller_shop_use_yn"    // 셀러샵 사요여부
            , "seller_shop_cs_yn"   // 셀러샵 고객센터 노출 여부
        );

        model.addAttribute("hmpImgUrl", this.hmpImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);
        model.addAttribute("sellerShopUseYn", code.get("seller_shop_use_yn"));
        model.addAttribute("sellerShopCsYn", code.get("seller_shop_cs_yn"));

        model.addAllAttributes(RealGridHelper.create("sellerShopListGridBaseInfo", SellerShopListSelectDto.class));

        return "/partner/sellerShopMain";
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 리스트
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerShopList.json"}, method = RequestMethod.GET)
    public List<SellerShopListSelectDto> getSellerShopList(SellerShopListParamDto sellerShopListParamDto) {
        String apiUri = "/partner/getSellerShopList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SellerShopListSelectDto>>>() {};
        return (List<SellerShopListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, SellerShopListParamDto.class, sellerShopListParamDto), typeReference).getData();
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 중복확인
     *
     * @param
     * @return
     */
    @ResponseBody @GetMapping(value = {"/setSellerShopValidation.json"})
    public ResponseResult validationByShopNmOrShopUrl(@RequestParam(value = "partnerId") String partnerId
        , @RequestParam(value = "type") String type
        , @RequestParam(value = "checkKeyword") String checkKeyword) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));
        getParameterList.add(SetParameter.create(type, checkKeyword));

        String apiUriRequest = "/partner/setSellerShopValidation" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){})
            .getData();
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 중복확인
     *
     * @param
     * @return
     */
    @ResponseBody @GetMapping(value = {"/getSellerShopItemCnt.json"})
    public ResponseResult getSellerShopItemCnt(@RequestParam(value = "partnerId") String partnerId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUriRequest = "/partner/getSellerShopItemCnt" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(
            ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>(){}
        ).getData();
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/setSellerShop.json"}, method = RequestMethod.POST)
    public ResponseResult setSellerShop(@RequestBody SellerShopSetParamDto setParamDto) {
        String apiUri = "/partner/setSellerShop";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}