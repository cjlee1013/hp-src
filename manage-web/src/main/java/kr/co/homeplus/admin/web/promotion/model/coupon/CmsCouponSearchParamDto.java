package kr.co.homeplus.admin.web.promotion.model.coupon;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "CmsCouponSearchParamDto", description = "CMS 쿠폰목록 조회 param Dto")
@Getter
@Setter
public class CmsCouponSearchParamDto {
    private String couponsdate; // 행사기간 시작일 "YYYYMMDD")
    private String couponedate; // 행사기간 종료일 "YYYYMMDD")
    private String couponType;  // 264: 장바구니
    private String checkflag;   // 1 또는 기타

    /* 발행부서

    1: e-마케팅기획팀
    2: 온라인영업개선팀
    3: 온라인식품영업팀
    4: 온라인비식품영업팀
    5: 프로모션팀
    6: 패밀리카드팀
    7: 지역마케팅팀
    8: 영업운영본부
    9: 신사업개발팀
    10: 뉴미디어팀
    11: FMC
    */
    private String issuedept;
    private String coyn;  // (Y: GHS, N: C.O, R:Express)
}
