package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.margin.MarginDetailListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginDetailListSetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginHeadListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginListSetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginPartListGetDto;
import kr.co.homeplus.admin.web.settle.model.margin.MarginTeamListGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarginService {

    private final ResourceClient resourceClient;

    /**
     * DC/DS 마진조회 팀별 리스트 조회
     */
    public List<MarginTeamListGetDto> getMarginTeamList(MarginListSetDto marginListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marginListSetDto,
            "/admin/margin/getMarginTeamList",
            new ParameterizedTypeReference<ResponseObject<List<MarginTeamListGetDto>>>(){}).getData();
    }

    /**
     * DC/DS 마진조회 파트별 리스트 조회
     */
    public List<MarginPartListGetDto> getMarginPartList(MarginListSetDto marginListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marginListSetDto,
            "/admin/margin/getMarginPartList",
            new ParameterizedTypeReference<ResponseObject<List<MarginPartListGetDto>>>(){}).getData();
    }

    /**
     * DC/DS 마진조회 유형별 리스트 조회
     */
    public List<MarginHeadListGetDto> getMarginHeadList(MarginListSetDto marginListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marginListSetDto,
            "/admin/margin/getMarginHeadList",
            new ParameterizedTypeReference<ResponseObject<List<MarginHeadListGetDto>>>(){}).getData();
    }

    /**
     * DC/DS 마진상세조회 메인 리스트 조회
     */
    public List<MarginDetailListGetDto> getMarginDetailList(MarginDetailListSetDto marginDetailListSetDto){
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            marginDetailListSetDto,
            "/admin/margin/getMarginDetailList",
            new ParameterizedTypeReference<ResponseObject<List<MarginDetailListGetDto>>>(){}).getData();
    }

}
