package kr.co.homeplus.admin.web.manage.model.dspMobileMain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MobileMainGetDto {

    private Integer mainNo;

    private String loc1Depth;

    private String dispStoreType;

    private String bgColor;

    private String closeType;

    private String division;

    private String template;

    private String templateDetail;

    private List<MobileMainStoreGetDto> storeList;

    private List<MobileMainLinkGetDto> linkList;

}