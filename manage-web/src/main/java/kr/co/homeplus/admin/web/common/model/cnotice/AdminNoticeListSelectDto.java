package kr.co.homeplus.admin.web.common.model.cnotice;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminNoticeListSelectDto {

    @RealGridColumnInfo(headText = "제목", width = 600 , columnType = RealGridColumnType.NAME)
    private String noticeTitle;

    @RealGridColumnInfo(headText = "등록자", width = 150)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일시", width = 150)
    private String regDt;

    @RealGridColumnInfo(headText = "NO", width = 0, hidden = true)
    private String noticeNo;

    @RealGridColumnInfo(headText = "공지사항 상단 고정여부", hidden = true)
    private String topYn;

    @RealGridColumnInfo(headText = "공지사항 내용", hidden = true)
    private String noticeDesc;

}
