package kr.co.homeplus.admin.web.core.exception.handler;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * buiness 에러코드 정의
 */
@RequiredArgsConstructor
public enum BusinessExceptionCode {
		ERROR_CODE_1001("1001", "%d건 이상 업로드 할 수 없습니다.")
	  , ERROR_CODE_1002("1002", "증복된 회원번호가 존재합니다.\n확인하세요.")
	  , ERROR_CODE_1003("1003", "조회된 회원정보가 없습니다.")
	  , ERROR_CODE_1004("1004", "유효하지 않은 회원번호를 포함하고 있으니 확인하세요.")
	  , ERROR_CODE_1005("1005", "파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.")
	  , ERROR_CODE_1006("1006", "적용할 유효한 데이터가 업습니다.")
	  , ERROR_CODE_1007("1007", "일괄등록 1회 수행시 최대 상품 %d개를 초과했습니다.")
	  , ERROR_CODE_1008("1008", "상품 정보 조회 중 오류가 발생하였습니다. 관리자에게 문의해 주세요.")
	  , ERROR_CODE_1009("1009", "상품 데이터 조합 중 오류가 발생하였습니다. 셀 갯수 / 유효한 데이터를 확인해 주세요. >> 셀 갯수 [%d] itemNo [%s]")
	  , ERROR_CODE_1010("1010", "점포유형이 잘못 되었습니다.")
	  , ERROR_CODE_1011("1011", "빈 데이터가 존재 합니다.")
	  , ERROR_CODE_1012("1012", "등록 요청 상품이 없습니다.")
	  , ERROR_CODE_1013("1013", "정상적으로 처리되지 않았습니다. 다시 시도해주세요.")
	  , ERROR_CODE_1014("1014", "파일 내 중복된 데이터를 확인 후 다시 시도해주세요.")
	  , ERROR_CODE_1015("1015", "파일 내 처리할 데이터가 없습니다.")
	  , ERROR_CODE_1016("1016", "일괄업로드는 한번에 %d건 씩 가능합니다. 파일 확인후 다시 업로드 해 주세요.")
	;

	@Getter
	private final String code;
	@Getter
	private final String desc;

	@Override
	public String toString() {
		return code + ": " + desc;
	}
}
