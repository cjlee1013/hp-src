package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsExhDto;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsItemDto;
import kr.co.homeplus.admin.web.statistics.model.ExhibitionSaleStatisticsSelectDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExhibitionSaleStatisticsService {
    private final ResourceClient resourceClient;

    public List<ExhibitionSaleStatisticsExhDto> getExhibitionSaleStatisticsExh(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        String apiUri = StringUtil.getRequestString(EscrowConstants.ESCROW_GET_EXHIBITION_SALE_STATISTICS_EXH, ExhibitionSaleStatisticsSelectDto.class, exhibitionSaleStatisticsSelectDto);
        var typeReference = new ParameterizedTypeReference<ResponseObject<List<ExhibitionSaleStatisticsExhDto>>>() {};
        return resourceClient.getForResponseObject(ResourceRouteName.ESCROWMNG, apiUri, typeReference).getData();
    }

    public List<ExhibitionSaleStatisticsItemDto> getExhibitionSaleStatisticsItem(ExhibitionSaleStatisticsSelectDto exhibitionSaleStatisticsSelectDto) {
        var typeReference = new ParameterizedTypeReference<ResponseObject<List<ExhibitionSaleStatisticsItemDto>>>() {};
        return resourceClient.postForResponseObject(ResourceRouteName.ESCROWMNG, exhibitionSaleStatisticsSelectDto, EscrowConstants.ESCROW_GET_EXHIBITION_SALE_STATISTICS_ITEM, typeReference).getData();
    }
}
