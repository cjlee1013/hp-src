package kr.co.homeplus.admin.web.item.model.issuePrice;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class IssuePriceListGetDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, columnType = RealGridColumnType.NAME, sortable = true)
    private String itemNm;

    @RealGridColumnInfo(headText = "점포ID", width = 60)
    private String storeId;

    @RealGridColumnInfo(headText = "점포명", columnType = RealGridColumnType.BASIC)
    private String storeNm;

    @RealGridColumnInfo(headText = "전일판매가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String beforeSalePrice;

    @RealGridColumnInfo(headText = "전일행사가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String beforeSimplePrice;

    @RealGridColumnInfo(headText = "전일상품할인가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String beforeDcSalePrice;

    @RealGridColumnInfo(headText = "당일판매가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String salePrice;

    @RealGridColumnInfo(headText = "당일행사가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String simpleSalePrice;

    @RealGridColumnInfo(headText = "당일상품할인가", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 80, sortable = true)
    private String dcSalePrice;
}