package kr.co.homeplus.admin.web.search.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelatedKeywordSetDto {

    private Long totalCount;
    private List<RelatedKeyword> result;

    @Getter
    @Setter
    static class RelatedKeyword {
        private Long relationId;
        private String siteType;
        private String keyword;
        private String relationKeyword;
        private String relationType;
        private String startDt;
        private String endDt;
        private String useYn;
        private String creator;
        private String modifier;
        private String createdDt;
        private String modifyDt;
        private String status;
    }
}
