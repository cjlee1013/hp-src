package kr.co.homeplus.admin.web.promotion.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.item.model.store.StoreListParamDto;
import kr.co.homeplus.admin.web.item.model.store.StoreListSelectDto;
import kr.co.homeplus.admin.web.promotion.model.promo.BenefitZoneDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.BenefitZoneDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.BenefitZoneListGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.BenefitZoneListParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.SpecialZoneDetailGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.SpecialZoneDetailSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.SpecialZoneListGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.SpecialZoneListParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequiredArgsConstructor
public class PromotionDisplayController {
    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String hmpImgUrl;

    @ApiOperation(value = "혜택존 전시 관리 main 화면")
    @ApiResponse(code = 200, message = "혜택존 전시 관리 main 화면")
    @GetMapping("/benefitZone/benefitZoneMain")
    public String main(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
             "site_type"       // 사이트 구분
            , "display_yn"              // 전시여부
            , "benefit_device"          // 혜택존 적용시스템
            , "benefit_disp_loc"        // 혜택존 전시 위치
            , "benefit_link_type"       // 혜택존 링크 유형
            , "promo_store_type"        // 점포유형
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("siteType", code.get("site_type"));
        model.addAttribute("dispYn", code.get("display_yn"));
        model.addAttribute("deviceType", code.get("benefit_device"));
        model.addAttribute("dispLoc", code.get("benefit_disp_loc"));
        model.addAttribute("linkType", code.get("benefit_link_type"));
        model.addAttribute("storeType", code.get("promo_store_type"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        //택배점 리스트 조회
        String apiUri = "/item/store/getStoreList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject< List<StoreListSelectDto>>>() {};
        List<StoreListSelectDto> dlvStoreList =  (List<StoreListSelectDto>) resourceClient.postForResponseObject
            (ResourceRouteName.ITEM, StoreListParamDto.builder().searchStoreKind("DLV").searchUseYn("Y").build(), apiUri, typeReference).getData();

        model.addAttribute("dlvStoreList", dlvStoreList);

        ObjectMapper om = new ObjectMapper();
        model.addAttribute("linkTypeJson", om.writeValueAsString(code.get("benefit_link_type")));
        model.addAttribute("dispLocJson", om.writeValueAsString(code.get("benefit_disp_loc")));
        model.addAttribute("storeTypeJson", om.writeValueAsString(code.get("promo_store_type")));
        model.addAttribute("dlvStoreListJson", om.writeValueAsString(dlvStoreList));

        model.addAllAttributes(RealGridHelper.create("benefitZoneGridBaseInfo", BenefitZoneListGetDto.class));

        return "/promotion/benefitZone";
    }

    /**
     * 혜택존 조회
     * @param benefitZoneListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/benefitZone/getBenefitZoneList.json"})
    public List<BenefitZoneListGetDto> getBenefitZoneList(BenefitZoneListParamDto benefitZoneListParamDto) {
        String apiUri = "/promotion/benefitZone/getBenefitZoneList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<BenefitZoneListGetDto>>>() {};

        return (List<BenefitZoneListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, benefitZoneListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 혜택존 상세 조회
     * @param benefitNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/benefitZone/getBenefitZoneDetail.json"})
    public BenefitZoneDetailGetDto getBenefitZoneDetail(@RequestParam(name = "benefitNo") Long benefitNo) {
        String apiUri = "/promotion/benefitZone/getBenefitZoneDetail";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<BenefitZoneDetailGetDto>>() {};

        return (BenefitZoneDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?benefitNo=" + benefitNo , typeReference).getData();
    }

    /**
     * 혜택존 등록/수정
     * @param benefitZoneDetailSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/benefitZone/setBenefitZone.json"})
    public Long setBenefitZone(@RequestBody BenefitZoneDetailSetDto benefitZoneDetailSetDto) throws Exception {
        benefitZoneDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/benefitZone/setBenefitZone";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, benefitZoneDetailSetDto, apiUri, typeReference).getData();
    }

    @ApiOperation(value = "전문관 전시 관리 main 화면")
    @ApiResponse(code = 200, message = "전문관 전시 관리 main 화면")
    @GetMapping("/specialZone/specialZoneMain")
    public String specialZoneMain(final Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"         // 사용여부
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("hmpImgUrl", this.hmpImgUrl);

        model.addAllAttributes(RealGridHelper.create("specialZoneGridBaseInfo", SpecialZoneListGetDto.class));

        return "/promotion/specialZone";
    }

    /**
     * 전문관 조회
     * @param specialZoneListParamDto
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/specialZone/getSpecialZoneList.json"})
    public List<SpecialZoneListGetDto> getSpecialZoneList(SpecialZoneListParamDto specialZoneListParamDto) {
        String apiUri = "/promotion/specialZone/getSpecialZoneList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SpecialZoneListGetDto>>>() {};

        return (List<SpecialZoneListGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, specialZoneListParamDto, apiUri, typeReference).getData();
    }

    /**
     * 전문관 상세 조회
     * @param specialNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/specialZone/getSpecialZoneDetail.json"})
    public SpecialZoneDetailGetDto getSpecialZoneDetail(@RequestParam(name = "specialNo") Long specialNo) {
        String apiUri = "/promotion/specialZone/getSpecialZoneDetail";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<SpecialZoneDetailGetDto>>() {};

        return (SpecialZoneDetailGetDto) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, apiUri + "?specialNo=" + specialNo , typeReference).getData();
    }

    /**
     * 전문관 등록/수정
     * @param specialZoneDetailSetDto
     * @return
     */
    @ResponseBody
    @PostMapping(value = {"/specialZone/setSpecialZone.json"})
    public Long setSpecialZone(@RequestBody SpecialZoneDetailSetDto specialZoneDetailSetDto) throws Exception {
        specialZoneDetailSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

        String apiUri = "/promotion/specialZone/setSpecialZone";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<Long>>() {};

        return (Long) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, specialZoneDetailSetDto, apiUri, typeReference).getData();
    }
}
