package kr.co.homeplus.admin.web.user.enums;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.plus.api.support.realgrid.RealGridLookUpSupport;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ConsumerType implements RealGridLookUpSupport {
    PURCHASE_RESTRICTION(1, "구매제한")
    , COMMUNITY_RESTRICTION(2, "커뮤니티 이용제한")
    , BLACK_CONSUMER(3, "블랙컨슈머")
    ;

    private static final Map<String, String> LOOK_UP_MAP = Stream
        .of(values()).collect(Collectors.toMap(ConsumerType::name, ConsumerType::getDesc));

    @Getter
    private final int code;
    @Getter
    private final String desc;

    @Override
    public Map<String, String> getLookUpMap() {
        return LOOK_UP_MAP;
    }
}
