package kr.co.homeplus.admin.web.voc.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.List;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryDetailGetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryDetailSetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryListGetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.MarketInquiryListSetDto;
import kr.co.homeplus.admin.web.voc.model.marketInquiry.NaverInquiryAnswerSetDto;
import kr.co.homeplus.admin.web.voc.service.MarketInquiryService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 회원관리 > 고객문의관리 > 제휴사 고객문의
 */
@Controller
@RequestMapping("/voc")
public class MarketInquiryController {
    @Autowired
    private MarketInquiryService marketInquiryService;
    @Autowired
    private ShipCommonService shipCommonService;

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    /**
     * 제휴사 고객문의 Main Page
     */
    @GetMapping("/marketInquiry/marketInquiryMain")
    public String marketInquiryMain(Model model) throws Exception {
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);
        model.addAllAttributes(RealGridHelper.create("marketInquiryGridBaseInfo", MarketInquiryListGetDto.class));

        return "/voc/marketInquiryMain";
    }

    /**
     * 제휴사 고객문의 리스트 조회
     */
    @ResponseBody
    @PostMapping("/marketInquiry/getMarketInquiryList.json")
    public List<MarketInquiryListGetDto> getMarketInquiryList(@RequestBody MarketInquiryListSetDto marketInquiryListSetDto) {
        return marketInquiryService.getMarketInquiryList(marketInquiryListSetDto);
    }

    /**
     * 제휴사 고객문의 상세 팝업 Page
     */
    @GetMapping("/popup/marketInquiryDetailPop")
    public String marketInquiryDetailPop(Model model
            , @RequestParam(value = "callback", required = true, defaultValue = "") String callBackScript
            , @RequestParam(value = "messageNo", required = true, defaultValue = "") String messageNo
            , @RequestParam(value = "marketType", required = true, defaultValue = "") String marketType
            , @RequestParam(value = "messageType", required = true, defaultValue = "") String messageType) throws Exception {

        model.addAttribute("messageNo", XssPreventer.escape(messageNo));
        model.addAttribute("marketType", XssPreventer.escape(marketType));
        model.addAttribute("messageType", XssPreventer.escape(messageType));
        model.addAttribute("callBackScript", callBackScript);

        return "/voc/pop/marketInquiryDetailPop";
    }

    /**
     * 제휴사 고객문의 상세 조회
     */
    @ResponseBody
    @PostMapping("/marketInquiry/getMarketInquiryDetail.json")
    public MarketInquiryDetailGetDto getMarketInquiryDetail(@RequestBody MarketInquiryDetailSetDto marketInquiryDetailSetDto) {
        return marketInquiryService.getMarketInquiryDetail(marketInquiryDetailSetDto);
    }

    /**
     * 네이버 고객문의 답변 등록
     */
    @ResponseBody
    @PostMapping(value = "/marketInquiry/setNaverInquiryAnswer.json")
    public ResponseObject<Object> setNaverInquiryAnswer(@RequestBody NaverInquiryAnswerSetDto naverInquiryAnswerSetDto) throws Exception {
        return marketInquiryService.setNaverInquiryAnswer(naverInquiryAnswerSetDto);
    }

}
