package kr.co.homeplus.admin.web.partner.model.sellerShopItem;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class SellerShopItemGetDto {

    @RealGridColumnInfo(headText = "판매업체 ID", width = 120, sortable = true)
    private String partnerId;

    @RealGridColumnInfo(headText = "셀러명", width = 120, sortable = true)
    private String shopNm;

    @RealGridColumnInfo(headText = "사용여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME, sortable = true, width = 150)
    private String chgDt;

    @RealGridColumnInfo(headText = "shopDispType", hidden = true)
    private String shopDispType;

}
