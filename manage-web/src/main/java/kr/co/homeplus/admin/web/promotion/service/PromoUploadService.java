package kr.co.homeplus.admin.web.promotion.service;

import com.github.jknack.handlebars.internal.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.DateTimeFormat;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.core.utility.DateTimeUtil;
import kr.co.homeplus.admin.web.promotion.model.coupon.CouponBundleIssueSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.EventItemExcelGetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.EventItemExcelParamDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoUploadResultSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromoUploadSetDto;
import kr.co.homeplus.admin.web.promotion.model.promo.PromotionSchSetDto;
import kr.co.homeplus.admin.web.user.model.user.UserSearchDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
@RequiredArgsConstructor
public class PromoUploadService {
    private final ResourceClient resourceClient;
    private final ExcelUploadService excelUploadService;
    private final LoginCookieService loginCookieService;

    /**
     * 적용대상 업로드를 위한 엑셀 데이터 읽기
     * @param multipartFile
     * @param promotionType
     * @param applyScope
     * @param exceptScope
     * @param maxRow
     * @return
     */
    public List<PromoUploadSetDto> getApplyScopeExcelData(MultipartFile multipartFile, String promotionType, String applyScope, String exceptScope, int maxRow)
        throws Exception {

        List<PromoUploadSetDto> excelResultList = new ArrayList<>(); // 엑셀에 넣을 결과값 list
        ExcelHeaders headers = null;
        int headerRow;
        int startRow;
        int lastCell;

        if (StringUtils.equals(promotionType, "CARD_DISCOUNT")) {
            headerRow = 3;
            startRow = 4;
            lastCell = 11;
            headers = new ExcelHeaders.Builder()
                .header(0, "", "itemNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
                .header(1, "적용수량", "applyQty")
                .header(2, "할인(수수료)구분", "discountType")
                .header(3, "할인액", "discountPrice")
                .header(4, "할인률(%)", "discountRate")
                .header(5, "수수료 금액", "feeAmt")
                .header(6, "수수료율(%)", "commissionRate")
                .header(7, "쿠폰코드", "couponCd", true)
                .header(8, "시작일", "issueStartDt", true)
                .header(9, "종료일", "issueEndDt", true)
                .header(10, "카드그룹코드", "groupCd", true)
                .build();
        } else {
            headerRow = 0;
            lastCell = 0;
            startRow = 1;
            switch (applyScope) {
                case "DRCT": //절임배추
                case "DRCT_NOR": //일반상품
                case "DRCT_DLV":  //명절선물
                    lastCell = 2;
                    headers = new ExcelHeaders.Builder()
                        .header(0, "상품번호", "itemNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
                        .header(1, "발송지(코드)", "sendPlace", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
                        .build();
                    break;
                case "ITEM":
                    headers = new ExcelHeaders.Builder()
                        .header(0, "상품번호", "itemNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
                        .build();
                    break;
                case "SELL":
                    headers = new ExcelHeaders.Builder()
                        .header(0, "판매업체ID", "partnerId", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
                        .build();
            }
        }
        ExcelUploadOption<PromoUploadSetDto> excelUploadOption = new ExcelUploadOption.Builder<>(
            PromoUploadSetDto.class, headers, startRow, 0)
            .headerRow(headerRow)
            .lastCell(lastCell)
            .build();

        try {
            excelResultList = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        if (ObjectUtils.isEmpty(excelResultList)) {
            //파일 내 처리할 데이터가 없습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1015);
        }

        if (excelResultList.size() > maxRow) {
            //일괄업로드는 한번에 %d건 씩 가능합니다. 파일 확인후 다시 업로드 해 주세요.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1016, maxRow);
        }

        return excelResultList;
    }

    /**
     * 업로드된 엑셀 데이터와 상품명 조회 데이터간 데이터 매칭
     * @param uploadList
     * @param resultList
     */
    public void setMatchCardDiscountInfo(List<PromoUploadSetDto> uploadList, List<PromoUploadSetDto> resultList, String storeType) {
        List<PromoUploadSetDto> finalList = new ArrayList<>();
        String regexDate = "(20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])";

        //업로드 내용
        uploadList.stream()
            .forEach(i -> {
                if (StringUtils.equals(storeType, "DS")) {
                    if (!(StringUtils.isEmpty(i.getItemNo())
                        || i.getItemNo().length() != 14
                        || !(StringUtils.equals(i.getDiscountType(), "1") || StringUtils.equals(i.getDiscountType(), "2"))
                        || ((StringUtils.equals(i.getDiscountType(), "2") && StringUtils.isEmpty(i.getDiscountRate()))
                        || (StringUtils.equals(i.getDiscountType(), "1") && StringUtils.isEmpty(i.getDiscountPrice())))
                        || (StringUtils.isEmpty(i.getGroupCd()) || !i.getGroupCd().matches("^[0-9]{2}"))
                        || (StringUtils.isEmpty(i.getIssueStartDt()) || !i.getIssueStartDt().matches(regexDate))
                        || (StringUtils.isEmpty(i.getIssueEndDt()) || !i.getIssueEndDt().matches(regexDate))
                        || (DateTimeUtil.isDateBefore(i.getIssueStartDt(), i.getIssueEndDt(), DateTimeFormat.DATE_FORMAT_DIGIT_YMD))))
                    {
                        //상품명 매칭
                        Optional<PromoUploadSetDto> itemDto = resultList.stream()
                            .filter(p -> StringUtils.equals(p.getItemNo(), i.getItemNo()))
                            .findAny();
                        if (itemDto.isPresent()) {
                            i.setItemNm(itemDto.get().getItemNm());
                        }

                        finalList.add(i);
                    }
                } else {
                    if (!(StringUtils.isEmpty(i.getCouponCd())
                        || !i.getCouponCd().matches("^[0-9]*$")
                        || StringUtils.equals(i.getItemNo(),  "2000".concat(i.getItemNo()))
                        || i.getItemNo().length() != 13
                        || !(StringUtils.equals(i.getDiscountType(), "1") || StringUtils.equals(i.getDiscountType(), "2"))
                        || ((StringUtils.equals(i.getDiscountType(), "2") && StringUtils.isEmpty(i.getDiscountRate()))
                        || (StringUtils.equals(i.getDiscountType(), "1") && StringUtils.isEmpty(i.getDiscountPrice())))
                        || (StringUtils.isEmpty(i.getGroupCd()) || !i.getGroupCd().matches("^[0-9]{2}"))
                        || (StringUtils.isEmpty(i.getIssueStartDt()) || !i.getIssueStartDt().matches(regexDate))
                        || (StringUtils.isEmpty(i.getIssueEndDt()) || !i.getIssueEndDt().matches(regexDate))
                        || (DateTimeUtil.isDateBefore(i.getIssueStartDt(), i.getIssueEndDt(), DateTimeFormat.DATE_FORMAT_DIGIT_YMD))))
                    {

                        //상품명 매칭
                        int itemNoLen = i.getItemNo().length();
                        Optional<PromoUploadSetDto> itemDto = resultList.stream()
                            .filter(p -> StringUtils.equals(p.getItemNo(), i.getItemNo().substring(4, itemNoLen)))
                            .findAny();
                        if (itemDto.isPresent()) {
                            i.setItemNo(itemDto.get().getItemNo());
                            i.setItemNm(itemDto.get().getItemNm());
                        } else {
                            i.setItemNo(i.getItemNo().substring(4, itemNoLen));
                        }

                        finalList.add(i);
                    }
                }
            });

        resultList.clear();
        resultList.addAll(finalList);
    }

    /**
     * 업로드된 엑셀 데이터와 상품명 조회 데이터간 데이터 매칭(예약상품판매관리 > 엑셀업로드)
     * @param uploadList
     * @param resultList
     */
    public void setMatchRsvSalePlaceInfo(List<PromoUploadSetDto> uploadList, List<PromoUploadSetDto> resultList, String scopeNm) {
        List<PromoUploadSetDto> finalList = new ArrayList<>();

        if (StringUtils.equals(scopeNm, "DRCT_DLV")) {
            uploadList.stream()
                .forEach(i -> {
                    if (!(StringUtils.isEmpty(i.getItemNo())
                        || i.getItemNo().length() != 9
                        || !(StringUtils.equals(i.getSendPlace(), "HAMAN") || StringUtils.equals(i.getSendPlace(), "ANSEONG") || StringUtils.equals(i.getSendPlace(), "STORE")))) {
                        Optional<PromoUploadSetDto> matchDto = resultList.stream().filter(x -> StringUtils.equals(x.getItemNo(), i.getItemNo())).findAny();

                        if (matchDto.isPresent()) {
                            i.setItemNo(matchDto.get().getItemNo());
                            i.setItemNm(matchDto.get().getItemNm());
                            i.setSendPlace(i.getSendPlace());
                        }

                        finalList.add(i);
                    }
                });
        } else {
            uploadList.stream()
                .forEach(i -> {
                    if (!(StringUtils.isEmpty(i.getItemNo())
                        || i.getItemNo().length() != 9
                        || !(StringUtils.equals(i.getSendPlace(), "STORE")))) {

                        Optional<PromoUploadSetDto> matchDto = resultList.stream().filter(x -> StringUtils.equals(x.getItemNo(), i.getItemNo())).findAny();

                        if (matchDto.isPresent()) {
                            i.setItemNo(matchDto.get().getItemNo());
                            i.setItemNm(matchDto.get().getItemNm());
                            i.setSendPlace(i.getSendPlace());
                        }

                        finalList.add(i);
                    }
                });
        }

        resultList.clear();
        resultList.addAll(finalList);
    }

    /**
     * 쿠폰 일괄발급 처리
     * @param multipartFile
     * @param couponNo
     * @return
     */
    public PromoUploadResultSetDto setUploadIssueCoupon(MultipartFile multipartFile, String couponNo) throws Exception {
        PromoUploadResultSetDto resultInfo = new PromoUploadResultSetDto();
        resultInfo.setSuccessYn("N");

        List<PromoUploadSetDto> excelResultList = new ArrayList<>(); // 엑셀에 넣을 결과값 list
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "회원번호", "userNo", true) // 엑셀 헤더명과 POJO 필드명이 다를 경우 별개로 입력
            .build();

        ExcelUploadOption<PromoUploadSetDto> excelUploadOption = new ExcelUploadOption.Builder<>(
            PromoUploadSetDto.class, headers, 1, 0)
            .lastCell(0) //  엑셀 데이터를 3열까지만 처리하고 싶을 경우 설정할 수 있습니다.
            .build();

            try {
                excelResultList = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
            } catch (Exception e) {
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
            }

            if (ObjectUtils.isEmpty(excelResultList)) {
                //파일 내 처리할 데이터가 없습니다.
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1015);
            }

            long uploadSize = excelResultList.size();

            if (uploadSize > 1000) {
                //일괄업로드는 한번에 %d건 씩 가능합니다. 파일 확인후 다시 업로드 해 주세요.
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1016, 1000);
            }

            Set<Long> userNos = excelResultList.stream().map(x -> x.getUserNo()).collect(Collectors.toSet());

            if (ObjectUtils.isEmpty(userNos) || (userNos.size() != excelResultList.size())) {
                //증복된 회원번호가 존재합니다.\n확인하세요.
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1002);
            }

            // user 정보 조회
            String usermngApiUri ="/external/api/search/userNoList/userList";

            List<UserSearchDto> responseResult = resourceClient.postForResponseObject(
                ResourceRouteName.USERMNG, userNos, usermngApiUri, new ParameterizedTypeReference<ResponseObject<List<UserSearchDto>>>() {}
            ).getData();

            if (ObjectUtils.isEmpty(responseResult)) {
               //조회된 회원정보가 없습니다.
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1003);
            } else if (responseResult.size() < userNos.size()) {
                //유효하지 않은 회원번호를 포함하고 있으니 확인하세요.
                throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1004);
            }

            //일괄발급 셋팅
            CouponBundleIssueSetDto couponBundleIssueSetDto = new CouponBundleIssueSetDto();
            couponBundleIssueSetDto.setCouponNo(Long.parseLong(couponNo));
            couponBundleIssueSetDto.setIssueUserList(responseResult);
            couponBundleIssueSetDto.setEmpNo(loginCookieService.getUserInfo().getEmpId());

            long issuedCnt = resourceClient.postForResponseObject(
                ResourceRouteName.MANAGE
                , couponBundleIssueSetDto
                , "/promotion/coupon/setIssueCouponArr"
                , new ParameterizedTypeReference<ResponseObject<Long>>() {
                }
            ).getData();

            resultInfo.setSuccessYn("Y");
            resultInfo.setUploadCount(uploadSize);
            resultInfo.setSuccessCount(issuedCnt);
            resultInfo.setFailCount(uploadSize - issuedCnt);

            return resultInfo;
    }

    /**
     * 적용, 제외정보 일괄등록시 적용대상명 조회 API 호출
     * @param apiNm
     * @param apiUrl
     *
     * @param scopeNm
     * @param uploadSetDtoList
     * @return
     */
    private List<PromoUploadSetDto> getUploadScopeNmApi(String apiNm, String apiUrl, String promotionType, String storeType, String scopeNm, List<PromoUploadSetDto> uploadSetDtoList) {
        List<PromoUploadSetDto> resultList = new ArrayList<>();

        uploadSetDtoList = uploadSetDtoList.stream().filter(c -> trimByApplyCd(scopeNm, c)).distinct().collect(Collectors.toList());

        int limitCount = 1000;
        int listCount = (uploadSetDtoList.size() / limitCount);

        if (uploadSetDtoList.size() % limitCount > 0) {
            listCount++;
        }

        AtomicInteger atomicInteger = new AtomicInteger(0);

        //총 리스트 갯수 + 1 만큼 루프
        for(int i=0; i < listCount; i++) {
            int startList = atomicInteger.intValue();
            int endList = (limitCount + atomicInteger.intValue());

            //endList 값이 리스트 전체 갯수보다 크다면 마지막이므로 리스트 갯수로 변경
            if(uploadSetDtoList.size() < endList) {
                endList = uploadSetDtoList.size();
            }

            //엑셀 데이터를 범위별로 추출하여 조회
            List<String> uploadApplyList = uploadSetDtoList.subList(startList, endList)
                .stream()
                .map(b -> {
                    String rtnValue = "";
                    switch (scopeNm) {
                        case "DRCT": //절임배추
                        case "DRCT_DLV" : //명절선물
                        case "DRCT_NOR" : //일반상품
                        case "ITEM":
                            if (StringUtils.equals(promotionType, "CARD_DISCOUNT")) {
                                int itemNoLen = b.getItemNo().length();
                                if (!StringUtils.isEmpty(b.getItemNo())) {
                                    if (StringUtils.equals(storeType, "DS")) {
                                        rtnValue = b.getItemNo();
                                    } else { //HYPER, CLUB
                                        rtnValue = b.getItemNo().substring(4, itemNoLen);
                                    }
                                } else {
                                    rtnValue = b.getItemNo();
                                }
                            } else {
                                rtnValue = b.getItemNo();
                            }
                            break;
                        case "SELL":
                            rtnValue = b.getPartnerId();
                            break;
                    }
                    return rtnValue;
                })
                .collect(Collectors.toList());

            PromotionSchSetDto promotionSchSetDto = new PromotionSchSetDto();
            if(scopeNm.equals("PROMO")) {
                promotionSchSetDto.setSchType("promotionNo");
                promotionSchSetDto.setSchValue(uploadApplyList.stream().collect(Collectors.joining(",")));
            }

            ResponseObject<List<PromoUploadSetDto>> response = resourceClient.postForResponseObject(
                apiNm
                , (scopeNm.equals("PROMO") ? promotionSchSetDto : uploadApplyList)
                , apiUrl
                , new ParameterizedTypeReference<ResponseObject<List<PromoUploadSetDto>>>() {}
            );

            resultList.addAll(response.getData());

            atomicInteger.set(endList);
        }

        return resultList;
    }

    private boolean trimByApplyCd(String scopeNm, PromoUploadSetDto applyData) {
        boolean isData = false;

        if (!ObjectUtils.isEmpty(applyData)) {

            switch (scopeNm) {
                case "ITEM":
                    applyData.setItemNo(applyData.getItemNo().trim());
                    break;
                case "SELL":
                    applyData.setPartnerId(applyData.getPartnerId().trim());
                    break;
            }

            isData = true;
        }

        return isData;
    }

    /**
     * 적용, 제외정보 일괄 등록시 이름 입력
     * @param scopeNm
     * @param prodUploadSetDtoList
     * @return
     * @throws Exception
     */
    public List<PromoUploadSetDto> getUploadScopeNm(String promotionType, String storeType, String scopeNm, List<PromoUploadSetDto> prodUploadSetDtoList) throws Exception {
        List<PromoUploadSetDto> uploadGetDtoList = new ArrayList<>();

        String apiNm = null;
        String apiUrl = null;

        switch (scopeNm) {
            case "ITEM":
            case "DRCT_DLV": //절임배추
            case "DRCT" :    //명절선물
            case "DRCT_NOR" :    //일반상품
                apiNm = ResourceRouteName.MANAGE;
                apiUrl = "/promotion/upload/getItemInfoList";
                break;
            case "SELL":
                apiNm = ResourceRouteName.MANAGE;
                apiUrl = "/promotion/upload/getPartnerInfoList";
                break;
        }

        if(apiNm == null) {
            PromoUploadSetDto promoUploadSetDto = new PromoUploadSetDto();
            promoUploadSetDto.setError(scopeNm);
            uploadGetDtoList.add(0, promoUploadSetDto);
        } else {
            uploadGetDtoList.addAll(getUploadScopeNmApi(apiNm, apiUrl, promotionType, storeType, scopeNm, prodUploadSetDtoList));
        }

        return uploadGetDtoList;
    }

    /**
     * 덤 행사 상품정보 조회
     *
     * @param eventItemParamDto
     * @return
     */
    public List<EventItemExcelGetDto> getPromoItemInfoList(EventItemExcelParamDto eventItemParamDto) {
        String apiUri = "/promotion/event/getPromoItemInfoList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<EventItemExcelGetDto>>>() {};

        return (List<EventItemExcelGetDto>) resourceClient.postForResponseObject(ResourceRouteName.MANAGE, eventItemParamDto, apiUri, typeReference).getData();
    }
}
