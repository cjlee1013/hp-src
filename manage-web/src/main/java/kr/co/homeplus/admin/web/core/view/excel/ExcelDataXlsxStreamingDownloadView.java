package kr.co.homeplus.admin.web.core.view.excel;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * xlsx 파일을 streaming download 해주는 view class
 */
@Slf4j
public class ExcelDataXlsxStreamingDownloadView extends AbstractXlsxStreamingView {
    public static final String DEFAULT = "#";
    public static final String INTEGER = "0_ ";
    public static final String FLOAT = "0.00";
    public static final String THOUSANDS_INTEGER = "#,##0";
    public static final String THOUSANDS_FLOAT = "#,##0.00";
    public static final String ACCOUNTING_INTEGER = "$#,##0;($#,##0)";
    public static final String ACCOUNTING_RED_INTEGER = "$#,##0;($#,##0)";
    public static final String ACCOUNTING_FLOAT = "$#,##0;($#,##0)";
    public static final String ACCOUNTING_RED_FLOAT = "$#,##0;($#,##0)";
    public static final String PERCENT_INTEGER = "0%";
    public static final String PERCENT_FLOAT = "0.00%";
    public static final String EXPONENTIAL = "0.00E00";
    public static final String FRACTION_ONE_DIGIT = "?/?";
    public static final String FRACTION_TWO_DIGITS = "??/??";
    public static final String FORMAT1 = "#,##0;(#,##0)";
    public static final String FORMAT2 = "#,##0;(#,##0)";
    public static final String FORMAT3 = "#,##0.00;(#,##0.00)";
    public static final String FORMAT4 = "#,##0.00;(#,##0.00)";
    public static final String FORMAT5 = "#,##0;(#,##0)";
    public static final String FORMAT6 = "#,##0;(#,##0)";
    public static final String FORMAT7 = "#,##0.00;(#,##0.00)";
    public static final String FORMAT8 = "#,##0.00;(#,##0.00)";
    public static final String FORMAT9 = "#,##0.00;(#,##0.00)";
    public static final String FORMAT10 = "##0.0E0";
    public static final String TEXT = "@";

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        // controller 에서 넘겨준 model 에서 엑셀정보를 받는다.
        ExcelBindInfo courses = (ExcelBindInfo) model.get("excelBindInfo");

        if(courses == null) {
            throw new IllegalArgumentException("excelBindInfo key not found.");
        }

        String downloadFile = fileNameEncode(courses.getDownloadExcelFileName() + ".xlsx", request, response);

        response.setHeader("Content-Disposition", "attachment; filename=\"" + downloadFile + "\"");

        Sheet sheet = workbook.createSheet(courses.getSheetName());
        Row header = sheet.createRow(0);

        // 셀 header 정보 세팅. (테이블 컬럼 제목)
        int cellHeaderIdx = 0;
        for (String title : courses.getCellHeaders()) {
            header.createCell(cellHeaderIdx++).setCellValue(title);
        }

        // column 너비 설정
        Map<Integer, Integer> columnWidths = courses.getColumnWidths();
        Iterator i = columnWidths.entrySet().iterator();

        while(i.hasNext()) {
            Map.Entry<Integer, Integer> entry = (Map.Entry<Integer, Integer>)i.next();
            if (!ObjectUtils.isEmpty(entry.getKey()) && courses.getCellHeaders().size() > entry.getKey()
                    && !ObjectUtils.isEmpty(entry.getValue())  && entry.getValue() > 0) {
                sheet.setColumnWidth(entry.getKey(), entry.getValue());
            }
        }

        // cell data 세팅
        int rowIdx = 1;
        Map<Integer, CellType> cellTypes = courses.getCellTypes();
        for (List<String> rowData : courses.getRows()) {
            Row row = sheet.createRow(rowIdx++);

            int cellIdx = 0;
            for (String cellValue : rowData) {
                Cell cell = row.createCell(cellIdx);

                // cell data type 설정
                try {
                    if(!ObjectUtils.isEmpty(cellTypes.get(cellIdx))) {
                        if (cellTypes.get(cellIdx).equals(CellType.NUMERIC)) {
                            setCellTypeNumeric(cell, cellValue);
                            setCellStyle(workbook, cell, INTEGER);
                        }
                        else if (cellTypes.get(cellIdx).equals(CellType.STRING)) {
                            setCellTypeString(cell, cellValue);
                            setCellStyle(workbook, cell, TEXT);
                        }
                    }
                    else {
                        cell.setCellValue(cellValue);
                    }
                }
                catch (Exception e) {
                    log.warn("CellType Setting was failed, cellValue<{}>, cellIdx<{}> : {}", cellValue, cellIdx, ExceptionUtils.getStackTrace(e));
                    cell.setCellValue(cellValue);
                }
                cellIdx++;
            }
        }
    }

    /**
     * 다운로드할 파일명이 한글일 경우 브라우저마다 인코딩 처리를 해서 한글 깨짐을 방지한다.
     * @param fileName
     * @param req
     * @param res
     * @return
     * @throws UnsupportedEncodingException
     */
    private String fileNameEncode(String fileName, HttpServletRequest req, HttpServletResponse res) throws UnsupportedEncodingException {
        String userAgent = req.getHeader("User-Agent");

        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            return URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "\\ ");
        }
        else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            return URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "\\ ");
        }
        else if (userAgent.indexOf("Trident") > -1) { // MS IE 11
            return URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "\\ ");
        }
        else { // 모질라, 오페라, 파폭, 크롬 등
            return new String(fileName.getBytes("UTF-8"), "8859_1").replaceAll("\\+", "\\ ");
        }
    }

    /**
     * 셀 데이터타입을 String으로 설정한다.
     * @param cell
     * @param cellValue
     * @return
     * @throws
     */
    private void setCellTypeString(Cell cell, String cellValue) {
        cell.setCellType(CellType.STRING);
        cell.setCellValue(cellValue);
    }

    /**
     * 셀 데이터타입을 numeric으로 설정한다.
     * @param cell
     * @param cellValue
     * @return
     * @throws
     */
    private void setCellTypeNumeric(Cell cell, String cellValue) {
        if(cellValue.length() == 0) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(cellValue);
        }
        else if(NumberUtils.isNumber(cellValue)) {
            Double replaceValue = Double.parseDouble(cellValue.replace("" + ',', ""));
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(replaceValue);
        }
        else {
            cell.setCellValue(cellValue);
        }
    }

    /**
     * 셀 스타일을 포맷에 맞게 설정한다.(String)
     * @param workbook
     * @param cell
     * @return
     * @throws
     */
    private void setCellStyle(Workbook workbook, Cell cell, String dataFormat) {
        CellStyle cs = workbook.createCellStyle();
        DataFormat df = workbook.createDataFormat();
        cs.setDataFormat(df.getFormat(dataFormat));
        cell.setCellStyle(cs);
    }

    /**
     * 셀 스타일을 포맷에 맞게 설정한다.(Short)
     * @param workbook
     * @param cell
     * @return
     * @throws
     */
    private void setCellStyle(Workbook workbook, Cell cell, Short dataFormat) {
        CellStyle cs = workbook.createCellStyle();
        DataFormat df = workbook.createDataFormat();
        cs.setDataFormat(dataFormat);
        cell.setCellStyle(cs);
    }
}