package kr.co.homeplus.admin.web.promotion.model.event;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ParticipateHistorySet {
    @NotBlank(message = "이벤트 코드가 없습니다.")
    private String eventCode;

    @NotNull(message = "회원번호가 없습니다.")
    private Long userNo;
}
