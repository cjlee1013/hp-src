package kr.co.homeplus.admin.web.partner.model.market;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketCategoryMapValueSetDto {

    private String partnerId;

    private String marketCateCd;

    private String dcateCd;

    private String applyYn;

}


