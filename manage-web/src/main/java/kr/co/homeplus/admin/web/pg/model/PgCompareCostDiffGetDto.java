package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class PgCompareCostDiffGetDto {
    @ApiModelProperty(notes = "PG사", position = 1)
    @RealGridColumnInfo(headText = "PG사", sortable = true, width = 100)
    private String pgKind;

    @ApiModelProperty(notes = "MID", position = 2)
    @RealGridColumnInfo(headText = "MID", sortable = true, width = 100)
    private String pgMid;

    @ApiModelProperty(notes = "거래일", position = 3)
    @RealGridColumnInfo(headText = "거래일", sortable = true, width = 100)
    private String basicDt;

    @ApiModelProperty(notes = "지급일", position = 4)
    @RealGridColumnInfo(headText = "지급일", sortable = true, width = 100)
    private String payScheduleDt;

    @ApiModelProperty(notes = "PG연동주문번호", position = 5)
    @RealGridColumnInfo(headText = "PG연동주문번호", sortable = true, width = 100)
    private String pgOid;

    @ApiModelProperty(notes = "TID", position = 6)
    @RealGridColumnInfo(headText = "TID", sortable = true, width = 100)
    private String pgTid;

    @ApiModelProperty(notes = "차이구분", position = 8)
    @RealGridColumnInfo(headText = "차이구분", sortable = true, width = 100)
    private String flag;

    @ApiModelProperty(notes = "홈플러스", position = 9)
    @RealGridColumnInfo(headText = "홈플러스", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long hmpAmt;

    @ApiModelProperty(notes = "PG", position = 10)
    @RealGridColumnInfo(headText = "PG", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long pgAmt;

    @ApiModelProperty(notes = "차액", position = 11)
    @RealGridColumnInfo(headText = "차액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private long diffAmt;
}
