package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배사코드관리 연동업체코드 리스트 응답 DTO")
public class ShipLinkCompanyCodeListGetDto {
    @ApiModelProperty(notes = "연동업체")
    private String dlvLinkCompany;

    @ApiModelProperty(notes = "연동업체코드")
    private String linkCompanyCd;

    @ApiModelProperty(notes = "연동업체코드명")
    private String linkCompanyCdNm;
}