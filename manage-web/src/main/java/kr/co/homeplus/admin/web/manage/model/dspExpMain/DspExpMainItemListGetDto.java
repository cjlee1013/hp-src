package kr.co.homeplus.admin.web.manage.model.dspExpMain;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true)
@Getter
@Setter
public class DspExpMainItemListGetDto {

    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 170, order = 1)
    private String itemNm;

    @RealGridColumnInfo(headText = "전시여부", width = 50, order = 2)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 3)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 4, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 5)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 6, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "itemSeq", order = 7, hidden = true)
    private Long itemSeq;
    @RealGridColumnInfo(headText = "dispId", order = 8, hidden = true)
    private Long dispId;
    @RealGridColumnInfo(headText = "priority", order = 9, hidden = true)
    private int priority;
    @RealGridColumnInfo(headText = "dispYn", order = 10, hidden = true)
    private String dispYn;
    @RealGridColumnInfo(headText = "regId", order = 11, hidden = true)
    private String regId;
    @RealGridColumnInfo(headText = "chgId", order = 12, hidden = true)
    private String chgId;

}
