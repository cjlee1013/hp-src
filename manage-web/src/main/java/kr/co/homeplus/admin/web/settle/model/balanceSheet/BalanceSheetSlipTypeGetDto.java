package kr.co.homeplus.admin.web.settle.model.balanceSheet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "전표조회 유형")
public class BalanceSheetSlipTypeGetDto {
  @ApiModelProperty(notes = "전표유형")
  private String slipType;

  @ApiModelProperty(notes = "코드명")
  private String typeName;

}
