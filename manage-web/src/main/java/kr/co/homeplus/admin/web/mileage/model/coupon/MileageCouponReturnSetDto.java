package kr.co.homeplus.admin.web.mileage.model.coupon;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 쿠폰 회수")
public class MileageCouponReturnSetDto {
    private long couponManageNo;
    private String mileageTypeNo;
    private String couponType;
    private long mileageAmt;
    private String requestId;
    private List<MileageCouponUserInfoDto> infoList;
}
