package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.admin.web.statistics.service.StorePeriodEKPIService;
import kr.co.homeplus.admin.web.statistics.model.storePeriodEKPI.StorePeriodListGetDto;
import kr.co.homeplus.admin.web.statistics.model.storePeriodEKPI.StorePeriodListSetDto;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/storePeriodEKPI")
public class StorePeriodEKPIController {

    private final StorePeriodEKPIService settleService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public StorePeriodEKPIController(StorePeriodEKPIService settleService, SettleCommonService settleCommonService, CodeService codeService) {
        this.settleService = settleService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 통계 > 상품주문통계 > 기간별 점포 매출 (E-KPI)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/storePeriodMain", method = RequestMethod.GET)
    public String storePeriodMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mallType", "TD");
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.createForGroup("storePeriodListBaseInfo", StorePeriodListGetDto.class));

        return "/statistics/storePeriodEKPIMain";
    }

    /**
     * 통계 > 상품주문통계 > 기간별 판매업체 매출 (E-KPI)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/DSPeriodMain", method = RequestMethod.GET)
    public String DSPeriodMain(Model model) throws Exception {
        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "store_type"              // 점포유형
        );
        model.addAttribute("storeType", code.get("store_type"));
        model.addAttribute("mallType", "DS");
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2021));
        //월 조회
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.createForGroup("storePeriodListBaseInfo", StorePeriodListGetDto.class));

        return "/statistics/storePeriodEKPIMain";
    }

    /**
     * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
     */
    @ResponseBody
    @RequestMapping(value = "/getStoreList.json", method = RequestMethod.GET)
    public List<StorePeriodListGetDto> getStoreList(StorePeriodListSetDto listParamDto) throws Exception {
        return settleService.getStoreList(listParamDto);
    }
}