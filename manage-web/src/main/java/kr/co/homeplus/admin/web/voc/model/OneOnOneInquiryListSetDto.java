package kr.co.homeplus.admin.web.voc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 1:1문의 리스트 요청 DTO")
public class OneOnOneInquiryListSetDto {
  @ApiModelProperty(notes = "문의종류")
  private String schInqryKind;

  @ApiModelProperty(notes = "검색조건")
  private String schKeywordType;

  @ApiModelProperty(notes = "검색어")
  private String schKeyword;

  @ApiModelProperty(notes = "사이트")
  private String schSiteType;

  @ApiModelProperty(notes = "처리상태")
  private String schInqryStatus;

  @ApiModelProperty(notes = "점포유형")
  private String schStoreType;

  @ApiModelProperty(notes = "점포코드/판매자ID")
  private String schStoreId;

  @ApiModelProperty(notes = "회원번호")
  private String schUserNo;

  @ApiModelProperty(notes = "구분")
  private String schInqryCategory;

  @ApiModelProperty(notes = "문의구분")
  private String schInqryType;

  @ApiModelProperty(notes = "문의항목")
  private String schInqryDetailType;

  @ApiModelProperty(notes = "검색일자타입")
  private String schDtType;

  @ApiModelProperty(notes = "시작일")
  private String schStartDt;

  @ApiModelProperty(notes = "종료일")
  private String schEndDt;

  @ApiModelProperty(notes = "SO권한여부")
  private String storeOfficeYn;
}