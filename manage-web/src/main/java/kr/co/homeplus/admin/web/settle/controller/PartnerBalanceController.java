package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.partnerBalance.PartnerBalanceListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerBalance.PartnerBalanceListSetDto;
import kr.co.homeplus.admin.web.settle.service.PartnerBalanceService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/partnerBalance")
public class PartnerBalanceController {

    @Autowired
    private PartnerBalanceService partnerBalanceService;
    @Autowired
    private SettleCommonService settleCommonService;

    /**
     * 파트너 잔액 조회 Main Page
     */
    @RequestMapping(value = "/partnerBalanceMain", method = RequestMethod.GET)
    public String partnerBalanceMain(Model model) throws Exception{
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getMonth", settleCommonService.getDateCalculate("M", null, null, 0));
        model.addAllAttributes(RealGridHelper.createForGroup("partnerBalanceGridBaseInfo", PartnerBalanceListGetDto.class));

        return "/settle/partnerBalanceMain";
    }

    /**
     * 파트너 잔액 조회 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerBalanceList.json", method = RequestMethod.GET)
    public List<PartnerBalanceListGetDto> getPartnerBalanceList(PartnerBalanceListSetDto partnerBalanceListSetDto) throws Exception{
        return partnerBalanceService.getPartnerBalanceList(partnerBalanceListSetDto);
    }

}