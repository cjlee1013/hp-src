package kr.co.homeplus.admin.web.item.model.item.hist;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class ItemDetailHistGetDto {

    @RealGridColumnInfo(headText = "변경일시", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true, width = 120)
    private String itemHistDt;

    @RealGridColumnInfo(headText = "판매기간-시작시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String saleStartDt = "-";

    @RealGridColumnInfo(headText = "판매기간-종료시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String saleEndDt = "-";

    @RealGridColumnInfo(headText = "예약판매부가설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String rsvPurchaseLimitYn = "-";

    @RealGridColumnInfo(headText = "온라인재고설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String onlineStockYn = "-";

    @RealGridColumnInfo(headText = "온라인재고설정기간-시작시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String onlineStockStartDt = "-";

    @RealGridColumnInfo(headText = "온라인재고설정기간-종료시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String onlineStockEndDt = "-";

    @RealGridColumnInfo(headText = "판매단위수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String saleUnit = "-";

    @RealGridColumnInfo(headText = "최소구매수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String purchaseMinQty = "-";

    @RealGridColumnInfo(headText = "구매수량제한여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String purchaseLimitYn = "-";

    @RealGridColumnInfo(headText = "구매수량제한-일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String purchaseLimitDay = "-";

    @RealGridColumnInfo(headText = "구매수량제한-개수", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
    private String purchaseLimitQty = "-";

    @RealGridColumnInfo(headText = "판매기간-시작시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgSaleStartDt = "-";

    @RealGridColumnInfo(headText = "판매기간-종료시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgSaleEndDt = "-";

    @RealGridColumnInfo(headText = "예약판매부가설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgRsvPurchaseLimitYn = "-";

    @RealGridColumnInfo(headText = "온라인재고설정", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgOnlineStockYn = "-";

    @RealGridColumnInfo(headText = "온라인재고설정기간-시작시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgOnlineStockStartDt = "-";

    @RealGridColumnInfo(headText = "온라인재고설정기간-종료시간", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgOnlineStockEndDt = "-";

    @RealGridColumnInfo(headText = "판매단위수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgSaleUnit = "-";

    @RealGridColumnInfo(headText = "최소구매수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgPurchaseMinQty = "-";

    @RealGridColumnInfo(headText = "구매수량제한여부", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgPurchaseLimitYn = "-";

    @RealGridColumnInfo(headText = "구매수량제한-일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgPurchaseLimitDay = "-";

    @RealGridColumnInfo(headText = "구매수량제한-개수", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgPurchaseLimitQty = "-";

    @RealGridColumnInfo(headText = "수정자", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String chgNm;
}
