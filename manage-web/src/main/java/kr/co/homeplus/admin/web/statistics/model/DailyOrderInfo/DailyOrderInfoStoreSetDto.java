package kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailyOrderInfoStoreSetDto {
  @ApiModelProperty(notes = "점포코드")
  private String storeId;
  @ApiModelProperty(notes = "점포유형")
  private String storeType;
  @ApiModelProperty(notes = "시작일")
  private String startDt;
  @ApiModelProperty(notes = "종료일")
  private String endDt;
  @ApiModelProperty(notes = "마켓")
  private String marketType;
}
