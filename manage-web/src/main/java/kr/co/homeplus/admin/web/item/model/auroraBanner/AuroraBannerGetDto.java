package kr.co.homeplus.admin.web.item.model.auroraBanner;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class AuroraBannerGetDto {

    @RealGridColumnInfo(headText = "새벽배송 ID", width = 50, sortable = true)
    private Long bannerId;

    @RealGridColumnInfo(headText = "구분", width = 70, order = 1)
    private String dispKind;

    @RealGridColumnInfo(headText = "새벽배송 배너명", width = 150, order = 2)
    private String bannerNm;

    @RealGridColumnInfo(headText = "전시시작일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 120, order = 3)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 120, order = 4)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 5)
    private String useYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 6)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, width = 150, order = 7)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 8)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 150, order = 9)
    private String chgDt;

    @RealGridColumnInfo(headText = "bannerType", order = 10, hidden = true)
    private String bannerType;
    @RealGridColumnInfo(headText = "template", order = 11, hidden = true)
    private String template;
    @RealGridColumnInfo(headText = "useYn", order = 12, hidden = true)
    private String useYn;

    private String regId;
    private String chgId;

}
