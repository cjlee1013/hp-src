package kr.co.homeplus.admin.web.item.model.harmful;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminHarmfulItemListSelectDto {


    @RealGridColumnInfo(headText = "유형", width = 200 )
    private String nmRpttype;

    @RealGridColumnInfo(headText = "검사기관", width = 200 )
    private String instituteTxt;

    @RealGridColumnInfo(headText = "제품명", width = 200 )
    private String nmProd;

    @RealGridColumnInfo(headText = "제조업소", width = 200 )
    private String nmManufacupso;

    @RealGridColumnInfo(headText = "판매업소", width = 200 )
    private String nmSalerupso;

    @RealGridColumnInfo(headText = "등록일시", width = 200 )
    private String recvDt;


}
