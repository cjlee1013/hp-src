package kr.co.homeplus.admin.web.delivery.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.BsdLinkTranSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BsdLinkService {

  private final ResourceClient resourceClient;
  private final ShipCommonService shipCommonService;

  /**
   * BSD연동조회 리스트 조회
   */
  public List<BsdLinkListGetDto> getBsdLinkList(BsdLinkListSetDto bsdLinkListSetDto) {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        bsdLinkListSetDto,
        "/admin/shipManage/getBsdLinkList",
        new ParameterizedTypeReference<ResponseObject<List<BsdLinkListGetDto>>>(){}).getData();
  }

  /**
   * BSD전송
   */
  public ResponseObject<Object> setBsdLinkTran(BsdLinkTranSetDto bsdLinkTranSetDto) throws Exception {
    return resourceClient.postForResponseObject(
        ResourceRouteName.SHIPPING,
        bsdLinkTranSetDto,
        "/admin/shipManage/setBsdLinkTran",
        new ParameterizedTypeReference<ResponseObject<Object>>(){});
  }
}
