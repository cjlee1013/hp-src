package kr.co.homeplus.admin.web.core.config;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lombok.extern.slf4j.Slf4j;

/**
 * web.xml 내 세션관련 설정을 대체하기 위한 Listener<p>
 *
 * web.xml내의 session-config > session-timeout의 설정을 대체한다<p>
 *
 * session-timeout 기본설정은 '1시간'이고 {@link CommonConfig}에서<br>
 * 생성자를 통해 properties에 설정을 읽어들여 세션시간을 설정한다.
 *
 * @see HttpSessionListener
 * @see CommonConfig
 */
@Slf4j
public class SessionListener implements HttpSessionListener {
    /**
     * web.xml의 session-timeout을 설정값을 대체<p>
     *
     * 기본시간은 1시간(60분 = 60 * 60 = 3600초)
     */
    private int sessionTimeOut = 3600;

    public SessionListener() {
    }

    public SessionListener(int sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setMaxInactiveInterval(sessionTimeOut);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    }
}