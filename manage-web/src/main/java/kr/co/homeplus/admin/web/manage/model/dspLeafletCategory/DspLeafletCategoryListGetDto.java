package kr.co.homeplus.admin.web.manage.model.dspLeafletCategory;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class DspLeafletCategoryListGetDto {

    @RealGridColumnInfo(headText = "번호", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, sortable = true)
    private Long cateNo;

    @RealGridColumnInfo(headText = "카테고리명", width = 200, order = 1, sortable = true)
    private String cateNm;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 2, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "사용여부", order = 3, hidden = true)
    private String useYn;

    @RealGridColumnInfo(headText = "사용여부", width = 50, order = 4, sortable = true)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 5, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 6, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 7, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 8, sortable = true)
    private String chgDt;

    @RealGridColumnInfo(headText = "pcImg", order = 9, hidden = true, sortable = true)
    private String pcImg;
    @RealGridColumnInfo(headText = "mobileImg", order = 10, hidden = true, sortable = true)
    private String mobileImg;


    private int pcImgWidth;
    private int pcImgHeight;
    private int mobileImgWidth;
    private int mobileImgHeight;
    private String regId;
    private String chgId;

}
