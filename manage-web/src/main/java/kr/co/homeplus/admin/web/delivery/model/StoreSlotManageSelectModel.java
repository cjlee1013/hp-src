package kr.co.homeplus.admin.web.delivery.model;

import lombok.Data;

@Data
public class StoreSlotManageSelectModel {

    private String schStartDt;

    private String schEndDt;

    private String schStoreType;

    private int schStoreId;

    private String schUseYn;
}
