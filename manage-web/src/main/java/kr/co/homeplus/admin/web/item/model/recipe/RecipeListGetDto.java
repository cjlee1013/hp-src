package kr.co.homeplus.admin.web.item.model.recipe;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class RecipeListGetDto {

    @RealGridColumnInfo(headText = "레시피 번호", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true)
    private Long recipeNo;

    @RealGridColumnInfo(headText = "레시피명", width = 200, order = 1)
    private String title;

    @RealGridColumnInfo(headText = "노출여부", order = 2, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", order = 3)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 4, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 5)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 6, sortable = true)
    private String chgDt;

}