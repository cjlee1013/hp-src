package kr.co.homeplus.admin.web.item.model.harmful;

import lombok.Data;

@Data
public class HarmfulItemDetailParamDto {
    private String cdInsptmachi;

    private String noDocument;

    private String seq;

}
