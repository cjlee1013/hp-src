package kr.co.homeplus.admin.web.manage.model.dspCacheManage;

import lombok.Data;

@Data
public class DspCacheManageListParamDto {
    private String searchCacheNm;
    private String searchUseYn;
}
