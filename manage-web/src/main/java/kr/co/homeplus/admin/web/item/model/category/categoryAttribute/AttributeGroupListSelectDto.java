package kr.co.homeplus.admin.web.item.model.category.categoryAttribute;


import lombok.Data;

@Data
public class AttributeGroupListSelectDto {


    private Long gattrNo;

    private String gattrMngNm;

}
