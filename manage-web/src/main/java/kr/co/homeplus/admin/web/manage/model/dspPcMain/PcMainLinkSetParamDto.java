package kr.co.homeplus.admin.web.manage.model.dspPcMain;

import lombok.Data;

@Data
public class PcMainLinkSetParamDto {

    private Integer linkNo;

    private String linkType;

    private String linkInfo;

    private String linkOptions;

    private String textYn;

    private String title;

    private String subTitle;

    private String dlvYn;

    private int dlvStoreId;

    private String imgUrl;

    private int imgWidth;

    private int imgHeight;

    private String useYn;


}
