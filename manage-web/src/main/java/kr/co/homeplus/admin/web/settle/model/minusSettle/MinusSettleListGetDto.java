package kr.co.homeplus.admin.web.settle.model.minusSettle;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridButtonType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MinusSettleListGetDto {
    @ApiModelProperty(value = "수기마감")
    @RealGridColumnInfo(headText = "수기마감", width = 150, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BUTTON, alwaysShowButton = true, buttonType = RealGridButtonType.ACTION)
    private String actionType;

    @ApiModelProperty(value = "정산상태")
    @RealGridColumnInfo(headText = "정산상태", width = 100, sortable = true)
    private String settleState;

    @ApiModelProperty(value = "지급SRL")
    @RealGridColumnInfo(headText = "지급SRL", width = 100, sortable = true)
    private String settleSrl;

    @ApiModelProperty(value = "지급예정일")
    @RealGridColumnInfo(headText = "지급예정일", width = 100, sortable = true)
    private String settlePreDt;

    @ApiModelProperty(value = "지급방법")
    @RealGridColumnInfo(headText = "지급방법", width = 100, sortable = true)
    private String settlePeriodCd;

    @ApiModelProperty(value = "정산주기")
    @RealGridColumnInfo(headText = "정산주기", width = 100, sortable = true)
    private String settleCycleType;

    @ApiModelProperty(value = "집계기간")
    @RealGridColumnInfo(headText = "집계기간", width = 180, sortable = true)
    private String settleStartEndDt;
    
    @ApiModelProperty(value = "판매업체ID")
    @RealGridColumnInfo(headText = "판매업체ID", width = 150, sortable = true)
    private String partnerId;

    @ApiModelProperty(value = "업체코드")
    @RealGridColumnInfo(headText = "업체코드", width = 120, sortable = true)
    private String vendorCd;

    @ApiModelProperty(value = "판매업체명")
    @RealGridColumnInfo(headText = "판매업체명", width = 180, sortable = true)
    private String partnerNm;

    @ApiModelProperty(value = "상호명")
    @RealGridColumnInfo(headText = "상호명", width = 180, sortable = true)
    private String businessNm;

    @ApiModelProperty(value = "매출집계금액")
    @RealGridColumnInfo(headText = "매출집계금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String saleSumAmt;

    @ApiModelProperty(value = "배송비")
    @RealGridColumnInfo(headText = "배송비", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String shipAmt;

    @ApiModelProperty(value = "반품배송비")
    @RealGridColumnInfo(headText = "반품배송비", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String claimReturnShipAmt;

    @ApiModelProperty(value = "판매수수료(EX VAT)")
    @RealGridColumnInfo(headText = "판매수수료(EX VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String saleFeeExVat;

    @ApiModelProperty(value = "판매수수료(VAT)")
    @RealGridColumnInfo(headText = "판매수수료(VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String saleFeeVat;

    @ApiModelProperty(value = "판매수수료(IN VAT)")
    @RealGridColumnInfo(headText = "판매수수료(IN VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String saleFeeInVat;

    @ApiModelProperty(value = "정산(IN VAT)")
    @RealGridColumnInfo(headText = "정산(IN VAT)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleInVatAmt;

    @ApiModelProperty(value = "자사할인")
    @RealGridColumnInfo(headText = "자사할인", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String homeChargeAmt;

    @ApiModelProperty(value = "상품할인(업체)")
    @RealGridColumnInfo(headText = "상품할인(업체)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String couponSellerChargeAmt;

    @ApiModelProperty(value = "배송비할인(업체)")
    @RealGridColumnInfo(headText = "배송비할인(업체)", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String shipDiscountAmt;

    @ApiModelProperty(value = "조정금액")
    @RealGridColumnInfo(headText = "조정금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String taxAdjustAmt;

    @ApiModelProperty(value = "조정수수료")
    @RealGridColumnInfo(headText = "조정수수료", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String adjustFee;

    @ApiModelProperty(value = "정산대상금액")
    @RealGridColumnInfo(headText = "정산대상금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleAmt;

    @ApiModelProperty(value = "정산예정금액")
    @RealGridColumnInfo(headText = "정산예정금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settlePreAmt;

    @ApiModelProperty(value = "이전보류금액")
    @RealGridColumnInfo(headText = "이전보류금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String holdAmt;

    @ApiModelProperty(value = "지급예정금액")
    @RealGridColumnInfo(headText = "지급예정금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String settleGivePreAmt;
    
    @ApiModelProperty(value = "보류발생금액")
    @RealGridColumnInfo(headText = "보류발생금액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String holdOccurAmt;

    @ApiModelProperty(value = "실지급액")
    @RealGridColumnInfo(headText = "실지급액", width = 150, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String realAmt;

    @ApiModelProperty(value = "변경일시")
    @RealGridColumnInfo(headText = "변경일시", width = 200, sortable = true)
    private String chgDt;

    @ApiModelProperty(value = "변경자")
    @RealGridColumnInfo(headText = "변경자", width = 150, sortable = true)
    private String chgId;
}
