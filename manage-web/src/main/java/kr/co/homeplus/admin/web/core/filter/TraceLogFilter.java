package kr.co.homeplus.admin.web.core.filter;

import static kr.co.homeplus.plus.util.WebCookieUtils.getCookie;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import kr.co.homeplus.plus.crypto.exception.CryptoClientException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

@Slf4j
public class TraceLogFilter implements Filter {

	private final RefitCryptoService refitCryptoService;

	public TraceLogFilter(final RefitCryptoService refitCryptoService) {
		this.refitCryptoService = refitCryptoService;
	}

	/**
	 * Logger 에 로그인 사용자 정보를 삽입 처리한다.
	 *
	 * @param request ServletRequest
	 * @param response ServletResponse
	 * @param chain FilterChain
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response,
		final FilterChain chain) throws IOException, ServletException {

		UserInfo userInfo = getLoginInfo();
		String userCode = "";
		String userDepartmentCode = "";
		if (userInfo != null) {
			userCode = userInfo.getEmpId();
			userDepartmentCode = userInfo.getDeptCd();
		}

		// empNo, deptNo, clientIp
		String metaLog = String.format("[%s;%s;%s]", userCode, userDepartmentCode, getClientIp(request));

		try {
			MDC.put("homeplusHeader", metaLog);
			chain.doFilter(request, response);
		}
		finally {
			MDC.clear();
		}

	}

	/**
	 * 로그인 쿠키로 부터 사용자 정보 객체를 생성하고 반환합니다.
	 *
	 * @return 로그인 쿠키가 존재할 경우 {@link UserInfo} 형태의 사용자 정보객체를 반환하고, 아닐 경우 null을 반환합니다.
	 */
	private UserInfo getLoginInfo() {
		UserInfo userInfo = null;
		Cookie cookie = Optional.ofNullable(getCookie(AdminConstants.ADMIN_LOGIN_COOKIE_NAME))
			.orElse(null);

		String decryptUserInfo;
		if (cookie != null && StringUtils.isNotEmpty(cookie.getValue())) {
			final String cookieValue = cookie.getValue();

			try {
				decryptUserInfo = refitCryptoService.decrypt(cookieValue);
				userInfo = new Gson().fromJson(decryptUserInfo, UserInfo.class);
			} catch (CryptoClientException | JsonSyntaxException e) {
				log.warn("An error occurred while decrypting a loginCookie. origin:<{}>", cookieValue);
			}
		}
		return userInfo;
	}

	/**
	 * request 를 전송한 클라이언트의 IP 주소를 반환한다.
	 *
	 * @param request ServletRequest
	 * @return 클라이언트 아이피
	 */
	private String getClientIp(ServletRequest request) {
		String ip = ((HttpServletRequest) request).getHeader("X-FORWARDED-FOR");
		if (StringUtils.isEmpty(ip)) {
			ip = ((HttpServletRequest) request).getHeader("Proxy-Client-IP");
		}

		if (StringUtils.isEmpty(ip)) {
			ip = ((HttpServletRequest) request).getHeader("WL-Proxy-Client-IP");  // 웹로직일 경우
		}

		if (StringUtils.isEmpty(ip)) {
			ip = request.getRemoteAddr();
		}

		return ip;
	}
}
