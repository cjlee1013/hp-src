package kr.co.homeplus.admin.web.delivery.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 점포배송정보 > 점포별 휴일 조회 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class StoreCloseDayModel {
    @ApiModelProperty(notes = "등록번호", position = 1)
    @RealGridColumnInfo(headText = "등록번호", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String closeDaySeq;

    @ApiModelProperty(notes = "점포유형(HYPER,EXP,CLUB)", position = 2)
    @RealGridColumnInfo(headText = "점포유형", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String storeType;

    @ApiModelProperty(notes = "휴무유형(F:자율휴무,R:정기휴무,S:설,C:추석,E:기타)", position = 3)
    @RealGridColumnInfo(headText = "휴무유형", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String closeDayType;

    @ApiModelProperty(notes = "등록제목", position = 4)
    @RealGridColumnInfo(headText = "등록제목", width = 300, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String titleNm;

    @ApiModelProperty(notes = "진행기간", position = 5)
    @RealGridColumnInfo(headText = "진행기간", sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATE, renderFormat = "yyyy-MM-dd")
    private String closeDt;

    @ApiModelProperty(notes = "허용점포개수", position = 6)
    @RealGridColumnInfo(headText = "허용점포개수", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyStoreCnt;

    @ApiModelProperty(notes = "허용점포명", position = 7)
    @RealGridColumnInfo(headText = "허용점포명", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyStoreNm;

    @ApiModelProperty(notes = "진행상태(Y:정상,N:취소)", position = 8)
    @RealGridColumnInfo(headText = "진행상태", width = 50, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;

    @ApiModelProperty(notes = "등록일", position = 9)
    @RealGridColumnInfo(headText = "등록일", width = 150, sortable = true, fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME)
    private String regDt;

    @ApiModelProperty(notes = "등록자", position = 10)
    @RealGridColumnInfo(headText = "등록자", width = 80, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;
}