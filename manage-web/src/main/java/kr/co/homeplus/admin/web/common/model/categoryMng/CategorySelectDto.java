package kr.co.homeplus.admin.web.common.model.categoryMng;

import lombok.Data;

@Data
public class CategorySelectDto {
    private String lcateCd;
    private String mcateCd;
    private String scateCd;
    private String dcateCd;
}
