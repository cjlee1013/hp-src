package kr.co.homeplus.admin.web.common.model.pop;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 회원조회 팝업 파라미터
 */
@Setter
@Getter
@ToString
public class MemberSearchParam {
    /** 회원번호 **/
    private Long userNo;

    /** 회원명 **/
    private String userNm;

    /** 회원아이디 **/
    private String userId;

    /** 모바일 **/
    private String mobile;
    /** 회원아이디, 회원명 마스킹 해제된 원본값 전달여부 **/
    @JsonProperty(value = "isIncludeUnmasking")
    private boolean isIncludeUnmasking;
}
