package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > 택배배송관리 리스트 응답 DTO")
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class ShipManageListGetDto {
    @ApiModelProperty(notes = "몰유형")
    @RealGridColumnInfo(headText = "몰유형", width = 150, sortable = true, hidden = true)
    private String mallType;

    @ApiModelProperty(notes = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String storeType;

    @ApiModelProperty(notes = "판매업체")
    @RealGridColumnInfo(headText = "판매업체", width = 120, sortable = true)
    private String storeNm;

    @ApiModelProperty(notes = "주문번호")
    @RealGridColumnInfo(headText = "주문번호", width = 100, sortable = true)
    private String purchaseOrderNo;

    @ApiModelProperty(notes = "배송번호")
    @RealGridColumnInfo(headText = "배송번호", width = 100, sortable = true)
    private String bundleNo;

    @ApiModelProperty(notes = "상품주문번호")
    @RealGridColumnInfo(headText = "상품주문번호", width = 100, sortable = true)
    private String orderItemNo;

    @ApiModelProperty(notes = "주문상태")
    @RealGridColumnInfo(headText = "주문상태", width = 100, sortable = true)
    private String shipStatusNm;

    @ApiModelProperty(notes = "주문상태코드")
    @RealGridColumnInfo(headText = "주문상태코드", width = 100, sortable = true, hidden = true)
    private String shipStatus;

    @ApiModelProperty(notes = "주문일시")
    @RealGridColumnInfo(headText = "주문일시", width = 150, sortable = true)
    private String orderDt;

    @ApiModelProperty(notes = "배송예정일")
    @RealGridColumnInfo(headText = "배송예정일", width = 100, sortable = true)
    private String scheduleShipDt;

    @ApiModelProperty(notes = "발송기한")
    @RealGridColumnInfo(headText = "발송기한", width = 100, sortable = true)
    private String orgShipDt;

    @ApiModelProperty(notes = "2차발송기한")
    @RealGridColumnInfo(headText = "2차발송기한", width = 100, sortable = true)
    private String delayShipDt;

    @ApiModelProperty(notes = "배송방법")
    @RealGridColumnInfo(headText = "배송방법", width = 90, sortable = true)
    private String shipMethodNm;

    @ApiModelProperty(notes = "배송방법코드")
    @RealGridColumnInfo(headText = "배송방법코드", width = 150, sortable = true, hidden = true)
    private String shipMethod;

    @ApiModelProperty(notes = "택배사")
    @RealGridColumnInfo(headText = "택배사", width = 100, sortable = true)
    private String dlvCdNm;

    @ApiModelProperty(notes = "택배사코드")
    @RealGridColumnInfo(headText = "택배사코드", width = 150, sortable = true, hidden = true)
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    @RealGridColumnInfo(headText = "송장번호", width = 120, sortable = true)
    private String invoiceNo;

    @ApiModelProperty(notes = "상품번호")
    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @ApiModelProperty(notes = "상품명")
    @RealGridColumnInfo(headText = "상품명",  width = 300, columnType = RealGridColumnType.NONE, sortable = true)
    private String itemNm1;

    @ApiModelProperty(notes = "옵션")
    @RealGridColumnInfo(headText = "옵션", width = 150, columnType = RealGridColumnType.NONE, sortable = true)
    private String txtOptVal;

    @ApiModelProperty(notes = "수량")
    @RealGridColumnInfo(headText = "수량", width = 80, sortable = true)
    private String itemQty;

    @ApiModelProperty(notes = "구매자")
    @RealGridColumnInfo(headText = "구매자", width = 80, sortable = true)
    private String buyerNm;

    @ApiModelProperty(notes = "구매자 연락처")
    @RealGridColumnInfo(headText = "구매자 연락처", width = 100, sortable = true)
    private String buyerMobileNo;

    @ApiModelProperty(notes = "법인")
    @RealGridColumnInfo(headText = "법인", width = 60, sortable = true)
    private String corpOrder;

    @ApiModelProperty(notes = "수령인")
    @RealGridColumnInfo(headText = "수령인", width = 80, sortable = true)
    private String receiverNm;

    @ApiModelProperty(notes = "수령인 연락처")
    @RealGridColumnInfo(headText = "수령인 연락처", width = 100, sortable = true)
    private String shipMobileNo;

    @ApiModelProperty(notes = "우편번호")
    @RealGridColumnInfo(headText = "우편번호", width = 80, sortable = true)
    private String zipcode;

    @ApiModelProperty(notes = "주소")
    @RealGridColumnInfo(headText = "주소", width = 350, columnType = RealGridColumnType.NONE, sortable = true)
    private String addr;

    @ApiModelProperty(notes = "배송메세지")
    @RealGridColumnInfo(headText = "배송메세지", width = 200, columnType = RealGridColumnType.NONE, sortable = true)
    private String dlvShipMsg;

    @ApiModelProperty(notes = "결제일시")
    @RealGridColumnInfo(headText = "결제일시", width = 150, sortable = true)
    private String paymentFshDt;

    @ApiModelProperty(notes = "사이트유형")
    @RealGridColumnInfo(headText = "사이트유형", width = 150, sortable = true, hidden = true)
    private String siteType;

    @ApiModelProperty(notes = "파트너ID")
    @RealGridColumnInfo(headText = "파트너ID", width = 150, sortable = true, hidden = true)
    private String partnerId;
}