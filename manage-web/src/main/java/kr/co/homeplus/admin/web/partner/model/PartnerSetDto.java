package kr.co.homeplus.admin.web.partner.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 * 상품 기본정보
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartnerSetDto {

	/**
	 * 기본정보
	 */
	private String partnerId;
	private String partnerType;
	private String operatorType;
	private String partnerGrade;
	private String partnerStatus;
	private String userId;
	private String isMod;

	/**
	 * 사업자정보
	 */
	private String partnerNm;
	private String partnerOwner;
	private String partnerNo;
	private String communityNotiNo;
	private String businessConditions;
	private String bizCateCd;
	private String zipcode;
	private String addr1;
	private String addr2;

	/**
	 * 판매자정보
	 */
	private PartnerSellerSetDto sellerInfo;

	/**
	 * 반품택배정보
	 */
	private List<PartnerSellerDeliveryGetDto> deliveryList = new ArrayList<>();

	/**
	 * 정산정보
	 */
	private PartnerSettleSetDto settleInfo;

	/**
	 * 담당자정보
	 */
	private List<PartnerManagerSetDto> managerList = new ArrayList<>();

	/**
	 * 2단계 로그인 담당자정보
	 */
	private List<CertificationManagerSetDto> certificationManagerList = new ArrayList<>();

	/**
	 * 판매상품유형 등 ( kind, type 등의 정보 )
	 */
	private List<PartnerTypesSetDto> typesList = new ArrayList<>();

	/**
	 * 파일정보
	 */
	private List<PartnerFileListGetDto> partnerFileList;
}
