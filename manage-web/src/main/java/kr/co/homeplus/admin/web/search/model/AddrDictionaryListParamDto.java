package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "주소사전 > 어휘 조회")
public class AddrDictionaryListParamDto {
    @ApiModelProperty(notes = "어휘명", position = 1, required = true)
    private String wordName;
    @ApiModelProperty(notes = "어휘포함(Y|N)", position = 2, required = true)
    private char includeNoun;
    @ApiModelProperty(notes = "offset(default=0)", position = 2)
    private int offset = 0;
    @ApiModelProperty(notes = "limit(default=100)", position = 3)
    private int limit = 100;
}
