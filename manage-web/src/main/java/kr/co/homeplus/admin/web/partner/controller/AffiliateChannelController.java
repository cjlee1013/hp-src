package kr.co.homeplus.admin.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateChannelHistGetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.AffiliateChannelHistGridGetDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.ChannelGroupListSelectDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.ChannelListParamDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.ChannelListSelectDto;
import kr.co.homeplus.admin.web.partner.model.affiliate.ChannelSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequiredArgsConstructor
@RequestMapping("/partner")
public class AffiliateChannelController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 > Main
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/affiliateChannelMain", method = RequestMethod.GET)
    public String affiliateChannelMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "use_yn"       // 사용여부
            , "affli_channel_type"    // 제휴채널타입
            , "affli_search_type"     // 제휴채널검색타입
            , "dsk_site_gubun"        // 사이트 구분
        );

        //코드정보
        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("dskSiteGugun", code.get("dsk_site_gubun"));
        model.addAttribute("affliSearchType", code.get("affli_search_type"));

        model.addAllAttributes(RealGridHelper.create("affliChannelListGridBaseInfo", ChannelListSelectDto.class));

        return "/partner/affiliateChannelMain";
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 > 제휴채널 리스트
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getAffiliateChannelList.json"}, method = RequestMethod.GET)
    public List<ChannelListSelectDto> getAffiliateChannelList(ChannelListParamDto channelListParamDto) {

        String apiUri = "/partner/affiliate/getChannelList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<ChannelListSelectDto>>>() {};

        return (List<ChannelListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, ChannelListParamDto.class, channelListParamDto), typeReference).getData();
    }

    /**
     * 제휴 업체명 조회
     * @param partnerId
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/getAffiliateNm.json"}, method = RequestMethod.GET)
    public ResponseResult getPartnerNm(@RequestParam(name = "partnerId") String partnerId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUriRequest = "/partner/affiliate/getAffiliateNm" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {})
            .getData();
    }

    /**
     * 제휴 업체 매츨코드 조회
     * @param channelId
     * @return ResponseResult
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/getChannelGroupList.json"}, method = RequestMethod.GET)
    public List<ChannelGroupListSelectDto> getChannelGroupList(@RequestParam(name = "channelId") String channelId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("channelId", channelId));

        String apiUriRequest = "/partner/affiliate/getChannelGroupList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<ChannelGroupListSelectDto>>>() {})
            .getData();
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 > 제휴채널 등록/수정
     * @param channelSetParamDto
     * @return ResponseResult
     * @throws
     */
    @ResponseBody
    @RequestMapping(value = {"/setAffliChannel.json"}, method = RequestMethod.POST)
    public ResponseResult setAffliChannel(@RequestBody ChannelSetParamDto channelSetParamDto) {
        String apiUri = "/partner/affiliate/setChannel";
        channelSetParamDto.setUserId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, channelSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 쿠키 유효기간 히스토리 (팝업)
     * @param model
     * @param channelId
     * @return
     * @throws
     */
    @RequestMapping("/popup/affiliateChannelCookieTimeHistoryPop")
    public String affiliateChannelCookieTimeHistoryPop(Model model, @RequestParam(value="channelId") Long channelId) {
        model.addAllAttributes(RealGridHelper.create("affiliateChannelCookieTimeHistoryPopBaseInfo", AffiliateChannelHistGetDto.class));
        model.addAttribute("channelId", channelId);
        return "/partner/pop/affiliateChannelCookieTimePop";
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 수수료 히스토리 (팝업)
     * @param model
     * @param channelId
     * @return
     * @throws
     */
    @RequestMapping("/popup/affiliateChannelCommissionRateHistoryPop")
    public String affiliateChannelCommissionRateHistoryPop(Model model, @RequestParam(value="channelId") Long channelId) {
        model.addAllAttributes(RealGridHelper.create("affiliateChannelCommissionRateHistoryPopBaseInfo", AffiliateChannelHistGridGetDto.class));
        model.addAttribute("channelId", channelId);
        return "/partner/pop/affiliateChannelCommissionRatePop";
    }

    /**
     * 업체관리 > 제휴업체관리 > 제휴채널관리 수수료 히스토리 조회
     *
     * @param channelId
     * @return List<AffiliateChannelHistGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/getAffiliateChannelHist.json"}, method = RequestMethod.GET)
    public List<AffiliateChannelHistGetDto> getAffiliateChannelHist(@RequestParam(value="channelId") Long channelId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("channelId", channelId));

        String apiUriRequest = "/partner/affiliate/getAffiliateChannelHist" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<AffiliateChannelHistGetDto>>>() {})
            .getData();
    }

}
