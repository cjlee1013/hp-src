package kr.co.homeplus.admin.web.core.certification.impl;


import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.core.certification.CertificationService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.certification.LoginLogService;
import kr.co.homeplus.admin.web.core.constants.AdminConstants;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.dto.LoginParam;
import kr.co.homeplus.admin.web.core.dto.UserInfo;
import kr.co.homeplus.admin.web.core.exception.AdminFirstLoginException;
import kr.co.homeplus.admin.web.core.exception.AdminLoginLockedException;
import kr.co.homeplus.admin.web.core.exception.CertificationException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.util.ServletUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * 인증 관련기능의 서비스 구현체
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CertificationServiceImpl implements CertificationService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final LoginLogService loginLogService;

    /** 로그인 계정잠금 상태코드**/
    private static final String ADMIN_LOGIN_LOCKED = "ADMIN_LOGIN_LOCKED";

    @Override
    public boolean getUserCertificationService(final HttpServletRequest request,
        final LoginParam loginParam, final BindingResult bindingResult) {
        validLoginParam(loginParam, bindingResult);

        final UserInfo userInfo = getUserInfoFromAuthApi(loginParam);

        //최초 로그인 체크
        if (userInfo.isFirstLogin()) {
            log.error("admin first login:<userId:{}>", loginParam.getUserId());
            throw new AdminFirstLoginException(userInfo,
                ExceptionCode.SYS_ERROR_CODE_9208.getDesc());
        }
        //로그인 쿠키 생성
        loginCookieService.createLoginCookie(userInfo);

        //아이디 저장 쿠키생성
        if (loginParam.isChkSaveUserId()) {
            loginCookieService.createIdSaveCookie(loginParam.getUserId());
        } else {
            loginCookieService.removeIdSaveCookie();
        }

        //로그인 로그 저장
        loginLogService.insertLoginLog(userInfo.getUserId(), userInfo.getEmpId(),
            ServletUtils.clientIP(request));

        return true;
    }

    /**
     * 로그인 파라미터 유효성 체크를 수행합니다.
     * @param param 로그인 파라미터
     * @param bindingResult {@link BindingResult}
     */
    private void validLoginParam(final LoginParam param, final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            final List<ObjectError> errors = bindingResult.getAllErrors();
            throw new CertificationException(param, errors.get(0).getDefaultMessage());
        }
    }

    /**
     * auth-api를 통해 로그인 사용자 정보를 조회하고, 조회 유형에 따라 {@link UserInfo} 또는 예외를 발생시킵니다.
     * @param loginParam 로그인 파라미터
     * @return {@link UserInfo}
     * @see
     */
    private UserInfo getUserInfoFromAuthApi(final LoginParam loginParam) {
        final ResponseObject<UserInfo> responseObject = getUserInfoFromAuthApiByResourceClient(loginParam);
        return getUserInfo(loginParam, responseObject);
    }

    /**
     * resouceClient를 통해 auth-api 에서 사용자 정보를 조회합니다.
     * @param loginParam 로그인 파라미터
     * @return {@link ResponseObject}
     */
    private ResponseObject<UserInfo> getUserInfoFromAuthApiByResourceClient(
        final LoginParam loginParam) {
        return resourceClient.postForResponseObject(ResourceRouteName.AUTHORITY, loginParam,
            AdminConstants.AUTH_ADMIN_LOGIN_URI,
            new ParameterizedTypeReference<>() {
            });
    }

    /**
     * auth-api의 응답결과를 유형에 따라 분류하여 반환합니다.
     * @param param 로그인 파라미터
     * @param responseObject auth-api 응답결과
     * @return {@link UserInfo}
     */
    private UserInfo getUserInfo(final LoginParam param,
        final ResponseObject<UserInfo> responseObject) {

        if (isResponseSuccess(responseObject.getReturnCode())) {
            return responseObject.getData();

        //계정 잠금여부
        } else if (isResponseAdminLoginLock(responseObject.getReturnCode())) {
            log.error("admin user locked:<{}>,<{}>", param.getUserId(),
                responseObject.getReturnMessage());
            throw new AdminLoginLockedException(param, responseObject.getReturnMessage());

        //기타 예외
        } else {
            log.error("admin user certificate failed:<{}>,<{}>", param.getUserId(),
                responseObject.getReturnMessage());
            throw new CertificationException(param, responseObject.getReturnMessage());
        }
    }

    /**
     * ResponseObject의 결과가 정상인지 확인합니다.
     * @param returnCode ResponseObject의 returnCode
     * @return returnCode가 "SUCCESS"일 경우 true, 아닐 경우 false를 반환합니다.
     */
    private boolean isResponseSuccess(final String returnCode) {
        return returnCode.equals(ResponseObject.RETURN_CODE_SUCCESS);
    }

    /**
     * 해당 계정이 잠금처리 된 계정인지 returnCode를 통해 확인합니다.
     * @param returnCode ResponseObject의 returnCode
     * @return returnCode가 "ADMIN_LOGIN_LOCKED"일 경우 잠금 처리가 된 계정으로 true, 아닐 경우 false를 반환합니다.
     */
    private boolean isResponseAdminLoginLock(final String returnCode) {
        return returnCode.equals(ADMIN_LOGIN_LOCKED);
    }
}
