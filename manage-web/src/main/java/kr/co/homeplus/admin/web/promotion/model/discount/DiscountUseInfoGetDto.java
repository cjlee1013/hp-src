package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountUseInfoGetDto {
    private int discountUseListCount;
    private List<DiscountUseGetDto> discountUseList;
    private int discountCount;
    private long sumDiscountAmt;
}
