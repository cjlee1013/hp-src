package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Locale;

@Getter
@RequiredArgsConstructor
public enum DataManagementTotalSearchSortCode {
    RANK("추천순", ""),
    NEW("최신순", ""),
    PRICE_DOWN("낮은 가격순", ""),
    PRICE_UP("높은 가격순", ""),
    SALES_UP("많이 팔린순", ""),
    REVIEW_UP("리뷰 많은순", "");

    private final String value;
    private final String explanation;

    public static DataManagementTotalSearchSortCode baseCode() {
        return RANK;
    }

    /**
     * String code => enum code 형식으로 반환
     *
     * @param codeName
     * @return BaseSortCode
     */
    public static DataManagementTotalSearchSortCode findCode(String codeName) {
        return Arrays.stream(DataManagementTotalSearchSortCode.values())
                .filter(c -> c.name()
                        .equals(codeName.toUpperCase(Locale.KOREA)))
                .findAny()
                .orElse(baseCode());
    }

    /**
     * Enum Code 유효성 검사
     *
     * @param codeName
     * @return boolean
     */
    public static boolean valid(String codeName) {
        return Arrays.stream(DataManagementTotalSearchSortCode.values())
                .anyMatch(c -> c.name().equals(codeName.toUpperCase(Locale.KOREA)));
    }
}
