package kr.co.homeplus.admin.web.settle.model.adjust;

import java.util.HashMap;

public class AdjustCode {
    private static HashMap<Integer,String> adjustTaxCode;
    private static HashMap<Integer,String> adjustCodeAll;

    public static HashMap<Integer,String> getAdjustTaxCode(){
        if (adjustTaxCode == null) {
            adjustTaxCode = new HashMap<Integer,String>();
            adjustTaxCode.put(1,"업체 보상비");
            adjustTaxCode.put(2,"업체 패널티");
            adjustTaxCode.put(3,"광고 수수료");
            adjustTaxCode.put(4,"판매 수수료");
            adjustTaxCode.put(5,"판매 수수료");
        }
        return adjustTaxCode;
    }

    public static HashMap<Integer,String> getAdjustCodeAll(){
        if (adjustCodeAll == null) {
            adjustCodeAll = new HashMap<Integer,String>();
            adjustCodeAll.put(1,"업체 보상비");
            adjustCodeAll.put(2,"업체 패널티");
            adjustCodeAll.put(3,"광고 수수료");
            adjustCodeAll.put(4,"판매 수수료");
        }
        return adjustCodeAll;
    }
}
