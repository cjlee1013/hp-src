package kr.co.homeplus.admin.web.app.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import kr.co.homeplus.admin.web.app.model.AppPushAgreeDto;
import kr.co.homeplus.admin.web.app.model.DateCalInfo;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppPushAgreeStatService {

    private final ResourceClient resourceClient;

    private static final String AGREE_STAT_API_URI = "/appAdver/push/agreements";
    private static final String STRING_PADDING_LEFT = "LEFT";
    private static final String STRING_PADDING_CHAR_0 = "0";
    private static final String YEARS_SELECT_BOX_OPTION_STRING = " 년";
    private static final String MONTHS_SELECT_BOX_OPTION_STRING = " 월";
    private static final String ZONE_ID_ASIA_SEOUL = "Asia/Seoul";

    /**
     * 통계 조회
     *
     * @param year  조회 년도(yyyy)
     * @param month 조회 월(mm)
     * @return
     */
    public List<AppPushAgreeDto> getAgreeStatList(final String year, final String month) {
        final String uri = AGREE_STAT_API_URI + "?year=" + year + "&month=" + month;

        return resourceClient.getForResponseObject(
            ResourceRouteName.USERMNG, uri,
            new ParameterizedTypeReference<ResponseObject<List<AppPushAgreeDto>>>() {
            }).getData();
    }

    /**
     * 검색조건 > 조회기간 > 셀렉트박스 노출할 년도 계산
     *
     * @param baseYear 기준 년도
     * @return {@link DateCalInfo}
     */
    public List<DateCalInfo> getCalculateYears(final int baseYear) {
        final int currentYear = getNowDate().getYear();
        return generateYears(baseYear, currentYear);
    }

    /**
     * 현재년도 부터 기준년도 까지 셀렉트박스 옵션, 값을 역순으로 생성
     *
     * @param baseYear 기준년도
     * @param currentYear 현재년도
     * @return {@link DateCalInfo}
     */
    private List<DateCalInfo> generateYears(final int baseYear, final int currentYear) {
        List<DateCalInfo> calInfoList = new ArrayList<>();

        IntStream.iterate(currentYear, idx -> idx >= baseYear, idx -> idx - 1)
            .forEachOrdered(idx -> {
                DateCalInfo dateCalInfo = new DateCalInfo();
                dateCalInfo.setOptionValue(getLeftPaddingLength4(idx));
                dateCalInfo
                    .setOptionStr(getLeftPaddingLength4(idx) + YEARS_SELECT_BOX_OPTION_STRING);
                calInfoList.add(dateCalInfo);
            });
        return calInfoList;
    }

    /**
     * 조회년도에 맞춰 월별 셀렉트 박스 값 출력
     *
     * @param year 조회 년도
     * @return {@link DateCalInfo}
     */
    public List<DateCalInfo> getCalculateMonth(final int year) {
        final LocalDate nowFirstDateOfYear = LocalDate.ofYearDay(getNowDate().getYear(), 1);
        final LocalDate selectDate = LocalDate.of(year, 1, 1);

        //조회년도가 현재년도보다 이전일 경우 1 ~ 12월 노출
        if (selectDate.isBefore(nowFirstDateOfYear)) {
            return generateMonths(12);
        } else {
            return generateMonths(getNowDate().getMonth().getValue());
        }
    }

    /**
     * 현재 일자를 조회합니다.
     * @return 현재 날짜를 {@link LocalDate} 타입으로 리턴합니다.
     */
    private LocalDate getNowDate() {
        return LocalDate.now(ZoneId.of(ZONE_ID_ASIA_SEOUL));
    }

    /**
     * 조회 월까지 셀렉트 박스 옵션, 값을 생성하여 리턴합니다.
     * @param month 조회 월
     * @return {@link DateCalInfo}
     */
    private List<DateCalInfo> generateMonths(final int month) {
        List<DateCalInfo> calInfoList = new ArrayList<>();

        IntStream.rangeClosed(1, month).forEachOrdered(idx -> {
            DateCalInfo dateCalInfo = new DateCalInfo();
            dateCalInfo.setOptionValue(getLeftPaddingLength2(idx));
            dateCalInfo.setOptionStr(getLeftPaddingLength2(idx) + MONTHS_SELECT_BOX_OPTION_STRING);
            calInfoList.add(dateCalInfo);
        });
        return calInfoList;
    }

    private String getLeftPaddingLength4(final int idx) {
        return StringUtil
            .stringPadding(String.valueOf(idx), STRING_PADDING_LEFT, STRING_PADDING_CHAR_0, 4);
    }

    private String getLeftPaddingLength2(final int idx) {
        return StringUtil
            .stringPadding(String.valueOf(idx), STRING_PADDING_LEFT, STRING_PADDING_CHAR_0, 2);
    }
}
