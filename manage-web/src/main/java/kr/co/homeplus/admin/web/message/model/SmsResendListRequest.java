package kr.co.homeplus.admin.web.message.model;

import java.util.List;
import lombok.Data;

@Data
public class SmsResendListRequest {

    private List<Long> smsWorkSeqList;
}
