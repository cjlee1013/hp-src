package kr.co.homeplus.admin.web.voc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 1:1문의 리스트 응답 DTO")
public class OneOnOneInquiryListGetDto {
  @ApiModelProperty(notes = "문의번호")
  @RealGridColumnInfo(headText = "문의번호", width = 120, sortable = true)
  private String inqryNo;

  @ApiModelProperty(notes = "사이트코드")
  @RealGridColumnInfo(headText = "사이트코드", width = 100, sortable = true, hidden = true)
  private String siteType;

  @ApiModelProperty(notes = "사이트")
  @RealGridColumnInfo(headText = "사이트", width = 100, sortable = true)
  private String siteTypeNm;

  @ApiModelProperty(notes = "문의종류코드")
  @RealGridColumnInfo(headText = "문의종류코드", width = 100, sortable = true, hidden = true)
  private String inqryKind;

  @ApiModelProperty(notes = "문의종류")
  @RealGridColumnInfo(headText = "문의종류", width = 100, sortable = true)
  private String inqryKindNm;

  @ApiModelProperty(notes = "처리상태코드")
  @RealGridColumnInfo(headText = "처리상태코드", width = 120, sortable = true, hidden = true)
  private String inqryStatus;

  @ApiModelProperty(notes = "처리상태")
  @RealGridColumnInfo(headText = "처리상태", width = 100, sortable = true)
  private String inqryStatusNm;

  @ApiModelProperty(notes = "구분코드")
  @RealGridColumnInfo(headText = "구분코드", width = 120, sortable = true, hidden = true)
  private String inqryCategory;

  @ApiModelProperty(notes = "구분")
  @RealGridColumnInfo(headText = "구분", width = 100, sortable = true)
  private String inqryCategoryNm;

  @ApiModelProperty(notes = "문의구분코드")
  @RealGridColumnInfo(headText = "문의구분코드", width = 120, sortable = true, hidden = true)
  private String inqryType;

  @ApiModelProperty(notes = "문의구분")
  @RealGridColumnInfo(headText = "문의구분", width = 150, sortable = true)
  private String inqryTypeNm;

  @ApiModelProperty(notes = "문의항목코드")
  @RealGridColumnInfo(headText = "문의항목코드", width = 120, sortable = true, hidden = true)
  private String inqryDetailType;

  @ApiModelProperty(notes = "문의항목")
  @RealGridColumnInfo(headText = "문의항목", width = 180, sortable = true)
  private String inqryDetailTypeNm;

  @ApiModelProperty(notes = "점포유형")
  @RealGridColumnInfo(headText = "점포유형", width = 100, sortable = true)
  private String storeType;

  @ApiModelProperty(notes = "판매자ID/점포코드")
  @RealGridColumnInfo(headText = "판매자", width = 120, sortable = true, hidden = true)
  private String storeId;

  @ApiModelProperty(notes = "판매자")
  @RealGridColumnInfo(headText = "판매자", width = 150, sortable = true)
  private String storeNm;

  @ApiModelProperty(notes = "제목")
  @RealGridColumnInfo(headText = "제목", width = 300, sortable = true, columnType = RealGridColumnType.NONE)
  private String inqryTitle;

  @ApiModelProperty(notes = "내용")
  @RealGridColumnInfo(headText = "내용", width = 500, sortable = true, columnType = RealGridColumnType.NONE, hidden = true)
  private String inqryCntnt;

  @ApiModelProperty(notes = "문의일")
  @RealGridColumnInfo(headText = "문의일", width = 130, sortable = true)
  private String inqryDt;

  @ApiModelProperty(notes = "파일첨부")
  @RealGridColumnInfo(headText = "파일첨부", width = 100, sortable = true)
  private String inqryImgYn;

  @ApiModelProperty(notes = "회원번호")
  @RealGridColumnInfo(headText = "회원번호", width = 120, sortable = true, hidden = true)
  private String userNo;

  @ApiModelProperty(notes = "작성자")
  @RealGridColumnInfo(headText = "작성자", width = 120, sortable = true)
  private String userNm;

  @ApiModelProperty(notes = "주문번호")
  @RealGridColumnInfo(headText = "주문번호", width = 120, sortable = true)
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "처리일")
  @RealGridColumnInfo(headText = "처리일", width = 130, sortable = true)
  private String inqryAnswrDt;

  @ApiModelProperty(notes = "처리소요시간")
  @RealGridColumnInfo(headText = "처리소요시간", width = 100, sortable = true)
  private String answrDiffTime;

  @ApiModelProperty(notes = "처리자ID")
  @RealGridColumnInfo(headText = "처리자ID", width = 120, sortable = true, hidden = true)
  private String inqryAnswrRegId;

  @ApiModelProperty(notes = "처리자")
  @RealGridColumnInfo(headText = "처리자", width = 120, sortable = true)
  private String inqryAnswrRegNm;

  @ApiModelProperty(notes = "SMS발송여부")
  @RealGridColumnInfo(headText = "SMS발송여부", width = 120, sortable = true)
  private String smsAlamYn;
}