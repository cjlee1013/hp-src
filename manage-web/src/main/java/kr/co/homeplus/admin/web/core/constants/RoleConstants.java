package kr.co.homeplus.admin.web.core.constants;

/**
 * 어드민 역할 권한 코드
 */
public class RoleConstants {
    /** 10.회원-회원관리_등급 : 등급변경 버튼 노출 **/
    public static final String MEMBER_GRADE = "member_grade";

    /** 10.회원-회원관리_조회 : 회원관리 목록조회 **/
    public static final String MEMBER_COMMON = "member_common";

    /** 03.업체관리-판매업체 관리_조회 **/
    public static final String SELLER_SETTLE_PAY = "seller_settle_pay";

    /** 02.카테고리-카테고리 관리_조회 **/
    public static final String CATEGORY_VIEW = "category_view";
    
    // 주문관리 - e-ticket 취소권한
    public static final String ORDER_TICKET_CANCEL = "system_escrow";
}
