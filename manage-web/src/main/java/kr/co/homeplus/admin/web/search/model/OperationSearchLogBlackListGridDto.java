package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.*;
import lombok.Getter;
import lombok.Setter;

public class OperationSearchLogBlackListGridDto {
    @Getter @Setter
    @RealGridInfo
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
    public static class blackList{
        @RealGridColumnInfo(headText = "블랙리스트 Key", width = 150, hidden = true, columnType = RealGridColumnType.BASIC)
        private String blacklistNo;
        @RealGridColumnInfo(headText = "블랙리스트 IP", width = 150, sortable = true, columnType = RealGridColumnType.BASIC)
        private String ip;
        @RealGridColumnInfo(headText = "서비스 유형", width = 70, columnType = RealGridColumnType.BASIC)
        private String serviceType;
        @RealGridColumnInfo(headText = "블랙리스트 유형", width = 70, columnType = RealGridColumnType.BASIC)
        private String blacklistType;
        @RealGridColumnInfo(headText = "유효시작일", width = 80, sortable = true, columnType = RealGridColumnType.BASIC)
        private String validStartDt;
        @RealGridColumnInfo(headText = "유효종료일", width = 80, sortable = true, columnType = RealGridColumnType.BASIC)
        private String validEndDt;
        @RealGridColumnInfo(headText = "활성화 상태", width = 50, columnType = RealGridColumnType.BASIC)
        private String enableYn;
        @RealGridColumnInfo(headText = "검색누적횟수", width = 50, columnType = RealGridColumnType.BASIC)
        private int applyCount;
        @RealGridColumnInfo(headText = "등록일", width = 80, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regDt;
        @RealGridColumnInfo(headText = "등록자", width = 50, columnType = RealGridColumnType.BASIC)
        private String regId;
        @RealGridColumnInfo(headText = "수정일", width = 80, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgDt;
        @RealGridColumnInfo(headText = "수정자", width = 50, columnType = RealGridColumnType.BASIC)
        private String chgId;
        @RealGridColumnInfo(headText = "비고(메모)", width = 99, columnType = RealGridColumnType.BASIC)
        private String memo;

    }

    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
    public static class ruleInfo{
        @RealGridColumnInfo(headText = "rule No", hidden = true, columnType = RealGridColumnType.BASIC)
        private long ruleNo;
        @RealGridColumnInfo(headText = "서비스 유형", width = 80, columnType = RealGridColumnType.BASIC)
        private String serviceType;
        @RealGridColumnInfo(headText = "블랙리스트 유형", width = 80, columnType = RealGridColumnType.BASIC)
        private String blacklistType;
        @RealGridColumnInfo(headText = "배치 주기(시간)", width = 60, columnType = RealGridColumnType.BASIC)
        private long ruleHourInterval;
        @RealGridColumnInfo(headText = "필터링 간격(분)", width = 60, columnType = RealGridColumnType.BASIC)
        private long ruleMinuteInterval;
        @RealGridColumnInfo(headText = "필터링 검색 빈도", width = 60, columnType = RealGridColumnType.BASIC)
        private long ruleMinuteFrequency;
        @RealGridColumnInfo(headText = "필터링 반복 횟수", width = 60, columnType = RealGridColumnType.BASIC)
        private long ruleRepeatCount;
        @RealGridColumnInfo(headText = "블랙리스트 등록 횟수", width = 60, columnType = RealGridColumnType.BASIC)
        private long blacklistAddCount;
        @RealGridColumnInfo(headText = "수정일", width = 50, columnType = RealGridColumnType.BASIC)
        private String chgDt;
        @RealGridColumnInfo(headText = "수정자", width = 50, columnType = RealGridColumnType.BASIC)
        private String chgId;

    }
}
