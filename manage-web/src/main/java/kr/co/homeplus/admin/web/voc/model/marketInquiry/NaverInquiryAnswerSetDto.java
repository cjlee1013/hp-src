package kr.co.homeplus.admin.web.voc.model.marketInquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 제휴사 고객문의 답변 등록 요청 DTO")
public class NaverInquiryAnswerSetDto {
  @ApiModelProperty(notes = "문의번호")
  private Long messageNo;

  @ApiModelProperty(notes = "문의유형")
  private String messageType;

  @ApiModelProperty(notes = "답변제목")
  private String responseTitle;

  @ApiModelProperty(notes = "답변내용")
  private String responseContents;

  @ApiModelProperty(notes = "답변자ID")
  private String isresponseId;

  @ApiModelProperty(notes = "답변자명")
  private String isresponseNm;
}