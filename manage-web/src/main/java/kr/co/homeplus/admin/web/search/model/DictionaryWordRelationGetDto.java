package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DictionaryWordRelationGetDto {
    private WordRegDto info;
    private List<RelationDto> synonyms;
    private List<RelationDto> hyponyms;
    private List<RelationDto> relatednyms;
    private List<String> typonyms;
}
