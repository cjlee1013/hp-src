package kr.co.homeplus.admin.web.escrow.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.escrow.model.ZipcodeManageDto;
import kr.co.homeplus.admin.web.escrow.model.ZipcodeManageSelectDto;
import kr.co.homeplus.admin.web.escrow.service.ZipcodeManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 시스템관리 > 기준정보관리 > 우편번호관리
 */
@Controller
@RequestMapping("/escrow/standard")
public class ZipcodeManageController {

    private LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    @Autowired
    private ZipcodeManageService zipcodeManageService;

    private static final List<RealGridColumn> ZIPCODE_MANAGE_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption ZIPCODE_MANAGE_GRID_OPTION = new RealGridOption("fill", false, true, false);

    public ZipcodeManageController(LoginCookieService loginCookieService, AuthorityService authorityService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;

        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("zipcode", "zipcode", "우편번호", RealGridColumnType.BASIC, 50, true, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("roadAddrFullTxt", "roadAddrFullTxt", "도로명", RealGridColumnType.NAME, 250, true, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("gibunAddrFullTxt", "gibunAddrFullTxt", "지번", RealGridColumnType.NAME, 250, true, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("buildMngNo", "buildMngNo", "건물관리번호", RealGridColumnType.BASIC, 150, true, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("islandType", "islandType", "도서산간여부", RealGridColumnType.BASIC, 100, false, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("islandTypeTxt", "islandTypeTxt", "도서산간여부", RealGridColumnType.BASIC, 100, true, true));
        ZIPCODE_MANAGE_GRID_HEAD.add(new RealGridColumn("chgDt", "chgDt", "수정일", RealGridColumnType.DATETIME, 100, true, true));
    }

    /**
     * 우편번호관리 Main Page
     */
    @GetMapping("/zipcodeManageMain")
    public String zipcodeManageMain(Model model) {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_SYSTEM_DEV)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        // 시도명 리스트 가져와서 model 에 담기
        List<String> zipcodeSidoList = zipcodeManageService.getZipcodeSidoList();
        model.addAttribute("zipcodeSidoList", zipcodeSidoList);
        model.addAttribute("zipcodeManageGridBaseInfo", new RealGridBaseInfo("zipcodeManageGridBaseInfo", ZIPCODE_MANAGE_GRID_HEAD, ZIPCODE_MANAGE_GRID_OPTION).toString());
        return "/escrow/zipcodeManageMain";
    }

    /**
     * 시군구 리스트 조회
     * @param sidoNm
     * @return
     */
    @ResponseBody
    @GetMapping("/getZipcodeSigunguList.json")
    public List<String> getZipcodeSigunguList(@RequestParam(value = "sidoNm") String sidoNm) {
        return zipcodeManageService.getZipcodeSigunguList(sidoNm);
    }

    /**
     * 우편번호/도로명/지번 검색
     * @return
     */
    @ResponseBody
    @GetMapping("/getZipcodeManageList.json")
    public List<ZipcodeManageDto> getZipcodeManageList(@ModelAttribute ZipcodeManageSelectDto zipcodeManageSelectDto) {
        return zipcodeManageService.getZipcodeManageList(zipcodeManageSelectDto);
    }

    /**
     * 도서산간여부(제주/도서산간/해당없음) 수정
     * @param zipcodeManageDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/updateZipcodeIslandType.json")
    public Integer updateZipcodeIslandType(@RequestBody ZipcodeManageDto zipcodeManageDto) throws Exception {
        zipcodeManageDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return zipcodeManageService.updateZipcodeIslandType(zipcodeManageDto);
    }
}

