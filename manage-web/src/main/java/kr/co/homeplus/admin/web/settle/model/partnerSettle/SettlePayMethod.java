package kr.co.homeplus.admin.web.settle.model.partnerSettle;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum SettlePayMethod {
    //지급방법 - 정산주기 settle_period_cd=12 만 직접지급,
    SETTLE_PAY_METHOD_AGENCY("11", "지급대행"),
    SETTLE_PAY_METHOD_DIRECT("12", "직접지급");

    private String code;
    private String name;
}
