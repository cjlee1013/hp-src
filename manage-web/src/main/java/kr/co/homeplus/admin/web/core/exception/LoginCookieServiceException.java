package kr.co.homeplus.admin.web.core.exception;

public class LoginCookieServiceException extends RuntimeException {

    public LoginCookieServiceException(Exception e) {
        super(e);
    }

    public LoginCookieServiceException(String message, Exception e) {
        super(message, e);
    }

    public LoginCookieServiceException(String message) {
        super(message);
    }
}
