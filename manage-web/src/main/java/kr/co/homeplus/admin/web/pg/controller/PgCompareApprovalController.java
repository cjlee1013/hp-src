package kr.co.homeplus.admin.web.pg.controller;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalDiffDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareApprovalSumDto;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.service.PgCompareApprovalService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * Admin > 결제관리 > PG 대사 > PG 승인대사 조회
 */
@Controller
@RequestMapping("/pg/pgCompareApproval")
public class PgCompareApprovalController {
    private final LoginCookieService loginCookieService;
    private final PgCompareApprovalService pgCompareApprovalService;

    public PgCompareApprovalController(LoginCookieService loginCookieService, PgCompareApprovalService pgCompareApprovalService) {
        this.loginCookieService = loginCookieService;
        this.pgCompareApprovalService = pgCompareApprovalService;
    }

    /**
     * PG 승인대사 조회 메인페이지 호출
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/pgCompareApprovalMain")
    public String paymentPolicyManageMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("pgCompareApprovalSumGridBaseInfo", PgCompareApprovalSumDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("pgCompareApprovalDiffGridBaseInfo", PgCompareApprovalDiffDto.class));

        model.addAttribute("pgKindList", Arrays.asList(PgKind.values()));
        model.addAttribute("paymentMethodList", ParentMethodCd.getBasicParentMethodList());

        return "/pg/pgCompareApprovalMain";
    }

    /**
     * PG 승인대사 합계내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/getPgCompareApprovalSumList.json")
    public List<PgCompareApprovalSumDto> getPgCompareApprovalSumList(@ModelAttribute PgCompareApprovalSelectDto pgCompareApprovalSelectDto) throws Exception {
        return pgCompareApprovalService.getPgCompareApprovalSumList(pgCompareApprovalSelectDto);
    }

    /**
     * PG 승인대사 차이내역 조회
     * @param pgCompareApprovalSelectDto
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/getPgCompareApprovalDiffList.json")
    public List<PgCompareApprovalDiffDto> getPgCompareApprovalDiffList(@ModelAttribute PgCompareApprovalSelectDto pgCompareApprovalSelectDto) throws Exception {
        return pgCompareApprovalService.getPgCompareApprovalDiffList(pgCompareApprovalSelectDto);
    }

}