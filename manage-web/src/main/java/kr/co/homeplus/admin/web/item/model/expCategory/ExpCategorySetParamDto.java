package kr.co.homeplus.admin.web.item.model.expCategory;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpCategorySetParamDto {

    private Integer cateCd;
    private String cateNm;
    private Integer priority;
    private int depth;
    private int parentCateCd;
    private String useYn;
    private String onIconPcImgUrl;
    private String offIconPcImgUrl;
    private String onIconMobileImgUrl;
    private String offIconMobileImgUrl;
    private String regId;

    private List<ExpRmsCategorySetParamDto> rmsCategoryList;

}
