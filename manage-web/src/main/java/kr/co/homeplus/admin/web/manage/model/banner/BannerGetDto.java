package kr.co.homeplus.admin.web.manage.model.banner;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class BannerGetDto {

    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true)
    private Long bannerNo;

    @RealGridColumnInfo(headText = "사이트 구분", width = 70, order = 1, sortable = true)
    private String siteTypeNm;

    @RealGridColumnInfo(headText = "디바이스", width = 80, order = 2, sortable = true)
    private String deviceTypeNm;

    @RealGridColumnInfo(headText = "전시위치(1Depth)", width = 120, order = 3, sortable = true)
    private String loc1DepthNm;

    @RealGridColumnInfo(headText = "전시위치(2Depth)", width = 120, order = 4, sortable = true)
    private String loc2DepthNm;

    @RealGridColumnInfo(headText = "전시위치(3Depth)", width = 120, order = 5, sortable = true)
    private String loc3DepthNm;

    @RealGridColumnInfo(headText = "배너명", width = 180, order = 6, sortable = true)
    private String bannerNm;

    @RealGridColumnInfo(headText = "전시 시작일", width = 120, order = 7, sortable = true)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시 종료일", width = 120, order = 8, sortable = true)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "전시순서", width = 50, columnType = RealGridColumnType.NUMBER_CENTER, order = 9, sortable = true)
    private int priority;

    @RealGridColumnInfo(headText = "전시여부", width = 70, order = 10, sortable = true)
    private String dispYnTxt;

    @RealGridColumnInfo(headText = "등록자", width = 80, order = 11, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", width = 120, columnType = RealGridColumnType.DATETIME, order = 12, sortable = true)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", width = 80, order = 13, sortable = true)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", width = 120, columnType = RealGridColumnType.DATETIME, order = 14, sortable = true)
    private String chgDt;


    @RealGridColumnInfo(headText = "template", order = 15, hidden = true, sortable = true)
    private String template;
    @RealGridColumnInfo(headText = "siteType", order = 16, hidden = true, sortable = true)
    private String siteType;
    @RealGridColumnInfo(headText = "device", order = 17, hidden = true, sortable = true)
    private String device;
    @RealGridColumnInfo(headText = "loc1Depth", order = 18, hidden = true, sortable = true)
    private String loc1Depth;
    @RealGridColumnInfo(headText = "loc2Depth", order = 19, hidden = true, sortable = true)
    private String loc2Depth;
    @RealGridColumnInfo(headText = "loc3Depth", order = 20, hidden = true, sortable = true)
    private String loc3Depth;
    @RealGridColumnInfo(headText = "dispYn", order = 21, hidden = true, sortable = true)
    private String dispYn;
    @RealGridColumnInfo(headText = "dispTimeYn", order = 22, hidden = true, sortable = true)
    private Character dispTimeYn;
    @RealGridColumnInfo(headText = "displayStartTime", order = 23, hidden = true, sortable = true)
    private String dispStartTime;
    @RealGridColumnInfo(headText = "displayEndTime", order = 24, hidden = true, sortable = true)
    private String dispEndTime;


    private String regId;
    private String chgId;

}
