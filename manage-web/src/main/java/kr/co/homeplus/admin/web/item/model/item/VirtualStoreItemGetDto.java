package kr.co.homeplus.admin.web.item.model.item;

import java.time.LocalDateTime;
import java.util.Date;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class VirtualStoreItemGetDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, sortable = true)
    private String itemNm;

    @RealGridColumnInfo(headText = "택배점포ID", hidden = true)
    private int storeId;

    @RealGridColumnInfo(headText = "기준점ID", hidden = true)
    private int originStoreId;

    @RealGridColumnInfo(headText = "택배점취급여부")
    private char dealYn;

    @RealGridColumnInfo(headText = "가상단독판매여부")
    private char virtualStoreOnlyYn;

    @RealGridColumnInfo(headText = "가상단독판매여부명", hidden = true)
    private String virtualStoreOnlyYnNm;

    @RealGridColumnInfo(headText = "배송정책번호")
    private long shipPolicyNo;

    @RealGridColumnInfo(headText = "배송정책명", width = 200)
    private String shipPolicyNm;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME_ISO, width = 200, sortable = true)
    private LocalDateTime regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.DATETIME, columnType = RealGridColumnType.DATETIME_ISO, width = 200, sortable = true)
    private LocalDateTime chgDt;
}
