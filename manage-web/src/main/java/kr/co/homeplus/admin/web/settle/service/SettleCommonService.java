package kr.co.homeplus.admin.web.settle.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.pop.StorePopupSearchGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SettleCommonService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    /**
     * 날짜 계산
     * @param type
     * @return
     * @throws Exception
     */
    public List<HashMap<String, Object>> getDateCalculate(String type, String pickYear, String pickMonth, int gapValue) throws Exception {
        //날짜 계산을 위한 함수 선언
        Calendar calendar = Calendar.getInstance();
        //매주 시작일을 월요일로 설정
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        //년도
        int year = pickYear != null ? Integer.parseInt(pickYear) : calendar.get(Calendar.YEAR);

        //입력받은 년도가 당해년도와 맞지 않는 경우
        //입력받은 년도의 마지막 날로 날짜를 셋팅한다.
        if (year != calendar.get(Calendar.YEAR)) {
            calendar.set(year, Calendar.DECEMBER, calendar.getActualMaximum(Calendar.DECEMBER));
        } else {
            if(!type.equalsIgnoreCase("Y")){
                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + gapValue);
            }
        }

        if (pickMonth != null) {
            //입력받은 날짜를 처리한다.
            int month = Integer.parseInt(pickMonth) - 1;

            //입력받은 달이 조회달보다 적을 경우에 처리한다.
            if (month < calendar.get(Calendar.MONTH)) {
                //특정월로 설정
                calendar.set(year, month, calendar.getActualMaximum(Calendar.DATE));

                //마지막날이 일요일이 아닌경우
                if (Calendar.SUNDAY != calendar.get(Calendar.DAY_OF_WEEK)) {
                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE) - 6);
                }
            }
        }

        //날짜 리턴을 위한 함수 선언
        List<HashMap<String, Object>> returnList = new ArrayList();
        HashMap<String, Object> parameterMap = new HashMap();

        if (type.equalsIgnoreCase("Y")) {
            //년도를 조회하는 경우
            //당해년도의 앞 2년 뒤 2년을 리턴함.
            if(gapValue != 0){
                gapValue = (gapValue - (year - 4));
            }
            for (int idx = year - (4 - gapValue); idx < year + 1; idx++) {
                parameterMap.put("dataValue", StringUtil.stringPadding(String.valueOf(idx), "LEFT", "0", 4));
                parameterMap.put("dataStr", StringUtil.stringPadding(String.valueOf(idx), "LEFT", "0", 4) + " 년");
                returnList.add(parameterMap);
                parameterMap = new HashMap();
            }
        } else if (type.equalsIgnoreCase("M")) {
            for (int idx = 0; idx < (calendar.get(Calendar.MONTH) + 1); idx++) {
                parameterMap.put("dataValue", StringUtil.stringPadding(String.valueOf(idx + 1), "LEFT", "0", 2));
                parameterMap.put("dataStr", StringUtil.stringPadding(String.valueOf(idx + 1), "LEFT", "0", 2) + " 월");
                returnList.add(parameterMap);
                parameterMap = new HashMap();
            }
        } else if (type.equalsIgnoreCase("W")) {
            //오늘이 월중 몇번째 주인지 조회
            int weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH);
            //오늘이 포한된 주가 당해년도 중 몇번째 주인지 조회
            int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

            //주를 조회하는 경우
            //조회하는 월이 마지막이 되도록 함.
            for (int idx = 0; idx < weekOfMonth; idx++) {
                //오늘이 당해년도의 몇번째 주인지 계산 후 리턴
                parameterMap.put("dataValue", StringUtil.stringPadding(String.valueOf((weekOfYear - weekOfMonth) + (idx + 1)), "LEFT", "0", 2));
                //오늘이 당월에 몊번째 주인지 계산 후 리턴
                parameterMap.put("dataStr", StringUtil.stringPadding(String.valueOf(idx + 1), "LEFT", "0", 2) + " 주");
                returnList.add(parameterMap);
                parameterMap = new HashMap();
            }
        }
        //Map 초기화
        parameterMap.clear();

        return returnList;
    }

    /**
     * 공통 Code명 조회
     */
    public List<MngCodeGetDto> getCode(String gmcCd) {
        try {
            Map<String, List<MngCodeGetDto>> code = codeService.getCode(gmcCd);

            return code.get(gmcCd);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * UserCd 조회
     */
    public String getUserCd() {
        return loginCookieService.getUserInfo().getEmpId();
    }

    /**
     * 점포 리스트 조회 (select box용)
     */
    public List<StorePopupSearchGetDto> getStoreList(String storeType, String storeRange) {
        return resourceClient.getForResponseObject(
            ResourceRouteName.ITEM,
            "/item/store/getStoreForSelectBox?storeType=" + storeType + "&storeRange=" + storeRange,
            new ParameterizedTypeReference<ResponseObject<List<StorePopupSearchGetDto>>>() {}).getData();
    }
}
