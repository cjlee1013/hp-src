package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

/**
 * 안전인증 조회
 */
@Getter
@Setter
public class ItemCertParamDto {

	private String certGroup;
	private String certNo;

}
