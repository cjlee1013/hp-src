package kr.co.homeplus.admin.web.message.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.message.model.PushImage;
import kr.co.homeplus.admin.web.message.model.PushImageGrid;
import kr.co.homeplus.admin.web.message.model.PushImageListRequest;
import kr.co.homeplus.admin.web.message.model.PushImageSetResult;
import kr.co.homeplus.admin.web.message.service.PushImageService;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(tags = "Push 이미지 관리 어드민용 컨트롤러")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@Controller
@RequestMapping("/message/admin/pushimage")
@RequiredArgsConstructor
public class PushImageManageController {

    private final ResourceClient resourceClient;
    private final PushImageService pushImageService;

    private final Map<String, Object> pushImageGrid = RealGridHelper
        .create("pushimageGridBaseInfo", PushImageGrid.class);

    @ApiOperation(value = "Push 이미지 관리 main 화면")
    @ApiResponse(code = 200, message = "Push 이미지 관리 main 화면")
    @GetMapping("/main")
    public String main(final Model model) {
        model.addAllAttributes(pushImageGrid);

        return "/message/pushimageMain";
    }

    @ApiOperation(value = "Push 이미지 관리 목록 조회")
    @ApiResponse(code = 200, message = "Push 이미지 관리 목록 조회")
    @PostMapping("/getList.json")
    @ResponseBody
    public ResponseObject<List<PushImage>> getList(
        @ApiParam(value = "알림톡 발송 상세 조회 조건", required = true) @Valid final PushImageListRequest pushImageListRequest) {
        return resourceClient
            .postForResponseObject(ResourceRouteName.MESSAGE, pushImageListRequest,
                "/admin/pushimage/list", new ParameterizedTypeReference<>() {
                });
    }

    @ApiOperation(value = "Push 이미지 등록")
    @ApiResponse(code = 200, message = "Push 이미지 등록")
    @PostMapping("/setPushImage.json")
    @ResponseBody
    public ResponseObject<PushImageSetResult> setPushImage(
        @RequestParam(name = "imageName") final String imageName,
        HttpServletRequest request) {
        return ResourceConverter.toResponseObject(pushImageService.setPushImage(imageName, request));
    }
}
