package kr.co.homeplus.admin.web.promotion.model.discount;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountUseSearchSetDto {
    private long discountNo;
    private Long userNo;
    private String startDt;
    private String endDt;
}
