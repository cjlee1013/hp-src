package kr.co.homeplus.admin.web.core.utility;

import static kr.co.homeplus.admin.web.core.constants.DateTimeFormat.DATE_FORMAT_DEFAULT_YMD_HIS;
import static kr.co.homeplus.admin.web.core.constants.DateTimeFormat.DATE_FORMAT_MILL_YMD_HIS;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import kr.co.homeplus.admin.web.core.constants.DateTimeFormat;

public class DateTimeUtil {
    private static final ZoneId ZONE_ID = ZoneId.of(DateTimeFormat.TIME_ZONE_KST);

    /**
     * 현재 날짜/시간 가져오기
     * @return LocalDateTime 타입의 현재 날짜/시간 정보를 리턴합니다.
     */
    public static LocalDateTime getNow() {
        return LocalDateTime.now(ZONE_ID);
    }

    /**
     * 현재 날짜/시간 가져오기(yyyy-MM-dd HH:mm:ss)
     * @return 현재 날짜/시간을 리턴합니다.
     */
    public static String getNowYmdHis() {
        return getNow().format(getFormatter(DATE_FORMAT_DEFAULT_YMD_HIS));
    }

    /**
     * 현재 날짜/시간 가져오기(yyyy-MM-dd HH:mm:ss.SSS)
     * @return 현재 날짜/시간을 리턴합니다.
     */
    public static String getNowMillYmdHis() {
        return getNow().format(getFormatter(DATE_FORMAT_MILL_YMD_HIS));
    }

    /**
     * format String to Formatter
     * @param formatStr 날짜 패턴
     * @return 패턴이 적용된 {@link DateTimeFormatter}를 리턴합니다.
     */
    private static DateTimeFormatter getFormatter(String formatStr) {
        return DateTimeFormatter.ofPattern(formatStr);
    }

    /**
     * format String to Formatter
     * @param date1
     * @param date2
     * @param dateFormat
     * @return 패턴이 적용된 {@link DateTimeFormatter}를 리턴합니다.
     */
    public static boolean isDateBefore(final String date1, final String date2, String dateFormat) {
        final DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(dateFormat);
        final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern(dateFormat);
        final LocalDate dateTime1 = LocalDate.parse(date1, formatter1);
        final LocalDate dateTime2 = LocalDate.parse(date2, formatter2);
        return dateTime2.isBefore(dateTime1);
    }
}
