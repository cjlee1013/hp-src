package kr.co.homeplus.admin.web.manage.model.dspLeaflet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspLeafletItemCategorySetParamDto {

    private boolean getDb;
    private Integer leafletNo;
    private Integer cateNo;
    private String useYn;
    private int priority;

}
