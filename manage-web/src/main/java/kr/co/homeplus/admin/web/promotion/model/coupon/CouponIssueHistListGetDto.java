package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class CouponIssueHistListGetDto {
    private long couponNo;
    @RealGridColumnInfo(headText = "회원번호", width = 100, sortable = true)
    private String userNo;

    @RealGridColumnInfo(headText = "회원ID", width = 150, sortable = true)
    private String userId;

    @RealGridColumnInfo(headText = "회원명", width = 100, sortable = true)
    private String userNm;

    @RealGridColumnInfo(headText = "난수번호", width = 200, sortable = true)
    private String randomNo;

    @RealGridColumnInfo(headText = "발급번호", width = 150, sortable = true)
    private long issueNo;

    @RealGridColumnInfo(headText = "발급일", width = 180, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, sortable = true)
    private String issueDt;

    @RealGridColumnInfo(headText = "주문번호", width = 100, sortable = true)
    private Long purchaseOrderNo;

    @RealGridColumnInfo(headText = "취소여부", width = 100, sortable = true)
    private String cancelYn;

    @RealGridColumnInfo(headText = "이전발급번호", width = 100, sortable = true)
    private Long preIssueNo;

    @RealGridColumnInfo(headText = "사용여부", width = 100, sortable = true)
    private String couponUseYn;

    @RealGridColumnInfo(headText = "사용일", width = 180, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, sortable = true)
    private String couponUseDt;

    @RealGridColumnInfo(headText = "발급주체", width = 100, sortable = true)
    private String regNm;

    @RealGridColumnInfo(headText = "회수여부", width = 100, sortable = true)
    private String withdrawYn;
    private String reissueYn;
    private String regDt;
    private String regId;
    private String chgId;
    @RealGridColumnInfo(headText = "처리자", width = 100, sortable = true)
    private String chgNm;
    @RealGridColumnInfo(headText = "처리일", width = 180, columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, sortable = true)
    private String chgDt;
}
