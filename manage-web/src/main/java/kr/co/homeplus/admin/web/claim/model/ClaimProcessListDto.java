package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimProcessListDto {

    @ApiModelProperty(value = "구분", position = 1)
    public String processType;

    @ApiModelProperty(value = "처리일시", position = 2)
    public String processDt;

    @ApiModelProperty(value = "처리채널", position = 3)
    public String regChannel;

    @ApiModelProperty(value = "처리자", position = 4)
    public String regId;
}
