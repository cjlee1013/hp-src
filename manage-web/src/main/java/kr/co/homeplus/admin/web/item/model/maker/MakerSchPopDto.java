package kr.co.homeplus.admin.web.item.model.maker;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class MakerSchPopDto {

    @RealGridColumnInfo(headText = "제조사 코드", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.SERIAL, sortable = true)
    private Long makerNo;

    @RealGridColumnInfo(headText = "제조사명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true, width = 250)
    private String makerNm;

}
