package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class StickerItemExcelDto {
    @EqualsAndHashCode.Include
    private String itemNo;
}