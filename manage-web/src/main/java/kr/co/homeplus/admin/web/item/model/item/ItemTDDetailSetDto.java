package kr.co.homeplus.admin.web.item.model.item;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 TD정보 Set Entry
 */
@Getter
@Setter
public class ItemTDDetailSetDto {

	//유입채널 (등록/수정) : (ADMIN: 어드민, PARTNER: 파트너, INBOUND: 인바운드)
	private String channel;

	/**
	 * 기본정보
	 */
	//상품번호
	private String itemNo;

	//거래유형 (DS : 업체상품 , TD : 매장상품)
	private String mallType;

	//등록/수정자
	private String userId;

	/**
	 * 판매정보
	 */

	//판매기간시작일
	private String saleStartDt;

	//판매기간종료일
	private String saleEndDt;

	//판매기간 기준-설정함/설정안함(Y/N)
	private String salePeriodYn;

	//온라인재고설정 -설정함/설정안함(Y/N)
	private String onlineStockYn;

	//온라인재고설정시작일
	private String onlineStockStartDt;

	//온라인재고설정종료일
	private String onlineStockEndDt;

	//예약판매사용여부
	private String rsvYn;

	//예약 판매정보
	private ItemRsvSetDto rsv;

	//판매단위
	private Integer saleUnit;

	//구매제한 - 최소구매수량
	private Integer purchaseMinQty;

	//1인당 구매제한 사용여부 - 미사용:Y / 사용:N
	private String purchaseLimitYn;

	//구매제한 타입 - 1회 : O / 기간제한 : P
	private String purchaseLimitDuration;

	//구매제한 일자 - ?일
	private Integer purchaseLimitDay;

	//구매제한 개수 - ?개
	private Integer purchaseLimitQty;

	//점포ID
	private int storeId;

	//점포유형
	private String storeType;

	/**
	 * 옵션정보
	 */
	//옵션리스트
	List<ItemOptionValueGetDto> optList;

	/**
	 * 점포상세정보
	 */
	List<ItemStoreListGetDto> storeList;

	//안전번호 여부
	private String  safeNumberUseYn;

}
