package kr.co.homeplus.admin.web.settle.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.settle.model.SettleCycleType;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettleDetail.PartnerSettleItemSetDto;
import kr.co.homeplus.admin.web.settle.model.partnerSettle.PartnerSettleListSetDto;
import kr.co.homeplus.admin.web.settle.service.PartnerSettleService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/settle/partnerSettleDetail")
public class PartnerSettleDetailController {

    private final PartnerSettleService partnerSettleService;
    private final SettleCommonService settleCommonService;
    private final CodeService codeService;

    public PartnerSettleDetailController(PartnerSettleService partnerSettleService, SettleCommonService settleCommonService, CodeService codeService) {
        this.partnerSettleService = partnerSettleService;
        this.settleCommonService = settleCommonService;
        this.codeService = codeService;
    }

    /**
     * 정산관리 > 판매자 정산정보 (SETTLE-16)
     * @param model
     * @return String
     */
    @RequestMapping(value = "/partnerSettleDetail", method = RequestMethod.GET)
    public String partnerSettleDetail(Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("settleCycleType", Arrays.asList(SettleCycleType.values()));
        model.addAttribute("partnerId", request.getParameter("partnerId"));
        model.addAttribute("settleSrl", request.getParameter("settleSrl"));

        model.addAllAttributes(RealGridHelper.create("partnerSettleGridBaseInfo", PartnerSettleDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("partnerSettleDetailBaseInfo", PartnerSettleItemGetDto.class));

        return "/settle/partnerSettleDetail";
    }

    /**
     * 정산관리 > 판매자 정산 정보
     * @return PartnerSettleListGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerSettleList.json", method = RequestMethod.GET)
    public List<PartnerSettleDetailGetDto> getPartnerSettleList(PartnerSettleListSetDto listParamDto) throws Exception {
        return partnerSettleService.getPartnerSettleDetail(listParamDto);
    }

    /**
     * 정산관리 > 판매자 정산 정보 상세
     * @return PartnerSettleItemGetDto
     */
    @ResponseBody
    @RequestMapping(value = "/getPartnerSettleItem.json", method = RequestMethod.GET)
    public List<PartnerSettleItemGetDto> getPartnerSettleProd(PartnerSettleItemSetDto listParamDto) throws Exception {
        return partnerSettleService.getPartnerSettleItem(listParamDto);
    }
}