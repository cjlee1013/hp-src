package kr.co.homeplus.admin.web.message.model;

import java.time.LocalDateTime;
import kr.co.homeplus.admin.web.message.enums.MessageSendResult;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class MailSendListGrid {

    @RealGridColumnInfo(hidden = true)
    private long mailListSeq;
    @RealGridColumnInfo(hidden = true)
    private long mailSendHistorySeq;
    @RealGridColumnInfo(headText = "수신 이메일")
    private String recipient;
    @RealGridColumnInfo(headText = "결과", columnType = RealGridColumnType.LOOKUP)
    private MessageSendResult sendResultCd;
    @RealGridColumnInfo(headText = "발송 에러 메시지")
    private String sendErrorMessage;
    @RealGridColumnInfo(headText = "등록일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime regDt;
    @RealGridColumnInfo(headText = "발송일시", columnType = RealGridColumnType.DATETIME_ISO, fieldType = RealGridFieldType.DATETIME)
    private LocalDateTime sendDt;
    @RealGridColumnInfo(headText = "비고")
    private String identifier;
}
