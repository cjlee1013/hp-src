package kr.co.homeplus.admin.web.item.model.item;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class VirtualStoreGetDto {
    @RealGridColumnInfo(headText = "택배점포ID", sortable = true)
    private int storeId;

    @RealGridColumnInfo(headText = "택배점포명", sortable = true, width = 150)
    private String storeNm;

    @RealGridColumnInfo(headText = "택배점포유형", sortable = true)
    private String storeType;

    @RealGridColumnInfo(headText = "사용여부", sortable = true)
    private Character useYn;

    @RealGridColumnInfo(headText = "기준점", sortable = true)
    private int originStoreId;

    @RealGridColumnInfo(headText = "기준점포명", sortable = true, width = 150)
    private String originStoreNm;

    @RealGridColumnInfo(headText = "기준점포유형", sortable = true)
    private String originStoreType;
}
