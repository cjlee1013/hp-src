package kr.co.homeplus.admin.web.common.model.pop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
public class ItemPopupOptionListGetDto {
	@RealGridColumnInfo(headText = "상품번호", sortable = true, width = 50)
	private String itemNo;

	@RealGridColumnInfo(headText = "상품명", sortable = true, width = 50)
	private String itemNm1;

	@RealGridColumnInfo(headText = "용도옵션", hidden = true)
	private String optSelUseYn;

	@RealGridColumnInfo(headText = "용도옵션", sortable = true, width = 50)
	private String optSelUseYnTxt;

	@RealGridColumnInfo(headText = "리스트분할", sortable = true, width = 50)
	private String listSplitDispYn;

	@RealGridColumnInfo(headText = "옵션번호", sortable = true, width = 50)
	private String optNo;

	@RealGridColumnInfo(headText = "옵션값", sortable = true, width = 50)
	private String opt1Val;

	@RealGridColumnInfo(headText = "대분류", sortable = true, width = 50)
	private String lcateNm;

	@RealGridColumnInfo(headText = "중분류", sortable = true, width = 50)
	private String mcateNm;

	@RealGridColumnInfo(headText = "소분류", sortable = true, width = 50)
	private String scateNm;

	@RealGridColumnInfo(headText = "세분류", sortable = true, width = 50)
	private String dcateNm;

	@RealGridColumnInfo(headText = "등록자", sortable = true, width = 50)
	private String regNm;

	@RealGridColumnInfo(headText = "등록일", sortable = true, width = 50)
	private String regDt;
}
