package kr.co.homeplus.admin.web.partner.model.affiliate;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AffiliateChannelHistSetParamDto {

    private String channelId;
    private BigDecimal commissionRate;
    private int cookieTime;
    private String regId;

}
