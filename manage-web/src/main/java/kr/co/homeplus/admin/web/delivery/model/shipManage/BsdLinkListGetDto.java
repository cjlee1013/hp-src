package kr.co.homeplus.admin.web.delivery.model.shipManage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "배송관리 > 배송관리 > BSD연동조회 리스트 응답 DTO")
public class BsdLinkListGetDto {
  @ApiModelProperty(notes = "순번")
  private String bsdLinkNo;

  @ApiModelProperty(notes = "주문번호")
  private String purchaseOrderNo;

  @ApiModelProperty(notes = "주문일자")
  private String orderDt;

  @ApiModelProperty(notes = "희망발송일")
  private String reserveShipDt;

  @ApiModelProperty(notes = "배송유형")
  private String sendPlaceNm;

  @ApiModelProperty(notes = "배송유형코드")
  private String sendPlace;

  @ApiModelProperty(notes = "BSD연동일자")
  private String linkDt;

  @ApiModelProperty(notes = "BSD연동결과")
  private String linkResultNm;

  @ApiModelProperty(notes = "BSD연동결과코드")
  private String linkResult;

  @ApiModelProperty(notes = "BSD체크결과")
  private String checkResultNm;

  @ApiModelProperty(notes = "BSD체크결과코드")
  private String checkResult;

  @ApiModelProperty(notes = "배송상태")
  private String shipStatusNm;

  @ApiModelProperty(notes = "배송상태코드")
  private String shipStatus;

  @ApiModelProperty(notes = "주문자명")
  private String buyerNm;

  @ApiModelProperty(notes = "수취인")
  private String receiverNm;

  @ApiModelProperty(notes = "상품명")
  private String itemNm1;

  @ApiModelProperty(notes = "주소")
  private String shipAddr;

  @ApiModelProperty(notes = "박스번호")
  private String dlvRefitSeq;

  @ApiModelProperty(notes = "택배사")
  private String dlvCdNm;

  @ApiModelProperty(notes = "택배사코드")
  private String dlvCd;

  @ApiModelProperty(notes = "송장번호")
  private String invoiceNo;
}