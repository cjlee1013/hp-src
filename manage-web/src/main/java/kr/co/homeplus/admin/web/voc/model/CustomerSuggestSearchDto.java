package kr.co.homeplus.admin.web.voc.model;

import lombok.Data;

@Data
public class CustomerSuggestSearchDto {

    private String schStartDate;

    private String schEndDate;

    private String osType;
}
