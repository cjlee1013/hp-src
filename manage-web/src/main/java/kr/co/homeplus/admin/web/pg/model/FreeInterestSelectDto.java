package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단 관리 > 무이자 혜택 관리 search DTO")
public class FreeInterestSelectDto {
    @ApiModelProperty(value = "카드무이자관리순번")
    private long schFreeInterestMngSeq;
    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    private String schSiteType;
    @ApiModelProperty(value = "카드무이자관리명")
    private String schFreeInterestNm;
    @ApiModelProperty(value = "적용시작일자")
    private String schApplyStartDt;
    @ApiModelProperty(value = "적용종료일자")
    private String schApplyEndDt;
    @ApiModelProperty(value = "사용여부")
    private String schUseYn;
    @ApiModelProperty(value = "적용순번")
    private long schApplySeq;
    @ApiModelProperty(value = "수단코드(카드사 별 카드종류)")
    private String schMethodCd;
}
