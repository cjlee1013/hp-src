package kr.co.homeplus.admin.web.item.model.commission;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommissionParamDto {
    private String partnerId;
    private String dcateCd;
}
