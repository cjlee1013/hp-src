package kr.co.homeplus.admin.web.promotion.model.coupon;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true, indicator = true)
public class CouponExceptItemDto {
    @RealGridColumnInfo(headText = "제외 상품번호", width = 80, sortable = true)
    private String itemNo;
    @RealGridColumnInfo(headText = "제외 상품명", width = 300, columnType = RealGridColumnType.NAME, sortable = true)
    private String itemNm;
    private String regNm;
    private String regDt;
}