package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WordRegDto {
    private Integer wordId;
    private String wordName;
    private String senseTag;
    private boolean useFlag;
    private String note;
    private String dicType;
    private String pos;
    private String nounAttr;
    private String verbAttr;
    private String neType;
    private String domain;
    private String language;
    private boolean dontSplitComp;
    private String description;
    private String example;
}
