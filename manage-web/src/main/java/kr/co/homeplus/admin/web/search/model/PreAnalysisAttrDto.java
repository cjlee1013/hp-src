package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreAnalysisAttrDto {
    private String preType;
    private String sufType;
    private String subType;
}
