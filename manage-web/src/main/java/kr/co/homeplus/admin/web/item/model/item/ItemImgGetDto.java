package kr.co.homeplus.admin.web.item.model.item;

import lombok.Getter;
import lombok.Setter;

/**
 * 이미지 정보
 */
@Getter
@Setter
public class ItemImgGetDto {
	//이미지 번호
	private Long imgNo;

	//대표이미지 여부
	private String	mainYn;

	//이미지 URL
	private String 	imgUrl;

	//이미지명
	private String 	imgNm;

	//수정여부
	private String 	changeYn;

	//이미지 유형 (MN: 메인, LST: 리스트)
	private String 	imgType;

	private Integer priority;
}
