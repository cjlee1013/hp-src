package kr.co.homeplus.admin.web.order.model.orderSearch;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문정보 상세 - 배송정보 DTO")
public class OrderSearchShipDto {

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value = "상품 주문번호")
    private long orderItemNo;

    @ApiModelProperty(value = "배송상태")
    private String shipStatusNm;

    @ApiModelProperty(value = "미수취신고상태")
    private String notReceiveType;

    @ApiModelProperty(value = "수령인")
    private String receiverNm;

    @ApiModelProperty(value = "수령인연락처")
    private String shipMobileNo;

    @ApiModelProperty(value = "안심번호")
    private String issuePhoneNo;

    @ApiModelProperty(value = "주소")
    private String shippingAddr;

    @ApiModelProperty(value = "판매자아이디", hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "판매자")
    private String partnerNm;

    @ApiModelProperty(value = "배송방법")
    private String shipMethodNm;

    @ApiModelProperty(value = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(value = "택배사명")
    private String dlvNm;

    @ApiModelProperty(value = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(value = "발송기한")
    private String orgShipDt;

    @ApiModelProperty(value = "2차발송기한")
    private String delayShipDt;

    @ApiModelProperty(value = "배송요청일")
    private String scheduleShipDt;

    @ApiModelProperty(value = "배송요청일")
    private String shipDt;

    @ApiModelProperty(value = "주문확인일")
    private String confirmDt;

    @ApiModelProperty(value = "발송처리일")
    private String shippingDt;

    @ApiModelProperty(value = "배송요청시간")
    private String shipTime;

    @ApiModelProperty(value = "배송완료일")
    private String completeDt;

    @ApiModelProperty(value = "배송상태코드", hidden = true)
    private String shipStatus;

    @ApiModelProperty(value = "배송방법코드", hidden = true)
    private String shipMethod;

    @ApiModelProperty(value = "클레임상태", hidden = true)
    private String claimStatus;

    @ApiModelProperty(value = "점포종류(NOR:일반점,DLV:택배점)", hidden = true)
    private String storeKind;

    @ApiModelProperty(value = "고객번호", hidden = true)
    private String userNo;

    @ApiModelProperty(value = "운송번호", hidden = true)
    private String shipNo;

    @ApiModelProperty(value = "주문수량(클레임건차감)", hidden = true)
    private String itemQty;

    @ApiModelProperty(value = "상품배송가능지역", hidden = true)
    private String shipAreaType;

    @ApiModelProperty(value = "배송기사명", hidden = true)
    private String driverNm;

    @ApiModelProperty(value = "배송기사연락처", hidden = true)
    private String driverTelNo;

    @ApiModelProperty(value = "옴니주문번호")
    private String purchaseStoreInfoNo;

    @ApiModelProperty(value = "단축번호")
    private String sordNo;

    @ApiModelProperty(value = "슬롯변경횟수")
    private int slotChgCnt;

    @ApiModelProperty(value = "다중배송번호")
    private String multiBundleNo;

    @ApiModelProperty(value = "애니타입슬롯여부")
    private String anytimeSlotYn;
}
