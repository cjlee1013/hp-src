package kr.co.homeplus.admin.web.manage.controller;

import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.manage.model.faq.FaqListParamDto;
import kr.co.homeplus.admin.web.manage.model.faq.FaqListSelectDto;
import kr.co.homeplus.admin.web.manage.model.faq.FaqSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/manage/faq")
public class FaqController {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;
    private final CodeService codeService;

    @Value("${plus.resource-routes.image.url}")
    private String homeImgUrl;

    @Value("${plus.resource-routes.imageFront.url}")
    private String hmpImgFrontUrl;

    /**
     * 사이트관리 > FAQ 관리 > 사이트 FAQ
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/faqFrontMain", method = RequestMethod.GET)
    public String faqFrontdMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                    "use_yn"            // 사용여부
                ,	"faq_class_front"   // faq 분류
                ,   "faq_search_type"   // faq 검색타입
        );

        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("faqClass", code.get("faq_class_front"));
        model.addAttribute("searchType", code.get("faq_search_type"));
        model.addAttribute("dispArea", "FRONT");
        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("faqListGridBaseInfo", FaqListSelectDto.class));

        return "/manage/faqMain";
    }

    /**
     * 사이트관리 > FAQ 관리 > 파트너 FAQ
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/faqPartnerMain", method = RequestMethod.GET)
    public String faqPartnerMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
                    "use_yn"              // 사용여부
                ,	"faq_class_partner"   // faq 분류
                ,   "faq_search_type"     // faq 검색타입
        );


        model.addAttribute("useYn", code.get("use_yn"));
        model.addAttribute("faqClass", code.get("faq_class_partner"));
        model.addAttribute("searchType", code.get("faq_search_type"));
        model.addAttribute("dispArea", "PARTNER");
        model.addAttribute("homeImgUrl", this.homeImgUrl);
        model.addAttribute("hmpImgFrontUrl", this.hmpImgFrontUrl);

        model.addAllAttributes(RealGridHelper.create("faqListGridBaseInfo", FaqListSelectDto.class));

        return "/manage/faqMain";
    }

    /**
     * 사이트관리 > FAQ 관리 > FAQ 리스트
     *
     * @param listParamDto
     * @return FaqListSelectDto
     */
    @ResponseBody
    @RequestMapping(value = {"/getFaqList.json"}, method = RequestMethod.GET)
    public List<FaqListSelectDto> getFaqList(FaqListParamDto listParamDto) {
        String apiUri = "/manage/faq/getFaqList";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<FaqListSelectDto>>>() {};
        return (List<FaqListSelectDto>) resourceClient.getForResponseObject(ResourceRouteName.MANAGE, StringUtil.getRequestString(apiUri, FaqListParamDto.class, listParamDto), typeReference).getData();
    }


    /**
     * 사이트관리 > FAQ 관리 > FAQ 등록/수정
     *
     * @param faqSetParamDto
     * @return ResponseResult
     */
    @ResponseBody
    @RequestMapping(value = {"/setFaq.json"}, method = RequestMethod.POST)
    public ResponseResult setFaq(FaqSetParamDto faqSetParamDto) {
        String apiUri = "/manage/faq/setFaq";
        faqSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.MANAGE, faqSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

}