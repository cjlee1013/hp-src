package kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailyOrderInfoPartnerSetDto {
  @ApiModelProperty(notes = "판매업체")
  private String partnerId;
  @ApiModelProperty(notes = "시작일")
  private String startDt;
  @ApiModelProperty(notes = "종료일")
  private String endDt;
}
