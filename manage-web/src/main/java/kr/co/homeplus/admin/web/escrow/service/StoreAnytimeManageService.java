package kr.co.homeplus.admin.web.escrow.service;

import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_STORE_ANYTIME_INFO;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_GET_STORE_ANYTIME_SHIFT_INFO;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_SAVE_STORE_ANYTIME_SHIFT_USE_YN;
import static kr.co.homeplus.admin.web.core.constants.EscrowConstants.ESCROW_SAVE_STORE_ANYTIME_USE_YN;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeManageDto;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeManageSelectDto;
import kr.co.homeplus.admin.web.escrow.model.StoreAnytimeShiftManageDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreAnytimeManageService {
    private final ResourceClient resourceClient;

    /**
     * 점포 애니타임 정보 조회
     * @param storeAnytimeManageSelectDto
     * @return
     */
    public List<StoreAnytimeManageDto> getStoreAnytimeInfo(StoreAnytimeManageSelectDto storeAnytimeManageSelectDto) throws Exception {
        String apiUri = ESCROW_GET_STORE_ANYTIME_INFO;
        ResourceClientRequest<List<StoreAnytimeManageDto>> request = ResourceClientRequest.<List<StoreAnytimeManageDto>>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(storeAnytimeManageSelectDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 점포 애니타임 Shift 정보 조회
     * @param storeId
     * @return
     * @throws Exception
     */
    public List<StoreAnytimeShiftManageDto> getStoreAnytimeShiftInfo(int storeId) throws Exception {
        List<SetParameter> setParameterList = new ArrayList<>();
        setParameterList.add(SetParameter.create("storeId", storeId));
        String apiUri = ESCROW_GET_STORE_ANYTIME_SHIFT_INFO + StringUtil.getParameter(setParameterList);
        ResourceClientRequest<List<StoreAnytimeShiftManageDto>> request = ResourceClientRequest.<List<StoreAnytimeShiftManageDto>>getBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 점포 애니타임 사용여부 저장
     * @param storeAnytimeManageDto
     * @return
     * @throws Exception
     */
    public Integer saveStoreAnytimeUseYn(StoreAnytimeManageDto storeAnytimeManageDto) throws Exception {
        String apiUri = ESCROW_SAVE_STORE_ANYTIME_USE_YN;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(storeAnytimeManageDto)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }

    /**
     * 점포 애니타임 Shift 사용여부 저장
     * @param storeAnytimeShiftManageDtoList
     * @return
     * @throws Exception
     */
    public Integer saveStoreAnytimeShiftUseYn(List<StoreAnytimeShiftManageDto> storeAnytimeShiftManageDtoList) throws Exception {
        String apiUri = ESCROW_SAVE_STORE_ANYTIME_SHIFT_USE_YN;
        ResourceClientRequest<Integer> request = ResourceClientRequest.<Integer>postBuilder()
                .apiId(ResourceRouteName.ESCROWMNG)
                .uri(apiUri)
                .postObject(storeAnytimeShiftManageDtoList)
                .typeReference(new ParameterizedTypeReference<>() {})
                .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody().getData();
    }
}
