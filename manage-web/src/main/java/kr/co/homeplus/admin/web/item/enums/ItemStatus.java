package kr.co.homeplus.admin.web.item.enums;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ItemStatus {

        T( "T", "임시저장")
    ,   A("A", "판매중")
    ,   P("P", "일시중지")
    ,   S("S", "영구중지")
    ,   E("E", "판매종료/취급중지")
    ;

    @Getter
    private final String type;

    @Getter
    private final String description;

    @Getter
    private static final ImmutableBiMap<ItemStatus, String> biMap;

    static {
        BiMap<ItemStatus, String> map = EnumHashBiMap.create(ItemStatus.class);

        for (ItemStatus policy : values()) {
            map.put(policy, policy.name());
        }

        biMap = ImmutableBiMap.copyOf(map);
    }

    public static String valueDescFor(final String type) {
        final ItemStatus policy =  biMap.inverse().get(type);

        return policy.description;
    }
}
