package kr.co.homeplus.admin.web.search.service;

import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.PopularManagementSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PopularKeywordService {

    private final ResourceClient resourceClient;

    public ResponseObject getPopularDisplayNm(String siteType, String popularType, String dateType) {
        String apiUri = "/popular/getPopularDisplayNm?siteType="+siteType+"&popularType="+popularType;
        if(StringUtils.isNotEmpty(dateType)){
            apiUri += "&dateType="+dateType;
        }
        return getResponseObject(apiUri);
    }
    public ResponseObject getPopularDisplayNmGroup(String siteType, String popularType, String dateType, String applyDt) {
        String apiUri = "/popular/getPopularDisplayNmGroup?siteType="+siteType+"&popularType="+popularType+"&applyDt="+applyDt;
        if(StringUtils.isNotEmpty(dateType)){
            apiUri += "&dateType="+dateType;
        }
        return getResponseObject(apiUri);
    }
    public ResponseObject getPopularKeyword(String popularType, String groupNo) {
        String apiUri = "/popular/getPopularKeyword?popularType="+popularType+"&groupNo="+groupNo;
        return getResponseObject(apiUri);
    }
    public ResponseObject getPopularManagement(String siteType, String type, String date) {
        String apiUri = "/popular/getPopularManagement?siteType="+siteType+"&type="+type;
        if(StringUtils.isNotEmpty(date)){
            apiUri += "&date=" + date;
        }
        return getResponseObject(apiUri);
    }
    public ResponseObject putPopularManagement(PopularManagementSetDto param) {
        String apiUri = "";
        if (param.getManagementNo() == null) {
            apiUri = "/popular/insertPopularManagement";
        } else {
            apiUri = "/popular/updatePopularManagement";
        }
        return getResponseObject(apiUri, param);
    }
    public ResponseObject deletePopularManagement(String managementNo) {
        String apiUri = "/popular/deletePopularManagement?managementNo="+managementNo;
        return getResponseObject(apiUri);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}