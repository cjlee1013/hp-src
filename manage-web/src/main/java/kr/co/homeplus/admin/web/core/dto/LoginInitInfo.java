package kr.co.homeplus.admin.web.core.dto;

import lombok.Getter;

@Getter
public class LoginInitInfo {
    /**
     * 아이디 저장여부
     */
    private boolean chkSaveUserId;
    /**
     * 저장된 아이디
     */
    private String saveUserId;

    public LoginInitInfo(final boolean chkSaveUserId, final String saveUserId) {
        this.chkSaveUserId = chkSaveUserId;
        this.saveUserId = saveUserId;
    }

    public LoginInitInfo() {
        saveUserId = "";
        chkSaveUserId = false;
    }
}
