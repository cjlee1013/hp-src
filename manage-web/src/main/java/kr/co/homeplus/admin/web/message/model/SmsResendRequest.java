package kr.co.homeplus.admin.web.message.model;

import lombok.Data;

@Data
public class SmsResendRequest {

    private long smsWorkSeq;

    private long smsListSeq;

    private long smsSendHistorySeq;
}
