package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchHistoryModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchHistorySelectModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchManageModel;
import kr.co.homeplus.admin.web.escrow.model.OrderBatchManageSelectModel;
import kr.co.homeplus.admin.web.escrow.service.OrderBatchManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 시스템관리 > 시스템관리 > 주문 배치 관리
 */
@Controller
@RequestMapping("/escrow/orderBatch")
public class OrderBatchManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final OrderBatchManageService orderBatchManageService;

    public OrderBatchManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            OrderBatchManageService orderBatchManageService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.orderBatchManageService = orderBatchManageService;
    }

    /**
     * 주문 배치 관리 메인페이지 호출
     */
    @GetMapping("/orderBatchManageMain")
    public String orderBatchManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_SYSTEM_ESCROW)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("orderBatchManageGridBaseInfo", OrderBatchManageModel.class));
        model.addAllAttributes(RealGridHelper.create("orderBatchHistoryGridBaseInfo", OrderBatchHistoryModel.class));
        model.addAttribute("empId", loginCookieService.getUserInfo().getEmpId());
        return "/escrow/orderBatchManageMain";
    }

    /**
     * 주문배치관리 마스터 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getOrderBatchManageList.json")
    public List<OrderBatchManageModel> getOrderBatchManageList(@ModelAttribute OrderBatchManageSelectModel orderBatchManageSelectModel) throws Exception {
        return orderBatchManageService.getOrderBatchManageList(orderBatchManageSelectModel);
    }

    /**
     * 주문배치관리 마스터 저장
     */
    @ResponseBody
    @PostMapping("/saveOrderBatchManage.json")
    public ResponseObject<Object> saveOrderBatchManage(@RequestBody OrderBatchManageModel orderBatchManageModel) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        orderBatchManageModel.setRegId(userCd);
        orderBatchManageModel.setChgId(userCd);
        return orderBatchManageService.saveOrderBatchManage(orderBatchManageModel);
    }

    /**
     * 주문배치관리 마스터 삭제
     */
    @ResponseBody
    @PostMapping("/deleteOrderBatchManage.json")
    public ResponseObject<Object> deleteOrderBatchManage(@RequestBody OrderBatchManageModel orderBatchManageModel) throws Exception {
        return orderBatchManageService.deleteOrderBatchManage(orderBatchManageModel);
    }

    /**
     * 주문배치히스토리 리스트 조회
     */
    @ResponseBody
    @RequestMapping("/getOrderBatchHistoryList.json")
    public List<OrderBatchHistoryModel> getOrderBatchHistoryList(@ModelAttribute OrderBatchHistorySelectModel orderBatchHistorySelectModel) throws Exception {
        return orderBatchManageService.getOrderBatchHistoryList(orderBatchHistorySelectModel);
    }

    /**
     * LGW 배치이력 목록조회 (프로시저 이력조회)
     */
    @ResponseBody
    @RequestMapping("/getLgwBatchHistoryList.json")
    public List<OrderBatchHistoryModel> getLgwBatchHistoryList(@ModelAttribute OrderBatchHistorySelectModel orderBatchHistorySelectModel) throws Exception {
        return orderBatchManageService.getLgwBatchHistoryList(orderBatchHistorySelectModel);
    }
}
