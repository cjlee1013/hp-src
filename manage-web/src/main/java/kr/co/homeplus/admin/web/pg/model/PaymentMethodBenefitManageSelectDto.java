package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "결제관리 > 결제수단관리 > 결제수단별 혜택관리 Search DTO")
@Data
public class PaymentMethodBenefitManageSelectDto {
    @ApiModelProperty(notes = "노출 시작기간", required = true)
    private String schApplyStartDt;
    @ApiModelProperty(notes = "노출 종료기간", required = true)
    private String schApplyEndDt;
    @ApiModelProperty(notes = "제목", required = true)
    private String schBenefitTitle;
    @ApiModelProperty(notes = "사이트유형(ALL:전체,HOME:홈플러스,CLUB:더클럽)", required = true)
    private String schSiteType;
    @ApiModelProperty(notes = "사용여부(Y:사용,N:미사용)", required = true)
    private String schUseYn;
}
