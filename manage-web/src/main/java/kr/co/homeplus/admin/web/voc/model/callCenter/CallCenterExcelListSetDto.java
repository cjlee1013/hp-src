package kr.co.homeplus.admin.web.voc.model.callCenter;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 엑셀 리스트 요청 DTO")
public class CallCenterExcelListSetDto extends CallCenterListSetDto{

}