package kr.co.homeplus.admin.web.partner.model.sellerShop;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
@Getter
@Setter
public class SellerShopListSelectDto {

    @RealGridColumnInfo(headText = "셀러샵 이름", width = 120)
    private String shopNm;

    @RealGridColumnInfo(headText = "셀러샵 URL", order = 1)
    private String shopUrl;

    @RealGridColumnInfo(headText = "판매업체 ID", width = 120, order = 2)
    private String partnerId;

    @RealGridColumnInfo(headText = "업체명", order = 3)
    private String businessNm;

    @RealGridColumnInfo(headText = "상호", order = 4)
    private String partnerNm;

    @RealGridColumnInfo(headText = "사업자등록번호", order = 5)
    private String partnerNo;

    @RealGridColumnInfo(headText = "회원상태", width = 80, order = 6)
    private String partnerStatusNm;

    @RealGridColumnInfo(headText = "등록자", order = 7)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, width = 120, order = 8)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 9)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, width = 120, order = 10)
    private String chgDt;

    @RealGridColumnInfo(headText = "고객센터 정보", hidden = true, order = 11)
    private String csInfo;

    @RealGridColumnInfo(headText = "고객센터 정보 노출 여부", hidden = true, order = 12)
    private String csUseYn;

    @RealGridColumnInfo(headText = "셀러샵 소개", hidden = true, order = 13)
    private String shopInfo;

    @RealGridColumnInfo(headText = "파트너몰 사용 여부", hidden = true, order = 14)
    private String useYn;

    @RealGridColumnInfo(headText = "셀러샵 프로필 이미지", hidden = true, order = 15)
    private String shopProfileImg;

    @RealGridColumnInfo(headText = "셀러샵 로고", hidden = true, order = 16)
    private String shopLogoImg;


    private String shopLogoYn;
    private String shopDispType;
    private String useYnTxt;
    private String partnerStatus;

}
