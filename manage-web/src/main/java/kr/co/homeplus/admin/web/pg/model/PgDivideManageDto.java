package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class PgDivideManageDto {
    @ApiModelProperty(value = "사이트유형명")
    @RealGridColumnInfo(headText = "사이트", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteTypeNm;

    @ApiModelProperty(value = "플랫폼(PC,모바일)")
    @RealGridColumnInfo(headText = "디바이스", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String platform;

    @ApiModelProperty(value = "상위결제수단명")
    @RealGridColumnInfo(headText = "결제수단", width = 120, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodNm;

    @ApiModelProperty(value = "inicis비율")
    @RealGridColumnInfo(headText = "INICIS", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer inicisRate;

    @ApiModelProperty(value = "tosspg비율")
    @RealGridColumnInfo(headText = "TOSSPG", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Integer tosspgRate;

    @ApiModelProperty(value = "수정일자")
    @RealGridColumnInfo(headText = "수정일", width = 200, sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;

    @ApiModelProperty(value = "수정자id")
    @RealGridColumnInfo(headText = "수정자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;

    @ApiModelProperty(value = "사이트유형(HOME:홈플러스,CLUB:더클럽)")
    @RealGridColumnInfo(headText = "siteType", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;

    @ApiModelProperty(value = "상위결제수단코드(신용카드,휴대폰결제등)")
    @RealGridColumnInfo(headText = "parentMethodCd", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String parentMethodCd;

    @ApiModelProperty(value = "inicismid")
    @RealGridColumnInfo(headText = "inicisMid", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String inicisMid;

    @ApiModelProperty(value = "tosspgmid")
    @RealGridColumnInfo(headText = "tosspgMid", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String tosspgMid;

    @ApiModelProperty(value = "배분순번")
    @RealGridColumnInfo(headText = "divideSeq", hidden = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private Long divideSeq;

    @ApiModelProperty(value = "배분명(기본배분율)")
    private String divideNm;
    @ApiModelProperty(value = "결제수단코드(삼성카드,현대카드,무통장등)")
    private String methodCd;
    @ApiModelProperty(value = "결제수단명")
    private String methodNm;
    @ApiModelProperty(value = "정책번호(pg_mng_type:POLICY이경우만 입력)")
    private Long policyNo;
    @ApiModelProperty(value = "pg관리유형(기본배분율:BASIC,기간배분율:PERIOD,결제정책배분율:POLICY)")
    private String pgMngType;
    @ApiModelProperty(value = "kakaopaymid")
    private String kakaoMid;
    @ApiModelProperty(value = "kakaopay비율")
    private Integer kakaoRate;
    @ApiModelProperty(value = "kakaomoneymid")
    private String kakaoMoneyMid;
    @ApiModelProperty(value = "kakaomoney비율")
    private Integer kakaoMoneyRate;
    @ApiModelProperty(value = "naverpaymid")
    private String naverMid;
    @ApiModelProperty(value = "naverpay비율")
    private Integer naverRate;
    @ApiModelProperty(value = "samsungpaymid")
    private String samsungMid;
    @ApiModelProperty(value = "samsungpay비율")
    private Integer samsungRate;
    @ApiModelProperty(value = "적용시작일자")
    private String applyStartDt;
    @ApiModelProperty(value = "적용종료일자")
    private String applyEndDt;
    @ApiModelProperty(value = "우선순위(기본배분율:8000,기간별배분율:5000)")
    private Integer priority;
    @ApiModelProperty(value = "사용여부")
    private String useYn;
    @ApiModelProperty(value = "등록자id")
    private String regId;
    @ApiModelProperty(value = "등록일자")
    private String regDt;
}