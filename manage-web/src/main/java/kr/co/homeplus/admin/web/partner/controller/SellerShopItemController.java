package kr.co.homeplus.admin.web.partner.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.partner.model.sellerShopItem.SellerShopItemGetDto;
import kr.co.homeplus.admin.web.partner.model.sellerShopItem.SellerShopItemListSelectDto;
import kr.co.homeplus.admin.web.partner.model.sellerShopItem.SellerShopItemSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/partner")
public class SellerShopItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 전시관리
     *
     * @param model
     * @return String
     * @throws Exception
     */
    @RequestMapping(value = "/sellerShopItemMain", method = RequestMethod.GET)
    public String sellerShopItemMain(Model model) throws Exception {

        Map<String, List<MngCodeGetDto>> code = codeService.getCode(
            "seller_itm_sch_type"    // 셀러샵전시관리 검색어 타입
            ,"seller_itm_disp_type"    // 셀러샵전시관리 노출타입
        );

        model.addAttribute("searchType", code.get("seller_itm_sch_type"));
        model.addAttribute("dispType", code.get("seller_itm_disp_type"));

        model.addAllAttributes(RealGridHelper.create("sellerShopListGridBaseInfo", SellerShopItemGetDto.class));
        model.addAllAttributes(RealGridHelper.create("sellerShopItemListGridBaseInfo", SellerShopItemListSelectDto.class));

        return "/partner/sellerShopItemMain";
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 아이템 리스트
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/getSellerShopItemList.json"}, method = RequestMethod.GET)
    public List<SellerShopItemListSelectDto> getSellerShopItemList(String partnerId) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("partnerId", partnerId));

        String apiUriRequest = "/partner/getSellerShopItemList" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<List<SellerShopItemListSelectDto>>>() {})
            .getData();
    }

    /**
     * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
     *
     * @param
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"/setSellerShopItem.json"}, method = RequestMethod.POST)
    public ResponseResult setSellerShopItem(@RequestBody SellerShopItemSetParamDto setParamDto) throws Exception {
        String apiUri = "/partner/setSellerShopItem";
        setParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return (ResponseResult) resourceClient.postForResponseObject(ResourceRouteName.ITEM, setParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }
}