package kr.co.homeplus.admin.web.pg.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "결제관리 > 결제수단관리 > PG상점ID관리 Search DTO")
public class PgMidManageSelectDto {
    @ApiModelProperty(notes = "PG종류(INICIS,TOSSPG)", required = true)
    private String schPgKind;
    @ApiModelProperty(notes = "사용여부(사용:Y,미사용:N)", required = true)
    private String schUseYn;
}
