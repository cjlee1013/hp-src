package kr.co.homeplus.admin.web.item.model.auroraCategory;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class AuroraCategoryGetDto {

    @RealGridColumnInfo(headText = "카테고리 ID", width = 120, sortable = true)
    private String cateCd;

    @RealGridColumnInfo(headText = "카테고리명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, width = 250, order = 1)
    private String cateNm;

    @RealGridColumnInfo(headText = "우선순위", columnType = RealGridColumnType.NUMBER_CENTER, width = 50, order = 2, sortable = true)
    private String priority;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 3)
    private String useYnNm;

    @RealGridColumnInfo(headText = "노출범위", order = 4)
    private String dispYnNm;

    @RealGridColumnInfo(headText = "등록자", order = 5)
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 6)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자", order = 7)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 8)
    private String chgDt;

    @RealGridColumnInfo(headText = "depth", order = 9, hidden = true)
    private String depth;
    @RealGridColumnInfo(headText = "parentCateCd", order = 10, hidden = true)
    private String parentCateCd;
    @RealGridColumnInfo(headText = "lCateCd", order = 11, hidden = true)
    private String lCateCd;
    @RealGridColumnInfo(headText = "mCateCd", order = 12, hidden = true)
    private String mCateCd;
    @RealGridColumnInfo(headText = "useYn", order = 13, hidden = true)
    private String useYn;
    @RealGridColumnInfo(headText = "dispYn", order = 14, hidden = true)
    private String dispYn;
    @RealGridColumnInfo(headText = "dept", order = 15, hidden = true)
    private String dept;
    @RealGridColumnInfo(headText = "lcateNm", order = 16, hidden = true)
    private String lcateNm;
    @RealGridColumnInfo(headText = "mcateNm", order = 17, hidden = true)
    private String mcateNm;


    private String regId;
    private String chgId;

}
