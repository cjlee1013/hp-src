package kr.co.homeplus.admin.web.item.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.utility.SetParameter;
import kr.co.homeplus.admin.web.core.utility.StringUtil;
import kr.co.homeplus.admin.web.item.model.safetyStock.SafetyStockGetDto;
import kr.co.homeplus.admin.web.item.model.safetyStock.SafetyStockHistGetDto;
import kr.co.homeplus.admin.web.item.model.safetyStock.SafetyStockListParamDto;
import kr.co.homeplus.admin.web.item.model.safetyStock.SafetyStockSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class SafetyStockController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/safetyStock/safetyStockMain", method = RequestMethod.GET)
    public String safetyStockMain(Model model) {

        codeService.getCodeModel(model
            , "use_yn"      // 사용여부
            , "safety_inventory_yn"    // 설정여부 ( 안전재고관리 )
        );

        model.addAllAttributes(RealGridHelper.create("safetyInventoryGridBaseInfo", SafetyStockGetDto.class));

        return "/item/safetyStockMain";
    }

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리 조회
     *
     * @param safetyStockListParamDto
     * @return List<SafetyStockGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/safetyStock/getSafetyStock.json"}, method = RequestMethod.GET)
    public List<SafetyStockGetDto> getSafetyStock(@Valid SafetyStockListParamDto safetyStockListParamDto) {
        String apiUri = "/item/safetyStock/getSafetyStock";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SafetyStockGetDto>>>() {};
        return (List<SafetyStockGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, StringUtil.getRequestString(apiUri, SafetyStockListParamDto.class,
            safetyStockListParamDto), typeReference).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리 조회 ( 상품별 )
     *
     * @param itemNo
     * @return SafetyStockGetDto
     */
    @ResponseBody
    @RequestMapping(value = {"/safetyStock/getSafetyStockInfo.json"}, method = RequestMethod.GET)
    public SafetyStockGetDto getSafetyStockInfo(@RequestParam(value="itemNo") String itemNo) {

        List<SetParameter> getParameterList = new ArrayList<>();
        getParameterList.add(SetParameter.create("itemNo", itemNo));

        String apiUriRequest = "/item/safetyStock/getSafetyStockInfo" + StringUtil.getParameter(getParameterList);

        return resourceClient.getForResponseObject(ResourceRouteName.ITEM
            , apiUriRequest
            , new ParameterizedTypeReference<ResponseObject<SafetyStockGetDto>>() {})
            .getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리 등록
     *
     * @param safetyStockSetParamDto
     * @return List<SafetyStockGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/safetyStock/setSafetyStock.json"}, method = RequestMethod.POST)
    public ResponseResult setSafetyStock(@Valid @RequestBody SafetyStockSetParamDto safetyStockSetParamDto) {
        String apiUri = "/item/safetyStock/setSafetyStock";
        safetyStockSetParamDto.setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, safetyStockSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리 변경이력 (팝업)
     * @param model
     * @param itemNo
     * @return
     * @throws Exception
     */
    @RequestMapping("/popup/safetyStockHistPop")
    public String priceChangeHistPop(Model model, @RequestParam(value="itemNo") String itemNo) {
        model.addAllAttributes(RealGridHelper.create("safetyStockHistGridBaseInfo", SafetyStockHistGetDto.class));
        model.addAttribute("itemNo", itemNo);
        return "/item/pop/safetyStockHistPop";
    }

    /**
     * 상품관리 > 점포상품관리 > 안전재고관리 변경이력 조회
     * @param itemNo
     * @return
     */
    @ResponseBody
    @GetMapping(value = {"/safetyStock/getSafetyStockHist.json"})
    public List<SafetyStockHistGetDto> getSafetyStockHist(@Valid @RequestParam(value="itemNo") String itemNo) {
        String apiUri = "/item/safetyStock/getSafetyStockHist?itemNo=" + itemNo;
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<SafetyStockHistGetDto>>>() {};

        return (List<SafetyStockHistGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri, typeReference).getData();
    }
}
