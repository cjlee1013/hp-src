package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class DeployGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class DeployServer{
        @RealGridColumnInfo(headText = "seqNo", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private Long seqNo;
        @RealGridColumnInfo(headText = "서비스번호", width = 0, columnType = RealGridColumnType.BASIC, hidden = true)
        private String serviceCd;
        @RealGridColumnInfo(headText = "도메인", width = 70, columnType = RealGridColumnType.NAME)
        private String privateDomain;
        @RealGridColumnInfo(headText = "구성", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String appType;
        @RealGridColumnInfo(headText = "사용", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String useYn;
        @RealGridColumnInfo(headText = "등록일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String createDt;
        @RealGridColumnInfo(headText = "등록일", width = 10, columnType = RealGridColumnType.BASIC, sortable = true)
        private String status;
        @RealGridColumnInfo(headText = "배포일", width = 20, columnType = RealGridColumnType.BASIC, sortable = true)
        private String lastDeployDt;
    }
}
