package kr.co.homeplus.admin.web.statistics.model;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponPeriodStatisticsGetDto {
    @ApiModelProperty(value = "점포유형")
    @RealGridColumnInfo(headText = "점포유형", width = 80, sortable = true)
    private String siteType;

    @ApiModelProperty(value = "쿠폰번호")
    @RealGridColumnInfo(headText = "쿠폰번호", width = 80, sortable = true)
    private String couponNo;

    @ApiModelProperty(value = "쿠폰종류")
    @RealGridColumnInfo(headText = "주문유형", width = 80, sortable = true)
    private String discountKindNm;

    @ApiModelProperty(value = "쿠폰종류코드")
    @RealGridColumnInfo(headText = "쿠폰종류코드", width = 80, sortable = true, hidden = true)
    private String discountKind;

    @ApiModelProperty(value = "마켓유형")
    @RealGridColumnInfo(headText = "마켓유형", width = 80, sortable = true, hidden = true)
    private String marketType;

    @ApiModelProperty(value = "쿠폰명")
    @RealGridColumnInfo(headText = "쿠폰명", width = 150, sortable = true, columnType = RealGridColumnType.NONE)
    private String couponNm;

    @ApiModelProperty(value = "쿠폰바코드")
    @RealGridColumnInfo(headText = "쿠폰바코드", width = 80, sortable = true)
    private String barcode;

    @ApiModelProperty(value = "주문건수")
    @RealGridColumnInfo(headText = "주문건수", width = 70, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String purchaseCnt;

    @ApiModelProperty(value = "주문금액")
    @RealGridColumnInfo(headText = "주문금액", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String completeAmt;

    @ApiModelProperty(value = "사용금액")
    @RealGridColumnInfo(headText = "사용금액", width = 80, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
    private String discountAmt;
}
