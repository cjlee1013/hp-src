package kr.co.homeplus.admin.web.core.constants;

/**
 * resource api 명을 관리
 * apiId의 url은 application.yml의 plus.resource-routes 에 정의 됨
 */
public class ResourceRouteName {
    /**
     * image upload
     */
    public static final String UPLOAD = "upload";

    /**
     * image view
     */
    public static final String IMAGE = "image";

    /**
     * product api
     */
    public static final String ITEM = "item";

    /**
     * manage api
     */
    public static final String MANAGE = "manage";

    /**
     * message api
     */
    public static final String MESSAGE = "message";

    /**
     * authority api
     */
    public static final String AUTHORITY = "ams";

    /**
     * image Upload Validation api
     */
    public static final String IMAGE_UPLOAD_VALIDATION = "imageUploadValidation";

    /**
     * user api
     */
    public static final String USER = "user";

    /**
     * escrowmng api
     */
    public static final String ESCROWMNG = "escrowmng";

    /***
     * searchmng api
     */
    public static final String SEARCHMNG = "searchmng";

    /***
     * mileage api
     */
    public static final String MILEAGE = "mileage";

    /***
     * settle api
     */
    public static final String SETTLE = "settle";

    /***
     * shipping api
     */
    public static final String SHIPPING = "shipping";

    /**
     * address api
     **/
    public static final String ADDRESS = "address";

    /**
     * usermng api
     **/
    public static final String USERMNG = "usermng";

    /**
     * escrow api
     */
    public static final String ESCROW = "escrow";

    /**
     * log-Service
     */
    public static final String LOGSERVICE = "logService";

    /**
     * event-api
     */
    public static final String EVENT = "event";

    /**
     * preferences-api
     */
    public static final String PREFERENCES = "preferences";
}
