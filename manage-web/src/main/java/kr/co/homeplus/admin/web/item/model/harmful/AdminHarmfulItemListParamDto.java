package kr.co.homeplus.admin.web.item.model.harmful;


import lombok.Data;

@Data
public class AdminHarmfulItemListParamDto {



    private String searchStartDt;


    private String searchEndDt;


    private String searchType;

    private String searchKeyword;
}
