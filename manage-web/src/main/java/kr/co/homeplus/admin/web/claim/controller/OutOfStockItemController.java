package kr.co.homeplus.admin.web.claim.controller;

import java.util.List;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.claim.model.outOfStock.OutOfStockItemListGetDto;
import kr.co.homeplus.admin.web.claim.model.outOfStock.OutOfStockItemListSetDto;
import kr.co.homeplus.admin.web.claim.model.outOfStock.StoreShiftListSetDto;
import kr.co.homeplus.admin.web.claim.service.ClaimMainService;
import kr.co.homeplus.admin.web.claim.service.OutOfStockItemService;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/claim/outOfStock")
@RequiredArgsConstructor
public class OutOfStockItemController {

    private final CodeService codeService;

    private final OutOfStockItemService outOfStockItemService;
    private final ClaimMainService claimMainService;
    private final LoginCookieService loginCookieService;

    /**
     * 결품/대체상품 조회 Main Page
     */
    @GetMapping("/outOfStockItemMain")
    public String outOfStockItemMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("outOfStockItemListGridBaseInfo", OutOfStockItemListGetDto.class));

        String storeId = loginCookieService.getUserInfo().getStoreId();

        if(StringUtils.isNotEmpty(storeId)){
            StoreInfoGetDto storeInfoGetDto = claimMainService.getStoreInfo(storeId);
            model.addAttribute("storeId", loginCookieService.getUserInfo().getStoreId());
            model.addAttribute("storeNm", storeInfoGetDto.getStoreNm());
            model.addAttribute("storeType", storeInfoGetDto.getStoreType());
        }

        return "/claim/outOfStockItemMain";
    }

    /**
     * 결품/대체상품 리스트 조회
     */
    @PostMapping("/getOutOfStockItemList.json")
    @ResponseBody
    public List<OutOfStockItemListGetDto> getOutOfStockItemList(@RequestBody OutOfStockItemListSetDto outOfStockItemListSetDto) {
        return outOfStockItemService.getOutOfStockItemList(outOfStockItemListSetDto);
    }

    /**
     * 점포 shift 리스트 조회
     */
    @PostMapping("/getStoreShiftList.json")
    @ResponseBody
    public List<String> getStoreShiftList(@RequestBody StoreShiftListSetDto storeShiftListSetDto) {
        return outOfStockItemService.getStoreShiftList(storeShiftListSetDto);
    }

}
