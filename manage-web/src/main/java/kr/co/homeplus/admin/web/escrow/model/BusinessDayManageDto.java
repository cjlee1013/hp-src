package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 영업일 정보 Dto
 */
@Data
public class BusinessDayManageDto {
    @ApiModelProperty(notes = "영업일관리 고유번호")
    private String businessNo;
    @ApiModelProperty(notes = "영업일(YYYY-MM-DD)", required = true)
    private String businessDay;
    @ApiModelProperty(notes = "비영업일 여부(y:비영업일,n:영업일)", required = true)
    private String holidayYn;
    @ApiModelProperty(notes = "배송지연 여부(y:배송지연,n:배송정상)", required = true)
    private String nonDeliveryYn;
    @ApiModelProperty(notes = "정산불가 여부(y:정산불가,n:정산가능)", required = true)
    private String nonSettleYn;

    @ApiModelProperty(notes = "등록자id")
    private String regId;
    @ApiModelProperty(notes = "수정자id")
    private String chgId;
}
