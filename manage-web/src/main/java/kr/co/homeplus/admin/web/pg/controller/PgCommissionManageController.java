package kr.co.homeplus.admin.web.pg.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.pg.model.ParentMethodCd;
import kr.co.homeplus.admin.web.pg.model.PaymentMethodDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionManageDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionManageSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionTargetDto;
import kr.co.homeplus.admin.web.pg.model.PgCommissionTargetSelectDto;
import kr.co.homeplus.admin.web.pg.model.PgKind;
import kr.co.homeplus.admin.web.pg.service.PgCommissionManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 결제관리 > 결제수단 관리 > PG 수수료 관리
 */
@Controller
@RequestMapping("/pg/paymentMethod")
public class PgCommissionManageController {
    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final PgCommissionManageService pgCommissionManageService;

    public PgCommissionManageController(LoginCookieService loginCookieService, AuthorityService authorityService,
            PgCommissionManageService pgCommissionManageService) {
        this.loginCookieService = loginCookieService;
        this.authorityService = authorityService;
        this.pgCommissionManageService = pgCommissionManageService;
    }

    /**
     * PG 수수료 관리 메인페이지 호출
     */
    @GetMapping("/pgCommissionManageMain")
    public String pgCommissionManageMain(Model model) throws Exception {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_PAY_MNG)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        model.addAllAttributes(RealGridHelper.create("pgCommissionManageGridBaseInfo", PgCommissionManageDto.class));
        model.addAttribute("pgKindList", Arrays.asList(PgKind.values()));
        model.addAttribute("parentMethodCdList", ParentMethodCd.getBasicParentMethodList());

        List<PaymentMethodDto> paymentMethodDtoList = pgCommissionManageService.getAllDistinctMethodCd();
        model.addAttribute("cardPaymentMethodList", paymentMethodDtoList.stream().filter(p -> ParentMethodCd.CARD.getMethodCd().equals(p.getParentMethodCd())).collect(Collectors.toList()));
        model.addAttribute("phonePaymentMethodList", paymentMethodDtoList.stream().filter(p -> ParentMethodCd.PHONE.getMethodCd().equals(p.getParentMethodCd())).collect(Collectors.toList()));
        return "/pg/pgCommissionManageMain";
    }

    /**
     * 중복제거 결제수단 조회
     */
    @ResponseBody
    @GetMapping("/getDistinctMethodCd.json")
    public List<PaymentMethodDto> getDistinctMethodCd(@RequestParam(value = "parentMethodCd") String parentMethodCd) throws Exception {
        return pgCommissionManageService.getDistinctMethodCd(parentMethodCd);
    }

    /**
     * PG 수수료 마스터 조회
     */
    @ResponseBody
    @RequestMapping("/getPgCommissionMngList.json")
    public List<PgCommissionManageDto> getPgCommissionMngList(@ModelAttribute PgCommissionManageSelectDto pgCommissionManageSelectDto) throws Exception {
        return pgCommissionManageService.getPgCommissionMngList(pgCommissionManageSelectDto);
    }

    /**
     * PG 수수료 대상 조회
     */
    @ResponseBody
    @GetMapping("/getPgCommissionTargetList.json")
    public List<PgCommissionTargetDto> getPgCommissionTargetList(@RequestParam(value = "pgCommissionMngSeq") Long pgCommissionMngSeq) throws Exception {
        PgCommissionTargetSelectDto pgCommissionTargetSelectDto = new PgCommissionTargetSelectDto();
        pgCommissionTargetSelectDto.setPgCommissionMngSeq(pgCommissionMngSeq);
        return pgCommissionManageService.getPgCommissionTargetList(pgCommissionTargetSelectDto);
    }

    /**
     * 중복 수수료 정책 조회
     */
    @ResponseBody
    @GetMapping("/checkDuplicateCommission.json")
    public ResponseObject<Integer> checkDuplicateCommission(@RequestParam(value = "pgKind") String pgKind, @RequestParam(value = "parentMethodCd") String parentMethodCd,
                                                            @RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
                                                            @RequestParam(value = "pgCommissionMngSeq", required = false) String pgCommissionMngSeq
    ) throws Exception {
        return pgCommissionManageService.checkDuplicateCommission(pgKind, parentMethodCd, startDt, endDt, pgCommissionMngSeq);
    }

    /**
     * PG 수수료 저장
     */
    @ResponseBody
    @PostMapping("/savePgCommission.json")
    public ResponseObject<Object> savePgCommission(@RequestBody PgCommissionManageDto pgCommissionManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        pgCommissionManageDto.setRegId(userCd);
        pgCommissionManageDto.setChgId(userCd);
        return pgCommissionManageService.savePgCommission(pgCommissionManageDto);
    }
}