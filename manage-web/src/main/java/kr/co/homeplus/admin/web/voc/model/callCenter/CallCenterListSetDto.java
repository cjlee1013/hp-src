package kr.co.homeplus.admin.web.voc.model.callCenter;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원관리 > 고객문의관리 > 콜센터문의 리스트 요청 DTO")
public class CallCenterListSetDto {
  @ApiModelProperty(notes = "일자구분")
  @JsonProperty("SCH_D_TYPE")
  private String SCH_D_TYPE;

  @ApiModelProperty(notes = "시작일")
  @JsonProperty("SCH_S_YMD")
  private String SCH_S_YMD;

  @ApiModelProperty(notes = "종료일")
  @JsonProperty("SCH_E_YMD")
  private String SCH_E_YMD;

  @ApiModelProperty(notes = "처리상태")
  @JsonProperty("ADVISOR_ING_TYPE")
  private String ADVISOR_ING_TYPE;

  @ApiModelProperty(notes = "접수채널")
  @JsonProperty("CHANNEL_CD")
  private String CHANNEL_CD;

  @ApiModelProperty(notes = "채널구분(판매자)")
  @JsonProperty("H_CHANNEL")
  private String H_CHANNEL;

  @ApiModelProperty(notes = "판매자")
  @JsonProperty("STORE_ID")
  private String STORE_ID;

  @ApiModelProperty(notes = "회원번호")
  @JsonProperty("CUST_ID")
  private String CUST_ID;

  @ApiModelProperty(notes = "주문번호")
  @JsonProperty("H_ORDER_NO")
  private String H_ORDER_NO;

  @ApiModelProperty(notes = "제목")
  @JsonProperty("HISTORY_TITLE")
  private String HISTORY_TITLE;

  @ApiModelProperty(notes = "처리자")
  @JsonProperty("COMPLET_ACCOUNT_NM")
  private String COMPLET_ACCOUNT_NM;

  @ApiModelProperty(notes = "문의번호")
  @JsonProperty("MASTER_SEQ")
  private String MASTER_SEQ;

  @ApiModelProperty(notes = "상담구분(고객구분)")
  @JsonProperty("VOC_YN")
  private String VOC_YN;

  @ApiModelProperty(notes = "상담구분")
  @JsonProperty("ADVISOR_TYPE_1")
  private String ADVISOR_TYPE_1;

  @ApiModelProperty(notes = "상담항목")
  @JsonProperty("ADVISOR_TYPE_2")
  private String ADVISOR_TYPE_2;

  @ApiModelProperty(notes = "처리부서")
  @JsonProperty("H_BRANCH_CD")
  private String H_BRANCH_CD;

  @ApiModelProperty(notes = "마켓연동")
  @JsonProperty("MARKET_TYPE")
  private String MARKET_TYPE;
}