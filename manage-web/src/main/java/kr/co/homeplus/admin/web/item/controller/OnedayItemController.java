package kr.co.homeplus.admin.web.item.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import kr.co.homeplus.admin.web.common.model.common.ResponseResult;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessExceptionCode;
import kr.co.homeplus.admin.web.core.exception.handler.BusinessLogicException;
import kr.co.homeplus.admin.web.item.model.onedayItem.OnedayItemCntGetDto;
import kr.co.homeplus.admin.web.item.model.onedayItem.OnedayItemExcelDto;
import kr.co.homeplus.admin.web.item.model.onedayItem.OnedayItemGetDto;
import kr.co.homeplus.admin.web.item.model.onedayItem.OnedayItemSetParamDto;
import kr.co.homeplus.admin.web.manage.model.dspLeaflet.DspLeafletItemGetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import kr.co.homeplus.plus.api.support.service.ExcelUploadService;
import kr.co.homeplus.plus.excel.support.model.ExcelHeaders;
import kr.co.homeplus.plus.excel.support.model.ExcelUploadOption;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("/item")
public class OnedayItemController {

    final private ResourceClient resourceClient;
    final private LoginCookieService loginCookieService;
    final private CodeService codeService;
    private final ExcelUploadService excelUploadService;

    /**
     * 사이트관리 > 싸데이 관리 > 싸데이 상품 관리
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/onedayItem/onedayItemMain", method = RequestMethod.GET)
    public String onedayItemMain(Model model) {

        codeService.getCodeModel(model
            , "disp_yn"		            // 사용여부
            , "dsp_main_store_type"     // 스토어 타입
        );

        model.addAllAttributes(RealGridHelper.create("onedayItemGridBaseInfo", OnedayItemGetDto.class));

        return "/item/onedayItemMain";
    }

    /**
     * 사이트관리 > 싸데이 관리 > 싸데이 상품 리스트
     *
     * @param dispDt
     * @return List<AuroraBannerGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/onedayItem/getOnedayItem.json"}, method = RequestMethod.GET)
    public List<OnedayItemGetDto> getOnedayItem(@RequestParam(value = "dispDt") String dispDt, @RequestParam(value = "siteType", defaultValue = "HOME") String siteType) {
        String apiUri = "/item/onedayItem/getOnedayItem";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<OnedayItemGetDto>>>() {};
        return (List<OnedayItemGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?dispDt=" + dispDt + "&siteType=" + siteType, typeReference).getData();
    }

    /**
     * 사이트관리 > 싸데이 관리 > 싸데이 상품 등록수정
     *
     * @param onedayItemSetParamDto
     * @return ResponseResult
     */
    @ResponseBody @PostMapping(value = {"/onedayItem/setOnedayItem.json"})
    public ResponseResult setOnedayItem(@Valid @RequestBody List<OnedayItemSetParamDto> onedayItemSetParamDto) {
        String apiUri = "/item/onedayItem/setOnedayItem";
        onedayItemSetParamDto.get(0).setRegId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(ResourceRouteName.ITEM, onedayItemSetParamDto, apiUri, new ParameterizedTypeReference<ResponseObject<ResponseResult>>() {}).getData();
    }

    /**
     * 사이트관리 > 싸데이 관리 > 싸데이 월별 상품 수
     *
     * @param dispDate
     * @return List<OnedayItemCntGetDto>
     */
    @ResponseBody
    @RequestMapping(value = {"/onedayItem/getOnedayItemCnt.json"}, method = RequestMethod.GET)
    public List<OnedayItemCntGetDto> getOnedayItemCnt(@RequestParam(value = "dispDate") String dispDate, @RequestParam(value = "siteType", defaultValue = "HOME") String siteType) {
        String apiUri = "/item/onedayItem/getOnedayItemCnt";
        ParameterizedTypeReference typeReference = new ParameterizedTypeReference<ResponseObject<List<OnedayItemCntGetDto>>>() {};
        return (List<OnedayItemCntGetDto>) resourceClient.getForResponseObject(ResourceRouteName.ITEM, apiUri + "?dispDate=" + dispDate + "&siteType=" + siteType, typeReference).getData();
    }

    /**
     * 사이트관리 > 전시관리 > 홈테마 리스트 상품 일괄등록 팝업
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/popup/uploadOnedayItemPop")
    public String uploadOnedayItemPop(Model model, @RequestParam(value = "callback") String callBackScript, @RequestParam(value = "targetDisp") String targetDisp) {
        model.addAttribute("callBackScript", callBackScript);
        model.addAttribute("targetDisp", targetDisp);
        return "/item/pop/uploadOnedayItemPop";
    }

    /**
     * 기획전 상품 엑셀 일괄등록 적용
     */
    @ResponseBody
    @PostMapping("/onedayItem/onedayItemExcel.json")
    public List<OnedayItemExcelDto> onedayItemExcel(@RequestParam(value="targetDisp") String targetDisp, MultipartHttpServletRequest multipartHttpServletRequest)
        throws BusinessLogicException {
        // 파일을 읽기위해 input type file 태그 name 작성
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("uploadFile");
        List<OnedayItemExcelDto> excelData;
        List<DspLeafletItemGetDto> arrResData;

        // 1. 엑셀 메타정보에 매핑정보 입력
        ExcelHeaders headers = new ExcelHeaders.Builder()
            .header(0, "상품유형", "itemStoreType", true)
            .header(1, "상품번호", "itemNo", true)
            .header(2, "전시여부", "dispYn", true)
            .build();

        //  1-1. 엑셀 메타정보 입력 (엑셀의 시작/마지막 행, 시작/마지막열, 읽을 대상시트, 맵핑 할 DTO 필드 정보입력)
        ExcelUploadOption<OnedayItemExcelDto> excelUploadOption = new ExcelUploadOption.Builder<>(OnedayItemExcelDto.class, headers, 4, 0).build();

        // 2. 엑셀 조회 (multipartFile 과 엑셀메타정보, 리턴받을 Class Type(DTO)을 입력한다.)
        try {
            excelData = excelUploadService.readExcelFile(multipartFile, excelUploadOption);
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1005);
        }

        // 3. 매핑된 데이터에 대한 검증
        Integer maxCnt = 100;

        // 3-1. 최대개수 확인
        if (excelData.size() > maxCnt) {
            //일괄등록 1회 수행시 최대 상품 100개를 초과했습니다.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1007, 100);
        }

        List<String> itemNoList = excelData.stream().map(OnedayItemExcelDto::getItemNo).distinct().collect(Collectors.toList());

        // 3-2. 일괄등록 요청상품 itemNo로 검증
        try {
            String apiUri = "/manage/dspLeaflet/getDspLeafletItemListByExcel";
            arrResData = resourceClient.postForResponseObject(ResourceRouteName.MANAGE, itemNoList, apiUri, new ParameterizedTypeReference<ResponseObject<List<DspLeafletItemGetDto>>>() {}).getData();
        } catch (Exception e) {
            //상품 정보 조회 중 오류가 발생하였습니다. 관리자에게 문의해 주세요.
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1008);
        }

        // 3-3. 데이터 검증하여 실제 insert 할 model list 생성
        String errorItemNo = "";
        try {
            for(OnedayItemExcelDto item : excelData) {
                if(item.getDispYn().equals("Y")) {
                    item.setDispYnTxt("전시");
                } else {
                    item.setDispYnTxt("전시안함");
                }

                DspLeafletItemGetDto setItemData =
                    arrResData
                        .stream()
                        .filter(data -> item.getItemNo().equals(data.getItemNo()))
                        .findAny().orElse(null);

                if(ObjectUtils.isNotEmpty(setItemData)) {
                    item.setItemNm(setItemData.getItemNm1());
                } else {
                    errorItemNo = item.getItemNo();
                    throw new BusinessLogicException(null);
                }
            }
        } catch (Exception e) {
            throw new BusinessLogicException(BusinessExceptionCode.ERROR_CODE_1009, excelData.size(), errorItemNo);
            //throw new ExcelUploadServiceException("상품 데이터 조합 중 오류가 발생하였습니다. 셀 갯수 / 유효한 데이터를 확인해 주세요. >> 셀 갯수 [" + excelData.size() + "] itemNo [" + errorItemNo + "]");
        }

        return excelData;
    }

}
