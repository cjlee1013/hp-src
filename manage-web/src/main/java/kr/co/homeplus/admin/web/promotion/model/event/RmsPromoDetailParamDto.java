package kr.co.homeplus.admin.web.promotion.model.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RmsPromoDetailParamDto {
    private Long rpmPromoCompDetailId;

    private Integer thresholdId;
}