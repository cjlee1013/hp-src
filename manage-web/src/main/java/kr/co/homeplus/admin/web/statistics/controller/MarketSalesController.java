package kr.co.homeplus.admin.web.statistics.controller;

import java.util.List;
import kr.co.homeplus.admin.web.statistics.model.market.MarketStatisticsSalesGetDto;
import kr.co.homeplus.admin.web.statistics.model.market.MarketStatisticsSalesSetDto;
import kr.co.homeplus.admin.web.statistics.service.MarketSalesService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 마켓 통계
 */
@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/statistics/market")
public class MarketSalesController {

    private final MarketSalesService marketSalesService;

    @RequestMapping(value = "/sales", method = RequestMethod.GET)
    public String sales(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("MarketStatisticsSalesBaseInfo", MarketStatisticsSalesGetDto.class));
        return "/statistics/marketSales";
    }

    @ResponseBody
    @RequestMapping(value = "/getSales.json", method = RequestMethod.GET)
    public List<MarketStatisticsSalesGetDto> getSales(
        MarketStatisticsSalesSetDto marketStatisticsSalesSetDto) throws Exception {
        return marketSalesService.getSales(marketStatisticsSalesSetDto);
    }
}