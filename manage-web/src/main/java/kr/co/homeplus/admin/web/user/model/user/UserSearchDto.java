package kr.co.homeplus.admin.web.user.model.user;

import lombok.Getter;
import lombok.Setter;

/**
 * 회원조회 DTO
 **/
@Setter
@Getter
public class UserSearchDto {
    private Long userNo;

    private String userNm;

    private String userId;

    private String mhcUcid;

    private String userStatus;

    private String mobile;

    private String email;
}
