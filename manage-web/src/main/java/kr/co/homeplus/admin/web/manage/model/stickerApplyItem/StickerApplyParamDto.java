package kr.co.homeplus.admin.web.manage.model.stickerApplyItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StickerApplyParamDto {

    //검색 시작일
    private String schStartDate;

    //검색 종류일
    private String schEndDate;

    //검색 날짜 타입
    private String schDateType;

    //검색 조건
    private String schType;

    //검색 키워드
    private String schKeyword;

    //검색 사용여부
    private String schUseYn ;
}

