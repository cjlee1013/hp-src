package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscountSearchSetDto {
    private String schDateType;
    private String schStartDt;
    private String schEndDt;
    private String schType;
    private String schValue;
    private String schUseYn;
    private String schStoreType;
    private String schDiscountKind;

    private int discountNo;
    private String schPageType;

    private boolean isCopy;

    private List<String> schValueList;
}