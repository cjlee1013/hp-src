package kr.co.homeplus.admin.web.escrow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "시스템관리 > 기준정보관리 > 우편번호관리 Search DTO")
public class ZipcodeManageSelectDto {
    @ApiModelProperty(notes = "도서산간유형 (JEJU:제주,ISLAND:도서산간,ALL:선택안함)")
    private String schIslandType;
    @ApiModelProperty(notes = "검색키워드유형 (ROADADDR:도로명,GIBUN:지번,ZIPCODE:우편번호:BUILD_MNG_NO:건물관리번호)", required = true)
    private String schKeywordType;
    @ApiModelProperty(notes = "시도명 (검색키워드가 ROADADDR 일 때 사용)")
    private String schSidoType;
    @ApiModelProperty(notes = "시군구명 (검색키워드가 ROADADDR 일 때 사용)")
    private String schSigunguType;
    @ApiModelProperty(notes = "검색키워드")
    private String schKeyword;
    @ApiModelProperty(notes = "건물번호")
    private String schBuildNo;
}
