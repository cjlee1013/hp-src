package kr.co.homeplus.admin.web.manage.model.dspMainTheme;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DspMainThemeSetParamDto {

    private Long themeId;
    private String themeNm;
    private String siteType;
    private int priority;
    private String useYn;
    private String dispStartDt;
    private String dispEndDt;
    private String regId;

}
