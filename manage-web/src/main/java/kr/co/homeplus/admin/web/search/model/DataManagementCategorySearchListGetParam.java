package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataManagementCategorySearchListGetParam {
    @ApiParam(value = "점포ID ( ex : 37, 38, 90163")
    private String storeIds;
    @ApiParam(value = "사이트 유형 ( 홈플러스 : HOME, 더클럽 : CLUB)",required = true,defaultValue = "HOME",allowableValues = "HOME,CLUB")
    private String siteType;
    @ApiParam(value = "카테고리번호", required = true, defaultValue = "100001")
    private String categoryId;
    @ApiParam(value = "카테고리뎁스", required = true, defaultValue = "1")
    private Integer categoryDepth;
    @ApiParam(value = "정렬 ( 추천순: RANK, 최신순: NEW, 낮은 가격순: PRICE_DOWN, 높은 가격순: PRICE_UP, 많이팔린순: SALES_UP, 리뷰많은순: REVIEW_UP )", required = true, allowableValues = "RANK, NEW, PRICE_DOWN, PRICE_UP, SALES_UP, REVIEW_UP", defaultValue = "RANK")
    private String sort;
    @ApiParam(value = "배송유형 ( 점포배송: HYPER_DRCT, 택배배송: DLV , 새벽배송: AURORA ) => ex) HYPER_DRCT:AURORA", allowableValues = "HYPER_DRCT,DLV,AURORA")
    private String delivery;
    @ApiParam(value = "혜택 ( 행사상품: BASIC, 골라담기: PICK, 함께할인: TOGETHER, 사은품: GIFT, 무료배송: FREE)", allowableValues = "BASIC, PICK, TOGETHER, GIFT, FREE")
    private String benefit;
    @ApiParam(value = "브랜드")
    private String brand;
    @ApiParam(value = "파트너")
    private String partner;
    @ApiParam(value = "쿼리 여부")
    private String explain;
    @ApiParam(value = "페이지번호",required = true, defaultValue = "1")
    private Integer page = 1;
    @ApiParam(value = "페이지사이즈",required = true, defaultValue = "100")
    private Integer perPage = 100;
}
