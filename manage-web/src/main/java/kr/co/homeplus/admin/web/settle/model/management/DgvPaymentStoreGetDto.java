package kr.co.homeplus.admin.web.settle.model.management;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
@ApiModel(description = "DGV 실적조회 > 점별 합계")
public class DgvPaymentStoreGetDto {
  @ApiModelProperty(notes = "점포유형")
  @RealGridColumnInfo(headText = "점포유형", sortable = true, width = 100)
  private String storeType;

  @ApiModelProperty(notes = "점포코드")
  @RealGridColumnInfo(headText = "점포코드", sortable = true, width = 100)
  private String storeId;

  @ApiModelProperty(notes = "점포명")
  @RealGridColumnInfo(headText = "점포명", sortable = true, width = 100)
  private String storeNm;

  @ApiModelProperty(notes = "코스트센터")
  @RealGridColumnInfo(headText = "코스트센터", sortable = true, width = 100)
  private String onlineCostCenter;

  @ApiModelProperty(notes = "매출액")
  @RealGridColumnInfo(headText = "매출액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String completeAmt;

  @ApiModelProperty(notes = "결제금액")
  @RealGridColumnInfo(headText = "결제금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;

  @ApiModelProperty(notes = "DGV 사용금액")
  @RealGridColumnInfo(headText = "DGV 사용금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String dgvAmt;

  @ApiModelProperty(notes = "GVS 전송금액")
  @RealGridColumnInfo(headText = "GVS 전송금액", sortable = true, width = 100, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String collectAmt;
}
