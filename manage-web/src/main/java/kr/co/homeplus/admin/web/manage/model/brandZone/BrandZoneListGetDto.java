package kr.co.homeplus.admin.web.manage.model.brandZone;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandZoneListGetDto {

    @ApiModelProperty(value = "브랜드존 등록번호", position = 1)
    @RealGridColumnInfo(headText = "번호", width = 100)
    private Long brandzoneNo;

    @ApiModelProperty(value = "브랜드코드", position = 2)
    @RealGridColumnInfo(headText = "브랜드코드", width = 100)
    private Long brandNo;

    @ApiModelProperty(value = "브랜드명", position = 3)
    @RealGridColumnInfo(headText = "브랜드명", width = 150)
    private String brandNm;

    @ApiModelProperty(value = "사이트구분")
    @RealGridColumnInfo(headText = "사이트구분", hidden = true)
    private String siteType;

    @ApiModelProperty(value = "사이트구분", position = 4)
    @RealGridColumnInfo(headText = "사이트구분", width = 150)
    private String siteTypeNm;

    @ApiModelProperty(value = "우선순위", position = 5)
    @RealGridColumnInfo(headText = "우선순위", width = 100)
    private String priority;

    @ApiModelProperty(value = "사용여부", position = 6)
    @RealGridColumnInfo(headText = "사용여부", width = 100)
    private String useYnNm;

    @ApiModelProperty(value = "사용여부")
    @RealGridColumnInfo(headText = "사용여부", hidden = true)
    private String useYn;

    @ApiModelProperty(value="PC 이미지 URL")
    @RealGridColumnInfo(headText = "PC 이미지 URL", hidden = true)
    private String pcImgUrl;

    @ApiModelProperty(value="PC 이미지 가로")
    @RealGridColumnInfo(headText = "PC 이미지 가로", hidden = true)
    private Integer pcImgWidth;

    @ApiModelProperty(value="PC 이미지 세로")
    @RealGridColumnInfo(headText = "PC 이미지 세로", hidden = true)
    private Integer pcImgHeight;

    @ApiModelProperty(value="모바일 이미지 URL")
    @RealGridColumnInfo(headText = "모바일 이미지 URL", hidden = true)
    private String mobileImgUrl;

    @ApiModelProperty(value="모바일 이미지 가로")
    @RealGridColumnInfo(headText = "모바일 이미지 가로", hidden = true)
    private Integer mobileImgWidth;

    @ApiModelProperty(value="모바일 이미지 세로")
    @RealGridColumnInfo(headText = "모바일 이미지 세로", hidden = true)
    private Integer mobileImgHeight;

    @ApiModelProperty(value = "등록일")
    @RealGridColumnInfo(headText = "등록일", width = 150)
    private String regDt;

    @ApiModelProperty(value = "등록자")
    @RealGridColumnInfo(headText = "등록자", width = 100)
    private String regNm;

    @ApiModelProperty(value = "수정일")
    @RealGridColumnInfo(headText = "수정일", width = 150)
    private String chgDt;

    @ApiModelProperty(value = "수정자")
    @RealGridColumnInfo(headText = "수정자", width = 100)
    private String chgNm;
}
