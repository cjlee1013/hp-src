package kr.co.homeplus.admin.web.item.model.market;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarketItemFailGetDto {

    @RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "연동사이트", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String marketNm;

    @RealGridColumnInfo(headText = "연동사이트 상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
    private String marketItemNo;

    @RealGridColumnInfo(headText = "상품명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME, sortable = true, width = 150)
    private String itemNm;

    @RealGridColumnInfo(headText = "전송일시",columnType= RealGridColumnType.DATETIME, sortable = true)
    private String sendDt;

    @RealGridColumnInfo(headText = "실패사유",fieldType = RealGridFieldType.TEXT, columnType= RealGridColumnType.NAME, width = 300)
    private String sendMsg;

}
