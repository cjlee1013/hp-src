package kr.co.homeplus.admin.web.manage.model.gnb;

import lombok.Data;

@Data
public class GnbListParamDto {

    private String searchStartDt;

    private String searchEndDt;

    private String searchSite;

    private String searchDevice;

    private String searchUseYn;

    private String searchType;

    private String searchKeyword;
}
