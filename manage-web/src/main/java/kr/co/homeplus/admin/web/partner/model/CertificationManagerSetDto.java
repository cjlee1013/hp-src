package kr.co.homeplus.admin.web.partner.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CertificationManagerSetDto {

    private String certificationSeq;
    private String certificationMngNm;
    private String certificationMngPhone;
    private String certificationState;

}
