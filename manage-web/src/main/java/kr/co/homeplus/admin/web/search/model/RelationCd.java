package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum RelationCd {

    ALL("ALL","전체"),
    SYNONYM("SYNONYM","동의어"),
    HYPONYM("HYPONYM","하위어"),
    RELATEDNYM("RELATEDNYM","관련어");

    private String codeCd;
    private String codeNm;

    public static RelationCd getType(String codeCd) {
        for(RelationCd code : RelationCd.values()) {
            if(code.codeCd.equals(codeCd)) {
                return code;
            }
        }
        return null;
    }

}
