package kr.co.homeplus.admin.web.item.model.auroraBanner;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuroraBannerSetParamDto {

    private Long bannerId;
    private String dispKind;
    private String bannerNm;
    private String dispStartDt;
    private String dispEndDt;
    private String useYn;
    private String template;
    private String bannerType;
    private String regId;

    private List<AuroraBannerLinkSetParamDto> linkList;

}
