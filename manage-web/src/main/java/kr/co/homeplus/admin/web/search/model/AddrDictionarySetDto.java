package kr.co.homeplus.admin.web.search.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "주소사전 > 명사등록")
public class AddrDictionarySetDto {
    @ApiModelProperty(notes = "명사",required = true, position = 1)
    @NotNull
    @NotEmpty
    private String wordName;
    @ApiModelProperty(notes = "사용여부(Y|N)",required = true, position = 2)
    private char useFlag = 'Y';
    private List<AddrSynonymGetDto> synonyms;
}
