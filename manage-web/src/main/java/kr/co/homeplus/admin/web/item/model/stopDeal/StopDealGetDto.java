package kr.co.homeplus.admin.web.item.model.stopDeal;

import kr.co.homeplus.plus.api.support.realgrid.*;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class StopDealGetDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명", width = 200, columnType = RealGridColumnType.NAME)
    private String itemNm;

    @RealGridColumnInfo(headText = "점포ID", width = 60 , columnType = RealGridColumnType.NUMBER_C, sortable = true)
    private int storeId;

    @RealGridColumnInfo(headText = "점포명", columnType = RealGridColumnType.BASIC, sortable = true)
    private String storeNm;

    @RealGridColumnInfo(headText = "취급중지여부", columnType = RealGridColumnType.BASIC, width = 80, sortable = true)
    private String stopDealNm;

    @RealGridColumnInfo(headText = "중지시작일", columnType = RealGridColumnType.BASIC, width = 80, sortable = true)
    private String stopStartDt;

    @RealGridColumnInfo(headText = "중지종료일", columnType = RealGridColumnType.BASIC, width = 80, sortable = true)
    private String stopEndDt;

    @RealGridColumnInfo(headText = "중지사유",columnType = RealGridColumnType.BASIC, width = 200)
    private String stopReason;

}