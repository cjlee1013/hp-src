package kr.co.homeplus.admin.web.pg.controller;

import java.util.List;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostCommissionSetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostDiffGetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostSetDto;
import kr.co.homeplus.admin.web.pg.model.PgCompareCostSumGetDto;
import kr.co.homeplus.admin.web.pg.service.PgCompareCostService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Admin > 결제관리 > PG 대사 > PG 정산대사 조회
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/pg/pgCompareCost")
public class PgCompareCostController {

    private final PgCompareCostService pgCompareCostService;

    @RequestMapping(value = "/pgCompareCost", method = RequestMethod.GET)
    public String pgCompareCost(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("pgCompareCostSumBaseInfo", PgCompareCostSumGetDto.class));
        model.addAllAttributes(RealGridHelper.create("pgCompareCostDiffBaseInfo", PgCompareCostDiffGetDto.class));
        return "/pg/pgCompareCost";
    }

    @ResponseBody
    @RequestMapping(value = "/getSum.json", method = RequestMethod.GET)
    public List<PgCompareCostSumGetDto> getSum(PgCompareCostSetDto pgCompareCostSetDto) throws Exception {
        return pgCompareCostService.getPgCompareCostSum(pgCompareCostSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getDiff.json", method = RequestMethod.GET)
    public List<PgCompareCostDiffGetDto> getDiff(PgCompareCostSetDto pgCompareCostSetDto) throws Exception {
        return pgCompareCostService.getPgCompareCostDiff(pgCompareCostSetDto);
    }


    @RequestMapping(value = "/Commission", method = RequestMethod.GET)
    public String pgCompareCostCommission(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("pgCompareCostCommissionBaseInfo", PgCompareCostDiffGetDto.class));
        return "/pg/pgCompareCostCommission";
    }

    @ResponseBody
    @RequestMapping(value = "/getCommission.json", method = RequestMethod.GET)
    public List<PgCompareCostDiffGetDto> getCommission(
        PgCompareCostCommissionSetDto pgCompareCostCommissionSetDto) throws Exception {
        return pgCompareCostService.getPgCompareCostCommission(pgCompareCostCommissionSetDto);
    }
}