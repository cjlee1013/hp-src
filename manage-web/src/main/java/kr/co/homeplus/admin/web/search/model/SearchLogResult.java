package kr.co.homeplus.admin.web.search.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchLogResult {
    private String keyword;
    private String count;
}