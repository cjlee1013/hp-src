package kr.co.homeplus.admin.web.statistics.controller;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipStoreInfoGetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.settle.service.SettleCommonService;
import kr.co.homeplus.admin.web.statistics.service.DailyOrderInfoService;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.*;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 통계 > 주문결제 통계
 */
@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/statistics/dailyOrderInfo")
public class DailyOrderInfoController {

    private final DailyOrderInfoService dailyOrderInfoService;
    private final SettleCommonService settleCommonService;
    private final ShipCommonService shipCommonService;

    @RequestMapping(value = "/partnerPayment", method = RequestMethod.GET)
    public String partnerPayment(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getName","판매업체");
        model.addAttribute("getType","Partner");
        model.addAttribute("getKind","Payment");

        model.addAllAttributes(RealGridHelper.createForGroup("DailyOrderInfoBaseInfo", DailyOrderInfoPartnerGetDto.class));
        return "/statistics/dailyOrderInfo";
    }
    @RequestMapping(value = "/storePayment", method = RequestMethod.GET)
    public String storePayment(Model model) throws Exception {
        List<MngCodeGetDto> mngCodeGetDtoList = new ArrayList<>();
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();

        String storeId = shipCommonService.getStoreId();
        if (storeId != null) {
            ShipStoreInfoGetDto shipStoreInfoGetDto = shipCommonService.getStoreInfo(storeId);
            model.addAttribute("getStoreId", storeId);
            model.addAttribute("getStoreNm", shipStoreInfoGetDto.getStoreNm());
            String storeType = shipStoreInfoGetDto.getStoreType();
            mngCodeGetDto.setMcCd(storeType);
            mngCodeGetDto.setMcNm(storeType);
            mngCodeGetDtoList.add(mngCodeGetDto);
        } else {
            mngCodeGetDto.setMcCd("HYPER");
            mngCodeGetDto.setMcNm("HYPER");
            mngCodeGetDtoList.add(mngCodeGetDto);
            mngCodeGetDto = new MngCodeGetDto();
            mngCodeGetDto.setMcCd("EXP");
            mngCodeGetDto.setMcNm("EXP");
            mngCodeGetDtoList.add(mngCodeGetDto);
        }
        model.addAttribute("storeType", mngCodeGetDtoList);
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getName","점포");
        model.addAttribute("getType","Store");
        model.addAttribute("getKind","Payment");
        model.addAllAttributes(RealGridHelper.createForGroup("DailyOrderInfoBaseInfo", DailyOrderInfoStoreGetDto.class));
        return "/statistics/dailyOrderInfo";
    }

    @RequestMapping(value = "/partnerDecision", method = RequestMethod.GET)
    public String partnerDecision(Model model) throws Exception {
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getName","판매업체");
        model.addAttribute("getType","Partner");
        model.addAttribute("getKind","Decision");

        model.addAllAttributes(RealGridHelper.createForGroup("DailyOrderInfoBaseInfo", DailyOrderInfoPartnerGetDto.class));
        return "/statistics/dailyOrderInfo";
    }
    @RequestMapping(value = "/storeDecision", method = RequestMethod.GET)
    public String storeDecision(Model model) throws Exception {
        List<MngCodeGetDto> mngCodeGetDtoList = new ArrayList<>();
        MngCodeGetDto mngCodeGetDto = new MngCodeGetDto();

        String storeId = shipCommonService.getStoreId();
        if (storeId != null) {
            ShipStoreInfoGetDto shipStoreInfoGetDto = shipCommonService.getStoreInfo(storeId);
            model.addAttribute("getStoreId", storeId);
            model.addAttribute("getStoreNm", shipStoreInfoGetDto.getStoreNm());
            String storeType = shipStoreInfoGetDto.getStoreType();
            mngCodeGetDto.setMcCd(storeType);
            mngCodeGetDto.setMcNm(storeType);
            mngCodeGetDtoList.add(mngCodeGetDto);
        } else {
            mngCodeGetDto.setMcCd("HYPER");
            mngCodeGetDto.setMcNm("HYPER");
            mngCodeGetDtoList.add(mngCodeGetDto);
            mngCodeGetDto = new MngCodeGetDto();
            mngCodeGetDto.setMcCd("EXP");
            mngCodeGetDto.setMcNm("EXP");
            mngCodeGetDtoList.add(mngCodeGetDto);
        }
        model.addAttribute("storeType", mngCodeGetDtoList);
        //년도 조회
        model.addAttribute("getYear", settleCommonService.getDateCalculate("Y", null, null, 2020));
        model.addAttribute("getName","점포");
        model.addAttribute("getType","Store");
        model.addAttribute("getKind","Decision");
        model.addAllAttributes(RealGridHelper.createForGroup("DailyOrderInfoBaseInfo", DailyOrderInfoStoreGetDto.class));
        return "/statistics/dailyOrderInfo";
    }

    @ResponseBody
    @RequestMapping(value = "/getPartnerPayment.json", method = RequestMethod.GET)
    public List<DailyOrderInfoPartnerGetDto> getPartnerPayment(DailyOrderInfoPartnerSetDto dailyOrderInfoPartnerSetDto) throws Exception {
        return dailyOrderInfoService.getPartnerPayment(dailyOrderInfoPartnerSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getStorePayment.json", method = RequestMethod.GET)
    public List<DailyOrderInfoStoreGetDto> getStorePayment(DailyOrderInfoStoreSetDto dailyOrderInfoStoreSetDto) throws Exception {
        return dailyOrderInfoService.getStorePayment(dailyOrderInfoStoreSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getPartnerDecision.json", method = RequestMethod.GET)
    public List<DailyOrderInfoPartnerGetDto> getPartnerDecision(DailyOrderInfoPartnerSetDto dailyOrderInfoPartnerSetDto) throws Exception {
        return dailyOrderInfoService.getPartnerDecision(dailyOrderInfoPartnerSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getStoreDecision.json", method = RequestMethod.GET)
    public List<DailyOrderInfoStoreGetDto> getStoreDecision(DailyOrderInfoStoreSetDto dailyOrderInfoStoreSetDto) throws Exception {
        return dailyOrderInfoService.getStoreDecision(dailyOrderInfoStoreSetDto);
    }
}