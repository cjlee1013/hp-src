package kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import lombok.Data;

@Data
public class DailyOrderInfoPartnerGetDto {
  @ApiModelProperty(notes = "판매업체명")
  @RealGridColumnInfo(headText = "판매업체명", width = 150)
  private String businessNm;
  @ApiModelProperty(notes = "판매업체ID")
  @RealGridColumnInfo(headText = "판매업체ID", width = 100)
  private String partnerId;
  @ApiModelProperty(notes = "판매업체코드")
  @RealGridColumnInfo(headText = "판매업체코드", width = 100)
  private String ofVendorCd;
  @ApiModelProperty(notes = "주문건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderQty;
  @ApiModelProperty(notes = "주문금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "주문", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String orderPrice;
  @ApiModelProperty(notes = "취소건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cancelQty;
  @ApiModelProperty(notes = "취소금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "취소", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String cancelPrice;
  @ApiModelProperty(notes = "반품건수")
  @RealGridColumnInfo(headText = "건수", width = 200, groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String returnQty;
  @ApiModelProperty(notes = "반품금액")
  @RealGridColumnInfo(headText = "금액", width = 200, groupName = "반품", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_C)
  private String returnPrice;
}
