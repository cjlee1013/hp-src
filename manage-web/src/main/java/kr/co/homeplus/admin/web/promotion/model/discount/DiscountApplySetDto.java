package kr.co.homeplus.admin.web.promotion.model.discount;

import java.util.Date;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, checkBar = true)
@Getter
@Setter
public class DiscountApplySetDto {
    @RealGridColumnInfo(headText = "상품번호", sortable = true, width = 100)
    private String applyCd;

    @RealGridColumnInfo(headText = "상품명", sortable = true, columnType = RealGridColumnType.NAME, fieldType = RealGridFieldType.TEXT, width = 300)
    private String applyNm;

    @RealGridColumnInfo(headText = "쿠폰번호", sortable = true, width = 100)
    private String couponCd;

    @RealGridColumnInfo(headText = "할인방법", sortable = true, width = 100)
    private String discountType;

    @RealGridColumnInfo(headText = "할인금액(원)", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 100)
    private Long discountPrice;

    @RealGridColumnInfo(headText = "할인율(%)", sortable = true, columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER, width = 100)
    private Integer discountRate;

    @RealGridColumnInfo(headText = "시작일", sortable = true, width = 100)
    private String issueStartDt;

    @RealGridColumnInfo(headText = "종료일", sortable = true, width = 100)
    private String issueEndDt;

    @RealGridColumnInfo(headText = "카드그룹코드", sortable = true, width = 100)
    private String groupCd;

    private long discountNo;
    private String regNm;
    private Date regDt;
}