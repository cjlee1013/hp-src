package kr.co.homeplus.admin.web.statistics.model.dailyDivisionEKPI;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailyDivisionEKPIListSetDto {
    @ApiModelProperty(value = "검색시작일")
    private String startDt;

    @ApiModelProperty(value = "검색종료일")
    private String endDt;

    @ApiModelProperty(value = "거래유형")
    private String mallType;

    @ApiModelProperty(value = "점포유형")
    private String storeType;

    @ApiModelProperty(value = "대분류")
    private String divisionNo;

    @ApiModelProperty(value = "중분류")
    private String groupNo;

    @ApiModelProperty(value = "소분류")
    private String deptNo;

    @ApiModelProperty(value = "세분류")
    private String classNo;

    @ApiModelProperty(value = "사이트타입")
    private String siteType;
}
