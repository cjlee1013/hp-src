package kr.co.homeplus.admin.web.item.model.store;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreSetParamDto {

    //점포코드
    private String storeId;

    //점포명
    private String storeNm;

    //점포명(영문)
    private String storeNm2;

    //점포유형
    private String storeType;

    //점포구분
    private String storeKind;

    //구분
    private String storeAttr;

    //대표자명
    private String mgrNm;

    //기준점포
    private Integer originStoreId;

    //FC점포
    private Integer fcStoreId;

    //대표전화
    private String phone;

    //코스트센터
    private String onlineCostCenter;

    //코스트센터
    private String storeCostCenter;

    //우편번호
    private String zipcode;

    //주소
    private String addr1;

    //상세주소
    private String addr2;

    //소재지역
    private String region;

    //팩스
    private String fax;

    //휴무안내
    private String closedInfo;

    //운영시간
    private String oprTime;

    //사용여부
    private String useYn;

    //온라인여부
    private String onlineYn;

    //픽업여부
    private String pickupYn;

    //등록/수정자
    private String userId;

    //신규/수정구분
    private String isNew;
}
