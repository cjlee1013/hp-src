package kr.co.homeplus.admin.web.item.model.priceChangeHist;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true, checkBar = true)
@Getter
@Setter
public class PriceChangeHistGetDto {

    @RealGridColumnInfo(headText = "상품번호", width = 120, sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "점포ID", width = 70, sortable = true, order = 1)
    private String storeId;

    @RealGridColumnInfo(headText = "점포명", width = 90, order = 2)
    private String storeNm;

    @RealGridColumnInfo(headText = "이전가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true, order = 3)
    private int originPrice;

    @RealGridColumnInfo(headText = "변경가격", fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER_CENTER, sortable = true, order = 4)
    private int salePrice;

    @RealGridColumnInfo(headText = "변경자", order = 5)
    private String chgNm;

    @RealGridColumnInfo(headText = "변경일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 6, sortable = true)
    private String chgDt;

}
