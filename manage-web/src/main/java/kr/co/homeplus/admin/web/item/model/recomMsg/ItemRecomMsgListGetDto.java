package kr.co.homeplus.admin.web.item.model.recomMsg;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ItemRecomMsgListGetDto {
    @RealGridColumnInfo(headText = "추천문구 번호", sortable = true)
    private Long recomNo;

    @RealGridColumnInfo(headText = "관리명", width = 400)
    private String recomMng;

    @RealGridColumnInfo(headText = "추천문구", width = 400)
    private String recomMsg;

    @RealGridColumnInfo(headText = "점포유형")
    private String storeType;

    @RealGridColumnInfo(headText = "전시시작일시", width = 200)
    private String dispStartDt;

    @RealGridColumnInfo(headText = "전시종료일시", width = 200)
    private String dispEndDt;

    @RealGridColumnInfo(headText = "사용여부")
    private String useYn;

    @RealGridColumnInfo(headText = "등록자")
    private String regNm;

    @RealGridColumnInfo(headText = "등록일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String regDt;

    @RealGridColumnInfo(headText = "수정자")
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", columnType = RealGridColumnType.DATETIME, fieldType = RealGridFieldType.DATETIME, width = 200)
    private String chgDt;
}
