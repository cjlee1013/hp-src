package kr.co.homeplus.admin.web.item.model.safetyStock;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class SafetyStockHistGetDto {

    @RealGridColumnInfo(headText = "상품번호", sortable = true)
    private String itemNo;

    @RealGridColumnInfo(headText = "HYPER", width = 80, order = 2)
    private String hyperDispTxt;

    @RealGridColumnInfo(headText = "CLUB", width = 80, order = 3, hidden = true)
    private String clubDispTxt;

    @RealGridColumnInfo(headText = "EXPRESS", width = 80, order = 4)
    private String expressDispTxt;

    @RealGridColumnInfo(headText = "사용여부", width = 80, sortable = true, order = 5)
    private String useYnTxt;

    @RealGridColumnInfo(headText = "수정자", order = 12)
    private String chgNm;

    @RealGridColumnInfo(headText = "수정일", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.DATETIME, width = 150, order = 13, sortable = true)
    private String chgDt;
}
