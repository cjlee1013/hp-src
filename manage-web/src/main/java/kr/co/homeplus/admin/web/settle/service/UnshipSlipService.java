package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipCreateSetDto;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipListGetDto;
import kr.co.homeplus.admin.web.settle.model.unshipSlip.TdUnshipSlipListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UnshipSlipService {

    private final ResourceClient resourceClient;

    /**
     * TD미배송 전표조회 리스트 조회
     */
    public List<TdUnshipSlipListGetDto> getTdUnshipSlipList(TdUnshipSlipListSetDto tdUnshipSlipListSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            tdUnshipSlipListSetDto,
            "/admin/unshipSlip/getTdUnshipSlipList",
            new ParameterizedTypeReference<ResponseObject<List<TdUnshipSlipListGetDto>>>(){}).getData();
    }

    /**
     * TD미배송 전표조회 전표생성 가능 여부
     */
    public int getTdUnshipSlipCnt(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            tdUnshipSlipCreateSetDto,
            "/admin/unshipSlip/getTdUnshipSlipCnt",
            new ParameterizedTypeReference<ResponseObject<Integer>>(){}).getData();
    }

    /**
     * TD미배송 전표조회 전표 생성
     */
    public ResponseObject<Object> setTdUnshipSlip(TdUnshipSlipCreateSetDto tdUnshipSlipCreateSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            tdUnshipSlipCreateSetDto,
            "/admin/unshipSlip/setTdUnshipSlip",
            new ParameterizedTypeReference<ResponseObject<Object>>() {});
    }

}
