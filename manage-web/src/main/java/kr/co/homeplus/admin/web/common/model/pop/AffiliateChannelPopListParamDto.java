package kr.co.homeplus.admin.web.common.model.pop;

import lombok.Data;

@Data
public class AffiliateChannelPopListParamDto {

    private String searchSiteType;

    private String searchType;

    private String searchKeyword;

}
