package kr.co.homeplus.admin.web.claim.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
public class ClaimDetailInfoGetDto {

  @ApiModelProperty(value = "클레임타입", position = 1)
  public String claimType;

  @ApiModelProperty(value = "클레임상테", position = 2)
  public String claimStatus;

  @ApiModelProperty(value = "클레임번호", position = 3)
  public String claimNo;

  @ApiModelProperty(value = "클레임배송번호", position = 4)
  private String claimBundleNo;

  @ApiModelProperty(value = "주문번호", position = 5)
  public String purchaseOrderNo;

  @ApiModelProperty(value = "결제상태", position = 6)
  public String paymentStatus;

  @ApiModelProperty(value = "주문일시", position = 7)
  public String orderDt;

  @ApiModelProperty(value = "결제일시", position = 8)
  public String paymentFshDt;

  @ApiModelProperty(value = "주문디바이스", position = 9)
  public String platform;

  @ApiModelProperty(value = "마켓연동", position = 10)
  private String marketType;

  @ApiModelProperty(value = "회원번호", position = 11)
  public String userNo;

  @ApiModelProperty(value = "구매자이름", position = 12)
  public String buyerNm;

  @ApiModelProperty(value = "휴대폰번호", position = 13)
  public String buyerMobileNo;

  @ApiModelProperty(value = "배송번호", position = 14)
  public String bundleNo;

  @ApiModelProperty(value = "클레임타입코드", position = 15)
  private String claimTypeCode;

  @ApiModelProperty(value = "클레임요청번호", position = 16)
  private String claimReqNo;

  @ApiModelProperty(value = "클레임상태코드", position = 17)
  private String claimStatusCode;

  @ApiModelProperty(value = "합배송주문번호", position = 18)
  private String orgPurchaseOrderNo;

  @ApiModelProperty(value = "전체취소 여부", position = 19)
  private String claimPartYn;

  @ApiModelProperty(value = "비회원여부")
  private String nomemOrderYn;

  @ApiModelProperty(value = "구매자아이디(이메일)")
  private String buyerEmail;

  @ApiModelProperty(value = "마켓주문번호", position = 22)
  private String marketOrderNo;
}
