package kr.co.homeplus.admin.web.statistics.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.admin.web.statistics.model.DailyOrderInfo.*;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

/**
 * 통계 > 주문결제 통계
 */
@Service
@RequiredArgsConstructor
public class DailyOrderInfoService {
    private final ResourceClient resourceClient;
    public List<DailyOrderInfoPartnerGetDto> getPartnerPayment(DailyOrderInfoPartnerSetDto dailyOrderInfoPartnerSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            dailyOrderInfoPartnerSetDto,
            "/admin/statistics/getPartnerPayment",
            new ParameterizedTypeReference<ResponseObject<List<DailyOrderInfoPartnerGetDto>>>(){}).getData();
    }
    public List<DailyOrderInfoStoreGetDto> getStorePayment(DailyOrderInfoStoreSetDto dailyOrderInfoStoreSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            dailyOrderInfoStoreSetDto,
            "/admin/statistics/getStorePayment",
            new ParameterizedTypeReference<ResponseObject<List<DailyOrderInfoStoreGetDto>>>(){}).getData();
    }
    public List<DailyOrderInfoPartnerGetDto> getPartnerDecision(DailyOrderInfoPartnerSetDto dailyOrderInfoPartnerSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            dailyOrderInfoPartnerSetDto,
            "/admin/statistics/getPartnerDecision",
            new ParameterizedTypeReference<ResponseObject<List<DailyOrderInfoPartnerGetDto>>>(){}).getData();
    }
    public List<DailyOrderInfoStoreGetDto> getStoreDecision(DailyOrderInfoStoreSetDto dailyOrderInfoStoreSetDto) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SHIPPING,
            dailyOrderInfoStoreSetDto,
            "/admin/statistics/getStoreDecision",
            new ParameterizedTypeReference<ResponseObject<List<DailyOrderInfoStoreGetDto>>>(){}).getData();
    }
}
