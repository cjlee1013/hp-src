package kr.co.homeplus.admin.web.search.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.search.model.KeywordAttrSetParamDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KeywordAttrService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    public ResponseObject getKeywordAttrList(String searchKeyword, String searchUseYn) {
        String apiUri = "/keywordAttr/getKeywordAttrList?searchKeyword="+ URLEncoder.encode(searchKeyword, StandardCharsets.UTF_8)+"&searchUseYn="+searchUseYn;
        return getResponseObject(apiUri);
    }
    public ResponseObject getKeywordAttrMng(String keywordAttrNo) {
        String apiUri = "/keywordAttr/getKeywordAttrMng?keywordAttrNo="+keywordAttrNo;
        return getResponseObject(apiUri);
    }
    public ResponseObject postKeywordAttr(KeywordAttrSetParamDto param) {
        String apiUri = "/keywordAttr/postKeywordAttr";
        param.setRegId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }
    public ResponseObject putKeywordAttr(KeywordAttrSetParamDto param) {
        String apiUri = "/keywordAttr/putKeywordAttr";
        param.setChgId(loginCookieService.getUserInfo().getEmpId());
        return getResponseObject(apiUri, param);
    }

    private ResponseObject getResponseObject(String apiUri){
        ResourceClientRequest<Object> request = ResourceClientRequest.getBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.get(request, new TimeoutConfig()).getBody();
    }

    private <E> ResponseObject getResponseObject(String apiUri, E elements){
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SEARCHMNG)
            .uri(apiUri)
            .postObject(elements)
            .typeReference(new ParameterizedTypeReference<>() {})
            .build();
        return resourceClient.post(request, new TimeoutConfig()).getBody();
    }
}
