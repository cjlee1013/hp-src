package kr.co.homeplus.admin.web.statistics.model;

import lombok.Data;

@Data
public class ExhibitionSaleStatisticsSelectDto {
    String schApplyStartDt;
    String schApplyEndDt;

    String schExhibitionNm;
}
