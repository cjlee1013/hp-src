package kr.co.homeplus.admin.web.core.config;

import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.interceptor.AuthorizationCheckInterceptor;
import kr.co.homeplus.admin.web.core.interceptor.CertificationInterceptor;
import kr.co.homeplus.admin.web.core.interceptor.StaticResourceInterceptor;
import kr.co.homeplus.admin.web.core.interceptor.ValidateAuthorizedMenuInterceptor;
import kr.co.homeplus.admin.web.core.usermenu.UserMenuService;
import kr.co.homeplus.admin.web.core.view.excel.ExcelDataXlsxStreamingDownloadView;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 * servlet context 설정을 위한 config
 *
 * <p>dispatcher-servlet.xml의 역할을 대체하는 클래스로 어드민 내의
 * servlet context와 관련된 설정을 관리한다.
 *
 * <p>해당 config내에서는 Resources, Interceptors, ViewResolver,
 * excelDataDownloadView를 등록하여 관리하고 있다.
 *
 * @see #addResourceHandlers(ResourceHandlerRegistry)
 * @see #addInterceptors(InterceptorRegistry)
 * @see WebMvcConfigurer
 * @see TilesView
 * @see TilesConfigurer
 * @see ExcelDataXlsxStreamingDownloadView
 * @see CertificationInterceptor
 */
@Configuration
@RequiredArgsConstructor
public class ServletConfig implements WebMvcConfigurer {

    private final LoginCookieService cookieService;
    private final UserMenuService userMenuService;

    private final AuthorizationProperties properties;
    private final RealGridProperties realGridProperties;
    private final AdminProperties adminProperties;
    private final AuthorizationProperties authorizationProperties;

    /**
     * mvc:resources 설정<br> mapping = "/static/**", location = "/static/"
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    /**
     * interceptors 설정<br> mapping path="/**", {@link CertificationInterceptor} 를 등록
     *
     * @param registry {@link InterceptorRegistry}
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(certificationInterceptor())
            .addPathPatterns("/**");
        registry.addInterceptor(validateAuthorizedMenuInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(staticResourceInterceptor());
    }

    /**
     * TilesViewResolver 설정<br> viewResolver order = 0
     *
     * @return tiewsViewResolver
     */
    @Bean
    public UrlBasedViewResolver viewResolver() {
        UrlBasedViewResolver urlBasedViewResolver = new UrlBasedViewResolver();
        urlBasedViewResolver.setViewClass(TilesView.class); //tilesView
        urlBasedViewResolver.setOrder(0);
        return urlBasedViewResolver;
    }

    /**
     * BeannameviewResolver 설정<br> viewResolver order = 1
     *
     * @return beanNameViewResolver
     */
    @Bean
    public BeanNameViewResolver beanNameViewResolver() {
        BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
        beanNameViewResolver.setOrder(1);
        return beanNameViewResolver;
    }

    /**
     * TilesConfigurer 설정
     *
     * @return tilesConfigurer
     */
    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions(new String[]{"/WEB-INF/views/**/tiles.xml"});
        return tilesConfigurer;
    }

    @Bean
    public TilesViewResolver tilesViewResolver() {
        final TilesViewResolver resolver = new TilesViewResolver();
        resolver.setViewClass(TilesView.class);
        return resolver;
    }

    /**
     * excelDataDownloadView 설정
     *
     * @return ExcelDataXlsxStreamingDownloadView excelDataDownloadView
     */
    @Bean(name = "excelDataDownloadView")
    public ExcelDataXlsxStreamingDownloadView excelDataDownloadView() {
        return new ExcelDataXlsxStreamingDownloadView();
    }

    /**
     * 인증 체크와 공통 css,js 버전 설정을 위한 인터셉터
     *
     * @return {@link CertificationInterceptor}
     */
    @Bean
    public CertificationInterceptor certificationInterceptor() {
        return new CertificationInterceptor(cookieService, userMenuService,
            adminProperties, authorizationProperties);
    }

    /**
     * 메뉴에 대한 접근권한을 체크하기 위한 인터셉터 입니다
     *
     * @return {@link AuthorizationCheckInterceptor}
     */
    @Bean
    public AuthorizationCheckInterceptor authorizationCheckInterceptor() {
        return new AuthorizationCheckInterceptor(properties.getNetworkSeparateAllowVdiIp(),
            properties.getNetworkSeparateAllowCustomerIp());
    }

    /**
     * 정적파일 버전(js,css, ...)과 리얼그리드 관련 정보(라이센스, 경로 등...)를 관리하는 인터셉터 입니다.
     *
     * @return {@link StaticResourceInterceptor}
     */
    @Bean
    public StaticResourceInterceptor staticResourceInterceptor() {
        return new StaticResourceInterceptor(realGridProperties, adminProperties);
    }

    /**
     * 메뉴에 대한 접근권한을 체크하기 위한 인터셉터 입니다
     *
     * @return {@link ValidateAuthorizedMenuInterceptor}
     */
    @Bean
    public ValidateAuthorizedMenuInterceptor validateAuthorizedMenuInterceptor() {
        return new ValidateAuthorizedMenuInterceptor(properties, userMenuService, cookieService);
    }
}