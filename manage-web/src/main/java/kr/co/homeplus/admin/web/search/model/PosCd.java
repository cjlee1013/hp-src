package kr.co.homeplus.admin.web.search.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum PosCd {

    ALL("ALL","전체"),
    NOUN("NOUN","명사"),
    PRONOUN("PRONOUN","대명사"),
    NUMERAL("NUMERAL","수사"),
    VERB("VERB","동사"),
    ADJECTIVE("ADJECTIVE","형용사"),
    ADVERB("ADVERB","부사"),
    DETERMINER("DETERMINER","관형사"),
    JOSA("JOSA","조사"),
    EOMI("EOMI","어미"),
    PREFIX("PREFIX","접두사"),
    SUFFIX("SUFFIX","접미사");

    private String posCd;
    private String posNm;

    public static PosCd getType(String posCd) {
        for(PosCd pos : PosCd.values()) {
            if(pos.posCd.equals(posCd)) {
                return pos;
            }
        }
        return null;
    }
}
