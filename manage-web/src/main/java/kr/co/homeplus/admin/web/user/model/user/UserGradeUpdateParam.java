package kr.co.homeplus.admin.web.user.model.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 회원상세 > 등급변경 > 회원등급 변경 파라미터
 */
@Setter
@Getter
@ToString
public class UserGradeUpdateParam {
    /** 회원번호 **/
    @NotNull(message = "회원번호를 입력해주세요.")
    private Long userNo;

    /** 등급코드번호 **/
    @NotNull(message = "등급을 입력해주세요")
    private Integer gradeSeq;

    /** 등급명 **/
    @NotBlank(message = "등급을 입력해주세요")
    private String gradeNm;

    /** 구매건수 **/
    @NotNull(message = "구매건수를 입력해주세요")
    private Integer orderCnt;

    /** 구매금액 **/
    @NotNull(message = "구매금액을 입력해주세요")
    private Long orderAmount;

    /** 사유 **/
    @NotBlank(message = "사유를 입력해주세요")
    @Size(max = 20, message = "사유는 최대 20글자까지 입력 가능합니다")
    @Pattern(regexp = "^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|\\s]+$", message = "한글/영문/숫자만 입력 가능합니다.")
    private String reason;

    /** 등록자사번 **/
    private String regId;

    /** 콜센터 문의번호 **/
    @NotNull(message = "콜센터 문의번호를 입력해주세요.")
    private Long callCenterSeq;
}
