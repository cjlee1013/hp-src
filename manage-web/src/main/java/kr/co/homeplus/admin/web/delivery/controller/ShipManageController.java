package kr.co.homeplus.admin.web.delivery.controller;

import com.nhncorp.lucy.security.xss.XssPreventer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridBaseInfo;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumn;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridColumnType;
import kr.co.homeplus.admin.web.core.utility.realgrid.RealGridOption;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipHistoryGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipInfoPrintGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageConfirmOrderSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageNotiDelaySetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllPopListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllPopListSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipAllSetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipCompletePopListGetDto;
import kr.co.homeplus.admin.web.delivery.model.shipManage.ShipManageShipCompleteSetDto;
import kr.co.homeplus.admin.web.delivery.service.ShipCommonService;
import kr.co.homeplus.admin.web.delivery.service.ShipManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 배송관리 > 배송관리 > 택배배송관리
 */
@Controller
@RequestMapping("/ship")
public class ShipManageController {

    @Value("${front.homeplus.url}")
    private String homeplusFrontUrl;

    @Value("${front.theclub.url}")
    private String theclubFrontUrl;

    private final ShipManageService shipManageService;
    private final ShipCommonService shipCommonService;
    private final AuthorityService authorityService;

    // 일괄발송처리 결과 엑셀용 그리드
    private static final List<RealGridColumn> SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_OPTION = new RealGridOption("fill", false, true, false);

    // 선택발송처리 팝업 그리드
    private static final List<RealGridColumn> SHIP_MANAGE_SHIP_ALL_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_MANAGE_SHIP_ALL_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    // 배송완료 팝업 그리드
    private static final List<RealGridColumn> SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD = new ArrayList<>();
    private static final RealGridOption SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_OPTION = new RealGridOption("fill", false, false, false);

    public ShipManageController(ShipManageService shipManageService
                                , ShipCommonService shipCommonService
                                , AuthorityService authorityService) {
        this.shipManageService = shipManageService;
        this.shipCommonService = shipCommonService;
        this.authorityService = authorityService;

        // 일괄발송처리 결과 엑셀용 그리드
        SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("invoiceNo", "invoiceNo", "송장번호", RealGridColumnType.BASIC, 120, true, true));
        SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("scheduleShipDt", "scheduleShipDt", "배송예정일", RealGridColumnType.BASIC, 120, true, true));
        SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD.add(new RealGridColumn("failMsg", "failMsg", "실패메세지", RealGridColumnType.BASIC, 200, true, true));

        // 선택발송처리 팝업 그리드
        SHIP_MANAGE_SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_MANAGE_SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_MANAGE_SHIP_ALL_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));

        // 배송완료 팝업 그리드
        SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("shipNo", "shipNo", "배송SEQ", RealGridColumnType.BASIC, 80, false, false));
        SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("bundleNo", "bundleNo", "배송번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("orderItemNo", "orderItemNo", "상품주문번호", RealGridColumnType.BASIC, 90, true, true));
        SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("receiverNm", "receiverNm", "수령인", RealGridColumnType.BASIC, 90, true, true));
        SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD.add(new RealGridColumn("itemNm1", "itemNm1", "상품명", RealGridColumnType.BASIC, 180, true, true));
    }

    /**
     * 택배배송관리 Main Page
     */
    @GetMapping("/shipManage/shipManageMain")
    public String shipManageMain(Model model) throws Exception {

        //SO 권한 데이터 설정
        shipCommonService.setStoreInfo(model);

        model.addAttribute("packageExcel", authorityService.hasRole("package_excel"));
        model.addAttribute("storeType", shipCommonService.getCode("store_type"));
        model.addAttribute("shipStatus", shipCommonService.getCode("ship_status"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("theclubFrontUrl", theclubFrontUrl);
        model.addAttribute("homeplusFrontUrl", homeplusFrontUrl);

        model.addAllAttributes(RealGridHelper.create("shipManageGridBaseInfo", ShipManageListGetDto.class));

        return "/delivery/shipManageMain";
    }

    /**
     * 택배배송관리 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipManageList.json", method = RequestMethod.GET)
    public List<ShipManageListGetDto> getShipManageList(ShipManageListSetDto shipManageListSetDto) {
        return shipManageService.getShipManageList(shipManageListSetDto);
    }

    /**
     * 주문 확인
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setConfirmOrder.json")
    public ResponseObject<Object> setConfirmOrder(@RequestBody ShipManageConfirmOrderSetDto shipManageConfirmOrderSetDto) throws Exception {
        return shipManageService.setConfirmOrder(shipManageConfirmOrderSetDto);
    }

    /**
     * 발송지연안내
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setNotiDelay.json")
    public ResponseObject<Object> setNotiDelay(@RequestBody ShipManageNotiDelaySetDto shipManageNotiDelaySetDto) throws Exception {
        return shipManageService.setNotiDelay(shipManageNotiDelaySetDto);
    }

    /**
     * 일괄발송처리
     */
    @ResponseBody
    @PostMapping("/shipManage/setShipAllExcel.json")
    public ResponseObject<Map<String, Object>> setShipAllExcel(MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
        return shipManageService.setShipAllExcel(multipartHttpServletRequest);
    }

    /**
     * 선택발송처리
     */
    @ResponseBody
    @PostMapping("/shipManage/setShipAll.json")
    public ResponseObject<Object> setShipAll(@RequestBody ShipManageShipAllSetDto shipManageShipAllSetDto) throws Exception {
        return shipManageService.setShipAll(shipManageShipAllSetDto);
    }

    /**
     * 배송완료
     */
    @ResponseBody
    @PostMapping(value = "/shipManage/setShipComplete.json")
    public ResponseObject<Object> setShipComplete(@RequestBody ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) throws Exception {
        return shipManageService.setShipComplete(shipManageShipCompleteSetDto);
    }

    /**
     * 배송완료 팝업 리스트 조회
     */
    @ResponseBody
    @PostMapping("/shipManage/getShipCompletePopList.json")
    public List<ShipManageShipCompletePopListGetDto> getShipCompletePopList(@RequestBody ShipManageShipCompleteSetDto shipManageShipCompleteSetDto) {
        return shipManageService.getShipCompletePopList(shipManageShipCompleteSetDto);
    }

    /**
     * 배송완료 팝업
     */
    @GetMapping("/popup/getShipCompletePop")
    public String getShipCompletePop(Model model, @RequestParam(value="callback") String callBackScript) throws Exception {
        model.addAttribute("callBackScript",callBackScript);
        model.addAttribute("shipManageShipCompletePopGridBaseInfo", new RealGridBaseInfo("shipManageShipCompletePopGridBaseInfo", SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_HEAD, SHIP_MANAGE_SHIP_COMPLETE_POP_GRID_OPTION).toString());

        return "/delivery/pop/shipManageShipCompletePop";
    }

    /**
     * 발송지연안내 팝업
     */
    @GetMapping("/popup/getNotiDelayPop")
    public String getNotiDelayPop(Model model
        , @RequestParam(value="callback") String callBackScript
        , @RequestParam(value="bundleNo") String bundleNo) throws Exception {
        model.addAttribute("delayShipCd", shipCommonService.getCode("delay_ship_cd"));
        model.addAttribute("bundleNo",bundleNo);
        model.addAttribute("callBackScript",callBackScript);

        return "/delivery/pop/shipManageNotiDelayPop";
    }

    /**
     * 일괄발송처리 팝업
     */
    @GetMapping("/popup/getShipAllExcelPop")
    public String getShipAllExcelPop(Model model
                                , @RequestParam(value="callback") String callBackScript
                                , @RequestParam(value="templateUrlDlv") String templateUrlDlv
                                , @RequestParam(value="templateUrlDrct") String templateUrlDrct) throws Exception {
        model.addAttribute("dlvCd", shipCommonService.getCode("dlv_cd"));
        model.addAttribute("callBackScript",callBackScript);
        model.addAttribute("templateUrlDlv",templateUrlDlv);
        model.addAttribute("templateUrlDrct",templateUrlDrct);
        model.addAttribute("shipManageShipAllExcelPopGridBaseInfo", new RealGridBaseInfo("shipManageShipAllExcelPopGridBaseInfo", SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_HEAD, SHIP_MANAGE_SHIP_ALL_EXCEL_POP_GRID_OPTION).toString());

        return "/delivery/pop/shipManageShipAllExcelPop";
    }

    /**
     * 선택발송처리 팝업
     */
    @GetMapping("/popup/getShipAllPop")
    public String getShipAllPop(Model model, @RequestParam(value="callback") String callBackScript
                                            , @RequestParam(value="bundleNo") String bundleNo
                                            , @RequestParam(value="mallType") String mallType) throws Exception {
        model.addAttribute("dlvCd", shipCommonService.getCode("dlv_cd"));
        model.addAttribute("shipMethod", shipCommonService.getCode("ship_method"));
        model.addAttribute("callBackScript",callBackScript);
        model.addAttribute("bundleNo",bundleNo);
        model.addAttribute("mallType",mallType);
        model.addAttribute("shipManageShipAllPopGridBaseInfo", new RealGridBaseInfo("shipManageShipAllPopGridBaseInfo", SHIP_MANAGE_SHIP_ALL_POP_GRID_HEAD, SHIP_MANAGE_SHIP_ALL_POP_GRID_OPTION).toString());

        return "/delivery/pop/shipManageShipAllPop";
    }

    /**
     * 선택발송처리 팝업 리스트 조회
     */
    @ResponseBody
    @RequestMapping(value = "/shipManage/getShipAllPopList.json", method = RequestMethod.GET)
    public List<ShipManageShipAllPopListGetDto> getShipAllPopList(ShipManageShipAllPopListSetDto shipManageShipAllPopListSetDto) {
        return shipManageService.getShipAllPopList(shipManageShipAllPopListSetDto);
    }

    /**
     * 배송조회 팝업
     */
    @GetMapping("/popup/getShipHistoryPop")
    public String getShipAllPop(Model model
        , @RequestParam(value="dlvCd") String dlvCd
        , @RequestParam(value="invoiceNo") String invoiceNo) {

        List<MngCodeGetDto> dlvCdList = shipCommonService.getCode("dlv_cd");
        String dlvNm = "";

        for (MngCodeGetDto dto : dlvCdList) {
            if (dlvCd.equals(dto.getMcCd())) {
                dlvNm = dto.getMcNm();
            }
        }

        model.addAttribute("dlvCd",dlvCd);
        model.addAttribute("dlvNm",dlvNm);
        model.addAttribute("invoiceNo",invoiceNo);

        return "/delivery/pop/shipManageShipHistoryPop";
    }

    /**
     * 배송 히스토리 조회
     */
    @ResponseBody
    @GetMapping("/shipManage/getShipHistory.json")
    public List<ShipHistoryGetDto> getShipHistory(@RequestParam(value = "dlvCd") String dlvCd
                                                  ,@RequestParam(value = "invoiceNo") String invoiceNo) {
        return shipManageService.getShipHistory(dlvCd, invoiceNo);
    }

    /**
     * 배송정보출력 팝업
     */
    @GetMapping("/popup/getShipInfoPrintPop")
    public String getShipInfoPrintPop(Model model
        , @RequestParam(value="bundleNo") String bundleNo) {

        model.addAttribute("bundleNo", XssPreventer.escape(bundleNo));

        return "/delivery/pop/shipInfoPrintPop";
    }

    /**
     * 배송정보출력 조회
     */
    @ResponseBody
    @GetMapping("/shipManage/getShipInfoPrintList.json")
    public List<ShipInfoPrintGetDto> getShipInfoPrintList(@RequestParam(value = "bundleNo") String bundleNo) {
        return shipManageService.getShipInfoPrintList(bundleNo);
    }
}
