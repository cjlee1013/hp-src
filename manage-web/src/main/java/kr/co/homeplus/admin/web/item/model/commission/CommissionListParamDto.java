package kr.co.homeplus.admin.web.item.model.commission;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommissionListParamDto {
    private String searchCateCd;
    private String searchKeyword;
    private String searchUseYn;
    private String searchType;
}
