package kr.co.homeplus.admin.web.partner.model.market;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import java.util.List;
import kr.co.homeplus.admin.web.partner.model.PartnerManagerGetDto;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarketGetDto {

    // 마켓연동업체ID
    private String partnerId;
    // 상호
    private String partnerNm;
    // 사업자유형
    private String operatorType;
    // 마켓연동업체유형
    private String affiliateType;
    // 회원상태
    private String partnerStatus;
    // 연동업체명
    private String businessNm;
    // 대표자명
    private String partnerOwner;
    // 사업자등록번호
    private String partnerNo;
    // 업태
    private String businessConditions;
    // 종목
    private String bizCateCd;
    // 종목명
    private String bizCateCdNm;
    // 우편번호
    private String zipcode;
    // 주소1
    private String addr1;
    // 주소2
    private String addr2;
    // 사이트명
    private String siteNm;
    // 사이트url
    private String siteUrl;
    // 수수료
    private BigDecimal commissionRate;
    // 벤더코드
    private String vendorCd;
    // 상품API인증키
    private String itemCertKey;
    // 주문API인증키
    private String orderCertKey;
    // 사용여부
    private String useYn;
    // 은행코드
    private String bankCd;
    // 은행계좌
    private String bankAccountNo;
    // 예금주
    private String depositor;
    // 정산주기
    private String settleCycleType;

    // 담당자정보
    List<PartnerManagerGetDto> managerList;

    // 수수료정보
    List<MarketCommissionListGetDto> commissionList;

}
