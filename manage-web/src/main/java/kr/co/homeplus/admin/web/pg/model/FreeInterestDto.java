package kr.co.homeplus.admin.web.pg.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Data;

@Data
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL)
public class FreeInterestDto {
    @RealGridColumnInfo(headText = "번호", width = 50, sortable = true, fieldType = RealGridFieldType.NUMBER, columnType = RealGridColumnType.NUMBER)
    private Long freeInterestMngSeq;
    @RealGridColumnInfo(headText = "사이트", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String siteType;
    @RealGridColumnInfo(headText = "제목", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.NAME)
    private String freeInterestNm;
    @RealGridColumnInfo(headText = "적용시작일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyStartDt;
    @RealGridColumnInfo(headText = "적용종료일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String applyEndDt;
    @RealGridColumnInfo(headText = "사용여부", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String useYn;
    @RealGridColumnInfo(headText = "등록자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regId;
    @RealGridColumnInfo(headText = "등록일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String regDt;
    @RealGridColumnInfo(headText = "수정자", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgId;
    @RealGridColumnInfo(headText = "수정일", sortable = true, fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC)
    private String chgDt;
}