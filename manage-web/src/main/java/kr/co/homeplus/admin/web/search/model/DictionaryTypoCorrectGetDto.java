package kr.co.homeplus.admin.web.search.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DictionaryTypoCorrectGetDto {

    private TypoCorrectModel typoCorrect;

    @Getter @Setter
    public class TypoCorrectModel {
        private Integer wordId;
        private String wordName;
        private String correctedTerms;
        private String updateDt;
    }
}
