package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class PopularKeywordGridDto {
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = false, checkBar = false)
    public static class PopularKeyword {
        @RealGridColumnInfo(headText = "순서", width = 30, sortable = true, columnType = RealGridColumnType.NUMBER)
        private Integer rank;
        @RealGridColumnInfo(headText = "키워드", width = 120, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "검색수", width = 40, sortable = true, columnType = RealGridColumnType.BASIC)
        private Integer count;
        @RealGridColumnInfo(headText = "변동", width = 30, sortable = true, columnType = RealGridColumnType.BASIC)
        private Integer variation;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = false, checkBar = false)
    public static class PopularFixed {
        @RealGridColumnInfo(headText = "키워드", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "순위", width = 40, sortable = true, columnType = RealGridColumnType.NUMBER, fieldType = RealGridFieldType.NUMBER)
        private Integer rank;
        @RealGridColumnInfo(headText = "시작일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "종료일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = false, checkBar = false)
    public static class PopularExcept {
        @RealGridColumnInfo(headText = "키워드", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "시작일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "종료일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = false, checkBar = true)
    public static class PopularFullExcept {
        @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, columnType = RealGridColumnType.BASIC)
        private String apply;
        @RealGridColumnInfo(headText = "정렬필드", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private String sort;
        @RealGridColumnInfo(headText = "ID", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer managementNo;
        @RealGridColumnInfo(headText = "키워드", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "시작일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "종료일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
        @RealGridColumnInfo(headText = "등록자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regNm;
        @RealGridColumnInfo(headText = "등록일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgNm;
        @RealGridColumnInfo(headText = "수정일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgDt;
        @RealGridColumnInfo(headText = "노출여부", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private String useYn;
    }
    @Getter @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = false, checkBar = true)
    public static class PopularFullFixed {
        @RealGridColumnInfo(headText = "사용여부", width = 50, sortable = true, columnType = RealGridColumnType.BASIC)
        private String apply;
        @RealGridColumnInfo(headText = "정렬필드", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private String sort;
        @RealGridColumnInfo(headText = "ID", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private Integer managementNo;
        @RealGridColumnInfo(headText = "키워드", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String keyword;
        @RealGridColumnInfo(headText = "순위", width = 50, sortable = true, columnType = RealGridColumnType.NUMBER, fieldType = RealGridFieldType.NUMBER)
        private Integer rank;
        @RealGridColumnInfo(headText = "시작일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String startDt;
        @RealGridColumnInfo(headText = "종료일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String endDt;
        @RealGridColumnInfo(headText = "등록자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regNm;
        @RealGridColumnInfo(headText = "등록일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String regDt;
        @RealGridColumnInfo(headText = "수정자", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgNm;
        @RealGridColumnInfo(headText = "수정일", width = 100, sortable = true, columnType = RealGridColumnType.BASIC)
        private String chgDt;
        @RealGridColumnInfo(headText = "노출여부", width = 50, sortable = true, columnType = RealGridColumnType.BASIC, hidden = true)
        private String useYn;
    }
}
