package kr.co.homeplus.admin.web.manage.model.dspMainThemePeriod;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class DspMainThemePeriodItemExcelDto {

    @EqualsAndHashCode.Include
    private String itemStoreType;
    @EqualsAndHashCode.Include
    private String itemNo;
    @EqualsAndHashCode.Include
    private String dispYn;
    @EqualsAndHashCode.Include
    private String itemNm;
    @EqualsAndHashCode.Include
    private String dispYnTxt;

}
