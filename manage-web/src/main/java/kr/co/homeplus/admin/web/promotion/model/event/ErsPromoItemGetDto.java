package kr.co.homeplus.admin.web.promotion.model.event;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
@Getter
@Setter
public class ErsPromoItemGetDto {
    @RealGridColumnInfo(headText = "상품번호")
    private String itemNo;

    @RealGridColumnInfo(headText = "상품명")
    private String itemNm;

    @RealGridColumnInfo(headText = "개별아이템 상품권금액", columnType = RealGridColumnType.NUMBER_C, fieldType = RealGridFieldType.NUMBER)
    private Integer ticketAmt;
}