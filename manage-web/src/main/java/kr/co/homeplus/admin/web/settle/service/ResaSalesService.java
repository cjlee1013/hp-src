package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesListGetDto;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesListSetDto;
import kr.co.homeplus.admin.web.settle.model.resaSales.ResaSalesUploadSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.ResourceClientRequest;
import kr.co.homeplus.plus.api.support.client.TimeoutConfig;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
@RequiredArgsConstructor
public class ResaSalesService {

    private final ResourceClient resourceClient;
    /**
     * 정산관리 > 회계자료조회 > SALES 자료조회
     */
    public List<ResaSalesListGetDto> getList(ResaSalesListSetDto resaSalesListSetDto) throws Exception {
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SETTLE)
            .uri("/admin/ResaSales/getList")
            .postObject(resaSalesListSetDto)
            .typeReference(new ParameterizedTypeReference<>(){})
            .build();

        ResponseObject<Object> responseObject = resourceClient.post(request,new TimeoutConfig()).getBody();
        return (List<ResaSalesListGetDto>)responseObject.getData();
    }

    public int getExist(String basicDt) throws Exception {
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            basicDt,
            "/admin/ResaSales/getExist",
            new ParameterizedTypeReference<ResponseObject<Integer>>(){}).getData();
    }

    public int setUpload(List<ResaSalesUploadSetDto> resaSalesUploadSetDtoList,String regId) throws Exception {
        for (ResaSalesUploadSetDto resaSalesUploadSetDto : resaSalesUploadSetDtoList) {
            resaSalesUploadSetDto.setRegId(regId);
        }
        ResourceClientRequest<Object> request = ResourceClientRequest.postBuilder()
            .apiId(ResourceRouteName.SETTLE)
            .uri("/admin/ResaSales/setUpload")
            .postObject(resaSalesUploadSetDtoList)
            .typeReference(new ParameterizedTypeReference<>(){})
            .build();

        ResponseObject<Object> responseObject = resourceClient.post(request,new TimeoutConfig()).getBody();
        return (Integer)responseObject.getData();
    }
}
