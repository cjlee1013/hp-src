package kr.co.homeplus.admin.web.search.model;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

public class DeployHistoryGridDto {
    @Getter
    @Setter
    @RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
    public static class DeployHistory{
        @RealGridColumnInfo(headText = "기능요소", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String actionType;
        @RealGridColumnInfo(headText = "사전유형", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String dictType;
        @RealGridColumnInfo(headText = "서비스", width = 70, columnType = RealGridColumnType.BASIC)
        private String service;
        @RealGridColumnInfo(headText = "구성", width = 50, columnType = RealGridColumnType.BASIC, sortable = true)
        private String appType;
        @RealGridColumnInfo(headText = "도메인", width = 200, columnType = RealGridColumnType.BASIC)
        private String privateDomain;
        @RealGridColumnInfo(headText = "성공여부", width = 50, columnType = RealGridColumnType.BASIC)
        private String successYn;
        @RealGridColumnInfo(headText = "배포자", width = 50, columnType = RealGridColumnType.BASIC)
        private String deployer;
        @RealGridColumnInfo(headText = "배포일", width = 100, columnType = RealGridColumnType.DATETIME, sortable = true)
        private String deployDt;
    }
}
