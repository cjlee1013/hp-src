package kr.co.homeplus.admin.web.item.model.market;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//외부연동 설정관리 상품 설정정보 (복수설정)
public class MarketItemsSettingSetDto {

	//파트너ID
	private String providerId;

	//설정여부
	private String useYn;

	//중지기간설정여부(Y:설정, N:설정안함)
	private String stopPeriodYn;

	//중지시작일
	private String stopStartDt;

	//중지종료일
	private String stopEndDt;

	//상품번호 리스트
	private List<String> itemNoList;

	//등록/수정자
	private String userId;
}
