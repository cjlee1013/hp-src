package kr.co.homeplus.admin.web.item.model.sameClass;

import lombok.Data;

@Data
public class SameClassGroupListParamDto {

    private String searchUseYn;

    private String searchType;

    private String searchKeyword;

}
