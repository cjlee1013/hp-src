package kr.co.homeplus.admin.web.settle.controller;

import java.util.List;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgReceivableGetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleDetailGetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleSetDto;
import kr.co.homeplus.admin.web.settle.model.pgSettle.PgSettleSumGetDto;
import kr.co.homeplus.admin.web.settle.service.PgSettleService;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/settle/pgSettle")
public class PgSettleController {
    private final PgSettleService pgSettleService;

    @RequestMapping(value = "/pgSettleList", method = RequestMethod.GET)
    public String pgSettleList(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.createForGroup("pgSettleListBaseInfo", PgSettleSumGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("pgSettleDetailBaseInfo", PgSettleDetailGetDto.class));
        model.addAllAttributes(RealGridHelper.createForGroup("pgSettleHistoryBaseInfo", PgReceivableGetDto.class));

        return "/settle/pgSettleList";
    }

    @ResponseBody
    @RequestMapping(value = "/getPgSettleSum.json", method = RequestMethod.GET)
    public List<PgSettleSumGetDto> getPgSettleSum(PgSettleSetDto pgSettleSetDto) throws Exception {
        return pgSettleService.getPgSettleSum(pgSettleSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getPgSettleDetail.json", method = RequestMethod.GET)
    public List<PgSettleDetailGetDto> getPgSettleDetail(PgSettleSetDto pgSettleSetDto) throws Exception {
        return pgSettleService.getPgSettleDetail(pgSettleSetDto);
    }

    @ResponseBody
    @RequestMapping(value = "/getPgReceivable.json", method = RequestMethod.GET)
    public List<PgReceivableGetDto> getPgReceivable(PgSettleSetDto pgSettleSetDto) throws Exception {
        return pgSettleService.getPgReceivable(pgSettleSetDto);
    }
}