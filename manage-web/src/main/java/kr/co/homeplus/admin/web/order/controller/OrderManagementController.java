package kr.co.homeplus.admin.web.order.controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import kr.co.homeplus.admin.web.claim.model.StoreInfoGetDto;
import kr.co.homeplus.admin.web.common.model.codeMng.MngCodeGetDto;
import kr.co.homeplus.admin.web.common.service.CodeService;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.order.enums.OrderShipCode;
import kr.co.homeplus.admin.web.order.model.orderPopup.MultiOrderShipAddrInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderCashReceiptDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderCouponDiscountDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderDelayShipInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderGiftInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderInvoiceEditGetDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderInvoiceRegGetDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderPartnerInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderPromoInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderSaveInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderShipAddrInfoDto;
import kr.co.homeplus.admin.web.order.model.orderPopup.OrderShipCompleteGetDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchClaimDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchDetailDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchDlvShipDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchGetDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchHistoryDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchPaymentDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchPriceDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchProdDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchSetDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchShipDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchStoreShipDto;
import kr.co.homeplus.admin.web.order.model.orderSearch.OrderSearchSubstitutionDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipEditDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipInfoGetDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipInfoSetDto;
import kr.co.homeplus.admin.web.order.model.ship.NoRcvShipRegGto;
import kr.co.homeplus.admin.web.order.model.ship.NoReceiveShipPopDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderInvoiceRegDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderProcessDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.admin.web.order.model.ship.OrderShipOriginInfo;
import kr.co.homeplus.admin.web.order.model.ship.StoreShipMemoDto;
import kr.co.homeplus.admin.web.order.model.ship.StoreShipMsgDto;
import kr.co.homeplus.admin.web.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.admin.web.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.admin.web.order.service.OrderManagementService;
import kr.co.homeplus.admin.web.order.utils.ObjectUtils;
import kr.co.homeplus.admin.web.order.utils.OrderUtil;
import kr.co.homeplus.admin.web.voc.model.OneOnOneInquiryMemberMemoListGetDto;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.realgrid.RealGridHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 주문관리 > 주문관리
 */
@Controller
@RequestMapping("/order")
@RequiredArgsConstructor
@Slf4j
public class OrderManagementController {
    @Value("${front.homeplus.url}")
    private String frontUrl;
    @Value("${front.theclub.url}")
    private String theClub;

    private final OrderManagementService orderManagementService;

    private final LoginCookieService loginCookieService;

    private final CodeService codeService;

    /**
     * 주문관리 Main
     *
     * @param model model
     * @return 주문관리 페이지
     */
    @GetMapping("/orderManageMain")
    public String orderManageMain(Model model) throws Exception {
        model.addAllAttributes(RealGridHelper.create("orderManageGrid", OrderSearchGetDto.class));
        model.addAttribute("storeType", codeService.getCode("store_type").get("store_type"));
        model.addAttribute("frontUrl", frontUrl);
        model.addAttribute("theClub", theClub);
        String storeId = loginCookieService.getUserInfo().getStoreId();

        if(StringUtils.isNotEmpty(storeId)){
            StoreInfoGetDto storeInfoGetDto = orderManagementService.getStoreInfo(storeId);
            model.addAttribute("storeId", storeId);
            model.addAttribute("storeNm", storeInfoGetDto.getStoreNm());
            model.addAttribute("soStoreType", storeInfoGetDto.getStoreType());
        }
        return "/order/orderManageMain";
    }

    /**
     * 주문정보 상세 페이지
     *
     * @param model model
     * @param purchaseOrderNo 주문번호
     * @return 주문정보 상세페이지
     */
    @GetMapping("/orderManageDetail")
    public String orderManageDetail(Model model, @RequestParam("purchaseOrderNo") long purchaseOrderNo) {
        model.addAttribute("frontUrl", frontUrl);
        model.addAttribute("theClub", theClub);
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        model.addAllAttributes(RealGridHelper.create("orderDetailGrid", OrderSearchDetailDto.class));
        model.addAllAttributes(RealGridHelper.create("orderDetailStoreShipGrid", OrderSearchStoreShipDto.class));
        model.addAllAttributes(RealGridHelper.create("orderDetailDlvShipGrid", OrderSearchDlvShipDto.class));
        model.addAllAttributes(RealGridHelper.create("orderClaimGrid", OrderSearchClaimDto.class));
        model.addAllAttributes(RealGridHelper.create("orderPaymentGrid", OrderSearchPaymentDto.class));
        model.addAllAttributes(RealGridHelper.create("orderHistoryGrid", OrderSearchHistoryDto.class));
        model.addAllAttributes(RealGridHelper.create("orderSubstitutionGrid", OrderSearchSubstitutionDto.class));
        //

        return "/order/orderManageDetail";
    }

    /**
     * 주문관리 - 배송관련 팝업
     * (송장등록 / 송장수정 / 배송완료
     *
     * @param model model
     * @param parameterMap 배송정보 관련 맵
    * @return 배송관련 팝업 페이지
     * @throws Exception 오류 발생시 오류 처리.
     */
    @GetMapping("/popup/orderShippingPop")
    public String orderInvoiceRegPop(Model model, @RequestParam HashMap<String, Object> parameterMap) throws Exception {
        String pageType = ObjectUtils.toString(parameterMap.get("pageType"));
        Map<String, List<MngCodeGetDto>> mngCodeMap = codeService.getCode("dlv_cd", "ship_method");
        model.addAttribute("bundleNo", ObjectUtils.toLong(parameterMap.get("bundleNo")));
        model.addAttribute("dlvCdList", mngCodeMap.get("dlv_cd"));
//        List<MngCodeGetDto> codeList = mngCodeMap.get("ship_method").stream().filter(dto -> dto.getRef1().equalsIgnoreCase(ObjectUtils.toString(parameterMap.get("shipType")))).collect(Collectors.toList());
        String shipMethod = new StringBuffer(ObjectUtils.toString(parameterMap.get("shipMethod"))).insert(2, "_").toString();
        List<MngCodeGetDto> codeList = new ArrayList<>();
        if(shipMethod.contains("TD")) {
            codeList = mngCodeMap.get("ship_method").stream().filter(dto -> dto.getRef1().equals("TD")).filter(dto -> shipMethod.contains(dto.getMcCd())).collect(Collectors.toList());
        } else {
            codeList = mngCodeMap.get("ship_method").stream().filter(dto -> dto.getRef1().equals("DS")).collect(Collectors.toList());
        }
        model.addAttribute("shipMethodList", codeList);

        if (pageType.equalsIgnoreCase("REG")) {
            model.addAllAttributes(RealGridHelper.create("orderInvoiceRegGrid", OrderInvoiceRegGetDto.class));
            return "/order/pop/orderInvoiceReg";
        } else if (pageType.equalsIgnoreCase("EDIT")) {
            model.addAllAttributes(RealGridHelper.create("orderInvoiceEditGrid", OrderInvoiceEditGetDto.class));
            return "/order/pop/orderInvoiceEdit";
        } else if (pageType.equalsIgnoreCase("COMPLETE")) {
            model.addAllAttributes(RealGridHelper.create("orderShipCompleteGrid", OrderShipCompleteGetDto.class));
            return "/order/pop/orderShipComplete";
        } else {
            return "/core/error/error404";
        }
    }

    /**
     * 주문관리 - 배송지변경
     *
     * @param model model
     * @param bundleNo 배송번호
     * @param shipType 배송타입(Tweak)
     * @return 배송지 정보 페이지
     */
    @GetMapping("/popup/orderShipChangePop")
    public String orderShipChangePop(Model model, @RequestParam("bundleNo") long bundleNo, @RequestParam("shipType") String shipType, @RequestParam("shipAreaType") String shipAreaType) throws Exception {
        model.addAttribute("bundleNo", bundleNo);
        model.addAttribute("shipType", shipType);
        model.addAttribute("shipAreaType", shipAreaType);
        model.addAttribute("dlvMsgCd", OrderUtil.getDlvCodeMap(shipType));

        if(shipType.equalsIgnoreCase("TD")){
            model.addAttribute("auroraMsgCd", OrderUtil.getDlvCodeMap("AURORA"));
            return "/order/pop/orderStoreShipChange";
        } else if (shipType.equalsIgnoreCase("MULTI")) {
            return "/order/pop/orderMultiShipChange";
        }
        return "/order/pop/orderDeliveryShipChange";
    }

    /**
     * 주문관리 - 미수취
     *
     * @param model model
     * @param popDto 미수취 페이지 데이터
     * @return 미수취 타입에 따른 페이지
     */
    @GetMapping("/popup/noReceiveShipPop")
    public String noReceiveShipPop(Model model, @ModelAttribute NoReceiveShipPopDto popDto) throws Exception {
        // basic Parameter Model
        model.addAttribute("noRcvShipParam", new Gson().toJson(ObjectUtils.getConvertMap(popDto)));

        if (popDto.getNoRcvPopType().equalsIgnoreCase("REG")) {
            model.addAttribute("noRcvDeclrTypeList", OrderUtil.getNoRcvCodeMap("declare_type"));
            return "/order/pop/noReceiveShipReg";
        } else if (popDto.getNoRcvPopType().equalsIgnoreCase("EDIT")) {
            model.addAttribute("noRcvProcessTypeList", OrderUtil.getNoRcvCodeMap("process_type"));
            return "/order/pop/noReceiveShipEdit";
        } else if (popDto.getNoRcvPopType().equalsIgnoreCase("COMPLETE")) {
            return "/order/pop/noReceiveShipComplete";
        } else {
            return "/core/error/error404";
        }
    }

    @GetMapping("/popup/orderShipChangeSlotPop")
    public String orderShipChangeSlotPop(Model model, @RequestParam LinkedHashMap<String, Object> parameterMap) throws Exception {

        OrderShipOriginInfo shipInfoDto = orderManagementService.getOrderShipOriginInfo(ObjectUtils.toLong(parameterMap.get("purchaseOrderNo"))).getData();
        if(shipInfoDto.getOrderType().equals("ORD_COMBINE")){
            parameterMap.put("purchaseOrderNo", shipInfoDto.getOrgPurchaseOrderNo());
            parameterMap.put("bundleNo", shipInfoDto.getOrgBundleNo());
        }
        model.addAttribute("parameter", new Gson().toJson(parameterMap));
        model.addAttribute("dlvMsgCd", OrderUtil.getDlvCodeMap(ObjectUtils.toString(parameterMap.get("shipType"))));
        model.addAttribute("auroraMsgCd", OrderUtil.getDlvCodeMap("AURORA"));
        return "/order/pop/orderShipChangeSlot";
    }

    @GetMapping("/popup/orderDelayShipPop")
    public String orderDelayShipPop(Model model, @RequestParam("shipNo") long shipNo) throws Exception {
        model.addAttribute("shipNo", shipNo);
        return "/order/pop/orderDelayShipPop";
    }

    @GetMapping("/popup/orderPartnerInfoPop")
    public String orderPartnerInfoPop(Model model, @RequestParam("partnerId") String partnerId, @RequestParam("bundleNo") long bundleNo) throws Exception {
        model.addAttribute("partnerId", partnerId);
        model.addAttribute("bundleNo", bundleNo);
        return "/order/pop/orderPartnerInfoPop";
    }

    /**
     * 주문관리 - 미수취
     *
     * @param model model
     * @return 미수취 타입에 따른 페이지
     */
    @GetMapping("/popup/orderDiscountPop")
    public String orderDiscountPop(Model model, @RequestParam("type") String type, @RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        // type : promotion / coupon / gift / save
        switch (type.toUpperCase(Locale.KOREA)){
            case "PROMOTION" :
                model.addAllAttributes(RealGridHelper.create("orderPromoInfoGrid", OrderPromoInfoDto.class));
                return "/order/pop/orderPromoInfoPop";
            case "COUPON" :
                model.addAllAttributes(RealGridHelper.create("orderCouponInfoGrid", OrderCouponDiscountDto.class));
                return "/order/pop/orderCouponInfoPop";
            case "GIFT" :
                model.addAllAttributes(RealGridHelper.create("orderGiftInfoGrid", OrderGiftInfoDto.class));
                return "/order/pop/orderGiftInfoPop";
            case "SAVE" :
                model.addAllAttributes(RealGridHelper.create("orderSaveInfoGrid", OrderSaveInfoDto.class));
                return "/order/pop/orderSaveInfoPop";
            default:
                return "/core/error/error404";
        }
    }

    @GetMapping("/popup/storeShipMemoPop")
    public String storeShipMemoPop(Model model, @RequestParam("bundleNo") long bundleNo, @RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        model.addAttribute("bundleNo", bundleNo);
        // type : promotion / coupon / gift / save
        return "/order/pop/orderStoreShipMemoPop";
    }

    @GetMapping("/popup/orderCashReceiptPop")
    public String orderCashReceiptPop(Model model, @RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        // type : promotion / coupon / gift / save
        return "/order/pop/orderCashReceiptPop";
    }

    @GetMapping("/popup/orderMultiShipStatusChangePop")
    public String orderMultiShipStatusChangePop(Model model, @RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        model.addAttribute("purchaseOrderNo", purchaseOrderNo);
        return "/order/pop/orderMultiShipStatusChangePop";
    }

    /**
     * 주문관리 - 주문조회
     * @param searchSetDto 주문조회
     * @return OrderSearchGetDto 주문조회 데이터 리스트
     */
    @ResponseBody
    @PostMapping("/getOrderSearch.json")
    public List<OrderSearchGetDto> getOrderSearch(@RequestBody OrderSearchSetDto searchSetDto) {
        return orderManagementService.getOrderMainSearch(searchSetDto);
    }

    /**
     * 주문정보 상세 - 구매자정보
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchProdDto 구매자정보
     */
    @ResponseBody
    @GetMapping("/getOrderDetailProdSearch.json")
    public OrderSearchProdDto getOrderDetailSearchByProd(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailProdSearch(purchaseOrderNo);
    }

    /**
     * 주문정보 상세 - 주문금액정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchPriceDto 주문금액정보
     */
    @ResponseBody
    @GetMapping("/getOrderDetailPriceSearch.json")
    public OrderSearchPriceDto getOrderDetailSearchByPrice(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPriceSearch(purchaseOrderNo);
    }

    /**
     * 주문정보 상세 - 주문정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchDetailDto 주문정보 리스트
     */
    @ResponseBody
    @GetMapping("/getOrderDetailPurchaseSearch.json")
    public List<OrderSearchDetailDto> getOrderDetailSearchByPurchase(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPurchaseSearch(purchaseOrderNo);
    }

    /**
     * 주문정보 상세 - 배송정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchStoreShipDto 배송정보
     */
    @ResponseBody
    @GetMapping("/getOrderDetailShipSearch.json")
    public List<OrderSearchShipDto> getOrderDetailSearchByShipping(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("mallType") String mallType){
        return orderManagementService.getOrderDetailShipSearch(purchaseOrderNo, mallType);
    }

    /**
     * 주문정보 상세 - 클레임정보
     *
     * @param purchaseOrderNo 주문번호
     * @return OrderSearchClaimDto 클레임정보 리스트
     */
    @ResponseBody
    @GetMapping("/getOrderDetailClaimSearch.json")
    public List<OrderSearchClaimDto> getOrderDetailSearchByClaim(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailClaimSearch(purchaseOrderNo);
    }

    @ResponseBody
    @GetMapping("/getOrderDetailPaymentSearch.json")
    public List<OrderSearchPaymentDto> getOrderDetailSearchByPayment(@RequestParam("purchaseOrderNo") long purchaseOrderNo){
        return orderManagementService.getOrderDetailPaymentSearch(purchaseOrderNo);
    }

    /**
     * 주문정보 상세 > 배송정보 > 배송정보 수정
     *
     * @param bundleNo 배송번호
     * @return OrderShipAddrInfoDto 배송정보 리스트
     */
    @ResponseBody
    @GetMapping("/getOrderShipAddrInfo.json")
    public List<OrderShipAddrInfoDto> getOrderShipAddrInfo(@RequestParam("bundleNo") long bundleNo){
        return orderManagementService.getOrderShipAddrInfo(bundleNo);
    }

    /**
     * 주문정보 상세 > 배송정보 > 다중배송정보 수정
     *
     * @param bundleNo 배송번호
     * @return OrderShipAddrInfoDto 배송정보 리스트
     */
    @ResponseBody
    @GetMapping("/getMultiOrderShipAddrInfo.json")
    public List<MultiOrderShipAddrInfoDto> getMultiOrderShipAddrInfo(@RequestParam("bundleNo") long bundleNo){
        return orderManagementService.getMultiOrderShipAddrInfo(bundleNo);
    }

    /**
     * 주문정보 상세 > 배송정보 > 송장등록
     *
     * @param bundleNo 배송번호
     * @return OrderInvoiceRegGetDto 송장등록 가능 데이터 리스트
     * @throws Exception 오류처리
     */
    @ResponseBody
    @GetMapping("/getOrderInvoiceRegInfo.json")
    public List<OrderInvoiceRegGetDto> getOrderInvoiceRegInfo(@RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getTransfer(bundleNo, OrderShipCode.ORDER_INVOICE_REG.getCode(), OrderInvoiceRegGetDto.class);
    }

    /**
     * 주문정보 상세 > 배송정보 > 송장수정
     *
     * @param bundleNo 배송번호
     * @return OrderInvoiceEditGetDto 송장수정 가능 데이터 리스트
     * @throws Exception 오류처리
     */
    @ResponseBody
    @GetMapping("/getOrderInvoiceEditInfo.json")
    public List<OrderInvoiceEditGetDto> getOrderInvoiceEditInfo(@RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getTransfer(bundleNo, OrderShipCode.ORDER_INVOICE_EDIT.getCode(), OrderInvoiceEditGetDto.class);
    }

    /**
     * 주문정보 상세 > 배송정보 > 배송완료
     *
     * @param bundleNo 배송번호
     * @return OrderShipCompleteGetDto 배송완료 가능 리스트
     * @throws Exception 오류처리
     */
    @ResponseBody
    @GetMapping("/getOrderInvoiceCompleteInfo.json")
    public List<OrderShipCompleteGetDto> getOrderInvoiceCompleteInfo(@RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getTransfer(bundleNo, OrderShipCode.ORDER_SHIP_COMPLETE.getCode(), OrderShipCompleteGetDto.class);
    }

    /**
     * 주문정보 상세 > 배송정보 > 주문확인
     *
     * @param bundleNo 배송번호
     * @return 주문확인 처리 성송/실패 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @GetMapping("/orderCheck.json")
    public ResponseObject<String> setOrderCheck(@RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.setOrderProcess(bundleNo, OrderShipCode.ORDER_CHECK.getCode());
    }

    /**
     * 주문정보 상세 > 배송정보 > 송장등록 처리
     *
     * @param invoiceRegDto 송장처리데이터
     * @return 송장등록 처리 성공/실패 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @PostMapping("/invoiceReg.json")
    public ResponseObject<String> setOrderInvoiceRegister(@RequestBody OrderInvoiceRegDto invoiceRegDto) throws Exception {
        return orderManagementService.setOrderInvoiceProcess(invoiceRegDto);
    }

    /**
     * 주문정보 상세 > 배송정보 > 송장수정 처리
     *
     * @param invoiceRegDto 송장처리데이터
     * @return 송장수정 처리 성공/실패 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @PostMapping("/invoiceEdit.json")
    public ResponseObject<String> setOrderInvoiceEdit(@RequestBody OrderInvoiceRegDto invoiceRegDto) throws Exception {
        return orderManagementService.setOrderInvoiceProcess(invoiceRegDto);
    }

    /**
     * 주문정보 상세 > 배송정보 > 배송완료 처리
     *
     * @param orderProcessDto  배송완료 처리 데이터
     * @return 배송완료 처리 성공/실패 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @PostMapping("/shipComplete.json")
    public ResponseObject<String> setOrderShipComplete(@RequestBody OrderProcessDto orderProcessDto) throws Exception {
        return orderManagementService.setOrderCompleteProcess(orderProcessDto);
    }

    /**
     * 주문정보 상세 > 배송정보 > 배송지변경 처리
     *
     * @param shipAddrEditDto 배송지정보
     * @return 배송지 변경 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @PostMapping("/shipAddrChange.json")
    public ResponseObject<String> setOrderAddrChange(@RequestBody OrderShipAddrEditDto shipAddrEditDto) throws Exception {
        return orderManagementService.setOrderShipChange(shipAddrEditDto);
    }

    /**
     * 주문정보 상세 > 배송정보 > 다중배송지변경 처리
     *
     * @param multiOrderShipAddrInfoDto 배송지정보
     * @return 배송지 변경 여부
     * @throws Exception 오류처리
     */
    @ResponseBody
    @PostMapping("/multiShipAddrChange.json")
    public ResponseObject<String> setMultiOrderAddrChange(@RequestBody MultiOrderShipAddrInfoDto multiOrderShipAddrInfoDto) throws Exception {
        return orderManagementService.setMultiOrderAddrChange(multiOrderShipAddrInfoDto);
    }

    @ResponseBody
    @PostMapping("/addNoReceiveReg.json")
    public ResponseObject<String> setNoReceiveReg(@RequestBody NoRcvShipRegGto noRcvShipRegGto) throws Exception {
        return orderManagementService.setNoReceiveReg(noRcvShipRegGto);
    }

    @ResponseBody
    @PostMapping("/getNoReceiveInfo.json")
    public NoRcvShipInfoGetDto noReceiveInfo(@RequestBody NoRcvShipInfoSetDto noRcvShipInfoSetDto) throws Exception {
        return orderManagementService.getNoReceiveInfo(noRcvShipInfoSetDto).getData();
    }

    @ResponseBody
    @PostMapping("/modifyNoReceiveInfo.json")
    public ResponseObject<String> modifyNoReceiveInfo(@RequestBody NoRcvShipEditDto noRcvShipEditDto) throws Exception {
        return orderManagementService.setNoReceiveModify(noRcvShipEditDto);
    }

    @ResponseBody
    @GetMapping("/getShipSlotInfo.json")
    public ResponseObject<FrontStoreSlot> getShipSlotInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getShipSlotInfo(purchaseOrderNo, bundleNo);
    }

    @ResponseBody
    @GetMapping("/modifyShipSlotInfo.json")
    public ResponseObject<String> modifyShipSlotInfo(@ModelAttribute ShipSlotStockChangeDto changeDto) throws Exception {
        return orderManagementService.setShipSlotChangeModify(changeDto);
    }

    @ResponseBody
    @GetMapping("/getOrderDelayShippingInfo.json")
    public OrderDelayShipInfoDto getOrderDelayShippingInfo(@RequestParam("shipNo") long shipNo){
        return orderManagementService.getOrderDelayShippingInfo(shipNo);
    }

    @ResponseBody
    @GetMapping("/getOrderPartnerInfo.json")
    public OrderPartnerInfoDto getOrderPartnerInfo(@RequestParam("partnerId") String partnerId, @RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getOrderPartnerInfo(partnerId, bundleNo);
    }

    @ResponseBody
    @GetMapping("/getOrderPromoInfo.json")
    public List<OrderPromoInfoDto> getOrderPromoInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOderPromoPopInfo(purchaseOrderNo);
    }

    @ResponseBody
    @GetMapping("/getOrderCouponDiscountInfo.json")
    public List<OrderCouponDiscountDto> getOrderCouponDiscountInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOrderCouponDiscountInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderGiftInfo.json")
    public List<OrderGiftInfoDto> getOrderGiftInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOderGiftPopInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderSubstitutionSearch.json")
    public List<OrderSearchSubstitutionDto> getOrderSubstitutionInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOrderSubstitutionInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/orderSaveInfo.json")
    public List<OrderSaveInfoDto> orderSaveInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.orderSaveInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getUserMemo.json")
    public List<OneOnOneInquiryMemberMemoListGetDto> getUserMemo(@RequestParam("userNo") String userNo) throws Exception {
        return orderManagementService.getUserMemo(userNo);
    }
    @ResponseBody
    @GetMapping("/getPresentReSend.json")
    public ResponseObject<Integer> getPresentReSend(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getPresentReSend(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderStoreShipMemoInfo.json")
    public ResponseObject<StoreShipMemoDto> getOrderStoreShipMemoInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo, @RequestParam("bundleNo") long bundleNo) throws Exception {
        return orderManagementService.getOrderStoreShipMemoInfo(purchaseOrderNo, bundleNo);
    }
    @ResponseBody
    @PostMapping("/setOrderStoreShipMemoInfo.json")
    public ResponseObject<String> setOrderStoreShipMemoInfo(@RequestBody StoreShipMemoDto memoDto) throws Exception {
        return orderManagementService.setOrderStoreShipMemoInfo(memoDto);
    }
    @ResponseBody
    @GetMapping("/getOrderStoreShipMsgInfo.json")
    public ResponseObject<StoreShipMsgDto> getOrderStoreShipMsgInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOrderStoreShipMsgInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderHistoryList.json")
    public List<OrderSearchHistoryDto> getOrderHistoryList(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOrderHistoryList(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderCashReceiptInfo.json")
    public ResponseObject<OrderCashReceiptDto> getOrderCashReceiptInfo(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.getOrderCashReceiptInfo(purchaseOrderNo);
    }
    @ResponseBody
    @GetMapping("/getOrderTicketCancelRoleCheck.json")
    public Boolean getOrderTicketCancelRoleCheck() {
        return orderManagementService.getOrderTicketCancelRoleCheck();
    }
    @ResponseBody
    @GetMapping("/multiShipStatusChange.json")
    public ResponseObject<String> setMultiShipStatusChange(@RequestParam("purchaseOrderNo") long purchaseOrderNo) throws Exception {
        return orderManagementService.setMultiShipStatusChange(purchaseOrderNo);
    }
}
