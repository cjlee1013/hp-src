package kr.co.homeplus.admin.web.claim.service;

import java.util.List;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RefundFailListSetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteGetDto;
import kr.co.homeplus.admin.web.claim.model.refundFail.RequestRefundCompleteSetDto;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RefundFailService {

    private final ResourceClient resourceClient;
    private final LoginCookieService loginCookieService;

    /**
     * 환불실패대상 조회
     * **/
    public List<RefundFailListGetDto> getRefundFailList(RefundFailListSetDto refundFailListSetDto) {
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            refundFailListSetDto,
            "/claim/refundFail/getRefundFailList",
            new ParameterizedTypeReference<ResponseObject<List<RefundFailListGetDto>>>() {}
        ).getData();
    }

    /**
     * 환불완료 요청
     * **/
    public ResponseObject<RequestRefundCompleteGetDto> reqRefundComplete (RequestRefundCompleteSetDto requestRefundCompleteSetDto){
        requestRefundCompleteSetDto.setChgId(loginCookieService.getUserInfo().getEmpId());
        return resourceClient.postForResponseObject(
            ResourceRouteName.ESCROWMNG,
            requestRefundCompleteSetDto,
            "/claim/refundFail/reqRefundComplete",
            new ParameterizedTypeReference<ResponseObject<RequestRefundCompleteGetDto>>() {
            }
        );
    }

}
