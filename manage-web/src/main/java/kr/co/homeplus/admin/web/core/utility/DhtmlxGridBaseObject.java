package kr.co.homeplus.admin.web.core.utility;

import java.util.List;

import lombok.Data;

/**
 * DHTMLX Grid 생성
 */
@Data
public class DhtmlxGridBaseObject {
	private String gridName;                                            // Grid 이름
	private String imgPath = "/static/jslib/dhtmlx/codebase/imgs/";     // Image Path
	private List<String> headers;                                       // Grid Header
	private List<String> events;                                        // Add Events
	private List<String> options;                                       // Add Options

	public DhtmlxGridBaseObject(String gridName, List<String> headers) {
		this.gridName   = gridName;
		this.headers    = headers;
	}

	public DhtmlxGridBaseObject(String gridName, List<String> headers, List<String> events) {
		this.gridName   = gridName;
		this.headers    = headers;
		this.events     = events;
	}

	public DhtmlxGridBaseObject(String gridName, List<String> headers, List<String> events, List<String> options) {
		this.gridName   = gridName;
		this.headers    = headers;
		this.events     = events;
		this.options    = options;
	}

	@Override
	public String toString() {
		StringBuffer returnStr = new StringBuffer();

		returnStr.append("var " + this.gridName + ";\n");
		returnStr.append("function " + this.gridName + "Load() { \n");
		returnStr.append(this.gridName + " = new dhtmlXGridObject('" + this.gridName + "');  \n");
		returnStr.append(this.gridName + ".setImagePath('" + this.imgPath + "');  \n");

		// 사용자 정의 헤더 세팅
		for (String hd : this.headers) {
			returnStr.append(this.gridName + "." + hd + " \n");
		}
		// 사용자 정의 이벤트 세팅
		if(this.events != null) {
			for (String ev : this.events) {
				returnStr.append(this.gridName + ".attachEvent(\"" + ev + "\"," + this.gridName + "_" + ev + ");\n");
			}
		}
		// 사용자 정의 옵션 세팅
		if(this.options != null) {
			for (String opt : this.options) {
				returnStr.append(this.gridName + "." + opt + " \n");
			}
		}

		// 공통 이벤트 세팅
		returnStr.append(this.gridName + ".attachEvent('onKeyPress', function(code, ctrl, shift, command) {GridonKeyPressed(code, ctrl, shift, command, " + this.gridName + ")}); \n");
		// 공통 옵션 세팅
		returnStr.append(this.gridName + ".enableEditEvents(false, false); \n");
		returnStr.append(this.gridName + ".enableBlockSelection(true); \n");
		/* 5.x 버전에서 Deprecated
		returnStr.append(this.gridName + ".enableSmartXMLParsing(true); \n");
		*/
		returnStr.append(this.gridName + ".init(); \n");
		returnStr.append("} \n");

		return returnStr.toString();
	}
}
