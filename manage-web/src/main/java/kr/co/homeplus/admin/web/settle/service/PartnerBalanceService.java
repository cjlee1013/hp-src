package kr.co.homeplus.admin.web.settle.service;

import java.util.List;
import kr.co.homeplus.admin.web.core.constants.ResourceRouteName;
import kr.co.homeplus.admin.web.settle.model.partnerBalance.PartnerBalanceListGetDto;
import kr.co.homeplus.admin.web.settle.model.partnerBalance.PartnerBalanceListSetDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PartnerBalanceService {

    private final ResourceClient resourceClient;

    /**
     * TD미배송 전표조회 리스트 조회
     */
    public List<PartnerBalanceListGetDto> getPartnerBalanceList(PartnerBalanceListSetDto partnerBalanceListSetDto) throws Exception{
        return resourceClient.postForResponseObject(
            ResourceRouteName.SETTLE,
            partnerBalanceListSetDto,
            "/admin/partnerBalance/getPartnerBalanceList",
            new ParameterizedTypeReference<ResponseObject<List<PartnerBalanceListGetDto>>>(){}).getData();
    }

}
