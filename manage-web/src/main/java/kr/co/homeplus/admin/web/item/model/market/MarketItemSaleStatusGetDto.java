package kr.co.homeplus.admin.web.item.model.market;

import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridColumnType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridDisplayStyle;
import kr.co.homeplus.plus.api.support.realgrid.RealGridFieldType;
import kr.co.homeplus.plus.api.support.realgrid.RealGridInfo;
import kr.co.homeplus.plus.api.support.realgrid.RealGridOptionInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * 상품 판매정보 Get Entry
 */
@Getter
@Setter
@RealGridInfo
@RealGridOptionInfo(displayStyle = RealGridDisplayStyle.FILL, indicator = true)
public class MarketItemSaleStatusGetDto {

	@RealGridColumnInfo(headText = "점포ID", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private Integer storeId;

	@RealGridColumnInfo(headText = "점포명", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String storeNm;

	@RealGridColumnInfo(headText = "점포유형(클럽/하이퍼)", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String storeAttr;

	@RealGridColumnInfo(headText = "상품번호", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String itemNo;

	@RealGridColumnInfo(headText = "마켓지점코드", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private String marketStoreId;

	@RealGridColumnInfo(headText = "정상가격", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private long originPrice;

	@RealGridColumnInfo(headText = "판매가격", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private long salePrice;

	@RealGridColumnInfo(headText = "simple 프로모션 적용가", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private long simplePrice;

	@RealGridColumnInfo(headText = "즉시할인 적용가(simple 프로모션 진행 시 포함가", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, hidden = true)
	private long couponPrice;

	@RealGridColumnInfo(headText = "점포옵션가격", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private long optionPrice;

	@RealGridColumnInfo(headText = "연동재고수량", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private int stockQty;

	@RealGridColumnInfo(headText = "연동점포별 상태", fieldType = RealGridFieldType.TEXT, columnType = RealGridColumnType.BASIC, sortable = true)
	private String sellStatus;

}