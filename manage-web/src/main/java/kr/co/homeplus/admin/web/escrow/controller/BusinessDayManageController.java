package kr.co.homeplus.admin.web.escrow.controller;

import java.util.List;
import kr.co.homeplus.admin.web.core.certification.LoginCookieService;
import kr.co.homeplus.admin.web.core.constants.EscrowConstants;
import kr.co.homeplus.admin.web.core.exception.HasNotRoleException;
import kr.co.homeplus.admin.web.core.exception.handler.ExceptionCode;
import kr.co.homeplus.admin.web.core.service.AuthorityService;
import kr.co.homeplus.admin.web.escrow.model.BusinessDayManageDto;
import kr.co.homeplus.admin.web.escrow.model.BusinessDayManageSelectDto;
import kr.co.homeplus.admin.web.escrow.model.CalendarDateDto;
import kr.co.homeplus.admin.web.escrow.service.BusinessDayManageService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 시스템관리 > 영업일관리 > 영업일관리
 */
@Controller
@RequestMapping("/escrow/businessDay")
@RequiredArgsConstructor
public class BusinessDayManageController {

    private final LoginCookieService loginCookieService;
    private final AuthorityService authorityService;
    private final BusinessDayManageService businessDayManageService;

    /**
     * 영업일관리 Main Page
     */
    @GetMapping("/businessDayManageMain")
    public String businessDayManageMain() {
        // 권한체크, 사용자 empId 기준으로 화면에 필요한 역할코드 없을시 에러페이지로 이동
//        if (!authorityService.hasRole(EscrowConstants.ROLE_CODE_SYSTEM_DEV)) {
//            throw new HasNotRoleException(ExceptionCode.SYS_ERROR_CODE_9204.getDesc());
//        }
        return "/escrow/businessDayManageMain";
    }

    /**
     * 캘린더 출력용 데이터(일자) 조회
     * @param businessDayManageSelectDto
     * @return
     */
    @ResponseBody
    @GetMapping("/getCalendarDate.json")
    public CalendarDateDto getCalendarDate(@ModelAttribute BusinessDayManageSelectDto businessDayManageSelectDto) {
        return businessDayManageService.getCalendarDataList(businessDayManageSelectDto);
    }

    /**
     * 영업일 리스트 조회
     * @param businessDayManageSelectDto
     * @return
     */
    @ResponseBody
    @GetMapping("/getBusinessDayList.json")
    public List<BusinessDayManageDto> getBusinessDayList(@ModelAttribute BusinessDayManageSelectDto businessDayManageSelectDto) {
        return businessDayManageService.getBusinessDayList(businessDayManageSelectDto);
    }

    /**
     * 영업일 등록
     * @param businessDayManageDto
     * @return
     */
    @ResponseBody
    @PostMapping("/saveBusinessDay.json")
    public ResponseObject<Object> saveBusinessDay(@RequestBody BusinessDayManageDto businessDayManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        businessDayManageDto.setRegId(userCd);

        return businessDayManageService.saveBusinessDay(businessDayManageDto);
    }

    /**
     * 영업일 수정
     * @param businessDayManageDto
     * @return
     */
    @ResponseBody
    @PostMapping("/updateBusinessDay.json")
    public ResponseObject<Object> updateBusinessDay(@RequestBody BusinessDayManageDto businessDayManageDto) throws Exception {
        String userCd = loginCookieService.getUserInfo().getEmpId();
        businessDayManageDto.setChgId(userCd);

        return businessDayManageService.updateBusinessDay(businessDayManageDto);
    }
}
