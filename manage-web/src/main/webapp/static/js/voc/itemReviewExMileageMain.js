/**
 * 회원관리 > 고객문의관리 > 상품평관리
 */

$(document).ready(function() {
    itemReviewExMileageListGrid.init();
    itemReviewExMileage.init();
    CommonAjaxBlockUI.global();
});

// itemReview 그리드
var itemReviewExMileageListGrid = {
    gridView : new RealGridJS.GridView("itemReviewExMileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemReviewExMileageListGrid.initGrid();
        itemReviewExMileageListGrid.initDataProvider();
        itemReviewExMileageListGrid.event();
    },
    initGrid : function() {
        itemReviewExMileageListGrid.gridView.setDataSource(itemReviewExMileageListGrid.dataProvider);
        itemReviewExMileageListGrid.gridView.setStyles(itemReviewExMileageListGridBaseInfo.realgrid.styles);
        itemReviewExMileageListGrid.gridView.setDisplayOptions(itemReviewExMileageListGridBaseInfo.realgrid.displayOptions);
        itemReviewExMileageListGrid.gridView.setColumns(itemReviewExMileageListGridBaseInfo.realgrid.columns);
        itemReviewExMileageListGrid.gridView.setOptions(itemReviewExMileageListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemReviewExMileageListGrid.dataProvider.setFields(itemReviewExMileageListGridBaseInfo.dataProvider.fields);
        itemReviewExMileageListGrid.dataProvider.setOptions(itemReviewExMileageListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemReviewExMileageListGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemReviewExMileage.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        itemReviewExMileageListGrid.dataProvider.clearRows();
        itemReviewExMileageListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(itemReviewExMileageListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        const _date = new Date();
        const fileName = "itemReviewList"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemReviewExMileageListGrid.gridView.exportGrid({
            type : "excel",
            target : "local",
            fileName : fileName + ".xlsx",
            showProgress : true,
            progressMessage : "엑셀 데이터 추줄 중입니다."
        });
    }
};

// itemReview 관리
var itemReviewExMileage = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
        this.initSearchDate('-7d');

        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#setCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#setCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#setCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#setCateCd4'));
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        itemReviewExMileage.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        itemReviewExMileage.setDate.setEndDate(0);
        itemReviewExMileage.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    /**
     * itemReview 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#itemReviewSetForm').ajaxForm({
            url: '/',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                itemReviewExMileage.searchReset();
                itemReviewExMileage.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(itemReviewExMileage.search); //itemReview 검색
        $('#searchResetBtn').bindClick(itemReviewExMileage.searchFormReset); //itemReview 검색폼 초기화
        $('#setBtn').bindClick(itemReviewExMileage.set); //itemReview 등록,수정
        $('#resetBtn').bindClick(itemReviewExMileage.reset); //itemReview 입력폼 초기화
        $('#addItemPopUp').bindClick(itemReviewExMileage.addItemPopUp);
        $('#delItemReviewExMileageBtn').bindClick(itemReviewExMileage.del);

        $('input[name="excKind"]').change(function() {
            const excKind = $("input:radio[name=excKind]:checked").val();

            $('#setCateCd1').val('').change();
            $('#itemNo, #cateKind, #cateCd').val('');

            if(excKind === 'ITEM') {
                $('#setCateCd1, #setCateCd2, #setCateCd3, #setCateCd4').attr('disabled', true);
            } else {
                $('#setCateCd1, #setCateCd2, #setCateCd3, #setCateCd4').attr('disabled', false);
            }
        });

        $("input[name='blockYn']").change(function() {
            const blockYn = $("input:radio[name=blockYn]:checked").val();
            if(blockYn === 'N') {
                $("input[name='blockType']").attr('disabled', true).prop('checked', false);
                $('#blockContents').attr('readonly', true).val('');
            } else {
                $("input[name='blockType']").removeAttr('disabled');
                $("input:radio[name='blockType']:radio[value='A']").prop('checked', true);
                $('#blockContents').removeAttr('readonly');
            }
        });

        $("input[name='blockType']").change(function() {
            const blockType = $("input:radio[name=blockType]:checked").val();
            if(blockType === 'A') {
                $('#blockContents').removeAttr('readonly').val('');
            } else {
                switch (blockType) {
                    case 'B' :
                        $('#blockContents').attr('readonly', true).val('욕설/비방/음란');
                        break;
                    case 'C' :
                        $('#blockContents').attr('readonly', true).val('동일내용반복입력(도배)');
                        break;
                    case 'D' :
                        $('#blockContents').attr('readonly', true).val('개인정보유출');
                        break;
                    case 'E' :
                        $('#blockContents').attr('readonly', true).val('광고/홍보성');
                        break;
                }
            }
        });
    },
    /**
     * itemReview 검색
     */
    search : function() {
        if(!itemReviewExMileage.valid.search()){
            return false;
        }

        const lCate = $('#searchCateCd1 option:selected').val();
        const mCate = $('#searchCateCd2 option:selected').val();
        const sCate = $('#searchCateCd3 option:selected').val();
        const dCate = $('#searchCateCd4 option:selected').val();

        if(lCate != "" && mCate == "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(lCate);
        } else if(lCate != "" && mCate != "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(mCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate == "") {
            $('#searchCateCd').val(sCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate != "") {
            $('#searchCateCd').val(dCate);
        } else {
            $('#searchCateCd').val('');
        }

        CommonAjax.basic({
            url : '/voc/itemReview/getItemReviewExMileageList.json?' + $('#itemReviewSearchForm').serialize(),
            data : null,
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {
                itemReviewExMileage.reset();
                itemReviewExMileageListGrid.setData(res);
                $('#itemReviewTotalCount').html($.jUtil.comma(itemReviewExMileageListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        itemReviewExMileage.initSearchDate('-7d');
        $('#searchCateCd1').val('').change();
        $('#searchCateCd').val('');
        $('#schType').val('itemNo');
        $('#schKeyword').val('');
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function() {
        if($('input[name="excKind"]:checked').val() === 'CATE') {
            alert('구분 값이 상품번호가 아닙니다.');
            return false;
        }

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=itemReviewExMileage.setItemInfo&isMulti=N&mallType=TD&storeType=HYPER', 1084, 650);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        $('#itemNo').val(resData[0].itemNo);
        $('#itemNm').val(resData[0].itemNm);
    },
    /**
     * itemReviewExMileage 등록/수정
     */
    set : function() {
        if(!itemReviewExMileage.valid.set()){
            return false;
        }

        if($('input[name="excKind"]:checked').val() === 'CATE') {
            const lCate = $('#setCateCd1 option:selected').val();
            const mCate = $('#setCateCd2 option:selected').val();
            const sCate = $('#setCateCd3 option:selected').val();
            const dCate = $('#setCateCd4 option:selected').val();

            if(lCate != "" && mCate == "" && sCate == "" && dCate == "") {
                $('#cateKind').val('L');
                $('#cateCd').val(lCate);
            } else if(lCate != "" && mCate != "" && sCate == "" && dCate == "") {
                $('#cateKind').val('M');
                $('#cateCd').val(mCate);
            } else if(lCate != "" && mCate != "" && sCate != "" && dCate == "") {
                $('#cateKind').val('S');
                $('#cateCd').val(sCate);
            } else if(lCate != "" && mCate != "" && sCate != "" && dCate != "") {
                $('#cateKind').val('D');
                $('#cateCd').val(dCate);
            } else {
                $('#cateKind').val('');
                $('#cateCd').val('');
            }
        }

        let setData = $('#itemReviewSetForm').serializeObject();

        CommonAjax.basic({
            url : '/voc/itemReview/setItemReviewExMileage.json',
            data : JSON.stringify(setData),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                itemReviewExMileage.searchFormReset();
                itemReviewExMileage.reset();
                itemReviewExMileage.search();
            }
        });
    },
    /**
     * itemReviewExMileage 삭제
     */
    del : function() {
        let arrSeq = new Array();

        const checkedRows = itemReviewExMileageListGrid.gridView.getCheckedRows(true);
        if (checkedRows.length == 0) {
            alert('삭제할 데이터를 선택해 주세요.');
            return false;
        }

        for (let checkRowId of checkedRows) {
            const checkedRow = itemReviewExMileageListGrid.dataProvider.getJsonRow(checkRowId);
            arrSeq.push(checkedRow.seq);
        }

        CommonAjax.basic({
            url : '/voc/itemReview/delItemReviewExMileage.json',
            data : JSON.stringify(arrSeq),
            method : "POST",
            successMsg : null,
            contentType: 'application/json',
            callbackFunc : function(res) {
                alert(res.returnMsg);
                itemReviewExMileage.reset();
                itemReviewExMileage.search();
            }
        });
    },
    /**
     * itemReview 입력/수정 폼 초기화
     */
    reset : function() {
        $("input:radio[name='excKind']:radio[value='ITEM']").prop('checked', true);
        $('#setCateCd1').val('').change();
        $('#itemNo, #itemNm, #cateKind, #cateCd').val('');
        $('#setCateCd1, #setCateCd2, #setCateCd3, #setCateCd4').attr('disabled', true);
    }
};

/**
 * itemReview Validation Check
 */
itemReviewExMileage.valid = {
    search : function () {
        // 날짜 체크
        const startDt = $("#schStartDt");
        const endDt = $("#schEndDt");
        const searchKeyword = $("#searchKeyword").val();

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        return true;
    },
    set : function () {
        if($("input:radio[name=excKind]:checked").val() === 'ITEM') {
            if(!$('#itemNo').val()) {
                alert("상품번호를 입력해 주세요.");
                $('#itemNo').focus();
                return false;
            }
        }
        return true;
    }
};