/** Main Script */
var oneOnOneInquiryMemberMemoPop = {
    /**
     * init 이벤트
     */
    init: function() {
        oneOnOneInquiryMemberMemoPop.bindingEvent();
        oneOnOneInquiryMemberMemoPop.search();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [저장] 버튼
        $("#addMemoBtn").bindClick(oneOnOneInquiryMemberMemoPop.addMemberMemo);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var reqData = "userNo="+userNo;

        CommonAjax.basic({
            url         : '/voc/oneOnOneInquiry/getInquiryMemberMemoList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                oneOnOneInquiryMemberMemoGrid.setData(res);
            }
        });
    },

    /**
     * 회원메모 등록
     */
    addMemberMemo: function() {
        const $content = $("#content");

        if ($.jUtil.isEmpty($content.val())) {
            $.jUtil.alert('회원메모를 입력해주세요.', 'content');
            return true;
        }

        let data = {};
        data.userNo = userNo;
        data.content = $content.val();

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/user/memberMemo/addMemberMemo.json',
                data        : JSON.stringify(data),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert("저장을 실패했습니다.");
                    } else {
                        alert("저장했습니다.");
                        location.reload();
                    }
                }
            });
        }
    }
};

/** Grid Script */
var oneOnOneInquiryMemberMemoGrid = {
    gridView: new RealGridJS.GridView("oneOnOneInquiryMemberMemoGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        oneOnOneInquiryMemberMemoGrid.initGrid();
        oneOnOneInquiryMemberMemoGrid.initDataProvider();
        oneOnOneInquiryMemberMemoGrid.event();
    },
    initGrid: function () {
        oneOnOneInquiryMemberMemoGrid.gridView.setDataSource(oneOnOneInquiryMemberMemoGrid.dataProvider);
        oneOnOneInquiryMemberMemoGrid.gridView.setStyles(oneOnOneInquiryMemberMemoGridBaseInfo.realgrid.styles);
        oneOnOneInquiryMemberMemoGrid.gridView.setDisplayOptions(oneOnOneInquiryMemberMemoGridBaseInfo.realgrid.displayOptions);
        oneOnOneInquiryMemberMemoGrid.gridView.setColumns(oneOnOneInquiryMemberMemoGridBaseInfo.realgrid.columns);
        oneOnOneInquiryMemberMemoGrid.gridView.setOptions(oneOnOneInquiryMemberMemoGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        oneOnOneInquiryMemberMemoGrid.dataProvider.setFields(oneOnOneInquiryMemberMemoGridBaseInfo.dataProvider.fields);
        oneOnOneInquiryMemberMemoGrid.dataProvider.setOptions(oneOnOneInquiryMemberMemoGridBaseInfo.dataProvider.options);
    },
    event: function() {
    },
    setData: function (dataList) {
        oneOnOneInquiryMemberMemoGrid.dataProvider.clearRows();
        oneOnOneInquiryMemberMemoGrid.dataProvider.setRows(dataList);
    }
};