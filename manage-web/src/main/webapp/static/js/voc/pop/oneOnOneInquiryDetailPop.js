/** Main Script */
var oneOnOneInquiryDetailPop = {
    global: {
        empMemo : "" //메모내용
        , dataMap : {} //상세 데이터
    },

    /**
     * init 이벤트
     */
    init: function() {
        oneOnOneInquiryDetailPop.setStoreOffice();
        oneOnOneInquiryDetailPop.bindingEvent();
        oneOnOneInquiryDetailPop.search();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 구분 SelctBox Change
        $("#schInqryCategory").on('change', function() { oneOnOneInquiryDetailPop.setInqryCategory("CATEGORY") });
        // 문의구분 SelctBox Change
        $("#schInqryType").on('change', function() { oneOnOneInquiryDetailPop.setInqryCategory("TYPE") });

        // [저장] 버튼
        $("#saveBtn").bindClick(oneOnOneInquiryDetailPop.save);
        // [회원메모] 버튼
        $("#memberMemoBtn").bindClick(oneOnOneInquiryDetailPop.openMemberMemo);
        // 답변 입력 글자 카운트
        $("#inqryAnswrCntnt").keyup(function(){ oneOnOneInquiryDetailPop.textCount($(this)) });
        // 재문의 답변 입력 글자 카운트
        $("#reInqryAnswrCntnt").keyup(function(){ oneOnOneInquiryDetailPop.textCount($(this)) });
    },

    /**
     * Store Office 권한 설정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schInqryStatus").prop("disabled",true);
            $("#schInqryCategory").prop("disabled",true);
            $("#schInqryType").prop("disabled",true);
            $("#schInqryDetailType").prop("disabled",true);
            $('#inqryAnswrCntnt').prop("readonly",true);
            $('#reInqryAnswrCntnt').prop("readonly",true);
        }
    },

    // 기능 전부 막기
    setDisabledAll: function () {
        $("#schInqryStatus").prop("disabled", true);
        $('#inqryAnswrCntnt').prop("readonly", true);
        $('#reInqryAnswrCntnt').prop("readonly", true);

        $("#schInqryCategory").prop("disabled", true);
        $("#schInqryType").prop("disabled", true);
        $("#schInqryDetailType").prop("disabled", true);
        $('#empMemo').prop("readonly", true);
        $('.center-button-group').hide();
    },

    // 문의유형 SelctBox Change
    setInqryCategory : function(type) {
        var $inqryCategory = $('#schInqryCategory');
        var $inqryType = $('#schInqryType');

        if(type == "CATEGORY") {
            $("#schInqryType option:eq(0)").prop("selected", true);
            $("#schInqryDetailType option:eq(0)").prop("selected", true);

            $('#schInqryType option').each(function () {
                var $this = $(this);
                if ($.jUtil.isEmpty($this.val())) {
                    return;
                }

                if ($.jUtil.isEmpty($inqryCategory.val())) {
                    $this.hide();
                } else {
                    $this.show();
                }
            });
        }

        if(type == "CATEGORY" || type == "TYPE") {
            $("#schInqryDetailType option:eq(0)").prop("selected", true);

            $('#schInqryDetailType option').each(function () {
                var $this = $(this);
                if ($.jUtil.isEmpty($this.val())) {
                    return;
                }

                if ($.jUtil.isEmpty($inqryType.val())) {
                    $this.hide();
                    return;
                }

                var ref1 = $this.attr("data-ref1");
                var ref2 = $this.attr("data-ref2");
                if (ref1 == $inqryCategory.val() && ref2 == $inqryType.val()) {
                    $this.show();
                } else {
                    $this.hide();
                }
            });

        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var reqData = new Object();

        if (!$.jUtil.isAllowInput(inqryNo,['NUM'])
            || !$.jUtil.isAllowInput(inqryKind,['ENG_UPPER'])) {
            alert("잘못된 파라미터입니다.");
            oneOnOneInquiryDetailPop.setDisabledAll();
            return;
        }

        reqData.inqryNo = inqryNo;

        CommonAjax.basic({
            url:'/voc/oneOnOneInquiry/getOneOnOneInquiryDetail.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                var dataMap = res;

                oneOnOneInquiryDetailPop.global.dataMap = dataMap;

                $("#schInqryStatus").val(dataMap.inqryStatus);
                $('#schInqryCategory').val(dataMap.inqryCategory);
                oneOnOneInquiryDetailPop.setInqryCategory("CATEGORY");
                $('#schInqryType').val(dataMap.inqryType);
                oneOnOneInquiryDetailPop.setInqryCategory("TYPE");
                $('#schInqryDetailType').val(dataMap.inqryDetailType);

                if(!$.jUtil.isEmpty(dataMap.storeType)) {
                    $('#storeId').text("[" + dataMap.storeId + "] " + dataMap.storeNm);
                }
                $('#site').text(dataMap.siteTypeNm);
                if($.jUtil.isEmpty(dataMap.userNm)) {
                    $('#userInfo').text(dataMap.userNo + "(탈퇴)");
                } else {
                    $('#userInfo').text("[" + dataMap.userNo + "] "+dataMap.userNm);
                }
                $('#purchaseOrderNo').text(dataMap.purchaseOrderNo);
                $('#itemNo').text(dataMap.itemNo);
                $('#inqryDt').text(dataMap.inqryDt);
                $('#inqryTitle').text(dataMap.inqryTitle);
                $('#inqryAnswrCntnt').text(dataMap.inqryAnswrCntnt);
                $('#inqryCntnt').text(dataMap.inqryCntnt);

                $('#inqryAnswrDt').text(dataMap.inqryAnswrDt);
                $('#inqryAnswrRegNm').text(dataMap.inqryAnswrRegNm);
                $('#reInqryCntnt').text(dataMap.reInqryCntnt);
                $('#reInqryAnswrCntnt').text(dataMap.reInqryAnswrCntnt);
                $('#reInqryAnswrDt').text(dataMap.reInqryAnswrDt);
                $('#reInqryAnswrRegNm').text(dataMap.reInqryAnswrRegNm);
                $('#empMemo').text(dataMap.empMemo);
                oneOnOneInquiryDetailPop.global.empMemo = dataMap.empMemo;
                $('#empMemoDt').text(dataMap.empMemoDt);
                $('#empMemoNm').text(dataMap.empMemoNm);

                // SMS 발송여부 + 발송시간
                if (dataMap.inqryKind == 'N') {
                    if ($.jUtil.isEmpty(dataMap.smsDt)) {
                        $('#smsAlamYn').text(dataMap.smsAlamYn);
                    } else {
                        $('#smsAlamYn').text(dataMap.smsAlamYn + " (" + dataMap.smsDt + ")");
                    }
                } else {
                    if ($.jUtil.isEmpty(dataMap.reSmsDt)) {
                        $('#smsAlamYn').text(dataMap.smsAlamYn);
                    } else {
                        $('#smsAlamYn').text(dataMap.smsAlamYn + " (" + dataMap.reSmsDt + ")");
                    }
                }

                // 첨부파일
                if (dataMap.inqryImgYn == "Y") {
                    if(!$.jUtil.isEmpty(dataMap.inqryImg1)) {
                        $('#inqryImg').append('<span style="color:blue; text-decoration: underline; cursor: pointer; padding-right:10px;"'
                            + 'onclick="oneOnOneInquiryDetailPop.imagePreview(\'' + dataMap.inqryImg1 +'\')">첨부파일1</span>');
                    }

                    if(!$.jUtil.isEmpty(dataMap.inqryImg2)) {
                        $('#inqryImg').append('<span style="color:blue; text-decoration: underline; cursor: pointer; padding-right:10px;"'
                            + 'onclick="oneOnOneInquiryDetailPop.imagePreview(\'' + dataMap.inqryImg2 +'\')">첨부파일2</span>');
                    }

                    if(!$.jUtil.isEmpty(dataMap.inqryImg3)) {
                        $('#inqryImg').append('<span style="color:blue; text-decoration: underline; cursor: pointer; padding-right:10px;"'
                            + 'onclick="oneOnOneInquiryDetailPop.imagePreview(\'' + dataMap.inqryImg3 +'\')">첨부파일3</span>');
                    }
                }

                // 신규문의 , 재문의, 완료, 취소 상태에 따라 event 적용
                if (dataMap.inqryKind == 'N') {
                    if (dataMap.inqryStatus == "I3") {
                        $("#schInqryStatus").prop("disabled",true);
                        $('#inqryAnswrCntnt').prop("readonly",true);
                    } else if(dataMap.inqryStatus == "I4") {
                        $("#schInqryStatus option:eq(3)").show();
                        $("#schInqryStatus").prop("disabled",true);
                        $('#inqryAnswrCntnt').prop("readonly",true);
                    }
                } else {
                    $('#inqryAnswrCntnt').prop("readonly",true);
                    if(dataMap.inqryStatus == "I3") {
                        $('#reInqryAnswrCntnt').prop("readonly",true);
                        $("#schInqryStatus").prop("disabled",true);
                    } else if (dataMap.inqryStatus == "I4") {
                        $('#reInqryAnswrCntnt').prop("readonly",true);
                        $("#schInqryStatus").prop("disabled",true);
                        $("#schInqryStatus option:eq(3)").show();
                    }
                }

                // 탈퇴회원인 경우 기능 전부 막음
                if ($.jUtil.isEmpty(dataMap.userNm)) {
                    oneOnOneInquiryDetailPop.setDisabledAll();
                }
            }
        });
    },

    /**
     * 답변 등록
     */
    save: function () {
        var reqData = new Object();
        var dataMap = oneOnOneInquiryDetailPop.global.dataMap;
        var inqryStatus = $('#schInqryStatus').val();
        var inqryCategory = $('#schInqryCategory').val();
        var inqryType = $('#schInqryType').val();
        var inqryDetailType = $('#schInqryDetailType').val();
        var inqryAnswrCntnt = $('#inqryAnswrCntnt').val();
        var reInqryAnswrCntnt = $('#reInqryAnswrCntnt').val();
        var empMemo = $('#empMemo').val();

        if ($.jUtil.isEmpty(inqryCategory)
                || $.jUtil.isEmpty(inqryType)
                || $.jUtil.isEmpty(inqryDetailType)) {
            alert("문의유형을 선택해 주세요.");
            return;
        }

        if (inqryStatus == "I3") {
            if (inqryKind == "N") {
                if ($.jUtil.isEmpty(inqryAnswrCntnt)) {
                    alert("답변 내용을 입력해 주세요.");
                    return;
                }
            } else {
                if ($.jUtil.isEmpty(reInqryAnswrCntnt)) {
                    alert("재문의 답변 내용을 입력해 주세요.");
                    return;
                }
            }
        }

        reqData.inqryNo = inqryNo;
        reqData.inqryStatus = inqryStatus;
        reqData.inqryCategory = inqryCategory;
        reqData.inqryType = inqryType;
        reqData.inqryDetailType = inqryDetailType;
        reqData.inqryAnswrCntnt = inqryAnswrCntnt;
        reqData.reInqryAnswrCntnt = reInqryAnswrCntnt;
        reqData.empMemo = empMemo;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/voc/oneOnOneInquiry/setInquiryAnswer.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                    } else {
                        alert("저장했습니다.");
                        eval("opener." + callBackScript + "(res);");
                        location.reload();
                    }
                }
            });
        }
    },

    /**
     * 회원메모 팝업 호출
     */
    openMemberMemo : function () {
        var userNo = oneOnOneInquiryDetailPop.global.dataMap.userNo;
        windowPopupOpen("/voc/popup/oneOnOneInquiryMemberMemoPop?userNo=" + userNo, "oneOnOneInquiryMemberMemoPop", 860, 720, 0,0);
    },

    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function (imgUrl) {
        $.jUtil.imgPreviewPopup(hmpImgUrl + "/provide/view?processKey=&fileId=" + imgUrl);
    },

    /**
     *  글자수 카운트
     */
    textCount: function($this) {
        var content = $this.val();
        var count = content.length;

        $this.next().find('i').text(count);
    }
};
