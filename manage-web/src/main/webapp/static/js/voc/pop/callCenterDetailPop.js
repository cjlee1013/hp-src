/** Main Script */
var callCenterDetailPop = {
    global: {
        dataMap : {}
    },

    /**
     * init 이벤트
     */
    init: function() {
        callCenterDetailPop.bindingEvent();
        callCenterDetailPop.search();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [문의추가] 버튼
        $("#addContentBtn").bindClick(callCenterDetailPop.addContent,"");

        // [변경] 버튼
        $("#chkWay").bindClick(callCenterDetailPop.setWayChk);

        // 처리방법 SelctBox Change
        $("#hBranchCd").on('change', function() {callCenterDetailPop.setSelectWay()});

        // 처리사유 SelctBox Change
        $("#complet_reason").on('change', function() {callCenterDetailPop.setSelectReason()});

        // [회원메모] 버튼
        $("#memberMemoBtn").bindClick(callCenterDetailPop.openMemberMemo);

        // [문의추가 저장] 버튼
        $("#saveAdd").bindClick(callCenterDetailPop.saveAdd);

        // [처리완료 저장] 버튼
        $("#saveComplete").bindClick(callCenterDetailPop.saveComplete);

        //파일선택시
        $('input[name="fileArr"]').change(function() {callCenterDetailPop.file.addFile();});

        //파일삭제 이벤트
        $('[id^=fileList]').on('click', '.close', function() {
            callCenterDetailPop.file.deleteFile($(this));
        });

        // 주문번호 링크 클릭
        $("#linkOrderNo").bindClick(callCenterDetailPop.linkOrderNo);
    },

    /**
     * 문의추가 영역 확장
     */
    addContent : function(data) {
        var start_time = "";
        var account_nm = "";
        var rmks = "";
        var rmksSelector = "";
        var readonly = "";

        if(!$.jUtil.isEmpty(data)) {
            start_time = data.reg_dt;
            account_nm = data.account_nm;
            rmks = data.rmks;
            readonly = "readonly";
        } else {
            $("#addContentBtn").prop("disabled",true);
            $("#lblWay").show();
            rmksSelector = 'rmksAdd';
        }

        $('#divInquiryContent').append(
            '<div class="ui wrap-table horizontal mg-t-10">'
            +'<table class="ui table">'
            +'  <colgroup>'
            +'      <col width="10%">'
            +'      <col width="40%">'
            +'      <col width="10%">'
            +'      <col width="40%">'
            +'</colgroup>'
            +'<tbody>'
            +'  <tr>'
            +'      <th>접수일</th>'
            +'      <td>'
            +'          <span class="text">'+start_time+'</span>'
            +'      </td>'
            +'      <th>접수자</th>'
            +'      <td>'
            +'          <span class="text">'+account_nm+'</span>'
            +'      </td>'
            +'  </tr>'
            +'  <tr>'
            +'      <th>접수내용</th>'
            +'          <td colspan="3">'
            +'          <textarea id="'+rmksSelector+'" class="ui input mg-t-5" style="width: 100%; height: 120px;" maxlength="1000" '+readonly+'>'+rmks+'</textarea>'
            +'      </td>'
            +'  </tr>'
            +'</tbody>'
            +'</table>'
            +'</div>'
        );
    },

    /**
     * 처리방법 변경 체크박스 이벤트
     */
    setWayChk : function() {
        var $chkWay = $('#chkWay');

        if($chkWay.is(":checked")) {
            $("#hBranchCd").show();
            $("#advisorIngType").show();

            $("#completWay").hide();

            $("#hBranchCd option:eq(0)").prop("selected", true);
            $("#advisorIngType option:eq(0)").prop("selected", true);
        } else {
            $("#hBranchCd").hide();
            $("#advisorIngType").hide();

            $("#completWay").show();
        }

    },

    /**
     * 상담항목 SelctBox Change
     */
    setSelectWay : function() {
        var $hBranchCd = $('#hBranchCd');
        var $hBranchCdVal = $hBranchCd.val();

        $('#advisorIngType option').each(function () {
            var $this = $(this);
            if ($.jUtil.isEmpty($this.val())) {
                return;
            }

            if($hBranchCdVal == 'H_BRANCH_1') {
                if($this.attr("gmcCd1") == $hBranchCdVal) {
                    $this.show();
                } else {
                    $this.hide();
                }
            } else if($hBranchCdVal == 'H_BRANCH_2') {
                if($this.attr("gmcCd2") == $hBranchCdVal) {
                    $this.show();
                } else {
                    $this.hide();
                }
            } else {
                $this.hide();
            }
        });

        $("#advisorIngType option:eq(0)").prop("selected", true);
    },

    /**
     * 처리사유 SelctBox Change
     */
    setSelectReason : function() {
        var $completReason = $('#complet_reason');
        var $completReasonVal = $completReason.val();

        $('#complet_detail option').each(function () {
            var $this = $(this);
            if ($.jUtil.isEmpty($this.val())) {
                return;
            }

            if($this.attr("gmcCd") == $completReasonVal) {
                $this.show();
            } else {
                $this.hide();
            }
        });

        $("#complet_detail option:eq(0)").prop("selected", true);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var reqData = new Object();

        if (!$.jUtil.isAllowInput(master_seq,['NUM'])
            || !$.jUtil.isAllowInput(channel_type,['ENG_UPPER','SPC_2'])) {
            alert("잘못된 파라미터입니다.");
            $("#saveAdd").hide();
            $("#saveComplete").hide();
            return;
        }

        reqData.MASTER_SEQ = master_seq;
        reqData.CHANNEL_TYPE = channel_type;

        CommonAjax.basic({
            url:'/voc/callCenter/getCallCenterDetail.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                mainData = res[0];

                callCenterDetailPop.global.dataMap = mainData;

                // 콜센터문의상세

                //문의번호
                $('#KEY').text(mainData.KEY);

                //처리상태
                $('#advisor_ing_nm').text(mainData.advisor_ing_nm);

                //상담유형
                var advisor_type = mainData.channel_nm;
                if (!$.jUtil.isEmpty(mainData.h_channel_nm)) {
                    advisor_type += " > " + mainData.h_channel_nm;
                }
                if (!$.jUtil.isEmpty(mainData.advisor_type_nm1)) {
                    advisor_type += " > " + mainData.advisor_type_nm1;
                }
                if (!$.jUtil.isEmpty(mainData.advisor_type_nm2)) {
                    advisor_type += " > " + mainData.advisor_type_nm2;
                }

                $('#advisor_type').text(advisor_type);

                // 판매업체
                if (!$.jUtil.isEmpty(mainData.store_id)) {
                    $('#store_name').text(
                        "[" + mainData.store_id + "]" + mainData.store_nm);
                }

                // 문의자
                if (!$.jUtil.isEmpty(mainData.cust_id) && mainData.cust_id != '비식별') {
                    $('#cust_name').text("[" + mainData.cust_id + "]" + mainData.cust_nm);
                    $('#memberMemoBtn').show();
                }

                // 주문번호
                $('#h_order_no').text(mainData.h_order_no);

                // 상담구분
                $('#voc_yn_nm').text(mainData.voc_yn_nm);

                // 상담주문상품
                $('#h_item_no').text(mainData.h_item_no);

                // 마켓연동
                $('#market_type_nm').text(mainData.market_type_nm);

                // 처리방법
                var completWay = mainData.h_branch_nm;
                if (!$.jUtil.isEmpty(mainData.advisor_ing_nm)) {
                    completWay += " > " + mainData.advisor_ing_nm;
                }
                if (!$.jUtil.isEmpty(mainData.trans_advisor_nm)) {
                    completWay += " > " + mainData.trans_advisor_nm;
                }

                $('#completWay').text(completWay);

                // 문의내용

                // 접수일
                $('#start_time').text(mainData.reg_dt);

                // 접수자
                $('#account_nm').text(mainData.account_nm);

                // 제목
                var grade = "";
                if (!$.jUtil.isEmpty(mainData.h_important_grade)) {
                    switch (mainData.h_important_grade) {
                        case "H_IMPORTANT_GRADE_1" : grade = "\★ | "; break;
                        case "H_IMPORTANT_GRADE_2" : grade = "\★★ | "; break;
                        case "H_IMPORTANT_GRADE_3" : grade = "\★★★ | "; break;
                    }
                }

                $('#history_title').text(grade+mainData.history_title);

                // 접수내용
                $('#rmks').text(mainData.rmks);

                if(res.length > 1) {
                    for(var i=1; i<res.length; i++){
                        callCenterDetailPop.addContent(res[i]);
                    }
                }

                // 처리내용

                // 처리사유
                var comepleteReason = mainData.complet_reason_nm;
                if (!$.jUtil.isEmpty(mainData.complet_detail_nm)) {
                    comepleteReason += ">" + mainData.complet_detail_nm;
                }

                $('#txt_complet_reason').text(comepleteReason);

                $('#complet_reason').val(mainData.complet_reason);
                $('#complet_detail').val(mainData.complet_detail);

                // 발생자
                $('#issue_generator').val(mainData.issue_generator);

                // 발생원인
                $('#issue_reason').val(mainData.issue_reason);

                // 처리내용
                $('#complet_txt').text(mainData.complet_txt);

                // 처리완료일시
                $('#complet_dt').text(mainData.complet_dt);

                // 처리자
                $('#complet_account_nm').text(mainData.complet_account_nm);

                // 첨부파일 리스트
                for(var idx in mainData.fileList) {
                    if(!$.jUtil.isEmpty(mainData.fileList[idx].fileId)){
                        callCenterDetailPop.file.appendFile(mainData.fileList[idx]);
                    }
                }

                // 처리완료시 수정불가
                if (!$.jUtil.isEmpty(mainData.complet_dt)) {
                    $('#txt_complet_reason').show();
                    $('#issue_generator').attr("readonly", true);
                    $('#complet_txt').attr("readonly", true);
                    $('#issue_reason').attr("readonly", true);
                    $("#saveComplete").prop("disabled",true);
                } else {
                    $('#complet_reason').show();
                    $('#complet_detail').show();
                }
            }
        });
    },

    /**
     * 문의추가 저장
     */
    saveAdd: function () {
        var reqData = new Object();
        var data = callCenterDetailPop.global.dataMap;

        var hBranch = data.h_branch;
        var advisorIngType = data.advisor_ing_type;
        var rmks = $('#rmksAdd').val();

        if($.jUtil.isEmpty($('#rmksAdd').attr("id"))) {
            alert("문의추가를 해주세요");
            return;
        }

        if($.jUtil.isEmpty(rmks)) {
            alert("접수내용을 입력해 주세요");
            return;
        }

        if($('#chkWay').is(":checked")) {
            if ($.jUtil.isEmpty($('#hBranchCd').val()) || $.jUtil.isEmpty($('#advisorIngType').val())) {
                alert("처리방법을 선택해 주세요.");
                return;
            }

            hBranch = $('#hBranchCd').val();
            advisorIngType = $('#advisorIngType').val();
        }

        reqData.par_history_seq = data.par_history_seq;
        reqData.h_branch = hBranch;
        reqData.advisor_ing_type = advisorIngType;
        reqData.channel_type = data.channel_type;
        reqData.rmks = rmks;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/voc/callCenter/setAddCounsel.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert("저장을 실패했습니다.");
                    } else {
                        alert("저장했습니다.");
                        location.reload();
                    }
                }
            });
        }
    },

    /**
     * 처리완료 저장
     */
    saveComplete: function () {
        var reqData = new Object();
        var data = callCenterDetailPop.global.dataMap;

        var completReason = $('#complet_reason').val();
        var completDetail = $('#complet_detail').val();
        var issueGenerator = $('#issue_generator').val();
        var issueReason = $('#issue_reason').val();
        var completTxt = $('#complet_txt').val();

        if ($.jUtil.isEmpty(data.complet_dt)) {
            if ($.jUtil.isEmpty(completReason)) {
                alert("처리사유를 선택해주세요");
                return;
            }

            if ($.jUtil.isEmpty(completDetail)) {
                alert("처리상세를 선택해주세요");
                return;
            }

            if ($.jUtil.isEmpty(completTxt)) {
                alert("처리내용을 입력해주세요");
                return;
            }

            reqData.par_history_seq = data.par_history_seq;
            reqData.complet_reason = completReason;
            reqData.complet_detail = completDetail;
            reqData.issue_generator = issueGenerator;
            reqData.issue_reason = issueReason;
            reqData.complet_txt = completTxt;

            if (confirm("저장하시겠습니까?")) {
                CommonAjax.basic({
                    url: '/voc/callCenter/setAddComplete.json',
                    data: JSON.stringify(reqData),
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    callbackFunc: function (res) {
                        if (res.returnCode != "SUCCESS") {
                            alert("저장을 실패했습니다.");
                        } else {
                            alert("저장했습니다.");
                            eval("opener." + callBackScript + "(res);");
                            location.reload();
                        }
                    }
                });
            }
        }
    },

    /**
     * 이미지 정보 저장
     */
    saveFileInfo: function () {
        var reqData = $('#callCenterFileForm').serializeObject(false);
        var data = callCenterDetailPop.global.dataMap;

        reqData.par_history_seq = data.par_history_seq;

        CommonAjax.basic({
            url         : '/voc/callCenter/setAddCompleteImg.json',
            data        : JSON.stringify(reqData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "SUCCESS") {
                    alert("이미지 저장을 실패했습니다.");
                    location.reload();
                }
            }
        });
    },

    /**
     * 회원메모 팝업 호출
     */
    openMemberMemo : function () {
        var userNo = callCenterDetailPop.global.dataMap.cust_id;

        windowPopupOpen("/voc/popup/oneOnOneInquiryMemberMemoPop?type=ADD&userNo=" + userNo, "callCenterMemberMemoPop", 860, 720, 0,0);
    },

    /**
     * 주문번호 상세 호출
     */
    linkOrderNo : function () {
        var orderNo = $('#h_order_no').text();
        eval("opener." + callbackLinkScript + "(orderNo);");
    }
};

/**
 * 첨부파일
 */
callCenterDetailPop.file = {
    /**
     * 파일 업로드 클릭 이벤트
     * @param obj
     */
    clickFile : function(obj) {
        var fileCnt = $("#fileList > li").length;
        if(fileCnt<3){
            $('input[name=fileArr]').click();
        } else{
            alert("파일은 최대 3개까지 첨부 가능합니다.");
            return ;
        }
    },

    /**
     * 파일 삭제 클릭 이벤트
     * @param obj
     */
    deleteFile : function(obj) {
        if(confirm("파일을 삭제하시겠습니까?")) {
            obj.closest('.file').remove();
            callCenterDetailPop.saveFileInfo();
        }
    },
    /**
     * 파일업로드
     */
    addFile : function() {
        if (!callCenterDetailPop.file.valid()) {
            return;
        }

        var file = $('#itemFile');
        var params = {
            processKey : "CallCenterImage",
            mode : "IMG"
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {

                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        callCenterDetailPop.file.appendFile(f.fileStoreInfo);
                        callCenterDetailPop.saveFileInfo();
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 파일 업로드 유효성 검사
     * @returns {boolean}
     */
    valid : function() {
        var $itemFile = $('#itemFile');
        var fileTemp = /(.*?)\.(jpg|png)$/;

        if ($itemFile.get(0).files[0].size > 5242880) {
            alert("5MB 이하로 업로드해주세요.");
            return false;
        }

        if (!$itemFile.get(0).files[0].name.match(fileTemp)) {
            alert('첨부 가능한 파일형식이 아닙니다.')
            return false;
        }

        return true;
    },
    appendFile : function(f){

        var html =  '<li class ="file" style="float: left"><span class="ui span-button text mg-r-10">'
            + '<input type="hidden" class="fileUrl" name="fileList[].fileId" value="'+f.fileId+'"/>'
            + '<input type="hidden" class="fileNm" name="fileList[].fileName" value="'+f.fileName+'"/>'
            + '<a href="'
            + hmpImgUrl + "/provide/view?processKey=&fileId=" + f.fileId
            + '" target="_blank" style="color:#ff685b; text-decoration: underline; ">'
            + f.fileName
            + '</a><span class="text close mg-l-5">[X]</span></span></li>';

        $("#fileList").append(html);
    }

};