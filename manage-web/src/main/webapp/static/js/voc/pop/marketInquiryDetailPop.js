/** Main Script */
var marketInquiryDetailPop = {
    /**
     * init 이벤트
     */
    init: function() {
        marketInquiryDetailPop.bindingEvent();
        marketInquiryDetailPop.search();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [저장] 버튼
        $("#saveBtn").bindClick(marketInquiryDetailPop.save);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var reqData = new Object();

        reqData.messageNo = messageNo;
        reqData.marketType = marketType;
        reqData.messageType = messageType;

        CommonAjax.basic({
            url: '/voc/marketInquiry/getMarketInquiryDetail.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                $('#messageNo').text(res.messageNo);
                $('#storeNm').text(res.storeNm);
                $('#marketNm').text(res.marketNm);
                $('#isresponseStatus').text(res.isresponseStatus);
                $('#contactNm').text(res.contactNm);
                $('#ordNo').text(res.ordNo);
                $('#goodId').text(res.goodId);
                $('#copGoodId').text(res.copGoodId);
                $('#receiveDate').text(res.receiveDate);
                if($.jUtil.isEmpty(res.inquirerPhone)) {
                    $('#inquirerName').text(res.inquirerName);
                } else {
                    $('#inquirerName').text(res.inquirerName + " (연락처 : " + res.inquirerPhone + ")");
                }
                $('#requestTitle').text(res.requestTitle);
                $('#requestComments').val(res.requestComments);
                $('#isresponseNm').text(res.isresponseNm);
                $('#isresponseDate').text(res.isresponseDate);

                if(marketType == 'NAVER') {
                    $('#responseTitle').val("고객님 문의에 답변드립니다.").prop("readonly",true);
                } else {
                    $('#responseTitle').val(res.responseTitle);
                }

                $('#responseContents').val(res.responseContents);
            }
        });
    },

    /**
     * 답변 등록
     */
    save: function () {
        var reqData = new Object();

        reqData.messageNo = messageNo;
        reqData.messageType = messageType;
        reqData.responseTitle = $('#responseTitle').val();
        reqData.responseContents = $('#responseContents').val();

        CommonAjax.basic({
            url: '/voc/marketInquiry/setNaverInquiryAnswer.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnCode != "SUCCESS") {
                    alert("저장을 실패했습니다.");
                } else {
                    alert("저장했습니다.");
                    eval("opener." + callBackScript + "(res);");
                    location.reload();
                }
            }
        });
    }
};
