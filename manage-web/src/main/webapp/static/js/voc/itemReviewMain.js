/**
 * 회원관리 > 고객문의관리 > 상품평관리
 */

$(document).ready(function() {
    itemReviewListGrid.init();
    itemReview.init();
    CommonAjaxBlockUI.global();
});

// itemReview 그리드
var itemReviewListGrid = {
    gridView : new RealGridJS.GridView("itemReviewListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemReviewListGrid.initGrid();
        itemReviewListGrid.initDataProvider();
        itemReviewListGrid.event();
    },
    initGrid : function() {
        itemReviewListGrid.gridView.setDataSource(itemReviewListGrid.dataProvider);
        itemReviewListGrid.gridView.setStyles(itemReviewListGridBaseInfo.realgrid.styles);
        itemReviewListGrid.gridView.setDisplayOptions(itemReviewListGridBaseInfo.realgrid.displayOptions);
        itemReviewListGrid.gridView.setColumns(itemReviewListGridBaseInfo.realgrid.columns);
        itemReviewListGrid.gridView.setOptions(itemReviewListGridBaseInfo.realgrid.options);
        orderUtil.setRealGridColumnLink(itemReviewListGrid, 'itemNo', 'unnecessary', false);
        orderUtil.setRealGridColumnLink(itemReviewListGrid, 'purchaseOrderNo', 'unnecessary', false);
    },
    initDataProvider : function() {
        itemReviewListGrid.dataProvider.setFields(itemReviewListGridBaseInfo.dataProvider.fields);
        itemReviewListGrid.dataProvider.setOptions(itemReviewListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemReviewListGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemReview.gridRowSelect(index.dataRow, index);
        };
    },
    setData : function(dataList) {
        itemReviewListGrid.dataProvider.clearRows();
        itemReviewListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(itemReviewListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        const _date = new Date();
        const fileName = "itemReviewList"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemReviewListGrid.gridView.exportGrid({
            type : "excel",
            target : "local",
            fileName : fileName + ".xlsx",
            showProgress : true,
            progressMessage : "엑셀 데이터 추줄 중입니다."
        });
    }
};

// itemReview 관리
var itemReview = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
        this.initSearchDate('-1d');
        this.initCategorySelectBox();
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        itemReview.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        itemReview.setDate.setEndDate(0);
        itemReview.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },

    /**
     * itemReview 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#itemReviewSetForm').ajaxForm({
            url: '/',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                itemReview.searchReset();
                itemReview.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(itemReview.search); //itemReview 검색
        $('#searchResetBtn').bindClick(itemReview.searchReset); //itemReview 검색폼 초기화
        $('#setBtn').bindClick(itemReview.set); //itemReview 등록,수정
        $('#resetBtn').bindClick(itemReview.reset); //itemReview 입력폼 초기화
        $('#excelDownloadBtn').bindClick(itemReview.excelDownload);

        $("input[name='blockYn']").change(function() {
            const blockYn = $("input:radio[name=blockYn]:checked").val();
            if(blockYn === 'N') {
                $("input[name='blockType']").attr('disabled', true).prop('checked', false);
                $('#blockContents').attr('readonly', true).val('');
            } else {
                $("input[name='blockType']").removeAttr('disabled');
                $("input:radio[name='blockType']:radio[value='A']").prop('checked', true);
                $('#blockContents').removeAttr('readonly');
            }
        });

        $("input[name='blockType']").change(function() {
            const blockType = $("input:radio[name=blockType]:checked").val();
            if(blockType === 'A') {
                $('#blockContents').removeAttr('readonly').val('');
            } else {
                switch (blockType) {
                    case 'B' :
                        $('#blockContents').attr('readonly', true).val('욕설/비방/음란');
                        break;
                    case 'C' :
                        $('#blockContents').attr('readonly', true).val('동일내용반복입력(도배)');
                        break;
                    case 'D' :
                        $('#blockContents').attr('readonly', true).val('개인정보유출');
                        break;
                    case 'E' :
                        $('#blockContents').attr('readonly', true).val('광고/홍보성');
                        break;
                }
            }
        });

        // 변경 이력조회 버튼 클릭
        $(document).on('click', '#itemReviewHistBtn', function() {
            if (!itemReview.popupValid()) {
                return false;
            }
            itemReview.popup();
        });
    },
    /**
     * itemReview 검색
     */
    search : function() {
        if(!itemReview.valid.search()){
            return false;
        }
        CommonAjax.basic({
            url : '/voc/itemReview/getItemReviewList.json?' + $('#itemReviewSearchForm').serialize(),
            data : null,
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {
                itemReview.reset();
                itemReviewListGrid.setData(res);
                $('#itemReviewTotalCount').html($.jUtil.comma(itemReviewListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchReset : function() {
        itemReview.initSearchDate('-1d');
        $('#schKeyword, #schStoreType, #schReserveYn, #schBlockYn, #schGrade').val('');
        $('#schType').val('userNo');
        itemReview.initCategorySelectBox();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId, index) {

        const rowDataJson = itemReviewListGrid.dataProvider.getJsonRow(selectRowId);

        switch (index.fieldName) {
            case 'itemNo' :
                var itemNo = rowDataJson.itemNo
                var storeType = rowDataJson.storeType
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
                break;
            case 'purchaseOrderNo' :
                var purchaseOrderNo = rowDataJson.purchaseOrderNo;
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo);
                break;
            default :

                break;
        }

        $('#reviewNo').val(rowDataJson.reviewNo);
        $('#storeType').html(rowDataJson.storeType);
        $('#itemNo').html(rowDataJson.itemNo);
        $('#itemNm1').html(rowDataJson.itemNm1);
        $('#blockYn').html(rowDataJson.blockYnNm + '&nbsp;&nbsp;' +'<button type="button" class="ui button large mg-r-5 dark-blue" id="itemReviewHistBtn">번경 이력조회</button>');
        $('#reserveYn').html(rowDataJson.reserveYnNm);

        if($.jUtil.isNotEmpty(rowDataJson.reserveDt)) {
            $('#reserveDt').html(moment(rowDataJson.reserveDt).format('YYYY-MM-DD HH:mm:ss'));
        }

        $('#regId').html(rowDataJson.userId + '(' + rowDataJson.userNm + ')');
        $('#regDt').html(moment(rowDataJson.regDt).format('YYYY-MM-DD HH:mm:ss'));
        $('#grade').html(rowDataJson.grade);
        $('#gradeStatus').html(rowDataJson.gradeStatusNm);
        $('#gradeAccuracy').html(rowDataJson.gradeAccuracyNm);
        $('#gradePrice').html(rowDataJson.gradePriceNm);
        $('#contents').html(rowDataJson.contents);

        CommonAjax.basic({
            url : '/voc/itemReview/getItemReviewImg.json',
            data : { 'reviewNo' : rowDataJson.reviewNo },
            method : "GET",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                var imgListHtml = '';
                if(!$.jUtil.isEmpty(res)) {
                    for ( var i in res ) {

                        if(res[i].imgUrl.startsWith('/UserFiles')) {
                            imgListHtml += '<img src="https://image.homeplus.co.kr' + res[i].imgUrl + '" id="popup_img" width="150px" height="150px">';
                        } else {
                            imgListHtml += '<img src="' + hmpImgUrl + "/provide/view?processKey=FrontBanner&fileId=" + res[i].imgUrl + '" id="popup_img" width="150px" height="150px">';
                        }
                    }
                }
                $('#imgList').html(imgListHtml);
            }
        });
    },
    /** 변경이력팝업 */
    popup : function() {
        const url = "/voc/popup/itemReviewBlockHistoryPop?reviewNo="+$("#reviewNo").val();
        // window.open(url,'_blank','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=900,height=400','');
        $.jUtil.openNewPopup(url, 1120, 540);
    },
    /** 변경이력팝업 조회 전 필수값 체크 */
    popupValid : function() {
        if ($.jUtil.isEmpty($("#reviewNo").val())) {
            alert("이력조회 대상 상품평을 선택해주세요.");
            return false;
        }
        return true;
    },
    /**
     * itemReview 등록/수정
     */
    set : function() {
        if(!itemReview.valid.set()){
            return false;
        }

        let arrReviewNo = new Array();
        const checkedItems = itemReviewListGrid.gridView.getCheckedItems();

        for(let i in checkedItems) {
            const rowData = itemReviewListGrid.gridView.getValues(checkedItems[i]);
            arrReviewNo.push(rowData.reviewNo);
        }

        let setData = $('#itemReviewSetForm').serializeObject();
        setData.reviewNos = arrReviewNo;

        CommonAjax.basic({
            url : '/voc/itemReview/setBlockContents.json',
            data : JSON.stringify(setData),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                itemReview.search();
                itemReview.reset();
            }
        });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        itemReviewListGrid.excelDownload();
    },
    /**
     * itemReview 입력/수정 폼 초기화
     */
    reset : function() {
        $("input:radio[name='blockYn']:radio[value='N']").prop('checked', true);
        $("input[name='blockType']").attr('disabled', true).prop('checked', false);
        $('#blockContents').attr('readonly', true).val('');
        $('#reviewNo').val('');
        $('#storeType, #itemNo, #itemNm1, #blockYn, #reserveYn, #reserveDt, #regId, #regDt, #grade, #gradeStatus, #gradeAccuracy, #gradePrice, #contents, #imgList').html('');
    }
};

/**
 * itemReview Validation Check
 */
itemReview.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-1d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "day", true) > 3) {
            alert("최대 3일 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-3, "day").format("YYYY-MM-DD"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        return true;
    },
    set : function () {
        const checkedItemCount = itemReviewListGrid.gridView.getCheckedRows(true).length;
        if(checkedItemCount == 0) {
            alert("변경 하시려는 아이템을 먼저 선택해 주세요.");
            return false;
        }
        const blockYn = $("input:radio[name=blockYn]:checked").val();
        if(blockYn != 'N') {
            const blockContents = $('#blockContents').val();
            if(!blockContents) {
                alert("미노출사유를 입력해 주세요.");
                $('#blockContents').focus();
                return false;
            }
        }
        return true;
    }
};