/** Main Script */
var marketInquiryMain = {
    /**
     * init 이벤트
     */
    init: function() {
        marketInquiryMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-14d", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 마켓연동 SelctBox Change
        $("#schMarketType").on('change', function() { marketInquiryMain.setMessageType($(this)) });

        // 등록구분 SelctBox Change
        $("#schMessageType").on('change', function() { marketInquiryMain.setContactType($(this)) });

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(marketInquiryMain.openStorePopup);

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(marketInquiryMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(marketInquiryMain.reset, "marketInquirySearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(marketInquiryMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-6d", "0")});
        // 달력 - [1개월] 버튼
        $("#setOneMonthBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-29d", "0")});
    },

    /**
     * 마켓별 등록구분 셋팅
     */
    setMessageType: function($this) {
        var $schMarketType = $('#schMarketType')
        var $schMessageType = $('#schMessageType');
        var optHtml = "";

        switch ($schMarketType.val()) {
            case "NAVER" :
                optHtml += '<option value="">전체</option>';
                optHtml += '<option value="B">상품문의</option>';
                optHtml += '<option value="E">판매자문의</option>';
                break;
            case "ELEVEN" :
                optHtml += '<option value="">전체</option>';
                optHtml += '<option value="B">게시판문의</option>';
                optHtml += '<option value="E">긴급메세지</option>';
                break;
            default :
                optHtml += '<option value="">전체</option>';
        }

        $schMessageType.html(optHtml);
        marketInquiryMain.setContactType();
    },

    /**
     * 등로구분별 문의타입 셋팅
     */
    setContactType: function() {
        var $schMarketType = $('#schMarketType');
        var $schMessageType = $('#schMessageType');
        var $schContactType = $('#schContactType');
        var optHtml = "";

        switch ($schMarketType.val()) {
            case "NAVER" :
                switch ($schMessageType.val()) {
                    case "B" :
                        optHtml += '<option value="">전체</option>';
                        optHtml += '<option value="상품">상품</option>';
                        break;
                    case "E" :
                        optHtml += '<option value="">전체</option>';
                        optHtml += '<option value="상품">상품</option>';
                        optHtml += '<option value="배송">배송</option>';
                        optHtml += '<option value="반품">반품</option>';
                        optHtml += '<option value="교환">교환</option>';
                        optHtml += '<option value="환불">환불</option>';
                        optHtml += '<option value="기타">기타</option>';
                        break;
                    default :
                        optHtml += '<option value="">전체</option>';;
                }
                break;
            case "ELEVEN" :
                switch ($schMessageType.val()) {
                    case "B" :
                        optHtml += '<option value="">전체</option>';
                        break;
                    case "E" :
                        optHtml += '<option value="">전체</option>';
                        break;
                    default :
                        optHtml += '<option value="">전체</option>';;
                }
                break;
            default :
                optHtml += '<option value="">전체</option>';
        }

        $schContactType.html(optHtml);
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#marketInquirySearchForm").serializeObject();

        CommonAjax.basic({
            url         : '/voc/marketInquiry/getMarketInquiryList.json',
            data        : JSON.stringify(reqData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                marketInquiryGrid.setData(res);
                $('#marketInquirySearchCnt').html($.jUtil.comma(marketInquiryGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (marketInquiryGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "제휴사고객문의_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        marketInquiryGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다.",
            done: function () {  //내보내기 완료 후 실행되는 함수
            }
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-14d", "0");
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        //  검색조건 (상품주문번호, 배송번호, 주문번호, 회원번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
                && (schKeywordTypeVal == 'messageNo'
                    || schKeywordTypeVal == 'ordNo'
                    || schKeywordTypeVal == 'copOrdNo')) {
            return true;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 29) {
            alert("조회기간은 30일까지 설정 가능합니다.");
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    }
}

/** Grid Script */
var marketInquiryGrid = {
    gridView: new RealGridJS.GridView("marketInquiryGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marketInquiryGrid.initGrid();
        marketInquiryGrid.initDataProvider();
        marketInquiryGrid.event();
    },
    initGrid: function () {
        marketInquiryGrid.gridView.setDataSource(marketInquiryGrid.dataProvider);
        marketInquiryGrid.gridView.setStyles(marketInquiryGridBaseInfo.realgrid.styles);
        marketInquiryGrid.gridView.setDisplayOptions(marketInquiryGridBaseInfo.realgrid.displayOptions);
        marketInquiryGrid.gridView.setColumns(marketInquiryGridBaseInfo.realgrid.columns);
        marketInquiryGrid.gridView.setOptions(marketInquiryGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        marketInquiryGrid.gridView.setColumnProperty("messageNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "messageNo", showUrl: false});
        marketInquiryGrid.gridView.setColumnProperty("ordNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "ordNo", showUrl: false});
        marketInquiryGrid.gridView.setColumnProperty("goodId", "renderer", {type: "link", url: "unnecessary", requiredFields: "goodId", showUrl: false});
        marketInquiryGrid.gridView.setColumnProperty("copGoodId", "renderer", {type: "link", url: "unnecessary", requiredFields: "copGoodId", showUrl: false});
    },
    initDataProvider: function () {
        marketInquiryGrid.dataProvider.setFields(marketInquiryGridBaseInfo.dataProvider.fields);
        marketInquiryGrid.dataProvider.setOptions(marketInquiryGridBaseInfo.dataProvider.options);
    },
    event: function() {
        marketInquiryGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var messageNo = grid.getValue(index.itemIndex, "messageNo");
            var marketType = grid.getValue(index.itemIndex, "marketType");
            var messageType = grid.getValue(index.itemIndex, "messageType");
            var itemNo = grid.getValue(index.itemIndex, "goodId");
            var ordNo = grid.getValue(index.itemIndex, "ordNo");
            var copGoodId = grid.getValue(index.itemIndex, "copGoodId");
            var storeType = 'HYPER';

            if (index.fieldName == "messageNo") {
                windowPopupOpen("/voc/popup/marketInquiryDetailPop?"
                    + "callback=marketInquiryMain.search"
                    + "&messageNo=" + messageNo
                    + "&marketType=" + marketType
                    + "&messageType=" + messageType, "marketInquiryDetailPop", 1, 1, "yes","yes");
            } else if (index.fieldName == "ordNo" && !$.jUtil.isEmpty(ordNo)) {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + ordNo );
            } else if (index.fieldName == "goodId" && !$.jUtil.isEmpty(itemNo)) {
                var frontUrl = homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            } else if (index.fieldName == "copGoodId" && !$.jUtil.isEmpty(copGoodId)) {
                window.open("https://shopping.naver.com/market/homeplus/products/" + copGoodId);
            }
        };
    },
    setData: function (dataList) {
        marketInquiryGrid.dataProvider.clearRows();
        marketInquiryGrid.dataProvider.setRows(dataList);
    }
};