/** Main Script */
var oneOnOneInquiryMain = {
    global : {
      tabId : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        oneOnOneInquiryMain.setStoreOffice();
        oneOnOneInquiryMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        oneOnOneInquiryMain.global.tabId = parent.Tabbar.getActiveTab();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 판매자(점포유형) SelctBox Change
        $("#schStoreType").on('change', function() { oneOnOneInquiryMain.setStoreBtnAble($(this)) });

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(oneOnOneInquiryMain.openStorePopup);

        // 구매자 조회
        $("#schBuyer").on('click', function() { oneOnOneInquiryMain.openMemberSearchPop() });

        // 구분 SelctBox Change
        $("#schInqryCategory").on('change', function() { oneOnOneInquiryMain.setInqryCategory("CATEGORY") });
        // 문의구분 SelctBox Change
        $("#schInqryType").on('change', function() { oneOnOneInquiryMain.setInqryCategory("TYPE") });
        // 문의항목 SelctBox Change
        $("#schInqryDetailType").on('change', function() { oneOnOneInquiryMain.setInqryCategory("DETAILTYPE") });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(oneOnOneInquiryMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(oneOnOneInquiryMain.reset, "oneOnOneInquirySearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(oneOnOneInquiryMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-6d", "0")});
        // 달력 - [1개월] 버튼
        $("#setOneMonthBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-29d", "0")});
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
        }
    },

    /**
     * 판매자(점포유형)에 따라 판매자 조회 버튼 활성화
     */
    setStoreBtnAble : function($this) {
        var $schStoreBtn = $('#schStoreBtn');
        if ($this.val() == '' || $this.val() == 'NONE') {
            $schStoreBtn.prop("disabled",true);
            $("#schStoreId").val("");
            $("#schStoreNm").val("");
        } else {
            $schStoreBtn.prop("disabled",false);
        }
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    // 문의유형 SelctBox Change
    setInqryCategory : function(type) {
        var $inqryCategory = $('#schInqryCategory');
        var $inqryType = $('#schInqryType');

        if(type == "CATEGORY") {
            $("#schInqryType option:eq(0)").prop("selected", true);
            $("#schInqryDetailType option:eq(0)").prop("selected", true);

            $('#schInqryType option').each(function () {
                var $this = $(this);
                if ($.jUtil.isEmpty($this.val())) {
                    return;
                }

                if ($.jUtil.isEmpty($inqryCategory.val())) {
                    $this.hide();
                } else {
                    $this.show();
                }
            });
        }

        if(type == "CATEGORY" || type == "TYPE") {
            $("#schInqryDetailType option:eq(0)").prop("selected", true);

            $('#schInqryDetailType option').each(function () {
                var $this = $(this);
                if ($.jUtil.isEmpty($this.val())) {
                    return;
                }

                if ($.jUtil.isEmpty($inqryType.val())) {
                    $this.hide();
                    return;
                }

                var ref1 = $this.attr("data-ref1");
                var ref2 = $this.attr("data-ref2");
                if (ref1 == $inqryCategory.val() && ref2 == $inqryType.val()) {
                    $this.show();
                } else {
                    $this.hide();
                }
            });

        }

    },

    /**
     * 회원 검색 팝업
     */
    openMemberSearchPop: function() {
        memberSearchPopup("callBack.searchMember");
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#oneOnOneInquirySearchForm").serialize();
        CommonAjax.basic({
            url         : '/voc/oneOnOneInquiry/getOneOnOneInquiryList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                oneOnOneInquiryGrid.setData(res);
                $('#oneOnOneInquirySearchCnt').html($.jUtil.comma(oneOnOneInquiryGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (oneOnOneInquiryGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "일대일문의관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        SettleCommon.setRealGridColumnVisible(oneOnOneInquiryGrid.gridView, [SettleCommon.getRealGridColumnName(oneOnOneInquiryGrid.gridView, "내용")], true);
        oneOnOneInquiryGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다.",
            done: function () {  //내보내기 완료 후 실행되는 함수
                SettleCommon.setRealGridColumnVisible(oneOnOneInquiryGrid.gridView, [SettleCommon.getRealGridColumnName(oneOnOneInquiryGrid.gridView, "내용")], false);
            }
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        oneOnOneInquiryMain.setStoreOffice();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        //  검색조건 (상품주문번호, 배송번호, 주문번호, 회원번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
                && (schKeywordTypeVal == 'purchaseOrderNo'
                    || schKeywordTypeVal == 'inqryAnswrRegId'
                    || schKeywordTypeVal == 'inqryNo')) {
            return true;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            //shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 29) {
            alert("조회기간은 30일까지 설정 가능합니다.");
            //shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-29d", "0");
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    },
    searchMember : function (res) {
        $('#schUserNo').val(res.userNo);
        $('#schUserNm').val(res.userNm);
    }
}

/** Grid Script */
var oneOnOneInquiryGrid = {
    gridView: new RealGridJS.GridView("oneOnOneInquiryGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        oneOnOneInquiryGrid.initGrid();
        oneOnOneInquiryGrid.initDataProvider();
        oneOnOneInquiryGrid.event();
    },
    initGrid: function () {
        oneOnOneInquiryGrid.gridView.setDataSource(oneOnOneInquiryGrid.dataProvider);
        oneOnOneInquiryGrid.gridView.setStyles(oneOnOneInquiryGridBaseInfo.realgrid.styles);
        oneOnOneInquiryGrid.gridView.setDisplayOptions(oneOnOneInquiryGridBaseInfo.realgrid.displayOptions);
        oneOnOneInquiryGrid.gridView.setColumns(oneOnOneInquiryGridBaseInfo.realgrid.columns);
        oneOnOneInquiryGrid.gridView.setOptions(oneOnOneInquiryGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        oneOnOneInquiryGrid.gridView.setColumnProperty("inqryTitle", "renderer", {type: "link", url: "unnecessary", requiredFields: "inqryTitle", showUrl: false});
        oneOnOneInquiryGrid.gridView.setColumnProperty("userNm", "renderer", {type: "link", url: "unnecessary", requiredFields: "userNm", showUrl: false});
        oneOnOneInquiryGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});

        // 전체선택 false
        //shipManageGrid.gridView.setCheckBar({showAll: false});
    },
    initDataProvider: function () {
        oneOnOneInquiryGrid.dataProvider.setFields(oneOnOneInquiryGridBaseInfo.dataProvider.fields);
        oneOnOneInquiryGrid.dataProvider.setOptions(oneOnOneInquiryGridBaseInfo.dataProvider.options);
    },
    event: function() {
        oneOnOneInquiryGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var inqryNo = grid.getValue(index.itemIndex, "inqryNo");
            var inqryKind = grid.getValue(index.itemIndex, "inqryKind");
            var userNo = grid.getValue(index.itemIndex, "userNo");
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            if (index.fieldName == "inqryTitle") {
                windowPopupOpen("/voc/popup/oneOnOneInquiryDetailPop?callback=oneOnOneInquiryMain.search&inqryNo=" + inqryNo + "&inqryKind=" + inqryKind, "oneOnOneInquiryDetailPop", 1, 1, "yes","yes");
            } else if (index.fieldName == "userNm") {
                UIUtil.addTabWindowBesideParent(
                    "/user/userInfo/detail?userNo=" + userNo, "userDetailTab",
                    "회원정보 상세", 120, oneOnOneInquiryMain.global.tabId);
            } else if (index.fieldName == "purchaseOrderNo" && !$.jUtil.isEmpty(purchaseOrderNo)) {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo );
            }
        };
    },
    setData: function (dataList) {
        oneOnOneInquiryGrid.dataProvider.clearRows();
        oneOnOneInquiryGrid.dataProvider.setRows(dataList);
    }
};