/**
 * menu depth info
 */
$(document).ready(function() {
    csQnaListGrid.init();
    csQna.init();
    CommonAjaxBlockUI.global();

});

// csQna 그리드
var csQnaListGrid = {

  gridView : new RealGridJS.GridView("csQnaListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),

  init : function() {

      csQnaListGrid.initGrid();
      csQnaListGrid.initDataProvider();
      csQnaListGrid.event();

  },
  initGrid : function() {

      csQnaListGrid.gridView.setDataSource(csQnaListGrid.dataProvider);
      csQnaListGrid.gridView.setStyles(csQnaListGridBaseInfo.realgrid.styles);
      csQnaListGrid.gridView.setDisplayOptions(csQnaListGridBaseInfo.realgrid.displayOptions);
      csQnaListGrid.gridView.setColumns(csQnaListGridBaseInfo.realgrid.columns);
      csQnaListGrid.gridView.setOptions(csQnaListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {

      csQnaListGrid.dataProvider.setFields(csQnaListGridBaseInfo.dataProvider.fields);
      csQnaListGrid.dataProvider.setOptions(csQnaListGridBaseInfo.dataProvider.options);
  },
  event : function() {
      // 그리드 선택
      csQnaListGrid.gridView.onDataCellClicked = function(gridView, index) {
          csQna.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
      csQnaListGrid.dataProvider.clearRows();
      csQnaListGrid.dataProvider.setRows(dataList);
  },
  delData : function(dataList){
    csQnaListGrid.dataProvider.removeRows(dataList,false);
  },
  excelDownload: function() {
    if(csQnaListGrid.gridView.getItemCount() == 0) {
        alert("검색 결과가 없습니다.");
        return false;
    }

    var _date = new Date();
    var fileName = "파일네임"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    csQnaListGrid.gridView.exportGrid( {
        type: "excel",
        target: "local",
        fileName: fileName+".xlsx",
        showProgress: true,
        progressMessage: " 엑셀 데이터 추줄 중입니다."
    });

}

};

// csQna관리
var csQna = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-1d');
        this.initCategorySelectBox();
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        csQna.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        csQna.setDate.setEndDate(0);
        csQna.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(csQna.search); //csQna 검색
        $('#searchResetBtn').bindClick(csQna.searchReset); //csQna 검색폼 초기화
        $('#addReply').bindClick(csQna.addReply); //csQna 등록,수정
        $('#resetBtn').bindClick(csQna.reset); //csQna 입력폼 초기화
        $('#setQnaStatus').bindClick(csQna.setQnaDisplayStatus); //csQna 노출정지
        $('#reply').calcTextLength('keyup', '#textCountReply');
        /*$('#searchPartnerId').focusout(csQna.getPartnerName);*/


    },

    /**
     * Q&A 답변 리스트 검색
     */
    search : function() {
       if(!csQna .valid.search()){
            return false;
        
        }
        CommonAjax.basic({url:'/voc/csQna/getQnaList.json?' + $('#csQnaSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            csQna.reset();
            csQnaListGrid.setData(res);
            $('#csQnaTotalCount').html($.jUtil.comma(csQnaListGrid.gridView.getItemCount()));
        }});

    },
    /**
     * 검색폼 초기화
     */
    searchReset : function() {
        $("#csQnaSearchForm").resetForm();
        csQna.initSearchDate('-1d');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = csQnaListGrid.dataProvider.getJsonRow(selectRowId);

        $('#qnaNo').html(rowDataJson.qnaNo);
        $('#itemNo').html(rowDataJson.itemNo);
        $('#itemNm').html(rowDataJson.itemNm);
        $('#regDt').html(rowDataJson.regDt);
        $('#qnaStatusTxt').html(rowDataJson.qnaStatusTxt);
        $('#displayStatusTxt').html(rowDataJson.displayStatusTxt);
        $('#secretYn').html(rowDataJson.secretYn);
        $('#purchaseYnTxt').html(rowDataJson.purchaseYn +'(' +rowDataJson.purchaseOrderNo +')' );
        $('#userNm').html(rowDataJson.userNm);
        $('#userId').html(rowDataJson.userId);
        $('#partnerNm').html(rowDataJson.partnerNm);
        $('#partnerId').html(rowDataJson.partnerId);
        $('#qnaContents').text(rowDataJson.qnaContents);
        $('#reply').val('');

        if(rowDataJson.displayStatus == 'DISP'){

            $('#setQnaStatusButton').show();
            $('#setReplyArea').show();

        }  else {

            $('#setQnaStatusButton').hide();
            $('#setReplyArea').hide();
        }

        csQna.getQnaReplyList(rowDataJson.qnaNo);

    },
    /**
     * Q&A 답변 리스트 조회
     */
    getQnaReplyList : function(qnaNo){

        $('#replyList').html('');
        CommonAjax.basic({url:'/voc/csQna/getQnaReplyList.json?qnaNo=' + qnaNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {

                var regInfo ="";
                for (var reply in res) {

                    if(res[reply].replyArea == 'PARTNER') {
                        regInfo = 'L 판매자(' + res[reply].partnerNm +' )';
                    } else{
                        regInfo = 'L 홈플러스(' + res[reply].regNm +' )';
                    }
                    regInfo += ' <span class="ui button small gray-dark font-malgun mg-r-5">'+res[reply].displayStatusTxt+'</span>';
                    csQna.appendReply(regInfo, res[reply].regDt, res[reply].qnaReply, res[reply].displayStatus, res[reply].qnaDisplayStatus, res[reply].replyArea,res[reply].qnaReplyNo  );
                    regInfo ="";
                }

            }});
    },
    /**
     *  Q&A 답변 리스트  추가
     */
    appendReply : function(regInfo, regDt, reply, displayStatus, qnaDisplayStatus, replyArea,replyNo ) {

        var appendReply =
            ' <tr><td colspan="2"><span>' + regInfo+'</span></td></tr>\n'
            + '<tr>\n'
            + '<td>\n'
            + '<textarea id="reply_'+ replyNo+ '"class="ui input" style="width: 100%; height: 70px;" maxlength="1500"'
            if (displayStatus != 'DISP' || replyArea != 'ADMIN' || qnaDisplayStatus!='DISP' ){ // QNA 문의(답변) 삭제, 노출 중지 , Partner 영역에서의 등록된 답변은 수정 불가
                appendReply += "disabled";
            }
            appendReply +='>' + reply+ '</textarea>\n'
            if (qnaDisplayStatus =='DISP' && displayStatus == 'DISP') {       //노춭 중일 경우에만 수정 및 노출 중지 버튼 노출
                if (replyArea == 'ADMIN') {     // 답변 영역이 파트너인 경우 수정 불가
                    appendReply+='<button type="button" onclick="csQna.modifyReply(' + replyNo+ ');" '
                        + 'class="ui button large middle mg-t-5" style="margin-right: 10px;">수정</button>&nbsp;\n'
                }
                appendReply += '<button type="button" id="" onclick="csQna.setQnaReplyDisplayStatus('+ replyNo + ');" '
                    + 'class="ui button large middle mg-t-5">노출중지</button>\n'
            }

            appendReply += '<span id="reply_txt_' + replyNo+ '"style="float: right;margin-top: 10px;" class="text">'
            if (qnaDisplayStatus =='DISP' && displayStatus == 'DISP' && replyArea == 'ADMIN') {
                appendReply+='( <span style="color:red;" id="reply_txt_cnt_'+ replyNo + '">'+ reply.length + '</span> / 1500자 )'
            }
            appendReply+= '</span></td></tr>\n'
        $('#replyList').append(appendReply);
        $('#reply_'+replyNo).calcTextLength('keyup', '#reply_txt_cnt_'+replyNo);

    },
    /**
     * Q&A 노출 상태값 변경
     */
    setQnaDisplayStatus : function(){

        if(!confirm("Q&A 내용을 중지하시겠습니까?\n노출 중지된 Q&A는 다시 노출할 수 없습니다.")){

            return false;
        }
        var setQna = new Object();
        setQna.qnaNo = $('#qnaNo').html();
        setQna.displayStatus = 'PAUSE';

        CommonAjax.basic({url:'/voc/csQna/setQnaDisplayStatus.json',data: JSON.stringify(setQna), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                csQna.search();
                alert(res.returnMsg);

        }});
    },
    /**
     * Q&A 답변 노출 상태값 변경
     */
    setQnaReplyDisplayStatus : function(replyNo){
        if(!confirm("Q&A 답변 내용을 중지하시겠습니까?\n노출 중지된 답변은 다시 노출할 수 없습니다.")){

            return false;
        }
        var setQnaReply = new Object();
        setQnaReply.qnaNo = $('#qnaNo').html();
        setQnaReply.qnaReplyNo = replyNo;
        setQnaReply.displayStatus = 'PAUSE';

        CommonAjax.basic({url:'/voc/csQna/setQnaReplyDisplayStatus.json',data: JSON.stringify(setQnaReply), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                csQna.search();
                alert(res.returnMsg);
            }});
    },
    /**
     *  Q&A 답변 내용 수정
     */
    modifyReply : function(replyNo){
        if(confirm("변경된 내용으로 수정하시겠습니까?")){
            csQna.setQnaReply(replyNo, $('#reply_' +replyNo ).val());
        }

    },
    /**
     *  Q&A 답변 등록
     */
    addReply : function() {

        csQna.setQnaReply(null, $('#reply').val());

    },
    setQnaReply : function(qnaReplyNo, replyTxt){

        var setQnaReplay = new Object();
        setQnaReplay.qnaNo = $('#qnaNo').html();  //선택된 Q&A 번호
        setQnaReplay.replyArea = 'ADMIN';  //답변영역(ADMIN, PARTNER)
        setQnaReplay.qnaReplyNo = qnaReplyNo;  //선택된 Q&A 답변 번호
        setQnaReplay.displayStatus = 'DISP'; //노출 여부
        setQnaReplay.qnaReply = replyTxt; //답변 내용

        if(!csQna .valid.setReply( setQnaReplay.qnaNo, replyTxt)){
            return false;
        }

        CommonAjax.basic({url:'/voc/csQna/setQnaReply.json',data: JSON.stringify(setQnaReplay), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                csQna.search();
            }});

    },
    /**
     * 제휴 업체 상호명 조회 이호셕
     */
    getPartnerName : function() {


        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback=csQna.addPartner"
            + "&partnerType="
            + "SELL"
            + "&isMulti=N"
            , "partnerPop", "1080", "600", "yes", "no");

    },
    addPartner : function(partnerList){
        $('#searchPartnerId').val(partnerList[0].partnerId);
        $('#searchPartnerNm').val(partnerList[0].businessNm);

        return true;
    },
    /**
     *  Q&A 입력/수정 폼 초기화
     */
    reset : function() {

        $('#qnaNo').html('');
        $('#itemNo').html('');
        $('#itemNm').html('');
        $('#regDt').html('');
        $('#qnaStatusTxt').html('');
        $('#displayStatusTxt').html('');
        $('#secretYn').html('');
        $('#purchaseYnTxt').html('');
        $('#userNm').html('');
        $('#userId').html('');
        $('#partnerNm').html('');
        $('#qnaContents').html('');
        $('#replyList').html('');
        $('#setQnaStatusButton').hide();
        $('#setReplyArea').hide();
        $('#textCountReply').html('0');
    }
};
/**
 *  Q&A 검색,입력,수정 validation check
 */
csQna.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            csQna.initSearchDate('-1d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }
      return true;

    },
    setReply : function (qnaNo, reply) {
        if($.jUtil.isEmpty(qnaNo)){
            alert("상품 Q&A를 선택해 주세요");
            return false;
        }
        if($.jUtil.isEmpty(reply) || reply.length<10){
            alert("10자 이상으로 작성해주세요.");
            return false;
        }

      return true;
    }
};