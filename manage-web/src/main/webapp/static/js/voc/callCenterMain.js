/** Main Script */
var callCenterMain = {
    global : {
        tabId : ""
        , reqData : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        callCenterMain.setStoreOffice();
        callCenterMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        callCenterMain.global.tabId = parent.Tabbar.getActiveTab();

        // 진입 시 회원번호, 회원명 받아왔을 경우
        if(!$.jUtil.isEmpty(userNo) && !$.jUtil.isEmpty(userNm)) {
            var res = new Object();
            res.userNo = userNo;
            res.userNm = userNm;
            callBack.searchMember(res);

            shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-179d", "0");
            callCenterMain.search();
        }
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 판매자(점포유형) SelctBox Change
        $("#hChannel").on('change', function() { callCenterMain.setStoreBtnAble($(this)); callCenterMain.setAdvisorType1()});

        // 상담구분 SelctBox Change
        $("#advisorType1").on('change', function() {callCenterMain.setAdvisorType2()});

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(callCenterMain.openStorePopup);

        // 작성자(회원) 회원 비회원 선택
        $('#schNomemOrder').on('change', function() { callCenterMain.setMemberSearch($(this)) });

        // 작성자(회원) 조회
        $("#schBuyer").on('click', function() { callCenterMain.openMemberSearchPop() });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(callCenterMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(callCenterMain.reset, "callCenterSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(callCenterMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-6d", "0")});
        $("#setOneMonthBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-29d", "0")});

        // // 달력 - [오늘] 버튼
        // $("#setTodayBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "0")});
        // // 달력 - [어제] 버튼
        // $("#setYesterDayBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "-1d")});
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (isSo) {
            $("#hChannel").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#hChannel").val(callCenterMain.getChangeStoreType(so_storeType));
        }
    },

    /**
     * 판매자(점포유형)에 따라 판매자 조회 버튼 활성화
     */
    setStoreBtnAble : function($this) {
        var $schStoreBtn = $('#schStoreBtn');
        if ($this.val() == '' || $this.val() == 'NONE') {
            $schStoreBtn.prop("disabled",true);
        } else {
            $schStoreBtn.prop("disabled",false);
        }
    },

    /**
     * 점포유형 설정
     */
    getChangeStoreType: function(storeType) {
        var changeType = "";

        switch (storeType) {
            case "H_CHANNEL_1" : changeType = "HYPER"; break;
            case "H_CHANNEL_2" : changeType = "CLUB"; break;
            case "H_CHANNEL_3" : changeType = "DS"; break;
            case "H_CHANNEL_4" : changeType = "EXP"; break;
            case "HYPER" : changeType = "H_CHANNEL_1"; break;
            case "CLUB" : changeType = "H_CHANNEL_2"; break;
            case "DS" : changeType = "H_CHANNEL_3"; break;
            case "EXP" : changeType = "H_CHANNEL_4"; break;
            default : changeType = "";
        }

        return changeType;
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#hChannel').val();
        var storeType = callCenterMain.getChangeStoreType(storeTypeVal);

        if ($.jUtil.isEmpty(storeType)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeType == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeType , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 작성자 회원 비회원 선택
     */
    setMemberSearch: function($this) {
        var $thisVal = $this.val();

        if ($thisVal == "회원") {
            $('#schBuyer').show();
            $('#schUserNm').show();
        } else {
            $('#schBuyer').hide();
            $('#schUserNm').hide();
        }
    },

    // 상담구분 SelctBox Change
    setAdvisorType1 : function() {
        var $hChannel = $('#hChannel');

        $('#advisorType1 option').each(function () {
            var $this = $(this);
            if ($.jUtil.isEmpty($this.val())) {
                return;
            }

            if($this.attr("gmcCd") == $hChannel.val()) {
                $this.show();
            } else {
                $this.hide();
            }
        });

        $("#advisorType1 option:eq(0)").prop("selected", true);
        callCenterMain.setAdvisorType2();
    },

    // 상담항목 SelctBox Change
    setAdvisorType2 : function() {
        var $advisorType1 = $('#advisorType1');

        $('#advisorType2 option').each(function () {
            var $this = $(this);
            if ($.jUtil.isEmpty($this.val())) {
                return;
            }

            if($this.attr("gmcCd") == $advisorType1.val()) {
                $this.show();
            } else {
                $this.hide();
            }
        });

        $("#advisorType2 option:eq(0)").prop("selected", true);
    },

    /**
     * 회원 검색 팝업
     */
    openMemberSearchPop: function() {
        memberSearchPopup("callBack.searchMember");
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#callCenterSearchForm").serialize();

        callCenterMain.global.reqData = reqData;

        CommonAjax.basic({
            url         : '/voc/callCenter/getCallCenterList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                callCenterGrid.setData(res);
                $('#callCenterSearchCnt').html($.jUtil.comma(callCenterGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        CommonAjax.basic({
            url         : '/voc/callCenter/getCallCenterExcelList.json?' + callCenterMain.global.reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                callCenterExcelGrid.setData(res);

                if (callCenterExcelGrid.gridView.getItemCount() == 0) {
                    alert("검색 결과가 없습니다.");
                    return false;
                }

                var _date = new Date();
                var fileName =  "콜센터문의관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
                callCenterExcelGrid.gridView.exportGrid({
                    type: "excel",
                    target: "local",
                    fileName: fileName + ".xlsx",
                    showProgress: true,
                    progressMessage: "엑셀 Export 중 입니다."
                });
            }
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        callCenterMain.setStoreOffice();

        $('#schUserNo').val("");
        $('#hidUserNo').val("");
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        // 비회원 검색 시 회원번호는 "비식별"로 검색
        var schNomemOrderVal = $('#schNomemOrder').val()
        if(schNomemOrderVal == '회원') {
            $('#schUserNo').val($('#hidUserNo').val());
        } else {
            $('#schUserNo').val(schNomemOrderVal);
        }

        // 점포코드 왼쪽에 문자열 0 추가
        var schStoreIdVal = $('#schStoreId').val();
        if(!$.jUtil.isEmpty(schStoreIdVal)) {
            $('#schStoreId').val(schStoreIdVal.lpad(4,"0"));
        }

        //  검색조건 (상품주문번호, 배송번호, 주문번호, 회원번호)
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        $('#H_ORDER_NO').val("");
        $('#HISTORY_TITLE').val("");
        $('#COMPLET_ACCOUNT_NM').val("");
        $('#MASTER_SEQ').val("");
        if(!$.jUtil.isEmpty(schKeywordVal)) {
            $('#'+schKeywordTypeVal).val(schKeywordVal);
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 29) {
            if(schNomemOrderVal == '회원' && !$.jUtil.isEmpty($('#schUserNo').val())) {
                if (schEndDt.diff(schStartDt, "day", true) > 179) {
                    alert("회원번호로 조회 시 180일까지 설정 가능합니다.");
                    return false;
                }
            } else {
                alert("조회기간은 30일까지 설정 가능합니다.");
                return false;
            }
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    },
    searchMember : function (res) {
        $('#hidUserNo').val(res.userNo);
        $('#schUserNm').val(res.userNm);
    },
    searchOrderDetail : function (res) {
        orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + res );
    }
}

/** Grid Script */
var callCenterGrid = {
    gridView: new RealGridJS.GridView("callCenterGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        callCenterGrid.initGrid();
        callCenterGrid.initDataProvider();
        callCenterGrid.event();
    },
    initGrid: function () {
        callCenterGrid.gridView.setDataSource(callCenterGrid.dataProvider);
        callCenterGrid.gridView.setStyles(callCenterGridBaseInfo.realgrid.styles);
        callCenterGrid.gridView.setDisplayOptions(callCenterGridBaseInfo.realgrid.displayOptions);
        callCenterGrid.gridView.setColumns(callCenterGridBaseInfo.realgrid.columns);
        callCenterGrid.gridView.setOptions(callCenterGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        callCenterGrid.gridView.setColumnProperty("history_title", "renderer", {type: "link", url: "unnecessary", requiredFields: "history_title", showUrl: false});
        callCenterGrid.gridView.setColumnProperty("cust_id_mask", "renderer", {type: "link", url: "unnecessary", requiredFields: "cust_id_mask", showUrl: false});
        callCenterGrid.gridView.setColumnProperty("h_order_no", "renderer", {type: "link", url: "unnecessary", requiredFields: "h_order_no", showUrl: false});
    },
    initDataProvider: function () {
        callCenterGrid.dataProvider.setFields(callCenterGridBaseInfo.dataProvider.fields);
        callCenterGrid.dataProvider.setOptions(callCenterGridBaseInfo.dataProvider.options);
    },
    event: function() {
        callCenterGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var KEY = grid.getValue(index.itemIndex, "KEY");
            var channel_type = grid.getValue(index.itemIndex, "channel_type");
            var cust_id = grid.getValue(index.itemIndex, "cust_id");
            var h_order_no = grid.getValue(index.itemIndex, "h_order_no");
            var market_type_nm = grid.getValue(index.itemIndex, "market_type_nm");

            if (index.fieldName == "history_title") {
                windowPopupOpen("/voc/popup/getCallCenterDetailPop?callback=callCenterMain.search&callbackLink=callBack.searchOrderDetail"
                    + "&master_seq=" + KEY + "&channel_type=" + channel_type, "callCenterDetailPop", 1, 1, "yes","yes");
            } else if (index.fieldName == "cust_id_mask"
                        && cust_id != "비식별"
                        && $.jUtil.isEmpty(market_type_nm) ) {
                UIUtil.addTabWindowBesideParent(
                    "/user/userInfo/detail?userNo=" + cust_id, "userDetailTab",
                    "회원정보 상세", 120, callCenterMain.global.tabId);
            } else if (index.fieldName == "h_order_no" && !$.jUtil.isEmpty(h_order_no)) {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + h_order_no );
            }
        };
    },
    setData: function (dataList) {
        callCenterGrid.dataProvider.clearRows();
        callCenterGrid.dataProvider.setRows(dataList);
    }
};

/** Excel Grid Script */
var callCenterExcelGrid = {
    gridView: new RealGridJS.GridView("callCenterExcelGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        callCenterExcelGrid.initGrid();
        callCenterExcelGrid.initDataProvider();
        callCenterExcelGrid.event();
    },
    initGrid: function () {
        callCenterExcelGrid.gridView.setDataSource(callCenterExcelGrid.dataProvider);
        callCenterExcelGrid.gridView.setStyles(callCenterExcelGridBaseInfo.realgrid.styles);
        callCenterExcelGrid.gridView.setDisplayOptions(callCenterExcelGridBaseInfo.realgrid.displayOptions);
        callCenterExcelGrid.gridView.setColumns(callCenterExcelGridBaseInfo.realgrid.columns);
        callCenterExcelGrid.gridView.setOptions(callCenterExcelGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        callCenterExcelGrid.dataProvider.setFields(callCenterExcelGridBaseInfo.dataProvider.fields);
        callCenterExcelGrid.dataProvider.setOptions(callCenterExcelGridBaseInfo.dataProvider.options);
    },
    event: function() {
    },
    setData: function (dataList) {
        callCenterExcelGrid.dataProvider.clearRows();
        callCenterExcelGrid.dataProvider.setRows(dataList);
    }
};