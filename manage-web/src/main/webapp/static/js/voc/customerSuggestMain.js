/**
 * 회원관리 > 고객문의관리 > 제안하기
 */
const customerSuggestMain = {
    init: function () {
        this.event();
        this.initSearchDate('-30d');
    },

    initSearchDate: function (flag) {
        customerSuggestMain.setDate =
            Calendar.datePickerRange('schStartDate', 'schEndDate');
        customerSuggestMain.setDate.setEndDate(0);
        customerSuggestMain.setDate.setStartDate(flag);
        $("#schEndDate").datepicker("option", "minDate",
            $("#schStartDate").val());
    },

    event: function () {
        $('#searchBtn').on('click', function () {
            customerSuggestMain.search();
        });

        $('#searchResetBtn').on('click', function () {
            customerSuggestMain.searchFormReset();
        });

        $('#excelDownloadBtn').on('click', function () {
            customerSuggestListGrid.excelDownload();
        })
    },

    gridRowSelect: function (selectRowId) {
        let json = customerSuggestListGrid.dataProvider.getJsonRow(selectRowId);
        let customerSuggestSeq = json.customerSuggestSeq;
        CommonAjax.basic({
            url: '/voc/customerSuggest/getCustomerSuggestInfo.json?customerSuggestSeq='
                + customerSuggestSeq,
            data: null,
            contentType: 'application/json',
            method: 'GET',
            successMsg: null,
            callbackFunc: function (res) {
                $('#customerSuggestForm').resetForm();
                customerSuggestMain.setDetailData(json, res.data);
            }
        });
    },

    setDetailData: function (req, res) {
        $('#userNo').text(res.userNo);
        $('#platformTypeData').text(req.platformType.toLowerCase());

        if (req.osType === 'ANDROID') {
            $('#osTypeData').text('Android');
        } else if (req.osType === 'IOS') {
            $('#osTypeData').text('iOS');
        } else {

        }
        $('#deviceModel').text(res.deviceModel);
        $('#customerSuggestMessage').text(res.customerSuggestMessage);
    },

    search: function () {
        let param = $('#customerSuggestSearchForm').serialize();
        $('#customerSuggestForm').resetForm();
        CommonAjax.basic({
            url: '/voc/customerSuggest/getCustomerSuggestInfos.json?' + param,
            data: null,
            contentType: 'application/json',
            method: 'GET',
            successMsg: null,
            callbackFunc: function (res) {
                customerSuggestListGrid.setData(res.data);
                $('#customerSuggestTotalCount').html($.jUtil.comma(
                    customerSuggestListGrid.dataProvider.getRowCount()))
            }
        });
    },

    searchFormReset: function () {
        $('#customerSuggestSearchForm').resetForm();
        customerSuggestMain.initSearchDate('-30d');
    }
};

const customerSuggestListGrid = {
    gridView: new RealGridJS.GridView("customerSuggestListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        customerSuggestListGrid.initGrid();
        customerSuggestListGrid.initDataProvider();
        customerSuggestListGrid.event();
    },
    initGrid: function () {
        customerSuggestListGrid.gridView.setDataSource(
            customerSuggestListGrid.dataProvider);
        customerSuggestListGrid.gridView.setStyles(
            customerSuggestGridBaseInfo.realgrid.styles);
        customerSuggestListGrid.gridView.setDisplayOptions(
            customerSuggestGridBaseInfo.realgrid.displayOptions);
        customerSuggestListGrid.gridView.setColumns(
            customerSuggestGridBaseInfo.realgrid.columns);
        customerSuggestListGrid.gridView.setOptions(
            customerSuggestGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        customerSuggestListGrid.dataProvider.setFields(
            customerSuggestGridBaseInfo.dataProvider.fields);
        customerSuggestListGrid.dataProvider.setOptions(
            customerSuggestGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        customerSuggestListGrid.gridView.onDataCellClicked =
            function (gridView, index) {
                customerSuggestMain.gridRowSelect(index.dataRow);
            };
    },
    setData: function (dataList) {
        customerSuggestListGrid.dataProvider.clearRows();
        customerSuggestListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (customerSuggestListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "고객_제안_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        customerSuggestListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            lookupDisplay: true,
            applyDynamicStyles: true,
            showProgress: true,
            applyFitStyle: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

$(document).ready(function () {
    customerSuggestMain.init();
    customerSuggestListGrid.init();
    CommonAjaxBlockUI.global();
});
