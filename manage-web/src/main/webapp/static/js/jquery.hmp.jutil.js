/**
 * 공통 Util 성격의 jQuery 플러그인
 *
 * $.fn.{function_name} 형태로 생성
 * $.jUtil.{function_name} 형태로 생성
 */
(function($) {

    /**
     * serializeObject
     */
    $.fn.serializeObject = function(isSerializeArray) {
        let result = {}
        let extend = function(i, element) {
            let strObj = element.name.split('.');

            if (strObj.length > 1) {
                let strArr = strObj[0].split('[]');

                if (strArr.length > 1) {
                    let node = result[strArr[0]];

                    if ("undefined" !== typeof node && node !== null) {
                        let idx = result[strArr[0]].length;

                        if (result[strArr[0]][idx-1].hasOwnProperty(strObj[1])) {
                            result[strArr[0]][idx] = {};
                            result[strArr[0]][idx][strObj[1]] = element.value;
                        } else {
                            result[strArr[0]][idx-1][strObj[1]] = {};
                            result[strArr[0]][idx-1][strObj[1]] = element.value;
                        }

                    } else {
                        result[strArr[0]] = [];
                        result[strArr[0]][0] = {};
                        result[strArr[0]][0][strObj[1]] = element.value;
                    }
                } else {
                    if (typeof result[strObj[0]] != 'object') {
                        result[strObj[0]] = {};
                    }

                    result[strObj[0]][strObj[1]] = element.value;
                }

            } else {
                result[element.name] = element.value;
            }
        }

        if (isSerializeArray) {
            $.each(this.serializeArray(), extend)
        } else {
            $.each(this.serializeAll(), extend)
        }

        return result
    };
    /**
     * serializeAll (disabled된 데이터 포함)
     */
    $.fn.serializeAll = function(){
        var data = $(this).serializeArray();
        $(':disabled[name]', this).each(function () {
            data.push({ name: this.name, value: $(this).val() });
        });
        return data;
    }
    /**
     * focus out 이벤트 바인딩
     *
     * @param func
     * @param param
     * @returns {jQuery}
     */
    $.fn.focusout = function(func, param) {
        return $.jUtil.bindEvent(this, 'focusout', func, param);
    };


    /**
     * click 이벤트 바인딩
     *
     * @param func
     * @param param
     * @returns {jQuery}
     */
    $.fn.bindClick = function(func, param) {
        return $.jUtil.bindEvent(this, 'click', func, param);
    };
    /**
     * change 이벤트 바인딩
     *
     * @param func
     * @param param
     * @returns {jQuery}
     */
    $.fn.bindChange = function(func, param) {
        return $.jUtil.bindEvent(this, 'change', func, param);
    };
    /**
     * 글자수 계산
     * @param action
     * @param targetObj
     * @returns {jQuery}
     */
    $.fn.calcTextLength = function(action, targetObj) {
        this.on(action, function() {
            var $element = $(this);
            $(targetObj).html($element.val().length);
        });
        return this;
    };
    /**
     * 글자수 표시
     * @param action
     * @param targetObj
     * @returns {jQuery}
     */
    $.fn.setTextLength = function(targetObj) {
        $(targetObj).html(this.val().length);
        return this;
    };
    /**
     *
     * @param event
     * @param types
     * @returns {jQuery}
     */
    $.fn.allowInput = function(event, types, focusTagId, fuc) {
        this.on(event, function () {
            var $element = $(this);
            var space = '\\s';
            var tailMsg = '는 입력할 수 없습니다.';
            var pattern = '';
            var msg = new Array();
            for(var i =0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'NUM':
                        msg.push($.jUtil.constants.message.NUM);
                        pattern += $.jUtil.constants.format.NUM; break;
                    case 'ENG':
                        msg.push($.jUtil.constants.message.ENG);
                        pattern += $.jUtil.constants.format.ENG; break;
                    case 'ENG_LOWER':
                        msg.push($.jUtil.constants.message.ENG_LOWER);
                        pattern += $.jUtil.constants.format.ENG_LOWER; break;
                    case 'ENG_UPPER':
                        msg.push($.jUtil.constants.message.ENG_UPPER);
                        pattern += $.jUtil.constants.format.ENG_UPPER; break;
                    case 'KOR':
                        msg.push($.jUtil.constants.message.KOR);
                        pattern += $.jUtil.constants.format.KOR; break;
                    case 'DOT':
                        msg.push($.jUtil.constants.message.DOT);
                        pattern += $.jUtil.constants.format.DOT; break;
                    case 'COMMA':
                        msg.push($.jUtil.constants.message.COMMA);
                        pattern += $.jUtil.constants.format.COMMA; break;
                    case 'NOT_SPACE':
                        tailMsg = ' 및 띄어쓰기는 입력할 수 없습니다.';
                        space = ''; break;
                    case 'SPC_1' :
                        msg.push($.jUtil.constants.message.SPC_1);
                        pattern += $.jUtil.constants.format.SPC_1; break;
                    case 'SPC_4' :
                        msg.push($.jUtil.constants.message.SPC_4);
                        pattern += $.jUtil.constants.format.SPC_4; break;
                    default:
                        alert('정의되지 않은 체크타입 입니다.  type=' + types[i]); break;
                }
            }

            var regexp = new RegExp('[^' + pattern + space + ']', 'g');

            if (regexp.test($element.val())) {
                alert(msg.join(', ') + ' 외의 문자' + tailMsg);
                if (focusTagId) {
                    $('#'+focusTagId).focus();
                }
            }
            $element.val($element.val().replace(regexp, ""));

            if (typeof fuc == 'function') {
                fuc($element.val());
            }
        });
        return this;
    };

    $.fn.notAllowInput = function(event, types, focusTagId, fuc) {
        this.on(event, function () {
            var $element = $(this);
            var pattern = '';
            var msg = $.jUtil.getNotAllowInputMessage(types)

            for (var i = 0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'SPC_SCH':
                        pattern += $.jUtil.constants.format.SPC_SCH;
                        break;
                    case 'SYS_DIVISION':
                        pattern += $.jUtil.constants.format.SYS_DIVISION;
                        break;
                    case 'SPACE':
                        pattern += $.jUtil.constants.format.SPACE;
                        break;
                    default:
                        break;
                }
            }

            var regexp = new RegExp('[' + pattern + ']', 'g');

            if (regexp.test($element.val())) {
                alert(msg);
                if (focusTagId) {
                    $('#'+focusTagId).focus();
                }
            }
            $element.val($element.val().replace(regexp, ""));

            if (typeof fuc == 'function') {
                fuc($element.val());
            }
        });
        return this;
    };

    $.jUtil = {
        constants : {
            message : {
                    NUM                 : '숫자'
                ,   ENG                 : '영문'
                ,   ENG_LOWER           : '영문소문자'
                ,   ENG_UPPER           : '영문대문자'
                ,   KOR                 : '한글'
                ,   SPC_SCH             : '% \\'
                ,   SYS_DIVISION        : '∑'
                ,   SPC_1               : '특수문자 - _ / ? ! ~ ( ) & % [ ] + ! * , . '
                ,   SPC_2               : '특수문자 - _ ( )[ ] . '
                ,   SPC_3               : '특수문자 $ * @ .'
                ,   SPC_4               : '특수문자 - ☆ ★ ● ○ ▷ ◁ ♥ ♧ ※ ↑ ↓ → ←'
                ,   SPACE               : '띄워쓰기'
                ,   DOT                 : '.'
                ,   COMMA               : '특수문자 ,'
                ,   SHARP               : '특수문자 #'
            }
            ,format : {
                    NUM                 : '0-9'
                ,   ENG                 : 'a-zA-Z'
                ,   ENG_LOWER           : 'a-z'
                ,   ENG_UPPER           : 'A-Z'
                ,   KOR                 : 'ㄱ-ㅎㅏ-ㅣ가-힣'
                ,   SPC_SCH             : '%\\\\'
                ,   SPC_1               : '-_/?!~&%,\\.\\(\\)\\[\\]\\+\\*'
                ,   SPC_2               : '-_\\.\\(\\)\\[\\]'
                ,   SPC_3               : '$\\*\\@\\.'
                ,   SPC_4               : '\\☆\\★\\●\\○\\▷\\◁\\♥\\♧\\※\\↑\\↓\\→\\←'
                ,   SYS_DIVISION        : '∑'
                ,   SPACE               : '\\s'
                ,   DOT                 : '.'
                ,   COMMA               : ','
                ,   SHARP               : '#'
            }
        },
        /**
         * 팝업창
         * ex) $.jUtil.openNewPopup('/notice/channelNotice/partnerAddPopup', 840, 460);
         *
         * @param url
         * @param width
         * @param height
         */
        openNewPopup : function(url, width, height) {
            var popupNewWin = window.open(url, '_blank', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=yes,width='+width+',height='+height, '');
            if (popupNewWin == null) {
                alert("팝업 차단기능 혹은 팝업차단 프로그램이 동작중입니다. 팝업 차단 기능을 해제한 후 다시 시도하세요.");
            } else {
                var loc = window.location;
                var url = loc.protocol + "//" + loc.host + url;
                popupNewWin.location.replace(url);
                return popupNewWin;
            }
        },
        /**
         * 이미지 미리 보기 팝업창
         * @param imgUrl
         */
        imgPreviewPopup : function(imgUrl) {
            var img = new Image();

            if ($("#imgPreviewCheck").length > 0) {
                $("#imgPreviewCheck").html('<img src="'+imgUrl+'" onerror="alert(\'이미지가 존재하지 않습니다.\');" style="display: none;">');
            } else {
                $(document).find('body').append('<div id="imgPreviewCheck" ><img src="'+imgUrl+'" onerror="alert(\'이미지가 존재하지 않습니다.\');" style="display: none;"></div>');
            }

            img.onload = function(){
                var width = this.width+16;
                var height = this.height+16;
                $.jUtil.openNewPopup('/common/popup/previewImg?imgUrl='+imgUrl, width, height);
            };
            img.src = imgUrl;
        },
        /**
         * 입력값이 NULL, Space만으로만 이루어져 있는지 체크
         * @param val 입력값
         * @returns
         */
        isEmpty : function(val) {
            return (val == null || val == undefined || val == "" || typeof val == "string" && (val.replace(/\s/gi,"") == "") || (typeof val == "object" && !Object.keys(val).length)) ? true : false;
        },
        /**
         * 입력값이 NULL, Space 만으로만 이루어져 있지 않을 경우 체크
         * @param val 입력값
         * @returns {boolean}
         */
        isNotEmpty : function(val) {
            return !this.isEmpty(val);
        },
        /**
         * 입력값이 NULL, Space만으로만 이루어져 있는지 체크
         * @param val Object
         * @returns
         */
        isEmptyObjectArray : function(objList) {
            for(var i =0; i < objList.length; i++) {
                var value ="";
                if(typeof objList[i] == 'object') {
                    value = $(objList[i]).val();
                    if($.jUtil.isEmpty(value)){
                        $(objList[i]).focus();
                        return true;
                    }
                }
            }
        },
        /**
         * 입력값 유효성 체크
         *
         * @param val 입력값
         * @param types 체크타입 배열
         *  - NUM : 숫자
         *  - ENG : 영문
         *  - KOR : 한글
         *  - NOT_SPACE : 공백문자 허용 않함 (기본 공백허용)
         * @returns boolean true: 통과, false: 에러검출(허용된 이외의 문자가 포함되어있음)
         */
        isAllowInput : function(val, types) {
            if($.jUtil.isEmpty(val)) {
                return false;
            }
            var space = '\\s';
            var pattern = '';
            for(var i =0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'NUM':
                        pattern += $.jUtil.constants.format.NUM; break;
                    case 'ENG':
                        pattern += $.jUtil.constants.format.ENG; break;
                    case 'ENG_LOWER':
                        pattern += $.jUtil.constants.format.ENG_LOWER; break;
                    case 'ENG_UPPER':
                        pattern += $.jUtil.constants.format.ENG_UPPER; break;
                    case 'KOR':
                        pattern += $.jUtil.constants.format.KOR; break;
                    case 'SPC_1':
                        pattern += $.jUtil.constants.format.SPC_1; break;
                    case 'COMMA':
                        pattern += $.jUtil.constants.format.COMMA; break;
                    case 'SHARP':
                        pattern += $.jUtil.constants.format.SHARP; break;
                    case 'SPC_2':
                        pattern += $.jUtil.constants.format.SPC_2; break;
                    case 'SPC_3':
                        pattern += $.jUtil.constants.format.SPC_3; break;
                    case 'NOT_SPACE':
                        space = ''; break;
                    case 'SPC_4':
                        pattern += $.jUtil.constants.format.SPC_4; break;
                    default:
                        return false; break;
                }
            }

            var regexp = new RegExp('^[' + pattern + space + ']+$');

            return regexp.test(val);
        },
        isNotAllowInput : function(val, types) {
            if($.jUtil.isEmpty(val)) {
                return false;
            }
            var pattern = '';
            for (var i = 0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'SPC_SCH':
                        pattern += $.jUtil.constants.format.SPC_SCH;
                        break;
                    case 'SYS_DIVISION':
                        pattern += $.jUtil.constants.format.SYS_DIVISION;
                        break;
                    case 'SPACE':
                        pattern += $.jUtil.constants.format.SPACE;
                        break;
                    default:
                        break;
                }
            }

            var regexp = new RegExp('[' + pattern + ']+');

            return regexp.test(val);
        },
        getAllowInputMessage : function(types) {
            var msg = new Array();
            var tailMsg = '만 사용가능합니다.';
            for (var i = 0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'KOR':
                        msg.push($.jUtil.constants.message.KOR);
                        break;
                    case 'ENG':
                        msg.push($.jUtil.constants.message.ENG);
                        break
                    case 'ENG_LOWER':
                        msg.push($.jUtil.constants.message.ENG_LOWER);
                        break;
                    case 'NUM':
                        msg.push($.jUtil.constants.message.NUM);
                        break;
                    default:
                        return '정의되지 않은 체크타입 입니다.  type=' + types;
                }
            }

            return msg.join(', ') + tailMsg
        },
        getNotAllowInputMessage : function(types) {
            var msg = new Array();
            var tailMsg = '는 입력할 수 없습니다.';
            for (var i = 0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'SPC_SCH':
                        return $.jUtil.constants.message.SPC_SCH + "의 특수문자" + tailMsg;
                    case 'SYS_DIVISION':
                        msg.push($.jUtil.constants.message.SYS_DIVISION);
                        break;
                    case 'SPACE':
                        msg.push($.jUtil.constants.message.SPACE);
                        break;
                    default:
                        return '정의되지 않은 체크타입 입니다.  type=' + types;
                }
            }

            return msg.join('와 ') + tailMsg
        },
        /**
         * 퍼센트 입력 검사 (최대 100, 소숫점 이하 2자리까지)
         * @param val
         * @returns {boolean}
         */
        isPercentInput : function(val) {
            if(val == "") return false;
            var _pattern = /^(\d{1,3}([.]\d{0,2})?)?$/;
            if (!_pattern.test(val)) {
                return false;
            } else {
                if(Math.ceil(val) > 100) return false;
            }
            return true;
        },
        /***
         * 전화번호(휴대폰 번호) 하이픈 삽입
         * @param num
         * @returns string xxx-xxxx-xxxx
         */
        phoneFormatter : function (num){
            if(/^[0-9_-]*$/.test(num) == false){
                alert("전화번호에는 문자가 포함될수 없습니다");
                return;
            }

            var formatNum = '';
            num = num.replace(/[^0-9]/g, '');
            if(num.length==11){
                formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
            }
            else if(num.length==8){
                formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
            }else{
                if(num.indexOf('02')==0){
                    formatNum = num.replace(/(\d{2})(\d{3,4})(\d{4})/, '$1-$2-$3');
                }else{
                    formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                }
            }
            return formatNum;
        },
        /**
         * 휴대폰번호 유효성 검사
         * @param num
         * @param hyphenFlag
         * @returns boolean true/false
         */
        phoneValidate : function(num, hyphenFlag) {
            if (num) {
                if (hyphenFlag) {
                    if (/^01([0|1|6|7|8|9])-[0-9]{3,4}-[0-9]{4}$/.test(num)) {
                        return true;
                    }
                } else {
                    if (/^01([0|1|6|7|8|9])[0-9]{3,4}[0-9]{4}$/.test(num)) {
                        return true;
                    }
                }
            }
            return false;
        },
        /**
         * 서버 날짜 구하기
         * @returns {string} 오늘 날짜 YYYY-MM-DD 반환
         */
        getServerTime : function () {
            var xmlHttp;
            var url = window.location.href.toString();

            if(window.XMLHttpRequest){
                xmlHttp = new XMLHttpRequest();
            }else{
                return null;
            }
            xmlHttp.open('HEAD', url, false);  //헤더 정보만 받기 위해 HEAD방식으로 요청.
            xmlHttp.setRequestHeader("Content-Type", "text/html");
            xmlHttp.send('');

            var date = new Date(xmlHttp.getResponseHeader("Date"));
            return moment(date).format("YYYY-MM-DD HH:mm:ss")
        },
        /**
         * 오늘 날짜 구하기
         * param 이 있을때는 해당 날짜로 변환, 없을때는 서버시간으로 가져옴
         * @param date
         * @returns {string} 오늘 날짜 YYYY-MM-DD 반환
         */
        getCurrentDate : function (param) {
            var dt = "";
            if(param != undefined) {
                dt = param;
            }
            else {
                dt = new Date($.jUtil.getServerTime());
            }
            var y, m, d;
            y = dt.getFullYear();
            m = dt.getMonth() + 1;
            d = dt.getDate();
            m = m < 10 ? "0" + m : m;
            d = d < 10 ? "0" + d : d;
            return [y, m, d].join('-');
        },
        /**
         * 서버 날짜 구하기 IE 브라워저 겸용
         * @returns {string} 오늘 날짜 YYYY-MM-DD 반환
         */
        getServerTime2 : function () {
            var xmlHttp;
            var url = window.location.href.toString();

            if(window.XMLHttpRequest){
              xmlHttp = new XMLHttpRequest();
            }else{
              return null;
            }
            xmlHttp.open('HEAD', url, false);  //헤더 정보만 받기 위해 HEAD방식으로 요청.
            xmlHttp.setRequestHeader("Content-Type", "text/html");
            xmlHttp.send('');

            var date = new Date(xmlHttp.getResponseHeader("Date"));
            return moment(date).format("YYYY/MM/DD HH:mm:ss")
        },
        /**
         * 오늘 날짜 구하기 IE 브라워저 겸용
         * param 이 있을때는 해당 날짜로 변환, 없을때는 서버시간으로 가져옴
         * @param date
         * @returns {string} 오늘 날짜 YYYY-MM-DD 반환
         */
        getCurrentDate2 : function (param) {
            var dt = "";
            if(param != undefined) {
                var arr = param.split("-");
                dt = new Date(arr[0] + "/" + arr[1] + "/" + arr[2]);
            }
            else {
                dt = new Date($.jUtil.getServerTime2());
            }

            var y, m, d;
            y = dt.getFullYear();
            m = dt.getMonth() + 1;
            d = dt.getDate();
            m = m < 10 ? "0" + m : m;
            d = d < 10 ? "0" + d : d;
            return [y, m, d].join('-');
        },
        getCurrentTime : function(format){
            if(format){
                return moment(new Date).format(format);
            }
            return moment(new Date).format("YYYY-MM-DD HH:mm");
        },
        /**
         * 문자열 갈이 초과 여부
         * @param string input : 문자열
         * @param int maxLength : 최대길이
         * @returns {boolean} maxLenth 미만시 true, maxLenth 초과시 false
         */
        isValidStringLength : function (input, maxLength) {
            if (input.length > maxLength){
                return false;
            }
            return true;
        },
        /**
         * 시작일과 종료일 사이의 차이 계산
         *
         * @param startDt 시작일 문자열(2017-01-01)
         * @param endDt 종료일 문자열(2017-01-8)
         * @param unit(optional) 계산 단위(Y:년, M:월, D:일(기본))
         * @returns {number} 두 날짜 사이의 차이 수
         */
        diffDate : function (startDt, endDt, unit) {
            unit = (unit) ? unit.toUpperCase() : 'D';
            var startDateTime = new Date(startDt);
            var endDateTime = new Date(endDt);
            var day = 24 * 60 * 60 * 1000; // 시 * 분 * 초 * 밀리세컨드

            if (unit === 'Y') {
                day = day * 30 * 12; // 년 단위
            } else if (unit === 'M') {
                day = day * 30; // 월 단위
            }
            // date를 밀리초단위로 바꾼 뒤 두 객체를 빼서 day로 나눔. 정수로 반환받기 위해 Math.floor 적용
            return Math.floor((endDateTime.getTime() - startDateTime.getTime())/day);
        },
        /**
         * 시간 차이 계산
         * @param startDt 시작일 문자열(2017-01-01 00:00:00)
         * @param endDt 종료일 문자열(2017-01-08 23:59:59)
         * @returns {number} 두 날짜 사이의 시간 차이
         */
        diffTime : function (startDt, endDt) {
            var startDt = moment(startDt);
            var endDt = moment(endDt);
            return Math.abs(startDt.diff(endDt , 'hour'));
        },
        /**
         * 금맥 콤마 찍기
         * ex) $.jUtil.comma("1234567"); // 1,234,567
         * @param str
         * @returns {string}
         */
        comma : function (str) {
            str = String(str);
            return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
        },
        /**
         * Split Data Doc id 에 val 넣기
         * @param idNm
         * @param RegExp
         */
        valSplit : function(str, delimiter, _id) {
            if (!$.jUtil.isEmpty(str)) {
                let tmp = str.split(delimiter);

                tmp.forEach(function (item, index, array) {
                    $('#' + _id + '_' + (index + 1)).val(item);
                });
            }
        },
        valNotSplit : function(_id, delimiter) {
            let str = '';
            let isEmpty = false;

            $('[id^=' + _id + '_]').each(function (index) {
                $(this).val() == "" ? isEmpty = true : isEmpty = false;
                if (index > 0) {
                    str += delimiter;
                }
                str += $(this).val();
            });

            if (isEmpty) {
                str = '';
            }

            return str;
        },
        /**
         * obj 사이즈
         * ex) $.jUtil.objSize(checkObj);
         *
         * @param obj
         * @returns {number}
         */
        objSize : function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        },
        /**
         * html clear 처리 (multi)
         * ex) $.jUtil.clearHtmlAll(['#checklistArea', '#itemArea', '#rejectReasonArea']);
         *
         * @param ids
         */
        clearHtmlAll : function (ids) {
            var selector = ids.join(', ');
            $(selector).html("");
        },
        /**
         * hide 처리 (multi)
         * ex) $.jUtil.hideAll(['#checklistArea', '#itemArea', '#rejectReasonArea']);
         *
         * @param ids
         */
        hideAll : function (ids) {
            var selector = ids.join(', ');
            $(selector).hide();
        },
        /**
         * 이벤트 바인딩
         * @param obj
         * @param action
         * @param func
         * @param param
         * @returns {*}
         */
        bindEvent : function (obj, action, func, param) {
            if (typeof func == 'function') {
                obj.each(function() {
                    var $element = $(this);
                    $element.on(action, function() {
                        if(param != null) {
                            func(param, $element[0]);
                        } else {
                            func($element[0]);
                        }
                    });
                });
            }
            return obj;
        },
        exceptReplace : function (obj, types, replaceValue){
            if(!obj) {
                return;
            }

            var pattern = '';
            for(var i =0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'NUM':
                        pattern += $.jUtil.constants.format.NUM; break;
                    case 'ENG':
                        pattern += $.jUtil.constants.format.ENG; break;
                    case 'ENG_LOWER':
                        pattern += $.jUtil.constants.format.ENG_LOWER; break;
                    case 'ENG_UPPER':
                        pattern += $.jUtil.constants.format.ENG_UPPER; break;
                    case 'KOR':
                        pattern += $.jUtil.constants.format.KOR; break;
                    case 'SPC_SCH':
                        pattern += $.jUtil.constants.format.SPC_SCH; break;
                    case 'SYS_DIVISION':
                        pattern += $.jUtil.constants.format.SYS_DIVISION; break;
                }
            }

            var regexp = new RegExp('[^' + pattern + ']', 'g');
            obj.val(obj.val().replace(regexp, replaceValue));
        },
        patternReplace : function (obj, types, replaceValue){
            if(!obj) {
                return;
            }

            var pattern = '';
            for(var i =0; i < types.length; i++) {
                switch (types[i].toUpperCase()) {
                    case 'NUM':
                        pattern += $.jUtil.constants.format.NUM; break;
                    case 'ENG':
                        pattern += $.jUtil.constants.format.ENG; break;
                    case 'ENG_LOWER':
                        pattern += $.jUtil.constants.format.ENG_LOWER; break;
                    case 'ENG_UPPER':
                        pattern += $.jUtil.constants.format.ENG_UPPER; break;
                    case 'KOR':
                        pattern += $.jUtil.constants.format.KOR; break;
                    case 'SPC_SCH':
                        pattern += $.jUtil.constants.format.SPC_SCH; break;
                    case 'SYS_DIVISION':
                        pattern += $.jUtil.constants.format.SYS_DIVISION; break;
                }
            }

            var regexp = new RegExp('[' + pattern + ']', 'g');
            obj.val(obj.val().replace(regexp, replaceValue));
        },
        getHttpParam : function (name) {
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null) {
                return '';
            } else {
                return results[1];
            }
        },
        /**
         * 유효성 검사 얼럿
         * @param msg
         * @returns {boolean}
         */
        alert : function(msg, focusId) {
            alert(msg);
            if (focusId) {
                $('#'+focusId).focus();
            }
            return false;
        },
        downloadFile : function(_url, _fileNm , _processKey) {
            let url = "/provide/view?processKey=" + _processKey + "&fileId=" + _url;

            let downloadIframe =  '<iframe id="iframeFileDownload" name="iframeFileDownload" style="display:none"></iframe>'
                    + '<form id="fileDownload" name="fileDownload" action="/common/getUrlDownloadFile" method="POST" target="iframeFileDownload">'
                    + '<input type="hidden" id="fileDownUrl" name="fileUrl" value="' + url + '" />'
                    + '<input type="hidden" id="fileDownName" name="fileName" value="' + _fileNm + '" />'
                    + '</form>';

            if ($('#fileDownload').length) {
                $('#fileDownUrl').val(url);
                $('#fileDownName').val(_fileNm);
            } else {
                $('body').append(downloadIframe);
            }

            $('#fileDownload').submit();
        },
        downloadFiles : function(_files) {
            if (_files.length > 0) {
                let file = _files.shift(0); //앞에서부터 하나씩 get & remove
                $.jUtil.downloadFile(file.url, file.fileNm, file.processKey);
                if (_files.length > 0) {
                    window.setTimeout(function () { $.jUtil.downloadFiles(_files) }, 1000);
                }
            }
        },
        /**
         * 원본 문자열이 null 이거나 빈 문자열("") 경우 대체할 문자열을 리턴하고, 아닐 경우 원본 문자열을 리턴합니다.
         * @param str 원본 문자열
         * @param newStr 대체할 문자열
         * @returns {String}
         */
        nvl: function (str, newStr) {
            if ($.jUtil.isEmpty(str)) {
                return $.jUtil.isEmpty(newStr) ? newStr : "";
            }
            return str;
        }
    }
})(jQuery);
