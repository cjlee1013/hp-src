/**
 * 공통으로 사용할 Calendar 관련 함수들을 정리.
 * CALENDAR 의 수정을 통해 사용자에게 제공할 Calendar 함수의 추가 및 수정 가능.
 * 추후 jquery 가 아닌 다른 모듈을 사용할 경우 CALENDAR 만 교체하여 사용 가능.
 */

// jquery 1.9 에서 삭제된 browser 에 의한 에러 처리를 위한 부분.
jQuery.browser = {};
(function () {
    document.documentElement.focus(); //IE9 - SCRIPT16389 error 방지
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
// jquery 1.9 에서 삭제된 browser 에 의한 에러 처리를 위한 부분.

// CALENDAR 디폴트.
var CALENDAR = function(){
    var dateFormat = "yy-mm-dd";
    var defaultConfig = {
        closeText : '닫기',
        prevText : '이전달',
        nextText : '다음달',
        showOn: "button",
        buttonImage : '/static/images/ui-kit/ui-input-calendar-sm.png',
        buttonImageOnly: false,
        buttonText: "날짜를 선택하세요",
        currentText : '오늘',
        monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames : ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
        weekHeader : 'Wk',
        dateFormat : dateFormat,
        firstDay : 0,
        isRTL : false,
        showMonthAfterYear : true,
        yearSuffix : '년'
    };

    var getDateFormat = function(){
        return dateFormat;
    }

    var getDefaultConfig = function(){
        return $.extend({}, defaultConfig);
    }

    var getDate = function( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( 'yy-mm-dd', element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    };

    // 단독 캘린더의 날짜 설정. date 에는 '0'이면 오늘 + 값이면 미래 - 값이면 과거 일자로 되돌림.
    var setCalDate = function(id, date){
        date = date.toString();
        var currentObj = $("#" + id);
        var disabled = currentObj.datepicker('option', 'disabled');
        if (disabled) {
            currentObj.datepicker('option', 'disabled', false);
        }
        currentObj.datepicker('option', 'minDate', null);
        currentObj.datepicker('option', 'maxDate', null);
        currentObj.datepicker('setDate', date);
        if (disabled) {
            currentObj.datepicker('option', 'disabled', true);
        }
    };

    var setRangeCalDate = function(startDateId, endDateId, date){
        date = date.toString();
        var currentDate = new Date();
        var targetDate = $.datepicker._determineDate(currentDate, date);
        if( currentDate > targetDate ) {
            setCalDate(startDateId, date);
            setCalDate(endDateId, '0');
        }else{
            setCalDate(startDateId, '0');
            setCalDate(endDateId, date);
        }
    }

    var setTomorrow = function(startDateId, endDateId){ //내일 : 내일~내일
        setCalDate(startDateId, '+1d');
        setCalDate(endDateId, '+1d');
    }

    var setYesterday = function(startDateId, endDateId){ //어제 : 어제~어제
        setCalDate(startDateId, '-1d');
        setCalDate(endDateId, '-1d');
    }

    var setMinDate = function (obj, date) {
        date = date.toString();
        var disabled = obj.datepicker('option', 'disabled');
        if (disabled) {
            obj.datepicker('option', 'disabled', false);
        }
        obj.datepicker('option', 'minDate', date);
        if (disabled) {
            obj.datepicker('option', 'disabled', true);
        }
    }
    var setMaxDate = function (obj, date) {
        date = date.toString();
        var disabled = obj.datepicker('option', 'disabled');
        if (disabled) {
            obj.datepicker('option', 'disabled', false);
        }
        obj.datepicker('option', 'maxDate', date);
        if (disabled) {
            obj.datepicker('option', 'disabled', true);
        }
    }

    // 한개의 캘린더만 사용할 경우 추가.
    var datePicker = function(id, config){
        var obj = $("#" + id).datepicker($.extend(this.getDefaultConfig(), config))
            .on("change", function () {
                checkDate(this);
            });
        return {
            setDate: function(date){
                setCalDate(id, date);
            },
            setMinDate: function(date){
                setMinDate(obj, date);
            },
            setMaxDate: function(date){
                setMaxDate(obj,date);
            },
        };
    }

    // 기간 검색에 사용할 캘린더 추가.
    var datePickerRange = function(startId, endId, config){
        var rangeConfig = $.extend(this.getDefaultConfig(), {
                changeMonth: true,
                numberOfMonths: 1
            }, config);
        var from = $( "#" + startId ).datepicker(rangeConfig)
            .on( "change", function() {
                checkDate(this);
                to.datepicker( "option", "minDate", getDate( this ) );
            });
        var to = $( "#" + endId ).datepicker(rangeConfig)
            .on( "change", function() {
                checkDate(this);
                from.datepicker( "option", "maxDate", getDate( this ) );
            });

        return {
            setDate: function(date){
                setRangeCalDate(startId, endId, date);
            },
            setStartDate: function(date){
                setCalDate(startId, date);
            },
            setEndDate: function(date){
                setCalDate(endId, date);
            },
            setMinDate: function(date){
                setMinDate(from, date);
                setMinDate(to, date);
            },
            setMaxDate: function(date){
                setMaxDate(from, date);
                setMaxDate(to, date);
            },
        }
    }

    // 기간 검색에 사용할 캘린더 추가.
    var dateBasicPicker = function(startId, endId, config){
        var rangeConfig = $.extend(this.getDefaultConfig(), {
            changeMonth: true,
            numberOfMonths: 1
        }, config);
        var from = $( "#" + startId ).datepicker(rangeConfig)
            .on( "change", function() {
                to.datepicker( "option", "minDate", getDate( this ) );
            });
        var to = $( "#" + endId ).datepicker(rangeConfig)
            .on( "change", function() {
                from.datepicker( "option", "maxDate", getDate( this ) );
            });
        return {
            setDate: function(date){
                setRangeCalDate(startId, endId, date);
            },
            setStartDate: function(date){
                setCalDate(startId, date);
            },
            setEndDate: function(date){
                setCalDate(endId, date);
            },
            setMinDate: function(date){
                setMinDate(from, date);
                setMinDate(to, date);
            },
            setMaxDate: function(date){
                setMaxDate(from, date);
                setMaxDate(to, date);
            },
        }
    }

    //입력된 날짜 체크(형식, 날짜 존재여부)
    var checkDate = function (element) {
        try {
            $.datepicker.parseDate('yy-mm-dd', element.value);
        } catch (error) {
            if (error == "Invalid date") {
                alert(element.value + " 일은 존재하지 않는 날짜입니다.");
                element.value = null;

            } else {
                element.value = null;
                alert("올바른 형식(YYYY-MM-DD)이 아닙니다.");
                element.focus();
            }
        }
    };

    // Calendar 를 통해 제공할 기능들만 노출.
    return {
        getDateFormat: getDateFormat,
        getDefaultConfig: getDefaultConfig,
        getDate: getDate,
        setCalDate: setCalDate,
        setRangeCalDate: setRangeCalDate,
        setMinDate: setMinDate,
        setMaxDate: setMaxDate,
        datePicker: datePicker,
        datePickerRange: datePickerRange,
        dateBasicPicker: dateBasicPicker,
        setTomorrow: setTomorrow,
        setYesterday: setYesterday,
    }
};

// 실제 사용할 Calendar 변수 생성.
var Calendar = CALENDAR();