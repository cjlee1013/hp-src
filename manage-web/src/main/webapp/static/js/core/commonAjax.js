/**
 * 공통 Ajax
 *
 CommonAjax.basic({url:'/sample/callAjax', data:{id : '123'}, method:'post', successMsg:'완료 되었습니다.', callbackFunc:function() {alert('postData - func()')}});
 CommonAjax.basic({
			url                 : 'test.json'
		,   data                : {id : '123'}
		,   method              : 'post'
		,   successMsg          : '완료 되었습니다.'
		,   callbackFunc        : function() {alert('callbackFunc()')}
		,   errorCallbackFunc   : function() {alert('errorCallbackFunc()')}
	});
 */
var CommonAjax = {
    basic: function (params) {
        if (params.url == null) {
            alert(CommonErrorMsg.urlErrorMsg);
            return false;
        }

        if (params.method == null) {
            alert(CommonErrorMsg.methodErrorMsg);
            return false;
        }
        var async = true;
        if (params.async !== undefined) {
            async = params.async;
        }

        var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
        if (params.contentType !== undefined && params.contentType != null) {

            contentType = params.contentType;
        }

        var cache = true;
        if (params.cache !== undefined) {
            cache = params.cache;
        }

        $.ajax({
            url: params.url,
            data: params.data,
            method: params.method,
            contentType: contentType,
            dataType: 'json',
            async: async,
            cache: cache,
            success: function (resData) {
                // 정의한 성공메세지 alert 노출
                if (params.successMsg != null) {
                    alert(params.successMsg);
                }
                // 정의한 callback function 이 존재하면 해당 function 호출
                if (params.callbackFunc != null && typeof params.callbackFunc === 'function') {
                    params.callbackFunc(resData);
                }
            },
            error: function (resError) {
                // 로그인 인증 실패 시 httpStatus(401)와 returnCode(-9201)를 확인하여 로그인 페이지 팝업을 오픈 합니다.
                if (resError.status == 401 && resError.responseJSON.returnCode == -9201) {
                    alert(CommonErrorMsg.sessionErrorMsg);
                    windowPopupOpen("/login/popup", "loginPop", 400, 481);
                // 정의한 errorCallback function 이 존재하면 해당 function 호출
                } else if (params.errorCallbackFunc != null && typeof params.errorCallbackFunc === 'function') {
                    params.errorCallbackFunc(resError);
                // 에러메세지 alert 노출
                } else {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.returnMessage != null) {
                            // api 에러메세지
                            alert(resError.responseJSON.returnMessage);
                        } else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    } else if (resError.status !== 200) {
                        alert(CommonErrorMsg.dfErrorMsg);
                    }
                }
            },
            fail: function () {
                alert(CommonErrorMsg.failMsg);
            }
        });
    },
    /**
     * 공통 이미지/파일 업로드
     * @param params
     * @param params.data.mode : IMG: 이미지, FILE: 파일
     * @returns {boolean}
     */
    upload: function (params) {
        if (!params.file) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }

        if (!params.data.processKey) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }

        if ($.jUtil.isEmpty(params.file.val())) {
            return false;
        }

        var $uploadForm = $("<form></form>");

        $uploadForm.append(params.file);
        $uploadForm.append(
            '<input type="hidden" name="processKey" value="' + params.data.processKey
            + '">');
        if (params.data.mode) {
            $uploadForm.append(
                '<input type="hidden" name="mode" value="' + params.data.mode
                + '">');
        }
        if (params.data.baseKeyCd) {
            $uploadForm.append('<input type="hidden" name="baseKeyCd" value="'
                + params.data.baseKeyCd + '">');
        }
        if (params.data.fileName) {
            $uploadForm.append('<input type="hidden" name="fileName" value="'
                + params.data.fileName + '">');
        }
        if (params.data.fieldName) {
            $uploadForm.append(
                    '<input type="hidden" name="fieldName" value="' + params.data.fieldName
                    + '">');
        }

        $uploadForm.ajaxForm({
            type: "POST",
            url: "/common/upload.json",
            success: function (resData) {
                if (params.successMsg) {
                    alert(params.successMsg);
                }

                if (params.callbackFunc && typeof params.callbackFunc
                    == 'function') {
                    params.callbackFunc(resData);
                }
            },
            error: function (resError) {
                // SizeLimitExceededException 발생 시 httpStatus(413)를 확인하여 alert 노출
                if (resError.status == 413) {
                    alert(CommonErrorMsg.uploadPermittedSizeErrorMsg);
                } else if (params.errorCallbackFunc
                    && typeof params.errorCallbackFunc == 'function') {
                    params.errorCallbackFunc(resError);
                } else {
                    console.log(resError.responseJSON);
                    // df 에러메세지
                    alert(CommonErrorMsg.dfErrorMsg);
                }
            },
            fail: function () {
                alert(CommonErrorMsg.failMsg);
            },
            complete: function() {
                $('body').append(params.file);
                params.file.val("");
            }
        }).submit();
    },
    /**
     * 이미지 업로드 Form - IE 브라우저에서 파일업로드 시 사용
     * @param params
     * @returns {boolean}
     */
    uploadImageForm: function (params) {
        if (!params.file) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }
        if (!params.data.processKey) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }
        params.file.ajaxForm({
            type: "POST",
            url: "/common/uploadImageIE.json",
            dataType: "json",
            data: {
                processKey: params.data.processKey,
                mode: params.data.mode,
                baseKeyCd: params.data.baseKeyCd,
                fileName: params.file.find('input[type=file]').attr('name')
            },
            success: function (resData) {
                if (params.successMsg) {
                    alert(params.successMsg);
                }

                if (params.callbackFunc && typeof params.callbackFunc
                    == 'function') {
                    params.callbackFunc(resData);
                }
            },
            error: function (resError) {
                // SizeLimitExceededException 발생 시 httpStatus(413)를 확인하여 alert 노출
                if (resError.status == 413) {
                    alert(CommonErrorMsg.uploadPermittedSizeErrorMsg);
                } else if (params.errorCallbackFunc
                    && typeof params.errorCallbackFunc == 'function') {
                    params.errorCallbackFunc(resError);
                } else {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.errors[0].detail != null) {
                            // api 에러메세지
                            alert(resError.responseJSON.errors[0].detail);
                        } else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    }
                }
            },
            fail: function () {
                alert(CommonErrorMsg.failMsg);
            }
        }).submit();
    },
    /**
     * 공통 파일 업로드
     * @param params
     * @returns {boolean}
     */
    uploadFile: function (params) {
        if (!params.file) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }
        if (!params.data.processKey) {
            alert(CommonErrorMsg.paramErrorMsg);
            return false;
        }

        var $uploadForm = $("<form></form>");
        params.file.clone(true).appendTo($uploadForm);
        params.file.val('');
        $uploadForm.append(
            '<input type="hidden" name="processKey" value="' + params.data.processKey
            + '">');
        if (params.data.mode) {
            $uploadForm.append(
                '<input type="hidden" name="mode" value="' + params.data.mode
                + '">');
        }
        if (params.data.baseKeyCd) {
            $uploadForm.append('<input type="hidden" name="baseKeyCd" value="'
                + params.data.baseKeyCd + '">');
        }
        if (params.data.fileFieldName) {
            $uploadForm.append(
                '<input type="hidden" name="fileFieldName" value="'
                + params.data.fileFieldName + '">');
        }

        $uploadForm.ajaxForm({
            type: "POST",
            url: "/common/uploadFile.json",
            success: function (resData) {
                if (params.successMsg) {
                    alert(params.successMsg);
                }

                if (params.callbackFunc && typeof params.callbackFunc
                    == 'function') {
                    params.callbackFunc(resData);
                }
            },
            error: function (resError) {
                // SizeLimitExceededException 발생 시 httpStatus(413)를 확인하여 alert 노출
                if (resError.status == 413) {
                    alert(CommonErrorMsg.uploadPermittedSizeErrorMsg);
                } else if (params.errorCallbackFunc
                    && typeof params.errorCallbackFunc == 'function') {
                    params.errorCallbackFunc(resError);
                } else {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.errors[0].detail != null) {
                            // api 에러메세지
                            alert(resError.responseJSON.errors[0].detail);
                        } else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    }
                }
            },
            fail: function () {
                alert(CommonErrorMsg.failMsg);
            }
        }).submit();
    }
};

/**
 * 에러메세지 정의
 */
var CommonErrorMsg = {
    urlErrorMsg: '호출 URL을 설정해 주세요. '
    , methodErrorMsg: '호출 method를 설정해 주세요. '
    , failMsg: '재시도 해주십시오.'
    , nullErrorMsg: '정상적으로 처리되지 않았습니다. 재시도 해주십시오. (null)'
    , dfErrorMsg: '정상적으로 처리되지 않았습니다. 재시도 해주십시오.'
    , sessionErrorMsg: '로그인 세션이 만료되었습니다. 다시 로그인 하시기 바랍니다.'
    , paramErrorMsg: '필수 파라미터가 누락되었습니다.'
    , uploadPermittedSizeErrorMsg: '용량이 초과되었습니다.'
    , uploadFailMsg: '파일 양식이 잘못되었거나 오류로 반영에 실패했습니다.'
};

/**
 * ajax 실행 시 blockUI 적용하는
 * ajaxStart시 blockUI가 적용 되고, ajaxStop시 blockUI가 해제 된다.
 *
 * 사용 예제)
 *  1. 전역 : 적용할 페이지에 CommonAjaxBlockUI.global(); 추가
 *    $(document).ready(function() {
 *       CommonAjaxBlockUI.global();
 *  });
 *
 *  2. 일부 ID만 사용 : 적용할 페이지에 CommonAjaxBlockUI.targetId('대상 id'); 추가
 *    $(document).ready(function() {
 *       CommonAjaxBlockUI.targetId('submitProdInfoSrchBtn');
 *  });*
 *
 *  3. 그리드 목록 행 선택 시(onRowSelect) 적용 : 대상 그리드의 id 추가
 *  $(document).ready(function() {
 *       CommonAjaxBlockUI.gridTargetId('harmGrid');
 *  });
 *
 * @type {{init: CommonAjaxBlockUI.init}}
 */
var CommonAjaxBlockUI = {
    loadingImage: '<img src="/static/images/loading.gif" width="80px" height="80px"/>',
    //전역으로 ajax 발생시 적용
    global: function () {
        $(document).ajaxStart(function () {
            $.blockUI({
                message: CommonAjaxBlockUI.loadingImage,
                css: {
                    border: 'none',
                    backgroundColor: 'none',
                    color: 'none',
                },
                overlayCSS: {
                    backgroundColor: '#cccccc',
                    opacity: 0.6,
                    cursor: 'wait'
                },
            });
        }).ajaxStop(function () {
            $.unblockUI({fadeOut: 100});
        });
    },
    //이벤트 대상 ID에 지정(버튼 등)
    targetId: function (id) {
        $(document).ajaxSend(function (event, xhr, options) {
            const targetId = event.currentTarget.activeElement.id;
            if (id == targetId) {
                $.blockUI({
                    message: CommonAjaxBlockUI.loadingImage,
                    css: {
                        border: 'none',
                        backgroundColor: 'none',
                        color: 'none',
                    },
                    overlayCSS: {
                        backgroundColor: '#cccccc',
                        opacity: 0.6,
                        cursor: 'wait'
                    },
                });
            }
        }).ajaxComplete(function (event) {
            const targetId = event.currentTarget.activeElement.id;
            if (id == targetId) {
                $.unblockUI({fadeOut: 100});
            }
        });
    },
    //그리드 목록 클릭시 적용
    //TODO 추후 개선
    gridTargetId: function (id) {
        $("#" + id + " > div.objbox > table > tbody").on("click", 'tr',
            function (event) {
                //그리드 클릭시 검색 결과가 없는 경우는 제외 함("n_emptyList")
                if (!$(event.currentTarget).children().hasClass(
                    "n_emptyList")) {
                    $.blockUI({
                        message: CommonAjaxBlockUI.loadingImage,
                        css: {
                            border: 'none',
                            backgroundColor: 'none',
                            color: 'none',
                        },
                        overlayCSS: {
                            backgroundColor: '#cccccc',
                            opacity: 0.6,
                            cursor: 'wait'
                        },
                        fadeOut: 100
                    });
                }
            });
        $(document).ajaxStop(function (event) {
            if (!$(event.currentTarget).children().hasClass("n_emptyList")) {
                $.unblockUI({fadeOut: 100});
            }
        });
    },
    //blockUI 수동 실행 : block ui 설정이 필요한 부분(ex.특정 함수 시작, 통신 시작 전)에서 선언한다 CommonAjaxBlockUI.startBlockUI();
    startBlockUI: function () {
        $.blockUI({
            message: CommonAjaxBlockUI.loadingImage,
            css: {
                border: 'none',
                backgroundColor: 'none',
                color: 'none',
            },
            overlayCSS: {
                backgroundColor: '#cccccc',
                opacity: 0.6,
                cursor: 'wait'
            },
        });
    },
    //blockUI 수동 실행 종료 : block ui 설정을 해제(ex.특정 함수 종료, 통신 종료 후)할 때 선언한다 CommonAjaxBlockUI.stopBlockUI();
    stopBlockUI: function () {
        $.unblockUI({fadeOut: 100});
    }
};
