/**
 * 공통으로 사용할 CalendarTime 관련 함수들을 정리.
 * CALENDAR_TIME 의 수정을 통해 사용자에게 제공할 Calendar 함수의 추가 및 수정 가능.
 * 추후 jquery 가 아닌 다른 모듈을 사용할 경우 CALENDAR_TIME 만 교체하여 사용 가능.
 */

// jquery 1.9 에서 삭제된 browser 에 의한 에러 처리를 위한 부분.
jQuery.browser = {};
(function () {
    document.documentElement.focus(); //IE9 - SCRIPT16389 error 방지
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
// jquery 1.9 에서 삭제된 browser 에 의한 에러 처리를 위한 부분.

// CALENDAR 디폴트.
var CALENDAR_TIME = function(){
    var dateFormat = 'yy-mm-dd';
    var defaultConfig = {
        closeText : '닫기',
        prevText : '이전달',
        nextText : '다음달',
        showOn: 'button',
        buttonImage : '/static/images/ui-kit/ui-input-calendar-sm.png', //ui개선작업 달력 이미지 공통화
        buttonImageOnly: false,
        buttonText: '날짜를 선택하세요',
        currentText : '오늘',
        monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames : ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
        weekHeader : 'Wk',
        dateFormat : dateFormat,
        firstDay : 0,
        isRTL : false,
        showMonthAfterYear : true,
        yearSuffix : '년',
        controlType : 'select',
        showTime : true,
        timeFormat: 'HH:mm:ss',
        timeSuffix: '',
        timeOnlyTitle: 'Choose Time',
        timeText: '시간',
        oneLine: true,
        changeMonth: true,
        hour: 0,
        minute: 0,
    };

    var getDateFormat = function(){
        return dateFormat;
    }

    var getDefaultConfig = function(){
        return $.extend({}, defaultConfig);
    }

    var getDate = function( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( 'yy-mm-dd', element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    };

    var replaceAll = function(str, target, replace){
        var result = str;
        while( result.indexOf(target) >= 0 ){
            result = result.replace(target, replace);
        }
        return result;
    };

    var padding = function(n, width) {
        n = n + '';
        if (n.length < 2) {
            return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
        } else {
            return n;
        }
    };

    // 단독 캘린더의 날짜 설정. date 에는 0이면 오늘 + 값이면 미래 - 값이면 과거 일자로 되돌림.
    var setCalDate = function(id, date){
        date = date.toString();
        var resultDate = date;
        var currentObj = $('#' + id);

        var hour =  currentObj.datetimepicker('option', 'hour');
        var minute = currentObj.datetimepicker('option', 'minute');
        var dateTimeFormat = 'YYYY-MM-DD ' + currentObj.datetimepicker('option', 'timeFormat');

        if (date.split('-').length > 2) {
            // 년월일을 - 를 구분자로 입력한 경우.
            resultDate = moment(date, 'YYYY-MM-DD HH:mm:ss').format(dateTimeFormat);
        } else {
            // -3d, -2w, 4m 등으로 입력한 경우 처리.
            var targetDate = $.datepicker._determineDate(new Date(), date);
            resultDate = moment(targetDate).format(dateTimeFormat);
        }

        if(resultDate.indexOf(" 00") && hour > 0){
            resultDate = resultDate.replace(" 00", " "+ padding(hour, 2));
        }
        if(resultDate.indexOf(":00") && minute > 0){
            resultDate = resultDate.replace(":00", ":"+ padding(minute, 2));
        }

        var disabled = currentObj.attr('disabled') == 'disabled';
        if (disabled) {
            currentObj.datetimepicker('enable');
        }
        currentObj.val(resultDate);
        if (disabled) {
            currentObj.datetimepicker('disable');
        }
    };

    var setRangeCalDate = function(startDateId, endDateId, date){
        date = date.toString();
        var currentDate = new Date();
        var targetDate = $.datepicker._determineDate(currentDate, date);
        var startObj = $('#'+startDateId);
        var endObj = $('#'+endDateId);

        startObj.datepicker('option', 'minDate', null);
        startObj.datepicker('option', 'maxDate', null);
        endObj.datepicker('option', 'minDate', null);
        endObj.datepicker('option', 'maxDate', null);

        if( currentDate > targetDate ) {
            setCalDate(startDateId, date);
            setCalDate(endDateId, '0');
        }else{
            setCalDate(startDateId, '0');
            setCalDate(endDateId, date);
        }
    }

    var setMinDate = function (obj, date) {
        date = date.toString();
        var disabled = obj.attr('disabled') == 'disabled';
        if (disabled) {
            obj.datetimepicker('enable');
        }
        obj.datetimepicker('option', 'minDate', date);
        if (disabled) {
            obj.datetimepicker('disable');
        }
    }
    var setMaxDate = function (obj, date) {
        date = date.toString();
        var disabled = obj.attr('disabled') == 'disabled';
        if (disabled) {
            obj.datetimepicker('enable');
        }
        obj.datetimepicker('option', 'maxDate', date);
        if (disabled) {
            obj.datetimepicker('disable');
        }
    }

    // 한개의 캘린더만 사용할 경우 추가.
    var datePicker = function(id, config){
        var obj = $('#' + id).datetimepicker($.extend(this.getDefaultConfig(), config));
        return {
            setDate: function(date){
                setCalDate(id, date);
            },
            setMinDate: function(date){
                setMinDate(obj, date);
            },
            setMaxDate: function(date){
                setMaxDate(obj,date);
            },
        };
    }

    // 기간 검색에 사용할 캘린더 추가.
    var datePickerRange = function(startId, endId, timeFormat, notClose, endTimeFormat, disabledMinute){
        var startDateTextBox = $('#' + startId);
        var endDateTextBox = $('#' + endId);
        if (!endTimeFormat) {endTimeFormat = timeFormat;}

        // 시작 일자 설정.
        var startConfig = $.extend(this.getDefaultConfig(), timeFormat);
        // 종료 일자 설정.
        var endConfig = $.extend(this.getDefaultConfig(), endTimeFormat);

        if (disabledMinute) {
            startConfig.onSelect = function (selectedDateTime){
                var endDt = endDateTextBox.val();

                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );

                if (moment(endDt).diff(startDateTextBox.val(), "day", true) < 0) {
                    endDateTextBox.val(moment(startDateTextBox.val()).format('YYYY-MM-DD ' + moment(endDateTextBox.val()).format(endTimeFormat.timeFormat)));
                } else {
                    endDateTextBox.val(endDt);
                }

                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(0);
                }, 0);
            }

            endConfig.onSelect = function (selectedDateTime){
                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(59);
                }, 0);
            }

            startConfig.onChangeMonthYear = function() {
                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(0);
                }, 0);
            }

            endConfig.onChangeMonthYear = function() {
                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(59);
                }, 0);
            }

            startConfig.beforeShow = function() {
                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(0);
                }, 0);
            }

            endConfig.beforeShow = function() {
                setTimeout(function () {
                    $('#ui-datepicker-div .ui_tpicker_minute, #ui-datepicker-div .ui_tpicker_minute_slider').removeClass('ui_tpicker_unit_hide');
                    $('#ui-datepicker-div .ui_tpicker_minute_slider .ui-timepicker-select').prop('disabled', true).val(59);
                }, 0);
            }
        } else {
            startConfig.onSelect = function (selectedDateTime){
                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
            }

            endConfig.onSelect = function (selectedDateTime){
                startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
            }
        }

        if (!notClose) {
            startConfig.onClose = function(dateText, inst) {
                if (endDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        endDateTextBox.datetimepicker('setDate', testStartDate);
                }
                else {
                    endDateTextBox.val(dateText);
                }
            }

            endConfig.onClose = function(dateText, inst) {
                if (startDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        startDateTextBox.datetimepicker('setDate', testEndDate);
                }
                else {
                    startDateTextBox.val(dateText);
                }
            }
        }

        var start = $('#' + startId).datetimepicker(startConfig)
            .on('change', function() {
                checkDateTime(this, startConfig);
            });
        var end = $('#' + endId).datetimepicker(endConfig)
            .on('change', function() {
                checkDateTime(this, endConfig);
            });

        return {
            setDate: function(date){
                setRangeCalDate(startId, endId, date);
            },
            setStartDate: function(date){
                setCalDate(startId, date);
            },
            setEndDate: function(date){
                setCalDate(endId, date);
            },
            setMinDate: function(date){
                setMinDate(start, date);
                setMinDate(end, date);
            },
            setMaxDate: function(date){
                setMaxDate(start, date);
                setMaxDate(end, date);
            },
            setEndDateByTimeStamp: function (date) {
                let resultDate;
                if (date === 0) {
                    // 기존 사용법과 동일하게 0일 경우 오늘 날짜를 사용.
                    resultDate = new Date().toISOString().substring(0, 10);
                } else {
                    // -3d, -2w, 4m 등으로 입력한 경우 처리.
                    resultDate = moment($.datepicker._determineDate(new Date(), date)).format('YYYY-MM-DD');
                }
                setCalDate(endId, resultDate + ' 23:59:59');
            },
        }
    }

    //입력된 날짜 체크(형식, 날짜 존재여부)
    var checkDateTime = function (element, config) {
        try {
            $.datepicker.parseDateTime('yy-mm-dd', config.timeFormat, element.value);
        } catch (error) {
            if (error == "Invalid date") {
                alert(element.value + " 일은 존재하지 않는 날짜입니다.");
                element.value = null;

            } else {
                element.value = null;
                alert("올바른 형식(YYYY-MM-DD "+config.timeFormat+")이 아닙니다.");
                element.focus();
            }
        }
    };

    // Calendar 를 통해 제공할 기능들만 노출.
    return {
        getDateFormat: getDateFormat,
        getDefaultConfig: getDefaultConfig,
        getDate: getDate,
        setCalDate: setCalDate,
        setRangeCalDate: setRangeCalDate,
        setMinDate: setMinDate,
        setMaxDate: setMaxDate,
        datePicker: datePicker,
        datePickerRange: datePickerRange,
    }
};

// 실제 사용할 CalendarTime 변수 생성.
var CalendarTime = CALENDAR_TIME();