/**
 * Common Validation
 * 클라이언트 사이드의 유효성 검증을 수행한다.
 *
 * 참고로 options 값이 null일 경우 default로 세팅
 */
(function($){

    $.fn.commonValidate = function (options) {

            // $.extend
            var settings = $.extend({}, $.fn.commonValidate.defaults, options);

            return this.validate({
                                     /**
                                      * onfocusout: onblur 시 해당항목을 validation 할 것인지 여부
                                      * default로 false 처리 함
                                      **/
                                     onfocusout: settings.onfocusout,

                                     /**
                                      * onkeyup : keyup시 검증
                                      * default로 false 처리 함
                                      **/
                                     onkeyup: settings.onkeyup,

                                     /**
                                      * onClick : 라디오버튼이나 체크박스 click시 검증(default:true)
                                      **/
                                     onclick: settings.onclick,

                                     /**
                                      * rules: 각 항목별로 validation rule을 지정
                                      **/
                                     rules: settings.rules,

                                     /**
                                      * message(선택사항): 검증 오류 시 출력할 메세지, 입력하지 않을 경우 default message 출력
                                      **/
                                     messages: settings.messages,

                                     /**
                                      * ignore
                                      *
                                      * ignore option을 사용하지 않으면 input type="hidden"은 기본으로 유효성 검증을 제외 함
                                      * 제외 할 클래스 이름은 임의로 지정가능(ex. ignore: ".exclude, ...")
                                      */
                                     ignore: settings.ignore,

                                     /**
                                      * errorPlacement
                                      *
                                      * validation 실패 시 메시지 위치
                                      * default는 node 우측
                                      *
                                      * 작동을 원하지 않으면 내용없는 errorPlacement 생성(현재 기본설정은 작동을 막음)
                                      */
                                     errorPlacement: settings.errorPlacement,

                                     /**
                                      * invalidHandler
                                      *
                                      * validation 실패 시의 핸들러를 정의
                                      * 현재 default는 검증 실패 시 alert 처리 하도록 처리 함
                                      */
                                     invalidHandler: settings.invalidHandler,

                                     /**
                                      * submitHandler
                                      *
                                      * 유효성 검사가 완료된 뒤 수행할 핸들러를 정의(사용하지 않을 경우 false)
                                      * 이 옵션을 명시할 경우 'submit' 이벤트만 발생하고 실제 FORM 전송은 일어나지 않음
                                      *
                                      * 만약 해당 옵션을 명시하고 form 전송이 필요한 경우 아래와 같이 처리한다.
                                      * ex)
                                      * form.action="URL 주소";
                                      * form.submit();
                                      */
                                     submitHandler: settings.submitHandler
                                 });
    }

    //commonValidate 기본 설정
    $.fn.commonValidate.defaults = {
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {},
        message: {},
        ignore: ":hidden",
        errorPlacement: function (error, element) {},
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                alert(validator.errorList[0].message);
                validator.errorList[0].element.focus();
            }
        },
        //실제 사용시 초기화로 수정 필요
        submitHandler: false
    };
})(jQuery);

/**
 * commoanValidation 기본 메세지
 *
 * 별도로 검증규칙에 대한 메세지 설정을 하지 않을 경우, 하단의 기본 메세지를 출력 함
 */
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "./jquery.validate"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require("jquery"));
    } else {
        factory(jQuery);
    }
}(function ($) {
    /*
     * Translated default messages for the jQuery validation plugin.
     * Locale: KO (Korean; 한국어)
     */
    $.extend($.validator.messages, {
        required: "필수 항목입니다.",
        remote: "항목을 수정하세요.",
        email: "유효하지 않은 E-Mail주소입니다.",
        url: "유효하지 않은 URL입니다.",
        date: "올바른 날짜를 입력하세요.",
        dateISO: "올바른 날짜(ISO)를 입력하세요.",
        number: "유효한 숫자가 아닙니다.",
        digits: "숫자만 입력 가능합니다.",
        equalTo: "같은 값을 다시 입력하세요.",
        extension: "올바른 확장자가 아닙니다.",
        maxlength: $.validator.format("{0}자를 넘을 수 없습니다. "),
        minlength: $.validator.format("{0}자 이상 입력하세요."),
        rangelength: $.validator.format("문자 길이가 {0} 에서 {1} 사이의 값을 입력하세요."),
        range: $.validator.format("{0} 에서 {1} 사이의 값을 입력하세요."),
        max: $.validator.format("{0} 이하의 값을 입력하세요."),
        min: $.validator.format("{0} 이상의 값을 입력하세요.")
    });
    return $;
}));