var analysisSearchGrid = {
    gridView : new RealGridJS.GridView("analysisSearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        analysisSearchGrid.initGrid();
        analysisSearchGrid.initDataProvider();
        analysisSearchGrid.event();
    },
    initGrid : function () {
        analysisSearchGrid.gridView.setDataSource(analysisSearchGrid.dataProvider);

        analysisSearchGrid.gridView.setStyles(analysisSearchGridBaseInfo.realgrid.styles);
        analysisSearchGrid.gridView.setDisplayOptions(analysisSearchGridBaseInfo.realgrid.displayOptions);
        analysisSearchGrid.gridView.setColumns(analysisSearchGridBaseInfo.realgrid.columns);
        analysisSearchGrid.gridView.setOptions(analysisSearchGridBaseInfo.realgrid.options);
        analysisSearchGrid.gridView.setCheckBar({ visible: false });
        var hoverMaskValue = $('#selHoverMask').val();
        analysisSearchGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
    },
    initDataProvider : function() {
        analysisSearchGrid.dataProvider.setFields(analysisSearchGridBaseInfo.dataProvider.fields);
        analysisSearchGrid.dataProvider.setOptions(analysisSearchGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        analysisSearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            analysisSearchGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        analysisSearchGrid.dataProvider.clearRows();
        analysisSearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
    }
};


var analysisIndexGrid = {
    gridView : new RealGridJS.GridView("analysisIndexGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        analysisIndexGrid.initGrid();
        analysisIndexGrid.initDataProvider();
        analysisIndexGrid.event();
    },
    initGrid : function () {
        analysisIndexGrid.gridView.setDataSource(analysisIndexGrid.dataProvider);

        analysisIndexGrid.gridView.setStyles(analysisIndexGridBaseInfo.realgrid.styles);
        analysisIndexGrid.gridView.setDisplayOptions(analysisIndexGridBaseInfo.realgrid.displayOptions);
        analysisIndexGrid.gridView.setColumns(analysisIndexGridBaseInfo.realgrid.columns);
        analysisIndexGrid.gridView.setOptions(analysisIndexGridBaseInfo.realgrid.options);
        analysisIndexGrid.gridView.setCheckBar({ visible: false });
        analysisIndexGrid.gridView.setSelectOptions({
            style: 'singleRow'
        });
        var hoverMaskValue = $('#selHoverMask').val();
        analysisIndexGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
    },
    initDataProvider : function() {
        analysisIndexGrid.dataProvider.setFields(analysisIndexGridBaseInfo.dataProvider.fields);
        analysisIndexGrid.dataProvider.setOptions(analysisIndexGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        analysisIndexGrid.gridView.onDataCellClicked = function(gridView, index) {
            analysisIndexGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        analysisIndexGrid.dataProvider.clearRows();
        analysisIndexGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
    }
};



var analysisSearchJs = {
    init : function () {
    },
    onload : function() {
    },
    event : function() {
        $('#searchBtn').click(function(){
            analysisSearchJs.search();
        });
    },
    search : function () {
        if(!$.trim($("#searchKeyword").val())) {
            alert('검색어를 입력해주세요.');
            return false;
        }
        var requestData = {};
        requestData.keyword = $("#searchKeyword").val();
        requestData.service = $("#serviceCd option:selected").val();
        requestData.search_analyzer_name = $("#serviceCd option:selected").attr("sch_anal");
        requestData.index_analyzer_name = $("#serviceCd option:selected").attr("idx_anal");

        CommonAjax.basic(
            {
                url:'/search/analysis/result.json',
                data: JSON.stringify(requestData),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                cache: false,
                successMsg:null,
                callbackFunc:function(res)
                {
                    analysisSearchGrid.setData(res.searchPlugin.tokens);
                    analysisIndexGrid.setData(res.indexPlugin.tokens);
                }
            });
    }
};