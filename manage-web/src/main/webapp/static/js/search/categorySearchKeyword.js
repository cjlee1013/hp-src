var categorySearchKeywordGrid = {
    gridView: new RealGridJS.GridView("categorySearchKeywordGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        categorySearchKeywordGrid.initGrid();
        categorySearchKeywordGrid.initDataProvider();
        categorySearchKeywordGrid.event();
    },
    page: 0,
    initGrid: function () {
        categorySearchKeywordGrid.gridView.setDataSource(categorySearchKeywordGrid.dataProvider);

        categorySearchKeywordGrid.gridView.setStyles(categorySearchKeywordGridBaseInfo.realgrid.styles);
        categorySearchKeywordGrid.gridView.setColumns(categorySearchKeywordGridBaseInfo.realgrid.columns);
        categorySearchKeywordGrid.gridView.setOptions(categorySearchKeywordGridBaseInfo.realgrid.options);
        categorySearchKeywordGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        categorySearchKeywordGrid.gridView.setDisplayOptions({
            rowHoverMask: {
                visible: true,
                styles: {
                    background: "#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        ///인디케이터바
        categorySearchKeywordGrid.gridView.setIndicator({
            visible: true
        });
        categorySearchKeywordGrid.gridView.onTopItemIndexChanged = function (grid, item) {
            if (item > categorySearchKeywordGrid.page) {
                categorySearchKeywordGrid.page += 50;
                categorySearchKeywordJs.searchList(categorySearchKeywordGrid.dataProvider.getRowCount(), categorySearchKeywordJs.limitPageSize());
            }
        };
    },
    initDataProvider: function () {
        categorySearchKeywordGrid.dataProvider.setFields(categorySearchKeywordGridBaseInfo.dataProvider.fields);
        categorySearchKeywordGrid.dataProvider.setOptions(categorySearchKeywordGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        categorySearchKeywordGrid.gridView.onDataCellClicked = function (gridView, index) {
            categorySearchKeywordGrid.gridRowSelect(index.dataRow)
        };
    },
    setData: function (dataList) {
        categorySearchKeywordGrid.dataProvider.clearRows();
        categorySearchKeywordGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = categorySearchKeywordGrid.dataProvider.getJsonRow(selectRowId);
        categorySearchKeywordJs.selectedGridIndex(rowDataJson);
    }
};

var categorySearchKeywordJs = {
    init: function () {
        this.initCategorySelectBox();
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox: function () {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    event: function () {
        $('#searchBtn').click(function () {
            categorySearchKeywordJs.searchList();
        });
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $("#saveBtn").click(function () {
            categorySearchKeywordJs.put();
        });
        $("#modBtn").click(function () {
            categorySearchKeywordJs.put();
        });
        $("#confirmBtn").click(function () {
            categorySearchKeywordJs.confirm();
        });
        $("#excelDownloadBtn").click(function () {
            categorySearchKeywordJs.excelDownload();
        });
        $("#resetBtn").bindClick(categorySearchKeywordJs.resetForm);
    },
    resetForm: function () {
        $(".saveArea").show();
        $(".modArea").hide();
        $("#keyword").val('');
        $("#dcateSearchKeywordNo").val('');
        categorySearchKeywordJs.initCategorySelectBox();
        $("#textCountKr_keyword").html(0);
        $("input:radio[name=useYn]").filter('[value="Y"]').prop("checked",true);
    },
    selectedGridIndex: function (data) {
        console.log(data)
        $(".saveArea").hide();
        $(".modArea").show();
        $("#dcateSearchKeywordNo").val(data.dcateSearchKeywordNo);
        $("#cateViewArea").html(data.cateNm);
        $("#keyword").val(data.keyword);
        $("#textCountKr_keyword").html(data.keyword.length);
        $("input:radio[name=useYn]").filter('[value="'+data.useYn+'"]').prop("checked",true);
    },
    searchList: function () {
        CommonAjax.basic({
            url: '/search/categorySearchKeyword/getCategorySearchKeyword.json',
            method: 'get',
            dataType: 'json',
            data: {
                keyword: encodeURI($('#searchKeyword').val())
                , searchType: $("#schSearchType option:selected").val()
                , schUseYn: $("#schUseYn option:selected").val()
                , schResetYn: $("#schResetYn option:selected").val()
            },
            successMsg: null,
            cache: false,
            async: false,
            callbackFunc: function (res) {
                $("#totalCount").html($.jUtil.comma(res.length));
                categorySearchKeywordGrid.setData(res);
            }
        });
    },
    put: function () {
        if ($("#dcateSearchKeywordNo").val() == "" && $('[name=dcateCd] option:selected').val() == "") {
            alert("카테고리를 모두 입력해주세요.");
            return;
        }
        if ($.trim($("#keyword").val()) == "") {
            alert("키워드를 입력해주세요.");
            return;
        }
        CommonAjax.basic({
            url: '/search/categorySearchKeyword/putCategorySearchKeyword.json',
            method: 'post',
            dataType: 'json',
            data: JSON.stringify($("#editForm").serializeObject()),
            contentType: 'application/json',
            successMsg: null,
            cache: false,
            callbackFunc:function(res) {
                if (res == 0) {
                    alert("이미 등록된 카테고리 입니다.");
                    return;
                }
                // categorySearchKeywordJs.resetForm();
                // categorySearchKeywordJs.searchList();
            }
        });
    },
    confirm: function () {
        if ($("#dcateSearchKeywordNo").val() == "") {
            alert("잘못된 접근 입니다.");
            return;
        }
        CommonAjax.basic({
            url: '/search/categorySearchKeyword/confirmCategorySearchKeyword.json',
            method: 'post',
            dataType: 'json',
            data: JSON.stringify($("#editForm").serializeObject()),
            contentType: 'application/json',
            successMsg: null,
            cache: false,
            callbackFunc:function(res) {
                if (res == 0) {
                    alert("확인을 실패하였습니다.");
                    return;
                }
                // categorySearchKeywordJs.resetForm();
                // categorySearchKeywordJs.searchList();
            }
        });
    },
    excelDownload: function () {
        var _date = new Date();
        var fileName = "카테고리유사어_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        categorySearchKeywordGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};