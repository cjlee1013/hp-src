/**
 *  현재 적용중인 제외어
 */
const exceptGrid = {
    gridView : new RealGridJS.GridView("exceptGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, exceptGridBaseInfo);
        this.gridView.setCheckBar({exclusive : true});
        this.event();
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        mngGridCss.setColumnReplace(this.gridView, getNowDay());
        mngGridCss.setColumnUpdateData(this.dataProvider, getNowDay());
        mngGridCss.setOrderByColumn(this.gridView, ["sort", "startDt"], ["ascending", "descending"]);
    },
    event : function() {
        // 그리드 선택
        this.gridView.onDataCellClicked = function(gridView, index) {
            popularManagementJs.gridRowSelect(mngType.EXCEPT, index.dataRow);
        };
    }
};

/**
 *  현재 적용중인 확정어
 */
const fixedGrid = {
    gridView : new RealGridJS.GridView("fixedGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, fixedGridBaseInfo);
        this.gridView.setCheckBar({exclusive : true});
        this.event();
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        mngGridCss.setColumnReplace(this.gridView, getNowDay());
        mngGridCss.setColumnUpdateData(this.dataProvider, getNowDay());
        mngGridCss.setOrderByColumn(this.gridView, ["sort", "rank" ,"startDt"], ["ascending", "ascending","descending"]);
    },
    event : function() {
        // 그리드 선택
        this.gridView.onDataCellClicked = function(gridView, index) {
            popularManagementJs.gridRowSelect(mngType.FIXED, index.dataRow);
        };
    }
};

/**
 * 인기검색어 관리 프로세스 변수
 */
const popularManagementJs = {
    /**
     * 초기화
     */
    init : function(isEvent) {
        this.initSchDate();
        for(let type in mngType){
            this.managementGridView(mngType[type]);
        }

        if(isEvent){
            this.event();
        }
        this.setRankList();
    },
    /**
     * 인기검색어 관리 확정어, 제외어 별 키워드 리스트 출력
     * @param type
     */
    managementGridView: function(type) {
        let resultData = commonPopularJs.getPopularManagement(commonPopularJs.getSiteType(), type, null);
        switch (type) {
            case mngType.FIXED:
                fixedGrid.setData(resultData);
                break;
            case mngType.EXCEPT:
                exceptGrid.setData(resultData);
                break;
        }

        $("#"+type+"TotalCount").html(resultData.length);
    },
    /**
     * 등록/수정 관련 순위 리스트
     */
    setRankList : function(){
        $("#rank").empty();
        $("#rank").append("<option>순위</option>");
        for (let idx = 1; idx <= 10; idx++){
            $("#rank").append("<option value="+idx+">" + idx + "</option>");
        }
    },
    /**
     * 조회 일자 초기화
     */
    initSchDate : function () {
        this.startDt = Calendar.datePicker('startDt');
        this.startDt.setDate(0);
        this.endDt = Calendar.datePicker('endDt');
        this.endDt.setDate(0);
    },
    /**
     * 이벤트
     */
    event : function () {
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $('#resetBtn').click(function () {
            popularManagementJs.reset();
        });
        $('#saveBtn').click(function () {
            popularManagementJs.putKeyword();
        });
        /*
            2020. 10. 26 삭제 버튼 기능 spec out
        $('#deleteFixedBtn').click(function () {
            popularManagementJs.deleteKeyword(mngType.FIXED);
        });
        $('#deleteExceptBtn').click(function () {
            popularManagementJs.deleteKeyword(mngType.EXCEPT);
        });
         */
        $('#type').on('change', function () {
            if ($('#type').val() == mngType.FIXED) {
                $("#rank").prop("disabled", false);
            } else {
                $("#rank").prop("disabled", true);
            }
        });
    },
    /**
     * 인기검색어 등록/수정 관련 정보 초기화
     */
    reset : function () {
        $('#siteType').val("");
        $("#managementNo").val("");
        $("#keyword").val("").prop("disabled", false);
        $("#type").val("").trigger("change").prop("disabled", false);
        $("#rank").prop("disabled", false);
        this.setRankList();
        $("#startDt").val("");
        $("#endDt").val("");
        $('#keyword').setTextLength('#textCountKr_keyword');
        this.startDt.setDate(0);
        this.endDt.setDate(0);
        $('#useYn').find('option:first').attr('selected', 'selected');
    },
    /**
     * 인기검색어 관리 등록/수정
     */
    putKeyword : function () {
        $('#siteType').val(commonPopularJs.getSiteType());

        if ($.trim($("#keyword").val()) == "") {
            alert("키워드를 입력해주세요.");
            return;
        }
        if ($("#type").val() == "") {
            alert("구분값을 선택해주세요.");
            return;
        }

        if($("#startDt").val() > $("#endDt").val()){
            alert("적용 시작일이 적용 종료일보다 클 수 없습니다.");
            return;
        }

        if ($("#type").val() == "fixed" && !$.isNumeric($("#rank").val())) {
            alert("순위를 선택해주세요.");
            return;
        }

        if($("#type").val() == mngType.EXCEPT){
            $('#rank').val(null);
        }

        if(!popularManagementJs.isDataValidation()){
            return;
        }

        CommonAjax.basic({
            url: '/search/popular/putPopularKeyword.json',
            method: 'post',
            dataType: 'json',
            data: JSON.stringify($("#popularForm").serializeObject()),
            contentType: 'application/json',
            successMsg: null,
            cache: false,
            callbackFunc:function(res) {
                if ($("#type").val() == mngType.FIXED) {
                    if ($("#managementNo").val() == "") {
                        alert("확정 키워드가 등록되었습니다.");
                    } else {
                        alert("확정 키워드가 수정되었습니다.");
                    }
                    popularManagementJs.reset();
                    popularManagementJs.managementGridView(mngType.FIXED);
                } else {
                    if ($("#managementNo").val() == "") {
                        alert("제외 키워드가 등록되었습니다.");
                    } else {
                        alert("제외 키워드가 수정되었습니다.");
                    }
                    popularManagementJs.reset();
                    popularManagementJs.managementGridView(mngType.EXCEPT);
                }
            }
        });
    },
    /**
     * realgrid 선택시 이벤트
     * @param type
     * @param selectRowId
     */
    gridRowSelect : function(type, selectRowId) {
        $("#type").prop("disabled", true);
        let rowDataJson = "";
        if (type == "fixed") {
            $("#rank").prop("disabled", false);
            rowDataJson = fixedGrid.dataProvider.getJsonRow(selectRowId);
        } else {
            $("#rank").prop("disabled", true);
            rowDataJson = exceptGrid.dataProvider.getJsonRow(selectRowId);
        }

        $("#managementNo").val(rowDataJson.managementNo);
        $("#keyword").val(rowDataJson.keyword).prop("disabled", true);
        $("#type").val(type).trigger("change");
        $("#rank").val(rowDataJson.rank);
        $("#startDt").val(rowDataJson.startDt.substring(0,10));
        $("#endDt").val(rowDataJson.endDt.substring(0,10));
        $('#keyword').setTextLength('#textCountKr_keyword');
        $('#apply').val(rowDataJson.apply);
        $('#useYn').val(rowDataJson.useYn);
    },
    /**
     * 인기검색어 관리 삭제 처리, 10.26 spec out
     * @param type
     * @returns {boolean}
     */
    deleteKeyword : function(type) {
        let data = "";
        let rows = "";
        if (type == mngType.FIXED) {
            rows = fixedGrid.gridView.getCheckedRows(true);
            data = fixedGrid.dataProvider.getJsonRow(fixedGrid.gridView.getCheckedRows(true))
        } else {
            rows = exceptGrid.gridView.getCheckedRows(true);
            data = exceptGrid.dataProvider.getJsonRow(exceptGrid.gridView.getCheckedRows(true))
        }
        if(rows.length == 0) {
            alert('삭제하려는 키워드를 선택해 주세요.');
            return false;
        }
        if(!confirm('선택하신 키워드를 삭제 하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url: '/search/popular/deletePopularManagement.json',
            dataType: 'json',
            data: {managementNo: data.managementNo},
            type: 'get',
            method:"GET",
            callbackFunc:function() {
                if (type == "fixed") {
                    alert("확정 키워드가 삭제되었습니다.");
                    popularManagementJs.reset();
                    popularManagementJs.managementGridView(mngType.FIXED);
                    //popularKeywordJs.getPopularKeyword(mngType.FIXED);
                } else {
                    alert("제외 키워드가 삭제되었습니다.");
                    popularManagementJs.reset();
                    popularManagementJs.managementGridView(mngType.EXCEPT);
                }
            }
        });
    },
    /**
     * 인기검색어 관리 등록/수정시에 날짜 범위 체크
     * @returns {boolean}
     */
    isDataValidation : function() {
        //if($("#type").val() == mngType.EXCEPT){
        let allData;
        if(mngType.FIXED === $("#type").val()){
            allData = fixedGrid.dataProvider.getJsonRows(0,-1);
        }else if(mngType.EXCEPT === $("#type").val()){
            allData = exceptGrid.dataProvider.getJsonRows(0,-1);
        }
        let startDt = $('#startDt').val();  //적용 시작 날짜
        let endDt = $('#endDt').val();      //적용 종료 날짜
        let rank = $('#rank').val();        //순위
        let keyword = $('#keyword').val();  //키워드
        let isApply = $('#apply').val();    //적용여부
        let isAllow = true;     // 검증 통과여부
        let isUse = $('#useYn').val() === 'Y';  //사용여부
        let managementNo = $('#managementNo').val();    //확정어,제외어 번호

        /* 사용여부를 미사용으로 할시에는 검증을 하지 않는다. */
        if(isUse){
            for(let data of allData){
                if(Number(rank) === data.rank && keyword !== data.keyword ){   //변경될 순위랑 같은지와 키워드가 다를때 ( 자기 자신을 제외)

                    if(isApply === '만료' && data.apply === '만료'){
                        /**
                         * 둘다 만료일 경우에는 체크하는 대상에서 제외한다.
                         */
                    }else if(mngType.FIXED === $("#type").val() && data.apply === '사용'){
                        /**
                         * 확정어에서만 비교, 비교를 만료와는 하지 않는다.
                         */
                        if(startDt >= data.startDt && endDt <= data.endDt){
                            alert("현재 범위 ["+data.keyword+"]와 ["+data.rank+"] 순위 데이터의 범위가 겹칩니다.");
                            isAllow = false;
                            break;
                        }else if(startDt <= data.startDt && data.startDt <= endDt) {
                            alert("시작 범위 [" + data.keyword + "]와 [" + data.rank + "] 순위 데이터의 범위가 겹칩니다.");
                            isAllow = false;
                            break;
                        }else if(startDt <= data.endDt && data.endDt <= endDt){
                            alert("종료 범위 ["+data.keyword+"]와 ["+data.rank+"] 순위 데이터의 범위가 겹칩니다.");
                            isAllow = false;
                            break;
                        }else if( startDt <= data.startDt && endDt >= data.endDt){
                            alert("시작과 종료 범위안에 포함 [" + data.keyword + "]와 [" + data.rank
                                + "] 순위 데이터의 범위가 겹칩니다.");
                            isAllow = false;
                            break;
                        }
                    }
                }else if(keyword === data.keyword){
                    /* 자기 자신은 제외 */
                    if(!(managementNo === data.managementNo)){
                        /* 키워드, 적용시작일, 종료일이 같을 경우  */
                        let alertMsg = [];
                        alertMsg.push("[" + data.keyword + "] 키워드가 이미 등록되어 있습니다.");
                        alertMsg.push("\n    - 사용여부 : " + data.apply);
                        if(mngType.FIXED === $("#type").val()){
                            alertMsg.push("\n    - 순위 : " + data.rank);
                        }
                        alertMsg.push("\n    - 적용시작일 : " + data.startDt);
                        alertMsg.push("\n    - 적용종료일 : " + data.endDt);
                        alert(alertMsg.join(''));
                        isAllow = false;
                        break;
                    }
                }
            }
        }

        return isAllow;
    }
};



/**
 *  해당 페이지에 공통으로 사용되는 Grid 묶음
 */
const mngGridCss = {
    /**
     * 날짜범위에 해당하지 않는 데이터는 css처리
     * 10. 26 미사용에 대한 데이터 css처리
     */
    setColumnReplace : function (gridView, nowDt){
        gridView.setStyles({
            body:{
                dynamicStyles: function(grid, index) {
                    let startDt = grid.getValue(index.itemIndex, "startDt");
                    let endDt = grid.getValue(index.itemIndex, "endDt");
                    let useYn = grid.getValue(index.itemIndex, "useYn");

                    if(useYn == 'N'){
                        return {
                            foreground: "#707b92"
                        }
                    }
                    //현재 범위 이외의 데이터
                    if(!(startDt <= nowDt && endDt >= nowDt)){
                        //현재범위보다 전의 데이터
                        if(startDt < nowDt && endDt < nowDt){
                            return {
                                foreground: "#707b92"
                            }
                        }else{
                            return {
                                foreground: "#47C83E"
                            }
                        }
                    }
                }
            }
        });
    },
    /**
     * 사용여부 컬럼 데이터 부여 ( 준비, 사용, 미사용, 만료 )
     * 정렬 순서 셋
     * @param nowDt
     */
    setColumnUpdateData : function(dataProvider, nowDt){
        let allData = dataProvider.getJsonRows(0,-1);
        let index = 0;
        let text = '';
        let sort = 0;
        /*
            정렬 순서
                - 사용 > 준비 > 만료 > 미사용
         */
        for( let data of allData){
            if(data.useYn == 'N'){
                text = '미사용';
                sort = 4;
            }else if((data.startDt <= nowDt && data.endDt >= nowDt)){
                text = '사용';
                sort = 1;
            }else{
                if(data.startDt < nowDt && data.endDt < nowDt){
                    text = '만료';
                    sort = 3;
                }else{
                    text = '준비';
                    sort = 2;
                }
            }
            this.setValueData(dataProvider, index, 'apply', text);
            this.setValueData(dataProvider, index, 'sort', sort);
            index++;
        }
    },
    /**
     * 데이터 변경
     * @param row
     * @param field
     * @param value
     */
    setValueData : function (dataProvider, row, field, value) {
        dataProvider.setValue(row, field, value);
    },
    /**
     * 데이터 정렬
     * @param fieldArr
     * @param sortArr
     */
    setOrderByColumn : function (gridView, fieldArr, sortArr){
        gridView.orderBy(fieldArr, sortArr);
    }
};



/**
 * 현재 날짜 조회 yyyy-MM-dd
 * @returns {string}
 */
function getNowDay(){
    return new Date().getFullYear() + "-" + ("0"+(new Date().getMonth()+1)).slice(-2) + "-" + ("0"+new Date().getDate()).slice(-2);
}


