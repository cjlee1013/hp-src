



/***
 * black 리스트 그리드 설정
 * @type {{init: blackListGrid.init, setData: blackListGrid.setData, initDataProvider: blackListGrid.initDataProvider, excelDownload: blackListGrid.excelDownload, dataProvider: *, initGrid: blackListGrid.initGrid, event: blackListGrid.event, gridView: *}}
 */
var blackListGrid = {
    gridView : new RealGridJS.GridView("blackListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        blackListGrid.initGrid();
        blackListGrid.initDataProvider();
        blackListGrid.event();
    },
    initGrid : function () {
        blackListGrid.gridView.setDataSource(blackListGrid.dataProvider);
        blackListGrid.gridView.setStyles(blackListGridBaseInfo.realgrid.styles);
        blackListGrid.gridView.setDisplayOptions(blackListGridBaseInfo.realgrid.displayOptions);
        blackListGrid.gridView.setColumns(blackListGridBaseInfo.realgrid.columns);
        blackListGrid.gridView.setOptions(blackListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        blackListGrid.dataProvider.setFields(blackListGridBaseInfo.dataProvider.fields);
        blackListGrid.dataProvider.setOptions(blackListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        blackListGrid.gridView.onDataCellClicked = function(gridView, index) {
            blackListGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        blackListGrid.dataProvider.clearRows();
        blackListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = blackListGrid.dataProvider.getJsonRow(selectRowId);
        blackListJs.selectedIndex(rowDataJson);
    }
};
var blackListJs = {
    init : function () {
        blackListJs.initSearchDate('0d');
        blackListJs.initDateRange();
        blackListJs.bindEvent();
        blackListJs.initRegDate();
        $("#actionType").val("REG");
    },
    initDateRange : function () {
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSearchDate : function (flag) {
        blackListJs.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        blackListJs.setDate.setEndDate(0);
        blackListJs.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());

        /* 버튼색상변경 */
        let buttonId = '';
        if(flag === '0d'){
            buttonId = 'today';
        }else if(flag === '-1d'){
            buttonId = 'oneday';
        }else if(flag === '-30d'){
            buttonId = 'month';
        }else if(flag === '-90d'){
            buttonId = '3month';
        }

        $('.ui.button.small:button').each(function(){
            if($(this).attr('id') === buttonId){
                $(this).removeClass("gray-dark");
                $(this).addClass("cyan");
            }else {
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark");
            }
        });
    },
    initRegDate : function () {
        blackListJs.setDate = Calendar.datePickerRange('regStartDt', 'regEndDt');
        blackListJs.setDate.setEndDate("");
        blackListJs.setDate.setStartDate("");
        $("#regEndDt").datepicker("option", "minDate", $("#regStartDt").val());
        $("#regStartDt").attr("disabled",true);
        $("#regEndDt").attr("disabled",true);
    },
    bindEvent : function() {
        $('#searchBtn').bindClick(function() {
            blackListJs.search();
        });
        $("#regBtn").bindClick(function(){
            blackListJs.insert();
        });
        $("#resetBtn").bindClick(function(){
            blackListJs.reset();
        });
        $("#regReset").click(function(){
            blackListJs.insertReset();
        });
        $('#regIp').allowInput("keyup", ["NUM", "DOT"]);

        $('.ui.button.small:button').bindClick(function(){
            $(this).removeClass("gray-dark");
            $(this).addClass("cyan");
        })

    },
    search : function (_fn_callback) {

        var data = {};
        data.serviceType = $("input[name=searchServiceType]:checked").val();
        data.blacklistType = $("select[name=searchBlackListType] option:selected").val();
        data.schKeyword = $("#searchIp").val();
        data.dateType = $("select[name=searchDateType] option:selected").val();
        data.schStartDt = $("input[name=searchStartDt]").val();
        data.schEndDt = $("input[name=searchEndDt]").val();

        CommonAjax.basic(
            {
                url:'/search/operation/searchLog/blackList.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    blackListGrid.setData(res);
                    if(blackListGrid.gridView.getItemCount() > 0){
                        blackListGrid.gridRowSelect(0);
                    }
                    $('#blackListGridTotalCount').html($.jUtil.comma(blackListGrid.gridView.getItemCount()));

                    if(_fn_callback != null && _fn_callback != "undefined") {
                        _fn_callback();
                    }
                }
            });

    },
    insert : function () {
        var inputIp = $("#regIp").val();
        if(inputIp == ''){
            alert("등록할 IP를 입력하세요.");
            return false;
        }
        if(!utilsJs.ipValidation(inputIp)){
            return false;
        }

        var data = {};
        data.serviceType = $("input[name=regServiceType]:checked").val();
        data.blacklistType = $("select[name=regBlackType] option:selected").val();
        data.regKeyword = $("#regIp").val();
        data.regStartDt = $("input[name=regStartDt]").val();
        data.regEndDt = $("input[name=regEndDt]").val();
        data.regMemo = $("textarea#regMemo").val();
        data.actionType = $("#actionType").val();

        CommonAjax.basic(
            {
                url:'/search/operation/searchLog/registerBlackListIp.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg: null,
                callbackFunc: function (res) {
                    if(res == 1) {
                        if(data.actionType == 'REG'){
                            alert('저장되었습니다.');
                        } else {
                            alert('수정되었습니다.');
                        }
                        $("input:radio[name=searchServiceType]:input[value="+data.serviceType+"]").prop("checked",true);
                        $('#searchBlackListType').val(data.blacklistType).prop("selected",true);
                        $("#searchIp").val(data.regKeyword);
                        blackListJs.initSearchDate('0d');
                        blackListJs.initDateRange();
                        blackListJs.search();
                    } else {
                        alert('이미 등록된 IP 입니다');
                    }
                }
            });
    },
    statusChange : function (status,desc) {
        // status = Y 활성화 상태로 업데이트 , N 비활성화 상태로 업데이트
        let arrIpNo = new Array();
        let rowArr = blackListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('상태 변경 할 IP를 선택해주세요.');
            return false;
        } else {
            if(!confirm('선택된 IP ' + rowArr.length + '건 ' + desc + ' 상태로 변경하시겠습니까?')){
                return false;
            }
        }
        for (let i in rowArr) {
            let rowData = blackListGrid.dataProvider.getJsonRow(rowArr[i]);
            arrIpNo.push(Number(rowData.blacklistNo));
        }
        let data = {};
        data.ips = arrIpNo;
        data.enableYn = status;
        CommonAjax.basic({
            url:'/search/operation/searchLog/modifyBlacklistStatus.json',
            data: JSON.stringify (data),
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            method: "POST",
            callbackFunc: function (res) {
                alert(res + '건의 IP 상태가 변경되었습니다.');
                $('#searchBtn').trigger('click');
            }
        });
    },
    blackTypeChange : function(){
        let type = $("#regBlackType").val();
        if(type == 'ABUSING') {
            $("#regStartDt").attr("disabled",false);
            $("#regEndDt").attr("disabled",false);
            blackListJs.setDate.setStartDate("0d");
            blackListJs.setDate.setEndDate("0d");
        } else {
            $("#regStartDt").attr("disabled",true);
            $("#regEndDt").attr("disabled",true);
            blackListJs.setDate.setStartDate("");
            blackListJs.setDate.setEndDate("");
        }
    },
    selectedIndex : function(jsonRow) {
        $("#regIp").val(jsonRow.ip);
        $("#regIp").prop("readonly",true);
        $("#regMemo").val(jsonRow.memo);
        $("input:radio[name=regServiceType]:input[value="+jsonRow.serviceType+"]").prop("checked",true);
        $("input:radio[name=regServiceType]").prop("disabled",true);
        $('#regBlackType').val(jsonRow.blacklistType).prop("selected",true);
        $('#regBlackType').prop("disabled",true);
        if(jsonRow.validStartDt != null && jsonRow.validStartDt != ""){
            $("#regStartDt").val(jsonRow.validStartDt.slice(0,10));
            $("#regEndDt").val(jsonRow.validEndDt.slice(0,10));
        }
        if(jsonRow.blacklistType == 'CRAWLING'){
            $("#regStartDt").attr("disabled",true);
            $("#regEndDt").attr("disabled",true);
        }else {
            $("#regStartDt").attr("disabled",false);
            $("#regEndDt").attr("disabled",false);
        }
        $("#actionType").val("MOD");

    },
    reset : function(){
        $("#searchIp").val('');
        $("#searchBlackListType").val('');
        $('#searchDateType').val('mod').prop("selected",true);
        $("input:radio[name=searchServiceType]:input[value='HOME']").prop("checked",true);
        blackListJs.initSearchDate('0d');
        blackListJs.initDateRange();

    },
    insertReset : function(){
        $("#regIp").val('');
        $("#regIp").prop("readonly",false);
        $("#regMemo").val('');
        $("input:radio[name=regServiceType]:input[value='HOME']").prop("checked",true);
        $('#regBlackType').val('CRAWLING').prop("selected",true);
        $('#regBlackType').prop("disabled",false);
        $("input:radio[name=regServiceType]").prop("disabled",false);
        $("#actionType").val("REG");
        blackListJs.initRegDate();
    }
};

var utilsJs = {
    ipValidation : function(inputText){
        var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        if (ipformat.test(inputText)) {
            return true;
        } else {
            alert("입력하신 값은 IP 형식이 아닙니다.");
            $("#regIp").focus();
            return false;
        }
    }
}