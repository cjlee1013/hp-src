function copy_to_clipboard(str) {
    $('#clip_target').val(str);
    $('#clip_target').select();
    try {
        var successful = document.execCommand('copy');
        if(successful) {
            alert('복사되었습니다.');
        }
    } catch (err) {
        alert('이 브라우저는 지원하지 않습니다.')
    }
}

var categorySearchJs = {
    init : function() {
        this.initCategorySelectBox();
        this.bindingEvent();
    },
    limitPageSize: 100,
    categoryId: '',
    categoryName: '',
    categoryDepth: '',
    isFirstSearch: false,
    drawCategorySelect: function(depth, prefix) {
        /* filter 초기화 */
        categorySearchJs.filterReset();
        $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);

        if(depth < 4){
            commonCategory.changeCategorySelectBox(depth, prefix);
        }
        let category = '#' + prefix + depth + ' option:checked';
        categorySearchJs.categoryDepth = depth;
        if ($(category).val() == '' && (depth - 1) != 0) {
            categorySearchJs.categoryDepth = (depth - 1);
            category = '#' + prefix + (depth - 1) + ' option:checked';
        }
        categorySearchJs.categoryId = $(category).val();
        categorySearchJs.categoryName = $(category).text();

        /* 검색결과 초기화 */
        // categorySearchGrid.clear();
        /* 초기 검색 여부 초기화 */
        categorySearchJs.isFirstSearch = false;
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    selectedIndex : function(jsonRow) {
        $("span[name=indexDocId]").html(jsonRow.docId);             //문서번호
        $("span[name=indexWeight]").html(jsonRow.weight);           //가중치명
        $("span[name=indexWeightCalc]").html(jsonRow.weightCalc);   //가중치 수식
        $("span[name=indexWeightDay]").html(jsonRow.weightDay); // 24시간 구매자수

        $("span[name=indexItemNo]").html(jsonRow.itemNo); // 상품번호
        $("span[name=indexSearchItemNm]").html(jsonRow.searchItemNm); // 상품명 원형
        $("span[name=indexBrandNmKor]").html(jsonRow.brandNmKor); // 브랜드 한글명
        $("span[name=indexBrandNmEng]").html(jsonRow.brandNmEng); // 브랜드 한글명
        $("span[name=indexShopNm]").html(jsonRow.shopNm); // 파트너

        let separator = ' > ';
        let category =  filterUtilJs.concat(jsonRow.lcateNm, separator,
                            filterUtilJs.concat(jsonRow.mcateNm, separator,
                                filterUtilJs.concat(jsonRow.scateNm, separator, jsonRow.dcateNm)
                            )
                        );
        $("span[name=indexCategory]").html(category); // 카테고리
    },
    reset : function() {
        /* 카테고리 초기화 */
        currentSearchJs.categoryDepth = '';
        currentSearchJs.categoryId = '';
        $("#searchCateCd1 option:eq(0)").prop("selected", true); //첫번째 option 선택
        $('#searchCateCd2').children('option:not(:first)').remove();
        $('#searchCateCd3').children('option:not(:first)').remove();
        $('#searchCateCd4').children('option:not(:first)').remove();

        $("#storeIds").val("37");
        $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);
        categorySearchJs.filterReset();
        categorySearchJs.isFirstSearch = false;
        $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
        $("#storeType").val('HYPER').prop("selected",true);
        categorySearchJs.setStoreCondition('DIRECT');
    },
    matchBoxReset : function(){
        $("span[name=indexDocId]").html('');
        $("span[name=indexWeight]").html('');
        $("span[name=indexWeightCalc]").html('');
        $("span[name=indexWeightDay]").html('');
        $("span[name=indexItemNo]").html('');
        $("span[name=indexSearchItemNm]").html('');
        $("span[name=indexBrandNmKor]").html('');
        $("span[name=indexBrandNmEng]").html('');
        $("span[name=indexShopNm]").html('');
        $("span[name=indexCategory]").html('');
    },
    filterReset: function(){
        /* 공통 필터 초기화 */
        filterJs.filterReset();
        $('#partnerList').html('');
        filterJs.falseCheckbox('delivery');
    },
    searchDataSet: function() {
        const regexp = /^[(\d{1,10}),]*$/;
        /* init parameter - Essential*/
        let storeIds;
        let siteType;
        let categoryId;
        let categoryDepth;

        /* set parameter */
        storeIds = $("input[name=storeIds]").val();
        siteType = $("input[name=siteType]:checked").val();
        categoryId = categorySearchJs.categoryId;
        categoryDepth = categorySearchJs.categoryDepth;

        /* parameter validate */
        if (storeIds == null || storeIds == ''){
            alert('점포를 입력해주세요.');
            return false;
        }
        if (storeIds != null && $.trim(storeIds) !== '' && !regexp.test(storeIds)) {
            alert('점포ID 형식이 올바르지 않습니다.');
            return false;
        }
        if(filterUtilJs.isEmpty(categoryId)) {
            alert('카테고리를 먼저 선택해주세요.');
            return false;
        }

        /* data return */
        return {
            storeIds: storeIds,
            siteType: siteType,
            categoryId: categoryId,
            categoryDepth: categoryDepth
        };
    },
    /*
        필터 UI view
     */
    filter: function () {
        let dataSet = categorySearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            dataSet.explain = 'N';
        }
        CommonAjax.basic(
            {
                url: '/search/dataManagement/category/filter.json',
                data: dataSet,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    // view
                    let data = res.data;
                    if(filterUtilJs.isNotEmpty(data.brandList)){
                        $('#brandList').html(filterJs.drawCheckbox(data.brandList, 'brand'));
                    }
                    if(filterUtilJs.isNotEmpty(data.partnerList)){
                        $('#partnerList').html(filterJs.drawCheckbox(data.partnerList, 'partner'));
                    }
                }
            }
        );
    },
    search : function (offset, limit, callback) {
        /* filter parameter validate & merge */
        let dataSet = categorySearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            Object.assign(dataSet, filterJs.filterDataSet(), {
                page:  offset,
                perPage: limit,
                explain: 'Y'
            });
        }

        CommonAjax.basic(
            {
                url: '/search/dataManagement/category/list.json',
                data: dataSet,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {

                    /* 최초검색 여부 설정 */
                    if(!categorySearchJs.isFirstSearch){
                        categorySearchJs.isFirstSearch = true;
                    }

                    //쿼리 저장
                    $("#hidden_query").val(JSON.stringify(res.data.query));

                    /***
                     * 검색 결과 수
                     */
                    $("span[name=totalCount]").html(filterUtilJs.getCommaCalc(res.data.totalCount));

                    if(res.data.dataList != null && res.data.dataList.length > 0) {
                        categorySearchGrid.page += 1;
                    }

                    if(offset > 1) {
                        categorySearchGrid.dataProvider.fillJsonData(res.data.dataList,{fillMode: "append"});
                    }
                    else
                    {
                        categorySearchGrid.setData(res.data.dataList);
                        categorySearchGrid.page = 1;
                        if(categorySearchGrid.dataProvider.getRowCount() > 0) {
                            categorySearchJs.selectedIndex(categorySearchGrid.dataProvider.getJsonRow(0));
                        } else {
                            categorySearchJs.matchBoxReset(); //검색 결과가 없을 시 매치정보 초기화
                        }
                    }

                    if(callback !== "undefined" && callback != null) {
                        callback(res);
                    }
                }
            }
        );
        return true;
    },
    bindingEvent : function() {
        /* 검색버튼 */
        $('#searchBtn').click(function() {
            categorySearchJs.filterReset();
            if(categorySearchJs.search(1,categorySearchJs.limitPageSize,null)){
                categorySearchJs.filter();
            }
        });
        /* 초기화 버튼 */
        $('#resetBtn').click(function() {
            categorySearchJs.reset();
        });

        /* 필터 이벤트 바인딩 */
        filterJs.bindEvent();
        /* 체크박스 */
        filterJs.bindCheckbox('delivery');
        filterJs.bindCheckbox('partner');

        $("#copyQuery").click(function() {
            if($("#hidden_query").val() == null || $("#hidden_query").val() == '') {
                alert('검색시도 후 복사가 가능합니다.');
                return false;
            }
            copy_to_clipboard($("#hidden_query").val());
        });
        $("input[name=siteType]").click(function() {
            if($(this).val() == "HOME") {
                $("#storeIds").val("37");
            }else {
                $("#storeIds").val("38");
            }
        });
        $("#excelDownloadBtn").click(function() {
            categorySearchJs.excelDownload();
        });

        $("input[name=storeSearchType]").click(function() {
            var searchType = $(this).val();
            categorySearchJs.setStoreCondition(searchType);
        });
    },
    excelDownload: function() {
        categorySearchJs.search(1,1000,function() {
            let _date = new Date();

            let categoryId = categorySearchJs.categoryId;
            let categoryName = categorySearchJs.categoryName;
            let fileName = "카테고리_"+$("#profilesActive").val() +"_"+categoryName + "("+categoryId+")_"+ $("input[name=siteType]:checked").val() + "_" + $("input[name=storeIds]").val().replace(/,/gi,"_") + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate() + "_" + _date.getHours() + "" + _date.getMinutes();
            categorySearchGrid.gridView.exportGrid({
                type: "excel",
                indicator: "visible",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다.",
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                showColumns: [
                    "eventItemYn",
                    "pickYn",
                    "togetherYn",
                    "giftYn",
                    "freeshipYn",
                    "regDt"
                ],
                hideColumns: [
                    "img",
                    "lcateNm",
                    "mcateNm",
                    "scateNm",
                    "dcateNm"
                ]
            });
        });
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=HYPER"
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },

    setStoreCondition : function (storeSearchType){
        if(storeSearchType == 'DIRECT'){
            $("#storeIds").val("37");
            $("#storeIds").attr("readonly",false);
            $("#schStoreNm").hide();
            $("#schStoreBtn").hide();
            $("#directText").show();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }else { // 'SEARCH'
            $("#storeIds").val("");
            $("#storeIds").attr("readonly",true);
            $("#schStoreNm").val("");
            $("#schStoreNm").show();
            $("#schStoreBtn").show();
            $("#directText").hide();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }
    }
};


const categorySearchGrid = {
    gridView : new RealGridJS.GridView("categorySearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        categorySearchGrid.initGrid();
        categorySearchGrid.initDataProvider();
        categorySearchGrid.event();
    },
    page : 1,
    initGrid : function () {
        categorySearchGrid.gridView.setDataSource(categorySearchGrid.dataProvider);

        categorySearchGrid.gridView.setStyles(categorySearchGridBaseInfo.realgrid.styles);
        categorySearchGrid.gridView.setDisplayOptions(categorySearchGridBaseInfo.realgrid.displayOptions);
        categorySearchGrid.gridView.setColumns(categorySearchGridBaseInfo.realgrid.columns);
        categorySearchGrid.gridView.setOptions(categorySearchGridBaseInfo.realgrid.options);
        categorySearchGrid.gridView.setCheckBar({ visible: false });
        categorySearchGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        categorySearchGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            },
            rowHeight: 80
        });
        categorySearchGrid.gridView.onScrollToBottom = function(grid) {
            if(categorySearchGrid.dataProvider.getRowCount() > 7){ // 한화면에 그려지는 개수
                categorySearchJs.search(categorySearchGrid.page + 1, categorySearchJs.limitPageSize, null);
            }
        };
    },
    initDataProvider : function() {
        categorySearchGrid.dataProvider.setFields(categorySearchGridBaseInfo.dataProvider.fields);
        categorySearchGrid.dataProvider.setOptions(categorySearchGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        categorySearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            categorySearchGrid.gridRowSelect(index.dataRow)
        };
    },
    clear: function() {
        categorySearchGrid.dataProvider.clearRows();
    },
    setData : function(dataList) {
        categorySearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = categorySearchGrid.dataProvider.getJsonRow(selectRowId);
        categorySearchJs.selectedIndex(rowDataJson);
    }
};