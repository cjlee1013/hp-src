var backupRecovery = {
    event : function() {
        $('#uploadFile').bindChange(backupRecovery.changeFile);
        $('#fileUpload').bindClick(backupRecovery.openFile);
        $('#backupBtn').bindClick(backupRecovery.backup);
        $("#recoveryBtn").bindClick(backupRecovery.recovery);
    },
    /***
     * 백업
     */
    backup : function() {
        if(!confirm('백업을 진행 하시겠습니다?\n\n최대 1분이 소요 됩니다. 잠시만 기다려주세요.')) {
            return false;
        }

        CommonAjax.basic({url:'/search/backupRecovery/backupTableList.json?' + $('#backupRecoveryForm').serialize(), cache:false, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                if(res.returnStatus == 200) {
                    var backupList = res.data;
                    for(var i = 0; i < backupList.length; i++) {
                        ///백업 테이블 수 만큼 다운로드 진행
                        var tableName = backupList[i];
                        var schServiceCd = $("select[name=schServiceCd] option:selected").val();
                        backupRecovery.fnSleep(10000); //각 파일별 시간 텀을 준다
                        $("#" + tableName).prop('src','/search/backupRecovery/downloadsAsTxt.json?schServiceCd=' + schServiceCd + "&tableName=" + tableName);
                    }
                    alert('백업이 완료되었습니다.');
                } else {
                    alert(res.returnMessage);
                }
            }});
    },
    fnSleep: function (delay){
        var start = new Date().getTime();
        while (start + delay > new Date().getTime());
    },
    /****
     * 복원
     */
    recovery : function() {
        var passwd=prompt("비밀번호 (관리자문의) :");
        if(passwd == null || passwd == undefined || passwd == '') {
            return false;
        }

        $("#backupRecoveryForm").ajaxForm({
            url         : "/search/backupRecovery/txtAsDB.json?passwd=" + passwd,
            method      : "POST",
            enctype     : "multipart/form-data",
            timeout     : 100000,
            success     : function(res) {
                if(res.returnStatus == 200 && res.returnCode == "SUCCESS") {
                    alert('복원이 완료 되었습니다.');
                } else {
                    alert(res.returnMessage);
                }
            },
            error       : function(resError) {
                // error message alert
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            },
        });
        $("#backupRecoveryForm").submit();
    },
    changeFile : function() {

        var fileInfo = $('#uploadFile')[0].files[0];
        if (!fileInfo) {
            alert('업로드 대상 파일을 확인해주세요.');
            return false;
        }

        var fileNameList = "<font color='black' size='2' style='color:#e83f3f'>";
        for(var i = 0; i < $('#uploadFile')[0].files.length; i++) {
            fileNameList += $('#uploadFile')[0].files[i].name + ((i == ($('#uploadFile')[0].files.length -1)) ? "" : " , ");
        }
        fileNameList += "</font>";
        $("#uploadFileNm").html(fileNameList);
    },
    openFile : function() {
        $('#uploadFile').trigger('click');
    }
};