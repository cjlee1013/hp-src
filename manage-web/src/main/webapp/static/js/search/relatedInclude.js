
var relatedKeywordListGrid = {
    gridView : new RealGridJS.GridView("relatedKeywordListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        relatedKeywordListGrid.initGrid();
        relatedKeywordListGrid.initDataProvider();
        relatedKeywordListGrid.event();
    },
    initGrid : function() {
        relatedKeywordListGrid.gridView.setDataSource(relatedKeywordListGrid.dataProvider);

        relatedKeywordListGrid.gridView.setStyles(relatedKeywordListGridBaseInfo.realgrid.styles);
        relatedKeywordListGrid.gridView.setDisplayOptions(relatedKeywordListGridBaseInfo.realgrid.displayOptions);
        relatedKeywordListGrid.gridView.setColumns(relatedKeywordListGridBaseInfo.realgrid.columns);
        relatedKeywordListGrid.gridView.setOptions(relatedKeywordListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        relatedKeywordListGrid.dataProvider.setFields(relatedKeywordListGridBaseInfo.dataProvider.fields);
        relatedKeywordListGrid.dataProvider.setOptions(relatedKeywordListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        relatedKeywordListGrid.gridView.onDataCellClicked = function(gridView, index) {
            relatedKeywordJs.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        relatedKeywordListGrid.dataProvider.clearRows();
        relatedKeywordListGrid.dataProvider.setRows(dataList);
    }
};

var relatedKeywordJs = {
    /***
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initAjaxForm();
        this.initSearchDate('-30d');
        this.initInputDate();
    },
    /***
     * 등록/수정
     */
    initAjaxForm : function() {
        $('#relatedKeywordSetForm').ajaxForm({
            url: '/search/related/setItem.json',
            type: 'post',
            success: function(resData) {
                if(resData.returnStatus == 200 && resData.returnCode == "SUCCESS") {
                    $("#schKeyword").val($("#keyword").val());
                    $('input:radio[name=schSiteType]').each(function() {
                        if($(this).val() == $("input[name=siteType]:checked").val()) {
                            $(this).prop("checked",true);
                        }
                    });
                    relatedKeywordJs.search();
                }

                alert(resData.returnMessage);
            },
            error: function(e) {
                alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
            }
        });
    },
    /***
     * 검색 날짜 폼 설정
     * @param flag
     * @param element
     */
    initSearchDate : function (flag, element) {
        relatedKeywordJs.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        relatedKeywordJs.setDate.setEndDate(0);
        relatedKeywordJs.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if(element){
            relatedKeywordJs.buttonClickEvent(element);
        }else{
            relatedKeywordJs.buttonClickEvent($('#rangeDefault'));
        }
    },
    /**
     * 버튼 클릭시 색상 변경 이벤트
     * @param element
     */
    buttonClickEvent: function(element){
        $('button[name=rangeBtn]').each(function(){
            if($(this).hasClass("cyan")){
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark")
            }
        });
        $(element).removeClass("gray-dark");
        $(element).addClass("cyan");
    },
    initInputDate : function() {
        relatedKeywordJs.setDate = Calendar.datePickerRange('startDt', 'endDt');
        relatedKeywordJs.setDate.setEndDate("");
        relatedKeywordJs.setDate.setStartDate("");
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());

        $("input:radio[name=operationPeriodYn]:input[value='N']").prop("checked",true);
        $("#startDt").attr("disabled",true);
        $("#endDt").attr("disabled",true);

    },
    /***
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(relatedKeywordJs.search);
        $('#searchResetBtn').bindClick(relatedKeywordJs.searchFormReset);
        $('#resetBtn').bindClick(relatedKeywordJs.resetForm);
        $('#relationKeyword').calcTextLength('keyup', '#textCountKr_relationKeyword');
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $('#setBtn').bindClick(relatedKeywordJs.setItem);
        $('input:radio[name="operationPeriodYn"]').on('change', function() {
            relatedKeywordJs.changeOperationPeriodYn($(this).val());
        });
    },
    /***
     * 검색 시도
     */
    search : function() {
        if(!relatedKeywordJs.valid.search()) {
            return false;
        }
        CommonAjax.basic({url:'/search/related/getItemList.json?' + $('#searchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                $('#relationId').val('');
                relatedKeywordListGrid.setData(res.result);
                $('#totalCount').html($.jUtil.comma(res.totalCount));
                if(res.totalCount > 0){
                    relatedKeywordJs.gridRowSelect(0);
                }
            }});
    },
    /***
     * 검색폼 초기화
     */
    searchFormReset : function() {
        relatedKeywordJs.initSearchDate('-30d');
        $("#schDateType option:eq(0)").prop("selected",true);
        $("#schStatus option:eq(0)").prop("selected",true);
        $("#schSearchType option:eq(0)").prop("selected",true);
        $("#schUseYn option:eq(0)").prop("selected",true);
        $('input[name="schSiteType"]')[0].checked = true;
        $("#schKeyword").val('');
    },
    gridRowSelect : function(selectRowId) {
        var rowDataJson = relatedKeywordListGrid.dataProvider.getJsonRow(selectRowId);

        $('#relationId').val(rowDataJson.relationId);
        $("#keyword").val(rowDataJson.keyword).prop("readonly",true);
        $("#textCountKr_keyword").html(rowDataJson.keyword.length);
        $("#relationKeyword").val(rowDataJson.relationKeyword);
        $("#textCountKr_relationKeyword").html(rowDataJson.relationKeyword.length);
        $("#dlv_siteTypeRadio").css("visibility", "visible");
        $("#dlv_siteTypeText").css("visibility", "visible");
        $("#dlv_siteTypeText_siteName").html($("input[name=schSiteType]:checked").attr("text"));
        $('input:radio[name=siteType]').each(function() {
            if($("input[name=schSiteType]:checked").val() == $(this).val())
            {
                $(this).prop("checked",true);
            }
        });
        $("input:radio[name=operationPeriodYn]:input[value="+rowDataJson.operationPeriodYn+"]").prop("checked",true);
        relatedKeywordJs.changeOperationPeriodYn(rowDataJson.operationPeriodYn, rowDataJson.startDt, rowDataJson.endDt);
        $('#useYn option').prop("selected",false).filter(function() {return this.text == rowDataJson.useYn;}).prop('selected', true);
    },
    resetForm : function() {
        $('#relationId').val('');
        $("#keyword").val('').prop("readonly",false);
        $('#useYn option').prop("selected",false);
        $('#useYn option:eq(0)').prop("selected",true);
        $('input:radio[name=siteType]').each(function() {
            if($("input[name=schSiteType]:checked").val() == $(this).val())
            {
                $(this).prop("checked",true);
            }
        });
        $("#dlv_siteTypeRadio").css("visibility", "hidden");
        $("#dlv_siteTypeText").css("visibility", "hidden");
        relatedKeywordJs.initInputDate();
        $('#relationKeyword').val('');
        $("#textCountKr_keyword").html(0);
        $("#textCountKr_relationKeyword").html(0);
    },
    setItem : function() {
        if(!relatedKeywordJs.valid.setItem()) {
            return false;
        }
        $('#relatedKeywordSetForm').submit();
    },
    changeOperationPeriodYn : function(operationPeriodYn, saleStartDt, saleEndDt) {
        if(operationPeriodYn == 'Y'){
            $("#startDt").attr("disabled",false);
            $("#endDt").attr("disabled",false);
        }else{
            $("#startDt").attr("disabled",true);
            $("#endDt").attr("disabled",true);
        }

        $("#startDt").val(saleStartDt);
        $("#endDt").val(saleEndDt);
    }
};

relatedKeywordJs.valid = {
    setItem : function() {
        var operationPeriodYn = $("input[name=operationPeriodYn]:checked").val();
        var startDt = $("#startDt");
        var endDt = $("#endDt");
        var keyword = $("#keyword").val();
        var relationKeyword = $('#relationKeyword').val();

        if (keyword == "" || keyword.length < 2) {
            alert('주제어는 2자 이상 입력해주세요.');
            keyword.focus();
            return false;
        }

        if( operationPeriodYn == 'Y' && (!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) ) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            relatedKeywordJs.initInputDate();
            return false;
        }

        if( operationPeriodYn == 'Y' && moment(endDt.val()).diff(startDt.val(), "day", true) < 0 ) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (relationKeyword == "" || relationKeyword.length < 2) {
            alert('연관검색어는 2자 이상 입력해주세요.');
            relationKeyword.focus();
            return false;
        }

        return true;
    },
    search : function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var searchKeyword = $("#schKeyword").val();

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    }
};

$(document).ready(function() {
    relatedKeywordJs.init();
    relatedKeywordListGrid.init();
});