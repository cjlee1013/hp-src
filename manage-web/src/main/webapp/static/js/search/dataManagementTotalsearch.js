function is_ie() {
    if(navigator.userAgent.toLowerCase().indexOf("chrome") != -1) return false;
    if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) return true;
    if(navigator.userAgent.toLowerCase().indexOf("windows nt") != -1) return true;
    return false;
}

function copy_to_clipboard(str) {
    $('#clip_target').val(str);
    $('#clip_target').select();
    try {
        var successful = document.execCommand('copy');
        if(successful) {
            alert('복사되었습니다.');
        }
    } catch (err) {
        alert('이 브라우저는 지원하지 않습니다.')
    }
}



function categoryChange(categoryDepth, categoryName) {
    utils.categoryReset(categoryDepth);
    let category = '#' + categoryName + categoryDepth + ' option:checked';
    totalsearchJs.categoryDepth = categoryDepth;
    if ($(category).val() == '' && (categoryDepth - 1) != 0) {
        totalsearchJs.categoryDepth = (categoryDepth - 1);
        category = '#' + categoryName + (categoryDepth - 1) + ' option:checked';
    }
    totalsearchJs.categoryId = $(category).val();
    totalsearchJs.search(1,totalsearchJs.limitPageSize,null);
    totalsearchJs.filter();
}

var totalsearchGrid = {
    gridView : new RealGridJS.GridView("totalsearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        totalsearchGrid.initGrid();
        totalsearchGrid.initDataProvider();
        totalsearchGrid.event();
    },
    page : 1,
    initGrid : function () {
        totalsearchGrid.gridView.setDataSource(totalsearchGrid.dataProvider);
        totalsearchGrid.gridView.setStyles(totalsearchGridBaseInfo.realgrid.styles);
        totalsearchGrid.gridView.setDisplayOptions(totalsearchGridBaseInfo.realgrid.displayOptions);
        totalsearchGrid.gridView.setColumns(totalsearchGridBaseInfo.realgrid.columns);
        totalsearchGrid.gridView.setOptions(totalsearchGridBaseInfo.realgrid.options);
        totalsearchGrid.gridView.setCheckBar({ visible: false });
        totalsearchGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        totalsearchGrid.gridView.setDisplayOptions({
            "hideDeleteRows" : false,
            "display" : { "fitStyle" : "fill" },
            "indicator" : { "visible" : true },
            "checkBar" : { "visible" : false },
            "panel" : { "visible" : false},
            "stateBar" : { "visible" : false},
            "footer" : { "visible" : false},
            "edit" : { "editable" : false, "updatable" : false},
            "displayOptions" : { "columResizable" : false, "columnMovable" : false },
            "header" : { "styles" : { "background" : "#E0E0E0"}},
            "select" : [{ "style": "singleRow" },{ "singleMode": true, "lookupDisplay": true },{"showEmptyMessage":true, "emptyMessage":"검색 결과가 없습니다."}],
            "rowResizable" : false,
            "rowHeight": 80,
            "heightMeasurer" : "fixed",
            "hscrollBar": true
        });

        totalsearchGrid.gridView.onScrollToBottom = function(grid) {
            if(totalsearchGrid.dataProvider.getRowCount() > 7){ // 한화면에 그려지는 개수
                totalsearchJs.search(totalsearchGrid.page + 1, totalsearchJs.limitPageSize, null);
            }
        };
    },
    initDataProvider : function() {
        totalsearchGrid.dataProvider.setFields(totalsearchGridBaseInfo.dataProvider.fields);
        totalsearchGrid.dataProvider.setOptions(totalsearchGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        totalsearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            totalsearchGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        totalsearchGrid.dataProvider.clearRows();
        totalsearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = totalsearchGrid.dataProvider.getJsonRow(selectRowId);
        totalsearchJs.selectedIndex(rowDataJson);
    }
};

var weight = {
    /***
     * 내부 키워드 : 기본 점수
     * @returns {number}
     */
    searchKeyword : function() {
        return 1;
    },
    /***
     * 카테고리 랭킹 - 기본 점수
     * @returns {number}
     */
    categoryRankDefaultScore : function() {
        return 1000;
    },
    /***
     * 키워드 부스트 - 기본 점수
     * @returns {number}
     */
    searchKeywordBoost : function() {
        return 100;
    },
    /***
     * 유사도 점수
     * @param type
     * @returns {number}
     */
    getTokenTypeForScore : function(type) {

        switch(type.toString().toUpperCase()) {
            case "WORD" :
            case "UNIT" :
            case "KOREAN" :
                return 10;
            case "RELATEDNYM" :
                return 6;
            case "HYPONYM" :
                return 7;
            case "SYNONYM" :
                return 8;
            default:
                return 10;
        }
        return 10;
    },
    brandScore : function() {
        return 10;
    },
    /***
     * 카테고리 부스트 - 부스트 유형에 따른 순위별 점수 반환
     * @param boostType
     * @param scorer
     * @returns {string}
     */
    categoryRankScorerMatchScore : function(boostType, scorer) {
        var totalScore = 0;
        if(boostType == "PLUS") {
            if(scorer == 1) {
                totalScore = "+5,000";
            } else if(scorer == 2) {
                totalScore = "+4,000";
            } else if(scorer == 3) {
                totalScore = "+3,000";
            } else if(scorer == 4) {
                totalScore = "+2,000";
            } else {
                totalScore = "+1,000";
            }
        }else {
            if(scorer == 1) {
                totalScore = "-200";
            } else if(scorer == 2) {
                totalScore = "-400";
            } else if(scorer == 3) {
                totalScore = "-600";
            } else if(scorer == 4) {
                totalScore = "-800";
            } else {
                totalScore = "-1,000";
            }
        }
        return totalScore ;
    }
}
var utils = {
    getCommaCalc : function(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },

    makeSearchItemNmMatchedListString : function(res) {

        var resultText = "";

        var totalScore = 0;
        var result = res.admin.keywordAnalyze.result;
        for(var i = 0; i < result.length; i++) {
            var tokenArr = result[i].token.toLowerCase().replace(/ /g, '').split(":");
            var type = result[i].type;
            var expansion = result[i].expansion;
            var split_word = result.length - 1 == i ? "" : " <br> ";
            var matchedCnt = 0;
            switch (type) {
                case "KOREAN" :
                case "WORD" :
                    ///표제어 매칭 정보를 모두 찾는다.
                    for (var t = 0; t < tokenArr.length; t++) {
                        ///상품명 분해 정보에서 모두 찾는다.
                        for (var j = 0; j < res.searchItemNmTokenList.length; j++) {
                            var itemNm = res.searchItemNmTokenList[j].toLowerCase().replace(/ /g, '');
                            if (tokenArr[t] == itemNm) {
                                ++matchedCnt;
                                break;
                            }
                        }
                    }
                    ///표제어가 모두 매칭 되었다면?
                    if (matchedCnt >= tokenArr.length) {
                        totalScore += (weight.getTokenTypeForScore(type) * tokenArr.length);
                        resultText += "<h3 class=\"sub-title\" align=\"left\" style='margin-bottom:5px;'><span class=\"text\" style='margin: 10px 0 10px 0'>" + result[i].token + " <font color='#dc143c'>("+(weight.getTokenTypeForScore(type) * tokenArr.length)+")</font> " + split_word + "</span></h3>";
                    }
                    break;
            }

            if(matchedCnt < tokenArr.length) {
                ///표제어 매칭이 실패 하였다면 확장어 매칭 시도
                for(var j = 0; j < expansion.length; j++) {
                    var expMatchedCnt = 0;
                    var exp_type = expansion[j].type;
                    var exp_token_arr = expansion[j].token.toLowerCase().replace(/ /g, '').split(":");
                    for(var x = 0; x < exp_token_arr.length; x++) {
                        for (var o = 0; o < res.searchItemNmTokenList.length; o++) {
                            var itemNm = res.searchItemNmTokenList[o].toLowerCase().replace(/ /g, '');
                            if (exp_token_arr[x] == itemNm) {
                                ++expMatchedCnt;
                                break;
                            }
                        }
                    }
                    ///표제어가 모두 매칭 되었다면?
                    if (expMatchedCnt >= exp_token_arr.length) {
                        totalScore += (weight.getTokenTypeForScore(exp_type) * tokenArr.length);

                        var split_word = result.length - 1 == i ? "" : " <br> ";

                        switch (exp_type) {
                            case "SYNONYM" :
                                resultText += "<h3 class=\"sub-title\" align=\"left\" style='margin-bottom:5px;'><span class=\"text\"><font color=\"#6495ed\">" + expansion[j].token + " </font><font color='#dc143c'>("+(weight.getTokenTypeForScore(exp_type) * tokenArr.length)+")</font>"+split_word+"</span></h3>";
                                break;
                            case "HYPONYM" :
                                resultText += "<h3 class=\"sub-title\" align=\"left\" style='margin-bottom:5px;'><span class=\"text\"><font color=\"#8a2be2\">" + expansion[j].token + " </font><font color='#dc143c'>("+(weight.getTokenTypeForScore(exp_type) * tokenArr.length)+")</font>"+split_word+"</span></h3>";
                                break;
                            case "RELATEDNYM" :
                                resultText += "<h3 class=\"sub-title\" align=\"left\" style='margin-bottom:5px;'><span class=\"text\"><font color=\"#6b8e23\">" + expansion[j].token + " </font><font color='#dc143c'>("+(weight.getTokenTypeForScore(exp_type) * tokenArr.length)+")</font>"+split_word+"</span></h3>";
                                break;
                            case "UNIT" :
                                resultText += "<h3 class=\"sub-title\" align=\"left\" style='margin-bottom:5px;'><span class=\"text\"><font color=\"#f4a460\">" + expansion[j].token + " </font><font color='#dc143c'>("+(weight.getTokenTypeForScore(exp_type) * tokenArr.length)+")</font>"+split_word+"</span></h3>";
                                break;
                        }

                        break;
                    }
                }
            }
        }

        $("span[name=indexSearchItemNmMatchedListScore]").html("<font color=\""+((totalScore > 0) ? "#dc143c" : "")+"\">" + ((totalScore > 0) ? "+" : "") + utils.getCommaCalc(totalScore)+"점</font>");
        return resultText + "";

    },

    /***
     * 상품명
     * @param res
     * @returns {string}
     */
    makeSearchItemNmTokenListString : function(res) {
        var resultText = "";
        for(var i = 0; i < res.searchItemNmTokenList.length; i++) {
            resultText += res.searchItemNmTokenList[i] + ((res.searchItemNmTokenList.length -1 == i) ? "" : " / ");
        }

        return resultText;
    },

    /***
     * 브랜드(한글 + 한/영)
     * @returns {string}
     */
    makeBrandNmString: function(res, brandKeyword, brandTargetScoreField) {
        var totalScore = 0;
        if(brandKeyword == null || brandKeyword == undefined) {
            brandKeyword = "";
        }

        var text = "";
        var schKeyword = $("input[name=schKeyword]").val().replace(/\s/gi,"").toLowerCase();
        if(schKeyword == brandKeyword.replace(/\s/gi,"").toLowerCase()) {
            totalScore = 10;
            text = "<font color=\"#dc143c\">" + brandKeyword.replace(/\s/gi,"") + "</font>";
        } else {
            text += "<font color=\"\">" + brandKeyword.replace(/\s/gi,"") + "</font>";
        }

        return text;
    },
    /***
     * 내부키워드
     * @param res
     * @returns {string}
     */
    makeSearchKeywordString : function(res) {
        var text = "";
        var totalScore = 0;
        var searchItemNmScore = $("span[name=indexSearchItemNmMatchedListScore]").text();
        if(res.searchKeyword != null) {
            for(var i = 0; i < res.searchKeyword.length; i++) {
                if($.trim(res.searchKeyword[i]).replace(/\s/gi,"") == $.trim($("input[name=schKeyword]").val()).replace(/\s/gi,"") && searchItemNmScore == "0점") {
                    totalScore += weight.searchKeyword();
                    text += "<font color=\"#dc143c\">" + (res.searchKeyword[i].replace(/\s/gi,"") + "</font>" + ((res.searchKeyword.length-1 == i) ? "" : " / "));
                } else {
                    text += (res.searchKeyword[i].replace(/\s/gi,"") + ((res.searchKeyword.length-1 == i) ? "" : " / "));
                }
            }
        }

        return text;
    },
    /***
     * 카테고리
     * @param res
     * @returns {string}
     */
    makeCategoryListString: function(res) {
        var text = "";
        if(res.physicalCategory != null){
            if(res.physicalCategory.lcateCd !=null){
                text += res.physicalCategory.lcateNm+"("+res.physicalCategory.lcateCd+")"
            }
            if(res.physicalCategory.mcateCd !=null){
                text += " > "+res.physicalCategory.mcateNm+"("+res.physicalCategory.mcateCd+")"
            }
            if(res.physicalCategory.scateCd !=null){
                text += " > "+res.physicalCategory.scateNm+"("+res.physicalCategory.scateCd+")"
            }
            if(res.physicalCategory.dcateCd !=null){
                text += " > "+res.physicalCategory.dcateNm+"("+res.physicalCategory.dcateCd+")"
            }
        }

        $("span[name=indexCategoryList]").html(text);
        return text;
    },
    /***
     * 카테고리 랭킹
     * @param res
     * @returns {string}
     */
    makeCategoryBoostString: function(res) {
        var totalScore = 0;
        var text = "";
        var totalCategoryRankCount = 0;
        if(res.categoryBoostList != null) {
            for(var i = 0; i < res.categoryBoostList.length; i++) {
                var token = res.categoryBoostList[i].token;
                totalCategoryRankCount += res.categoryBoostList[i].ranks.length;
                if(res.categoryBoostList[i].ranks != null && res.categoryBoostList[i].ranks.length > 0) {
                    var categoryIds = ""
                    for(var k = 0; k < res.categoryBoostList[i].ranks.length; k++) {
                        totalScore += weight.categoryRankDefaultScore();
                        if(res.physicalCategory.lcateCd == res.categoryBoostList[i].ranks[k].categoryId
                            || res.physicalCategory.mcateCd == res.categoryBoostList[i].ranks[k].categoryId
                            || res.physicalCategory.scateCd == res.categoryBoostList[i].ranks[k].categoryId
                            || res.physicalCategory.dcateCd == res.categoryBoostList[i].ranks[k].categoryId) {

                            //totalScore
                            var scorer = res.categoryBoostList[i].ranks[k].scorer;
                            var boostType = res.categoryBoostList[i].ranks[k].boostType;

                            var operatedScore = Number(weight.categoryRankScorerMatchScore(boostType,scorer).replace("+","").replace("-","").replace(",",""));
                            if(boostType == "PLUS") {
                                totalScore += operatedScore;
                            } else if(boostType == "MINUS") {
                                totalScore -= operatedScore;
                            }

                            categoryIds +=  "<br>&nbsp;ㄴ&nbsp;"  + res.categoryBoostList[i].ranks[k].categoryNm + " - <font color='#dc143c'>" + res.categoryBoostList[i].ranks[k].categoryId + " (" + weight.categoryRankScorerMatchScore(boostType,scorer)+")</font>";
                        } else {
                            categoryIds += "<br>&nbsp;ㄴ&nbsp;"  + res.categoryBoostList[i].ranks[k].categoryNm + " - " + (res.categoryBoostList[i].ranks[k].categoryId);
                        }
                    }
                }
                text += "<font color=\"blue\">" + token + "</font> : " + categoryIds + (res.categoryBoostList.length -1 > i ? "<br><br>" : "");
            }
        }

        $("span[name=indexCategoryBoostScore]").html("<font color=\""+((totalScore > 0) ? "#dc143c" : "")+"\">"+ ((totalScore > 0) ? "+" : "")+ utils.getCommaCalc(totalScore)+"점</font>");
        return (text != "" ? "<font color='#dc143c'>기본점수 : +" + utils.getCommaCalc((totalCategoryRankCount * weight.categoryRankDefaultScore()) )+ "</font><br><br>" + text : "");
    },
    /***
     * 제외 카테고리
     * @param res
     * @param jsonRow
     */
    makeCategoryExclusiveString: function(res) {
        var text = "";
        if(res.categoryExclusiveList != null) {
            for(var i = 0; i < res.categoryExclusiveList.length; i++) {
                var token = res.categoryExclusiveList[i].token;
                if(res.categoryExclusiveList[i].ranks != null && res.categoryExclusiveList[i].ranks.length > 0) {
                    var categoryIds = "";
                    for(var k = 0; k < res.categoryExclusiveList[i].ranks.length; k++) {
                        if(res.physicalCategory.lcateCd == res.categoryExclusiveList[i].ranks[k].categoryId
                            || res.physicalCategory.mcateCd == res.categoryExclusiveList[i].ranks[k].categoryId
                            || res.physicalCategory.scateCd == res.categoryExclusiveList[i].ranks[k].categoryId
                            || res.physicalCategory.dcateCd == res.categoryExclusiveList[i].ranks[k].categoryId) {
                            categoryIds += "<font color='#dc143c'>" + res.categoryExclusiveList[i].ranks[k].categoryId + " (제외)</font>" + ((res.categoryExclusiveList[i].ranks.length-1 == k) ? "" : " / ");
                        } else {
                            categoryIds += res.categoryExclusiveList[i].ranks[k].categoryId + ((res.categoryExclusiveList[i].ranks.length-1 == k) ? "" : " / ");
                        }
                    }
                }
                text += token + " : " + categoryIds + (res.categoryBoostList.length -1 > i ? "<br>" : "");
            }
        }
        return text;
    },
    /***
     * 카테고리 유사어
     * @param res
     * @param jsonRow
     * @returns {string}
     */
    makeCategorySearchKeywordString: function(res) {
        var text = "";
        if (res.categorySearchKeyword != null) {
            for(var i = 0; i < res.categorySearchKeyword.split(';').length; i++) {
                var keyword = $.trim(res.categorySearchKeyword.split(';')[i].replace(/\s/gi,"").toLowerCase());
                var searchKeyword = $.trim($("input[name=schKeyword]").val()).toLowerCase().replace(/\s/gi,"");
                if(keyword == searchKeyword) {
                    text += ("<font color=\"#dc143c\">"+keyword+"</font>" + (res.categorySearchKeyword.split(';').length-1 == i ? "" : " / "));
                } else {
                    text += (keyword + (res.categorySearchKeyword.split(';').length-1 == i ? "" : " / "));
                }
            }
        }
        return text;
    },
    /***
     * 카테고리 세분류명
     * @param res
     * @param jsonRow
     */
    makeDcateNmString: function(res, jsonRow) {

        var matchedTokenMap = new Map();

        var originTokens = new Array();

        ///표제어 리스트와 확장어 리스트를 구한다.
        var result = res.admin.keywordAnalyze.result;
        for(var i = 0; i < result.length; i++) {

            switch(result[i].type) {
                case "KOREAN" :
                case "WORD" :
                    var tokenArr = result[i].token.toLowerCase().replace(/ /g, '').split(":");
                    for (var t = 0; t < tokenArr.length; t++) {
                        originTokens.push(tokenArr[t]);
                    }
                    break;
            }
        }

        var matchedTokenCnt = 0;
        for(var k = 0; k < originTokens.length; k++) {
            for(var i = 0; i < res.dcateNmTokenList.length; i++) {
                if(originTokens[k].toLowerCase() == res.dcateNmTokenList[i].toLowerCase()) {
                    matchedTokenMap.set(res.dcateNmTokenList[i].toLowerCase(),1);
                    ++matchedTokenCnt;
                }
                if(matchedTokenMap.get(res.dcateNmTokenList[i].toLowerCase()) == null) {
                    matchedTokenMap.set(res.dcateNmTokenList[i].toLowerCase(),0);
                }
            }
        }

        var i = 0;
        var text = "";
        matchedTokenMap.forEach(function(value, key) {
            if(Number(value) > 0) {
                text += "<font color=\"#dc143c\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            } else {
                text += "<font color=\"\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            }
            ++i;
        });

        return text;
    },
    /***
     * 행사 키워드
     * @param res
     * @param jsonRow
     * @returns {string}
     */
    makeEventKeywordListString: function(res) {
        var matchedTokenMap = new Map();
        var originTokens = new Array();

        ///표제어 리스트와 확장어 리스트를 구한다.
        var result = res.admin.keywordAnalyze.result;
        for(var i = 0; i < result.length; i++) {

            switch(result[i].type) {
                case "KOREAN" :
                case "WORD" :
                    var tokenArr = result[i].token.toLowerCase().replace(/ /g, '').split(":");
                    for (var t = 0; t < tokenArr.length; t++) {
                        originTokens.push(tokenArr[t]);
                    }
                    break;
            }
        }
        var matchedTokenCnt = 0;
        for(var k = 0; k < originTokens.length; k++) {
            for(var i = 0; i < res.eventKeywordList.length; i++) {
                if(originTokens[k].toLowerCase() == res.eventKeywordList[i].toLowerCase()) {
                    matchedTokenMap.set(res.eventKeywordList[i].toLowerCase(),1);
                    ++matchedTokenCnt;
                }
                if(matchedTokenMap.get(res.eventKeywordList[i].toLowerCase()) == null) {
                    matchedTokenMap.set(res.eventKeywordList[i].toLowerCase(),0);
                }
            }
        }

        var i = 0;
        var text = "";
        matchedTokenMap.forEach(function(value, key) {
            if(Number(value) > 0) {
                text += "<font color=\"#dc143c\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            } else {
                text += "<font color=\"\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            }
            ++i;
        });


        return text;
    },
    makeCategoryFilter : function(categoryList,categoryDepth,selectId){
        var selectTxt = "";
        if(categoryDepth==0)selectTxt += "<option value=''>"+"대분류"+"</option>";
        if(categoryDepth==1)selectTxt += "<option value=''>"+"중분류"+"</option>";
        if(categoryDepth==2)selectTxt += "<option value=''>"+"소분류"+"</option>";
        if(categoryDepth==3)selectTxt += "<option value=''>"+"세분류"+"</option>";
        for(var i=0; i<categoryList.length; i++){
            selectTxt += "<option value="+categoryList[i].id+">"+categoryList[i].name+"</option>"
        }
        utils.categoryReset(categoryDepth);
        $('#'+selectId).empty();
        $('#'+selectId).append(selectTxt);

    },
    categoryReset : function(categoryDepth) {
        if (categoryDepth == 0) {
            $("#schCateCd1").empty();
            $("#schCateCd1").append("<option value=''>" + "대분류" + "</option>");
            $("#schCateCd2").empty();
            $("#schCateCd2").append("<option value=''>" + "중분류" + "</option>");
            $("#schCateCd3").empty();
            $("#schCateCd3").append("<option value=''>" + "소분류" + "</option>");
            $("#schCateCd4").empty();
            $("#schCateCd4").append("<option value=''>" + "세분류" + "</option>");
        }
        if (categoryDepth == 1) {
            $("#schCateCd2").empty();
            $("#schCateCd2").append("<option value=''>" + "중분류" + "</option>");
            $("#schCateCd3").empty();
            $("#schCateCd3").append("<option value=''>" + "소분류" + "</option>");
            $("#schCateCd4").empty();
            $("#schCateCd4").append("<option value=''>" + "세분류" + "</option>");
        }
        if (categoryDepth == 2) {
            $("#schCateCd3").empty();
            $("#schCateCd3").append("<option value=''>" + "소분류" + "</option>");
            $("#schCateCd4").empty();
            $("#schCateCd4").append("<option value=''>" + "세분류" + "</option>");
        }
        if (categoryDepth == 3) {
            $("#schCateCd4").empty();
            $("#schCateCd4").append("<option value=''>" + "세분류" + "</option>");
        }
    },
    /***
     * 용도 옵션 매칭
     * @param res
     * @returns {string}
     */
    makeItemOptionNmListString : function(res) {

        let matchedTokenMap = new Map();
        let originTokens = new Array();
        let expansions = new Array();
        let result = res.admin.keywordAnalyze.result;

        for(let i = 0; i < result.length; i++) {
            let expansion = result[i].expansion;
            switch(result[i].type) {
                case "KOREAN" :
                case "WORD" :
                    let tokenArr = result[i].token.toLowerCase().replace(/ /g, '').split(":");
                    for (var t = 0; t < tokenArr.length; t++) {
                        originTokens.push(tokenArr[t]);
                    }
                    for(let j=0; j< expansion.length; j++) {
                        expansions.push(expansion[j].token.toLowerCase().replace(/ /g, '').split(":"));
                    }
                    break;
            }
            for(let k = 0; k < originTokens.length; k++) {
                for(var t = 0; t < res.itemOptionNmTokenList.length; t++) {
                    if(originTokens[k].toLowerCase() == res.itemOptionNmTokenList[t].toLowerCase()) {
                        matchedTokenMap.set(res.itemOptionNmTokenList[t].toLowerCase(),1);
                    }
                    // 표제어 매칭이 없을 경우 확장어와 매칭
                    if(matchedTokenMap.get(res.itemOptionNmTokenList[t].toLowerCase()) == null) {
                        for(var x = 0; x < expansions.length; x++) {
                            if (expansions[x] == res.itemOptionNmTokenList[t].toLowerCase()) {
                                matchedTokenMap.set(res.itemOptionNmTokenList[t].toLowerCase(),1);
                            }
                        }
                    }
                }
            }
        }
        // 질의어와 매칭되지 않은 용도옵션 리스트 값 설정
        for(var t = 0; t < res.itemOptionNmTokenList.length; t++) {
            if(matchedTokenMap.get(res.itemOptionNmTokenList[t].toLowerCase()) == null) {
                matchedTokenMap.set(res.itemOptionNmTokenList[t].toLowerCase(),0);
            }
        }

        var i = 0;
        var text = "";
        matchedTokenMap.forEach(function(value, key) {
            if(Number(value) > 0) {
                text += "<font color=\"#dc143c\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            } else {
                text += "<font color=\"\">" + key + "</font>"+  ((matchedTokenMap.size - 1 == i) ? "" : " / ");
            }
            ++i;
        });

        return text;
    }
};

var totalsearchJs = {
    init: function () {
        this.event();
    },
    limitPageSize : 100,
    categoryId: '',
    categoryDepth: '',
    categoryName: '',
    isFirstSearch: false,
    isFirstFilter: false,
    selectedIndex : function(jsonRow) {
        CommonAjax.basic(
            {
                url: '/search/dataManagement/totalSearch/item.json',
                data: {
                    docId : jsonRow.docId,
                    keyword: $("input[name=schKeyword]").val(),
                },
                cache: false,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    $("span[name=indexDocId]").html(res.docId); //문서번호
                    $("span[name=indexSearchItemNmTokenList]").html(utils.makeSearchItemNmTokenListString(res));    //검색상품명

                    $("span[name=indexSearchItemNmMatchedList]").html(utils.makeSearchItemNmMatchedListString(res));    //검색상품명(신규)

                    $("span[name=indexSearchKeyword]").html(utils.makeSearchKeywordString(res));            //내부검색어
                    $("span[name=indexCategoryBoost]").html(utils.makeCategoryBoostString(res));    //카테고리부스트
                    $("span[name=indexCategoryExclusive]").html(utils.makeCategoryExclusiveString(res)); //제외카테고리
                    $("span[name=indexCategorySearchKeyword]").html(utils.makeCategorySearchKeywordString(res)); //카테고리 유사어
                    $("span[name=indexBrandNmKor]").html(utils.makeBrandNmString(res,res.brandNmKor,"indexBrandNmKorScore"));  //브랜드 한글
                    $("span[name=indexBrandNmEng]").html(utils.makeBrandNmString(res,res.brandNmEng,"indexBrandNmEngScore"));  //브랜드 영문

                    $("span[name=indexDcateNm]").html(utils.makeDcateNmString(res,jsonRow)); //카테고리명 세분류

                    $("span[name=indexWeightCalc]").html(res.weightCalc);   //가중치 수식
                    $("span[name=indexWeightDay]").html(res.buyerCnt); // 24시간 구매자수
                    $("span[name=indexEventKeywordList]").html(utils.makeEventKeywordListString(res));    //행사 키워드(신규)
                    $("span[name=indexCategoryList]").html(utils.makeCategoryListString(res));       // 카테고리
                    $("span[name=indexShopNm]").html(res.shopNm);       // 파트너
                    $("span[name=indexItemOptionNm]").html(utils.makeItemOptionNmListString(res));       //용도옵션

                    //검색상품명 원형
                    var searchItemNmListArr = jsonRow.searchItemNm.split('|');
                    var searchItemNmListString = "";
                    for(var i = 0; i < searchItemNmListArr.length; i++) {
                        searchItemNmListString += searchItemNmListArr[i] + (((searchItemNmListArr.length-1) == i) ? "" : "</br>");
                    }
                    $("span[name=indexSearchItemNmList]").html(searchItemNmListString);
                    //상품번호
                    var linkHref = (jsonRow.storeType == "DS") ? "javascript:parent.initialTab.addTab('menu_1_1_1', '셀러상품 등록/수정', '/item/itemMain/DS');" : "javascript:parent.initialTab.addTab('menu_1_2_1', '점포상품 기본정보', '/item/itemMain/TD/basic');";
                    $("span[name=indexItemNo]").html("<a href = \""+linkHref+"\" style=\"text-decoration:underline;\"><font color='blue'>" + jsonRow.itemNo + "</font></a>");
                    //가중치
                    $("span[name=indexWeight]").html(jsonRow.weight);
                    //텀간거리가중치
                    var generals = res.admin.keywordAnalyze.generals;
                    var termScore = generals != null && generals.length > 0 ? "00" : jsonRow.score.split('.')[1];
                    if(termScore == "00"){
                        $("span[name=indexPosDistance]").html("0.00점");
                    }else{
                        $("span[name=indexPosDistance]").html("<font color='dc143c'>+0." + jsonRow.score.split('.')[1] + "점</font>");
                    }
                }
            });
    }
    ,
    initSearchDate : function(flag) {
    },
    filterReset: function(){
        /* 공통 필터 초기화 */
        filterJs.filterReset();
        $('#partnerList').html('');
        filterJs.falseCheckbox('delivery');
        $("#reKeyword").val('');
        utils.categoryReset(0);
        $('input[name=reKeyword_attr]').each(function(){
            filterJs.removeReKeyword($(this).val());
        });
        totalsearchJs.categoryId = '';
        totalsearchJs.categoryDepth = '';
    },
    reset : function() {
        /***
         * 검색결과 없을 시 초기화
         */
        $("span[name=indexDocId]").html('');
        $("span[name=indexItemNo]").html('');
        $("span[name=indexSearchItemNmList]").html('');
        $("span[name=indexSearchItemNmTokenList]").html('');
        $("span[name=indexSearchKeywordScore]").html('-');
        $("span[name=indexSearchKeyword]").html('');
        $("span[name=indexCategoryBoostScore]").html('-');
        $("span[name=indexCategoryBoost]").html('');
        $("span[name=indexCategoryExclusive]").html('');
        $("span[name=indexBrandNmKorScore]").html('-');
        $("span[name=indexBrandNmKor]").html('');
        $("span[name=indexBrandNmEngScore]").html('-');
        $("span[name=indexBrandNmEng]").html('');
        $("span[name=indexWeight]").html('-');
        $("span[name=indexWeightCalc]").html('');
        $("span[name=indexWeightDay]").html('');
        $("span[name=indexDcateNmScore]").html('-');
        $("span[name=indexDcateNm]").html('');
        $("span[name=indexSearchItemNmMatchedList]").html('');
        $("span[name=indexSearchItemNmMatchedListScore]").html('-');
        $("span[name=indexEventKeywordList]").html('');
        $("span[name=indexItemOptionNm]").html('');
        $("span[name=indexCategoryList]").html('');
        $("span[name=indexShopNm]").html('');
        $("span[name=indexCategorySearchKeyword]").html('');
    },
    delete : function() {
    },
    event: function () {
        $("#searchBtn").click(function() {
            totalsearchJs.filterReset();
            $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);
            if(totalsearchJs.search(1,totalsearchJs.limitPageSize,null)){
                totalsearchJs.isFirstFilter = false;
                totalsearchJs.filter();
        }
        });
        $("#reSearchBtn").click(function(){
            let reKeywordValue = $('#reKeyword').val();
            if(filterUtilJs.isEmpty(reKeywordValue)){
                alert('재검색어를 입력해주세요.');
                $('#reKeyword').focus();
            }else{
                $('#reKeyword').val('');

                /* 속성 부여 */
                let reKeywordAttr = "<input type='hidden' name='reKeyword_attr' value='" + reKeywordValue + "' display='" + reKeywordValue + "' />";
                $('body').append(reKeywordAttr);
                // draw
                $('input[name=reKeyword_attr]').each(function(){
                    filterJs.drawAttribute($(this), false);
                });
                totalsearchJs.search(1, totalsearchJs.limitPageSize, null);
            }
        });

        $("#copyQuery").click(function() {
            if($("#hidden_query").val() == null || $("#hidden_query").val() == '') {
                alert('검색시도 후 복사가 가능합니다.');
                return false;
            }
            copy_to_clipboard($("#hidden_query").val());
        });
        $("input[name=siteType]").click(function() {
            if($(this).val() == "HOME") {
                $("#storeIds").val("37");
            }else {
                $("#storeIds").val("38");
            }
        });

        $("input[name=storeSearchType]").click(function() {
            let searchType = $(this).val();
            totalsearchJs.setStoreCondition(searchType);
        });

        /* 초기화 버튼 */
        $('#resetBtn').click(function() {
            $("#keyword").val('');
            $("input[name=searchType]").val('');
            $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);
            totalsearchJs.filterReset();
            totalsearchJs.isFirstSearch = false;
            totalsearchJs.isFirstFilter = false;
            $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
            totalsearchJs.setStoreCondition('DIRECT');
        });

        /* 필터 이벤트 바인딩 */
        filterJs.bindEvent();

        /* 체크박스 */
        filterJs.bindCheckbox('delivery');
        filterJs.bindCheckbox('partner');

        $("#excelDownloadBtn").click(function() {
            totalsearchJs.excelDownload();
        });
    },
    write : function() {
    },
    searchDataSet: function() {
        const regexp = /^[(\d{1,10}),]*$/;
        /* init parameter - Essential*/
        let storeIds;
        let siteType;
        let keyword;

        /* set parameter */
        storeIds = $("input[name=storeIds]").val();
        siteType = $("input[name=siteType]:checked").val();

        /* parameter validate */
        if (storeIds == null || storeIds == ''){
            alert('점포를 입력해주세요.');
            return false;
        }
        if (storeIds != null && $.trim(storeIds) !== '' && !regexp.test(storeIds)) {
            alert('점포ID 형식이 올바르지 않습니다.');
            return false;
        }
        if(filterUtilJs.isNotEmpty($('#keyword').val())){
            keyword = $('#keyword').val();          //검색어
        }else{
            alert('검색어를 입력해주세요.');
            $('#keyword').focus();
            return false;
        }

        /* data return */
        return {
            storeIds: storeIds,
            siteType: siteType,
            categoryId: totalsearchJs.categoryId,
            categoryDepth: totalsearchJs.categoryDepth,
            keyword: keyword
        };
    },
    excelDownload: function() {
        totalsearchJs.search(1,3000,function(response) {
            var _date = new Date();
            var fileName = "통합검색_"+$("#profilesActive").val()+"_"+$("#keyword").val() + "_"+ $("input[name=siteType]:checked").val() + "_" + $("input[name=storeIds]").val().replace(/,/gi,"_") + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
            totalsearchGrid.gridView.exportGrid( {
                type: "excel",
                indicator: "default",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다.",
                //allColumns: true,
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                showColumns: [
                    "adultType",
                    "searchKeyword",
                    "weightBuyer",
                    "weightHour",
                    "weightDay",
                    "weightPrice",
                    "weightStoreType",
                    "weightOpenDay",
                    "weightShipMethod",
                    "weightPromotion",
                    "weightBenefit",
                    "weightBunddleSum",
                    "storeType",
                    "shopNm",
                    "eventItemYn",
                    "pickYn",
                    "togetherYn",
                    "giftYn",
                    "freeshipYn",
                    "regDt"
                ],
                hideColumns: ["img"]
            });

        });
    },
    search: function (offset, limit, callback) {
        let dataSet = totalsearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            Object.assign(dataSet, filterJs.filterDataSet(), {
                page:  offset,
                perPage: limit,
                explain: 'Y'
            });
        }
        if(filterUtilJs.isNotEmpty($('#searchType').val())){
            dataSet.searchType = $('#searchType').val();   // 검색유형 (NONE/TYPO/SUB/HANENG)
        }
        let reKeyword = '';
        $('input[name=reKeyword_attr]').each(function(){
            reKeyword = filterUtilJs.concat($(this).val(), ':', reKeyword);
        });
        if(filterUtilJs.isNotEmpty(reKeyword)){
            dataSet.reKeyword = reKeyword;     // 결과내 재검색어
        }
        CommonAjax.basic(
            {
                url: '/search/dataManagement/totalSearch/list.json',
                data: dataSet,
                cache: false,
                method: "GET",
                successMsg: null,
                callbackFunc: function (response) {
                    /* 최초검색 여부 설정 */
                    if(!totalsearchJs.isFirstSearch){
                        totalsearchJs.isFirstSearch = true;
                    }

                    var res = response.data;

                    //쿼리 저장
                    $("#hidden_query").val(JSON.stringify(res.query));

                    if(res.dataList != null && res.dataList.length > 0) {
                        totalsearchGrid.page += 1;
                    }

                    /***
                     * 형태소분석
                     */
                    var result = res.admin.keywordAnalyze.result;
                    $("span[name=analysisResultTxt]").html(totalsearchJs.getAnalysisResultString(result,"N"));

                    /***
                     * 범용 검색어 여부
                     */
                    var generals = res.admin.keywordAnalyze.generals;
                    $("span[name=generalTxt]").html(totalsearchJs.getGeneralResultString(generals));

                    /***
                     * 원본 검색어
                     */
                    var originalResult = res.admin.keywordAnalyze.originalResult;
                    $("span[name=analysisOriginalResultTxt]").html(totalsearchJs.getAnalysisResultString(originalResult,"Y"));

                    /***
                     * 금칙어
                     */
                    var filtered = res.admin.keywordAnalyze.filtered;
                    $("span[name=filteredTxt]").html(totalsearchJs.getAnalysisFilteredString(filtered));

                    /***
                     * 오타변환
                     */
                    $("span[name=typoTxt]").html(totalsearchJs.getAnalysisChangedKeywordString(res.search.type,res.search.changeKeyword,"TYPO"));

                    /***
                     * 한/영
                     */
                    $("span[name=hanEngTxt]").html(totalsearchJs.getAnalysisChangedKeywordString(res.search.type,res.search.changeKeyword,"HANENG"));

                    /***
                     * 대체검색어
                     */
                    $("span[name=subTxt]").html(totalsearchJs.getAnalysisChangedKeywordString(res.search.type,res.search.changeKeyword,"SUB"));

                    /***
                     * 검색 결과 수
                     */
                    $("span[name=totalCount]").html(utils.getCommaCalc(res.totalCount >= 3000 ? 3000 : res.totalCount));

                    /***
                     * 변환 된 검색어 저장
                     */
                    if(res.search.changeKeyword != null && $.trim(res.search.changeKeyword) != '') {
                        $("input[name=schKeyword]").val(res.search.changeKeyword);
                    } else {
                        $("input[name=schKeyword]").val(res.search.keyword);
                    }

                    /***
                     * 검색유형 저장
                     */
                    $("input[name=searchType]").val(res.search.type);

                    if(offset > 1) {
                        totalsearchGrid.dataProvider.fillJsonData(res.dataList,{fillMode: "append"});
                    }
                    else
                    {
                        totalsearchGrid.setData(res.dataList);
                        totalsearchGrid.page = 1;
                        if(totalsearchGrid.dataProvider.getRowCount() > 0) {
                            totalsearchJs.selectedIndex(totalsearchGrid.dataProvider.getJsonRow(0));
                        } else {
                            totalsearchJs.reset(); //검색 결과가 없을 시 초기화
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback(response);
                    }
                }
            });
        return true;
    },
    filter : function(){
        let dataSet = totalsearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            dataSet.explain = 'N';
        }

        let categoryDepth = totalsearchJs.categoryDepth;
        let categorySelected = "";
        if(categoryDepth !=null && categoryDepth != ''){
            if(categoryDepth ==1){
                categorySelected = 'schCateCd2';
            }else if (categoryDepth == 2){
                categorySelected = 'schCateCd3';
            }else if(categoryDepth == 3){
                categorySelected = 'schCateCd4';
            }
        } else{
            categoryDepth = 0;
            categorySelected = 'schCateCd1';
        }
        CommonAjax.basic(
            {
                url: '/search/dataManagement/totalSearch/filter.json',
                async : false,
                data: dataSet,
                cache: false,
                method: "GET",
                successMsg: null,
                callbackFunc: function (response) {
                    let data = response.data;
                    let categoryList = categoryDepth == 0 ? data.categoryList : data.categorySubList;
                    if(categoryDepth != 4 && categoryList != null && categoryList.length > 0) {
                        utils.makeCategoryFilter(categoryList,categoryDepth,categorySelected);
                    }
                    if(totalsearchJs.isFirstFilter === false && filterUtilJs.isNotEmpty(data.brandList)){
                        $('#brandList').html(filterJs.drawCheckbox(data.brandList, 'brand'));
                    }
                    if(totalsearchJs.isFirstFilter === false && filterUtilJs.isNotEmpty(data.partnerList)){
                        $('#partnerList').html(filterJs.drawCheckbox(data.partnerList, 'partner'));
                    }
                }
            });
        totalsearchJs.isFirstFilter = true;
    },
    getAnalysisChangedKeywordString : function(correctedType, correctedText, getType) {
        if(correctedType == getType) {
            return "<font color=''>" + correctedText + "</font>";
        }
        return "";
    },
    /***
     * 금칙어 생성
     * @param filtered
     * @returns {string}
     */
    getAnalysisFilteredString : function(filtered) {
        var filteredString = "";
        if(filtered != null && filtered.length > 0){
            for(var i = 0; i < filtered.length; i++) {
                filteredString += "<font color='#dc143c'>"+filtered[i]+"</font>" + ((filtered.length -1  == i) ? "" : " / ");
            }
        }
        return filteredString;
    },
    /***
     * 형태소분석 결과 생성
     * @param result
     * @returns {string}
     */
    getAnalysisResultString : function(result,originYn) {
        var resultText = "<h3 class=\"sub-title\" align=\"left\">";
        for(var i = 0; i < result.length; i++) {
            var returnString = "";
            var token = result[i].token;
            var type = result[i].type;
            var expansion = result[i].expansion;
            switch(type) {
                case "KOREAN" :
                case "WORD" :
                case "GENERAL" :
                    returnString += "<span class=\"text\">" + token + "</span> <br>";
                    break;
            }
            if(originYn == "Y"){
                for(var j = 0; j < expansion.length; j++) {
                    switch(expansion[j].type) {
                        case "SYNONYM" :
                            returnString += '&nbsp;&nbsp; - <span class=\"text\"><font color="#6495ed">' + expansion[j].token + '</font></span> <br>';
                            break;
                        case "RELATEDNYM" :
                            returnString += "&nbsp;&nbsp; - <span class=\"text\"><font color=\"#6b8e23\">" + expansion[j].token + "</font></span> <br>";
                            break;
                        case "HYPONYM" :
                            returnString += "&nbsp;&nbsp; - <span class=\"text\"><font color=\"#8a2be2\">" + expansion[j].token + "</font></span> <br>";
                            break;
                        case "UNIT" :
                            returnString += "&nbsp;&nbsp; - <span class=\"text\"><font color=\"#f4a460\">" + expansion[j].token + "</font></span> <br>";
                            break;
                    }
                }
            }
            resultText += returnString + "<br>";
        }
        return resultText + "</h3>";
    },
    getGeneralResultString : function (generals) {
        var resultText = "<h3 class=\"sub-title\" align=\"left\">";

        if(generals != null && generals.length > 0) {
            for (var i = 0; i < generals.length; i++) {
                resultText += generals[i] + ((generals.length -1  == i) ? "" : " / ");
            }
        }

        return resultText + "</h3>";
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=HYPER"
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },

    setStoreCondition : function (storeSearchType){
        if(storeSearchType == 'DIRECT'){
            $("#storeIds").val("37");
            $("#storeIds").attr("readonly",false);
            $("#schStoreNm").hide();
            $("#schStoreBtn").hide();
            $("#directText").show();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }else { // 'SEARCH'
            $("#storeIds").val("");
            $("#storeIds").attr("readonly",true);
            $("#schStoreNm").val("");
            $("#schStoreNm").show();
            $("#schStoreBtn").show();
            $("#directText").hide();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }
    }

};

