function is_ie() {
    if(navigator.userAgent.toLowerCase().indexOf("chrome") != -1) return false;
    if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) return true;
    if(navigator.userAgent.toLowerCase().indexOf("windows nt") != -1) return true;
    return false;
}

function copy_to_clipboard(str) {
    if( is_ie() ) {
        window.clipboardData.setData("Text", str);
        alert("복사되었습니다.");
        return;
    }
    prompt("Ctrl+C를 눌러 복사하세요.", str);
}
var newItemJs = {
    init : function() {
        this.initCategorySelectBox();
        this.bindingEvent();
    },
    limitPageSize : function() {
        return 100;
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        CommonAjax.basic({url:'/item/category/getGroupList.json?schType=GCATENM&schKeyword=&schUseYn=Y', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                for(var i = 0; i < res.length; i++) {
                    $("#schCategory option:eq("+i+")").after("<option value=\""+res[i].gcateCd+"\" name=\""+res[i].gcateNm+"\">"+res[i].gcateNm+"</option>");
                }
            }});
    },
    selectedIndex : function(jsonRow) {
        $("span[name=indexDocId]").html(jsonRow.docId);             //문서번호
        $("span[name=indexWeight]").html(jsonRow.weight);           //가중치
        $("span[name=indexWeightCalc]").html(jsonRow.weightCalc);   //가중치 수식
        $("span[name=indexWeightDay]").html(jsonRow.weightDay);     //24시간판매자수
    },
    reset : function() {
    },
    search : function (offset, limit, callback) {
        var storeIds = $("input[name=storeIds]").val();
        var siteType = $("input[name=siteType]:checked").val();
        var regexp = /^[(\d{1,10}),]*$/;
        if (storeIds == null || storeIds == ''){
            alert('점포를 입력해주세요.');
            return false;
        }
        if (storeIds != null && $.trim(storeIds) != '' && !regexp.test(storeIds)) {
            alert('점포ID 형식이 옳바르지 않습니다.');
            return false;
        }
        if (storeIds.split(",").length > 3) {
            alert('점포ID는 (,)쉼표 구분해서 최대 3개까지만 입력 가능합니다.');
            $("input[name=storeIds]").focus();
            return false;
        }
        var categoryId = $("#schCategory option:selected").val();

        CommonAjax.basic(
            {
                url: '/search/dataManagement/newItem/list.json',
                data: {
                    page: offset,
                    perPage: limit,
                    storeIds: storeIds,
                    siteType: siteType,
                    categoryId: categoryId
                },
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {

                    if(res.data.dataList != null && res.data.dataList.length > 0) {
                        newItemGrid.page += 1;
                    }

                    if(offset > 1) {
                        newItemGrid.dataProvider.fillJsonData(res.data.dataList,{fillMode: "append"});
                    }
                    else
                    {
                        newItemGrid.setData(res.data.dataList);
                        newItemGrid.page = 1;
                        if(newItemGrid.dataProvider.getRowCount() <= 0) {
                            newItemJs.reset(); //검색 결과가 없을 시 초기화
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback(res);
                    }
                }
            }
        );
    },
    bindingEvent : function() {
        $('#searchBtn').click(function() {
            newItemJs.search(1,newItemJs.limitPageSize,null);
        });
        $("#copyQuery").click(function() {
            if($("#hidden_query").val() == null || $("#hidden_query").val() == '') {
                alert('검색시도 후 복사가 가능합니다.');
                return false;
            }
            copy_to_clipboard($("#hidden_query").val());
        });
        $("input[name=siteType]").click(function() {
            if($(this).val() == "HOME") {
                $("#storeIds").val("37");
            }else {
                $("#storeIds").val("38");
            }
        });
        $("#excelDownloadBtn").click(function() {
            newItemJs.excelDownload();
        });
        /* 초기화 버튼 */
        $('#resetBtn').click(function() {
            $("#schCategory").val('0');
            $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
            newItemJs.setStoreCondition('DIRECT');
        });
        $("input[name=storeSearchType]").click(function() {
            var searchType = $(this).val();
            newItemJs.setStoreCondition(searchType);
        });
    },
    excelDownload: function() {
        newItemJs.search(1,1000,function(res) {
            var _date = new Date();
            var fileName = "신규상품_"+ $("#profilesActive").val() + "_"+$("#schCategory option:selected").attr("name") + "_"+ $("input[name=siteType]:checked").val() + "_" + $("input[name=storeIds]").val().replace(/,/gi,"_") + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate() + "_" + _date.getHours() + "_" + _date.getMinutes();
            newItemGrid.gridView.exportGrid({
                type: "excel",
                indicator: "visible",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다.",
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                showColumns: [
                    "adultType",
                    "weightBuyer",
                    "weightHour",
                    "weightDay",
                    "weightPrice",
                    "weightStoreType",
                    "weightOpenDay",
                    "weightShipMethod",
                    "weightPromotion",
                    "weightBenefit",
                    "weightBunddleSum",
                    "storeType",
                    "eventItemYn",
                    "pickYn",
                    "togetherYn",
                    "giftYn",
                    "freeshipYn",
                    "regDt"
                ],
                hideColumns: ["img"]
            });
        })
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=HYPER"
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },

    setStoreCondition : function (storeSearchType){
        if(storeSearchType == 'DIRECT'){
            $("#storeIds").val("37");
            $("#storeIds").attr("readonly",false);
            $("#schStoreNm").hide();
            $("#schStoreBtn").hide();
            $("#directText").show();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }else { // 'SEARCH'
            $("#storeIds").val("");
            $("#storeIds").attr("readonly",true);
            $("#schStoreNm").val("");
            $("#schStoreNm").show();
            $("#schStoreBtn").show();
            $("#directText").hide();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }
    }
};
var utils = {
    getCommaCalc: function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
};

var newItemGrid = {
    gridView : new RealGridJS.GridView("newItemGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        newItemGrid.initGrid();
        newItemGrid.initDataProvider();
        newItemGrid.event();
    },
    page : 1,
    initGrid : function () {
        newItemGrid.gridView.setDataSource(newItemGrid.dataProvider);

        newItemGrid.gridView.setStyles(newItemGridBaseInfo.realgrid.styles);
        newItemGrid.gridView.setDisplayOptions(newItemGridBaseInfo.realgrid.displayOptions);
        newItemGrid.gridView.setColumns(newItemGridBaseInfo.realgrid.columns);
        newItemGrid.gridView.setOptions(newItemGridBaseInfo.realgrid.options);
        newItemGrid.gridView.setCheckBar({ visible: false });
        newItemGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        newItemGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            },
            rowHeight: 80
        });
        newItemGrid.gridView.onScrollToBottom = function(grid) {
            var currentPage = newItemGrid.page + 1;
            newItemJs.search(currentPage,newItemJs.limitPageSize(),null,$("input[name=searchType]").val());
        };
    },
    initDataProvider : function() {
        newItemGrid.dataProvider.setFields(newItemGridBaseInfo.dataProvider.fields);
        newItemGrid.dataProvider.setOptions(newItemGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        newItemGrid.gridView.onDataCellClicked = function(gridView, index) {
            newItemGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        newItemGrid.dataProvider.clearRows();
        newItemGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = newItemGrid.dataProvider.getJsonRow(selectRowId);
        newItemJs.selectedIndex(rowDataJson);
    }
};