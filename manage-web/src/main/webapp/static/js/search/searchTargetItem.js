/**
 * 날짜포맷
 * @param f
 * @returns {string|void|*}
 */
Date.prototype.format = function (f) {
    if (!this.valueOf()) return " ";
    let weekKorName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    let weekKorShortName = ["일", "월", "화", "수", "목", "금", "토"];
    let weekEngName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let weekEngShortName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    let d = this;
    return f.replace(/(yyyy|yy|MM|dd|KS|KL|ES|EL|HH|hh|mm|ss|a\/p)/gi, function ($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear(); // 년 (4자리)
            case "yy": return (d.getFullYear() % 1000).zf(2); // 년 (2자리)
            case "MM": return (d.getMonth() + 1).zf(2); // 월 (2자리)
            case "dd": return d.getDate().zf(2); // 일 (2자리)
            case "KS": return weekKorShortName[d.getDay()]; // 요일 (짧은 한글)
            case "KL": return weekKorName[d.getDay()]; // 요일 (긴 한글)
            case "ES": return weekEngShortName[d.getDay()]; // 요일 (짧은 영어)
            case "EL": return weekEngName[d.getDay()]; // 요일 (긴 영어)
            case "HH": return d.getHours().zf(2); // 시간 (24시간 기준, 2자리)
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2); // 시간 (12시간 기준, 2자리)
            case "mm": return d.getMinutes().zf(2); // 분 (2자리)
            case "ss": return d.getSeconds().zf(2); // 초 (2자리)
            case "a/p": return d.getHours() < 12 ? "오전" : "오후"; // 오전/오후 구분
            default: return $1;
        }
    });
};

String.prototype.string = function (len) { let s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };


String.prototype.replaceAll = function(org, dest) {
    return this.split(org).join(dest);
};

let redFontStart = "<span style='font-weight: bold;' class=\"text-red\">";
let redFontEnd = "</span>";

var targetItemJs = {
    setInit: function() {
        $('#storehyper').removeClass("gray-dark");
        $('#storehyper').addClass("cyan");
        $('input[name=itemNo]').val('');
        $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
        $("#storeType").val('HYPER').prop("selected",true);
        targetItemJs.setStoreCondition('HYPER','DIRECT');
        this.event();
    },
    event : function() {
        $("input[name=storeSearchType]").click(function() {
            var storeType = $("#storeType option:selected").val();
            var searchType = $("input:radio[name='storeSearchType']:checked").val();
            targetItemJs.setStoreCondition(storeType,searchType);
        });
        $("#storeType").change(function(){
            var storeType = $("#storeType option:selected").val();
            var searchType = $("input:radio[name='storeSearchType']:checked").val();
            targetItemJs.setStoreCondition(storeType,searchType);
        });
    },
    getItemList: function () {
        /* Set Selector */
        let itemNoSelector = 'input[name=itemNo]';
        let storeIdSelector = 'input[name=storeId]';

        /* Parameter Validation */
        let regexp = /^[0-9]*$/;
        if($(itemNoSelector).val() === ''){
            alert('상품번호를 입력해야 합니다.');
            return false;
        }else{
            /* replace blank - 빈값으로 인한 잘못된 url 이슈 처리 */
            $(itemNoSelector).val($(itemNoSelector).val().replace(/ /g,''));
            if(!regexp.test($(itemNoSelector).val())){
                alert('상품번호는 숫자로만 구성되어야 합니다.');
                return false;
            }
        }
        if($(storeIdSelector).val() === ''){
            alert('점포번호를 입력해야 합니다.');
            return false;
        }
        CommonAjax.basic({
            url: '/search/dataManagement/searchTargetItem/getCrawlerTarget.json',
            method: 'get',
            dataType: 'json',
            data: $('#searchForm').serializeObject(true),
            successMsg: null,
            callbackFunc: function (res) {
                if(res.itemNo == null){
                    targetItemJs.setNoResult();
                }else{
                    targetItemJs.setViewResult(res);
                }
            }
        });
    },
    setNoResult: function(){
        $('#search_result').hide();
        $('#no_search_result').show();
    },
    setViewResult: function(res){
        $('#no_search_result').hide();

        /* 상품기본정보 */
        $('#itemNo').html(res.itemNo);
        $('#itemNm').html(res.itemNm);
        $('#img').attr("src", res.imageFrontDomainPrefix+"/it/"+res.itemNo+"s0120");
        $('#onlineYn').html(res.onlineYn);
        $('#cartLimitYn').html(res.cartLimitYn);

        /* 상품기본정보 정책 */
        targetItemViewPolicy.setValueYn(res.dispYn, 'dispYn');
        targetItemViewPolicy.setItemStatus(res);
        targetItemViewPolicy.setOptSelInfo(res);
        targetItemViewPolicy.setAdultInfo(res);
        targetItemViewPolicy.setCategory(res);
        targetItemViewPolicy.setExpCategory(res);
        targetItemViewPolicy.setValueYn(res.imgMainYn, 'imgMainYn');

        /* 상품 판매 정보 */
        targetItemViewPolicy.setSaleDateInfo(res);
        targetItemViewPolicy.setRsvInfo(res);
        targetItemViewPolicy.setStockInfo(res);

        /* 점포정보 */
        $('#storeNm').html(res.storeNm);
        targetItemViewPolicy.setStoreId(res);
        targetItemViewPolicy.setVirtualStoreOnlyYn(res);
        targetItemViewPolicy.setPriceInfo(res);
        targetItemViewPolicy.setStopDealInfo(res);
        targetItemViewPolicy.setShipTypeInfo(res);
        targetItemViewPolicy.setShipPolicyInfo(res);
        targetItemViewPolicy.setStoreUseYn(res);
        targetItemViewPolicy.setPfrYn(res);

        /* 상품 기타 정보 */
        targetItemViewPolicy.setItemGroupInfo(res);
        targetItemViewPolicy.setStickerInfo(res);
        targetItemViewPolicy.setPromoInfo(res);
        targetItemViewPolicy.setEventInfo(res);
        targetItemViewPolicy.setCrossOutYn(res);
        targetItemViewPolicy.setRelationInfo(res);

        /* Data Show */
        $('#search_result').show();
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        var storeType = $('#storeType').val();


        if ($.jUtil.isEmpty(storeType)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=" + storeType
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },

    setStoreCondition : function (storeType, storeSearchType){
        if(storeType == 'DS'){
            if(storeSearchType == 'DIRECT'){
                $("#storeIds").val("0");
                $("#storeIds").attr("readonly",true);
                $("input:radio[name=storeSearchType]").attr("disabled",true);
            }else { // 'SEARCH'
                $("#storeIds").val("0");
                $("#storeIds").attr("readonly",true);
                $("#schStoreNm").hide();
                $("#schStoreBtn").hide();
                $("input:radio[name=storeSearchType]").attr("disabled",true);
            }
        }else { // 'HYPER/EXP'
            var storeId = '';
            if(storeType == 'HYPER'){
                storeId = '37';
            }else {
                storeId = '515'
            }
            if(storeSearchType == 'DIRECT'){
                $("#storeIds").val(storeId);
                $("#storeIds").attr("readonly",false);
                $("#schStoreNm").hide();
                $("#schStoreBtn").hide();
                $("input:radio[name=storeSearchType]").attr("disabled",false);
            }else { // 'SEARCH'
                $("#storeIds").val("");
                $("#storeIds").attr("readonly",true);
                $("#schStoreNm").val("");
                $("#schStoreNm").show();
                $("#schStoreBtn").show();
                $("input:radio[name=storeSearchType]").attr("disabled",false);
            }
        }
    }
};


const targetItemViewPolicy = {
    setCategory: function(res){
      let text = [];
      if(res.category === null || res.category === undefined){
          text.push(redFontStart + '해당 정보가 없습니다.' + redFontEnd);
      }else{
          text.push(res.category);
      }
      $('#category').html(text.join(''));
    },
    setExpCategory: function(res){
        if(res.storeType !== 'EXP'){
            $('#trExpCategory').hide();
            return;
        }else{
            $('#trExpCategory').show();
        }
        let text = [];
        if(res.expCategory === null || res.expCategory === undefined){
            text.push(redFontStart + '해당 정보가 없습니다.' + redFontEnd);
        }else{
            text.push(res.expCategory);
        }
        $('#expCategory').html(text.join(''));
    },
    setStoreId: function(res) {
        let text = [];
        text.push(res.storeId);
        switch(res.storeKind){
            case 'NOR':
                text.push(' (일반점)'); break;
            case 'DLV':
                text.push(' (택배점)'); break;
        }
        $('#storeId').html(text.join(''));
    },
    setItemStatus: function (res) {
        let text = [];
        switch (res.itemStatus) {
            case 'T':
                text.push(redFontStart + '임시저장' + redFontEnd); break;
            case 'A':
                text.push('판매중'); break;
            case 'P':
                text.push(redFontStart + '일시중지' + redFontEnd); break;
            case 'S':
                text.push(redFontStart + '영구중지' + redFontEnd); break;
        }

        $('#itemStatus').html(text.join(""));
    },
    setOptSelInfo: function(res) {
        if(res.storeType === 'DS'){
            $('#trOptSelInfo').hide();
        }else{
            $('#trOptSelInfo').show();
        }
        let text = [];
        text.push('용도옵션 [ ' + res.optSelUseYn);
        text.push(' ] / 리스트분할노출 [ ' + res.optListSplitDispYn + ' ]');

        $('#optSelInfo').html(text.join(""));
    },
    setAdultInfo: function(res) {
        let text = [];
        text.push('유형 ');
        switch(res.adultType){
            case 'NORMAL':
                text.push('[ 일반 ]'); break;
            case 'ADULT':
                text.push('[ 성인 ]'); break;
            case 'LOCAL_LIQUOR':
                text.push('[ 전통주 ]'); break;
        }
        text.push(' / 이미지 노출 여부 [ ' + res.imgDispYn + ' ]');

        $('#adultInfo').html(text.join(""));
    },
    setSaleDateInfo: function(res) {
        let text = [];
        text.push('판매기간 설정 여부 [ ' + res.salePeriodYn + ' ]');
        if(res.salePeriodYn !== 'Y'){
            $('#saleDateInfo').html(text.join(""));
            $('#nowDate').html('');
        }else{
            let saleRange = '판매기간 ' + targetItemUtils.getStrDetailDateKr(res.saleStartDt) + ' ~ ' + targetItemUtils.getStrDetailDateKr(res.saleEndDt);
            text.push('<br/>');
            if(targetItemUtils.dateToEpoch(res.saleEndDt) > targetItemUtils.dateToEpochNow()){
                text.push(saleRange);
            }else{
                text.push(redFontStart + saleRange + redFontEnd);
            }
            $('#saleDateInfo').html(text.join(''));
            $('#nowDate').html('[ 현재 날짜 ' + targetItemUtils.getNowDateKr() + ' ]');
        }
    },
    setRsvInfo: function(res) {
        let text = [];
        if(res.rsvSaleType != null && res.rsvSaleStartDt != null && res.rsvSaleEndDt != null){
            if(res.rsvSaleType === 'DRCT_DLV'){
                text.push('명절선물 ');
            }else if(res.rsvSaleType === 'DRCT'){
                text.push('절임배추 ');
            }
            text.push(res.rsvSaleStartDt + ' ~ ' + res.rsvSaleEndDt);
        }else{
            text.push('N');
        }
        $('#rsvInfo').html(text.join(""));
    },
    setStockInfo: function(res) {
        let text = [];

        text.push('<pre class="pre-font">');
        if(res.policySoldOut === 'Y'){
            text.push('품절');
        }else{
            text.push('판매가능')
        }
        text.push('\n');
        if(res.stockOnlineYn !== 'Y'){
            text.push('    - 재고관리유형 [ ' + (res.stockType === 'N' ? '관리안함' : (res.stockType === 'OFF' ? '오프라인' : '온라인')) + ' ]');
            text.push(' / ');
            text.push('FC 상태구분 [ ' + res.fcStatusFlag + ' ]');
            if(res.storeType !== 'DS' && res.stockLoc !== null){
                text.push('\n');
                text.push('    - 오프라인 Loc 번호 [ ' + res.stockLoc + ' ]');
                text.push(' / ');
                text.push('오프라인 재고수량 [ ' + res.stockQty + ' ]');
                text.push(' / ');
                text.push('안전 재고수량 [ ' + res.stockSafetyQty + ' ]');
            }
        }else{
            text.push('    - 온라인재고설정 [ 설정 ]');
            text.push(' / ');
            text.push('재고남은수량 [ ' + res.remainCnt + ' ]');
        }
        text.push('</pre>');
        if(res.policySoldOut === 'Y'){
            $('#stockInfo').html(redFontStart + text.join("") + redFontEnd);
        }else{
            $('#stockInfo').html(text.join(""));
        }
    },
    setPriceInfo: function(res) {
        let text = [];
        text.push('판매가격 [ '+res.salePrice+' ]');
        if (res.rmsCrossOutYn == 'Y') {
            text.push(' / 심플행사할인가격 [ ' + res.policyRmsPrice + ' ]');
        }
        text.push(' / 할인가격 [ '+res.policyDcPrice+' ]');
        switch(res.policyDcType){
            case 'CARD_DISCOUNT_PRICE':
                text.push(', 카드할인번호 [ '+res.cardDiscountNo+' ]'); break;
            case 'ITEM_DISCOUNT_PRICE':
                text.push(', 쿠폰할인번호 [ '+res.couponDiscountNo+' ]'); break;
            case 'BASIC_DISCOUNT_PRICE':
                text.push(', RMS할인번호 [ '+res.rmsEventNo +' ]'); break;
        }
        $('#priceInfo').html(text.join(""));
    },
    setStopDealInfo: function(res) {
        let text = [];
        text.push('취급중지 여부 [ ' + res.stopDealYn + ' ]');
        if(res.stopDealYn !== 'Y'){
            $('#stopDealInfo').html(text.join(""));
        }else{
            text.push(' / 중지제한여부 [ ' + res.stopLimitYn + ' ]<br/>');
            let stopRange = '중지기간 ';
            if(res.stopLimitYn === 'N'){
                stopRange += '무제한';
            }else {
                stopRange += targetItemUtils.getStrDateKr(res.stopStartDt) + ' ~ ' + targetItemUtils.getStrDateKr(res.stopEndDt);
            }
            if(res.stopLimitYn === 'Y'
                && targetItemUtils.dateToEpoch(res.stopStartDt) > targetItemUtils.dateToEpochNow()
                && targetItemUtils.dateToEpochNow() > targetItemUtils.dateToEpoch(res.stopEndDt)
            ){
                text.push(stopRange);
            }else{
                text.push(redFontStart + stopRange + redFontEnd);
            }
            $('#stopDealInfo').html(text.join(''));
        }
    },
    setShipTypeInfo: function(res){
        if(res.storeType === 'DS'){
            $('#trShipTypeInfo').hide();
        }else{
            $('#trShipTypeInfo').show();
        }
        let text = [];
        if(res.storeType === 'EXP'){
            text.push('퀵여부 [ '+ res.quickYn +' ]');
            if(res.quickYn === 'Y'){
                $('#shipTypeInfo').html(text.join(""));
            }else{
                $('#shipTypeInfo').html(redFontStart + text.join("") + redFontEnd);
            }
        }else{
            text.push('자차배송여부 [ '+res.drctYn+' ], 택배여부 [ '+res.dlvYn+' ], 픽업여부 [ '+res.pickYn+' ]');
            if(res.drctYn === 'Y' || res.dlvYn === 'Y' || res.pickYn === 'Y'){
                $('#shipTypeInfo').html(text.join(""));
            }else{
                $('#shipTypeInfo').html(redFontStart + text.join("") + redFontEnd);
            }
        }


    },
    setShipPolicyInfo: function(res){
        let text = [];
        if(res.shipPolicyNo == null){
            text.push('N');
        }else{
            text.push('<pre class="pre-font">');
            text.push('배송정책번호 [ '+res.shipPolicyNo+' ]\n');
            text.push('    - 배송비종류 [ ');
            switch(res.shipKind){
                case 'FREE':
                    text.push('무료'); break;
                case 'FIXED':
                    text.push('유료'); break;
                case 'COND':
                    text.push('조건부 무료'); break;
            }
            text.push(' ], 배송유형 [ ');
            switch(res.shipType){
                case 'ITEM':
                    text.push('상품별'); break;
                case 'BUNDLE':
                    text.push('묶음배송'); break;
            }
            text.push(' ], 무료조건비 [ ' + res.shipFreeCondition + ' ]');
            text.push(', 배송비노출여부 [ ' + res.shipDispYn + ' ]');
            text.push(', 출고기한 [ ');
            if(res.shipReleaseDay === "-1"){
                text.push('미정]');
            }else{
                text.push(res.shipReleaseDay+' ]');
            }
            if(res.shipReleaseDay === "1"){
                text.push(', 출고시간 [ ' + res.shipReleaseTime + ' ]');
            }
            text.push('</pre>');
        }
        $('#shipPolicyInfo').html(text.join(""));
    },
    setItemGroupInfo: function(res){
        if(res.storeType === 'DS'){
            $('#trItemGroupInfo').hide();
        }else{
            $('#trItemGroupInfo').show();
        }
        let text = [];
        if(res.groupNo !== 'N'){
            if(res.groupRepresentYn === 'Y'){
                text.push('묶음대표상품');
            }else{
                text.push('묶음상품');
            }
            text.push(' / 그룹번호 [ ' + res.groupNo + ' ]');
        }else{
            text.push('N');
        }
        $('#itemGroupInfo').html(text.join(""));
    },
    setStickerInfo: function(res){
        let text = [];
        if(res.pcStickerNo != null){
            text.push('PC [ ' + res.pcStickerNo + ' ] ');
        }
        if(res.mobileStickerNo != null){
            text.push('MOBILE [ ' + res.mobileStickerNo + ' ]')
        }
        $('#stickerInfo').html(text.join(""));
    },
    setPromoInfo: function(res){
      let text = [];
      $.each(res.promoList, function(idx, item){
          if(item.promoNo === 'N'){
              text.push('N');
          }else{
              text.push('프로모션번호 [ '+item.promoNo+' ], 테마번호 [ '+item.themeNo+' ]');
              text.push(', 전시기간 [ '+targetItemUtils.getStrDateKr(item.dispStartDt)+' ~ '+targetItemUtils.getStrDateKr(item.dispEndDt) + ' ]');
              if(Object.keys(res.promoList).length - 1 > idx){
                  text.push('<br/>');
              }
          }
      });
      $('#promoInfo').html(text.join(""));
    },
    setEventInfo: function(res){
        if(res.storeType === 'DS'){
            $('#trEventInfo').hide();
        }else{
            $('#trEventInfo').show();
        }
      let text =  [];
      if(res.rmsEventType !== 'N'){
          text.push('RMS [ ');
          switch (res.rmsEventType) {
              case 'BASIC':
                  text.push('기본할인'); break;
              case 'PICK':
                  text.push('골라담기'); break;
              case 'TOGETHER':
                  text.push('함께할인(M)'); break;
              case 'INTERVAL':
                  text.push('함께할인(T)'); break;
          }
          text.push('('+res.rmsEventCd+') ]');
      }

      if(res.ersEventType !== 'N'){
          if(text.join("") !== ""){
              text.push(' / ');
          }
          text.push('ERS [ ');
          switch (res.ersEventType) {
              case '10':
                  text.push('사은행사'); break;
              case '20':
                  text.push('응모행사'); break;
          }
          text.push('('+res.ersEventCd+') ]');
      }

      if(text.join('') === ''){
          $('#eventInfo').html('N');
      }else{
          $('#eventInfo').html(text.join(''));
      }
    },
    setCrossOutYn: function(res) {
        if(res.storeType === 'DS' || res.rmsEventType != 'BASIC'){
            $('#trCrossOutYnInfo').hide();
        }else{
            $('#trCrossOutYnInfo').show();
            $('#crossOutYn').html(res.rmsCrossOutYn);
        }
    },
    setRelationInfo: function(res){
        if(res.storeType !== 'DS'){
            $('#trRelationInfo').hide();
        }else{
            $('#trRelationInfo').show();
        }
        let text = [];
        if(res.relationNo !== 'N'){
            text.push('연관번호 [ '+ res.relationNo + ' ]');
        }else{
            text.push('N');
        }
        $('#relationInfo').html(text.join(''));
    },
    setStoreUseYn: function(res){
        if(res.storeType === 'DS'){
            $('#trStoreInfo').hide();
        }else{
            $('#trStoreInfo').show();
        }
        targetItemViewPolicy.setValueYn(res.storeUseYn, 'storeUseYn');
        targetItemViewPolicy.setValueYn(res.storeOnlineYn, 'storeOnlineYn');
    },
    setVirtualStoreOnlyYn: function(res){
        let text = [];
        if(res.storeKind === 'NOR' && res.virtualStoreOnlyYn === 'Y'){
            text.push(redFontStart + res.virtualStoreOnlyYn + redFontEnd);
        }else{
            text.push(res.virtualStoreOnlyYn);
        }
        $('#virtualStoreOnlyYn').html(text.join(''));
    },
    setPfrYn: function(res){
        let value = res.pfrYn;
        if(res.storeType === 'DS'){
            $('#pfrYnEssential').hide();
            $('#pfrYn').html(value);
        }else{
            $('#pfrYnEssential').show();
            targetItemViewPolicy.setValueYn(value, 'pfrYn');
        }
    },
    setValueYn: function(value, id){
        $('#'+id).html(value === 'Y' ? value : redFontStart + value + redFontEnd);
    }
};

const targetItemUtils = {
    dateToEpoch(dateStr){
        if(dateStr === '' || dateStr == null){
            return '';
        }
        return Math.round(new Date(dateStr).getTime() / 1000.0);
    },
    dateToEpochNow(){
        return Math.round(new Date().getTime() / 1000.0);
    },
    getNowDateKr(){
        return new Date().format('yyyy년 MM월 dd일');
    },
    getStrDateKr(dateStr){
        if(dateStr === '' || dateStr == null){
            return '';
        }
        return new Date(dateStr).format('yyyy년 MM월 dd일');
    },
    getStrDetailDateKr(dateStr){
        if(dateStr === '' || dateStr == null){
            return '';
        }
        return new Date(dateStr).format('yyyy년 MM월 dd일 HH:mm:ss');
    }
};