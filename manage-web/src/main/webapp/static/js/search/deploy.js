
var deployGrid = {
    gridView : new RealGridJS.GridView("deployGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        deployGrid.initGrid();
        deployGrid.initDataProvider();
        deployGrid.event();
    },
    initGrid : function() {
        deployGrid.gridView.setDataSource(deployGrid.dataProvider);

        deployGrid.gridView.setStyles(deployGridBaseInfo.realgrid.styles);
        deployGrid.gridView.setDisplayOptions(deployGridBaseInfo.realgrid.displayOptions);
        deployGrid.gridView.setColumns(deployGridBaseInfo.realgrid.columns);
        deployGrid.gridView.setOptions(deployGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        deployGrid.dataProvider.setFields(deployGridBaseInfo.dataProvider.fields);
        deployGrid.dataProvider.setOptions(deployGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        deployGrid.gridView.onDataCellClicked = function(gridView, index) {
            deploy.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        deployGrid.dataProvider.clearRows();
        deployGrid.dataProvider.setRows(dataList);
    }
};

Date.prototype.yyyymmddhhmm = function()
{
    var YYYY = this.getFullYear().toString();
    var MM = (this.getMonth() + 1).toString();
    var DD = this.getDate().toString();
    var hh = this.getHours().toString();
    var mm = this.getMinutes().toString();
    var ss = this.getSeconds().toString();

    return YYYY + "-" + (MM[1] ? MM : '0'+MM[0]) + "-" + (DD[1] ? DD : '0'+DD[0]) + " " + (hh[1] ? hh : '0' + hh[0]) + ":" + (mm[1] ? mm : '0' + mm[0]) + ":" + (ss[1] ? ss : '0' + ss[0]);
}


var totaldeploy = 0;
var totalrows = 0;
var useNcount = 0;

var deploy = {
    /***
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initAjaxForm();
    },
    /***
     * 등록/수정
     */
    initAjaxForm : function() {
        $('#deplpySetForm').ajaxForm({
            url: '/search/deploy/setItem.json',
            type: 'post',
            success: function(resData) {
                if(resData.returnStatus == 200 && resData.returnCode == "SUCCESS") {
                    deploy.searchServerList();
                }

                alert(resData.returnMessage);
            },
            error: function(e) {
                alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
            }
        });
    },
    /***
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#dictionaryBuildDeployBtn").bindClick(deploy.dictionaryBuildAndDeploy);
        $("#dictionaryBuildBtn").bindClick(deploy.dictionaryBuild);
        $("#schServiceCd").change(function() {
            if($("#schAppType option:selected").val() == 'API' && $(this).val() == 'HOMEPLUS') {
                $("#deployDictType").prop("disabled","");
            } else {
                $("#deployDictType option:eq(0)").prop("selected", true);
                $("#deployDictType").prop("disabled","disabled");
            }
            deploy.searchServerList();
        });
        $("#schAppType").change(function() {
            if($(this).val() == 'API' && $("#schServiceCd option:selected").val() == 'HOMEPLUS') {
                $("#deployDictType").prop("disabled","");
            } else {
                $("#deployDictType option:eq(0)").prop("selected", true);
                $("#deployDictType").prop("disabled","disabled");
            }
            deploy.searchServerList();
        });
        $("#resetBtn").bindClick(deploy.resetForm);
        $('#setBtn').bindClick(deploy.setItem);
        $("#dictionaryDeployBtn").bindClick(deploy.dictionaryDeploy);
    },
    searchServerList: function() {
        CommonAjax.basic({cache: false, url:'/search/deploy/getServerList.json?' + $('#deployForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                deployGrid.setData(res.data);
                deploy.gridRowSelect(0);
            }});

        $('#serviceCd option').prop("selected",false).filter(function() {return this.value == $('#schServiceCd option:selected').val();}).prop('selected', true);
    },
    /***
     * 사전 빌드
     */
    dictionaryBuild : function() {

        if($("#schServiceCd option:selected").val() == 'SUGGEST') {
            alert('자동완성 서비스는 사전빌드를 할 수 없습니다.');
            return false;
        }

        CommonAjax.basic({cache: false, url:'/search/deploy/makeDictionaryFromTxt.json?' + $('#deployForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res_make) {
                if(res_make.returnStatus == 200) {
                    alert('사전빌드가 완료되었습니다.');
                } else {
                    alert('사전빌드가 실패하였습니다.\n관리자에게 문의해주세요.');
                }
            }});
    },
    /***
     * 사전 빌드 및 배포
     */
    dictionaryBuildAndDeploy : function() {

        if($("#schAppType option:selected").val() == "") {
            alert('배포 대상을 선택하세요.');
            return false;
        }

        CommonAjax.basic({cache: false, url:'/search/deploy/downloadsAsTxt.json?' + $('#deployForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res_download) {
                if(res_download.returnStatus == 200) {
                    CommonAjax.basic({cache: false, url:'/search/deploy/makeDictionaryFromTxt.json?' + $('#deployForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res_make) {
                            if(res_make.returnStatus == 200) {
                                CommonAjax.basic({cache: false, url:'/search/deploy/uploadDictionary.json?' + $('#deployForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res_upload) {
                                        if(res_upload.returnStatus == 200) {
                                            deploy.dictionaryDeploy();
                                        } else {
                                            alert('사전 업로드가 실패하였습니다.\n관리자에게 문의해주세요.');
                                        }
                                    }});
                            } else {
                                alert('사전 생성이 실패하였습니다.\n관리자에게 문의해주세요.');
                            }
                        }});
                } else {
                    alert('사전 백업이 실패하였습니다.\n관리자에게 문의해주세요.');
                }
            }});
    },
    checkSuccessDeployStatus : function(totalRow,start) {
        if(start) {
            totaldeploy = 0;
            totalrows = totalRow;
            useNcount = 0;
        }
        else {
            ++totaldeploy;
            if(totaldeploy == (totalrows - useNcount)) {
                var rows = deployGrid.dataProvider.getJsonRows();
                for(var i = 0; i < rows.length; i++) {
                    if(rows[i].appType.toUpperCase() == 'NODE') {
                        CommonAjax.basic({
                            url:'/search/deploy/search_analyzer_reload.json?domain=' + escape(rows[i].privateDomain) + '&schServiceCd=' + $('#schServiceCd option:selected').val(),
                            data:null,
                            cache: false,
                            method:"GET",
                            successMsg:null,
                            callbackFunc:function(res) {

                            }});
                        break;
                    }
                }

                alert('배포가 완료 되었습니다.');
                totalrows = 0;
            }
        }
    },
    /***
     * 사전 배포
     */
    dictionaryDeploy : function() {

        if($("#schServiceCd option:selected").val() == 'SUGGEST') {
            alert('자동완성 서비스는 사전배포를 할 수 없습니다.');
            return false;
        }

        if($("#schAppType option:selected").val() == "") {
            alert('배포 대상을 선택하세요.');
            return false;
        }

        if($("#schServiceCd option:selected").val() == 'ADDRESS' && $("#schAppType option:selected").val() == 'API') {
            alert('주소검색 서비스는 NODE 형식에만 배포가 가능 합니다.');
            return false;
        }

        var rows = deployGrid.dataProvider.getJsonRows();
        deploy.checkSuccessDeployStatus(rows.length,true);
        for(var i = 0; i < rows.length; i++) {
            //row 단위 순차 배포
            if(rows[i].useYn == "N") {
                useNcount += 1;
                continue;
            }
            CommonAjax.basic({
                url:'/search/deploy/deploy.json?deployId=' + rows[i].seqNo + '&rowId=' + i + '&deployDictType=' + $("#deployDictType option:selected").val(),
                data:null,
                cache: false,
                method:"GET",
                successMsg:null,
                callbackFunc:function(res) {
                    if(res.returnStatus != 200 || res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                    } else {
                        var values = {
                            seqNo: res.data.seqNo,
                            serviceCd: res.data.serviceCd,
                            privateDomain: res.data.privateDomain,
                            appType: res.data.appType,
                            useYn: res.data.useYn,
                            createDt:res.data.createDt,
                            lastDeployDt: (new Date()).yyyymmddhhmm(),
                            status: '성공'
                        };
                        deployGrid.dataProvider.updateRows(res.data.rowId, [values]);
                        deploy.checkSuccessDeployStatus(null,false);
                    }
                }});
        }
    },
    gridRowSelect : function(selectRowId) {

        if(deployGrid.dataProvider.getRowCount() == 0) {
            $('#seqNo').val('');
            $('#appType option:eq(0)').prop("selected",true);
            $('#useYn option:eq(0)').prop("selected",true);
            $('#privateDomain').val('');
        } else {
            var rowDataJson = deployGrid.dataProvider.getJsonRow(selectRowId);

            $('#seqNo').val(rowDataJson.seqNo);
            $('#appType option').prop("selected",false).filter(function() {return this.value == rowDataJson.appType;}).prop('selected', true);
            $('#useYn option').prop("selected",false).filter(function() {return this.value == rowDataJson.useYn;}).prop('selected', true);
            $('#privateDomain').val(rowDataJson.privateDomain);
        }


    },
    resetForm : function() {
        $('#seqNo').val('');
        $('#appType option:eq(0)').prop("selected",true);
        $('#serviceCd option:eq(0)').prop("selected",true);
        $('#useYn option:eq(0)').prop("selected",true);
        $('#privateDomain').val('');
    },
    setItem : function() {
        if(!deploy.valid.setItem()) {
            return false;
        }
        $('#deplpySetForm').submit();
    },
};

deploy.valid = {
    setItem : function() {
        var privateDomain = $('#privateDomain').val();

        if (privateDomain == "" || privateDomain.length < 1) {
            alert('내부도메인을 입력해주세요.');
            privateDomain.focus();
            return false;
        }

        return true;
    }
};

$(document).ready(function() {
    deploy.init();
    CommonAjaxBlockUI.global();
    deployGrid.init();
    deploy.searchServerList();
});