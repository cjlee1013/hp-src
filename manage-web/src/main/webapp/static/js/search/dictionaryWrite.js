
var dictionaryWrite = {
    init : function(){
        wordListGrid.initGrid();
        wordListGrid.initDataProvider();
        wordListGrid.event();
    },
    clear : function() {
        location.href=location.href;
    },
    goBackList : function() {
        location.href='/search/dictionary/wordList/' + $("#wordName").val() + '.json';
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    event : function() {
        ///초기화
        $("#resetBtn").click(function(){
            dictionaryWrite.goBackList();
        });

        ///수정
        $("#editItemBtn").click(function() {

            if($("input[name=dicType]:checked").length == 0) {
                alert('사전 유형은 하나이상 선택해주세요.');
                return;
            }
            if($("select[name=pos] option:selected").val() == 'ALL') {
                alert('품사정보를 선택해주세요.');
                return;
            }

            var dicTypeList = new Array();
            $("input[name=dicType]:checked").each(function(){
                dicTypeList.push($(this).val());
            });

            var nounAttrList = new Array();
            $("input[name=nounAttr]:checked").each(function(){
                nounAttrList.push($(this).val());
            });

            var neTypeList = new Array();
            $("input[name=neType]:checked").each(function(){
                neTypeList.push($(this).val());
            });

            var domainList = new Array();
            $("input[name=domain]:checked").each(function(){
                domainList.push($(this).val());
            });

            var languageList = new Array();
            $("input[name=language]:checked").each(function(){
                languageList.push($(this).val());
            });

            var isGeneral = "N";
            $("input[name=isGeneral]:checked").each(function(){
                isGeneral = "Y";
            });

            var useFlag = false;
            $("input[name=useFlag]:checked").each(function(){
                useFlag = true;
            });

            var stopFlag = false;
            $("input[name=stopFlag]:checked").each(function(){
                stopFlag = true;
            });

            var requestData = {};
            requestData.wordName = $("#wordName").val();
            requestData.senseTag = $("#senseTag").val();
            requestData.dicType = dicTypeList;
            requestData.pos = $("#pos option:selected").val();
            requestData.nounAttr = nounAttrList;
            requestData.verbAttr = $("#verbAttr option:selected").val();
            requestData.neType = neTypeList;
            requestData.domain = domainList;
            requestData.language = languageList;
            requestData.note =$("#note").val();
            requestData.description = $("#description").val();
            requestData.example =$("#example").val();
            requestData.isGeneral = isGeneral;
            requestData.useFlag = useFlag;
            requestData.stopFlag = stopFlag;

            CommonAjax.basic({ url:'/search/dictionary/'+$('#wordId').val()+'/updateWordInfo.json',
                data: JSON.stringify(requestData),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg : null,
                callbackFunc : function(res) {
                    if(res != 1)
                    {
                        alert('수정에 실패하였습니다.');
                    }
                    else
                    {
                        alert('수정에 성공하였습니다.');
                        for(var i = 0; i < dicTypeList.length; i++) {
                            if(dicTypeList[i] == 'COMPOUNDS') {
                                var url = '/search/popup/dictionary/compoundsInfo/' + $('#wordId').val();
                                dictionaryWrite.windowOpen(url, 1132, 731, 'compounds');
                            } else if(dicTypeList[i] == 'PREANALYSIS') {
                                var url = '/search/popup/dictionary/preAnalysisInfo/' + $('#wordId').val();
                                dictionaryWrite.windowOpen(url, 1132, 731, 'preanalysis');
                            }
                        }
                        if($('#popup').val() == "true") {
                            if(typeof(opener) != "undefined") {
                                try {
                                    parent.opener.reSearch();
                                    self.close();
                                }catch (e) {
                                }
                            }
                        }
                        else{
                            if($('#direct').val() == "direct") {
                                location.href = location.href; //reset
                            }
                            else {
                                location.href='/search/dictionary/wordList/' + $("#wordName").val() + '.json';
                            }
                        }
                    }
                }});
        });

        ///등록
        $("#setItemBtn").click(function(){
            var wordName = $("#wordName").val();
            if($.trim(wordName) == '')
            {
                alert('키워드를 입력하세요.');
                return;
            }
            if($("input[name=dicType]:checked").length == 0) {
                alert('사전 유형은 하나이상 선택해주세요.');
                return;
            }
            if($("select[name=pos] option:selected").val() == 'ALL') {
                alert('품사정보를 선택해주세요.');
                return;
            }

            var dicTypeList = new Array();
            $("input[name=dicType]:checked").each(function(){
                dicTypeList.push($(this).val());
            });

            var nounAttrList = new Array();
            $("input[name=nounAttr]:checked").each(function(){
                nounAttrList.push($(this).val());
            });

            var neTypeList = new Array();
            $("input[name=neType]:checked").each(function(){
                neTypeList.push($(this).val());
            });

            var domainList = new Array();
            $("input[name=domain]:checked").each(function(){
                domainList.push($(this).val());
            });

            var languageList = new Array();
            $("input[name=language]:checked").each(function(){
                languageList.push($(this).val());
            });

            var isGeneral = "N";
            $("input[name=isGeneral]:checked").each(function(){
                isGeneral = "Y";
            });

            var useFlag = false;
            $("input[name=useFlag]:checked").each(function(){
                useFlag = true;
            });

            var stopFlag = false;
            $("input[name=stopFlag]:checked").each(function(){
                stopFlag = true;
            });

            var data = {};
            data.wordName = $("#wordName").val();
            data.senseTag = $("#senseTag").val();
            data.dicType = dicTypeList;
            data.pos = $("#pos option:selected").val();
            data.nounAttr = nounAttrList;
            data.verbAttr = $("#verbAttr option:selected").val();
            data.neType = neTypeList;
            data.domain = domainList;
            data.language = languageList;
            data.note = $("#note").val();
            data.description = $("#description").val();
            data.example = $("#example").val();
            data.isGeneral = isGeneral;
            data.useFlag = useFlag;
            data.stopFlag = stopFlag;

            CommonAjax.basic({ url:'/search/dictionary/insertWordInfo.json',
                data:JSON.stringify(data),
                contentType: 'application/json',
                type:'post',
                method:"POST",
                successMsg : null,
                callbackFunc : function(res) {
                    if(res < 0)
                    {
                        alert('저장에 실패하였습니다.');
                    }
                    else
                    {
                        alert('저장에 성공하였습니다.');
                        for(var i = 0; i < dicTypeList.length; i++) {
                            if(dicTypeList[i] == 'COMPOUNDS') {
                                var url = '/search/popup/dictionary/compoundsInfo/' + res;
                                dictionaryWrite.windowOpen(url, 1132, 731, 'compounds');
                            } else if(dicTypeList[i] == 'PREANALYSIS') {
                                var url = '/search/popup/dictionary/preAnalysisInfo/' + res;
                                dictionaryWrite.windowOpen(url, 1132, 731, 'preanalysis');
                            }
                        }
                        if($('#popup').val() == "true") {
                            if(typeof(opener) != "undefined") {
                                try {
                                    parent.opener.reSearch();
                                    self.close();
                                }catch (e) {
                                }
                            }
                        }
                        else{
                            if($('#direct').val() == "direct") {
                                location.href = location.href; //reset
                            }
                            else {
                                location.href='/search/dictionary/wordList/' + $("#wordName").val() + '.json';
                            }
                        }
                    }
                }});
        });

        ///동형어 조회
        $("#searchBtn").click(function(){
            var searchKeyword = $("#wordName").val();
            if($.trim(searchKeyword) == '')
            {
                alert('키워드를 입력하세요.');
                return;
            }

            var data = {};
            data.wordName = searchKeyword;
            CommonAjax.basic({url:'/search/dictionary/getSrchWordInfo.json', data:JSON.stringify(data),                     type: 'post',
                contentType: 'application/json',
                method:"POST", successMsg:null, callbackFunc:function(res) {
                    wordListGrid.setData(res);
                }});

            $("#groupSearch").show();
        });
    }
};

var wordListGrid = {
    gridView : new RealGridJS.GridView("wordListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        wordListGrid.initGrid();
        wordListGrid.initDataProvider();
        wordListGrid.event();
    },
    initGrid : function () {
        wordListGrid.gridView.setDataSource(wordListGrid.dataProvider);

        wordListGrid.gridView.setStyles(wordListGridBaseInfo.realgrid.styles);
        wordListGrid.gridView.setDisplayOptions(wordListGridBaseInfo.realgrid.displayOptions);
        wordListGrid.gridView.setColumns(wordListGridBaseInfo.realgrid.columns);
        wordListGrid.gridView.setOptions(wordListGridBaseInfo.realgrid.options);
        wordListGrid.gridView.setCheckBar({ visible: false });
    },
    initDataProvider : function() {
        wordListGrid.dataProvider.setFields(wordListGridBaseInfo.dataProvider.fields);
        wordListGrid.dataProvider.setOptions(wordListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        wordListGrid.gridView.onDataCellClicked = function(gridView, index) {
            wordListGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        wordListGrid.dataProvider.clearRows();
        wordListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = wordListGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
    }
};
