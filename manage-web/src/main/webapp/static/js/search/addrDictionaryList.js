
/***
 * 어휘 조회 리스트 그리드 설정
 * @type {{init: dictionaryListGrid.init, setData: dictionaryListGrid.setData, initDataProvider: dictionaryListGrid.initDataProvider, excelDownload: dictionaryListGrid.excelDownload, dataProvider: *, initGrid: dictionaryListGrid.initGrid, event: dictionaryListGrid.event, gridView: *}}
 */
var dictionaryListGrid = {
    gridView : new RealGridJS.GridView("dictionaryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dictionaryListGrid.initGrid();
        dictionaryListGrid.initDataProvider();
        dictionaryListGrid.event();
    },
    page : 0,
    initGrid : function () {
        dictionaryListGrid.gridView.setDataSource(dictionaryListGrid.dataProvider);

        dictionaryListGrid.gridView.setStyles(dictionaryListGridBaseInfo.realgrid.styles);
        dictionaryListGrid.gridView.setDisplayOptions(dictionaryListGridBaseInfo.realgrid.displayOptions);
        dictionaryListGrid.gridView.setColumns(dictionaryListGridBaseInfo.realgrid.columns);
        dictionaryListGrid.gridView.setOptions(dictionaryListGridBaseInfo.realgrid.options);
        dictionaryListGrid.gridView.setCheckBar({ visible: false });
        dictionaryListGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > dictionaryListGrid.page) {
                dictionaryListGrid.page += 50;
                dictionaryList.search(dictionaryListGrid.dataProvider.getRowCount(),dictionaryList.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        dictionaryListGrid.dataProvider.setFields(dictionaryListGridBaseInfo.dataProvider.fields);
        dictionaryListGrid.dataProvider.setOptions(dictionaryListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dictionaryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dictionaryListGrid.gridRowSelect(index.dataRow)
        };
        $('#wordName').calcTextLength('keyup', '#textCountKr_wordName');
    },
    setData : function(dataList) {
        dictionaryListGrid.dataProvider.clearRows();
        dictionaryListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = dictionaryListGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
        $("#wordName").val(rowDataJson.wordName);
        dictionaryList.selectedIndex(rowDataJson);
        dictionaryList.getSynonym(rowDataJson.wordId);
    },
    excelDownload: function() {
        if(dictionaryListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "사전_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        dictionaryListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var synonymListGrid = {
    gridView : new RealGridJS.GridView("synonymListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        synonymListGrid.initGrid();
        synonymListGrid.initDataProvider();
        synonymListGrid.event();
    },
    initGrid : function () {
        synonymListGrid.gridView.setDataSource(synonymListGrid.dataProvider);

        synonymListGrid.gridView.setStyles(synonymListGridBaseInfo.realgrid.styles);
        synonymListGrid.gridView.setDisplayOptions(synonymListGridBaseInfo.realgrid.displayOptions);
        synonymListGrid.gridView.setColumns(synonymListGridBaseInfo.realgrid.columns);
        synonymListGrid.gridView.setOptions(synonymListGridBaseInfo.realgrid.options);
        synonymListGrid.gridView.setCheckBar({ visible: false });
        synonymListGrid.gridView.setSelectOptions({
            style: 'singleRow'
        });


        synonymListGrid.gridView.onDataCellDblClicked = function (grid, index) {
            synonymListGrid.dataProvider.removeRow(index.dataRow);
            /*const purchaseOrderNo = orderUtil.getRealGridColumnValue(orderManageGird, "주문번호", index.dataRow);
            orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )*/
        }
    },
    initDataProvider : function() {
        synonymListGrid.dataProvider.setFields(synonymListGridBaseInfo.dataProvider.fields);
        synonymListGrid.dataProvider.setOptions(synonymListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        synonymListGrid.gridView.onDataCellClicked = function(gridView, index) {
            synonymListGrid.gridRowSelect(index.dataRow)
        };
        $('#synonymName').calcTextLength('keyup', '#textCountKr_synonymName');
    },
    setData : function(dataList) {
        synonymListGrid.dataProvider.clearRows();
        synonymListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = synonymListGrid.dataProvider.getJsonRow(selectRowId);
        synonymListGrid.selectedIndex(rowDataJson);
    },
    selectedIndex : function(jsonRow) {
        $('#wordName').prop('readonly', true);
        //$('#synonymName').val(jsonRow.synonymName);
        //$("#textCountKr_synonymName").html(jsonRow.synonymName.length);
    }
};

var dictionaryList = {
    init : function () {
    },
    limitPageSize : function() {
        return 100;
    },
    bindingEvent : function() {
        $('#searchBtn').click(function(){
            dictionaryList.search(0,dictionaryList.limitPageSize());
        });
        $('#saveBtn').bindClick(dictionaryList.write);
        $('#editBtn').bindClick(dictionaryList.modify);
        $('#resetBtn').bindClick(dictionaryList.reset);
        $('#synonymSaveBtn').bindClick(dictionaryList.synonymWrite);
        $('#synonymName').keydown(function(e) {
            if (e.keyCode == 13) {
                dictionaryList.synonymWrite();
                return;
            }
        });
    },
    selectedIndex : function(jsonRow) {
        $('#wordId').val(jsonRow.wordId);

        //입력폼 데이터 생성
        $("#wordName").val(jsonRow.wordName);
        $("#useFlag").val(jsonRow.useFlag).prop("selected",true);
        $("#textCountKr_wordName").html(jsonRow.wordName.length);
    },
    ///어휘등록
    write : function() {
        var data = {};
        data.wordName = $('#wordName').val();
        data.useFlag = $("#useFlag option:selected").val();
        data.synonyms = synonymListGrid.dataProvider.getJsonRows();

        CommonAjax.basic(
            {
                url:'/search/addr-dictionary/insertNounInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(res != 1) {
                        alert(res);
                    }else{
                        dictionaryList.search(0,dictionaryList.limitPageSize(),data.wordName);
                    }
                }
            });

    },
    //동의어 추가
    synonymWrite : function() {

        for(var i =0; i < synonymListGrid.dataProvider.getRowCount(); i++) {
            if(synonymListGrid.dataProvider.getJsonRow(i).synonymName == $.trim($('#synonymName').val())) {
                alert('이미 존재하는 동의어 입니다.');
                return false;
            }
        }

        if($.trim($('#synonymName').val()) == "") {
            return false;
        }

        var values = [$.trim($('#synonymName').val())];
        synonymListGrid.dataProvider.addRow(values);
        return false;
    },
    ///어휘수정
    modify : function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('수정하려는 명사를 먼저 선택해주세요.');
            return;
        }

        var data = {};
        data.wordName = $('#wordName').val();
        data.useFlag = $("#useFlag option:selected").val();
        data.synonyms = synonymListGrid.dataProvider.getJsonRows();

        CommonAjax.basic(
            {
                url:'/search/addr-dictionary/'+$("#wordId").val()+'/updateNounInfo.json',
                data: JSON.stringify(data),
                type: 'put',
                contentType: 'application/json',
                method:"PUT",
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(res != 1) {
                        alert(res);
                    }else{
                        dictionaryList.search(0,dictionaryList.limitPageSize(),data.wordName);
                    }
                }
            });
    },
    reset : function() {
        $('#wordId').val('');
        $('#wordName').val('');
        $("#useFlag option:first").prop("selected",true);
        $("#textCountKr_wordName").html(0);

        $('#synonymName').val('');
        $("#textCountKr_synonymName").html(0);
        synonymListGrid.setData(null);
        $('#wordName').prop('readonly', false);
    },
    search : function (offset, limit, searchName) {

        if(searchName != undefined && searchName != null) {
            $("#searchKeyword").val(searchName);
        }

        var data = {};
        data.wordName = searchName != undefined && searchName != null ? searchName : $('#searchKeyword').val();
        data.includeNoun = $('input[name=includeNoun]:checked').val() != undefined ? "Y" : "N";
        data.offset = offset;
        data.limit = limit;

        CommonAjax.basic(
            {
                url:'/search/addr-dictionary/getSrchNounInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(offset > 0) {
                        dictionaryListGrid.dataProvider.fillJsonData(res.list,{fillMode: "append"});
                    }else {
                        dictionaryListGrid.setData(res.list);
                        $('#dictionaryTotalCount').html($.jUtil.comma(res.totalCount));
                        dictionaryListGrid.page = 0;
                        if(dictionaryListGrid.dataProvider.getRowCount() > 0) {
                            $('#wordId').val(dictionaryListGrid.dataProvider.getJsonRow(0).wordId);
                            dictionaryList.selectedIndex(dictionaryListGrid.dataProvider.getJsonRow(0));
                            dictionaryList.getSynonym(dictionaryListGrid.dataProvider.getJsonRow(0).wordId);
                        }
                        else {
                            dictionaryList.reset(); //검색 결과가 없을 시 초기화
                            $("#wordName").val(data.wordName);
                        }
                    }
                }
            });


    },
    getSynonym : function(wordId) {
        CommonAjax.basic(
            {
                url:'/search/addr-dictionary/'+wordId+'/getSrchSynonymInfo.json',
                type: 'get',
                method:"GET",
                successMsg:null,
                callbackFunc:function(res)
                {
                    synonymListGrid.setData(res);
                    if(synonymListGrid.dataProvider.getRowCount() > 0) {
                        synonymListGrid.selectedIndex(synonymListGrid.dataProvider.getJsonRow(0));
                    }else {
                        $('#synonymName').val('');
                        $("#textCountKr_synonymName").html(0);
                    }
                }
            });
    }
};