jQuery.fn.exists = function(){return this.length > 0;};
/**
 * 타입종류에 따른 js event
 * @type {{CATEGORY: string, KEYWORD: string}}
 */
const searchType = {
    KEYWORD: 'keyword',
    CATEGORY: 'category',
    EXP_KEYWORD: 'exp-keyword',
    EXP_CATEGORY: 'exp-category'
};
let currentSearchJs;
let currentSearchType;
function setSearchType (type) {
    currentSearchType = type;
    switch (type) {
        case searchType.KEYWORD:
            currentSearchJs = totalsearchJs; break;
        case searchType.CATEGORY:
            currentSearchJs = categorySearchJs; break;
        case searchType.EXP_CATEGORY:
            currentSearchJs = expCategorySearchJs; break;
        case searchType.EXP_KEYWORD:
            currentSearchJs = expsearchJs; break;
    }
}

const filterJs = {
    regExp: /[\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"\s]/gi,
    /* 필터박스 토글 이벤트 */
    toggle: function() {
        if(currentSearchJs.isFirstSearch === false){
            alert('검색을 먼저 수행해 주세요');
            return false;
        }
        $('#filter-box').toggle('fast', function(){
            if ($(this).is(':visible')) {
                $('#filterBtn').text('필터 검색 ∨');
            } else {
                $('#filterBtn').text('필터 검색 ∧');
            }
        });
    },
    /* 필터관련 초기화 */
    filterReset: function() {
        /* 필터박스 관련 hide 및 초기화 */
        $('#brandList').html('');
        $('#filter-box').hide();
        /* 속성박스 관련 hide 및 초기화 */
        $('.attribute-box').hide();
        $('#attributeList').html('');
        /* 필터 공통 혜택 */
        filterJs.falseCheckbox('benefit');
        /* 더보기 관련 초기화 */
        $('[id$=More]').each(function(){
            $(this).children('button').text('+ 더보기');
            //$(this).hide();
            $(this).css("visibility", "hidden");
        });
    },
    /* 더보기 버튼 */
    moreButton: function(label) {
        $('[id^='+label+'Area]').toggle(0, function(){
            if ($(this).is(':visible')) {
                $('#'+label+'Button').text('+ 접기');
            } else {
                $('#'+label+'Button').text('+ 더보기');
            }
        });
    },
    /* 체크박스 관련 draw */
    drawCheckbox: function(list, label) {
        const checkboxCount = 5; // 체크박스 한줄에 노출되는 개수
        let setHtml = [];
        let checkbox = "<label class='ui checkbox mg-r-10' style='display: inline-block'>"
            + "<input type='checkbox' name='{label}' value='{code}' attr='{attr}' display='{name}'/><span>{name}</span></label>";

        let isMore = filterJs.isMoreCheck(list, label, checkboxCount);

        setHtml.push("<div>");
        $.each(list, function(index, item){
            setHtml.push(checkbox
            .replace(/{label}/g, label)
            .replace(/{code}/g, item.code)
            .replace(/{attr}/g, item.code.replace(filterJs.regExp, ''))
            .replace(/{name}/g, item.name))
            ;
            if(index % checkboxCount === checkboxCount - 1 && isMore){
                setHtml.push("</div>");
                setHtml.push("<div id='"+label+"Area_"+index+"' style='display: none; line-height: 5px;'>");
                setHtml.push("<br/>");
            }
        });
        setHtml.push("</div>");

        return setHtml.join('');
    },
    /* 속성 더보기 버튼 활성화 */
    isMoreCheck: function(list, label, checkboxCount) {

        let isMore = false;
        if(list.length > checkboxCount){
            //$('#'+label+'More').show(); // 더보기 버튼 활성화
            $('#'+label+'More').css("visibility", "visible");
            //$('#'+label+'List').attr('colspan', col);
            isMore = true;
        }else{
            //$('#'+label+'More').hide(); // 더보기 버튼 비활성화
            $('#'+label+'More').css("visibility", "hidden");
            //$('#'+label+'List').attr('colspan', col+1);
        }
        return isMore;
    },
    /* 체크박스 체크된 데이터 */
    joinCheckbox: function(label, separator){
        let value = '';
        $("input:checkbox[name='"+label+"']").each(function() {
            if($(this).prop("checked")){
                value += $(this).val() + separator;
            }
        });
        return value.replace(new RegExp(separator+'$', 'gi'), '');
    },
    /* 체크박스 전체 해제 */
    falseCheckbox: function(label){
        $("input:checkbox[name='"+label+"']").each(function() {
            $(this).prop('checked', false);
        });
    },
    /* 필터박스 click event 동적 바인딩을 위한 set */
    bindCheckbox: function(label){
        $(document).on("click", "input[name='"+label+"']", function(){
            currentSearchJs.search(1, currentSearchJs.limitPageSize, null);
            filterJs.drawAttribute($(this), true);
        });
    },
    /* 속성박스 제거 */
    attributeRemove: function(object){
        object.remove();
        // attribute box hide check
        if($('#attributeList').children().length === 0){
            $('.attribute-box').hide();
        }
    },
    /* 선택한 필터 그리기 */
    drawAttribute: function(object, isContinue){
        let attrId = object.val().replace(filterJs.regExp,'')+"_attr";
        let attrObj = $("div[id='"+attrId+"']");
        if(attrObj.exists()) {
            if(isContinue){
                filterJs.attributeRemove(attrObj);
            }
        }else{
            let setHtml = [];
            setHtml.push("<div id="+attrId+" class='ui button large dark-blue' style='margin-top: 4px;'>");
            setHtml.push("<span style='line-height: 1;'>"+object.attr('display')+" X</span>");
            setHtml.push("</div> ");
            $('#attributeList').append(setHtml.join(''));
            // attribute box show check
            if($('#attributeList').children().length !== 0){
                $('.attribute-box').show();
            }
        }
    },
    /* 결과내 재검색 input 제거 */
    removeReKeyword: function(value) {
        let reKeywordTag = "input:hidden[name=reKeyword_attr]:input[value='" + value + "']";
        $(reKeywordTag).remove();
    },
    /*
        필터 검색 파라미터 설정
     */
    filterDataSet: function() {
        /* init parameter - Option */
        let sort;
        let delivery = '';
        let benefit = '';
        let brand = '';
        let partner = '';

        /* set parameter */
        sort = $('input:radio[name=sort]:checked').val();
        delivery = filterJs.joinCheckbox('delivery', ':');
        benefit = filterJs.joinCheckbox('benefit', ':');
        brand = filterJs.joinCheckbox('brand', ':');
        partner = filterJs.joinCheckbox('partner', ':');

        /* data set */
        let data = {
            sort: sort
        };

        if(filterUtilJs.isNotEmpty(delivery)){
            data.delivery = delivery;
        }
        if(filterUtilJs.isNotEmpty(benefit)){
            data.benefit = benefit;
        }
        if(filterUtilJs.isNotEmpty(brand)){
            data.brand = brand;
        }
        if(filterUtilJs.isNotEmpty(partner)){
            data.partner = partner;
        }

        return data;
    },
    bindEvent: function(){
        /* 속성박스 클릭 이벤트 */
        $(document).on("click", "[id$='_attr']", function(){
            let value = $(this).attr('id').replace(/_attr/g,'');
            // 결과내 재검색 속성 삭제
            filterJs.removeReKeyword(value);
            // 속성 박스의 속성 삭제
            filterJs.attributeRemove($(this));
            /* 속성을 클릭시 체크박스의 값도 지워지게 처리 */
            $("#filter-content :input[attr='"+value+"']").prop('checked', false);
            /* 검색 */
            currentSearchJs.search(1, currentSearchJs.limitPageSize, null);
        });
        /* 라디오 */
        $('input:radio[name=sort]').click(function() {
            if(currentSearchJs.isFirstSearch){
                currentSearchJs.search(1, currentSearchJs.limitPageSize, null);
            }else{
                alert('검색을 먼저 수행해 주세요');
                $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);
            }
        });
        /* 체크박스 */
        filterJs.bindCheckbox('benefit');
        filterJs.bindCheckbox('brand');
    }
};

const filterUtilJs = {
    /* 객체 null 체크 */
    isEmpty: function (value) {
        return value === "" || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },
    isNotEmpty: function(value){
        return !this.isEmpty(value);
    },
    /* 문자 이어 붙이기 */
    concat: function(str1, separator, str2) {
        return this.isNotEmpty(str2) ? str1 + separator + str2 : str1;
    },
    /* 문자열 존재여부에 따른 반환값 정의 */
    exist: function(str1, str2){
        return this.isNotEmpty(str2) ? str2 : str1;
    },
    getCommaCalc: function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
};