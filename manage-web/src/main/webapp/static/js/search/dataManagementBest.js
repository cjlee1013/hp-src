function is_ie() {
    if(navigator.userAgent.toLowerCase().indexOf("chrome") != -1) return false;
    if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) return true;
    if(navigator.userAgent.toLowerCase().indexOf("windows nt") != -1) return true;
    return false;
}

function copy_to_clipboard(str) {
    if( is_ie() ) {
        window.clipboardData.setData("Text", str);
        alert("복사되었습니다.");
        return;
    }
    prompt("Ctrl+C를 눌러 복사하세요.", str);
}
var bestJs = {
    init : function() {
        this.initCategorySelectBox();
        this.bindingEvent();
    },
    limitPageSize : function() {
        return 100;
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        CommonAjax.basic({url:'/item/category/getGroupList.json?schType=GCATENM&schKeyword=&schUseYn=Y', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                for(var i = 0; i < res.length; i++) {
                    $("#schCategory option:eq("+i+")").after("<option value=\""+res[i].gcateCd+"\" name=\""+res[i].gcateNm+"\">"+res[i].gcateNm+"</option>");
                }
            }});
    },
    selectedIndex : function(jsonRow) {
        $("span[name=indexDocId]").html(jsonRow.docId);             //문서번호
        $("span[name=indexWeight]").html(jsonRow.weight);           //가중치
        $("span[name=indexWeightCalc]").html(jsonRow.weightCalc);   //가중치 수식
        $("span[name=indexWeightDay]").html(jsonRow.weightDay);     //24시간 구매자수
    },
    reset : function() {
    },
    search : function (offset, limit, callback) {
        var storeIds = $("input[name=storeIds]").val();
        var siteType = $("input[name=siteType]:checked").val();
        var regexp = /^[(\d{1,10}),]*$/;
        if (storeIds == null || storeIds == ''){
            alert('점포를 입력해주세요.');
            return false;
        }
        if (storeIds != null && $.trim(storeIds) != '' && !regexp.test(storeIds)) {
            alert('점포ID 형식이 옳바르지 않습니다.');
            return false;
        }
        if (storeIds.split(",").length > 3) {
            alert('점포ID는 (,)쉼표 구분해서 최대 3개까지만 입력 가능합니다.');
            $("input[name=storeIds]").focus();
            return false;
        }
        var categoryId = $("#schCategory option:selected").val();

        CommonAjax.basic(
            {
                url: '/search/dataManagement/best/list.json',
                data: {
                    offset: offset,
                    limit: limit,
                    storeIds: storeIds,
                    siteType: siteType,
                    categoryId: categoryId
                },
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {

                    if(offset > 1) {
                        bestGrid.dataProvider.fillJsonData(res.data.dataList,{fillMode: "append"});
                    }
                    else
                    {
                        bestGrid.setData(res.data.dataList);
                        if(bestGrid.dataProvider.getRowCount() > 0) {
                            //bestJs.selectedIndex(bestGrid.dataProvider.getJsonRow(0));
                        } else {
                            bestJs.reset(); //검색 결과가 없을 시 초기화
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback(res);
                    }
                }
            }
        );
    },
    bindingEvent : function() {
        $('#searchBtn').click(function() {
            bestJs.search(0,bestJs.limitPageSize,null);
        });
        $("#copyQuery").click(function() {
            if($("#hidden_query").val() == null || $("#hidden_query").val() == '') {
                alert('검색시도 후 복사가 가능합니다.');
                return false;
            }
            copy_to_clipboard($("#hidden_query").val());
        });
        $("input[name=siteType]").click(function() {
            if($(this).val() == "HOME") {
                $("#storeIds").val("37");
            }else {
                $("#storeIds").val("38");
            }
        });
        $("#excelDownloadBtn").click(function() {
            bestJs.excelDownload();
        });
        /* 초기화 버튼 */
        $('#resetBtn').click(function() {
            $("#schCategory").val('0');
            $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
            bestJs.setStoreCondition('DIRECT');
        });
        $("input[name=storeSearchType]").click(function() {
            var searchType = $(this).val();
            bestJs.setStoreCondition(searchType);
        });
        $("#storeType").change(function(){
            var searchType = $("input:radio[name='storeSearchType']:checked").val();
            bestJs.setStoreCondition(searchType);
        });
    },
    excelDownload: function() {
        bestJs.search(0,1000,function(res) {
            var _date = new Date();
            var fileName = "베스트_" + $("#profilesActive").val()+"_"+$("#schCategory option:selected").attr("name") + "_"+ $("input[name=siteType]:checked").val() + "_" + $("input[name=storeIds]").val().replace(/,/gi,"_") + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate() + "_" + _date.getHours() + "_" + _date.getMinutes();
            bestGrid.gridView.exportGrid({
                type: "excel",
                indicator: "visible",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다.",
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                showColumns: [
                    "adultType",
                    "weightBuyer",
                    "weightHour",
                    "weightDay",
                    "weightPrice",
                    "weightStoreType",
                    "weightOpenDay",
                    "weightShipMethod",
                    "weightPromotion",
                    "weightBenefit",
                    "weightBunddleSum",
                    "storeType",
                    "eventItemYn",
                    "pickYn",
                    "togetherYn",
                    "giftYn",
                    "freeshipYn",
                    "regDt"
                ],
                hideColumns: ["img"]
            });
        })
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=HYPER"
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },

    setStoreCondition : function (storeSearchType){
        if(storeSearchType == 'DIRECT'){
            $("#storeIds").val("37");
            $("#storeIds").attr("readonly",false);
            $("#schStoreNm").hide();
            $("#schStoreBtn").hide();
            $("#directText").show();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }else { // 'SEARCH'
            $("#storeIds").val("");
            $("#storeIds").attr("readonly",true);
            $("#schStoreNm").val("");
            $("#schStoreNm").show();
            $("#schStoreBtn").show();
            $("#directText").hide();
            $("input:radio[name=storeSearchType]").attr("disabled",false);
        }
    }
};
var utils = {
    getCommaCalc: function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
};

var bestGrid = {
    gridView : new RealGridJS.GridView("bestGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        bestGrid.initGrid();
        bestGrid.initDataProvider();
        bestGrid.event();
    },
    initGrid : function () {
        bestGrid.gridView.setDataSource(bestGrid.dataProvider);

        bestGrid.gridView.setStyles(bestGridBaseInfo.realgrid.styles);
        bestGrid.gridView.setDisplayOptions(bestGridBaseInfo.realgrid.displayOptions);
        bestGrid.gridView.setColumns(bestGridBaseInfo.realgrid.columns);
        bestGrid.gridView.setOptions(bestGridBaseInfo.realgrid.options);
        bestGrid.gridView.setCheckBar({ visible: false });
        bestGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        bestGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            },
            rowHeight: 80
        });
        bestGrid.gridView.onScrollToBottom = function(grid) {
            if(bestGrid.dataProvider.getRowCount() >= 1000) {
                return;
            }
            bestJs.search(bestGrid.dataProvider.getRowCount(),bestJs.limitPageSize(),null,$("input[name=searchType]").val());
        };
    },
    initDataProvider : function() {
        bestGrid.dataProvider.setFields(bestGridBaseInfo.dataProvider.fields);
        bestGrid.dataProvider.setOptions(bestGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        bestGrid.gridView.onDataCellClicked = function(gridView, index) {
            bestGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        bestGrid.dataProvider.clearRows();
        bestGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = bestGrid.dataProvider.getJsonRow(selectRowId);
        bestJs.selectedIndex(rowDataJson);
    }
};