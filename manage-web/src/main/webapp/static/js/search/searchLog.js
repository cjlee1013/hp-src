var searchLogGrid = {
    gridView : new RealGridJS.GridView("searchLogGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        searchLogGrid.initGrid();
        searchLogGrid.initDataProvider();
        searchLogGrid.event();
    },
    page : 0,
    initGrid : function () {
        searchLogGrid.gridView.setDataSource(searchLogGrid.dataProvider);

        searchLogGrid.gridView.setStyles(searchLogGridBaseInfo.realgrid.styles);
        searchLogGrid.gridView.setDisplayOptions(searchLogGridBaseInfo.realgrid.displayOptions);
        searchLogGrid.gridView.setColumns(searchLogGridBaseInfo.realgrid.columns);
        searchLogGrid.gridView.setOptions(searchLogGridBaseInfo.realgrid.options);
        searchLogGrid.gridView.setCheckBar({ visible: false });
        searchLogGrid.gridView.setSelectOptions({
            style: 'none'
        });

        searchLogGrid.gridView.setIndicator({
            headText: '순위'
        });
        var hoverMaskValue = $('#selHoverMask').val();
        searchLogGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
    },
    initDataProvider : function() {
        searchLogGrid.dataProvider.setFields(searchLogGridBaseInfo.dataProvider.fields);
        searchLogGrid.dataProvider.setOptions(searchLogGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        searchLogGrid.gridView.onDataCellClicked = function(gridView, index) {
            searchLogGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        searchLogGrid.dataProvider.clearRows();
        searchLogGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {

        let siteType = $("input:radio[name='siteType']:checked").val();

        let startDt = $('#excelStartDt').val().replace(/-/g,'');
        let endDt = $('#excelEndDt').val().replace(/-/g, '');
        if(!startDt || !endDt){
            alert('다운로드할 데이터가 없습니다.');
            return;
        }

        let fileName = [];
        switch(siteType){
            case 'HOME':
                fileName.push('홈플러스_'); break;
            case 'CLUB':
                fileName.push('더클럽_'); break;
            case 'EXP':
                fileName.push('익스프레스_'); break;
            default:
        }
        fileName.push(($("#logType").val() === "success" ? "성공검색어_" : "실패검색어_"));
        fileName.push(startDt + "-" + endDt + '.xlsx');

        searchLogGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            fileName: fileName.join(''),
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = searchLogGrid.dataProvider.getJsonRow(selectRowId);
        searchLogJs.selectedIndex(rowDataJson);
    }
};

var searchLogJs = {
    init : function () {
        this.initSearchDate('0');
        this.event();
    },
    initDateRange: function(){
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag, element) {
        searchLogJs.setDate = Calendar.datePickerRange('startDt', 'endDt');
        searchLogJs.setDate.setEndDate(0);
        searchLogJs.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());

        if(element){
            searchLogJs.buttonClickEvent(element);
        }else{
            searchLogJs.buttonClickEvent($('#todayBtn'));
        }
    },
    /**
     * 버튼 클릭시 색상 변경 이벤트
     * @param element
     */
    buttonClickEvent: function(element){
        $('button[name=rangeBtn]').each(function(){
            if($(this).hasClass("cyan")){
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark")
            }
        });
        $(element).removeClass("gray-dark");
        $(element).addClass("cyan");
    },
    event : function() {
        $('#searchBtn').click(function () {
            searchLogJs.search();
        });
        $("#searchResetBtn").click(function () {
            searchLogJs.searchReset();
        });
        $("#excelDownloadBtn").click(function () {
            searchLogGrid.excelDownload();
        });
        $("#searchKeyword").keydown(function(key) {
            if (key.keyCode === 13) {
                searchLogJs.search();
            }
        });
    },
    search : function () {

        if(moment($("#endDt").val()).diff($("#startDt").val(), "day", true) >= 31) {
            alert("최대 조회 기간은 31일 입니다.");
            return false;
        }

        CommonAjax.basic({
            url: '/search/statistics/getSearchlog.json',
            method: 'get',
            dataType: 'json',
            data: {
                logType: $("#logType").val(),
                startDt: $("#startDt").val(),
                endDt: $("#endDt").val(),
                siteType: $("input:radio[name='siteType']:checked").val(),
                searchKeyword: $("#searchKeyword").val(),
            },
            successMsg: null,
            callbackFunc: function (res) {
                if(res.length > 0){
                    searchLogGrid.setData(res);
                    $("#totalCount").html(res.length);

                    /* 엑셀 다운로드를 위한 변수 설정 */
                    $('#excelStartDt').val($('#startDt').val());
                    $('#excelEndDt').val($('#endDt').val());
                }else{
                    alert('조회 기간에 대한 검색결과가 없습니다.');
                }
            }
        });
    },
    searchReset : function () {
        $("input:radio[name='siteType'][value='HOME']").click();
        $("#searchKeyword").val('');
        searchLogJs.initSearchDate('0');
    },
    selectedIndex : function(jsonRow) {

        if($("#logType").val() != "failure"){
            return;
        }

        CommonAjax.basic({
            url: '/search/statistics/failureLogRelatedWord.json',
            data: {
                failureKeyword : jsonRow.keyword
            },
            cache: false,
            method: "GET",
            dataType: 'json',
            successMsg: null,
            callbackFunc: function (res) {
                var relateWordText = "";
                for(var i=0; i < res.length; i++){
                    relateWordText += res[i] + ((res.length-1==i) ? "" : ", ");
                }

                $("span[name=indexRelateWord]").html(relateWordText);
            }
        });
    }
};