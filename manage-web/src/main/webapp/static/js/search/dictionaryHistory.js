var dictionaryHistory = {
    init : function () {
    },
    onLoad : function() {
        CommonAjax.basic({url:'/search/dictionary/'+wordId+'/getWordHistory.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            dictionaryHistoryGrid.setData(res);
        }});
    }
};

var dictionaryHistoryGrid = {
    gridView : new RealGridJS.GridView("dictionaryHistoryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dictionaryHistoryGrid.initGrid();
        dictionaryHistoryGrid.initDataProvider();
        dictionaryHistoryGrid.event();
    },
    initGrid : function () {
        dictionaryHistoryGrid.gridView.setDataSource(dictionaryHistoryGrid.dataProvider);

        dictionaryHistoryGrid.gridView.setStyles(dictionaryHistoryGridBaseInfo.realgrid.styles);
        dictionaryHistoryGrid.gridView.setDisplayOptions(dictionaryHistoryGridBaseInfo.realgrid.displayOptions);
        dictionaryHistoryGrid.gridView.setColumns(dictionaryHistoryGridBaseInfo.realgrid.columns);
        dictionaryHistoryGrid.gridView.setOptions(dictionaryHistoryGridBaseInfo.realgrid.options);
        dictionaryHistoryGrid.gridView.setCheckBar({ visible: false });
        dictionaryHistoryGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        dictionaryHistoryGrid.dataProvider.setFields(dictionaryHistoryGridBaseInfo.dataProvider.fields);
        dictionaryHistoryGrid.dataProvider.setOptions(dictionaryHistoryGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dictionaryHistoryGrid.gridView.onDataCellClicked = function(gridView, index) {
            dictionaryHistoryGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        dictionaryHistoryGrid.dataProvider.clearRows();
        dictionaryHistoryGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = dictionaryHistoryGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
    },
    excelDownload: function() {
        if(dictionaryHistoryGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "사전_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        dictionaryHistoryGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};