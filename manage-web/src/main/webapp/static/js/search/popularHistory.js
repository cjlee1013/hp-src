
/**
 * 현재 인기검색어
 */
const nowPopularGrid = {
    gridView : new RealGridJS.GridView("nowPopularGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, nowPopularGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        commonGridCss.setColumnReplace(this.gridView);
        commonGridCss.setColumnUpdateData(this.dataProvider);
    }
};

/**
 * 선택된 인기검색어 (이전 인기검색어)
 */
const selPopularGrid = {
    gridView : new RealGridJS.GridView("selPopularGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, basicPopularGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        commonGridCss.setColumnReplace(this.gridView);
        commonGridCss.setColumnUpdateData(this.dataProvider);
    },
    clearData: function(){
        this.dataProvider.clearRows();
    }
};

/**
 *  현재 적용중인 확정어
 */
const fixedGrid = {
    gridView : new RealGridJS.GridView("fixedGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, fixedGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);
        this.gridView.orderBy(["rank", "startDt"], ["ascending", "ascending"]);
    }
};

/**
 *  현재 적용중인 제외어
 */
const exceptGrid = {
    gridView : new RealGridJS.GridView("exceptGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, exceptGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);
        this.gridView.orderBy(["startDt"], ["ascending"]);
    }
};

/**
 * 인기검색어 프로세스 변수
 */
const popularHistoryJs = {
    /**
     * 초기화
     */
    init: function (isEvent) {
        this.initSchDate();
        if (isEvent) {
            this.event();
        }

        this.nowPopularGridView();
        this.beforePopularGridView();
        this.nowPopularManagementGridView();
    },
    /**
     * 현재 인기검색어 그리기
     */
    nowPopularGridView: function (){
        let groupInfo = commonPopularJs.getPopularNowList(commonPopularJs.getSiteType(), popularType.POPULAR);
        $("#nowDisplayGroup").html(groupInfo.groupNm);

        let viewData = commonPopularJs.getPopularKeyword(groupInfo.groupNo, popularType.POPULAR);
        nowPopularGrid.setData(viewData);
    },
    /**
     * 이전 인기검색어 목록 그리기
     */
    beforePopularGridView: function (){
        let applyDt = $("#selDt").val().replace(/-/gi, "");
        let groupInfo = commonPopularJs.getPopularGroupList(commonPopularJs.getSiteType(), popularType.POPULAR, applyDt, null);

        $("#selDisplayGroupOption").empty();
        if(groupInfo.length === 0){
            $("#selDisplayGroupOption").append("<option>해당 날짜의 이전 인기검색어가 없습니다</option>");
        }else{
            $("#selDisplayGroupOption").append("<option>조회할 데이터를 선택해주세요</option>");
            $.each(groupInfo, function (i, item) {
                if (item.groupNm !== $("#nowDisplayGroup").html()) {
                    $("#selDisplayGroupOption").append(
                        "<option value=" + item.groupNo + ">" + item.groupNm + "</option>");
                }
            });
        }
        /* 이전 인기검색어 grid 초기화 */
        selPopularGrid.clearData();
    },
    /**
     * 이전 인기검색어 목록중 가장 최근( 1번 index값)의 groupNo가져오기
     */
    getBeforePopularGroupNo: function() {
        let groupNo = $('#selDisplayGroupOption option:eq(1)').val();
        if(groupNo !== undefined){
            let resultData = commonPopularJs.getPopularKeyword(groupNo, popularType.POPULAR);
            selPopularGrid.setData(resultData);
            /* 색인된 인덱스중 가장 마지막 값 설정 */
            $('#selDisplayGroupOption option:eq(1)').attr('selected', 'selected');
        }else{
            $("#selDisplayGroupOption").empty();
            $("#selDisplayGroupOption").append("<option>해당 날짜의 이전 인기검색어가 없습니다</option>");
            selPopularGrid.clearData();
        }
    },
    /**
     * 새로고침시 이전인기검색어 목록을 갱신하되 선택되어 있는 상태로 그리기
     */
    refreshBeforePopularGridView: function(){

        /* 현재 선택중인 group_no */
        let selectGroupNo = $('#selDisplayGroupOption option:selected').val();

        /* 새로 그리기 */
        this.beforePopularGridView();

        $('#selDisplayGroupOption').val(selectGroupNo).prop("selected", true);

        let regExp = /\d/;
        if(regExp.test(selectGroupNo)){
            let resultData = commonPopularJs.getPopularKeyword(selectGroupNo, popularType.POPULAR);
            selPopularGrid.setData(resultData);
        }
    },
    /**
     * 현재 인기검색어 관리 그리기
     */
    nowPopularManagementGridView: function(){
        let date = new Date();
        let nowDate = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2)
            + "-" + ("0" + date.getDate()).slice(-2);

        for(let type in mngType){
            let viewData = commonPopularJs.getPopularManagement(commonPopularJs.getSiteType(), mngType[type], nowDate);
            if (mngType[type] == mngType.FIXED) {
                fixedGrid.setData(viewData);
            } else {
                exceptGrid.setData(viewData);
            }
        }
    },
    /**
     * 새로고침 refresh
     */
    refreshEvent: function(){

        /* 현재 적용중인 인기검색어 */
        this.nowPopularGridView();
        /* 이전 인기검색어 리스트 호출 */
        this.refreshBeforePopularGridView();
        /* 확정어 제외어 리스트 호출 */
        this.nowPopularManagementGridView();
    },
    /**
     * 조회 일자 초기화
     */
    initSchDate: function () {
        popularHistoryJs.selDt = Calendar.datePicker('selDt');
        popularHistoryJs.selDt.setDate(0);
    },
    /**
     * 날짜 변환 클릭 이벤트
     */
    event: function () {
        $("#selDt").on('change', function () {
            popularHistoryJs.beforePopularGridView();
        });
    }
};

