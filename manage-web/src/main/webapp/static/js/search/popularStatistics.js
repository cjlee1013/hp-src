/**
 * 일간 인기검색어
 */
const dayPopularGrid = {
    gridView : new RealGridJS.GridView("dayPopularGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, dayPopularGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        commonGridCss.setColumnReplace(this.gridView);
        commonGridCss.setColumnUpdateData(this.dataProvider);
    }
};
/**
 * 주간 인기검색어
 */
const weekPopularGrid = {
    gridView : new RealGridJS.GridView("weekPopularGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, weekPopularGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        commonGridCss.setColumnReplace(this.gridView);
        commonGridCss.setColumnUpdateData(this.dataProvider);
    }
};
/**
 * 월간 인기검색어
 */
const monthPopularGrid = {
    gridView : new RealGridJS.GridView("monthPopularGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, monthPopularGridBaseInfo);
    },
    setData : function(dataList) {
        this.dataProvider.clearRows();
        this.dataProvider.setRows(dataList);

        commonGridCss.setColumnReplace(this.gridView);
        commonGridCss.setColumnUpdateData(this.dataProvider);
    }
};

/**
 * 통계 프로세스 변수
 */
const popularStatisticsJs = {
    /**
     * 초기화
     */
    init : function() {
        for(let type in dateType){
            this.initSchDate(dateType[type]);
            this.popularGridView(dateType[type], true);
        }
    },
    /**
     * 통계 인기검색어 그리기 ( 일간, 주간, 월간 )
     * @param type
     * @param isInit
     */
    popularGridView: function(type, isInit){
        let applyDt = $('#'+type+'SelDt').val().replace(/-/gi, "");
        let groupInfo = commonPopularJs.getPopularGroupList(commonPopularJs.getSiteType(), popularType.STATISTICS, applyDt, type);

        if(type == dateType.MONTH){
            if(groupInfo.length > 0){
                this.setGridData(type, groupInfo[0].groupNo);
            }else{
                this.setGridData(type, 0);
            }
        }else{
            $('#'+type+'StatisticsGroupOption').empty();
            $('#'+type+'StatisticsGroupOption').append("<option>조회할 데이터를 선택해주세요.</option>");
            $.each(groupInfo, function (i, item) {
                $('#'+type+'StatisticsGroupOption').append("<option value=" + item.groupNo + ">" + item.groupNm + "</option>");
            });

            if(isInit){
                let selectBoxSize = $("#"+type+"StatisticsGroupOption option").length;
                /** 사이즈가 있는 경우에만 수행 **/
                if(selectBoxSize > 1){
                    $("#"+type+"StatisticsGroupOption option:eq(1)").attr("selected","selected");
                    let groupNo = $("#"+type+"StatisticsGroupOption").val();
                    this.setGridData(type, groupNo);
                }
            }
        }
    },
    /**
     * 인자값 type에 따라 통계 인기검색어를 화면에 그려준다.
     * @param type
     * @param groupNo
     */
    setGridData: function(type, groupNo) {
        if($.isNumeric(groupNo)){
            let resultData = commonPopularJs.getPopularKeyword(groupNo, popularType.STATISTICS);
            switch (type) {
                case dateType.DAY:
                    dayPopularGrid.setData(resultData);
                    break;
                case dateType.WEEK:
                    weekPopularGrid.setData(resultData);
                    break;
                case dateType.MONTH:
                    monthPopularGrid.setData(resultData);
                    break;
            }
        }
    },
    /**
     * 조회 일자 초기화
     */
    initSchDate : function (type) {
        monthConfig.onSelect = function (dataText, inst) {
            if(dataText > popularStatisticsJs.getLimitDate(type)) {
                alert('프로세스가 실행되지 않는 달보다 큰 달을 지정할 수 없습니다.');
                $('#' + type + 'SelDt').val(inst.lastVal);
            }else{
                popularStatisticsJs.popularGridView(type, false);
            }
        };

        $('#'+type+'SelDt').monthpicker(monthConfig);
        $('#'+type+'SelDt').val(popularStatisticsJs.getLimitDate(type));
    },
    /**
     * 현재 년월 가져오기
     * @returns {string}
     */
    getLimitDate : function (type){
        if(type != dateType.MONTH){
            return new Date().getFullYear() + "-" + ("0"+(new Date().getMonth()+1)).slice(-2);
        }else{
            return new Date().getFullYear() + "-" + ("0"+(new Date().getMonth())).slice(-2);
        }
    }
};