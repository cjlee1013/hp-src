/**
 * 인기검색어 관리 확정어, 제외어
 * @type {{FIXED: string, EXCEPT: string}}
 */
const mngType = {
    FIXED: 'fixed',
    EXCEPT: 'except'
};
/**
 * 인기검색어 종류 통계, 인기검색어
 * @type {{STATISTICS: string, POPULAR: string}}
 */
const popularType = {
    POPULAR: 'popular',
    STATISTICS: 'statistics'
}
/**
 * 인기검색어 통계 종류 일간, 주간, 월간
 * @type {{MONTH: string, WEEK: string, DAY: string}}
 */
const dateType = {
    DAY: 'day',
    WEEK: 'week',
    MONTH: 'month'
};

/**
 * 인기검색어 통계 Monthpicker 설정
 */
const monthConfig = {
    dateFormat: 'yy-mm',
    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    openOnFocus: true,
    showOn: "button",
    buttonImage : '/static/images/ui-kit/ui-input-calendar-sm.png',
    buttonText: "날짜를 선택하세요",
    onSelect: function (dataText, inst) {
    }
}

/**
 * contains 비교함수
 * @param element
 * @returns {boolean}
 */
Array.prototype.contains = function (element) {
    for (let idx = 0; idx < this.length; idx++){
        if(this[idx] == element){
            return true;
        }
    }
    return false;
}

/**
 * 공통 그리드 변수
 */
const commonGridBase = {
    grid : null,
    baseInfo : null,
    init : function(grid, baseInfo) {
        this.grid = grid;
        this.baseInfo = baseInfo;
        this.initGrid();
        this.initDataProvider();
    },
    initGrid : function() {
        this.grid.gridView.setDataSource(this.grid.dataProvider);
        this.grid.gridView.setStyles(this.baseInfo.realgrid.styles);
        this.grid.gridView.setDisplayOptions(this.baseInfo.realgrid.displayOptions);
        this.grid.gridView.setColumns(this.baseInfo.realgrid.columns);
        this.grid.gridView.setOptions(this.baseInfo.realgrid.options);
    },
    initDataProvider : function() {
        this.grid.dataProvider.setFields(this.baseInfo.dataProvider.fields);
        this.grid.dataProvider.setOptions(this.baseInfo.dataProvider.options);
    }
};

/**
 * 공통 그리드 동적 CSS변경 변수
 */
const commonGridCss = {
    /**
     * 변동폭에 따른 css 처리
     */
    setColumnReplace: function (gridView) {
        gridView.setStyles({
            body: {
                dynamicStyles: function (grid, index) {
                    let variation = grid.getValue(index.itemIndex, "variation");
                    if (variation > 0) {
                        return {
                            foreground: "#ff000a"
                        }
                    } else if (variation < 0) {
                        return {
                            foreground: "#0015ff"
                        }
                    }
                }
            }
        });
    },
    /**
     * 변동 컬럼 데이터 부여 ( + )
     * 확정어 데이터 표시
     * @param nowDt
     */
    setColumnUpdateData: function (dataProvider) {
        dataProvider.getJsonRows(0, -1).forEach(function (data, index) {
            let variation = data.variation;
            if(variation === '0'){
                dataProvider.setValue(index, 'variation', '-');
            }else if (variation > 0) {
                dataProvider.setValue(index, 'variation', '+' + variation);
            }else if(variation == null){
                dataProvider.setValue(index, 'variation', '신규');
            }
        });
    }
};


const commonPopularJs = {
    /**
     * 현재 인기검색어 인덱스 조회
     */
    getPopularNowList: function (siteType, popularType) {
        let resultData;
        CommonAjax.basic({
            url: '/search/popular/getPopularDisplayNm.json',
            method: 'get',
            dataType: 'json',
            data: {
                siteType: siteType,
                popularType: popularType
            },
            successMsg: null,
            cache: false,
            async: false,
            callbackFunc: function (res) {
                resultData = res;
            }
        });
        return resultData;
    },
    /**
     * 해당일 인기검색어 인덱스 조회
     */
    getPopularGroupList: function (siteType, popularType, applyDt, dateType) {
        let dataRange = {
            siteType: siteType
            , popularType: popularType
            , applyDt: applyDt
        };
        if( dateType != null ){
            dataRange.dateType = dateType;
        }

        let resultData;
        CommonAjax.basic({
            url: '/search/popular/getPopularDisplayNmGroup.json',
            method: 'get',
            dataType: 'json',
            data: dataRange,
            successMsg: null,
            cache: false,
            async: false,
            callbackFunc: function (res) {
                resultData = res;
            }
        });
        return resultData;
    },
    /**
     * 조건에 따른 인기검색어 조회
     */
    getPopularKeyword: function (groupNo, popularType) {
        let resultData;
        CommonAjax.basic({
            url: '/search/popular/getPopularKeyword.json',
            method: 'get',
            dataType: 'json',
            async: false,
            data: {
                popularType: popularType
                , groupNo: groupNo
            },
            successMsg: null,
            cache: false,
            callbackFunc: function (res) {
                resultData = res;
            }
        });

        return resultData;
    },
    /**
     * 현재 적옹중인 키워드 조회
     */
    getPopularManagement: function (siteType, type, date) {
        let dataRange = {
            siteType: siteType
            , type: type
        };
        if( date != null ){
            dataRange.date = date;
        }

        let resultData;
        CommonAjax.basic({
            url: '/search/popular/getPopularManagement.json',
            method: 'get',
            dataType: 'json',
            async: false,
            data: dataRange,
            successMsg: null,
            cache: false,
            callbackFunc: function (res) {
                resultData = res;
            }
        });
        return resultData;
    },
    /**
     * 현재 적용중인 사이트 정보 가져오기
     * @returns {jQuery|string|undefined}
     */
    getSiteType: function () {
        return $('input:radio[name=siteTypeRadio]:checked').val();
    }
};

