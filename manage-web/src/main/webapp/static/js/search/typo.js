var candidateGrid = {
    gridView : new RealGridJS.GridView("candidateGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        candidateGrid.initGrid();
        candidateGrid.initDataProvider();
        candidateGrid.event();
    },
    initGrid : function () {
        candidateGrid.gridView.setDataSource(candidateGrid.dataProvider);

        candidateGrid.gridView.setStyles(candidateGridBaseInfo.realgrid.styles);
        candidateGrid.gridView.setDisplayOptions(candidateGridBaseInfo.realgrid.displayOptions);
        candidateGrid.gridView.setColumns(candidateGridBaseInfo.realgrid.columns);
        candidateGrid.gridView.setOptions(candidateGridBaseInfo.realgrid.options);
        candidateGrid.gridView.setCheckBar({ visible: false });
        candidateGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        candidateGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
    },
    initDataProvider : function() {
        candidateGrid.dataProvider.setFields(candidateGridBaseInfo.dataProvider.fields);
        candidateGrid.dataProvider.setOptions(candidateGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        candidateGrid.dataProvider.clearRows();
        candidateGrid.dataProvider.setRows(dataList);
    }
};
var typoGrid = {
    gridView : new RealGridJS.GridView("typoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        typoGrid.initGrid();
        typoGrid.initDataProvider();
        typoGrid.event();
    },
    page : 0,
    initGrid : function () {
        typoGrid.gridView.setDataSource(typoGrid.dataProvider);

        typoGrid.gridView.setStyles(typoGridBaseInfo.realgrid.styles);
        typoGrid.gridView.setDisplayOptions(typoGridBaseInfo.realgrid.displayOptions);
        typoGrid.gridView.setColumns(typoGridBaseInfo.realgrid.columns);
        typoGrid.gridView.setOptions(typoGridBaseInfo.realgrid.options);
        typoGrid.gridView.setCheckBar({ visible: false });
        typoGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        typoGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        typoGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > typoGrid.page) {
                typoGrid.page += 50;
                typoJs.search(typoGrid.dataProvider.getRowCount(),typoJs.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        typoGrid.dataProvider.setFields(typoGridBaseInfo.dataProvider.fields);
        typoGrid.dataProvider.setOptions(typoGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        typoGrid.gridView.onDataCellClicked = function(gridView, index) {
            typoGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        typoGrid.dataProvider.clearRows();
        typoGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = typoGrid.dataProvider.getJsonRow(selectRowId);
        typoJs.selectedIndex(rowDataJson);
    },
    excelDownload: function() {

        $("#schKeyword").val('');
        $("input[name=inner]").prop('checked', false);
        typoJs.initSearchDate('-5y');

        //전체 다운로드를 위해 전체 데이터를 RealGrid에 바인딩 해야 한다.
        typoJs.search(0,100000000, function() {
            var _date = new Date();
            var fileName = "오탈자_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            typoGrid.gridView.exportGrid( {
                type: "excel",
                indicator: "default",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        });
    }
};

var typoJs = {
    init : function () {
        this.initSearchDate('-5y');
    },
    limitPageSize : function() {
        return 100;
    },
    onload : function() {
        typoJs.search(0,typoJs.limitPageSize());
        typoJs.candidate();
    },
    selectedIndex : function(jsonRow) {
        $('#typoId').val(jsonRow.typoId);

        //입력폼 데이터 생성
        $("#keyword").val(jsonRow.keyword);
        $("#correctedTerms").val(jsonRow.correctedTerms);

        $("#textCountKr_keyword").html(jsonRow.keyword.length);
        $("#textCountKr_correctedTerms").html(jsonRow.correctedTerms.length);
    },
    initSearchDate : function (flag) {
        typoJs.setDate = Calendar.datePickerRange('schSdate', 'schEdate');
        typoJs.setDate.setEndDate(0);
        typoJs.setDate.setStartDate(flag);
        $("#schEdate").datepicker("option", "minDate", $("#schSdate").val());
    },
    event : function() {
        $('#searchBtn').click(function(){
            typoJs.search(0,typoJs.limitPageSize());
        });
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $('#correctedTerms').calcTextLength('keyup', '#textCountKr_correctedTerms');
        $("#deleteBtn").bindClick(typoJs.delete);
        $("#saveBtn").click(function(){
            typoJs.write();
        });
        $('#resetBtn').bindClick(typoJs.reset);
        $("#excelDownloadBtn").click(function() {
            typoGrid.excelDownload();
        });
    },
    reset : function() {
        $('#typoId').val('');
        $('#keyword').val('');
        $('#correctedTerms').val('');
        $("#textCountKr_keyword").html(0);
        $("#textCountKr_correctedTerms").html(0);
        $('#keyword').attr("readonly",false);
    },
    delete : function() {
        if(!$.trim($("#typoId").val())) {
            alert('삭제하려는 검색어를 선택해주세요.');
            return false;
        }
        if(confirm('해당 검색어를 삭제하시겠습니까?')) {
            CommonAjax.basic({
                url: '/search/typo/'+$("#typoId").val()+'/delete.json',
                data: null,
                type: 'get',
                method:"GET",
                callbackFunc:function() {
                    alert('삭제되었습니다.');
                    typoJs.search(0,typoJs.limitPageSize());
                }
            });
        }
        return false;
    },
    ///대체검색어 등록
    write : function() {

        if(!$.trim($("#keyword").val())) {
            alert('검색어를 입력해주세요.');
            $("#keyword").focus();
            return false;
        }

        if(!$.trim($("#correctedTerms").val())) {
            alert('오탈자 항목을 입력해주세요.');
            $("#correctedTerms").focus();
            return false;
        }

        var requestData = {};
        requestData.typoId = $('#typoId').val();
        requestData.keyword  = $("#keyword").val();
        requestData.correctedTerms = $("#correctedTerms").val();

        var url = '/search/typo/insert.json';

        CommonAjax.basic({
            url: url,
            data: JSON.stringify(requestData),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            callbackFunc:function(res) {
                if (res.data == 1 && res.returnStatus == 200) {
                    $("#schKeyword").val($("#keyword").val());
                    typoJs.search(0,typoJs.limitPageSize());
                    typoJs.candidate();
                    alert('저장 되었습니다');
                }
            }
        });
    },
    candidate : function() {
        CommonAjax.basic(
            {
                url:'/search/typo/candidate.json',
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                callbackFunc:function(res)
                {
                    candidateGrid.setData(res.data);
                    $("#candidateCount").html($.jUtil.comma(candidateGrid.dataProvider.getRowCount()));
                }
            });

    },
    search : function (offset, limit, callback) {

        var data = {};

        /// 시작일
        data.schStartDt = $("input[name=schSdate]").val();
        /// 종료일
        data.schEndDt = $("input[name=schEdate]").val();
        /// 정렬
        data.schOrder = $('#schOrder option:selected').val();

        data.schKeyword = $('#schKeyword').val();

        data.offset = offset;
        data.limit = limit;

        CommonAjax.basic(
            {
                url:'/search/typo/search.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                callbackFunc:function(res)
                {
                    if(offset > 0) {
                        var newStart = typoGrid.dataProvider.getRowCount();
                        typoGrid.dataProvider.fillJsonData(res.data,{fillMode: "append"});
                    }
                    else
                    {
                        typoGrid.setData(res.data);
                        $('#totalCount').html($.jUtil.comma(res.pagination.totalCount));
                        typoGrid.page = 0;
                        if(typoGrid.dataProvider.getRowCount() > 0) {
                            $('#typoId').val(typoGrid.dataProvider.getJsonRow(0).typoId);
                            typoJs.selectedIndex(typoGrid.dataProvider.getJsonRow(0));
                        } else {
                            typoJs.reset(); //검색 결과가 없을 시 초기화
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback();
                    }
                }
            });

    }
};