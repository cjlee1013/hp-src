var deployHistoryGrid = {
    gridView: new RealGridJS.GridView("deployHistoryGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        deployHistoryGrid.initGrid();
        deployHistoryGrid.initDataProvider();
        deployHistoryGrid.event();
    },
    initGrid: function () {
        deployHistoryGrid.gridView.setDataSource(deployHistoryGrid.dataProvider);
        deployHistoryGrid.gridView.setStyles(deployHistoryGridBaseInfo.realgrid.styles);
        deployHistoryGrid.gridView.setDisplayOptions(deployHistoryGridBaseInfo.realgrid.displayOptions);
        deployHistoryGrid.gridView.setColumns(deployHistoryGridBaseInfo.realgrid.columns);
        deployHistoryGrid.gridView.setOptions(deployHistoryGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        deployHistoryGrid.dataProvider.setFields(deployHistoryGridBaseInfo.dataProvider.fields);
        deployHistoryGrid.dataProvider.setOptions(deployHistoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        deployHistoryGrid.gridView.onDataCellClicked = function (gridView, index) {
            historyJs.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        deployHistoryGrid.dataProvider.clearRows();
        deployHistoryGrid.dataProvider.setRows(dataList);
    }
};

var historyJs = {
    /***
     * 초기화
     */
    init: function () {
        this.initSearchDate('0');
        this.bindingEvent();
        this.initAjaxForm();
    },
    initAjaxForm: function () {
        //code
    },
    /***
     * 이벤트 바인딩
     */
    bindingEvent: function () {
        $("#actionType").change(function () {
            if ($(this).val() == "BUILD") {
                $("#appType option").eq(0).prop("selected",true);
                $("#appType").prop("readonly", true).prop("disabled",true);
                $("#dictType option").eq(0).prop("selected",true);
                $("#dictType").prop("readonly", true).prop("disabled",true);
            } else {
                $("#appType").prop("readonly", false).prop("disabled",false);
                $("#dictType").prop("readonly", false).prop("disabled",false);
            }
        });
        $('#searchBtn').bindClick(historyJs.search);
        $("#searchResetBtn").bindClick(historyJs.searchReset);
        $("#excelDownloadBtn").bindClick(historyJs.excelDownload);
    },
    excelDownload: function () {
        if (deployHistoryGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "배포이력_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        deployHistoryGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    searchReset: function () {
        historyJs.setDate.setStartDate(0);
        historyJs.setDate.setEndDate(0);
        historyJs.initSearchDate('0');
        $("#actionType option").eq(0).prop("selected", true);
        $("#service option").eq(0).prop("selected", true);
        $("#appType option").eq(0).prop("selected", true);
        $("#appType").prop("readonly", false).prop("disabled",false);
        $("#dictType").prop("readonly", false).prop("disabled",false);
        $("#dictType option").eq(0).prop("selected", true);
        $("#successYn option").eq(0).prop("selected", true);
    },
    gridRowSelect: function (selectRowId) {
        //code
    },
    initSearchDate: function (flag) {
        historyJs.setDate = Calendar.datePickerRange('startDeployDt', 'endDeployDt');

        historyJs.setDate.setEndDate(0);
        historyJs.setDate.setStartDate(flag);

        /* 날짜 클릭시 버튼 초기화 */
        $("#startDeployDt, #endDeployDt").datepicker("option", "onSelect", function () {
            $('.ui.button.small:button').each(function () {
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark");
            });
            historyJs.initDateRange();
        });

        /* 버튼색상변경 */
        var buttonId = '';
        if (flag === '0') {
            buttonId = 'todayBtn';
        } else if (flag === '-1d') {
            buttonId = 'onedayBtn';
        } else if (flag === '-6d') {
            buttonId = 'weekBtn';
        } else if (flag === '-1m') {
            buttonId = 'monthBtn';
        }

        $('.ui.button.small:button').each(function () {
            if ($(this).attr('id') === buttonId) {
                $(this).removeClass("gray-dark");
                $(this).addClass("cyan");
            } else {
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark");
            }
        });
    },
    search: function () {
        if (moment($("#endDeployDt").val()).diff($("#startDeployDt").val(), "month", true) > 1) {
            alert("최대 조회 기간은 1개월 입니다.");
            return false;
        }

        CommonAjax.basic({
            cache: false,
            url: '/search/deploy/history.json',
            method: 'get',
            dataType: 'json',
            data: {
                startDeployDt: $("#startDeployDt").val(),
                endDeployDt: $("#endDeployDt").val(),
                actionType: $("#actionType option:selected").val(),
                appType: $("#appType option:selected").val(),
                service: $("#service option:selected").val(),
                dictType: $("#dictType option:selected").val(),
                successYn: $("#successYn option:selected").val()
            },
            successMsg: null,
            callbackFunc: function (res) {
                deployHistoryGrid.setData(res.data);
                /* 엑셀 다운로드를 위한 변수 설정 */
                $("#totalCount").html(res.data.length);
                $('#excelStartDt').val($('#startDeployDt').val());
                $('#excelEndDt').val($('#endDeployDt').val());
            }
        });
    }
};

