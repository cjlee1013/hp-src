/**
 * 날짜포맷
 * @param f
 * @returns {string|void|*}
 */
Date.prototype.format = function (f) {
    if (!this.valueOf()) return " ";
    let weekKorName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    let weekKorShortName = ["일", "월", "화", "수", "목", "금", "토"];
    let weekEngName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let weekEngShortName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    let d = this;
    return f.replace(/(yyyy|yy|MM|dd|KS|KL|ES|EL|HH|hh|mm|ss|a\/p)/gi, function ($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear(); // 년 (4자리)
            case "yy": return (d.getFullYear() % 1000).zf(2); // 년 (2자리)
            case "MM": return (d.getMonth() + 1).zf(2); // 월 (2자리)
            case "dd": return d.getDate().zf(2); // 일 (2자리)
            case "KS": return weekKorShortName[d.getDay()]; // 요일 (짧은 한글)
            case "KL": return weekKorName[d.getDay()]; // 요일 (긴 한글)
            case "ES": return weekEngShortName[d.getDay()]; // 요일 (짧은 영어)
            case "EL": return weekEngName[d.getDay()]; // 요일 (긴 영어)
            case "HH": return d.getHours().zf(2); // 시간 (24시간 기준, 2자리)
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2); // 시간 (12시간 기준, 2자리)
            case "mm": return d.getMinutes().zf(2); // 분 (2자리)
            case "ss": return d.getSeconds().zf(2); // 초 (2자리)
            case "a/p": return d.getHours() < 12 ? "오전" : "오후"; // 오전/오후 구분
            default: return $1;
        }
    });
};

String.prototype.string = function (len) { let s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };

/**
 * contains 비교함수
 * @param element
 * @returns {boolean}
 */
Array.prototype.contains = function (element) {
    for (let idx = 0; idx < this.length; idx++){
        if(this[idx] == element){
            return true;
        }
    }
    return false;
};

/**
 * 시간 처리 메소드
 * @param h
 * @returns {Date}
 */
Date.prototype.addHours = function(hour) {
    this.setTime(this.getTime() + (hour * 60 * 60 * 1000));
    return this;
};
Date.prototype.minusDays = function(days) {
    this.setTime(this.getTime() - (days * 24 * 60 * 60 * 1000));
    return this;
};
Date.prototype.minusWeek = function(weeks) {
    this.setTime(this.getTime() - (weeks * 7 * 24 * 60 * 60 * 1000));
    return this;
};

/**
 * 검색증감률 날짜 Enum
 * @type {{MONTH: string, WEEK: string, DAY: string}}
 */
const DateType = {
    DAY: 'day',
    WEEK: 'week',
    MONTH: 'month'
};

/**
 * Date 함수 Format 관련 Enum
 * @type {{POST_FORMAT: string, TZ_FIRST_FORMAT: string, RANGE_MONTH_FORMAT: string, TZ_LAST_FORMAT: string, VIEW_DAY_FORMAT: string, VIEW_WEEK_FORMAT: string, VIEW_MONTH_FORMAT: string, EXCEL_FORMAT: string, RANGE_FORMAT: string}}
 */
const DateFormat = {
    RANGE_FORMAT: 'yyyy-MM-dd',
    RANGE_MONTH_FORMAT: 'yyyy-MM',
    POST_FORMAT: 'yyyy-MM-dd',
    TZ_FIRST_FORMAT: 'T00:00:00',
    TZ_LAST_FORMAT: 'T23:59:59',
    VIEW_DAY_FORMAT: 'HH:mm',
    VIEW_WEEK_FORMAT: 'KS',
    VIEW_MONTH_FORMAT: 'dd',
    EXCEL_FORMAT: 'yyyyMMddHHmm'
};

/**
 * Realgrid column이름
 * @type {{PAST: string, CURRENT: string}}
 */
const HeaderType = {
    PAST: 'pastCount',
    CURRENT: 'currentCount'
};

/**
 * 사이트 타입 구분
 * @type {{CLUB: string, EXP: string, HOME: string}}
 */
const SiteType = {
    HOME: 'home',
    CLUB: 'club',
    EXP: 'exp'
};

const utils = {
    getCommaCalc: function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
};
/**
 * 결과 화면
 */
const increaseRateGrid = {
    gridView : new RealGridJS.GridView("increaseRateGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commonGridBase.init(this, increaseRateGridBaseInfo);
    },
    clearData: function(){
        this.dataProvider.clearRows();
    },
    setData : function(jsonResult, gridResult) {
        this.clearData();
        this.dataProvider.setRows(gridResult);

        /** css **/
        //this.setColumnReplace();

        /** Column rename **/
        this.updateColumn(HeaderType.PAST, jsonResult.past.column);
        this.updateColumn(HeaderType.CURRENT, jsonResult.current.column);

        /** Grid select none **/
        this.selectNone();

        /** Grid Height resize **/
        this.displaySize();
    },
    /** Grid 행이름 변경 **/
    updateColumn: function(column, name){
        let header = this.gridView.getColumnProperty(column, 'header');
        header.text = name;
        this.gridView.setColumnProperty(column, 'header', header);
    },
    setColumnReplace : function (){
        this.gridView.setStyles({
            body:{
                dynamicStyles: function(grid, index) {
                    let increaseRate = grid.getValue(index.itemIndex, "increaseRate");
                    if(increaseRate.startsWith('-')){
                        return {
                            foreground: "#e83f3f"
                        }
                    }
                }
            }
        });
    },
    /**  엑셀 다운로드  **/
    excelDownload: function(){
        let startDt = $('#startDt').val().replace(/-/g,'');
        let endDt = $('#endDt').val().replace(/-/g, '');
        let fileName = [];
        switch ($('#siteType').val()) {
            case SiteType.HOME:
                fileName.push('홈플러스_'); break;
            case SiteType.CLUB:
                fileName.push('더클럽_'); break;
            case SiteType.EXP:
                fileName.push('익스프레스_'); break;
            default:
        }
        fileName.push('검색증감률_');
        fileName.push(startDt + "-" + endDt + '.xlsx');

        this.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName.join(''),
            showProgress: true,
            indicator: "default",
            header: "default",
            footer: "default",
            applyFitStyle: true,
            progressMessage: "Excel downloading..",
            done: function () {
                //Do someting
            }
        });
    },
    /** Grid 화면 높이 조절 **/
    displaySize: function(){
        let rowCount = this.gridView.getItemCount();                  //화면의 행 수

        let panelHeight = this.gridView.getPanel().height;            //panel의 높이
        let headerHeight = this.gridView.getHeader().height;          //header의 높이
        let rowHeight = this.gridView.getDisplayOptions().rowHeight;  //행의 높이
        let footerHeight = this.gridView.getFooter().height;          //footer의 높이

        let totalHeight = headerHeight + footerHeight + panelHeight + (rowHeight * rowCount) + (this.gridView._gv._hscrollBar ? this.gridView._gv._hscrollBar.height() + 2 : 2);

        $("#increaseRateGrid").height(totalHeight);

        this.gridView.resetSize();
    },
    /** Grid 화면 row선택 못하도록 **/
    selectNone: function(){
        this.gridView.setSelectOptions({"style" : "none"});
    }
};

/**
 * 그리드 변수
 */
const commonGridBase = {
    grid : null,
    baseInfo : null,
    init : function(grid, baseInfo) {
        this.grid = grid;
        this.baseInfo = baseInfo;
        this.initGrid();
        this.initDataProvider();
    },
    initGrid : function() {
        this.grid.gridView.setDataSource(this.grid.dataProvider);
        this.grid.gridView.setStyles(this.baseInfo.realgrid.styles);
        this.grid.gridView.setDisplayOptions(this.baseInfo.realgrid.displayOptions);
        this.grid.gridView.setColumns(this.baseInfo.realgrid.columns);
        this.grid.gridView.setOptions(this.baseInfo.realgrid.options);
    },
    initDataProvider : function() {
        this.grid.dataProvider.setFields(this.baseInfo.dataProvider.fields);
        this.grid.dataProvider.setOptions(this.baseInfo.dataProvider.options);
    }
};

/**
 * 증감률 프로세스 변수
 */
const increaseRateJs = {
    /**
     * 초기화
     */
    init: function () {
        this.initSchDate();
    },
    /**
     * 조회 일자 초기화
     */
    initSchDate : function (type) {
        if(logicIncreaseRateJs.isEmpty(type)){
            $('#dateType').val(DateType.DAY);

            /** 첫페이지 호출시의 달력 설정 **/
            increaseRateJs.setDate = Calendar.datePickerRange('startDt', 'endDt');
            $("#startDt, #endDt").datepicker("option", "onSelect", function(dateText){
                /** 오늘 이후 선택 불가능 **/
                if(logicIncreaseRateJs.dateToEpoch(new Date().format(DateFormat.RANGE_FORMAT)) <=  logicIncreaseRateJs.dateToEpoch(dateText)){
                    alert('금일 이후의 날짜를 선택할 수 없습니다.');
                    logicIncreaseRateJs.selDateSetDateRange(new Date().format(DateFormat.RANGE_FORMAT));
                }else {
                    logicIncreaseRateJs.selDateSetDateRange(dateText);
                }
            });
        }else{
            $('#dateType').val(type);
            logicIncreaseRateJs.chgBtnImage();
        }
        /** 날짜 start, end 범위 설정 **/
        this.setDateRange(undefined);

    },
    setDateRange: function(selDate) {
        let type = $('#dateType').val();
        let date;
        if(logicIncreaseRateJs.isEmpty(selDate) ){
            date = new Date();
        }else{
            date = new Date(selDate);
        }

        switch (type) {
            case undefined:
                type = DateType.DAY;
                break;
            case DateType.DAY:
                this.setDate.setStartDate(date.format(DateFormat.RANGE_FORMAT));
                this.setDate.setEndDate(date.format(DateFormat.RANGE_FORMAT));
                break;
            case DateType.WEEK:
                this.setDate.setStartDate(logicIncreaseRateJs.getWeekDay(date, 1, DateFormat.RANGE_FORMAT));
                this.setDate.setEndDate(logicIncreaseRateJs.getWeekDay(date, 7, DateFormat.RANGE_FORMAT));
                break;
            case DateType.MONTH:
                this.setDate.setStartDate(logicIncreaseRateJs.getMonthDay(date, 0, DateFormat.RANGE_FORMAT));
                this.setDate.setEndDate(logicIncreaseRateJs.getMonthDay(date, 1, DateFormat.RANGE_FORMAT));
                break;
            default:
        }
    },
    /** 초기화 버튼 클릭시 이벤트 **/
    initBtn: function(){
        /** Clear Grid data **/
        increaseRateGrid.clearData();
        /** Init Date Type Day **/
        increaseRateJs.initSchDate(DateType.DAY);
        /** Set Site type HOME **/
        $("input:radio[name='siteTypeRadio']:radio[value='"+SiteType.HOME+"']").prop('checked', true);
        $('#siteType').val(SiteType.HOME);
    },
    /** 검색 **/
    searchBtn : function() {
        getForResponseIncreaseRateJs.getIncreaseData();
        if($("#dateType").val() == "month"){
            getForResponseIncreaseRateJs.getSearchCount();
            $("#monthSearchStatic").show();
        }else{
            $("#monthSearchStatic").hide();
        }
    },
    /** 엑셀 **/
    excelBtn : function() {
        increaseRateGrid.excelDownload();
    }
};

const logicIncreaseRateJs = {
    /** 빈값 여부 **/
    isEmpty: function(obj) {
        if(typeof obj == undefined || obj == null || obj == "")
            return true;
        else
            return false;
    },
    /** 빈값 여부 반대 **/
    isNotEmpty: function(obj) {
        return !logicIncreaseRateJs.isEmpty(obj);
    },
    /** 입력받은 날짜에서 지정한 요일번호로 된 Date반환 **/
    getWeekDay: function(date, weekNum, format){
        let day = date.getDay();
        let diff = date.getDate() - day + (day == 0 ? -6 : weekNum);
        return new Date(date.setDate(diff)).format(format);
    },
    /** 입력받은 날짜의 1일과 그달의 마지막 날짜 **/
    getMonthDay: function(date, monthNum, format){
        switch (monthNum) {
            case 0:
                return new Date(date.getFullYear(), date.getMonth(), 1).format(format);
            case 1:
                return new Date(date.getFullYear(), date.getMonth()+1, 0).format(format);
            default:
        }
    },
    /** 버튼 클릭시 색상변경 이벤트 **/
    chgBtnImage : function (){
        let btnId = $('#dateType').val();
        $('.ui.button.small:button').each(function(){
            if($(this).attr('id') == btnId+'Btn'){
                $(this).removeClass("gray-dark");
                $(this).addClass("cyan");
            }else {
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark");
            }
        });
    },
    /** 달력에서 직접 날짜를 선택하였을 경우 **/
    selDateSetDateRange: function (dateText) {
        increaseRateJs.setDateRange(dateText);
    },
    /** 결과 데이터 파싱 **/
    resultDataParsing: function (res) {
        /** datePicker to epochTime **/
        let epochStartTime = logicIncreaseRateJs.dateToEpoch($('#startDt').val() + DateFormat.TZ_FIRST_FORMAT);
        let epochEndTime = logicIncreaseRateJs.dateToEpoch($('#endDt').val() + DateFormat.TZ_LAST_FORMAT);

        let jsonResult = new Object();
        logicIncreaseRateJs.setJsonColumn(jsonResult, $('#startDt').val());

        let pastJsonArray = new Array();        //과거
        let currentJsonArray = new Array();     //현재
        $.each(res, function(index, item){
            let epochTime = logicIncreaseRateJs.dateToEpoch(item.dateTime);
            if(epochStartTime <= epochTime && epochTime <= epochEndTime){
                logicIncreaseRateJs.setJsonArray(currentJsonArray, item);
            }else{
                logicIncreaseRateJs.setJsonArray(pastJsonArray, item);
            }
        });

        /** Set Array json **/
        jsonResult.past.data = pastJsonArray;
        jsonResult.current.data = currentJsonArray;

        /** RealGrid 형식 **/
        let gridResult = new Array();

        /** Grid에 맞는 형식의 Json **/
        logicIncreaseRateJs.setGridResult(gridResult, jsonResult.past.data, jsonResult.current.data);

        /** Set View **/
        increaseRateGrid.setData(jsonResult, gridResult);

        /** chart js line chart **/
        logicIncreaseRateJs.setChart(jsonResult,gridResult);
    },
    /** 차트 **/
    setChart : function(jsonResult,gridResult){
        let myChart;
        let range = [];
        let currentData = [];
        let pastData = [];

        $.each(gridResult, function (index) {
            if(gridResult[index].range !== "전체") {
            range.push(gridResult[index].range);
            currentData.push(gridResult[index].chartCurrentCount);
            pastData.push(gridResult[index].chartPastCount);
            }
        });

        var ctx = document.getElementById('myChart').getContext('2d');
        var data = {
                type: 'line',
                data: {
                    labels: range,
                    datasets: [
                        {
                            label: jsonResult.past.column,
                            data: pastData,
                            borderColor: "rgba(75, 192, 192)",
                            borderDash: [5,5],
                            fill: false,
                            lineTension: 0
                        },
                        {
                            label: jsonResult.current.column,
                            data: currentData,
                            borderColor: "rgba(255,10,13,255)",
                            pointStyle: 'rectRot',
                            pointRadius: 5,
                            pointBorderColor: 'rgba(255,10,13,255)',
                            fill: false,
                            lineTension: 0
                        }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: '통합 검색 횟수 '
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    plugins: {
                        legend: {
                            labels: {
                                usePointStyle: true
                            }
                        }
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true
                            }
                        }]
                    },
                }
            };
        if(window.myChart.data != undefined){
            window.myChart.destroy();
        }
        window.myChart = new Chart(ctx, data);
    },
    /** 전월 검색 횟수 결과 셋  **/
    setSearchCountResult: function(res){
        $("span[name=totalCount]").html(utils.getCommaCalc(res.data.count));
        $("span[name=successCount]").html(utils.getCommaCalc(res.data.successCount));
        $("span[name=failureCount]").html(utils.getCommaCalc(res.data.failureCount));
        $("span[name=exceptionCount]").html(utils.getCommaCalc(res.data.exceptionCount));
        if(res.data.count > 0 ){
            $("span[name=successRate]").html("("+res.data.successRate + "%)");
            $("span[name=failureRate]").html("("+res.data.failureRate + "%)");
            $("span[name=exceptionRate]").html("("+res.data.exceptionRate + "%)");
        }

    },
    /** Grid용 JsonArray **/
    setGridResult: function(gridResult, pastData, currentData){
        let data = pastData.length > currentData.length ? pastData : currentData;

        let pastTotalCount = 0;
        let currentTotalCount = 0;

        $.each(data, function(index, item){
            let pastCount = logicIncreaseRateJs.isNotEmpty(pastData[index]) ? pastData[index].count : 0;
            let currentCount = logicIncreaseRateJs.isNotEmpty(currentData[index]) ? currentData[index].count : 0;

            pastTotalCount += pastCount;
            currentTotalCount += currentCount;

            gridResult.push(
                {
                    range : item.range,
                    pastCount: pastCount,
                    currentCount: currentCount,
                    chartPastCount: pastCount,
                    chartCurrentCount: currentCount,
                    increaseRate: logicIncreaseRateJs.getIncreaseRate(pastCount, currentCount),
                }
            );
        });

        gridResult.push(
            {
                range : "전체",
                pastCount: pastTotalCount,
                currentCount: currentTotalCount,
                chartPastCount: pastTotalCount,
                chartCurrentCount: currentTotalCount,
                increaseRate: logicIncreaseRateJs.getIncreaseRate(pastTotalCount, currentTotalCount),
            }
        );
    },
    /** 증감률 계산 **/
    getIncreaseRate: function(pastCount, currentCount){
        // (current - past) / past * 100
        if(pastCount != 0 && currentCount != 0){
            return ((currentCount - pastCount) / pastCount * 100).toFixed(2) + '%';
        } else {
            return '-';
        }
    },
    /** RealGrid Column값 설정 **/
    setJsonColumn: function(jsonResult){
        let pastName;
        let currentName;
        switch ($('#dateType').val()) {
            case DateType.DAY:
                pastName = new Date($('#startDt').val()).minusDays(1).format(DateFormat.RANGE_FORMAT);
                currentName = new Date($('#startDt').val()).format(DateFormat.RANGE_FORMAT);
                break;
            case DateType.WEEK:
                pastName = new Date($('#startDt').val()).minusWeek(1).format(DateFormat.RANGE_FORMAT) + ' ~ ' + new Date($('#endDt').val()).minusWeek(1).format(DateFormat.RANGE_FORMAT);
                currentName = $('#startDt').val() + ' ~ ' + $('#endDt').val();
                break;
            case DateType.MONTH:
                pastName = new Date($('#startDt').val()).minusDays(1).format(DateFormat.RANGE_MONTH_FORMAT);
                currentName = new Date($('#startDt').val()).format(DateFormat.RANGE_MONTH_FORMAT);
                break;
            default:
        }

        /** Column name Set **/
        jsonResult.past = {
            column: pastName
        };
        jsonResult.current = {
            column: currentName
        };
    },
    /** 검색결과 타입에 맞게 분류 **/
    setJsonArray: function(jsonArray, item){
        let range;
        switch ($('#dateType').val()) {
            case DateType.DAY:
                range = new Date(item.dateTime).format(DateFormat.VIEW_DAY_FORMAT) + ' ~ ' + new Date(item.dateTime).addHours(1).format(DateFormat.VIEW_DAY_FORMAT);
                break;
            case DateType.WEEK:
                range = new Date(item.dateTime).format(DateFormat.VIEW_WEEK_FORMAT);
                break;
            case DateType.MONTH:
                range = new Date(item.dateTime).format(DateFormat.VIEW_MONTH_FORMAT) + '일';
                break;
            default:
        }
        /** Data Set **/
        jsonArray.push({
            range : range,
            count: item.docCount
        });

        return jsonArray;
    },
    /** 날짜를 Unix EpochTIme으로 변환 **/
    dateToEpoch: function(date){
        return Math.round(new Date(date).getTime() / 1000.0);
    }
};

/**
 * URL 콜 부분
 * @type {{getIncreaseData: (function(): *)}}
 */
const getForResponseIncreaseRateJs = {
    /**
     * 결과 가져오기
     */
    getIncreaseData: function () {
        CommonAjax.basic({
            url: '/search/statistics/getIncreaseSearchLogList.json',
            method: 'get',
            dataType: 'json',
            data: {
                dateType: $('#dateType').val(),
                siteType: $('#siteType').val(),
                startDt: new Date($('#startDt').val()).format(DateFormat.POST_FORMAT),
                endDt: new Date($('#endDt').val()).format(DateFormat.POST_FORMAT)
            },
            successMsg: null,
            callbackFunc: function (res) {
                if(logicIncreaseRateJs.isEmpty(res) || res.searchIncreaseRateList.length === 0){
                    alert('검색 결과가 없습니다.');
                }else {
                    logicIncreaseRateJs.resultDataParsing(res.searchIncreaseRateList);
                }
            },
            errorCallbackFunc: function (resError){
                alert('색인된 검색 로그의 범위가 벗어났거나 검색결과를 가져올 수 없습니다.');
                console.log(resError);
            }
        });
    },

    /**
     * 월별 검색 카운트 조회
     */
    getSearchCount: function() {
        CommonAjax.basic({
            url: '/search/statistics/getSearchLogCount.json',
            method: 'get',
            dataType: 'json',
            data: {
                siteType: $('#siteType').val(),
                startDt: new Date($('#startDt').val()).format(DateFormat.POST_FORMAT),
                endDt: new Date($('#endDt').val()).format(DateFormat.POST_FORMAT)
            },
            successMsg: null,
            callbackFunc: function (res) {
                logicIncreaseRateJs.setSearchCountResult(res);
            },
            errorCallbackFunc: function (resError){
                alert('색인된 검색 로그의 범위가 벗어났거나 검색결과를 가져올 수 없습니다.');
                console.log(resError);
            }
        });
    }
};