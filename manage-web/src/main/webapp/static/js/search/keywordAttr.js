var keywordAttrGrid = {
    gridView : new RealGridJS.GridView("keywordAttrGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        keywordAttrGrid.initGrid();
        keywordAttrGrid.initDataProvider();
        keywordAttrGrid.event();
    },
    page : 0,
    initGrid : function () {
        keywordAttrGrid.gridView.setDataSource(keywordAttrGrid.dataProvider);

        keywordAttrGrid.gridView.setStyles(keywordAttrListGridBaseInfo.realgrid.styles);
        keywordAttrGrid.gridView.setDisplayOptions(keywordAttrListGridBaseInfo.realgrid.displayOptions);
        keywordAttrGrid.gridView.setColumns(keywordAttrListGridBaseInfo.realgrid.columns);
        keywordAttrGrid.gridView.setOptions(keywordAttrListGridBaseInfo.realgrid.options);
        keywordAttrGrid.gridView.setCheckBar({ visible: false });
        keywordAttrGrid.gridView.setIndicator({
            visible: false
        });
        var hoverMaskValue = $('#selHoverMask').val();
        keywordAttrGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        keywordAttrGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > keywordAttrGrid.page) {
                keywordAttrGrid.page += 50;
                replacedKeywordJs.search(keywordAttrGrid.dataProvider.getRowCount(),replacedKeywordJs.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        keywordAttrGrid.dataProvider.setFields(keywordAttrListGridBaseInfo.dataProvider.fields);
        keywordAttrGrid.dataProvider.setOptions(keywordAttrListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        keywordAttrGrid.gridView.onDataCellClicked = function(gridView, index) {
            keywordAttrGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        keywordAttrGrid.dataProvider.clearRows();
        keywordAttrGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = keywordAttrGrid.dataProvider.getJsonRow(selectRowId);
        keywordAttrJs.selectedIndex(rowDataJson);
    },
    excelDownload: function() {

        $("#searchKeyword").val('');
        $("input[name=inner]").prop('checked', false);
        replacedKeywordJs.initSearchDate('-5y');

        //전체 다운로드를 위해 전체 데이터를 RealGrid에 바인딩 해야 한다.
        replacedKeywordJs.search(0,100000000, function() {
            var _date = new Date();
            var fileName = "대체검색어_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            keywordAttrGrid.gridView.exportGrid( {
                type: "excel",
                target: "local",
                indicator: "default",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        });
    }
};

var replaceId  = /[!@#$%^&*()/'";:\\{},.?\[\]]/gi;
var keywordAttrJs = {
    event : function() {
        $('#searchBtn').click(function(){
            keywordAttrJs.search();
        });
        $("#searchResetBtn").click(function() {
            keywordAttrJs.searchReset();
        });
        $("#deleteBtn").bindClick(keywordAttrJs.delete);
        $("#getGAttrBtn").click(function() {
            keywordAttrJs.getGAattr();
        });
        $("#saveBtn").click(function(){
            keywordAttrJs.put();
        });
        $("#resetBtn").click(function() {
            keywordAttrJs.reset();
        });
        $('#keyword').calcTextLength('keyup focusout', '#textCount_keyword');
        $('#keyword').notAllowInput('focusout', ['SYS_DIVISION']);

        $(document).on("keyup", "input[name='priority']", function() {
            $(this).allowInput("keyup", ["NUM"], $(this).attr("id"));
            let chGattrNo = $(this).closest("tr").children().eq(0).html();
            let priority = $(this).val();
            $("input[name='priority']").each(function () {
                if($(this).val() >= priority) {
                    let gattrNo = $(this).closest("tr").children().eq(0).html();
                    if (chGattrNo != gattrNo) {
                        $(this).val(Number($(this).val()) + 1);
                    }
                }
            });
        });
        $("input[name='priority']").on("keyup");
    },
    searchReset : function () {
        $("#searchKeyword").val("");
        $("#searchUseYn").val("");
    },
    search : function () {
        keywordAttrJs.getGAattrList();
    },
    getGAattrList : function() {
        CommonAjax.basic({url:'/search/keywordAttr/getKeywordAttrList.json?searchKeyword=' + encodeURI($('#searchKeyword').val()) + '&searchUseYn=' + $('#searchUseYn').val(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            $("#totalCount").html(res.length);
            keywordAttrGrid.setData(res);
            }});
    },
    getGAattr : function() {
        CommonAjax.basic({url:'/item/mngAttribute/getMngAttributeGroupList.json?searchGattrType=ATTR&searchUseYn=Y&searchType=gattrMngNm&searchKeyword=' + $('#searchGAttr').val(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                const resultCount = res.length;
                if (resultCount == 0) {
                    $('#searchGAttrLayer').html('<li class="ui-menu-item-wrapper" >검색결과가 없습니다.</li>');
                } else {
                    $('#searchGAttrLayer').html('');
                    for (let idx = 0; resultCount > idx; idx++) {
                        let data = res[idx];
                        $('#searchGAttrLayer').append(
                            '<li class="ui-menu-item-wrapper" id="gattrSearchList_'
                            + data.gattrNo + '"' + '">' + data.gattrMngNm + ' | ' + data.gattrNo
                            + '</li>');
                    }
                    keywordAttrJs.addGattr();
                }
                $('#searchGAttrLayer').show();
            }});
    },
    addGattr : function() {
        $('li[id^=gattrSearchList_]').click(function() {
            let htmlArr = $(this).html().split('|');
            let gattrNo = htmlArr[1];
            let gattrMngNm = htmlArr[0];

            keywordAttrJs.addGAttrHtml($.trim(gattrNo), $.trim(gattrMngNm), '');
            $('#searchGAttrLayer').hide();
        });
    },
    addGAttrHtml : function(gattrNo, gattrMngNm, priority) {
        if (typeof $("#gattr_tr_" + gattrNo).html() != "undefined") {
            alert("이미 등록된 분류입니다.");
            return;
        }

        // 분류명 -> 관리분류명으로 변경되면서 undefined 노출되지 않도록 추가
        if ($.jUtil.isEmpty(gattrMngNm)) {
            gattrMngNm = "";
        }

        $("#gattrEmptyTr").hide();
        $('#gattrTable').append(
            '<tr class="gattrTr" id="gattr_tr_'+gattrNo+'">'
            + '<td style="border: 1px solid #cccccc; text-align: center;">' + gattrNo + '</td>'
            + '<td style="border: 1px solid #cccccc; text-align: center;">' + gattrMngNm + '</td>'
            + '<td style="border: 1px solid #cccccc; text-align: center;"><input type="text" name="priority" value="' + priority + '" style="width: 50px;"></td>'
            + '<td style="border: 1px solid #cccccc;" class="attrHtml" id="attrHtml_'+gattrNo+'"></td>'
            + '<td style="border: 1px solid #cccccc; text-align: center;"><button type="button" class="ui button small mg-l-5 mg-r-5" onclick="keywordAttrJs.delGAttr(' + gattrNo + ')">삭제</button></td>'
            + '</tr>>');
        keywordAttrJs.getAttr(gattrNo);
    },
    delGAttr : function(gattrNo) {
        $("#gattr_tr_" + gattrNo).remove();
        $('button[id^=gattr_tr_]')

        let gattrCount = 0
        $(".gattrTr").each(function () {
            gattrCount++;
        });

        if (gattrCount == 0) {
            $("#gattrEmptyTr").show();
        }
    },
    getAttr : function(gattrNo) {
        CommonAjax.basic({url:'/item/mngAttribute/getMngAttributeList.json?gattrNo=' + gattrNo, data:null, method:"GET", successMsg:null, async: false, callbackFunc:function(res) {
            const resultCount = res.length;
            let dispCount = 0;
            let attrHtml = "<span>";
            if (resultCount > 0) {
                for (let idx = 0; resultCount > idx; idx++) {
                    let data = res[idx];
                    if (data.useYn == 'Y') {
                        dispCount++;
                        attrHtml += '<label class="ui checkbox inline mg-t-5">'
                            + '<input type="checkbox" id="attrNo_' + data.attrNo
                            + '" value="' + data.attrNo + '">'
                            + '<span class="text">' + data.attrNm + '</span>'
                            + '</label>';
                    }
                }
            }
            if (dispCount == 0) {
                attrHtml += "등록된 속성이 없습니다.";
            }
            attrHtml += "</span>";
            $('#attrHtml_'+gattrNo+'').html(attrHtml);
        }});
    },
    put : function() {
        if (!$("#keyword").val()) {
            alert("키워드를 입력해주세요.");
            return;
        }

        let attrMngList = new Array();
        let priorityValid = true;
        let attrValid = true;
        $(".gattrTr").each(function () {
            let gattrNo = $(this).children().eq(0).html();
            if ($(this).find("input[name='priority']").val() == '') {
                priorityValid = false;
                return;
            }
            if ($(this).find(".attrHtml").find("input").length > 0) {
                if ($(this).find(".attrHtml").find("input:checked").length == 0) {
                    attrValid = false;
                    return;
                }
                let priority = $(this).find("input[name='priority']").val();
                $(this).find(".attrHtml").find("input:checked").each(function() {
                    let attrMng = new Object();
                    attrMng.gattrNo = gattrNo;
                    attrMng.priority = priority;
                    attrMng.attrNo = $(this).val();
                    attrMngList.push(attrMng);
                });
            }
        });

        if (!priorityValid) {
            alert("우선순위를 입력해 주세요.");
            return;
        }

        if (!$("input[name=useYn]:checked").val()) {
            alert("사용여부를 선택해주세요.");
            return;
        }

        if ($("input[name=useYn]:checked").val() == 'Y')
        {
            if (!attrValid || attrMngList.length == 0) {
                alert("속성정보는 최소 1개 이상 선택되어야 합니다.");
                return;
            }
        }

        let edit = false;
        if ($("#keywordAttrNo").val() != '') {
            edit = true;
        }
        let gattrData = new Object();
        if (edit) {
            gattrData.keywordAttrNo = $("#keywordAttrNo").val();
        }
        gattrData.keyword = $("#keyword").val();
        gattrData.useYn = $("input[name=useYn]:checked").val();
        gattrData.keywordAttrMngList = attrMngList;

        let url;
        if (edit) {
            url = '/search/keywordAttr/putKeywordAttr.json';
        } else {
            url = '/search/keywordAttr/postKeywordAttr.json';
        }

        CommonAjax.basic({url:url, data:JSON.stringify(gattrData), method:"POST", contentType: 'application/json', callbackFunc:function(res) {
            if (res.data == 1) {
                if(edit) {
                    alert('수정 되었습니다');
                } else {
                    alert('등록 되었습니다');
                }
                keywordAttrJs.getGAattrList();
            } else if(res.data == -1) {
                alert('이미 등록된 키워드 입니다.');
            }
        }});
    },
    reset : function () {
        $("#keywordAttrNo").val("");
        $("#keywordAttrNohtml").html("");
        $("#keyword").val("");
        $("input[name='useYn']").prop('checked', false);
        $("#gattrTable").html('<tr id="gattrEmptyTr"><td colspan="5" style="text-align: center">선택된 분류가 없습니다.</td></tr>');
    },
    selectedIndex : function(jsonRow) {
        keywordAttrJs.reset();
        $('#keywordAttrNo').val(jsonRow.keywordAttrNo);
        $('#keywordAttrNohtml').html(jsonRow.keywordAttrNo);
        $("#keyword").val(jsonRow.keyword);
        $("#textCount_keyword").html(jsonRow.keyword.length);
        if (jsonRow.useYn == "사용") {
            $("input[name='useYn'][value='Y']").prop('checked', true);
        } else {
            $("input[name='useYn'][value='N']").prop('checked', true);
        }


        CommonAjax.basic({url:'/search/keywordAttr/getKeywordAttrMng.json?keywordAttrNo=' + jsonRow.keywordAttrNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            keywordAttrJs.setAttr(res.keywordAttrMngList);
        }});
    },
    setAttr : function(attrMngList) {
        $.each(attrMngList, function(i,o) {
            if (typeof $("#gattr_tr_" + o.gattrNo).html() == "undefined") {
                CommonAjax.basic({url:'/item/mngAttribute/getMngAttributeGroupList.json?searchGattrType=ATTR&searchUseYn=Y&searchType=gattrNo&searchKeyword=' + o.gattrNo, data:null, method:"GET", successMsg:null, async: false, callbackFunc:function(res) {
                        const resultCount = res.length;
                        if (resultCount > 0) {
                            for (let idx = 0; resultCount > idx; idx++) {
                                let data = res[idx];
                                keywordAttrJs.addGAttrHtml(data.gattrNo, data.gattrMngNm, o.priority);
                            }
                            keywordAttrJs.addGattr();
                        }
                    }});
            }
            $("#attrNo_" + o.attrNo).prop('checked', true);
        });
    }
};