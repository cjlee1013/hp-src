var searchDataExportJs = {
    init : function() {
        searchDataExportJs.getStoreList();
        $("#searchStoreType").on("change", function() {searchDataExportJs.getStoreList()});
        $("#excelDownloadBtn").on("click", function() {searchDataExportJs.excelDownload()});
    },
    getStoreList: function() {
        CommonAjax.basic({
            url: '/item/store/getStoreList.json',
            data:  JSON.stringify($('#searchForm').serializeObject()),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                $('#storeId').children().remove();
                $.each(res, function (i, e) {
                    $('#storeId').append($("<option></option>").attr("value",e.storeId).text(e.storeNm+"("+e.storeId+")"));
                });
            }
        });
    },
    excelDownload: function() {
        if ($("input[name='fields']:checked").length == 0) {
            alert('출력필드를 선택해주세요.');
            return;
        }
        var includeFields = '';
        $("input[name='fields']:checked").each(function(e){
            if (includeFields != '') {
                includeFields += ',';
            }
            includeFields += $(this).val();
        });
        var param = "?storeNm=" + $('#storeId option:selected').text();
        param += "&storeType=" + $('#searchStoreType').val();
        param += "&storeId=" + $('#storeId').val();
        param += "&sort=" + $("input[name=sort]:checked").val();
        param += "&includeFields=" + includeFields;
        $('#excelDownloadFrame').attr('src', "/search/operation/searchDataExport/excel.json" + param);
    }
};