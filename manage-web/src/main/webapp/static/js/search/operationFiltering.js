



/***
 * black 리스트 그리드 설정
 * @type {{init: blackListGrid.init, setData: blackListGrid.setData, initDataProvider: blackListGrid.initDataProvider, excelDownload: blackListGrid.excelDownload, dataProvider: *, initGrid: blackListGrid.initGrid, event: blackListGrid.event, gridView: *}}
 */
var blackListRuleInfoGrid = {
    gridView : new RealGridJS.GridView("blackListRuleInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        blackListRuleInfoGrid.initGrid();
        blackListRuleInfoGrid.initDataProvider();
        blackListRuleInfoGrid.event();
    },
    initGrid : function () {
        blackListRuleInfoGrid.gridView.setDataSource(blackListRuleInfoGrid.dataProvider);
        blackListRuleInfoGrid.gridView.setStyles(blackListRuleInfoGridBaseInfo.realgrid.styles);
        blackListRuleInfoGrid.gridView.setDisplayOptions(blackListRuleInfoGridBaseInfo.realgrid.displayOptions);
        blackListRuleInfoGrid.gridView.setColumns(blackListRuleInfoGridBaseInfo.realgrid.columns);
        blackListRuleInfoGrid.gridView.setOptions(blackListRuleInfoGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        blackListRuleInfoGrid.dataProvider.setFields(blackListRuleInfoGridBaseInfo.dataProvider.fields);
        blackListRuleInfoGrid.dataProvider.setOptions(blackListRuleInfoGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        blackListRuleInfoGrid.gridView.onDataCellClicked = function(gridView, index) {
            blackListRuleInfoGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        blackListRuleInfoGrid.dataProvider.clearRows();
        blackListRuleInfoGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = blackListRuleInfoGrid.dataProvider.getJsonRow(selectRowId);
        filteringJs.selectedIndex(rowDataJson);
    }
};
var filteringJs = {
    init : function () {
        filteringJs.bindingEvent();
        $('#ruleMinuteFrequency,#ruleRepeatCount,#blacklistAddCount').allowInput("keyup", ["NUM"]);
        filteringJs.search();

    },
    bindingEvent : function() {
        $('#searchBtn').click(function() {
            filteringJs.search();
        });
        $("#modifyRule").click(function(){
            filteringJs.updateRuleInfo();
        })
        $("#resetBtn").click(function(){
            filteringJs.reset();
        })

    },
    search : function (_fn_callback) {

        let data = {};
        data.serviceType = $("#searchServiceType").val();
        data.blacklistType = $("#searchBlackListType").val();
        CommonAjax.basic(
            {
                url:'/search/operation/filtering/getBlackListRuleInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    blackListRuleInfoGrid.setData(res);
                    blackListRuleInfoGrid.gridRowSelect(0);

                    if(_fn_callback != null && _fn_callback != "undefined") {
                        _fn_callback();
                    }
                }
            });

    },
    updateRuleInfo : function() {

        let ruleMinuteFrequency = $("#ruleMinuteFrequency").val();
        let ruleRepeatCount = $("#ruleRepeatCount").val();
        let blacklistAddCount = $("#blacklistAddCount").val();
        let serviceType = $("#ruleServiceType").val();
        if(ruleMinuteFrequency == '' ||ruleRepeatCount == '' ||blacklistAddCount == ''){
            alert("설정 정보를 입력하세요.");
            return false;
        }

        var data = {};
        data.ruleNo = $("#ruleNo").val();
        data.ruleMinuteFrequency = ruleMinuteFrequency;
        data.ruleRepeatCount = ruleRepeatCount;
        data.blacklistAddCount = blacklistAddCount;
        data.serviceType = serviceType;

        CommonAjax.basic({
            url:'/search/operation/filtering/modifyBlackListRuleInfo.json',
            data: JSON.stringify(data),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            successMsg: null,
            callbackFunc: function (res) {
                if(res > 0) {
                    alert('수정되었습니다.');
                    filteringJs.search();
                }
            }
        })
    },
    selectedIndex : function(jsonRow) {
        $("input[name=ruleNo]").val(jsonRow.ruleNo);
        $("span[name=serviceType]").html(jsonRow.serviceType);
        $("span[name=blacklistType]").html(jsonRow.blacklistType);
        $("span[name=ruleHourInterval]").html(jsonRow.ruleHourInterval);
        $("span[name=ruleMinuteInterval]").html(jsonRow.ruleMinuteInterval);
        $("input[name=ruleMinuteFrequency]").val(jsonRow.ruleMinuteFrequency);
        $("input[name=ruleRepeatCount]").val(jsonRow.ruleRepeatCount);
        $("input[name=blacklistAddCount]").val(jsonRow.blacklistAddCount);
        $("#ruleServiceType").val(jsonRow.serviceType);

    },
    reset : function(){
        $("#searchBlackListType").val('');
        $('#searchServiceType').val('');
    }
};