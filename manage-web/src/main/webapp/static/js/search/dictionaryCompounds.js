/***
 * 복합명사 구성원 조회
 * @type {{init: compoundsSearchGrid.init, setData: compoundsSearchGrid.setData, initDataProvider: compoundsSearchGrid.initDataProvider, gridRowSelect: compoundsSearchGrid.gridRowSelect, dataProvider: *, initGrid: compoundsSearchGrid.initGrid, event: compoundsSearchGrid.event, gridView: *}}
 */
var compoundsSearchGrid = {
    gridView : new RealGridJS.GridView("compoundsSearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        compoundsSearchGrid.initGrid();
        compoundsSearchGrid.initDataProvider();
        compoundsSearchGrid.event();
    },
    initGrid : function () {
        compoundsSearchGrid.gridView.setDataSource(compoundsSearchGrid.dataProvider);

        compoundsSearchGrid.gridView.setStyles(dictionaryCompoundsSearchBaseInfo.realgrid.styles);
        compoundsSearchGrid.gridView.setDisplayOptions(dictionaryCompoundsSearchBaseInfo.realgrid.displayOptions);
        compoundsSearchGrid.gridView.setColumns(dictionaryCompoundsSearchBaseInfo.realgrid.columns);
        compoundsSearchGrid.gridView.setOptions(dictionaryCompoundsSearchBaseInfo.realgrid.options);
        compoundsSearchGrid.gridView.setCheckBar({ visible: false });
        compoundsSearchGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        compoundsSearchGrid.dataProvider.setFields(dictionaryCompoundsSearchBaseInfo.dataProvider.fields);
        compoundsSearchGrid.dataProvider.setOptions(dictionaryCompoundsSearchBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        compoundsSearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            compoundsSearchGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        compoundsSearchGrid.dataProvider.clearRows();
        compoundsSearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = compoundsSearchGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
        $("#wordName").val(rowDataJson.wordName);
        $('#dicType').val(rowDataJson.dicType);
        $('#pos').val(rowDataJson.pos);
        $('#description').val(rowDataJson.description);
    }
};

var upDown = [{
    label: "up",
    enabled: true,
    tag : 1
}, {
    label: "down",
    enabled: true,
    tag : 0
}];

var remove = [{
    label : "del",
    enabled : true,
    tag : "del"
}];

/***
 * 복합명사 : 검색용
 * @type {{init: compoundsResultGrid.init, setData: compoundsResultGrid.setData, initDataProvider: compoundsResultGrid.initDataProvider, gridRowSelect: compoundsResultGrid.gridRowSelect, dataProvider: *, initGrid: compoundsResultGrid.initGrid, event: compoundsResultGrid.event, gridView: *}}
 */
var compoundsResultGrid = {
    gridView : new RealGridJS.GridView("compoundsResultGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        compoundsResultGrid.initGrid();
        compoundsResultGrid.initDataProvider();
        compoundsResultGrid.event();
    },
    txtFieldName : function() {
        return "txtCompoundsResult";
    },
    compoundType : function() {
        return "result";
    },
    initGrid : function () {
        compoundsResultGrid.gridView.setDataSource(compoundsResultGrid.dataProvider);

        compoundsResultGrid.gridView.setStyles(dictionaryCompoundsResultBaseInfo.realgrid.styles);
        compoundsResultGrid.gridView.setDisplayOptions(dictionaryCompoundsResultBaseInfo.realgrid.displayOptions);
        compoundsResultGrid.gridView.setColumns(dictionaryCompoundsResultBaseInfo.realgrid.columns);
        compoundsResultGrid.gridView.setOptions(dictionaryCompoundsResultBaseInfo.realgrid.options);
        compoundsResultGrid.gridView.setCheckBar({ visible: false });
        compoundsResultGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        compoundsResultGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///순서 조정 메뉴 팝업
        compoundsJs.setUpDownButton(compoundsResultGrid);

        ///삭제 버튼
        compoundsResultGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            compoundsResultGrid.dataProvider.removeRow(itemIndex);
            compoundsJs.makeCompoundsTxt(compoundsResultGrid);
            compoundsJs.makeUpDownOrder(compoundsResultGrid);
        };

        ///행 편집 시도
        compoundsResultGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        compoundsResultGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        compoundsResultGrid.dataProvider.setFields(dictionaryCompoundsResultBaseInfo.dataProvider.fields);
        compoundsResultGrid.dataProvider.setOptions(dictionaryCompoundsResultBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        compoundsResultGrid.dataProvider.clearRows();
        compoundsResultGrid.dataProvider.setRows(dataList);
    }
};

/***
 * 복합명사 : 사전용
 * @type {{init: compoundsDictionaryGrid.init, setData: compoundsDictionaryGrid.setData, initDataProvider: compoundsDictionaryGrid.initDataProvider, gridRowSelect: compoundsDictionaryGrid.gridRowSelect, dataProvider: *, initGrid: compoundsDictionaryGrid.initGrid, event: compoundsDictionaryGrid.event, gridView: *}}
 */
var compoundsDictionaryGrid = {
    gridView : new RealGridJS.GridView("compoundsDictionaryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        compoundsDictionaryGrid.initGrid();
        compoundsDictionaryGrid.initDataProvider();
        compoundsDictionaryGrid.event();
    },
    txtFieldName : function() {
        return "txtCompoundsDictionary";
    },
    compoundType : function() {
        return "dic";
    },
    initGrid : function () {
        compoundsDictionaryGrid.gridView.setDataSource(compoundsDictionaryGrid.dataProvider);

        compoundsDictionaryGrid.gridView.setStyles(dictionaryCompoundsDictionaryBaseInfo.realgrid.styles);
        compoundsDictionaryGrid.gridView.setDisplayOptions(dictionaryCompoundsDictionaryBaseInfo.realgrid.displayOptions);
        compoundsDictionaryGrid.gridView.setColumns(dictionaryCompoundsDictionaryBaseInfo.realgrid.columns);
        compoundsDictionaryGrid.gridView.setOptions(dictionaryCompoundsDictionaryBaseInfo.realgrid.options);
        compoundsDictionaryGrid.gridView.setCheckBar({ visible: false });
        compoundsDictionaryGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        compoundsDictionaryGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///순서 조정 메뉴 팝업
        compoundsJs.setUpDownButton(compoundsDictionaryGrid);

        ///삭제 버튼
        compoundsDictionaryGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            compoundsDictionaryGrid.dataProvider.removeRow(itemIndex);
            compoundsJs.makeCompoundsTxt(compoundsDictionaryGrid);
            compoundsJs.makeUpDownOrder(compoundsDictionaryGrid);
        };

        ///행 편집 시도
        compoundsDictionaryGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        compoundsDictionaryGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        compoundsDictionaryGrid.dataProvider.setFields(dictionaryCompoundsDictionaryBaseInfo.dataProvider.fields);
        compoundsDictionaryGrid.dataProvider.setOptions(dictionaryCompoundsDictionaryBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        compoundsDictionaryGrid.dataProvider.clearRows();
        compoundsDictionaryGrid.dataProvider.setRows(dataList);
    }
};

/***
 * 복합명사 : 공통스크립트
 * @type {{init: compoundsJs.init, search: compoundsJs.search, event: compoundsJs.event}}
 */
var compoundsJs = {
    init : function() {
        compoundsSearchGrid.init();
        compoundsResultGrid.init();
        compoundsDictionaryGrid.init();
        compoundsJs.onLoad();
    },
    onLoad : function() {
        CommonAjax.basic({url:'/search/dictionary/'+$('#requestWordId').val()+'/getCompoundsInfo.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                if(res.resultList != null)
                    compoundsResultGrid.setData(res.resultList);
                if(res.dictionaryList != null)
                    compoundsDictionaryGrid.setData(res.dictionaryList);

                compoundsJs.makeCompoundsTxt(compoundsResultGrid);
                compoundsJs.makeCompoundsTxt(compoundsDictionaryGrid);
            }});
    },
    event : function() {
        $('#searchBtn').click(function(){
            compoundsJs.search();
        });
        $('#writeBtn').click(function(){
            compoundsJs.windowOpen('/search/dictionary/wordWrite/' + $('#searchKeyword').val() + '.json?popup=1',972,1171, 'wordWrite');
        });
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    /***
     * 복합어 순서 조정 버튼 생성
     * @param g
     */
    setUpDownButton : function(g) {
        ///순서 조정 메뉴 팝업
        g.gridView.addPopupMenu("upDown", upDown);
        g.gridView.onMenuItemClicked = function (grid, data, index) {
            var flag = data.tag;
            if(flag == 1) { // 1 = up
                flag = index.itemIndex - ((index.itemIndex == 0) ? 0 : 1);
            } else if(flag == 0) {
                flag = index.itemIndex + 1;
            }
            g.dataProvider.moveRow(index.itemIndex,flag);
            compoundsJs.makeUpDownOrder(g);
            compoundsJs.makeCompoundsTxt(g);
        };
    },
    /***
     * 복합어 순서 생성
     * @param g
     */
    makeUpDownOrder : function(g) {
        for(var i = 0; i < g.dataProvider.getRowCount(); i++)
        {
            g.dataProvider.setValue(i, "compOrder", i + 1);
        }
    },
    /***
     * 복합어 검색용/사전용 이동 처리
     * @param compoundType
     * @returns {boolean}
     */
    move : function(grid) {
        if(compoundsSearchGrid.dataProvider.getRowCount() <= 0) {
            return false;
        }

/*        for(var i = 0; i < grid.dataProvider.getRowCount(); i++)
        {
            if(grid.dataProvider.getJsonRow(i).compId == $('#wordId').val())
                return false;
        }*/
        var values = [$('#wordId').val(), $("#wordName").val(),  $('#pos').val(), $('#dicType').val(), $('#description').val(),  grid.dataProvider.getRowCount() + 1, ''];
        grid.dataProvider.addRow(values);
        compoundsJs.makeCompoundsTxt(grid);
    },
    /***
     * 복합명사 구성 텍스트 노출
     * @param grid
     * @param txtField
     */
    makeCompoundsTxt : function(grid) {
        var txt = "";
        for(var i = 0; i < grid.dataProvider.getRowCount(); i++)
        {
            txt += (grid.dataProvider.getJsonRow(i).compName + ((i == grid.dataProvider.getRowCount() -1 ) ? "" : " + "));
        }
        $('#' + grid.txtFieldName()).html(txt);
    },
    search : function() {
        $('#searchBtn').show();
        $('#writeBtn').hide();

        var searchKeyword = $('#searchKeyword').val().trim();
        if(searchKeyword == "") {
            return false;
        }

        var data = {};
        data.wordName = searchKeyword;
        CommonAjax.basic({url:'/search/dictionary/getSrchWordInfo.json', data: JSON.stringify(data),                     type: 'post',
            contentType: 'application/json',
            method:"POST", successMsg:null, callbackFunc:function(res) {
                compoundsSearchGrid.setData(res);
                if(compoundsSearchGrid.dataProvider.getRowCount() > 0) {
                    $('#wordId').val(compoundsSearchGrid.dataProvider.getJsonRow(0).wordId);
                    $('#wordName').val(compoundsSearchGrid.dataProvider.getJsonRow(0).wordName);
                    $('#dicType').val(compoundsSearchGrid.dataProvider.getJsonRow(0).dicType);
                    $('#pos').val(compoundsSearchGrid.dataProvider.getJsonRow(0).pos);
                    $('#description').val(compoundsSearchGrid.dataProvider.getJsonRow(0).description);
                }
                else {
                    $('#searchBtn').hide();
                    $('#writeBtn').show();
                }
            }});
    },
    /***
     * 복합어 저장
     */
    save : function (grid) {
        var endpoint = grid.compoundType() == "result" ? "insertCompoundsResult.json" : "insertCompounds.json";

        var compounds = {};
        compounds.list = new Array();
        compounds.list = grid.dataProvider.getJsonRows();

        CommonAjax.basic({ url:'/search/dictionary/'+$('#requestWordId').val()+'/' + endpoint,
            data: JSON.stringify(compounds),
            type: 'post',
            method : 'POST',
            contentType: 'application/json',
            successMsg : null,
            callbackFunc : function(res) {
                if(res != 1)
                {
                    alert('저장에 실패하였습니다.');
                }
                else
                {
                    alert('저장에 성공하였습니다.');
                }
            }});
    }
};

function reSearch() {
    compoundsJs.search();
}