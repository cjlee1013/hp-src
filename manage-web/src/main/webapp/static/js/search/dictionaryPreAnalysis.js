/***
 * 기분석명사 구성원 조회
 * @type {{init: preAnalysisSearchGrid.init, setData: preAnalysisSearchGrid.setData, initDataProvider: preAnalysisSearchGrid.initDataProvider, gridRowSelect: preAnalysisSearchGrid.gridRowSelect, dataProvider: *, initGrid: preAnalysisSearchGrid.initGrid, event: preAnalysisSearchGrid.event, gridView: *}}
 */
var preAnalysisSearchGrid = {
    gridView : new RealGridJS.GridView("preAnalysisSearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        preAnalysisSearchGrid.initGrid();
        preAnalysisSearchGrid.initDataProvider();
        preAnalysisSearchGrid.event();
    },
    initGrid : function () {
        preAnalysisSearchGrid.gridView.setDataSource(preAnalysisSearchGrid.dataProvider);

        preAnalysisSearchGrid.gridView.setStyles(dictionaryPreAnalysisSearchBaseInfo.realgrid.styles);
        preAnalysisSearchGrid.gridView.setDisplayOptions(dictionaryPreAnalysisSearchBaseInfo.realgrid.displayOptions);
        preAnalysisSearchGrid.gridView.setColumns(dictionaryPreAnalysisSearchBaseInfo.realgrid.columns);
        preAnalysisSearchGrid.gridView.setOptions(dictionaryPreAnalysisSearchBaseInfo.realgrid.options);
        preAnalysisSearchGrid.gridView.setCheckBar({ visible: false });
        preAnalysisSearchGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        preAnalysisSearchGrid.dataProvider.setFields(dictionaryPreAnalysisSearchBaseInfo.dataProvider.fields);
        preAnalysisSearchGrid.dataProvider.setOptions(dictionaryPreAnalysisSearchBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        preAnalysisSearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            preAnalysisSearchGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        preAnalysisSearchGrid.dataProvider.clearRows();
        preAnalysisSearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = preAnalysisSearchGrid.dataProvider.getJsonRow(selectRowId);
        $('#wordId').val(rowDataJson.wordId);
        $("#wordName").val(rowDataJson.wordName);
        $('#dicType').val(rowDataJson.dicType);
        $('#pos').val(rowDataJson.pos);
        $('#description').val(rowDataJson.description);
    }
};

var upDown = [{
    label: "up",
    enabled: true,
    tag : 1
}, {
    label: "down",
    enabled: true,
    tag : 0
}];

var doAnalysis = [{
    label: "Y",
    enabled: true,
    tag : "Y"
}, {
    label: "N",
    enabled: true,
    tag : "N"
}];

var remove = [{
    label : "del",
    enabled : true,
    tag : "del"
}];

/***
 * 기분석명사 : 검색용
 * @type {{init: preAnalysisResultGrid.init, setData: preAnalysisResultGrid.setData, initDataProvider: preAnalysisResultGrid.initDataProvider, gridRowSelect: preAnalysisResultGrid.gridRowSelect, dataProvider: *, initGrid: preAnalysisResultGrid.initGrid, event: preAnalysisResultGrid.event, gridView: *}}
 */
var preAnalysisResultGrid = {
    gridView : new RealGridJS.GridView("preAnalysisResultGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        preAnalysisResultGrid.initGrid();
        preAnalysisResultGrid.initDataProvider();
        preAnalysisResultGrid.event();
    },
    txtFieldName : function() {
        return "txtPreAnalysisResult";
    },
    preAnalysisType : function() {
        return "result";
    },
    initGrid : function () {
        preAnalysisResultGrid.gridView.setDataSource(preAnalysisResultGrid.dataProvider);

        preAnalysisResultGrid.gridView.setStyles(dictionaryPreAnalysisResultBaseInfo.realgrid.styles);
        preAnalysisResultGrid.gridView.setDisplayOptions(dictionaryPreAnalysisResultBaseInfo.realgrid.displayOptions);
        preAnalysisResultGrid.gridView.setColumns(dictionaryPreAnalysisResultBaseInfo.realgrid.columns);
        preAnalysisResultGrid.gridView.setOptions(dictionaryPreAnalysisResultBaseInfo.realgrid.options);
        preAnalysisResultGrid.gridView.setCheckBar({ visible: false });
        preAnalysisResultGrid.gridView.setSelectOptions({
            style: 'rows'
        });
        preAnalysisResultGrid.gridView.setEditOptions({
            editable : true,
            updatable : true
        });

        ///순서 조정 메뉴 팝업
        preAnalysisJs.setUpDownButton(preAnalysisResultGrid);

        ///기분석 분해 여부 메뉴 팝업
        preAnalysisJs.setDoAnalysisButton(preAnalysisResultGrid);

        ///삭제 버튼
        preAnalysisResultGrid.gridView.onCellButtonClicked = function (grid, itemIndex, column) {
            preAnalysisResultGrid.dataProvider.removeRow(itemIndex);
            preAnalysisJs.makePreAnalysisTxt(preAnalysisResultGrid);
            preAnalysisJs.makeUpDownOrder(preAnalysisResultGrid);
        };

        ///행 편집 시도
        preAnalysisResultGrid.dataProvider.onRowUpdating = function (provider, row) {
            return true;
        };

        ///행 편집 완료
        preAnalysisResultGrid.dataProvider.onRowUpdated = function (provider, row) {
            var r = provider.getJsonRow(row);
        }
    },
    initDataProvider : function() {
        preAnalysisResultGrid.dataProvider.setFields(dictionaryPreAnalysisResultBaseInfo.dataProvider.fields);
        preAnalysisResultGrid.dataProvider.setOptions(dictionaryPreAnalysisResultBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        preAnalysisResultGrid.dataProvider.clearRows();
        preAnalysisResultGrid.dataProvider.setRows(dataList);
    }
};

/***
 * 기분석명사 : 공통스크립트
 * @type {{init: preAnalysisJs.init, search: preAnalysisJs.search, event: preAnalysisJs.event}}
 */
var preAnalysisJs = {
    init : function() {
        preAnalysisSearchGrid.init();
        preAnalysisResultGrid.init();
        preAnalysisJs.onLoad();
    },
    onLoad : function() {
        CommonAjax.basic({url:'/search/dictionary/'+$('#requestWordId').val()+'/getPreAnalysisInfo.json', data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                if(res.list != null && res.list.resultList != null)
                {
                    preAnalysisResultGrid.setData(res.list.resultList);
                }
                preAnalysisJs.makePreAnalysisTxt(preAnalysisResultGrid);

                if(res.attr != null) {
                    $("input[name=preType]").each(function(){
                        if($(this).val() == res.attr.preType) {
                            $(this).attr("checked",true);
                        }else{
                            $(this).attr("checked",false);
                        }
                    });
                    $("input[name=sufType]").each(function(){
                        if($(this).val() == res.attr.sufType) {
                            $(this).attr("checked",true);
                        }else{
                            $(this).attr("checked",false);
                        }
                    });
                }
            }});
    },
    event : function() {
        $('#searchBtn').click(function(){
            preAnalysisJs.search();
        });
        $('#writeBtn').click(function(){
            preAnalysisJs.windowOpen('/search/dictionary/wordWrite/' + $('#searchKeyword').val() + '.json?popup=1',972,1171, 'wordWrite');
        });
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    /***
     * 기분석 순서 조정 버튼 생성
     * @param g
     */
    setUpDownButton : function(g) {
        ///순서 조정 메뉴 팝업
        g.gridView.addPopupMenu("upDown", upDown);
    },
    /***
     * doAnalysisYn 조정 버튼 생성
     * @param g
     */
    setDoAnalysisButton : function(g) {
        ///분해 여부 팝업
        g.gridView.addPopupMenu("doAnalysis", doAnalysis);
        g.gridView.onMenuItemClicked = function (grid, data, index) {
            if(index.fieldName == "compOrder") {
                var flag = data.tag;
                if(flag == 1) { // 1 = up
                    flag = index.itemIndex - ((index.itemIndex == 0) ? 0 : 1);
                } else if(flag == 0) {
                    flag = index.itemIndex + 1;
                }
                g.dataProvider.moveRow(index.itemIndex,flag);
                preAnalysisJs.makeUpDownOrder(g);
                preAnalysisJs.makePreAnalysisTxt(g);
            }
            else {
                var dataRow = g.gridView.getCurrent().dataRow;
                var fieldIndex = g.gridView.getCurrent().fieldIndex;
                var value = data.tag;

                g.dataProvider.setValue(dataRow,fieldIndex,value);
                g.dataProvider.setValue(dataRow,fieldIndex-1, (value == 'Y' ? true : false));
            }
        }
    },
    /***
     * 기분석 순서 생성
     * @param g
     */
    makeUpDownOrder : function(g) {
        for(var i = 0; i < g.dataProvider.getRowCount(); i++)
        {
            g.dataProvider.setValue(i, "compOrder", i + 1);
        }
    },
    /***
     * 기분석 검색용/사전용 이동 처리
     * @param preAnalysisType
     * @returns {boolean}
     */
    move : function(grid) {
        if(preAnalysisSearchGrid.dataProvider.getRowCount() <= 0) {
            return false;
        }

        /*for(var i = 0; i < grid.dataProvider.getRowCount(); i++)
        {
            if(grid.dataProvider.getJsonRow(i).compId == $('#wordId').val())
                return false;
        }*/
        var values = [$('#wordId').val(), $("#wordName").val(), $('#pos').val(), $('#dicType').val(), $('#description').val(), false, 'N',grid.dataProvider.getRowCount() + 1, ''];
        grid.dataProvider.addRow(values);
        preAnalysisJs.makePreAnalysisTxt(grid);
    },
    /***
     * 기분석명사 구성 텍스트 노출
     * @param grid
     * @param txtField
     */
    makePreAnalysisTxt : function(grid) {
        var txt = "";
        for(var i = 0; i < grid.dataProvider.getRowCount(); i++)
        {
            txt += (grid.dataProvider.getJsonRow(i).compName + ((i == grid.dataProvider.getRowCount() -1 ) ? "" : " + "));
        }
        $('#' + grid.txtFieldName()).html(txt);
    },
    search : function() {

        $('#searchBtn').show();
        $('#writeBtn').hide();

        var searchKeyword = $('#searchKeyword').val().trim();
        if(searchKeyword == "") {
            return false;
        }

        var data = {};
        data.wordName = searchKeyword;
        CommonAjax.basic({url:'/search/dictionary/getSrchWordInfo.json', data:JSON.stringify(data),                     type: 'post',
            contentType: 'application/json',
            method:"POST", successMsg:null, callbackFunc:function(res) {
                preAnalysisSearchGrid.setData(res);
                if(preAnalysisSearchGrid.dataProvider.getRowCount() > 0) {
                    $('#wordId').val(preAnalysisSearchGrid.dataProvider.getJsonRow(0).wordId);
                    $('#wordName').val(preAnalysisSearchGrid.dataProvider.getJsonRow(0).wordName);
                    $('#dicType').val(preAnalysisSearchGrid.dataProvider.getJsonRow(0).dicType);
                    $('#pos').val(preAnalysisSearchGrid.dataProvider.getJsonRow(0).pos);
                    $('#description').val(preAnalysisSearchGrid.dataProvider.getJsonRow(0).description);
                }
                else{
                    $('#searchBtn').hide();
                    $('#writeBtn').show();
                }
            }});
    },
    /***
     * 기분석 저장
     */
    save : function (grid) {
        var endpoint = grid.preAnalysisType() == "result" ? "insertPreAnalysisResult.json" : "insertPreAnalysis.json";

        var preAnalysis = {};
        preAnalysis.list = new Array();
        preAnalysis.list = grid.dataProvider.getJsonRows();
        preAnalysis.attr = {
            preType : $('input:radio[name=preType]:checked').val(),
            sufType : $('input:radio[name=sufType]:checked').val(),
            subType : 'UNUSED'
        };

        CommonAjax.basic({ url:'/search/dictionary/'+$('#requestWordId').val()+'/' + endpoint,
            data: JSON.stringify(preAnalysis),
            type: 'post',
            method : 'POST',
            contentType: 'application/json',
            successMsg : null,
            callbackFunc : function(res) {
                if(res != 1)
                {
                    alert('저장에 실패하였습니다.');
                }
                else
                {
                    alert('저장에 성공하였습니다.');
                }
            }});
    }
};

function reSearch() {
    preAnalysisJs.search();
}