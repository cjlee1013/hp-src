function copy_to_clipboard(str) {
    $('#clip_target').val(str);
    $('#clip_target').select();
    try {
        var successful = document.execCommand('copy');
        if(successful) {
            alert('복사되었습니다.');
        }
    } catch (err) {
        alert('이 브라우저는 지원하지 않습니다.')
    }
}

var expCategorySearchJs = {
    init : function() {
        this.initCategorySelectBox();
        this.bindingEvent();
    },
    limitPageSize: 100,
    categoryId: '',
    categoryName: '',
    categoryDepth: '',
    isFirstSearch: false,
    drawCategorySelect: function(depth, prefix) {
        /* filter 초기화 */
        expCategorySearchJs.filterReset();
        expCategorySearchJs.changeCategorySelectBox(depth, prefix);
        expCategorySearchJs.categoryDepth = depth;
        expCategorySearchJs.categoryId = $('#'+prefix+depth).val();
        $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);

        let category = '#' + prefix + depth + ' option:checked';
        expCategorySearchJs.categoryDepth = depth;
        if ($(category).val() == '' && (depth - 1) != 0) {
            expCategorySearchJs.categoryDepth = (depth - 1);
            category = '#' + prefix + (depth - 1) + ' option:checked';
        }
        expCategorySearchJs.categoryId = $(category).val();
        expCategorySearchJs.categoryName = $(category).text();

        /* 검색결과 초기화 */
        // expCategorySearchGrid.clear();
        /* 초기 검색 여부 초기화 */
        expCategorySearchJs.isFirstSearch = false;
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        expCategorySearchJs.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        expCategorySearchJs.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
    },
    selectedIndex : function(jsonRow) {
        $("span[name=indexDocId]").html(jsonRow.docId);             //문서번호
        $("span[name=indexWeight]").html(jsonRow.weight);           //가중치
        $("span[name=indexWeightCalc]").html(jsonRow.weightCalc);   //가중치 수식
        $("span[name=indexWeightDay]").html(jsonRow.weightDay); // 24시간 구매자수

        $("span[name=indexItemNo]").html(jsonRow.itemNo); // 상품번호
        $("span[name=indexSearchItemNm]").html(jsonRow.searchItemNm); // 상품명 원형
        $("span[name=indexBrandNmKor]").html(jsonRow.brandNmKor); // 브랜드 한글명
        $("span[name=indexBrandNmEng]").html(jsonRow.brandNmEng); // 브랜드 한글명

        let separator = ' > ';
        let category =  filterUtilJs.concat(jsonRow.lcateNm, separator, jsonRow.mcateNm);
        $("span[name=indexCategory]").html(category); // 카테고리
    },
    matchBoxReset : function(){
        $("span[name=indexDocId]").html('');
        $("span[name=indexWeight]").html('');
        $("span[name=indexWeightCalc]").html('');
        $("span[name=indexWeightDay]").html('');
        $("span[name=indexItemNo]").html('');
        $("span[name=indexSearchItemNm]").html('');
        $("span[name=indexBrandNmKor]").html('');
        $("span[name=indexBrandNmEng]").html('');
        $("span[name=indexCategory]").html('');
    },
    reset : function() {
        /* 카테고리 초기화 */
        currentSearchJs.categoryDepth = '';
        currentSearchJs.categoryId = '';
        $("#searchCateCd1 option:eq(0)").prop("selected", true); //첫번째 option 선택
        $('#searchCateCd2').children('option:not(:first)').remove();

        $("#storeIds").val("515");
        $("input:radio[name=sort]:input[value='RANK']").prop("checked",true);
        expCategorySearchJs.filterReset();
        expCategorySearchJs.isFirstSearch = false;
        $("input:radio[name=storeSearchType]:input[value='DIRECT']").prop("checked",true);
        $("#storeIds").val("515");
        $("#storeIds").attr("readonly",false);
        $("#schStoreNm").hide();
        $("#schStoreBtn").hide();
        $("#directText").show();
    },
    filterReset: function(){
        /* 공통 필터 초기화 */
        filterJs.filterReset();
    },
    searchDataSet: function() {
        const regexp = /^[(\d{1,10}),]*$/;
        /* init parameter - Essential*/
        let storeIds;
        let categoryId;
        let categoryDepth;

        /* set parameter */
        storeIds = $("input[name=storeIds]").val();
        categoryId = expCategorySearchJs.categoryId;
        categoryDepth = expCategorySearchJs.categoryDepth;

        /* parameter validate */
        if (storeIds == null || storeIds == ''){
            alert('점포를 입력해주세요.');
            return false;
        }
        if (storeIds != null && $.trim(storeIds) !== '' && !regexp.test(storeIds)) {
            alert('점포ID 형식이 올바르지 않습니다.');
            return false;
        }
        if(filterUtilJs.isEmpty(categoryId)) {
            alert('카테고리를 먼저 선택해주세요.');
            return false;
        }

        /* data return */
        return {
            storeIds: storeIds,
            categoryId: categoryId,
            categoryDepth: categoryDepth
        };
    },
    /*
        필터 UI view
     */
    filter: function () {
        let dataSet = expCategorySearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            dataSet.explain = 'N';
        }
        CommonAjax.basic(
            {
                url: '/search/dataManagement/expCategory/filter.json',
                data: dataSet,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    // view
                    let data = res.data;
                    if(filterUtilJs.isNotEmpty(data.brandList)){
                        $('#brandList').html(filterJs.drawCheckbox(data.brandList, 'brand'));
                        //$("#brandList").attr('colspan','1');
                    }
                }
            }
        );
    },
    search : function (offset, limit, callback) {
        /* filter parameter validate & merge */
        let dataSet = expCategorySearchJs.searchDataSet();
        if(Object.keys(dataSet).length === 0){
            return false;
        }else{
            Object.assign(dataSet, filterJs.filterDataSet(), {
                page:  offset,
                perPage: limit,
                explain: 'Y'
            });
        }
        CommonAjax.basic(
            {
                url: '/search/dataManagement/expCategory/list.json',
                data: dataSet,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {

                    /* 최초검색 여부 설정 */
                    if(!expCategorySearchJs.isFirstSearch){
                        expCategorySearchJs.isFirstSearch = true;
                    }

                    //쿼리 저장
                    $("#hidden_query").val(JSON.stringify(res.data.query));

                    /***
                     * 검색 결과 수
                     */
                    $("span[name=totalCount]").html(filterUtilJs.getCommaCalc(res.data.totalCount));

                    if(res.data.dataList != null && res.data.dataList.length > 0) {
                        expCategorySearchGrid.page += 1;
                    }

                    if(offset > 1) {
                        expCategorySearchGrid.dataProvider.fillJsonData(res.data.dataList,{fillMode: "append"});
                    }
                    else
                    {
                        expCategorySearchGrid.setData(res.data.dataList);
                        expCategorySearchGrid.page = 1;
                        if(expCategorySearchGrid.dataProvider.getRowCount() > 0) {
                            expCategorySearchJs.selectedIndex(expCategorySearchGrid.dataProvider.getJsonRow(0));
                        } else {
                            expCategorySearchJs.matchBoxReset(); //검색 결과가 없을 시 매치정보 초기화
                        }
                    }

                    if(callback !== "undefined" && callback != null) {
                        callback(res);
                    }
                }
            }
        );
        return true;
    },
    bindingEvent : function() {
        /* 검색버튼 */
        $('#searchBtn').click(function() {
            expCategorySearchJs.filterReset();
            if(expCategorySearchJs.search(1,expCategorySearchJs.limitPageSize,null)){
                expCategorySearchJs.filter();
            }
        });
        /* 초기화 버튼 */
        $('#resetBtn').click(function() {
            expCategorySearchJs.reset();
        });

        /* 필터 이벤트 바인딩 */
        filterJs.bindEvent();

        $("#copyQuery").click(function() {
            if($("#hidden_query").val() == null || $("#hidden_query").val() == '') {
                alert('검색시도 후 복사가 가능합니다.');
                return false;
            }
            copy_to_clipboard($("#hidden_query").val());
        });
        $("#excelDownloadBtn").click(function() {
            expCategorySearchJs.excelDownload();
        });
        $("input[name=storeSearchType]").click(function() {
            if($(this).val() == "DIRECT") {
                $("#storeIds").val("515");
                $("#storeIds").attr("readonly",false);
                $("#schStoreNm").hide();
                $("#schStoreBtn").hide();
                $("#directText").show();
            }else {
                $("#storeIds").val("");
                $("#schStoreNm").val("");
                $("#storeIds").attr("readonly",true);
                $("#directText").hide();
                $("#schStoreNm").show();
                $("#schStoreBtn").show();
            }
        });
    },
    excelDownload: function() {
        expCategorySearchJs.search(1,1000,function() {
            let _date = new Date();

            let categoryId = expCategorySearchJs.categoryId;
            let categoryName = expCategorySearchJs.categoryName;
            let fileName = "카테고리_"+$("#profilesActive").val()+"_"+categoryName + "("+categoryId+")_EXP_" + $("input[name=storeIds]").val().replace(/,/gi,"_") + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate() + "_" + _date.getHours() + "" + _date.getMinutes();
            expCategorySearchGrid.gridView.exportGrid({
                type: "excel",
                indicator: "visible",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                target: "local",
                fileName: fileName+".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다.",
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                showColumns: [
                    "eventItemYn",
                    "pickYn",
                    "togetherYn",
                    "giftYn",
                    "regDt"
                ],
                hideColumns: [
                    "img",
                    "lcateNm",
                    "mcateNm",
                ]
            });
        });
    },
    /**
     * 카테고리 셀렉트박스 생성
     *
     * @param depth
     * @param parentCateCd
     * @param cateCd
     * @param selector
     */
    setCategorySelectBox: function (depth, parentCateCd, cateCd, selector) {
        $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
        if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
            CommonAjax.basic({
                url: '/item/expCategory/getExpCategoryByDisplay.json',
                data: {
                    depth: depth,
                    parentCateCd: parentCateCd
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $.each(resData, function (idx, val) {
                        var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                        $(selector).append(
                            '<option value="' + val.cateCd + '" ' + selected + '>'
                            + val.cateNm
                            + '</option>');
                    });
                    $(selector).css('-webkit-padding-end', '30px');
                }
            });
        }
    },
    /**
     * 카테고리 select box change 이벤트
     *
     * @param depth 해당 카테고리 뎁스
     * @param prefix 셀렉트박스 아이디 prefix
     */
    changeCategorySelectBox : function(depth, prefix, dispYnArr) {
        var cateCd = $('#'+prefix+depth).val();
        expCategorySearchJs.setCategorySelectBox(depth+1, cateCd, '', $('#'+prefix+(depth+1)));
        $('#'+prefix+(depth+1)).change();
    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        windowPopupOpen("/common/popup/storePop?callback="+ callBack + "&storeType=EXP"
            , "partnerStatus", "1100", "620", "yes", "no");

    },

    callBack : function(res) {
        $('#storeIds').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    }
};


const expCategorySearchGrid = {
    gridView : new RealGridJS.GridView("expCategorySearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        expCategorySearchGrid.initGrid();
        expCategorySearchGrid.initDataProvider();
        expCategorySearchGrid.event();
    },
    page : 1,
    initGrid : function () {
        expCategorySearchGrid.gridView.setDataSource(expCategorySearchGrid.dataProvider);

        expCategorySearchGrid.gridView.setStyles(expCategorySearchGridBaseInfo.realgrid.styles);
        expCategorySearchGrid.gridView.setDisplayOptions(expCategorySearchGridBaseInfo.realgrid.displayOptions);
        expCategorySearchGrid.gridView.setColumns(expCategorySearchGridBaseInfo.realgrid.columns);
        expCategorySearchGrid.gridView.setOptions(expCategorySearchGridBaseInfo.realgrid.options);
        expCategorySearchGrid.gridView.setCheckBar({ visible: false });
        expCategorySearchGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        expCategorySearchGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            },
            rowHeight: 80
        });
        expCategorySearchGrid.gridView.onScrollToBottom = function(grid) {
            if(expCategorySearchGrid.dataProvider.getRowCount() > 7){ // 한화면에 그려지는 개수
                expCategorySearchJs.search(expCategorySearchGrid.page + 1, expCategorySearchJs.limitPageSize, null);
            }
        };
    },
    initDataProvider : function() {
        expCategorySearchGrid.dataProvider.setFields(expCategorySearchGridBaseInfo.dataProvider.fields);
        expCategorySearchGrid.dataProvider.setOptions(expCategorySearchGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        expCategorySearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            expCategorySearchGrid.gridRowSelect(index.dataRow)
        };
    },
    clear: function(){
        expCategorySearchGrid.dataProvider.clearRows();
    },
    setData : function(dataList) {
        expCategorySearchGrid.clear();
        expCategorySearchGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = expCategorySearchGrid.dataProvider.getJsonRow(selectRowId);
        expCategorySearchJs.selectedIndex(rowDataJson);
    }
};