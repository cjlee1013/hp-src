var unknownMorphologyGrid = {
    gridView : new RealGridJS.GridView("unknownMorphologyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        unknownMorphologyGrid.initGrid();
        unknownMorphologyGrid.initDataProvider();
        unknownMorphologyGrid.event();
    },
    page : 0,
    initGrid : function () {
        unknownMorphologyGrid.gridView.setDataSource(unknownMorphologyGrid.dataProvider);

        unknownMorphologyGrid.gridView.setStyles(unknownMorphologyGridBaseInfo.realgrid.styles);
        unknownMorphologyGrid.gridView.setDisplayOptions(unknownMorphologyGridBaseInfo.realgrid.displayOptions);
        unknownMorphologyGrid.gridView.setColumns(unknownMorphologyGridBaseInfo.realgrid.columns);
        unknownMorphologyGrid.gridView.setOptions(unknownMorphologyGridBaseInfo.realgrid.options);
        unknownMorphologyGrid.gridView.setCheckBar({ visible: false });
        unknownMorphologyGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        unknownMorphologyGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        unknownMorphologyGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > unknownMorphologyGrid.page) {
                unknownMorphologyGrid.page += 100;
                unknownMorphologyJs.search(unknownMorphologyGrid.dataProvider.getRowCount(),unknownMorphologyJs.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        unknownMorphologyGrid.dataProvider.setFields(unknownMorphologyGridBaseInfo.dataProvider.fields);
        unknownMorphologyGrid.dataProvider.setOptions(unknownMorphologyGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        unknownMorphologyGrid.gridView.onDataCellClicked = function(gridView, index) {
            unknownMorphologyGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        unknownMorphologyGrid.dataProvider.clearRows();
        unknownMorphologyGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = unknownMorphologyGrid.dataProvider.getJsonRow(selectRowId);
        unknownMorphologyJs.selectedIndex(rowDataJson);
    },
    excelDownload: function() {

        $("#schKeyword").val('');

        //전체 다운로드를 위해 전체 데이터를 RealGrid에 바인딩 해야 한다.
        unknownMorphologyJs.search(0,100000000, function() {
            var _date = new Date();
            var fileName = "미등록어_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            unknownMorphologyGrid.gridView.exportGrid( {
                type: "excel",
                target: "local",
                fileName: fileName+".xlsx",
                indicator: "default",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        });
    }
};

var unknownMorphologyJs = {
    init : function () {
        this.initSearchDate('-5y');
    },
    limitPageSize : function() {
        return 100;
    },
    onload : function() {
        unknownMorphologyJs.search(0,unknownMorphologyJs.limitPageSize());
    },
    selectedIndex : function(rowDataJson) {
        unknownMorphologyJs.validation(rowDataJson);
    },
    initSearchDate : function (flag) {
    },
    event : function() {
        $('#searchBtn').click(function(){
            unknownMorphologyJs.search(0,unknownMorphologyJs.limitPageSize());
        });
        $("#excelDownloadBtn").click(function() {
            unknownMorphologyGrid.excelDownload();
        });
    },
    reset : function() {
    },
    delete : function() {
    },
    ///대체검색어 등록
    write : function() {
    },
    search : function (offset, limit, callback) {

        var data = {};

        /// 시작일
        data.schKeyword = $('#schKeyword').val();

        data.offset = offset;
        data.limit = limit;

        CommonAjax.basic(
            {
                url:'/search/statistics/unkownMorphology/search.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                callbackFunc:function(res)
                {
                    if(offset > 0) {
                        unknownMorphologyGrid.dataProvider.fillJsonData(res.data,{fillMode: "append"});
                    }
                    else
                    {
                        unknownMorphologyGrid.setData(res.data);
                        $('#totalCount').html($.jUtil.comma(res.pagination.totalCount));
                        unknownMorphologyGrid.page = 0;
                    }

                    if(callback != "undefined" && callback != null) {
                        callback();
                    }
                }
            });

    },
    validation : function (rowDataJson) {
        var data = {};
        data.wordName = rowDataJson.keyword;
        CommonAjax.basic(
            {
                url:'/search/dictionary/getSrchWordInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(res.length < 1){
                        unknownMorphologyJs.windowOpen('/search/dictionary/wordWrite/' + rowDataJson.keyword + '.json?popup=1',972,1171, 'wordWrite');
                    } else {
                        alert("이미 사전에 등록된 어휘 입니다.");
                    }
                }
            });
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    }
};

function reSearch() {

}