



/***
 * 어휘 조회 리스트 그리드 설정
 * @type {{init: dictionaryListGrid.init, setData: dictionaryListGrid.setData, initDataProvider: dictionaryListGrid.initDataProvider, excelDownload: dictionaryListGrid.excelDownload, dataProvider: *, initGrid: dictionaryListGrid.initGrid, event: dictionaryListGrid.event, gridView: *}}
 */
var dictionaryListGrid = {
    gridView : new RealGridJS.GridView("dictionaryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dictionaryListGrid.initGrid();
        dictionaryListGrid.initDataProvider();
        dictionaryListGrid.event();
    },
    initGrid : function () {
        dictionaryListGrid.gridView.setDataSource(dictionaryListGrid.dataProvider);

        dictionaryListGrid.gridView.setStyles(dictionaryListGridBaseInfo.realgrid.styles);
        dictionaryListGrid.gridView.setDisplayOptions(dictionaryListGridBaseInfo.realgrid.displayOptions);
        dictionaryListGrid.gridView.setColumns(dictionaryListGridBaseInfo.realgrid.columns);
        dictionaryListGrid.gridView.setOptions(dictionaryListGridBaseInfo.realgrid.options);
        dictionaryListGrid.gridView.setCheckBar({ visible: false });

    },
    initDataProvider : function() {
        dictionaryListGrid.dataProvider.setFields(dictionaryListGridBaseInfo.dataProvider.fields);
        dictionaryListGrid.dataProvider.setOptions(dictionaryListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dictionaryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dictionaryListGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        dictionaryListGrid.dataProvider.clearRows();
        dictionaryListGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        if(dictionaryListGrid.gridView.getItemCount()  > 0) {
            var rowDataJson = dictionaryListGrid.dataProvider.getJsonRow(selectRowId);
            $('#wordId').val(rowDataJson.wordId);
            $("#wordName").val(rowDataJson.wordName);
            $("#synonym").val(rowDataJson.synonym);
            $("#hyponym").val(rowDataJson.hyponym);
            $("#relatednym").val(rowDataJson.relatednym);

            $("#textCountKr_synonym").html($("#synonym").val().length);
            $("#textCountKr_hyponym").html($("#hyponym").val().length);
            $("#textCountKr_relatednym").html($("#relatednym").val().length);
        }
    },
    excelDownload: function() {
        dictionaryList.search(function() {
            var _date = new Date();
            var fileName = "사전_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
            dictionaryListGrid.gridView.exportGrid( {
                type: "excel",
                target: "local",
                fileName: fileName+".xlsx",
                indicator: "default",
                header: "default",
                footer: "default",
                allItems: true,
                compatibility: true,
                pagingAllItems: true,
                applyFitStyle: true,
                showProgress: true,
                progressMessage: "엑셀 데이터 추줄 중입니다. 잠시만 기다려주세요.",
                hideColumns: ["senseTag","dicType","pos","note","useFlagMenu","stopFlagMenu"],
                showColumns: ["synonym","hyponym","relatednym"]
            });
        });
    }
};

var dictionaryList = {
    init : function () {
        this.initSearchDate(null);
    },
    initSearchDate : function (flag) {
        dictionaryList.setDate = Calendar.datePickerRange('sdate', 'edate');
    },
    bindingEvent : function() {
        $('#searchBtn').click(function() {
            dictionaryList.search();
        });
        $('#writeBtn').bindClick(dictionaryList.write);
        $('#modityBtn').bindClick(dictionaryList.modify);
        $('#synonym').calcTextLength('keyup', '#textCountKr_synonym');
        $('#hyponym').calcTextLength('keyup','#textCountKr_hyponym');
        $('#relatednym').calcTextLength('keyup','#textCountKr_relatednym');
        $("#morphExpansionSaveBtn").bindClick(dictionaryList.morphExpansionSaveBtnEvent);
        $("#morphExpansionDeleteBtn").bindClick(dictionaryList.morphExpansionDeleteBtnEvent);
        $("#excelDownloadBtn").bindClick(dictionaryListGrid.excelDownload);
        $("#searchType").change(function() {
            if($(this).val() == 'wordId') {
                dictionaryList.inputAttrDisabled(true);
            } else {
                dictionaryList.inputAttrDisabled(false);
            }
        });
        $("#resetBtn").bindClick(dictionaryList.resetBtn);
    },
    inputAttrDisabled : function(v) {
        $("input[name=wordInclude]").attr("disabled",v);
        $("input[name=useFlag]").attr("disabled",v);
        $("input[name=stopFlag]").attr("disabled",v);
        $("input[name=isGeneral]").attr("disabled",v);
        $("input[name=synonymInclude]").attr("disabled",v);
        $("input[name=hyponymInclude]").attr("disabled",v);
        $("input[name=relatednymInclude]").attr("disabled",v);
        $("select[name=pos]").attr("disabled",v);
        $("input[name=dicType]").attr("disabled",v);
        $("input[name=sdate]").attr("disabled",v);
        $("input[name=edate]").attr("disabled",v);
    },
    hiddenFieldInit : function() {
        $('#wordId').val('');
        $('#wordName').val('');
        $("#searchBtn").show();
        $("#writeBtn").hide();
    },
    ///어휘등록
    write : function() {
        location.href = '/search/dictionary/wordWrite/' + $('#searchKeyword').val() + '.json';
    },
    ///기분석구성
    preAnalysis : function() {
        if($('#wordId').val() == '') {
            alert('기분석을 구성하려는 단어를 먼저 선택해주세요');
            return false;
        }
        var url = '/search/popup/dictionary/preAnalysisInfo/' + $('#wordId').val();
        dictionaryList.windowOpen(url, 1132, 731,'preAnalysis');
    },
    ///연관어 저장
    morphExpansionSaveBtnEvent : function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('저장하려는 어휘를 먼저 선택해주세요.');
            return;
        }
        var synonym = $.trim($("#synonym").val());
        var hyponym = $.trim($("#hyponym").val());
        var relatednym = $.trim($("#relatednym").val());

        var regexp = /^[(a-zA-Z)(가-힣)(0-9)(\s);]*$/;
        if(!regexp.test(synonym) || !regexp.test(hyponym) || !regexp.test(relatednym)) {
            alert("관련어는 한글+숫자+영문의 조합으로만 등록 가능 합니다.\n\n키워드간 구분은 (;) 세미콜론으로 지정해야 합니다.");
            return;
        }

        if(synonym=='' && hyponym=='' && relatednym=='') {
            alert('동의어/하위어/연관어 중 하나의 요소는 필수입니다.');
            return;
        }

        var data = {};
        data.wordId = $("#wordId").val();
        data.synonym = synonym;
        data.hyponym = hyponym;
        data.relatednym = relatednym;

        CommonAjax.basic(
            {
                url: '/search/dictionary/insertMorphExpansion.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    if(res == 1) {
                        alert('저장되었습니다.');
                        dictionaryList.search();
                    }
                }
            });
    },
    ///확장어 삭제
    morphExpansionDeleteBtnEvent : function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('삭제하려는 어휘를 먼저 선택해주세요.');
            return;
        }
        CommonAjax.basic(
            {
                url: '/search/dictionary/deleteMorphExpansion.json?wordId=' + $("#wordId").val(),
                data: null,
                type: 'get',
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    if(res == 1) {
                        alert('삭제되었습니다.');
                        dictionaryList.search();
                    }
                }
            });
    },
    ///복합어구성
    compounds : function() {
        if($('#wordId').val() == '') {
            alert('복합어를 구성하려는 단어를 먼저 선택해주세요');
            return false;
        }
        var url = '/search/popup/dictionary/compoundsInfo/' + $('#wordId').val();
        dictionaryList.windowOpen(url, 1132, 731,'compounds');
    },
    ///어휘수정
    modify : function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('수정하려는 단어를 먼저 선택해주세요.');
            return;
        }
        location.href = '/search/dictionary/wordModify/' + $('#wordId').val() + ".json";
    },
    ///초기화
    resetBtn : function() {
        dictionaryList.hiddenFieldInit();
        $("#searchKeyword").val('');
        $("#pos option:eq(0)").prop("selected",true);
        $("input[name='dicType']:checked").each(
            function () {
                $(this).prop('checked', false);
            }
        );
        $("#schDateType option:eq(0)").prop("selected",true);
        $("#sdate").val('');
        $("#edate").val('');
        $("input[name=wordInclude]").prop("checked",false);
        $("#searchType option:eq(0)").prop("selected",true);
        $("input[name=isGeneral]").prop("checked",false);
        $("input[name=useFlag]").prop("checked",false);
        $("input[name=stopFlag]").prop("checked",false);
        $("input[name=synonymInclude]").prop("checked",true);
        $("input[name=hyponymInclude]").prop("checked",true);
        $("input[name=relatednymInclude]").prop("checked",true);
        dictionaryList.inputAttrDisabled(false);
    },
    search : function (_fn_callback) {

        dictionaryList.hiddenFieldInit();
        var data = {};

        var search_keyword = $('#searchKeyword').val();

        /// 품사
        data.pos = $('#pos').val();
        /// 사전 형식
        var dicTypeList = new Array();
        $("input[name='dicType']:checked").each(function(){
            dicTypeList.push($(this).val());
        });
        data.dicType = dicTypeList;
        ///날짜유형
        if($("#sdate").val() != '' || $("#edate").val() != '') {
            var startDt = moment($("#sdate").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#edate").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#sdate").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
        }
        data.schDateType = $("#schDateType option:selected").val();
        if(($("input[name=sdate]").val() == '' && $("input[name=edate]").val() != '') ||
            ($("input[name=sdate]").val() != '' && $("input[name=edate]").val() == '')){
            alert('검색날짜를 모두 지정해주세요.');
            return false;
        }
        /// 시작일
        if($("input[name=sdate]").val() != '') {
            data.sdate = Date.parse($("input[name=sdate]").val() + " 00:00:00") / 1000;
        }
        /// 종료일
        if($("input[name=edate]").val() != '') {
            data.edate = Date.parse($("input[name=edate]").val() + " 23:59:59") / 1000;
        }

        ///포함된 어휘 모두 찾기
        var wordInclude = $("input[name=wordInclude]:checked").val();
        if(wordInclude == 1) {
            data.wordName = '%' + search_keyword + '%';
        }
        else {
            data.wordName = search_keyword;
        }

        data.searchType = $("#searchType").val();
        if($("#searchType").val() == 'wordId' && (search_keyword == null || search_keyword == '')) {
            alert('번호를 입력해주세요.'); return false;
        }

        data.synonymInclude = ($("input[name=synonymInclude]:checked").val() == undefined) ? "N" : "Y";
        data.hyponymInclude = $("input[name=hyponymInclude]:checked").val() == undefined ? "N" : "Y";
        data.relatednymInclude = $("input[name=relatednymInclude]:checked").val() == undefined ? "N" : "Y";
        data.isGeneral = $("input[name=isGeneral]:checked").val() == undefined ? null : $("input[name=isGeneral]:checked").val();
        data.useFlag = $("input[name=useFlag]:checked").val() == undefined ? null : $("input[name=useFlag]:checked").val();
        data.stopFlag = $("input[name=stopFlag]:checked").val() == undefined ? null : $("input[name=stopFlag]:checked").val();

        CommonAjax.basic(
            {
                url:'/search/dictionary/getSrchWordInfo.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res)
                {
                    dictionaryListGrid.setData(res);
                    $('#dictionaryTotalCount').html($.jUtil.comma(dictionaryListGrid.gridView.getItemCount()));
                    dictionaryListGrid.gridRowSelect(0);
                    if(dictionaryListGrid.dataProvider.getRowCount() > 0) {

                        $('#wordId').val(dictionaryListGrid.dataProvider.getJsonRow(0).wordId);
                        $('#wordName').val(dictionaryListGrid.dataProvider.getJsonRow(0).wordName);
                    }
                    else {
                        if (search_keyword != null && $.trim(search_keyword) != '') {
                            $("#searchBtn").hide();
                            $("#writeBtn").show();
                        }
                    }

                    if(_fn_callback != null && _fn_callback != "undefined") {
                        _fn_callback();
                    }
                }
            });

    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
    history: function () {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('조회하려는 단어를 먼저 선택해주세요.');
            return;
        }
        dictionaryList.windowOpen('/search/popup/dictionary/wordHistory/' + $('#wordId').val(), 1200, 443,'history');
    },
    delete: function() {
        if(dictionaryListGrid.dataProvider.getRowCount() == 0) {
            alert('삭제하려는 단어를 먼저 선택해주세요.');
            return;
        }
        if(confirm("'" + $('#wordName').val() + "' 을(를) 삭제하시겠습니까?")) {
            CommonAjax.basic({url:'/search/dictionary/'+$('#wordId').val()+'/deleteWordInfo.json' , data:null, method:"POST",type:"post", contentType: 'application/json', successMsg:null, callbackFunc:function(res) {
                if(res != '1') {
                    alert('삭제에 실패하였습니다');
                } else {
                    alert('삭제되었습니다.');
                    dictionaryList.search();
                }
            }});
        }

    },
    overviewAnalysis : function(top) {
        $("#overview_analysis").css("margin-top",top);
        $("#overview_analysis").show();
    },
    overviewTokenizedResult : function(obj) {
        var keyword = $(obj).val();
        var searchKeyword = "";
        if(keyword.split(";").length > 1) {
            for(var i = 0; i < keyword.split(";").length; i++) {
                searchKeyword = keyword.split(";")[i];
            }
        } else {
            searchKeyword = keyword;
        }

        if($.trim(searchKeyword) == "") { $("#analysisResult").val(""); return; }
        CommonAjax.basic(
            {
                url:'/search/dictionary/analysis.json?keyword=' + searchKeyword,
                data: null,
                type: 'get',
                contentType: 'application/json',
                method:"GET",
                successMsg:null,
                callbackFunc:function(res)
                {
                    if(res != null) {
                        var token = "";
                        for(var i = 0; i < res.tokens.length; i++) {
                            token += (i + 1) + ". " + res.tokens[i].term + "\n";
                        }
                        $("#analysisResult").val(token);
                    }

                }
            });

        //analysis.json
    }
};