var replacedKeywordGrid = {
    gridView : new RealGridJS.GridView("replacedKeywordGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        replacedKeywordGrid.initGrid();
        replacedKeywordGrid.initDataProvider();
        replacedKeywordGrid.event();
    },
    page : 0,
    initGrid : function () {
        replacedKeywordGrid.gridView.setDataSource(replacedKeywordGrid.dataProvider);

        replacedKeywordGrid.gridView.setStyles(replacedKeywordGridBaseInfo.realgrid.styles);
        replacedKeywordGrid.gridView.setDisplayOptions(replacedKeywordGridBaseInfo.realgrid.displayOptions);
        replacedKeywordGrid.gridView.setColumns(replacedKeywordGridBaseInfo.realgrid.columns);
        replacedKeywordGrid.gridView.setOptions(replacedKeywordGridBaseInfo.realgrid.options);
        replacedKeywordGrid.gridView.setCheckBar({ visible: false });
        replacedKeywordGrid.gridView.setEditOptions({
            appendable: true,
            insertable: true
        });
        var hoverMaskValue = $('#selHoverMask').val();
        replacedKeywordGrid.gridView.setDisplayOptions({
            rowHoverMask:{
                visible:true,
                styles:{
                    background:"#2065686b"
                },
                hoverMask: hoverMaskValue
            }
        });
        /*replacedKeywordGrid.gridView.onScrollToBottom = function() {
            replacedKeywordJs.search(replacedKeywordGrid.dataProvider.getRowCount(),replacedKeywordJs.limitPageSize());
        };*/
        replacedKeywordGrid.gridView.onTopItemIndexChanged = function(grid, item) {
            if (item > replacedKeywordGrid.page) {
                replacedKeywordGrid.page += 50;
                replacedKeywordJs.search(replacedKeywordGrid.dataProvider.getRowCount(),replacedKeywordJs.limitPageSize());
            }
        };
    },
    initDataProvider : function() {
        replacedKeywordGrid.dataProvider.setFields(replacedKeywordGridBaseInfo.dataProvider.fields);
        replacedKeywordGrid.dataProvider.setOptions(replacedKeywordGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        replacedKeywordGrid.gridView.onDataCellClicked = function(gridView, index) {
            replacedKeywordGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        replacedKeywordGrid.dataProvider.clearRows();
        replacedKeywordGrid.dataProvider.setRows(dataList);
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = replacedKeywordGrid.dataProvider.getJsonRow(selectRowId);
        replacedKeywordJs.selectedIndex(rowDataJson);
    },
    excelDownload: function() {

        $("#searchKeyword").val('');
        $("input[name=inner]").prop('checked', false);

        //전체 다운로드를 위해 전체 데이터를 RealGrid에 바인딩 해야 한다.
        replacedKeywordJs.search(0,100000000, function() {
            var _date = new Date();
            var fileName = "대체검색어_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            replacedKeywordGrid.gridView.exportGrid( {
                type: "excel",
                target: "local",
                fileName: fileName+".xlsx",
                indicator: "default",
                header: "default",
                footer: "default",
                applyFitStyle: true,
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        });
    }
};

var replacedKeywordJs = {
    init : function () {
        this.initSearchDate('-30d');
        this.initInputDate();
    },
    limitPageSize : function() {
       return 100;
    },
    selectedIndex : function(jsonRow) {
        $('#replacedId').val(jsonRow.replacedId);

        //입력폼 데이터 생성
        $("#keyword").val(jsonRow.keyword);
        $("#replacedKeyword").val(jsonRow.replacedKeyword);
        $("#useFlag").val(jsonRow.useFlag).prop("selected",true);
        $("#sdate").val(jsonRow.sdate);
        $("#edate").val(jsonRow.edate);
        $("input:radio[name=operationPeriodYn]:input[value="+jsonRow.operationPeriodYn+"]").prop("checked",true);
        if(jsonRow.operationPeriodYn == 'Y'){
            $("#sdate").attr("disabled",false);
            $("#edate").attr("disabled",false);
        }else {
            $("#sdate").attr("disabled",true);
            $("#edate").attr("disabled",true);
        }

        $("#textCountKr_keyword").html(jsonRow.keyword.length);
        $("#textCountKr_replacedKeyword").html(jsonRow.replacedKeyword.length);

        $("#saveBtn").hide();
        $("#editBtn").show();
    },
    initSearchDate : function (flag, element) {
        replacedKeywordJs.setDate = Calendar.datePickerRange('searchSdate', 'searchEdate');
        replacedKeywordJs.setDate.setEndDate(0);
        replacedKeywordJs.setDate.setStartDate(flag);
        $("#searchEdate").datepicker("option", "minDate", $("#searchSdate").val());

        if(element){
            replacedKeywordJs.buttonClickEvent(element);
        }else{
            replacedKeywordJs.buttonClickEvent($('#rangeDefault'));
        }
    },
    /**
     * 버튼 클릭시 색상 변경 이벤트
     * @param element
     */
    buttonClickEvent: function(element){
        $('button[name=rangeBtn]').each(function(){
            if($(this).hasClass("cyan")){
                $(this).removeClass("cyan");
                $(this).addClass("gray-dark")
            }
        });
        $(element).removeClass("gray-dark");
        $(element).addClass("cyan");
    },
    initInputDate : function () {
        replacedKeywordJs.setDate = Calendar.datePickerRange('sdate', 'edate');
        replacedKeywordJs.setDate.setEndDate("");
        replacedKeywordJs.setDate.setStartDate("");
        $("#edate").datepicker("option", "minDate", $("#sdate").val());
    },
    event : function() {
        $('#searchBtn').click(function(){
            replacedKeywordJs.search(0,replacedKeywordJs.limitPageSize());
        });
        $('#keyword').calcTextLength('keyup', '#textCountKr_keyword');
        $('#replacedKeyword').calcTextLength('keyup', '#textCountKr_replacedKeyword');
        $("#deleteBtn").bindClick(replacedKeywordJs.delete);
        $("#saveBtn").click(function(){
            replacedKeywordJs.write(false);
        });
        $("#editBtn").click(function() {
            replacedKeywordJs.write(true);
        });
        $("#excelDownloadBtn").click(function() {
            replacedKeywordGrid.excelDownload();
        });
        $("#resetBtn").click(function() {
            replacedKeywordJs.reset();
        });
    },
    reset : function() {
        $('#replacedId').val('');
        $('#keyword').val('');
        $('#replacedKeyword').val('');
        $("#useFlag option:first").prop("selected",true);
        replacedKeywordJs.initInputDate();
        $("#sdate").attr("disabled",true);
        $("#edate").attr("disabled",true);
        $("input:radio[name='operationPeriodYn']:radio[value='N']").prop('checked', true);
        $("#textCountKr_keyword").html(0);
        $("#textCountKr_replacedKeyword").html(0);
        $("#saveBtn").show();
        $("#editBtn").hide();
    },
    delete : function() {
        if(!$.trim($("#replacedId").val())) {
            alert('삭제하려는 검색어를 선택해주세요.');
            return false;
        }
        if(confirm('해당 검색어를 삭제하시겠습니까?')) {
            CommonAjax.basic({
                url: '/search/replaced/'+$("#replacedId").val()+'/deleteKeyword.json',
                data: null,
                type: 'get',
                method:"GET",
                callbackFunc:function() {
                    alert('삭제되었습니다.');
                    replacedKeywordJs.search(0,replacedKeywordJs.limitPageSize());
                }
            });
        }
        return false;
    },
    ///대체검색어 등록
    write : function(edit) {

        if(!$.trim($("#keyword").val())) {
            alert('검색어를 입력해주세요.');
            $("#keyword").focus();
            return false;
        }

        if(!$.trim($("#replacedKeyword").val())) {
            alert('대체검색어를 입력해주세요.');
            $("#replacedKeyword").focus();
            return false;
        }

        let operationPeriodYn = $("input:radio[name=operationPeriodYn]:checked").val();
        let sdate = $("#sdate").val();
        let edate = $("#edate").val();
        if(operationPeriodYn == 'Y'){
            if(sdate == ''){
                alert("사용기간을 설정해주세요.");
                $("#sdate").focus();
                return false;
            } else if (edate == ''){
                alert("사용기간을 설정해주세요.");
                $("#edate").focus();
                return false;
            }
        }

        let requestData = {};
        requestData.keyword  = $("#keyword").val();
        requestData.replacedKeyword = $("#replacedKeyword").val();
        requestData.useFlag = $("#useFlag option:selected").val();
        requestData.sdate = sdate;
        requestData.edate = edate;
        requestData.operationPeriodYn = operationPeriodYn;
        let url = '';
        if(!edit)
        {
            url = '/search/replaced/insertKeyword.json';
        }
        else
        {
            if(!$("#replacedId").val()) {
                alert('수정하려는 검색어를 먼저 선택해 주세요.');
                return false;
            }
            url = '/search/replaced/'+$("#replacedId").val()+'/updateKeyword.json';
        }

        CommonAjax.basic({
            url: url,
            data: JSON.stringify(requestData),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            callbackFunc:function(res) {
                if (res == 1) {
                    $("#searchKeyword").val($("#keyword").val());
                    replacedKeywordJs.search(0,replacedKeywordJs.limitPageSize());
                    if(edit) {
                        alert('수정 되었습니다');
                    } else {
                        alert('등록 되었습니다');
                    }
                }
                else if(res == -1) {
                    alert('이미 등록된 검색어 입니다.');
                }
            }
        });
    },
    search : function (offset, limit, callback) {

        var data = {};

        var search_keyword = $('#searchKeyword').val();

        /// 시작일
        data.sdate = $("input[name=searchSdate]").val();
        /// 종료일
        data.edate = $("input[name=searchEdate]").val();

        ///대체검색어 포함 모두 찾기
        data.innerYn = $("input[name=inner]:checked").val() == "Y" ? "Y" : "N";

        ///원형검색어
        if($.trim(search_keyword) != "")
        {
            data.keyword = "%"+search_keyword+"%";
        }
        else {
            data.keyword = "";
        }

        data.schDateType = $("select[name=schDateType] option:selected").val();

        data.offset = offset;
        data.limit = limit;

        CommonAjax.basic(
            {
                url:'/search/replaced/keywordList.json',
                data: JSON.stringify(data),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                callbackFunc:function(res)
                {
                    if(offset > 0) {
                        var newStart = replacedKeywordGrid.dataProvider.getRowCount();
                        replacedKeywordGrid.dataProvider.fillJsonData(res.list,{fillMode: "append"});
                    }
                    else
                    {
                        replacedKeywordGrid.setData(res.list);
                        $('#dictionaryTotalCount').html($.jUtil.comma(res.totalCount));
                        replacedKeywordGrid.page = 0;
                        if(replacedKeywordGrid.dataProvider.getRowCount() > 0) {
                            $('#replacedId').val(replacedKeywordGrid.dataProvider.getJsonRow(0).replacedId);
                            replacedKeywordJs.selectedIndex(replacedKeywordGrid.dataProvider.getJsonRow(0));
                        } else {
                            replacedKeywordJs.reset(); //검색 결과가 없을 시 초기화
                        }
                    }

                    if(callback != "undefined" && callback != null) {
                        callback();
                    }
                }
            });

    },
    radioClickEvent: function(value){
        replacedKeywordJs.initInputDate();
        if(value == 'Y'){
            $("#sdate").attr("disabled",false);
            $("#edate").attr("disabled",false);
        } else {
            $("#sdate").attr("disabled",true);
            $("#edate").attr("disabled",true);
        }
    }
};