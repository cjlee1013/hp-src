/** 실시간 판매업체 상품 판매 현황 Main */
var realtimePartnerItemSaleStatisticsMng = {
    /**
     * init 이벤트
     */
    init: function () {
        realtimePartnerItemSaleStatisticsMng.bindEvent();
        realtimePartnerItemSaleStatisticsMng.initDate();
        deliveryCore_category.initCategory();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        realtimePartnerItemSaleStatisticsMng.setDate = Calendar.datePicker("schBasicDt");
        realtimePartnerItemSaleStatisticsMng.setDate.setDate(0);
        $("#schTodayYn").val("Y");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/realtimePartnerItemSaleStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 730);
        });
        // 조회일자 변경시
        $("#schBasicDt").on("change", function() {
            var todayDate = $.datepicker.formatDate('yy-mm-dd', new Date());
            if (todayDate === $("#schBasicDt").val()) {
                $("#schTodayYn").val("Y");
            } else {
                $("#schTodayYn").val("N");
            }
        });
        // 상품번호 입력시 패턴체크
        $("#schItemNoList").on("keyup", function() {
            var schItemNoList = $("#schItemNoList").val();
            if (!$.jUtil.isEmpty(schItemNoList)) {
                let pattern = /[^0-9,]/g;
                if (pattern.test(schItemNoList)) {
                    alert('숫자 또는 ","만 입력하세요.');
                    $("#schItemNoList").val("").focus();
                }
                var itemNoArr = schItemNoList.split(',');
                if (itemNoArr.length > 50) {
                    alert('최대 50개 까지만 입력하세요.');
                    $("#schItemNoList").val("").focus();
                }
            }
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!realtimePartnerItemSaleStatisticsMng.validSearch()) {
                return false;
            }
            realtimePartnerItemSaleStatisticsMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(realtimePartnerItemSaleStatisticsMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(realtimePartnerItemSaleStatisticsMng.excelDownload);
    },
    /**
     * 판매업체 조회 팝업
     */
    openPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=" + callBack + "&partnerType=SELL", "partnerPop", "1080", "600", "yes", "yes");
    },
    /**
     * 판매업체 팝업 callback
     */
    openPartnerPopCallback : function(res) {
        $("#schPartnerId").val(res[0].partnerId);
        $("#schPartnerNm").val(res[0].partnerNm);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schBasicDt = $("#schBasicDt").val();
        var schItemNoList = $("#schItemNoList").val();
        if ($.jUtil.isEmpty(schBasicDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        }
        if(!$.jUtil.isEmpty(schItemNoList)) {
            $("#schItemNoList").val(schItemNoList.replace(/(?:\r\n|\r|\n)/g, ''));
            let pattern = /[^0-9,]/g;
            if(pattern.test(schItemNoList)) {
                alert('숫자 또는 ","만 입력하세요.');
                $("#schItemNoList").focus();
                return false;
            }
        }
        return true;
    },
    /**
     * 통계주문정보 리스트 조회
     */
    search: function () {
        var data = $("#realtimePartnerItemSaleStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getRealtimePartnerItemSaleStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                realtimePartnerItemSaleStatisticsGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#realtimePartnerItemSaleStatisticsSearchForm").resetForm();
        realtimePartnerItemSaleStatisticsMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(realtimePartnerItemSaleStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "실시간판매업체상품판매현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        // 추출이력 저장
        realtimePartnerItemSaleStatisticsMng.saveExtractHistory();
        realtimePartnerItemSaleStatisticsGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/statistics/saveRealtimePartnerItemSaleExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var realtimePartnerItemSaleStatisticsGrid = {
    gridView: new RealGridJS.GridView("realtimePartnerItemSaleStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        realtimePartnerItemSaleStatisticsGrid.initGrid();
        realtimePartnerItemSaleStatisticsGrid.initDataProvider();
        realtimePartnerItemSaleStatisticsGrid.event();
    },
    initGrid: function () {
        realtimePartnerItemSaleStatisticsGrid.gridView.setDataSource(realtimePartnerItemSaleStatisticsGrid.dataProvider);
        realtimePartnerItemSaleStatisticsGrid.gridView.setStyles(realtimePartnerItemSaleStatisticsGridBaseInfo.realgrid.styles);
        realtimePartnerItemSaleStatisticsGrid.gridView.setDisplayOptions(realtimePartnerItemSaleStatisticsGridBaseInfo.realgrid.displayOptions);
        realtimePartnerItemSaleStatisticsGrid.gridView.setColumns(realtimePartnerItemSaleStatisticsGridBaseInfo.realgrid.columns);
        realtimePartnerItemSaleStatisticsGrid.gridView.setOptions(realtimePartnerItemSaleStatisticsGridBaseInfo.realgrid.options);
        realtimePartnerItemSaleStatisticsGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
        setColumnLookupOptionForJson(realtimePartnerItemSaleStatisticsGrid.gridView, "taxYn", taxYnJson);
    },
    initDataProvider: function () {
        realtimePartnerItemSaleStatisticsGrid.dataProvider.setFields(realtimePartnerItemSaleStatisticsGridBaseInfo.dataProvider.fields);
        realtimePartnerItemSaleStatisticsGrid.dataProvider.setOptions(realtimePartnerItemSaleStatisticsGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        realtimePartnerItemSaleStatisticsGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            if ($.jUtil.isNotEmpty(itemNo)) {
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            }
        };
    },
    setData: function (dataList) {
        realtimePartnerItemSaleStatisticsGrid.dataProvider.clearRows();
        realtimePartnerItemSaleStatisticsGrid.dataProvider.setRows(dataList);
    }
};