/** 실시간 판매업체별 판매 현황 Main */
var realtimePartnerOrderStatisticsMng = {
    /**
     * init 이벤트
     */
    init: function () {
        realtimePartnerOrderStatisticsMng.bindEvent();
        realtimePartnerOrderStatisticsMng.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        realtimePartnerOrderStatisticsMng.setDate = Calendar.datePicker("schBasicDt");
        realtimePartnerOrderStatisticsMng.setDate.setDate(0);
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/realtimePartnerOrderStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 480);
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!realtimePartnerOrderStatisticsMng.validSearch()) {
                return false;
            }
            realtimePartnerOrderStatisticsMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(realtimePartnerOrderStatisticsMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(realtimePartnerOrderStatisticsMng.excelDownload);
    },
    /**
     * 판매업체 조회 팝업
     */
    openPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=" + callBack + "&partnerType=SELL", "partnerPop", "1080", "600", "yes", "yes");
    },
    /**
     * 판매업체 팝업 callback
     */
    openPartnerPopCallback : function(res) {
        $("#schPartnerId").val(res[0].partnerId);
        $("#schPartnerNm").val(res[0].partnerNm);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schBasicDt = $("#schBasicDt").val();
        if ($.jUtil.isEmpty(schBasicDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 통계주문정보 리스트 조회
     */
    search: function () {
        var data = $("#realtimePartnerOrderStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getRealtimePartnerOrderStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                realtimePartnerOrderStatisticsGrid.setData(dataList);
                $('#realtimePartnerSearchCnt').html($.jUtil.comma(realtimePartnerOrderStatisticsGrid.gridView.getItemCount()));
                var headerCells = ["orderQty", "orderPrice", "cancelQty", "cancelPrice", "returnQty", "returnPrice"];
                realtimePartnerOrderStatisticsGrid.setGridSumData(realtimePartnerOrderStatisticsGrid.gridView, headerCells);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#realtimePartnerOrderStatisticsSearchForm").resetForm();
        realtimePartnerOrderStatisticsMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(realtimePartnerOrderStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "실시간판매업체별판매현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        // 추출이력 저장
        realtimePartnerOrderStatisticsMng.saveExtractHistory();
        realtimePartnerOrderStatisticsGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/statistics/saveRealtimePartnerOrderExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var realtimePartnerOrderStatisticsGrid = {
    gridView: new RealGridJS.GridView("realtimePartnerOrderStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        realtimePartnerOrderStatisticsGrid.initGrid();
        realtimePartnerOrderStatisticsGrid.initDataProvider();
    },
    initGrid: function () {
        realtimePartnerOrderStatisticsGrid.gridView.setDataSource(realtimePartnerOrderStatisticsGrid.dataProvider);
        realtimePartnerOrderStatisticsGrid.gridView.setStyles(realtimePartnerOrderStatisticsGridBaseInfo.realgrid.styles);
        realtimePartnerOrderStatisticsGrid.gridView.setDisplayOptions(realtimePartnerOrderStatisticsGridBaseInfo.realgrid.displayOptions);
        realtimePartnerOrderStatisticsGrid.gridView.setColumns(realtimePartnerOrderStatisticsGridBaseInfo.realgrid.columns);
        realtimePartnerOrderStatisticsGrid.gridView.setOptions(realtimePartnerOrderStatisticsGridBaseInfo.realgrid.options);
        realtimePartnerOrderStatisticsGrid.gridView.onDataCellClicked = realtimePartnerOrderStatisticsGrid.selectRow;
        var mergeCells = ["businessNm", "partnerId", "ofVendorCd"];
        realtimePartnerOrderStatisticsGrid.setGridSumRow(realtimePartnerOrderStatisticsGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        realtimePartnerOrderStatisticsGrid.dataProvider.setFields(realtimePartnerOrderStatisticsGridBaseInfo.dataProvider.fields);
        realtimePartnerOrderStatisticsGrid.dataProvider.setOptions(realtimePartnerOrderStatisticsGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        realtimePartnerOrderStatisticsGrid.dataProvider.clearRows();
        realtimePartnerOrderStatisticsGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "statistical"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
};