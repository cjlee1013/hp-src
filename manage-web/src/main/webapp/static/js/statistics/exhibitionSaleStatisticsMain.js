var exhibitionSaleStatisticsMain = {
    /**
     * init 이벤트
     */
    init: function() {
        exhibitionSaleStatisticsMain.initUi();
        exhibitionSaleStatisticsMain.bindingEvent();
        deliveryCore.initSearchDate("0", "0");
    },
    initUi: function() {
        $("#schExhibitionNm").css("width", "500px");
    },
    bindingEvent: function() {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/exhibitionSaleStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 460);
        });
        // 상단 검색영역 검색
        $("#schBtn").bindClick(exhibitionSaleStatisticsMain.search);
        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(exhibitionSaleStatisticsMain.reset,"search");
        // 기획전엑셀다운로드
        $("#ExhibitionExcelDownBtn").bindClick(exhibitionGrid.excelDownload);
        // 기획전상품엑셀다운로드
        $("#ExhibitionItemExcelDownBtn").bindClick(exhibitionItemGrid.excelDownload);
    },

    search: function() {
        if (!exhibitionSaleStatisticsMain.valid()) {
            return;
        }

        var data = $("#exhibitionSaleStatisticsSearchForm").serialize();
        exhibitionAjax.searchExhibition(data);
    },
    reset: function() {
        $("#exhibitionSaleStatisticsSearchForm").resetForm();
        deliveryCore.initSearchDate("0", "0");
        exhibitionGrid.dataProvider.clearRows();
        exhibitionItemGrid.dataProvider.clearRows();
    },

    valid: function() {
        // 날짜 체크
        if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid"])) {
            return false;
        }

        // 최대 조회기간 체크
        if (!exhibitionSaleStatisticsMain.checkMaxSearchDay()) {
            return false;
        }


        return true;
    },

    checkMaxSearchDay() {
        if (moment($("#schApplyEndDt").val()).diff($("#schApplyStartDt").val(), "day", true) > 7) {
            alert("조회기간은 최대 7일까지 설정 가능합니다.");
            $("#schApplyStartDt").val(moment($("#schApplyEndDt").val()).add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }
        return true;
    }
};

var exhibitionGrid = {
    gridView: new RealGridJS.GridView("exhibitionSaleStatisticsExhGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),

    init: function() {
        exhibitionGrid.initGrid();
        exhibitionGrid.initDataProvider();
        exhibitionGrid.event();
    },
    initGrid: function() {
        exhibitionGrid.gridView.setDataSource(exhibitionGrid.dataProvider);
        exhibitionGrid.gridView.setStyles(exhibitionSaleStatisticsExhBaseInfo.realgrid.styles);
        exhibitionGrid.gridView.setDisplayOptions(exhibitionSaleStatisticsExhBaseInfo.realgrid.displayOptions);
        exhibitionGrid.gridView.setColumns(exhibitionSaleStatisticsExhBaseInfo.realgrid.columns);
        exhibitionGrid.gridView.setOptions(exhibitionSaleStatisticsExhBaseInfo.realgrid.options);
    },
    initDataProvider: function() {
        exhibitionGrid.dataProvider.setFields(exhibitionSaleStatisticsExhBaseInfo.dataProvider.fields);
        exhibitionGrid.dataProvider.setOptions(exhibitionSaleStatisticsExhBaseInfo.dataProvider.options);
    },
    event: function() {
        exhibitionGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = {
                "schApplyStartDt": $("#exhSearchStartDt").val(),
                "schApplyEndDt": $("#exhSearchEndDt").val(),
                "schExhibitionNm" : exhibitionGrid.dataProvider.getValue(index.dataRow, "exhibitionNm")
            };

            exhibitionAjax.searchExhibitionItem(data);
        };
    },
    setData: function(dataList) {
        exhibitionGrid.dataProvider.clearRows();
        exhibitionGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(exhibitionGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "기획전별판매현황_기획전내역" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        exhibitionGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중입니다."
        });
    }
};

var exhibitionItemGrid = {
    gridView: new RealGridJS.GridView("exhibitionSaleStatisticsItemGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function() {
        exhibitionItemGrid.initGrid();
        exhibitionItemGrid.initDataProvider();
        exhibitionItemGrid.event();
    },
    initGrid: function() {
        exhibitionItemGrid.gridView.setDataSource(exhibitionItemGrid.dataProvider);
        exhibitionItemGrid.gridView.setStyles(exhibitionSaleStatisticsItemBaseInfo.realgrid.styles);
        exhibitionItemGrid.gridView.setDisplayOptions(exhibitionSaleStatisticsItemBaseInfo.realgrid.displayOptions);
        exhibitionItemGrid.gridView.setColumns(exhibitionSaleStatisticsItemBaseInfo.realgrid.columns);
        exhibitionItemGrid.gridView.setOptions(exhibitionSaleStatisticsItemBaseInfo.realgrid.options);
    },
    initDataProvider: function() {
        exhibitionItemGrid.dataProvider.setFields(exhibitionSaleStatisticsItemBaseInfo.dataProvider.fields);
        exhibitionItemGrid.dataProvider.setOptions(exhibitionSaleStatisticsItemBaseInfo.dataProvider.options);
    },
    event: function() {},
    setData: function(dataList) {
        exhibitionItemGrid.dataProvider.clearRows();
        exhibitionItemGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(exhibitionItemGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "기획전별판매현황_기획전_상품내역" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        exhibitionItemGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중입니다."
        });
    }
};

var exhibitionAjax = {
    searchExhibition : function (data) {
        CommonAjax.basic({
            url         : '/statistics/getExhibitionSaleStatisticsExh.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                exhibitionGrid.setData(res);
                $('#exhibitionSaleStatisticsExhSearchCount').html($.jUtil.comma(exhibitionGrid.gridView.getItemCount()));
                $("#exhSearchStartDt").val($("#schApplyStartDt").val());
                $("#exhSearchEndDt").val($("#schApplyEndDt").val());
                exhibitionItemGrid.dataProvider.clearRows();
            }
        });
    },

    searchExhibitionItem : function(data) {
        CommonAjax.basic({
            url         : '/statistics/getExhibitionSaleStatisticsItem.json',
            data        : JSON.stringify(data),
            method      : 'post',
            dataType    : 'json',
            contentType : 'application/json; charset=utf-8',
            callbackFunc: function(res) {
                exhibitionItemGrid.setData(res);
                $('#exhibitionSaleStatisticsItemSearchCount').html($.jUtil.comma(exhibitionItemGrid.gridView.getItemCount()));
            }
        });
    }
};