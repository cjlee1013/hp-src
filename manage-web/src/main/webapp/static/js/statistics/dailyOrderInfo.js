var DailyOrderInfoGrid = {
    gridView : new RealGridJS.GridView("DailyOrderInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        DailyOrderInfoGrid.initGrid();
        DailyOrderInfoGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        DailyOrderInfoGrid.gridView.setDataSource(DailyOrderInfoGrid.dataProvider);
        DailyOrderInfoGrid.gridView.setStyles(DailyOrderInfoBaseInfo.realgrid.styles);
        DailyOrderInfoGrid.gridView.setDisplayOptions(DailyOrderInfoBaseInfo.realgrid.displayOptions);
        DailyOrderInfoGrid.gridView.setColumns(DailyOrderInfoBaseInfo.realgrid.columns);
        DailyOrderInfoGrid.gridView.setOptions(DailyOrderInfoBaseInfo.realgrid.options);
        DailyOrderInfoGrid.gridView.onDataCellClicked = DailyOrderInfoGrid.selectRow;
        var mergeCells = [["storeNm", "storeId"],["businessNm", "partnerId", "ofVendorCd"]];
        DailyOrderInfoGrid.setRealGridSumHeader(DailyOrderInfoGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        DailyOrderInfoGrid.dataProvider.setFields(DailyOrderInfoBaseInfo.dataProvider.fields);
        DailyOrderInfoGrid.dataProvider.setOptions(DailyOrderInfoBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        DailyOrderInfoGrid.dataProvider.clearRows();
        DailyOrderInfoGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(DailyOrderInfoGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        DailyOrderInfoGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: DailyOrderInfoGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    fileName : ""
};
/***
 * 매출조회 파트너 상세
 */
var DailyOrderInfoForm = {
    init : function () {
        this.initSearchDate('-3d');
        this.initSearchYM();
        DailyOrderInfoForm.bindingEvent();
    },
    initSearchDate : function (flag) {
        DailyOrderInfoForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        DailyOrderInfoForm.setDate.setEndDate('-1d');
        DailyOrderInfoForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startYear, #endYear').val(year);
        DailyOrderInfoForm.setMonth($('#startYear')[0],'startMonth');
        DailyOrderInfoForm.setMonth($('#endYear')[0],'endMonth');
        $('#startMonth, #endMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(DailyOrderInfoForm.search);
        $('#clearBtn').bindClick(DailyOrderInfoForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(DailyOrderInfoGrid.excelDownload);
    },
    search : function () {
        var startDt;
        var endDt;
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).endOf('month');

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }

        }
        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var searchForm = "startDt="+startDt+"&endDt="+endDt;
        DailyOrderInfoGrid.fileName = "기간별_" + getName + "_판매현황_" + startDt+"_"+endDt;

        var storeType = $('#schStoreType').val();
        searchForm = searchForm + "&storeType=" + storeType;
        var storeId = $('#schStoreId').val();
        if (storeId != "") {
            searchForm = searchForm + "&" + getType + "Id=" + storeId;
            DailyOrderInfoGrid.fileName += "_" + $('#schStoreNm').val();
        }
        var marketType = $('#schMarketType').val();
        if (marketType != "") {
            searchForm = searchForm + "&marketType=" + marketType;
        }
        CommonAjax.basic(
        {
            url:'/statistics/dailyOrderInfo/get'+ getType+ getKind + '.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                DailyOrderInfoGrid.setData(res);
                var stringCol = [ "orderQty", "orderPrice", "cancelQty", "cancelPrice", "returnQty", "returnPrice", "subQty", "subPrice"];
                DailyOrderInfoGrid.setRealGridHeaderSum(DailyOrderInfoGrid.gridView, stringCol);
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            DailyOrderInfoForm.init();
        });
    },
    setMonth : function(_year,_id) {
        var nowDate = new Date();
        var maxMonth;
        if (_year.value == nowDate.getFullYear()) {
            maxMonth = nowDate.getMonth();
        } else {
            maxMonth = 11;
        }
        $('#'+_id).empty();
        for (var i=1;i<=maxMonth+1;i++) {
            if (i<10) {
                $('#'+_id).append("<option value='0" +i+ "'>0" + i + " 월</option>");
            } else  {
                $('#'+_id).append("<option value='" +i+ "'>" + i + " 월</option>");
            }
        }
    },
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    }
};

var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    }
}