const combineOrderStatusMain = {
    /**
     * init 이벤트
     */
    init: function() {
        combineOrderStatusMain.initUi();
        combineOrderStatusMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schOrderStartDt", "schOrderEndDt", "-3", "-1");
        deliveryCore.initSearchDateCustom("schShipReqStartDt", "schShipReqEndDt", "-1", "-1");
    },
    initUi: function() {
        $("#schStoreType").css("width", "100px");
    },
    bindingEvent: function() {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/combineOrderStatusHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 630);
        });
        // 상단 검색영역 검색
        $("#schBtn").bindClick(combineOrderStatusMain.search);
        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(combineOrderStatusMain.reset,"search");
        // 엑셀다운로드
        $("#excelDownBtn").bindClick(combineOrderStatusMain.excelDownload);
    },
    search: function() {
        if (!combineOrderStatusMain.valid()) {
            return;
        }

        var data = {
            "schOrderStartDt" : $("#schOrderStartDt").val()
            , "schOrderEndDt" : $("#schOrderEndDt").val()
            , "schShipReqStartDt" : $("#schShipReqStartDt").val()
            , "schShipReqEndDt" : $("#schShipReqEndDt").val()
            , "schStoreType" : "HYPER"
            , "schStoreId" : $("#schStoreId").val()
        };
        combineOrderStatusAjax.searchCombineOrderStatus(data);
    },
    reset: function() {
        $("#combineOrderStatusSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schOrderStartDt", "schOrderEndDt", "-3", "-1");
        deliveryCore.initSearchDateCustom("schShipReqStartDt", "schShipReqEndDt", "-1", "-1");
    },
    valid: function() {
        // 날짜 체크
        if (!deliveryCore.dateValid($("#schOrderStartDt"), $("#schOrderEndDt"), ["isValid"])) { return false }
        if (!deliveryCore.dateValid($("#schShipReqStartDt"), $("#schShipReqEndDt"), ["isValid"])) { return false }
        if (!combineOrderStatusMain.checkMaxSearchDay()) { return false }

        return true;
    },
    checkMaxSearchDay: function() {
        if (moment($("#schOrderEndDt").val()).diff($("#schOrderStartDt").val(), "month", true) > 1) {
            alert("주문일 조회기간은 최대 1개월까지 설정 가능합니다.");
            $("#schOrderStartDt").val(moment($("#schOrderEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schShipReqEndDt").val()).diff($("#schShipReqStartDt").val(), "month", true) > 1) {
            alert("배송일 조회기간은 최대 1개월까지 설정 가능합니다.");
            $("#schShipReqStartDt").val(moment($("#schShipReqEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    },
    excelDownload: function() {
        if(combineOrderStatusGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "합배송_주문현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        /* 히스토리 적재 */
        combineOrderStatusAjax.saveExtractHistoryForExcelDown();

        combineOrderStatusGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중입니다."
        });
    }
};

const combineOrderStatusGrid = {
    gridView: new RealGridJS.GridView("combineOrderStatusGridView"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function() {
        combineOrderStatusGrid.initGrid();
        combineOrderStatusGrid.initDataProvider();
    },
    initGrid: function() {
        combineOrderStatusGrid.gridView.setDataSource(combineOrderStatusGrid.dataProvider);
        combineOrderStatusGrid.gridView.setStyles(combineOrderStatusBaseInfo.realgrid.styles);
        combineOrderStatusGrid.gridView.setDisplayOptions(combineOrderStatusBaseInfo.realgrid.displayOptions);
        combineOrderStatusGrid.gridView.setColumns(combineOrderStatusGrid.makeHeaderColumn(combineOrderStatusBaseInfo));
        combineOrderStatusGrid.gridView.setOptions(combineOrderStatusBaseInfo.realgrid.options);
        combineOrderStatusGrid.gridView.setCopyOptions({enabled: false});
        combineOrderStatusGrid.setGridSumRow(combineOrderStatusGrid.gridView, ["storeNm", "storeId"]);
        combineOrderStatusGrid.setGridSumData(combineOrderStatusGrid.gridView, [
            "orgOrderCount","orgOrderAmt","orgCancelCount","orgCancelAmt","orgReturnCount","orgReturnAmt","orgSubCount","orgSubAmt",
            "combOrderCount","combOrderAmt","combCancelCount","combCancelAmt","combReturnCount","combReturnAmt","combSubCount","combSubAmt"
        ]);
    },
    initDataProvider: function() {
        combineOrderStatusGrid.dataProvider.setFields(combineOrderStatusBaseInfo.dataProvider.fields);
        combineOrderStatusGrid.dataProvider.setOptions(combineOrderStatusBaseInfo.dataProvider.options);
    },
    setData: function(dataList) {
        combineOrderStatusGrid.dataProvider.clearRows();
        combineOrderStatusGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(combineOrderStatusGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "합배송_주문현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        /* 히스토리 적재 */
        combineOrderStatusAjax.saveExtractHistoryForExcelDown();

        combineOrderStatusGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중입니다."
        });
    },

    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "aggregate"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },

    /**
     * 그리드 재구성(3depth group 헤더 만드는 공통함수가 없음)
     */
    makeHeaderColumn: function(combineOrderStatusBaseInfo) {
        let colums = combineOrderStatusBaseInfo.realgrid.columns;
        let newColums = [];
        let orgOrderCols = combineOrderStatusGrid.getGroupCol("주문");
        let orgCancelCols = combineOrderStatusGrid.getGroupCol("취소");
        let orgReturnCols = combineOrderStatusGrid.getGroupCol("반품");
        let orgSubCols = combineOrderStatusGrid.getGroupCol("대체주문");
        let combOrderCols = combineOrderStatusGrid.getGroupCol("주문");
        let combCancelCols = combineOrderStatusGrid.getGroupCol("취소");
        let combReturnCols = combineOrderStatusGrid.getGroupCol("반품");
        let combSubCols = combineOrderStatusGrid.getGroupCol("대체주문");
        let orgCols = combineOrderStatusGrid.getGroupCol("원주문", [orgOrderCols, orgCancelCols, orgReturnCols, orgSubCols]);
        let combCols =  combineOrderStatusGrid.getGroupCol("합배송", [combOrderCols, combCancelCols, combReturnCols, combSubCols]);

        for (let i = 0; i < colums.length; i++) {
            let col = colums[i];
            if (!col.type || col.type !== 'group') {
                newColums.push(col);
            } else {
                let subColumns = col.columns;
                for (let j = 0; j < subColumns.length; j++) {
                    let subCol = subColumns[j];
                    if (subColumns[j].fieldName.indexOf("orgOrder") === 0) {
                        orgOrderCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("orgCancel") === 0) {
                        orgCancelCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("orgReturn") === 0) {
                        orgReturnCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("orgSub") === 0) {
                        orgSubCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("combOrder") === 0) {
                        combOrderCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("combCancel") === 0) {
                        combCancelCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("combReturn") === 0) {
                        combReturnCols.columns.push(subCol);
                    } else if (subColumns[j].fieldName.indexOf("combSub") === 0) {
                        combSubCols.columns.push(subCol);
                    }
                }
            }
        }
        newColums.push(orgCols);
        newColums.push(combCols);

        return newColums;
    },

    getGroupCol: function(groupName, columns) {
        columns = columns ? columns : [];
        return {"type":"group","name":groupName,"columns":columns,"width":450};
    }
};

const combineOrderStatusAjax = {
    searchCombineOrderStatus : function (data) {
        CommonAjax.basic({
            url         : '/statistics/orderPayment/getCombineOrderStatus.json',
            data        : JSON.stringify(data),
            method      : 'post',
            dataType    : 'json',
            contentType : 'application/json; charset=utf-8',
            callbackFunc: function(res) {
                combineOrderStatusGrid.setData(res);
            }
        });
    },
    saveExtractHistoryForExcelDown : function () {
        CommonAjax.basic({
            url         : '/statistics/orderPayment/saveExtractHistoryCombineOrderStatus.json',
            data        : null,
            method      : 'post',
            dataType    : 'json',
            contentType : 'application/json; charset=utf-8'
        });
    }
};