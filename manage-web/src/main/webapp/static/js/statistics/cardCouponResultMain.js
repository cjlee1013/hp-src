/** Main Script */
var cardCouponResultMain = {
    global : {
      selectedCouponNo : 0
    },
    /**
     * init 이벤트
     */
    init: function() {
        cardCouponResultMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(cardCouponResultMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(cardCouponResultMain.reset);
        // 이번주 버튼
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", -(new Date()).getDay()+1, "-1") ;});
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "-1m", "-1") ;});
        // 엑셀다운로드 메인
        $("#schExcelDownloadBtn").bindClick(cardCouponResultMain.excelDownloadMain);

        $("#descriptionBtn").on('click', function(){
            // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
            var _width  = 780;
            var _url = '/escrow/statistics/popup/cardCouponResultStatisticsPop';
            var _height = 570;
            var _left = ($(window).width()/2)-(_width/2);
            var _top = ($(window).height()/2)-(_height/2);
            window.open(_url, '_blank',
                'toolbar=no, location=no, menubar=no, scrollbars=no, resizable=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top);
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // 유효성 체크
        if (!cardCouponResultMain.valid()) {
            return;
        }

        var data = $("#cardCouponResultSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getCardCouponResultStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                cardCouponResultGrid.setData(res);
                cardCouponResultDetailGrid.setData();
            }
        });
    },

    /**
     * 상세검색
     * @param couponNo 쿠폰번호
     * @param couponKind 쿠폰종류
     */
    detailSearch: function(rowDataJson) {
        var couponNo = rowDataJson.couponNo;
        var storeType = rowDataJson.storeType;

        if (!cardCouponResultMain.valid()) {
            return;
        }

        var reqData = new Object();

        reqData.schStartDt = $("#schStartDt").val().replace(/-/gi, "");
        reqData.schEndDt = $("#schEndDt").val().replace(/-/gi, "");
        reqData.schCouponNo = couponNo;
        reqData.schStoreType = storeType;

        CommonAjax.basic({
            url         : '/escrow/statistics/getCardCouponResultStatisticsDetailList.json',
            data        : JSON.stringify(reqData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                cardCouponResultDetailGrid.setData(res);

                cardCouponResultMain.global.selectedCouponNo = couponNo;
            }
        });

    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        //  검색조건(필수)
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if ($.jUtil.isEmpty(schKeywordVal)) {
            alert("검색어를 입력해 주세요.");
            return false;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var _date = new Date();
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var nowDt =  _date.getFullYear() + "-" + ("0" + (_date.getMonth() + 1)).slice(-2) + "-" + ("0" + _date.getDate()).slice(-2);
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schStartDt.diff(nowDt, "day", true) >= 0 || schEndDt.diff(nowDt, "day", true) >= 0) {
            alert("오늘 날짜로 조회가 불가합니다.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            return false;
        }

        return true;
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#cardCouponResultSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
    },

    /**
     * 엑셀 다운로드 상세
     */
    excelDownloadMain: function() {
        if (cardCouponResultDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "카드사별쿠폰실적조회_" + cardCouponResultMain.global.selectedCouponNo + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        SettleCommon.setRealGridColumnVisible(cardCouponResultDetailGrid.gridView, [SettleCommon.getRealGridColumnName(cardCouponResultDetailGrid.gridView, "쿠폰번호")], true);
        SettleCommon.setRealGridColumnVisible(cardCouponResultDetailGrid.gridView, [SettleCommon.getRealGridColumnName(cardCouponResultDetailGrid.gridView, "쿠폰명")], true);

        cardCouponResultDetailGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다.",
            done: function () {  //내보내기 완료 후 실행되는 함수
                SettleCommon.setRealGridColumnVisible(cardCouponResultDetailGrid.gridView, [SettleCommon.getRealGridColumnName(cardCouponResultDetailGrid.gridView, "쿠폰번호")], false);
                SettleCommon.setRealGridColumnVisible(cardCouponResultDetailGrid.gridView, [SettleCommon.getRealGridColumnName(cardCouponResultDetailGrid.gridView, "쿠폰명")], false);
            }
        });
    },


    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = cardCouponResultGrid.dataProvider.getJsonRow(selectRowId);
        cardCouponResultMain.detailSearch(rowDataJson);
    }
};

/** Grid Script */
var cardCouponResultGrid = {
    gridView: new RealGridJS.GridView("cardCouponResultGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardCouponResultGrid.initGrid();
        cardCouponResultGrid.initDataProvider();
        cardCouponResultGrid.event();
    },
    initGrid: function () {
        cardCouponResultGrid.gridView.setDataSource(cardCouponResultGrid.dataProvider);
        cardCouponResultGrid.gridView.setStyles(cardCouponResultGridBaseInfo.realgrid.styles);
        cardCouponResultGrid.gridView.setDisplayOptions(cardCouponResultGridBaseInfo.realgrid.displayOptions);
        cardCouponResultGrid.gridView.setColumns(cardCouponResultGridBaseInfo.realgrid.columns);
        cardCouponResultGrid.gridView.setOptions(cardCouponResultGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        cardCouponResultGrid.dataProvider.setFields(cardCouponResultGridBaseInfo.dataProvider.fields);
        cardCouponResultGrid.dataProvider.setOptions(cardCouponResultGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        cardCouponResultGrid.dataProvider.clearRows();
        cardCouponResultGrid.dataProvider.setRows(dataList);
    },
    event: function() {
        cardCouponResultGrid.gridView.onDataCellClicked = function(gridView, index) {
            cardCouponResultMain.gridRowSelect(index.dataRow);
        };
    },
};

/** Grid Script */
var cardCouponResultDetailGrid = {
    gridView: new RealGridJS.GridView("cardCouponResultDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardCouponResultDetailGrid.initGrid();
        cardCouponResultDetailGrid.initDataProvider();
    },
    initGrid: function () {
        cardCouponResultDetailGrid.gridView.setDataSource(cardCouponResultDetailGrid.dataProvider);
        cardCouponResultDetailGrid.gridView.setStyles(cardCouponResultDetailGridBaseInfo.realgrid.styles);
        cardCouponResultDetailGrid.gridView.setDisplayOptions(cardCouponResultDetailGridBaseInfo.realgrid.displayOptions);
        cardCouponResultDetailGrid.gridView.setColumns(cardCouponResultDetailGridBaseInfo.realgrid.columns);
        cardCouponResultDetailGrid.gridView.setOptions(cardCouponResultDetailGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        cardCouponResultDetailGrid.dataProvider.setFields(cardCouponResultDetailGridBaseInfo.dataProvider.fields);
        cardCouponResultDetailGrid.dataProvider.setOptions(cardCouponResultDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        cardCouponResultDetailGrid.dataProvider.clearRows();
        cardCouponResultDetailGrid.dataProvider.setRows(dataList);
    }
};