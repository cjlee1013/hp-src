/** 실시간 점포별 판매 현황 Main */
var realtimeStoreOrderStatisticsMng = {
    /**
     * init 이벤트
     */
    init: function () {
        realtimeStoreOrderStatisticsMng.bindEvent();
        realtimeStoreOrderStatisticsMng.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-7d", "0");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/realtimeStoreOrderStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 700);
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!realtimeStoreOrderStatisticsMng.validSearch()) {
                return false;
            }
            realtimeStoreOrderStatisticsMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(realtimeStoreOrderStatisticsMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(realtimeStoreOrderStatisticsMng.excelDownload);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStartDtVal = moment(schStartDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);
        var schStoreType = $("#schStoreType").val();

        if ($.jUtil.isEmpty(schStartDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "month", true) > 1) {
            alert("조회기간은 최대 1개월 이내만 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            return false;
        } else if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 통계주문정보 리스트 조회
     */
    search: function () {
        var data = $("#realtimeStoreOrderStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getRealtimeStoreOrderStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                realtimeStoreOrderStatisticsGrid.setData(dataList);
                var headerCells = ["orderQty", "orderPrice", "cancelQty", "cancelPrice", "returnQty", "returnPrice", "subQty", "subPrice"];
                realtimeStoreOrderStatisticsGrid.setGridSumData(realtimeStoreOrderStatisticsGrid.gridView, headerCells);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#realtimeStoreOrderStatisticsSearchForm").resetForm();
        realtimeStoreOrderStatisticsMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(realtimeStoreOrderStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "실시간점포별판매현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        // 추출이력 저장
        realtimeStoreOrderStatisticsMng.saveExtractHistory();
        realtimeStoreOrderStatisticsGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/statistics/saveRealtimeStoreOrderExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var realtimeStoreOrderStatisticsGrid = {
    gridView: new RealGridJS.GridView("realtimeStoreOrderStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        realtimeStoreOrderStatisticsGrid.initGrid();
        realtimeStoreOrderStatisticsGrid.initDataProvider();
    },
    initGrid: function () {
        realtimeStoreOrderStatisticsGrid.gridView.setDataSource(realtimeStoreOrderStatisticsGrid.dataProvider);
        realtimeStoreOrderStatisticsGrid.gridView.setStyles(realtimeStoreOrderStatisticsGridBaseInfo.realgrid.styles);
        realtimeStoreOrderStatisticsGrid.gridView.setDisplayOptions(realtimeStoreOrderStatisticsGridBaseInfo.realgrid.displayOptions);
        realtimeStoreOrderStatisticsGrid.gridView.setColumns(realtimeStoreOrderStatisticsGridBaseInfo.realgrid.columns);
        realtimeStoreOrderStatisticsGrid.gridView.setOptions(realtimeStoreOrderStatisticsGridBaseInfo.realgrid.options);
        realtimeStoreOrderStatisticsGrid.gridView.onDataCellClicked = realtimeStoreOrderStatisticsGrid.selectRow;
        var mergeCells = ["basicTime"];
        realtimeStoreOrderStatisticsGrid.setGridSumRow(realtimeStoreOrderStatisticsGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        realtimeStoreOrderStatisticsGrid.dataProvider.setFields(realtimeStoreOrderStatisticsGridBaseInfo.dataProvider.fields);
        realtimeStoreOrderStatisticsGrid.dataProvider.setOptions(realtimeStoreOrderStatisticsGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        realtimeStoreOrderStatisticsGrid.dataProvider.clearRows();
        realtimeStoreOrderStatisticsGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "statistical"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
};