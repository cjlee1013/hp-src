
/***
 * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
 */
var storePeriodListGrid = {
    gridView : new RealGridJS.GridView("storePeriodListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        storePeriodListGrid.initGrid();
        storePeriodListGrid.initDataProvider();
        storePeriodListGrid.event();
    },
    initGrid : function () {
        storePeriodListGrid.gridView.setDataSource(storePeriodListGrid.dataProvider);
        storePeriodListGrid.gridView.setStyles(storePeriodListBaseInfo.realgrid.styles);
        storePeriodListGrid.gridView.setDisplayOptions(storePeriodListBaseInfo.realgrid.displayOptions);
        storePeriodListGrid.gridView.setOptions(storePeriodListBaseInfo.realgrid.options);
        storePeriodListGrid.gridView.setColumns(storePeriodListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ [ "basicDt", "storeNm", "originStoreId" ] ];
        SettleCommon.setRealGridSumHeader(storePeriodListGrid.gridView, mergeCells);

        if ($("#mallType").val() == 'DS') {
            storePeriodListGrid.gridView.setColumnProperty(storePeriodListGrid.gridView.columnByName("storeNm"), "visible", false);
            storePeriodListGrid.gridView.setColumnProperty(storePeriodListGrid.gridView.columnByName("originStoreId"), "visible", false);
            storePeriodListGrid.gridView.setColumnProperty(storePeriodListGrid.gridView.columnByName("subOrderCnt"), "visible", false);
            storePeriodListGrid.gridView.setColumnProperty(storePeriodListGrid.gridView.columnByName("subOrderAmt"), "visible", false);
            storePeriodListGrid.gridView.setColumnProperty(storePeriodListGrid.gridView.columnByName("대체주문"), "visible", false);

        } else {
            storePeriodListGrid.gridView.setColumnProperty("대체주문", "displayWidth", 110);
        }

        //컬럼 그룹핑 너비 조정
        storePeriodListGrid.gridView.setColumnProperty("전체 주문", "displayWidth", 180);
        storePeriodListGrid.gridView.setColumnProperty("유효 주문", "displayWidth", 180);
        storePeriodListGrid.gridView.setColumnProperty("당일 전체취소", "displayWidth", 180);
        storePeriodListGrid.gridView.setColumnProperty("부분취소", "displayWidth", 180);
        storePeriodListGrid.gridView.setColumnProperty("부분반품", "displayWidth", 180);
    },
    initDataProvider : function() {
        storePeriodListGrid.dataProvider.setFields(storePeriodListBaseInfo.dataProvider.fields);
        storePeriodListGrid.dataProvider.setOptions(storePeriodListBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        storePeriodListGrid.dataProvider.clearRows();
        storePeriodListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(storePeriodListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = ($("#mallType").val() == 'DS')? storePeriodListForm.setExcelFileName("기간별 판매업체 매출 현황(E-KPI)")
            : storePeriodListForm.setExcelFileName("기간별 점포 매출 현황(E-KPI)");
        storePeriodListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * 판매현황 폼
 */
var storePeriodListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
    },
    initSearchDate : function (flag) {
        storePeriodListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        storePeriodListForm.setDate.setEndDate(0);
        storePeriodListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startYear, #endYear').val(year);
        $('#startMonth, #endMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(storePeriodListForm.search);
        $('#searchResetBtn').bindClick(storePeriodListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(storePeriodListGrid.excelDownload);


        $('#storeType').on('change', function() {
            $('#originStoreId, #storeNm').val('');
        });
    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 92) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-2, "month").format("YYYY-MM-DD"));
                return false;
            }
        }

        return true;
    },
    openStorePopup: function() {
        var storeTypeVal = $('#storeType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
    },
    setSearchData : function () {
        var searchForm = "&"+ $("form[name=searchForm]").serialize();
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#endYear").val() +'-'+ $("#endMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($("#mallType").val() == 'DS') {
            searchForm += '&searchType=D';
        }

        return searchForm;
    },
    search : function () {
        if(!storePeriodListForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = storePeriodListForm.setSearchData();

        CommonAjax.basic(
            {
                url: '/statistics/storePeriodEKPI/getStoreList.json?' + encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    storePeriodListGrid.setData(res);

                    //그룹헤더컬럼 합계
                    var stringCol = ["orderCnt", "orderAmt", "orderItemCnt", "effectiveOrderCnt", "effectiveOrderAmt", "effectiveOrderItemCnt",
                        "claimCnt", "claimAmt", "claimItemCnt", "claimPartCnt", "claimPartAmt", "claimPartItemCnt",
                        "refundPartCnt", "refundPartAmt", "refundPartItemCnt", "subOrderCnt", "subOrderAmt"];
                    SettleCommon.setRealGridHeaderSum(storePeriodListGrid.gridView, stringCol);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val() +'_'+ $("#endYear").val() + $("#endMonth").val();
        }
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            storePeriodListForm.init();
        });
    }
};

var callBack = {
    searchStore : function (res) {
        $('#originStoreId').val(res[0].storeId);
        $('#storeNm').val(res[0].storeNm);
    }
}