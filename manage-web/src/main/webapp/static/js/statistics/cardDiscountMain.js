/** Main Script */
var cardDiscountMain = {
    /**
     * init 이벤트
     */
    init: function() {
        cardDiscountMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(cardDiscountMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(cardDiscountMain.reset);
        // 이번주 버튼
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", -(new Date()).getDay()+1, "0") ;});
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "-1m", "0") ;});
        // 엑셀다운로드
        $("#excelDownloadBtn").bindClick(cardDiscountMain.excelDownload);
    },


    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!cardDiscountMain.valid()) {
            return;
        }

        var data = $("#cardDiscountSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getCardDiscountList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                cardDiscountGrid.setData(res);
            }
        });
    },

    /**
     * 상세검색
     * @param discountNo
     */
    detailSearch: function(discountNo) {
        if (!cardDiscountMain.valid()) {
            return;
        }

        if ($.jUtil.isEmpty(discountNo)) {
            return;
        } else {
            $("#schDetailDiscountNo").val(discountNo);    // 검색시 카드할인번호 설정
        }

        var data = $("#cardDiscountSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getCardDiscountDetailList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                cardDiscountDetailGrid.setData(res);
                var summaryCells = ["orderCnt", "payAmt", "discountAmt", "claimCnt", "claimAmt", "claimDiscountAmt"] ;
                cardDiscountDetailGrid.setRealGridHeaderSum(cardDiscountDetailGrid.gridView, summaryCells);
            }
        });

    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 검색어 hidden value 초기화
        $("#schDiscountNo").val("");
        $("#schDiscountNm").val("");
        $("#schDetailDiscountNo").val("");

        // 날짜 입력값 체크
        if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        }

        var schKeywordVal = $("#schKeyword").val();

        // 검색어 값 있는 경우 설정
        if($.jUtil.isNotEmpty(schKeywordVal)) {
            if ($("#schKeywordKind").val() == "schDiscountNo" ) {
                $("#schDiscountNo").val(schKeywordVal); // 카드할인번호
            } else {
                $("#schDiscountNm").val(schKeywordVal); // 카드할인명
           }
        }
        return true;
    },
    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#cardDiscountSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        // 검색어 hidden value 초기화
        $("#schDiscountNo").val("");
        $("#schDiscountNm").val("");
        $("#schDetailDiscountNo").val("");
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(cardDiscountDetailGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
           return false;
        }

        /* 히스토리 적재 */
        CommonAjax.basic({
            url         : '/escrow/statistics/cardDiscount/saveExtractHistoryForExcelDown.json',
            data        : null,
            method      : "GET"
        });

        var _date = new Date();
        var fileName =  "카드즉시할인통계" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        cardDiscountDetailGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다.",
            allColumns: true
        });
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = cardDiscountGrid.dataProvider.getJsonRow(selectRowId);
        var discountNo = rowDataJson.discountNo;
        cardDiscountMain.detailSearch(discountNo);
    }
};

/** Grid Script */
var cardDiscountGrid = {
    gridView: new RealGridJS.GridView("cardDiscountGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardDiscountGrid.initGrid();
        cardDiscountGrid.initDataProvider();
        cardDiscountGrid.event();
    },
    initGrid: function () {
        cardDiscountGrid.gridView.setDataSource(cardDiscountGrid.dataProvider);
        cardDiscountGrid.gridView.setStyles(cardDiscountGridBaseInfo.realgrid.styles);
        cardDiscountGrid.gridView.setDisplayOptions(cardDiscountGridBaseInfo.realgrid.displayOptions);
        cardDiscountGrid.gridView.setColumns(cardDiscountGridBaseInfo.realgrid.columns);
        cardDiscountGrid.gridView.setOptions(cardDiscountGridBaseInfo.realgrid.options);
        cardDiscountGrid.gridView.setCopyOptions({enabled: false})
    },
    initDataProvider: function () {
        cardDiscountGrid.dataProvider.setFields(cardDiscountGridBaseInfo.dataProvider.fields);
        cardDiscountGrid.dataProvider.setOptions(cardDiscountGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        cardDiscountGrid.dataProvider.clearRows();
        cardDiscountGrid.dataProvider.setRows(dataList);
    },
    event: function() {
        cardDiscountGrid.gridView.onDataCellClicked = function(gridView, index) {
            cardDiscountMain.gridRowSelect(index.dataRow);
        };
    },
};

/** Grid Script */
var cardDiscountDetailGrid = {
    gridView: new RealGridJS.GridView("cardDiscountDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardDiscountDetailGrid.initGrid();
        cardDiscountDetailGrid.initDataProvider();
    },
    initGrid: function () {
        cardDiscountDetailGrid.gridView.setDataSource(cardDiscountDetailGrid.dataProvider);
        cardDiscountDetailGrid.gridView.setStyles(cardDiscountDetailGridBaseInfo.realgrid.styles);
        cardDiscountDetailGrid.gridView.setDisplayOptions(cardDiscountDetailGridBaseInfo.realgrid.displayOptions);
        cardDiscountDetailGrid.gridView.setColumns(cardDiscountDetailGridBaseInfo.realgrid.columns);
        cardDiscountDetailGrid.gridView.setOptions(cardDiscountDetailGridBaseInfo.realgrid.options);
        cardDiscountDetailGrid.gridView.setCopyOptions({enabled: false})

        //헤더 merge
        var mergeCells = ["discountNo", "mallType", "discountNm", "orderDt" ];
        cardDiscountDetailGrid.setRealGridSumHeader(cardDiscountDetailGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        cardDiscountDetailGrid.dataProvider.setFields(cardDiscountDetailGridBaseInfo.dataProvider.fields);
        cardDiscountDetailGrid.dataProvider.setOptions(cardDiscountDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        cardDiscountDetailGrid.dataProvider.clearRows();
        cardDiscountDetailGrid.dataProvider.setRows(dataList);
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "data" && idx > 0){
                if (_groupColumns.includes(_gridView.getColumns()[idx].name)) {
                    _gridView.setColumnProperty(
                        _gridView.getColumns()[idx].name, "header", {
                            summary: {
                                styles: {
                                    textAlignment: "far",
                                    "numberFormat": "#,##0"
                                },
                                expression: "sum"
                            }
                        });
                }
            }
        });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[3], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[3], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};