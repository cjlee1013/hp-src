var MarketStatisticsSalesGrid = {
    gridView : new RealGridJS.GridView("MarketStatisticsSalesGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        MarketStatisticsSalesGrid.initGrid();
        MarketStatisticsSalesGrid.initDataProvider();
    },
    initGrid : function () {
        MarketStatisticsSalesGrid.gridView.setDataSource(MarketStatisticsSalesGrid.dataProvider);
        MarketStatisticsSalesGrid.gridView.setStyles(MarketStatisticsSalesBaseInfo.realgrid.styles);
        MarketStatisticsSalesGrid.gridView.setDisplayOptions(MarketStatisticsSalesBaseInfo.realgrid.displayOptions);
        MarketStatisticsSalesGrid.gridView.setColumns(MarketStatisticsSalesBaseInfo.realgrid.columns);
        MarketStatisticsSalesGrid.gridView.setOptions(MarketStatisticsSalesBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        MarketStatisticsSalesGrid.dataProvider.setFields(MarketStatisticsSalesBaseInfo.dataProvider.fields);
        MarketStatisticsSalesGrid.dataProvider.setOptions(MarketStatisticsSalesBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        MarketStatisticsSalesGrid.dataProvider.clearRows();
        MarketStatisticsSalesGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(MarketStatisticsSalesGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        MarketStatisticsSalesGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: MarketStatisticsSalesGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};

var MarketStatisticsSalesForm = {
    init : function () {
        this.initSearchDate();
        MarketStatisticsSalesForm.bindingEvent();
        CommonAjaxBlockUI.global();
    },
    initSearchDate : function () {
        MarketStatisticsSalesForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        MarketStatisticsSalesForm.setDate.setEndDate('-1d');
        MarketStatisticsSalesForm.setDate.setStartDate('-1d');
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(MarketStatisticsSalesForm.search);
        $('#clearBtn').bindClick(MarketStatisticsSalesForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(MarketStatisticsSalesGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 31) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var searchForm = "startDt="+startDt+"&endDt="+endDt;
        MarketStatisticsSalesGrid.fileName = "마켓별_판매현황_" + startDt+"_"+endDt;

        CommonAjax.basic(
        {
            url:'/statistics/market/getSales.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                MarketStatisticsSalesGrid.setData(res);
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            MarketStatisticsSalesForm.init();
        });
    }
};