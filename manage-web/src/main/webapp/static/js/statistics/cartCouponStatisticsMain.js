/**
 * 장바구니쿠폰통계
 */

// 장바구니쿠폰통계(점포별) 그리드
var cartCouponStatisticsGrid = {
    realGrid: new RealGridJS.GridView("cartCouponStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cartCouponStatisticsGrid.initGrid();
        cartCouponStatisticsGrid.initDataProvider();
        cartCouponStatisticsGrid.event();
    },
    initGrid: function () {
        cartCouponStatisticsGrid.realGrid.setDataSource(cartCouponStatisticsGrid.dataProvider);
        cartCouponStatisticsGrid.realGrid.setStyles(cartCouponStatisticsBaseInfo.realgrid.styles);
        cartCouponStatisticsGrid.realGrid.setDisplayOptions(cartCouponStatisticsBaseInfo.realgrid.displayOptions);
        cartCouponStatisticsGrid.realGrid.setColumns(cartCouponStatisticsBaseInfo.realgrid.columns);
        cartCouponStatisticsGrid.realGrid.setOptions(cartCouponStatisticsBaseInfo.realgrid.options);
        cartCouponStatisticsGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        cartCouponStatisticsGrid.dataProvider.setFields(cartCouponStatisticsBaseInfo.dataProvider.fields);
        cartCouponStatisticsGrid.dataProvider.setOptions(cartCouponStatisticsBaseInfo.dataProvider.options);
    },
    event: function () {
        cartCouponStatisticsGrid.realGrid.onDataCellClicked = function (gridView, index) {
            var data = cartCouponStatisticsGrid.dataProvider.getJsonRow(index.dataRow);
            console.log(data);
            cartCouponStatisticsMain.searchByStoreId(data);
        };
    },
    setData: function (dataList) {
        cartCouponStatisticsGrid.dataProvider.clearRows();
        cartCouponStatisticsGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        setTimeout(function () {
            if (cartCouponStatisticsGrid.realGrid.getItemCount() == 0) {
                alert("검색 결과가 없습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "장바구니쿠폰통계_점포별_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            var data = cartCouponStatisticsGrid.dataProvider.getJsonRow(0);
            console.log(data);

            cartCouponStatisticsGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다.",
                showColumns: [
                    "storeId",
                    "storeNm",
                    "useCnt",
                    "useAmt",
                    "claimCnt",
                    "claimAmt",
                    "sumAmt"
                ],
                hideColumns: ["storeType"],
                documentTitle: {
                    message: "조회기간: "+data.schFromDt+" ~ "+data.schEndDt,
                    visible: true,
                    styles: {
                        fontSize: 14,
                        textAlignment: "left"
                    }
                }
            });
        }, 1500);
    }
};

// 장바구니쿠폰통계(일자별) 그리드
var cartCouponStatisticsOrdDtGrid = {
    realGrid: new RealGridJS.GridView("cartCouponStatisticsOrdDtGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cartCouponStatisticsOrdDtGrid.initGrid();
        cartCouponStatisticsOrdDtGrid.initDataProvider();
        cartCouponStatisticsOrdDtGrid.event();
    },
    initGrid: function () {
        cartCouponStatisticsOrdDtGrid.realGrid.setDataSource(cartCouponStatisticsOrdDtGrid.dataProvider);
        cartCouponStatisticsOrdDtGrid.realGrid.setStyles(cartCouponStatisticsOrdDtBaseInfo.realgrid.styles);
        cartCouponStatisticsOrdDtGrid.realGrid.setDisplayOptions(cartCouponStatisticsOrdDtBaseInfo.realgrid.displayOptions);
        cartCouponStatisticsOrdDtGrid.realGrid.setColumns(cartCouponStatisticsOrdDtBaseInfo.realgrid.columns);
        cartCouponStatisticsOrdDtGrid.realGrid.setOptions(cartCouponStatisticsOrdDtBaseInfo.realgrid.options);
        cartCouponStatisticsOrdDtGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        cartCouponStatisticsOrdDtGrid.dataProvider.setFields(cartCouponStatisticsOrdDtBaseInfo.dataProvider.fields);
        cartCouponStatisticsOrdDtGrid.dataProvider.setOptions(cartCouponStatisticsOrdDtBaseInfo.dataProvider.options);
    },
    event: function () {
    },
    setData: function (dataList) {
        cartCouponStatisticsOrdDtGrid.dataProvider.clearRows();
        cartCouponStatisticsOrdDtGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        setTimeout(function () {
            if (cartCouponStatisticsOrdDtGrid.realGrid.getItemCount() == 0) {
                alert("검색 결과가 없습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "장바구니쿠폰통계_일자별_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            cartCouponStatisticsOrdDtGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다.",
                showColumns: [
                    "storeId",
                    "storeNm",
                    "orderDt",
                    "useCnt",
                    "useAmt",
                    "claimCnt",
                    "claimAmt",
                    "sumAmt"
                ]
            });
        }, 1500);
    }
};

var cartCouponStatisticsMain = {
    init: function () {
        cartCouponStatisticsMain.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "0");
    },
    /** event binding */
    bindEvent: function () {
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!cartCouponStatisticsMain.validSearch()) {
                return false;
            }
            cartCouponStatisticsMain.search();
        });
        // 장바구니쿠폰통계 조회 form 초기화
        $("#searchResetBtn").on("click", function () {
            cartCouponStatisticsMain.resetSearchForm();
        });

        $("#storeExcelDownBtn").on("click", function () {
            cartCouponStatisticsGrid.excelDownload();
        });
        $("#ordDtExcelDownBtn").on("click", function () {
            cartCouponStatisticsOrdDtGrid.excelDownload();
        });
        // 점포유형 변경시 쿠폰유형 제어
        $("#schStoreType").on("change", function () {
            if ($(this).val() === "EXP") {
                $("#addDsLabel").hide();
                $("input:radio[id='schCouponKindCart']").prop('checked',true);
            } else {
                $("#addDsLabel").show();
            }
        });
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 2주 초과 검색제한
        var schFromDt = $("#schFromDt").val();
        var schEndDt = $("#schEndDt").val();
        var schFromDtVal = moment(schFromDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);
        if ($.jUtil.isEmpty(schFromDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("검색일을 선택해 주세요.");
            return false;
        } else if (schEndDtVal.diff(schFromDtVal, "week", true) > 2) {
            alert("조회기간은 최대 2주 이내만 가능합니다.");
            $("#schEndDt").val(schFromDtVal.format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    /** 장바구니쿠폰통계 조회 */
    search: function () {
        var data = $("#cartCouponStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/statistics/cartcoupon/getCartCouponStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                cartCouponStatisticsGrid.setData(res);
                cartCouponStatisticsOrdDtGrid.init();
            }
        });
    },

    searchByStoreId: function (data) {
        data.schStoreType = data.storeType.toUpperCase().indexOf("EXP") > -1 ? "EXP" : "HYPER";
        data.schStoreId = data.storeId;

        CommonAjax.basic({
            url         : '/statistics/cartcoupon/getCartCouponStatisticsListByStoreId.json',
            data        : data,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                cartCouponStatisticsOrdDtGrid.setData(res);
            }
        });
    },

    /** 장바구니쿠폰통계 조회 form 초기화 */
    resetSearchForm: function () {
        $("#cartCouponStatisticsSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "0");
        $("#addDsLabel").show();
    },
    setSearchDate: function (dateTypCd) {
        if (dateTypCd === "W") {
            deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1w", "0");
        } else if (dateTypCd === "2W") {
            deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-2w", "0");
        }
    }
};

