/** Main Script */
var counselStatisticsMain = {

    initObj: function() {
        return {adv_total: "0"
            ,advisortype_one: ""
            ,advisortype_two: "합계"
            ,hb1_advisor_ing_end: "0"
            ,hb1_clame_except: "0"
            ,hb1_confirm: "0"
            ,hb2_ing_end: "0"
            ,hb2_ing_notend: "0"
            ,hb2_reception: "0"
            ,hb3_callback: "0"
            ,hb3_ing_notend: "0"
            ,hb3_reception: "0"
            ,hb3_tarns: "0"
            ,voc_yn_nm: ""}
    },

    /**
     * init 이벤트
     */
    init: function() {
        counselStatisticsMain.setAdvisorType1();
        counselStatisticsMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(counselStatisticsMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(counselStatisticsMain.reset);
        // 판매자(점포유형) SelctBox Change
        $("#hChannel").on('change', function() {counselStatisticsMain.setAdvisorType1()});
        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(counselStatisticsMain.openStorePopup);
        // 이번주 버튼
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", -(new Date()).getDay()+1, "-1") ;});
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "-1m", "-1") ;});
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(counselStatisticsMain.excelDownload);
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#hChannel').val();
        var storeType = counselStatisticsMain.getChangeStoreType(storeTypeVal);

        windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeType , "storePop", 1100, 620, "yes", "yes");
    },

    /**
     * 점포유형 설정
     */
    getChangeStoreType: function(storeType) {
        var changeType = "";

        switch (storeType) {
            case "H_CHANNEL_1" : changeType = "HYPER"; break;
            case "H_CHANNEL_2" : changeType = "CLUB"; break;
            case "H_CHANNEL_3" : changeType = "DS"; break;
            case "H_CHANNEL_4" : changeType = "EXP"; break;
            case "HYPER" : changeType = "H_CHANNEL_1"; break;
            case "CLUB" : changeType = "H_CHANNEL_2"; break;
            case "DS" : changeType = "H_CHANNEL_3"; break;
            case "EXP" : changeType = "H_CHANNEL_4"; break;
            default : changeType = "";
        }

        return changeType;
    },

    // 상담구분 SelctBox Change
    setAdvisorType1 : function() {
        var $hChannel = $('#hChannel');

        $('#advisorType1 option').each(function () {
            var $this = $(this);
            if ($.jUtil.isEmpty($this.val())) {
                return;
            }

            if($this.attr("gmcCd") == $hChannel.val()) {
                $this.show();
            } else {
                $this.hide();
            }
        });

        $("#advisorType1 option:eq(0)").prop("selected", true);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // 유효성 체크
        if (!counselStatisticsMain.valid()) {
            return;
        }

        var data = $("#counselStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/statistics/getCounselStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                var obj = counselStatisticsMain.initObj();

                var data = new Array();
                var type1 = "";
                var type2 = "";

                if(res.length > 0) {
                    for(var i = 0; i < res.length; i++) {
                        data.push(res[i]);

                        if(i == res.length-1) {
                            type1 = "temp1";
                            type2 = "temp2";
                        } else {
                            type1 = res[i].advisortype_one;
                            type2 = res[i+1].advisortype_one;
                        }

                        obj.adv_total = counselStatisticsMain.comSum(obj.adv_total,res[i].adv_total);
                        obj.hb1_advisor_ing_end = counselStatisticsMain.comSum(obj.hb1_advisor_ing_end,res[i].hb1_advisor_ing_end);
                        obj.hb1_clame_except = counselStatisticsMain.comSum(obj.hb1_clame_except,res[i].hb1_clame_except);
                        obj.hb1_confirm = counselStatisticsMain.comSum(obj.hb1_confirm,res[i].hb1_confirm);
                        obj.hb2_ing_end = counselStatisticsMain.comSum(obj.hb2_ing_end,res[i].hb2_ing_end);
                        obj.hb2_ing_notend = counselStatisticsMain.comSum(obj.hb2_ing_notend,res[i].hb2_ing_notend);
                        obj.hb2_reception = counselStatisticsMain.comSum(obj.hb2_reception,res[i].hb2_reception);
                        obj.hb3_callback = counselStatisticsMain.comSum(obj.hb3_callback,res[i].hb3_callback);
                        obj.hb3_ing_notend = counselStatisticsMain.comSum(obj.hb3_ing_notend,res[i].hb3_ing_notend);
                        obj.hb3_reception = counselStatisticsMain.comSum(obj.hb3_reception,res[i].hb3_reception);
                        obj.hb3_tarns = counselStatisticsMain.comSum(obj.hb3_tarns,res[i].hb3_tarns);

                        if(type1 != type2) {
                            data.push(obj);
                            obj = counselStatisticsMain.initObj();
                        }
                    }
                }

                counselStatisticsGrid.setData(data);
                counselStatisticsMain.setRealGridHeaderSum(counselStatisticsGrid.gridView, data);

            }
        });
    },

    comSum: function(numA, numB){
        return $.jUtil.comma(
            Number(numA.toString().replace(/,/g, ""))
            + Number(numB.toString().replace(/,/g, "")));
    },

    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var _date = new Date();
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var nowDt =  _date.getFullYear() + "-" + ("0" + (_date.getMonth() + 1)).slice(-2) + "-" + ("0" + _date.getDate()).slice(-2);
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        // 점포코드 왼쪽에 문자열 0 추가
        var schStoreIdVal = $('#schStoreId').val();
        if(!$.jUtil.isEmpty(schStoreIdVal)) {
            $('#schStoreId').val(schStoreIdVal.lpad(4,"0"));
        }

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schStartDt.diff(nowDt, "day", true) >= 0 || schEndDt.diff(nowDt, "day", true) >= 0) {
            alert("오늘 날짜로 조회가 불가합니다.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            return false;
        }

        return true;
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#counselStatisticsSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
    },

    /**
     * 엑셀 다운로드 메인
     */
    excelDownload: function() {
        if (counselStatisticsGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "상담통계_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        counselStatisticsGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    setRealGridHeaderSum : function (_gridView, data) {

        //부분합계 행 배경색 적용
        _gridView.addCellStyles([
            {id:"합계",
                background:"#11ff0000"
            }
        ]);

        for(var i = 0; i < 14; i++) {
            _gridView.setCellStyleRows(data, {"advisortype_two": i});
        }

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });

        //상단 합계 적용
        var obj = counselStatisticsMain.initObj();
        delete obj["advisortype_one"];
        delete obj["advisortype_two"];
        delete obj["adv_total"];
        delete obj["voc_yn_nm"];

        for (var i=0; i<data.length; i++) {
            if(data[i].advisortype_two != "합계") {
                obj.hb1_advisor_ing_end = counselStatisticsMain.comSum(obj.hb1_advisor_ing_end,data[i].hb1_advisor_ing_end);
                obj.hb1_clame_except = counselStatisticsMain.comSum(obj.hb1_clame_except,data[i].hb1_clame_except);
                obj.hb1_confirm = counselStatisticsMain.comSum(obj.hb1_confirm,data[i].hb1_confirm);
                obj.hb2_ing_end = counselStatisticsMain.comSum(obj.hb2_ing_end,data[i].hb2_ing_end);
                obj.hb2_ing_notend = counselStatisticsMain.comSum(obj.hb2_ing_notend,data[i].hb2_ing_notend);
                obj.hb2_reception = counselStatisticsMain.comSum(obj.hb2_reception,data[i].hb2_reception);
                obj.hb3_callback = counselStatisticsMain.comSum(obj.hb3_callback,data[i].hb3_callback);
                obj.hb3_ing_notend = counselStatisticsMain.comSum(obj.hb3_ing_notend,data[i].hb3_ing_notend);
                obj.hb3_reception = counselStatisticsMain.comSum(obj.hb3_reception,data[i].hb3_reception);
                obj.hb3_tarns = counselStatisticsMain.comSum(obj.hb3_tarns,data[i].hb3_tarns);
            }
        }

        for(var col in obj) {
            _gridView.setColumnProperty(col, "header",{
                summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:obj[col] } });
        }
    }
};

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    }
};

/** Grid Script */
var counselStatisticsGrid = {
    gridView: new RealGridJS.GridView("counselStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        counselStatisticsGrid.initGrid();
        counselStatisticsGrid.initDataProvider();
        counselStatisticsGrid.event();
    },
    initGrid: function () {
        counselStatisticsGrid.gridView.setDataSource(counselStatisticsGrid.dataProvider);
        counselStatisticsGrid.gridView.setStyles(counselStatisticsGridBaseInfo.realgrid.styles);
        counselStatisticsGrid.gridView.setDisplayOptions(counselStatisticsGridBaseInfo.realgrid.displayOptions);
        counselStatisticsGrid.gridView.setColumns(counselStatisticsGridBaseInfo.realgrid.columns);
        counselStatisticsGrid.gridView.setOptions(counselStatisticsGridBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = ["advisortype_one", "advisortype_two","voc_yn_nm","adv_total"];
        SettleCommon.setRealGridSumHeader(counselStatisticsGrid.gridView, mergeCells);

        counselStatisticsGrid.gridView.setColumnProperty("점포", "displayWidth", 210);
        counselStatisticsGrid.gridView.setColumnProperty("콜센터", "displayWidth", 280);
        counselStatisticsGrid.gridView.setColumnProperty("본사", "displayWidth", 210);

        var sameDataMerge = ['advisortype_one'];
        orderUtil.setRealGridMerge(counselStatisticsGrid, sameDataMerge, "mergeRule");
    },
    initDataProvider: function () {
        counselStatisticsGrid.dataProvider.setFields(counselStatisticsGridBaseInfo.dataProvider.fields);
        counselStatisticsGrid.dataProvider.setOptions(counselStatisticsGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        counselStatisticsGrid.dataProvider.clearRows();
        counselStatisticsGrid.dataProvider.setRows(dataList);
    },
    event: function() {
    }
};