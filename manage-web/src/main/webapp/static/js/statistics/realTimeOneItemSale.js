/** 실시간 점포 상품 판매 현황 Main */
var realTimeOneItemSaleMng = {
    /**
     * init 이벤트
     */
    init: function () {
        realTimeOneItemSaleMng.bindEvent();
        realTimeOneItemSaleMng.initSearchDate();
    },
    /**
     * 조회일자 초기화
     */
    initSearchDate : function () {
        var defaultTime = new Date();
        realTimeOneItemSaleMng.setDate = CalendarTime.datePickerRange('schStartDt', 'schEndDt', {timeFormat:"HH:mm"});
        realTimeOneItemSaleMng.setDate.setEndDate(moment(defaultTime).format('YYYY-MM-DD HH:mm'));
        realTimeOneItemSaleMng.setDate.setStartDate(moment(defaultTime).format('YYYY-MM-DD')+ " 00:00");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 상품번호 입력시 패턴체크
        $("#schItemNo").on("keyup", function() {
            var schItemNo = $("#schItemNo").val();
            if (!$.jUtil.isEmpty(schItemNo)) {
                let pattern = /[^0-9,]/g;
                if (pattern.test(schItemNo)) {
                    alert('숫자 또는 ","만 입력하세요.');
                    $("#schItemNo").val("").focus();
                }
            }
        });

        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!realTimeOneItemSaleMng.validSearch()) {
                return false;
            }
            realTimeOneItemSaleMng.search();
        });

        // 초기화 버튼
        $("#resetBtn").bindClick(realTimeOneItemSaleMng.resetInfo);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStoreType = $("#schStoreType").val();
        var schItemNo = $("#schItemNo").val();

        if ($.jUtil.isEmpty(schStartDt)) {
            alert("주문일시을 선택해 주세요.");
            return false;
        }

        if ($.jUtil.isEmpty(schEndDt)) {
            alert("주문일시을 선택해 주세요.");
            return false;
        }
        console.log(moment(schEndDt).diff(schStartDt, "day", true));
        if (moment(schEndDt).diff(schStartDt, "day", true) > 1) {
            alert("24시간 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment(schEndDt).add(-1, "day").format("YYYY-MM-DD HH:mm"));
            return false;
        }

        if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해 주세요.");
            return false;
        }

        if ($.jUtil.isEmpty(schItemNo)) {
            alert("상품번호를 입력하세요.");
            return false;
        }
        else {
            $("#schItemNo").val(schItemNo.replace(/(?:\r\n|\r|\n)/g, ''));
            let pattern = /[^0-9,]/g;
            if(pattern.test(schItemNo)) {
                alert('숫자 또는 ","만 입력하세요.');
                $("#schItemNo").focus();
                return false;
            }
        }

        return true;
    },
    /**
     * 실시간 특정 상품 판매현황 리스트 조회
     */
    search: function () {
        var data = $("#realTimeOneItemSaleSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getRealTimeOneItemSaleList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                realTimeOneItemSaleGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#realTimeOneItemSaleSearchForm").resetForm();
        realTimeOneItemSaleMng.initSearchDate();
    }
};

/** Grid Script */
var realTimeOneItemSaleGrid = {
    gridView: new RealGridJS.GridView("realTimeOneItemSaleGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        realTimeOneItemSaleGrid.initGrid();
        realTimeOneItemSaleGrid.initDataProvider();
        realTimeOneItemSaleGrid.event();
    },
    initGrid: function () {
        realTimeOneItemSaleGrid.gridView.setDataSource(realTimeOneItemSaleGrid.dataProvider);
        realTimeOneItemSaleGrid.gridView.setStyles(realTimeOneItemSaleGridBaseInfo.realgrid.styles);
        realTimeOneItemSaleGrid.gridView.setDisplayOptions(realTimeOneItemSaleGridBaseInfo.realgrid.displayOptions);
        realTimeOneItemSaleGrid.gridView.setColumns(realTimeOneItemSaleGridBaseInfo.realgrid.columns);
        realTimeOneItemSaleGrid.gridView.setOptions(realTimeOneItemSaleGridBaseInfo.realgrid.options);
        realTimeOneItemSaleGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        realTimeOneItemSaleGrid.dataProvider.setFields(realTimeOneItemSaleGridBaseInfo.dataProvider.fields);
        realTimeOneItemSaleGrid.dataProvider.setOptions(realTimeOneItemSaleGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        realTimeOneItemSaleGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            if ($.jUtil.isNotEmpty(itemNo)) {
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            }
        };
    },
    setData: function (dataList) {
        realTimeOneItemSaleGrid.dataProvider.clearRows();
        realTimeOneItemSaleGrid.dataProvider.setRows(dataList);
    }
};