/** Main Script */
var couponPeriodMain = {
    global : {
      selectedCouponNo : 0
    },
    /**
     * init 이벤트
     */
    init: function() {
        couponPeriodMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
        couponPeriodMain.setDiscountKind();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(couponPeriodMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(couponPeriodMain.reset);
        // 점포유형 SelctBox Change
        $("#schSiteType").on('change', function() { couponPeriodMain.setDiscountKind() });
        // 쿠폰유형 SelctBox Change
        $("#schDiscountKind").on('change', function() { couponPeriodMain.setMarketType() });
        // 이번주 버튼
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", -(new Date()).getDay()+1, "-1") ;});
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "-1m", "-1") ;});
        // 엑셀다운로드 메인
        $("#excelDownloadMainBtn").bindClick(couponPeriodMain.excelDownloadMain);
        // 엑셀다운로드 상세
        $("#excelDownloadDetailBtn").bindClick(couponPeriodMain.excelDownloadDetail);
    },

    /**
     * 주문유형 설정
     */
    setDiscountKind: function() {
        var siteType = $('#schSiteType').val();
        var htmlStr = "";

        if (siteType == "DS") {
            htmlStr += '<option value="item_coupon"">상품</option>';
            htmlStr += '<option value="ship_coupon"">배송비</option>';
            htmlStr += '<option value="add_ds_coupon"">중복쿠폰</option>';
        } else {
            htmlStr += '<option value="cart_coupon">장바구니</option>';
            htmlStr += '<option value="item_coupon">상품</option>';
            htmlStr += '<option value="ship_coupon">배송비</option>';
            htmlStr += '<option value="item_discount">상품할인</option>';
            htmlStr += '<option value="add_td_coupon">중복쿠폰</option>';
        }

        $("#schDiscountKind").html(htmlStr);
    },

    /**
     * 마켓유형 설정
     */
    setMarketType: function() {
        var $schDiscountKind = $("#schDiscountKind");
        var $schMarketType = $("#schMarketType");

        if($schDiscountKind.val() == "item_discount") {
            $schMarketType.show();
        } else {
            $schMarketType.hide();
        }

        $("#schMarketType option:eq(0)").prop("selected", true);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // 유효성 체크
        if (!couponPeriodMain.valid()) {
            return;
        }

        var data = $("#couponPeriodSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getCouponPeriodStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                couponPeriodGrid.setData(res);
                couponPeriodDetailGrid.setData();
            }
        });
    },

    /**
     * 상세검색
     * @param couponNo 쿠폰번호
     * @param couponKind 쿠폰종류
     */
    detailSearch: function(rowDataJson) {
        var couponNo = rowDataJson.couponNo;
        var siteType = rowDataJson.siteType;
        var discountKind = rowDataJson.discountKind;
        var marketType = rowDataJson.marketType;
        var barcode = rowDataJson.barcode;

        if (!couponPeriodMain.valid()) {
            return;
        }

        var reqData = new Object();

        reqData.schStartDt = $("#schStartDt").val().replace(/-/gi, "");
        reqData.schEndDt = $("#schEndDt").val().replace(/-/gi, "");
        reqData.schCouponNo = couponNo;
        reqData.schSiteType = siteType;
        reqData.schDiscountKind = discountKind;
        reqData.marketType = marketType;
        reqData.schBarcode = barcode;

        CommonAjax.basic({
            url         : '/escrow/statistics/getCouponPeriodStatisticsDetailList.json',
            data        : JSON.stringify(reqData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                couponPeriodDetailGrid.setData(res);
                var summaryCells = ["purchaseCnt", "completeAmt", "discountAmt"] ;
                couponPeriodDetailGrid.setRealGridHeaderSum(couponPeriodDetailGrid.gridView, summaryCells);

                couponPeriodMain.global.selectedCouponNo = couponNo;
            }
        });

    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        //  검색조건
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'schCouponNo')) {
            return true;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var _date = new Date();
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var nowDt =  _date.getFullYear() + "-" + ("0" + (_date.getMonth() + 1)).slice(-2) + "-" + ("0" + _date.getDate()).slice(-2);
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schStartDt.diff(nowDt, "day", true) >= 0 || schEndDt.diff(nowDt, "day", true) >= 0) {
            alert("오늘 날짜로 조회가 불가합니다.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            return false;
        }

        return true;
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#couponPeriodSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1", "-1");
    },

    /**
     * 엑셀 다운로드 메인
     */
    excelDownloadMain: function() {
        if (couponPeriodGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "기간별쿠폰통계_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        couponPeriodGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 엑셀 다운로드 상세
     */
    excelDownloadDetail: function() {
        if (couponPeriodDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "기간별쿠폰통계점포별_" + couponPeriodMain.global.selectedCouponNo + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        couponPeriodDetailGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },


    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = couponPeriodGrid.dataProvider.getJsonRow(selectRowId);
        couponPeriodMain.detailSearch(rowDataJson);
    }
};

/** Grid Script */
var couponPeriodGrid = {
    gridView: new RealGridJS.GridView("couponPeriodGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        couponPeriodGrid.initGrid();
        couponPeriodGrid.initDataProvider();
        couponPeriodGrid.event();
    },
    initGrid: function () {
        couponPeriodGrid.gridView.setDataSource(couponPeriodGrid.dataProvider);
        couponPeriodGrid.gridView.setStyles(couponPeriodGridBaseInfo.realgrid.styles);
        couponPeriodGrid.gridView.setDisplayOptions(couponPeriodGridBaseInfo.realgrid.displayOptions);
        couponPeriodGrid.gridView.setColumns(couponPeriodGridBaseInfo.realgrid.columns);
        couponPeriodGrid.gridView.setOptions(couponPeriodGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        couponPeriodGrid.dataProvider.setFields(couponPeriodGridBaseInfo.dataProvider.fields);
        couponPeriodGrid.dataProvider.setOptions(couponPeriodGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        couponPeriodGrid.dataProvider.clearRows();
        couponPeriodGrid.dataProvider.setRows(dataList);
    },
    event: function() {
        couponPeriodGrid.gridView.onDataCellClicked = function(gridView, index) {
            couponPeriodMain.gridRowSelect(index.dataRow);
        };
    },
};

/** Grid Script */
var couponPeriodDetailGrid = {
    gridView: new RealGridJS.GridView("couponPeriodDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        couponPeriodDetailGrid.initGrid();
        couponPeriodDetailGrid.initDataProvider();
    },
    initGrid: function () {
        couponPeriodDetailGrid.gridView.setDataSource(couponPeriodDetailGrid.dataProvider);
        couponPeriodDetailGrid.gridView.setStyles(couponPeriodDetailGridBaseInfo.realgrid.styles);
        couponPeriodDetailGrid.gridView.setDisplayOptions(couponPeriodDetailGridBaseInfo.realgrid.displayOptions);
        couponPeriodDetailGrid.gridView.setColumns(couponPeriodDetailGridBaseInfo.realgrid.columns);
        couponPeriodDetailGrid.gridView.setOptions(couponPeriodDetailGridBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = ["storeId","storeNm"];
        couponPeriodDetailGrid.setRealGridSumHeader(couponPeriodDetailGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        couponPeriodDetailGrid.dataProvider.setFields(couponPeriodDetailGridBaseInfo.dataProvider.fields);
        couponPeriodDetailGrid.dataProvider.setOptions(couponPeriodDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        couponPeriodDetailGrid.dataProvider.clearRows();
        couponPeriodDetailGrid.dataProvider.setRows(dataList);
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "data" && idx > 0){
                if (_groupColumns.includes(_gridView.getColumns()[idx].name)) {
                    _gridView.setColumnProperty(
                        _gridView.getColumns()[idx].name, "header", {
                            summary: {
                                styles: {
                                    textAlignment: "far",
                                    "numberFormat": "#,##0"
                                },
                                expression: "sum"
                            }
                        });
                }
            }
        });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};