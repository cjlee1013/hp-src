/**
 * 통계 > 주문결제통계 > 주문당 평균 주문 상품수
 */
$(document).ready(function() {
  averageOrderItemQty.init();
  CommonAjaxBlockUI.global();
});

// 클레임관리
var averageOrderItemQty = {

    /**
    * 초기화
    */
    init : function() {
        this.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "0");
        this.initSearchYM();
        averageItemQtyListGrid.init();

    },
    initSearchDate : function (flag) {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        //[대체여부] 선택
        $('#schSubstitutionYn').bindChange(averageOrderItemQty.chgSubstitutionYn);
        // [검색] 버튼
        $("#searchBtn").bindClick(averageOrderItemQty.search);
        // [초기화] 버튼
        $("#resetBtn").bindClick(averageOrderItemQty.reset);
        // [엑셀다운로드] 버튼
        $('#excelDownloadBtn').bindClick(averageItemQtyListGrid.excelDownload);
    },

    /**
     * 조회 검색
     */
    search : function() {

        var startDt;
        var endDt;

        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).endOf('month');

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
        }
        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");

        var data = {
            "schStartDt" : startDt,
            "schEndDt" : endDt,
            "schStoreType" : $('#schStoreType').val(),
            "schMallType" : $('#schMallType').val()
        }

        CommonAjax.basic(
            {
              url:'/statistics/order/getAverageOrderItemQtyList.json',
              data: JSON.stringify(data),
              contentType: 'application/json',
              method: "POST",
              successMsg: null,
              callbackFunc:function(res) {
                averageItemQtyListGrid.setData(res);
                $('#averageItemQtyListTotalCount').html($.jUtil.comma(averageItemQtyListGrid.gridView.getItemCount()));
                var stringCol = [ "averageItemCnt", "averageItemQty"];

                averageItemQtyListGrid.setRealGridHeaderSum(averageItemQtyListGrid.gridView, stringCol);

              }
            });
    },

    /**
     * 검색폼 초기화
     */
    reset : function() {
        $("input[name='dateType'][value='D']").prop('checked',true);
        $('#schShiftId').val('ALL');
        $('#schStoreType').val('HYPER');
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "0");
        averageOrderItemQty.initSearchYearMonth();
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startYear, #endYear').val(year);
        averageOrderItemQty.setMonth($('#startYear')[0],'startMonth');
        averageOrderItemQty.setMonth($('#endYear')[0],'endMonth');
        $('#startMonth, #endMonth').val(month);
    },

    initSearchYearMonth : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },

    setMonth : function(_year,_id) {
        var nowDate = new Date();
        var maxMonth;
        if (_year.value == nowDate.getFullYear()) {
            maxMonth = nowDate.getMonth();
        } else {
            maxMonth = 11;
        }
        $('#'+_id).empty();
        for (var i=1;i<=maxMonth+1;i++) {
            if (i<10) {
                $('#'+_id).append("<option value='0" +i+ "'>0" + i + " 월</option>");
            } else  {
                $('#'+_id).append("<option value='" +i+ "'>" + i + " 월</option>");
            }
        }
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = averageItemQtyListGrid.dataProvider.getJsonRow(selectRowId);
        UIUtil.addTabWindow('/claim/info/getClaimDetailInfoTab?purchaseOrderNo='
            + rowDataJson.purchaseOrderNo + "&claimNo=" + rowDataJson.claimNo
            + "&claimBundleNo=" + rowDataJson.claimBundleNo + "&claimType="+ rowDataJson.claimTypeCode,
            'claimDetailTab', '클레임정보상세', '135px')
    }
};

// 클레임관리 그리드
var averageItemQtyListGrid = {
    gridView : new RealGridJS.GridView("averageItemQtyListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
      averageItemQtyListGrid.initGrid();
      averageItemQtyListGrid.initDataProvider();
      averageItemQtyListGrid.event();
    },
    initGrid : function() {
      averageItemQtyListGrid.gridView.setDataSource(averageItemQtyListGrid.dataProvider);

      averageItemQtyListGrid.gridView.setStyles(averageItemQtyListGridBaseInfo.realgrid.styles);
      averageItemQtyListGrid.gridView.setDisplayOptions(averageItemQtyListGridBaseInfo.realgrid.displayOptions);
      averageItemQtyListGrid.gridView.setColumns(averageItemQtyListGridBaseInfo.realgrid.columns);
      averageItemQtyListGrid.gridView.setOptions(averageItemQtyListGridBaseInfo.realgrid.options);
      var mergeCells = [["storeNm", "storeId"]];
      averageItemQtyListGrid.setRealGridSumHeader(averageItemQtyListGrid.gridView, mergeCells);

    },
    initDataProvider : function() {
      averageItemQtyListGrid.dataProvider.setFields(averageItemQtyListGridBaseInfo.dataProvider.fields);
      averageItemQtyListGrid.dataProvider.setOptions(averageItemQtyListGridBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
      averageItemQtyListGrid.dataProvider.clearRows();
      averageItemQtyListGrid.dataProvider.setRows(dataList);
    },

    excelDownload: function() {
      if(averageItemQtyListGrid.gridView.getItemCount() == 0) {
        alert("검색 결과가 없습니다.");
        return false;
      }

      var _date = new Date();
      var fileName =  "주문당 평균 주문 상품수_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

      averageItemQtyListGrid.gridView.exportGrid({
        type: "excel",
        target: "local",
        fileName: fileName + ".xlsx",
        showProgress: true,
        progressMessage: "엑셀 Export중입니다."
      });
    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "center", "numberFormat": "#.##"},
                    expression: "avg"}
            });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
};