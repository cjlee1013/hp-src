/** Main Script */
var couponMain = {
    /**
     * init 이벤트
     */
    init: function() {
        couponMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(couponMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(couponMain.reset);
        // 이번주 버튼
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", -(new Date()).getDay()+1, "0") ;});
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "-1m", "0") ;});
        // 엑셀다운로드
        $("#excelDownloadBtn").bindClick(couponMain.excelDownload);
    },


    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!couponMain.valid()) {
            return;
        }

        var data = $("#couponSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getCouponStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                couponGrid.setData(res);
            }
        });
    },

    /**
     * 상세검색
     * @param couponNo 쿠폰번호
     * @param couponKind 쿠폰종류
     */
    detailSearch: function(couponNo, couponKind, couponNm) {
        if (!couponMain.valid()) {
            return;
        }

        if ($.jUtil.isEmpty(couponNo) || $.jUtil.isEmpty(couponKind)) {
            return;
        } else {
            $("#schCouponNm").val(couponNm);
            $("#schDetailCouponNo").val(couponNo);
            $("#schDetailCouponKind").val(couponKind);
        }

        var url;
        if (couponKind === "item_coupon" || couponKind === "item_discount") {
            url = '/escrow/statistics/getItemCouponStatisticsDetailList.json?';
        } else {
            url = '/escrow/statistics/getCartCouponStatisticsDetailList.json?';
        }
        var data = $("#couponSearchForm").serialize();
        CommonAjax.basic({
            url         : url + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                couponDetailGrid.setData(res);
                var summaryCells = ["orderCnt", "orderAmt", "discountAmt", "couponUseCnt", "claimCnt", "claimDiscountAmt"] ;
                couponDetailGrid.setRealGridHeaderSum(couponDetailGrid.gridView, summaryCells);
            }
        });

    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 검색어 hidden value 초기화
        $("#schCouponNo").val("");
        $("#schCouponNm").val("");
        $("#schDetailCouponNo").val("");
        $("#schDetailCouponKind").val("");

        // 날짜 입력값 체크
        if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid", "overOneWeekRange"])) {
            return false;
        }

        var schKeywordVal = $("#schKeyword").val();

        // 검색어 값 있는 경우 설정
        if($.jUtil.isNotEmpty(schKeywordVal)) {
            if ($("#schKeywordKind").val() == "schCouponNo" ) {
                $("#schCouponNo").val(schKeywordVal); // 쿠폰번호
            } else {
                $("#schCouponNm").val(schKeywordVal); // 쿠폰명
            }
        }
        return true;
    },
    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#couponSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        // 검색어 hidden value 초기화
        $("#schCouponNo").val("");
        $("#schCouponNm").val("");
        $("#schDetailCouponNo").val("");
        $("#schDetailCouponKind").val("");
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(couponDetailGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
           return false;
        }

        var couponKind = $("#schDetailCouponKind").val();
        if (couponKind === "item_coupon" || couponKind === "item_discount") {
            couponKind = "item";
        } else {
            couponKind = "cart";
        }

        /* 히스토리 적재 */
        CommonAjax.basic({
            url         : '/escrow/statistics/coupon/saveExtractHistoryForExcelDown.json?couponKind=' + couponKind,
            data        : null,
            method      : "GET"
        });

        var _date = new Date();
        var fileName =  "쿠폰통계" + "_" + $("#schDetailCouponNo").val() + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        couponDetailGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다.",
            allColumns: true
        });
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = couponGrid.dataProvider.getJsonRow(selectRowId);
        var couponNo = rowDataJson.couponNo;
        var couponKind = rowDataJson.couponKind;
        var couponNm = rowDataJson.couponNm;
        couponMain.detailSearch(couponNo, couponKind, couponNm);
    }
};

/** Grid Script */
var couponGrid = {
    gridView: new RealGridJS.GridView("couponGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        couponGrid.initGrid();
        couponGrid.initDataProvider();
        couponGrid.event();
    },
    initGrid: function () {
        couponGrid.gridView.setDataSource(couponGrid.dataProvider);
        couponGrid.gridView.setStyles(couponGridBaseInfo.realgrid.styles);
        couponGrid.gridView.setDisplayOptions(couponGridBaseInfo.realgrid.displayOptions);
        couponGrid.gridView.setColumns(couponGridBaseInfo.realgrid.columns);
        couponGrid.gridView.setOptions(couponGridBaseInfo.realgrid.options);
        couponGrid.gridView.setCopyOptions({enabled: false})
    },
    initDataProvider: function () {
        couponGrid.dataProvider.setFields(couponGridBaseInfo.dataProvider.fields);
        couponGrid.dataProvider.setOptions(couponGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        couponGrid.dataProvider.clearRows();
        couponGrid.dataProvider.setRows(dataList);
    },
    event: function() {
        couponGrid.gridView.onDataCellClicked = function(gridView, index) {
            couponMain.gridRowSelect(index.dataRow);
        };
    },
};

/** Grid Script */
var couponDetailGrid = {
    gridView: new RealGridJS.GridView("couponDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        couponDetailGrid.initGrid();
        couponDetailGrid.initDataProvider();
    },
    initGrid: function () {
        couponDetailGrid.gridView.setDataSource(couponDetailGrid.dataProvider);
        couponDetailGrid.gridView.setStyles(couponDetailGridBaseInfo.realgrid.styles);
        couponDetailGrid.gridView.setDisplayOptions(couponDetailGridBaseInfo.realgrid.displayOptions);
        couponDetailGrid.gridView.setColumns(couponDetailGridBaseInfo.realgrid.columns);
        couponDetailGrid.gridView.setOptions(couponDetailGridBaseInfo.realgrid.options);
        couponDetailGrid.gridView.setCopyOptions({enabled: false})

        //헤더 merge
        var mergeCells = ["storeNm", "couponNm", "itemNo", "lcateNm", "mcateNm" ];
        couponDetailGrid.setRealGridSumHeader(couponDetailGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        couponDetailGrid.dataProvider.setFields(couponDetailGridBaseInfo.dataProvider.fields);
        couponDetailGrid.dataProvider.setOptions(couponDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        couponDetailGrid.dataProvider.clearRows();
        couponDetailGrid.dataProvider.setRows(dataList);
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "data" && idx > 0){
                if (_groupColumns.includes(_gridView.getColumns()[idx].name)) {
                    _gridView.setColumnProperty(
                        _gridView.getColumns()[idx].name, "header", {
                            summary: {
                                styles: {
                                    textAlignment: "far",
                                    "numberFormat": "#,##0"
                                },
                                expression: "sum"
                            }
                        });
                }
            }
        });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};