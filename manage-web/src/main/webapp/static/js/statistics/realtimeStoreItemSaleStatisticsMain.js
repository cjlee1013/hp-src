/** 실시간 점포 상품 판매 현황 Main */
var realtimeStoreItemSaleStatisticsMng = {
    /**
     * init 이벤트
     */
    init: function () {
        realtimeStoreItemSaleStatisticsMng.bindEvent();
        realtimeStoreItemSaleStatisticsMng.initDate();
        deliveryCore_category.initCategory();
        $("#schGroupStd").prop("disabled", true);
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        realtimeStoreItemSaleStatisticsMng.setDate = Calendar.datePicker("schBasicDt");
        realtimeStoreItemSaleStatisticsMng.setDate.setDate(0);
        $("#schTodayYn").val("Y");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/realtimeStoreItemSaleStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 460);
        });
        // 조회일자 변경시
        $("#schBasicDt").on("change", function() {
            var todayDate = $.datepicker.formatDate('yy-mm-dd', new Date());
            if (todayDate === $("#schBasicDt").val()) {
                $("#schTodayYn").val("Y");
            } else {
                $("#schTodayYn").val("N");
            }
        });
        // 상품번호 입력시 패턴체크
        $("#schItemNoList").on("keyup", function() {
            var schItemNoList = $("#schItemNoList").val();
            if (!$.jUtil.isEmpty(schItemNoList)) {
                let pattern = /[^0-9,]/g;
                if (pattern.test(schItemNoList)) {
                    alert('숫자 또는 ","만 입력하세요.');
                    $("#schItemNoList").val("").focus();
                }
                var itemNoArr = schItemNoList.split(',');
                if (itemNoArr.length > 50) {
                    alert('최대 50개 까지만 입력하세요.');
                    $("#schItemNoList").val("").focus();
                }
            }
        });
        // 마켓구분 변경시
        $("#schMarketType").on("change", function () {
            var marketType = $("#schMarketType").val();
            var storeId = $("#schStoreId").val();
            if ($.jUtil.isEmpty(storeId) && $.jUtil.isNotEmpty(marketType) && marketType !== "ALL" && marketType !== "EXCEPT") {
                $("#schGroupStd").prop("disabled", false);
            } else {
                $("#schGroupStd").prop("disabled", true);
            }
        });
        // 점포ID 선택시
        $("#schStoreId").on('input', function() {
            $("#schGroupStd").prop("disabled", true);
        });
        (function ($) {
            var originalVal = $.fn.val;
            $.fn.val = function (value) {
                var res = originalVal.apply(this, arguments);
                if (this.is('input:text') && arguments.length >= 1) {
                    this.trigger("input");
                }
                return res;
            }
        })(jQuery);
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!realtimeStoreItemSaleStatisticsMng.validSearch()) {
                return false;
            }
            realtimeStoreItemSaleStatisticsMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(realtimeStoreItemSaleStatisticsMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(realtimeStoreItemSaleStatisticsMng.excelDownload);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schBasicDt = $("#schBasicDt").val();
        var schStoreType = $("#schStoreType").val();
        var schItemNoList = $("#schItemNoList").val();
        if ($.jUtil.isEmpty(schBasicDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해 주세요.");
            return false;
        }
        if(!$.jUtil.isEmpty(schItemNoList)) {
            $("#schItemNoList").val(schItemNoList.replace(/(?:\r\n|\r|\n)/g, ''));
            let pattern = /[^0-9,]/g;
            if(pattern.test(schItemNoList)) {
                alert('숫자 또는 ","만 입력하세요.');
                $("#schItemNoList").focus();
                return false;
            }
        }
        return true;
    },
    /**
     * 통계주문정보 리스트 조회
     */
    search: function () {
        var data = $("#realtimeStoreItemSaleStatisticsSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/statistics/getRealtimeStoreItemSaleStatisticsList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                realtimeStoreItemSaleStatisticsGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#realtimeStoreItemSaleStatisticsSearchForm").resetForm();
        realtimeStoreItemSaleStatisticsMng.initDate();
        $("#schGroupStd").prop("disabled", true);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(realtimeStoreItemSaleStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "실시간점포상품판매현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        // 추출이력 저장
        realtimeStoreItemSaleStatisticsMng.saveExtractHistory();
        realtimeStoreItemSaleStatisticsGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/statistics/saveRealtimeStoreItemSaleExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var realtimeStoreItemSaleStatisticsGrid = {
    gridView: new RealGridJS.GridView("realtimeStoreItemSaleStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        realtimeStoreItemSaleStatisticsGrid.initGrid();
        realtimeStoreItemSaleStatisticsGrid.initDataProvider();
        realtimeStoreItemSaleStatisticsGrid.event();
    },
    initGrid: function () {
        realtimeStoreItemSaleStatisticsGrid.gridView.setDataSource(realtimeStoreItemSaleStatisticsGrid.dataProvider);
        realtimeStoreItemSaleStatisticsGrid.gridView.setStyles(realtimeStoreItemSaleStatisticsGridBaseInfo.realgrid.styles);
        realtimeStoreItemSaleStatisticsGrid.gridView.setDisplayOptions(realtimeStoreItemSaleStatisticsGridBaseInfo.realgrid.displayOptions);
        realtimeStoreItemSaleStatisticsGrid.gridView.setColumns(realtimeStoreItemSaleStatisticsGridBaseInfo.realgrid.columns);
        realtimeStoreItemSaleStatisticsGrid.gridView.setOptions(realtimeStoreItemSaleStatisticsGridBaseInfo.realgrid.options);
        realtimeStoreItemSaleStatisticsGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        realtimeStoreItemSaleStatisticsGrid.dataProvider.setFields(realtimeStoreItemSaleStatisticsGridBaseInfo.dataProvider.fields);
        realtimeStoreItemSaleStatisticsGrid.dataProvider.setOptions(realtimeStoreItemSaleStatisticsGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        realtimeStoreItemSaleStatisticsGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            if ($.jUtil.isNotEmpty(itemNo)) {
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            }
        };
    },
    setData: function (dataList) {
        realtimeStoreItemSaleStatisticsGrid.dataProvider.clearRows();
        realtimeStoreItemSaleStatisticsGrid.dataProvider.setRows(dataList);
    }
};