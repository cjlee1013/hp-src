/** 대체공제 쿠폰 현황 Main */
$(document).ready(function() {
    orderSubstitutionStatisticsMain.init();
    CommonAjaxBlockUI.global();
});

var orderSubstitutionStatisticsMain = {
    /**
     * init 이벤트
     */
    init: function () {
        this.initGrid();
        this.bindEvent();
        this.initDate();
    },
    initGrid : function () {
        orderSubstitutionStatisticsGrid.init();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!orderSubstitutionStatisticsMain.validSearch()) {
                return false;
            }
            orderSubstitutionStatisticsMain.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(orderSubstitutionStatisticsMain.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(orderSubstitutionStatisticsMain.excelDownload);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);
        var isCheckable = true;
        if (schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }
        if (schEndDt.diff(schStartDt, "week", true) > 1) {
            alert("조회기간은 최대 1주일까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "week").format("YYYY-MM-DD"));
            return false;
        }
        var schStoreType = $("#schStoreType").val();
        if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 통계주문정보 리스트 조회
     */
    search: function () {
        var data = $("form[name=orderSubstitutionStatisticsSearchForm]").serializeObject();
        CommonAjax.basic({
            url         : '/statistics/promotion/getOrderSubstitutionInfo.json',
            data        : JSON.stringify(data),
            contentType : 'application/json',
            method      : "POST",
            callbackFunc: function (dataList) {
                orderSubstitutionStatisticsGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#orderSubstitutionStatisticsMainSearchForm").resetForm();
        orderSubstitutionStatisticsMain.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(orderSubstitutionStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "대체공제쿠폰통계" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        // 추출이력 저장
        orderSubstitutionStatisticsGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    }
};