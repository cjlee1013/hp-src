var zipcodeOrderStatisticsMain = {
    /**
     * init 이벤트
     */
    init: function() {
        zipcodeOrderStatisticsMain.bindingEvent();
        zipcodeOrderStatisticsMain.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    bindingEvent: function() {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/zipcodeOrderStatisticsHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 290);
        });
        // 상단 검색영역 검색
        $("#schBtn").bindClick(zipcodeOrderStatisticsMain.search);
        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(zipcodeOrderStatisticsMain.reset,"search");
        // 상단 엑셀다운로드
        $("#excelDownloadBtn").bindClick(zipcodeOrderStatisticsGrid.excelDownload);
    },

    search: function() {
        if (!zipcodeOrderStatisticsMain.valid()) {
            return;
        }

        CommonAjax.basic({
            url         : '/statistics/getZipcodeOrderStatisticsList.json',
            data        : _F.getFormDataByJson($("#zipcodeOrderStatisticsSearchForm")),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function(res) {
                zipcodeOrderStatisticsGrid.setData(res);
                $('#zipcodeOrderStatisticsSearchCount').html($.jUtil.comma(zipcodeOrderStatisticsGrid.gridView.getItemCount()));
                let headerCells = ["orderCnt", "orderPrice", "shift1OrderCnt", "shift1OrderPrice", "shift2OrderCnt", "shift2OrderPrice",
                    "shift3OrderCnt", "shift3OrderPrice", "shift4OrderCnt", "shift4OrderPrice", "shift5OrderCnt", "shift5OrderPrice",
                    "shift6OrderCnt", "shift6OrderPrice", "shift7OrderCnt", "shift7OrderPrice", "shift8OrderCnt", "shift8OrderPrice",
                    "shift9OrderCnt", "shift9OrderPrice"];
                zipcodeOrderStatisticsGrid.setGridSumData(zipcodeOrderStatisticsGrid.gridView, headerCells);
            }
        });
    },

    reset: function() {
        $("#zipcodeOrderStatisticsSearchForm").resetForm();
        zipcodeOrderStatisticsMain.initDate();
        zipcodeOrderStatisticsGrid.dataProvider.clearRows();
        $("#zipcodeOrderStatisticsSearchCount").text("0");
    },

    valid: function() {
        var schStartDtVal = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDtVal = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);
        // 날짜 체크
        if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid"])) {
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "week", true) > 1) {
            alert("조회기간은 최대 1주일까지 설정 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            return false;
        }
        // 점포 체크
        if ($.jUtil.isEmpty($("#schStoreType").val()) || $.jUtil.isEmpty($("#schStoreId").val())) {
            alert("점포를 선택해 주세요.");
            return false;
        }
        return true;
    },
};

var zipcodeOrderStatisticsGrid = {
    gridView: new RealGridJS.GridView("zipcodeOrderStatisticsGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),

    init: function() {
        zipcodeOrderStatisticsGrid.initGrid();
        zipcodeOrderStatisticsGrid.initDataProvider();
    },
    initGrid: function() {
        zipcodeOrderStatisticsGrid.gridView.setDataSource(zipcodeOrderStatisticsGrid.dataProvider);
        zipcodeOrderStatisticsGrid.gridView.setStyles(zipcodeOrderStatisticsBaseInfo.realgrid.styles);
        zipcodeOrderStatisticsGrid.gridView.setDisplayOptions(zipcodeOrderStatisticsBaseInfo.realgrid.displayOptions);
        zipcodeOrderStatisticsGrid.gridView.setColumns(zipcodeOrderStatisticsBaseInfo.realgrid.columns);
        zipcodeOrderStatisticsGrid.gridView.setOptions(zipcodeOrderStatisticsBaseInfo.realgrid.options);
        var mergeCells = ["storeType", "storeNm", "zipcode", "sidoNm", "sigunguNm"];
        zipcodeOrderStatisticsGrid.setGridSumRow(zipcodeOrderStatisticsGrid.gridView, mergeCells);
    },
    initDataProvider: function() {
        zipcodeOrderStatisticsGrid.dataProvider.setFields(zipcodeOrderStatisticsBaseInfo.dataProvider.fields);
        zipcodeOrderStatisticsGrid.dataProvider.setOptions(zipcodeOrderStatisticsBaseInfo.dataProvider.options);
    },
    setData: function(dataList) {
        zipcodeOrderStatisticsGrid.dataProvider.clearRows();
        zipcodeOrderStatisticsGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(zipcodeOrderStatisticsGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "우편번호별주문현황_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        /* 히스토리 적재 */
        CommonAjax.basic({
            url         : '/statistics/saveZipcodeOrderStatExtractHistoryForExcelDown.json',
            data        : null,
            method      : 'post',
            dataType    : 'json',
            contentType : 'application/json; charset=utf-8'
        });

        zipcodeOrderStatisticsGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중입니다."
        });
    },
    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "statistical"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"
                }
            });
        }
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
};