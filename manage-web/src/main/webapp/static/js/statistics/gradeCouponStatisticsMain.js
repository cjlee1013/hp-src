/**
 * 통계 > 주문결제통계 > 주문당 평균 주문 상품수
 */
$(document).ready(function() {
    gradeCouponStatistics.init();
    gradeCouponStatisticsListGrid.init();
    CommonAjaxBlockUI.global();
});

// 클레임관리
var gradeCouponStatistics = {

    /**
    * 초기화
    */
    init : function() {
        this.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
    },
    initSearchDate : function (flag) {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "-1d");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#searchBtn").bindClick(gradeCouponStatistics.search);
        // [초기화] 버튼
        $("#resetBtn").bindClick(gradeCouponStatistics.reset);
        // [엑셀다운로드] 버튼
        $('#excelDownloadBtn').bindClick(gradeCouponStatisticsListGrid.excelDownload);

        $("#descriptionBtn").on('click', function(){
            // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
            var _width  = 780;
            var _url = '/statistics/promotion/popup/gradeCouponStatisticsPop';
            var _height = 570;
            var _left = ($(window).width()/2)-(_width/2);
            var _top = ($(window).height()/2)-(_height/2);
            window.open(_url, '_blank',
                'toolbar=no, location=no, menubar=no, scrollbars=no, resizable=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top);
        });
    },

    /**
     * 조회 검색
     */
    search : function() {

        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);
        var today = gradeCouponStatistics.getToday();
        var todayFormat = moment(today, 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        if(todayFormat.diff(schEndDt, "day", true) < 0) {
            alert("당일 기준 전일자까지만 조회 가능합니다.");
            return false;
        }

        if($("#schEndDt").val() == today) {
            alert("당일 조회는 불가능합니다.");
            return false;
        }

        schStartDt = schStartDt.format("YYYYMMDD");
        schEndDt = schEndDt.format("YYYYMMDD");

        var data = {
            "schStartDt" : schStartDt,
            "schEndDt" : schEndDt
        }

        CommonAjax.basic(
            {
              url:'/statistics/promotion/getGradeCouponStatisticsList.json',
              data: JSON.stringify(data),
              contentType: 'application/json',
              method: "POST",
              successMsg: null,
              callbackFunc:function(res) {
                gradeCouponStatisticsListGrid.setData(res);
                $('#gradeCouponStatisticsListTotalCount').html($.jUtil.comma(gradeCouponStatisticsListGrid.gridView.getItemCount()));

              }
            });
    },

    getToday : function(){
        var date = new Date();
        var year = date.getFullYear();
        var month = ("0" + (1 + date.getMonth())).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);

        return year + "-" + month + "-" + day;
    },

    /**
     * 검색폼 초기화
     */
    reset : function() {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "-1d");
    }
};

// 클레임관리 그리드
var gradeCouponStatisticsListGrid = {
    gridView : new RealGridJS.GridView("gradeCouponStatisticsListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
      gradeCouponStatisticsListGrid.initGrid();
      gradeCouponStatisticsListGrid.initDataProvider();
      gradeCouponStatisticsListGrid.event();
    },
    initGrid : function() {
      gradeCouponStatisticsListGrid.gridView.setDataSource(gradeCouponStatisticsListGrid.dataProvider);

      gradeCouponStatisticsListGrid.gridView.setStyles(gradeCouponStatisticsListGridBaseInfo.realgrid.styles);
      gradeCouponStatisticsListGrid.gridView.setDisplayOptions(gradeCouponStatisticsListGridBaseInfo.realgrid.displayOptions);
      gradeCouponStatisticsListGrid.gridView.setColumns(gradeCouponStatisticsListGridBaseInfo.realgrid.columns);
      gradeCouponStatisticsListGrid.gridView.setOptions(gradeCouponStatisticsListGridBaseInfo.realgrid.options);


      gradeCouponStatisticsListGrid.gridView.setColumnProperty('storeType', "mergeRule", {criteria:"value"});
    },
    initDataProvider : function() {
      gradeCouponStatisticsListGrid.dataProvider.setFields(gradeCouponStatisticsListGridBaseInfo.dataProvider.fields);
      gradeCouponStatisticsListGrid.dataProvider.setOptions(gradeCouponStatisticsListGridBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
      gradeCouponStatisticsListGrid.dataProvider.clearRows();
      gradeCouponStatisticsListGrid.dataProvider.setRows(dataList);
    },

    excelDownload: function() {
      if(gradeCouponStatisticsListGrid.gridView.getItemCount() == 0) {
        alert("검색 결과가 없습니다.");
        return false;
      }

      var _date = new Date();
      var fileName =  "등급별 쿠폰 통계_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

      gradeCouponStatisticsListGrid.gridView.exportGrid({
        type: "excel",
        target: "local",
        fileName: fileName + ".xlsx",
        showProgress: true,
        progressMessage: "엑셀 Export중입니다."
      });
    }
};