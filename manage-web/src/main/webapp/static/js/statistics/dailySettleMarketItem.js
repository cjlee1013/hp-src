
/***
 * 통계 > 상품 주문 통계 > 기간별 상품 판매현황
 */
var dailySettleMarketItemGrid = {
    gridView : new RealGridJS.GridView("dailySettleMarketItemGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleMarketItemGrid.initGrid();
        dailySettleMarketItemGrid.initDataProvider();
        dailySettleMarketItemGrid.event();
    },
    initGrid : function () {
        dailySettleMarketItemGrid.gridView.setDataSource(dailySettleMarketItemGrid.dataProvider);
        dailySettleMarketItemGrid.gridView.setStyles(dailySettleMarketItemBaseInfo.realgrid.styles);
        dailySettleMarketItemGrid.gridView.setDisplayOptions(dailySettleMarketItemBaseInfo.realgrid.displayOptions);
        dailySettleMarketItemGrid.gridView.setOptions(dailySettleMarketItemBaseInfo.realgrid.options);
        dailySettleMarketItemGrid.gridView.setColumns(dailySettleMarketItemBaseInfo.realgrid.columns);

        dailySettleMarketItemGrid.gridView.setColumnProperty( "itemNm", "styles", {
            "textAlignment": "near",
            "paddingLeft" : 10
        });
    },
    initDataProvider : function() {
        dailySettleMarketItemGrid.dataProvider.setFields(dailySettleMarketItemBaseInfo.dataProvider.fields);
        dailySettleMarketItemGrid.dataProvider.setOptions(dailySettleMarketItemBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
        dailySettleMarketItemGrid.dataProvider.clearRows();
        dailySettleMarketItemGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailySettleMarketItemGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = '기간별 상품 판매현황_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");

        dailySettleMarketItemGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * 기간별 마켓상품 판매현황 폼
 */
var dailySettleMarketItemForm = {
    init : function () {
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        dailySettleMarketItemForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailySettleMarketItemForm.setDate.setEndDate(0);
        dailySettleMarketItemForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailySettleMarketItemForm.search);
        $('#searchResetBtn').bindClick(dailySettleMarketItemForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailySettleMarketItemGrid.excelDownload);
    },
    valid : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 7) {
            alert("조회기간은 최대 일주일 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    },
    setSearchData : function () {
        var searchForm = "&"+ $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        return searchForm;
    },
    search : function () {
        if(!dailySettleMarketItemForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = dailySettleMarketItemForm.setSearchData();

        CommonAjax.basic(
            {
                url: '/statistics/dailyDivisionEKPI/getDailySettleMarketItemList.json?' + encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailySettleMarketItemGrid.setData(res);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            dailySettleMarketItemForm.init();
        });
    }
};