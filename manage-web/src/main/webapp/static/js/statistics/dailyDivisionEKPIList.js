
/***
 * 통계 > 상품 주문 통계 > 카테고리별 매출 (E-KPI)
 */
var dailyDivisionEKPIListGrid = {
    gridView : new RealGridJS.GridView("dailyDivisionEKPIListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyDivisionEKPIListGrid.initGrid();
        dailyDivisionEKPIListGrid.initDataProvider();
        dailyDivisionEKPIListGrid.event();
    },
    initGrid : function () {
        dailyDivisionEKPIListGrid.gridView.setDataSource(dailyDivisionEKPIListGrid.dataProvider);
        dailyDivisionEKPIListGrid.gridView.setStyles(dailyDivisionEKPIListBaseInfo.realgrid.styles);
        dailyDivisionEKPIListGrid.gridView.setDisplayOptions(dailyDivisionEKPIListBaseInfo.realgrid.displayOptions);
        dailyDivisionEKPIListGrid.gridView.setOptions(dailyDivisionEKPIListBaseInfo.realgrid.options);
        dailyDivisionEKPIListGrid.gridView.setColumns(dailyDivisionEKPIListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ [ "mallType", "divisionNm", "groupNm", "deptNm", "classNm" ] ];
        SettleCommon.setRealGridSumHeader(dailyDivisionEKPIListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        dailyDivisionEKPIListGrid.dataProvider.setFields(dailyDivisionEKPIListBaseInfo.dataProvider.fields);
        dailyDivisionEKPIListGrid.dataProvider.setOptions(dailyDivisionEKPIListBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        dailyDivisionEKPIListGrid.dataProvider.clearRows();
        dailyDivisionEKPIListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailyDivisionEKPIListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailyDivisionEKPIListForm.setExcelFileName("카테고리별 매출(E-KPI)");
        dailyDivisionEKPIListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * 판매현황 폼
 */
var dailyDivisionEKPIListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
        $('#storeType').val('HYPER');
        $('#mallType').val('TD');
        this.setSearchDivision(1);
    },
    initSearchDate : function (flag) {
        dailyDivisionEKPIListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailyDivisionEKPIListForm.setDate.setEndDate(0);
        dailyDivisionEKPIListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startYear, #endYear').val(year);
        $('#startMonth, #endMonth').val(month);
    },
    setSearchDivision: function (depth) {
        var division = (depth == 1)? 0 : $('select[name=divisionNo]').val();

        var selectBoxStr = '';
        var divisionNoStr = "<option value=''>중분류</option>";
        var deptNoStr = "<option value=''>소분류</option>";
        var classNoStr = "<option value=''>세분류</option>";
        CommonAjax.basic({
            url: '/common/category/getCategoryForMappingSelectBox.json',
            data: {
                depth: depth,
                division: division,
                groupNo: $('select[name=groupNo]').val(),
                dept: $('select[name=deptNo]').val(),
                classCd: 0,
            },
            method: 'get',
            callbackFunc: function (res) {
                switch (depth) {
                    case 1:
                        $("select[name='divisionNo']").empty();
                        selectBoxStr = "<option value=''>대분류</option>"; break;
                    case 2:
                        dailyDivisionEKPIListForm.initGroup();
                        dailyDivisionEKPIListForm.initDept();
                        dailyDivisionEKPIListForm.initClass();
                        selectBoxStr = divisionNoStr; break;
                    case 3:
                        dailyDivisionEKPIListForm.initDept();
                        dailyDivisionEKPIListForm.initClass();
                        selectBoxStr = deptNoStr; break;
                    case 4:
                        dailyDivisionEKPIListForm.initClass();
                        selectBoxStr = classNoStr; break;
                }

                $.each(res, function (key, resList) {
                    switch (depth) {
                        case 1:
                            selectBoxStr += '<option value="'+ resList.division +'" >'+ resList.cateNm +'</option>'; break;
                        case 2:
                            selectBoxStr += '<option value="'+ resList.groupNo +'" >'+ resList.cateNm +'</option>'; break;
                        case 3:
                            selectBoxStr += '<option value="'+ resList.dept +'" >'+ resList.cateNm +'</option>'; break;
                        case 4:
                            selectBoxStr += '<option value="'+ resList.class +'" >'+ resList.cateNm +'</option>'; break;
                    }
                });

                switch (depth) {
                    case 1: $('#divisionNo').html(selectBoxStr); break;
                    case 2: $('#groupNo').html(selectBoxStr); break;
                    case 3: $('#deptNo').html(selectBoxStr); break;
                    case 4: $('#classNo').html(selectBoxStr); break;
                }


            }, error: function(xhr) {
                alert('잠시 뒤에 시도해주세요.');
                return;
            }
        });
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailyDivisionEKPIListForm.search);
        $('#searchResetBtn').bindClick(dailyDivisionEKPIListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailyDivisionEKPIListGrid.excelDownload);

        $('#mallType').on('change', function() {
            if($(this).val() == "TD") {
                $('select[name="storeType"]').removeAttr('readonly').val('HYPER');
            } else {
                $('select[name="storeType"]').attr('readonly', true).val('');
            }
        });

        $('#divisionNo').on('change', function() {
            dailyDivisionEKPIListForm.setSearchDivision(2);
        });

        $('#groupNo').on('change', function() {
            dailyDivisionEKPIListForm.setSearchDivision(3);
        });

        $('#deptNo').on('change', function() {
            dailyDivisionEKPIListForm.setSearchDivision(4);
        });
    },
    initGroup : function () {
        $("select[name='groupNo']").empty();
        var selectBoxStr = "<option value=''>중분류</option>";
        $('#groupNo').html(selectBoxStr);
    },
    initDept : function () {
        $("select[name='deptNo']").empty();
        var selectBoxStr = "<option value=''>소분류</option>";
        $('#deptNo').html(selectBoxStr);
    },
    initClass : function () {
        $("select[name='classNo']").empty();
        var selectBoxStr = "<option value=''>세분류</option>";
        $('#classNo').html(selectBoxStr);
    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 31) {
                alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }

        }

        return true;
    },
    setSearchData : function () {
        var searchForm = "&"+ $("form[name=searchForm]").serialize();
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#endYear").val() +'-'+ $("#endMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        return searchForm;
    },
    search : function () {
        if(!dailyDivisionEKPIListForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = dailyDivisionEKPIListForm.setSearchData();

        CommonAjax.basic(
            {
                url: '/statistics/dailyDivisionEKPI/dailyDivisionEKPIList.json?' + encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailyDivisionEKPIListGrid.setData(res);

                    //그룹헤더컬럼 합계
                    SettleCommon.setRealGridHeaderSum(dailyDivisionEKPIListGrid.gridView, []);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val() +'_'+ $("#endYear").val() + $("#endMonth").val();
        }
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            dailyDivisionEKPIListForm.init();
        });
    }
};