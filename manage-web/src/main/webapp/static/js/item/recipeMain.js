/**
 * 상품관리 > 점포상품관리 > 레시피 관리
 */
$(document).ready(function() {

    recipeListGrid.init();
    recipe.init();
    recipeItemListGrid.init();
    recipeItem.init();

    CommonAjaxBlockUI.global();
    commonEditor.initEditor('#contentsDescEdit', 300, 'FrontBanner');
});
var commonEditor;

var recipeItemListGrid = {

    gridView : new RealGridJS.GridView("recipeItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        recipeItemListGrid.initGrid();
        recipeItemListGrid.initDataProvider();
        recipeItemListGrid.event();

    },
    initGrid : function() {

        recipeItemListGrid.gridView.setDataSource(recipeItemListGrid.dataProvider);
        recipeItemListGrid.gridView.setStyles(recipeItemListGridBaseInfo.realgrid.styles);
        recipeItemListGrid.gridView.setDisplayOptions(recipeItemListGridBaseInfo.realgrid.displayOptions);
        recipeItemListGrid.gridView.setColumns(recipeItemListGridBaseInfo.realgrid.columns);
        recipeItemListGrid.gridView.setOptions(recipeItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        recipeItemListGrid.dataProvider.setFields(recipeItemListGridBaseInfo.dataProvider.fields);
        recipeItemListGrid.dataProvider.setOptions(recipeItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        recipeItemListGrid.gridView.onDataCellClicked = function(gridView, index) {

        };
    },
    setData : function(dataList) {
        recipeItemListGrid.dataProvider.clearRows();
        recipeItemListGrid.dataProvider.setRows(dataList);

    },
    addDataList : function (dataList) {
        recipeItemListGrid.dataProvider.addRows(dataList);
    },
    addData : function (data) {
        recipeItemListGrid.dataProvider.addRow(data);
    },
    delDataList : function(dataList){
        recipeItemListGrid.dataProvider.removeRows(dataList,false);
    },
    delData : function(data){
        recipeItemListGrid.dataProvider.removeRow(data);
    }
};
var recipeItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#addItemOptionPopUp').bindClick(recipeItem.addItemOptionPopUp);
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        let check = recipeItemListGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }
        recipeItemListGrid.dataProvider.removeRows(check, false);
        recipeItem.resetItemPriority();
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        let checkedRows = recipeItemListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품 선택해 주세요.");
            return false;
        }

        realGridMoveRow(recipeItemListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬
        recipeItem.resetItemPriority();

    },

    /**
     * 상품 우선순위 재 정렬
     */
    resetItemPriority : function(){
        // Priority 정렬
        recipeItemListGrid.dataProvider.getJsonRows(0,-1).forEach(function (data, index) {

            let itemInfo = recipeItemListGrid.gridView.getValues(index);
            itemInfo.priority = parseInt(index) + 1
            recipeItemListGrid.gridView.setValues(index , itemInfo ,true);

        });
    },
    /**
     * 상품조회(용도옵션포함)팝업
     */
    addItemOptionPopUp : function() {
        $.jUtil.openNewPopup('/common/popup/itemOptionPop?callback=recipeItem.setItemList&isMulti=Y&mallType=TD&limitSize=0', 1084, 650);
    },
    /**
     * 상품리스트 설정
     */
    setItemList : function(itemList) {

        var gridList = recipeItemListGrid.dataProvider.getJsonRows();

        itemList.forEach(function (item) {
            gridList.forEach(function (gridItem, idx) {
                if(gridItem.itemNo == item.itemNo && gridItem.optNo == item.optNo ){
                    gridList.splice(idx, 1)
                }
            });
        });
        if( (gridList.length + itemList.length ) > 20){
            alert("최대 20개까지 등록할 수 있습니다.");
            return false;
        }

        recipeItemListGrid.dataProvider.clearRows();
        recipeItemListGrid.setData(itemList);
        recipeItemListGrid.addDataList(gridList);
        recipeItem.resetItemPriority();

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        recipeItem.resetForm();

        var rowDataJson = recipeItemListGrid.dataProvider.getJsonRow(selectRowId);

    }

}

// 레시피 그리드
var recipeListGrid = {

    gridView : new RealGridJS.GridView("recipeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        recipeListGrid.initGrid();
        recipeListGrid.initDataProvider();
        recipeListGrid.event();

    },
    initGrid : function() {

        recipeListGrid.gridView.setDataSource(recipeListGrid.dataProvider);
        recipeListGrid.gridView.setStyles(recipeListGridBaseInfo.realgrid.styles);
        recipeListGrid.gridView.setDisplayOptions(recipeListGridBaseInfo.realgrid.displayOptions);
        recipeListGrid.gridView.setColumns(recipeListGridBaseInfo.realgrid.columns);
        recipeListGrid.gridView.setOptions(recipeListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        recipeListGrid.dataProvider.setFields(recipeListGridBaseInfo.dataProvider.fields);
        recipeListGrid.dataProvider.setOptions(recipeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        recipeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            recipe.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        recipeListGrid.dataProvider.clearRows();
        recipeListGrid.dataProvider.setRows(dataList);

    }

};

// 레시피관리
var recipe = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
    },
    initSearchDate : function (flag) {
        recipe.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        recipe.setDate.setEndDate(0);
        recipe.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        //검색 역영
        $('#searchBtn').bindClick(recipe.search);   // 검색
        $('#searchResetBtn').bindClick(recipe.searchFormReset); // 검색폼 초기화
        $('#setRecipeBtn').bindClick(recipe.setRecipe); // 레시피 정보 저장
        $('#resetBtn').bindClick(recipe.resetForm); // 레시피 정보 저장
        $('input[name="fileArr"]').change(function() {
            recipe.img.addImg();
        });


    },
    /**
     * 레시피 검색
     */
    search : function() {

        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            recipe.initSearchDate('-30d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        CommonAjax.basic({url:'/item/recipe/getRecipeList.json?' + $('#recipeSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                recipe.resetForm();
                recipeListGrid.setData(res);
                $('#recipeTotalCount').html($.jUtil.comma(recipeListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        recipe.initSearchDate('-30d');
        $('#searchType').val('RECIPENM');  // 검색어 구분
        $('#searchKeyword').val('');    //검색어
        $('#searchDispYn').val('');    //노출여부

    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        recipe.resetForm();

        var rowDataJson = recipeListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({url:'/item/recipe/getRecipeDetail.json?recipeNo='+rowDataJson.recipeNo
            , data: null
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(res) {
                //레시피 상세 정보


                $('#title').val(res.title); // 레시피명
                $('#recipeNo').val(res.recipeNo); // 레시피번호
                $('#cookingQuantity').val(res.cookingQuantity); // 분량
                $('#cookingTime').val(res.cookingTime); // 분량

                //난이도
                $("input:radio[name=grade]:input[value=" + res.grade + "]").prop("checked", true);

                //대표 이미지(pc>상세)
                recipe.img.setImg($('#titleImg'), {
                    'imgUrl': res.titleImgUrl,
                   /* 'src'   : hmpImgUrl + res.titleImgUrl+"?"+Math.floor(Math.random()*1000)*/
                    'src'    : hmpImgUrl + "/provide/view?processKey=ItemRecipe1&fileId=" + res.titleImgUrl

                });
                $('#cookingTip').val(res.cookingTip); // 레시피 팁

                //대표 이미지(공통)
                recipe.img.setImg($('#contentsImg'), {
                    'imgUrl': res.contentsImgUrl,
                    /*'src'   : hmpImgUrl + res.contentsImgUrl+"?"+Math.floor(Math.random()*1000)*/
                    'src'    : hmpImgUrl + "/provide/view?processKey=ItemRecipe2&fileId=" + res.contentsImgUrl

                });

                //요리방법
                commonEditor.setHtml('#contentsDescEdit' ,res.contents);

                //주재료 , 부재
                let stuffList  =  res.stuffList;
                for (let i in stuffList) {
                    if(stuffList[i].stuffType === 'M'){ //주재료
                        $('#stuffListM').val(stuffList[i].stuffDesc);
                    } else{ //부재료
                        $('#stuffListS').val(stuffList[i].stuffDesc);
                    }

                }

                $("input:radio[name=dispYn]:input[value=" + res.dispYn + "]").prop("checked", true);

                recipeItemListGrid.setData(res.itemList);
                recipeItem.resetItemPriority();


            }});

    },
    /**
     * 레시피 등록/수정 유효성 검사
     */
    setRecipeValid : function(){

        $('#contents').val( commonEditor.getHtmlData('#contentsDescEdit'));
        var objectArray = [$('#title'), $('#cookingQuantity'), $('#cookingTime') , $('#cookingTip') , $('#contents')];

        var grade = $("input[name='grade']:checked").val();


        if($.jUtil.isEmptyObjectArray(objectArray)){
            alert("필수 입력 항목을 선택/입력 해주세요." );
            return false;
        }
        if($.jUtil.isEmpty($("input[name='grade']:checked").val())){
            alert("난이도를 선택 해주세요." );
            return false;

        }
        if($.jUtil.isEmpty($("input[name='dispYn']:checked").val())){
            alert("노출 여부를 선택 해주세요." );
            return false;

        }
        if($.jUtil.isEmpty($('#titleImgUrl').val())){
            alert("레시피상세 이미지(와이드형)를 등록해 주세요." );
            return false;

        }
        if($.jUtil.isEmpty($('#contentsImgUrl').val())){
            alert("상품상세 이미지(정방형)를 등록해 주세요." );
            return false;

        }
        var itemList = recipeItemListGrid.dataProvider.getJsonRows();

        if(itemList.length < 1){

            alert("상품을 등록해 주세요." );
            return false;
        }

        return true;
    },
    /**
     * 레시피 등록/수정
     */
    setRecipe : function() {
        if(recipe.setRecipeValid()){

            var setRecipe = $('#recipeSetForm').serializeObject(true);
            setRecipe.itemList = recipeItemListGrid.dataProvider.getJsonRows();

            CommonAjax.basic({url:'/item/recipe/setRecipe.json',data: JSON.stringify(setRecipe), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                    alert(res.returnMsg);
                    recipe.search();
                }});

        }

    },
    /**
     * 레시피 입력/수정 폼 초기화
     */
    resetForm : function() {

        $('#title, #recipeNo, #cookingQuantity, #cookingTime, #stuffListM, #stuffListS,#cookingTip').val('');

        $("input:radio[name=grade]").prop("checked", false);
        $('input:radio[name="dispYn"][value="Y"]').prop("checked", true);

        recipe.img.resetImg();
        commonEditor.setHtml('#contentsDescEdit' ,'');
        recipeItemListGrid.dataProvider.clearRows();


    }

};
recipe.img = {
    imgDiv : null,
    imgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        recipe.img.resetImg();
        recipe.img.imgDiv = null;
        recipe.img.imgType = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ recipe.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {



        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var file = $('#itemFile');
        var params;

        if(recipe.img.imgType == 'titleImg'){
            params = {
                processKey : 'ItemRecipe1',
                mode : 'IMG'
            };

        } else{
            params = {
                processKey : 'ItemRecipe2',
                mode : 'IMG'
            };
        }



        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        recipe.img.setImg(recipe.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'    : hmpImgUrl + "/provide/view?processKey=" +params.processKey+"&fileId=" + f.fileStoreInfo.fileId

                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        recipe.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        recipe.img.imgDiv = $('#'+imgType);
        recipe.img.imgType = imgType;
        $('input[name=fileArr]').click();
    }
};