$(document).ready(function () {
    auroraBannerMng.init();
    auroraBannerGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var isEdit = false;

var auroraBannerGrid = {
    gridView: new RealGridJS.GridView("auroraBannerGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        auroraBannerGrid.initGrid();
        auroraBannerGrid.initDataProvider();
        auroraBannerGrid.event();
    },
    initGrid: function () {
        auroraBannerGrid.gridView.setDataSource(auroraBannerGrid.dataProvider);

        auroraBannerGrid.gridView.setStyles(auroraBannerGridBaseInfo.realgrid.styles);
        auroraBannerGrid.gridView.setDisplayOptions(auroraBannerGridBaseInfo.realgrid.displayOptions);
        auroraBannerGrid.gridView.setColumns(auroraBannerGridBaseInfo.realgrid.columns);
        auroraBannerGrid.gridView.setOptions(auroraBannerGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        auroraBannerGrid.dataProvider.setFields(auroraBannerGridBaseInfo.dataProvider.fields);
        auroraBannerGrid.dataProvider.setOptions(auroraBannerGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        auroraBannerGrid.gridView.onDataCellClicked = function (gridView, index) {
            auroraBannerMng.getGrid(index.dataRow);
        };
    },
    setData: function (dataList) {
        auroraBannerGrid.dataProvider.clearRows();
        auroraBannerGrid.dataProvider.setRows(dataList);
    }
};

var auroraBannerMng = {
    init: function () {
        this.bindingEvent();
        this.initSearchDate('-10d');
        this.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        auroraBannerMng.img.init();

        $('#selectItemPopup').hide();
    },
    initSearchDate : function (flag) {
        auroraBannerMng.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        auroraBannerMng.setDate.setEndDate(0);
        auroraBannerMng.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    bindingEvent: function () {
        $('#searchBtn').bindClick(auroraBannerMng.search);
        $('#searchResetBtn').bindClick(auroraBannerMng.searchFormReset);
        $('#setAuroraBannerBtn').bindClick(auroraBannerMng.setAuroraBanner);
        $('#resetBtn').bindClick(auroraBannerMng.resetForm);
        $('#linkType').bindChange(auroraBannerMng.chgLinkInfo);
        $('#dispKind').bindChange(auroraBannerMng.chgDispKind);
        $('#addItemPopUp').bindClick(auroraBannerMng.addItemPopUp);
        $('#template').bindChange(auroraBannerMng.changeTemplate);

        $('#dispKind').bindChange(auroraBannerMng.switchTextBox);

        $(document).on('change','#bannerType',function(){
            auroraBannerMng.changeDescription($('#template').val(), $('#bannerType').val())
        });

        $('.textYn').change(function(){
            auroraBannerMng.setText(this);
        });

        $(document).on('change','input[name="fileArr"]',function(){
            auroraBannerMng.img.addImg();
        });

        //이미지 등록 버튼
        $(document).on('click','.addImgBtn',function(){
            auroraBannerMng.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();
        });

        //이미지 삭제 버튼
        $(document).on('click','.deleteImgBtn',function(){
            auroraBannerMng.img.setImg($(this).next(), null, true);
        });
    },
    search: function () {
        CommonAjax.basic({
            url: '/item/auroraBanner/getAuroraBanner.json',
            data: $('#searchForm').serialize(),
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                auroraBannerGrid.setData(res);
                $('#auroraBannerGridCnt').html($.jUtil.comma(auroraBannerGrid.gridView.getItemCount()));
                auroraBannerMng.resetForm();
            }
        });
    },
    searchFormReset: function( ) {
        $('#searchPeriodType').val('regDt');
        $('#schKeyword').val('');
        auroraBannerMng.initSearchDate('-10d');
    },
    resetForm: function () {
        $('#bannerId, #bannerNm').val('');
        auroraBannerMng.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        $('#dispKind').val('PC');
        $('#useYn').val('Y');
        $("#template").val('1').trigger('change');
        auroraBannerMng.chgDispKind();
    },
    getGrid: function (selectRowId) {
        var rowDataJson = auroraBannerGrid.dataProvider.getJsonRow(selectRowId);
        var dispKind = rowDataJson.dispKind;

        $('#bannerId').val(rowDataJson.bannerId);
        $('#bannerNm').val(rowDataJson.bannerNm);

        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);

        $('#dispKind').val(rowDataJson.dispKind);
        $('#useYn').val(rowDataJson.useYn);

        auroraBannerMng.chgDispKind();
        auroraBannerMng.switchTextBox();

        CommonAjax.basic({url:'/item/auroraBanner/getAuroraBannerLink.json?bannerId=' + rowDataJson.bannerId, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                auroraBannerMng.setBannerDetail(res, rowDataJson.template);
            }});
    },
    setAuroraBanner : function() {
        if(!auroraBannerMng.setValid()) {
            return false;
        }

        var bannerDetail = $("#auroraBannerForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < parseInt($("#template").val()) ; i++){
            linkInfo = bannerDetail.linkList[i];
            if ($('input:checkbox').eq(i).is(":checked") == true ) {
                linkInfo.textYn = "Y";
            } else {
                linkInfo.textYn = "N";
            }
            // if(linkInfo.linkType == 'ITEM_TD') {
            //     //링크 옵션 설정1
            //     if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
            //         linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;
            //
            //     } else {
            //         linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
            //     }
            // }
            linkList.push(linkInfo);
            linkInfo = new Object();
        }
        bannerDetail.linkList = linkList;

        CommonAjax.basic({
            url : '/item/auroraBanner/setAuroraBanner.json',
            data : JSON.stringify(bannerDetail),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                auroraBannerMng.resetForm();
                auroraBannerMng.search();
            }
        });
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        auroraBannerMng.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        auroraBannerMng.setDateTime.setEndDateByTimeStamp(flag);
        auroraBannerMng.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function(obj) {
        linkInfoDiv = $(obj).parent();

        var linkeType  = $(obj).parent().prev('select').val();
        var storeType = $(obj).parent().find('.storeType').val();

        switch (linkeType) {
            case "EXH"  :
                auroraBannerMng.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                auroraBannerMng.addPromoPopUp('EVENT');
                break;
            case "ITEM_DS" :
                auroraBannerMng.addItemPopUp('DS');
                break;
            case "ITEM_TD" :
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                auroraBannerMng.addItemPopUp('TD' , 'AURORA');
                break;
        }
    },
    /**
     * 배너 노출 구분
     */
    chgDispKind : function() {
        // Type에 따른 노출 입력폼 변경
        switch ($('#dispKind option:selected').val()) {
            case 'PC' :
                $('#bannerTypePc, #bannerTitlePc').show();
                break;
            default :
                $('#bannerTypePc, #bannerTitlePc').hide();
                break;
        }
    },
    /**
     * 전시위치 > 카테고리 정보
     */
    switchTextBox : function() {
        $("#template").val('1').trigger('change');
        var dispKind = $('#dispKind').val();
        if(dispKind === 'MOBILE') {
            $('.textYn').each(function() {
                $(this).val('N').attr('disabled', true);
                $(this).parent().parent().find('.bannerTitle').val('').prop('readOnly', true);
                $(this).parent().parent().find('.bannerSubTitle').val('').prop('readOnly', true);
            });
        } else {
            $('.textYn').each(function() {
                $(this).val('N').attr('disabled', false).prop('checked', false);
                $(this).parent().parent().find('.bannerTitle').val('').prop('readOnly', true);
                $(this).parent().parent().find('.bannerSubTitle').val('').prop('readOnly', true);
            });
        }
    },
    /**
     * 배너 단 설정
     */
    changeTemplate : function(){
        var template = $("#template").val();
        var bannerType = $("#bannerType").val();

        auroraBannerMng.resetLinkTypeAll();
        auroraBannerMng.chgBannerType(template);

        // 배너 입력 영역 설정
        switch (template) {
            case "6" :
                $('.bannerDiv6').css("display", "");
            case "5" :
                $('.bannerDiv5').css("display", "");
            case "4" :
                $('.bannerDiv4').css("display", "");
            case "3" :
                $('.bannerDiv3').css("display", "");
            case "2" :
                $('.bannerDiv2').css("display", "");
            case "1" :
                $('.bannerDiv1').css("display", "");
                break;
        };

        // Description 변경
        auroraBannerMng.changeDescription(template, bannerType);
    },
    /**
     * 배너 단 Description
     */
    changeDescription : function(template, bannerType) {
        var dispKind = $('#dispKind').val();

        switch (template) {
            case "1" :
                if(dispKind === 'PC') {
                    $('#bannerMainTitleDiv1').prop('maxlength','20');
                    $('#bannerSubTitleDiv1').prop('maxlength','28');
                    $('#bannerImgSizeDesc1').html('1200*450');
                } else {
                    $('#bannerImgSizeDesc1').html('750*460');
                }
                break;
            case "2" :
                if(bannerType === '2LP') {
                    $('#bannerMainTitleDiv1').prop('maxlength','20');
                    $('#bannerSubTitleDiv1').prop('maxlength','28');
                    $('#bannerImgSizeDesc1').html('790*450');

                    $('#bannerMainTitleDiv2').prop('maxlength','15');
                    $('#bannerSubTitleDiv2').prop('maxlength','20');
                    $('#bannerImgSizeDesc2').html('390*450');
                } else {
                    $('#bannerMainTitleDiv1').prop('maxlength','20');
                    $('#bannerSubTitleDiv1').prop('maxlength','28');
                    $('#bannerImgSizeDesc1').html('590*450');

                    $('#bannerMainTitleDiv2').prop('maxlength','20');
                    $('#bannerSubTitleDiv2').prop('maxlength','28');
                    $('#bannerImgSizeDesc2').html('590*450');
                }
                break;
            case "3" :
                if(bannerType === '3CP') {
                    $('#bannerMainTitleDiv1').prop('maxlength','15');
                    $('#bannerSubTitleDiv1').prop('maxlength','20');
                    $('#bannerImgSizeDesc1').html('290*450');

                    $('#bannerMainTitleDiv2').prop('maxlength','20');
                    $('#bannerSubTitleDiv2').prop('maxlength','28');
                    $('#bannerImgSizeDesc2').html('590*450');

                    $('#bannerMainTitleDiv3').prop('maxlength','15');
                    $('#bannerSubTitleDiv3').prop('maxlength','20');
                    $('#bannerImgSizeDesc3').html('290*450');
                } else if(bannerType === '3LP') {
                    $('#bannerMainTitleDiv1').prop('maxlength','20');
                    $('#bannerSubTitleDiv1').prop('maxlength','28');
                    $('#bannerImgSizeDesc1').html('790*450');

                    $('#bannerMainTitleDiv2').prop('maxlength','20');
                    $('#bannerSubTitleDiv2').prop('maxlength','28');
                    $('#bannerImgSizeDesc2').html('390*220');

                    $('#bannerMainTitleDiv3').prop('maxlength','20');
                    $('#bannerSubTitleDiv3').prop('maxlength','28');
                    $('#bannerImgSizeDesc3').html('390*220');
                } else {
                    $('#bannerMainTitleDiv1').prop('maxlength','15');
                    $('#bannerSubTitleDiv1').prop('maxlength','20');
                    $('#bannerImgSizeDesc1').html('390*450');

                    $('#bannerMainTitleDiv2').prop('maxlength','15');
                    $('#bannerSubTitleDiv2').prop('maxlength','20');
                    $('#bannerImgSizeDesc2').html('390*450');

                    $('#bannerMainTitleDiv3').prop('maxlength','15');
                    $('#bannerSubTitleDiv3').prop('maxlength','20');
                    $('#bannerImgSizeDesc3').html('390*450');
                }
                break;
            case "4" :
                break;
            case "5" :
                $('#bannerMainTitleDiv1').prop('maxlength','20');
                $('#bannerSubTitleDiv1').prop('maxlength','28');
                $('#bannerImgSizeDesc1').html('390*220');

                $('#bannerMainTitleDiv2').prop('maxlength','20');
                $('#bannerSubTitleDiv2').prop('maxlength','28');
                $('#bannerImgSizeDesc2').html('390*220');

                $('#bannerMainTitleDiv3').prop('maxlength','20');
                $('#bannerSubTitleDiv3').prop('maxlength','28');
                $('#bannerImgSizeDesc3').html('390*450');

                $('#bannerMainTitleDiv4').prop('maxlength','20');
                $('#bannerSubTitleDiv4').prop('maxlength','28');
                $('#bannerImgSizeDesc4').html('390*220');

                $('#bannerMainTitleDiv5').prop('maxlength','20');
                $('#bannerSubTitleDiv5').prop('maxlength','28');
                $('#bannerImgSizeDesc5').html('390*220');
                break;
            case "6" :
                $('#bannerMainTitleDiv1').prop('maxlength','20');
                $('#bannerSubTitleDiv1').prop('maxlength','28');
                $('#bannerImgSizeDesc1').html('390*220');

                $('#bannerMainTitleDiv2').prop('maxlength','20');
                $('#bannerSubTitleDiv2').prop('maxlength','28');
                $('#bannerImgSizeDesc2').html('390*220');

                $('#bannerMainTitleDiv3').prop('maxlength','20');
                $('#bannerSubTitleDiv3').prop('maxlength','28');
                $('#bannerImgSizeDesc3').html('390*220');

                $('#bannerMainTitleDiv4').prop('maxlength','20');
                $('#bannerSubTitleDiv4').prop('maxlength','28');
                $('#bannerImgSizeDesc4').html('390*220');

                $('#bannerMainTitleDiv5').prop('maxlength','20');
                $('#bannerSubTitleDiv5').prop('maxlength','28');
                $('#bannerImgSizeDesc5').html('390*220');

                $('#bannerMainTitleDiv6').prop('maxlength','20');
                $('#bannerSubTitleDiv6').prop('maxlength','28');
                $('#bannerImgSizeDesc6').html('390*220');
                break;
        };
    },
    /**
     * 링크 타입 전체 초기화
     */
    resetLinkTypeAll : function() {
        $("[class^='bannerDiv']").css("display", "none");
        $("[class^='bannerDiv']").find('.linkInfo').val('');
        $("[class^='bannerDiv']").find('.linkInfoNm').val('');
        $("[class^='bannerDiv']").find('.bannerTitle').val('');
        $("[class^='bannerDiv']").find('.bannerSubTitle').val('');
        $("[class^='bannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='bannerDiv']").find('.deleteImgBtn').trigger('click');
        $("[class^='bannerDiv']").find('.bannerTitle').prop('readonly' ,true).val('');
        $("[class^='bannerDiv']").find('.bannerSubTitle').prop('readonly' ,true).val('');

        $('.bannerDiv1').css("display", "");
    },
    /**
     * 링크 타입 초기화 (단 번호별 초기화)
     */
    resetLinkInfo : function(linkTemplateNumber) {
        $('.bannerDiv'+linkTemplateNumber).find('.linkInfo').val('');
        $('.bannerDiv'+linkTemplateNumber).find('.linkInfoNm').val('');
        $('.bannerDiv'+linkTemplateNumber).find('.bannerTitle').val('').prop('readOnly' , true);;
        $('.bannerDiv'+linkTemplateNumber).find('.bannerSubTitle').val('').prop('readOnly' , true);;
        $('.bannerDiv'+linkTemplateNumber).find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.bannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');
        $('.bannerDiv'+linkTemplateNumber).find('.bannerTitle').prop('readonly' ,true).val('');
        $('.bannerDiv'+linkTemplateNumber).find('.bannerSubTitle').prop('readonly' ,true).val('');
    },
    /**
     * 배너 상세 정보 입력
     */
    setBannerDetail : function(data, template) {
        $("#template").val(template).trigger('change');

        if (!$.jUtil.isEmpty(data)) {
            for(var i =0 ; i < parseInt(template) ; i++) {
                var link  = data[i];
                var templateNum = i+1;

                if(link.textYn == 'Y') {
                    $('.bannerDiv' + templateNum).find('.textYn').prop('checked', true);
                    $('.bannerDiv' + templateNum).find('.bannerTitle').prop('readonly' ,false).val(link.title);
                    $('.bannerDiv' + templateNum).find('.bannerSubTitle').prop('readonly' ,false).val(link.subTitle);
                } else {
                    $('.bannerDiv' + templateNum).find('.textYn').prop('checked', false);
                    $('.bannerDiv' + templateNum).find('.bannerTitle').prop('readonly' ,true).val('');
                    $('.bannerDiv' + templateNum).find('.bannerSubTitle').prop('readonly' ,true).val('');
                }

                auroraBannerMng.img.setImg($('.bannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'src'   : hmpImgUrl + "/provide/view?processKey=" + auroraBannerMng.img.getProcessKey() + "&fileId=" + link.imgUrl,
                });

                $("#bannerLinkType" + templateNum ).val(link.linkType).trigger('change');
                auroraBannerMng.setLinkInfo(link, $("#bannerLinkInfo" + templateNum));
            }
        }
    },
    /**
     * 링크 상세 정보 입력
     */
    setLinkInfo : function (data, obj) {
        $(obj).find('.linkInfo').val(data.linkInfo);
        $(obj).find('.linkInfoNm').val(data.linkInfoNm);
    },
    /**
     * 배너 설정 타입 변경
     */
    chgBannerType : function(template) {
        $('#bannerType').find("option").remove();
        $.each(bannerTypeTemplate, function(key, code) {
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            if(codeRef1 === template) {
                $('#bannerType').append("<option value=" + codeCd + ">" + codeNm + "</option>");
            }
        });
    },
    /**
     * 타이틀 입력란 설정
     */
    setText : function(obj) {
        if($(obj).prop("checked"))  {
            $("[class^='bannerDiv']").find('.bannerTitle').val('').prop('readOnly' , false);
            $("[class^='bannerDiv']").find('.bannerSubTitle').val('').prop('readOnly' , false);
            $("[class^='bannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", true).val('Y');
        } else {
            $("[class^='bannerDiv']").find('.bannerTitle').val('').prop('readOnly' , true);
            $("[class^='bannerDiv']").find('.bannerSubTitle').val('').prop('readOnly' , true);
            $("[class^='bannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false).val('N');
        }
        // if($(obj).prop("checked")) {
        //     $(obj).val('Y');
        //     $(obj).parent().parent().find('.bannerTitle').prop('readOnly' ,false);
        //     $(obj).parent().parent().find('.bannerSubTitle').prop('readOnly' ,false);
        // } else {
        //     $(obj).val('N');
        //     $(obj).parent().parent().find('.bannerTitle').val('').prop('readOnly' ,true);
        //     $(obj).parent().parent().find('.bannerSubTitle').val('').prop('readOnly' ,true);
        // }
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType , storeType){
        var url = '/common/popup/itemPop?callback=auroraBannerMng.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;
            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;
        }
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 상품조회팝업 콜백
     */
    addItemPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    setValid: function () {
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if($.jUtil.isEmpty($('#bannerNm').val())) {
            alert("새벽배송명을 입력해 주세요.");
            $('#bannerNm').focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 설정 가능합니다. \n전시 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD HH:mm:ss"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 베너 폼 검사
        var bannerDetail = $("#auroraBannerForm").serializeObject(true);
        for(var i=0; i < parseInt($("#template").val()); i++) {
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].title)) {
                alert("타이틀을 입력해 주세요." );
                return false;
            }
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].subTitle)) {
                alert("서브 타이틀을 입력해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].imgUrl)) {
                alert("이미지를 등록해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].linkInfo) && bannerDetail.linkList[i].linkType != 'NONE') {
                alert("링크정보를 등록해 주세요." );
                return false;
            }
            if(bannerDetail.linkList[i].dlvStoreYn == 'Y' && $.jUtil.isEmpty(bannerDetail.linkList[i].dlvStoreId) && bannerDetail.linkList[i]) {
                alert("택배점을 선택해 주세요");
                return false;
            }
        }
        return true;
    },
    /**
     * 입력 폼 초기화 ( 등록 또는 검색 후 )
     */
    resetBannerForm : function() {
        $("#template").val('1').trigger('change');
        $('#siteType').val('HOME');
        $('#dispYn').val('Y');
        $('#dispKind').val('PC');
    }
};

/**
 * 파트너공지 팝업 이미지
 */
auroraBannerMng.img = {
    imgDiv : null,
    bannerProcessMap : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        auroraBannerMng.img.resetImg();
        auroraBannerMng.img.setImgProcessKey();
        auroraBannerMng.img.imgDiv = null;
    },
    setImgProcessKey : function() {
        /**
         *  AuroraBannerType1	1200*450
         *  AuroraBannerType2	590*450
         *  AuroraBannerType3	790*450
         *  AuroraBannerType4	390*450
         *  AuroraBannerType5	390*220
         *  AuroraBannerType6	290*450
         *  AuroraBannerType7	350*200
         *  AuroraBannerType8	750*460
         **/
        auroraBannerMng.img.bannerProcessMap =
            [
                [ "AuroraBannerType1" ],                      // 1단 기본 배너
                [ "AuroraBannerType2", "AuroraBannerType2" ], // 2단 기본 배너
                [ "AuroraBannerType3", "AuroraBannerType4" ], // 2단 좌측강조 배너
                [ "AuroraBannerType4", "AuroraBannerType4", "AuroraBannerType4" ], // 3단 기본 배너
                [ "AuroraBannerType3", "AuroraBannerType5", "AuroraBannerType5" ], // 3단 좌측강조 배너
                [ "AuroraBannerType6", "AuroraBannerType2", "AuroraBannerType6" ], // 3단 중앙강조 배너
                [ "AuroraBannerType5", "AuroraBannerType5", "AuroraBannerType4", "AuroraBannerType5", "AuroraBannerType5" ],                     // 5단 중앙 강조
                [ "AuroraBannerType5", "AuroraBannerType5", "AuroraBannerType5", "AuroraBannerType5", "AuroraBannerType5", "AuroraBannerType5" ] // 6단 기본 배너
            ];
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function() {
        $('.uploadType').each(function(){ auroraBannerMng.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');
        let params = {
            processKey : auroraBannerMng.img.getProcessKey(),
            mode : 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        auroraBannerMng.img.setImg(auroraBannerMng.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        auroraBannerMng.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        auroraBannerMng.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    getProcessKey : function() {
        var processKey = 'AuroraBannerType8';
        var device = $('#dispKind').val();
        var template = $('#template').val();
        var bannerType = $('#bannerType').val();
        var divisionValue = auroraBannerMng.img.imgDiv.find('.division').val();

        if(device === 'PC') {
            switch (template) {
                case "1" :
                    processKey = 'AuroraBannerType1';
                    break;
                case "2" :
                    if(bannerType === '2N') {
                        processKey = auroraBannerMng.img.bannerProcessMap[1][divisionValue];
                    } else {
                        processKey = auroraBannerMng.img.bannerProcessMap[2][divisionValue];
                    }
                    break;
                case "3" :
                    if(bannerType === '3CP') {
                        processKey = auroraBannerMng.img.bannerProcessMap[5][divisionValue];

                    } else if(bannerType === '3LP') {
                        processKey = auroraBannerMng.img.bannerProcessMap[4][divisionValue];
                    } else {
                        processKey = auroraBannerMng.img.bannerProcessMap[3][divisionValue];
                    }
                    break;
                case "5" :
                    processKey = auroraBannerMng.img.bannerProcessMap[6][divisionValue];
                    break;
                case "6" :
                    processKey = auroraBannerMng.img.bannerProcessMap[7][divisionValue];
                    break;
                default :
                    processKey = 'AuroraBannerType8';
                    break;
            }
        }

        return processKey;
    }
};