/**
 * 상품관리 > 마켓연동상품관리 > 외부연동 설정관리
 */
/**
 * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정 > 공통그리드
 */
let marketItemSettingListGrid = {
    gridView: new RealGridJS.GridView("marketItemSettingListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marketItemSettingListGrid.initGrid();
        marketItemSettingListGrid.initDataProvider();
        marketItemSettingListGrid.event();
    },
    initGrid: function () {
        marketItemSettingListGrid.gridView.setDataSource(
                marketItemSettingListGrid.dataProvider);
        marketItemSettingListGrid.gridView.setStyles(
                MarketItemSettingListGridBaseInfo.realgrid.styles);
        marketItemSettingListGrid.gridView.setDisplayOptions(
                MarketItemSettingListGridBaseInfo.realgrid.displayOptions);
        marketItemSettingListGrid.gridView.setColumns(
                MarketItemSettingListGridBaseInfo.realgrid.columns);
        marketItemSettingListGrid.gridView.setOptions(
                MarketItemSettingListGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        marketItemSettingListGrid.dataProvider.setFields(
                MarketItemSettingListGridBaseInfo.dataProvider.fields);
        marketItemSettingListGrid.dataProvider.setOptions(
                MarketItemSettingListGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        marketItemSettingListGrid.gridView.onDataCellClicked = function (gridView,
                index) {
            marketItemMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        let schDate = $('#schDate').val() == 'saleDt' ? 'regDt' : $(
                '#schDate').val();
        marketItemSettingListGrid.dataProvider.clearRows();
        marketItemSettingListGrid.dataProvider.setRows(dataList);
        marketItemSettingListGrid.gridView.orderBy([schDate],
                [RealGridJS.SortDirection.DESCENDING]);
    },
    excelDownload: function () {
        if (marketItemSettingListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName = "설정관리_" + _date.getFullYear() + (_date.getMonth() + 1)
                + _date.getDate();

        marketItemSettingListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

let partnerIdList = ['naver', 'eleven'];
var marketItemMain = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initDate('-30d', 'schStartDate', 'schEndDate');
        this.initDate('0', 'naverStopStartDt', 'naverStopEndDt');
        this.initDate('0', 'elevenStopStartDt', 'elevenStopEndDt');
        this.initDate('0', 'setStopPeriodStartDt', 'setStopPeriodEndDt');
        this.resetForm();
        this.initCategorySelectBox();
    },
    initDateTime: function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        marketItemMain.setDateTime = CalendarTime.datePickerRange(startId,
                endId, {timeFormat: _timeFormat}, true,
                {timeFormat: _endTimeFormat}, true);

        marketItemMain.setDateTime.setEndDateByTimeStamp(flag);
        marketItemMain.setDateTime.setStartDate(0);

        $("#" + endId).datepicker("option", "minDate", $("#" + startId).val());
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initDate: function (flag, startId, endId) {
        marketItemMain.setDate = Calendar.datePickerRange(startId, endId);

        if ($.jUtil.isEmpty(flag)) {
            marketItemMain.setDate.setEndDate(flag);
        } else {
            marketItemMain.setDate.setEndDate(0);
        }

        marketItemMain.setDate.setStartDate(flag);

        $("#" + endId).datepicker("option", "minDate", $("#" + startId).val());
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        marketItemMain.setDate = Calendar.datePickerRange('schStartDate',
                'schEndDate');

        marketItemMain.setDate.setEndDate(0);
        marketItemMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate",
                $("#schStartDate").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox: function () {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 검색 영역 초기화
     */
    resetSearchForm : function () {

        $('#itemSearchForm').resetForm();
        $("#schPartnerId").val("naver");
        marketItemMain.initSearchDate('-30d');
    },
    resetForm() {
        $('#itemNo').val('');
        $('#itemNoTxt, #itemNmTxt').text('');
        $('#itemSetForm, #itemsSetForm').resetForm();
        $('#setStopPeriodStartDt, #setStopPeriodEndDt, #naverStopStartDt, #naverStopEndDt, #elevenStopStartDt, #elevenStopEndDt').prop(
                'disabled', true).val('');
        $('#naverStopPeriodYn, #elevenStopPeriodYn, #setStopPeriodYn').prop(
                'disabled', true);
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {
        $('#searchResetBtn').bindClick(marketItemMain.resetSearchForm);
        $('#excelDownloadBtn').bindClick(marketItemSettingListGrid.excelDownload);

        $('#resetBtn').on('click', function () {
            marketItemMain.getDetail();
        });

        $('#searchBtn').on('click', function () {
            marketItemMain.search();
        });

        $('#resetBtn').on('click', function () {
            marketItemMain.resetForm();
        });

        $('#setMarketItemSettingBtn').on('click', function () {
            marketItemMain.setMarketItemSetting();
        });

        $('#setMarketItemsSettingBtn').on('click', function () {
            marketItemMain.setMarketItemsSetting();
        });

        $('.stopPeriodYn').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).closest('.stopPeriodArea').find('.hasDatepicker').prop(
                        'disabled', false);
            } else {
                $(this).closest('.stopPeriodArea').find('.hasDatepicker').val(
                        '').prop('disabled', true);
            }
        });

        $('#setStopPeriodYn').on('click', function () {
            if ($(this).is(':checked')) {
                $('#setStopPeriodStartDt, #setStopPeriodEndDt').prop('disabled',false);
            } else {
                $('#setStopPeriodStartDt, #setStopPeriodEndDt').prop('disabled',true);
            }
        });

        $('#setYn').on('change', function () {
            if ($(this).val() == 'Y') {
                $('#setStopPeriodYn').prop('disabled', false);
            } else {
                $('#setStopPeriodYn').prop('disabled', false).prop('checked',
                        true).trigger('click').prop('disabled', true);
            }
        });

        $('input:radio[name=naverSetYn]').on('click', function () {
            if ($(this).val() == 'Y') {
                $('#naverStopPeriodYn').prop('disabled', false);
            } else {
                $('#naverStopPeriodYn').prop('disabled', false).prop('checked',
                        true).trigger('click').prop('disabled', true);
            }
        });

        $('input:radio[name=elevenSetYn]').on('click', function () {
            if ($(this).val() == 'Y') {
                $('#elevenStopPeriodYn').prop('disabled', false);
            } else {
                $('#elevenStopPeriodYn').prop('disabled', false).prop('checked',
                        true).trigger('click').prop('disabled', true);
            }
        });
    },
    /**
     * 상품 검색
     */
    search: function () {
        // 날짜 체크
        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");
        var searchKeyword = $("#schKeyword").val();

        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(
                endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            marketItemMain.initSearchDate('-1d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(
                    moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        if ($('#schType').val() == 'itemNo') {
            $('#schKeyword').val(
                    $('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));
        } else {
            $('#schKeyword').val(
                    $('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ''));
        }

        CommonAjax.basic({
            url: '/item/market/getMarketItemSettingList.json',
            data: JSON.stringify($('#itemSearchForm').serializeObject(true)),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                marketItemSettingListGrid.setData(res);
                $('#itemTotalCount').html($.jUtil.comma(
                        marketItemSettingListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect: function (selectRowId) {
        let rowDataJson = marketItemSettingListGrid.dataProvider.getJsonRow(
                selectRowId);
        marketItemMain.getDetail(rowDataJson.itemNo);
    },
    getDetail(itemNo) {
        if (itemNo) {
            this.resetForm();

            CommonAjax.basic({
                url: '/item/market/getMarketItemSettingDetail.json',
                data: {itemNo: itemNo},
                method: 'GET',
                callbackFunc: function (data) {
                    $('#itemNo').val(itemNo);
                    $('#itemNoTxt').text(data.itemNo);
                    $('#itemNmTxt').text(data.itemNm);
                    for (let i in data.providerList) {
                        let provider = data.providerList[i];
                        let providerId = provider.providerId.replace('coop',
                                '');

                        $('input:radio[name="' + providerId + 'SetYn"][value="' + provider.useYn + '"]').trigger('click');
                        if (provider.stopPeriodYn == 'Y') {
                            $('input:checkbox[name=' + providerId + 'StopPeriodYn]').prop("checked", true);
                            $('#' + providerId + 'StopStartDt').prop('disabled', false).val(provider.stopStartDt);
                            $('#' + providerId + 'StopEndDt').prop('disabled', false).val(provider.stopEndDt);
                        }
                    }
                }
            })
        }
    },
    setMarketItemSetting() {
        if (!$.jUtil.isEmpty($('#itemNo').val())) {
            let form = $('#itemSetForm').serializeObject();
            let result = {'returnMsg': ''};
            form.providerList = [];

            for (let i in partnerIdList) {
                let _id = partnerIdList[i];
                let _setYn = $('input:radio[name=' + _id + 'SetYn]:checked').val();
                let _stopPeriodYn = $('#' + _id + 'StopPeriodYn:checked').val() == 'Y' ? 'Y' : 'N';
                let _stopStartDt = $('#' + _id + 'StopStartDt').val();
                let _stopEndDt = $('#' + _id + 'StopEndDt').val();

                if (_setYn == 'Y') {
                    if (_stopPeriodYn == 'Y' && ($.jUtil.isEmpty(_stopStartDt) || $.jUtil.isEmpty(_stopEndDt))) {
                        result.returnMsg = '중지기간 날짜를 입력해주세요.';
                        break;
                    }
                }

                form.providerList.push({
                    'providerId': _id,
                    'useYn': _setYn,
                    'stopPeriodYn': _stopPeriodYn,
                    'stopStartDt': _stopStartDt,
                    'stopEndDt': _stopEndDt
                });
            }

            if (!$.jUtil.isEmpty(result.returnMsg)) {
                return $.jUtil.alert(result.returnMsg);
            }

            CommonAjax.basic({
                url: '/item/market/setMarketItemSetting.json',
                data: JSON.stringify(form),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    alert(res.returnMsg);
                }
            });
        } else {
            $.jUtil.alert("상품을 선택해주세요.");
        }
    },
    setMarketItemsSetting: function () {

        if ($.jUtil.isEmpty($('#setPartnerId').val()) || $.jUtil.isEmpty(
                $('#setYn').val())) {
            return $.jUtil.alert('일괄설정을 확인해주세요.');
        }

        if ($('#setStopPeriodYn').is(":checked") && ($.jUtil.isEmpty(
                $('#setStopPeriodStartDt').val()) || $.jUtil.isEmpty(
                $('#setStopPeriodEndDt').val()))) {
            return $.jUtil.alert('중지기간을 확인해주세요.');
        }

        let rowArr = marketItemSettingListGrid.gridView.getCheckedItems();

        if (rowArr.length == 0) {
            return $.jUtil.alert('상품을 선택해주세요.');
        }

        if (confirm("선택한 상품의 상품정보를 일괄적용하시겠습니까?")) {

            let form = $('#itemsSetForm').serializeObject(true);
            form.itemNoList = marketItemMain.getItemNoList();

            CommonAjax.basic({
                url: '/item/market/setMarketItemsSetting.json',
                data: JSON.stringify(form),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    $('#itemsSetForm').resetForm();
                    $('#setStopPeriodStartDt, #setStopPeriodEndDt').prop('disabled',true);
                    alert(res.returnMsg);
                }
            });
        }
    },
    getItemNoList : function() {
        let rowArr = marketItemSettingListGrid.gridView.getCheckedItems();
        let itemNoList = [];

        for (let i in rowArr) {
            const rowData = marketItemSettingListGrid.gridView.getValues(rowArr[i]);
            itemNoList.push(rowData.itemNo);
        }

        return itemNoList
    },
    setMarketItemSend : function () {
        let rowArr = marketItemSettingListGrid.gridView.getCheckedItems();

        if (rowArr.length == 0) {
            return $.jUtil.alert('상품을 선택해주세요.');
        } else if (rowArr.length > 1000) {
            return $.jUtil.alert('상품을 1000건 이하로 선택해주세요.');
        }

        if (confirm("선택한 상품의 상품정보를 각 외부연동 사이트로 즉시전송하시겠습니까?\n데이터의 양에 따라 시간이 소요될수 있습니다.")) {
            CommonAjax.basic({
                url: '/item/market/setMarketItemSend.json',
                data: JSON.stringify({itemList:marketItemMain.getItemNoList()}),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    alert(res.returnMsg);
                }
            });

        }
    }
};


