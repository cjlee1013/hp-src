$(document).ready(function () {
    auroraCategoryItemMng.init();
    auroraCategoryItemGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var isEdit = false;

var auroraCategoryItemGrid = {
    gridView: new RealGridJS.GridView("auroraCategoryItemGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        auroraCategoryItemGrid.initGrid();
        auroraCategoryItemGrid.initDataProvider();
        auroraCategoryItemGrid.event();
    },
    initGrid: function () {
        auroraCategoryItemGrid.gridView.setDataSource(auroraCategoryItemGrid.dataProvider);

        auroraCategoryItemGrid.gridView.setStyles(auroraCategoryItemGridBaseInfo.realgrid.styles);
        auroraCategoryItemGrid.gridView.setDisplayOptions(auroraCategoryItemGridBaseInfo.realgrid.displayOptions);
        auroraCategoryItemGrid.gridView.setColumns(auroraCategoryItemGridBaseInfo.realgrid.columns);
        auroraCategoryItemGrid.gridView.setOptions(auroraCategoryItemGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        auroraCategoryItemGrid.dataProvider.setFields(auroraCategoryItemGridBaseInfo.dataProvider.fields);
        auroraCategoryItemGrid.dataProvider.setOptions(auroraCategoryItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        auroraCategoryItemGrid.gridView.onDataCellClicked = function (gridView, index) {
            auroraCategoryItemMng.getGrid(index.dataRow);
        };
    },
    setData: function (dataList) {
        auroraCategoryItemGrid.dataProvider.clearRows();
        auroraCategoryItemGrid.dataProvider.setRows(dataList);
    }
};

var auroraCategoryItemMng = {
    init: function () {
        this.event();
        auroraCategoryItemMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        auroraCategoryItemMng.setCategorySelectBox(2, '', '', $('#formCateCd2'));
    },
    event: function () {
        // 검색 버튼
        $('#searchBtn').click(function () {
            auroraCategoryItemMng.search();
        });

        // 상품 검색 버튼
        $('#addItemPopUp').click(function () {
            auroraCategoryItemMng.addItemPopUp();
        });

        // 카테고리 상품 저장 버튼 클릭
        $('#setCategoryItemBtn').click(function () {
            auroraCategoryItemMng.setCategoryItem();
        });

        //카테고리 리셋 버튼 클릭
        $('#resetBtn').click(function () {
            auroraCategoryItemMng.formReset();
        });

        //카테고리 검색 리셋 버튼 클릭
        $('#searchResetBtn').click(function () {
            auroraCategoryItemMng.searchFormReset();
        });

        //뎁스 변경시
        $('#depth').change(function () {
            $('#formCateCd1, #formCateCd2, #formCateCd3').prop('disabled', false);
            $('select[id^="formMappingCateCd"]').prop('disabled', true);

            switch ($(this).val()) {
                case '1' :
                    $('#formCateCd1').prop('disabled', true);
                case '2' :
                    $('#formCateCd2').prop('disabled', true);
            }
        });

        $('#addItemPopUpByExcel').bindClick(auroraCategoryItemMng.addItemPopUpByExcel); // 상품조회 팝업 ( 일괄등록 )
    },
    /**
     * 상품조회팝업 ( 일괄등록 )
     */
    addItemPopUpByExcel : function() {
        var target = "auroraCategoryItemPop";
        var callback = "auroraCategoryItemMng.itemExcelPopCallback";
        var url = "/item/popup/uploadAuroraCategoryItemPop?callback=" + callback;

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function () {
        auroraCategoryItemMng.search();
    },
    search: function () {
        CommonAjax.basic({
            url: '/item/auroraCategoryItem/getAuroraCategoryItem.json',
            data: $('#searchForm').serialize(),
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                auroraCategoryItemGrid.setData(res);
                $('#auroraCategoryItemCnt').html($.jUtil.comma(auroraCategoryItemGrid.gridView.getItemCount()));
            }
        });
    },
    searchFormReset: function( ) {
        $('#searchCateCd1').val('').change();
        $('#searchType').val('itemNm');
        $('#searchKeyword, #searchDispYn').val('');
    },
    formReset: function () {
        $('#itemNo, #itemNm').val('');
        $('#dispYn').val('Y');

        auroraCategoryItemMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        auroraCategoryItemMng.setCategorySelectBox(2, '', '', $('#formCateCd2'));

        $('#formCateCd1, #formCateCd2').prop('disabled', false);

        isEdit = false;
    },
    getGrid: function (selectRowId) {
        var rowDataJson = auroraCategoryItemGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').val(rowDataJson.itemNo);
        $('#itemNm').val(rowDataJson.itemNm);
        $('#dispYn').val(rowDataJson.dispYn);

        auroraCategoryItemMng.setCategorySelectBox(1, '', rowDataJson.lcateCd, $('#formCateCd1'));
        auroraCategoryItemMng.setCategorySelectBox(2, rowDataJson.lcateCd, rowDataJson.mcateCd, $('#formCateCd2'));

        $('#formCateCd1, #formCateCd2').prop('disabled', true);

        isEdit = true;
    },
    setCategoryItem : function() {
        if(!auroraCategoryItemMng.setValid()){
            return false;
        }

        CommonAjax.basic({
            url : '/item/auroraCategoryItem/setAuroraCategoryItem.json',
            data : JSON.stringify($('#auroraCategoryItemForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                auroraCategoryItemMng.formReset();
                auroraCategoryItemMng.search();
            }
        });
    },
    setCategorySelectBox: function (depth, parentCateCd, cateCd, selector) {
        $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
        if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
            CommonAjax.basic({
                url: '/item/auroraCategory/getAuroraCategory.json',
                data: {
                    lCateCd: parentCateCd
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $(selector).css('-webkit-padding-end', '30px');
                    if (depth == 1) {
                        $.each(resData, function (idx, val) {
                            var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                            $(selector).append(
                                '<option value="' + val.cateCd + '" ' + selected + '>'
                                + val.cateNm
                                + '</option>');
                        });

                        $('#formCateCd1').html($(selector).html());
                    } else {
                        $.each(resData[0].rows, function (idx, val) {
                            var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                            $(selector).append(
                                '<option value="' + val.cateCd + '" ' + selected + '>'
                                + val.cateNm
                                + '</option>');
                        });
                    }
                }
            });
        }
    },
    changeCategorySelectBox: function (depth, prefix) {
        var cateCd = $('#' + prefix + depth).val();
        this.setCategorySelectBox(depth + 1, cateCd, '', $('#' + prefix + (depth + 1)));
        $('#' + prefix + (depth + 1)).change();
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function (){
        if(isEdit) {
            alert("상품 수정은 불가능 합니다. 전시 여부만 수정해 주세요.");
            return false;
        }
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=auroraCategoryItemMng.applyPopupCallback&isMulti=N&mallType=TD&storeType=AURORA' , 1084, 650);
    },
    applyPopupCallback : function(resDataArr) {
        $('#itemNo').val(resDataArr[0].itemNo);
        $('#itemNm').val(resDataArr[0].itemNm);
    },
    setValid: function () {
        if($.jUtil.isEmpty($('#itemNo').val())) {
            alert("상품을 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#formCateCd1').val())) {
            alert("대분류를 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#formCateCd2').val())) {
            alert("중분류를 선택해 주세요.");
            return false;
        }
        return true;
    }
};