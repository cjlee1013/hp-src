/**
 * 상품관리 > 점포상품관리 > 점포상품 취급중지 일괄설정
 */
$(document).ready(function() {
    stopDealGrid.init();
    stopDealSearch.init();
    CommonAjaxBlockUI.global();
});

// 점포상품 취급중지 그리드
var stopDealGrid = {

    gridView : new RealGridJS.GridView("stopDealGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        stopDealGrid.initGrid();
        stopDealGrid.initDataProvider();
    },
    initGrid : function () {
        stopDealGrid.gridView.setDataSource(stopDealGrid.dataProvider);

        stopDealGrid.gridView.setStyles(stopDealGridBaseInfo.realgrid.styles);
        stopDealGrid.gridView.setDisplayOptions(stopDealGridBaseInfo.realgrid.displayOptions);
        stopDealGrid.gridView.setColumns(stopDealGridBaseInfo.realgrid.columns);
        stopDealGrid.gridView.setOptions(stopDealGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        stopDealGrid.dataProvider.setFields(stopDealGridBaseInfo.dataProvider.fields);
        stopDealGrid.dataProvider.setOptions(stopDealGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        stopDealGrid.dataProvider.clearRows();
        stopDealGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(stopDealGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "stopDeal_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        stopDealGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

var stopDealSearch = {
    init : function () {
        stopDealSearch.searchFormReset();
        stopDealSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(stopDealSearch.searchStopDealList);
        $("#excelDownBtn").bindClick(stopDealGrid.excelDownload);
    }
    , valid : function() {

        var schKeyword = $('#schKeyword').val();
        var pattern = /[^0-9,]/g;
        if(pattern.test(schKeyword)) {
            alert('숫자 또는 ","를 입력하세요');
            $('#schKeyword').focus();
            return false;
        }

        var isChecked = $("input:radio[name=storeType]:checked").val();
        if ($.jUtil.isEmpty(isChecked))
        {

            alert("점포유형을 선택하세요"+isChecked);
            $('#storeType').focus();
            return false;
        }

        return true;
    }
    /**
     * 점포상품 취급중지 현황 조회
     */
    , searchStopDealList : function () {

        $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));

        if (stopDealSearch.valid()) {

            // 조회 개수 초기화
            $("#stopDealTotalCnt").html("0");

            // 조회
            CommonAjax.basic({
                url: "/item/stopDeal/getItemList.json?" + $('#searchForm').serialize()
                , data: null
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    stopDealGrid.setData(res);

                    $("#stopDealTotalCnt").html($.jUtil.comma(stopDealGrid.dataProvider.getRowCount()));
                }
            });
        }
    }

};

var stopDealExcel = {
    init : function () {

    }
    , openExcelPopup : function() {
        windowPopupOpen('/item/popup/stopDealExcelReadPop?callback=stopDealExcel.itemExcelPopCallback','stopDealpop',630,350);
    }
    , itemExcelPopCallback : function (resDataArr) {
        // 전송단위
        var sendDataCount = 1000;
        // 엑셀 데이터 숫자..
        var _ExcelDataSize = resDataArr.ExcelDataSize;

        // resDataArr.data는 유효한 데이터 ..
        var totalExcelDataCnt = resDataArr.data.length;

        // if (confirm("점포별 취급중지여부를 일괄 업로드하시겠습니까?")) {

            // 엑셀에 데이터가 없는 경우..
            if ( _ExcelDataSize < 1) {
                console.log((new Date()) + '(CASE1):엑셀 작업 시작.(' + totalExcelDataCnt + ')건');

                alert('처리할 데이터가 없습니다.');

                stopDealGrid.setData(null);
                $("#stopDealTotalCnt").html($.jUtil.comma(0));

            // 엑셀에 데이터가 있으나 유효한 데이터가 없는 경우..
            } else if ( totalExcelDataCnt < 1 ) {

                var _msg = '0건이 완료되었습니다.'
                _msg = _msg + "(" + $.jUtil.comma(_ExcelDataSize) + "건 실패. 업로드한 엑셀 파일에 오류가 없는지 확인하시고 다시 한 번 등록해 주세요.)";

                alert(_msg);

                console.log((new Date()) + '(CASE2):엑셀 작업 시작.(' + totalExcelDataCnt + ')건');

                stopDealGrid.setData(null);
                $("#stopDealTotalCnt").html($.jUtil.comma(0));

            // 엑셀에 데이터 있고, 호출할 데이터도 있는 경우...
            } else {

                console.log((new Date()) + '(CASE3):엑셀 작업 시작.(' + totalExcelDataCnt + ')건');

                // timeStamp 생성하고..
                var dt = new Date();
                // 작업용키 : 년월일시분초(2차리씩)+밀리초(3자리)
                var workId = dt.getFullYear().toString().substring(2,4) + (dt.getMonth()+1).toString().padStart(2,'0') + dt.getDate().toString()  + dt.getHours().toString().padStart(2,'0') + dt.getMinutes().toString().padStart(2,'0') + dt.getSeconds().toString().padStart(2,'0') + dt.getMilliseconds();

                // 전송용 Object
                var sendData = new Object();
                sendData.workId = workId.toString();

                // 전송 데이터 단위로 루프
                var loopCount = Math.ceil( totalExcelDataCnt / sendDataCount );

                for(var i = 0; i < loopCount ; i++) {

                    // console.log ('slice:'+i*sendDataCount+","+(i+1) * sendDataCount);
                    // 받은 데이터 전송 단위로 신규 array
                    var stopDealList = resDataArr.data.slice(i*sendDataCount , (i+1) * sendDataCount);
                    sendData.stopDealListSet = stopDealList;

                    if ( i == ( loopCount -1 ) ) { // 마지막인지 체크
                        sendData.step = 'E';
                    } else {
                        sendData.step = 'I';
                    }

                    console.log ( (new Date()) + ':i['+i+']');

                    // 전송
                    CommonAjax.basic({
                        url: "/item/stopDeal/setStopDealList.json"
                        , data: JSON.stringify(sendData)
                        , method: "POST"
                        , successMsg: null
                        , async : false
                        , contentType: 'application/json'
                        , callbackFunc: function (res) {
                            // 나중에 엑셀 디버깅 용
                            console.log ( 'i='+i + ":" + JSON.stringify(res));
                        }
                    });
                }

                console.log ( (new Date()) + ':엑셀 작업 처리 끝.');

                // 그리고 처리한 건에 대해서 재 조회를 한다..
                CommonAjax.basic({
                    url: "/item/stopDeal/getStopDealList.json?workId=" + workId
                    , data: null
                    , method: "GET"
                    , successMsg: null
                    , callbackFunc: function (res) {
                        stopDealGrid.setData(res);

                        var successCnt = stopDealGrid.dataProvider.getRowCount();

                        var failCnt = _ExcelDataSize - successCnt;
                        var _msg = $.jUtil.comma(successCnt) +'건이 완료되었습니다.'
                        if ( _ExcelDataSize != stopDealGrid.dataProvider.getRowCount()) {
                            _msg = _msg + "(" + $.jUtil.comma(failCnt) + "건 실패. 업로드한 엑셀 파일에 오류가 없는지 확인하시고 다시 한 번 등록해 주세요.)";
                        }
                        alert(_msg);

                        $("#stopDealTotalCnt").html($.jUtil.comma(successCnt));
                    }
                });
                console.log ( (new Date()) + ':엑셀 작업 조회');
            }
        //}

        return true;

    }
}