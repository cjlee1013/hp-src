/**
 * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정 > 공통그리드 (TD/DS)
 */
let itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        if (mallType == 'TD' && menuType == 'detail') {
            itemListGridBaseInfo.realgrid.options.checkBar.visible = false;
        } else {
            itemListGridBaseInfo.realgrid.options.checkBar.visible = true;
        }

        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
        if (mallType == 'TD') {
            if (menuType == 'basic') {
                itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("itemParent"), "visible", true);
            }
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("relationNo"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("businessNm"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("sellerItemCd"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("salePrice"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("saleStartDt"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("saleEndDt"), "visible", false);
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("dcPrice"), "visible", false);
        } else {
            itemListGrid.gridView.setColumnProperty(itemListGrid.gridView.columnByName("itemDivision"), "visible", false);
        }
    },
    initGrid : function() {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            item.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        let schDate = $('#schDate').val() == 'saleDt' ? 'regDt' : $('#schDate').val();
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
        itemListGrid.gridView.orderBy([schDate],[RealGridJS.SortDirection.DESCENDING]);
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName =  "상품관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};
if (mallType == 'TD') {
    // 옵션 그리드
    var optListGrid = {
        selectedRowId : null,
        gridView : new RealGridJS.GridView("optListGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            optListGrid.initGrid();
            optListGrid.initDataProvider();
            optListGrid.event();
            if (menuType == 'detail') {
                optListGrid.gridView.setColumnProperty(optListGrid.gridView.columnByName("dispYnNm"), "visible", true);
            }
        },
        initGrid : function() {
            optListGrid.gridView.setDataSource(optListGrid.dataProvider);

            optListGrid.gridView.setStyles(optListGridBaseInfo.realgrid.styles);
            optListGrid.gridView.setDisplayOptions(optListGridBaseInfo.realgrid.displayOptions);
            optListGrid.gridView.setColumns(optListGridBaseInfo.realgrid.columns);
            optListGrid.gridView.setOptions(optListGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            optListGrid.dataProvider.setFields(optListGridBaseInfo.dataProvider.fields);
            optListGrid.dataProvider.setOptions(optListGridBaseInfo.dataProvider.options);
        },
        event : function() {
            $('.optMoveCtrl').on('click', function() {
                optListGrid.optGridMoveRow(this);
            });

            $('.optAddMoveCtrl').on('click', function() {
                optAddListGrid.optAddGridMoveRow(this);
            });

            // 그리드 선택
            optListGrid.gridView.onDataCellClicked = function(gridView, index) {
                optListGrid.selectedRowId = index.dataRow;
                item.opt.gridRowSelect(index.dataRow);
            };
        },
        // 옵션 그리드 row 이동
        optGridMoveRow : function(obj) {
            let checkedRows = optListGrid.gridView.getCheckedRows(false);

            if (checkedRows.length == 0) {
                alert('순서변경할 옵션을 선택해 주세요.');
                return false;
            }

            realGridMoveRow(optListGrid, $(obj).attr('key'), checkedRows);
        },
        setData : function(dataList) {
            optListGrid.dataProvider.clearRows();
            optListGrid.dataProvider.setRows(dataList);
        },
        excelDownload: function() {
            if(optListGrid.gridView.getItemCount() == 0) {
                alert("검색 결과가 없습니다.");
                return false;
            }

            let _date = new Date();
            let fileName =  "옵션_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            optListGrid.gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }
    };
}