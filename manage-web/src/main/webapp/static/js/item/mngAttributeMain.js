/**
 * 상품속성관리 > 상품속성관리 > 상품속성관리
 */
$(document).ready(function() {
    attributeGroupListGrid.init();
    attributeGroupList.init();
    attributeListGrid.init();
    attributeList.init();
    CommonAjaxBlockUI.global();
});

// 속성분류정보
var attributeGroupListGrid = {
    gridView : new RealGridJS.GridView("attributeGroupListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        attributeGroupListGrid.initGrid();
        attributeGroupListGrid.initDataProvider();
        attributeGroupListGrid.event();
    },
    initGrid : function() {
        attributeGroupListGrid.gridView.setDataSource(attributeGroupListGrid.dataProvider);
        attributeGroupListGrid.gridView.setStyles(attributeGroupListGridBaseInfo.realgrid.styles);
        attributeGroupListGrid.gridView.setDisplayOptions(attributeGroupListGridBaseInfo.realgrid.displayOptions);
        attributeGroupListGrid.gridView.setColumns(attributeGroupListGridBaseInfo.realgrid.columns);
        attributeGroupListGrid.gridView.setOptions(attributeGroupListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        attributeGroupListGrid.dataProvider.setFields(attributeGroupListGridBaseInfo.dataProvider.fields);
        attributeGroupListGrid.dataProvider.setOptions(attributeGroupListGridBaseInfo.dataProvider.options);
    },
    headerCtrl : function(gattrFlag) {
        var isAttr = gattrFlag == "ATTR" ? true : false;
        attributeGroupListGrid.gridView.setColumnProperty(attributeGroupListGrid.gridView.columnByName("gattrNm"), "visible", !isAttr);
        attributeGroupListGrid.gridView.setColumnProperty(attributeGroupListGrid.gridView.columnByName("gattrMngNm"), "visible", isAttr);
    },
    event : function() {
        // 그리드 선택
        attributeGroupListGrid.gridView.onDataCellClicked = function(gridView, index) {
            attributeGroupList.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        attributeGroupListGrid.dataProvider.clearRows();
        attributeGroupListGrid.dataProvider.setRows(dataList);
    }
};

// 속성정보
var attributeListGrid = {
    gridView : new RealGridJS.GridView("attributeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        attributeListGrid.initGrid();
        attributeListGrid.initDataProvider();
        attributeListGrid.event();
    },
    initGrid : function() {
        attributeListGrid.gridView.setDataSource(attributeListGrid.dataProvider);

        attributeListGrid.gridView.setStyles(attributeListGridBaseInfo.realgrid.styles);
        attributeListGrid.gridView.setDisplayOptions(attributeListGridBaseInfo.realgrid.displayOptions);
        attributeListGrid.gridView.setColumns(attributeListGridBaseInfo.realgrid.columns);
        attributeListGrid.gridView.setOptions(attributeListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        attributeListGrid.dataProvider.setFields(attributeListGridBaseInfo.dataProvider.fields);
        attributeListGrid.dataProvider.setOptions(attributeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        attributeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            attributeList.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        attributeListGrid.dataProvider.clearRows();
        attributeListGrid.dataProvider.setRows(dataList);
    }

};

// 속성분류정보
var attributeGroupList = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initAjaxForm();
        this.searchFormReset();
        this.setAttributeGroupReset();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchGroupBtn').bindClick(attributeGroupList.search);
        $('#searchResetGroupBtn').bindClick(attributeGroupList.searchFormReset);
        $('#setAttributeGroupBtn').bindClick(attributeGroupList.setAttributeGroup);
        $('#resetAttributeGroupBtn').bindClick(attributeGroupList.setAttributeGroupReset);
        $("#gattrDispType").bindChange(attributeGroupList.setDispTypeArea);
        // 구분이 검색테그일 경우 선택방식을 사용하지 않는다. ( 따라서, 디비 컬림이 Not Null 이므로 단일로 선택되게 강제 한다. )
        $('#gattrType').change(function() {
            var gattrType = $('#gattrType option:selected').val();
            if(gattrType === 'TAG') {
                $('#multiYn').prop('disabled', true);
                $('#multiYn').val('N');

                $("#gattrMngNm").val("");
                $("#gattrMngNmTr").hide();
                $("#gattrDispType").val("");
                $("#gattrDispTypeTr").hide();
            } else {
                $('#multiYn').val('');
                $('#multiYn').prop('disabled', false);

                $("#gattrMngNmTr").show();
                $("#gattrDispTypeTr").show();
            }
        });
        $("#searchGattrType").bindChange(attributeGroupList.searchTypeViewCtrl);
    },
    searchTypeViewCtrl : function() {
        var searchGattrType = $("#searchGattrType").val();

        switch (searchGattrType) {
            case "ATTR" :
                $("#searchType").find("option[value='gattrNm']").remove();

                if ($("#searchType").find("option[value='gattrMngNm']").index() < 0) {
                    $("#searchType").prepend("<option value='gattrMngNm'>관리분류명</option>");
                }
                break;
            default :
                $("#searchType").find("option[value='gattrMngNm']").remove();

                if ($("#searchType").find("option[value='gattrNm']").index() < 0) {
                    $("#searchType").prepend("<option value='gattrNm'>노출분류명</option>");
                }
                break;
        }

        $("#searchType option:eq(0)").attr("selected", "selected");
        attributeGroupListGrid.headerCtrl(searchGattrType);
    },
    /**
     * 노출유형에 따른 입력 폼 변경
     */
    setDispTypeArea : function(){
        var dispType = $("#gattrDispType").val();
        //색상, 이미지 입력란 초기화
        attributeList.resetAddColor();
        attributeList.resetAddImg();

        switch (dispType) {
            case 'IMG' :
                $("#addImg").show();
                $("#addColor").hide();
                break;
            case 'CLR' :
                $("#addColor").show();
                $("#addImg").hide();
                break;
            default :
                $("#addImg").hide();
                $("#addColor").hide();
                break;
        }
    },
    /**
     * 속성분류정보 검색
     */
    search : function() {
        CommonAjax.basic({url:'/item/mngAttribute/getMngAttributeGroupList.json?' + $('#mngAttributeSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            attributeGroupList.setAttributeGroupReset();
            attributeGroupListGrid.setData(res);
            attributeList.resetGrid();
            attributeList.resetForm();
            $('#attributeGroupTotalCount').html($.jUtil.comma(attributeGroupListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = attributeGroupListGrid.dataProvider.getJsonRow(selectRowId);
        var gattrNo = rowDataJson.gattrNo;
        var gattrType = rowDataJson.gattrType;
        var dispType  = rowDataJson.dispType;

        $('#gattrNo').val(gattrNo);
        $('#mGattrNo').val(gattrNo);
        $('#gattrType').val(gattrType).prop("disabled", true);
        $('#gattrNm').val(rowDataJson.gattrNm);
        if(gattrType === 'TAG') {
            $('#multiYn').prop('disabled', true);
            $('#multiYn').val('N');

            $('#gattrMngNm').val("");
            $("#gattrMngNmTr").hide();

            $('#gattrDispType').val("");
            $("#gattrDispTypeTr").hide();
        } else {
            $('#multiYn').prop('disabled', false);
            $('#multiYn').val(rowDataJson.multiYn);

            $('#gattrMngNm').val(rowDataJson.gattrMngNm);
            $("#gattrMngNmTr").show();

            //노출 유형에 따른 속성정보 입력창 변경
            $('#gattrDispType').val(dispType);
            $("#gattrDispTypeTr").show();
            attributeGroupList.setDispTypeArea();
        }

        $("#gattrUseYn").val(rowDataJson.useYn);

        //노출 유형에 따른 속성정보 입력창 변경
        $('#gattrDispType').val(dispType);
        attributeGroupList.setDispTypeArea();

        $('#useYn').val(rowDataJson.useYn);

        attributeList.getAttributeList(gattrNo);
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchGattrType').val('').change();
        $('#searchKeyword').val('');
        $('#searchType').val('gattrNm');
        $('#searchUseYn').val('');
    },
    /**
     * 등록/수정폼 초기화
     */
    setAttributeGroupReset : function() {
        $('#gattrNo').val('');
        $('#gattrNm').val('');
        $('#gattrMngNm').val('');
        $('#multiYn').val('');
        $('#multiYn').prop('disabled', false);
        $('#gattrUseYn').val('Y');
        attributeList.resetAddColor();
        attributeList.resetAddImg();
        $('#gattrType').val('').prop("disabled", false).change();
        $('#gattrDispType').val('').change();

    },
    /**
     * 속성분류정보 등록/수정
     */
    setAttributeGroup : function() {
        if($.jUtil.isEmpty($('#gattrType').val())) {
            return $.jUtil.alert("속성 구분을 선택해주세요.")
        }

        if($.jUtil.isEmpty($('#gattrNm').val())) {
            return $.jUtil.alert("노출분류명을 입력해주세요.")
        }

        // 구분 = 상품속성 & 관리분류명 입력 확인
        if($("#gattrType").val() == "ATTR" && $.jUtil.isEmpty($('#gattrMngNm').val())) {
            return $.jUtil.alert("관리분류명을 입력해주세요.")
        }

        if($.jUtil.isEmpty($('#multiYn').val())) {
            return $.jUtil.alert("속성 선택방식을 선택해주세요.")
        }

        if($("#gattrType").val() == "ATTR" && $.jUtil.isEmpty($('#gattrDispType').val())) {
            return $.jUtil.alert("노출유형을 선택해주세요.")
        }

        $('#setAttributeGroupForm').submit();
    },
    /**
     * 속성분류정보 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#setAttributeGroupForm').ajaxForm({
            url: '/item/mngAttribute/setMngAttributeGroup.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                attributeGroupList.search();
            },
            error: function(e) {
                if(e.responseJSON.data == null) {
                    alert(e.responseJSON.returnMessage);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
};

//속성정보
var attributeList = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
        this.resetForm();
    },
    /**
     * 속성분류 리스트 조회
     */
    getAttributeList : function(gattrNo) {
        CommonAjax.basic({url:'/item/mngAttribute/getMngAttributeList.json?', data:{ gattrNo : gattrNo }, method:"GET", successMsg:null, callbackFunc:function(res) {
            attributeList.resetForm();
            attributeListGrid.setData(res);
            $('#attributeTotalCount').html($.jUtil.comma(attributeListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 속성분류 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#setAttributeForm').ajaxForm({
            url: '/item/mngAttribute/setMngAttribute.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                attributeList.getAttributeList(resData.returnKey);
                attributeList.resetForm();
            },
            error: function(e) {
                if(e.responseJSON.data == null) {
                    alert(e.responseJSON.returnMessage);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#resetAttributeBtn').bindClick(attributeList.resetForm);
        $('#setAttributeBtn').bindClick(attributeList.setAttribute);
        $('#colorCode').focusout(attributeList.setColor);
        $('input[name="fileArr"]').change(function() {
            attributeList.img.addImg();
        });
        $('#colorType').change(attributeList.resetColorType);

    },

    resetForm : function() {
        $('#attrNo').val('');
        $('#attrNm').val('');
        $('#priority').val('');
        $('#attrUseYn').val('Y');
        attributeList.resetAddColor();
        attributeList.resetAddImg();
    },
    /**
     * 그리드 리스트 초기화
     */
    resetGrid : function(){
        attributeListGrid.dataProvider.clearRows();
        $('#attributeTotalCount').html($.jUtil.comma(attributeListGrid.gridView.getItemCount()));
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = attributeListGrid.dataProvider.getJsonRow(selectRowId);
        $('#attrNo').val(rowDataJson.attrNo);
        $('#attrNm').val(rowDataJson.attrNm);
        $('#priority').val(rowDataJson.priority);
        $('#attrUseYn').val(rowDataJson.useYn);

        var dispType = $("#gattrDispType").val();
        if(dispType === 'IMG'){

            attributeList.img.setImg(attributeList.img.imgDiv, {
                'imgNm' :  rowDataJson.imgNm,
                'imgUrl': rowDataJson.imgUrl,
                'imgWidth': rowDataJson.imgWidth,
                'imgHeight': rowDataJson.imgHeight,
                /*'src'   : hmpImgUrl + rowDataJson.imgUrl+"?"+Math.floor(Math.random()*1000)*/
                'src'    : hmpImgUrl + "/provide/view?processKey=ItemAttribute&fileId=" + rowDataJson.imgUrl

            });

        } else if(dispType === 'CLR'){
            $("#colorType").val(rowDataJson.colorType);
            $("#colorCode").val(rowDataJson.colorCode);
            attributeList.setColor();
        }

    },
    /**
     * 속성 분류 등록/수정
     */
    setAttribute: function() {
        if ($.jUtil.isEmpty($("#mGattrNo").val())) {
            return $.jUtil.alert("속성을 등록할 속성분류를 선택해주세요.");
        }

        if($.jUtil.isEmpty($('#attrNm').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#attrNm').focus();
            return false;
        }

        // 검색태그일 경우 6자리, 상품속성일 경우 12자리
        if ($("#gattrType").val() == "TAG" && $("#attrNm").val().length > 6) {
            return $.jUtil.alert("검색태그의 속성명은 최대 6자까지 입력가능합니다.");
        } else if ($("#gattrType").val() == "ATTR" && $("#attrNm").val().length > 12) {
            return $.jUtil.alert("상품속성의 속성명은 최대 12자까지 입력가능합니다.");
        }

        if($.jUtil.isEmpty($('#priority').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#priority').focus();
            return false;
        }

        var dispType =  $('#gattrDispType').val();
        $('#dispType').val(dispType);
        if(dispType === 'IMG'){ // 이미지형

            if($.jUtil.isEmpty($('#imgUrl').val())) {
                alert("이미지를 등록해 주세요.");
                return false;
            }

        } else if(dispType === 'CLR') {    // 색상형

            var colorType = $('#colorType').val();

            if(colorType === 'S'){  //단일 생삭의 경우 색상코드 입력 필수

                if($.jUtil.isEmpty($('#colorCode').val())){

                    alert("색상코드를 등록해 주세요.");
                    $('#colorCode').focus();
                    return false;

                }
            }

        }

        $('#setAttributeForm').submit();
    },
    /**
     * 이미지 등록창 초기화
     */
    resetAddImg : function(){
        attributeList.img.resetImg();
    },
    /**
     * 색상 등록창 초기화
     */
    resetAddColor : function(){
        $("#colorType").val('S');
        $("#colorCode").val('');
        attributeList.setColor();
    },
    /**
     * 색상코드 변경에 따른 색상 확인 영역 .변경
     */
    setColor : function(){

        var colorType = $('#colorType').val();
        if(colorType === 'M'){

            $('#colorCode').val('');
            $('#colorCode').prop('readonly', true);
            $('#singleColor').hide();
            $('#mixColor').show();

        } else {
            $('#colorCode').prop('readonly', false);
            $('#singleColor').show();
            $('#mixColor').hide();
            $("#singleColor").css("background-color", $('#colorCode').val());
        }


    },
    /**
     * 색상타입 변경에 따른 색상 입력창 변경
     */
    resetColorType :  function(){
        $("#colorCode").val('');
        attributeList.setColor();

    },
};
attributeList.img = {
    imgDiv: $('#uploadImg'),
    /**
     * 이미지 초기화
     */
    init: function () {
        attributeList.img.resetImg();
        attributeList.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg: function () {
        $('.uploadType').each(function () {
            attributeList.img.setImg($(this))
        });
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg: function (obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg: function () {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var file = $('#itemFile');
        var params = {
            processKey: 'ItemAttribute',
            mode: 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        attributeList.img.setImg(attributeList.img.imgDiv, {
                            'imgNm': f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            /*'src': hmpImgUrl + f.fileStoreInfo.fileId + "?"
                                + Math.floor(Math.random() * 1000)*/
                            /*'src'    : hmpImgUrl + "/provide/view?processKey=FrontBanner&fileId=" + rowDataJson.imgUrl*/
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,

                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg: function (imgType) {
        attributeList.img.setImg(attributeList.img.imgDiv, null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickAddImgButton: function () {
        $('input[name=fileArr]').click();
    }
}