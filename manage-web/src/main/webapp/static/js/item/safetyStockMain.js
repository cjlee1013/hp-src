/**
 * 회원관리 > 고객문의관리 > 상품평관리
 */

$(document).ready(function() {
    safetyInventoryListGrid.init();
    safetyInventory.init();
    CommonAjaxBlockUI.global();
});

// itemReview 그리드
var safetyInventoryListGrid = {
    gridView : new RealGridJS.GridView("safetyInventoryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        safetyInventoryListGrid.initGrid();
        safetyInventoryListGrid.initDataProvider();
        safetyInventoryListGrid.event();
    },
    initGrid : function() {
        safetyInventoryListGrid.gridView.setDataSource(safetyInventoryListGrid.dataProvider);
        safetyInventoryListGrid.gridView.setStyles(safetyInventoryGridBaseInfo.realgrid.styles);
        safetyInventoryListGrid.gridView.setDisplayOptions(safetyInventoryGridBaseInfo.realgrid.displayOptions);
        safetyInventoryListGrid.gridView.setColumns(safetyInventoryGridBaseInfo.realgrid.columns);
        safetyInventoryListGrid.gridView.setOptions(safetyInventoryGridBaseInfo.realgrid.options);
        safetyInventoryListGrid.gridView.setColumnProperty("histBtn", "renderer", {type: "link", url: "/item/popup/safetyStockHistPop", requiredFields: "histBtn", showUrl: false});
    },
    initDataProvider : function() {
        safetyInventoryListGrid.dataProvider.setFields(safetyInventoryGridBaseInfo.dataProvider.fields);
        safetyInventoryListGrid.dataProvider.setOptions(safetyInventoryGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        safetyInventoryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            safetyInventory.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        safetyInventoryListGrid.dataProvider.clearRows();
        safetyInventoryListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(safetyInventoryListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "safetyStockList"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        safetyInventoryListGrid.gridView.exportGrid({
            type : "excel",
            target : "local",
            fileName : fileName + ".xlsx",
            showProgress : true,
            progressMessage : "엑셀 데이터 추줄 중입니다."
        });
    }
};

// itemReview 관리
var safetyInventory = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();

        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * itemReview 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#itemReviewSetForm').ajaxForm({
            url: '/',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                safetyInventory.searchReset();
                safetyInventory.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(safetyInventory.search); // safetyStock 검색
        $('#searchResetBtn').bindClick(safetyInventory.searchFormReset); //safetyStock 검색폼 초기화
        $('#setBtn').bindClick(safetyInventory.set); //safetyStock 등록,수정
        $('#resetBtn').bindClick(safetyInventory.reset); //safetyStock 입력폼 초기화
        $('#addItemPopUp').bindClick(safetyInventory.addItemPopUp);
        $('#itemNo').focusout(safetyInventory.getItemName);

        // HYPER 안전재고 Event 처리 ( Radio, qty )
        $('input:radio[name="hyperUseYn"]').change(function() {
            if($("input:radio[name=hyperUseYn]:checked").val() === 'Y') {
                $("input:radio[name='hyperUseYn']:radio[value='Y']").prop('checked', true);
                $('#hyperQty').val('').attr('readonly', false);
            } else {
                $("input:radio[name='hyperUseYn']:radio[value='N']").prop('checked', true);
                $('#hyperQty').val('').attr('readonly', true);
            }
        });

        // EXPRESS 안전재고 Event 처리 ( Radio, qty )
        $('input:radio[name="expressUseYn"]').change(function() {
            if($("input:radio[name=expressUseYn]:checked").val() === 'Y') {
                $("input:radio[name='expressUseYn']:radio[value='Y']").prop('checked', true);
                $('#expressQty').val('').attr('readonly', false);
            } else {
                $("input:radio[name='expressUseYn']:radio[value='N']").prop('checked', true);
                $('#expressQty').val('').attr('readonly', true);
            }
        });

        // 이력보기
        safetyInventoryListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = $("input:radio[name=storeType]:checked").val();

            $.jUtil.openNewPopup(url + "?itemNo=" + itemNo, 1090, 540);
        };
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = safetyInventoryListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url : '/item/safetyStock/getSafetyStockInfo.json',
            data : { itemNo : rowDataJson.itemNo },
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {

                safetyInventory.setSafetyInfoData(res);

                $('#itemNo').val(res.itemNo);
                $('#itemNm').val(res.itemNm);

                if(res.hyperUseYn === 'Y') {
                    $("input:radio[name='hyperUseYn']:radio[value='Y']").prop('checked', true);
                    $('#hyperQty').val(res.hyperQty).attr('readonly', false);
                } else {
                    $("input:radio[name='hyperUseYn']:radio[value='N']").prop('checked', true);
                    $('#hyperQty').val('').attr('readonly', true);
                }

                if(res.expressUseYn === 'Y') {
                    $("input:radio[name='expressUseYn']:radio[value='Y']").prop('checked', true);
                    $('#expressQty').val(res.expressQty).attr('readonly', false);
                } else {
                    $("input:radio[name='expressUseYn']:radio[value='N']").prop('checked', true);
                    $('#expressQty').val('').attr('readonly', true);
                }

                $('#useYn').val(res.useYn);
            }
        });
    },
    /**
     * 상품명입력
     */
    setItemName : function(res) {
        $('#itemNm').val(res.returnKey);
    },
    /**
     * 상품명검색
     */
    getItemName : function() {

        var itemNo  = $('#itemNo').val();

        if(!$.jUtil.isEmpty(itemNo)){

            if (!$.jUtil.isAllowInput( itemNo, ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#itemNo").val('');
                $("#itemNm").val('');
                $("#itemNo").focus();
                return;
            } else {
                commonItem.getItemName($('#itemNo').val(), safetyInventory.setItemName);
            }

        }
    },
    /**
     * itemReview 검색
     */
    search : function() {
        if(!safetyInventory.valid.search()) {
            return false;
        }

        var lCate = $('#searchCateCd1 option:selected').val();
        var mCate = $('#searchCateCd2 option:selected').val();
        var sCate = $('#searchCateCd3 option:selected').val();
        var dCate = $('#searchCateCd4 option:selected').val();

        if(lCate != "" && mCate == "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(lCate);
        } else if(lCate != "" && mCate != "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(mCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate == "") {
            $('#searchCateCd').val(sCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate != "") {
            $('#searchCateCd').val(dCate);
        } else {
            $('#searchCateCd').val('');
        }


        if ($('#schType').val() == 'itemNo') {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));
        } else {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ''));
        }

        CommonAjax.basic({
            url : '/item/safetyStock/getSafetyStock.json?' + $('#safetyInventorySearchForm').serialize(),
            data : null,
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {
                safetyInventory.reset();
                safetyInventoryListGrid.setData(res);
                $('#safetyInventoryTotalCount').html($.jUtil.comma(safetyInventoryListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchCateCd1').val('').change();
        $('#searchCateCd').val('');
        $('#schType').val('itemNo');
        $('#schKeyword, #schUseYn').val('');
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function() {
        if(!$.jUtil.isEmpty($('#itemNo').val())) {
            alert('상품 수정은 불가합니다. 초기화 또는 상품 번호를 삭제 후 신규로 등록해 주세요.');
            return false;
        }

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=safetyInventory.setItemInfo&isMulti=N&mallType=TD&storeType=ALL', 1084, 650);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        $('#itemNo').val(resData[0].itemNo);
        $('#itemNm').val(resData[0].itemNm);
    },
    /**
     * safetyStock 등록/수정
     */
    set : function() {
        if(!safetyInventory.valid.set()){
            return false;
        }

        CommonAjax.basic({
            url : '/item/safetyStock/setSafetyStock.json',
            data : JSON.stringify($('#safetyInventorySetForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                safetyInventory.setSafetyInfoData($('#safetyInventorySetForm').serializeObject());
                safetyInventory.reset();
            }
        });
    },
    /**
     * safetyStockInfo 등록/수정
     */
    setSafetyInfoData : function(safetyInfo) {
        var options = {
            fields: ['itemNo'],
            values: [safetyInfo.itemNo]
        }
        var itemIndex = safetyInventoryListGrid.gridView.searchItem(options);

        // 신규 등록 시 오류를 방지하기 위함. ( 그리드에 찾는 데이터가 없을 경우 -1 )
        if(itemIndex != -1) {
            var hyperDispTxt = '설정안함';
            if(safetyInfo.hyperUseYn === 'Y') {
                hyperDispTxt = safetyInfo.hyperQty;
            }

            var expressDispTxt = '설정안함';
            if(safetyInfo.expressUseYn === 'Y') {
                expressDispTxt = safetyInfo.expressQty;
            }

            var useYnTxt = '사용안함';
            if(safetyInfo.useYn === 'Y') {
                useYnTxt = '사용';
            }

            // RealGrid 값 변경.
            var setGridValue = {
                itemNo : safetyInfo.itemNo
                , hyperUseYn : safetyInfo.hyperUseYn
                , hyperDispTxt : hyperDispTxt
                , expressUseYn : safetyInfo.expressUseYn
                , expressDispTxt : expressDispTxt
                , useYn : safetyInfo.useYn
                , useYnTxt : useYnTxt
            };
            safetyInventoryListGrid.gridView.setValues(itemIndex, setGridValue, true);
        }
    },
    /**
     * safetyStock 입력/수정 폼 초기화
     */
    reset : function() {
        $('#itemNo, #itemNm').val('');
        $("input:radio[name='hyperUseYn']:radio[value='N'], input:radio[name='expressUseYn']:radio[value='N']").prop('checked', true);
        $('#hyperQty, #expressQty').val('').attr('readonly', true);
        $('#useYn').val('Y');
    }
};

/**
 * safetyStock Validation Check
 */
safetyInventory.valid = {
    search : function () {
        var schKeyword = $('#schKeyword').val();
        // 검색어 자릿수 확인
        if (schKeyword !== "" && schKeyword.length < 2) {
            return $.jUtil.alert("검색 키워드는 2자 이상 입력해주세요.", 'schKeyword');
        }
        if (schKeyword.split(',').length > 20) {
            return $.jUtil.alert("검색키워드는 최대 20개 까지 등록 가능합니다.", 'schKeyword');
        }
        return true;
    },
    set : function () {
        if($.jUtil.isEmpty($('#itemNo').val())) {
            alert("상품을 검색 또는 선택해 주세요.");
            $("#itemNo").val('');
            $("#itemNo").focus();
            return;
        }
        if($.jUtil.isEmpty($('#itemNm').val())) {
            alert("상품이 정상적으로 조회되지 않았습니다.");
            $("#itemNo").val('');
            $("#itemNo").focus();
            return;
        }
        if($("input:radio[name=hyperUseYn]:checked").val() === 'Y') {
            if (!$.jUtil.isAllowInput( $('#hyperQty').val(), ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#hyperQty").val('');
                $("#hyperQty").focus();
                return;
            }
            if ( $('#hyperQty').val()> 999) {
                alert("최대 999까지 입력 가능합니다.");
                $("#hyperQty").val('');
                $("#hyperQty").focus();
                return;
            }
        }
        if($("input:radio[name=expressUseYn]:checked").val() === 'Y') {
            if (!$.jUtil.isAllowInput( $('#expressQty').val(), ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#expressQty").val('');
                $("#expressQty").focus();
                return;
            }
            if ( $('#expressQty').val()> 999) {
                alert("최대 999까지 입력 가능합니다.");
                $("#expressQty").val('');
                $("#expressQty").focus();
                return;
            }
        }
        return true;
    }
};