/**
 * 상품 추천문구 관리 에서 사용, 그리드 관련 컨트롤 분리
 * */

// 상품 추천문구 관리 조회 그리드
var recomMsgGrid = {
    gridView : new RealGridJS.GridView("recomMsgGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        recomMsgGrid.initRecomMsgGrid();
        recomMsgGrid.initDataProvider();
        recomMsgGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initRecomMsgGrid : function() {
        recomMsgGrid.gridView.setDataSource(recomMsgGrid.dataProvider);
        recomMsgGrid.gridView.setStyles(recomMsgGridBaseInfo.realgrid.styles);
        recomMsgGrid.gridView.setDisplayOptions(recomMsgGridBaseInfo.realgrid.displayOptions);
        recomMsgGrid.gridView.setColumns(recomMsgGridBaseInfo.realgrid.columns);
        recomMsgGrid.gridView.setOptions(recomMsgGridBaseInfo.realgrid.options);

        setColumnLookupOption(recomMsgGrid.gridView, "useYn", $("#useYn"));
        setColumnLookupOption(recomMsgGrid.gridView, "storeType", $("#storeType"));
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        recomMsgGrid.dataProvider.setFields(recomMsgGridBaseInfo.dataProvider.fields);
        recomMsgGrid.dataProvider.setOptions(recomMsgGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        recomMsgGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = recomMsgGrid.dataProvider.getJsonRow(recomMsgGrid.gridView.getCurrent().dataRow);
            recomMsgMain.getRecomMsgDetail(gridRow);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        recomMsgGrid.dataProvider.clearRows();
        recomMsgGrid.dataProvider.setRows(dataList);
    }
};

// 상품 조회 그리드
var recomMsgItemGrid = {
    gridView : new RealGridJS.GridView("recomMsgItemGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        recomMsgItemGrid.initRecomMsgItemGrid();
        recomMsgItemGrid.initDataProvider();
        recomMsgItemGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initRecomMsgItemGrid : function() {
        recomMsgItemGrid.gridView.setDataSource(recomMsgItemGrid.dataProvider);
        recomMsgItemGrid.gridView.setStyles(recomItemGridBaseInfo.realgrid.styles);
        recomMsgItemGrid.gridView.setDisplayOptions(recomItemGridBaseInfo.realgrid.displayOptions);
        recomMsgItemGrid.gridView.setColumns(recomItemGridBaseInfo.realgrid.columns);
        recomMsgItemGrid.gridView.setOptions(recomItemGridBaseInfo.realgrid.options);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        recomMsgItemGrid.dataProvider.setFields(recomItemGridBaseInfo.dataProvider.fields);
        recomMsgItemGrid.dataProvider.setOptions(recomItemGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        recomMsgItemGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        recomMsgItemGrid.dataProvider.clearRows();
        recomMsgItemGrid.dataProvider.setRows(dataList);

        recomMsgMain.item.setItemCnt();
    }
    /**
     * 그리드에 데이터 추가
     * @param data
     */
    , appendData : function(data) {
        var currentDatas = recomMsgItemGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        recomMsgItemGrid.dataProvider.beginUpdate();

        try {
            data.forEach(function (value, index, array1) {
                if (currentDatas.findIndex(function (currentData) {
                    return currentData.itemNo === value.itemNo;
                }) == -1) {
                    recomMsgItemGrid.dataProvider.addRow(value);
                } else {
                    var options = {
                        startIndex: 0,
                        fields: ["itemNo"],
                        values: [value.itemNo]
                    };

                    var _row = recomMsgItemGrid.dataProvider.searchDataRow(options);

                    // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                    if (recomMsgItemGrid.dataProvider.getRowState(_row) == "deleted") {
                        recomMsgItemGrid.dataProvider.setRowState(_row, "none");
                    }
                }
            });
        } finally {
            recomMsgItemGrid.dataProvider.endUpdate(true);
        }

        recomMsgMain.item.setItemCnt();
    }
    /**
     * 선택한 row 삭제
     */
    , removeCheckData : function () {
        var check = recomMsgItemGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        recomMsgItemGrid.dataProvider.removeRows(check, false);
        recomMsgMain.item.setItemCnt();
    }
};