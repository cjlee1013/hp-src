/**
 * 상품관리 > 마켓연동상품관리 > 외부연동현황관리 NEW
 */

$(document).ready(function() {

    commonCategory.init();
    CommonAjaxBlockUI.global();

    marketItemListGrid.init();
    marketItemStorePriceGrid.init();
    marketItem.init();
});

//외부연동현황관리
var marketItemListGrid = {

    gridView : new RealGridJS.GridView("marketItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        marketItemListGrid.initGrid();
        marketItemListGrid.initDataProvider();
        marketItemListGrid.event();

    },
    initGrid : function() {
        marketItemListGrid.gridView.setDataSource(marketItemListGrid.dataProvider);
        marketItemListGrid.gridView.setStyles(marketItemListGridBaseInfo.realgrid.styles);
        marketItemListGrid.gridView.setDisplayOptions(marketItemListGridBaseInfo.realgrid.displayOptions);
        marketItemListGrid.gridView.setColumns(marketItemListGridBaseInfo.realgrid.columns);
        marketItemListGrid.gridView.setOptions(marketItemListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        marketItemListGrid.dataProvider.setFields(marketItemListGridBaseInfo.dataProvider.fields);
        marketItemListGrid.dataProvider.setOptions(marketItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        marketItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            marketItem.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        marketItemListGrid.dataProvider.clearRows();
        marketItemListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketItemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "외부연동현황_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        marketItemListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }

};

//외부연동현황관리가격정보 grid 
var marketItemStorePriceGrid = {

    gridView : new RealGridJS.GridView("marketItemStorePriceGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

         marketItemStorePriceGrid.initGrid();
         marketItemStorePriceGrid.initDataProvider();
         marketItemStorePriceGrid.event();

    },
    initGrid : function() {
         marketItemStorePriceGrid.gridView.setDataSource( marketItemStorePriceGrid.dataProvider);
         marketItemStorePriceGrid.gridView.setStyles( marketItemStorePriceGridBaseInfo.realgrid.styles);
         marketItemStorePriceGrid.gridView.setDisplayOptions( marketItemStorePriceGridBaseInfo.realgrid.displayOptions);
         marketItemStorePriceGrid.gridView.setColumns( marketItemStorePriceGridBaseInfo.realgrid.columns);
         marketItemStorePriceGrid.gridView.setOptions( marketItemStorePriceGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
         marketItemStorePriceGrid.dataProvider.setFields( marketItemStorePriceGridBaseInfo.dataProvider.fields);
         marketItemStorePriceGrid.dataProvider.setOptions( marketItemStorePriceGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
         marketItemStorePriceGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    setData : function(dataList) {
         marketItemStorePriceGrid.dataProvider.clearRows();
         marketItemStorePriceGrid.dataProvider.setRows(dataList);
    },

};

//외부연동현황관리
var marketItem = {
    /**
     * 초기화
     */
    init : function() {

        marketItem.bindingEvent();

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

    },

    /**
     * bind event
     */
    bindingEvent : function() {

        //검색
        $('#searchBtn').bindClick(marketItem.search);
        $('#searchResetBtn').bindClick(marketItem.resetSearchForm);

        //저장
        $('#resetBannerBtn').bindClick(marketItem.resetEventBannerForm);
        $('#setBannerBtn').bindClick(marketItem.setEventBanner);
        $('#excelDownloadBtn').bindClick(marketItem.excelDownload);

        $('#marketItemHist').on('click', function() {
            windowPopupOpen('/item/popup/marketItemHistPop?itemNo=' + $('#itemNo').text() + "&partnerId="+ $('#partnerId').val() , "itemHistPop", 1600, 800);
        });

        $('#schType').on('change', function() {
            if ($(this).val() == 'itemNo' || $(this).val() == 'marketItemNo' ) {
                $("#schKeyword").prop('placeholder', '상품번호 복수검색시 Enter또는 ,로 구분');
            } else {
                $("#schKeyword").prop('placeholder', '');
            }
        });

    },
    /**
     * 조회 유효성 검사
     * @returns {*|boolean|boolean}
     */
    valid : function() {
        let schKeyword = $('#schKeyword').val();

        if(schKeyword != "") {
            let schType = $('#schType').val();

            switch(schType) {
                case "itemNo" :
                case "marketItemNo" :
                    let pattern = /[^0-9,]/g;
                    if(pattern.test(schKeyword)) {
                        alert('숫자 또는 ","를 입력하세요');
                        $('#schKeyword').focus();
                        return false;
                    }
                    break;
            }

            if (schKeyword.length < 2) {
                return $.jUtil.alert('검색 키워드는 2자 이상 입력해주세요.', 'schKeyword');
            }
        }
        return true;

    },
    /**
     * 조회
     * @returns {boolean}
     */
    search : function() {

        if ($('#schType').val() == 'itemNo' || $('#schType').val() == 'marketItemNo' ) {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));
        } else {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ''));
        }

        if (marketItem.valid()) {
            let form =  $('#marketItemSearchForm').serializeObject();

            CommonAjax.basic({
                url: '/item/market/getMarketItemList.json',
                data: JSON.stringify(form),
                contentType: "application/json",
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    marketItemListGrid.setData(res);
                    $('#marketItemTotalCount').html($.jUtil.comma(marketItemListGrid.gridView.getItemCount()));
                }
            });
        }
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        marketItemListGrid.excelDownload();
    },
    /**
     * reset
     */
    resetText : function() {

        //그리드 초기화
        marketItemStorePriceGrid.dataProvider.clearRows();
        $('#marketItemStoreTotalCount').html("0");

        //상세항목 초기화
        $('#itemNo, #itemNm').text('');
        $('#marketItemNo, #marketNm').text('');
        $('#cateNmm, #marketCateNm').text('');
        $('#salePrice, #discountPrice, #promoCd, #sendMsg').text('');
        $('#marketItemHist').hide();

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        marketItem.resetText();
        let rowDataJson = marketItemListGrid.dataProvider.getJsonRow(selectRowId);

       CommonAjax.basic({
            url: '/item/market/getMarketItemDetail.json?',
            data: {itemNo: rowDataJson.itemNo ,
                    partnerId: rowDataJson.partnerId},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                if ( res == null ) {
                    if (true) {
                        alert('정상적으로 조회되지 않았습니다.');
                    }
                } else {
                    $('#marketItemHist').show();

                    $('#itemNo').text(res.itemNo);
                    $('#itemNm').text(res.itemNm);
                    $('#marketNm').text(res.marketNm);
                    $('#marketItemNo').text(res.marketItemNo);
                    $('#cateNm').text(!$.jUtil.isEmpty(res.lcateNm) ?
                        res.lcateNm + " > " + res.mcateNm + " > " + res.scateNm + " > " + res.dcateNm  + " (" + res.dcateCd+ ")": '' );
                    $('#marketCateNm').text(marketItem.getMarketCateNm(res));
                    $('#partnerId').val(res.partnerId);

                    
                    $('#itemNm').text(res.itemNm);
                    $('#salePrice').text($.jUtil.isEmpty(res.salePrice) ? '' : res.salePrice + '원');
                    $('#discountPrice').text($.jUtil.isEmpty(res.couponPrice) ? '' : res.couponPrice + '원');
                    $('#promoCd').text($.jUtil.isEmpty(res.customCd) ? '' : res.customCd);

                    // 연동실패인 경우만
                    if (res.marketItemStatus == "S" || res.marketItemStatus == "E") {
                        let failMsg = "  [" + res.marketItemStatusNm + " : "
                        + ($.jUtil.isEmpty(res.sendMsg) ? "" : res.sendMsg) + "]";
                        $('#sendMsg').text(failMsg);
                    }

                    if( res.storeList != null ) {
                        marketItemStorePriceGrid.setData(res.storeList);
                        $('#marketItemStoreTotalCount').html($.jUtil.comma(
                            marketItemStorePriceGrid.gridView.getItemCount()));
                    }

                }
            },
            error: function(e) {
              alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
            }
        });

    },

    getMarketCateNm : function (obj) {
        let cate;
        if( !$.jUtil.isEmpty(obj.marketLcateNm)) {
            cate = obj.marketLcateNm + " > " + obj.marketMcateNm;

            if ($.jUtil.isNotEmpty(obj.marketScateNm)) {
                cate = cate + " > " + obj.marketScateNm;
            }

            if ($.jUtil.isNotEmpty(obj.marketDcateNm)) {
                cate = cate + " > " + obj.marketDcateNm;
            }

            //마지막 카테고리 번호
            cate = cate + " (" + obj.marketCateCd + ")"
        }

        return cate;
    },
    /**
     * 검색항목 초기화
     */
    resetSearchForm : function() {

        $('input:radio[name="schPartner"]:input[value="naver"]').trigger('click');

        $('#selectCateCd1').val('').change();
        $('#schType').val("itemNo").trigger('change');
        $('#schKeyword').val('');
        $('#schItemStatus').val('');
        $('#schMarketItemStatus').val('');
        $('#schSendYn').val('');
    },

};
