/**
 * 상품관리 > 점포상품관리 > 점포상품 상세정보
 */
var item = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        itemMain.util.initDateTime('10d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        itemMain.util.initDateTime('10d', 'onlineStockStartDt', 'onlineStockEndDt', "HH:00:00", "HH:59:59");
        itemMain.util.initDate('-30d', 'rsvStartDt', 'rsvEndDt');
        itemMain.util.initDate('0d', 'prDispStartDt', 'prDispEndDt');
        itemMain.util.initDate('-30d', 'stopStartDt', 'stopEndDt');
        Calendar.datePicker('setStopStartDt');
        Calendar.datePicker('setStopEndDt');
        this.resetForm();
        $('#rsvPurchaseLimitQty, #stockQty, #setStockQty, #saleUnit, #purchaseMinQty, #purchaseLimitQty, #purchaseLimitDay').allowInput("keyup", ["NUM"]);
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);
        item.getDetail(rowDataJson.itemNo);
    },
    /**
     * 상품상세 > 점포상품 상세정보 조회
     */
    getDetail : function(itemNo) {
        if (itemNo) {
            item.resetForm();
            CommonAjaxBlockUI.startBlockUI();
            CommonAjax.basic({
                url: '/item/getDetail.json',
                data: {itemNo: itemNo, mallType: mallType, storeId:0, menuType:'detail', storeType: $('input[name="schStoreType"]:checked').val()},
                method: 'GET',
                callbackFunc: function (data) {
                    if (data.itemStatus == 'S') {
                        $('#itemTitleArea').show();
                    } else if (data.itemStatus == 'P') {
                        $('#itemTitleArea').show();
                    }
                    $('#storeTypeTxt').text($('input[name="schStoreType"]:checked').val());

                    $('#itemNo').val(itemNo);
                    $('#itemNoView').html(itemNo);
                    $('#itemNmView').html(data.itemNm);
                    $('#statusDesc').html(data.statusDesc);
                    $('#storeType').val(data.storeType);
                    $('.itemStatusNm').html(data.itemStatusNm);
                    $('#itemChgDt').html(data.chgDt);
                    $('#optStoreTypeNm').text(data.storeTypeNm);
                    //판매정보
                    $('input:radio[name="salePeriodYn"]:input[value="'+data.salePeriodYn+'"]').trigger('click');
                    item.sale.changeSalePeriodYn(data.salePeriodYn, data.saleStartDt, data.saleEndDt);
                    item.sale.changeOnlindStockYn(data.onlineStockYn, data.onlineStockStartDt, data.onlineStockEndDt, data.stockTypeNm);

                    let nowDt = moment().format("YYYY-MM-DD HH:mm:ss");
                    let column = storeListGrid.gridView.columnByName("stockQty");

                    if (data.onlineStockYn == 'Y' && moment(data.onlineStockStartDt).isBefore(nowDt) && moment(nowDt).isBefore(data.onlineStockEndDt)) {
                        storeListGrid.gridView.setColumnProperty(column, "header", '온라인재고수량');

                        column = storeListGrid.gridView.columnByName("offStockQtyUI");
                        storeListGrid.gridView.setColumnProperty(column, "visible", true);

                        column = storeListGrid.gridView.columnByName("stockQtyUI");
                        storeListGrid.gridView.setColumnProperty(column, "visible", false);
                    } else {
                        storeListGrid.gridView.setColumnProperty(column, "header", '오프라인재고수량');

                        column = storeListGrid.gridView.columnByName("offStockQtyUI");
                        storeListGrid.gridView.setColumnProperty(column, "visible", false);

                        column = storeListGrid.gridView.columnByName("stockQtyUI");
                        storeListGrid.gridView.setColumnProperty(column, "visible", true);
                    }

                    $('input:radio[name="rsvYn"]:input[value="'+data.rsvYn+'"]').trigger('click');
                    if (data.rsvYn == 'Y' && !$.jUtil.isEmpty(data.rsvSaleNm) && !$.jUtil.isEmpty(data.rsvStartDt)) {
                        $('#rsvInfoArea').text(data.rsvSaleNm + '(' + data.rsvSaleSeq + ')');
                        $('#rsvDtArea').text(data.rsvStartDt + ' ~ ' + data.rsvEndDt);
                    } else {
                        $('#rsvInfoArea, #rsvDtArea').text('');
                    }

                    item.sale.changeRsvYn(data.rsvYn, data.rsvStartDt, data.rsvEndDt, data.shipStartDt);

                    $('input:radio[name="rsv.rsvPurchaseLimitYn"]:input[value="'+data.rsvPurchaseLimitYn+'"]').trigger('click');
                    $('#rsvPurchaseLimitQty').val(data.rsvPurchaseLimitQty);

                    $('#stockQty').val(data.stockQty);
                    $('#changeStockQtyLabel').show();

                    $('#saleUnit').val(data.saleUnit);
                    $('#purchaseMinQty').val(data.purchaseMinQty);

                    $('input:radio[name="purchaseLimitYn"]:input[value="'+data.purchaseLimitYn+'"]').trigger('click');

                    if (data.purchaseLimitDuration) {
                        $('#purchaseLimitDuration').val(data.purchaseLimitDuration).trigger('change');
                    } else {
                        $('#purchaseLimitDuration').trigger('change');
                    }
                    $('#purchaseLimitDay').val(data.purchaseLimitDay == 0 ? 1: data.purchaseLimitDay);
                    $('#purchaseLimitQty').val(data.purchaseLimitQty == 0 ? 1: data.purchaseLimitQty);

                    //옵션정보
                    $('input:radio[name="optSelUseYn"]:input[value="'+data.optSelUseYn+'"]').trigger('click');
                    $('input:radio[name="optSelUseYn"]').prop('disabled', true);
                    $('#optDepth').val(data.optTitleDepth);
                    let opt1Column = optListGrid.gridView.columnByName("opt1Val");

                    opt1Column.header.text = $.jUtil.isEmpty(data.opt1Title) ? '옴션명' : data.opt1Title;
                    optListGrid.gridView.setColumn(opt1Column);

                    optListGrid.setData(data.optList);
                    $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));

                    storeListGrid.setData(data.storeList);
                    $('#itemShipTotalCount').text($.jUtil.comma(storeListGrid.gridView.getItemCount()));
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#prPeriodYn').bindChange(item.sale.changePrPeriodYn);
        $('input:radio[name="purchaseLimitYn"]').bindClick(item.sale.changePurchaseLimitYn);

        $('#purchaseLimitDuration').bindChange(item.sale.getPurchaseLimitDuration);
        $('#changeStockQtyCheck').bindChange(item.sale.changeStockQtyCheck);

        $('input:radio[name="salePeriodYn"]').on('change', function() {
            item.sale.changeSalePeriodYn($(this).val());
        });

        $('input:checkbox[name="onlineStockYn"]').on('change', function() {
            if ($(this).is(':checked')) {
                item.sale.changeOnlindStockYn('Y');
            } else {
                item.sale.changeOnlindStockYn('N');
            }
        });

        $('input:radio[name="rsvYn"]').on('change', function() {
            item.sale.changeRsvYn($(this).val());
        });

        $('input:radio[name="rsv.rsvPurchaseLimitYn"]').on('change', function() {
            item.sale.changeRsvPurchaseLimitYn($(this).val());
        });

        $('input:radio[name="optSelUseYn"]').on('click', function() {
            item.opt.getOptHandler($(this).val());
        });

        $('#shipArea input:radio').bindClick(item.ship.setSelectUIControl);

        $('#resetBtn').on('click', function () {
            item.resetForm();
        });

        $('#setStopDealYn').on('change', function() {
            if ($(this).val() == 'Y') {
                $('#setStopReason').prop('disabled', false);
                $('#setStopLimitYn').prop('disabled', false);
            } else {
                $('#setStopReason').val('').prop('disabled', true);
                $('#setStopLimitYn').val("N").trigger('change').prop('disabled', true);
            }
        });

        $('#setStopLimitYn').on('change', function() {
            if ($(this).val() == 'Y') {
                $('#setStopStartDt').prop('disabled', false);
                $('#setStopEndDt').prop('disabled', false);
            } else {
                $('#setStopStartDt').val('').prop('disabled', true);
                $('#setStopEndDt').val('').prop('disabled', true);
            }
        });



        $('#itemHist').on('click', function() {
            if (!$.jUtil.isEmpty($('#storeTypeTxt').text())) {
                windowPopupOpen('/item/popup/itemHistPop?itemNo=' + $('#itemNo').val() + '&storeType=' + $('#storeTypeTxt').text(), "itemHistPop", 1600, 800);
            }
        });

        item.ship.event();
    },
    valid : function () {
        //판매기간 검증
        if ($('input[name="salePeriodYn"]:checked').val() == 'Y') {
            if ($.jUtil.isEmpty($('#saleStartDt').val()) || $.jUtil.isEmpty($('#saleEndDt').val())) {
                return $.jUtil.alert('판매기간을 입력해주세요.', 'saleStartDt');
            }

            if (moment($('#saleEndDt').val()).isBefore($('#saleStartDt').val()) == true) {
                return $.jUtil.alert('판매종료일은 시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
            }
        }

        //판매단위수량 검증
        if ($.jUtil.isEmpty($('#saleUnit').val())) {
            return $.jUtil.alert("판매단위수량을 입력해주세요.", 'saleUnit');
        }

        //최소구매수량 검증
        if ($.jUtil.isEmpty($('#purchaseMinQty').val())) {
            return $.jUtil.alert("최소구매수량을 입력해주세요.", 'purchaseMinQty');
        }

        //구매수량제한 검증
        if ($('input[name="purchaseLimitYn"]:checked').val() == 'Y') {
            if (Number($('#saleUnit').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 판매단위수량 보다 적을 수 없습니다.", 'saleUnit');
            }

            if (Number($('#purchaseMinQty').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 최소구매수량 보다 적을 수 없습니다.", 'purchaseMinQty');
            }

            if ($('#purchaseLimitDuration').val() == 'P' && Number($('#purchaseLimitDay').val()) > 30) {
                return $.jUtil.alert("구매수량제한 기간은 30일을 초과할 수 없습니다.", 'purchaseLimitDay');
            }
        }

        return true;
    },
    setItemTD : function(isEdit) {
        if (item.valid()) {
            let itemForm            = $('#itemSetForm').serializeObject(true);
            itemForm.optList        = optListGrid.dataProvider.getJsonRows();
            itemForm.storeList        = storeListGrid.dataProvider.getJsonRows();

            CommonAjax.basic({
                url: '/item/setItemTDDetail.json',
                data: JSON.stringify(itemForm),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                    alert(res.returnMsg);
                    item.getDetail(res.returnKey);
                }
            });
        }
    },
    /**
     * 상품 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#itemSetForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });
        $("#itemSetForm").resetForm();
        $('#itemTitleArea').hide();
        $('.addRsvDiv').remove();
        $('#statusDesc').html('');
        $('#stockTypeNm, #rsvInfoArea, #rsvDtArea, #storeTypeTxt').text('');
        $('#purchaseLimitDuration, #prPeriodYn').trigger('change');
        $('input:radio[name="rsv.rsvPurchaseLimitYn"]').trigger('change');
        $('input:radio[name="salePeriodYn"]:input[value="'+$('input:radio[name="salePeriodYn"]:checked').val()+'"]').trigger('change');
        $('input[name="onlineStockYn"]').prop('checked', false).trigger('change');
        $('input:radio[name="purchaseLimitYn"]:input[value="'+$('input:radio[name="purchaseLimitYn"]:checked').val()+'"]').trigger('click');
        $('input:radio[name="optSelUseYn"]').prop('disabled', false);
        $('input:radio[name="optSelUseYn"]:input[value="N"]').trigger('click');
        item.sale.changeRsvYn($('input:radio[name="rsvYn"]:checked').val());

        optListGrid.dataProvider.clearRows();
        storeListGrid.dataProvider.clearRows();

        $('#storeTotalCount, #optTotalCount, #prTotalCount, #itemShipTotalCount').text(0);

        item.ship.getDetail();
    }
};

item.opt = {
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {},
    /**
     * 옵션 전시여부 일괄적용
     * @returns {boolean}
     */
    setDispYn : function() {
        let rowArr = optListGrid.gridView.getCheckedItems();

        if (rowArr.length == 0) {
            alert('옵션을 선택해 주세요.');
            return false;
        }

        for (let i in rowArr) {
            optListGrid.gridView.setValue(rowArr[i], "dispYn", $('#optDispYn').val());
            optListGrid.gridView.setValue(rowArr[i], "dispYnNm", $('#optDispYn option:selected').text());
        }
    },
    /**
     * 옵션 핸들러 - 선택형옵션사용여부 따른 옵션 컨트롤
     * @param optSelUseYn
     * @param event
     * @param prodStatus
     */
    getOptHandler : function(optSelUseYn) {
        if (optSelUseYn == 'Y') {
            $('#optSelUseYnArea').show();
        } else {
            $('#optSelUseYnArea').hide();
        }
    }
};

item.sale = {
    /**
     * 판매정보 > 구매수량제한 > 구매제한타입
     * @param _this
     */
    getPurchaseLimitDuration : function(_this) {
        $('.purchaseLimitDuration').css('display', '');
        $('.purchaseLimitDuration' + $(_this).val()).css('display', 'none');

        if ($(_this).val() == 'P') {
            $('#purchaseLimitDay').prop('disabled', false);

        } else {
            $('#purchaseLimitDay').prop('disabled', true);
        }
    },
    /**
     * 재고수량변경 체크박스 변경 이벤트
     * @param obj
     */
    changeStockQtyCheck : function(obj) {
        if ($('input[name="onlineStockYn"]').is(':checked') == true) {
            if ($(obj).is(':checked')) {
                $('#stockQty').prop('readonly', false);
            } else {
                $('#stockQty').prop('readonly', true);
            }
        }
    },
    /**
     * 판매기간 셋팅
     * @param salePeriodYn
     * @param saleStartDt
     * @param saleEndDt
     */
    changeSalePeriodYn : function(salePeriodYn, saleStartDt, saleEndDt) {
        if (salePeriodYn == 'Y') {
            $('#saleStartDt, #saleEndDt').prop('disabled', false);
            $('#saleStartDt').val(saleStartDt);
            $('#saleEndDt').val(saleEndDt);
        } else {
            $('#saleStartDt, #saleEndDt').val('').prop('disabled', true);
        }
    },
    /**
     * 온라인재고설정 셋팅
     * @param onlineStockYn
     * @param onlineStockStartDt
     * @param onlineStockEndDt
     */
    changeOnlindStockYn : function(onlineStockYn, onlineStockStartDt, onlineStockEndDt, stockTypeNm) {
        if (onlineStockYn == 'Y') {
            $('input[name="onlineStockYn"]').prop('checked', true);
            $('#onlineStockStartDt, #onlineStockEndDt').prop('disabled', false);
            $('#onlineStockStartDt').val(onlineStockStartDt);
            $('#onlineStockEndDt').val(onlineStockEndDt);
            $('#stockQty').prop('readonly', false);
        } else {
            $('input[name="onlineStockYn"]').prop('checked', false);
            $('#onlineStockStartDt, #onlineStockEndDt').val('').prop('disabled', true);
            $('#stockQty').val('').prop('readonly', true);
        }

        if (stockTypeNm) {
            $('#stockTypeNm').text(stockTypeNm);
        }

        $('#changeStockQtyCheck').prop('checked', false);
    },
    /**
     * 구매제한여부 컨트롤
     * @param obj
     */
    changePurchaseLimitYn : function (obj) {
        let purchaseLimitYn = $(obj).val();

        switch (purchaseLimitYn) {
            case 'Y' :
                itemMain.util.setDisabledAll('purchaseLimitArea', false);
                break;
            case 'N' :
                itemMain.util.setDisabledAll('purchaseLimitArea', true);
                break;
        }
    },
    /**
     * 예약판매설정여부 변경 이벤트
     * @param rsvYn
     * @param rsvStartDt
     * @param rsvEndDt
     * @param shipStartDt
     */
    changeRsvYn : function(rsvYn, rsvStartDt, rsvEndDt, shipStartDt) {
        if (rsvYn == 'Y') {
            if (rsvStartDt) {
                $('#rsvStartDt').val(rsvStartDt);
                $('#rsvEndDt').val(rsvEndDt);
                $('#shipStartDt').val(shipStartDt);
            }
            $('#rsvStartDt, #rsvEndDt').prop('disabled', false);
            $('#rsvDetailArea').show();
        } else {
            $('.deleteRsvs').closest('div').remove();
            $('#rsvStartDt, #rsvEndDt').prop('disabled', true);
            $('#rsvDetailArea').hide();
        }
    },
    /**
     * 예약판매 부가설정여부 변경 이벤트
     */
    changeRsvPurchaseLimitYn : function() {
        if ($('input:radio[name="rsv.rsvPurchaseLimitYn"]:checked').val() == 'Y') {
            $('#rsvPurchaseLimitQty').prop('disabled', false);
        } else {
            $('#rsvPurchaseLimitQty').val('').prop('disabled', true);
        }
    }
};

item.ship = {
    event : function() {
        $('input[name="stopDealYn"]').on('change', function(){
            if ($(this).val() == 'Y') {
                $('#stopLimitYn, #stopReason').prop('disabled', false);
            } else {
                $('#stopLimitYn, #stopReason, #stopStartDt, #stopEndDt').val('').prop('disabled', true);
            }
        });

        $('#stopLimitYn').on('change', function () {
            if ($(this).val() == 'Y') {
                $('#stopStartDt, #stopEndDt').prop('disabled', false);
            } else {
                $('#stopStartDt, #stopEndDt').val('').prop('disabled', true);
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = storeListGrid.dataProvider.getJsonRow(selectRowId);

        item.ship.getDetail(rowDataJson);

    },
    getDetail : function(rowData) {
        if (rowData) {
            item.ship.getDetail();
            storeListGrid.selectedRowId = storeListGrid.gridView.getCurrent().dataRow;

            $('#storeId').text(rowData.storeId);
            $('#storeNm').text(rowData.storeNm);
            $('#storeTypeNm').text(rowData.storeTypeNm);
            $('#storeKindNm').text(rowData.storeKindNm);
            $('#pfrYnNm').text(rowData.pfrYnNm);
            $('#stockQty').val(rowData.stockQty).prop('readonly', true);
            $('#salePrice').text($.jUtil.comma(rowData.salePrice) + ' 원');
            if (rowData.stopDealYn == 'Y') {
                $('#stopReason').val(rowData.stopReason);
                $('#stopLimitYn').val(rowData.stopLimitYn).trigger('change');
                $('#stopStartDt').val(rowData.stopStartDt);
                $('#stopEndDt').val(rowData.stopEndDt);
            }

            $('#safeNumberUseYnNm').text(rowData.safeNumberUseYnNm);
            $('#changeStockQtyLabel').show();
            $('#changestockQtyCheck').prop('checked', false).trigger('change');

            $('input:radio[name="virtualStoreOnlyYn"]:input[value="'+rowData.virtualStoreOnlyYn+'"]').trigger('click');
            $('input:radio[name="stopDealYn"]:input[value="'+rowData.stopDealYn+'"]').trigger('click').prop('checked', true);
            $('input:radio[name="drctYn"]:input[value="'+rowData.drctYn+'"]').trigger('click');
            $('input:radio[name="dlvYn"]:input[value="'+rowData.dlvYn+'"]').trigger('click');
            $('input:radio[name="pickYn"]:input[value="'+rowData.pickYn+'"]').trigger('click');
            $('input:radio[name="quickYn"]:input[value="'+rowData.quickYn+'"]').trigger('click');

            item.ship.getSellerShipSelect(rowData.storeId, rowData.drctShipPolicyNo, rowData.dlvShipPolicyNo, rowData.quickShipPolicyNo, rowData.pickShipPolicyNo);
            item.ship.setTrUIControl(rowData.storeType, rowData.storeKind)

        } else {
            storeListGrid.selectedRowId = null;
            $('#storeId, #storeNm, #storeTypeNm, #salePrice, #pfrYnNm, #storeKindNm').text('');
            $("#shipArea").find('input[type=text]').val('');
            $('#stopLimitYn').val('');

            $(".shipSelect").each(function() {
                $(this).html('<option value="">선택해주세요</option>');
            });

            $("#shipArea").find('input:radio').each(function() {
                if ($(this).val() == 'N') {
                    $(this).prop('checked', true).trigger('click');
                }
            });
            $('#changeStockQtyLabel').hide();
            $('#stockQty').prop('readonly', false);
            $('#changeStockQtyCheck').prop('checked', false);

            $('#stopLimitYn, #stopReason, #stopStartDt, #stopEndDt').prop('disabled', true);
            item.ship.setTrUIControl();
        }
    },
    /**
     * 배송정보 > 배송상세정보출력
     * @param shipPolicyNo
     * @param storeId
     */
    getSellerShipSelect : function(storeId, drctShipPolicyNo, dlvShipPolicyNo, quickShipPolicyNo, pickShipPolicyNo) {
        if (storeId) {
            CommonAjax.basic({
                url: '/partner/getShipPolicyList.json',
                data: {partnerId: 'homeplus', storeId:storeId, shipPolicyNo: ''},
                method: 'GET',
                callbackFunc: function (res) {
                    if (res.length > 0) {
                        for (let idx in res) {
                            let data = res[idx];
                            let idNm = '';

                            switch (data.shipMethod) {
                                case 'TD_DRCT' :
                                    idNm = 'drctShipPolicyNo';
                                    break;
                                case 'TD_DLV' :
                                    idNm = 'dlvShipPolicyNo';
                                    break;
                                case 'TD_QUICK' :
                                    idNm = 'quickShipPolicyNo';
                                    break;
                                case 'TD_PICK' :
                                    idNm = 'pickShipPolicyNo';
                                    break;
                            }

                            if (!$.jUtil.isEmpty(idNm)) {
                                if (data.defaultYnTxt == '기본') {
                                    $('#' + idNm).append('<option value="' + data.shipPolicyNo + '" >[' + data.defaultYnTxt + ']' + data.shipPolicyNm + '</option>');
                                } else {
                                    $('#' + idNm).append('<option value="' + data.shipPolicyNo + '">' + data.shipPolicyNm + '</option>');
                                }
                            }
                        }

                        if (drctShipPolicyNo) {
                            $('#drctShipPolicyNo').val(drctShipPolicyNo).prop('selected', true);
                        }

                        if (dlvShipPolicyNo) {
                            $('#dlvShipPolicyNo').val(dlvShipPolicyNo).prop('selected', true);
                        }

                        if (quickShipPolicyNo) {
                            $('#quickShipPolicyNo').val(quickShipPolicyNo).prop('selected', true);
                        }

                        if (pickShipPolicyNo) {
                            $('#pickShipPolicyNo').val(pickShipPolicyNo).prop('selected', true);
                        }

                    }
                }
            });

        } else {

        }
    },
    setTrUIControl : function(storeType, storeKind) {
        $('#drctTr, #dlvTr, #pickTr, #quickTr').hide();

        switch (storeType) {
            case 'HYPER':
            case 'CLUB':
                    if (storeKind == 'NOR') {
                        $('#drctTr, #dlvTr, #pickTr').show();
                    } else {
                        $('#dlvTr').show();
                    }
                break;
            case 'EXP':
                    $('#quickTr').show();
                break;
        }
    },
    setSelectUIControl : function(obj) {
        if ($(obj).val() == 'N') {
            $(obj).closest('td').find('select').val('').prop('disabled', true);
        } else {
            $(obj).closest('td').find('select').prop('disabled', false);
        }
    },
    setShip : function() {
        if ($('input[name="drctYn"]:checked').val() == 'Y' && $.jUtil.isEmpty($('#drctShipPolicyNo').val())) {
            return $.jUtil.alert('자차를 선택해 주세요.');
        }

        if ($('input[name="dlvYn"]:checked').val() == 'Y' && $.jUtil.isEmpty($('#dlvShipPolicyNo').val())) {
            return $.jUtil.alert('택배를 선택해 주세요.');
        }

        if ($('input[name="pickYn"]:checked').val() == 'Y' && $.jUtil.isEmpty($('#pickShipPolicyNo').val())) {
            return $.jUtil.alert('픽업을 선택해 주세요.');
        }

        if ($('input[name="quickYn"]:checked').val() == 'Y' && $.jUtil.isEmpty($('#quickShipPolicyNo').val())) {
            return $.jUtil.alert('퀵을 선택해 주세요.');
        }

        if ($('input[name="stopDealYn"]:checked').val() == 'Y' && ($.jUtil.isEmpty($('#stopLimitYn').val()) || ($('#stopLimitYn').val() == 'Y' && ($.jUtil.isEmpty($('#stopStartDt').val()) || $.jUtil.isEmpty($('#stopEndDt').val()))) || $.jUtil.isEmpty($('#stopReason').val()))) {
            return $.jUtil.alert('취급중지 중지기간을 선택해 주세요.');
        }

        //취급중지 검증
        if ($('#stopLimitYn').val() == 'Y') {
            if ($.jUtil.isEmpty($('#stopStartDt').val()) || $.jUtil.isEmpty($('#stopEndDt').val())) {
                return $.jUtil.alert('중지기간을 입력해주세요.', 'saleStartDt');
            }

            if (moment($('#stopEndDt').val()).isBefore($('#stopStartDt').val()) == true) {
                return $.jUtil.alert('중지종료일은 시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
            }
        }

        if ($.jUtil.isEmpty($('#storeId').text())) {
            alert('점포를 선택해 주세요.');
        } else {
            let row = storeListGrid.selectedRowId;
            let values = storeListGrid.dataProvider.getJsonRow(row);

            values.stockQty             = $('#stockQty').val();
            values.chgStockQtyYn        = $('#changeStockQtyCheck').is(':checked') ? 'Y' : 'N';
            values.stopDealYnNm         = $('input[name="stopDealYn"]:checked').siblings('span').text();
            values.stopDealYn           = $('input[name="stopDealYn"]:checked').val();
            values.virtualStoreOnlyYnNm = $('input[name="virtualStoreOnlyYn"]:checked').siblings('span').text();
            values.virtualStoreOnlyYn   = $('input[name="virtualStoreOnlyYn"]:checked').val();
            values.stopReason           = $('#stopReason').val();
            values.stopLimitYn          = $('#stopLimitYn').val();
            values.stopStartDt          = $('#stopStartDt').val();
            values.stopEndDt            = $('#stopEndDt').val();
            values.drctYnNm             = $('input[name="drctYn"]:checked').siblings('span').text();
            values.dlvYnNm              = $('input[name="dlvYn"]:checked').siblings('span').text();
            values.quickYnNm            = $('input[name="quickYn"]:checked').siblings('span').text();
            values.pickYnNm             = $('input[name="pickYn"]:checked').siblings('span').text();
            values.drctYn               = $('input[name="drctYn"]:checked').val();
            values.dlvYn                = $('input[name="dlvYn"]:checked').val();
            values.quickYn              = $('input[name="quickYn"]:checked').val();
            values.pickYn               = $('input[name="pickYn"]:checked').val();
            values.drctShipPolicyNo     = $('#drctShipPolicyNo').val();
            values.dlvShipPolicyNo      = $('#dlvShipPolicyNo').val();
            values.quickShipPolicyNo    = $('#quickShipPolicyNo').val();
            values.pickShipPolicyNo     = $('#pickShipPolicyNo').val();

            storeListGrid.dataProvider.updateRows(row, [values]);

            $('#itemShipTotalCount').text($.jUtil.comma(storeListGrid.gridView.getItemCount()));

            alert('적용되었습니다.');
        }
    },
    setStockDealYn : function() {
        let rowArr = storeListGrid.gridView.getCheckedItems();
        let optSetValueObj = {};
        let setStockQty = $('#setStockQty').val();
        let setStopDealYn = $('#setStopDealYn').val();
        let stopDealYnNm = $('#setStopDealYn option:selected').text();
        let setStopReason = $('#setStopReason').val();
        let setStopLimitYn  = $('#setStopLimitYn').val();
        let setStopStartDt  = $('#setStopStartDt').val();
        let setStopEndDt  = $('#setStopEndDt').val();

        if (rowArr.length == 0 || ($.jUtil.isEmpty(setStockQty) && $.jUtil.isEmpty(setStopDealYn))) {
            return $.jUtil.alert('재고수량 또는 취급중지를 선택해 주세요.');
        }

        if (!$.jUtil.isEmpty(setStockQty)) {
            optSetValueObj['stockQty'] = setStockQty;
            optSetValueObj['chgStockQtyYn'] = 'Y';
        }

        if (!$.jUtil.isEmpty(setStopDealYn)) {
            optSetValueObj['stopDealYn'] = setStopDealYn;
            optSetValueObj['stopDealYnNm'] = stopDealYnNm;

            if (setStopDealYn == 'Y') {
                if ($.jUtil.isEmpty(setStopReason)) {
                    return $.jUtil.alert('중지사유를 입력해주세요.');
                } else {
                    optSetValueObj['stopReason'] = setStopReason;
                }
            } else {
                optSetValueObj['stopReason'] = '';
            }

            optSetValueObj['stopLimitYn'] = setStopLimitYn;

            if (setStopLimitYn == 'Y' ) { // 기간제한
                if ($.jUtil.isEmpty(setStopStartDt) || $.jUtil.isEmpty(setStopEndDt)) {
                    return $.jUtil.alert('중지기간을 입력해주세요.', 'saleStartDt');
                }

                if (moment(setStopEndDt).isBefore(setStopStartDt) == true) {
                    return $.jUtil.alert('중지종료일은 중지시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
                }

                optSetValueObj['stopStartDt'] = setStopStartDt;
                optSetValueObj['stopEndDt'] = setStopEndDt;

            } else {

                optSetValueObj['stopStartDt'] = '';
                optSetValueObj['stopEndDt'] = '';
            }

        }

        for (let i in rowArr) {
            for (let j in optSetValueObj) {
                storeListGrid.gridView.setValue(rowArr[i], j, optSetValueObj[j]);
            }
        }

        return $.jUtil.alert('저장되었습니다.');
    }
}

let store = {
    /**
     * 판매업체 조회 팝업창
     */
    getStorePop : function(callBack) {
        windowPopupOpen("/common/popup/storePop?callback="
                + callBack + "&storeType="
                + $('input:radio[name="schStoreType"]:checked').val()
                , "partnerStatus", "1100", "620", "yes", "no");

    }
}


item.callBack = {
    schStore: function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);

    }
}