/**
 * 상품관리 > 셀러/점포상품관리 > 상품 등록/수정
 */
let itemMain = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.initSearchDate('-30d');
        this.initCategorySelectBox();
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemMain.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemMain.setDate.setEndDate(0);
        itemMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#searchResetBtn').on('click', function() {itemMain.searchFormReset();});
        $('#searchBtn').on('click', function() {itemMain.search();});
        $('#excelDownloadBtn').on('click', function() {itemMain.excelDownload();});
        $('input:radio[name="schStoreType"]').on('change', function() {itemMain.changeSchStoreType();});
        $('#schRequiredEmptyAll').on('click', function() {
            let checked = $(this).is(':checked');

            $('.requiredEmpty').each(function() {
                if (checked && !$(this).is(':checked') || !checked && $(this).is(':checked')) {
                    $(this).prop('checked', checked);
                }
            });
        });

        $('.requiredEmpty').on('click', function() {
            if (!$(this).is(':checked') && $('#schRequiredEmptyAll').is(':checked')) {
                $('#schRequiredEmptyAll').prop('checked', false);
            }
        });

        $('#schType').on('change', function() {
            if ($(this).val() == 'itemNo') {
                $("#schKeyword").prop('placeholder', '상품번호 복수검색 시 Enter 또는 ,로 구분');
            } else {
                $("#schKeyword").prop('placeholder', '');
            }
        });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        itemListGrid.excelDownload();
    },
    /**
     * 검색 유효성 검사
     * @returns {boolean}
     */
    valid : function() {
        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");
        let schKeyword = $('#schKeyword').val();

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(schKeyword != "") {
            let schType = $('#schType').val();

            switch(schType) {
                case "itemNo" :
                    let pattern = /[^0-9,]/g;
                    if(pattern.test(schKeyword)) {
                        alert('숫자 또는 ","를 입력하세요');
                        $('#schKeyword').focus();
                        return false;
                    }
                    break;
            }

            if (schKeyword.length < 2) {
                return $.jUtil.alert('검색 키워드는 2자 이상 입력해주세요.', 'schKeyword');
            }
        }

        return true;
    },
    /**
     * 상품 검색
     */
    search : function() {
        if ($('#schType').val() == 'itemNo') {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));
        } else {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ''));
        }

        if (itemMain.valid()) {
            CommonAjax.basic({url:'/item/getItemList.json', data:JSON.stringify($('#itemSearchForm').serializeObject(true)), method:"POST",
                contentType: 'application/json', successMsg:null, callbackFunc:function(res) {
                    itemListGrid.setData(res);
                    $('#itemStatusP, #itemStatusS, #itemStatusA').show();
                    $('#itemTotalCount').html($.jUtil.comma(itemListGrid.gridView.getItemCount()));
                }});
        }
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#itemSearchForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });

        $("#itemSearchForm").resetForm();

        itemMain.initSearchDate('-30d');
    },
    /**
     * 판매업체 조회 팝업창
     */
    getPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
                + '' + "&callback="
                + callBack + "&partnerType="
                + "SELL"
                , "partnerPop", "1080", "600", "yes", "no");
    },
    getStopReason : function(itemStatus, isItemNo) {
        let itemNoList = '';
        let itemNo = $('#itemNo').val();

        if (isItemNo) {
            if ($.jUtil.isEmpty($('#itemNo').val())) {
                return false;
            } else {
                itemNoList = $('#itemNo').val();
            }

        } else {
            itemNoList = '';
            let rowArr = itemListGrid.gridView.getCheckedRows(false);

            if (rowArr.length == 0) {
                alert('상품을 선택해주세요.');
                return false;
            }

            for (let i in rowArr) {
                let rowData = itemListGrid.dataProvider.getJsonRow(rowArr[i]);
                if (rowData.itemStatus != 'T') {
                    itemNoList += ',' + rowData.itemNo;
                }
            }

            itemNoList = itemNoList.substr(1);
        }

        if (itemStatus == 'A') {
            CommonAjax.basic({
                url: "/item/setItemStatus.json",
                data: {
                    itemNo: itemNoList, itemStatus: itemStatus, mallType : mallType
                },
                method: "post",
                callbackFunc: function (res) {
                    alert(res.returnMsg);
                    if (!$.jUtil.isEmpty(itemNo)) {
                        item.getDetail(itemNo);
                    }
                    $('#searchBtn').trigger('click');
                }
            });
        } else {
            $('#stopReasonPop').find('input[name="itemStatus"]').val(itemStatus);
            $('#stopReasonPop').find('input[name="mallType"]').val(mallType);
            $('#stopReasonPop').find('input[name="itemNo"]').val(itemNoList);

            windowPopupOpen("/item/popup/stopReasonPop", "stopReasonPop", 800, 300, "yes", "yes");

            $("#stopReasonPop").attr("method", "post")
            .attr("action", "/item/popup/stopReasonPop")
            .attr("target", "stopReasonPop")
            .submit();

        }
    },
    setItemDispYn : function(dispYn) {
        let itemNo = '';
        let rowArr = itemListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('상품을 선택해주세요.');
            return false;
        }

        for (let i in rowArr) {
            let rowData = itemListGrid.dataProvider.getJsonRow(rowArr[i]);
            if ($.jUtil.isEmpty(itemNo)) {
                itemNo += rowData.itemNo;
            } else {
                itemNo += ',' + rowData.itemNo;
            }
        }

        CommonAjax.basic({
            url: "/item/setItemDispYn.json",
            data: {
                itemNo: itemNo, dispYn: dispYn, mallType : mallType
            },
            method: "post",
            callbackFunc: function (res) {
                alert(rowArr.length + '건의 상품이 변경되었습니다.');
                $('#searchBtn').trigger('click');
            }
        });

    },
    changeSchStoreType : function() {
        $('#schStoreId, #schStoreNm').val('');
    }
};

itemMain.util = {
    /**
     * set disabled
     * @param id
     * @param isBool
     */
    setDisabledAll : function(id, isBool) {
        $('#' + id).find('input,select,textarea,button').prop('disabled', isBool);
    },
    /**
     * select box 초기화 (공통)
     * @param obj
     * @param isInit
     * @param initMsg
     */
    initSelectBoxOptions : function(obj, isInit, initMsg) {
        let initOption = '';

        if (isInit) {
            initOption = '<option value="">' + initMsg + '</option>';
        }

        $(obj).find('option').remove().end().append(initOption).val();
    },
    /**
     * Object key 리턴
     * @param key
     * @returns {*}
     */
    getKey : function(key) {
        return key;
    },
    /**
     * 일자 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDate : function (flag, startId, endId) {
        item.setDate = Calendar.datePickerRange(startId, endId);

        if ($.jUtil.isEmpty(flag)) {
            item.setDate.setEndDate(flag);
        } else {
            item.setDate.setEndDate(0);
        }

        item.setDate.setStartDate(flag);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        item.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        item.setDateTime.setEndDateByTimeStamp(flag);
        item.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * set disabled
     * @param className
     * @param isBool
     */
    setClassDisabledAll : function(className, isBool) {
        $('.' + className).find('input,select,textarea,button').prop('disabled', isBool);
    }
};
