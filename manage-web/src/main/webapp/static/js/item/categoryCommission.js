/**
 * 카테고리별 수수료율
 */

var commissionGrid = {
    gridView : new RealGridJS.GridView("commissionGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        commissionGrid.initGrid();
        commissionGrid.initDataProvider();
        commissionGrid.event();
    },
    initGrid : function(){
        commissionGrid.gridView.setDataSource(commissionGrid.dataProvider);
        commissionGrid.gridView.setStyles(commissionGridBaseInfo.realgrid.styles);
        commissionGrid.gridView.setDisplayOptions(commissionGridBaseInfo.realgrid.displayOptions);
        commissionGrid.gridView.setColumns(commissionGridBaseInfo.realgrid.columns);
        commissionGrid.gridView.setOptions(commissionGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        commissionGrid.dataProvider.setFields(commissionGridBaseInfo.dataProvider.fields);
        commissionGrid.dataProvider.setOptions(commissionGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        commissionGrid.gridView.onDataCellClicked = function(gridView, index) {
            categoryCommission.regFormFillData(index.dataRow);
        };
    },
    setData : function(dataList) {
        commissionGrid.dataProvider.clearRows();
        commissionGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(commissionGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return;
        }
        var fileName =  "commissionList" + moment(new Date()).format("YYYY-MM-DD HH:mm:ss"); //파일명에 : 포함 안되서 _로 변환되어 다운받아짐
        commissionGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }

};

var categoryCommission = {
    init : function() {
        this.event();
        commonCategory.setCategorySelectBox(1,'','',$('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2,'','',$('#searchCateCd2'),true);
        commonCategory.setCategorySelectBox(3,'','',$('#searchCateCd3'),true);
        commonCategory.setCategorySelectBox(4,'','',$('#searchCateCd4'),true);
    },
    event : function() {
        //카테고리 검색 버튼 클릭
        $('#searchBtn').click(function(){
            categoryCommission.search();
        });

        //카테고리 저장 버튼 클릭
        $('#setCategoryBtn').click(function(){
            categoryCommission.setCategory();
        });

        //카테고리 리셋 버튼 클릭
        $('#resetBtn').click(function() {
            categoryCommission.formReset();
        });

        //카테고리 검색 리셋 버튼 클릭
        $('#searchResetBtn').click(function() {
            categoryCommission.searchFormReset();
        });
    },
    search : function() {
        var dispCondition = ["A" , "Y"];
        var url = $('#searchForm').serialize()+"&useYn=Y&commissionUseYn=Y&dispYn="+dispCondition;

        CommonAjax.basic({url:'/item/commission/getCategoryCommission.json?' + url, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            commissionGrid.setData(res);
            $('#commissionCnt').html($.jUtil.comma(commissionGrid.gridView.getItemCount()));
        }});
    },
    searchFormReset : function() {
        $('#searchCateCd1').val('').change();
    },
    formReset : function() {
        $('#one').val('');
        $('#two').val('');
        $('#three').val('');
        $('#four').val('');
        $('#comm').val('');
        $('#cateCd').val('');
    },
    setCategory : function() {

        if($.jUtil.isPercentInput($('#comm').val()) == false) {
            alert("기준 수수료율(%)은 100 이하의 값만 가능합니다.");
            return false;
        }

        CommonAjax.basic({
            url:'/common/category/setCategoryCommission.json',
            data:{
                cateCd : $('#cateCd').val(),
                commissionRate : $('#comm').val(),
                useYn : 'Y'
            },
            method:'get',
            successMsg:'저장 되었습니다.',
            callbackFunc:function() {
                categoryCommission.formReset();
                categoryCommission.search();
            }
        });
    },
    regFormFillData : function (rowId) {
        $('#one').val(commissionGrid.dataProvider.getJsonRow(rowId).lcateNm);
        $('#two').val(commissionGrid.dataProvider.getJsonRow(rowId).mcateNm);
        $('#three').val(commissionGrid.dataProvider.getJsonRow(rowId).scateNm);
        $('#four').val(commissionGrid.dataProvider.getJsonRow(rowId).dcateNm);
        $('#comm').val(commissionGrid.dataProvider.getJsonRow(rowId).commissionRate);
        $('#cateCd').val(commissionGrid.dataProvider.getJsonRow(rowId).dcateCd);
    }
};

$(document).ready(function() {
    commissionGrid.init();
    categoryCommission.init();
});
