/**
 * 상품관리>점포상품관리>묶음형상품관리
 */
$(document).ready(function() {
    itemGroupListGrid.init();
    itemGroupDetailListGrid.init();
    itemGroup.init();
    CommonAjaxBlockUI.global();

});

var itemGroupListGrid = {
    gridView : new RealGridJS.GridView("itemGroupListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemGroupListGrid.initGrid();
        itemGroupListGrid.initDataProvider();
        itemGroupListGrid.event();
    },
    initGrid : function () {
        itemGroupListGrid.gridView.setDataSource(itemGroupListGrid.dataProvider);

        itemGroupListGrid.gridView.setStyles(itemGroupListGridBaseInfo.realgrid.styles);
        itemGroupListGrid.gridView.setDisplayOptions(itemGroupListGridBaseInfo.realgrid.displayOptions);
        itemGroupListGrid.gridView.setColumns(itemGroupListGridBaseInfo.realgrid.columns);
        itemGroupListGrid.gridView.setOptions(itemGroupListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemGroupListGrid.dataProvider.setFields(itemGroupListGridBaseInfo.dataProvider.fields);
        itemGroupListGrid.dataProvider.setOptions(itemGroupListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemGroupListGrid.gridView.onDataCellClicked = function(gridView, index) {

            let rowDataJson = itemGroupListGrid.dataProvider.getJsonRow(index.dataRow);
            itemGroup.search({groupNo : rowDataJson.groupNo});
        };
    },
    setData : function(dataList) {
        itemGroupListGrid.dataProvider.clearRows();
        itemGroupListGrid.dataProvider.setRows(dataList);
    },
};

var itemGroupDetailListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("itemGroupDetailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemGroupDetailListGrid.initGrid();
        itemGroupDetailListGrid.initDataProvider();
        itemGroupDetailListGrid.event();
    },
    initGrid : function () {
        itemGroupDetailListGrid.gridView.setDataSource(itemGroupDetailListGrid.dataProvider);

        itemGroupDetailListGrid.gridView.setStyles(itemGroupDetailListGridBaseInfo.realgrid.styles);
        itemGroupDetailListGrid.gridView.setDisplayOptions(itemGroupDetailListGridBaseInfo.realgrid.displayOptions);
        itemGroupDetailListGrid.gridView.setColumns(itemGroupDetailListGridBaseInfo.realgrid.columns);
        itemGroupDetailListGrid.gridView.setOptions(itemGroupDetailListGridBaseInfo.realgrid.options);
        itemGroupDetailListGrid.gridView.setColumnProperty("representYn", "renderer", {
            "type": "check", "shape":"box", "editable": true, "startEditOnClick": true, "trueValues": "Y", "falseValues": "N", "labelPosition": "center"
        });

    },
    initDataProvider : function() {
        itemGroupDetailListGrid.dataProvider.setFields(itemGroupDetailListGridBaseInfo.dataProvider.fields);
        itemGroupDetailListGrid.dataProvider.setOptions(itemGroupDetailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemGroupDetailListGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemGroupDetailListGrid.selectedRowId = index.dataRow;
            itemGroup.gridDetailRowSelect(index.dataRow)

            if (index.column == "representYn") {
                itemGroup.setRepresentYn(index.dataRow);
            }
        };
    },
    setData : function(dataList) {
        itemGroupDetailListGrid.dataProvider.clearRows();
        itemGroupDetailListGrid.dataProvider.setRows(dataList);
    },
    //그리드 초기화
    clear : function () {
        itemGroupDetailListGrid.dataProvider.clearRows();
        $('#itemGroupDetailTotalCount').html(0);
    },
};
var itemGroup = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.initSearchDate('-30d');
        this.resetForm();
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemGroup.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemGroup.setDate.setEndDate(0);
        itemGroup.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {

        $('#searchResetBtn').bindClick(itemGroup.resetSearchForm);
        $('#searchBtn').bindClick(itemGroup.search);
        $('#resetBtn').bindClick(itemGroup.resetForm);
        $('#setItemGroupBtn').bindClick(itemGroup.setItemGroup);

        $('#itemNo').focusout(itemGroup.getItemName);
        $('#schItemPopBtn').bindClick(itemGroup.addItemPopUp);

        $('#addDetailBtn').bindClick(itemGroup.addItemGroupDetail);
        $('#delDetailBtn').bindClick(itemGroup.deleteItemGroupDetail);
        $('#updateDetailBtn').bindClick(itemGroup.updateItemGroupDetail);
        $('#resetDetailBtn').bindClick(itemGroup.resetItemGroupDetail);

        $('#depth').bindChange(itemGroup.changeDepth);
        $('#groupItemNm').calcTextLength('keyup', '#itemNmCount');
    },
    /**
     * 검색 영역 초기화
     */
    resetSearchForm : function () {

        itemGroup.initSearchDate('-30d');

        $('#schUseYn').val('');
        $('#schType').val('groupNo');
        $('#schKeyword').val('');
    },
    /**
     * 묶음형 상품관리 조회
     * @returns {boolean}
     */
    search : function (param) {

        var form = $('#itemGroupSearchForm').serializeObject();

        if(param.groupNo) {
            form.schType ='groupNo';
            form.schKeyword = param.groupNo;
        }else{
            if(!itemGroup.valid.search()){
                return false;
            }
        }
        
        CommonAjax.basic({
            url: '/item/itemGroup/getItemGroupList.json',
            data:  JSON.stringify(form),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                if(param.groupNo){
                    itemGroup.getItemGroupDetail(res[0]);
                }else {
                    itemGroupListGrid.setData(res);
                    $('#itemGroupTotalCount').html($.jUtil.comma(
                        itemGroupListGrid.gridView.getItemCount()));
                }
            }
        });

    },
    /**
     * 묶음형 상품관리 등록/수정
     * @returns {boolean}
     */
    setItemGroup : function () {

        if(!itemGroup.valid.set()){
            return false;
        }

        var form = $('#itemGroupSetForm').serializeObject();
        form.itemGroupDetailList = itemGroupDetailListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({
                url: '/item/itemGroup/setItemGroup.json',
                data: JSON.stringify(form),
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                alert(res.returnMsg);
                $('#groupNo').val(res.returnKey);
                $('#groupNoText').text(res.returnKey);
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
         });

    },
    /**
     * 묶음형 상품관리 등록창 초기화
     */
    resetForm : function () {

        //기본정보
        $('#groupNo').val('');
        $('#groupNoText').text('');
        $('#groupItemNm').val('');
        $('#depth').val('1').trigger('change');;

        $('#opt1Title').val('');
        $('#opt2Title').val('');
        $('#useYn').val('Y');
        $('#itemNmCount').text(0);

        itemGroup.optBtnShow();

        itemGroup.resetItemGroupDetail();
        itemGroupDetailListGrid.clear();
    },
    /**
     * 묶음형 상품관리 상품조회 팝업
     * @returns {boolean}
     */
    addItemPopUp : function(){
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=itemGroup.applyItemPopupCallback&isMulti=N&mallType=TD&storeType=ALL' , 1084, 650);
    },
    /**
     * 상품 팝업 Callback
     * @param res
     */
    applyItemPopupCallback :function (res) {

        $('#itemNo').val(res[0].itemNo);
        $('#itemNm').val(res[0].itemNm);

    },
    /**
     * 상품명검색
     */
    getItemName : function(){

        let itemNo = $('#itemNo').val().trim();
        if(!$.jUtil.isEmpty(itemNo)){
            if (!$.jUtil.isAllowInput( itemNo, ["NUM", "NOT_SPACE"])) {
                $("#itemNm, #itemNo").val('');
                return $.jUtil.alert("숫자만 입력 가능 합니다.", 'itemNo');

            } else{
                commonItem.getItemName( itemNo, itemGroup.setItemName);
                $('#itemNo').val(itemNo);

            }


        }
    },
    /**
     * 상품명입력
     */
    setItemName : function(res){
        $('#itemNm').val(res.returnKey);
    },
    /**
     * 묶음형 상품관리 상품 추가
     * @returns {boolean}
     */
    addItemGroupDetail : function() {

        if(!itemGroup.valid.add(false)) {
            return false;
        }

        var value = {};

        value.groupNo = $('#groupNo').val();
        value.itemNo  = $('#itemNo').val();
        value.itemNm  = $('#itemNm').val();
        value.opt1Val  = $('#opt1Val').val();
        value.opt2Val  = $('#opt2Val').val();
        value.representYn = "N"
        value.useYn ='Y';

        CommonAjax.basic({
            url: '/item/itemGroup/getOptSelUseYnNm.json?',
            data: { itemNo: $('#itemNo').val()},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {

                value.optSelUseYnNm = res.optSelUseYnNm;
                value.optSelUseYn = res.optSelUseYn;
                value.itemParent = res.itemParent;

                itemGroupDetailListGrid.dataProvider.addRow(value);

            }
        });
        itemGroup.resetItemGroupDetail();
    },
    /**
     * 옵션 수정
     * @returns {boolean}
     */
    updateItemGroupDetail : function () {

        if(!itemGroup.valid.add(true)) {
            return false;
        }

        var rowDataJson = itemGroupDetailListGrid.dataProvider.getJsonRow( itemGroupDetailListGrid.gridView.getCurrent().dataRow );
        var value = {};

        value.groupNo = rowDataJson.groupNo;
        value.itemNo  = rowDataJson.itemNo;
        value.itemNm  = rowDataJson.itemNm;
        value.opt1Val  = $('#opt1Val').val();
        value.opt2Val  = $('#opt2Val').val();
        value.representYn = rowDataJson.representYn;
        value.useYn ='Y';

        CommonAjax.basic({
            url: '/item/itemGroup/getOptSelUseYnNm.json?',
            data: { itemNo: $('#itemNo').val()},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {

                value.optSelUseYnNm = res.optSelUseYnNm;
                value.optSelUseYn = res.optSelUseYn;
                value.itemParent = res.itemParent;

                var row = itemGroupDetailListGrid.gridView.getCurrent().itemIndex;
                itemGroupDetailListGrid.dataProvider.updateRows(row, [value]);
            }
        });
        itemGroup.resetItemGroupDetail();
    },
    /**
     * 옵션 선택삭제
     * @returns {boolean}
     */
    deleteItemGroupSelect : function() {

        var rowArr = itemGroupDetailListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('삭제하려는 옵션을 선택해주세요.');
            return false;
        }

        if (confirm('해당 옵션을 삭제하시겠습니까?')) {
            for (var i in rowArr) {
                itemGroupDetailListGrid.dataProvider.removeRow(rowArr[i] - parseInt(i));
            }

            $('#itemGroupDetailTotalCount').text($.jUtil.comma(itemGroupDetailListGrid.gridView.getItemCount()));
            itemGroup.resetItemGroupDetail();
        }
    },
    /**
     * Detail 삭제
     * @returns {boolean}
     */
    deleteItemGroupDetail : function() {

        var curr = itemGroupDetailListGrid.gridView.getCurrent();
        itemGroupDetailListGrid.dataProvider.removeRow(curr.dataRow);

        $('#addDetailBtn').text('추가');
        itemGroupDetailListGrid.selectedRowId = null;
        itemGroup.resetItemGroupDetail();
    },

    /**
     * 디테일 등록영역 초기화
     */
    resetItemGroupDetail : function () {

        //묶음상품정보
        $('#itemNo').val('');
        $('#itemNm').val('');
        $('#opt1Val').val('');
        $('#opt2Val').val('');

        //신규등록
        itemGroup.optBtnShow();

    },
    /**
     * 묶음형 상품관리 그리드 선택 영역
     * @param selectRowId
     */
    getItemGroupDetail : function (data) {

        itemGroup.resetForm();

        $('#groupNo').val(data.groupNo);
        $('#groupNoText').text(data.groupNo);
        $('#groupItemNm').val(data.groupItemNm);
        $('#depth').val(data.depth);
        $('#opt1Title').val(data.opt1Title);
        $('#opt2Title').val(data.opt2Title);
        $('#useYn').val(data.useYn);

        $('#groupItemNm').setTextLength('#itemNmCount');

        itemGroup.changeDepth();

        CommonAjax.basic( {
            url: '/item/itemGroup/getItemGroupDetailList.json?',
            data: {groupNo: (data.groupNo)},
            method: "GET",
            callbackFunc: function (res) {
                itemGroupDetailListGrid.setData(res);
                $('#itemGroupDetailTotalCount').html($.jUtil.comma(itemGroupDetailListGrid.gridView.getItemCount()));
            }

        })
    },
    setRepresentYn : function( selectRowId ) {

        var itemCnt = itemGroupDetailListGrid.gridView.getItemCount();

        if(itemCnt > 0) {
            for (var i = 0; i < itemCnt ; i++) {
                itemGroupDetailListGrid.gridView.setValue(i, "representYn", 'N');
            }
        }
        itemGroupDetailListGrid.gridView.setValue(selectRowId, "representYn", 'Y');
    },
    /**
     * 묶음형 상품관리 디테일 그리드 선택영역
     * @param selectRowId
     */
    gridDetailRowSelect : function (selectRowId) {

        var rowDtaJson = itemGroupDetailListGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').val(rowDtaJson.itemNo);
        $('#itemNm').val(rowDtaJson.itemNm);

        $('#opt1Val').val(rowDtaJson.opt1Val);
        $('#opt2Val').val(rowDtaJson.opt2Val);

        itemGroup.optBtnHide();
    },
    optBtnHide : function (){
        $('#schItemPopBtn').hide();
        $('#addDetailBtn').hide();
        $('#updateDetailBtn').show();
    },
    optBtnShow : function (){
        $('#schItemPopBtn').show();
        $('#addDetailBtn').show();
        $('#updateDetailBtn').hide();
    },
    /**
     * 옵션단계 변경 이벤트
     */
    changeDepth : function () {

        if($('#depth').val() == '1') {
            $("input[name=opt2Title]").prop("disabled", true);
            $("input[name=opt2Val]").prop("disabled", true);

            $("#opt2Title, #opt2Val").val('');

        } else {
            $("input[name=opt2Title]").prop("disabled", false);
            $("input[name=opt2Val]").prop("disabled", false);
        }
    }
}

/**
 * itemGroup validation.
 * @type {{search: itemGroup.valid.search, set: itemGroup.valid.set}}
 */
itemGroup.valid = {

    search : function () {

        // 날짜 체크
        var startDt = $("#schStartDate");
        var endDt = $("#schEndDate");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            $("#schStartDate").focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#schStartDate").focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("검색 기간은 최대 2년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        var schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
            if( "groupNo" != ($('#schType').val()) && schKeyword.length < 2){
                alert("검색 키워드는 2자리 이상 입력해 주세요");
                $('#schKeyword').focus();
                return false;
            }
        }
        return true;

    },
    set : function() {

        //기본정보 check
        if ($.jUtil.isEmpty($('#groupItemNm').val())) {
            alert("묶음 상품명을 입력해주세요.");
            $('#groupItemNm').focus();
            return false;
        }

        if ($.jUtil.isEmpty($('#opt1Title').val())) {
            alert("1단계 타이틀을 입력해주세요 ");
            $('#opt1Title').focus();
            return false;
        }

        if ($('#depth').val() == "2") {
            if ($.jUtil.isEmpty($('#opt2Title').val())) {
                alert("2단계 타이틀 값을 입력해주세요. ");
                $('#opt2Title').focus();
                return false;
            }
        }

        if(itemGroupDetailListGrid.gridView.getItemCount() <= 0){
            alert("묶음 상품 정보를 등록해주세요 ");
            return false;
        }

        //묶음상품정보 check
        var gridList = itemGroupDetailListGrid.dataProvider.getJsonRows();
        var isCheck = true;
        var cnt = 0;

        if (!$.jUtil.isEmpty(gridList)) {
            //상품용도옵션 체크
            for( var i in gridList) {
                if (gridList[i].optSelUseYn == "Y") {
                    alert("상품용도옵션이 설정되지 않은 일반 상품만 묶음 가능합니다. ")
                    isCheck = false;
                    return false;
                }
                //대표옵션 체크
                if(gridList[i].representYn == 'Y') {
                    cnt += 1;
                }
            }

            if( cnt != 1 ){
                alert("대표상품을 확인해주세요.");
                return false;
            }

            //부모상품 중복 체크
            if (itemGroupDetailListGrid.gridView.getItemCount() > 1 ){
                var _data = itemGroupDetailListGrid.dataProvider.getJsonRow(0);

                for (var i in gridList) {
                    if (_data.itemParent != gridList[i].itemParent) {
                        alert("동일한 parent ID의 상품만 묶음상품 등록이 가능합니다.");
                        isCheck = false;
                        return false;
                    }
                }
            }

        }else {
            alert("묶음상품 정보를 입력헤주세요");
            return false;
        }

        return true;
    },

    add : function (isMod) {

        if( $.jUtil.isEmpty($('#itemNm').val()) || $.jUtil.isEmpty($('#opt1Val').val())) {
            alert("필수 정보를 입력해주세요.");
            return false;
        }

        if (itemGroupDetailListGrid.gridView.getItemCount() > 100){
            alert("묶음 당 최대 100개의 상품까지 설정 할 수 있습니다.");
            return false;
        }

        var gridList = itemGroupDetailListGrid.dataProvider.getJsonRows();

        if($('#depth').val() == "2") {
            //2단계
            if( $.jUtil.isEmpty($('#opt2Val').val())) {
                alert("2단계 옵션 값을 입력해주세요. ");
                $('#opt2Val').focus();
                return false;
            }

            //1단계 + 2단계 옵션값이 기존에 등록된 경우
            for (var i in gridList) {
                if(gridList[i].opt1Val == $('#opt1Val').val() &&
                    gridList[i].opt2Val == $('#opt2Val').val()) {
                    alert("옶션 값이 동일한 상품이 존재합니다. 옵션 값을 수정해주세요.");
                    return false;
                }
            }

        }else{
            //1단계
            for (var i in gridList) {
                if (gridList[i].opt1Val == $('#opt1Val').val()) {
                    alert("옶션 값이 동일한 상품이 존재합니다. 옵션 값을 수정해주세요. ");
                    return false;
                }
            }
        }

        if(!isMod) {
            for (var i in gridList) {
                if (gridList[i].itemNo == $('#itemNo').val()) {
                    alert(" 이미 등록된 상품 입니다. 등록된 상품의 정보를 수정해주세요.");
                    return false;
                }
            }
        }
        return true;
    },

};