/**
 * 기준정보관리 > 금칙어 관리
 */
$(document).ready(function() {
    commissionListGrid.init();
    commission.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();
});

// 금칙어관리 그리드
var commissionListGrid = {
    gridView : new RealGridJS.GridView("commissionListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        commissionListGrid.initGrid();
        commissionListGrid.initDataProvider();
        commissionListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#setCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#setCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#setCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#setCateCd4'));
    },
    initGrid : function() {
        commissionListGrid.gridView.setDataSource(commissionListGrid.dataProvider);

        commissionListGrid.gridView.setStyles(commissionListGridBaseInfo.realgrid.styles);
        commissionListGrid.gridView.setDisplayOptions(commissionListGridBaseInfo.realgrid.displayOptions);
        commissionListGrid.gridView.setColumns(commissionListGridBaseInfo.realgrid.columns);
        commissionListGrid.gridView.setOptions(commissionListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        commissionListGrid.dataProvider.setFields(commissionListGridBaseInfo.dataProvider.fields);
        commissionListGrid.dataProvider.setOptions(commissionListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        commissionListGrid.gridView.onDataCellClicked = function(gridView, index) {
            commission.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        commissionListGrid.dataProvider.clearRows();
        commissionListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(commissionListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "판매자별 기순수수료율_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        commissionListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};


// 판매자별 기준수수료율 관리
var commission = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(commission.search);
        $('#searchResetBtn').bindClick(commission.searchFormReset);
        $('#excelDownloadBtn').bindClick(commission.excelDownload);

        $('#resetBtn').bindClick(commission.resetForm);

        $('#searchPartner').bindClick(commission.popupSearchPartner);

        $('#keyword').calcTextLength('keyup', '#keywordCount');
        $('#keywordDesc').calcTextLength('keyup', '#keywordDescCount');

        $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);

        $('#setCommissionBtn').bindClick(commission.setCommission);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        commissionListGrid.excelDownload();
    },
    /**
     * 파트너 검색 팝업창
     */
    popupSearchPartner : function() {
        $.jUtil.openNewPopup('/item/popup/searchPartnerPopup', 860, 640);
    },
    /**
     * 판매자별 기준수수료율 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }

        if(!searchKeyword || searchKeyword.length < 2) {
            alert("검색어는 최소 2자리 이상 입력해 주세요.");
            $('#searchKeyword').focus();
            return false;
        }

        var lCate = $('#searchCateCd1 option:selected').val();
        var mCate = $('#searchCateCd2 option:selected').val();
        var sCate = $('#searchCateCd3 option:selected').val();
        var dCate = $('#searchCateCd4 option:selected').val();

        if(lCate != "" && mCate == "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(lCate);
        } else if(lCate != "" && mCate != "" && sCate == "" && dCate == "") {
            $('#searchCateCd').val(mCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate == "") {
            $('#searchCateCd').val(sCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate != "") {
            $('#searchCateCd').val(dCate);
        } else {
            $('#searchCateCd').val('');
        }

        CommonAjax.basic({url:'/item/commission/getCommissionList.json?' + $('#commissionSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                commission.resetForm();
                commissionListGrid.setData(res);
                $('#commissionTotalCount').html($.jUtil.comma(commissionListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 판매업체 조회 팝업창
     */
    getPartnerPop : function() {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback=commission.schPartner"
            + "&partnerType="
            + "SELL"
            , "partnerPop", "1080", "600", "yes", "no");
    },
    schPartner : function(res) {
        $('#partnerId').val(res[0].partnerId);
        $('#partnerNm').val(res[0].partnerNm);
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchCateCd1').val('').change();
        $('#searchCateCd').val('');
        $('#searchType').val('partnerNm');
        $('#searchKeyword').val('');
        $('#searchUseYn').val('');
    },
    /**
     * 판매자별 기준수수료율 등록/수정
     * @param isEdit
     */
    setCommission : function() {
        var lCate = $('#setCateCd1 option:selected').val();
        var mCate = $('#setCateCd2 option:selected').val();
        var sCate = $('#setCateCd3 option:selected').val();
        var dCate = $('#setCateCd4 option:selected').val();

        if(lCate != "" && mCate == "" && sCate == "" && dCate == "") {
            $('#cateKind').val('L');
            $('#cateCd').val(lCate);
        } else if(lCate != "" && mCate != "" && sCate == "" && dCate == "") {
            $('#cateKind').val('M');
            $('#cateCd').val(mCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate == "") {
            $('#cateKind').val('S');
            $('#cateCd').val(sCate);
        } else if(lCate != "" && mCate != "" && sCate != "" && dCate != "") {
            $('#cateKind').val('D');
            $('#cateCd').val(dCate);
        } else {
            $('#cateKind').val('');
            $('#cateCd').val('');
        }

        if($.jUtil.isEmpty($('#partnerId').val())) {
            alert("판매자명은 필수 항목 입니다.");
            $('#partnerNm').focus();
            return false;
        } else if($.jUtil.isEmpty($('#cateCd').val())) {
            alert("대분류는 필수 항목 입니다.");
            $('#setCateCd1').focus();
            return false;
        } else if($.jUtil.isEmpty($('#commissionRate').val())) {
            alert("기준수수료율은 필수 항목 입니다.");
            $('#commissionRate').focus();
            return false;
        }

        CommonAjax.basic({
            url : '/item/commission/setCommission.json',
            data : JSON.stringify($('#commissionSetForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                commission.resetForm();
                commission.search();
            }
        });
    },
    /**
     * 판매자별 기준수수료율 등록/수정 폼 초기화
     */
    resetForm : function() {
        $('#commissionSeq').val('');
        $('#partnerId').val('');
        $('#partnerNm').val('');
        $('#commissionRate').val('');
        $('#setCateCd1').val('').change();
        $('#useYn').val('Y');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = commissionListGrid.dataProvider.getJsonRow(selectRowId);

        $('#commissionSeq').val(rowDataJson.commissionSeq);
        $('#partnerId').val(rowDataJson.partnerId);
        $('#partnerNm').val(rowDataJson.partnerNm);
        $('#commissionRate').val(rowDataJson.commissionRate);
        $('#useYn').val(rowDataJson.useYn);

        // Init
        commonCategory.setCategorySelectBox(1, '', '', $('#setCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#setCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#setCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#setCateCd4'));

        switch (rowDataJson.cateKind) {
            case 'L' :
                CommonAjax.basic({url:'/common/category/getCategoryDetail.json?depth=1&cateCd='+rowDataJson.cateCd, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                        commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#setCateCd1'));
                        commonCategory.setCategorySelectBox(2, res.lcateCd, '', $('#setCateCd2'));
                        commonCategory.setCategorySelectBox(3, '', '', $('#setCateCd3'));
                        commonCategory.setCategorySelectBox(4, '', '', $('#setCateCd4'));
                    }});
                break;
            case 'M' :
                CommonAjax.basic({url:'/common/category/getCategoryDetail.json?depth=2&cateCd='+rowDataJson.cateCd, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                        commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#setCateCd1'));
                        commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#setCateCd2'));
                        commonCategory.setCategorySelectBox(3, res.mcateCd, '', $('#setCateCd3'));
                        commonCategory.setCategorySelectBox(4, '', '', $('#setCateCd4'));
                    }});
                break;
            case 'S' :
                CommonAjax.basic({url:'/common/category/getCategoryDetail.json?depth=3&cateCd='+rowDataJson.cateCd, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                        commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#setCateCd1'));
                        commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#setCateCd2'));
                        commonCategory.setCategorySelectBox(3, res.mcateCd, res.scateCd, $('#setCateCd3'));
                        commonCategory.setCategorySelectBox(4, res.scateCd, '', $('#setCateCd4'));
                    }});
                break;
            case 'D' :
                CommonAjax.basic({url:'/common/category/getCategoryDetail.json?depth=4&cateCd='+rowDataJson.cateCd, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                        commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#setCateCd1'));
                        commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#setCateCd2'));
                        commonCategory.setCategorySelectBox(3, res.mcateCd, res.scateCd, $('#setCateCd3'));
                        commonCategory.setCategorySelectBox(4, res.scateCd, res.dcateCd, $('#setCateCd4'));
                    }});
                break;
            default :
                // 모두 전체 Case 이므로 카테고리 정보를 set 하지 않는다.
                break;
        }
    },
    /**
     * 한글 처리 function
     * @param evt
     */
    delKorWord : function (evt) {
        var objTarget = evt.srcElement || evt.target;
        var _value = event.srcElement.value;
        if(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g.test(_value)) {
            objTarget.value = objTarget.value.replace(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g,''); // g가 핵심: 빠르게 타이핑할때 여러 한글문자가 입력되어 버린다.
            objTarget.value = null;
            return false;
        }
    },
     /**
     * 소수점 처리 function
     * @param evt
     */
    isNumberKey : function (evt) {
        // 숫자를 제외한 값을 입력하지 못하게 한다.
        var charCode = (evt.which) ? evt.which : event.keyCode;
        // 수수료율
        var _value = event.srcElement.value;

        if (event.keyCode < 48 || event.keyCode > 57) {
            if (event.keyCode !== 46) { //숫자와 . 만 입력가능하도록함
                return false;
            }
        }

        // 소수점(.)이 두번 이상 나오지 못하게
        var _pattern0 = /^\d*[.]\d*$/; // 현재 value 값에 소수점(.) 이 있으면 . 입력불가
        if (_pattern0.test(_value)) {
            if (charCode == 46) {
                return false;
            }
        }

        // 두자리 이하의 숫자만 입력가능
        var _pattern1 = /^\d{2}$/; // 현재 value 값이 2자리 숫자이면 . 만 입력가능
        // {숫자}의 값을 변경하면 자리수를 조정할 수 있다.
        if (_pattern1.test(_value)) {
            if (charCode != 46 && !_value.endsWith('.')) {
                alert("두자리 이하의 숫자만 입력가능합니다");
                return false;
            }
        }

        // 소수점 둘째자리까지만 입력가능
        var _pattern2 = /^\d*[.]\d{2}$/; // 현재 value 값이 소수점 둘째짜리 숫자이면 더이상 입력 불가
        // {숫자}의 값을 변경하면 자리수를 조정할 수 있다.
        if (_pattern2.test(_value)) {
            alert("소수점 둘째자리까지만 입력가능합니다.");
            return false;
        }
        return true;
    }
};