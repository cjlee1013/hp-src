/**
 * 상품관리 > 점포상품관리 > 점포상품 강제전시
 */

var item = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setItemBtn').bindClick(item.setItemForce);
        $('#resetBtn').bindClick(item.resetForm);
    },
    /**
     * 상품전시 처리
     */
    setItemForce : function() {

        // 상품번호 체크
        if($('#itemNo').val().length < 1 ) {
            alert("상품번호는 필수입력항목입니다.");
            $('#itemNo').focus();
            return false;
        }

        // 상품번호 체크
        if($('#itemNo').val().length != 9 ) {
            alert("상품번호는 9자리 숫자입니다.");
            $('#itemNo').focus();
            return false;
        }

        // 점포 타입 체크
        if($.jUtil.isEmpty($('#storeType').val())) {
            alert("점포타입은 필수선택항목입니다.");
            $('#storeType').focus();
            return false;
        }

        // 점포 코드 체크
        if($('#storeIds').val().length < 2 ) {
            alert("점포코드는 필수입력항목입니다.");
            $('#storeIds').focus();
            return false;
        }

        CommonAjax.basic({
            url : '/item/setItemForceDisplay.json',
            data : JSON.stringify($('#itemDisplaySetForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                item.resetForm();
            }
        });
    },
    /**
     * 입력 폼 초기화
     */
    resetForm : function() {
        $('#itemNo').val('');
        $('#storeType').val('');
        $('#storeIds').val('');

    }
};



