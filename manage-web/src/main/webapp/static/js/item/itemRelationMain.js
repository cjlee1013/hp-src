/**
 * 상품관리>점포상품관리>연관상품관리
 */
$(document).ready(function() {
    itemRelationListGrid.init();
    itemRelationDetailListGrid.init();
    itemRelation.init();
    CommonAjaxBlockUI.global();

});

var itemRelationListGrid = {
    gridView : new RealGridJS.GridView("itemRelationListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemRelationListGrid.initGrid();
        itemRelationListGrid.initDataProvider();
        itemRelationListGrid.event();
    },
    initGrid : function () {
        itemRelationListGrid.gridView.setDataSource(itemRelationListGrid.dataProvider);

        itemRelationListGrid.gridView.setStyles(itemRelationListGridBaseInfo.realgrid.styles);
        itemRelationListGrid.gridView.setDisplayOptions(itemRelationListGridBaseInfo.realgrid.displayOptions);
        itemRelationListGrid.gridView.setColumns(itemRelationListGridBaseInfo.realgrid.columns);
        itemRelationListGrid.gridView.setOptions(itemRelationListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemRelationListGrid.dataProvider.setFields(itemRelationListGridBaseInfo.dataProvider.fields);
        itemRelationListGrid.dataProvider.setOptions(itemRelationListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemRelationListGrid.gridView.onDataCellClicked = function(gridView, index) {
            let rowDataJson = itemRelationListGrid.dataProvider.getJsonRow(index.dataRow);

            itemRelation.search({relationNo : rowDataJson.relationNo});
        };
    },
    setData : function(dataList) {
        itemRelationListGrid.dataProvider.clearRows();
        itemRelationListGrid.dataProvider.setRows(dataList);
        itemRelationListGrid.gridView.orderBy([$('#schDate').val()],[RealGridJS.SortDirection.DESCENDING]);
    },
};

var itemRelationDetailListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("itemRelationDetailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemRelationDetailListGrid.initGrid();
        itemRelationDetailListGrid.initDataProvider();
        itemRelationDetailListGrid.event();
    },
    initGrid : function () {
        itemRelationDetailListGrid.gridView.setDataSource(itemRelationDetailListGrid.dataProvider);

        itemRelationDetailListGrid.gridView.setStyles(itemRelationDetailListGridBaseInfo.realgrid.styles);
        itemRelationDetailListGrid.gridView.setDisplayOptions(itemRelationDetailListGridBaseInfo.realgrid.displayOptions);
        itemRelationDetailListGrid.gridView.setColumns(itemRelationDetailListGridBaseInfo.realgrid.columns);
        itemRelationDetailListGrid.gridView.setOptions(itemRelationDetailListGridBaseInfo.realgrid.options);
        itemRelationDetailListGrid.gridView.setColumnProperty("representYn", "renderer", {
            "type": "check", "shape":"box", "editable": true, "startEditOnClick": true, "trueValues": "Y", "falseValues": "N", "labelPosition": "center"
        });

    },
    initDataProvider : function() {
        itemRelationDetailListGrid.dataProvider.setFields(itemRelationDetailListGridBaseInfo.dataProvider.fields);
        itemRelationDetailListGrid.dataProvider.setOptions(itemRelationDetailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        itemRelationDetailListGrid.dataProvider.clearRows();
        itemRelationDetailListGrid.dataProvider.setRows(dataList);
    },
    //그리드 초기화
    clear : function () {
        itemRelationDetailListGrid.dataProvider.clearRows();
        $('#itemRelationDetailTotalCount').html(0);
    },
    // 상품 그리드 row 이동
    itemGridMoveRow : function(obj) {
        let checkedRows = itemRelationDetailListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert('순서변경할 상품을 선택해 주세요.');
            return false;
        }

        realGridMoveRow(itemRelationDetailListGrid, $(obj).attr('key'), checkedRows);
    }
};
var itemRelation = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initSearchDate('-30d');
        this.resetForm();
        this.initCategorySelectBox();
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        itemRelation.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        itemRelation.setDate.setEndDate(0);
        itemRelation.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {

        $('#searchResetBtn').bindClick(itemRelation.resetSearchForm);
        $('#searchBtn').bindClick(itemRelation.search);
        $('#setItemRelationBtn').bindClick(itemRelation.setItemRelation);

        $('#schItemPopBtn').bindClick(itemRelation.addItemPopUp);

        $('.itemMoveCtrl').on('click', function() {
            itemRelationDetailListGrid.itemGridMoveRow(this);
        });

        $('#relationNm').calcTextLength('keyup', '#relationNmCount');

        $('#schType').on('change', function() {
            if ($(this).val() == 'relationNo') {
                $("#schKeyword").prop('placeholder', '상품번호 복수검색 시 Enter 또는 ,로 구분');
            } else {
                $("#schKeyword").prop('placeholder', '');
            }
        });
        $('#resetBtn').on('click', function() {
            itemRelation.resetForm(true);
        });

        //파일선택시
        $('input[name="introImg"]').change(function() {
            item.img.addImg($(this).data('file-id'), $(this).prop('name'), 3145728, '3MB');
        });

        item.img.event();

    },
    /**
     * 검색 영역 초기화
     */
    resetSearchForm : function () {
        $('#itemSearchForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });

        $("#itemSearchForm").resetForm();

        itemRelation.initSearchDate('-30d');
    },
    /**
     * 연관 상품관리 조회
     * @returns {boolean}
     */
    search : function (params) {
        let data = $('#itemSearchForm').serializeArray();

        if (params.relationNo) {
            data = [ {name: "schType", value: "relationNo"}, {name: "schKeyword", value: params.relationNo}];
        }

        if(itemRelation.valid.search()){
            CommonAjax.basic({
                url: '/item/getItemRelationList.json',
                data: data,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    if (params.relationNo) {
                        itemRelation.getItemRelationDetail(res[0]);
                    } else {
                        itemRelationListGrid.setData(res);
                        $('#itemRelationTotalCount').html($.jUtil.comma(itemRelationListGrid.gridView.getItemCount()));
                    }

                }
            });
        }
    },
    /**
     * 연관 상품관리 그리드 선택 영역
     * @param selectRowId
     */
    getItemRelationDetail : function (data) {

        itemRelation.resetForm();

        $('#relationNo').val(data.relationNo);
        $('#relationNoText').text(data.relationNo);
        $('#relationNm').val(data.relationNm);
        $('#scateCd').val(data.scateCd);
        $('#partnerId').val(data.partnerId);
        $('input:radio[name="dispYn"]:input[value="' + data.dispYn + '"]').trigger('click');
        $('input:radio[name="useYn"]:input[value="' + data.useYn + '"]').trigger('click');
        $('input:radio[name="dispPcType"]:input[value="' + data.dispPcType + '"]').trigger('click');
        $('input:radio[name="dispMobileType"]:input[value="' + data.dispMobileType + '"]').trigger('click');

        $('#relationNm').setTextLength('#relationNmCount');

        if (!$.jUtil.isEmpty(data.imgUrl)) {
            item.img.setImg($('#imgIntro'), {
                'imgNm' : data.imgNm,
                'imgUrl': data.imgUrl,
                'src'   : hmpImgUrl + "/provide/view?processKey=" + $('#imgIntro').data('processkey') + "&fileId=" + data.imgUrl,
            });
        }

        CommonAjax.basic( {
            url: '/item/getItemRelationDetailList.json?',
            data: {relationNo: (data.relationNo)},
            method: "GET",
            callbackFunc: function (res) {
                itemRelationDetailListGrid.setData(res);
                $('#itemRelationDetailTotalCount').html($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
            }

        });
    },
    /**
     * 연관 상품관리 등록/수정
     * @returns {boolean}
     */
    setItemRelation : function () {

        if(!itemRelation.valid.set()){
            return false;
        }

        if (itemRelation.valid.set()) {
            let form = $('#itemRelationSetForm').serializeObject();
            form.relationItemList = [];
            form.useYn = $('input[name="useYn"]:checked').val();

            for(let _idx = 0; _idx < itemRelationDetailListGrid.dataProvider.getRowCount(); _idx++){
                form.relationItemList.push(itemRelationDetailListGrid.dataProvider.getValue(_idx, "itemNo"));
            }

            CommonAjax.basic({
                url: '/item/setItemRelation.json',
                data: JSON.stringify(form),
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                    alert(res.returnMsg);
                    itemRelation.search({relationNo : res.returnKey});
                },
                error: function(e) {
                    if(e.responseJSON.errors[0].detail != null) {
                        alert(e.responseJSON.errors[0].detail);
                    } else {
                        alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                    }
                }
            });
        }
    },
    /**
     * 연관 상품관리 등록창 초기화
     */
    resetForm : function (isEvent) {
        if (!isEvent || confirm("초기화 후 신규연관으로 등록이 가능합니다. 입력정보를 초기화하시겠습니까?")) {
            //기본정보
            $('#itemRelationSetForm').resetForm();
            $('#relationNmCount').text(0);
            $('#relationNo, #scateCd, #partnerId').val('');
            $('#relationNoText').text('');

            itemRelationDetailListGrid.clear();
            item.img.init();
        }
    },
    /**
     * 연관 상품관리 상품조회 팝업
     * @returns {boolean}
     */
    addItemPopUp : function(){

        let url = "/common/popup/itemPop?callback=";
        let callback = "itemRelation.applyItemPopupCallback"
        let param ="&isMulti=Y&mallType=DS&storeType=DS";

        $.jUtil.openNewPopup(
            url + callback + param
            , 1084, 650);
    },
    /**
     * 상품 팝업 Callback
     * @param res
     */
    applyItemPopupCallback :function (param) {
        let data = param.reverse();
        let checkCount = itemRelationDetailListGrid.gridView.getItemCount();
        let setData = [];
        let limitCnt = 50;

        for (let i in data) {
            if (checkCount < limitCnt) {
                if (itemRelationDetailListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [data[i].itemNo]}) == -1) {
                    setData.push(data[i]);
                    checkCount++;
                }
            } else {
                checkCount++;
            }
        }

        for (let i in setData) {
            itemRelation.applyItemPopupCallbackAjax(setData[i]);
        }

        if (checkCount > limitCnt) {
            $.jUtil.alert('하나의 연관에 최대 ' + limitCnt + '개 상품을 설정 할 수 있습니다.');
        }
    },
    applyItemPopupCallbackAjax : function(data) {
        CommonAjax.basic( {
            url: '/item/getItemRelationDetail.json?',
            data: {itemNo: (data.itemNo)},
            method: "GET",
            callbackFunc: function (res) {
                if ($.jUtil.isEmpty($('#scateCd').val()) || itemRelationDetailListGrid.gridView.getItemCount() == 0) {
                    $('#scateCd').val(res.scateCd);
                    $('#partnerId').val(res.partnerId);
                }

                itemRelation.addItemRelationDetail(res);

                $('#itemRelationDetailTotalCount').html($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 연관 상품관리 상품 추가
     * @returns {boolean}
     */
    addItemRelationDetail : function(data) {

        let value = {};

        value.adultTypeNm = data.adultTypeNm;
        value.adultType = data.adultType;
        value.cartLimitYn = data.cartLimitYn;
        value.cartLimitYnNm = data.cartLimitYnNm;
        value.optTxtUseYn = data.optTxtUseYn;
        value.optTxtUseYnNm = data.optTxtUseYnNm;
        value.itemNm = data.itemNm;
        value.itemNo = data.itemNo;
        value.itemStatusNm = data.itemStatusNm;
        value.lcateNm = data.lcateNm;
        value.mcateNm = data.mcateNm;
        value.partnerId = data.partnerId;
        value.saleEndDt = data.saleEndDt;
        value.salePrice = data.salePrice;
        value.saleStartDt = data.saleStartDt;
        value.scateNm = data.scateNm;
        value.scateCd = data.scateCd;

        itemRelationDetailListGrid.dataProvider.addRow(value);
    },
    /**
     * 상품 선택삭제
     * @returns {boolean}
     */
    deleteItemRelationSelect : function() {

        let rowArr = itemRelationDetailListGrid.gridView.getCheckedRows(true);

        if (rowArr.length == 0) {
            alert('삭제하려는 상품을 선택해주세요.');
            return false;
        }

        if (confirm('선택한 상품을 모두 삭제하시겠습니까?')) {
            for (let i in rowArr) {
                itemRelationDetailListGrid.dataProvider.removeRow(rowArr[i] - parseInt(i));
            }

            $('#itemRelationDetailTotalCount').text($.jUtil.comma(itemRelationDetailListGrid.gridView.getItemCount()));
        }
    },
    /**
     * 판매업체 조회 팝업창
     */
    getPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
                + '' + "&callback="
                + callBack + "&partnerType="
                + "SELL"
                , "partnerPop", "1080", "600", "yes", "no");
    }
}

/**
 * itemRelation validation.
 * @type {{search: itemRelation.valid.search, set: itemRelation.valid.set}}
 */
itemRelation.valid = {

    search : function () {
        $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));

        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");
        let schKeyword = $('#schKeyword').val();

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(schKeyword != "") {
            let schType = $('#schType').val();

            switch(schType) {
                case "relationNo" :
                    let pattern = /[^0-9,]/g;
                    if(pattern.test(schKeyword)) {
                        alert('숫자 또는 ","를 입력하세요');
                        $('#schKeyword').focus();
                        return false;
                    }
                    break;
            }

            if (schType != "relationNo" && schKeyword.length < 2) {
                return $.jUtil.alert('검색 키워드는 2자 이상 입력해주세요.', 'schKeyword');
            }
        }

        return true;

    },
    set : function() {
        let checkSet = true;

        //기본정보 check
        if ($.jUtil.isEmpty($('#relationNm').val())) {
            return $.jUtil.alert("연관명을 입력해주세요.", 'relationNm');
        }

        if (itemRelationDetailListGrid.gridView.getItemCount() == 0) {
            return $.jUtil.alert("연관상품 정보를 등록해주세요.");
        }

        let checkAdultTypeJson = itemRelationDetailListGrid.dataProvider.getJsonRow(0);
        let adultType = checkAdultTypeJson.adultType;

        for (let i=0; i < itemRelationDetailListGrid.gridView.getItemCount(); i++) {
            let rowDataJson = itemRelationDetailListGrid.dataProvider.getJsonRow(i);


            if (rowDataJson.adultType != adultType || rowDataJson.cartLimitYn == "Y" || $('#scateCd').val() != rowDataJson.scateCd || $('#partnerId').val() != rowDataJson.partnerId || 'Y' == rowDataJson.optTxtUseYn) {
                checkSet = false;
                return $.jUtil.alert('연관상품의 조건이 다른 상품이 있습니다.');
            }
        }

        return checkSet;
    }
};

itemRelation.callBack = {
    schPartner : function(res) {
        $('#schPartnerId').val(res[0].partnerId);
        $('#schPartnerNm').val(res[0].partnerNm);
    }
};