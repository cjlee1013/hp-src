/**
 * 사이트관리 > 전시 관리 > 상품 추천문구 관리
 */
$(document).ready(function() {
    recomMsgGrid.init();
    recomMsgItemGrid.init();
    CommonAjaxBlockUI.global();
    recomMsgSearch.init();
    recomMsgMain.init();
});

/**
 * 상품 추천문구 조회
 */
var recomMsgSearch = {
    init : function () {
        recomMsgSearch.searchFormReset();
        recomMsgSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#recomMsgSearchForm").resetForm();

        // 조회 기간 초기화
        recomMsgMain.initCalendarDate("SEARCH", "schStartDt", "schEndDt", "-1d", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(recomMsgSearch.searchRecomMsgList);
        $("#schResetBtn").bindClick(recomMsgSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                recomMsgSearch.searchRecomMsgList();
            }
        });
    }
    /**
     * 상품 추천문구 정보 조회
     */
    , searchRecomMsgList : function (recomNo) {
        // 검색조건 validation check
        if (recomMsgMain.valid.searchValid()) {
            // 추천문구 정보 조회 개수 초기화
            $("#recomMsgTotalCount").html("0");

            // 조회
            var recomMsgSearchForm = $("#recomMsgSearchForm").serializeObject();

            CommonAjax.basic({
                url : "/item/recomMsg/getRecomMsgList.json"
                , data : recomMsgSearchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    recomMsgGrid.setData(res);

                    $("#recomMsgTotalCount").html($.jUtil.comma(recomMsgGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(recomNo)) {
                        var rowIndex = recomMsgGrid.dataProvider.searchDataRow({fields : ["recomNo"], values : [recomNo]});

                        if (rowIndex > -1) {
                            recomMsgGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            recomMsgGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

/**
 * 추천문구 등록/수정
 */
var recomMsgMain = {
    selectRecomNo : null
    , storeType : null

    /**
     * 추천문구 상세 초기화
     */
    , init : function () {
        recomMsgMain.bindingEvent();
        recomMsgMain.recomMsgSetFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(recomMsgMain.setRecomMsg);   // 등록
        $("#resetBtn").bindClick(recomMsgMain.recomMsgSetFormReset);    // 초기화
        $("#itemGridSearchBtn").bindClick(recomMsgMain.item.searchItemGridData);    // 상품내역 등록상품 검색

        $("#storeType").bindChange(recomMsgMain.changeStoreType);     // 점포유형 변경시

        // 등록 상품 검색 숫자만 입력
        $("#itemGridSearchValue").allowInput("keyup", ["NUM"]);

        // 관리명 특수문자 확인
        $("#recomMng").allowInput("keyup", ["KOR", "ENG", "NUM", "SPC_1"], $(this).attr("id"));

        // 추천문구 특수문자 확인
        $("#recomMsg").allowInput("keyup", ["KOR", "ENG", "NUM", "SPC_1", "SPC_4"], $(this).attr("id"));

        // 입력 문자 길이 계산
        $("#recomMng").calcTextLength("keyup", "#recomMngLen");
        $("#recomMsg").calcTextLength("keyup", "#recomMsgLen");
    }
    /**
     * 추천문구 상세 내용 초기화
     */
    , recomMsgSetFormReset : function (siteType) {
        $("#recomMsgSetForm").resetForm();
        recomMsgMain.selectRecomNo = null;

        // 전시 기간 초기화
        recomMsgMain.initCalendarDate("SET", "dispStartDt", "dispEndDt", "1d", "15d");

        // hidden 초기화

        // span 초기화
        $("#recomMngLen").html("0");
        $("#recomMsgLen").html("0");
        $("#recomMsgItemCnt").html("0");

        // 그리드 초기화
        recomMsgItemGrid.dataProvider.clearRows();

        // 점포유형
        recomMsgMain.storeType = $("#storeType").val();
    }
    /**
     * 달력 초기화
     */
    , initCalendarDate : function (form, startId, endId, startFlag, endFlag) {
        if (form == "SEARCH") {
            recomMsgSearch.setDate = Calendar.datePickerRange(startId, endId);
            recomMsgSearch.setDate.setEndDate(endFlag);
            recomMsgSearch.setDate.setStartDate(startFlag);
        } else {
            recomMsgMain.setDate = CalendarTime.datePickerRange(startId, endId, {timeFormat: "HH:mm"});
            recomMsgMain.setDate.setEndDateByTimeStamp(endFlag);
            recomMsgMain.setDate.setStartDate(startFlag);
        }
    }
    /**
     * 상세정보
     */
    , getRecomMsgDetail : function (gridData) {
        // 추천문구 일련번호
        recomMsgMain.selectRecomNo = gridData.recomNo;

        $("#dispStartDt").val(gridData.dispStartDt);
        $("#dispEndDt").val(gridData.dispEndDt);
        $("#recomMng").val(gridData.recomMng).setTextLength("#recomMngLen");
        $("#recomMsg").val(gridData.recomMsg).setTextLength("#recomMsgLen");
        $("#storeType").val(gridData.storeType);
        $("#useYn").val(gridData.useYn);

        // 점포유형 정보 저장
        recomMsgMain.storeType = gridData.storeType;

        // 대상상품 조회
        CommonAjax.basic({
            url : "/item/recomMsg/getRecomMsgDetail.json?recomNo=" + gridData.recomNo
            , method : "GET"
            , successMsg : null
            , callbackFunc : function (res) {
                recomMsgItemGrid.setData(res);
            }
        });
    }
    /**
     * 점포유형 변경 시 선택한 상품의 점포유형과 상이한지 확인
     */
    , changeStoreType : function () {
        var storeType = $("#storeType").val();
        var itemListLen = recomMsgItemGrid.dataProvider.getRowCount();

        if (storeType != recomMsgMain.storeType && itemListLen > 0) {
            if (confirm("점포유형 변경시 등록한 상품내역이 초기화 됩니다.\n변경하시겠습니까?")) {
                recomMsgItemGrid.dataProvider.clearRows();
                recomMsgMain.item.setItemCnt();
                recomMsgMain.storeType = storeType;
            } else {
                $("#storeType").val(recomMsgMain.storeType);
            }
        } else {
            recomMsgMain.storeType = storeType;
        }

    }
    /**
     * 추천 문구 등록/수정
     */
    , setRecomMsg : function () {
        if (recomMsgMain.valid.setRecomMsgValid()) {
            var recomMsgSetForm = $("#recomMsgSetForm").serializeObject(true);

            // 추천문구 일련번호
            recomMsgSetForm.recomNo = recomMsgMain.selectRecomNo;

            // 등록 상품
            var matchList = recomMsgItemGrid.dataProvider.getJsonRows();
            var itemNoList = new Array();
            for (var idx in matchList) {
                var itemNo = matchList[idx].itemNo;
                itemNoList.push(itemNo);
            }

            recomMsgSetForm.itemNoList = itemNoList;

            // 기획전 저장 호출
            CommonAjax.basic({
                url : "/item/recomMsg/setRecomMsg.json"
                , data : JSON.stringify(recomMsgSetForm)
                , method : "POST"
                , contentType: 'application/json'
                , successMsg : "등록/수정이 완료되었습니다"
                , callbackFunc : function (res) {
                    recomMsgSearch.searchRecomMsgList(res);
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        recomMsgMain.recomMsgSetFormReset();
                        recomMsgSearch.searchRecomMsgList();
                    }
                }
            });
        }
    }
};

/**
 * 대상 상품 관련
 */
recomMsgMain.item = {
    /**
     * 등록된 대상상품 건수
     */
    setItemCnt : function () {
        $("#recomMsgItemCnt").html($.jUtil.comma(recomMsgItemGrid.dataProvider.getRowCount()));
    }
    /**
     * 그리드에서 상품 검색
     * @returns {*|boolean|void}
     */
    , searchItemGridData : function () {
        if (recomMsgItemGrid.dataProvider.getRowCount() < 1) {
            return alert("검색 가능한 상품이 없습니다.");
        }

        var itemNo = $("#itemGridSearchValue").val();

        // 유효성 검사
        if (!$.jUtil.isAllowInput(itemNo, ["NUM"]) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", "itemGridSearchValue");
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = recomMsgItemGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                recomMsgItemGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 0});
            } else {
                return $.jUtil.alert("검색된 상품이 없습니다.", "itemGridSearchValue");
            }
        }
    }
};

/**
 * 팝업 관련
 */
recomMsgMain.popup = {
    /**
     * 공통팝업 호출
     */
    openPopup : function (flag) {
        var url = "";
        var target = "";
        var callback = "";

        var popWidth = "1084";
        var popHeight = "555";
        var storeType = $("#storeType").val();

        switch (flag) {
            case "ITEM" :
                var mallType;

                if (storeType == "DS") {
                    mallType = "DS";
                } else {
                    mallType = "TD";
                }

                target = "recomItemPopup";
                callback = "recomMsgMain.popup.itemPopCallback";
                url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + $("#siteType").val() + "&storeType=" + $("#storeType").val();
                popHeight = "650";
                break;
            case "EXCEL" :
                target = "recomExcelPop";
                callback = "recomMsgMain.popup.recomMsgExcelCallback";
                url = "/item/popup/recomMsgExcelUploadPop?storeType=" + $("#storeType").val() + "&callback=" + callback;
                popWidth = "600";
                popHeight = "350";
                break;
            default:
                break;
        }

        if (url != "") {
            windowPopupOpen(url, target, popWidth, popHeight);
        } else {
            alert(alertMsg);
        }
    }
    /**
     * 상품 공통팝업 콜백
     *  - 상품번호로 상세정보 조회
     */
    , itemPopCallback : function (resDataArr) {
        recomMsgItemGrid.appendData(resDataArr);
    }
    /**
     * 상품 일괄등록 팝업 콜백
     * - 데이터가 있으면 기존 상품내역 삭제 후 그리드에 추가
     */
    , recomMsgExcelCallback : function (resDataArr) {
        if ($.jUtil.isEmpty(resDataArr)) {
            return $.jUtil.alert("상품정보가 없습니다.")
        } else {
            recomMsgItemGrid.appendData(resDataArr);
        }
    }
};

/**
 * 유효성 검사 관련
 */
recomMsgMain.valid = {
    searchValid : function () {
        var dateFormatForm = "YYYY-MM-DD";

        if (!moment($("#schStartDt").val(), dateFormatForm, true).isValid() || !moment($("#schEndDt").val(), dateFormatForm,true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        return true;
    }
    , setRecomMsgValid : function () {
        var recomMng = $("#recomMng").val();
        var recomMsg = $("#recomMsg").val();

        if ($.jUtil.isEmpty(recomMng)) {
            return $.jUtil.alert("관리명을 입력해주세요.", "recomMng");
        }

        if(!$.jUtil.isAllowInput(recomMng, ["ENG", "NUM", "KOR", "SPC_1"]) || recomMng.length > 10) {
            return $.jUtil.alert("관리명은 최대 10자리의\n한글, 영문, 숫자, 특수문자 사용가능합니다.", "mileageMngNm");
        }

        if ($.jUtil.isEmpty(recomMsg)) {
            return $.jUtil.alert("추천문구를 입력해주세요.", "recomMsg");
        }

        if(!$.jUtil.isAllowInput(recomMsg, ["ENG", "NUM", "KOR", "SPC_1", "SPC_4"]) || recomMsg.length > 14) {
            return $.jUtil.alert("추천문구는 최대 14자리의\n한글, 영문, 숫자, 특수문자 사용가능합니다.", "mileageMngNm");
        }

        var dateFormat = "YYYY-MM-DD HH:mm";
        var nowDt = moment().format(dateFormat);

        // 전시시작기간
        if (!moment($("#dispStartDt").val(), dateFormat, true).isValid()) {
            alert("전시시작일 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#dispEndDt").val(), dateFormat, true).isValid()) {
            alert("전시종료일 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 종료일이 확인
        if (moment($("#dispEndDt").val()).diff(nowDt, "day", true) < 0) {
            alert("종료일은 등록일보다 빠를 수 없습니다.");
            return false;
        }

        // 상품 존재 여부
        if (recomMsgItemGrid.dataProvider.getRowCount() <= 0) {
            return $.jUtil.alert("상품을 등록해 주세요.");
        }

        return true;
    }
};