//상품리스트
var itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
    },
    initGrid : function() {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            onlineStockMng.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
        itemListGrid.gridView.orderBy(['regDt'],[RealGridJS.SortDirection.DESCENDING]);
    }
};

//점포정보 그리드
var storeListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("storeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        storeListGrid.initGrid();
        storeListGrid.event();
        storeListGrid.initDataProvider();
    },
    initGrid : function () {
        storeListGrid.gridView.setDataSource(storeListGrid.dataProvider);
        storeListGrid.gridView.setStyles(storeListGridBaseInfo.realgrid.styles);
        storeListGrid.gridView.setDisplayOptions(storeListGridBaseInfo.realgrid.displayOptions);
        storeListGrid.gridView.setColumns(storeListGridBaseInfo.realgrid.columns);
        storeListGrid.gridView.setOptions(storeListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        storeListGrid.dataProvider.setFields(storeListGridBaseInfo.dataProvider.fields);
        storeListGrid.dataProvider.setOptions(storeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        storeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            storeListGrid.selectedRowId = index.dataRow;
            onlineStockMng.getStoreDetail(storeListGrid.dataProvider.getJsonRow(storeListGrid.selectedRowId));
        };

        $('#storeListGridCopy').on('click', function() {
            storeListGrid.gridView.copyToClipboard();
            alert("텍스트가 복사되었습니다.");
        });
    },
    setData : function(dataList) {
        storeListGrid.dataProvider.clearRows();
        storeListGrid.dataProvider.setRows(dataList);
    }
};

var onlineStockMng = {
    onlineStockYn : '',
    init : function() {
        this.event();
        onlineStockMng.initDateTime('0d', 'onlineStockStartDt', 'onlineStockEndDt', "HH:00:00", "HH:59:59");
        $('#stockQty, #setStockQty, #setRemainCntNaver, #setRemainCntEleven').allowInput("keyup", ["NUM"]);
    },
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        onlineStockMng.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        onlineStockMng.setDateTime.setEndDateByTimeStamp(flag);
        onlineStockMng.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    event : function () {
        $('#searchResetBtn').on('click', function() {onlineStockMng.searchFormReset();});
        $('#searchBtn').on('click', function() {onlineStockMng.search();});
        $('.changeStockQtyCheck').bindChange(onlineStockMng.changeStockQtyCheck);

        $('#itemHist').on('click', function() {
            if (!$.jUtil.isEmpty($('#itemNo').val())) {
                windowPopupOpen('/item/popup/itemHistPop?itemNo=' + $('#itemNo').val() + '&storeType=HYPER', "itemHistPop", 1600, 800);
            }
        });

        $('#resetBtn').on('click', function () {
            onlineStockMng.resetForm();
        });

        $('input[name="onlineStockYn"]').on('change', function() {
            let onlineYn = $(this).is(':checked') ? 'Y' : 'N';
            onlineStockMng.changeOnlindStockYn(onlineYn);
        });

        $('#setType').on('change', function(){
            if (!$.jUtil.isEmpty($(this).val())) {
                let stockPrefix = '비율입력(%) : ';
                switch ($(this).val()) {
                    case 'quantity':
                        stockPrefix = '수량입력(개) : ';
                        $('#setKind, #setEvenNumber').hide();
                        $('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
                        break;
                    case 'rate':
                        $('#setEvenNumber').show();
                        $('#setKind').hide();
                        $('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
                        break;
                    case 'auto':
                        $('#setKind, #setEvenNumber').show();
                        $('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', true);
                        break;
                    case 'mix':
                        $('#setKind, #setEvenNumber').show();
                        $('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
                        break;
                }

                $('#stockPrefix').text(stockPrefix);
            } else {
                $('#setKind, #setEvenNumber').hide();
                $('#stockPrefix').text('수량입력(개) : ');
                $('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);
        onlineStockMng.getDetail(rowDataJson.itemNo);
    },
    /**
     * 상품 검색 초기화
     */
    searchFormReset : function() {
        $('#itemSearchForm').resetForm();
    },
    /**
     * 상품 검색
     */
    search : function() {
        $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));

        if (onlineStockMng.valid()) {
            CommonAjax.basic({
                url: '/item/getOnlineStockList.json',
                data: $('#itemSearchForm').serializeObject(true),
                method: "GET",
                contentType: 'application/json',
                successMsg: null,
                callbackFunc: function (res) {
                    itemListGrid.setData(res);
                    $('#itemTotalCount').html($.jUtil.comma(itemListGrid.gridView.getItemCount()));
                }
            });
        }
    },
    /**
     * 검색 유효성 검사
     * @returns {boolean}
     */
    valid : function() {
        let schKeyword = $('#schKeyword').val();

        if($('input[name="schType"]:checked').val() == 'itemNo') {
            let schType = $('#schType').val();

            switch(schType) {
                case "itemNo" :
                    let pattern = /[^0-9,]/g;
                    if(pattern.test(schKeyword)) {
                        alert('숫자 또는 ","를 입력하세요');
                        $('#schKeyword').focus();
                        return false;
                    }
                    break;
            }

            if (schKeyword.length < 2) {
                return $.jUtil.alert('검색 키워드는 2자 이상 입력해주세요.', 'schKeyword');
            }
        } else if ($('input[name="schType"]:checked').val() == 'itemNo') {
            return $.jUtil.alert('상품번호를 입력해주세요.', 'schKeyword');
        }

        return true;
    },
    resetForm : function() {
        $('#itemSetForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });

        $('#itemNo, #setStockQty').val('');
        $('#itemNoView, #itemNmView').html('');
        $('#setType option[value="rate"], #setType option[value="auto"], #setType option[value="mix"]').css('display', 'block').trigger('change');
        $('input[name="onlineStockYn"]').prop('checked', false).trigger('change');
        $('#itemShipTotalCount').text(0);

        onlineStockMng.getStoreDetail();
        onlineStockMng.onlineStockYn = '';
        storeListGrid.dataProvider.clearRows();
    },
    /**
     * 상품상세 > 조회
     * @param itemNo
     */
    getDetail : function(itemNo) {
        if (itemNo) {
            onlineStockMng.resetForm();
            CommonAjaxBlockUI.startBlockUI();
            CommonAjax.basic({
                url: '/item/getDetail.json',
                data: {itemNo: itemNo, mallType: 'TD', storeId:0, menuType:'detail', storeType: 'HYPER', onlineStockMngYn: 'Y'},
                method: 'GET',
                callbackFunc: function (data) {
                    $('#itemNo').val(itemNo);
                    $('#itemNoView').html(itemNo);
                    $('#itemNmView').html(data.itemNm);

                    if (data.stockType == 'N') {
                        $('#setType option[value="rate"], #setType option[value="auto"], #setType option[value="mix"]').css('display', 'none');
                    } else {
                        $('#setType option[value="rate"], #setType option[value="auto"], #setType option[value="mix"]').css('display', 'block');
                    }

                    onlineStockMng.onlineStockYn = data.onlineStockYn;
                    onlineStockMng.changeOnlindStockYn(data.onlineStockYn, data.onlineStockStartDt, data.onlineStockEndDt, data.stockTypeNm);

                    storeListGrid.setData(data.storeList);
                    $('#itemShipTotalCount').text($.jUtil.comma(storeListGrid.gridView.getItemCount()));
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    getStoreDetail : function (rowData) {
        if (rowData) {
            onlineStockMng.getStoreDetail();
            storeListGrid.selectedRowId = storeListGrid.gridView.getCurrent().dataRow;

            $('#storeId').text(rowData.storeId);
            $('#storeNm').text(rowData.storeNm);
            $('#storeTypeNm').text(rowData.storeTypeNm);
            $('#storeKindNm').text(rowData.storeKindNm);
            $('#pfrYnNm').text(rowData.pfrYnNm);
            $('#stockQty').val(rowData.stockQty).prop('readonly', true);
            $('#remainCntNaver').val(rowData.remainCntNaver).prop('readonly', true);
            $('#remainCntEleven').val(rowData.remainCntEleven).prop('readonly', true);
            $('#salePrice').text($.jUtil.comma(rowData.salePrice) + ' 원');
            if (rowData.stopDealYn == 'Y') {
                $('#stopReason').val(rowData.stopReason);
                $('#stopLimitYn').val(rowData.stopLimitYn).trigger('change');
                $('#stopStartDt').val(rowData.stopStartDt);
                $('#stopEndDt').val(rowData.stopEndDt);
            }

            $('#safeNumberUseYnNm').text(rowData.safeNumberUseYnNm);
            $('.changeStockQtyLabel').show();
            $('.changestockQtyCheck').prop('checked', false).trigger('change');

            $('input:radio[name="virtualStoreOnlyYn"]:input[value="'+rowData.virtualStoreOnlyYn+'"]').trigger('click');
            $('input:radio[name="stopDealYn"]:input[value="'+rowData.stopDealYn+'"]').trigger('click').prop('checked', true);
            $('input:radio[name="drctYn"]:input[value="'+rowData.drctYn+'"]').trigger('click');
            $('input:radio[name="dlvYn"]:input[value="'+rowData.dlvYn+'"]').trigger('click');
            $('input:radio[name="pickYn"]:input[value="'+rowData.pickYn+'"]').trigger('click');
            $('input:radio[name="quickYn"]:input[value="'+rowData.quickYn+'"]').trigger('click');

        } else {
            storeListGrid.selectedRowId = null;
            $('#storeId, #storeNm, #storeTypeNm, #salePrice, #pfrYnNm, #storeKindNm').text('');
            $("#shipArea").find('input[type=text]').val('');
            $('#stopLimitYn').val('');

            $('.changeStockQtyLabel').hide();
            $('#stockQty').prop('readonly', false);
            $('.changeStockQtyCheck').prop('checked', false);

            $('#stopLimitYn, #stopReason, #stopStartDt, #stopEndDt').prop('disabled', true);
        }
    },
    /**
     * 재고수량변경 체크박스 변경 이벤트
     * @param obj
     */
    changeStockQtyCheck : function(obj) {
        if ($('input[name="onlineStockYn"]:checked').val() == 'Y') {
            let targetId = '#' + $(obj).data("param1");

            if ($(obj).is(':checked')) {
                $(targetId).prop('readonly', false);
            } else {
                $(targetId).prop('readonly', true);
            }
        }
    },
    /**
     * 온라인재고설정 셋팅
     * @param onlineStockYn
     * @param onlineStockStartDt
     * @param onlineStockEndDt
     */
    changeOnlindStockYn : function(onlineStockYn, onlineStockStartDt, onlineStockEndDt, stockTypeNm) {
        $('input[name="onlineStockYn"][value="'+onlineStockYn+'"]').prop('checked', true);

        if (onlineStockYn == 'Y') {
            $('#onlineStockStartDt, #onlineStockEndDt').prop('disabled', false);
            $('#onlineStockStartDt').val(onlineStockStartDt);
            $('#onlineStockEndDt').datetimepicker('option', 'minDate', onlineStockStartDt );
            $('#onlineStockEndDt').val(onlineStockEndDt);
            $('#stockQty').prop('readonly', false);
        } else {
            $('#onlineStockStartDt, #onlineStockEndDt').val('').prop('disabled', true);
            $('#stockQty').val('').prop('readonly', true);
        }

        if (stockTypeNm) {
            $('#stockTypeNm').text(stockTypeNm);
        }

        $('.changeStockQtyCheck').prop('checked', false);
    },
    setStore : function() {
        if ($.jUtil.isEmpty($('#storeId').text())) {
            alert('점포를 선택해 주세요.');
        } else {
            let row = storeListGrid.selectedRowId;
            let values = storeListGrid.dataProvider.getJsonRow(row);

            values.stockQtyBasic            = $('#stockQty').val();
            values.stockQty                 = $('#stockQty').val();
            values.stockQtyNaver            = $('#remainCntNaver').val();
            values.remainCntNaver           = $('#remainCntNaver').val();
            values.stockQtyEleven         = $('#remainCntEleven').val();
            values.remainCntEleven        = $('#remainCntEleven').val();
            values.chgStockQtyYn            = $('#changeStockQtyCheck').is(':checked') ? 'Y' : 'N';
            values.chgStockQtyYnNaver       = $('#changeStockQtyCheckNaver').is(':checked') ? 'Y' : 'N';
            values.chgStockQtyYnEleven    = $('#changeStockQtyCheckEleven').is(':checked') ? 'Y' : 'N';

            storeListGrid.dataProvider.updateRows(row, [values]);

            $('#itemShipTotalCount').text($.jUtil.comma(storeListGrid.gridView.getItemCount()));

            alert('적용되었습니다.');
        }
    },
    setOnlineStock : function() {
        let itemForm        = $('#itemSetForm').serializeObject(true);
        itemForm.storeList  = storeListGrid.dataProvider.getJsonRows();

        if (onlineStockMng.onlineStockYn == 'N') {
            itemForm.storeList.forEach(function(data, index){
                data.chgStockQtyYn = 'Y';
            });
        }

        CommonAjax.basic({
            url: '/item/setOnlineStock.json',
            data: JSON.stringify(itemForm),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            successMsg:null,
            callbackFunc:function(res) {
                onlineStockMng.getDetail($('#itemNoView').text());
                alert(res.returnMsg);
            }
        });
    },
    setStockGrid : function() {
        CommonAjax.basic({
            url: '/item/getStockTemplate.json',
            contentType: 'application/json',
            method:"GET",
            successMsg:null,
            callbackFunc:function(res) {
                let stockTemplate = res;
                let setType = $('#setType').val();
                let setKind = $('#setKind').val();

                if (setType != 'auto' && ($.jUtil.isEmpty($('#setStockQty').val()) || $.jUtil.isEmpty($('#setRemainCntNaver').val()) || $.jUtil.isEmpty($('#setRemainCntEleven').val()))) {
                    return $.jUtil.alert('수량 입력은 필수 입니다.');
                }

                if ($.jUtil.isEmpty(setType)) {
                    return $.jUtil.alert('일괄설정방법 입력해주세요.');
                }

                if (setType == 'auto' || setType == 'mix') {
                    if ($.jUtil.isEmpty(setKind)) {
                        return $.jUtil.alert('일괄설정방법 입력해주세요.');
                    }
                }

                let rowArr = storeListGrid.gridView.getCheckedItems();
                let setValueObj = {};
                let setDefaultObj = {};
                let setStockQty = Number($('#setStockQty').val());
                let remainCntNaver = Number($('#setRemainCntNaver').val());
                let remainCntEleven = Number($('#setRemainCntEleven').val());

                if (rowArr.length == 0) {
                    return $.jUtil.alert('점포선택 및 일괄설정방법 입력해주세요.');
                }

                if (setType != 'auto' && (setStockQty < remainCntNaver || setStockQty < remainCntEleven)) {
                    return $.jUtil.alert('제휴사 온라인재고는 홈플러스 온라인재고보다 크게 설정 할 수 없습니다.');
                }

                setValueObj['stockQty'] = setStockQty;
                setValueObj['remainCntNaver'] = remainCntNaver;
                setValueObj['remainCntEleven'] = remainCntEleven;
                setValueObj['chgStockQtyYn'] = 'Y';
                setValueObj['chgStockQtyYnNaver'] = 'Y';
                setValueObj['chgStockQtyYnEleven'] = 'Y';

                setDefaultObj['stockQty'] = {'default' : 'onlineStockDefault', 'basic' : 'stockQtyBasic', 'setKind' : setKind};
                setDefaultObj['remainCntNaver'] = {'default' : 'onlineStockDefaultNaver', 'basic' : 'stockQtyNaver', 'setKind' : 'naver'};
                setDefaultObj['remainCntEleven'] = {'default' : 'onlineStockDefaultEleven', 'basic' : 'stockQtyEleven', 'setKind' : 'eleven'};

                for (let i in rowArr) {
                    let stockQtyOff = Number(storeListGrid.gridView.getValue(rowArr[i], 'stockQtyOff'));
                    let stock = 0;

                    for (let j in setValueObj) {

                        if (j == 'stockQty' || j == 'remainCntNaver' || j == 'remainCntEleven') {
                            let setRate = 0;

                            if (setType == 'quantity') {
                                stock = setValueObj[j];
                            } else {
                                switch (setType) {
                                    case 'rate' :
                                        setRate = onlineStockMng.getFcRate(storeListGrid.gridView.getValue(rowArr[i], 'fcStatusFlag'), setValueObj[j]);

                                        if (stockQtyOff > 0) {
                                            stock = Math.floor((stockQtyOff * setRate) / 100);
                                        }

                                        break;
                                    case 'auto' :
                                        if (!$.jUtil.isEmpty(stockTemplate[storeListGrid.gridView.getValue(rowArr[i], 'storeId')])) {
                                            setRate = onlineStockMng.getFcRate(storeListGrid.gridView.getValue(rowArr[i], 'fcStatusFlag'), stockTemplate[storeListGrid.gridView.getValue(rowArr[i], 'storeId')][setDefaultObj[j]['setKind']]);
                                        }

                                        if (stockQtyOff > 0) {
                                            stock = Math.floor((stockQtyOff * setRate) / 100);
                                        }

                                        break;
                                    case 'mix' :
                                        if (!$.jUtil.isEmpty(stockTemplate[storeListGrid.gridView.getValue(rowArr[i], 'storeId')])) {
                                            setRate = onlineStockMng.getFcRate(storeListGrid.gridView.getValue(rowArr[i], 'fcStatusFlag'), stockTemplate[storeListGrid.gridView.getValue(rowArr[i], 'storeId')][setDefaultObj[j]['setKind']], setValueObj[j]);
                                        }

                                        if (stockQtyOff > 0) {
                                            stock = Math.floor((stockQtyOff * setRate) / 100);
                                        }

                                        break;
                                }

                                storeListGrid.gridView.setValue(rowArr[i], setDefaultObj[j]['default'], setRate + '%');

                                if ($('#setEvenNumber').val() == 'E' && (stock % 2) != 0) {
                                    stock = stock - 1;
                                }
                            }

                            storeListGrid.gridView.setValue(rowArr[i], j, stock);
                            storeListGrid.gridView.setValue(rowArr[i], setDefaultObj[j]['basic'], stock);
                        } else {
                            storeListGrid.gridView.setValue(rowArr[i], j, setValueObj[j]);
                        }
                    }
                }

                return $.jUtil.alert('저장되었습니다.');
            }
        });

    },
    getOnlineStockMngPop : function() {
        windowPopupOpen('/item/popup/onlineStockMngPop', "onlineStockMngPop", 1000, 1000);
    },
    setOnlineStockPop : function() {
        let rowArr = itemListGrid.gridView.getCheckedItems();

        if (rowArr.length == 0) {
            return $.jUtil.alert('상품을 선택 해주세요.');
        }

        if (rowArr.length > 20) {
            return $.jUtil.alert('상품을 20개이하로 선택해주세요.');
        }

        windowPopupOpen('/item/popup/setOnlineStockPop', "setOnlineStockPop", 800, 370);
    },
    getFcRate : function(fcStatusFlag, setRateAuto, setRate) {
        if (fcStatusFlag == '40'|| fcStatusFlag == '50') {
            return 100;
        }

        if (setRate && setRateAuto < setRate) {
            setRateAuto = setRate;
        }

        return setRateAuto;
    }
};