/**
 * 브랜드/제조사관리 > 제조사 관리
 */
$(document).ready(function() {
    makerListGrid.init();
	  makerMain.init();
    CommonAjaxBlockUI.global();
});

// 제조사관리 그리드
var makerListGrid = {
    gridView : new RealGridJS.GridView("makerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        makerListGrid.initGrid();
        makerListGrid.initDataProvider();
        makerListGrid.event();
    },
    initGrid : function() {
        makerListGrid.gridView.setDataSource(makerListGrid.dataProvider);

        makerListGrid.gridView.setStyles(makerListGridBaseInfo.realgrid.styles);
        makerListGrid.gridView.setDisplayOptions(makerListGridBaseInfo.realgrid.displayOptions);
        makerListGrid.gridView.setColumns(makerListGridBaseInfo.realgrid.columns);
        makerListGrid.gridView.setOptions(makerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        makerListGrid.dataProvider.setFields(makerListGridBaseInfo.dataProvider.fields);
        makerListGrid.dataProvider.setOptions(makerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        makerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            makerMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        makerListGrid.dataProvider.clearRows();
        makerListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(makerListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "제조사관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        makerListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var makerMain = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function () {

        $('#searchResetBtn').bindClick(makerMain.searchFormReset);
        $('#setMakerBtn').bindClick(makerMain.setMaker);
        $('#editMakerBtn').bindClick(makerMain.setMaker, true);
        $('#resetBtn').bindClick(makerMain.resetForm);
        $('#searchBtn').bindClick(makerMain.search);
        $('#excelDownloadBtn').bindClick(makerMain.excelDownload);

        $('#makerNm').calcTextLength('keyup', '#textCountKr');

        $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
        $('#makerNm').notAllowInput('focusout', ['SYS_DIVISION']);

        $("#siteUrl").on({
            keyup: function(e) {
                if (e.which === 8 || e.which === 9 || e.which === 37 || e.which === 39 || e.which === 46) return;
                this.value = this.value.replace(/[\ㄱ-ㅎㅏ-ㅣ가-힣]/g, "");

            },
            change: function() {
                this.value = this.value.replace(/[\ㄱ-ㅎㅏ-ㅣ가-힣]/g, "");
            }
        });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        makerListGrid.excelDownload();
    },
    /**
     * 제조사 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }

        CommonAjax.basic({url:'/item/maker/getMakerList.json?' + $('#makerSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            makerListGrid.setData(res);
            $('#makerTotalCount').html($.jUtil.comma(makerListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchType').val('makerNm');
        $('#searchKeyword').val('');
    },
    /**
     * 제조사 등록/수정
     */
    setMaker : function() {
        let isReg = $.jUtil.isEmpty($('#makerNo').val());

        if($('#makerNm').val() == "") {
            alert("제조사명은 필수입력항목입니다.");
            $('#makerNm').focus();
            return false;
        }

        if ($.jUtil.isNotAllowInput($("#makerNm").val(), ['SYS_DIVISION'])) {
            $.jUtil.patternReplace($('#makerNm'), ['SYS_DIVISION'], "");
            alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]));
            $('#makerNm').focus();
            return false;
        }

        CommonAjax.basic({
            url: '/item/maker/setMaker.json',
            data: $('#makerSetForm').serializeArray(),
            method: 'POST',
            callbackFunc: function (res) {
                alert(res.returnMsg);
                makerMain.search();
                if (isReg) {
                    makerMain.resetForm();
                }
            }
        });
    },
    /**
     * 제조사 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#makerNo, #makerNm, #makerNmEng, #siteUrl').val('');
        $('#useYn').val('Y');
        $('#textCountEng, #textCountKr').html('0');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = makerListGrid.dataProvider.getJsonRow(selectRowId);

        $('#makerNo').val(rowDataJson.makerNo);
        $('#makerNm').val(rowDataJson.makerNm).setTextLength('#textCountKr');
        $('#useYn').val(rowDataJson.useYn);
        $('#siteUrl').val(rowDataJson.siteUrl);
    }
};
