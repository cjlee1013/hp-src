$(document).ready(function () {
    onedayItemMng.init();
    onedayItemListGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var onedayItemListGrid = {
    gridView: new RealGridJS.GridView("onedayItemListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        onedayItemListGrid.initGrid();
        onedayItemListGrid.initDataProvider();
        onedayItemListGrid.event();
    },
    initGrid: function () {
        onedayItemListGrid.gridView.setDataSource(onedayItemListGrid.dataProvider);

        onedayItemListGrid.gridView.setStyles(onedayItemGridBaseInfo.realgrid.styles);
        onedayItemListGrid.gridView.setDisplayOptions(onedayItemGridBaseInfo.realgrid.displayOptions);
        onedayItemListGrid.gridView.setColumns(onedayItemGridBaseInfo.realgrid.columns);
        onedayItemListGrid.gridView.setOptions(onedayItemGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        onedayItemListGrid.dataProvider.setFields(onedayItemGridBaseInfo.dataProvider.fields);
        onedayItemListGrid.dataProvider.setOptions(onedayItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        onedayItemListGrid.gridView.onDataCellClicked = function (gridView, index) {
            // onedayItemMng.getGrid(index.dataRow);
        };
    },
    setData: function (dataList) {
        onedayItemListGrid.dataProvider.clearRows();
        onedayItemListGrid.dataProvider.setRows(dataList);
    }
};

var onedayItemMng = {
    year: null,             // 출력 달의 년(YYYY)
    month: null,            // 출력 달의 월(M/MM)
    firstDayOfWeek: null,   // 출력 달의 1일자 요일
    today: null,            // 오늘 날짜 (YYYY-MM-DD)
    selectTdId: null,       // 선택한 td id
    dispDt: null,           // 전시날짜
    isUpdate: false,
    onProgress: false,
    onedayItemList : {},    // 싸데이 아이템 리스트

    init: function () {
        this.bindingEvent();
        this.initSetDate();     // today date setting
        this.getCalendarData(); // display calendar data

        onedayItemList = new Array(); // 상품 리스트
    },
    /**
     * 오늘 년,월,일 조회 후 세팅
     */
    initSetDate: function(){
        var date = $.jUtil.getCurrentDate();
        var tmpDate = date.split("-");

        onedayItemMng.year = tmpDate[0];
        onedayItemMng.month = tmpDate[1];
        onedayItemMng.today = date;
    },
    bindingEvent: function () {
        $('#searchBtn').bindClick(onedayItemMng.search);
        $('#setOnedayItem').bindClick(onedayItemMng.setOnedayItem);
        $('#resetOnedayItem').bindClick(onedayItemMng.resetForm);
        $('#addItemPopUp').bindClick(onedayItemMng.addItemPopUp);
        $('#schGridItemBtn').bindClick(onedayItemMng.schGridItemNo);

        // 사이트 구분 변경시
        $('input[name="siteType"]').change(function () {
            onedayItemMng.resetCalendar();
            onedayItemMng.setDispStoreType($(this));
            onedayItemListGrid.dataProvider.clearRows();
            onedayItemList = new Array(); // 상품 리스트
            $("#"+onedayItemMng.dispDt).removeAttr("style");
            onedayItemMng.dispDt = null;
            onedayItemMng.resetForm();
        });

        // 스토어 구분 변경시
        $('#storeType').on('change', function() {
            onedayItemMng.setItemStoreType($(this));
        });

        // click event binding ( calandar )
        $("#prevMonth").bindClick(onedayItemMng.clickEventBinding, "prevMonth");    // [이전 달] 버튼
        $("#nextMonth").bindClick(onedayItemMng.clickEventBinding, "nextMonth");    // [다음 달] 버튼
        $("#prevYear").bindClick(onedayItemMng.clickEventBinding, "prevYear");      // [이전 년도] 버튼
        $("#nextYear").bindClick(onedayItemMng.clickEventBinding, "nextYear");      // [다음 년도] 버튼
        $("#saveBtn").bindClick(onedayItemMng.resetBusinessDay);                    // [초기화] 버튼
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = onedayItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                onedayItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
            } else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * 전시여부 변경
     */
    chgDispYn : function (action) {
        var checkedRows = onedayItemListGrid.gridView.getCheckedRows(false);
        var setDispYn, setDispYnNm;

        if (checkedRows.length == 0) {
            alert("전시여부 변경할 상품을 선택해 주세요.");
            return false;
        }

        switch (action) {
            case 'Y' :
                setDispYn = 'Y';
                setDispYnNm = '전시';
                break;
            case 'N' :
                setDispYn = 'N';
                setDispYnNm = '전시안함';
                break;
        }

        // RealGrid 값 변경.
        var setGridValue = { dispYn : setDispYn, dispYnTxt : setDispYnNm };
        checkedRows.forEach(function(data) {
            onedayItemListGrid.gridView.setValues(data, setGridValue, true);
        })

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        onedayItemMng.onedayItemList = new Array();
        onedayItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            // var siteType = $('input[name="siteType"]:checked').val();  -- [CLUB] Spec out 처리되어 주석처리.
            var itemNo = onedayItemListGrid.gridView.getValue(idx, 0);
            var dispYn = onedayItemListGrid.gridView.getValue(idx, 6);
            var itemStoreType = onedayItemListGrid.gridView.getValue(idx, 8);

            onedayItemList[idx] = {
                dispDt : onedayItemMng.dispDt
                , dispYn : dispYn
                , itemNo : itemNo
                , siteType : 'HOME'
                , itemStoreType : itemStoreType
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 상품 업로드 팝업 ( 엑셀 )
     */
    openItemReadPopup : function () {
        var target = "onedayItemMng";
        var targetDisp = onedayItemMng.dispDt;
        var callback = "onedayItemMng.itemExcelPopCallback";
        var url = "/item/popup/uploadOnedayItemPop?callback=" + callback + "&targetDisp=" + targetDisp;

        if($.jUtil.isEmpty(targetDisp)) {
            alert('등록일자가 존재하지 않습니다.');
            return;
        }

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function (data) {
        onedayItemMng.setItemInfoByExcel(data);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfoByExcel : function(resData) {
        var addObject = new Array();


        resData.forEach(function (data, index) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = onedayItemListGrid.dataProvider.searchDataRow(options);
            var mallType = 'TD'
            var mallTypeNm = '점포상품';
            var setItemStoreType = data.itemStoreType;
            if($.jUtil.isNotEmpty(setItemStoreType) && setItemStoreType === 'DS') {
                mallType = 'DS';
                mallTypeNm = '셀러상품'
            }

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , mallTypeNm : mallTypeNm
                , itemNm : data.itemNm
                , dispYnTxt: data.dispYnTxt
                , regNm : ''
                , regDt : ''
                , dispYn : data.dispYn
                , siteType : ''
                , itemStoreType : setItemStoreType
                , mallType : mallType
                , priority : index
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        onedayItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        onedayItemMng.onedayItemList = new Array();
        onedayItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            // var siteType = $('input[name="siteType"]:checked').val();  -- [CLUB] Spec out 처리되어 주석처리.
            var itemNo = onedayItemListGrid.gridView.getValue(idx, 0);
            var dispYn = onedayItemListGrid.gridView.getValue(idx, 6);
            var itemStoreType = onedayItemListGrid.gridView.getValue(idx, 8);

            onedayItemList[idx] = {
                dispDt : onedayItemMng.dispDt
                , dispYn : dispYn
                , itemNo : itemNo
                , siteType : 'HOME'
                , itemStoreType : itemStoreType
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var storeType = $('#storeType').val();
        var siteType  = $('input[name="siteType"]:checked').val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(storeType === 'TD') {
            $('#storeType').append('<option value="DS">셀러상품</option>');
            $dispStore.prepend($('#storeTypeHome').html());
        } else {
            $dispStore.prepend($('#storeTypeNone').html());
        }

        $dispStore.find('select').attr('name' ,'itemStoreType');
        $dispStore.show();
    },
    /**
     * 노출 상품 구분 영역 세팅
     */
    setItemStoreType : function(obj) {
        var storeType = $('#storeType').val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(storeType === 'TD') {
            $dispStore.prepend($('#storeTypeHome').html());
        } else {
            $dispStore.prepend($('#storeTypeNone').html());
        }

        $dispStore.find('select').attr('name' ,'itemStoreType');
        $dispStore.show();
    },
    /**
     * click event binding
     */
    clickEventBinding: function(gubun) {
        switch (gubun) {
            case "prevMonth" :  // [이전 달] 버튼
                if(onedayItemMng.year === 2000 && onedayItemMng.month === 1){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return false;
                }
                if(onedayItemMng.month === 1) { // [이전 달] 1월일 경우 전 년도 12월로 바꿔준다.
                    onedayItemMng.getCalendarData(onedayItemMng.year - 1, 12);
                } else {
                    onedayItemMng.getCalendarData(onedayItemMng.year, onedayItemMng.month - 1);
                }
                break;
            case "nextMonth" :  // [다음 달] 버튼
                if(onedayItemMng.month === 12) { // [다름 달] 12월일 경우 다음 년도 1월로 바꿔준다.
                    onedayItemMng.getCalendarData(onedayItemMng.year + 1, 1);
                } else {
                    onedayItemMng.getCalendarData(onedayItemMng.year, onedayItemMng.month + 1);
                }
                break;
            case "prevYear"  :  // [이전 년도] 버튼
                if(onedayItemMng.year === 2000){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return;
                }
                onedayItemMng.getCalendarData(onedayItemMng.year - 1, onedayItemMng.month);
                break;
            case "nextYear"  :  // [다음 년도] 버튼
                onedayItemMng.getCalendarData(onedayItemMng.year + 1, onedayItemMng.month);
                break;
        }
    },
    /**
     * 캘린더 초기화
     */
    resetCalendar: function() {
        $("#onedayItemCalendar > tbody").empty();
    },
    /**
     * 화면 출력용 캘린더 데이터 조회
     */
    getCalendarData: function (year, month) {
        // 전달 파라미터가 비어있으면 오늘 날짜 기준으로 조회
        if($.jUtil.isEmpty(year) && $.jUtil.isEmpty(month)) {
            year = onedayItemMng.year;
            month = onedayItemMng.month;
        }

        CommonAjax.basic({
            url         : "/escrow/businessDay/getCalendarDate.json?schYear=" + year + "&schMonth="+ month,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                if (res.length == 0) {
                    alert("정상적으로 조회되지 않았습니다.\n잠시 후에 다시 시도해 주시길 바랍니다.");
                    return;
                }
                // 캘린더 초기화
                onedayItemMng.resetCalendar();
                // 전역 date 변수 setting
                onedayItemMng.year = res.year;
                onedayItemMng.month = res.month;
                onedayItemMng.today = res.today;
                onedayItemMng.firstDayOfWeek = res.firstDayOfWeek;
                // 캘린더 상단 년도,월 출력
                $("#curYear").html(res.year);
                $("#curMonth").html(res.month);
                // 캘린더 draw
                onedayItemMng.drawCalendar(res.dayList);

                // 기존 선택된 영업일 정보 음영처리
                if(!$.jUtil.isEmpty(onedayItemMng.selectTdId)) {
                    $("#" + onedayItemMng.selectTdId).css('background-color', '#828282');
                }

                // 일자별 상품 수 그리기.
                // onedayItemMng.getOnedayItemCntList();
            }
        })
    },
    /**
     * 캘린더 출력을 위한 태그 create
     */
    drawCalendar: function(data) {
        var tags = '';
        var weekend = 1;
        var dateCnt = 1;

        // '1일' 그리기 전, 전 달 여백 채울 태그 create
        var firstBlankTags = '';
        var firstBlankDateCnt = 0;
        for (firstBlankDateCnt; firstBlankDateCnt<onedayItemMng.firstDayOfWeek-1; firstBlankDateCnt++) {
            firstBlankTags += "<td></td>";
        }

        for (var i in data) {
            // N주차 데이터 그리기... start
            if (dateCnt % 7 === 1) {
                tags += "<tr name=" + weekend + "week>";
                // 1주차 일 때, 전 달 여백 채울 태그 draw
                if (weekend === 1) {
                    dateCnt = onedayItemMng.firstDayOfWeek;
                    tags += firstBlankTags;
                }
            }
            // 일자 draw
            tags += "<td id=" + data[i].date + " onclick='onedayItemMng.displayonedayItemDay($(this));'>";
            // 평일은 검정색, 주말은 빨간색으로 draw
            if(data[i].dayOfWeek === 6 || data[i].dayOfWeek === 7) {
                tags += "<div class='dateArea text-red'>";
            } else {
                tags += "<div class='dateArea'>";
            }
            tags += data[i].day;

            // N주차 데이터 그리기... 종료
            if (dateCnt % 7 === 0) {
                tags += "</tr>";
                weekend++;
            }
            dateCnt++;
        }
        // 마지막 일자 그린 후, 캘린더 여백 채울 태그 create
        var lastMarginCnt = (weekend*7) - (firstBlankDateCnt+data.length);
        for (var lastBlankDateCnt=0; lastBlankDateCnt<lastMarginCnt; lastBlankDateCnt++) {
            tags += "<td></td>";
        }

        // 최종 draw
        $("#onedayItemCalendar > tbody").append(tags);
    },
    /**
     * 캘린더에서 클릭 시, 상세 정보 하단에 노출
     * @param selectDay
     */
    displayonedayItemDay: function ($selectDate) {
        // 하단 영업일 정보 영역 초기화
        $("#" + onedayItemMng.selectTdId).removeAttr("style");

        // 선택된 td의 id 저장
        onedayItemMng.selectTdId = $selectDate.attr("id");
        $selectDate.css('background-color', '#828282');

        // 선택한 날짜 선택
        onedayItemMng.dispDt = $selectDate.attr("id");

        // 싸데이 아이템 리스트 초기화
        onedayItemList = new Array();

        // 싸데이 아이템 리스트 채우기.
        onedayItemMng.search();
    },
    /**
     * 일자별 상품수 조회
     */
    getOnedayItemCntList: function() {
        var dispDate = onedayItemMng.year + '-' + onedayItemMng.month;
        var siteType = $('input[name="siteType"]:checked').val();

        if(onedayItemMng.month < 10) {
            dispDate = onedayItemMng.year + '-' + '0' + onedayItemMng.month;
        }

        CommonAjax.basic({
            url         : "/item/onedayItem/getOnedayItemCnt.json?dispDate=" + dispDate,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                if (res.length == 0) {
                    return;
                }
                onedayItemMng.drawOnedayItemCntList(res);
            }
        });
    },
    /**
     * 일자별 상품수 리스트 draw
     */
    drawOnedayItemCntList: function(data) {
        for(var i in data) {
            onedayItemMng.drawOnedayItemCnt(data[i]);
        }
    },
    /**
     * 영업일 정보 draw
     */
    drawOnedayItemCnt: function(data) {
        var dataDispDt = data.dispDt;
        if ($.jUtil.isEmpty(dataDispDt)) {
            return;
        }

        $('<div>', {
            text: data.itemCnt,
            css: {
                backgroundColor: '#f54040',
                padding: '3px 5px',
                marginBottom: '3px',
                color: '#fff'
            }
        }).appendTo($("#"+dataDispDt));
    },
    search: function () {
        CommonAjax.basic({
            url: '/item/onedayItem/getOnedayItem.json',
            data: { dispDt : onedayItemMng.dispDt },
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                onedayItemListGrid.setData(res);
                $('#onedayItemTotalCount').html($.jUtil.comma(onedayItemListGrid.gridView.getItemCount()));

                onedayItemMng.onedayItemList = new Array();
                res.forEach(function (data, index) {
                    onedayItemMng.onedayItemList[index] = {
                        dispDt : data.dispDt
                        , itemNo : data.itemNo
                        , itemNm : data.itemNm
                        , dispYn : data.dispYn
                        , dispYnTxt : data.dispYnTxt
                        , priority : data.priority
                        , siteType : data.siteType
                    }
                });
            }
        });
    },
    resetForm: function () {
        $('#storeType').val('TD').trigger('change');
        // $('#itemStoreType').val('HYPER');

        onedayItemMng.getCalendarData();

        onedayItemMng.isUpdate = false;
        onedayItemListGrid.dataProvider.clearRows();
        onedayItemMng.onedayItemList = new Array();
        onedayItemMng.dispDt = null;
        $('#onedayItemTotalCount').html(0);
    },
    getGrid: function (selectRowId) {
        var rowDataJson = onedayItemListGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').val(rowDataJson.itemNo);
        $('#itemNm').val(rowDataJson.itemNm);
        $('#dispYn').val(rowDataJson.dispYn);

        $('#storeType').val(rowDataJson.mallType).trigger('change');
        $('select[name="storeType"]').attr('readonly', true);
        $('select[name="itemStoreType"]').val(rowDataJson.itemStoreType);
        $('select[name="itemStoreType"]').attr('readonly', true);

        onedayItemMng.isUpdate = true;
    },
    setOnedayItem : function() {
        if(!onedayItemMng.setValid()) {
            return false;
        }

        if(onedayItemMng.onProgress) {
            alert('요청 처리중 입니다. 잠시만 기다려 주세요.');
            return;
        }

        onedayItemMng.onProgress = true;

        CommonAjax.basic({
            url : '/item/onedayItem/setOnedayItem.json',
            data : JSON.stringify(onedayItemList),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                onedayItemMng.onProgress = false;
                onedayItemMng.search();
            }
        });
    },
    /**
     * onedayItemListGrid 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = onedayItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(onedayItemListGrid, $(obj).attr("key"), checkedRows);

        // 우선순위 변동에 따른 정렬
        onedayItemMng.onedayItemList = new Array();
        onedayItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            // var siteType = $('input[name="siteType"]:checked').val();  -- [CLUB] Spec out 처리되어 주석처리.
            var itemNo = onedayItemListGrid.gridView.getValue(idx, 0);
            var dispYn = onedayItemListGrid.gridView.getValue(idx, 6);
            var itemStoreType = onedayItemListGrid.gridView.getValue(idx, 8);

            onedayItemList[idx] = {
                dispDt : onedayItemMng.dispDt
                , dispYn : dispYn
                , itemNo : itemNo
                , siteType : 'HOME'
                , itemStoreType : itemStoreType
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 상품조회팝업 ( 점포 )
     */
    addItemPopUp : function() {
        if($.jUtil.isEmpty(onedayItemMng.dispDt)) {
            alert('등록일자가 존재하지 않습니다.');
            return;
        }
        switch ($('#storeType').val()) {
            case 'TD' :
                $.jUtil.openNewPopup('/common/popup/itemPop?callback=onedayItemMng.setItemInfo&isMulti=Y&mallType=TD&storeType=' + $("#itemStoreType option:selected").val(), 1084, 650);
                break;
            case 'DS' :
                $.jUtil.openNewPopup('/common/popup/itemPop?callback=onedayItemMng.setItemInfo&isMulti=Y&mallType=DS&storeType=DS', 1084, 650);
                break;
        }
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {

        var addObject = new Array();
        var mallType = 'TD'
        var mallTypeNm = '점포상품';
        var selectedItemStoreType = $("#itemStoreType option:selected").val();
        if($.jUtil.isEmpty(selectedItemStoreType)) {
            mallType = 'DS';
            mallTypeNm = '셀러상품'
        }

        resData.forEach(function (data, index) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = onedayItemListGrid.dataProvider.searchDataRow(options);

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , mallTypeNm : mallTypeNm
                , itemNm : data.itemNm
                , dispYnTxt: '전시'
                , regNm : ''
                , regDt : ''
                , dispYn : 'Y'
                , siteType : ''
                , itemStoreType : selectedItemStoreType
                , mallType : mallType
                , priority : index
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        onedayItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        onedayItemMng.onedayItemList = new Array();
        onedayItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            // var siteType = $('input[name="siteType"]:checked').val();  -- [CLUB] Spec out 처리되어 주석처리.
            var itemNo = onedayItemListGrid.gridView.getValue(idx, 0);
            var dispYn = onedayItemListGrid.gridView.getValue(idx, 6);
            var itemStoreType = onedayItemListGrid.gridView.getValue(idx, 8);

            onedayItemList[idx] = {
                dispDt : onedayItemMng.dispDt
                , dispYn : dispYn
                , itemNo : itemNo
                , siteType : 'HOME'
                , itemStoreType : itemStoreType
                , priority : parseInt(idx) + 1
            };
        });
    },
    setValid: function () {
        if($.jUtil.isEmpty(onedayItemMng.dispDt)) {
            alert("전시날짜를 선택해주세요.");
            return false;
        }

        return true;
    }
};