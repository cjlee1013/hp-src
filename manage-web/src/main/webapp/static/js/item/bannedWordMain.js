/**
 * 기준정보관리 > 금칙어 관리
 */
$(document).ready(function() {
    bannedWordListGrid.init();
    bannedWord.init();
    CommonAjaxBlockUI.global();
});

// 금칙어관리 그리드
var bannedWordListGrid = {
    gridView : new RealGridJS.GridView("bannedWordListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        bannedWordListGrid.initGrid();
        bannedWordListGrid.initDataProvider();
        bannedWordListGrid.event();
    },
    initGrid : function() {
        bannedWordListGrid.gridView.setDataSource(bannedWordListGrid.dataProvider);

        bannedWordListGrid.gridView.setStyles(bannedWordListGridBaseInfo.realgrid.styles);
        bannedWordListGrid.gridView.setDisplayOptions(bannedWordListGridBaseInfo.realgrid.displayOptions);
        bannedWordListGrid.gridView.setColumns(bannedWordListGridBaseInfo.realgrid.columns);
        bannedWordListGrid.gridView.setOptions(bannedWordListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        bannedWordListGrid.dataProvider.setFields(bannedWordListGridBaseInfo.dataProvider.fields);
        bannedWordListGrid.dataProvider.setOptions(bannedWordListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        bannedWordListGrid.gridView.onDataCellClicked = function(gridView, index) {
            bannedWord.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        bannedWordListGrid.dataProvider.clearRows();
        bannedWordListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(bannedWordListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "금칙어관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        bannedWordListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};


// 금칙어 관리
var bannedWord = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
    },
    /**
     * 금칙어 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#bannedWordSetForm').ajaxForm({
            url: '/item/bannedWord/setBannedWord.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                bannedWord.setBannedWordInfoData($('#bannedWordSetForm').serializeObject());
                bannedWord.resetForm();
            },
            error: function(e) {
                if(e.responseJSON.data == null) {
                    alert(e.responseJSON.returnMessage);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * bannedWord 그리드 정보 갱신
     */
    setBannedWordInfoData : function(bannedWordInfo) {
        var options = {
            fields: ['bannedSeq'],
            values: [bannedWordInfo.bannedSeq]
        }
        var itemIndex = bannedWordListGrid.gridView.searchItem(options);

        // 신규 등록 시 오류를 방지하기 위함. ( 그리드에 찾는 데이터가 없을 경우 -1 )
        if(itemIndex != -1) {
            var bannedTypeTxt = '상품명금칙어';
            if(bannedWordInfo.bannedType === 'SRV') {
                bannedTypeTxt = '서비스금칙어';
            }

            var useYnTxt = '사용안함';
            if(bannedWordInfo.useYn === 'Y') {
                useYnTxt = '사용';
            }

            // RealGrid 값 변경.
            var setGridValue = {
                bannedTypeTxt : bannedTypeTxt
                , bannedType : bannedWordInfo.bannedType
                , keyword : bannedWordInfo.keyword
                , keywordDesc : bannedWordInfo.keywordDesc
                , useYn : bannedWordInfo.useYn
                , useYnTxt : useYnTxt
            };
            bannedWordListGrid.gridView.setValues(itemIndex, setGridValue, true);
        }
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(bannedWord.search);
        $('#searchResetBtn').bindClick(bannedWord.searchFormReset);
        $('#excelDownloadBtn').bindClick(bannedWord.excelDownload);

        $('#resetBtn').bindClick(bannedWord.resetForm);
        $('#setBannedWordBtn').bindClick(bannedWord.setBannedWord);
        $('#editBannedWordBtn').bindClick(bannedWord.setBannedWord, true);

        $('#keyword').calcTextLength('keyup', '#keywordCount');
        $('#keywordDesc').calcTextLength('keyup', '#keywordDescCount');

        $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        bannedWordListGrid.excelDownload();
    },
    /**
     * 금칙어 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }

        CommonAjax.basic({url:'/item/bannedWord/getBannedWordList.json?' + $('#bannedWordSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            bannedWord.resetForm();
            bannedWordListGrid.setData(res);
            $('#bannedWordTotalCount').html($.jUtil.comma(bannedWordListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchType').val('keyword');
        $('#searchKeyword, #searchBannedType, #searchUseYn').val('');
    },
    /**
     * 금칙어 등록/수정
     * @param isEdit 수정모드 여부
     */
    setBannedWord : function(isEdit) {
        let bannedSeq = $('#bannedSeq').val();
        if(isEdit == true) {
            if(bannedSeq == "") {
                alert("신규 금칙어는 수정할 수 없습니다. 등록 버튼으로 저장해주세요.");
                return false;
            }
        } else {
            if(bannedSeq != "") {
                alert("기존 금칙어는 등록할 수 없습니다. 수정 버튼으로 수정해주세요.");
                return false;
            }
        }

        if($.jUtil.isEmpty($('#bannedType').val())) {
            alert("유형은 필수항목 입니다.");
            $('#bannedType').focus();
            return false;
        }

        if($.jUtil.isEmpty($('#keyword').val())) {
            alert("등록/수정 영역의 필수항목은 빠짐없이 입력해주세요.");
            $('#keyword').focus();
            return false;
        }

        $('#bannedWordSetForm').submit();
    },
    /**
     * 금칙어 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#bannedSeq').val('');
        // 유형 > 금칙어 키워드 > 설명 > 적용 카테고리 > 사용여부
        $('#bannedType').val('');
        $('#keyword').val('');
        $('#keywordDesc').val('');
        $('#keywordCount, #keywordDescCount').html('0');
        $('#useYn').val('Y');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = bannedWordListGrid.dataProvider.getJsonRow(selectRowId);

        $('#bannedSeq').val(rowDataJson.bannedSeq);
        $('#bannedType').val(rowDataJson.bannedType);
        $('#keyword').val(rowDataJson.keyword).setTextLength('#keywordCount');
        $('#keywordDesc').val(rowDataJson.keywordDesc).setTextLength('#keywordDescCount');
        $('#useYn').val(rowDataJson.useYn);

    }
};