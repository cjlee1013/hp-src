/**
 * 사이트관리 > 전문관관리 > 마트전단 전시 관리
 */
$(document).ready(function() {
    auroraThemeListGrid.init();
    auroraThemeItemListGrid.init();

    auroraTheme.init();
    CommonAjaxBlockUI.global();

    auroraThemeItem.init();
});

// dspLeaflet 그리드
var auroraThemeListGrid = {
    gridView : new RealGridJS.GridView("auroraThemeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        auroraThemeListGrid.initGrid();
        auroraThemeListGrid.initDataProvider();
        auroraThemeListGrid.event();
    },
    initGrid : function() {
        auroraThemeListGrid.gridView.setDataSource(auroraThemeListGrid.dataProvider);
        auroraThemeListGrid.gridView.setStyles(auroraThemeGridBaseInfo.realgrid.styles);
        auroraThemeListGrid.gridView.setDisplayOptions(auroraThemeGridBaseInfo.realgrid.displayOptions);
        auroraThemeListGrid.gridView.setColumns(auroraThemeGridBaseInfo.realgrid.columns);
        auroraThemeListGrid.gridView.setOptions(auroraThemeGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        auroraThemeListGrid.dataProvider.setFields(auroraThemeGridBaseInfo.dataProvider.fields);
        auroraThemeListGrid.dataProvider.setOptions(auroraThemeGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        auroraThemeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            auroraTheme.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        auroraThemeListGrid.dataProvider.clearRows();
        auroraThemeListGrid.dataProvider.setRows(dataList);
    }
};

// auroraThemeItemListGrid 그리드
var auroraThemeItemListGrid = {
    gridView : new RealGridJS.GridView("auroraThemeItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        auroraThemeItemListGrid.initGrid();
        auroraThemeItemListGrid.initDataProvider();
        auroraThemeItemListGrid.event();
    },
    initGrid : function() {
        auroraThemeItemListGrid.gridView.setDataSource(auroraThemeItemListGrid.dataProvider);
        auroraThemeItemListGrid.gridView.setStyles(auroraThemeItemGridBaseInfo.realgrid.styles);
        auroraThemeItemListGrid.gridView.setDisplayOptions(auroraThemeItemGridBaseInfo.realgrid.displayOptions);
        auroraThemeItemListGrid.gridView.setColumns(auroraThemeItemGridBaseInfo.realgrid.columns);
        auroraThemeItemListGrid.gridView.setOptions(auroraThemeItemGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        auroraThemeItemListGrid.dataProvider.setFields(auroraThemeItemGridBaseInfo.dataProvider.fields);
        auroraThemeItemListGrid.dataProvider.setOptions(auroraThemeItemGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        auroraThemeItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            auroraThemeItem.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        auroraThemeItemListGrid.dataProvider.clearRows();
        auroraThemeItemListGrid.dataProvider.setRows(dataList);
    }
};

// auroraTheme 관리
var auroraTheme = {
    auroraThemeList : {},
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.setOnData();
        auroraTheme.auroraThemeList = new Array();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setAuroraTheme').bindClick(auroraTheme.setAuroraTheme);
        $('#resetAuroraTheme').bindClick(auroraTheme.resetForm);
        $('#setAuroraThemeItems').bindClick(auroraThemeItem.setAuroraThemeItem);

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            auroraTheme.img.addImg();
        });

        // 카테고리 이미지 (PC) 삭제
        $('#pcDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                auroraTheme.img.deleteImg('pc');
            }
        });

        // 카테고리 이미지 (MOBILE) 삭제
        $('#mobileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                auroraTheme.img.deleteImg('mobile');
            }
        });
    },
    /**
     * auroraTheme Init 데이터.
     */
    setOnData : function() {
        CommonAjax.basic({
            url : '/item/auroraThemeItem/getAuroraTheme.json'
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                auroraThemeListGrid.setData(res);
                auroraThemeItem.resetForm();
                auroraThemeItemListGrid.dataProvider.clearRows();
                auroraThemeItem.itemList = new Array();
                res.forEach(function (data, index) {
                    auroraTheme.auroraThemeList[index] = {
                        getDb : false
                        , themeId : data.themeId
                        , themeNm : data.themeNm
                        , themeSubNm : data.themeSubNm
                        , useYn : data.useYn
                        , priority : parseInt(index) + 1
                    }
                });
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = auroraThemeListGrid.dataProvider.getJsonRow(selectRowId);

        $('#themeId').val(rowDataJson.themeId);
        $('#themeNm').val(rowDataJson.themeNm);
        $('#themeSubNm').val(rowDataJson.themeSubNm);
        $('#setAuroraThemeUseYn').val(rowDataJson.useYn);

        if(!auroraTheme.auroraThemeList[selectRowId].getDb === true) {
            auroraTheme.auroraThemeList[selectRowId].getDb = true;
            auroraThemeItem.search(rowDataJson.themeId);
        } else {
            auroraThemeItemListGrid.setData(auroraThemeItem.itemList[selectRowId].itemList);
        }

        auroraTheme.img.setImg('pc', {
            'imgUrl': rowDataJson.pcImgUrl,
            'src'   : hmpImgUrl + "/provide/view?processKey=AuroraThemeBannerPc&fileId=" + rowDataJson.pcImgUrl,
            'changeYn' : 'Y'
        });

        auroraTheme.img.setImg('mobile', {
            'imgUrl': rowDataJson.mobileImgUrl,
            'src'   : hmpImgUrl + "/provide/view?processKey=AuroraThemeBannerMobile&fileId=" + rowDataJson.mobileImgUrl,
            'changeYn' : 'Y'
        });
    },
    /**
     * 그리드 row 값 셋팅
     */
    setAuroraTheme : function() {

        if($.jUtil.isEmpty($('#themeNm').val())) {
            alert("메인 테마명을 입력해 주세요.");
            $('#themeNm').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#themeSubNm').val())) {
            alert("서브 테마명을 입력해 주세요.");
            $('#themeSubNm').focus();
            return false;
        }

        if($.jUtil.isEmpty($('#pcImgUrl').val())) {
            alert('카테고리 이미지를 등록해 주세요. ( PC )');
            return false;
        }
        if($.jUtil.isEmpty($('#mobileImgUrl').val())) {
            alert('카테고리 이미지를 등록해 주세요. ( MOBILE )');
            return false;
        }

        var priority = auroraThemeListGrid.gridView.getValue(auroraThemeListGrid.gridView.getCurrent().itemIndex, 8);
        if($.jUtil.isEmpty($('#themeId').val())) {
            priority = 1;
        }
        var pushData = {
            themeId : $('#themeId').val()
            , themeNm : $('#themeNm').val()
            , themeSubNm : $('#themeSubNm').val()
            , pcImgUrl : $('#pcImgUrl').val()
            , mobileImgUrl : $('#mobileImgUrl').val()
            , useYn : $('#setAuroraThemeUseYn').val()
            , priority : priority
        };

        CommonAjax.basic({
            url : '/item/auroraThemeItem/setAuroraTheme.json',
            data : JSON.stringify(pushData),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                auroraTheme.resetForm();
                auroraTheme.setOnData();
            }
        });

    },
    /**
     * auroraThemeItemCategory 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#themeId, #themeNm, #themeSubNm').val('');
        $('#setAuroraThemeUseYn').val('Y');
        auroraTheme.img.init();
    },
    /**
     * auroraThemeListGrid 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = auroraThemeListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서변경할 테마를 선택해 주세요.");
            return false;
        }

        realGridMoveRow(auroraThemeListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬
        auroraTheme.auroraThemeList.forEach(function (data, index) {
            var themeId = auroraThemeListGrid.gridView.getValue(index, 0);
            var themeNm = auroraThemeListGrid.gridView.getValue(index, 1);
            var themeSubNm = auroraThemeListGrid.gridView.getValue(index, 2);
            var useYn = auroraThemeListGrid.gridView.getValue(index, 5);

            auroraTheme.auroraThemeList[index] = {
                themeId : themeId
                , themeNm : themeNm
                , themeSubNm : themeSubNm
                , useYn : useYn
                , priority : parseInt(index) + 1
            }
        });
    }
};

// auroraThemeItem 관리
var auroraThemeItem = {
    itemList : {},
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        auroraThemeItem.itemList = new Array();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setAuroraThemeItem').bindClick(auroraThemeItem.setGridRowSelect); //auroraThemeItem 검색
        $('#resetAuroraThemeItem').bindClick(auroraThemeItem.resetForm); //auroraThemeItem 검색폼 초기화
        $('#addItemPopUp').bindClick(auroraThemeItem.addItemPopUp); // 상품조회 팝업 ( 단일등록 )
        $('#addItemPopUpByExcel').bindClick(auroraThemeItem.addItemPopUpByExcel); // 상품조회 팝업 ( 일괄등록 )
    },
    /**
     * auroraThemeItem 검색 ( themeId )
     */
    search : function(themeId) {
        CommonAjax.basic({
            url : '/item/auroraThemeItem/getAuroraThemeItem.json?themeId=' + themeId
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                auroraThemeItemListGrid.setData(res);

                var selectRowId = auroraThemeListGrid.gridView.getCurrent().itemIndex;
                var arrList = new Array();
                res.forEach(function (data, index) {
                    var itemList;
                    itemList = {
                        themeId : data.themeId
                        , itemNo : data.itemNo
                        , itemNm : data.itemNm
                        , useYn : data.useYn
                        , useYnNm : data.useYnNm
                        , itemTypeNm : data.itemTypeNm
                        , priority : parseInt(index) + 1
                        , regNm : data.regNm
                    }
                    arrList.push(itemList);
                });
                auroraThemeItem.itemList[selectRowId] = {
                    itemList : arrList
                };
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = auroraThemeItemListGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').val(rowDataJson.itemNo);
        $('#setAuroraThemeItemUseYn').val(rowDataJson.useYn);
    },
    /**
     * 그리드 row 선택
     */
    setGridRowSelect : function() {
        if($.jUtil.isEmpty($('#itemNo').val())) {
            alert("변경 하실 상품을 먼저 선택해 주세요.");
            return false;
        }

        let setuseYnNm = '사용';
        var curRow = auroraThemeItemListGrid.gridView.getCurrent().itemIndex;
        var setUseYn = $('#setAuroraThemeItemUseYn').val();

        if(setUseYn === 'N') {
            setuseYnNm = '사용안함';
        }

        // RealGrid 값 변경.
        var values = { useYn : setUseYn, useYnNm : setuseYnNm };
        auroraThemeItemListGrid.gridView.setValues(curRow, values, true);

        // Back Data 변경.
        var curCategoryRow = auroraThemeListGrid.gridView.getCurrent().itemIndex;
        auroraThemeItem.itemList[curCategoryRow].itemList[curRow] = {
            itemNo : auroraThemeItem.itemList[curCategoryRow].itemList[curRow].itemNo
            , itemNm : auroraThemeItem.itemList[curCategoryRow].itemList[curRow].itemNm
            , itemTypeNm : auroraThemeItem.itemList[curCategoryRow].itemList[curRow].itemTypeNm
            , useYn : setUseYn
            , useYnNm : setuseYnNm
            , priority : auroraThemeItem.itemList[curCategoryRow].itemList[curRow].priority
            , themeId : auroraThemeItem.itemList[curCategoryRow].itemList[curRow].themeId
        }
    },
    /**
     * auroraThemeItem 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#itemNo').val('');
        $('#setAuroraThemeItemUseYn').val('Y');
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = auroraThemeItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(auroraThemeItemListGrid, $(obj).attr("key"), checkedRows);

        var curCategoryRow = auroraThemeListGrid.gridView.getCurrent().itemIndex;

        // Priority 정렬
        auroraThemeItem.itemList[curCategoryRow].itemList.forEach(function (data, index) {
            var itemNo = auroraThemeItemListGrid.gridView.getValue(index, 0);
            var itemNm = auroraThemeItemListGrid.gridView.getValue(index, 1);
            var itemTypeNm = auroraThemeItemListGrid.gridView.getValue(index, 2);
            var useYnNm = auroraThemeItemListGrid.gridView.getValue(index, 3);
            var regNm = auroraThemeItemListGrid.gridView.getValue(index, 4);
            var themeId = auroraThemeItemListGrid.gridView.getValue(index, 5);
            var useYn = auroraThemeItemListGrid.gridView.getValue(index, 6);

            auroraThemeItem.itemList[curCategoryRow].itemList[index] = {
                itemNo : itemNo
                , itemNm : itemNm
                , itemTypeNm : itemTypeNm
                , useYn : useYn
                , useYnNm : useYnNm
                , priority : parseInt(index) + 1
                , themeId : themeId
                , regNm : regNm
            }
        });
    },
    /**
     * 상품조회팝업 ( 단일등록 )
     */
    addItemPopUp : function() {
        if(!auroraTheme.auroraThemeList[auroraThemeListGrid.gridView.getCurrent().itemIndex].getDb === true) {
            alert("추가하실 테마를 먼저 선택해 주세요.");
            return false;
        }
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=auroraThemeItem.setItemInfo&isMulti=Y&mallType=TD&storeType=AURORA', 1084, 650);
    },
    /**
     * 상품조회팝업 ( 일괄등록 )
     */
    addItemPopUpByExcel : function() {
        var target = "auroraThemePop";
        var callback = "auroraThemeItem.itemExcelPopCallback";
        var url = "/item/auroraThemeItem/uploadAuroraMainThemeItemPop?callback=" + callback;

        if(!auroraTheme.auroraThemeList[auroraThemeListGrid.gridView.getCurrent().itemIndex].getDb === true) {
            alert("추가하실 테마를 먼저 선택해 주세요.");
            return false;
        }

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function () {
        auroraThemeItem.search($('#themeId').val());
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        var selectRowId = auroraThemeListGrid.gridView.getCurrent();
        var arrList = auroraThemeItem.itemList[selectRowId.dataRow].itemList;
        var lastIdx = arrList.length;

        // 상품이 없어 최초 등록일 경우
        if(lastIdx === 0) {
            // 현재 셀렉트 된 themeId 가져온다.
            var themeId = auroraThemeListGrid.gridView.getValue(auroraThemeListGrid.gridView.getCurrent().itemIndex, 0);

            arrList = new Array();
            resData.forEach(function (data, index) {
                var itemList;
                var options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                var dataRow = auroraThemeItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    themeId : themeId
                    , itemNo : data.itemNo
                    , itemNm : data.itemNm
                    , useYn : "Y"
                    , useYnNm : "사용중"
                    , itemTypeNm : "저장 후 확인 가능"
                    , regNm : "저장 후 확인 가능"
                    , priority : parseInt(index)
                }
                if(dataRow === -1) {
                    auroraThemeItem.setItemGrid(data);
                    arrList.push(itemList);
                }
            });
        } else {
            resData.forEach(function (data, index) {
                let itemList;
                let options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                let dataRow = auroraThemeItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    themeId : arrList[0].themeId
                    , itemNo : data.itemNo
                    , itemNm : data.itemNm
                    , useYn : "Y"
                    , useYnNm : "사용중"
                    , itemTypeNm : "저장 후 확인 가능"
                    , regNm : "저장 후 확인 가능"
                    , priority : parseInt(lastIdx + index)
                }

                if(dataRow === -1) {
                    auroraThemeItem.setItemGrid(data);
                    arrList.push(itemList);
                }
            });
        }

        auroraThemeItem.itemList[selectRowId.dataRow] = {
            itemList : arrList
        };
    },
    /**
     * 상품을 Grid 에 추가해준다.
     * Item 상태는 공통 팝업에서 제공해 주지 않으므로
     * 저장 후 다시 조회 시에 노출 가능하나... 필요시 공통 팝업에서 제공 요청.
     *
     * 현재 버전은 아래와 같이 저장 후 확인 가능으로 노출해준다.
     */
    setItemGrid : function(data) {
        var itemData = {
            itemNo : data.itemNo
            , itemNm : data.itemNm
            , itemTypeNm : "저장 후 확인 가능"
            , useYn : "Y"
            , useYnNm : "사용"
            , regNm : "저장 후 확인 가능"
        }
        auroraThemeItemListGrid.dataProvider.addRow(itemData);
    },
    setAuroraThemeItem : function() {

        auroraThemeItem.sortPriority();

        var auroraThemeItemsSetForm = $('#dspLeafletSetForm').serializeObject(true);
        auroraThemeItemsSetForm.auroraThemeList = auroraTheme.auroraThemeList;
        auroraThemeItemsSetForm.itemList = auroraThemeItem.itemList;

        CommonAjax.basic({
            url:'/item/auroraThemeItem/setAuroraThemeItem.json',
            data : JSON.stringify(auroraThemeItemsSetForm)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                auroraTheme.resetForm();
                auroraThemeItem.resetForm();
                auroraTheme.setOnData();
                auroraThemeItemListGrid.dataProvider.clearRows();
            }
        });
    },
    sortPriority : function () {

        // Priority 정렬 ( 테마 )
        auroraTheme.auroraThemeList.forEach(function (data, index) {
            var themeId = auroraThemeListGrid.gridView.getValue(index, 0);
            var themeNm = auroraThemeListGrid.gridView.getValue(index, 1);
            var themeSubNm = auroraThemeListGrid.gridView.getValue(index, 2);
            var pcImgUrl = auroraThemeListGrid.gridView.getValue(index, 5);
            var mobileImgUrl = auroraThemeListGrid.gridView.getValue(index, 6);
            var useYn = auroraThemeListGrid.gridView.getValue(index, 7);

            auroraTheme.auroraThemeList[index] = {
                themeId : themeId
                , themeNm : themeNm
                , themeSubNm : themeSubNm
                , pcImgUrl : pcImgUrl
                , mobileImgUrl : mobileImgUrl
                , useYn : useYn
                , priority : parseInt(index) + 1
            }
        });

        // Priority 정렬 ( 테마상품 )
        auroraTheme.auroraThemeList.forEach(function (data, index) {
            if(undefined != auroraThemeItem.itemList[index]) {
                auroraThemeItem.itemList[index].itemList.forEach(
                    function (item, idx) {
                        auroraThemeItem.itemList[index].itemList[idx] = {
                            itemNo: item.itemNo
                            , itemNm: item.itemNm
                            , itemTypeNm: item.itemTypeNm
                            , useYn: item.useYn
                            , useYnNm: item.useYnNm
                            , priority: parseInt(idx) + 1
                            , themeId: data.themeId
                            , regNm: item.regNm
                        }
                    });
            }
        });
    }
};

/**
 * 셀러샵관리 이미지
 */
auroraTheme.img = {
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        auroraTheme.img.setImg('pc', null);
        auroraTheme.img.setImg('mobile', null);
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'pc':
                if (params) {
                    $('#pcImgUrlTag').prop('src', params.src);
                    $('#pcImgUrl').val(params.imgUrl);
                    $('#pcImgWidth').val(params.width);
                    $('#pcImgHeight').val(params.height);
                    $('#pcImgChangeYn').val(params.changeYn);

                    $('.pcDisplayView').show();
                    $('.pcDisplayView').siblings('.pcDisplayReg').hide();
                } else {
                    $('#pcImgUrlTag').prop('src', '');
                    $('#pcImgUrl').val('');
                    $('#pcImgWidth').val('');
                    $('#pcImgHeight').val('');
                    $('#pcImgChangeYn').val('N');

                    $('.pcDisplayView').hide();
                    $('.pcDisplayView').siblings('.pcDisplayReg').show();
                }
                break;
            case 'mobile':
                if (params) {
                    $('#mobileImgUrlTag').prop('src', params.src);
                    $('#mobileImgUrl').val(params.imgUrl);
                    $('#mobileImgWidth').val(params.width);
                    $('#mobileImgHeight').val(params.height);
                    $('#mobileImgChangeYn').val(params.changeYn);

                    $('.mobileDisplayView').show();
                    $('.mobileDisplayView').siblings('.mobileDisplayReg').hide();
                } else {
                    $('#mobileImgUrlTag').prop('src', '');
                    $('#mobileImgUrl').val('');
                    $('#mobileImgWidth').val('');
                    $('#mobileImgHeight').val('');
                    $('#mobileImgChangeYn').val('N');

                    $('.mobileDisplayView').hide();
                    $('.mobileDisplayView').siblings('.mobileDisplayReg').show();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#imageFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#imageFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#imageFile');
        let ext = $('#imageFile').get(0).files[0].name.split('.');
        let params = {
            processKey : 'AuroraThemeBannerPc',
            mode : 'IMG'
        };

        if(auroraTheme.img.setImgType === 'mobile') {
            params.processKey = 'AuroraThemeBannerMobile';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        auroraTheme.img.setImg(auroraTheme.img.setImgType, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        auroraTheme.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        auroraTheme.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};