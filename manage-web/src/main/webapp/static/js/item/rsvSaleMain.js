/**
 * 배송관리 > 배송관리 > 예약상품관리
 */
$(document).ready(function() {
    main.init();
    rsvSaleListGrid.init();
    itemListGrid.init();
    CommonAjaxBlockUI.global();
});

//예약상품관리 리스트 그리드
var rsvSaleListGrid = {
    selectedRowIdx : 0,
    gridView : new RealGridJS.GridView("rsvSaleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        rsvSaleListGrid.initGrid();
        rsvSaleListGrid.initDataProvider();
        rsvSaleListGrid.event();
    },
    initGrid : function () {
        rsvSaleListGrid.gridView.setDataSource(rsvSaleListGrid.dataProvider);

        rsvSaleListGrid.gridView.setStyles(rsvSaleListGridBaseInfo.realgrid.styles);
        rsvSaleListGrid.gridView.setDisplayOptions(rsvSaleListGridBaseInfo.realgrid.displayOptions);
        rsvSaleListGrid.gridView.setColumns(rsvSaleListGridBaseInfo.realgrid.columns);
        rsvSaleListGrid.gridView.setOptions(rsvSaleListGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(rsvSaleListGrid.gridView, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(rsvSaleListGrid.gridView, "saleType", saleTypeJson);
        setColumnLookupOptionForJson(rsvSaleListGrid.gridView, "useYn", useYnJson);
    },
    initDataProvider : function() {
        rsvSaleListGrid.dataProvider.setFields(rsvSaleListGridBaseInfo.dataProvider.fields);
        rsvSaleListGrid.dataProvider.setOptions(rsvSaleListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        rsvSaleListGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = rsvSaleListGrid.dataProvider.getJsonRow(rsvSaleListGrid.gridView.getCurrent().dataRow);
            rsvSaleListGrid.gridRowSelect(gridRow);
        };
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect :  function (rowDataJson) {
        //var rowDataJson = rsvSaleListGrid.dataProvider.getJsonRow(selectRowId);
        var rsvSaleSeq = rowDataJson.rsvSaleSeq;

        //기본정보
        $("#detailSetForm").resetForm();

        //상품 그리드 클리어
        itemListGrid.dataProvider.clearRows();
        $("#itemTotalCount").html("0");

        //단건 정보
        rsvSaleListGrid.getRsvSaleInfo(rsvSaleSeq);
    },
    /**
     * 예약상품판매관리 단건 조회
     * @param storeId
     */
    getRsvSaleInfo : function(rsvSaleSeq) {
        CommonAjax.basic({
            url:"/item/rsvSale/getRsvSaleInfo.json?rsvSaleSeq="+rsvSaleSeq
            , method : "GET"
            , callbackFunc : function(data) {
                main.detailCallback(data);

                //대상 상품리스트 조회
                rsvSaleListGrid.getRsvSaleItemList(rsvSaleSeq);

                //발송지 리스트 조회
                main.getRsvSendPlaceList(rsvSaleSeq);
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            }
        });
    },
    /**
     * 예약상품판매관리 리스트 조회
     * @param storeId
     */
    getRsvSaleItemList : function(rsvSaleSeq) {
        CommonAjax.basic({
            url:"/item/rsvSale/getRsvSaleItemList.json?rsvSaleSeq="+rsvSaleSeq
            , method : "GET"
            , callbackFunc : function(dataList) {
                itemListGrid.setData(dataList);
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            }
        });
    },
    setData : function(dataList) {
        rsvSaleListGrid.dataProvider.clearRows();
        rsvSaleListGrid.dataProvider.setRows(dataList);

        $("#rsvSaleTotalCount").val(rsvSaleListGrid.gridView.getItemCount())
    },
    excelDownload: function() {
        if(rsvSaleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "예약상품판매관리_" + _date.getFullYear() + month + date;

        rsvSaleListGrid.gridView.exportGrid( {
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

//취급대상 리스트 그리드
var itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
    },
    initGrid : function () {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        var options = itemListGridBaseInfo.realgrid.options;
        options.hideDeletedRows = true;
        itemListGrid.gridView.setOptions(options);
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        var options = itemListGridBaseInfo.dataProvider.options;
        options.softDeleting = true;
        itemListGrid.dataProvider.setOptions(options);

        setColumnLookupOptionForJson(itemListGrid.gridView, "sendPlace", sendPlaceJson);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            //itemListGrid.gridRowSelect(index.dataRow)
        };
    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
        $("#itemTotalCount").html(itemListGrid.gridView.getItemCount());
    },
    /**
     * 그리드 내 조회 초기화
     */
    controlClear : function() {
        //itemListGrid.clearAllFilter();
        $("#itemTotalCount").text(itemListGrid.gridView.getItemCount());
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = itemListGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("항목을 선택해 주세요.");
            return;
        }

        itemListGrid.dataProvider.removeRows(check, false);
    },
    /**
     * 대상상품 그리드에 데이터 추가
     * @param data
     */
    appendData : function(data) {
        var currentDatas = itemListGrid.dataProvider.getJsonRows(0, -1);

        var createdDataList = itemListGrid.dataProvider.getStateRows("created");
        createdDataList = createdDataList.concat(itemListGrid.dataProvider.getStateRows("updated"));
        createdDataList = createdDataList.concat(itemListGrid.dataProvider.getStateRows("none"));

        var createdDatas = new Array();
        createdDataList.forEach(function(row, index) {
            var dataRow = itemListGrid.dataProvider.getJsonRow(row);
            createdDatas.push(dataRow);
        });

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        if (!currentDatas !== undefined && currentDatas.length > 0) {
            itemListGrid.dataProvider.beginUpdate();
            try {
                data.forEach(function(value, index, array1) {
                    if (currentDatas.findIndex(function (currentData) { return currentData.itemNo === value.itemNo;}) == -1) {
                        itemListGrid.dataProvider.addRow(value);
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ['itemNo'],
                            values: [value.itemNo]
                        };

                        var _row = itemListGrid.dataProvider.searchDataRow(options);
                        var rowStatus = itemListGrid.dataProvider.getRowState(_row);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (rowStatus == 'deleted') {
                            var values = [{
                                itemNo: value.itemNo,
                                sendPlace: value.sendPlace
                            }];
                            itemListGrid.dataProvider.updateRows(_row, values);
                            itemListGrid.dataProvider.setRowState(_row, 'updated');
                        } else if (rowStatus == "createAndDeleted") {
                            if (createdDatas.findIndex(function(createdData) { return createdData.itemNo === value.itemNo; }) == -1) {
                                itemListGrid.dataProvider.addRow(value);
                            }
                        }/*else {
                            alert("이미 추가된 상품입니다.");
                        }*/
                    }
                });
            } finally {
                itemListGrid.dataProvider.endUpdate(true);
            }
        } else {
            itemListGrid.dataProvider.addRows(data);
        }
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "적용상품_" + _date.getFullYear() + month + date;

        itemListGrid.gridView.exportGrid( {
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
//메인
var main = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.resetSearchForm();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {
        $('#searchBtn').bindClick(main.search);
        $('#excelDownloadBtn').bindClick(main.excelDownload);
        $('#searchResetBtn').bindClick(main.resetSearchForm);
        $("#addItemBtn").bindClick(main.addItem);
        $("#deleteItemBtn").bindClick(main.removeItemGridData);
        $('#resetBtn').bindClick(main.resetForm);
        //$('#clearBtn').bindClick(main.clearForm);
        $('#setBtn').bindClick(main.setRsvSale);
        $('#itemPopBtn').bindClick(main.getItemPop);
        $("#uploadBtn").bindClick(main.callUploadPopup);

        //점포유형
        $('#storeType').bindChange(main.changeStoreType);
        //판매유형
        $('#saleType').bindChange(main.changeSaleType);
        //점포배송요일설정
        $('#chkSetYn').bindChange(main.changeChkSetYn);
        //판매방법
        $('input:radio[name="saleWay"]').bindChange(main.changeSaleWay);
        //희망발송일 입력
        $("#hopeSendDays").allowInput("keyup", ["NUM"], $(this).attr("id"));

    },
    /**
     * 등록/수정
     */
    setRsvSale : function () {
        if (!main.valid.set()) {
            return;
        }

        if ($('input:radio[name=saleWay]:checked').val() == "NOR") {
            $('#trSendPeriod_haman td, #trSendPeriod_anseong td, #trSendPeriod_store td').find("input:text").each(function () {
                $(this).val('');
            });
        } else {
            $("#hopeSendDays").val("");
        }

        var paramObject = {
            rsvSaleSeq: $("#rsvSaleSeq").val(),
            storeType: $("#storeType").val(),
            saleType: $("#saleType").val(),
            useYn: $("#useYn").val(),
            rsvSaleNm: $("#rsvSaleNm").val(),
            rsvSaleStartDt: $("#rsvSaleStartDt").val(),
            rsvSaleEndDt: $("#rsvSaleEndDt").val(),
            saleWay: $('input:radio[name=saleWay]:checked').val(),
            hopeSendDays: $("#hopeSendDays").val(),

            createList : [],
            deleteList : [],
            noneList : [],
            sendPlaceList : []
        };

        /*var detailSetForm = $("#detailSetForm").serializeArray();
        $.map(detailSetForm, function(value, index) {
            paramObject[value.name] = value.value;
        });*/

        // 적용대상 셋팅
        var createdDataList = itemListGrid.dataProvider.getStateRows("created");
        createdDataList = createdDataList.concat(itemListGrid.dataProvider.getStateRows("updated"));

        var deletedDataList = itemListGrid.dataProvider.getStateRows("deleted");
        var noneDataList = itemListGrid.dataProvider.getStateRows("none");

        createdDataList.forEach(function(_row, index) {
            var _data = itemListGrid.dataProvider.getJsonRow(_row);
            paramObject.createList.push(_data);
        });

        deletedDataList.forEach(function(_row, index) {
            var _data = itemListGrid.dataProvider.getJsonRow(_row);
            paramObject.deleteList.push(_data);
        });

        noneDataList.forEach(function(_row, index) {
            var _data = itemListGrid.dataProvider.getJsonRow(_row);
            paramObject.noneList.push(_data);
        });

        var monYn = $("#monYn");
        var tueYn = $("#tueYn");
        var wedYn = $("#wedYn");
        var thuYn = $("#thuYn");
        var friYn = $("#friYn");
        var satYn = $("#satYn");
        var sunYn = $("#sunYn");

        (monYn.is(":checked") ? monYn.val("Y") : monYn.val("N"));
        (tueYn.is(":checked") ? tueYn.val("Y") : tueYn.val("N"));
        (wedYn.is(":checked") ? wedYn.val("Y") : wedYn.val("N"));
        (thuYn.is(":checked") ? thuYn.val("Y") : thuYn.val("N"));
        (friYn.is(":checked") ? friYn.val("Y") : friYn.val("N"));
        (satYn.is(":checked") ? satYn.val("Y") : satYn.val("N"));
        (sunYn.is(":checked") ? sunYn.val("Y") : sunYn.val("N"));

        var _dataObj = new Array();
        var _data = new Object();
        _data.sendPlace = "STORE";
        _data.sendStartDt = $("#sendStartDt_store").val();
        _data.sendEndDt = $("#sendEndDt_store").val();
        _data.monYn = $("#monYn").val();
        _data.tueYn = $("#tueYn").val();
        _data.wedYn = $("#wedYn").val();
        _data.thuYn = $("#thuYn").val();
        _data.friYn = $("#friYn").val();
        _data.satYn = $("#satYn").val();
        _data.sunYn = $("#sunYn").val();
        paramObject.sendPlaceList.push(_data);

        if ($("#saleType").val() == "DRCT_DLV") {  //명절선물
            _data = new Object();
            _data.sendPlace = "HAMAN";
            _data.sendStartDt = $("#sendStartDt_haman").val();
            _data.sendEndDt = $("#sendEndDt_haman").val();
            paramObject.sendPlaceList.push(_data);

            _data = new Object();
            _data.sendPlace = "ANSEONG";
            _data.sendStartDt = $("#sendStartDt_anseong").val();
            _data.sendEndDt = $("#sendEndDt_anseong").val();
            paramObject.sendPlaceList.push(_data);
        }

        //console.log(JSON.stringify(paramObject));

        CommonAjax.basic({
            url: "/item/rsvSale/setRsvSale.json",
            method: "post",
            dataType: "json",
            contentType : "application/json; charset=utf-8",
            data: JSON.stringify(paramObject),
            callbackFunc: function (seq) {
                alert("저장되었습니다.");
                main.search(seq);
            },
            error: function(e) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        alert(resError.responseJSON.returnMessage);
                    } else {
                        alert(CommonErrorMsg.dfErrorMsg);
                    }
                }
            }
        });
    },
    callUploadPopup : function(obj) {
        if ($.jUtil.isEmpty($("#saleType").val())) {
            alert("판매유형을 선택하세요.");
            return;
        }
        var popWidth = "600";
        var popHeight = "250";
        var callback = "main.uploadPopupCallback";
        var applyScope = $("#saleType").val();
        var uri = "/promotion/popup/upload/uploadApplyScopePop?promotionType=RSV_SALE&applyScope=" + applyScope + "&callBackScript=" + callback;

        windowPopupOpen(uri, "uploadScopePop", popWidth, popHeight);
    },
    /**
     * 업로드팝업 콜백 - 선택한 데이터 적용 대상 그리드에 추가
     * @param callbackDataArray
     */
    uploadPopupCallback : function(callbackDataArray) {
        var dataArray = new Array();
        var saleType = $("#saleType").val();

        for (var i in callbackDataArray) {
            var dataObj = new Object();

            dataObj.itemNo = callbackDataArray[i].itemNo;
            dataObj.itemNm = callbackDataArray[i].itemNm;
            dataObj.sendPlace = callbackDataArray[i].sendPlace;

            dataArray.push(dataObj);
        }

        itemListGrid.appendData(dataArray);
        itemListGrid.controlClear();
    },
    removeItemGridData : function() {
        itemListGrid.removeCheckData();

        itemListGrid.gridView.clearColumnFilters("itemNo");
        itemListGrid.gridView.clearColumnFilters("itemNm");
        itemListGrid.gridView.clearColumnFilters("sendPlace");
    },
    /**
     * 상품팝업 호출
     * @param callBack
     */
    getItemPop : function() {
        var callback = "main.itemPopupCallBack";
        var storeType = $('#storeType').val();
        var siteType = "";

        if ($.jUtil.isEmpty(storeType)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        if (storeType == "CLUB") {
            siteType = 'CLUB';
        } else {
            siteType = 'HOME';
        }

        var url = "/common/popup/itemPop?callback=" + callback + "&isMulti=N&mallType=TD&siteType=" + siteType + "&storeType=" + storeType;
        var target = "itemPopup";
        var popWidth = "1084";
        var popHeight = "650";

        windowPopupOpen(url, target, popWidth, popHeight);
    }
    ,
    /**
     * 상품 그리드 추가
     */
    addItem : function () {
        if (!main.valid.addItem()) {
            return false;
        }

        var addDataArr = new Array();
        var addDataObj = new Object();
        addDataObj.itemNo = $("#itemNo").val();
        addDataObj.itemNm = $("#itemNm").val();
        addDataObj.sendPlace = $('#sendPlace').val();
        addDataArr.push(addDataObj);

        itemListGrid.appendData(addDataArr);
        $("#itemNo, #itemNm").val("");
    },
    /**
     * 예약상품판매관리 메인 리스트 조회
     */
    search : function (rsvSaleSeq) {
        if (!main.valid.search()) {
            return ;
        }

        $("#detailSetForm").resetForm();
        $("#itemNo, #itemNm").val("");
        itemListGrid.dataProvider.clearRows();

        $("#rsvSaleTotalCount").html("0");

        var params = $('#searchForm').serialize();

        CommonAjax.basic({
            url: '/item/rsvSale/getRsvSaleList.json?',
            data: params,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                rsvSaleListGrid.setData(res);
                $('#rsvSaleTotalCount').html($.jUtil.comma(rsvSaleListGrid.gridView.getItemCount()));

                if (rsvSaleSeq !== undefined && rsvSaleSeq != "") {
                    var rowIndex = rsvSaleListGrid.dataProvider.searchDataRow({fields: ["rsvSaleSeq"], values: [rsvSaleSeq]});

                    if (rowIndex > -1) {
                        rsvSaleListGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                        rsvSaleListGrid.gridView.onDataCellClicked();
                    }
                }
            }
        });
    },
    /**
     * 검색 초기화
     */
    resetSearchForm : function () {
        $("#searchForm").resetForm();

        // 검색기간 초기화
        calendarInit.initCalendarDate("schStartDt", "schEndDt", "SEARCH", "3m");
    },
    /**
     * 등록/수정 form 초기화
     */
    resetForm : function() {
        main.formElementControl(false);

        // 검색기간 초기화
        calendarInit.initCalendarDate("schStartDt", "schEndDt", "SEARCH", "3m");

        // 판매기간
        calendarInit.initCalendarDate("rsvSaleStartDt", "rsvSaleEndDt", "DATA", "1w");

        // 발송기간
        calendarInit.initCalendarDate("sendStartDt_haman", "sendEndDt_haman", "DATA", "1w");
        calendarInit.initCalendarDate("sendStartDt_anseong", "sendEndDt_anseong", "DATA", "1w");
        calendarInit.initCalendarDate("sendStartDt_store", "sendEndDt_store", "DATA", "1w");

        //기본정보
        $("#detailSetForm").resetForm();

        //판매유형
        $("#saleType").val("").change();

        //희망발솔일
        $("#hopeSendDays").val("");

        //발송지
        $("#itemNo, #itemNm").val("");

        //그리드 클리어
        itemListGrid.dataProvider.clearRows();

        $("#rsvSaleTotalCount, #itemTotalCount").html("0");

        //점포배송요일
        $("#chkLine").hide();
    },
    /**
     * 등록/수정 form 초기화
     */
    clearForm : function() {
        //기본정보
        $("#detailSetForm").resetForm();

        //발송지
        $("#tdSendPeriod").html("");

        var rsvSaleSeq = rsvSaleListGrid.dataProvider.getJsonRow(rsvSaleListGrid.gridView.getCurrent().dataRow).rsvSaleSeq;
        $("#rsvSaleSeq").val(rsvSaleSeq);

        // 예약판매 기간 초기화
        calendarInit.initCalendarDate("rsvSaleStartDt", "rsvSaleEndDt", "DATA", "1m");
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        itemListGrid.excelDownload();
    },
    itemPopupCallBack: function (res) {
        $('#itemNo').val(res[0].itemNo);
        $('#itemNm').val(res[0].itemNm);
    },
    /**
     * 발송지 리스트 조회
     * @param rsvSaleSeq
     */
    getRsvSendPlaceList : function(rsvSaleSeq) {
        var saleType = main.detailOriginalData.saleType;

        CommonAjax.basic({
            url:"/item/rsvSale/getRsvSaleSendPlaceList.json?rsvSaleSeq="+rsvSaleSeq
            , method : "GET"
            , callbackFunc : function(dataList) {
                if (dataList != null) {
                    $.each(dataList, function (i, data) {
                        if (data.sendPlace == 'HAMAN') {
                            $("#sendStartDt_haman").val(data.sendStartDt);
                            $("#sendEndDt_haman").val(data.sendEndDt);
                        } else if (data.sendPlace == 'ANSEONG') {
                            $("#sendStartDt_anseong").val(data.sendStartDt);
                            $("#sendEndDt_anseong").val(data.sendEndDt);
                        } else if (data.sendPlace == 'STORE') {
                            $("#sendStartDt_store").val(data.sendStartDt);
                            $("#sendEndDt_store").val(data.sendEndDt);

                            if (saleType == "DRCT") {
                                $("#labelChkSetYn").css("display", "inline-block");
                                if (data.monYn == "Y" || data.tueYn == "Y" || data.wedYn == "Y" || data.thuYn == "Y" || data.friYn == "Y" || data.satYn == "Y" || data.sunYn == "Y") {
                                    $("#chkSetYn").prop("checked", true);
                                }

                                $("#chkSetYn").change();

                                // 적용요일
                                $("#monYn").prop("checked", data.monYn == "Y");
                                $("#tueYn").prop("checked", data.tueYn == "Y");
                                $("#wedYn").prop("checked", data.wedYn == "Y");
                                $("#thuYn").prop("checked", data.thuYn == "Y");
                                $("#friYn").prop("checked", data.friYn == "Y");
                                $("#satYn").prop("checked", data.satYn == "Y");
                                $("#sunYn").prop("checked", data.sunYn == "Y");
                            } else {
                                $("#labelChkSetYn").css("display", "none");
                            }
                        }
                    });
                }
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            }
        });
    },
    /**
     * 메타 정보 조회 콜백
     * @param rsvSaleData
     */
    detailCallback : function(data) {
        // 원본 데이터 값 설정
        main.formElementControl(false);
        main.detailOriginalData = data;

        var chgYn = data.chgYn;

        $("#rsvSaleSeq").val(data.rsvSaleSeq);
        $("#storeType").val(data.storeType).change();
        $("#saleType").val(data.saleType).change();
        $("#rsvSaleNm").val(data.rsvSaleNm);
        $("#useYn").val(data.useYn);
        $("#rsvSaleStartDt").val(data.rsvSaleStartDt);
        $("#rsvSaleEndDt").val(data.rsvSaleEndDt);
        $('input:radio[name=saleWay]:input[value=' + data.saleWay + ']').prop("checked", true).change();

        if (data.hopeSendDays > 0) {
            $("#hopeSendDays").val(data.hopeSendDays);
        } else {
            $("#hopeSendDays").val('');
        }

        if (chgYn == "N") {
            main.formElementControl(true);
        }
    },
    formElementControl : function(flag) {
        var onclickStr = "";
        if (flag) onclickStr = "return false;";

        $("#detailSetForm").find("select").each(function() {
            var formElementId = $(this).attr("id");
            if($("#" + formElementId).attr("isUpdate") != "true") {
                if (flag) {
                    removeAllSelectOption($(this));
                } else {
                    addSelectOption($(this));
                }
            }
        });

        $("#detailSetForm").find("input:text").each(function() {
            var formElementId = $(this).attr("id");
            if($("#" + formElementId).attr("isUpdate") != "true") {
                $("#" + formElementId).attr("readonly", flag);
            }
        });

        $("#detailSetForm").find("input:checkbox").each(function() {
            var formElementId = $(this).attr("id");
            if (formElementId != undefined) {
                if($("#" + formElementId).attr("isUpdate") != "true") {
                    $("#" + formElementId).attr("onClick", onclickStr);
                }
            }
        });

        $("#detailSetForm").find("input:radio").each(function() {
            var formElementName = $(this).attr("name");
            if (formElementName != undefined) {
                if($("#" + formElementName).attr("isUpdate") != "true") {
                    $('input:radio[name=' + formElementName +']').prop("disabled", flag);
                }
            }
        });

        $(".dateControl .ui-datepicker-trigger").attr("disabled", flag);
    },
    /**
     * 점포유형 변경 시
     * @param obj
     */
    changeStoreType : function(obj) {
        var saleType = $("#saleType");
        addSelectOption(saleType);
    },
    /**
     * 판매유형 변경 시
     * @param obj
     */
    changeSaleType : function(obj) {
        var saleType = $(obj).val();
        var sendPlace = $("#sendPlace");

        removeAllSelectOption(sendPlace);
        addSelectOption(sendPlace);

        if (saleType == "DRCT_DLV") { //명절선물
            //판매방법
            $('input:radio[name=saleWay]:input[value="NOR"]').prop("checked", true).change();
            $("#trSaleWay").show();
        } else {
            //판매방법
            $("#trSaleWay").hide();
            $('input:radio[name=saleWay]:input[value="RSV"]').prop("checked", true).change();
        }
    },
    /**
     * 점포유형 변경 시
     * @param obj
     */
    changeChkSetYn : function(obj) {
        $(".chkDay").prop("checked", false);
        var isChkSetYn = $(obj).prop("checked");

        if (isChkSetYn) {
            $("#chkLine").show();
        } else {
            $("#chkLine").hide();
        }
    },
    /**
     * 판매방법 변경 시
     * @param obj
     */
    changeSaleWay : function (obj) {
        var saleType = $("#saleType").val();
        var saleWay = $(obj).val();
        var sendPlace = $("#sendPlace");

        if (saleType == "DRCT_DLV" && saleWay == "NOR") { //명절선물, 일반판매
            $("#trSendPeriod_haman, #trSendPeriod_anseong, #trSendPeriod_store").hide();
            $(".trHopeSendDays").show();
            //4일로 고정 하드코
            $("#hopeSendDays").val("4").prop("disabled", true);
        } else {
            $(".trHopeSendDays").hide();
            $("#trSendPeriod_haman, #trSendPeriod_anseong, #trSendPeriod_store").show();

            removeAllSelectOption(sendPlace);
            addSelectOption(sendPlace);

            switch (saleType) {
                default:
                case "DRCT_DLV": //명절선물
                    $("#trSendPeriod_haman tr").prop("rowspan", 3);
                    $("#trSendPeriod").hide();
                    $('#trSendPeriod_haman, #trSendPeriod_anseong').show();
                    $("#labelChkSetYn").css("display", "none");
                    //$('#sendStartDt_haman, #sendEndDt_haman, #sendStartDt_anseong, #sendEndDt_anseong').val("");
                    /*/!*$('#trSendPeriod_haman, #trSendPeriod_anseong').find('input').each(function() {
                        this.value = '';
                    });*/
                    break;
                case "DRCT": //절임배추
                case "DRCT_NOR": //일반상품
                    $("#trSendPeriod_haman tr").prop("rowspan", 2).hide();
                    $("#trSendPeriod").show();
                    $('#trSendPeriod_haman, #trSendPeriod_anseong').hide();
                    //$('#sendStartDt_haman, #sendEndDt_haman, #sendStartDt_anseong, #sendEndDt_anseong').val("");
                    /*
                    $('#trSendPeriod_haman, #trSendPeriod_anseong').find('input').each(function() {
                        this.value = '';

                    });*/

                    $("#sendPlace").find("option[value!="+ "STORE" +"]").remove();
                    if (saleType == "DRCT") {
                        $("#labelChkSetYn").css("display", "inline-block");
                    } else {
                        $("#labelChkSetYn").css("display", "none");
                    }
                    break;
            }

            $("#chkSetYn").prop("checked", false).change();
        }
    }
};

//select box 전체 초기화 (처음 옵션 적용)
function addSelectOption(obj) {
    var jsonData = "";
    var defaultOption = "";
    var appendOption = "";
    switch ($(obj).attr("id")) {
        case "sendPlace":
            jsonData = sendPlaceJson;
            defaultOption = "<option value=''>발송지</option>";
            break;
        case "storeType":
            jsonData = storeTypeJson;
            break;
        case "saleType":
            jsonData = saleTypeJson;
            defaultOption = "<option value=''>선택</option>";
            break;
        case "useYn":
            jsonData = useYnJson;
            break;
    }

    if (jsonData == "") return false;

    $(obj).find("option").remove();

    if (defaultOption != "") $(obj).prepend(defaultOption);

    $.each(jsonData, function(key, code) {
        var chkOption = 0;
        var codeCd = code.mcCd;
        var codeNm = code.mcNm;
        var codeRef1 = code.ref1;
        var codeRef3 = code.ref3;

        $(obj).find("option").each(function() {
            if ($(this).val() == codeCd) {
                chkOption++;
            }
        });

        if (chkOption == 0) {
            if (($(obj).attr("id") != "storeType" || ($(obj).attr("id") == "storeType" && codeCd != 'DS' && codeCd != 'AURORA' && codeCd != 'EXP'))) {
                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
            }
        }
    });

    if (appendOption != "") $(obj).append(appendOption);
}
// select box 옵션 전체 제거 (선택된 옵션 제외)
function removeAllSelectOption(obj){
    $.each(obj, function() {
        $(this).find("option").not(":selected").remove();
    });
}

function removeSelectOption(obj, val){
    $(obj).find("option[value="+ val +"]").remove();
}

/**
 * 유효성 검사 영역
 * @type {{set: main.valid.set}}
 */
main.valid = {
    set : function () {
        var storeType = $('#storeType').val();
        var saleType = $('#saleType').val();
        var saleWay = $('input:radio[name="saleWay"]:checked').val();
        var rsvSaleNm = $('#rsvSaleNm').val();
        var rsvSaleStartDt = $('#rsvSaleStartDt').val();
        var rsvSaleEndDt = $('#rsvSaleEndDt').val();
        var nowDt = moment().format("YYYY-MM-DD");

        if ($.jUtil.isEmpty(storeType)) {
            alert('점포유형을 선택하세요.');
            return false;
        }

        if ($.jUtil.isEmpty(saleType)) {
            alert('판매유형을 선택하세요.');
            return false;
        }

        if ($.jUtil.isEmpty(rsvSaleNm)) {
            alert('예약판매명을 입력하세요.');
            return false;
        }

        var rsvSaleNmLen = $("#rsvSaleNm").attr("maxLength");
        if (rsvSaleNm.length > rsvSaleNmLen) {
            alert('예약판매명은 최대 25자까지 입력가능합니다.');
            return false;
        }

        if ($.jUtil.isEmpty(rsvSaleStartDt) || $.jUtil.isEmpty(rsvSaleEndDt)) {
            alert("예약판매기간을 입력하세요.");
            return false;
        }

        if (!moment(rsvSaleStartDt, 'YYYY-MM-DD', true).isValid()) {
            alert("예약판매기간 날짜 형식이 맞지 않습니다.");
            $("#rsvSaleStartDt").val(nowDt);
            return false;
        }

        if (!moment(rsvSaleEndDt, 'YYYY-MM-DD', true).isValid()) {
            alert("예약판매기간 날짜 형식이 맞지 않습니다.");
            $("#rsvSaleEndDt").val(nowDt);
            return false;
        }

        /*if ($.jUtil.diffDate(nowDt, rsvSaleStartDt, "D") < 0) {
            alert("예약판매시작일은 과거일을 선택할 수 없습니다.");
            $("#rsvSaleStartDt").val(nowDt);
            return false;
        }*/

        if ($.jUtil.diffDate(rsvSaleStartDt, rsvSaleEndDt, "D") < 0) {
            alert("예약판매시작일은 종료일을 앞설 수 없습니다.");
            $("#rsvSaleStartDt").val(rsvSaleEndDt);
            return false;
        }

        if ($.jUtil.isEmpty(saleWay)) {
            alert("판매방법을 선택하세요.");
            return false;
        }

        if (saleWay == "NOR") {
            return true;
        }

        var saleStartDt = $("#rsvSaleStartDt").val();
        var sendStartDt_haman = $("#sendStartDt_haman").val();
        var sendEndDt_haman = $("#sendEndDt_haman").val();
        var sendStartDt_anseong = $("#sendStartDt_anseong").val();
        var sendEndDt_anseong = $("#sendEndDt_anseong").val();
        var sendStartDt_store = $("#sendStartDt_store").val();
        var sendEndDt_store = $("#sendEndDt_store").val();

        switch (saleType) {
            case "DRCT_DLV": //명절선물
                if ((!$.jUtil.isEmpty(sendStartDt_haman) && !moment(sendStartDt_haman, 'YYYY-MM-DD', true).isValid())
                    || (!$.jUtil.isEmpty(sendEndDt_haman) && !moment(sendEndDt_haman, 'YYYY-MM-DD', true).isValid())
                    || (!$.jUtil.isEmpty(sendStartDt_anseong) && !moment(sendStartDt_anseong, 'YYYY-MM-DD', true).isValid())
                    || (!$.jUtil.isEmpty(sendEndDt_anseong) && !moment(sendEndDt_anseong, 'YYYY-MM-DD', true).isValid())
                    || (!$.jUtil.isEmpty(sendStartDt_store) && !moment(sendStartDt_store, 'YYYY-MM-DD', true).isValid())
                    || (!$.jUtil.isEmpty(sendEndDt_store) && !moment(sendEndDt_store, 'YYYY-MM-DD', true).isValid())
                ) {
                    alert("발송기간 날짜 형식이 맞지 않습니다.");
                    //$("#rsvSaleStartDt").val(nowDt);
                    return false;
                }

                if (isExistData("HAMAN") && ($.jUtil.isEmpty(sendStartDt_haman) || $.jUtil.isEmpty(sendEndDt_haman))) {
                    alert("[중앙(함안) 발송기간을 입력하세요.");
                    return false;
                }

                if (isExistData("ANSEONG") && ($.jUtil.isEmpty(sendStartDt_anseong) || $.jUtil.isEmpty(sendEndDt_anseong))) {
                    alert("[중앙(안성) 발송기간을 입력하세요.");
                    return false;
                }

                if (isExistData("STORE") && ($.jUtil.isEmpty(sendStartDt_store) || $.jUtil.isEmpty(sendEndDt_store))) {
                    alert("[점포] 발송기간을 입력하세요.");
                    return false;
                }

                if (!$.jUtil.isEmpty(sendStartDt_haman) && !$.jUtil.isEmpty(sendEndDt_haman)) {
                    if ($.jUtil.diffDate(sendStartDt_haman, sendEndDt_haman, "D") < 0) {
                        alert("[중앙(함안)] 종료일이 시작일보다 빠를 수 없습니다.");
                        $("#sendStartDt_haman").val(sendEndDt_haman);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_anseong) && !$.jUtil.isEmpty(sendEndDt_anseong)) {
                    if ($.jUtil.diffDate(sendStartDt_anseong, sendEndDt_anseong, "D") < 0) {
                        alert("[중앙(안성)] 종료일이 시작일보다 빠를 수 없습니다.");
                        $("#sendStartDt_anseong").val(sendEndDt_anseong);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_store) && !$.jUtil.isEmpty(sendEndDt_store)) {
                    if ($.jUtil.diffDate(sendStartDt_store, sendEndDt_store, "D") < 0) {
                        alert("[점포] 종료일이 시작일보다 빠를 수 없습니다.");
                        $("#sendStartDt_store").val(sendEndDt_store);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_haman)) {
                    if ($.jUtil.diffDate(saleStartDt, sendStartDt_haman,"D") < 0) {
                        alert("[중앙(함안)] 발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_haman").val(saleStartDt);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_anseong)) {
                    if ($.jUtil.diffDate(saleStartDt, sendStartDt_anseong, "D") < 0) {
                        alert("[중앙(안성)] 발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_anseong").val(saleStartDt);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_store)) {
                    if ($.jUtil.diffDate(saleStartDt, sendStartDt_store, "D") < 0) {
                        alert("[점포] 발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_store").val(saleStartDt);
                        return false;
                    }
                }

                /*

                var sendStartDt_haman_dt = new Date($("#sendStartDt_haman").val());
                var sendStartDt_anseong_dt = new Date($("#sendStartDt_anseong").val());
                var sendStartDt_store_dt = new Date($("#sendStartDt_store").val());
                var saleStartDt = new Date($("#saleStartDt").val())

                if (!$.jUtil.isEmpty(sendStartDt_haman) && !$.jUtil.isEmpty(sendEndDt_haman)) {
                    var validTerm = sendStartDt_haman_dt - saleStartDt;
                    if ((validTerm / day) < 0) {
                        alert("발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_haman").val($("#saleStartDt").val());
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_anseong) && !$.jUtil.isEmpty(sendEndDt_anseong)) {
                    var validTerm = sendStartDt_anseong_dt - saleStartDt;
                    if ((validTerm / day) < 0) {
                        alert("발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_anseong").val($("#saleStartDt").val());
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_store) && !$.jUtil.isEmpty(sendEndDt_store)) {
                    var validTerm = sendStartDt_store_dt - saleStartDt;
                    if ((validTerm / day) < 0) {
                        alert("발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_store").val($("#saleStartDt").val());
                        return false;
                    }
                }*/

                break;
            case "DRCT": //절임배추
            case "DRCT_NOR": //일반상품
                if ($.jUtil.isEmpty(sendStartDt_store) || $.jUtil.isEmpty(sendEndDt_store)) {
                    alert("[점포] 발송기간을 입력하세요.");
                    return false;
                }

                if (!$.jUtil.isEmpty(sendStartDt_store) && !moment(sendEndDt_store, 'YYYY-MM-DD', true).isValid()) {
                    alert("[점포] 발송기간 날짜 형식이 맞지 않습니다.");
                    //$("#rsvSaleStartDt").val(nowDt);
                    return false;
                }

                if (!$.jUtil.isEmpty(sendStartDt_store) && !$.jUtil.isEmpty(sendEndDt_store)) {
                    if ($.jUtil.diffDate(sendStartDt_store, sendEndDt_store, "D") < 0) {
                        alert("[점포] 종료일이 시작일보다 빠를 수 없습니다.");
                        $("#sendStartDt_store").val(sendEndDt_store);
                        return false;
                    }
                }

                if (!$.jUtil.isEmpty(sendStartDt_store)) {
                    if ($.jUtil.diffDate(saleStartDt, sendStartDt_store,"D") < 0) {
                        alert("발송기간은 예약판매기간보다 앞설 수 없습니다.");
                        $("#sendStartDt_store").val(saleStartDt);
                        return false;
                    }
                }

                break;
        }

        return true;
    },
    addItem : function () {
        var itemNo = $("#itemNo").val();
        var sendPalce = $("#sendPlace").val();

        if ($.jUtil.isEmpty(itemNo)) {
            alert("추가할 상품이 없습니다.");
            return false;
        }

        if ($.jUtil.isEmpty(sendPalce)) {
            alert("발송지를 선택하세요.");
            return false;
        }

        return true;
    },
    search : function () {
        $("#rsvSaleTotalCount").html("0");

        var rsvSaleNm = $.trim($('#schRsvSalesNm').val());
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if (!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다.");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 앞설 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (!$.jUtil.isEmpty(rsvSaleNm) && (rsvSaleNm.length < 2)) {
            alert("2글자 이상 입력해야 합니다.");
            $("rsvSaleNm").focus();
            return false;
        }
        return true;
    }
}

// 달력 초기화
var calendarInit = {
    initCalendarDate : function (startId, endId, form, flag) {
        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        if (form == "SEARCH") {
            main.setDate = Calendar.datePickerRange(startId, endId);

            main.setDate.setStartDate(0);
            main.setDate.setEndDate(flag);
        } else {
            main.setDate = Calendar.datePickerRange(startId, endId);

            main.setDate.setStartDate(0);
            main.setDate.setEndDate(flag);
        }

        endObj.datepicker("option", "minDate", startObj.val());
    }
};

/**
 * 유효한 그리드 데이터 중에 해당 발송지 데이터가 존재하는지를 확인
 * @param sendPlace
 */
function isExistData(sendPlace) {
    var count = 0;

    var createdDataList = itemListGrid.dataProvider.getStateRows("created");
    createdDataList = createdDataList.concat(itemListGrid.dataProvider.getStateRows("updated"));
    createdDataList = createdDataList.concat(itemListGrid.dataProvider.getStateRows("none"));

    var createdDatas = new Array();
    createdDataList.forEach(function(row, index) {
        var dataRow = itemListGrid.dataProvider.getJsonRow(row);
        createdDatas.push(dataRow);
    });

    createdDatas.some( data => {
        if(data.sendPlace == sendPlace){
            count++;
        }
        return (count > 0);
    });

    return (count > 0);
}





