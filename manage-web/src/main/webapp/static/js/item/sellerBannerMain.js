
/**
 * 상품관리  > 판매자 상품상세 일괄공지
 */
$(document).ready(function() {
    sellerBannerListGrid.init();
    sellerBanner.init();

    commonCategory.init();
    CommonAjaxBlockUI.global();
});

// 상품상세 일괄공지 리스트 그리드
var sellerBannerListGrid = {
    gridView : new RealGridJS.GridView("sellerBannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        sellerBannerListGrid.initGrid();
        sellerBannerListGrid.initDataProvider();
        sellerBannerListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));

    },
    initGrid : function() {
        sellerBannerListGrid.gridView.setDataSource(sellerBannerListGrid.dataProvider);

        sellerBannerListGrid.gridView.setStyles(sellerBannerListGridBaseInfo.realgrid.styles);
        sellerBannerListGrid.gridView.setDisplayOptions(sellerBannerListGridBaseInfo.realgrid.displayOptions);
        sellerBannerListGrid.gridView.setColumns(sellerBannerListGridBaseInfo.realgrid.columns);
        sellerBannerListGrid.gridView.setOptions(sellerBannerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        sellerBannerListGrid.dataProvider.setFields(sellerBannerListGridBaseInfo.dataProvider.fields);
        sellerBannerListGrid.dataProvider.setOptions(sellerBannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        sellerBannerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            sellerBanner.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        sellerBannerListGrid.dataProvider.clearRows();
        sellerBannerListGrid.dataProvider.setRows(dataList);
    },

};
// 판매자 상품상세공지
var sellerBanner = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d') //default :30일
    },
    /**
     * 검색일 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        sellerBanner.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        sellerBanner.setDate.setEndDate(0);
        sellerBanner.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(sellerBanner.search); //검색
        $('#searchResetBtn').bindClick(sellerBanner.resetSearchForm); //초기화
        
     },
    /**
     * detail 영역 초기화
     */
    resetSellerBanner : function() {

        $('#bannerNm').text('');
        $('#bannerDesc').text('');
        $('#dispType').text('');
        $('#partnerId').text('');
        $('#dispYn').text('');
        $('#dispStartDt').text('');
        $('#dispStartEnd').text('');

        $('#cateRow').hide();
        $('#itemRow').hide();
        $('#dispDateRow').hide();
    },
    /**
     * 상품상세공지검색
     * @returns {boolean}
     */
    search : function() {

        if(!sellerBanner.valid.search()){
            return false;
        }

        var form = $('#sellerBannerSchForm').serializeObject();
        CommonAjax.basic({
            url: '/item/banner/getSellerBannerList.json',
            data: JSON.stringify(form),
            method: "POST",
            contentType: "application/json",
            successMsg: null,
            callbackFunc: function (res) {
                sellerBannerListGrid.setData(res);
                $('#sellerBannerTotalCount').html($.jUtil.comma(sellerBannerListGrid.gridView.getItemCount()));
            }
        });

        sellerBanner.resetSellerBanner();
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        var rowDataJson = sellerBannerListGrid.dataProvider.getJsonRow(selectRowId);

        $('#bannerNm').text(rowDataJson.bannerNm); //제목
        $('#dispType').text(rowDataJson.dispTypeNm); //구분
        $('#partnerId').text((rowDataJson.partnerId) + "(" + rowDataJson.partnerNm + ")") //판매자명

        $('#bannerDesc').text(rowDataJson.bannerDesc); //공지내용
        $('#dispYn').text(rowDataJson.dispYnNm); //노출여부

        $("#dispDateRow").show();

        $('#dispStartDt').text(rowDataJson.dispStartDt);
        $('#dispEndDt').text(rowDataJson.dispEndDt);

        switch (rowDataJson.dispType) {

            case "ALL" :
                $('#items').text('');
                $('#categoryNm').text('');
                $('#itemRow').hide();
                $('#cateRow').hide();
                break;

            case "CATE" :
                $('#cateRow').show();
                $('#itemRow').hide();
                $('#categoryNm').text( rowDataJson.lcateNm +">"+ rowDataJson.mcateNm+">"+rowDataJson.scateNm +">"+rowDataJson.dcateNm );
                break;

            case "ITEM" :
                $('#itemRow').show();
                $('#cateRow').hide();

                sellerBanner.getItemList(rowDataJson.bannerNo);

                break;
        }

    },
    /**
     * 공지타입 : 상품별 > 등록 상품 조회
     */
    getItemList : function(bannerNo) {

        CommonAjax.basic( {
            url:'/item/banner/getItemMatchList.json?',
            data: {bannerNo: (bannerNo)},
            method: "GET",
            callbackFunc: function (res) {
                var resultCount = res.length;
                var items;

                if( resultCount >0 ){

                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];
                        if (idx > 0) {
                            items = items + "<br/>" + data.itemNo + " | " + data.itemNm;
                        }else{
                            items = data.itemNo + " | " + data.itemNm;
                        }
                    }
                    $('#items').html(items);
                }else {
                    alert("조회된 상품이 없습니다. ");
                }
            }
        })
    },
    /**
     * 검색폼 초기화
     */
    resetSearchForm : function() {

        sellerBanner.initSearchDate('-30d');

        $('#schCateCd1').val('').change();

        $('#schKeyword').val('');
        $('#schDispType').val('');
        $('#schDispYn').val('');
        $('#schDateType').val("REGDT");


    },

};
/**
 * 검증
 * @type {{search: sellerBanner.valid.search}}
 */
sellerBanner.valid = {

    search: function () {
        // 날짜 체크
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            return false;
        }
        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        //검색어
        var searchKeyword = $('#schKeyword').val();

        if ( !$.jUtil.isEmpty(searchKeyword) && searchKeyword.length < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            $('#schKeyword').focus();
            return false;
        }

        return true;
    }
}