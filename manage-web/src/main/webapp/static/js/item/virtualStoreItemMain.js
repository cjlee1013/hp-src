/**
 * 상품관리 > 점포상품관리 > 택배점 취급상품관리
 */
$(document).ready(function() {
    main.init();
    storeListGrid.init();
    itemListGrid.init();
    CommonAjaxBlockUI.global();
});

//점포 리스트 그리드
var storeListGrid = {
    selectedRowIdx : 0,
    gridView : new RealGridJS.GridView("storeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        storeListGrid.initGrid();
        storeListGrid.initDataProvider();
        storeListGrid.event();
    },
    initGrid : function () {
        storeListGrid.gridView.setDataSource(storeListGrid.dataProvider);

        storeListGrid.gridView.setStyles(storeListGridBaseInfo.realgrid.styles);
        storeListGrid.gridView.setDisplayOptions(storeListGridBaseInfo.realgrid.displayOptions);
        storeListGrid.gridView.setColumns(storeListGridBaseInfo.realgrid.columns);
        storeListGrid.gridView.setOptions(storeListGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(storeListGrid.gridView, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(storeListGrid.gridView, "useYn", useYnJson);
        setColumnLookupOptionForJson(storeListGrid.gridView, "originStoreType", storeTypeJson);
    },
    initDataProvider : function() {
        storeListGrid.dataProvider.setFields(storeListGridBaseInfo.dataProvider.fields);
        storeListGrid.dataProvider.setOptions(storeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        storeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = storeListGrid.dataProvider.getJsonRow(storeListGrid.gridView.getCurrent().dataRow);
            storeListGrid.gridRowSelect(gridRow);
        };
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect :  function (rowDataJson) {
        //var rowDataJson = storeListGrid.dataProvider.getJsonRow(selectRowId);
        var storeId = rowDataJson.storeId;

        storeListGrid.getVirtualStoreItemList(storeId);

        //상품등록 폼 초기화
        $("#itemSetForm").resetForm();

        //row 선택 후 기본 정보 셋팅
        var storeType = rowDataJson.storeType;
        var storeNm = rowDataJson.storeNm;
        var originStoreId = rowDataJson.originStoreId;
        var originStoreNm = rowDataJson.originStoreNm;

        $("#storeType").val(storeType);
        $("#storeId").val(storeId);
        $("#spanStoreId").text(storeId);
        $("#spanStoreNm").html(storeNm);
        $("#originStoreId").html(originStoreId);
        $("#originStoreNm").html(originStoreNm);

        //본점포의 택배배송정책 리스트
        main.getShipPolicyList(originStoreId);
    },
    /**
     * 가상점포 취급상품 리스트 조회
     * @param storeId
     */
    getVirtualStoreItemList : function(storeId) {
        CommonAjax.basic({
            url:"/item/virtualStoreItem/getVirtualStoreItemList.json?storeId="+storeId
            , method : "GET"
            , callbackFunc : function(dataList) {
                itemListGrid.setData(dataList);
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.errors[0].detail != null) {
                        // api 에러메세지
                        alert(resError.responseJSON.errors[0].detail);
                    }
                }
            }
        });
    },
    setData : function(dataList) {
        storeListGrid.dataProvider.clearRows();
        storeListGrid.dataProvider.setRows(dataList);

        $("#storeTotalCount").val(storeListGrid.gridView.getItemCount())
    },
    excelDownload: function() {
        if(storeListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "취급상품_" + _date.getFullYear() + month + date;

        storeListGrid.gridView.exportGrid( {
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

//취급대상 리스트 그리드
var itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
    },
    initGrid : function () {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);

        itemListGrid.gridView.setStyles(itemListGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemListGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemListGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemListGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(itemListGrid.gridView, "dealYn", dealYnJson);
        setColumnLookupOptionForJson(itemListGrid.gridView, "virtualStoreOnlyYn", virtualStoreOnlyYnJson);
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemListGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            itemListGrid.gridRowSelect(index.dataRow)
        };
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect :  function (selectRowId) {
        var storeType = storeListGrid.dataProvider.getJsonRow(storeListGrid.gridView.getCurrent().dataRow).storeType;
        var rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);
        var storeId = rowDataJson.storeId;
        var itemNo = rowDataJson.itemNo;

        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
        if (itemListGrid.dataProvider.getRowState(selectRowId) == 'created') {
            var storeNm = rowDataJson.storeNm;
            var originStoreId = rowDataJson.originStoreId;
            var originStoreNm = rowDataJson.originStoreNm;
            var itemNm = rowDataJson.itemNm;
            var dealYn = rowDataJson.dealYn;
            var shipPolicyNo = rowDataJson.shipPolicyNo;
            var virtualStoreOnlyYn = rowDataJson.virtualStoreOnlyYn;
            var virtualStoreOnlyYnNm = (rowDataJson.virtualStoreOnlyYn == "Y" ? "설정":"설정안함");

            $("#storeType").val(storeType);
            $("#storeId").val(storeId);
            $("#spanStoreId").text(storeId);
            $("#spanStoreNm").text(storeNm);
            $("#originStoreId").text(originStoreId);
            $("#originStoreNm").text(originStoreNm);
            $("#itemNo").val(itemNo);
            $("#itemNm").val(itemNm);

            //취급여부
            $('input:radio[name=dealYn]:input[value=' + dealYn + ']').prop("checked", true);

            //가상점 단독판매여부
            $("#virtualStoreOnlyYn").text(virtualStoreOnlyYn);
            $("#virtualStoreOnlyYnNm").text(virtualStoreOnlyYnNm);
            $("#shipPolicyNo").val(shipPolicyNo);
        } else {
            itemListGrid.getVirtualStoreItemInfo(storeId, itemNo);
        }
    },
    /**
     * 가상점포 취급상품 리스트 조회
     * @param storeId
     */
    getVirtualStoreItemInfo : function(storeId, itemNo) {
        CommonAjax.basic({
            url:"/item/virtualStoreItem/getVirtualStoreItemInfo.json?storeId="+storeId+"&itemNo="+itemNo
            , method : "GET"
            , callbackFunc : function(data) {
                itemListGrid.setItemInfo(data);
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.errors[0].detail != null) {
                        // api 에러메세지
                        alert(resError.responseJSON.errors[0].detail);
                    }
                }
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    setItemInfo :  function (data) {
        $("#itemSetForm").resetForm();
        $("#chgYn").val("Y");

        //점포타입
        var storeType = storeListGrid.dataProvider.getJsonRow(storeListGrid.gridView.getCurrent().dataRow).storeType;
        $("#storeType").val(storeType);

        //기본정보
        $('#storeId').val(data.storeId);
        $('#storeNm').val(data.storeNm);

        $('#originStoreId').val(data.originStoreId);
        $('#originStoreNm').val(data.originStoreNm);

        $('#itemNo').val(data.itemNo);
        $('#itemNm').val(data.itemNm);

        $("#virtualStoreOnlyYn").val(data.virtualStoreOnlyYn);
        $("#virtualStoreOnlyYnNm").text(data.virtualStoreOnlyYnNm);

        //취급여부
        $('input:radio[name=dealYn]:input[value=' + data.dealYn + ']').prop("checked", true);

        //가상점 단독판매여부
        //TODO: 상품상세에 작업 되면 추후 작업
        $("#shipPolicyNo").val(data.shipPolicyNo);
    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
        $("#itemTotalCount").html(itemListGrid.gridView.getItemCount());
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "취급상품_" + _date.getFullYear() + month + date;

        itemListGrid.gridView.exportGrid( {
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
//메인
var main = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.resetSearchForm();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {
        $('#searchBtn').bindClick(main.search);
        $('#excelDownloadBtn').bindClick(main.excelDownload);
        $('#searchResetBtn').bindClick(main.resetSearchForm);
        $('#resetBtn').bindClick(main.clearForm);
        $('#setBtn').bindClick(main.setItem);
        $('#itemPopBtn').bindClick(main.getItemPop);
        $("#itemNo").focusout(function (e) {
            if ($.jUtil.isEmpty($("#originStoreId").text())) {
                return false;
            }
            //상품명 조회
            main.getItem();
            //가상점 단독판매여부 조회
            main.getVirtualStoreOnlyYn();
        });
    },
    /**
     * 상품팝업 호출
     * @param callBack
     */
    getItemPop : function() {
        var callback = "main.itemPopupCallBack";
        var storeType = $('#storeType').val();
        var siteType;

        if (storeType == "CLUB") {
            siteType = "CLUB";
        } else {
            siteType = "HOME";
        }

        if ($.jUtil.isEmpty(storeType)) {
            alert("점포를 선택해주세요.");
            return false;
        }

        var url = "/common/popup/itemPop?callback=" + callback + "&isMulti=N&mallType=TD&siteType=" + siteType + "&storeType=" + storeType;
        var target = "itemPopup";
        var popWidth = "1084";
        var popHeight = "650";

        windowPopupOpen(url, target, popWidth, popHeight);
    }
    ,
    /**
     * 그리드 추가
     */
    addItem : function () {
        if (!main.valid.set()) {
            return false;
        }

        var addDataArr = new Object();
        addDataArr.itemNo = $("#itemNo").val();
        addDataArr.itemNm = $("#itemNm").val();
        addDataArr.dealYn = $('input:radio[name="dealYn"]:checked').val();
        addDataArr.virtualStoreOnlyYn = $('#virtualStoreOnlyYn').val();
        addDataArr.shipPolicyNo = $("#shipPolicyNo").val();
        addDataArr.shipPolicyNm = $("#shipPolicyNo option:checked").text();
        addDataArr.storeId = $("#storeId").val();

        main.appendData(addDataArr);
    },
    /**
     * 취급상품 그리드에 데이터 추가
     * @param data
     */
    appendData : function(value) {
        var currentDatas = itemListGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        if (!currentDatas !== undefined && currentDatas.length > 0) {
            itemListGrid.dataProvider.beginUpdate();
            try {
                if (currentDatas.findIndex(function(currentData) { return currentData.itemNo === value.itemNo; }) == -1) {
                    itemListGrid.dataProvider.addRow(value);
                } else {
                    var options = {
                        startIndex: 0,
                        fields: ['itemNo'],
                        values: [value.itemNo]
                    };

                    var _row = itemListGrid.dataProvider.searchDataRow(options);
                    var rowData = storeListGrid.dataProvider.getJsonRow(_row)
                    if (rowData.dealYn != value.dealYn || rowData.shipPolicyNo != value.shipPolicyNo) {
                        itemListGrid.dataProvider.updateRow(_row, value);
                        itemListGrid.dataProvider.setRowState(_row, 'updated');
                    }
                }
            } finally {
                itemListGrid.dataProvider.endUpdate(true);
            }
        } else {
            itemListGrid.dataProvider.addRow(value);
        }
    }
    ,
    /**
     * 점포등록/수정 그리드 리스트 저장 (deprecated)
     */
    setItemList : function () {
        if (!main.valid.save()) {
            return;
        }

        var storeId = storeListGrid.dataProvider.getJsonRow(storeListGrid.gridView.getCurrent().dataRow).storeId;
        var paramObject = {
            /*createList : [],
            updateList : [],*/
            saveList : []
        };

        // 적용대상 셋팅
        var createdDataList = itemListGrid.dataProvider.getStateRows("created");
        var updatedDataList = itemListGrid.dataProvider.getStateRows("updated");

        createdDataList.forEach(function(_row, index) {
            var _data = itemListGrid.dataProvider.getJsonRow(_row);
            paramObject.saveList.push(_data);
        });
        updatedDataList.forEach(function(_row, index) {
            var _data = itemListGrid.dataProvider.getJsonRow(_row);
            paramObject.saveList.push(_data);
        });

        CommonAjax.basic({
            url: "/item/virtualStoreItem/setVirtualStoreItemList.json",
            method: "post",
            dataType: "json",
            contentType : "application/json; charset=utf-8",
            data: JSON.stringify(paramObject),
            callbackFunc: function (res) {
                alert("저장되었습니다.");
                var rowIndex = storeListGrid.dataProvider.searchDataRow({fields : ["storeId"], values : [storeId]});
                //storeListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                storeListGrid.gridView.onDataCellClicked();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 점포등록/수정 단건 저장
     */
    setItem : function () {
        if(!main.valid.set()){
            return false;
        }

        var params = $('#itemSetForm').serializeArray();

        CommonAjax.basic({
            url: "/item/virtualStoreItem/setVirtualStoreItem.json",
            method: "post",
            data: params,
            callbackFunc: function (res) {
                alert("저장되었습니다.");
                var rowIndex = storeListGrid.dataProvider.searchDataRow({fields : ["storeId"], values : [storeId]});
                storeListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                storeListGrid.gridView.onDataCellClicked();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 택배점 점포 리스트 검색
     */
    search : function () {
        //화면 전체 초기화
        main.resetForm();

        $("#storeTotalCount").html("0");

        var schKeyword = $('#schKeyword').val();
        var schKeywordLength = $('#schKeyword').val().length;

        if (schKeyword != "" && schKeywordLength < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            $('#schKeyword').focus();
            return false;
        }

        CommonAjax.basic({
            url: '/item/virtualStoreItem/getVirtualStoreList.json?' + $('#storeSearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                storeListGrid.setData(res);
                $('#storeTotalCount').html($.jUtil.comma(storeListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색 초기화
     */
    resetSearchForm : function () {
        $("#storeSearchForm").resetForm();
    },
    /**
     * 등록/수정 form 초기화
     */
    resetForm : function() {
        //기본정보
        $("#spanStoreId, #spanStoreNm, #originStoreId, #originStoreNm, #virtualStoreOnlyYnNm").text("");
        $("#itemSetForm").resetForm();
        itemListGrid.dataProvider.clearRows();
        $("#itemTotalCount").html("0");

        //$("#storeTotalCount, #itemTotalCount").html("0");
        //storeListGrid.dataProvider.clearRows();
        //itemListGrid.dataProvider.clearRows();
    },
    /**
     * 등록/수정 form 초기화
     */
    clearForm : function() {
        //기본정보
        $("#itemSetForm").resetForm();

        var storeCurRow  = storeListGrid.dataProvider.getJsonRow(storeListGrid.gridView.getCurrent().dataRow);
        var storeId = storeCurRow.storeId;
        $("#storeType").val(storeCurRow.storeType);
        $("#storeId").val(storeCurRow.storeId);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        itemListGrid.excelDownload();
    },
    /**
     * 상품조회
     * @param type
     */
    getItem : function () {
        var itemNo = $('#itemNo').val();

        // 조회 조건
        if ($.jUtil.isEmpty(itemNo)) {
            return;
        }

        CommonAjax.basic({
            url: '/item/virtualStoreItem/getItemNm.json?',
            data: {itemNo: itemNo},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                $('#itemNm').val(res.data);
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    itemPopupCallBack: function (res) {
        $('#itemNo').val(res[0].itemNo);
        $('#itemNm').val(res[0].itemNm);

        //가상점 단독판매여부 조회
        main.getVirtualStoreOnlyYn();
    },
    /**
     * 본 점포의 택배배송정책 리스트 조회
     * @param storeId
     */
    getShipPolicyList : function(originStoreId) {
        CommonAjax.basic({
            url:"/item/virtualStoreItem/getShipPolicyList.json?storeId="+originStoreId
            , method : "GET"
            , callbackFunc : function(dataList) {
                var selectOptionHtml = "<option value=''>선택해주세요</option>";

                dataList.forEach(function (data) {
                    selectOptionHtml +="<option value=" + data.shipPolicyNo + ">"+data.shipPolicyNm +"</option>";
                }, this);

                $("#shipPolicyNo").html(selectOptionHtml);

            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.errors[0].detail != null) {
                        // api 에러메세지
                        alert(resError.responseJSON.errors[0].detail);
                    }
                }
            }
        });
    },
    /**
     * 가상점 단독판매여부명 조회
     * @param
     */
    getVirtualStoreOnlyYn : function () {
        if ($.jUtil.isEmpty($("#itemNm").val())) {
            $('#virtualStoreOnlyYn').val("N");
            $('#virtualStoreOnlyYnNm').text("설정안함");
        }

        var itemNo = $('#itemNo').val();
        //$('#itemNo').val(res.itemNo);
        var orgStoreId = parseInt($('#originStoreId').text());

        // 조회 조건
        if ($.jUtil.isEmpty(itemNo)) {
            //alert("상품번호를 입력해주세요.");
            //$("#itemNo").focus();
            return;
        }

        CommonAjax.basic({
            url: '/item/virtualStoreItem/getVirtualStoreOnlyYn.json?',
            data: {itemNo: itemNo, storeId: orgStoreId},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                var resName = "";
                if ($.jUtil.isNotEmpty(res.data)) {
                    resName = res.data;
                } else {
                    resName = "N";
                }
                $('#virtualStoreOnlyYn').val(resName);
                $('#virtualStoreOnlyYnNm').text(resName=="Y"?"설정":'설정안함');
            },
            error: function (e) {
                if (e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    }
};

/**
 * 유효성 검사 영역
 * @type {{set: main.valid.set}}
 */
main.valid = {
    set : function () {
        var storeId = $('#storeId').val();

        if ($.jUtil.isEmpty(storeId)) {
            alert('점포를 선택하세요.');
            return false;
        }

        var itemNo = $('#itemNo').val();
        var itemNm = $('#itemNm').val();
        var shipPolicyNo = $('#shipPolicyNo').val();

        if ($.jUtil.isEmpty() && !$.jUtil.isAllowInput(itemNo,['NUM'])) {
            alert("숫자만 사용 가능합니다.");
            $("#itemNo").focus();
            return false;
        }

        if ($.jUtil.isEmpty(itemNo) || $.jUtil.isEmpty(itemNm)) {
            alert('적용상품을 선택하세요.');
            $("#itemNo").focus();
            return false;
        }

        if ($.jUtil.isEmpty(shipPolicyNo)) {
            alert('택배배송정책을 선택하세요.');
            $("#shipPolicyNo").focus();
            return false;
        }

        if ($("#chgYn").val() != "Y") {
            var rowIndex = itemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});
            if (rowIndex > -1) {
                alert("이미 등록된 상품입니다. 등록된 상품의 정보를 수정해주세요.");
                return false;
            }
        }

        return true;
    },
    save : function() {
        if (itemListGrid.gridView.getItemCount() == 0) {
            alert("취급상품을 등록해주세요.");
            return false;
        }
        return true;
    },
    search : function () {
        var schKeyword = $("#schKeyword").val();

        if (!($.jUtil.isEmpty(schKeyword)) && $.trim((schKeyword).length < 2)) {
            alert('검색키워드는 2자 이상 입력해주세요.');
            $("#itemNo").focus();
            return false;
        }

        return true;
    }
}




