/**
 * 상품관리  > 점포상품관리 >  대체불가상품관리
 */
$(document).ready(function() {
    restrictionListGrid.init();
    restriction.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();
});


// 대체불가상품 리스트 그리드
var restrictionListGrid = {
    gridView : new RealGridJS.GridView("restrictionListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        restrictionListGrid.initGrid();
        restrictionListGrid.initDataProvider();
        restrictionListGrid.event();
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    initGrid : function() {
        restrictionListGrid.gridView.setDataSource(restrictionListGrid.dataProvider);

        restrictionListGrid.gridView.setStyles(restrictionListGridBaseInfo.realgrid.styles);
        restrictionListGrid.gridView.setDisplayOptions(restrictionListGridBaseInfo.realgrid.displayOptions);
        restrictionListGrid.gridView.setColumns(restrictionListGridBaseInfo.realgrid.columns);
        restrictionListGrid.gridView.setOptions(restrictionListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        restrictionListGrid.dataProvider.setFields(restrictionListGridBaseInfo.dataProvider.fields);
        restrictionListGrid.dataProvider.setOptions(restrictionListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        restrictionListGrid.gridView.onDataCellClicked = function(gridView, index) {
            restriction.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        restrictionListGrid.dataProvider.clearRows();
        restrictionListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(restrictionListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "대체불가상품관리"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        restrictionListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
// 대체불가상품리스트
var restriction = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initAjaxForm();
        this.initSearchDate('-30d')
    },
    /**
     * 대체불가상품 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#restrictionSetForm').ajaxForm({
            url: '/item/substitute/setRestrictionItem.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                restriction.setrestrictionData($('#restrictionSetForm').serializeObject());
                restriction.resetForm();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 대체불가상품 등록/수정 후 데이터 갱신
     */
    setrestrictionData : function(restriction) {
        var options = {
            fields: ['itemNo'],
            values: [restriction.itemNo]
        };

        var itemIndex = restrictionListGrid.gridView.searchItem(options);

        // 신규 등록 시 오류를 방지하기 위함. ( 그리드에 찾는 데이터가 없을 경우 -1 )
        if(itemIndex != -1) {
            var useYnTxt = '사용안함';
            if (restriction.useYn === 'Y') {
                useYnTxt = '사용';
            }

            // RealGrid 값 변경.
            var setGridValue = {
                useYn: restriction.useYn
                , useYnTxt: useYnTxt
            };

            restrictionListGrid.gridView.setValues(itemIndex, setGridValue, true);
        }
    },
    initSearchDate : function (flag) {
        restriction.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        restriction.setDate.setEndDate(0);
        restriction.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(restriction.search);
        $('#resetBtn').bindClick(restriction.resetForm);
        $('#searchResetBtn').bindClick(restriction.searchFormReset);
        $('#setRestrictionBtn').bindClick(restriction.setRestriction);
        $('#excelDownloadBtn').bindClick(restriction.excelDownload);
        $('#itemNo').focusout(restriction.getItemName);
        $('#addItemPopUp').bindClick(restriction.addItemPopUp);

    },
    /**
     * 대체상품조회팝업
     */
    addItemPopUp : function(){

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=restriction.applyPopupCallback&isMulti=N&mallType=TD&storeType=ALL' , 1084, 650);

    },
    applyPopupCallback : function(resDataArr) {

        $('#itemNo').val(resDataArr[0].itemNo);
        $('#itemNm').val(resDataArr[0].itemNm);
        $('#useYn').val('Y');

    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function () {
        restrictionListGrid.excelDownload();
    },
    /**
     * 상품명입력
     */
    setItemName : function(res){
        $('#itemNm').val(res.returnKey);
    },
    /**
     * 상품명검색
     */
    getItemName : function(){

        var itemNo  = $('#itemNo').val();

        if(!$.jUtil.isEmpty(itemNo)){

            if (!$.jUtil.isAllowInput( itemNo, ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#itemNm").val('');
                $("#itemNo").val('');
                $("#itemNo").focus();
                return;
            } else{
                commonItem.getItemName($('#itemNo').val(), restriction.setItemName);
            }


        }
    },
    /**
     * 대체불가상품 등록/수정
     */
    setRestriction : function() {

        var objectArray = [$('#itemNo') ,$('#itemNm') ];

        if($.jUtil.isEmptyObjectArray(objectArray)) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            return false;
        }

        $('#restrictionSetForm').submit();
    },
    /**
     * 대체불가상품검색
     */
    search : function() {

        if(!restriction.valid.search()){
            return false;

        }
        CommonAjax.basic({url:'/item/substitute/getRestrictionItemList.json?' + $('#restrictionSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            restriction.resetForm();
            restrictionListGrid.setData(res);
            $('#restrictionTotalCount').html($.jUtil.comma(restrictionListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = restrictionListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url : '/item/substitute/getRestrictionItem.json',
            data : { itemNo : rowDataJson.itemNo },
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {
                restriction.setrestrictionData(res);

                $('#itemNo').val(rowDataJson.itemNo);
                $('#itemNm').val(rowDataJson.itemNm1);
                $('#useYn').val(rowDataJson.useYn);
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        restriction.initSearchDate('-30d');
        $('#searchCateCd1').val('').change();
        $('#searchUseYn').val('');
        $('#searchKeyword').val('');
        $('#searchType').val('ITEMNM');

    },
    /**
     * 대체불가상품 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#itemNo').val('');
        $('#itemNm').val('');
        $('#useYn').val('Y');
    },
};

restriction.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
      return true;



    }
};