/**
 * 상품관리  > 홈플러스 상품상세 일괄공지
 */
$(document).ready(function() {
    homeplusBannerListGrid.init();
    homeplusBanner.init();

    commonCategory.init();
    CommonAjaxBlockUI.global();
});

// 홈플러스 상품상세 일괄공지 리스트 그리드
var homeplusBannerListGrid = {

    gridView : new RealGridJS.GridView("homeplusBannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        homeplusBannerListGrid.initGrid();
        homeplusBannerListGrid.initDataProvider();
        homeplusBannerListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

    },
    initGrid : function() {
        homeplusBannerListGrid.gridView.setDataSource(homeplusBannerListGrid.dataProvider);

        homeplusBannerListGrid.gridView.setStyles(homeplusBannerListGridBaseInfo.realgrid.styles);
        homeplusBannerListGrid.gridView.setDisplayOptions(homeplusBannerListGridBaseInfo.realgrid.displayOptions);
        homeplusBannerListGrid.gridView.setColumns(homeplusBannerListGridBaseInfo.realgrid.columns);
        homeplusBannerListGrid.gridView.setOptions(homeplusBannerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        homeplusBannerListGrid.dataProvider.setFields(homeplusBannerListGridBaseInfo.dataProvider.fields);
        homeplusBannerListGrid.dataProvider.setOptions(homeplusBannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        homeplusBannerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            homeplusBanner.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        homeplusBannerListGrid.dataProvider.clearRows();
        homeplusBannerListGrid.dataProvider.setRows(dataList);
    },

};

// 홈플러스 상품상세공지
var homeplusBanner = {
    /**
     * 초기화
     */
    init : function() {

        this.bindingEvent();
        this.initSearchDate('-30d'); //default :30일
        this.initDispDate('+7d');

        homeplusBanner.resetForm();
    },

    /**
     * 검색일 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        homeplusBanner.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        homeplusBanner.setDate.setEndDate(0);
        homeplusBanner.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },

    /**
     * 노출일 초기화
     * @param flag
     */
    initDispDate : function (flag) {

        homeplusBanner.setDateTime = CalendarTime.datePickerRange( 'dispStartDt', 'dispEndDt', {timeFormat:"HH:00:00"}, true, {timeFormat:"HH:59:59"}, true);
        homeplusBanner.setDateTime.setEndDateByTimeStamp(flag);
        homeplusBanner.setDateTime.setStartDate(0);

        $('#dispEndDt').datepicker("option", "minDate", $('#dispStartDt').val());
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(homeplusBanner.search);
        $('#resetFormBtn').bindClick(homeplusBanner.resetForm);
        $('#searchResetBtn').bindClick(homeplusBanner.resetSearchForm);
        $('#setBannerBtn').bindClick(homeplusBanner.setHomepluBanner);

        $("input[name='dispType']").bindChange(homeplusBanner.changeDispType);
        $("input[name='dispYn']").bindChange(homeplusBanner.changeDispYn);

     },

    /**
     * 상품상세공지 수정
     */
    setHomepluBanner: function() {
        // 공통체크
        if(!homeplusBanner.valid.set()){
            return false;
        }

        var form = $('#homeplusBannerSetForm').serializeArray();

        CommonAjax.basic({
            url: '/item/banner/setHomeplusBanner.json',
            data: form,
            method: "POST",
            callbackFunc: function (res) {
                alert(res.returnMsg);
                
                if(!$.jUtil.isEmpty($('#bannerNo').val())) {
                    homeplusBanner.applySetHomeplusGrid(res.returnKey);
                }
                $('#bannerNo').val(res.returnKey);
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 변경된 정보 그리드 반영
     * @param bannerNo
     * @returns {boolean}
     */
    applySetHomeplusGrid : function (bannerNo) {

        if($.jUtil.isEmpty(bannerNo)) {
            homeplusBanner.search();
            return false;
        }

        let form = $('#homeplusBannerSchForm').serializeObject();

        form.schType = 'bannerNo';
        form.schKeyword = bannerNo;

        CommonAjax.basic({
            url: '/item/banner/getHomeplusBannerList.json?',
            data: JSON.stringify(form),
            contentType: "application/json",
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                if(!$.jUtil.isEmpty(res)) {
                    let index = homeplusBannerListGrid.gridView.searchItem({
                        fields: ['bannerNo'],
                        values: [bannerNo]
                    });

                    if (index != -1) {
                        homeplusBannerListGrid.gridView.setValues(index, res[0], true);
                    }
                }
            }
        });

    },
    /**
     * 구분값에 따른 노출 영역 활성/비활성화
     * @param dispType
     */
    changeDispType : function(dispType) {

        if (typeof dispType == 'object') {
            dispType = $(dispType).val();
        }

        switch (dispType) {
            case "ALL" :
                $('#cateRow').css('display', 'none'); //hide
                break;

            case "CATE" :
                $('#cateRow').css('display', ''); //show
                $('#selectCateCd1').val('').change(); //init
                break;
        }
    },
    /**
     * 노출여부에 따른 노출영역 활성/비활성화
     * @param obj
     */
    changeDispYn : function(obj) {

        if ($(obj).val() == "Y") {
            $('#bannerDescRow').css('display', ''); //show
        }
        else {
            $('#bannerDescRow').css('display', 'none'); //hide
        }
    },
    /**
     * 상품상세공지검색
     * @returns {boolean}
     */
    search : function() {

        if(!homeplusBanner.valid.search()){
            return false;
        }

        let form = $('#homeplusBannerSchForm').serializeObject();

        CommonAjax.basic({
            url: '/item/banner/getHomeplusBannerList.json?',
            data: JSON.stringify(form),
            method: "POST",
            contentType: "application/json",
            successMsg: null,
            callbackFunc: function (res) {
                homeplusBannerListGrid.setData(res);
                $('#homeplusBannerTotalCount').html($.jUtil.comma(homeplusBannerListGrid.gridView.getItemCount()));
            }
        });

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = homeplusBannerListGrid.dataProvider.getJsonRow(selectRowId);

        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNo').val(rowDataJson.bannerNo);

        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);

        $('#storeType').val(rowDataJson.storeType);

        $('input:radio[name="dispType"]:input[value="'+rowDataJson.dispType+'"]').trigger('click'); //구분
        $('input:radio[name="dispYn"]:input[value="'+rowDataJson.dispYn+'"]').trigger('click'); //구분

        $('#bannerDesc').val(rowDataJson.bannerDesc)

        if (!$.jUtil.isEmpty(rowDataJson.dcateCd)){
            CommonAjax.basic({
                url: '/common/category/getCategoryDetail.json?depth=4&cateCd=' + rowDataJson.dcateCd,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#selectCateCd1'));
                    commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#selectCateCd2'));
                    commonCategory.setCategorySelectBox(3, res.mcateCd, res.scateCd, $('#selectCateCd3'));
                    commonCategory.setCategorySelectBox(4, res.scateCd, res.dcateCd, $('#selectCateCd4'));

                }});
        }else{
            $('#selectCateCd1').val('').change();
        }

    },
    /**
     * 검색폼 초기화
     */
    resetSearchForm : function() {
        homeplusBanner.initSearchDate('-30d');

        $('#schCateCd1').val('').change();
        $('#schRegNm').val('');
        $('#schDispType').val('');
        $('#schDispYn').val('');
        $('#schDateType').val('REGDT');
    },
    /**
     * 상품상세 입력 폼 초기화
     */
    resetForm : function() {

        $('#bannerNo, #bannerNm').val('');
        $('#bannerDesc').val('');
        $('#storeType').val('HYPER');

        $('input:radio[name="dispType"]:input[value="ALL"]').trigger('click');
        $('input:radio[name="dispYn"]:input[value="Y"]').trigger('click');

        homeplusBanner.initDispDate('+7d');

        $('#dispStartDt').val('');
        $('#dispEndDt').val('');

        $('#cateRow').hide();
        $('#bannerDescRow').show();

        $('#selectCateCd1').val('').change();
    },
};

/**
 * 검증
 * @type {{search: homeplusBanner.valid.search, set: homeplusBanner.valid.set}}
 */
homeplusBanner.valid = {

    search : function () {
        // 날짜 체크
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        return true;

    },
    set : function(){

        //수정, 등록버튼 체크
        var bannerNm = $('#bannerNm').val();
        var bannerDesc = $('#bannerDesc').val();
        var dispType = $('input:radio[name="dispType"]:checked').val();
        var dispYn = $('input:radio[name="dispYn"]:checked').val();

        //제목체크
        if(!$.jUtil.isEmpty(bannerNm)) {
            if (!$.jUtil.isAllowInput(bannerNm, ['ENG', 'NUM', 'KOR'])) {
                alert("특수문자 사용이 불가 합니다.");
                $('#bannerNm').focus();
                return false;
            }
        }else {
            alert("모든 항목은 빠짐없이 입력해주세요 ");
            $('#bannerNm').focus();
            return false;
        }

        if ("CATE" == dispType ){
            if ($.jUtil.isEmpty($('#selectCateCd4').val()))  {
                alert("카테고리 정보를 등록하세요.");
                $('#selectCateCd1').focus();
                return false;
            }
        }

        //공지사항 체크
        if(!$.jUtil.isEmpty(bannerDesc)) {
            if (bannerDesc.length >1000) {
                alert("공지사항은 최대 1000자까지만 입력 가능합니다. ")
                $("#bannerDesc").focus();
                return false;
            }
            if(!$.jUtil.isAllowInput(bannerDesc, ['ENG', 'NUM', 'KOR'])) {
                alert("특수문자 사용이 불가 합니다.");
                $("#bannerDesc").focus();
                return false;
            }

        } else {
            if(dispYn == "Y") {
                alert("모든 항목은 빠짐없이 입력해주세요 ")
                $('#bannerDesc').focus();
                return false;
            }
        }

        //점포유형 체크
        if($.jUtil.isEmpty($('#storeType').val())) {
            alert("점포 유형을 선택해 주세요 ")
            $('#storeType').focus();
            return false;
        }

        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("노출기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("노출 기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-364, "day").format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("노출기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(moment(endDt.val()).format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        return true;
    }
};