/**
 * 회원관리 > 고객문의관리 > 상품평관리
 */

$(document).ready(function() {
    itemListGrid.init();
    itemStoreListGrid.init();
    item.init();
    CommonAjaxBlockUI.global();
});

// itemReview 그리드
var itemListGrid = {
    gridView : new RealGridJS.GridView("itemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemListGrid.initGrid();
        itemListGrid.initDataProvider();
        itemListGrid.event();
    },
    initGrid : function() {
        itemListGrid.gridView.setDataSource(itemListGrid.dataProvider);
        itemListGrid.gridView.setStyles(itemGridBaseInfo.realgrid.styles);
        itemListGrid.gridView.setDisplayOptions(itemGridBaseInfo.realgrid.displayOptions);
        itemListGrid.gridView.setColumns(itemGridBaseInfo.realgrid.columns);
        itemListGrid.gridView.setOptions(itemGridBaseInfo.realgrid.options);
        itemListGrid.gridView.setColumnProperty("histBtn", "renderer", {type: "link", url: "/item/popup/priceChangeHistPop", requiredFields: "histBtn", showUrl: false});
    },
    initDataProvider : function() {
        itemListGrid.dataProvider.setFields(itemGridBaseInfo.dataProvider.fields);
        itemListGrid.dataProvider.setOptions(itemGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            item.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        itemListGrid.dataProvider.clearRows();
        itemListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(itemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        const _date = new Date();
        const fileName = "priceChangeHist"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemListGrid.gridView.exportGrid({
            type : "excel",
            target : "local",
            fileName : fileName + ".xlsx",
            showProgress : true,
            progressMessage : "엑셀 데이터 추줄 중입니다."
        });
    }
};

// itemReview 그리드
var itemStoreListGrid = {
    gridView : new RealGridJS.GridView("itemStoreListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        itemStoreListGrid.initGrid();
        itemStoreListGrid.initDataProvider();
        itemStoreListGrid.event();
    },
    initGrid : function() {
        itemStoreListGrid.gridView.setDataSource(itemStoreListGrid.dataProvider);
        itemStoreListGrid.gridView.setStyles(itemStoreGridBaseInfo.realgrid.styles);
        itemStoreListGrid.gridView.setDisplayOptions(itemStoreGridBaseInfo.realgrid.displayOptions);
        itemStoreListGrid.gridView.setColumns(itemStoreGridBaseInfo.realgrid.columns);
        itemStoreListGrid.gridView.setOptions(itemStoreGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        itemStoreListGrid.dataProvider.setFields(itemStoreGridBaseInfo.dataProvider.fields);
        itemStoreListGrid.dataProvider.setOptions(itemStoreGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        itemStoreListGrid.gridView.onDataCellClicked = function(gridView, index) {
            item.gridRowDetailSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        itemStoreListGrid.dataProvider.clearRows();
        itemStoreListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(itemStoreListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        const _date = new Date();
        const fileName = "priceChangeHist"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemStoreListGrid.gridView.exportGrid({
            type : "excel",
            target : "local",
            fileName : fileName + ".xlsx",
            showProgress : true,
            progressMessage : "엑셀 데이터 추줄 중입니다."
        });
    }
};

// itemReview 관리
var item = {
    selectedItemNo : ''
    , selectedItemNm : ''
    , selectedGridNo : ''
    , arrPriceChangeHist : null
    /**
     * 초기화
     */
    , init : function() {
        this.initAjaxForm();
        this.bindingEvent();
    },
    /**
     * itemReview 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#itemReviewSetForm').ajaxForm({
            url: '/',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                item.searchReset();
                item.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(item.search); // 검색
        $('#searchResetBtn').bindClick(item.searchFormReset); // 검색폼 초기화
        $('#setBtn').bindClick(item.set); // 등록,수정
        $('#resetBtn').bindClick(item.reset); // 입력폼 초기화
        $('#updateSalePrice').bindClick(item.setList); // 입력폼 초기화
        $("input:radio[name=storeType]").bindChange(item.resetData);

        // 이력보기
        itemListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = $("input:radio[name=storeType]:checked").val();

            $.jUtil.openNewPopup(url + "?itemNo=" + itemNo + "&storeType=" + storeType, 1120, 540);
        };
    },
    gridRowSelect: function (selectRowId) {
        item.selectedGridNo = selectRowId;
        var rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);

        var itemNo = rowDataJson.itemNo;
        var itemNm = rowDataJson.itemNm;
        var storeType = $("input:radio[name=storeType]:checked").val();

        item.selectedItemNo = itemNo;
        item.selectedItemNm = itemNm;

        item.reset();

        CommonAjax.basic({
            url : '/item/priceChangeHist/getItemStoreList.json?itemNo='+itemNo+"&storeType="+storeType
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                itemStoreListGrid.setData(res);
                $('#itemStoreListTotalCount').html($.jUtil.comma(itemStoreListGrid.gridView.getItemCount()));
            }
        });
    },
    gridRowDetailSelect: function (selectRowId) {
        var rowDataJson = itemStoreListGrid.dataProvider.getJsonRow(selectRowId);

        $('#setOriginPrice').val(rowDataJson.salePrice);
        $('#dispItemNo').val(item.selectedItemNo);
        $('#dispItemNm').val(item.selectedItemNm);
        $('#dispStoreId').val(rowDataJson.storeId);
        $('#dispStoreNm').val(rowDataJson.storeNm);
        $('#setSalePrice').val(rowDataJson.salePrice);
    },
    /**
     * itemReview 검색
     */
    search : function() {
        if(!item.valid.search()) {
            return false;
        }

        if ($('#schType').val() == 'itemNo') {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ','));
        } else {
            $('#schKeyword').val($('#schKeyword').val().replace(/(?:\r\n|\r|\n)/g, ''));
        }

        CommonAjax.basic({
            url : '/item/priceChangeHist/getItemList.json?' + $('#itemSearchForm').serialize(),
            data : null,
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {
                item.reset();
                itemListGrid.setData(res);
                $('#itemListTotalCount').html($.jUtil.comma(itemListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchCateCd1').val('').change();
        $('#searchCateCd').val('');
        $('#schType').val('itemNo');
        $('#schKeyword').val('');
        $('input:radio[name="storeType"]:input[value=HYPER]').trigger('click');
    },
    /**
     * safetyStock 등록/수정
     */
    set : function() {
        if(!item.valid.set()) {
            return false;
        }

        item.arrPriceChangeHist = new Array();
        var requestData = {
            itemNo : $('#dispItemNo').val()
            , salePrice : $('#setSalePrice').val()
            , originPrice : $('#setOriginPrice').val()
            , storeId : $('#dispStoreId').val()
        };

        item.arrPriceChangeHist.push(requestData);

        CommonAjax.basic({
            url : '/item/priceChangeHist/setPriceChangeHist.json',
            data : JSON.stringify(item.arrPriceChangeHist),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                item.reset();
                item.gridRowSelect(item.selectedGridNo);
            }
        });
    },
    /**
     * safetyStock 등록/수정
     */
    setList : function() {
        var checkedRows = itemStoreListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("일괄적용할 점포를 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#setSalePriceList').val())) {
            alert("일괄적용할 판매가를 입력해 주세요.");
            $('#setSalePriceList').val('');
            $('#setSalePriceList').focus();
            return false;
        }
        if (!$.jUtil.isAllowInput( $('#setSalePriceList').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#setSalePriceList").val('');
            $("#setSalePriceList").focus();
            return;
        }

        item.arrPriceChangeHist = new Array();

        checkedRows.forEach(function(rowIds){
            var rowDataJson = itemStoreListGrid.dataProvider.getJsonRow(rowIds)
            var requestData = {
                itemNo : item.selectedItemNo
                , salePrice : $('#setSalePriceList').val()
                , originPrice : rowDataJson.salePrice
                , storeId : rowDataJson.storeId
            }
            item.arrPriceChangeHist.push(requestData)
        });

        CommonAjax.basic({
            url : '/item/priceChangeHist/setPriceChangeHist.json',
            data : JSON.stringify(item.arrPriceChangeHist),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                item.reset();
                item.gridRowSelect(item.selectedGridNo);
            }
        });
    },
    /**
     * 입력/수정 폼 초기화
     */
    reset : function() {
        $('#dispItemNo, #dispItemNm, #dispStoreId, #dispStoreNm, #setSalePrice, #setOriginPrice, #setSalePriceList').val('');
    },
    resetData : function() {
        item.reset();
        itemListGrid.dataProvider.clearRows();
        $('#itemListTotalCount').html('0');
        itemStoreListGrid.dataProvider.clearRows();
        $('#itemStoreListTotalCount').html('0');
    }
};

/**
 * safetyStock Validation Check
 */
item.valid = {
    search : function () {
        var schKeyword = $('#schKeyword').val();
        // 검색어 자릿수 확인
        if (schKeyword.length < 2) {
            return $.jUtil.alert("검색 키워드는 2자 이상 입력해주세요.", 'schKeyword');
        }
        if (schKeyword.split(',').length > 20) {
            return $.jUtil.alert("검색키워드는 최대 20개 까지 등록 가능합니다.", 'schKeyword');
        }
        return true;
    },
    set : function () {
        if($.jUtil.isEmpty($('#dispItemNo').val()) || $.jUtil.isEmpty($('#dispItemNm').val()) || $.jUtil.isEmpty($('#dispStoreId').val())
        || $.jUtil.isEmpty($('#dispStoreNm').val()) || $.jUtil.isEmpty($('#setOriginPrice').val())) {
            alert("변경 하실 점포를 선택해 주세요.");
            return;
        }
        if (!$.jUtil.isAllowInput( $('#setSalePrice').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#setSalePrice").val('');
            $("#setSalePrice").focus();
            return;
        }
        return true;
    }
};