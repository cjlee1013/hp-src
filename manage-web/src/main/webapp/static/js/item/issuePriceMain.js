/**
 * 상품관리 > 점포상품관리 > 이상매가조회
 */
$(document).ready(function() {
    issuePriceGrid.init();
    issuePriceSearch.init();
    CommonAjaxBlockUI.global();

});

// 이상매가 그리드
var issuePriceGrid = {

    gridView : new RealGridJS.GridView("issuePriceGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        issuePriceGrid.initGrid();
        issuePriceGrid.initDataProvider();
    },
    initGrid : function () {
        issuePriceGrid.gridView.setDataSource(issuePriceGrid.dataProvider);

        issuePriceGrid.gridView.setStyles(issuePriceGridBaseInfo.realgrid.styles);
        issuePriceGrid.gridView.setDisplayOptions(issuePriceGridBaseInfo.realgrid.displayOptions);
        issuePriceGrid.gridView.setColumns(issuePriceGridBaseInfo.realgrid.columns);
        issuePriceGrid.gridView.setOptions(issuePriceGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        issuePriceGrid.dataProvider.setFields(issuePriceGridBaseInfo.dataProvider.fields);
        issuePriceGrid.dataProvider.setOptions(issuePriceGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        issuePriceGrid.dataProvider.clearRows();
        issuePriceGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(issuePriceGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "이상매가_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        issuePriceGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

var issuePriceSearch = {
    init : function () {
        issuePriceSearch.searchFormReset();
        issuePriceSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(issuePriceSearch.searchIssuePriceList);
        $("#excelDownBtn").bindClick(issuePriceGrid.excelDownload);
    }
    /**
     * 이상매가 현황 조회
     */
    , searchIssuePriceList : function () {
        var isChecked = $("input:radio[name='schStoreType']:checked").val();

        if (!$.jUtil.isEmpty(isChecked)) {
            // 기획전 정보 조회 개수 초기화
            $("#issudPriceTotalCnt").html("0");

            // 조회
            CommonAjax.basic({
                url : "/item/issuePrice/getIssuePriceList.json"
                , data : {schStoreType : isChecked}
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    issuePriceGrid.setData(res);

                    $("#issudPriceTotalCnt").html($.jUtil.comma(issuePriceGrid.dataProvider.getRowCount()));
                }
            });
        } else {
            return $.jUtil.alert("점포유형을 선택하세요");
        }
    }
};
