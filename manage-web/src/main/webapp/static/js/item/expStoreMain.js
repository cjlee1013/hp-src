/**
 * 상품관리>점포관리>EXPRESS점포주문시간 관리
 */
$(document).ready(function() {
    expStoreListGrid.init();
    expStoreHistTotalListPopGrid.init();

    expStore.init();
    CommonAjaxBlockUI.global();

});

//EXPRESS 점포 주문시간 관리
var expStoreListGrid = {

    gridView : new RealGridJS.GridView("expStoreListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        expStoreListGrid.initGrid();
        expStoreListGrid.initDataProvider();
        expStoreListGrid.event();
    },
    initGrid : function () {
        expStoreListGrid.gridView.setDataSource(expStoreListGrid.dataProvider);

        expStoreListGrid.gridView.setStyles(expStoreListGridBaseInfo.realgrid.styles);
        expStoreListGrid.gridView.setDisplayOptions(expStoreListGridBaseInfo.realgrid.displayOptions);
        expStoreListGrid.gridView.setColumns(expStoreListGridBaseInfo.realgrid.columns);
        expStoreListGrid.gridView.setOptions(expStoreListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        expStoreListGrid.dataProvider.setFields(expStoreListGridBaseInfo.dataProvider.fields);
        expStoreListGrid.dataProvider.setOptions(expStoreListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        expStoreListGrid.gridView.onDataCellClicked = function(gridView, index) {
            expStore.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        expStoreListGrid.dataProvider.clearRows();
        expStoreListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        //조회 후 다운

        if(expStoreListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "EXPRESS_점포주문시간관리_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        expStoreListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

// total gid (excel down)
var expStoreHistTotalListPopGrid = {
    gridView : new RealGridJS.GridView("expStoreHistTotalListPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        expStoreHistTotalListPopGrid.initGrid();
        expStoreHistTotalListPopGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정
     */
    initGrid : function() {
        expStoreHistTotalListPopGrid.gridView.setDataSource(expStoreHistTotalListPopGrid.dataProvider);
        expStoreHistTotalListPopGrid.gridView.setStyles(expStoreHistTotalListPopGridBaseInfo.realgrid.styles);
        expStoreHistTotalListPopGrid.gridView.setDisplayOptions(expStoreHistTotalListPopGridBaseInfo.realgrid.displayOptions);
        expStoreHistTotalListPopGrid.gridView.setColumns(expStoreHistTotalListPopGridBaseInfo.realgrid.columns);
        expStoreHistTotalListPopGrid.gridView.setOptions(expStoreHistTotalListPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        expStoreHistTotalListPopGrid.dataProvider.setFields(expStoreHistTotalListPopGridBaseInfo.dataProvider.fields);
        expStoreHistTotalListPopGrid.dataProvider.setOptions(expStoreHistTotalListPopGridBaseInfo.dataProvider.options);
    },

    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        expStoreHistTotalListPopGrid.dataProvider.clearRows();
        expStoreHistTotalListPopGrid.dataProvider.setRows(dataList);
    },

    excelDownload: function() {

        if(expStoreHistTotalListPopGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "EXPRESS점포_변경이력_전체_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        expStoreHistTotalListPopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// EXPRESS 점포 주문시간 관리
var expStore = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {
        $('#searchBtn').bindClick(expStore.search);
        $('#excelDownloadBtn').bindClick(expStore.excelDownload);
        $('#searchResetBtn').bindClick(expStore.resetSearchForm);

        $('#resetBtn').bindClick(expStore.resetForm);
        $('#setStoreBtn').bindClick(expStore.setExpStore);

        $('#orderAvailYn').on("change", function () {
            expStore.changeOrderAvailYn($(this).val());
        });

        $('#stopReasonCd').on("change", function () {
            $(this).val() == 'ETC1' ? $('#stopReasonTxt').val('').show() : $('#stopReasonTxt').val('').hide();
        });

        $('#deliveryTime').allowInput("keyup", ["NUM"]);


        $('#expStoreHist').on('click', function() {
            if (!$.jUtil.isEmpty($('#storeId').val())) {
                windowPopupOpen('/common/popup/expStoreHistPop?storeId=' + $('#storeId').val(), "expStoreHistPop", 1200 ,600);
            }else {
                alert("점포를 선택해주세요.");
            }
        });

        $('#histExcelDownloadBtn').bindClick(expStore.histDownload);

    },
    /**
     * 주문가능여부 변경 시 동작
     * @param val
     */
    changeOrderAvailYn : function (val){
        //주문불가사유 영역 초기화
        $('#stopReasonCd').val('').prop('disabled', true);
        $('#stopReasonTxt').hide();

        if( val == 'Y') {
            $('.timeArea').prop('disabled', false);
        } else{
            $('.timeArea').prop('disabled', true)
            $('#orderStartHour, #orderStartMinute, #orderEndHour, #orderEndMinute, #deliveryTime').val('');

            if (val =='N') {
                //주문불가일 경우
                $('#stopReasonCd').prop('disabled', false);
            }
        }
    },
    /**
     * 등록/수정
     */
    setExpStore : function () {

        if(!expStore.valid.set()){
            return false;
        }

        var form = $('#expStoreSetForm').serializeArray();

        CommonAjax.basic({
            url: "/item/store/setExpStore.json",
            method: "post",
            data: form,
            callbackFunc: function (res) {
                alert(res.returnMsg);
                if(!$.jUtil.isEmpty(res.returnKey)){
                    expStore.applySetSearchGrid(res.returnKey);
                }
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    applySetSearchGrid : function(storeId) {

        if( $.jUtil.isEmpty(storeId) ){
            //정상적인 동작이 아니므로
            expStore.search();
            return false;
        }

        let data = $('#expStoreSearchForm').serializeArray();

        if(storeId) {
            data = [ {name: "searchType", value: "storeId"}, {name: "searchKeyword", value: storeId}];
        }

        CommonAjax.basic({
            url: '/item/store/getExpStoreList.json',
            data: data,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                let index = expStoreListGrid.gridView.searchItem({
                    fields: ['storeId'],
                    values: [storeId]
                });

                if(index != -1) {
                   expStoreListGrid.gridView.setValues(index, res[0], true);
                }
            }
        });
    },
    /**
     * 조회
     * @returns {boolean}
     */
    search : function() {

        let data = $('#expStoreSearchForm').serializeArray();

        let searchKeyword = $('#searchKeyword').val();
        let searchType = $('#searchType').val();
        let searchKeywordLength = $('#searchKeyword').val().length;

        if (searchKeyword != "" && searchType != "storeId") {
        if (searchKeywordLength < 2) {
                alert("검색 키워드는 2자 이상 입력해주세요.");
                $('#searchKeyword').focus();
                return false;
            }
        }

        CommonAjax.basic({
            url: '/item/store/getExpStoreList.json',
            data: data,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                expStoreListGrid.setData(res);
                $('#expStoreTotalCount').html($.jUtil.comma(
                    expStoreListGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 검색 폼 초기화
     */
    resetSearchForm : function () {
        $('#searchType').val('storeNm');
        $('#searchKeyword').val('');
        $('#searchOrderAvailYn').val('');
    },
    /**
     * 등록/수정 form 초기화
     */
    resetForm : function() {

        $('#storeId, #storeNm, #orderAvailYn, #storeInfo, #stopReasonCd').val('');
        $('#orderStartHour, #orderStartMinute, #orderEndHour, #orderEndMinute, #orderAvailYn, #storeInfo').val('');

        $('#useYn').val('');
        $('.timeArea').prop('disabled', false);

        $('#orderAvailYn').trigger('change');
        $('#expStoreHist').hide();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        expStoreListGrid.excelDownload();
    },
    /**
     * 이력다운
     */
    histDownload : function () {
        //조회
        CommonAjax.basic({
            url: '/item/store/getExpStoreHistTotalList.json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                expStoreHistTotalListPopGrid.setData(res);
                expStoreHistTotalListPopGrid.excelDownload();
            },
            error: function(err) {
                alert('잠시 뒤에 시도해주세요.');
                return;
            }
        });
    },
    /**
     * selected grid
     * @param selectRowId
     */
    gridRowSelect :  function (selectRowId) {
        expStore.resetForm();

        let rowDataJson = expStoreListGrid.dataProvider.getJsonRow(selectRowId);

        $('#useYn').val(rowDataJson.useYn);
        $('#storeId').val(rowDataJson.storeId);
        $('#storeNm').val(rowDataJson.storeNm);
        $('#orderAvailYn').val(rowDataJson.orderAvailYn).trigger('change');

        //주문 불가일 경우.
        if(rowDataJson.orderAvailYn =='N') {
            $('#stopReasonCd').val(rowDataJson.stopReasonCd).trigger('change');
            $('#stopReasonTxt').val($.jUtil.isEmpty(rowDataJson.stopReasonTxt) ? '' : rowDataJson.stopReasonTxt );
        }

        $('#orderStartHour').val(rowDataJson.orderStartHour);
        $('#orderStartMinute').val(rowDataJson.orderStartMinute);
        $('#orderEndHour').val(rowDataJson.orderEndHour);
        $('#orderEndMinute').val(rowDataJson.orderEndMinute);
        $('#deliveryTime').val(rowDataJson.deliveryTime);

        $('#expStoreHist').show();

    },
};

expStore.valid = {
    set : function () {

        if ( $('#useYn').val() == 'N') {
            return $.jUtil.alert('사용여부가 사용안함인 점포는 운영시간 설정이 불가합니다.');
        }

        if ($.jUtil.isEmpty( $('#storeId').val()) || $.jUtil.isEmpty( $('#storeNm').val()) ){
            return $.jUtil.alert('추가정보를 등록할 점포를 먼저 선택해주세요.');
        }

        let orderAvailYn = $('#orderAvailYn').val();

        if ($.jUtil.isEmpty(orderAvailYn)) {
            return $.jUtil.alert('주문가능 여부를 선택해주세요.', 'orderAvailYn');
        }

        if( orderAvailYn == 'Y') {

            if ($.jUtil.isEmpty($('#orderStartHour').val())) {
                return $.jUtil.alert('주문가능 시간을 설정해주세요.', 'orderStartHour');
            }

            if ($.jUtil.isEmpty($('#orderStartMinute').val())) {
                return $.jUtil.alert('주문가능 시간을 설정해주세요.', 'orderStartMinute');
            }

            if ($.jUtil.isEmpty($('#orderEndHour').val())) {
                return $.jUtil.alert('주문가능 시간을 설정해주세요.', 'orderEndHour');
            }

            if ($.jUtil.isEmpty($('#orderEndMinute').val())) {
                return $.jUtil.alert('주문가능 시간을 설정해주세요.', 'orderEndMinute');
            }

            if($('#orderStartHour').val()+$('#orderStartMinute').val() == $('#orderEndHour').val()+$('#orderEndMinute').val()) {
                return $.jUtil.alert('주문가능 시간을 다시 확인해주세요.', 'orderStartHour');
            }

            let startTime = moment(($('#orderStartHour').val()+":"+$('#orderStartMinute').val()), 'HH:mm');
            let endTime = moment(($('#orderEndHour').val()+":"+$('#orderEndMinute').val()), 'HH:mm');

            if(moment(endTime).isBefore(startTime) == true) {
                return $.jUtil.alert('주문가능 시간을 다시 확인해주세요.', 'orderStartHour');
            }

            if ($.jUtil.isEmpty($('#deliveryTime').val())) {
                return $.jUtil.alert('배달소요시간을 입력해주세요.', 'deliveryTime');
            }
        } else if (orderAvailYn == 'N') {
            //주문불가
            if ($.jUtil.isEmpty($('#stopReasonCd').val())) {
                return $.jUtil.alert('주문불가 사유를 선택해주세요.', 'stopReasonCd');
            }

            //기타(직접입력일 경우)
            if ($('#stopReasonCd').val() =='ETC1' && $.jUtil.isEmpty($('#stopReasonTxt').val())){
                return $.jUtil.alert('주문불가 사유를 입력해주세요.', 'stopReasonTxt');
            }
        }

        return true;
    }
}





