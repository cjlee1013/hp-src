/**
 * 상품관리 > 마켓연동상품관리 > 외부연동현황관리 NEW
 */

$(document).ready(function() {
    CommonAjaxBlockUI.global();
    marketItemFailListGrid.init();
    marketItemFail.init();
});

//외부연동현황관리
var marketItemFailListGrid = {

    gridView : new RealGridJS.GridView("marketItemFailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        marketItemFailListGrid.initGrid();
        marketItemFailListGrid.initDataProvider();
        marketItemFailListGrid.event();

    },
    initGrid : function() {
        marketItemFailListGrid.gridView.setDataSource(marketItemFailListGrid.dataProvider);
        marketItemFailListGrid.gridView.setStyles(marketItemFailListGridBaseInfo.realgrid.styles);
        marketItemFailListGrid.gridView.setDisplayOptions(marketItemFailListGridBaseInfo.realgrid.displayOptions);
        marketItemFailListGrid.gridView.setColumns(marketItemFailListGridBaseInfo.realgrid.columns);
        marketItemFailListGrid.gridView.setOptions(marketItemFailListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        marketItemFailListGrid.dataProvider.setFields(marketItemFailListGridBaseInfo.dataProvider.fields);
        marketItemFailListGrid.dataProvider.setOptions(marketItemFailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        marketItemFailListGrid.dataProvider.clearRows();
        marketItemFailListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketItemFailListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "연동실패_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        marketItemFailListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }

};

//연동실패
var marketItemFail = {
    /**
     * 초기화
     */
    init : function() {
        marketItemFail.bindingEvent();
        marketItemFail.initSearchDate('-30d');
    },

    /**
     * bind event
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(marketItemFail.search);
        $('#searchResetBtn').bindClick(marketItemFail.resetSearchForm);
        $('#excelDownloadBtn').bindClick(marketItemFail.excelDownload);
        $('#btn_helpPop').bindClick(marketItemFail.helpPop);

    },
    initSearchDate : function (flag) {
        marketItemFail.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        marketItemFail.setDate.setEndDate(0);
        marketItemFail.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 조회
     * @returns {boolean}
     */
    search : function() {

        var schKeyword = $("#schKeyword").val();
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");

        if (!($.jUtil.isEmpty(schKeyword))) {
            if( schKeyword.length < 2 ) {
                return $.jUtil.alert('검색키워드는 2자 이상 입력해주세요.', 'itemNo');
            }
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            marketItemFail.initSearchDate('-30d');
            return false;
        }

        var form =  $('#marketItemFailSearchForm').serializeObject();

        CommonAjax.basic({
            url: '/item/market/getMarketItemFailList.json',
            data: JSON.stringify(form),
            contentType: "application/json",
            method: "POST",
            successMsg: null,
                callbackFunc: function (res) {
                marketItemFailListGrid.setData(res);
                $('#marketItemFailTotalCount').html($.jUtil.comma(marketItemFailListGrid.gridView.getItemCount()));
            }
        });

    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        marketItemFailListGrid.excelDownload();
    },

    /**
     * 검색항목 초기화
     */
    resetSearchForm : function() {

        $('#schPartner').val('');
        $('#schType').val("itemNo");
        $('#schKeyword').val('');
        $("#failReason").val("");
        marketItemFail.initSearchDate('-30d');

    },
    /**
     * 도움말
     */
    helpPop : function(){
        $.jUtil.openNewPopup('/item/market/popup/marketItemFailHelpPop', 1084, 650);
    }

};
