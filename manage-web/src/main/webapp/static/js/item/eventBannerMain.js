/**
 * 상품관리  > 상품상세 일괄공지
 */
$(document).ready(function() {
    eventBannerListGrid.init();
    bannerDetailListGrid.init();

    eventBanner.init();

    commonCategory.init();
    CommonAjaxBlockUI.global();
});

// 상품상세 일괄공지 리스트 그리드
var eventBannerListGrid = {

    gridView : new RealGridJS.GridView("eventBannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        eventBannerListGrid.initGrid();
        eventBannerListGrid.initDataProvider();
        eventBannerListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

    },
    initGrid : function() {
        eventBannerListGrid.gridView.setDataSource(eventBannerListGrid.dataProvider);
        eventBannerListGrid.gridView.setStyles(eventBannerListGridBaseInfo.realgrid.styles);
        eventBannerListGrid.gridView.setDisplayOptions(eventBannerListGridBaseInfo.realgrid.displayOptions);
        eventBannerListGrid.gridView.setColumns(eventBannerListGridBaseInfo.realgrid.columns);
        eventBannerListGrid.gridView.setOptions(eventBannerListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        eventBannerListGrid.dataProvider.setFields(eventBannerListGridBaseInfo.dataProvider.fields);
        eventBannerListGrid.dataProvider.setOptions(eventBannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        eventBannerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            eventBanner.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        eventBannerListGrid.dataProvider.clearRows();
        eventBannerListGrid.dataProvider.setRows(dataList);
    },

};

// 상품상세 일괄공지 리스트 그리드
var bannerDetailListGrid = {

    gridView : new RealGridJS.GridView("bannerDetailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        bannerDetailListGrid.initGrid();
        bannerDetailListGrid.initDataProvider();
        bannerDetailListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

    },
    initGrid : function() {
        bannerDetailListGrid.gridView.setDataSource(bannerDetailListGrid.dataProvider);

        bannerDetailListGrid.gridView.setStyles(bannerDetailListGridBaseInfo.realgrid.styles);
        bannerDetailListGrid.gridView.setDisplayOptions(bannerDetailListGridBaseInfo.realgrid.displayOptions);
        bannerDetailListGrid.gridView.setColumns(bannerDetailListGridBaseInfo.realgrid.columns);
        bannerDetailListGrid.gridView.setOptions(bannerDetailListGridBaseInfo.realgrid.options);

        bannerDetailListGrid.gridView.setColumnProperty("imgNm", "renderer", {type: "link", url: "unnecessary", requiredFields: "imgNm", showUrl: false});
    },
    initDataProvider : function() {
        bannerDetailListGrid.dataProvider.setFields(bannerDetailListGridBaseInfo.dataProvider.fields);
        bannerDetailListGrid.dataProvider.setOptions(bannerDetailListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        bannerDetailListGrid.gridView.onDataCellClicked = function(gridView, index) {
            eventBanner.gridRowDetailSelect(index);
        };

        bannerDetailListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            eventBanner.gridRowDetailSelect(index);
            eventBanner.img.imagePreview('#imgBanner');
        }

    },
    setData : function(dataList) {
        bannerDetailListGrid.dataProvider.clearRows();
        bannerDetailListGrid.dataProvider.setRows(dataList);
    },

};

// 이벤트/마케팅 상품상세공지
var eventBanner = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate("-30");
        this.initDispDate("0");

        eventBanner.resetEventBannerForm();
    },
    /**
     * 검색일 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        eventBanner.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        eventBanner.setDate.setEndDate(0);
        eventBanner.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },

    /**
     * 노출일 초기화
     * @param flag
     */
    initDispDate : function (flag) {
        eventBanner.setDateTime = CalendarTime.datePickerRange( 'dispStartDt', 'dispEndDt', {timeFormat:"HH:00:00"}, true, {timeFormat:"HH:59:59"}, true);
        eventBanner.setDateTime.setEndDateByTimeStamp(flag);
        eventBanner.setDateTime.setStartDate(0);

        $('#dispEndDt').datepicker("option", "minDate", $('#dispStartDt').val());
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        //검색
        $('#searchBtn').bindClick(eventBanner.search);
        $('#searchResetBtn').bindClick(eventBanner.resetSearchForm);

        //저장
        $('#resetBannerBtn').bindClick(eventBanner.resetEventBannerForm);
        $('#setBannerBtn').bindClick(eventBanner.setEventBanner);

        //팝업
        $('#schPartnerPopBtn').bindClick(eventBanner.addPartnerPopup);
        $('#schItemPopBtn').bindClick(eventBanner.addItemPopUp);
        $('#schPromoPopBtn' ).bindClick(eventBanner.addPromoPopup);

        //디테일
        $('#addDetailBtn').bindClick(eventBanner.addBannerDetailRow);
        $('#saveDetailBtn').bindClick(eventBanner.updateBannerDetailRow);
        $('#delDetailBtn').bindClick(eventBanner.deleteBannerDetailRow);
        $('#resetDetailForm').bindClick(eventBanner.resetDetailForm);

        //변경
        $('#linkType').on('change', function() {
            eventBanner.changeLinkType($(this).val());
        });

        $('#dispType').on('change', function() {
            eventBanner.changeDispType($(this).val());
        });

        $('#storeType').on('change', function() {
            eventBanner.dispTypeOptReset(null);
        });

        //이미지
        eventBanner.img.init();
        //파일선택시
        $('input[name="fileArr"]').change(function() {
            eventBanner.img.addImg();
        });

        $('.uploadType').bindClick(eventBanner.img.imagePreview);

    },
    /**
     * Reset disp_type Option
     */
    dispTypeOptReset : function(value) {

        let storeType = $('#storeType').val();
        $("select#dispType option").remove();

        for (let i in dispTypeJson) {
            if(storeType != 'DS') {
                //TD (HYPER. EXP)
                if (dispTypeJson[i].ref2 == 'TD') {
                    $("select#dispType").append(
                        "<option value=" + dispTypeJson[i].mcCd + ">"
                        + dispTypeJson[i].mcNm + "</option>");
                }

            }else {
                $("select#dispType").append(
                    "<option value=" + dispTypeJson[i].mcCd + ">"
                    + dispTypeJson[i].mcNm + "</option>");
            }
        }

        if(!$.jUtil.isEmpty(value)) {
            $('#dispType').val(value).trigger('change');
        }else {
            //기본값 셋팅
            $('#dispType').val('ITEM').trigger('change');
        }
    },

    /**
     * 상품상세공지 등록/수정
     */
    setEventBanner: function() {

        if(!eventBanner.valid.set()){
            return false;
        }

        let itemList = new Array();
        let size = $('#items').find("li").length;

        //상품별 > 등록상품
        for ( var idx=0; idx < size; idx++){
            var data = new Object();
            data.itemNo = $('#items').find('li').eq(idx).attr('id');
            data.useYn = "Y";
            itemList.push(data);
        }

        var form = $('#eventBannerSetForm').serializeObject();

        //상품상세 디테일
        form.detailList = bannerDetailListGrid.dataProvider.getJsonRows();
        form.itemList = itemList;

        CommonAjax.basic({
            url: '/item/banner/setEventBanner.json',
            data: JSON.stringify(form),
            type: 'post',
            contentType: 'application/json',
            method:"POST",
            successMsg:null,
            callbackFunc:function(res) {
                alert(res.returnMsg);
                if (!$.jUtil.isEmpty($('#bannerNo').val())) {
                    eventBanner.applySetEventGrid(res.returnKey);
                }

                $('#bannerNo').val(res.returnKey);
            },

        });

    },
    /**
     * 변경된 정보 그리드 반영
     * @param bannerNo
     */
    applySetEventGrid : function (bannerNo) {

        if( $.jUtil.isEmpty(bannerNo) ){
            //정상적인 동작이 아니므로
            eventBanner.search();
            return false;
        }

        let form =  $('#eventBannerSearchForm').serializeObject();

        form.schType = 'bannerNo';
        form.schKeyword = bannerNo;

        CommonAjax.basic({
            url: '/item/banner/getEventBannerList.json',
            data: JSON.stringify(form),
            contentType: "application/json",
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                if(!$.jUtil.isEmpty(res)) {

                    let index = eventBannerListGrid.gridView.searchItem({
                        fields: ['bannerNo'],
                        values: [bannerNo]
                    });

                    if (index != -1) {
                        eventBannerListGrid.gridView.setValues(index, res[0], true);
                    }
                }else {
                    //검색결과 없으면 조회
                    eventBanner.search();
                }
            }
        });

    },
    /**
     * Detail 추가
     * @returns {boolean}
     */
    addBannerDetailRow : function() {

        if(!eventBanner.valid.detail()){
            return false;
        }

        var url;
        switch ($('#linkType').val()) {
            case "EXH" :
            case "EVENT" :
                url = $('#promoNo').val();

                break;

            case "URL" :
                url = $('#url').val();
                break;
        }

        var value = {
            bannerNo    : $('#bannerNo').val(),
            priority    : $('#priority').val(),
            linkType    : $('#linkType').val(),
            imgUrl      : $('.imgUrl').val(),
            imgNm       : $('.imgNm').val(),
            imgWidth    : $('.imgWidth').val(),
            imgHeight   : $('.imgHeight').val(),
            linkTypeNm  : $("#linkType option:checked").text(),
            changeYn    : "Y",
            useYn       : "Y",
            linkInfo    : url,
            promoNm     : $('#promoNm').val()

        };
        bannerDetailListGrid.dataProvider.addRow(value);
        eventBanner.resetDetailForm();
    },
    /**
     * Detail 삭제
     * @returns {boolean}
     */
    deleteBannerDetailRow : function() {

        let curr = bannerDetailListGrid.gridView.getCurrent();

        if( curr.itemIndex < 0){
            alert ("삭제하려는 데이터가 없습니다. ");
            return false;
        }
        bannerDetailListGrid.dataProvider.removeRow(curr.itemIndex);
    },
    /**
     * Detail 수정
     */
    updateBannerDetailRow : function () {

        if(!eventBanner.valid.detail()){
            return false;
        }

        var row = bannerDetailListGrid.gridView.getCurrent().itemIndex;
        if( row < 0 ){
            alert ("수정 하려는 데이터가 없습니다.");
            return false;
        }

        var url;

        switch ($('#linkType').val()) {
            case "EXH" :
            case "EVENT" :
                url = $('#promoNo').val();
                break;

            case "URL" :
                url = $('#url').val();
                break;
        }

        var value = {
            priority    : $('#priority').val(),
            linkType    : $('#linkType').val(),
            linkTypeNm  : $("#linkType option:checked").text(),
            useYn       : "Y",
            imgUrl      : $('.imgUrl').val(),
            imgNm       : $('.imgNm').val(),
            imgWidth    : $('.imgWidth').val(),
            imgHeight   : $('.imgHeight').val(),
            changeYn    : "Y",
            linkInfo    : url,
            promoNm     : $('#promoNm').val()
        };
        bannerDetailListGrid.gridView.setValues(row, value, true);
        eventBanner.resetDetailForm();

    },
    /**
     * 파트너 검색 팝업창
     */
    addPartnerPopup : function() {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback=eventBanner.callback.applyPartnerPopupCallback"
            + "&partnerType="
            + "SELL"
            , "partnerPop", "1080", "600", "yes", "no");
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(){
        let callback ='eventBanner.callback.applyItemsPopupCallback';
        let storeType = $('#storeType').val();

        let mallType;
        if(storeType == "CLUB" || storeType == "HYPER" || storeType == "EXP"){
            mallType='TD';
        }else if(storeType == "DS"){
            mallType = "DS";
        }

        let url = "/common/popup/itemPop?callback=" + callback
            + "&isMulti=Y&storeType=" + storeType
            + "&mallType="+mallType;

        $.jUtil.openNewPopup( url, 1084, 650);
    },
    /**
     * 이벤트/기획전팝업
     * @param type
     */
    addPromoPopup : function() {

        let linkType  = $('#linkType').val();
        let siteType ='HOME';

        let url = '/common/popup/promoPop?callback=eventBanner.callback.applyPromoPopUpCallback'
            + '&isMulti=N&siteType=' + siteType
            + "&promoType=" + linkType;

        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 그리드 상품 삭제
     */
    deleteSelectedItem : function (obj){
        var id = ("#"+obj);
        $(id).remove();
    },
    /**
     * 노출 타입 UI 활성화/비활성화
     * @param dispType
     */
    changeDispType : function(dispType) {

        switch (dispType) {
            case "SELLER" :
                $('#partnerRow').show();
                $('#cateRow').hide();
                $('#itemRow').hide();
                break;

            case "CATE" :
                $('#partnerRow').hide();
                $('#cateRow').show();
                $('#itemRow').hide();
                $('#selectCateCd1').val('').change();
                break;

                case "ITEM" :
                $('#partnerRow').hide();
                $('#cateRow').hide();
                $('#itemRow').show();
                break;

            case "ALL" :
                $('#partnerRow').hide();
                $('#cateRow').hide();
                $('#itemRow').hide();
                break;

        }
    },
    /**
     * LINK_TYPE 별 UI 활성화/비활성화
     */
    changeLinkType : function (linkType) {

        switch (linkType) {
            case "EXH" :
            case "EVENT" :
                $('#promoArea').show();
                $('#urlArea').hide();

                $("#url").prop("disabled", true);
                $("#promoNo").prop("disabled", false);
                break;

            case "URL" :
                $('#promoArea').hide();
                $('#urlArea').show();

                $("#url").prop("disabled", false);
                $("#promoNo").prop("disabled", true);
                break;

            case "NONE" :
                $('#promoArea').hide();
                $('#urlArea').hide();
                break;
        }
    },

    /**
     * 상품상세공지검색
     * @returns {boolean}
     */
    search : function() {
        if(!eventBanner.valid.search()){
            return false;
        }
        var form =  $('#eventBannerSearchForm').serializeObject();

        CommonAjax.basic({
            url: '/item/banner/getEventBannerList.json',
            data: JSON.stringify(form),
            contentType: "application/json",
            method: "POST",
            successMsg: null,
                callbackFunc: function (res) {
                eventBannerListGrid.setData(res);
                $('#eventBannerTotalCount').html($.jUtil.comma(eventBannerListGrid.gridView.getItemCount()));
            }
        });

    },

    /**
     * 공지타입 : 상품별 > 등록 상품 조회
     *
     */
    getItemList : function(bannerNo) {

        $('#items').children().remove(); //초기 데이터 삭제

        CommonAjax.basic( {
            url:'/item/banner/getItemMatchList.json?',
            data: {bannerNo: (bannerNo)},
            method: "GET",
            callbackFunc: function (res) {
                var resultCount = res.length;

                if (resultCount > 0) {
                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];
                        $('#items').append("<li style='line-height: 20px;' id="+ data.itemNo + ">"
                            +"ㄴ"+  data.itemNo +" | "+ data.itemNm
                            +" <button type=button id="+ data.itemNo
                            +" class=btn_close onclick='eventBanner.deleteSelectedItem(this.id)'>X</button> </li>")
                    }
                }
            }
        })

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        //초기화
        eventBanner.resetEventBannerForm();

        let rowDataJson = eventBannerListGrid.dataProvider.getJsonRow(selectRowId);

        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNo').val(rowDataJson.bannerNo);

        $('#partnerId').val(rowDataJson.partnerId);
        $('#partnerNm').val(rowDataJson.partnerNm);

        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);

        $('#storeType').val(rowDataJson.storeType);
        $('#dispType').val(rowDataJson.dispType);

        eventBanner.dispTypeOptReset(rowDataJson.dispType);
        eventBanner.changeDispType(rowDataJson.dispType);

        $('input:radio[name="dispYn"]:input[value="'+rowDataJson.dispYn+'"]').trigger('click'); //노출여부

        eventBanner.img.init();

        //detail 조회
        CommonAjax.basic({
            url: '/item/banner/getEventBannerDetailList.json?',
            data: {bannerNo: rowDataJson.bannerNo},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                bannerDetailListGrid.setData(res);
            }
        });

        let type = rowDataJson.dispType;

        //구분: 카테고리
        if("CATE" == type) {
            eventBanner.getCategoryDetail(rowDataJson.dcateCd);
            return false;
        }
        //구분 : 상품
        else if("ITEM" == type) {
            eventBanner.getItemList(rowDataJson.bannerNo);
        }


    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowDetailSelect : function(index) {

        eventBanner.resetDetailForm();

        let rowDataJson = bannerDetailListGrid.dataProvider.getJsonRow(index.dataRow);
        let type = rowDataJson.linkType;

        $('#priority').val(rowDataJson.priority);
        $('#linkType').val(type).trigger('change');


        switch (type) {
            case "EXH" :
            case "EVENT" :
                $('#promoNo').val(rowDataJson.linkInfo);
                $('#promoNm').val(rowDataJson.promoNm);
                break;

            case "URL" :
                $('#url').val(rowDataJson.linkInfo);
                break;

            case "NONE" :
                break;
        }

        if (!$.jUtil.isEmpty(rowDataJson.imgUrl)) {
            eventBanner.img.setImg($('#imgBanner'), {
                'imgNm' : rowDataJson.imgNm,
                'imgUrl': rowDataJson.imgUrl,
                'imgWidth' : rowDataJson.imgWidth,
                'imgHeight' : rowDataJson.imgHeight,
                'src'   : rowDataJson.imgUrl,
                'changeYn' : 'N'
                }
            );
        }
    },
    /**
     * 상세카테고리 조회
     */
    getCategoryDetail : function(dcateCd) {

        if(!$.jUtil.isEmpty(dcateCd)){
            CommonAjax.basic({
                url: '/common/category/getCategoryDetail.json?depth=4&cateCd=' + dcateCd,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#selectCateCd1'));
                    commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#selectCateCd2'));
                    commonCategory.setCategorySelectBox(3, res.mcateCd, res.scateCd, $('#selectCateCd3'));
                    commonCategory.setCategorySelectBox(4, res.scateCd, res.dcateCd, $('#selectCateCd4'));

                }});
        }
    },
    /**
     * 검색폼 초기화
     */
    resetSearchForm : function() {

        eventBanner.initSearchDate("-30d");

        $('#schCateCd1').val('').change();
        $('#schDispType').val('');
        $('#schDispYn').val('');
        $('#schKeyword').val('')
        $('#schType').val("partnerNm");
        $('#schDateType').val("REGDT");

    },
    /**
     * 상품상세 입력 폼 초기화
     */
    resetEventBannerForm : function() {

        $('#bannerNm').val('');
        $('#bannerNo').val('');
        $('input:radio[name="dispYn"]:input[value="Y"]').trigger('click');

        eventBanner.initDispDate(0);

        $('#dispStartDt, #dispEndDt').val('');
        $('#partnerId, #partnerNm').val('');

        $('#storeType').val("HYPER").trigger('change');
        $('#dispType').val("ITEM").trigger('change');

        $('#selectCateCd1').val('').change();
        $('#items').children().remove();


        eventBanner.resetDetailForm();

        bannerDetailListGrid.dataProvider.clearRows();
    },

    /**
     * Detail Form 초기화
     */
    resetDetailForm : function (){

        $('#linkType').val('EXH').trigger('change');
        $('#priority').val('');

        $('#promoNo ,#promoNm').val('');
        $('#url').val('https://');

        eventBanner.img.init();
    },
    /**
     * URL 체크
     * @param url
     * @returns {boolean}
     */
    checkUrl : function(url){
        var expUrl = /(http(s)?:\/\/)([a-z0-9\w])/gi;
        return expUrl.test(url);
    },

};

/**
 * validation
 * @type {{search: eventBanner.valid.search, set: eventBanner.valid.set, detail: eventBanner.valid.detail}}
 */
eventBanner.valid = {

    search : function () {
        // 날짜 체크
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            $("#schStartDt").focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#schStartDt").focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        return true;

    },
    set : function(){

        //수정, 등록버튼 체크
        let bannerNm = $('#bannerNm').val();

        //제목체크
        if(!$.jUtil.isEmpty(bannerNm)) {
            if(!$.jUtil.isAllowInput(bannerNm, ['ENG', 'NUM', 'KOR'])) {
                alert("특수문자 사용이 불가 합니다.");
                $('#bannerNm').focus();
                return false;
            }
        }else {
            alert("모든 항목은 빠짐없이 입력해주세요 ")
            $('#bannerNm').focus();
            return false;
        }

        //적용대상
        let dispType = $('#dispType').val();

        switch (dispType) {
            case "SELLER":
                if($.jUtil.isEmpty($('#partnerId').val())) {
                    return $.jUtil.alert("판매자명(ID)를 입력해주세요.");
                }
                break;

            case "CATE":
                if($.jUtil.isEmpty($('#selectCateCd4').val())) {
                    return $.jUtil.alert("카테고리를 입력해주세요." , 'selectCateCd1');
                }
                break;

            case "ITEM":
                let size = $('#items').find("li").length;
                if (size <= 0) {
                    return $.jUtil.alert("공지 등록 상품을 입력해주세요.", 'schItemPopBtn' );
               }
                break;
        }

        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("노출기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid()) {
            alert("노출기간 날짜 형식이 맞지 않습니다");
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("노출기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add( -364, "day").format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("노출기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(moment(endDt.val()).format("YYYY-MM-DD 00:00:00"));
            return false;
        }

        if( bannerDetailListGrid.gridView.getItemCount()== 0 ){
            alert ("등록할 이미지를 추가해주세요.");
            $('#linkType').focus();
            return false;
        }

        return true;
    },
    /**
     * 등록 validation
     * @returns {boolean}
     */
    detail : function () {

        var linkType = $('#linkType').val();

        if($.jUtil.isEmpty(linkType)){
            return $.jUtil.alert("링크정보를 입력해주세요. ", 'linkType');
        }

        switch (linkType) {
            case "EXH" :
                if ($.jUtil.isEmpty($('#promoNo').val())){
                    return $.jUtil.alert("기획전을 등록해주세요.", 'promoNo' );
                }
                break;

            case "EVENT" :
                if ($.jUtil.isEmpty($('#promoNo').val())){
                    return $.jUtil.alert("이벤트를 등록해주세요.", 'promoNo' );
                }
                break;

            case "URL" :
                if ($.jUtil.isEmpty($('#url').val())){
                    return $.jUtil.alert("URL을 입력헤주세요.", 'url' );
                }

                if (!eventBanner.checkUrl($('#url').val())) {
                    return $.jUtil.alert("http:// 또는 https:// 를 포함하여 url을 입력해 주세요.", 'url');
                }
                break;
            }

        let form = $('#eventBannerSetForm').serializeObject(true);

        if (form.imgUrl == '' || form.imgUrl == 'undefined') {
            return $.jUtil.alert("필수 이미지를 등록해 주세요.");
        }

        if ($.jUtil.isEmpty($('#priority').val())) {
            return $.jUtil.alert("노출순서를 입력해주세요.", 'priority');
        }

        if(bannerDetailListGrid.gridView.getItemCount() > 10){
            return $.jUtil.alert("최대 10개까지만 등록 가능합니다. ");
        }

        return true;
    },
};

/**
 * 이미지 등록/수정
 * @type {{init: eventBanner.img.init, imgDiv: null, setImg: eventBanner.img.setImg, deleteImg: eventBanner.img.deleteImg, addImg: eventBanner.img.addImg, imagePreview: eventBanner.img.imagePreview, getProcessKey: (function(*=): *), clickFile: eventBanner.img.clickFile}}
 */
eventBanner.img = {
    imgDiv : null,

    /**
     * 이미지 초기화
     */
    init : function() {
        $('.uploadType').each(function(){ eventBanner.img.setImg($(this))});
        eventBanner.img.imgDiv = null;
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgUrlTag').prop('src', hmpImgUrl + "/provide/view?processKey=" + eventBanner.img.getProcessKey(obj) + "&fileId=" + params.src);
            obj.find('.changeYn').val(params.changeYn);

            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();

        } else {

            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.find('.changeYn').val('');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {
       var file = $('#bannerImgFile');
       var params = {
            processKey : eventBanner.img.getProcessKey(eventBanner.img.imgDiv),
            mode : "IMG"
        };

        if ($('#bannerImgFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        }

        if ($('#bannerImgFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
               var errorMsg = "";
                for (let i in resData.fileArr) {
                   var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        alert(f.error);
                    } else {
                        eventBanner.img.setImg(eventBanner.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth' : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'   : f.fileStoreInfo.fileId,
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        var imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(obj) {
        if (confirm('삭제하시겠습니까?')) {
            eventBanner.img.setImg($('#' + obj), null);
        }
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(obj) {
        eventBanner.img.imgDiv = $('#'+obj);
        $('input[name=fileArr]').click();
    },
    /**
     * ProcessKey
     * @param obj
     * @returns {*}
     */
    getProcessKey : function (obj) {
        var processKey;

        if (typeof obj != 'undefined') {
            processKey = obj.data('processkey');
        }
        return processKey;
    }
};

/**
 * callback
 * @type {{applyPartnerPopupCallback: eventBanner.callback.applyPartnerPopupCallback, applyPromoPopUpCallback: eventBanner.callback.applyPromoPopUpCallback, applyItemsPopupCallback: eventBanner.callback.applyItemsPopupCallback}}
 */
eventBanner.callback = {
    /**
     * 판매자 조회 팝업 Callback
     * @param resDataArr
     */
    applyPartnerPopupCallback : function(resDataArr) {
        $('#partnerId').val(resDataArr[0].partnerId);
        $('#partnerNm').val(resDataArr[0].partnerNm);
    },
    /**
     * 기획전 팝업 Callback
     * @param resDataArr
     */
    applyPromoPopUpCallback : function(resDataArr) {
        $('#promoNo').val(resDataArr[0].promoNo);
        $('#promoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 공지타입 : 상품별 > 등록 상품 조회
     */
    applyItemsPopupCallback : function(resDataArr) {
        var resultCount = resDataArr.length;

        if( resultCount >0 ){
            for (var idx = 0; resultCount > idx; idx++) {
                var data = resDataArr[idx];
                $('#items').append("<li id=" + data.itemNo + ">"
                    + "ㄴ" + data.itemNo + " | " + data.itemNm
                    + " <button type=button id=" + data.itemNo
                    + " class=btn_close onclick='eventBanner.deleteSelectedItem(this.id)'>X</button> </li>")
            }

        }else {
            alert("조회된 상품이 없습니다. ");
        }
    },
}