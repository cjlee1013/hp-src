var prodOptAllExcelPop = {

    init : function() {
        this.event();
    },

    event : function() {
        $('#typeCSampleDownBtn').bindClick(optExcelFormObj.downloadExcelForm, 'C');     // 위탁 상품 엑셀양식 다운로드
        $('#typeDSampleDownBtn').bindClick(optExcelFormObj.downloadExcelForm, 'D');     // 직매입 상품 엑셀양식 다운로드
        $('#schFileBtn').bindClick(optExcelFormObj.openFileSearch);                     // 파일 찾기
        $('#uploadFile').bindChange(optExcelFormObj.changeFileSearch);                  // 파일 선택 후
        $('#fileUploadBtn').bindClick(optExcelFormObj.uploadExcelFile);                 // 파일 업로드
    }
};

// 엑셀양식 - 다운로드 / 업로드 영역
var optExcelFormObj = {

    //상품 엑셀양식 다운로드
    downloadExcelForm : function(type) {
        var fileUrl = "/static/templates/선택형옵션일괄등록_엑셀양식.xlsx";

        if (fileUrl) {
            location.href = fileUrl+'?v08';
        } else {
            alert("양식 파일이 없습니다.");
        }
    },

    // 파일 찾기 버튼 선택
    openFileSearch : function() {
        $('#uploadFile').trigger('click');
    },

    // 파일 선택 후
    changeFileSearch : function() {
        var fileInfo = $('#uploadFile')[0].files[0];

        if (!fileInfo) {
            alert('업로드 대상 파일을 찾아주세요.');
            return false;
        }

        var fileExt = fileInfo.name.split(".")[1];

        if (fileExt.indexOf("xlsx") < 0 && fileExt.indexOf("xls") < 0) {
            alert("상품 일괄등록은 지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            $('#uploadFile').val('');
            return false;
        }

        $("#uploadFileNm").val($('#uploadFile')[0].files[0].name);
    },

    //엑셀 파일 업로드
    uploadExcelFile : function() {
        if (!$('#uploadFile').val() || !$("#uploadFileNm").val()) {
            alert('업로드 대상 파일을 찾아주세요.');
            $('#schFileBtn').focus();
            return false;
        }

        $('#optExcelTransType').val($('input:radio[name="transType"]:checked').val());
        $('#optExcelOptDepth').val($('#optDepth').val());

        $('#uploadOptExcelPopForm').ajaxForm({
            url: '/item/excel/addProdOptExcelFile.json',
            enctype: 'multipart/form-data',
            success: function(res) {
                if (res.returnCode == '0' && res.returnMsg) {
                    prodCommon.getLayerPopupClose();
                    alert(res.returnMsg);
                    prodOptSub.addProdOptGrid(res.resultList);
                } else {
                    alert(res);
                }
            },
            error: function(resError) {
                alert('지정된 엑셀양식으로만 파일 업로드해주세요.');
            }
        }).submit();
    }
};

$(document).ready(function() {
    prodOptAllExcelPop.init();
});