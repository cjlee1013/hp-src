/**
 * 상품관리>점포관리
 */
$(document).ready(function() {
    storeListGrid.init();
    store.init();
    CommonAjaxBlockUI.global();

});

//점포관리 그리드
var storeListGrid = {

    gridView : new RealGridJS.GridView("storeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        storeListGrid.initGrid();
        storeListGrid.initDataProvider();
        storeListGrid.event();
    },
    initGrid : function () {
        storeListGrid.gridView.setDataSource(storeListGrid.dataProvider);

        storeListGrid.gridView.setStyles(storeListGridBaseInfo.realgrid.styles);
        storeListGrid.gridView.setDisplayOptions(storeListGridBaseInfo.realgrid.displayOptions);
        storeListGrid.gridView.setColumns(storeListGridBaseInfo.realgrid.columns);
        storeListGrid.gridView.setOptions(storeListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        storeListGrid.dataProvider.setFields(storeListGridBaseInfo.dataProvider.fields);
        storeListGrid.dataProvider.setOptions(storeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        storeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            store.gridRowSelect(index.dataRow)

        };
    },
    setData : function(dataList) {
        storeListGrid.dataProvider.clearRows();
        storeListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(storeListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "점포관리_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        storeListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

// 점포관리
var store = {
    /**
     * 초기화
     */
    init: function () {
        this.bindingEvent();
        this.resetSearchForm();
        this.resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {

        $('#searchBtn').bindClick(store.search);
        $('#excelDownloadBtn').bindClick(store.excelDownload);
        $('#searchResetBtn').bindClick(store.resetSearchForm);
        $('#resetBtn').bindClick(store.resetForm);
        $('#setStoreBtn').bindClick(store.setStore);
        $('#storeType').bindChange(store.changeStoreType);
        $('#storeKind').bindChange(store.changeStoreKind);

        $("#originStoreId").keyup(function (e) {
            if (e.keyCode == 13) {
                store.getStore('originStore');
            }
        });

        $("#fcStoreId").keyup(function (e) {
            if (e.keyCode == 13) {
                store.getStore('fcStore');
            }
        });

    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        var storeType = $('#storeType').val();

        if ($.jUtil.isEmpty(storeType)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        windowPopupOpen("/common/popup/storePop?callback="
            + callBack + "&storeType="
            + ( storeType == "AURORA" ? "HYPER" : storeType)
            , "partnerStatus", "1100", "620", "yes", "no");

    },
    /**
     * 점포등록/수정 검증
     */
    setStore : function () {

        if(!store.valid.set()){
            return false;
        }

        var form = $('#storeSetForm').serializeObject();

        CommonAjax.basic({
            url: "/item/store/setStore.json",
            method: "post",
            contentType: 'application/json',
            data: JSON.stringify(form),
            callbackFunc: function (res) {
                alert(res.returnMsg);
                store.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },

    /**
     * 점포 리스트 검색
     */
    search : function () {

        var searchKeyword = $('#searchKeyword').val();
        var searchType = $('#searchType').val();
        var searchKeywordLength = $('#searchKeyword').val().length;

        if(searchKeyword != "" && searchType != "storeId") {
            if (searchKeywordLength < 2) {
                alert("검색 키워드는 2자 이상 입력해주세요.");
                $('#searchKeyword').focus();
                return false;
            }
        }

        var form = $('#storeSearchForm').serializeObject();
        CommonAjax.basic({
            url: '/item/store/getStoreList.json',
            data:  JSON.stringify(form),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                storeListGrid.setData(res);

                $('#storeTotalCount').html($.jUtil.comma(storeListGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 검색 초기화
     */
    resetSearchForm : function () {
        $('#searchStoreType').val('');
        $('#searchStoreKind').val('');
        $('#searchKeyword').val('');
        $('#searchStoreAttr').val('');
        $('#searchType').val('storeNm');
        $('#searchUseYn').val('');

    },

    /**
     * 등록/수정 form 초기화
     */
    resetForm : function() {

        //기본정보
        $('#isNew').val("Y");
        $('#storeId').prop('readonly', false );
        $('#storeId, #storeNm ,#storeNm2, #onlineCostCenter, #storeCostCenter').val('');

        $('#storeType').val('');
        $('#storeKind').val('');

        $('#originStoreId').val('');
        $('#originStoreNm').val('');

        $('#fcStoreId').val('');
        $('#fcStoreNm').val('');

        $('#region').val('');
        $('input:radio[name="pickupYn"]:input[value="Y"]').trigger('click');
        $('#useYn').val('Y');
        $('#onlineYn').val('Y');

        $('#originStoreId, #fcStoreId').prop('readonly', false);

        //상세정보
        $('#mgrNm, #phone, #fax, #closedInfo, #oprTime, #zipcode, #addr1, #addr2').val('');
        $('#gibunAddrArea').hide();

        store.changeStoreKind();
        store.changeStoreType();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        storeListGrid.excelDownload();
    },
    /**
     * 점포유형 Change
     */
    changeStoreType : function() {
        //hyper, club 일 경우 일반점, 택배점 가능 || 새벽배송 익스프레스 : 일반점 고정
        var storeType = $('#storeType').val();

        switch (storeType) {

            case "EXP" :
                $("#storeKind option[value='DLV']").remove();
                $('#storeKind').val("NOR"); //일반점

                store.storeAttrOptReset("EXP");
                break;

            case "AURORA" :
                $("#storeKind option[value='DLV']").remove();
                $('#storeKind').val("NOR"); //고정
                store.storeAttrOptReset();

                break;

            default :
                store.storeKindOptReset($('#storeKind').val()); //reset=
                store.storeAttrOptReset();

                break;
        }
        store.changeStoreKind();
    },
    /**
     * 점포구분 Change
     */
    changeStoreKind : function() {

        var storeKind = $('#storeKind').val();

        switch (storeKind) {
            case "NOR" :
                if($('#storeType').val() == "AURORA") {
                    store.showOriginStoreIdArea();
                }else{
                    store.hideOriginStoreIdArea();
                }
                store.storeTypeOptReset($('#storeType').val());
                break;

            case "DLV" :
                store.showOriginStoreIdArea();

                if($('#storeType').val()=="EXP"){
                    $('#storeType').val('');
                }

                $("#storeType option[value='EXP']").remove();
                break;

            default :
                store.hideOriginStoreIdArea();
                store.storeTypeOptReset($('#storeType').val());
        }
    },
    /**
     * Reset specialAttr option
     */
    storeAttrOptReset : function(storeAttr) {

        $("select#storeAttr option").remove();
        $("select#storeAttr").append("<option value=''>선택</option>");

        let storeType = $('#storeType').val();

        if( !$.jUtil.isEmpty(storeType)) {
            for (let i in storeAttrJson) {
                if (storeAttrJson[i].ref1 == storeType) {
                    $("select#storeAttr").append("<option value=" + storeAttrJson[i].mcCd + ">" + storeAttrJson[i].mcNm + "</option>");
                }
            }
        } else {
            for (let i in storeAttrJson) {
                $("select#storeAttr").append("<option value=" + storeAttrJson[i].mcCd + ">" + storeAttrJson[i].mcNm + "</option>");
            }
        }

        $.jUtil.isEmpty(storeAttr) ? $('#storeAttr').val(storeAttr) : $('#storeAttr').val('');
    },
    /**
     * Reset storeKind option
     *
     */
    storeKindOptReset : function(storeKind) {

        $("select#storeKind option").remove();
        $("select#storeKind").append("<option value=''>선택</option>");

        for ( let i in storeKindJson){
            $("select#storeKind").append("<option value="+storeKindJson[i].mcCd+">"+storeKindJson[i].mcNm+"</option>");
        }

        $('#storeKind').val(storeKind);
    },
    /**
     * Reset storeType option
     */
    storeTypeOptReset : function(storeType) {

        $("select#storeType option").remove();
        $("select#storeType").append("<option value=''>선택</option>");

        for (let i in storeTypeJson) {
            $("select#storeType").append("<option value="+ storeTypeJson[i].mcCd+">"+storeTypeJson[i].mcNm+"</option>");
        }

        $('#storeType').val(storeType);
    },
    /**f
     * 기준점포 노출
     */
    showOriginStoreIdArea : function() {
        $('#originStoreIdArea').show();

    },
    /**
     * 기준점포 숨김
     */
    hideOriginStoreIdArea : function() {
        $('#originStoreIdArea').hide();
        $('#originStoreId').val('');
        $('#originStoreNm').val('');
    },
    /**
     * 점포 조회
     * @param type
     */
    getStore  : function (type) {

        var storeId = $('#' +type+ 'Id').val();
        // 조회 조건
        if ($.jUtil.isEmpty(storeId)) {
            alert("점포ID를 입력해주세요.");
            return;
        }

        CommonAjax.basic({
            url: '/item/store/getStore.json?',
            data: { storeId : storeId},
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {

                $('#' +type+ 'Id').val(res.storeId);
                $('#' +type+ 'Nm').val(res.storeNm);
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect :  function (selectRowId) {

        store.resetForm();

        var rowDataJson = storeListGrid.dataProvider.getJsonRow(selectRowId);

        //기본정보
        $('#storeId').val(rowDataJson.storeId);
        $('#storeNm').val(rowDataJson.storeNm);
        $('#storeNm2').val(rowDataJson.storeNm2);
        $('#storeType').val(rowDataJson.storeType).trigger('change');
        $('#storeKind').val(rowDataJson.storeKind).trigger('change');

        $('#originStoreId').val(rowDataJson.originStoreId);
        $('#originStoreNm').val(rowDataJson.originStoreNm);

        $('#fcStoreId').val(rowDataJson.fcStoreId == 0 ? null : rowDataJson.fcStoreId);
        $('#fcStoreNm').val(rowDataJson.fcStoreNm);

        $('#onlineYn').val(rowDataJson.onlineYn);
        $('input:radio[name="pickupYn"]:input[value="'+rowDataJson.pickupYn+'"]').trigger('click');

        $('#useYn').val(rowDataJson.useYn);

        $('#region').val(rowDataJson.region);
        $('#onlineCostCenter').val(rowDataJson.onlineCostCenter);
        $('#storeCostCenter').val(rowDataJson.storeCostCenter);

        $('#storeId').attr('readonly', true);

        $('#storeAttr').val(rowDataJson.storeAttr);

        //상세정보
        $('#mgrNm').val(rowDataJson.mgrNm);
        $('#phone').val(rowDataJson.phone);
        $('#fax').val(rowDataJson.fax);
        $('#closedInfo').val(rowDataJson.closedInfo);
        $('#oprTime').val(rowDataJson.oprTime);
        $('#zipcode').val(rowDataJson.zipcode);
        $('#addr1').val(rowDataJson.addr1);
        $('#addr2').val(rowDataJson.addr2);

        $('#isNew').val("N");

    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setZipcode : function (res) {
        $("#zipcode").val(res.zipCode);
        $("#addr1").val(res.roadAddr);
        $("#addr2").val('');
    }
};

/**
 * Callback 영역
 * @type {{schOriginStore: store.callBack.schOriginStore, schFcStore: store.callBack.schFcStore}}
 */
store.callBack = {
    schOriginStore: function (res) {
        $('#originStoreId').val(res[0].storeId);
        $('#originStoreNm').val(res[0].storeNm);

        $('#originStoreId').prop('readonly', true);

    },
    schFcStore: function (res) {
        $('#fcStoreId').val(res[0].storeId);
        $('#fcStoreNm').val(res[0].storeNm);

        $('#fcStoreId').prop('readonly', true);
    }
};

/**
 * 유효성 검사 영역
 * @type {{set: store.valid.set}}
 */
store.valid = {
    set : function () {

        var storeId = $('#storeId').val();

        if ($.jUtil.isEmpty(storeId)) {
            alert('사용할 점포코드를 입력해주세요.');
            $("#storeId").focus();
            return false;
        }

        if (!$.jUtil.isAllowInput(storeId,['NUM'])) {
            alert("숫자만 사용 가능합니다.");
            $("#storeId").focus();
            return false;
        }

        var fcStoreId = $('#fcStoreId').val();

        if (!$.jUtil.isEmpty(fcStoreId)) {
            if (!$.jUtil.isAllowInput(fcStoreId,['NUM'])) {
                alert("Fulfillment 점포는 숫자만 사용 가능합니다.");
                $("#fcStoreId").focus();
                return false;
            }
        }

        var originStoreId = $('#originStoreId').val();

        if (!$.jUtil.isEmpty(originStoreId)) {
            if (!$.jUtil.isAllowInput(originStoreId,['NUM'])) {
                alert("기준점포 코드는 숫자만 사용 가능합니다.");
                $("#originStoreId").focus();
                return false;
            }
        }

        if( "DLV" == $('#storeKind').val()) {
            if ($.jUtil.isEmpty(originStoreId)) {
                alert("점포종류가 택배점인 경우 기준점포를 등록해주세요.")
                $('#originStoreId').focus();
                return false;
           }
        }

        if( "AURORA" == $('#storeType').val()) {
            if ($.jUtil.isEmpty(originStoreId)) {
                alert("점포유형이 새벽배송인 경우 기준점포를 등록해주세요.")
                $('#originStoreId').focus();
                return false;
            }
        }

        if ($.jUtil.isEmpty($('#phone').val())) {
            alert('대표번호를 입력해주세요.');
            $('#phone').focus();
            return false;
        }

        if ($.jUtil.isEmpty($('#mgrNm').val())) {
            alert('대표자명을 입력해주세요.');
            $('#mgrNm').focus();
            return false;
        }
        if ($.jUtil.isEmpty($('#closedInfo').val())) {
            alert('휴무안내를 입력해주세요.');
            $('#closedInfo').focus();
            return false;
        }

        if ($.jUtil.isEmpty($('#zipcode').val())
            || $.jUtil.isEmpty($('#addr1').val())
            || $.jUtil.isEmpty($('#addr2').val())) {
            alert('주소를 입력해주세요.');
            return false;
        }

        return true;

    }
}





