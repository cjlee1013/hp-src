/**
 * 상품관리 >  점포상품관리  > 동류그룹관리
 */
$(document).ready(function() {
    sameClassListGrid.init();
    sameClassGroupListGrid.init();
    sameClassGroupList.init();
    sameClassList.init();
    CommonAjaxBlockUI.global();
});


var sameClassListGrid = {
    gridView : new RealGridJS.GridView("sameClassListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        sameClassListGrid.initGrid();
        sameClassListGrid.initDataProvider();
        sameClassListGrid.event();
    },
    initGrid : function() {
        sameClassListGrid.gridView.setDataSource(sameClassListGrid.dataProvider);

        sameClassListGrid.gridView.setStyles(sameClassListGridBaseInfo.realgrid.styles);
        sameClassListGrid.gridView.setDisplayOptions(sameClassListGridBaseInfo.realgrid.displayOptions);
        sameClassListGrid.gridView.setColumns(sameClassListGridBaseInfo.realgrid.columns);
        sameClassListGrid.gridView.setOptions(sameClassListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        sameClassListGrid.dataProvider.setFields(sameClassListGridBaseInfo.dataProvider.fields);
        sameClassListGrid.dataProvider.setOptions(sameClassListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        sameClassListGrid.gridView.onDataCellClicked = function(gridView, index) {
            sameClassList.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        sameClassListGrid.dataProvider.clearRows();
        sameClassListGrid.dataProvider.setRows(dataList);
    }

};

// 동류그룹 리스트 그리드
var sameClassGroupListGrid = {
    gridView : new RealGridJS.GridView("sameClassGroupListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        sameClassGroupListGrid.initGrid();
        sameClassGroupListGrid.initDataProvider();
        sameClassGroupListGrid.event();

    },
    initGrid : function() {
        sameClassGroupListGrid.gridView.setDataSource(sameClassGroupListGrid.dataProvider);

        sameClassGroupListGrid.gridView.setStyles(sameClassGroupListGridBaseInfo.realgrid.styles);
        sameClassGroupListGrid.gridView.setDisplayOptions(sameClassGroupListGridBaseInfo.realgrid.displayOptions);
        sameClassGroupListGrid.gridView.setColumns(sameClassGroupListGridBaseInfo.realgrid.columns);
        sameClassGroupListGrid.gridView.setOptions(sameClassGroupListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        sameClassGroupListGrid.dataProvider.setFields(sameClassGroupListGridBaseInfo.dataProvider.fields);
        sameClassGroupListGrid.dataProvider.setOptions(sameClassGroupListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        sameClassGroupListGrid.gridView.onDataCellClicked = function(gridView, index) {
            sameClassGroupList.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        sameClassGroupListGrid.dataProvider.clearRows();
        sameClassGroupListGrid.dataProvider.setRows(dataList);
    }

};
// 동류그룹리스트그리드
var sameClassGroupList = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initAjaxForm();
    },
    /**
     * 동류그룹 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#setSameClassGroupForm').ajaxForm({
            url: '/item/sameclass/setSameClassGroup.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                sameClassGroupList.setSameClassData($('#setSameClassGroupForm').serializeObject());
                sameClassGroupList.resetForm();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 동류상품 등록/수정 후 데이터 갱신
     */
    setSameClassData : function(sameClass) {
        var options = {
            fields: ['sameClassNo'],
            values: [sameClass.sameClassNo]
        };

        var itemIndex = sameClassGroupListGrid.gridView.searchItem(options);

        // 신규 등록 시 오류를 방지하기 위함. ( 그리드에 찾는 데이터가 없을 경우 -1 )
        if(itemIndex != -1) {
            var hyperLimitTxt = '제한안함';
            if(sameClass.hyperLimitYn === 'Y') {
                hyperLimitTxt = sameClass.hyperLimitQty;
            }

            var clubLimitTxt = '제한안함';
            if(sameClass.clubLimitYn === 'Y') {
                clubLimitTxt = sameClass.clubLimitQty;
            }

            var expLimitTxt = '제한안함';
            if(sameClass.expLimitYn === 'Y') {
                expLimitTxt = sameClass.expLimitQty;
            }

            var useYnTxt = '사용안함';
            if(sameClass.useYn === 'Y') {
                useYnTxt = '사용';
            }

            // RealGrid 값 변경.
            var setGridValue = {
                hyperLimitYn : sameClass.hyperLimitYn
                , hyperLimitTxt : hyperLimitTxt
                , clubLimitYn : sameClass.clubLimitYn
                , clubLimitTxt : clubLimitTxt
                , expLimitYn : sameClass.expLimitYn
                , expLimitTxt : expLimitTxt
                , useYn : sameClass.useYn
                , useYnTxt : useYnTxt
                , sameClassNm : sameClass.sameClassNm
            };

            sameClassGroupListGrid.gridView.setValues(itemIndex, setGridValue, true);
        }
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchSameClassGroupBtn').bindClick(sameClassGroupList.search);
        $('#searchSameClassGroupResetBtn').bindClick(sameClassGroupList.searchFormReset);
        $('#resetSameClassGroupBtn').bindClick(sameClassGroupList.resetForm);
        $('#setSameClassGroupBtn').bindClick(sameClassGroupList.setSameClassGroup);


        $('input[name="hyperLimitYn"]').bindChange(sameClassGroupList.setLimitYn);
        $('input[name="clubLimitYn"]').bindChange(sameClassGroupList.setLimitYn);
        $('input[name="expLimitYn"]').bindChange(sameClassGroupList.setLimitYn);
    },
    /**
     * 동류그룹 등록/수정
     */
    setSameClassGroup: function() {
        var hyperLimitYn = $("input[name='hyperLimitYn']:checked").val();
        var clubLimitYn  = $("input[name='clubLimitYn']:checked").val();
        var expLimitYn   = $("input[name='expLimitYn']:checked").val();

        if($.jUtil.isEmpty($('#sameClassNm').val())){
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#sameClassNm').focus();
            return false;

        }

        if (hyperLimitYn =='Y') {
            if(!$.jUtil.isAllowInput( $('#hyperLimitQty').val(), ['NUM'])){
                return $.jUtil.alert('숫자만 입력 가능 합니다.', 'hyperLimitQty');
                $("#hyperLimitQty").val('');
                return;
            }
            if($('#hyperLimitQty').val() <= 0){
                return $.jUtil.alert('1이상 등록이 가능합니다.', 'hyperLimitQty');
                $("#hyperLimitQty").val('');
                return;
            }

        }
        if (clubLimitYn =='Y') {
            if(!$.jUtil.isAllowInput( $('#clubLimitQty').val(), ['NUM'])){
                return $.jUtil.alert('숫자만 입력 가능 합니다.', 'clubLimitQty');
                $("#clubLimitQty").val('');
                return;
            }
            if($('#clubLimitQty').val() <= 0){
                return $.jUtil.alert('1이상 등록이 가능합니다.', 'clubLimitQty');
                $("#clubLimitQty").val('');
                return;
            }

        }
        if (expLimitYn =='Y') {
            if(!$.jUtil.isAllowInput( $('#expLimitQty').val(), ['NUM'])){
                return $.jUtil.alert('숫자만 입력 가능 합니다.', 'expLimitQty');
                $("#expLimitQty").val('');
                return;
            }

            if($('#expLimitQty').val() <= 0){
                return $.jUtil.alert('1이상 등록이 가능합니다.', 'expLimitQty');
                $("#expLimitQty").val('');
                return;
            }

        }
        $('#setSameClassGroupForm').submit();
    },

    /**
     * 동류그룹 검색
     */
    search : function() {

        CommonAjax.basic({url:'/item/sameclass/getSameClassGroupList.json?' + $('#sameClassGroupSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {

                sameClassList.resetGrid();
                sameClassList.resetForm();
                $('#setSameClassNo').val('');
                sameClassGroupListGrid.setData(res);
                $('#sameClassGroupTotalCount').html($.jUtil.comma(sameClassGroupListGrid.gridView.getItemCount()));
            }});
    },

    /**
     * 동류그룹 제한수령 설정폼
     */
    setLimitYn : function(storeType){

        if(typeof storeType == 'object') {

            var limitYn = storeType.value;
            var store =  storeType.name.replace("Yn" ,"")+"Qty";

            if(limitYn =='Y'){
                $('#'+store).prop( "disabled", false );
            }else{
                $('#'+store).prop( "disabled", true );
                $('#'+store).val('');
            }
        }
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = sameClassGroupListGrid.dataProvider.getJsonRow(selectRowId);

        sameClassGroupList.resetForm();
        sameClassList.resetForm();

        CommonAjax.basic({
            url : '/item/sameclass/getSameClassGroup.json',
            data : { sameClassNo : rowDataJson.sameClassNo },
            method : "GET",
            successMsg : null,
            callbackFunc : function(res) {

                sameClassGroupList.setSameClassData(res);

                $('#sameClassNo').val(res.sameClassNo);
                $('#sameClassNm').val(res.sameClassNm);
                $('#sameClassGroupUseYn').val(res.useYn);

                if(res.hyperLimitQty > 0 && res.hyperLimitYn =='Y'){
                    $('input:radio[name="hyperLimitYn"][value="Y"]').trigger('click');
                    $('#hyperLimitQty').val(res.hyperLimitQty);
                }
                if(res.clubLimitQty > 0 && res.clubLimitYn =='Y'){
                    $('input:radio[name="clubLimitYn"][value="Y"]').trigger('click');
                    $('#clubLimitQty').val(res.clubLimitQty);
                }
                if(res.expLimitQty > 0 && res.expLimitYn =='Y'){
                    $('input:radio[name="expLimitYn"][value="Y"]').trigger('click');
                    $('#expLimitQty').val(res.expLimitQty);
                }
                $('#setSameClassNo').val(res.sameClassNo);
                sameClassList.getSameClassList(res.sameClassNo);
            }
        });
    },
    /**
     * 동류그룹입력폼 초기화
     */
    resetForm : function() {
        $('#sameClassNo').val('');
        $('#sameClassNm').val('');
        $('#sameClassGroupUseYn').val('Y');
        $('input:radio[name$="LimitYn"][value="N"]').trigger('click');

    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchUseYn, #searchKeyword').val('');
        $('#searchType').val('CLASSNM');
    }
};


//동류상품리스트그리드
var sameClassList = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
        this.resetForm();
    },

    /**
     * 동류상품 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#setSameClassForm').ajaxForm({
            url: '/item/sameclass/setSameClass.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                sameClassList.resetForm();
                $('#setSameClassNo').val(resData.returnKey);
                sameClassList.getSameClassList(resData.returnKey);
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#resetSameClassBtn').bindClick(sameClassList.resetForm);
        $('#setSameClassBtn').bindClick(sameClassList.setSameClass);
        $('#itemNo').focusout(sameClassList.getItemName);
        $('#addItemPopUp').bindClick(sameClassList.addItemPopUp);

    },
    resetForm : function() {
        $('#itemNo').val('');
        $('#itemNm').val('');
        $('#sameClassUseYn').val('Y');
    },
    /**
     * 상품명입력
     */
    setItemName : function(res){
        $('#itemNm').val(res.returnKey);
    },
    /**
     * 상품명검색
     */
    getItemName : function(){

        var itemNo  = $('#itemNo').val();

        if(!$.jUtil.isEmpty(itemNo)){

            if (!$.jUtil.isAllowInput( itemNo, ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#itemNo").val('');
                $("#itemNm").val('');
                $("#itemNo").focus();
                return;
            } else{
                commonItem.getItemName($('#itemNo').val(), sameClassList.setItemName);
            }

        }
    },
    /**
     * 대체상품조회팝업
     */
    addItemPopUp : function(){

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=sameClassList.applyPopupCallback&isMulti=N&mallType=TD&storeType=ALL' , 1084, 650);

    },
    applyPopupCallback : function(resDataArr) {

        $('#itemNo').val(resDataArr[0].itemNo);
        $('#itemNm').val(resDataArr[0].itemNm);
        $('#useYn').val('Y');

    },
    /**
     * 동류상품 리스트 조회
     */
    getSameClassList : function(sameClassNo){
        CommonAjax.basic({url:'/item/sameclass/getSameClassList.json?', data:{sameClassNo: sameClassNo}, method:"GET", successMsg:null, callbackFunc:function(res) {
                sameClassListGrid.setData(res);
                $('#sameClassTotalCount').html($.jUtil.comma(sameClassListGrid.gridView.getItemCount()));
        }});
    },

    /**
     * 그리드 리스트 초기화
     */
    resetGrid : function(){
        sameClassListGrid.dataProvider.clearRows();
        $('#sameClassTotalCount').html($.jUtil.comma(sameClassListGrid.gridView.getItemCount()));
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = sameClassListGrid.dataProvider.getJsonRow(selectRowId);
        $('#itemNo').val(rowDataJson.itemNo);
        $('#itemNm').val(rowDataJson.itemNm1);
        $('#sameClassUseYn').val(rowDataJson.useYn);

    },
    /**
     * 동류상품 등록/수정
     */
    setSameClass: function() {

        if($.jUtil.isEmpty($('#itemNo').val())){
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#itemNo').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#itemNm').val())){
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#itemNo').focus();
            return false;
        }
        CommonAjax.basic({url:'/item/sameclass/getSameClassItem.json?', data:{sameClassNo: $('#setSameClassNo').val() , itemNo: $('#itemNo').val()}, method:"GET", successMsg:null, callbackFunc:function(res) {

            var confirmMsg  ="동류그룹번호 '%s' 에 등록된 상품 입니다. 해당 상품의 동류그룹을 이동 하시겠습니까?";
            if(res.returnKey != null && res.returnKey != '' ){

               if(!confirm( confirmMsg.replace("%s" , res.returnKey))){
                  return false;
               }
            }
            $('#setSameClassForm').submit();
            }
        });
    }

};