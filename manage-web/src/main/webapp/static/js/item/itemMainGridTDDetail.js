/**
 * 상품관리 > 점포상품관리 > 상품 등록/수정 > 그리드
 */
// 점포정보 그리드
let storeListGrid = {
    selectedRowId : null,
    gridView : new RealGridJS.GridView("storeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        storeListGrid.initGrid();
        storeListGrid.event();
        storeListGrid.initDataProvider();
    },
    initGrid : function () {
        storeListGrid.gridView.setDataSource(storeListGrid.dataProvider);

        storeListGrid.gridView.setStyles(storeListGridBaseInfo.realgrid.styles);
        storeListGrid.gridView.setDisplayOptions(storeListGridBaseInfo.realgrid.displayOptions);
        storeListGrid.gridView.setColumns(storeListGridBaseInfo.realgrid.columns);
        storeListGrid.gridView.setOptions(storeListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        storeListGrid.dataProvider.setFields(storeListGridBaseInfo.dataProvider.fields);
        storeListGrid.dataProvider.setOptions(storeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        storeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            storeListGrid.selectedRowId = index.dataRow;
            item.ship.gridRowSelect(index.dataRow);
        };

        $('#storeListGridCopy').on('click', function() {
            storeListGrid.gridView.copyToClipboard();
            alert("텍스트가 복사되었습니다.");
        });
    },
    setData : function(dataList) {
        storeListGrid.dataProvider.clearRows();
        storeListGrid.dataProvider.setRows(dataList);
    }
};
