/**
 * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정
 */
var item = {
    uploadType : 'IMG',
    safetyHtml: $('#safetyTbodyArea').html(),
    /**
     * 초기화
     */
    init : function() {
        this.event();
        itemMain.util.initDate('-30d', 'rsvStartDt', 'rsvEndDt');
        Calendar.datePicker('makeDt');
        Calendar.datePicker('expDt');
        commonEditor.initEditor($('#editor1'), 300, 'ItemDetail');
        commonEditor.hmpImgUrl = hmpImgFrontUrl;
        commonEditor.setHtml($('#editor1'), "");
        this.initCategorySelectBox();
        this.resetForm();
        item.common.getItemProviderList();
        item.attr.getAttrList("TAG");
        $('#opt1TitleTxt').allowInput("keyup", ["KOR", "ENG"]);
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#cateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#cateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#cateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#cateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#itemType').bindChange(item.sale.changeItemType);
        $('input:radio[name="adultType"]').bindClick(item.sale.changeAdultType);

        $('#cateCd4').bindChange(item.sub.changeCate4);

        $('input:radio[name="optSelUseYn"]').on('click', function() {
            item.opt.getOptHandler($(this).val());
        });

        $('#opt1TitleBtn').on('click', function() {
            item.opt.setOpt1Title($('#opt1TitleTxt').val());
        });

        $('#resetBtn').on('click', function () {
            item.resetForm();
        });

        $('#itemNm').calcTextLength('keyup', '#itemNmCount');
        $('#itemDesc').calcTextLength('keyup', '#textCountDesc');

        $('#itemNm').notAllowInput('focusout', ['SYS_DIVISION']);

        //파일선택시
        $('input[name="mainImg0"], input[name="mainImg1"], input[name="mainImg2"], input[name="mainImg3"], input[name="mainImg5"], input[name="mainImg4"], input[name="mainImg6"], input[name="listImg"], input[name="labelImg"], '
                + 'input[name="proof1"], input[name="proof2"], input[name="proof3"], input[name="proof4"], input[name="videoFile"]').change(function() {
            if (item.uploadType == 'IMG') {
                item.img.addImg($(this).data('file-id'), $(this).prop('name'));
            } else {
                item.file.addFile($(this).data('file-id'), $(this).prop('name'));
            }
        });

        $('#itemSetForm').on('propertychange change keyup paste input', '#fr-image-by-url-layer-text-1', function(e){
            commonEditor.inertImgurl = $('#fr-image-by-url-layer-text-1').val();
        });

        // 브랜드/제조사 조회
        $('#getBrandBtn').bindClick(item.attr.getBrand);
        $('#getMakerBtn').bindClick(item.attr.getMaker);

        $("#brandNm, #makerNm").keydown(function(key) {
            if (key.keyCode == 13) {
                switch ($(this).prop('id')) {
                    case 'brandNm' :
                        item.attr.getBrand();
                        break;
                    case 'makerNm' :
                        item.attr.getMaker();
                        break;
                }
            }
        });

        $('#uploadVideoFileSpan').on('click', '.close', function() {
            $(this).closest('.videoFile').remove();
        });

        item.attr.event();
        item.ext.event();
        item.common.event();
        item.img.event();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);
        item.getDetail(rowDataJson.itemNo);
    },
    /**
     * 상품상세 > 점포상품 기본정보 조회
     */
    getDetail : function(itemNo) {
        if (itemNo) {
            item.resetForm();

            CommonAjaxBlockUI.startBlockUI();
            CommonAjax.basic({
                url: '/item/getDetail.json',
                data: {itemNo: itemNo, mallType: mallType, storeId:storeId, menuType:'basic', storeType: $('input[name="schStoreType"]:checked').val()},
                method: 'GET',
                callbackFunc: function (data) {
                    $('#setStatusArea').show();

                    $('#setStatusArea button').hide();
                    if (data.itemStatus == 'A') {
                        $('#setStatusP, #setStatusS').show();
                    } else if (data.itemStatus == 'S') {
                        $('#setStatusP, #setStatusA, #itemTitleArea').show();
                    } else if (data.itemStatus == 'P') {
                        $('#setStatusS, #setStatusA, #itemTitleArea').show();
                    }

                    //기본정보
                    $('#itemNo').val(data.itemNo);
                    $('#itemParent').text(data.itemParent);
                    $('#lgcItemNm').text(data.lgcItemNm);
                    $('#itemNm').val(data.itemNm).setTextLength('#itemNmCount');
                    $('#itemNoView').text(itemNo);
                    $('#itemNmView').text(data.itemNm);
                    $('.itemStatusNm').html(data.itemStatusNm);
                    $('#statusDesc').html(data.statusDesc);
                    $('#itemChgDt').text(data.chgDt);
                    $('#itemDivision').text(data.itemDivision);
                    $('#itemType').val(data.itemType);

                    if (data.cartYn == 'Y' || data.itemType == 'P') {
                        item.sale.setCartLimitYn('Y');
                    }

                    commonCategory.setCategorySelectBox(1, '', data.lcateCd, $('#cateCd1'));
                    commonCategory.setCategorySelectBox(2, data.lcateCd, data.mcateCd, $('#cateCd2'));
                    commonCategory.setCategorySelectBox(3, data.mcateCd, data.scateCd, $('#cateCd3'));
                    commonCategory.setCategorySelectBox(4, data.scateCd, data.dcateCd, $('#cateCd4'));

                    $('#lgcCate').text(data.lgcCateNm);
                    $('#dealTypeNm').text(data.dealTypeNm);

                    $('#partnerId').val(data.partnerId);
                    $('#businessNm').val(data.businessNm);
                    $('#dispYn').val(data.dispYn);

                    $('#storeType').val(data.storeType);
                    $('#optStoreTypeNm').text(data.storeTypeNm);
                    //판매정보
                    $('input:radio[name="taxYn"]:input[value="'+data.taxYn+'"]').trigger('click');
                    saleDFListGrid.setData(data.saleDFList);
                    $('input:radio[name="cartLimitYn"]:input[value="'+data.cartLimitYn+'"]').trigger('click');
                    $('#originTxt').val(data.originTxt);

                    let packHtml = data.packYnNm;
                    if (data.packYn == 'Y') {
                        packHtml += ' (낱개 상품번호 : ' + data.itemPackList.join(', ') + ')';
                    } else if (data.packYn == 'N') {
                        packHtml += ' (팩 상품번호 : ' + data.itemPackList.join(', ') + ')';
                    }

                    $('#packArea').html(packHtml);

                    //옵션정보
                    if (!$.jUtil.isEmpty(data.representYn)) {
                        item.opt.getOptHandler("N");
                        $('input[name="optSelUseYn"]').prop('disabled', true);
                    } else {
                        $('input:radio[name="optSelUseYn"]:input[value="'+data.optSelUseYn+'"]').trigger('click');
                        item.opt.setOpt1Title(data.opt1Title);

                        optListGrid.setData(data.optList);
                        $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));

                        if (data.listSplitDispYn == 'Y') {
                            $('#listSplitDispYn').prop('checked', true);
                        }
                    }


                    //이미지정보
                    commonEditor.setHtml($('#editor1'), data.itemDesc);
                    $('input:radio[name="videoYn"]:input[value="'+data.videoYn+'"]').trigger('click');
                    if (!$.jUtil.isEmpty(data.imgList)) {
                        let changeYn = 'N';

                        data.imgList.forEach(obj => {

                            let priority = obj.priority;

                            if (obj.imgType == 'MN' && !$.jUtil.isEmpty($('#MN' + priority + ' .imgNo').val())) {
                                changeYn = 'Y';

                                $('.itemImg').each(function(idx1) {
                                    if ($.jUtil.isEmpty($('#MN' + (idx1 + 1) + ' .imgNo').val())) {
                                        priority = idx1 + 1;
                                        return false;
                                    }
                                });
                            }

                            let _this = $('#MN' + priority);

                            if (obj.imgType == 'LST') {
                                _this = $('#imgLST');
                            } else if (obj.imgType == 'LBL') {
                                _this = $('#imgLabel');
                            }

                            item.img.setImg(_this, {
                                'imgNo' : obj.imgNo,
                                'imgNm' : obj.imgNm,
                                'imgUrl': obj.imgUrl,
                                'src'   : hmpImgUrl + "/provide/view?processKey=" + _this.data('processkey') + "&fileId=" + obj.imgUrl,
                                'changeYn' : 'N'
                            });
                        });

                        if (changeYn == 'Y') {
                            $('#imgMNDiv').find('.itemImg').each(function() {
                                if (!$.jUtil.isEmpty($(this).find('.imgUrl').val())) {
                                    $(this).find('.changeYn').val('Y');
                                }
                            });
                        }
                    }
                    if (data.videoFile != null) {
                        item.file.appendVideoFile(data.videoFile);
                    }

                    //상품속성정보
                    item.attr.getAttrList("ATTR", data.itemNo, data.scateCd);
                    item.attr.getAttrList("TAG", data.itemNo);
                    item.attr.addBrandHtml(data.brandNo, data.brandNm);
                    item.attr.addMakerHtml(data.makerNo, data.makerNm);

                    $('#makeDt').val(data.makeDt);
                    $('#expDt').val(data.expDt);
                    if ($.jUtil.isEmpty(data.unitQty) || $.jUtil.isEmpty(data.unitMeasure) || $.jUtil.isEmpty(data.totalUnitQty) ) {
                        $('#unitDispYn').prop('disabled', true);
                    } else {
                        $('#unitDispYn').prop('disabled', false);
                    }

                    $('#unitQty').val(data.unitQty);
                    $('#unitMeasure').val(data.unitMeasure);
                    $('#totalUnitQty').val(data.totalUnitQty);
                    if (data.unitDispYn == 'Y') {
                        $('#unitDispYn').prop('checked', true);
                    } else if (data.unitDispYn == null) {
                        $('#unitDispYn').prop('disabled', true);
                    }

                    if (data.noticeList.length > 0) {
                        $('#noticeHtml').html(data.noticeList[0].noticeHtml);
                    } else {
                        $('#noticeHtml').html('-');
                    }

                    /** 부가정보 **/
                    let imgDispYn = 'Y';
                    if (data.imgDispYn != null) {
                        imgDispYn = data.imgDispYn;
                    }

                    $('#storeIdView').html(data.storeId);
                    $('#storeNmView').html(data.storeNm);
                    $('#storeTypeNmView').html(data.storeTypeNm);
                    item.sale.setAdultLimitYn(data.adultLimitYn, imgDispYn);
                    $('input[name="adultType"][value="'+data.adultType+'"]').trigger('click');
                    $('#imgDispYn').val(imgDispYn);

                    $('input[name="epYn"][value="'+data.epYn+'"]').trigger('click');

                    item.common.changeLinkageYn(data.linkageYn, false);

                    for (let i in data.providerList) {
                        let provider = data.providerList[i];
                        let providerId = provider.providerId.replace('coop', '');

                        $('#' + providerId + 'SetTxt').text(provider.regYnNm);
                        if (provider.useYn == 'Y' && !$.jUtil.isEmpty(provider.stopStartDt)) {
                            $('#' + providerId + 'DtTxt').text('(중지설정: ' + provider.stopStartDt + '~' + provider.stopEndDt + ' )');
                        }
                    }

                    $('#srchKeyword').val(data.srchKeyword);
                    if (data.isCert) {
                        $('input[name="isCert"][value="'+data.isCert+'"]').trigger('click');
                    }
                    $('input[name="giftYn"][value="'+data.giftYn+'"]').trigger('click');

                    //증빙서류
                    for (let i in data.proofFileList) {
                        item.file.appendProofFile(data.proofFileList[i]);
                    }

                    //안전인증
                    item.ext.setSafetyUI(data);

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    valid : function() {
        if ($.jUtil.isEmpty($('#itemNo').val())) {
            return $.jUtil.alert("상품을 선택해주세요");
        }

        if ($.jUtil.isEmpty($('#itemNm').val())) {
            return $.jUtil.alert("상품명은 필수입력항목입니다.", 'itemNm');
        }

        if ($.jUtil.isEmpty($('#originTxt').val())) {
            return $.jUtil.alert("원산지는 필수입력항목입니다.", 'originTxt');
        }

        if ($.jUtil.isNotAllowInput($("#itemNm").val(), ['SYS_DIVISION'])) {
            alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]));
            $('#itemNm').focus();
            return false;
        }

        if ($('#MN1').find('input[name="imgList[].imgUrl"]:input[value!=""]').length == 0) {
            return $.jUtil.alert("상품이미지를 등록해 주세요.");
        }

        if ($('#imgLST').find('input[name="imgList[].imgUrl"]:input[value!=""]').length == 0) {
            $("#imgLSTDiv").prop("tabindex", -1).focus();
            return $.jUtil.alert("리스팅이미지를 등록해 주세요.");
        }

        if ($('#imgLabel').find('input[name="imgList[].imgUrl"]:input[value!=""]').length == 0) {
            $("#imgLBLDiv").prop("tabindex", -1).focus();
            return $.jUtil.alert("라벨이미지를 등록해 주세요.");
        }

        if ($('#srchKeyword').val().split(',').length > 20) {
            return $.jUtil.alert("검색키워드는 최대 20개 까지 등록 가능합니다.", 'srchKeyword');
        }

        if ($('input[name="optSelUseYn"]:checked').val() == 'Y' && $.jUtil.isEmpty($('#opt1Title').val())) {
            return $.jUtil.alert("옵션명을 입력해주세요.", 'opt1TitleTxt');
        }

        //안전인증 검증
        let checkCert = true;
        $('.certNo').each(function(){
            if (!$(this).prop('readonly') && $(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                checkCert = false;
                return $.jUtil.alert('안전인증을 입력해주세요.');
            }
        });

        if (checkCert) {
            $('.energy').each(function(){
                if ($(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                    checkCert = false;
                    return $.jUtil.alert('안전인증을 입력해주세요.');
                }
            });
        }

        if (!checkCert) {
            return checkCert;
        }

        return true;
    },
    /**
     * 점포상품 기본정보 수정
     * @param isEdit
     */
    setItemTD : function() {
        if (item.valid()) {
            let itemForm        = $('#itemSetForm').serializeObject(true);
            itemForm.optList    = optListGrid.dataProvider.getJsonRows();
            itemForm.itemDesc   = commonEditor.getHtmlData('#editor1');
            itemForm.storeId    = storeId;
            itemForm.attrNoList = item.attr.getAttrNoList();

            CommonAjax.basic({
                url: '/item/setItemTDBasic.json',
                data: JSON.stringify(itemForm),
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                    $.jUtil.alert(res.returnMsg);
                    item.getDetail(res.returnKey);
                },
                errorCallbackFunc:function(resError) {
                    let res = resError.responseJSON;

                    if (res.returnCode == '1057') { //금칙어
                        $.jUtil.alert(res.returnMessage, 'itemNm');
                    } else {
                        $.jUtil.alert(res.returnMessage);
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    /**
     * 점포상품 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#itemSetForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });
        $("#itemSetForm").resetForm();

        $('#itemNo, #itemNm, #itemDesc').val('');
        $('#itemNm').setTextLength('#itemNmCount');
        $('#itemTitleArea, #setStatusArea').hide();
        $('.addRsvDiv').remove();
        $('#storeIdView, #storeNmView, #storeTypeNmView, #prTypeNm, #prContents, #uploadVideoFileSpan, #statusDesc').html('');
        $('#noticeHtml, .itemStatusNm').html('-');
        $('.isCertRadio').trigger('change');
        $('#cateCd1 option:eq(0)').prop('selected', true).trigger('change');
        item.opt.getOptHandler('N');
        saleDFListGrid.dataProvider.clearRows();
        optListGrid.dataProvider.clearRows();
        $('input[name="providerList[].useYn"]').val('N');
        $('#unitDispYn').prop('disabled', false);
        $('input[name="optSelUseYn"]').prop('disabled', false);
        $('#optTotalCount').text(0);
        $('.certCheck').show();
        $('.energy').hide();
        $('.certNo').prop('placeholder','');
        $('#unitDispYn').prop('disabled', false);
        $('#naverSetTxt, #elevenSetTxt').text('등록안함');
        $('#naverDtTxt, #elevenDtTxt').text('');

        item.sale.setCartLimitYn('N');
        item.sale.setAdultLimitYn('NORMAL');

        item.opt.getDetail();
        item.opt.initOptTitle();
        item.attr.addBrandHtml(0,'');
        item.attr.addMakerHtml(0,'');

        item.sub.dcateCartYn    = 'N';
        item.sub.imgDispYn      = 'Y';

        $('#safetyTbodyArea').html(item.safetyHtml);

        commonEditor.setHtml($('#editor1'), "");

        item.img.init();

        item.attr.init();

        item.common.changeLinkageYn('Y', true);

        $('[id^=uploadProofFileSpan_]').each(function(){
            $(this).html('');
        });
    }
};

item.opt = {
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = optListGrid.dataProvider.getJsonRow(selectRowId);

        item.opt.getDetail(rowDataJson);

    },
    /**
     * 옵션 상세
     * @param rowData
     */
    getDetail : function(rowData) {
        if (rowData){
            $('#optNo').html(rowData.optNo);
            $('#optAddBtn').text('수정');
            $('#opt1Val').val(rowData.opt1Val);
            $('#optPrice').val(rowData.optPrice);
            $('#optStockQty').val(rowData.stockQty);
            $('#sellerOptCd').val(rowData.sellerOptCd);
            $('#optUseYn').val(rowData.useYn);
        } else {
            optListGrid.selectedRowId = null;
            $('#optNo').html('');
            $('#optAddBtn').text('추가');
            $("#optSelUseYnArea table").find('input').val('');
            $("#optSelUseYnArea table").find('select').each(function(){
                $(this).find('option:eq(0)').prop('selected', true);
            });
        }
    },
    /**
     * 옵션 유효성 검사
     * @returns {boolean}
     */
    valid : function(isCopy) {
        if (!optListGrid.gridView.getColumnProperty('opt1Val', 'visible')) {
            return $.jUtil.alert('옵션단계를 설정해주세요.');
        }

        if (!isCopy && $.jUtil.isEmpty($('#opt1Val').val())) {
            return $.jUtil.alert('옵션값을 입력해주세요.', 'opt1Val');
        }

        if ((isCopy || optListGrid.selectedRowId == null) && optListGrid.gridView.getItemCount() > 4) {
            return $.jUtil.alert('용도옵션은 최대 5개 까지 등록 가능합니다.');
        }

        return true;
    },
    /**
     * 옵션추가
     */
    setOpt : function() {
        if (item.opt.valid()) {
            let values = {
                optNo: $('#optNo').html(),
                opt1Val: $('#opt1Val').val(),
                opt2Val: $('#opt2Val').val(),
                optPrice: $('#optPrice').val(),
                stockQty: $('#optStockQty').val(),
                sellerOptCd: $('#sellerOptCd').val(),
                useYn: $('#optUseYn').val(),
                useYnNm: $('#optUseYn option:selected').text(),
            };

            if (optListGrid.selectedRowId == null) {
                optListGrid.dataProvider.addRow(values);
                item.opt.getDetail();
            } else {
                let row = optListGrid.gridView.getCurrent().itemIndex;

                optListGrid.dataProvider.updateRows(row, [values]);
            }

            $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));
        }
    },
    /**
     * 옵션삭제
     */
    deleteOpt : function() {
        optListGrid.dataProvider.removeRow(optListGrid.gridView.getCurrent().itemIndex);
        item.opt.getDetail();
        $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));
    },
    /**
     * 옵션복사
     */
    copyOpt : function() {
        if (item.opt.valid(true)) {
            let values = optListGrid.dataProvider.getValues(optListGrid.gridView.getCurrent().itemIndex);
            values[0] = "";
            optListGrid.dataProvider.addRow(values);
            $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));
        }
    },
    /**
     * 옵션 선택삭제
     * @returns {boolean}
     */
    deleteOptSelect : function() {
        let rowArr = optListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('삭제하려는 옵션을 선택해주세요.');
            return false;
        }

        if (confirm('해당 옵션을 삭제하시겠습니까?')) {

            for (let i in rowArr) {
                optListGrid.dataProvider.removeRow(rowArr[i] - parseInt(i));
            }

            $('#optTotalCount').text($.jUtil.comma(optListGrid.gridView.getItemCount()));

        }
    },
    setOpt1Title : function(opt1Title) {
        if (!$.jUtil.isEmpty(opt1Title)) {
            $('#opt1Title, #opt1TitleTxt').val(opt1Title);
            let opt1Column = optListGrid.gridView.columnByName("opt1Val");

            opt1Column.header.text = opt1Title;
            optListGrid.gridView.setColumn(opt1Column);
        }
    },
    /**
     * 옵션 핸들러 - 선택형옵션사용여부 따른 옵션 컨트롤
     * @param optSelUseYn
     * @param event
     * @param prodStatus
     */
    getOptHandler : function(optSelUseYn) {
        $('#listSplitDispYn').prop('checked', false);
        if (optSelUseYn == 'Y') {
            $('#optSelUseYnArea').show();
            $('#listSplitDispYn').closest('label').show();
        } else {
            $('#optSelUseYnArea').hide();
            $('#listSplitDispYn').closest('label').hide();
        }
    },
    /**
     * 선택형 옵션 타이틀 초기화
     */
    initOptTitle : function() {
        $('#optDepth').val(1);
        $('#opt1Title, #opt2Title').val('');

        let opt2Column = optListGrid.gridView.columnByName("opt2Val");

        optListGrid.gridView.setColumnProperty(opt2Column, "visible", false);
    }
};

item.sale = {
    /**
     * 상품유형 변경 이벤트
     * @param obj
     */
    changeItemType : function(obj) {
        switch ($(obj).val()) {
            case 'P':
                item.sale.setCartLimitYn('Y');
                break;
            default :
                if (item.sub.dcateCartYn == 'N') {
                    item.sale.setCartLimitYn('N');
                }

                break;
        }
    },
    /**
     * 성인상품유형 변경 컨트롤
     * @param obj
     */
    changeAdultType : function(obj) {
        switch ($(obj).val()) {
            case 'NORMAL':
                $('#imgDispYn').val('Y').prop('disabled', true);
                $('input[name="giftYn"]').prop('disabled', false);
                break;
            case 'ADULT':
                if (item.sub.imgDispYn == 'Y') {
                    $('#imgDispYn').val('N').prop('disabled', false);
                } else {
                    $('#imgDispYn').val('N').prop('disabled', true);
                }
                $('input[name="giftYn"][value="Y"]').prop('disabled', true);
                $('input[name="giftYn"][value="N"]').trigger('click');
                break;
            case 'LOCAL_LIQUOR':
                $('#imgDispYn').val('Y').prop('disabled', true);
                $('input[name="giftYn"][value="Y"]').prop('disabled', true);
                $('input[name="giftYn"][value="N"]').trigger('click');
                break;
        }
    },
    /**
     * 장바구니 제한 UI적용
     * @param cartLimitYn
     */
    setCartLimitYn : function(cartLimitYn) {
        if (cartLimitYn == 'Y') {
            $('input[name="cartLimitYn"][value="Y"]').trigger('click');
            $('input[name="cartLimitYn"][value="N"]').prop('disabled', true);
        } else {
            if ($('#itemType').val() != 'P') {
                $('input[name="cartLimitYn"]').prop('disabled', false);
                $('input[name="cartLimitYn"][value="N"]').trigger('click');
            }
        }
    },
    /**
     * 성인 인증 제한 UI적용
     * @param adultLimitYn
     */
    setAdultLimitYn : function(adultLimitYn, imgDispYn) {
        if (adultLimitYn == 'Y') {
            $('input[name="adultType"]').prop('disabled', true);
            $('input[name="adultType"][value="ADULT"]').prop('disabled', false).trigger('click');
            item.sub.imgDispYn = 'N';

            if (imgDispYn) {
                $('#imgDispYn').val(imgDispYn);
            }
        } else {
            $('input[name="adultType"]').prop('disabled', false);
            $('input[name="adultType"][value="NORMAL"]').trigger('click');
            item.sub.imgDispYn = 'Y';
        }
    }
};

item.ext = {
    event : function() {
        $('.isCertRadio').on('change', function(){
            item.ext.changeCert($(this));
        });

        $('#certTable').on('change', '.certType', function() {
            if ($.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').show();
                $(this).closest('.form').find('.energy').hide();
                $(this).closest('.form').find('.certCheck').show();
            } else {
                if ($(this).find('option:selected').data('ref1') == 'selectbox') {
                    $(this).closest('.form').find('.certNo').hide();
                    $(this).closest('.form').find('.energy').show();
                } else {
                    $(this).siblings('.certNo').prop('placeholder', $(this).find('option:selected').data('ref1'));
                    $(this).closest('.form').find('.certNo').show();
                    $(this).closest('.form').find('.energy').hide();
                }

                let ref2 = $(this).find('option:selected').data('ref2');
                if ($.jUtil.isEmpty(ref2)) {
                    $(this).closest('.form').find('.certNo').hide();
                } else if(ref2 == '1') {
                    $(this).closest('.form').find('.certNo').show();
                }

                let ref3 = $(this).find('option:selected').data('ref3');
                if (!$.jUtil.isEmpty(ref3)) {
                    $(this).closest('.form').find('.certCheck').hide();
                } else {
                    $(this).closest('.form').find('.certCheck').show();
                }
            }

        });

        $('#certTable').on('change', '.energy', function() {
            if (!$.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').val($(this).val());
            }
        });

        $('.certAdd').on('click', function() {

            if ($(this).closest('td').find('.isCertRadio:checked').val() == 'Y' && $(this).closest('td').find('.certType').length < 20) {
                $(this).closest('td').append(
                        '<div id="cert_'+$('.addObj').length+'" class="addObj ui form inline mg-t-10"><input type="hidden" name="certList[].isCert" value="Y" ><input type="hidden" name="certList[].itemCertSeq" class="itemCertSeq"><input type="hidden" name="certList[].certGroup" value="'
                        + $(this).closest('td').find('.certGroup').val() + '">'
                        + $(this).siblings('.addArea').html() + '</div>');
            }
        });
    },
    changeCert : function(obj) {
        let _tr = obj.closest('tr');

        _tr.find('.isCert').val(obj.val());

        switch (obj.val()) {
            case 'Y':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'E':
                _tr.find('.certExemptType, .certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'N':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', true).prop('readonly', true).val('');
                _tr.find('.addObj').remove();
                _tr.find('.certNo').prop('placeholder','');
                break;
        }
    },
    setSafetyUI : function (data) {
        let itemCertTypeNmArr = [];
        let itemCertGroupNmArr = [];

        itemCertType.forEach(element => itemCertTypeNmArr[element.mcCd] = element.mcNm);
        itemCertGroup.forEach(element => itemCertGroupNmArr[element.mcCd] = element.mcNm);

        let innerSafetyHtml = '';

        if (data.certList.length > 0) {
            data.certList.forEach(element => innerSafetyHtml += '<tr><th>' + itemCertGroupNmArr[element.certGroup] + '</th><td>' + itemCertTypeNmArr[element.certType] + ' ' + element.certNo + '</td></tr>');
        }

        if (!$.jUtil.isEmpty(innerSafetyHtml)) {
            $('#safetyTbodyArea').html(innerSafetyHtml);
        } else {
            $('#safetyTbodyArea').html(item.safetyHtml);
        }
    }
};

item.sub = {
    dcateCartYn : 'N',
    imgDispYn   : 'Y',
    changeCate4 : function(obj) {
        if ($(obj).val()) {
            CommonAjax.basic({
                url: '/common/category/getCategoryDivide.json',
                data: {dcateCd: $(obj).val()},
                method: 'GET',
                callbackFunc: function (res) {
                    //장바구니 제한
                    item.sub.dcateCartYn = res.cartYn;
                    item.sale.setCartLimitYn(res.cartYn);

                    //19성인인증 제한
                    item.sale.setAdultLimitYn(res.adultLimitYn);

                    item.sale.changeAdultType($('input[name="adultType"]:checked'));
                }
            });
        }
    }
};