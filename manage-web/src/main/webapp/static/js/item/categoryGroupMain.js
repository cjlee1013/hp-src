/**
 * 카테고리관리 > 대대분류관리
 */
$(document).ready(function() {
    categoryGroupListGrid.init();
    categoryGroupLcateListGrid.init();
    categoryGroup.init();
    categoryGroupLcate.init();
    CommonAjaxBlockUI.global();

});

var categoryGroupLcateListGrid = {

    gridView : new RealGridJS.GridView("categoryGroupLcateListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        categoryGroupLcateListGrid.initGrid();
        categoryGroupLcateListGrid.initDataProvider();
        categoryGroupLcateListGrid.event();

    },
    initGrid : function() {

        categoryGroupLcateListGrid.gridView.setDataSource(categoryGroupLcateListGrid.dataProvider);
        categoryGroupLcateListGrid.gridView.setStyles(categoryGroupLcateListGridBaseInfo.realgrid.styles);
        categoryGroupLcateListGrid.gridView.setDisplayOptions(categoryGroupLcateListGridBaseInfo.realgrid.displayOptions);
        categoryGroupLcateListGrid.gridView.setColumns(categoryGroupLcateListGridBaseInfo.realgrid.columns);
        categoryGroupLcateListGrid.gridView.setOptions(categoryGroupLcateListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        categoryGroupLcateListGrid.dataProvider.setFields(categoryGroupLcateListGridBaseInfo.dataProvider.fields);
        categoryGroupLcateListGrid.dataProvider.setOptions(categoryGroupLcateListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        categoryGroupLcateListGrid.gridView.onDataCellClicked = function(gridView, index) {
            categoryGroupLcate.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        categoryGroupLcateListGrid.dataProvider.clearRows();
        categoryGroupLcateListGrid.dataProvider.setRows(dataList);

    },
    delDataList : function(dataList){
        categoryGroupLcateListGrid.dataProvider.removeRows(dataList,false);
    },
    delData : function(data){
        categoryGroupLcateListGrid.dataProvider.removeRow(data);
    },
    addData : function (data) {
        categoryGroupLcateListGrid.dataProvider.addRow(data);
    },

};
// 카테고리그룹 그리드
var categoryGroupListGrid = {

    gridView : new RealGridJS.GridView("categoryGroupListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        categoryGroupListGrid.initGrid();
        categoryGroupListGrid.initDataProvider();
        categoryGroupListGrid.event();

    },
    initGrid : function() {

        categoryGroupListGrid.gridView.setDataSource(categoryGroupListGrid.dataProvider);
        categoryGroupListGrid.gridView.setStyles(categoryGroupListGridBaseInfo.realgrid.styles);
        categoryGroupListGrid.gridView.setDisplayOptions(categoryGroupListGridBaseInfo.realgrid.displayOptions);
        categoryGroupListGrid.gridView.setColumns(categoryGroupListGridBaseInfo.realgrid.columns);
        categoryGroupListGrid.gridView.setOptions(categoryGroupListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        categoryGroupListGrid.dataProvider.setFields(categoryGroupListGridBaseInfo.dataProvider.fields);
        categoryGroupListGrid.dataProvider.setOptions(categoryGroupListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        categoryGroupListGrid.gridView.onDataCellClicked = function(gridView, index) {
            categoryGroup.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        categoryGroupListGrid.dataProvider.clearRows();
        categoryGroupListGrid.dataProvider.setRows(dataList);

    },


};
// 카테고리그룹관리
var categoryGroup = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(categoryGroup.search);
        $('#searchResetBtn').bindClick(categoryGroup.searchFormReset);
        $('#resetBtn').bindClick(categoryGroup.resetForm);
        $('#setBtn').bindClick(categoryGroup.setcategoryGroup);
        $('#gcateNm').calcTextLength('keyup', '#gcateNmCnt');
        $('input[name="fileArr"]').change(function() {
            categoryGroup.img.addImg();
        });
        //정적 카테고리 생성
        $('#setStaticCategoryBtn').bindClick(categoryGroup.setStaticCategory);
    },
    /**
     * 카테고리그룹 검색
     */
    search : function() {

        CommonAjax.basic({url:'/item/category/getGroupList.json?' + $('#categoryGroupSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                categoryGroup.resetForm();
                categoryGroupListGrid.setData(res);
                $('#noticeTotalCount').html($.jUtil.comma(categoryGroupListGrid.gridView.getItemCount()));
            }});
    },

    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schType').val('GCATENM');
        $("#schKeyword").val('');
        $('#schUseYn').val('');
    },

    /**
     * 카테고리그룹 등록/수정
     */
    setcategoryGroup : function() {

        if(categoryGroup.setCategoryGrouopValid()){
            var setCategroup = $('#categoryGroupSetForm').serializeObject(true);
            setCategroup.lcateList  = categoryGroupLcateListGrid.dataProvider.getJsonRows();
            CommonAjax.basic({url:'/item/category/setCategoryGroup.json',data: JSON.stringify(setCategroup), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                    alert(res.returnMsg);
                    categoryGroup.search();
                }});
        }

    },


    //대대 카테고리 등록 유효성 검사
    setCategoryGrouopValid : function(){

        var objectArray = [$('#gcateNm'), $('#priority')];
        var saveYn = true;
        var setCategroup = $('#categoryGroupSetForm').serializeObject(true);
        var lCateList =  categoryGroupLcateListGrid.dataProvider.getJsonRows();

        var lcateGroupList  = new Object();
        lcateGroupList.gcateCd = setCategroup.gcateCd;
        lcateGroupList.lcateList =  lCateList;


        //필수 입력 값 유효성 검사
        if($.jUtil.isEmptyObjectArray(objectArray)){
            alert("필수 입력 항목을 선택/입력 해주세요." );
            return false;
        }

        //이미지 등록 여부 조회
        for(let i in setCategroup.imgList) {
            let f = setCategroup.imgList[i];
            if(f.imgUrl == '' || f.imgUrl == 'undefined'){

                alert("이미지를 등록해 주세요.");
                return false;
            }

        }

        if(lCateList.length > 0){

        //하위 카테고리가 다른 대대 카테고리에 등록 되어 있는지 확인
            CommonAjax.basic({url:'/item/category/checkLargeCategory.json',data: JSON.stringify(lcateGroupList), method:"POST", successMsg:null, contentType:"application/json", async:false ,callbackFunc:function(res) {

                var existCate  = '등록 된 카테고리가 있습니다.\n대분류 이동시 기존 대대분류에서 삭제 후 신규대대분류로 추가 됩니다.\n등록 하시겠습니까?\n';

                if(res.length > 0){
                    for (let i in res) {
                        existCate += '대대분류명 : '+ res[i].gcateNm +'  대카테고리ID : '+ res[i].lcateCd +'\n'
                    }
                    if(!confirm(existCate)){
                        saveYn = false;
                    }
                }

            }});
        }
        return saveYn;



    },
    /**
     * 대대분류 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#gcateCd, #gcateNm, #priority').val('');
        $('#useYn').val('Y');
        categoryGroup.img.resetImg();
        categoryGroupLcateListGrid.dataProvider.clearRows();
        $('#largeCategory').val('').change();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        categoryGroup.resetForm();

        var rowDataJson = categoryGroupListGrid.dataProvider.getJsonRow(selectRowId);
        $('#gcateCd').val(rowDataJson.gcateCd);
        $('#gcateNm').val(rowDataJson.gcateNm);
        $('#gcateNmCnt').html(rowDataJson.gcateNm.length);
        $('#useYn').val(rowDataJson.useYn);
        $('#priority').val(rowDataJson.priority);

        CommonAjax.basic({url:'/item/category/getGroupDetail.json?gcateCd=' + rowDataJson.gcateCd, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {

                categoryGroupLcateListGrid.setData(res.categoryGroupLcateGetDtoList);
                let imgList  =  res.categoryGroupImgGetDtoList;
                for (let i in imgList) {
                    categoryGroup.img.setImg($('#'+imgList[i].imgType), {
                        'imgNm' :  imgList[i].imgNm,
                        'imgUrl': imgList[i].imgUrl,
                        /*'src'   : hmpImgUrl + imgList[i].imgUrl+"?"+Math.floor(Math.random()*1000)*/
                        'src'    : hmpImgUrl + "/provide/view?processKey=" +categoryGroup.img.getProcessKey(imgList[i].imgType)+"&fileId=" + imgList[i].imgUrl

                    });
                }

            }});

    },

    /**
     * 정적 카테고리 생성
      */
    setStaticCategory: function () {
        CommonAjax.basic({
            url: '/common/category/setStaticCategory.json',
            data: { },
            method: 'get',
            callbackFunc: function (resData) {
                alert(resData.returnMsg);
            }
        });
    },
};
var categoryGroupLcate ={

    init : function() {
        this.bindingEvent();
        $('#largeCategory').change();
    },
    bindingEvent : function() {
        $('#saveLcate').bindClick(categoryGroupLcate.saveLcater);
        $("#largeCategory").change(function() {
            categoryGroupLcate.setLcateInfo($(this));
        });
    },

    /**
     * 하위 대분류 추가 또는 삭제
     */
    saveLcater : function(){

        let obj  = $('#largeCategory');
        let addObj = new Object();
        let existYn ='N';
        let selectRow;
        //그리드 기본 정보 세팅
        addObj.useYnTxt = obj.find("option:selected").data("useyntxt");
        addObj.lcateNm = obj.find("option:selected").data("lcatenm");
        addObj.lcateCd = obj.find("option:selected").val();
        addObj.dispYnTxt = obj.find("option:selected").data("dispyntxt");
        addObj.regNm = obj.find("option:selected").data("regnm");
        addObj.chgNm = obj.find("option:selected").data("chgnm");
        addObj.regDt = obj.find("option:selected").data("regdt");
        addObj.chgDt = obj.find("option:selected").data("chgdt");


        if(addObj.lcateCd == '' || addObj.lcateCd == 'undefined'){
            alert("카테고리를 선택해 주세요.");
            return false;
        }

        var gridList = categoryGroupLcateListGrid.dataProvider.getJsonRows();
        gridList.forEach(function (item, index) {
            if(item.lcateCd == addObj.lcateCd){
                selectRow = index;
                existYn='Y';
                return false;
            }
        });

        if($('#lCateuseYn').val() == 'addCate'){

            if(existYn == 'Y'){
                alert("기 등록된 카테고리 입니다.");
                return false;
            }else{
                categoryGroupLcateListGrid.addData(addObj)
            }

        }else{
            if(existYn == 'Y'){
                categoryGroupLcateListGrid.delData(selectRow);
            }else{
                alert("등록된 카테고리 아닙니다.");
                return false;
            }

        }

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = categoryGroupLcateListGrid.dataProvider.getJsonRow(selectRowId);
        $('#largeCategory').val(rowDataJson.lcateCd).change();
    },
    /**
     * 하위 대분류 섹렉트 박스 선택시
     */
    setLcateInfo : function(obj) {

        $("#lcateuseYnTxt").val(obj.find("option:selected").data("useyntxt"));
        $("#lCatedispYnTxt").val(obj.find("option:selected").data("dispyntxt"));
    }


    };
categoryGroup.img = {
    imgDiv : null,
    imgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        categoryGroup.img.resetImg();
        categoryGroup.img.imgDiv = null;
    },
    getProcessKey : function(imgType){
        var processKey ="CategoryGroup1";

        switch (imgType) {
            case 'CATEON' :
            case 'CATEOFF' :
                processKey ="CategoryGroup1"
                break;
            case 'SBP' :
                processKey ="CategoryGroup2"
                break;
            case 'SBM' :
                processKey ="CategoryGroup3"
                break;

        }
        return processKey;

    },

    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ categoryGroup.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');

        var params = {
            processKey : categoryGroup.img.getProcessKey(categoryGroup.img.imgType),
            mode : 'IMG'
        };


        CommonAjax.upload({
            file: file,
            data:params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        categoryGroup.img.setImg(categoryGroup.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            /*'src'   : hmpImgUrl + f.fileStoreInfo.fileId+"?"+Math.floor(Math.random()*1000)
                            hmpImgUrl + "/provide/view?processKey="*/
                            'src'    : hmpImgUrl + "/provide/view?processKey=" +params.processKey+"&fileId=" + f.fileStoreInfo.fileId
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        categoryGroup.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        categoryGroup.img.imgType = imgType;
        categoryGroup.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    }
};