/**
 * 상품속성 (셀러상품 등록/수정, 점포상품 기본정보) 사용
 */
item.attr  = {
    init : function() {
        item.attr.initAttr();
        item.attr.initAttrTag();
    },
    initAttr : function() {
        $('#attrArea').html('');
        $('#attrArea').closest('table').hide();
    },
    initAttrTag : function() {
        $('#selectedTags').html('');
    },
    event : function() {
        $('#schTagTbodyArea').on('click', '.show-select li', function() {
            item.attr.setAttrTag($(this).closest('ul').prop('id'), $(this).data('attr-no'), $(this).text(), $(this).closest('ul').data('multi-yn'));
        });

        $('#selectedTags').on('click', '.close', function() {
            $(this).closest('.span-button').remove();
        });

        $('[id^=uploadProofFileSpan_]').on('click', '.close', function() {
            if (confirm('등록된 파일을 삭제하시겠습니까?')) {
                $(this).closest('.proofFile').remove();
            }
        });

        $('#cateCd3').on('change', function() {
            item.attr.getAttrList("ATTR", '', $(this).val());
        });

        $('#gnoticeNo').on('change', function() {
            item.attr.getMngNoticeList($(this).val());
        });

        $('#addNoticeArea').on('change', '.checkComment', function() {
            if ($(this).is(':checked')) {
                $(this).closest('td').find('input[name="noticeList[].noticeDesc"]').val($(this).data('comment'));
            } else {
                $(this).closest('td').find('input[name="noticeList[].noticeDesc"]').val('');
            }
        });

        $('#checkboxAllNoticeExp').on('change', function() {
            let checked = $(this).is(':checked');

            $('#addNoticeArea').find('input:checkbox').each(function(){
                if (checked == true && !$(this).is(':checked')) {
                    $(this).trigger('click');
                } else if (checked == false && $(this).is(':checked')) {
                    $(this).trigger('click');
                }
            });
        });

        $('html').click(function(e) {
            if ($('#searchBrandLayer').css('display') != 'none') {
                $('#searchBrandLayer').hide();
            }

            if ($('#searchMakerLayer').css('display') != 'none') {
                $('#searchMakerLayer').hide();
            }
        });

    },
    /**
     * 속성 리스트 조회 및 UI셋팅 (상품속성/검색태그)
     * @param gattrType
     * @param itemNo
     * @param scateCd
     * @returns {boolean}
     */
    getAttrList : function (gattrType, itemNo, scateCd) {
        if (gattrType == "ATTR" && $.jUtil.isEmpty(scateCd)) {
            item.attr.initAttr();
            return false;
        }

        CommonAjax.basic({
            url: '/item/attribute/getAttributeItemList.json',
            data: {
                gattrType: gattrType,
                itemNo: itemNo,
                scateCd: scateCd
            },
            method: 'GET',
            callbackFunc: function (res) {
                let trIdx = 0;
                let selectIdx = 0;
                let gattrNo = 0;
                let checkLast = 0;
                let checkBoxBrCnt = 0;
                let innerHtml = "";
                let selected = '';
                let checked = '';

                if (gattrType == "ATTR") {
                    $('#attrArea').html('');
                    $('#attrArea').closest('table').show();

                    if (res.length == 0) {
                        item.attr.initAttr();
                        return false;
                    }

                    for (let i in res) {
                        if (res[i].useYn == 'Y') {
                            selected = 'selected';
                            checked = 'checked';
                        } else {
                            selected = '';
                            checked = '';
                        }

                        if ((checkLast % 2) == 0) {
                            innerHtml = '<tr id="attrTr'+trIdx+'"></tr>';
                            $('#attrArea').append(innerHtml);

                            trIdx++;
                        }

                        if (res[i].multiYn == 'Y') { //checkBox 생성
                            checkBoxBrCnt++;

                            if (gattrNo != res[i].gattrNo) {
                                gattrNo = res[i].gattrNo;
                                checkLast++;

                                innerHtml = '<th>' + res[i].gattrMngNm + '</th>'
                                        + '<td id="attrSelect'+selectIdx+'" class="ui form inline">'
                                        + '</td>';

                                $('#attrTr'+ (trIdx-1)).append(innerHtml);

                                selectIdx++;
                            }

                            innerHtml = '<label class="ui checkbox mg-t-5">'
                                    + '<input type="checkbox" name="attrNo" value="' + res[i].attrNo + '" '+checked+'><span class="text">' + res[i].attrNm + '</span>'
                                    + '</label>';

                            if (checkBoxBrCnt % 5 == 0) {
                                innerHtml += '<br>';
                            }
                        } else { //selectBox 생성
                            if (gattrNo != res[i].gattrNo) {
                                gattrNo = res[i].gattrNo;
                                checkLast++;

                                innerHtml = '<th>' + res[i].gattrMngNm + '</th>'
                                        + '<td>'
                                        + '<select style="width: 180px" name="attrNo" id="attrSelect'+selectIdx+'" class="ui input medium">'
                                        + '<option value="">선택</option>'
                                        + '</select>'
                                        + '</td>';

                                $('#attrTr'+ (trIdx-1)).append(innerHtml);

                                selectIdx++;
                            }

                            innerHtml = '<option value="' + res[i].attrNo + '" '+selected+'>' + res[i].attrNm + '</option>';
                        }

                        $('#attrSelect' + (selectIdx-1)).append(innerHtml);
                    }

                    if (checkLast % 2 != 0) {
                        $('#attrSelect' + (selectIdx-1)).prop('colspan', '3');
                    }
                } else {
                    $('#schTagTbodyArea, #selectedTags').html('');

                    let basicIdNo = 0;

                    for (let i in res) {

                        if ($.jUtil.isEmpty(res[i].attrNm)) {
                            continue;
                        }

                        if (gattrNo != res[i].gattrNo) {
                            gattrNo = res[i].gattrNo;
                            if ((checkLast % 6) == 0) {
                                basicIdNo++;
                                innerHtml = '<tr id="schTagThArea_'+basicIdNo+'"></tr><tr id="schTagTdArea_'+basicIdNo+'"></tr>';
                                $('#schTagTbodyArea').append(innerHtml);
                            }

                            checkLast++;

                            innerHtml = '<th width="16%">' + res[i].gattrNm + '</th>';

                            $('#schTagTbodyArea').find('#schTagThArea_' + basicIdNo).append(innerHtml);

                            innerHtml = '<td>'
                                    + '<ul id="gattrNo'+res[i].gattrNo+'" class="show-select" data-multi-yn="'+res[i].multiYn+'" style="overflow-x: hidden">'
                                    + '</ul>'
                                    + '</td>';

                            $('#schTagTbodyArea').find('#schTagTdArea_' + basicIdNo).append(innerHtml);
                        }

                        innerHtml = '<li data-attr-no="' + res[i].attrNo + '">' + res[i].attrNm + '</li>';
                        $('#gattrNo'+res[i].gattrNo).append(innerHtml);

                        if (res[i].useYn == 'Y') {
                            item.attr.setAttrTag('gattrNo'+res[i].gattrNo, res[i].attrNo, res[i].attrNm, res[i].multiYn);
                        }

                    }

                }
            }
        });
    },
    /**
     * 속성리스트 추출 (상품속성/검색태그)
     * @returns {[]}
     */
    getAttrNoList : function() {
        let attrNoList = [];

        $("input[name='attrNo']:checked, select[name='attrNo'], [id^='selectedgattrNo']").each(function() {
            if (!$.jUtil.isEmpty($(this).val())) {
                attrNoList.push($(this).val());
            } else if (!$.jUtil.isEmpty($(this).data('attr-no'))) {
                attrNoList.push($(this).data('attr-no'));
            }
        });

        return attrNoList;
    },
    /**
     * 검색태그 설정 (선택한 태그)
     * @param gattrNoId
     * @param attrNo
     * @param attrTxt
     */
    setAttrTag : function(gattrNoId, attrNo, attrTxt, multiYn) {
        let selectedId = 'selected'+gattrNoId;

        if (multiYn == 'Y') {
            selectedId += attrNo;
        }

        if($('#'+selectedId).length != 0) {
            $('#'+selectedId).remove();
        }

        $('#selectedTags').append('<span id="' + selectedId +'" class="ui span-button mg-l-5" data-attr-no="'+attrNo+'"># '+attrTxt+'('+attrNo+') <span class="text close">[X]</span></span>');
    },
    /**
     * 브랜드 조회
     * @returns {boolean}
     */
    getBrand : function() {
        let brandNm = $('#brandNm').val();

        if (0 == brandNm.length) {
            alert('검색 키워드는 1자 이상 입력해주세요.');
            $('#brandNm').focus();
            return false;

        } else if ($('#brandNm').val() && $.jUtil.isNotAllowInput($('#brandNm').val(), ['SPC_SCH'])) {
            alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
            $.jUtil.patternReplace($('#brandNm'), ['SPC_SCH'], "");
            $('#brandNm').focus();
            return false;
        }

        CommonAjax.basic({url:'/item/brand/getBrandListByNm.json', data:{brandNm:brandNm}, method:'GET', callbackFunc:function(res) {
                let resultCount = res.length;

                if (resultCount == 0) {
                    $('#searchBrandLayer').html('');
                    $('#searchBrandLayer').append(
                            '<li class="ui-menu-item-wrapper" >검색결과가 없습니다.</li>');

                } else {
                    $('#searchBrandLayer').html('');

                    let searchResult;

                    for (let idx = 0; resultCount > idx; idx++) {
                        let data = res[idx];
                        let brandInfo = new Array();
                        brandInfo.push(data.brandNm);
                        if (data.brandNmEng) {
                            brandInfo.push(data.brandNmEng);
                        }
                        brandInfo.push(data.brandNo);

                        searchResult = brandInfo.join(' | ');

                        $('#searchBrandLayer').append(
                                '<li class="ui-menu-item-wrapper" id="brandSearchList_'
                                + data.brandNo + '"' + '">' + searchResult
                                + '</li>');
                    }

                    item.attr.addBrand();
                }

                $('#searchBrandLayer').show();
            }
        });
    },
    /**
     * 브랜드 추가
     */
    addBrand : function() {
        $('li[id^=brandSearchList_]').click(function() {
            let idArr = $(this).attr('id').split('_');
            let htmlArr = $(this).html().split('|');
            let brandNo = idArr[1];
            let brandNm = htmlArr[0];

            item.attr.addBrandHtml(brandNo, $.trim(brandNm));
            $('#searchBrandLayer').hide();
        });
    },
    /**
     * 브랜드 추가 html
     * @param brandNo
     * @param brandNm
     */
    addBrandHtml : function(brandNo, brandNm) {
        if (brandNo && brandNm) {
            item.attr.setBrandYn('Y');

            $('#brandNo').val(brandNo);
            $('#brandNm').val(brandNm);
            $('#spanBrandNm').html(brandNm);
        } else {
            item.attr.setBrandYn('N');
        }
    },
    /**
     * 브랜드 radio
     * @param val
     */
    setBrandYn : function(val) {
        if (val == 'N') {
            $('#brandArea').hide();
            $('#brandNo, #brandNm').val('');
            $('#spanBrandNm').html('');
        } else {
            $('#brandArea').show();
            $('#brandNo').val(0);
        }
    },
    /**
     * 제조사 조회
     * @returns {boolean}
     */
    getMaker : function() {
        let makerNm = $('#makerNm').val();

        if (0 == makerNm.length) {
            alert('검색 키워드는 1자 이상 입력해주세요.');
            $('#makerNm').focus();
            return false;
        } else if ($('#makerNm').val() && $.jUtil.isNotAllowInput($('#makerNm').val(), ['SPC_SCH'])) {
            alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
            $.jUtil.patternReplace($('#makerNm'), ['SPC_SCH'], "");
            $('#makerNm').focus();
            return false;
        }

        CommonAjax.basic({url:'/item/maker/getMakerListByNm.json', data:{makerNm:makerNm}, method:'GET', callbackFunc:function(res) {
                let resultCount = res.length;

                if (resultCount == 0) {
                    $('#searchMakerLayer').html('');
                    $('#searchMakerLayer').append(
                            '<li class="ui-menu-item-wrapper" >검색결과가 없습니다.</li>');

                } else {
                    $('#searchMakerLayer').html('');

                    let searchResult;

                    for (let idx = 0; resultCount > idx; idx++) {
                        let data = res[idx];
                        let makeInfo = new Array();
                        makeInfo.push(data.makerNm);
                        if (data.makerNmEng) {
                            makeInfo.push(data.makerNmEng);
                        }
                        makeInfo.push(data.makerNo);

                        searchResult = makeInfo.join(' | ');

                        $('#searchMakerLayer').append(
                                '<li class="ui-menu-item-wrapper" id="makerSearchList_'
                                + data.makerNo + '">' + searchResult + '</li>');
                    }

                    item.attr.addMaker();
                }

                $('#searchMakerLayer').show();
            }
        });
    },
    /**
     * 제조사 추가
     */
    addMaker : function() {
        $('li[id^=makerSearchList_]').click(function() {
            let idArr = $(this).attr('id').split('_');
            let htmlArr = $(this).html().split('|');
            let makerNo = idArr[1];
            let makerNm = htmlArr[0];

            item.attr.addMakerHtml(makerNo, $.trim(makerNm));

            $('#searchMakerLayer').hide();
        });
    },
    /**
     * 제조사 추가 html
     * @param makerNo
     * @param makerNm
     */
    addMakerHtml : function(makerNo, makerNm) {
        if (makerNo && makerNm) {
            item.attr.setMakerYn('Y');

            $('#makerNo').val(makerNo);
            $('#makerNm').val(makerNm);
            $('#spanMakerNm').html(makerNm);
        } else {
            item.attr.setMakerYn('N');
        }

    },
    /**
     * 제조사 radio
     * @param val
     */
    setMakerYn : function(val) {
        if (val == 'N') {
            $('#makerArea').hide();
            $('#makerNo, #makerNm').val('');
            $('#spanMakerNm').html('');

        } else {
            $('#makerArea').show();
            $('#makerNo').val(0);
        }

        $('input:radio[name=makerYn]:input[value=' + val + ']').prop('checked', true);
    },
    getMngNoticeGroupList : function() {
        CommonAjax.basic({
            url: '/item/getMngNoticeGroupList.json',
            data: {
            },
            method: 'GET',
            callbackFunc: function (res) {
                for (let i in res) {
                    $('#gnoticeNo').append('<option value="'+res[i].gnoticeNo+'">' + res[i].gnoticeNm + '</option>');
                }
            }
        });
    },
    getMngNoticeList : function(gnoticeNo, noticeList) {
        $('#addNoticeArea').html('');
        $('#checkboxAllNoticeExp').prop('checked', false);

        if (gnoticeNo) {
            CommonAjax.basic({
                url: '/item/getMngNoticeList.json',
                data: {
                    gnoticeNo : gnoticeNo
                },
                method: 'GET',
                callbackFunc: function (res) {
                    $('#addNoticeData').show();

                    let idx = 0;
                    let trId = 0;

                    for (let i in res) {
                        let innerHtml = '';
                        idx++;
                        if (idx % 2 != 0) {
                            trId++;
                            innerHtml = '<tr id="noticeTr'+trId+'"></tr>';
                            $('#addNoticeArea').append(innerHtml);
                        }

                        innerHtml = '<th>'+res[i].noticeNm + '<br>' + res[i].noticeDetail + '</th>'
                                + '<td>'

                        if (!$.jUtil.isEmpty(res[i].commentTitle)) {
                            innerHtml += '<span class="pull-right mg-b-5">'
                                    + '<label class="ui checkbox mg-r-10">'
                                    + '<input type="checkbox" name="checkboxNoticeExp'+res[i].noticeNo+'" data-comment="'+res[i].commentNm+'" class="checkComment" value="Y"><span class="text">'+res[i].commentTitle+'</span>'
                                    + '</label>'
                                    + '</span>';
                        }

                        innerHtml += '<div class="ui form inline mg-t-20">'
                                + '<input type="hidden" name="noticeList[].noticeNo" value="'+res[i].noticeNo+'" >'
                                + '<input type="text" name="noticeList[].noticeDesc" id="noticeDesc'+res[i].noticeNo+'" class="ui input medium" maxlength="500">'
                                + '</div>'
                                + '</td>';

                        $('#noticeTr'+trId).append(innerHtml);
                    }

                    if (idx % 2 != 0) {
                        $('#noticeTr'+trId).append('<td></td><td></td>');
                    }

                    if (noticeList) {
                        for (let i in noticeList) {
                            $('#noticeDesc'+noticeList[i].noticeNo).val(noticeList[i].noticeDesc);
                        }
                    }
                }
            });
        } else {
            $('#addNoticeData').hide();
        }
    }
};

/**
 * 상품이미지 (셀러상품 등록/수정, 점포상품 기본정보) 사용
 * @type {{beforeMainId: null, init: item.img.init, imgDiv: null, setImg: item.img.setImg, deleteImg: item.img.deleteImg, addImg: item.img.addImg, imagePreview: item.img.imagePreview, clickFile: item.img.clickFile}}
 */
item.img = {
    imgDiv : null,
    beforeMainId : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        $('.uploadType').each(function(){ item.img.setImg($(this))});
        item.img.imgDiv = null;
        item.img.beforeMainId = null;
    },
    event : function() {
        // 등록 > 이미지 삭제
        $('.deleteImgBtn').click(function() {
            if (confirm('삭제하시겠습니까?')) {
                item.img.deleteImg($(this).siblings('.uploadType'));
            }
        });

        $('.uploadType').bindClick(item.img.imagePreview);

        $('#mainImgTest').on('click', function() {
            item.img.imagePreview($(this), '/it/' + $('#itemNo').val() + 's');
        });

        $('#mainImgTDTest').on('click', function() {
            item.img.imagePreview($(this), '/it/' + $('#itemNo').val() + 's');
        });

        $('#listImgTest').on('click', function() {
            item.img.imagePreview($(this), '/it/' + $('#itemNo').val() + 'r');
        });
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params, isDelete) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            if (!obj.find('.imgNo').val()) {
                obj.find('.imgNo').val(params.imgNo);
            }
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.changeYn').val(params.changeYn);
            obj.find('.imgUrlTag').prop('src', params.src);

            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.mainYn').val('N');
            obj.find('.imgUrl').val('');
            if (!isDelete) {
                obj.find('.imgNo').val('');
            }
            obj.find('.imgNm').val('');
            obj.find('.changeYn').val('N');
            obj.find('.imgUrlTag').prop('src', '');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function(id, fieldName, size, sizeMsg) {
        if (!size) {
            size = 4194304;
            sizeMsg = '4MB';
        }

        if ($('input[name="'+fieldName+'"]').get(0).files[0].size > size) {
            $('input[name="'+fieldName+'"]').val('');
            return $.jUtil.alert("용량을 초과하였습니다.\n" + sizeMsg + " 이하로 업로드해주세요.");
        } else if ($('input[name="'+fieldName+'"]').get(0).files[0].name.length > 45) {
            $('input[name="'+fieldName+'"]').val('');
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('input[name="'+fieldName+'"]');
        let params = {
            fieldName : fieldName,
            processKey : 'ItemMainImage',
            mode : 'IMG'
        };

        if (item.img.imgDiv.data('type') == 'LST') {
            params.processKey = 'ItemListImage';
        } else if(item.img.imgDiv.data('type') == 'INTRO') {
            params.processKey = 'ItemRelation';
        } else if(item.img.imgDiv.data('type') == 'LBL') {
            params.processKey = 'ItemLabelImage';
        }

        let ext = $('input[name="'+fieldName+'"]').get(0).files[0].name.split('.');

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        item.img.setImg($('#'+id), {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj, imgurl) {

        if (imgurl) {
            var imgUrl = hmpImgFrontUrl + imgurl;
        } else {
            var imgUrl = $(obj).find('img').prop('src');
        }
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(obj) {
        item.img.setImg(obj, null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(obj) {
        item.uploadType = 'IMG';
        item.img.imgDiv = obj.parents('.preview-image').find('.uploadType');
        $('input[name="'+obj.data('field-name')+'"]').click();
    }
};

item.file = {
    fileObj : null,
    /**
     * 증빙서류 전체 다운로드
     */
    proofFileTotalDownload : function() {
        let files = [];
        $('#proofFileTable .proofFile').each(function(){
            files.push({'url' : $(this).find('.fileUrl').val(), 'fileNm' : $(this).find('.fileNm').val(), 'processKey' : 'ItemFile'});
        });

        $.jUtil.downloadFiles(files);
    },
    /**
     * 파일 업로드 클릭 이벤트
     * @param obj
     */
    clickFile : function(obj, type) {
        item.uploadType = 'FILE';
        if (type == 'PROOF') {
            if ($('#uploadProofFileSpan_'+obj.data('file-type')).find('.span-button').length > 9) {
                return $.jUtil.alert("파일 추가는 최대 10개까지 가능합니다.");
            }

            item.file.fileObj = obj.closest('tr').find('#uploadProofFileSpan_'+obj.data('file-type'));
        } else {
            if (!$.jUtil.isEmpty($('#uploadVideoFileSpan').html())) {
                return $.jUtil.alert('동영상은 1개 이하로 등록 가능합니다.');
            }

            item.file.fileObj = obj.closest('tr').find('#uploadVideoFileSpan');
        }

        $('input[name="'+obj.data('field-name')+'"]').click();
    },
    /**
     * 파일업로드
     */
    addFile : function(id, fieldName) {
        let file = $('input[name="'+fieldName+'"]');
        let fileType = item.file.fileObj.data('file-type');
        let params = {};
        let fileNm = '';

        if (fileType == 'VIDEO') {
            params = {
                processKey : "ItemVideo",
                mode : "FILE",
                fieldName : fieldName,
                fileName : $('#itemNo').val() + '.' + $('input[name="'+fieldName+'"]').val().split('.').pop().toLowerCase()
            };
            fileNm = $('input[name="'+fieldName+'"]').get(0).files[0].name;

            if (1048576 < $('input[name="'+fieldName+'"]').get(0).files[0].size) {
                return $.jUtil.alert('업로드 파일 용량(1MB)을 초과 하였습니다.');
            }

        } else {
            params = {
                processKey : "ItemFile",
                fieldName : fieldName,
                mode : "FILE"
            };
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        if (fileType == 'VIDEO') {
                            item.file.appendVideoFile(f.fileStoreInfo, id, fileNm);
                        } else {
                            item.file.appendProofFile(f.fileStoreInfo, id);
                        }
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    appendVideoFile : function(f, id, fileNm) {
        if (id) { //등록
            let html = '<span class="ui text mg-r-10 videoFile">'
                    + '<input type="hidden" class="fileSeq" name="videoFile.fileSeq" value=""/>'
                    + '<input type="hidden" class="fileType" name="videoFile.fileType" value="'+item.file.fileObj.data('file-type')+'"/>'
                    + '<input type="hidden" class="fileType" name="videoFile.fileExt" value="'+f.fileName.split('.').pop().toLowerCase()+'"/>'
                    + '<input type="hidden" class="fileUrl" name="videoFile.fileUrl" value="'+f.fileId+'"/>'
                    + '<input type="hidden" class="fileNm" name="videoFile.fileNm" value="'+fileNm+'"/>'
                    + '<a href="'
                    + hmpImgUrl + "/provide/view?processKey=ItemVideo&fileId=" + f.fileId
                    + '" target="_blank" style="color:blue; text-decoration: underline; ">'
                    + fileNm
                    + '</a><span class="text close mg-l-5" style="cursor:pointer;">[X]</span></span>';

            $('#'+id).append(html);
        } else {
            let html = '<span class="ui text mg-r-10 videoFile">'
                    + '<input type="hidden" class="fileSeq" name="videoFile.fileSeq" value="'+f.fileSeq+'"/>'
                    + '<input type="hidden" class="fileType" name="videoFile.fileType" value="'+f.fileType+'"/>'
                    + '<input type="hidden" class="fileUrl" name="videoFile.fileUrl" value="'+f.fileUrl+'"/>'
                    + '<input type="hidden" class="fileNm" name="videoFile.fileNm" value="'+f.fileNm+'"/>'
                    + '<a href="'
                    + hmpImgUrl + "/provide/view?processKey=ItemVideo&fileId=" + f.fileUrl
                    + '" target="_blank" style="color:blue; text-decoration: underline; ">'
                    + f.fileNm
                    + '</a><span class="text close mg-l-5" style="cursor:pointer;">[X]</span></span>';

            $('#uploadVideoFileSpan').append(html);
        }
    },
    appendProofFile : function(f, id) {
        if (id) { //등록
            let html = '<span class="ui span-button text mg-r-10 proofFile">'
                    + '<input type="hidden" class="fileSeq" name="proofFileList[].fileSeq" value=""/>'
                    + '<input type="hidden" class="fileType" name="proofFileList[].fileType" value="'+item.file.fileObj.data('file-type')+'"/>'
                    + '<input type="hidden" class="fileUrl" name="proofFileList[].fileUrl" value="'+f.fileId+'"/>'
                    + '<input type="hidden" class="fileNm" name="proofFileList[].fileNm" value="'+f.fileName+'"/>'
                    + '<span class="proofDownLoad" data-url="'+ f.fileId + '" '
                    + 'data-process-key="ItemFile" '
                    + 'data-file-nm="' + f.fileName + '" '
                    + '" target="_blank" style="color:blue; text-decoration: underline; cursor: pointer;">'
                    + f.fileName
                    + '</span><span class="text close mg-l-5">[X]</span></span>';

            $('#'+id).append(html);
        } else { //조회
            let html = '<span class="ui span-button text mg-r-10 proofFile">'
                    + '<input type="hidden" class="fileSeq" name="proofFileList[].fileSeq" value="'+f.fileSeq+'"/>'
                    + '<input type="hidden" class="fileType" name="proofFileList[].fileType" value="'+f.fileType+'"/>'
                    + '<input type="hidden" class="fileUrl" name="proofFileList[].fileUrl" value="'+f.fileUrl+'"/>'
                    + '<input type="hidden" class="fileNm" name="proofFileList[].fileNm" value="'+f.fileNm+'"/>'
                    + '<span class="proofDownLoad" data-url="'+ f.fileUrl + '" '
                    + 'data-process-key="ItemFile" '
                    + 'data-file-nm="' + f.fileNm + '" '
                    + '" target="_blank" style="color:blue; text-decoration: underline; cursor: pointer;">'
                    + f.fileNm
                    + '</span><span class="text close mg-l-5">[X]</span></span>';

            $('#uploadProofFileSpan_'+f.fileType).append(html);
        }
    }
}

item.common = {
    event : function() {
        $('#itemMarket, #itemAffi').on('click', 'input[name="providerCheck"]', function() {

            if ($(this).is(':checked')) {
                $(this).siblings('input[name="providerList[].useYn"]').val('Y');
            } else {
                $(this).siblings('input[name="providerList[].useYn"]').val('N');
            }
        });


        $('input[name="linkageYn"]').on('click', function() {
            item.common.changeLinkageYn($(this).val(), true);
        });

        $('#itemHist').on('click', function() {
            windowPopupOpen('/item/popup/itemHistPop?itemNo=' + $('#itemNo').val() + '&storeType=' + $('#itemHist').data('store-type'), "itemHistPop", 1600, 800);
        });

        $('#proofFileTable').on('click', '.proofDownLoad', function() {
            $.jUtil.downloadFile($(this).data('url'), $(this).data('file-nm'), $(this).data('process-key'));
        });
    },
    getItemProviderList : function () {
        CommonAjax.basic({
            url: '/item/getItemProviderList.json',
            data: {},
            method: 'GET',
            callbackFunc: function (res) {
                let innerHtmlMarket = '';
                let innerHtmlAffi = '';

                for (let i in res) {
                    let innerHtml =
                            '<label class="ui checkbox checkbox-t">'
                            + '<input type="checkbox" name="providerCheck" value="Y">'
                            + '<span class="text">'
                            + res[i].providerNm
                            + '</span>'
                            + '<input type="hidden" name="providerList[].useYn" value="N" />'
                            + '<input type="hidden" name="providerList[].providerId" value="'
                            +  res[i].providerId
                            + '" />'
                            + '<input type="hidden" name="providerList[].providerType" value="'
                            +  res[i].providerType
                            + '" />'
                            + '<input type="hidden" name="providerList[].partnerType" value="'
                            +  res[i].partnerType
                            + '" />'
                            + '</label>';
                    if (res[i].partnerType == 'COOP') {
                        innerHtmlMarket += innerHtml;
                    } else {
                        innerHtmlAffi += innerHtml;
                    }
                }

                $('#itemMarket').html(innerHtmlMarket);
                item.common.changeLinkageYn('Y', true);
            }
        });
    },
    changeLinkageYn : function (val, isClick) {
        $('input[name="linkageYn"][value="'+val+'"]').prop('checked', true);

        if (val == 'Y') {
            $('#itemMarket').find('input[name="providerCheck"]').prop('disabled', false);
            $('#itemMarket').find('input[name="providerCheck"]').prop('checked', isClick);
            if (isClick) {
                $('#itemMarket').find('input[name="providerList[].useYn"]').val('Y');
            } else {
                $('#itemMarket').find('input[name="providerList[].useYn"]').val('N');
            }
        } else {
            $('#itemMarket').find('input[name="providerCheck"]').prop('checked', false).prop('disabled', true);
            $('#itemMarket').find('input[name="providerList[].useYn"]').val('N');
        }
    }
};

item.callBack = {
    schPartner : function(res) {
        $('#schPartnerId').val(res[0].partnerId);
        $('#schPartnerNm').val(res[0].partnerNm);
    }
};


