/**
 * 상품속성관리  > 카테고리별 속성관리
 */
$(document).ready(function() {
  attributeListGrid.init();
  categoryListGrid.init();
  categoryList.init();
  attributeList.init();
  commonCategory.init();
  CommonAjaxBlockUI.global();
});


var attributeListGrid = {
  gridView : new RealGridJS.GridView("attributeListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    attributeListGrid.initGrid();
    attributeListGrid.initDataProvider();
    attributeListGrid.event();
    attributeList.setAttributeGroupSelectBox();
  },
  initGrid : function() {
    attributeListGrid.gridView.setDataSource(attributeListGrid.dataProvider);

    attributeListGrid.gridView.setStyles(attributeListGridBaseInfo.realgrid.styles);
    attributeListGrid.gridView.setDisplayOptions(attributeListGridBaseInfo.realgrid.displayOptions);
    attributeListGrid.gridView.setColumns(attributeListGridBaseInfo.realgrid.columns);
    attributeListGrid.gridView.setOptions(attributeListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
    attributeListGrid.dataProvider.setFields(attributeListGridBaseInfo.dataProvider.fields);
    attributeListGrid.dataProvider.setOptions(attributeListGridBaseInfo.dataProvider.options);
  },
  event : function() {
    // 그리드 선택
      attributeListGrid.gridView.onDataCellClicked = function(gridView, index) {
        attributeList.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
    attributeListGrid.dataProvider.clearRows();
    attributeListGrid.dataProvider.setRows(dataList);
  }

};

// 카테고리 리스트 그리드
var categoryListGrid = {
  gridView : new RealGridJS.GridView("categoryListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    categoryListGrid.initGrid();
    categoryListGrid.initDataProvider();
    categoryListGrid.event();
    commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
    commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
    commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
  },
  initGrid : function() {
    categoryListGrid.gridView.setDataSource(categoryListGrid.dataProvider);

    categoryListGrid.gridView.setStyles(categoryListGridBaseInfo.realgrid.styles);
    categoryListGrid.gridView.setDisplayOptions(categoryListGridBaseInfo.realgrid.displayOptions);
    categoryListGrid.gridView.setColumns(categoryListGridBaseInfo.realgrid.columns);
    categoryListGrid.gridView.setOptions(categoryListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
    categoryListGrid.dataProvider.setFields(categoryListGridBaseInfo.dataProvider.fields);
    categoryListGrid.dataProvider.setOptions(categoryListGridBaseInfo.dataProvider.options);
  },
  event : function() {
    // 그리드 선택
      categoryListGrid.gridView.onDataCellClicked = function(gridView, index) {
        categoryList.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
    categoryListGrid.dataProvider.clearRows();
    categoryListGrid.dataProvider.setRows(dataList);
  }

};
// 카테고리리스트
var categoryList = {
  /**
   * 초기화
   */
  init : function() {
    this.bindingEvent();
  },
  /**
   * 이벤트 바인딩
   */
  bindingEvent : function() {
    $('#searchBtn').bindClick(categoryList.search);
    $('#searchResetBtn').bindClick(categoryList.searchFormReset);
  },

  /**
   * 카테고리 검색
   */
  search : function() {

    CommonAjax.basic({url:'/item/attribute/getCategoryList.json?' + $('#categorySearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
        attributeList.resetForm();
        attributeList.resetGrid();
        categoryListGrid.setData(res);
        $('#categoryTotalCount').html($.jUtil.comma(categoryListGrid.gridView.getItemCount()));
      }});
  },

  /**
   * 그리드 row 선택
   * @param selectRowId
   */
  gridRowSelect : function(selectRowId) {
    var rowDataJson = categoryListGrid.dataProvider.getJsonRow(selectRowId);
    $('#scateCd').val(rowDataJson.scateCd);
    attributeList.getAttributeList(rowDataJson.scateCd);
  },
  /**
   * 검색폼 초기화
   */
  searchFormReset : function() {
    $('#searchCateCd1').val('').change();
    $('#searchResistYn').val('');
  }
};


//속성분류 정보
var attributeList  ={
  /**
   * 초기화
   */
  init : function() {
    this.initAjaxForm();
    this.bindingEvent();
    this.resetForm();
  },
  /**
   * 속성분류 리스트 조회
   */
  getAttributeList : function(scateCd){
    CommonAjax.basic({url:'/item/attribute/getAttributeList.json?', data:{scateCd: scateCd}, method:"GET", successMsg:null, callbackFunc:function(res) {
        attributeList.resetForm();
        attributeListGrid.setData(res);
        $('#attributeTotalCount').html($.jUtil.comma(attributeListGrid.gridView.getItemCount()));
      }});
  },
  /**
   * 속성분류 등록/수정 Ajax 폼
   */
  initAjaxForm : function() {
    $('#setAttributeForm').ajaxForm({
      url: '/item/attribute/setAttribute.json',
      type: 'post',
      success: function(resData) {
        alert(resData.returnMsg);
        attributeList.resetForm();
        attributeList.getAttributeList($('#scateCd').val());
      },
      error: function(e) {
      if(e.responseJSON.data == null) {
          alert(e.responseJSON.returnMessage);
      } else {
          alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
        }
      }
    });
  },
  /**
   * 이벤트 바인딩
   */
  bindingEvent : function() {

    $('#resetAttributeBtn').bindClick(attributeList.resetForm);
    $('#setAttributeBtn').bindClick(attributeList.setAttribute);


  },
  resetForm : function() {
    $('#attrNo').val('');
    $('#selectAttrNo').val('');
    $('#priority').val('');
    $('#useYn').val('Y');
    $('#dispTypeTxt').val('');
    $('#selectAttrNo').prop( "disabled", false );

  },
  /**
   * 그리드 리스트 초기화
   */
  resetGrid : function(){
    attributeListGrid.dataProvider.clearRows();
    $('#attributeTotalCount').html($.jUtil.comma(attributeListGrid.gridView.getItemCount()));
  },
  /**
   * 그리드 row 선택
   * @param selectRowId
   */
  gridRowSelect : function(selectRowId) {
    var rowDataJson = attributeListGrid.dataProvider.getJsonRow(selectRowId);
    $('#attrNo').val(rowDataJson.attrNo);
    $('#selectAttrNo').val(rowDataJson.attrNo);
    $('#priority').val(rowDataJson.priority);
    $('#useYn').val(rowDataJson.useYn);
    $('#dispTypeTxt').val(rowDataJson.dispTypeTxt);
    $('#selectAttrNo').prop( "disabled", true );

  },
  /**
   * 분류그룹 select box 값 설정
   */
  setAttributeGroupSelectBox : function(){


    CommonAjax.basic({
      url: "/item/attribute/getAttributeGroupList.json",
      data: null,
      method: "GET",
      callbackFunc: function (res) {
        $("#selectAttrNo").append( "<option value=''>선택</option>");
        $.each(res, function (idx, val) {
          $("#selectAttrNo").append("<option value="+ val.gattrNo +" >" + val.gattrNo + " | " + val.gattrMngNm + "</option>");
        });
      }
    });
  },
  /**
   * 속성 분류 등록/수정
   */
  setAttribute: function() {

    var objectArray = [$('#selectAttrNo') ,$('#priority') ];

    if($.jUtil.isEmpty($('#scateCd').val())){
      alert("카테고리를 선택해 주세요.");
      return;
    }

    if($.jUtil.isEmptyObjectArray(objectArray)) {
      alert("필수 입력 항목을 선택/입력 해주세요.");
      return false;
    }

    if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
      alert("숫자만 입력 가능 합니다.");
      $("#priority").val('');
      $("#priority").focus();
      return;
    }

    if ( $('#priority').val()> 9999) {
      alert("최대 9999까지 입력 가능합니다.");
      $("#priority").val('');
      $("#priority").focus();
      return;
    }

    $('#setAttributeForm').submit();
  }

};