$(document).ready(function () {
    auroraCategoryMng.init();
    auroraCategoryGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var auroraCategoryGrid = {
    treeView: new RealGridJS.TreeView("auroraCategoryGrid"),
    treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
    init: function () {
        auroraCategoryGrid.initGrid();
        auroraCategoryGrid.initDataProvider();
        auroraCategoryGrid.event();
    },
    initGrid: function () {
        auroraCategoryGrid.treeView.setDataSource(auroraCategoryGrid.treeDataProviders);

        auroraCategoryGrid.treeView.setStyles(auroraCategoryGridBaseInfo.realgrid.styles);
        auroraCategoryGrid.treeView.setDisplayOptions(auroraCategoryGridBaseInfo.realgrid.displayOptions);
        auroraCategoryGrid.treeView.setColumns(auroraCategoryGridBaseInfo.realgrid.columns);
        auroraCategoryGrid.treeView.setOptions(auroraCategoryGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        auroraCategoryGrid.treeDataProviders.setFields(auroraCategoryGridBaseInfo.dataProvider.fields);
        auroraCategoryGrid.treeDataProviders.setOptions(auroraCategoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        auroraCategoryGrid.treeView.onDataCellClicked = function (gridView, index) {
            auroraCategoryMng.getDetail(index.dataRow);
        };
    },
    setData: function (dataList) {
        auroraCategoryGrid.treeDataProviders.clearRows();
        auroraCategoryGrid.treeDataProviders.fillJsonData({"rows": dataList},
            {rows: "rows", icon: "lcateCd"});
    }
};

var auroraCategoryMng = {
    categoryCnt: 4,
    init: function () {
        this.event();
        auroraCategoryMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        auroraCategoryMng.changeCategorySelectItem('1');
    },
    event: function () {
        //카테고리 검색 버튼 클릭
        $('#searchBtn').click(function () {
            auroraCategoryMng.search();
        });

        //카테고리 저장 버튼 클릭
        $('#setCategoryBtn').click(function () {
            auroraCategoryMng.setCategory();
        });

        //카테고리 리셋 버튼 클릭
        $('#resetBtn').click(function () {
            auroraCategoryMng.formReset();
        });

        //카테고리 검색 리셋 버튼 클릭
        $('#searchResetBtn').click(function () {
            auroraCategoryMng.searchFormReset();
        });

        //뎁스 변경시
        $('#depth').change(function () {
            $('#formCateCd1, #formCateCd2, #formCateCd3').prop('disabled', false);
            $('select[id^="formMappingCateCd"]').prop('disabled', true);

            switch ($(this).val()) {
                case '1' :
                    $('#formCateCd1').prop('disabled', true);
                case '2' :
                    $('#formCateCd2').prop('disabled', true);
            }
        });

        //사용여부 변경시
        $('#useYn').change(function () {
            if ($(this).val() == 'N') {
                $('#dispYn').val('N');
                $('#dispYn  option[value=N]').prop('disabled', false);
                $('select[id=dispYn]').prop('disabled', true);
                $('#priority').val('0').prop('readonly', true);
            } else {
                $('select[id=dispYn]').prop('disabled', false);
                $('#priority').val('').prop('readonly', false);
            }
        });

        $("#priority").on({
            keydown: function (e) {
                if (e.which === 32) return false;
            },
            keyup: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            },
            change: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });

        $('#setSchBrandBtn').on('click', function() {
            auroraCategoryMng.getSchLgcMapping();
        });

    },
    search: function (cateArr) {
        var url = $('#searchForm').serialize();
        if (typeof cateArr == 'object') {
            url = $.param(cateArr)
        }

        CommonAjax.basic({
            url: '/item/auroraCategory/getAuroraCategory.json?' + url,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                auroraCategoryGrid.setData(res);
                var searchVal;
                for (var i = auroraCategoryMng.categoryCnt; i > 0; i--) {
                    if ($('#searchCateCd' + i).val() != "") {
                        searchVal = $('#searchCateCd' + i + ' option:selected').text();
                        break;
                    }
                }

                var ret = auroraCategoryGrid.treeDataProviders.searchData(
                    {fields: ["cateNm"], value: searchVal, wrap: true});
                if (ret) {
                    var rowId = ret.dataRow;
                    var parents = auroraCategoryGrid.treeDataProviders.getAncestors(rowId);
                    if (parents) {
                        for (var i = parents.length - 1; i >= 0; i--) {
                            auroraCategoryGrid.treeView.expand(
                                auroraCategoryGrid.treeView.getItemIndex(parents[i]));
                        }
                        auroraCategoryGrid.treeView.setCurrent({
                            itemIndex: auroraCategoryGrid.treeView.getItemIndex(rowId),
                            fieldIndex: ret.fieldIndex
                        })
                        auroraCategoryGrid.treeView.expand(auroraCategoryGrid.treeView.getItemIndex(rowId));
                    }
                }

                $('#auroraCategoryGridCnt').html($.jUtil.comma(auroraCategoryGrid.treeView.getItemCount()));
            }
        });
    },
    searchFormReset: function () {
        $('#searchCateCd1').val('').change();
    },
    formReset: function (option) {
        $('#cateCd').val('');
        $('#cateNm').val('');
        $('#useYn').val('Y').change();
        $('#dispYn').val('Y').change();
        $('#priority').val('');
        $('#depth').val('').change();
        $("#depth[id=depth]").prop("disabled", false);
        $('#parentCateCd').val('');

        auroraCategoryMng.changeCategorySelectItem('1');

        if (option != 'grid') {
            auroraCategoryMng.setCategorySelectBox(1, '', '', $('#formCateCd1'));
            $('#formCateCd1').val('').change();
            $('#formMappingCateCd1 option:first').prop('selected', true).change();
        }
    },
    getDetail: function (selectRowId) {
        var rowDataJson = auroraCategoryGrid.treeDataProviders.getJsonRow(selectRowId);
        auroraCategoryMng.formReset('grid');

        var cateCd = rowDataJson.cateCd;
        var cateNm = rowDataJson.cateNm;
        var useYn = rowDataJson.useYn;
        var dispYn = rowDataJson.dispYn;
        var priority = rowDataJson.priority;
        var depth = rowDataJson.depth;
        var lcateCd = rowDataJson.lcateCd;
        var mcateCd = rowDataJson.mcateCd;

        $('#cateCd').val(cateCd);
        $('#parentCateCd').val(lcateCd);
        $('#cateNm').val(cateNm);
        $('#depth').val(depth).change();
        $('#depth  option[value=' + depth + ']').prop('disabled', false);
        $("select[id=depth]").prop("disabled", true);

        auroraCategoryMng.setCategorySelectBox(1, '', lcateCd, $('#formCateCd1'));
        auroraCategoryMng.setCategorySelectBox(2, lcateCd, mcateCd, $('#formCateCd2'));

        auroraCategoryMng.changeCategorySelectItem(depth);

        $('[id^="formMappingCateCd"]').val(0);
        $('#useYn').val(useYn).change();
        $('#dispYn').val(dispYn).change();
        $('#priority').val(priority);
    },
    setCategory: function () {
        if(!auroraCategoryMng.setValid()){
            return false;
        }

        var parentCateCd    = 0;
        var lcateCd         = '';

        parentCateCd = $('#formCateCd1').val();
        $('#parentCateCd').val(parentCateCd);
        lcateCd = $('#formCateCd1').val();

        if ($('#depth').val() > 1 && parentCateCd == 0) {
            alert('부모카테고리를 선택해주세요.');
            return false;
        }

        CommonAjax.basic({
            url : '/item/auroraCategory/setAuroraCategory.json',
            data : JSON.stringify($('#auroraCategoryForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                var cateObj = {
                    lcateCd: lcateCd,
                };

                auroraCategoryMng.formReset();
                // auroraCategoryMng.setCategorySelectBox(1, '', lcateCd, $('#searchCateCd1'));
                auroraCategoryMng.search(cateObj);
            }
        });
    },
    setCategorySelectBox: function (depth, parentCateCd, cateCd, selector) {
        $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
        if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
            CommonAjax.basic({
                url: '/item/auroraCategory/getAuroraCategory.json',
                data: {
                    depth: depth,
                    parentCateCd: parentCateCd
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $.each(resData, function (idx, val) {
                        var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                        $(selector).append(
                            '<option value="' + val.cateCd + '" ' + selected + '>'
                            + val.cateNm
                            + '</option>');
                    });
                    $(selector).css('-webkit-padding-end', '30px');
                    if (depth == 1) {
                        $('#formCateCd1').html($(selector).html());
                    }
                }
            });
        }
    },
    changeCategorySelectItem: function (depth) {
        if(depth === '4') {
            $('#isbnYn, #reviewDisplay').prop('disabled', false);
        } else {
            $('#isbnYn, #reviewDisplay').prop('disabled', true);
        }
    },
    setValid: function () {
        var isEdit = $('#cateCd').val() == '' ? false : true;

        if (isEdit == true) {
            if ($('#cateCd').val() == "") {
                alert("등록 버튼을 클릭하여 카테고리 신규 등록하세요.");
                return false;
            }
        }
        if ($('#cateNm').val() == '') {
            alert('카테고리명을 입력해주세요.');
            $('#cateNm').focus();
            return false;
        }

        if ($.jUtil.isNotAllowInput($('#cateNm').val(), ['SPC_SCH']) || $('#cateNm').val().length > 16) {
            alert('카테고리명 최대 16자까지 가능하며, 입력가능 문자 : 숫자, 영문, 한글'
                + ' 입니다.');
            $('#cateNm').focus();
            return false;
        }

        if ($('#depth').val() == '') {
            alert('Depth를 선택해주세요.');
            $('#depth').focus();
            return false;
        }

        if ($('#useYn').val() == 'Y') {
            if ($('#priority').val() == '') {
                alert('우선순위를 입력해주세요.');
                $('#priority').focus();
                return false;
            }

            var pattern_number = /[0-9]+$/;
            if (!pattern_number.test($('#priority').val())) {
                alert('숫자만 입력 가능하며, 최대 9999까지만 입력하실 수 있습니다.');
                $('#priority').focus();
                return false;
            }
        }
        return true;
    }
};