/**
 * 상품관리 > 셀러상품관리/점포상품관리 > 상품 등록/수정
 */
var item = {
    isReg : true,
    uploadType : 'IMG',
    relationInfo : {
        partnerId : '',
        scateCd : '',
        adultType : ''
    },
    /**
     * 초기화
     */
    init : function() {
        this.event();
        itemMain.util.initDateTime('10d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        itemMain.util.initDateTime('-30d', 'dcStartDt', 'dcEndDt', "HH:00:00", "HH:59:59");
        itemMain.util.initDate('-30d', 'rsvStartDt', 'rsvEndDt');
        itemMain.util.initDate('0d', 'prDispStartDt', 'prDispEndDt');
        Calendar.datePicker('makeDt');
        Calendar.datePicker('expDt');
        Calendar.datePicker('shipStartDt');
        commonEditor.initEditor($('#editor1'), 300, 'ItemDetail');
        commonEditor.hmpImgUrl = hmpImgFrontUrl;
        commonEditor.setHtml($('#editor1'), "");
        this.initCategorySelectBox();
        item.resetForm();
        item.attr.getAttrList("TAG");
        item.attr.getMngNoticeGroupList();
        $('#saleUnit, #purchaseMinQty, #purchaseLimitDay, #optPrice, #optStockQty, #rentalFee, #rentalPeriod, #stockQty, #commissionPrice, #dcPrice, #dcCommissionPrice, #originPrice, #salePrice, #purchaseLimitQty, #claimShipFee, #rentalRegFee, #purchasePrice').allowInput("keyup", ["NUM"]);
        $('#commissionRate, #dcCommissionRate').allowInput("keyup", ["NUM", "DOT"]);
    },
    /**
     * 카테고리 셀렉트박스 초기화
     */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#cateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#cateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#cateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#cateCd4'));
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#itemType').bindChange(item.sale.changeItemType);
        $('#cateCd4').bindChange(item.sub.changeCate4);
        $('#changeStockQtyCheck').bindChange(item.sale.changeStockQtyCheck);
        $('input:radio[name="adultType"]').bindClick(item.sale.changeAdultType);
        $('input:radio[name="purchaseLimitYn"]').bindClick(item.sale.changePurchaseLimitYn);

        $('#purchaseLimitDuration').bindChange(item.sale.getPurchaseLimitDuration);

        $('input:radio[name="salePeriodYn"]').on('change', function() {
            item.sale.changeSalePeriodYn($(this).val());
        });

        $('input:radio[name="rsvYn"]').on('change', function() {
            item.sale.changeRsvYn($(this).val());
        });

        $('input:radio[name="globalAgcyYn"]').on('change', function() {
            item.sale.changeGiftYn($(this).val());
        });

        $('input:radio[name="optTxtUseYn"]').on('change', function() {
            item.opt.getOptTxtUseYnRadio($(this).val());
        });

        $('#optTxtDepth').on('change', function() {
            item.opt.getOptTxtDepth($(this).val());
        });

        $('#cateCd4').on('change', function() {
            if (!$.jUtil.isEmpty($('#cateCd4').val()) && !$.jUtil.isEmpty($('#businessNm').val())) {
                item.sale.getBaseCommissionRate($('#partnerId').val(), $('#cateCd4').val());
            }
        });

        $('#rentalFee, #rentalPeriod').on('keyup', function() {
            item.sale.changeRental();
        });

        $('#shipPolicyNo').on('change', function() {
            if ($(this).val() != '') {
                item.ship.getSellerShipDetail($('#partnerId').val(), true, $(this).val());
            }
        });

        $('#commissionType').on('change', function() {
            item.sale.setCommissionHandler($(this).val(), 0, 0, 'commission');
            item.sale.calculateSaleCostPrice();
        });

        $('#salePrice, #commissionRate, #commissionPrice').on('keyup', function() {
            item.sale.calculateSaleCostPrice();
        });

        $('#dcCommissionType').on('change', function() {
            item.sale.setCommissionHandler($(this).val(), 0, 0, 'dcCommission');
        });

        $('input:radio[name="dcYn"]').on('change', function() {
            $(this).val() == 'Y' ? $('.dcArea').show() : $('.dcArea').hide();
        });

        //파일선택시
        $('input[name="mainImg0"], input[name="mainImg1"], input[name="mainImg2"], input[name="listImg"], '
                + 'input[name="proof1"], input[name="proof2"], input[name="proof3"], input[name="proof4"]').change(function() {
            if (item.uploadType == 'IMG') {
                item.img.addImg($(this).data('file-id'), $(this).prop('name'));
            } else {
                item.file.addFile($(this).data('file-id'), $(this).prop('name'));
            }
        });

        $('#itemSetForm').on('propertychange change keyup paste input', '#fr-image-by-url-layer-text-1', function(e){
            commonEditor.inertImgurl = $('#fr-image-by-url-layer-text-1').val();
        });

        $('#dcPeriodYn').on('change', function(){
            if ($(this).is(':checked')) {
                $('#dcStartDt, #dcEndDt').prop('disabled', false);
            } else {
                $('#dcStartDt, #dcEndDt').prop('disabled', true);
            }
        });

        $("#dcPrice, #salePrice").keyup(function(key) {
            item.sale.setDcRate();
        });

        $('#setItemBtn').on('click', function(){
            item.setItem('N');
        });

        $('#setItemTempBtn').on('click', function(){
            item.setItem('Y');
        });

        $('#resetBtn').on('click', function () {
            item.resetForm();
        });

        $('#itemNm').calcTextLength('keyup', '#itemNmCount');
        $('#itemDesc').calcTextLength('keyup', '#textCountDesc');

        $('#itemNm').notAllowInput('focusout', ['SYS_DIVISION']);

        // 브랜드/제조사 조회
        $('#getBrandBtn').bindClick(item.attr.getBrand);
        $('#getMakerBtn').bindClick(item.attr.getMaker);
        $('#getPartnerBtn').bindClick(item.sale.getPartner);

        $("#getBrandBtn").keydown(function(key) {
            if (key.keyCode == 13) {
                item.sale.getBrand();
            }
        });

        $("#getMakerBtn").keydown(function(key) {
            if (key.keyCode == 13) {
                item.sale.getMaker();
            }
        });

        $("#businessNm").keydown(function(key) {
            if (key.keyCode == 13) {
                item.sale.getPartner();
            }
        });

        $("#brandNm, #makerNm").keydown(function(key) {
            if (key.keyCode == 13) {
                switch ($(this).prop('id')) {
                    case 'brandNm' :
                        item.attr.getBrand();
                        break;
                    case 'makerNm' :
                        item.attr.getMaker();
                        break;
                }
            }
        });

        item.attr.event();
        item.ship.event();
        item.ext.event();
        item.img.event();
        item.common.event();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        let rowDataJson = itemListGrid.dataProvider.getJsonRow(selectRowId);

        item.getDetail(rowDataJson.itemNo);

    },
    /**
     * 상품상세 > 조회
     * @param itemNo
     */
    getDetail : function(itemNo) {
        if (itemNo) {
            item.resetForm();

            CommonAjaxBlockUI.startBlockUI();
            CommonAjax.basic({
                url: '/item/getDetail.json',
                data: {itemNo: itemNo, mallType: $('#mallType').val(), storeId:0},
                method: 'GET',
                callbackFunc: function (data) {
                    item.isReg = false;

                    if (!$.jUtil.isEmpty(data.relationNo)) {
                        item.relationInfo.partnerId = data.partnerId;
                        item.relationInfo.scateCd = data.scateCd;
                        item.relationInfo.adultType = data.adultType;
                    } else {
                        item.sub.initRelationItem();
                    }

                    $('#itemHist').show();
                    if (data.itemStatus == 'T') {
                        $('#setItemTempBtn').show();
                    } else {
                        $('#setStatusArea').show();
                        $('#setItemTempBtn, #setStatusArea button').hide();
                        if (data.itemStatus == 'A') {
                            $('#setStatusP, #setStatusS').show();
                        } else if (data.itemStatus == 'S') {
                            $('#setStatusP, #setStatusA, #itemTitleArea').show();
                        } else if (data.itemStatus == 'P') {
                            $('#setStatusS, #setStatusA, #itemTitleArea').show();
                        }
                    }

                    /** 기본정보 **/
                    $('#itemType').val(data.itemType).prop('disabled', true);
                    item.sale.changeItemType($('#itemType'), true);
                    $('#itemTitle').html('<span class="mg-l-5">상품 안내사항│상품번호 : '+data.itemNo+', 상품명 : '+data.itemNm + '</span>');
                    $('#itemNo').val(data.itemNo);
                    $('#itemNm').val(data.itemNm).setTextLength('#itemNmCount');
                    $('#itemNoView').html(itemNo);
                    $('#itemNmView').html(data.itemNm);
                    $('.itemStatusNm').html(data.itemStatusNm);
                    $('#statusDesc').html(data.statusDesc);
                    $('#itemChgDt').html(data.chgDt);
                    $('#itemDivision').html('일반상품');
                    $('#sellerItemCd').val(data.sellerItemCd);

                    if (data.cartYn == 'Y' || data.itemType == 'T' || data.itemType == 'M') {
                        item.sale.setCartLimitYn('Y');
                    } else {
                        $('input:radio[name="cartLimitYn"]:input[value="'+data.cartLimitYn+'"]').trigger('click');
                    }

                    commonCategory.setCategorySelectBox(1, '', data.lcateCd, $('#cateCd1'));
                    commonCategory.setCategorySelectBox(2, data.lcateCd, data.mcateCd, $('#cateCd2'));
                    commonCategory.setCategorySelectBox(3, data.mcateCd, data.scateCd, $('#cateCd3'));
                    commonCategory.setCategorySelectBox(4, data.scateCd, data.dcateCd, $('#cateCd4'));

                    item.sale.addPartnerHtml(data.partnerId, data.businessNm, data.shipPolicyNo);
                    $('#dispYn').val(data.dispYn);

                    /** 판매정보 **/
                    $('input:radio[name="taxYn"]:input[value="'+data.taxYn+'"]').trigger('click');
                    $('#originPrice').val(data.originPrice);
                    $('#purchasePrice').val(data.purchasePrice);
                    item.sale.getBaseCommissionRate(data.partnerId, data.dcateCd);

                    $('input:radio[name="dcYn"]:input[value="'+data.dcYn+'"]').trigger('click');
                    if (data.dcYn == 'Y') {
                        $('#dcStartDt').val(data.dcStartDt);
                        $('#dcEndDt').val(data.dcEndDt);
                        if (data.dcPeriodYn == 'Y') {
                            $('#dcPeriodYn').prop('checked', true);
                            $('#dcStartDt, #dcEndDt').prop('disabled', false);
                        }

                        $('#dcCommissionType').val(data.dcCommissionType).trigger('change');
                        $('#dcCommissionRate').val(data.dcCommissionRate);
                        $('#dcCommissionPrice').val(data.dcCommissionPrice);
                        $('#dcPrice').val(data.dcPrice);
                    }

                    $('input:radio[name="salePeriodYn"]:input[value="'+data.salePeriodYn+'"]').trigger('click');
                    item.sale.changeSalePeriodYn(data.salePeriodYn, data.saleStartDt, data.saleEndDt);

                    $('input:radio[name="rsvYn"]:input[value="'+data.rsvYn+'"]').trigger('click');
                    item.sale.changeRsvYn(data.rsvYn, data.saleRsvNo, data.rsvStartDt, data.rsvEndDt, data.shipStartDt);

                    $('#changeStockQtyLabel').show();
                    $('#changestockQtyCheck').prop('checked', false).trigger('change');

                    $('#saleUnit').val(data.saleUnit);
                    $('#purchaseMinQty').val(data.purchaseMinQty);

                    $('input:radio[name="purchaseLimitYn"]:input[value="'+data.purchaseLimitYn+'"]').trigger('click');

                    if (data.purchaseLimitDuration) {
                        $('#purchaseLimitDuration').val(data.purchaseLimitDuration).trigger('change');
                    } else {
                        $('#purchaseLimitDuration').trigger('change');
                    }
                    $('#purchaseLimitDay').val(data.purchaseLimitDay == 0 ? 1: data.purchaseLimitDay);
                    $('#purchaseLimitQty').val(data.purchaseLimitQty == 0 ? 1: data.purchaseLimitQty);
                    item.sale.setPartnerSaleTypeUI(data.saleType);

                    /** 옵션정보 **/
                    $('input:radio[name="optTxtUseYn"]:input[value="'+data.optTxtUseYn+'"]').trigger('click');
                    $('#optTxtDepth').val(data.optTxtDepth).trigger('change');
                    $('#opt1Text').val(data.opt1Text);
                    $('#opt2Text').val(data.opt2Text);

                    commonEditor.setHtml($('#editor1'), data.itemDesc);

                    /** 속성정보 **/
                    item.attr.getAttrList("ATTR", data.itemNo, data.scateCd);
                    item.attr.getAttrList("TAG", data.itemNo);
                    item.attr.addBrandHtml(data.brandNo, data.brandNm);
                    item.attr.addMakerHtml(data.makerNo, data.makerNm);
                    $('#gnoticeNo').val(data.gnoticeNo);
                    item.attr.getMngNoticeList(data.gnoticeNo, data.noticeList);

                    $('#makeDt').val(data.makeDt);
                    $('#expDt').val(data.expDt);

                    $('#stockQty').val(data.remainStockQty).prop('readonly', true);

                    if (!$.jUtil.isEmpty(data.imgList)) {
                        let changeYn = 'N';

                        data.imgList.forEach(obj => {

                            let priority = obj.priority;

                            if (obj.imgType == 'MN' && !$.jUtil.isEmpty($('#MN' + priority + ' .imgNo').val())) {
                                changeYn = 'Y';

                                $('.itemImg').each(function(idx1) {
                                    if ($.jUtil.isEmpty($('#MN' + (idx1) + ' .imgNo').val())) {
                                        priority = idx1;
                                        return false;
                                    }
                                });
                            }

                            let _this = $('#MN' + priority);

                            if (!$.jUtil.isEmpty(obj.mainYn) && obj.mainYn == 'Y') {
                                $('input[name="mainYnRadio"][value='+priority+']').trigger('click');
                                item.img.beforeMainId = priority;
                            }

                            if (obj.imgType == 'LST') {
                                _this = $('#imgLST');
                            }

                            item.img.setImg(_this, {
                                'imgNo' : obj.imgNo,
                                'imgNm' : obj.imgNm,
                                'imgUrl': obj.imgUrl,
                                'src'   : hmpImgUrl + "/provide/view?processKey=" + _this.data('processkey') + "&fileId=" + obj.imgUrl,
                                'changeYn' : 'N'
                            });
                        });

                        if (changeYn == 'Y') {
                            $('#imgMNDiv').find('.itemImg').each(function() {
                                if (!$.jUtil.isEmpty($(this).find('.imgUrl').val())) {
                                    $(this).find('.changeYn').val('Y');
                                }
                            });
                        }
                    }

                    /** 배송정보 **/
                    if (data.itemType != 'E') {
                        $('#claimShipFee').val(data.claimShipFee);

                        $('#releaseZipcode').val(data.releaseZipcode);
                        $('#releaseAddr1').val(data.releaseAddr1);
                        $('#releaseAddr2').val(data.releaseAddr2);
                        $('#returnZipcode').val(data.returnZipcode);
                        $('#returnAddr1').val(data.returnAddr1);
                        $('#returnAddr2').val(data.returnAddr2);
                        $('#shipReleaseDay').val(data.releaseDay).trigger('change');
                        $('#shipReleaseTime').val(data.releaseTime);
                        $('#shipHolidayExceptYn').val(data.holidayExceptYn);
                        $('input[name="safeNumberUseYn"][value="'+data.safeNumberUseYn+'"]').trigger('click');
                    }

                    /** 부가정보 **/
                    let imgDispYn = 'Y';
                    if (data.imgDispYn != null) {
                        imgDispYn = data.imgDispYn;
                    }

                    item.sale.setAdultLimitYn(data.adultLimitYn, imgDispYn);
                    $('input[name="adultType"][value="'+data.adultType+'"]').trigger('click');
                    $('#imgDispYn').val(imgDispYn);
                    $('input[name="epYn"][value="'+data.epYn+'"]').trigger('click');
                    $('input[name="globalAgcyYn"][value="'+data.globalAgcyYn+'"]').prop('checked', true);
                    $('#srchKeyword').val(data.srchKeyword);

                    if (data.providerList.length > 0) {
                        for (let i in data.providerList) {
                            $('input[name="providerList[].providerId"][value="'+data.providerList[i].providerId+'"]').siblings('input[name="providerCheck"]').trigger('click');
                        }
                    }

                    if (data.isCert) {
                        $('input[name="isCert"][value="'+data.isCert+'"]').trigger('click');
                    }
                    $('input[name="giftYn"][value="'+data.giftYn+'"]').trigger('click');
                    if (!$.jUtil.isEmpty(data.isbn)) {
                        $('#isbnArea').show();
                        $('#isbn').val(data.isbn);
                    }

                    item.sub.setIsbnUI(data.isbnYn);

                    //증빙서류
                    for (let i in data.proofFileList) {
                        item.file.appendProofFile(data.proofFileList[i]);
                    }

                    if (data.itemType == 'T') {
                        $('#rentalFee').val(data.rentalFee);
                        $('#rentalRegFee').val(data.rentalRegFee);
                        $('#rentalPeriod').val(data.rentalPeriod);
                        item.sale.changeRental();
                    }

                    //안전인증
                    for (let i in data.certList) {
                        let cert = data.certList[i];
                        let certId = '#certTr' + cert.certGroup;

                        if (i > 1 && !$.jUtil.isEmpty($(certId).find('.certType').val())) {
                            $(certId).find('.certAdd').trigger('click');
                            certId = '#cert_' + ($('.addObj').length - 1);
                        } else {
                            $('#cert_' + cert.certGroup + '_isCert_' + cert.isCert).trigger('click');
                        }

                        $(certId).find('.itemCertSeq').val(cert.itemCertSeq);
                        $(certId).find('.isCert').val(cert.isCert);
                        $(certId).find('.certType').val(cert.certType).trigger('change');
                        if (cert.certType == 'ET_CER') {
                            $(certId).find('.energy').val(cert.certNo);
                        }

                        if (!$.jUtil.isEmpty(cert.certNo)) {
                            $(certId).find('.certNo').val(cert.certNo).prop('readonly', true);

                            if (cert.certNo.includes('|')) {
                                let certNoValArr = cert.certNo.split('|');

                                $(certId).find('.certNo').hide();
                                $(certId).find('.certNoSub').each(function(i) {
                                    $(this).val(certNoValArr[i]);
                                });
                            }
                        }
                    }

                    $('#salePrice').val(data.salePrice);

                    //수수료 셋팅
                    let costPrice = data.salePrice;
                    $('#commissionType').val(data.commissionType);
                    if (data.commissionType == 'R') {
                        $('#commissionRate').val(data.commissionRate).show();
                        $('#commissionPrice').hide();
                        costPrice = costPrice * (100 - data.commissionRate) / 100;
                    } else {
                        $('#commissionPrice').val(data.commissionPrice).show();
                        $('#commissionRate').hide();
                        costPrice = costPrice - data.commissionPrice;
                    }
                    $('#costPrice').html($.jUtil.comma(costPrice) + ' 원');

                    if (data.globalAgcyYn == 'Y') {
                        item.sale.changeGiftYn("Y");
                    }

                    item.sale.setDcRate();

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    /**
     * 상품 등록/수정 유효성 검사
     */
    valid : function() {

        if ($.jUtil.isEmpty($('#itemNm').val())) {
            return $.jUtil.alert("상품명은 필수입력항목입니다.", 'itemNm');
        }

        if ($.jUtil.isEmpty($('#cateCd4').val())) {
            return $.jUtil.alert("카테고리는 필수입력항목입니다.", 'cateCd4');
        }

        if ($.jUtil.isEmpty($('#originPrice').val())) {
            return $.jUtil.alert("정상가격은 필수입력항목입니다.", 'originPrice');
        }

        if ($.jUtil.isEmpty($('#stockQty').val())) {
            return $.jUtil.alert("재고수량은 필수입력항목입니다.", 'stockQty');
        }

        if ($('#purchasePrice').is(':visible') == false) {
            if ($('#commissionType').val() == 'R' && $.jUtil.isEmpty($('#commissionRate').val())) {
                return $.jUtil.alert("수수료는 필수입력항목입니다.", 'commissionRate');
            }

            if ($('#commissionType').val() == 'A' && $.jUtil.isEmpty($('#commissionPrice').val())) {
                return $.jUtil.alert("수수료는 필수입력항목입니다.", 'commissionPrice');
            }
        }

        if ($('#itemType').val() != 'E' && $('#itemType').val() != 'M' && $('#itemType').val() != 'T' && $('#shipPolicyNo').val() == '') {
            return $.jUtil.alert("배송정책은 필수입력항목입니다.", 'shipPolicyNo');
        }

        if ($.jUtil.isNotAllowInput($("#itemNm").val(), ['SYS_DIVISION'])) {
            return $.jUtil.alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]), 'itemNm');
        }

        //할인가격 특정기간 할인 날짜 검증
        if ($('input[name="dcYn"]:checked').val() == 'Y') {
            if ($('input[name="salePeriodYn"]:checked').val() == 'Y'
                    && $('#dcPeriodYn').is(':checked') == true
                    &&
                    (
                            $.jUtil.isEmpty($('#saleStartDt').val()) ||
                            $.jUtil.isEmpty($('#dcEndDt').val()) ||
                            moment($('#dcEndDt').val()+" 23:59:59").isBefore($('#saleStartDt').val()) == true
                    )) {
                return $.jUtil.alert('할인종료일은 시작일 이전으로 입력 할 수 없습니다.', 'dcEndDt');
            }

            if (
                    $.jUtil.isEmpty($('#dcPrice').val()) ||
                    $.jUtil.isEmpty($('#salePrice').val()) ||
                    Number($('#dcPrice').val()) > Number($('#salePrice').val())
            ) {
                return $.jUtil.alert('할인가격이 판매가격보다 클 수 없습니다', 'dcPrice');
            }
        }

        //판매기간 검증
        if ($('input[name="salePeriodYn"]:checked').val() == 'Y') {
            if ($.jUtil.isEmpty($('#saleStartDt').val()) || $.jUtil.isEmpty($('#saleEndDt').val())) {
                return $.jUtil.alert('판매기간을 입력해주세요.', 'saleStartDt');
            }

            if (moment($('#saleEndDt').val()).isBefore($('#saleStartDt').val()) == true) {
                return $.jUtil.alert('판매종료일은 시작일 이전으로 입력 할 수 없습니다.', 'saleEndDt');
            }
        }

        //판매단위수량 검증
        if ($.jUtil.isEmpty($('#saleUnit').val())) {
            return $.jUtil.alert("판매단위수량을 입력해주세요.", 'saleUnit');
        }

        //최소구매수량 검증
        if ($.jUtil.isEmpty($('#purchaseMinQty').val())) {
            return $.jUtil.alert("최소구매수량을 입력해주세요.", 'purchaseMinQty');
        }

        //구매수량제한 검증
        if ($('input[name="purchaseLimitYn"]:checked').val() == 'Y') {
            if (Number($('#saleUnit').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 판매단위수량 보다 적을 수 없습니다.", 'saleUnit');
            }

            if (Number($('#purchaseMinQty').val()) > Number($('#purchaseLimitQty').val())) {
                return $.jUtil.alert("구매수량제한 값은 최소구매수량 보다 적을 수 없습니다.", 'purchaseMinQty');
            }

            if ($('#purchaseLimitDuration').val() == 'P' && Number($('#purchaseLimitDay').val()) > 30) {
                return $.jUtil.alert("구매수량제한 기간은 30일을 초과할 수 없습니다.", 'purchaseLimitDay');
            }
        }

        if ($('#srchKeyword').val().split(',').length > 20) {
            return $.jUtil.alert("검색키워드는 최대 20개 까지 등록 가능합니다.", 'srchKeyword');
        }

        if (!$('input:radio[name=taxYn]').is(':checked')) {
            $("#taxYn").prop("tabindex", -1).focus();
            return $.jUtil.alert("판매정보 과세여부를 설정해 주세요.");
        }

        if ($('input[name="imgList[].imgUrl"]:input[value!=""]').length == 0) {
            $("#imgMNDiv").prop("tabindex", -1).focus();
            return $.jUtil.alert("이미지를 등록해 주세요.");
        }

        if ($.jUtil.isEmpty($('input[name="mainYnRadio"]:checked').val())
                || $.jUtil.isEmpty($('input[name="mainYnRadio"]:checked').closest('.imgMainYnRadioView').siblings('.preview-image').find('.imgUrl').val())
        ) {
            $("#imgMNDiv").prop("tabindex", -1).focus();
            return $.jUtil.alert("대표이미지를 선택해 주세요.");
        }

        //상품유형 렌탈 검사
        if ($('#itemType').val() == 'T') {
            if ($.jUtil.isEmpty($('#rentalFee').val())) {
                return $.jUtil.alert("월렌탈료를 입력해 주세요.", 'rentalFee');
            }

            if ($.jUtil.isEmpty($('#rentalRegFee').val())) {
                return $.jUtil.alert("등록비를 입력해 주세요.", 'rentalRegFee');
            }

            if ($.jUtil.isEmpty($('#rentalPeriod').val())) {
                return $.jUtil.alert("의무사용기간을 입력해 주세요.", 'rentalPeriod');
            }
        }

        //판매가격, 할인가격 검증
        if ($('#itemType').val() == 'T' || $('#itemType').val() == 'E' || $('#itemType').val() == 'M') {
        } else {
            if (Number($('#salePrice').val()) < 100) {
                return $.jUtil.alert("판매가격은 100원 이상 등록 가능합니다.", 'salePrice');
            }

            if ($('input[name="dcYn"]:checked').val() == 'Y' &&  Number($('#dcPrice').val()) < 100) {
                return $.jUtil.alert("할인가격은 100원 이상 등록 가능합니다.", 'dcPrice');
            }
        }

        //매입가 검증
        if ($('#purchasePrice').is(':visible') == true && $.jUtil.isEmpty($('#purchasePrice').val())) {
            return $.jUtil.alert("매입가를 입력해주세요.", 'purchasePrice');
        }

        //isbn 검증
        if ($('#isbnArea').is(':visible') && $.jUtil.isEmpty($('#isbn').val())) {
            return $.jUtil.alert("도서 카테고리는 ISBN을 입력해야 합니다.", 'isbn');
        }

        //에디터 검증
        if (commonEditor.checkIsEmpty('#editor1')) {
            $("#editor1").prop("tabindex", -1).focus();
            return $.jUtil.alert("상품상세를 입력해 주세요.", 'editor1');
        }

        if ($('input[name="optTxtUseYn"]:checked').val() == 'Y') {
            if ($.jUtil.isEmpty($('#opt1Text').val())) {
                return $.jUtil.alert("텍스트 옵션 타이틀을 입력해 주세요.", 'opt1Text');
            } else if ($('#optTxtDepth').val() == '2' && $.jUtil.isEmpty($('#opt2Text').val())) {
                return $.jUtil.alert("텍스트 옵션 타이틀을 입력해 주세요.", 'opt2Text');
            }
        }

        if (!$.jUtil.isEmpty(item.relationInfo.partnerId)) {

            let preMsg = '';

            if (item.relationInfo.partnerId != $('#partnerId').val()) {
                preMsg = '판매자가';
            } else if (item.relationInfo.scateCd != $('#cateCd3').val()) {
                preMsg = '카테고리 조건이';
            } else if (item.relationInfo.adultType != $('input[name="adultType"]:checked').val()) {
                preMsg = '성인상품유형 조건이';
            } else if ($('input[name="cartLimitYn"]:checked').val() == 'Y') {
                preMsg = '장바구니제한 조건이';
            } else if ($('input[name="optTxtUseYn"]:checked').val() == 'Y') {
                preMsg = '텍스트옵션 조건이';
            }

            if (!$.jUtil.isEmpty(preMsg) && !confirm(preMsg + " 변경되면 기존 연관상품이 해지됩니다. 변경하시겠습니까?")) {
                return false;
            }
        }

        //해외배송 검증
        if ($('input[name="globalAgcyYn"]:checked').val() == 'Y' && $('input[name="safeNumberUseYn"]:checked').val() == 'Y') {
            return $.jUtil.alert("해외배송의 경우 안심번호를 사용할 수 없습니다.", 'globalAgcyYn');
        }

        //안전인증 검증
        let checkCert = true;
        $('.certNo').each(function(){
            if ($(this).css('display') != 'none' ) {
                if (!$(this).prop('readonly') && $.jUtil.isEmpty($(this).val())) {
                    checkCert = false;
                    return $.jUtil.alert('안전인증을 입력해주세요.');
                } else if (!$(this).prop('readonly') && $(this).siblings('.certCheck').css('display') != 'none') {
                    checkCert = false;
                    return $.jUtil.alert('안전인증을 완료해주세요.');
                }

            }
        });

        $('.certNoSub').each(function(){
            if ($(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                checkCert = false;
                return $.jUtil.alert('안전인증을 입력해주세요.');
            }
        });

        if (checkCert) {
            $('.energy').each(function(){
                if ($(this).css('display') != 'none' && $.jUtil.isEmpty($(this).val())) {
                    checkCert = false;
                    return $.jUtil.alert('안전인증을 입력해주세요.1');
                }
            });
        }

        if (!checkCert) {
            return checkCert;
        }


        return true;
    },
    /**
     * 상품 등록/수정
     * @param isEdit 수정모드 여부
     */
    setItem : function(tempYn) {
        if (tempYn == 'Y' || item.valid()) {
            let mainId = $('input[name="mainYnRadio"]:checked').val();
            if (item.img.beforeMainId != mainId) {
                $('[id^="MN"]').each(function(idx) {
                    if (mainId == idx) {
                        $(this).find('.mainYn').val('Y');
                        $(this).find('.changeYn').val('Y');
                    } else {
                        $(this).find('.mainYn').val('N');
                    }
                });
            }

            let itemForm = $('#itemSetForm').serializeObject(true);

            itemForm.itemDesc   = commonEditor.getHtmlData('#editor1');
            itemForm.attrNoList = item.attr.getAttrNoList();
            itemForm.tempYn     = tempYn;

            if ($('input[name="mainYnRadio"]').is(':checked')) {
                itemForm.imgList[$('input[name="mainYnRadio"]:checked').val()].mainYn = "Y";
            }

            CommonAjax.basic({
                url: '/item/setItem.json',
                data: JSON.stringify(itemForm),
                type: 'post',
                contentType: 'application/json',
                method:"POST",
                successMsg:null,
                callbackFunc:function(res) {
                    if (res.returnStatus == 422) {
                        $.jUtil.alert(res.returnMessage);
                    } else {
                        $.jUtil.alert(res.data.returnMsg);
                        item.getDetail(res.data.returnKey);
                    }
                },
                errorCallbackFunc:function(resError) {
                    let res = resError.responseJSON;

                    if (res.returnCode == '1057') { //금칙어
                        $.jUtil.alert(res.returnMessage, 'itemNm');
                    } else {
                        $.jUtil.alert(res.returnMessage);
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
    },
    /**
     * 상품 입력/수정 폼 초기화
     */
    resetForm : function() {
        item.isReg = true;
        $('#itemSetForm select').each( function() {
            $(this).val( $(this).find("option[selected]").val() );
        });
        $("#itemSetForm").resetForm();

        $('#itemNo, #itemNm, #itemDesc, #saleRsvNo, #totalRentalFee').val('');
        $('#itemNm').setTextLength('#itemNmCount');
        $('#statusDesc, #baseCommissionRate, .itemStatusNm').html('');
        $('#useYn, #itemDispYn').val('Y');
        $('#setItemTempBtn').show();
        $('#itemTitleArea, #itemHist, .rentalArea, #changeStockQtyLabel, .dcArea, #setStatusArea, #isbnArea').hide();
        $('#cateCd1 option:eq(0)').prop('selected', true).trigger('change');
        $('#itemType, input[name="giftYn"]').prop('disabled', false);
        $('#purchaseLimitDuration, #prPeriodYn, #dcCommissionType').trigger('change');
        $('input:radio[name="salePeriodYn"]:input[value="'+$('input:radio[name="salePeriodYn"]:checked').val()+'"]').trigger('change');
        $('input:radio[name="adultType"]:input[value="NORMAL"]').trigger('click');
        item.sale.changeRsvYn($('input:radio[name="rsvYn"]:checked').val());
        item.sale.setCartLimitYn('N');
        item.sale.setAdultLimitYn('NORMAL');
        item.sale.setDcRate();
        $('input[name="providerList[].useYn"]').val('N');
        $('input[name="providerCheck"], #getPartnerBtn').prop('disabled', false);
        $("#stockQty, #businessNm, #commissionRate, #commissionPrice").prop('readonly', false);
        $('#dcStartDt, #dcEndDt').prop('disabled', true);
        $('.certCheck').show();
        $('.energy').hide();
        $('.certNo').prop('placeholder','');
        $('.itemCertSeq').val('');

        $('#certTable').find('.certExemptType').prop('disabled', true).hide();
        $('#certTable').find('.certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
        $('#certTable').find('.certNoSubArea').html('');
        $("[id^='cert_']").html('');
        $('.isCertRadio:checked').trigger('click');

        $('input[name="purchaseLimitYn"][value="N"]').prop('disabled', false).trigger('click');
        $('#purchaseLimitDuration').val('O').trigger('change');
        $('#purchaseLimitQty').val(1).prop('readonly', false);

        item.opt.getOptTxtUseYnRadio('N');
        $('#addTotalCount, #prTotalCount').text(0);

        item.ship.initShipInfoRelease();
        item.ship.initShipAddInfo();
        item.ship.initShipCheckBox();
        item.ship.initShipInfoDetail();
        item.attr.addBrandHtml(0,'');
        item.attr.addMakerHtml(0,'');
        item.sale.addPartnerHtml('', '');
        item.sale.setPartnerSaleTypeUI('N');

        item.sub.dcateCartYn    = 'N';
        item.sub.imgDispYn      = 'Y';

        commonEditor.setHtml($('#editor1'), "");

        item.img.init();
        commonEditor.inertImgurl = null;
        item.attr.init();
        item.attr.getMngNoticeList();

        item.sub.initRelationItem();

        $('[id^=uploadProofFileSpan_]').each(function(){
            $(this).html('');
        });

        /** 셋팅영역 **/
        $('#saleUnit, #purchaseMinQty').val(1);
    }
};

item.opt = {
    /**
     * 텍스트형 옵션 라디오 버튼 컨트롤
     * @param val
     */
    getOptTxtUseYnRadio : function(val) {
        item.opt.getOptTxtDepth('1');
        $('#optTxtDepth').val('1').prop("selected", true);

        switch (val) {
            case 'Y' :
                itemMain.util.setClassDisabledAll('setoptTxtFormAll', false);

                $('.setoptTxtFormAll').show();
                break;

            case 'N' :
                itemMain.util.setClassDisabledAll('setoptTxtFormAll', true);

                $('.setoptTxtFormAll').hide();
                break;
        }
    },
    /**
     * 옵션정보 > 텍스트형 옵션 > 단계선택
     * @param val
     */
    getOptTxtDepth : function(val) {
        $('.optTxtDepthTr').css('display', 'none');

        for (let idx = 0; val > idx; idx++) {
            let idxNum = idx + 1;

            $('#optTxtDepthTr' + idxNum).css('display', '');
        }
    }
};

item.ship = {
    event : function() {

        //사업자 주소 동일 적용
        $('#copyPartnerAddr').bindClick(item.ship.copyPartnerAddr);
        //출고지 주소 동일 적용
        $('#copyReleaseAddr').bindClick(item.ship.copyReleaseAddr);

        //도로명 주소2 입력
        $('#releaseRoadAddr2, #returnRoadAddr2').on('propertychange change keyup paste input', function() {
            let _td = $(this).closest('td');

            _td.find('span.roadAddr2').text($(this).val());
            _td.find('input.roadAddr2').val($(this).val());
        });

        //출고기한 변경 이벤트
        $('#shipReleaseDay').on('change', function(){
            item.ship.getShipReleaseDay($(this).val());
        });
    },
    releaseSchZipcode : function(res) {
        if ($("#copyPartnerAddr").prop('checked') == true) {
            $('#copyPartnerAddr').trigger('click');
        }
        $('#releaseZipcode').val(res.zipCode);
        $('#releaseAddr1').val(res.roadAddr);
    },
    returnSchZipcode : function(res) {
        if ($("#copyReleaseAddr").prop('checked') == true) {
            $('#copyReleaseAddr').trigger('click');
        }
        $('#returnZipcode').val(res.zipCode);
        $('#returnAddr1').val(res.roadAddr);
    },
    copyPartnerAddr : function() {
        if ($("#copyPartnerAddr").prop('checked') == true) {

            if ($('#partnerId').val() == "") {
                $("#copyPartnerAddr").prop('checked', false);
                return $.jUtil.alert("판매자를 먼저 선택해주세요.");
            }

            CommonAjax.basic({
                url:'/partner/getSellerAddress.json',
                data:{
                    partnerId : $('#partnerId').val(),
                },
                method:'get',
                callbackFunc:function(resData) {
                    $("#releaseZipcode").val(resData.zipCode);
                    $("#releaseAddr1").val(resData.addr1);
                    $("#releaseAddr2").val(resData.addr2).prop('readonly', true);

                    if ($("#copyReleaseAddr").prop('checked') == true) {
                        $("#returnZipcode").val($("#releaseZipcode").val());
                        $("#returnAddr1").val($("#releaseAddr1").val());
                        $("#returnAddr2").val($("#releaseAddr2").val());
                    }
                },
                errorCallbackFunc : function() {
                    alert('등록된 사업자 주소가 없습니다!');
                    $("#copyPartnerAddr").prop("checked", false);
                }
            });
        } else {
            $("#releaseZipcode").val("");
            $("#releaseAddr1").val("");
            $("#releaseAddr2").val("").prop('readonly', false);

            if ($("#copyReleaseAddr").prop('checked') == true) {
                $("#returnZipcode").val("");
                $("#returnAddr1").val("");
                $("#returnAddr2").val("");
            }
        }
    },
    copyReleaseAddr : function() {
        if ($("#copyReleaseAddr").prop('checked') == true) {
            $("#returnAddr2").prop('readonly', true);

            $("#returnZipcode").val($("#releaseZipcode").val());
            $("#returnAddr1").val($("#releaseAddr1").val());
            $("#returnAddr2").val($("#releaseAddr2").val());

        } else {
            $("#returnAddr2").prop('readonly', false);
            $("#returnZipcode").val("");
            $("#returnAddr1").val("");
            $("#returnAddr2").val("");
        }
    },
    /**
     * 배송정보 > 배송정보 선택 select box
     * @param partnerId
     * @param selectShipPolicyNo
     */
    getSellerShipList : function(partnerId, selectShipPolicyNo) {
        if (partnerId) {
            CommonAjax.basic({
                url: '/partner/getSellerShipList.json',
                data: {partnerId: partnerId, useYn: 'Y'},
                method: 'GET',
                callbackFunc: function (res) {
                    if (res) {
                        item.ship.initShipInfoDetail();

                        if (res.length > 0) {
                            for (let idx in res) {
                                let data = res[idx];

                                if (data.defaultYn == 'Y') {
                                    $('#shipPolicyNo').append('<option value="' + data.shipPolicyNo + '" >[기본] ' + data.shipPolicyNm + '</option>');

                                    if (!selectShipPolicyNo) {
                                        $('#shipPolicyNo').val(data.shipPolicyNo);
                                        item.ship.getSellerShipDetail(partnerId, true);
                                    }

                                } else {
                                    $('#shipPolicyNo').append('<option value="' + data.shipPolicyNo + '" >' + data.shipPolicyNm + '</option>');
                                }
                            }

                            if (selectShipPolicyNo) {
                                $('#shipPolicyNo').val(selectShipPolicyNo);
                                item.ship.getSellerShipDetail(partnerId, false, selectShipPolicyNo);
                            }

                        } else {
                            item.ship.initShipInfoRelease();
                            item.ship.initShipAddInfo();
                        }

                    } else {
                        item.ship.initShipAddInfo();
                    }
                }});
        }
    },
    /**
     * 배송정보 > 배송상세정보출력
     * @param shipPolicyNo
     * @param isSelectShipPolicyNo
     */
    getSellerShipDetail : function(partnerId, isSelectShipPolicyNo, shipPolicyNo) {
        $('.deliveryDetail').html('');
            item.ship.initShipCheckBox();

        if (partnerId) {
            CommonAjax.basic({
                url: '/partner/getShipPolicyList.json',
                data: {partnerId: partnerId, storeId:0, shipPolicyNo: shipPolicyNo},
                method: 'GET',
                callbackFunc: function (data) {
                    let res = data[0];
                    if (res) {
                        // 배송정보번호
                        $('#detailShipPolicyNo').html(res.shipPolicyNo);
                        // 배송정보명
                        $('#detailShipPolicyNm').html(res.shipPolicyNm);
                        // 배송주체
                        $('#detailShipMngNm').html('업체배송');

                        // 배송유형
                        $('#detailShipTypeNm').html(res.shipTypeNm);
                        // 배송방법
                        $('#detailShipMethodNm').html(res.shipMethodNm);
                        // 배송비종류
                        $('#detailShipKindNm').html(res.shipKindNm);
                        // 배송비
                        $('#detailShipFee').html($.jUtil.comma($.jUtil.comma(res.shipFee)) + '원');

                        if (res.freeCondition) {
                            $('#detailShipFee').append(' ( ' + $.jUtil.comma($.jUtil.comma(res.freeCondition)) + '원 이상 구매 시 무료 )');
                        }

                        if (res.diffYn == 'Y') {
                            $('#detailShipFee').append(' ( 수량별 차등 : ' + $.jUtil.comma(res.diffQty) + '개 초과 시 마다 )');
                        }

                        // 추가 배송비
                        if (res.prepaymentYn == 'Y') {
                            $('#detailPrepaymentYn').html('선결제');

                        } else {
                            $('#detailPrepaymentYn').html('착불');
                        }

                        // 배송비 노출여부
                        if (res.dispYn == 'Y'){
                            $('#detailShipFeeDispYn').html('노출');

                        } else {
                            $('#detailShipFeeDispYn').html('노출안함');
                        }

                        if (isSelectShipPolicyNo) {
                            // 반품/교환비(편도)
                            $('#claimShipFee').val(res.claimShipFee);
                            //출고지
                            $('#releaseZipcode').val(res.releaseZipcode);
                            $('#releaseAddr1').val(res.releaseAddr1);
                            $('#releaseAddr2').val(res.releaseAddr2);
                            //회수지
                            $('#returnZipcode').val(res.returnZipcode);
                            $('#returnAddr1').val(res.returnAddr1);
                            $('#returnAddr2').val(res.returnAddr2);
                            //출고기한
                            $('.shipBundleLayer').prop('disabled', false);
                            $('#shipReleaseDay').val(res.releaseDay).trigger('change');
                            $('#shipReleaseTime').val(res.releaseTime);
                            $('#shipHolidayExceptYn').val(res.holidayExceptYn);

                            //안심번호 서비스
                            $('input:radio[name="safeNumberUseYn"]:input[value="'+res.safeNumberUseYn+'"]').trigger('click');
                        }

                        // 배송유형
                        if (res.shipKind == 'BUNDLE') {
                            // 묶음
                            // $('.shipBundleLayer').prop('disabled', true);

                        } else {
                            // 상품별
                            $('.shipBundleLayer').prop('disabled', false);
                        }

                    }
                }
            });

        } else {
            $('#deliveryDetailTr').hide();
            $('#shipReleaseDay').val('');
            $('#safetyNoDispYnNm').html('-');

            item.ship.getShipReleaseDay(0);

            $('#shipHolidayExceptYn option:eq(0)').prop('selected', true);
            $('.shipBundleLayer').prop('disabled', false);

            item.ship.initShipAddInfo();
        }
    },
    getShipPolicyMainPop : function() {
        let partnerId = $('#partnerId').val();

        if (partnerId == "") {
            alert("판매자를 먼저 선택해주세요.");
            return false;
        }
        windowPopupOpen('/partner/popup/shipPolicyMainPop?schType=partnerIdPop&schKeyword=' + partnerId, "getShipPolicyMainPop", 1400, 1220);
    },
    /**
     * 배송정보 선택/상세정보 초기화
     */
    initShipInfoDetail : function() {
        itemMain.util.initSelectBoxOptions($('#shipPolicyNo'), true, '선택하세요');

        $('.deliveryDetail').html('');
        $('#deliveryDetailTr').hide();

        item.ship.initShipCheckBox();

    },
    /**
     * 배송정보 동일적용 체크박스 초기화
     */
    initShipCheckBox : function() {
        $('#copyPartnerAddr').prop('checked', false);
        $('#copyPartnerAddr').prop('checked', false);
        $('#copyReleaseAddr').prop('checked', false);
        $('#copyReleaseAddr').prop('checked', false);
        $("#returnAddr2, #releaseAddr2").prop('readonly', false);
    },
    /**
     * 배송정보 추가정보 초기화
     */
    initShipAddInfo : function() {
        // 반품/교환비(편도)
        $('#claimShipFee').val('');
        //출고지
        $('#releaseZipcode').val('');
        $('#releaseAddr1').val('');
        $('#releaseAddr2').val('');
        //회수지
        $('#returnZipcode').val('');
        $('#returnAddr1').val('');
        $('#returnAddr2').val('');
    },
    /**
     * 배송정보 출고기한 초기화
     */
    initShipInfoRelease : function() {
        $('#shipReleaseDay').val('');

        item.ship.getShipReleaseDay(0);

        $('#shipHolidayExceptYn option:eq(0)').prop('selected', true);
        $('.shipBundleLayer').prop('disabled', false);
    },
    /**
     * 배송정보 > 출고기한 날짜 선택
     * @param val
     */
    getShipReleaseDay : function(val) {
        let display = '';
        $("#releaseDetailInfo").show();

        if (val != 1) {
            display = 'none';
            $('#shipDays').show();
            $('#shipOneDay').hide();

            if (val == -1){
                $("#releaseDetailInfo").hide();
            }
        } else {
            $('#shipOneDay').show();
            $('#shipDays').hide();
        }

        $('#shipReleaseTimeSpanId').css('display', display);
    }
};

item.sale = {
    commissionTypeObj : {R : 'R', A : 'A'}, // 수수료부과기준 (정률, 정액)
    /**
     * 판매정보 > 구매수량제한 > 구매제한타입
     * @param _this
     */
    getPurchaseLimitDuration : function(_this) {
        $('.purchaseLimitDuration').css('display', '');
        $('.purchaseLimitDuration' + $(_this).val()).css('display', 'none');

        if ($(_this).val() == 'P') {
            $('#purchaseLimitDay').prop('disabled', false);

        } else {
            $('#purchaseLimitDay').prop('disabled', true);
        }
    },
    /**
     * 판매기간 셋팅
     * @param salePeriodYn
     * @param saleStartDt
     * @param saleEndDt
     */
    changeSalePeriodYn : function(salePeriodYn, saleStartDt, saleEndDt) {
        if (salePeriodYn == 'Y') {
            $('#saleStartDt, #saleEndDt').prop('disabled', false);
            $('#saleStartDt').val(saleStartDt);
            $('#saleEndDt').val(saleEndDt);
        } else {
            $('#saleStartDt, #saleEndDt').val('').prop('disabled', true);
        }
    },
    /**
     * 판매정보 > 수수료 원가 계산
     */
    calculateSaleCostPrice : function() {
        let costPrice = 0;
        let salePrice = Number($('#salePrice').val());
        let commissionType = $("#commissionType option:selected").val();
        let commissionRate = Number($('#commissionRate').val());
        let commissionPrice = Number($('#commissionPrice').val());

        if (commissionType == item.sale.commissionTypeObj.R) {
            // 수수료 정률: 판매가격 - (판매가격X수수료율)
            costPrice = salePrice * (100 - commissionRate) / 100;

        } else if (commissionType == item.sale.commissionTypeObj.A) {
            //수수료 정액: 판매가격 - 수수료금액
            costPrice = salePrice - commissionPrice;
        }

        if (!isNaN(costPrice)) {
            $('#costPrice').text($.jUtil.comma(costPrice) + '원');
        }
    },
    /**
     * 수수료 세팅 핸들러
     * @param commissionType
     * @param commissionRate
     * @param commissionPrice
     * @param idNm
     */
    setCommissionHandler : function(commissionType, commissionRate, commissionPrice, idNm) {
        if ($('#commissionRate').prop('readonly') == true && $('#commissionPrice').prop('readonly') == true) {
            switch (commissionType) {
                case item.sale.commissionTypeObj.R:
                    $('#'+idNm+'Rate').show();
                    $('#'+idNm+'Price').hide();
                    break;
                case item.sale.commissionTypeObj.A:
                    $('#'+idNm+'Rate').hide();
                    $('#'+idNm+'Price').show();
                    break;
            }
        } else {
            switch (commissionType) {
                case item.sale.commissionTypeObj.R:
                    item.sale.setCommissionRate(commissionRate, idNm);
                    break;
                case item.sale.commissionTypeObj.A:
                    item.sale.setCommissionPrice(commissionPrice, idNm);
                    break;
            }
        }
    },
    /**
     * 수수료 정률 세팅
     * @param commissionRate
     * @param idNm
     */
    setCommissionRate : function(commissionRate, idNm) {
        $('#'+idNm+'Rate').prop('readonly', false).val(commissionRate).show();
        $('#'+idNm+'Price').val('').hide().prop('readonly', true);
    },
    /**
     * 수수료 정액 세팅
     * @param commissionPrice
     * @param idNm
     */
    setCommissionPrice : function(commissionPrice, idNm) {
        $('#'+idNm+'Price').prop('readonly', false).val(commissionPrice).show();
        $('#'+idNm+'Rate').val('').hide().prop('readonly', true);
    },
    /**
     * 구매제한여부 컨트롤
     * @param obj
     */
    changePurchaseLimitYn : function (obj) {
        let purchaseLimitYn = $(obj).val();

        switch (purchaseLimitYn) {
            case 'Y' :
                if ($('#itemType').val() != 'T' && $('#itemType').val() != 'M') {
                    itemMain.util.setDisabledAll('purchaseLimitArea', false);
                }

                break;
            case 'N' :
                itemMain.util.setDisabledAll('purchaseLimitArea', true);
                $('#purchaseLimitQty').val(1);
                break;
        }
    },
    /**
     * 예약판매설정여부 컨트롤
     * @param rsvYn
     * @param rsvStartDt
     * @param rsvEndDt
     * @param shipStartDt
     */
    changeRsvYn : function(rsvYn, saleRsvNo, rsvStartDt, rsvEndDt, shipStartDt) {
        if (rsvYn == 'Y') {
            if (saleRsvNo) {
                $('#saleRsvNo').val(saleRsvNo);
                $('#rsvStartDt').val(rsvStartDt);
                $('#rsvEndDt').val(rsvEndDt);
                $('#shipStartDt').val(shipStartDt);
            }
            $('#rsvStartDt, #rsvEndDt, #shipStartDt').prop('disabled', false);
            item.sale.changeGiftYn('Y');
        } else {
            $('#rsvStartDt, #rsvEndDt, #shipStartDt').val('').prop('disabled', true);
            item.sale.changeGiftYn('N');
        }
    },
    changeGiftYn :function(giftYn) {
        if (giftYn == 'Y') {
            $('input[name="giftYn"][value="N"]').trigger('click');
            $('input[name="giftYn"][value="Y"]').prop('disabled', true);
        } else if ($('input:radio[name="rsvYn"]:checked').val() == 'N' && $('input[name="adultType"]:checked').val() == 'NORMAL'
                && $('#itemType').val() != 'T' && $('#itemType').val() != 'M'
        ) {
            $('input[name="giftYn"][value="Y"]').prop('disabled', false);
        }
    },
    /**
     * 재고수량변경 체크박스 변경 이벤트
     * @param obj
     */
    changeStockQtyCheck : function(obj) {
        if ($(obj).is(':checked')) {
            $('#stockQty').prop('readonly', false);
        } else {
            $('#stockQty').prop('readonly', true);
        }
    },
    /**
     * 상품유형 변경 이벤트
     * @param obj
     */
    changeItemType : function(obj, isDetail) {
        $('input[name="dcYn"][value="Y"]').prop('disabled', false);

        if (!isDetail) {
            $('input[name="purchaseLimitYn"][value="N"]').prop('disabled', false).trigger('click');
            $('#purchaseLimitDuration').val('O').trigger('change');
            $('#purchaseLimitQty').val(1).prop('disabled', false);
        }

        switch ($(obj).val()) {
            case 'E': //e-ticket상품
                $('.shipArea, .rentalArea').hide();
                item.sale.changeGiftYn('Y');
                if (!isDetail && item.sub.dcateCartYn == 'N') {
                    item.sale.setCartLimitYn('N');
                }
                $('#salePrice, #commissionRate, #commissionPrice').prop('readonly', false);
                //해외배송 사용안함 강제처리
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', true);
                $('input[name="globalAgcyYn"][value="N"]').prop('checked', true).trigger('change');
                break;
            case 'T': //렌탈
                $('.rentalArea').show();
            case 'M': //휴대폰-가입상품
                item.sale.changeGiftYn('Y');
                item.sale.setCartLimitYn('Y');
                $('#salePrice, #commissionRate, #commissionPrice').prop('readonly', true).val(0);
                $('.shipArea').hide();
                //할인가격 셋팅
                $('input[name="dcYn"][value="Y"]').prop('disabled', true);
                $('input[name="dcYn"][value="N"]').prop('checked', true).trigger('change');
                //최대구매수량 셋팅
                $('input[name="purchaseLimitYn"][value="Y"]').prop('checked', true).trigger('click');
                $('input[name="purchaseLimitYn"][value="N"]').prop('disabled', true);
                $('#purchaseLimitDuration').val('O').trigger('change').prop('disabled', true);
                $('#purchaseLimitQty').val(1).prop('disabled', true);
                //해외배송 사용안함 강제처리
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', true);
                $('input[name="globalAgcyYn"][value="N"]').prop('checked', true).trigger('change');
                break;
            default :
                $('.shipArea').show();
                $('.rentalArea').hide();
                item.sale.changeGiftYn('N');
                if (!isDetail && item.sub.dcateCartYn == 'N') {
                    item.sale.setCartLimitYn('N');
                }

                $('#salePrice, #commissionRate, #commissionPrice').prop('readonly', false);

                //해외배송 사용안함 강제처리 해제
                $('input[name="globalAgcyYn"][value="Y"]').prop('disabled', false);
                break;
        }
    },
    /**
     * 성인상품유형 변경 컨트롤
     * @param obj
     */
    changeAdultType : function(obj) {
        switch ($(obj).val()) {
            case 'NORMAL':
                $('#imgDispYn').val('Y').prop('disabled', true);
                item.sale.changeGiftYn('N');
                item.sale.changeItemType($('#itemType'), true);
                break;
            case 'ADULT':
                if (item.sub.imgDispYn == 'Y') {
                    $('#imgDispYn').val('N').prop('disabled', false);
                } else {
                    $('#imgDispYn').val('N').prop('disabled', true);
                }
                item.sale.changeGiftYn('Y');
                break;
            case 'LOCAL_LIQUOR':
                $('#imgDispYn').val('Y').prop('disabled', true);
                item.sale.changeGiftYn('Y');
                break;
        }
    },
    /**
     * 장바구니 제한 UI적용
     * @param cartLimitYn
     */
    setCartLimitYn : function(cartLimitYn) {
        if (cartLimitYn == 'Y') {
            $('input[name="cartLimitYn"][value="Y"]').trigger('click');
            $('input[name="cartLimitYn"][value="N"]').prop('disabled', true);
        } else {
            if ($('#itemType').val() != 'T' && $('#itemType').val() != 'M') {
                $('input[name="cartLimitYn"]').prop('disabled', false);
                $('input[name="cartLimitYn"][value="N"]').trigger('click');
            }
        }
    },
    /**
     * 성인 인증 제한 UI적용
     * @param adultLimitYn
     */
    setAdultLimitYn : function(adultLimitYn, imgDispYn) {
        if (adultLimitYn == 'Y') {
            $('input[name="adultType"]').prop('disabled', true);
            $('input[name="adultType"][value="ADULT"]').prop('disabled', false).trigger('click');
            item.sub.imgDispYn = 'N';

            if (imgDispYn) {
                $('#imgDispYn').val(imgDispYn);
            }
        } else {
            $('input[name="adultType"]').prop('disabled', false);
            item.sub.imgDispYn = 'Y';

            if (item.isReg) {
                $('input[name="adultType"][value="NORMAL"]').trigger('click');
            }
        }

    },
    /**
     * 총렌탈료 계산
     */
    changeRental : function() {
        let rentalFee = $('#rentalFee').val();
        let rentalPeriod = $('#rentalPeriod').val();
        let rentalRegFee = $('#rentalRegFee').val();

        if (!$.jUtil.isEmpty(rentalFee) && !$.jUtil.isEmpty(rentalPeriod) && !$.jUtil.isEmpty(rentalRegFee)) {
            $('#totalRentalFee').text($.jUtil.comma(Number(rentalFee) * Number(rentalPeriod) + Number(rentalRegFee)));
        }

    },
    /**
     * 파트너 조회
     * @returns {boolean}
     */
    getPartner : function() {
        let businessNm = $('#businessNm').val();

        if (2 > businessNm.length) {
            alert('검색 키워드는 2자 이상 입력해주세요.');
            $('#businessNm').focus();
            return false;
        } else if ($('#businessNm').val() && $.jUtil.isNotAllowInput($('#businessNm').val(), ['SPC_SCH'])) {
            alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
            $.jUtil.patternReplace($('#businessNm'), ['SPC_SCH'], "");
            $('#businessNm').focus();
            return false;
        }

        let sellerSearchForm = {
            schType         : 'partnerId_businessNm',
            schKeyword      : businessNm,
        };

        CommonAjax.basic({
            url: '/partner/getSellerList.json',
            data: sellerSearchForm,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                let resultCount = res.length;

                if (resultCount == 0) {
                    $('#searchPartnerLayer').html('');
                    $('#searchPartnerLayer').append(
                            '<li class="ui-menu-item-wrapper" >검색결과가 없습니다.</li>');

                } else {
                    $('#searchPartnerLayer').html('');

                    let searchResult;

                    for (let idx = 0; resultCount > idx; idx++) {
                        let data = res[idx];
                        let partnerInfo = new Array();
                        partnerInfo.push(data.businessNm);
                        if (data.businessNmEng) {
                            partnerInfo.push(data.businessNmEng);
                        }
                        partnerInfo.push(data.partnerId);

                        searchResult = partnerInfo.join(' | ');

                        $('#searchPartnerLayer').append(
                                '<li class="ui-menu-item-wrapper" id="partnerSearchList_'
                                + data.partnerId + '" data-sale-type="'+data.saleType+'">' + searchResult + '</li>');
                    }

                    item.sale.addPartner();
                }

                $('#searchPartnerLayer').show();
            }
        });
    },
    /**
     * 파트너 추가
     */
    addPartner : function() {
        $('li[id^=partnerSearchList_]').click(function() {
            let idArr = $(this).prop('id').split('_');
            let htmlArr = $(this).html().split('|');
            let partnerId = idArr[1];
            let businessNm = htmlArr[0];

            item.sale.setPartnerSaleTypeUI($(this).data('sale-type'));
            item.sale.addPartnerHtml(partnerId, $.trim(businessNm));

            $('#searchPartnerLayer').hide();
        });
    },
    /**
     * 파트너 추가 html
     * @param partnerId
     * @param businessNm
     */
    addPartnerHtml : function(partnerId, businessNm, shipPolicyNo) {
        if ($.jUtil.isEmpty(partnerId)) {
            item.sale.setPartnerYn('N');
        } else {
            item.sale.setPartnerYn('Y');

            item.sale.getBaseCommissionRate($('#partnerId').val(), $('#cateCd4').val());

            //렌탈,휴대폰,e-ticket 권한 처리
            if (!shipPolicyNo && $.jUtil.isEmpty($('#itemNo').val())) {
                CommonAjax.basic({
                    url: '/partner/getPartnerTypesList.json',
                    data: {partnerId: partnerId},
                    method: 'GET',
                    callbackFunc: function (res) {
                        let itemType = $('#itemType').val();
                        $('#itemType option[value="T"], #itemType option[value="M"], #itemType option[value="E"]').hide();

                        for (let i in res) {
                            $('#itemType option[value="' + res[i].types + '"]').show();
                        }

                        if ($('#itemType option[value="' + itemType + '"]').css('display') == 'none') {
                            $("#itemType option:eq(0)").prop("selected",true);
                        } else {
                            $("#itemType").val(itemType);
                        }
                    }
                });
            }
        }

        if (shipPolicyNo) {
            $('#businessNm').prop('readonly', true);
            $('#getPartnerBtn').prop('disabled', true);
            $('#partnerArea').find('button').hide();
        }

        $('#partnerId').val(partnerId);
        $('#businessNm').val(businessNm);
        $('#spanPartnerNm').html(businessNm + '(' + partnerId + ')');
        item.ship.getSellerShipList(partnerId, shipPolicyNo);
    },
    setPartnerSaleTypeUI : function(saleType) {
        if (saleType == 'N') {
            $('.saleTypeNArea').show();
            $('.saleTypeDArea').hide();
        } else {
            $('.saleTypeNArea').hide();
            $('.saleTypeDArea').show();
        }
    },
    /**
     * 파트너 UI
     * @param val
     */
    setPartnerYn : function(val) {
        if (val == 'N') {
            $('#partnerArea').hide();
            $('#partnerId, #businessNm').val('');
            $('#spanPartnerNm').html('');

        } else {
            $('#partnerArea').show();
            $('#partnerId').val(0);
        }
    },
    getBaseCommissionRate : function(partnerId, dcateCd) {
        if (partnerId && dcateCd) {
            CommonAjax.basic({
                url: '/item/commission/getBaseCommissionRate.json',
                data: {partnerId: partnerId, dcateCd: dcateCd},
                method: 'GET',
                callbackFunc: function (res) {
                    $('#baseCommissionRate').html(res);
                }
            });
        }
    },
    setDcRate : function() {
        if (!$.jUtil.isEmpty($('#salePrice').val()) && !$.jUtil.isEmpty($('#dcPrice').val())) {
            let dcRate = ($('#salePrice').val() - $('#dcPrice').val()) / $('#salePrice').val() * 100;
            $('#dcRate').text(dcRate.toFixed(2));
        } else {
            $('#dcRate').text('0');
        }
    }
};

item.ext = {
    event : function() {
        $('.isCertRadio').on('click', function(){
            item.ext.changeCert($(this));
        });

        $('#certTable').on('keyup', '.certNoSubArea', function() {
            let innerHtml = [];
            $('.certNoSubArea').find('.certNoSub').each(function(){
                innerHtml.push($(this).val());
            });

            $(this).siblings('.certNo').val(innerHtml.join('|'));
        });

        $('#certTable').on('change', '.certType', function() {
            $(this).closest('.form').find('.certNo').val('');

            if ($.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').val('').show().prop('placeholder', '');
                $(this).closest('.form').find('.energy').hide();
                $(this).closest('.form').find('.certCheck').show();
                $(this).closest('.form').find('.certNoSubArea').html('');
            } else {
                if ($(this).find('option:selected').data('ref1') == 'selectbox') {
                    $(this).closest('.form').find('.certNo').hide();
                    $(this).closest('.form').find('.energy').show();
                } else {
                    $(this).siblings('.certNo').prop('placeholder', $(this).find('option:selected').data('ref1'));
                    $(this).closest('.form').find('.certNo').show();
                    $(this).closest('.form').find('.energy').hide();
                }

                let ref2 = $(this).find('option:selected').data('ref2');
                if ($.jUtil.isEmpty(ref2)) {
                    $(this).closest('.form').find('.certNo').hide();
                    $(this).closest('.form').find('.certNoSubArea').html('');
                } else if(ref2 == '1') {
                    $(this).closest('.form').find('.certNo').show();
                    $(this).closest('.form').find('.certNoSubArea').html('');
                } else if(ref2 == '2') {
                    let ref1Arr = $(this).find('option:selected').data('ref1').split('_');
                    $(this).closest('.form').find('.certNoSubArea').html('<input type="text" class="ui input mg-r-5 certNoSub" style="width:300px;" maxlength="25" placeholder="'+ref1Arr[0]+'"/><input type="text" class="ui input mg-r-5 certNoSub" style="width:300px;" maxlength="25" placeholder="'+ref1Arr[1]+'"/>');
                    $(this).closest('.form').find('.certNo').hide();
                } else {
                    $(this).closest('.form').find('.certNoSubArea').html('');
                }

                let ref3 = $(this).find('option:selected').data('ref3');
                if (!$.jUtil.isEmpty(ref3)) {
                    $(this).closest('.form').find('.certCheck').hide();
                } else {
                    $(this).closest('.form').find('.certCheck').show();
                }

                $(this).closest('.form').find('.certNo').prop('readonly', false);
            }
        });

        $('#certTable').on('change', '.energy', function() {
            if (!$.jUtil.isEmpty($(this).val())) {
                $(this).closest('.form').find('.certNo').val($(this).val());
            }
        });

        $('#certTable').on('click', '.certDelete', function() {
            $(this).closest('.addObj').remove();
        });

        $('.certAdd').on('click', function() {
            if ($(this).closest('td').find('.isCertRadio:checked').val() == 'Y' && $(this).closest('td').find('.certType').length < 20) {
                let idNum = $('.addObj').length;

                $(this).closest('td').append(
                        '<div id="cert_'+idNum+'" class="addObj ui form inline mg-t-10"><input type="hidden" name="certList[].isCert" value="Y" ><input type="hidden" name="certList[].itemCertSeq" class="itemCertSeq"><input type="hidden" name="certList[].certGroup" value="'
                        + $(this).closest('td').find('.certGroup').val() + '">'
                        + $(this).siblings('.addArea').html() + '<button type="button" class="ui button medium dark-blue mg-r-5 certDelete">삭제</button></div>');

                $('#cert_'+idNum).find('.certType').trigger('change');
                $('#cert_'+idNum).find('.certNo').prop('readonly', false).prop("placeholder","");
            }
        });
    },
    changeCert : function(obj) {
        let _tr = obj.closest('tr');

        _tr.find('.isCert').val(obj.val());

        switch (obj.val()) {
            case 'Y':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'E':
                _tr.find('.certExemptType, .certNo, .certType').prop('disabled', false).prop('readonly', false).val('').show();
                break;
            case 'N':
                _tr.find('.certExemptType').prop('disabled', true).hide();
                _tr.find('.certNo, .certType').prop('disabled', true).prop('readonly', true).val('');
                _tr.find('.addObj').remove();
                _tr.find('.certNo').prop('placeholder','').show();
                _tr.find('.certNoSubArea').html('');
                break;
        }
    },
    /**
     * 안전인증 조회
     * @param obj
     * @returns {boolean}
     */
    checkCert : function (obj) {

        let _row = obj.closest('.form');
        let certType = _row.find('.certType').val();

        if (!$.jUtil.isEmpty(certType)) {

            let certGroup = obj.data('cert-group');
            let certNo = _row.find('.certNo').val().replace(/^\s*/, "");

            if (!$.jUtil.isEmpty(certGroup) && !$.jUtil.isEmpty(certNo)) {

                CommonAjax.basic({
                    url: "/item/checkCert.json"
                    , data: {
                        certGroup: certGroup,
                        certNo: certNo
                    }
                    , method: "GET"
                    , callbackFunc: function (res) {

                        if (res.data.returnKey == 'Y') {
                            alert('완료 되었습니다.');
                            _row.find('.certNo').val(certNo);
                            _row.find('.certNo').prop('readonly', true);
                        } else {
                            alert("유효하지 않은 번호입니다. ");
                            _row.find('.certNo').prop('readonly', false);
                        }
                    },
                    error: function (e) {
                        if (e.responseJSON.errors[0].detail != null) {
                            alert(e.responseJSON.errors[0].detail);
                        } else {
                            alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                        }
                    }
                });
            } else {
                alert("인증번호를 확인해주세요. ");
            }

        } else {
            alert("인증유형을 선택해주세요");
            return false;
        }
    }
};

item.sub = {
    dcateCartYn : 'N',
    imgDispYn   : 'Y',
    /**
     * 연관상품 초기화
     */
    initRelationItem : function() {
        item.relationInfo.partnerId = '';
        item.relationInfo.scateCd   = '';
        item.relationInfo.adultType = '';
    },
    setIsbnUI : function(isbnYn) {
        if (isbnYn == 'Y') {
            $('#isbnArea').show();
        } else {
            $('#isbnArea').hide();
            $('#isbn').val('');
        }
    },
    changeCate4 : function(obj) {
        if ($(obj).val()) {
            CommonAjax.basic({
                url: '/common/category/getCategoryDivide.json',
                data: {dcateCd: $(obj).val()},
                method: 'GET',
                callbackFunc: function (res) {
                    //장바구니 제한
                    item.sub.dcateCartYn = res.cartYn;
                    item.sale.setCartLimitYn(res.cartYn);

                    //19성인인증 제한
                    item.sale.setAdultLimitYn(res.adultLimitYn);

                    item.sale.changeAdultType($('input[name="adultType"]:checked'));

                    item.sub.setIsbnUI(res.isbnYn);
                }
            });
        }
    }
};