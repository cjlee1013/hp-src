/**
 * 상품관리 > 점포상품관리 > 상품 등록/수정 > 그리드
 */
// 대표판매정보 리스트 그리드
let saleDFListGrid = {
    gridView : new RealGridJS.GridView("saleDFListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        saleDFListGrid.initGrid();
        saleDFListGrid.initDataProvider();
    },
    initGrid : function () {
        saleDFListGrid.gridView.setDataSource(saleDFListGrid.dataProvider);

        saleDFListGrid.gridView.setStyles(saleDFListGridBaseInfo.realgrid.styles);
        saleDFListGrid.gridView.setDisplayOptions(saleDFListGridBaseInfo.realgrid.displayOptions);
        saleDFListGrid.gridView.setColumns(saleDFListGridBaseInfo.realgrid.columns);
        saleDFListGrid.gridView.setOptions(saleDFListGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        saleDFListGrid.dataProvider.setFields(saleDFListGridBaseInfo.dataProvider.fields);
        saleDFListGrid.dataProvider.setOptions(saleDFListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        saleDFListGrid.dataProvider.clearRows();
        saleDFListGrid.dataProvider.setRows(dataList);
    }
};