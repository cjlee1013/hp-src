/**
 * 브랜드/제조사관리 > 브랜드 관리
 */
$(document).ready(function() {
    brandListGrid.init();
    brand.init();
    CommonAjaxBlockUI.global();
});

// 브랜드관리 그리드
var brandListGrid = {
    gridView : new RealGridJS.GridView("brandListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        brandListGrid.initGrid();
        brandListGrid.initDataProvider();
        brandListGrid.event();
    },
    initGrid : function() {
        brandListGrid.gridView.setDataSource(brandListGrid.dataProvider);

        brandListGrid.gridView.setStyles(brandListGridBaseInfo.realgrid.styles);
        brandListGrid.gridView.setDisplayOptions(brandListGridBaseInfo.realgrid.displayOptions);
        brandListGrid.gridView.setColumns(brandListGridBaseInfo.realgrid.columns);
        brandListGrid.gridView.setOptions(brandListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        brandListGrid.dataProvider.setFields(brandListGridBaseInfo.dataProvider.fields);
        brandListGrid.dataProvider.setOptions(brandListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        brandListGrid.gridView.onDataCellClicked = function(gridView, index) {
            brand.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        brandListGrid.dataProvider.clearRows();
        brandListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(brandListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "브랜드관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        brandListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 브랜드관리
var brand = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#itemDispYn').bindChange(brand.changeDisp);
        $('#useYn').bindChange(brand.changeUseYn);
        $('input[name=dispNm]').bindChange(brand.changeDisp);

        $('#searchResetBtn').bindClick(brand.searchFormReset);
        $('#setBrandBtn').bindClick(brand.setBrand);
        $('#resetBtn').bindClick(brand.resetForm);
        $('#searchBtn').bindClick(brand.search);
        $('#excelDownloadBtn').bindClick(brand.excelDownload);

        $('#brandNm').calcTextLength('keyup', '#textCountKr');
        $('#brandNmEng').calcTextLength('keyup', '#textCountEng');
        $('#brandDesc').calcTextLength('keyup', '#textCountDesc');

        $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
        $('#brandNm, #brandNmEng').notAllowInput('focusout', ['SYS_DIVISION']);
    },
    /**
     * 상품명 노출여부 값에 따라 전시명 선택 여부가 변경됨
     * 전시명 선택에 따라 안내문구가 변경됨
     */
    changeDisp : function () {
        if($('#itemDispYn').val() == 'Y') {
            if($("input[name=dispNm][value=KOR]").prop("checked") == true) {
                $('#prodDispDesc').html("(*한글 브랜드명 기준으로 노출됩니다.)");
            } else {
                $('#prodDispDesc').html("(*영문 브랜드명 기준으로 노출됩니다.)");
            }
            $('#prodDispDesc').show();
        } else {
            $('#prodDispDesc').hide();
        }

        if ( $(":input:radio[name=dispNm]:checked").val() == 'KOR' ){
            $('#initialKor').show();
            $('#initialEng').hide();
        } else {
            $('#initialKor').hide();
            $('#initialEng').show();
        }
    },
    /**
     * 사용여부 값에 따라 상품명 노출여부 선택 여부가 변경됨
     */
    changeUseYn : function () {
        if($('#useYn').val() == 'Y') {
            $("#itemDispYn").prop("disabled", false);
        } else {
            $("#itemDispYn").val('N').change().prop("disabled", true);
        }
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        brandListGrid.excelDownload();
    },
    /**
     * 브랜드 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }

        CommonAjax.basic({url:'/item/brand/getBrandList.json?' + $('#brandSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            brandListGrid.setData(res);
            $('#brandTotalCount').html($.jUtil.comma(brandListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchType').val('brandNm');
        $('#searchKeyword').val('');
    },
    /**
     * 브랜드 등록/수정
     */
    setBrand : function() {
        let isReg = $.jUtil.isEmpty($('#brandNo').val());

        if($('#brandNo').val() != "") {
            $('#isImageUpdate').val('1');
        }

        if($.jUtil.isEmpty($('#brandNm').val())) {
            alert("브랜드명은 필수입력항목입니다.");
            $('#brandNm').focus();
            return false;
        }

        if ($.jUtil.isNotAllowInput($("#brandNm").val(), ['SYS_DIVISION'])) {
            alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]));
            $('#brandNm').focus();
            return false;
        }

        if($("input[name=dispNm][value=ENG]").prop("checked") == true) {
            if($('#brandNmEng').val() == "") {
                alert("전시명 영문을 선택 하셨으므로 브랜드명(영문)은 반드시 입력해주세요.");
                $('#brandNmEng').focus();
                return false;
            }
        }

        if(!$.jUtil.isEmpty($('#brandNmEng').val())) {
            if ($.jUtil.isNotAllowInput($("#brandNmEng").val(), ['SYS_DIVISION'])) {
                alert($.jUtil.getNotAllowInputMessage(["SYS_DIVISION"]));
                $('#brandNmEng').focus();
                return false;
            }
        }

        if ( $(":input:radio[name=dispNm]:checked").val() == 'KOR' ){
            $('#initial').val( $('#initialKor').val() );
        } else {
            $('#initial').val( $('#initialEng').val() );
        }

        CommonAjax.basic({
            url: '/item/brand/setBrand.json',
            data: $('#brandSetForm').serializeArray(),
            method: 'POST',
            callbackFunc: function (res) {
                alert(res.returnMsg);
                if (isReg) {
                    brand.resetForm();
                }
            }
        });
    },
    /**
     * 브랜드 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#brandNo, #brandNm, #brandNmEng, #brandDesc, #siteUrl').val('');
        $('#useYn').val('Y');
        $('#itemDispYn').val('Y');

        $('#textCountDesc, #textCountEng, #textCountKr').html('0');

        $("input[name=dispNm][value=KOR]").prop("checked", true);

        $('#initialKor').val('-');
        $('#initialEng').val('-');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        this.resetForm();
        var rowDataJson = brandListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: '/item/brand/getBrand.json?brandNo=' + rowDataJson.brandNo,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                $('#brandNo').val(res.brandNo);
                $('#brandNm').val(res.brandNm).setTextLength('#textCountKr');
                $('#brandNmEng').val(res.brandNmEng).setTextLength(
                        '#textCountEng');
                $('#useYn').val(res.useYn);
                $('#itemDispYn').val(res.itemDispYn);
                $('#siteUrl').val(res.siteUrl);
                $('#brandDesc').val(res.brandDesc);

                $("input[name=dispNm][value=" + res.dispNm + "]").prop("checked", true);

                if (res.dispNm == 'KOR') {
                    $('#initialKor').val(res.initial);
                } else {
                    $('#initialEng').val(res.initial);
                }
                brand.changeDisp();
            }
        });
    }
};
