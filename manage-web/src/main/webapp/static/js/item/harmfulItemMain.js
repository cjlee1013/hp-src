/**
 * 심사관리 > 위해상품 관리
 */
$(document).ready(function() {

    harmfulItemListGrid.init();
    harmfulItem.init();

    harmfulDSItemListGrid.init();
    harmfulDSItem.init();

    CommonAjaxBlockUI.global();

});

// DS 위해상품 그리드
var harmfulDSItemListGrid = {
    gridView : new RealGridJS.GridView("harmfulDSItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        harmfulDSItemListGrid.initGrid();
        harmfulDSItemListGrid.initDataProvider();
    },
    initGrid : function() {

        harmfulDSItemListGrid.gridView.setDataSource(harmfulDSItemListGrid.dataProvider);
        harmfulDSItemListGrid.gridView.setStyles(harmfulDSItemListGridBaseInfo.realgrid.styles);
        harmfulDSItemListGrid.gridView.setDisplayOptions(harmfulDSItemListGridBaseInfo.realgrid.displayOptions);
        harmfulDSItemListGrid.gridView.setColumns(harmfulDSItemListGridBaseInfo.realgrid.columns);
        harmfulDSItemListGrid.gridView.setOptions(harmfulDSItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        harmfulDSItemListGrid.dataProvider.setFields(harmfulDSItemListGridBaseInfo.dataProvider.fields);
        harmfulDSItemListGrid.dataProvider.setOptions(harmfulDSItemListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        harmfulDSItemListGrid.dataProvider.clearRows();
        harmfulDSItemListGrid.dataProvider.setRows(dataList);

    },
    addData : function(dataList){
        harmfulDSItemListGrid.dataProvider.addRows(dataList);
    },
    delData : function(dataList){
        harmfulDSItemListGrid.dataProvider.removeRows(dataList,false);
    }


}
var harmfulDSItem = {
    init : function() {
        this.bindingEvent();
    },
    bindingEvent : function() {
        $('#addItem').bindClick(harmfulDSItem.addItemPopUp);
        $('#deleteItem').bindClick(harmfulDSItem.deleteItem);
        $('#searchUpssPop').bindClick(harmfulDSItem.searchUpssPop);
    },
    /**
     * 대체상품조회팝업 -> addItem
     */
    addItemPopUp : function(){

        if( $("input[name='statusResult']:checked").val() == '2'){
            alert("처리상태가 미취급일 경우 상품등록이 불가 합니다.");
            return false;
        }

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=harmfulDSItem.addItem&isMulti=Y&mallType=DS&storeType=DS' , 1084, 650);

    },
    /**
     * DS 위해 상품 추가
     */
    addItem : function(resDataArr) {


        var gridList = harmfulDSItemListGrid.dataProvider.getJsonRows();
        var addItemList = new Array();

        //기 등록되어 있는 항목 중복 제거 후 상품리스트 조회
        var duplicateYn ='N';
        resDataArr.forEach(function (additem , index) {
            duplicateYn ='N';
            gridList.forEach(function (griditem) {

                if(additem.itemNo == griditem.itemNo){
                    duplicateYn='Y'
                }
            });
            if(duplicateYn =='N'){
                addItemList.push(additem.itemNo);

            }
        });
        CommonAjax.basic({url:'/item/harmful/getDSItemList.json' ,data: {itemList :addItemList.toString() } , method:"GET", successMsg:null, callbackFunc:function(res) {
                harmfulDSItemListGrid.addData(res);
            }});
    },
    /**
     * DS 위해 상품 초기화
     */
    deleteItem : function(){

        var checkedVal = harmfulDSItemListGrid.gridView.getCheckedRows(true);

        if (checkedVal.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }
        harmfulDSItemListGrid.delData(checkedVal);

    },
    searchUpssPop  : function () {

        var setCdInsptmachiPop = $("#setCdInsptmachi").val(); // KA
        var setNoDocumentPop = $("#setNoDocument").val(); //
        var setSeqPop = $("#setSeq").val(); // 01

        if( !$.jUtil.isEmpty(setCdInsptmachiPop) && !$.jUtil.isEmpty(setNoDocumentPop) && !$.jUtil.isEmpty(setSeqPop) ) {

            var popUrl = 'http://upss.gs1kr.org/home/harzd/harzdDetail.gs1?cdInsptmachi=' + setCdInsptmachiPop + '&seq=' + setSeqPop + '&noDocument=' + setNoDocumentPop;
            var _width = 1350;
            var _height = 650;

            var popupNewWin = window.open(popUrl, '_blank', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=yes,width='+_width+',height='+_height, '');
            if (popupNewWin == null) {
                alert("팝업 차단기능 혹은 팝업차단 프로그램이 동작중입니다. 팝업 차단 기능을 해제한 후 다시 시도하세요.");
            }

        } else {
            alert('선택 후 클릭해 주세요.');
        }

    }

}
// 위해상품 그리드
var harmfulItemListGrid = {

    gridView : new RealGridJS.GridView("harmfulItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        harmfulItemListGrid.initGrid();
        harmfulItemListGrid.initDataProvider();
        harmfulItemListGrid.event();

    },
    initGrid : function() {

        harmfulItemListGrid.gridView.setDataSource(harmfulItemListGrid.dataProvider);
        harmfulItemListGrid.gridView.setStyles(harmfulItemListGridBaseInfo.realgrid.styles);
        harmfulItemListGrid.gridView.setDisplayOptions(harmfulItemListGridBaseInfo.realgrid.displayOptions);
        harmfulItemListGrid.gridView.setColumns(harmfulItemListGridBaseInfo.realgrid.columns);
        harmfulItemListGrid.gridView.setOptions(harmfulItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        harmfulItemListGrid.dataProvider.setFields(harmfulItemListGridBaseInfo.dataProvider.fields);
        harmfulItemListGrid.dataProvider.setOptions(harmfulItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        harmfulItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            harmfulItem.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        harmfulItemListGrid.dataProvider.clearRows();
        harmfulItemListGrid.dataProvider.setRows(dataList);

    },

};

// 위해상품관리
var harmfulItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
    },
    initSearchDate : function (flag) {
        harmfulItem.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        harmfulItem.setDate.setEndDate(0);
        harmfulItem.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        //검색 역영
        $('#searchBtn').bindClick(harmfulItem.search);   // 검색
        $('#searchResetBtn').bindClick(harmfulItem.searchFormReset); // 검색폼 초기화
        $('#excelDownloadBtn').bindClick(harmfulItem.excelDownload); // 검색폼 초기화
        $('input[name="statusResult"]').bindChange(harmfulItem.setStatusResult); //처리상태변경 이벤트
        $('#setHarmfulItemBtn').bindClick(harmfulItem.setHarmfulItem); // 위해상품 정보 저장
        $('#resetBtn').bindClick(harmfulItem.resetForm); // 위해상품 정보 저장


    },
    /**
     * 위해상품 검색
     */
    search : function() {

        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            harmfulItem.initSearchDate('-30d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        CommonAjax.basic({url:'/item/harmful/getHarmfulItemList.json?' + $('#harmfulItemSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                harmfulItem.resetForm();
                harmfulItemListGrid.setData(res);
                $('#harmfulItemTotalCount').html($.jUtil.comma(harmfulItemListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $('#searchPeriodType').val('RECVDT');  //조회기간
        harmfulItem.initSearchDate('-30d');
        $('#searchItemStatus').val('');   //처리상태
        $('#searchItemType').val('');     // 유형
        $('#searchInstitute').val('');    //검사기관
        $('#searchType').val('NMPROD');  // 검색어 구분
        $('#searchKeyword').val('');    //검색어

    },
    /**
     * 위해 상품 excelDownload
     */
    excelDownload: function() {
        if(harmfulItemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "위해상품관리"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        harmfulItemListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        harmfulItem.resetForm();

        var rowDataJson = harmfulItemListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({url:'/item/harmful/getHarmfulItemDetail.json?'
            , data:{cdInsptmachi: rowDataJson.cdInsptmachi
            , noDocument: rowDataJson.noDocument , seq: rowDataJson.seq}
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(res) {
                //위해상품 상세 정보
                for (var item in res) {
                    if(item.startsWith("imgAttach")  && !$.jUtil.isEmpty(res[ item ])){  // 위해 상품 등록 이미지
                        //$('#'+item).attr('src', res[ item ]);
                        //$('#'+item).css("display","block");
                        //$('#'+item).bindClick(harmfulItem.harmfullImagePopUp);


                    }else{ // 위해 상품 기본정보

                        $('#'+item).html(res[ item ]);

                    }
                }

                harmfulDSItemListGrid.setData(res.harmfulDSItemListSelectDtoList);

                //위해 상품 변경을 위한 hidden값 설정
                $("#setNoDocument").val(res.noDocument);
                $("#setSeq").val(res.seq);
                $("#setCdInsptmachi").val(res.cdInsptmachi);

                // 처리 상태 변경
                $('input[name="statusResult"][value="' + res.statusResultCode + '"]').trigger('click');

            }});

    },
    setStatusResult : function(status) {

        if(typeof status == 'object') {
           if(status.value == '1'){
               if(harmfulDSItemListGrid.gridView.getItemCount() < 1 ){
                   alert("차단 완료된 위해상품을 등록해주세요.");
                   $('input[name="statusResult"][value="0"]').trigger('click');
               }
           }
           else if(status.value == '2'){
               if(harmfulDSItemListGrid.gridView.getItemCount() > 1 ){
                   alert("차단 완료된 위해상품이 있어 상태변경이 불가합니다.\n" +
                       "위해 상품을 잘못 등록한 경우 등록된 상품을 먼저 삭제해주세요.");
                   $('input[name="statusResult"][value="0"]').trigger('click');
               }
           }
        }
    },
    /**
     * 위해상품 등록/수정
     */
    setHarmfulItem : function() {
        var setHarmfulItem = $('#harmfulItemSetForm').serializeObject();

        setHarmfulItem.statusResult = $("input[name='statusResult']:checked").val();
        setHarmfulItem.itemNo = harmfulDSItemListGrid.dataProvider.getFieldValues("itemNo", 0, -1);

        CommonAjax.basic({
            url : '/item/harmful/setHarmfulItem.json'
            , data : JSON.stringify(setHarmfulItem)
            , method : "POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                harmfulItem.resetForm();
                alert(res.returnMsg);
            }
        });
    },
    /**
     * 위해상품 입력/수정 폼 초기화
     */
    resetForm : function() {

        $("td[name=itemDesc]").html('');
        $("[id^='imgAttach']").attr('src', '');
        $("[id^='imgAttach']").css("display","none");
        $("[id^='imgAttach']").off("click"); // 기존 등록 되어 있던 이벤트 삭제
        $("#setNoDocument").val('');
        $("#setSeq").val('');
        $("#setCdInsptmachi").val('');
        $('input[name="statusResult"][value="0"]').trigger('click');
        harmfulDSItemListGrid.dataProvider.clearRows();
    },
    /**
     * 위해상품 이미지 팝업
     */
    harmfullImagePopUp : function(img){

        var width=img.naturalWidth;
        var height=img.naturalHeight;

        if(img.naturalWidth > 1000){
            width=1000;
        }
        if(img.naturalHeight > 1000){
            height=1000;
        }
        $.jUtil.openNewPopup('/item/popup/getImagePopUp?imgUrl=' +img.src ,width, height);

    }



};
