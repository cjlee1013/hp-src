$(document).ready(function () {
    expCategoryItemMng.init();
    expCategoryItemGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var isEdit = false;

var expCategoryItemGrid = {
    gridView: new RealGridJS.GridView("expCategoryItemGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        expCategoryItemGrid.initGrid();
        expCategoryItemGrid.initDataProvider();
        expCategoryItemGrid.event();
    },
    initGrid: function () {
        expCategoryItemGrid.gridView.setDataSource(expCategoryItemGrid.dataProvider);

        expCategoryItemGrid.gridView.setStyles(expCategoryItemGridBaseInfo.realgrid.styles);
        expCategoryItemGrid.gridView.setDisplayOptions(expCategoryItemGridBaseInfo.realgrid.displayOptions);
        expCategoryItemGrid.gridView.setColumns(expCategoryItemGridBaseInfo.realgrid.columns);
        expCategoryItemGrid.gridView.setOptions(expCategoryItemGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        expCategoryItemGrid.dataProvider.setFields(expCategoryItemGridBaseInfo.dataProvider.fields);
        expCategoryItemGrid.dataProvider.setOptions(expCategoryItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        expCategoryItemGrid.gridView.onDataCellClicked = function (gridView, index) {
            expCategoryItemMng.getGrid(index.dataRow);
        };
    },
    setData: function (dataList) {
        expCategoryItemGrid.dataProvider.clearRows();
        expCategoryItemGrid.dataProvider.setRows(dataList);
    }
};

var expCategoryItemMng = {
    init: function () {
        this.event();
        expCategoryItemMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'), 'search');
        expCategoryItemMng.setCategorySelectBox(1, '', '', $('#formCateCd1'), 'set');
        expCategoryItemMng.setCategorySelectBox(2, '', '', $('#formCateCd2'), 'set');
    },
    event: function () {
        // 검색 버튼
        $('#searchBtn').click(function () {
            expCategoryItemMng.search();
        });

        // 상품 검색 버튼
        $('#addItemPopUp').click(function () {
            expCategoryItemMng.addItemPopUp();
        });

        // 카테고리 상품 저장 버튼 클릭
        $('#setCategoryItemBtn').click(function () {
            expCategoryItemMng.setCategoryItem();
        });

        //카테고리 리셋 버튼 클릭
        $('#resetBtn').click(function () {
            expCategoryItemMng.formReset();
        });

        //카테고리 검색 리셋 버튼 클릭
        $('#searchResetBtn').click(function () {
            expCategoryItemMng.searchFormReset();
        });

        //뎁스 변경시
        $('#depth').change(function () {
            // $('#formCateCd1, #formCateCd2, #formCateCd3').prop('disabled', false);
            $('select[id^="formMappingCateCd"]').prop('disabled', true);

            switch ($(this).val()) {
                case '1' :
                    $('#formCateCd1').prop('disabled', true);
                case '2' :
                    $('#formCateCd2').prop('disabled', true);
            }
        });
    },
    search: function () {
        CommonAjax.basic({
            url: '/item/expCategoryItem/getExpCategoryItem.json',
            data: $('#searchForm').serialize(),
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                expCategoryItemGrid.setData(res);
                $('#expCategoryItemCnt').html($.jUtil.comma(expCategoryItemGrid.gridView.getItemCount()));
                expCategoryItemMng.formReset();
            }
        });
    },
    searchFormReset: function( ) {
        $('#searchCateCd1').val('').change();
        $('#searchType').val('itemNo');
        $('#searchKeyword, #searchStatusYn').val('');
    },
    formReset: function () {
        $('#itemNo, #itemNm').val('');

        expCategoryItemMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'), 'search');
        expCategoryItemMng.setCategorySelectBox(2, '', '', $('#formCateCd2'), 'set');

        $('#formCateCd1, #formCateCd2').prop('disabled', false);

        isEdit = false;
    },
    getGrid: function (selectRowId) {
        var rowDataJson = expCategoryItemGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').val(rowDataJson.itemNo);
        $('#itemNm').val(rowDataJson.itemNm);

        expCategoryItemMng.setCategorySelectBox(1, '', rowDataJson.lcateCd, $('#formCateCd1'), 'set');
        expCategoryItemMng.setCategorySelectBox(2, rowDataJson.lcateCd, rowDataJson.mcateCd, $('#formCateCd2'), 'set');

        // $('#formCateCd1, #formCateCd2').prop('disabled', true);

        isEdit = true;
    },
    setCategoryItem : function() {
        if(!expCategoryItemMng.setValid()){
            return false;
        }

        CommonAjax.basic({
            url : '/item/expCategoryItem/setExpCategoryItem.json',
            data : JSON.stringify($('#expCategoryItemForm').serializeObject()),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                expCategoryItemMng.formReset();
                expCategoryItemMng.search();
            }
        });
    },
    setCategorySelectBox: function (depth, parentCateCd, cateCd, selector, type) {
        var itemDisplayYn = null;

        if(type != 'search') {
            itemDisplayYn = 'Y';
        }

        $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
        if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
            CommonAjax.basic({
                url: '/item/expCategory/getExpCategory.json',
                data: {
                    lCateCd: parentCateCd
                    , itemDisplayYn : itemDisplayYn
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $(selector).css('-webkit-padding-end', '30px');
                    if (depth == 1) {
                        $.each(resData, function (idx, val) {
                            var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                            $(selector).append(
                                '<option value="' + val.cateCd + '" ' + selected + '>'
                                + val.cateNm
                                + '</option>');
                        });
                    } else {
                        $.each(resData[0].rows, function (idx, val) {
                            var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                            $(selector).append(
                                '<option value="' + val.cateCd + '" ' + selected + '>'
                                + val.cateNm
                                + '</option>');
                        });
                    }
                }
            });
        }
    },
    changeCategorySelectBox: function (depth, prefix) {
        var cateCd = $('#' + prefix + depth).val();
        this.setCategorySelectBox(depth + 1, cateCd, '', $('#' + prefix + (depth + 1)), 'set');
        $('#' + prefix + (depth + 1)).change();
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function (){
        expCategoryItemMng.formReset();
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=expCategoryItemMng.applyPopupCallback&isMulti=N&mallType=TD&storeType=EXP', 1084, 650);
    },
    applyPopupCallback : function(resDataArr) {
        $('#itemNo').val(resDataArr[0].itemNo);
        $('#itemNm').val(resDataArr[0].itemNm);
    },
    setValid: function () {
        if($.jUtil.isEmpty($('#itemNo').val())) {
            alert("상품을 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#formCateCd1').val())) {
            alert("대분류를 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#formCateCd2').val())) {
            alert("중분류를 선택해 주세요.");
            return false;
        }
        return true;
    }
};