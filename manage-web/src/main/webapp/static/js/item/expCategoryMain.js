$(document).ready(function () {
    expCategoryMng.init();
    expCategoryGrid.init();
    expCategoryMappingGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

var expCategoryGrid = {
    treeView: new RealGridJS.TreeView("expCategoryGrid"),
    treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
    init: function () {
        expCategoryGrid.initGrid();
        expCategoryGrid.initDataProvider();
        expCategoryGrid.event();
    },
    initGrid: function () {
        expCategoryGrid.treeView.setDataSource(expCategoryGrid.treeDataProviders);

        expCategoryGrid.treeView.setStyles(expCategoryGridBaseInfo.realgrid.styles);
        expCategoryGrid.treeView.setDisplayOptions(expCategoryGridBaseInfo.realgrid.displayOptions);
        expCategoryGrid.treeView.setColumns(expCategoryGridBaseInfo.realgrid.columns);
        expCategoryGrid.treeView.setOptions(expCategoryGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        expCategoryGrid.treeDataProviders.setFields(expCategoryGridBaseInfo.dataProvider.fields);
        expCategoryGrid.treeDataProviders.setOptions(expCategoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        expCategoryGrid.treeView.onDataCellClicked = function (gridView, index) {
            expCategoryMng.getDetail(index.dataRow);
        };
    },
    setData: function (dataList) {
        expCategoryGrid.treeDataProviders.clearRows();
        expCategoryGrid.treeDataProviders.fillJsonData({"rows": dataList},
            {rows: "rows", icon: "lcateCd"});
    }
};

var expCategoryMappingGrid = {
    gridView: new RealGridJS.GridView("expCategoryMappingGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        expCategoryMappingGrid.initGrid();
        expCategoryMappingGrid.initDataProvider();
        expCategoryMappingGrid.event();
    },
    initGrid: function () {
        expCategoryMappingGrid.gridView.setDataSource(expCategoryMappingGrid.dataProvider);

        expCategoryMappingGrid.gridView.setStyles(expRmsCategoryMappingGridBaseInfo.realgrid.styles);
        expCategoryMappingGrid.gridView.setDisplayOptions(expRmsCategoryMappingGridBaseInfo.realgrid.displayOptions);
        expCategoryMappingGrid.gridView.setColumns(expRmsCategoryMappingGridBaseInfo.realgrid.columns);
        expCategoryMappingGrid.gridView.setOptions(expRmsCategoryMappingGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        expCategoryMappingGrid.dataProvider.setFields(expRmsCategoryMappingGridBaseInfo.dataProvider.fields);
        expCategoryMappingGrid.dataProvider.setOptions(expRmsCategoryMappingGridBaseInfo.dataProvider.options);
    },
    event: function () {

    },
    setData: function (dataList) {
        expCategoryMappingGrid.dataProvider.clearRows();
        expCategoryMappingGrid.dataProvider.setRows(dataList);
    }
};

var expCategoryMng = {
    categoryCnt: 4,
    init: function () {
        this.event();
        expCategoryMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));

        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd2'), true);
        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd3'), true);
        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd4'), true);
        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd5'), true);
    },
    event: function () {
        //카테고리 검색 버튼 클릭
        $('#searchBtn').click(function () {
            expCategoryMng.search();
        });

        //카테고리 저장 버튼 클릭
        $('#setCategoryBtn').click(function () {
            expCategoryMng.setCategory();
        });

        //카테고리 리셋 버튼 클릭
        $('#resetBtn').click(function () {
            expCategoryMng.formReset();
        });

        //카테고리 검색 리셋 버튼 클릭
        $('#searchResetBtn').click(function () {
            expCategoryMng.searchFormReset();
        });

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            expCategoryMng.img.addImg();
        });

        $('#cateNm').calcTextLength('keyup', '#cateNmCnt');

        //뎁스 변경시
        $('#depth').change(function () {
            $('#formCateCd1, #formCateCd2, #formCateCd3').prop('disabled', false);

            $('#iconMobileTr, #iconPcTr, #2depthDiv').hide();
            expCategoryMng.img.init();

            switch ($(this).val()) {
                case '1' :
                    $('#formCateCd1').prop('disabled', true);
                    $('#iconMobileTr, #iconPcTr').show();
                    break;
                case '2' :
                    $('#formCateCd2').prop('disabled', true);
                    $('#2depthDiv').show();
                    expCategoryMappingGrid.gridView.resetSize(function(){});
                    break;
            }
        });

        // 아이콘 ON(M) 삭제
        $('#onIconMobileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expCategoryMng.img.deleteImg('onIconMobile');
            }
        });

        // 아이콘 OFF(M) 삭제
        $('#offIconMobileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expCategoryMng.img.deleteImg('offIconMobile');
            }
        });

        // 아이콘 ON(PC) 삭제
        $('#onIconPcDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expCategoryMng.img.deleteImg('onIconPc');
            }
        });

        // 아이콘 OFF(PC) 삭제
        $('#offIconPcDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expCategoryMng.img.deleteImg('offIconPc');
            }
        });

        //사용여부 변경시
        $('#useYn').change(function () {
            if ($(this).val() == 'N') {
                $('#priority').val('0').prop('readonly', true);
            } else {
                $('#priority').val('').prop('readonly', false);
            }
        });

        $("#priority").on({
            keydown: function (e) {
                if (e.which === 32) return false;
            },
            keyup: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            },
            change: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
    },
    search: function (cateArr) {
        var url = $('#searchForm').serialize();
        if (typeof cateArr == 'object') {
            url = $.param(cateArr)
        }

        CommonAjax.basic({
            url: '/item/expCategory/getExpCategory.json?' + url,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                expCategoryGrid.setData(res);
                var searchVal;
                for (var i = expCategoryMng.categoryCnt; i > 0; i--) {
                    if ($('#searchCateCd' + i).val() != "") {
                        var splitVal = $('#searchCateCd1 option:selected').text().split('|');
                        if($.jUtil.isNotEmpty(splitVal[1])) {
                            searchVal = splitVal[1].trim();
                        }
                        break;
                    }
                }

                var ret = expCategoryGrid.treeDataProviders.searchData(
                    {fields: ["cateNm"], value: searchVal, wrap: true});
                if (ret) {
                    var rowId = ret.dataRow;
                    var parents = expCategoryGrid.treeDataProviders.getAncestors(rowId);
                    if (parents) {
                        for (var i = parents.length - 1; i >= 0; i--) {
                            expCategoryGrid.treeView.expand(
                                expCategoryGrid.treeView.getItemIndex(parents[i]));
                        }
                        expCategoryGrid.treeView.setCurrent({
                            itemIndex: expCategoryGrid.treeView.getItemIndex(rowId),
                            fieldIndex: ret.fieldIndex
                        })
                        expCategoryGrid.treeView.expand(expCategoryGrid.treeView.getItemIndex(rowId));
                    }
                }

                $('#expCategoryGridCnt').html($.jUtil.comma(expCategoryGrid.treeView.getItemCount()));
                expCategoryMng.formReset();
            }
        });
    },
    searchFormReset: function () {
        $('#searchCateCd1').val('').change();
        $('#searchUseYn').val('');
    },
    formReset: function (option) {
        $('#cateCd').val('');
        $('#cateNm').val('');
        $('#useYn').val('Y').change();
        $('#priority').val('');
        $('#depth').val('').change();
        $("#depth[id=depth]").prop("disabled", false);
        $('#parentCateCd').val('');
        $('#cateNmCnt').html('0');

        if (option != 'grid') {
            expCategoryMng.setCategorySelectBox(1, '', '', $('#formCateCd1'));
            $('#formCateCd1').val('').change();
            $('#formMappingCateCd1 option:first').prop('selected', true).change();
        }
    },
    getDetail: function (selectRowId) {
        var rowDataJson = expCategoryGrid.treeDataProviders.getJsonRow(selectRowId);
        expCategoryMng.formReset('grid');

        var cateCd = rowDataJson.cateCd;
        var cateNm = rowDataJson.cateNm;
        var useYn = rowDataJson.useYn;
        var priority = rowDataJson.priority;
        var depth = rowDataJson.depth;
        var lcateCd = rowDataJson.lcateCd;
        var mcateCd = rowDataJson.mcateCd;
        var parentCateCd = rowDataJson.parentCateCd;

        $('#cateCd').val(cateCd);
        $('#cateNm').val(cateNm);
        $('#depth').val(depth).change();
        $('#depth  option[value=' + depth + ']').prop('disabled', false);
        $("select[id=depth]").prop("disabled", true);
        $('#cateNmCnt').html(cateNm.length);

        switch (depth) {
            case '1' :
                expCategoryMng.setCategorySelectBox(1, '', '', $('#formCateCd1'));
                expCategoryMng.img.setImg('onIconMobile', {
                    'imgUrl': rowDataJson.onIconMobileImgUrl,
                    'src' : hmpImgUrl + "/provide/view?processKey=ExpCategoryIconMobile&fileId=" + rowDataJson.onIconMobileImgUrl,
                    'changeYn' : 'Y'
                });

                expCategoryMng.img.setImg('offIconMobile', {
                    'imgUrl': rowDataJson.offIconMobileImgUrl,
                    'src' : hmpImgUrl + "/provide/view?processKey=ExpCategoryIconMobile&fileId=" + rowDataJson.offIconMobileImgUrl,
                    'changeYn' : 'Y'
                });

                expCategoryMng.img.setImg('onIconPc', {
                    'imgUrl': rowDataJson.onIconPcImgUrl,
                    'src' : hmpImgUrl + "/provide/view?processKey=ExpCategoryIconPc&fileId=" + rowDataJson.onIconPcImgUrl,
                    'changeYn' : 'Y'
                });

                expCategoryMng.img.setImg('offIconPc', {
                    'imgUrl': rowDataJson.offIconPcImgUrl,
                    'src' : hmpImgUrl + "/provide/view?processKey=ExpCategoryIconPc&fileId=" + rowDataJson.offIconPcImgUrl,
                    'changeYn' : 'Y'
                });
                break;
            case '2' :
                expCategoryMng.setCategorySelectBox(1, '', parentCateCd, $('#formCateCd1'));
                $('#parentCateCd').val(parentCateCd);
                $('#formCateCd1').val(parentCateCd);

                // ajax List 가져오기
                var mcateCd = $('#cateCd').val();

                CommonAjax.basic({
                    url : '/item/expCategory/getExpRmsCategory.json',
                    data : {mcateCd : mcateCd},
                    method : "GET",
                    successMsg : null,
                    contentType : "application/json",
                    callbackFunc : function(res) {
                        expCategoryMappingGrid.setData(res);
                    }
                });
                break;
        }

        $('[id^="formMappingCateCd"]').val(0);
        $('#useYn').val(useYn).change();
        $('#priority').val(priority);
    },
    setCategory: function () {
        if(!expCategoryMng.setValid()){
            return false;
        }

        var parentCateCd    = 0;
        var lcateCd         = '';

        parentCateCd = $('#formCateCd1').val();
        $('#parentCateCd').val(parentCateCd);
        lcateCd = $('#formCateCd1').val();

        if ($('#depth').val() > 1 && parentCateCd == 0) {
            alert('부모카테고리를 선택해주세요.');
            return false;
        }

        var setObject = $('#expCategoryForm').serializeObject(true);
        setObject.depth = $("#depth option:selected").val();

        if($('#depth').val() > 1) {
            var rmsCategoryList = new Array();
            expCategoryMappingGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
                rmsCategoryList[idx] = {
                    division : data[0]
                    , groupNo : data[2]
                    , dept : data[4]
                    , classCd : data[6]
                    , subclass : data[8]
                };
            });

            setObject.rmsCategoryList = rmsCategoryList;
        }

        CommonAjax.basic({
            url : '/item/expCategory/setExpCategory.json',
            data : JSON.stringify(setObject),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                var cateObj = {
                    lcateCd: lcateCd,
                };

                expCategoryMng.formReset();
                // expCategoryMng.setCategorySelectBox(1, '', lcateCd, $('#searchCateCd1'));
                expCategoryMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
                expCategoryMng.search(cateObj);
            }
        });
    },
    setCategorySelectBox: function (depth, parentCateCd, cateCd, selector) {
        $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
        if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
            CommonAjax.basic({
                url: '/item/expCategory/getExpCategory.json',
                data: {
                    depth: depth,
                    parentCateCd: parentCateCd
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $.each(resData, function (idx, val) {
                        var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                        $(selector).append(
                            '<option value="' + val.cateCd + '" ' + selected + '>'
                            + val.cateCd
                            + " | "
                            + val.cateNm
                            + '</option>');
                    });
                    $(selector).css('-webkit-padding-end', '30px');
                    if (depth == 1) {
                        $('#formCateCd1').html($(selector).html());
                    }
                }
            });
        }
    },
    setCategoryMappingSelectBox: function (depth, selector, isInit, rowDataJson) {

        $(selector).html('<option value="0">' + $(selector).data('default') + '</option>');

        if (!isInit) {
            var fieldNm     = '';
            var division    = $('select[name=division]').val();
            var groupNo     = $('select[name=groupNo]').val();
            var dept        = $('select[name=dept]').val();
            var classCd     = $('select[name=classCd]').val();

            switch (depth) {
                case 1 :
                    fieldNm     = 'division';
                    break;
                case 2 :
                    fieldNm     = 'groupNo';
                    division    = rowDataJson ? rowDataJson.division : division;
                    break;
                case 3 :
                    fieldNm     = 'dept';
                    groupNo     = rowDataJson ? rowDataJson.groupNo : groupNo;
                    break;
                case 4 :
                    fieldNm     = 'classCd';
                    dept        = rowDataJson ? rowDataJson.dept    : dept;
                    break;
                case 5 :
                    fieldNm     = 'subclass';
                    if (rowDataJson) {
                        dept        = rowDataJson.dept;
                        classCd     = rowDataJson.classCd;
                    }
                    break;
            }

            CommonAjax.basic({
                url: '/common/category/getCategoryForMappingSelectBox.json',
                data: {
                    depth: depth,
                    division: division,
                    groupNo: groupNo,
                    dept: dept,
                    classCd: classCd,
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $.each(resData, function (idx, val) {
                        var selected = '';

                        if (rowDataJson) {
                            selected = (rowDataJson[fieldNm] == val[fieldNm]) ? 'selected="selected"' : '';
                        }

                        $(selector).append(
                            '<option value="' + val[fieldNm] + '" ' + selected + '>'
                            + val.cateNm
                            + '</option>');
                    });
                    $(selector).css('-webkit-padding-end', '30px');
                }
            });
        }
    },
    changeCategoryMappingSelectBox: function (depth, prefix) {
        var cateCd = $('#' + prefix + depth).val();

        this.setCategoryMappingSelectBox(depth + 1, $('#' + prefix + (depth + 1)), cateCd == '' ? true : false);
        $('#' + prefix + (depth + 1)).change();
    },
    setValid: function () {
        var isEdit = $('#cateCd').val() == '' ? false : true;

        if (isEdit == true) {
            if ($('#cateCd').val() == "") {
                alert("등록 버튼을 클릭하여 카테고리 신규 등록하세요.");
                return false;
            }
        }
        if ($('#cateNm').val() == '') {
            alert('카테고리명을 입력해주세요.');
            $('#cateNm').focus();
            return false;
        }

        if ($.jUtil.isNotAllowInput($('#cateNm').val(), ['SPC_SCH']) || $('#cateNm').val().length > 20) {
            alert('카테고리명 최대 20자까지 가능하며, 입력가능 문자 : 숫자, 영문, 한글, 특수기호'
                + ' 입니다.');
            $('#cateNm').focus();
            return false;
        }

        if ($('#depth').val() == '') {
            alert('Depth를 선택해주세요.');
            $('#depth').focus();
            return false;
        }

        if ($('#useYn').val() == 'Y') {
            if ($('#priority').val() == '') {
                alert('우선순위를 입력해주세요.');
                $('#priority').focus();
                return false;
            }

            var pattern_number = /[0-9]+$/;
            if (!pattern_number.test($('#priority').val())) {
                alert('숫자만 입력 가능하며, 최대 9999까지만 입력하실 수 있습니다.');
                $('#priority').focus();
                return false;
            }
        }
        return true;
    }
};

/**
 * RMS 카테고리 매핑
 */
var expRmsCategoryMappingMng = {

    /**
     * RMS 카테고리 그리드 추가
     */
    addRmsCategory : function() {
        var formMappingCateCd5 = $('#formMappingCateCd5').val();
        if($.jUtil.isEmpty(formMappingCateCd5) || formMappingCateCd5 === "0") {
            alert('5depth까지 모두 선택해 주세요.');
            return;
        }

        var fromMappingCateObject = {
            division : $('#formMappingCateCd1').val()
            , divisionNm : $('#formMappingCateCd1 option:checked').text()
            , groupNo : $('#formMappingCateCd2').val()
            , groupNoNm : $('#formMappingCateCd2 option:checked').text()
            , dept : $('#formMappingCateCd3').val()
            , deptNm : $('#formMappingCateCd3 option:checked').text()
            , classCd : $('#formMappingCateCd4').val()
            , classCdNm : $('#formMappingCateCd4 option:checked').text()
            , subclass : $('#formMappingCateCd5').val()
            , subclassNm : $('#formMappingCateCd5 option:checked').text()
        };

        CommonAjax.basic({
            url: '/item/expCategory/validationRmsCategory.json',
            data: {
                division: fromMappingCateObject.division,
                groupNo: fromMappingCateObject.groupNo,
                dept: fromMappingCateObject.dept,
                classCd: fromMappingCateObject.classCd,
                subclass: fromMappingCateObject.subclass
            },
            method: 'get',
            callbackFunc: function (resData) {
                if(resData.returnCode != '1') {
                    alert('이미 매핑된 카테고리로 추가 매핑이 불가합니다. ( 타 카테고리 )')
                    return;
                } else {
                    // success -> add grid // fail -> alert 후 select box 초기화
                    var duplicated = false;
                    expCategoryMappingGrid.dataProvider.getRows(0, -1).forEach(function(data) {
                        if(data[0] === fromMappingCateObject.division
                            && data[2] === fromMappingCateObject.groupNo
                            && data[4] === fromMappingCateObject.dept
                            && data[6] === fromMappingCateObject.classCd
                            && data[8] === fromMappingCateObject.subclass) {
                            duplicated = true;
                            return;
                        }
                    });

                    if(!duplicated) {
                        expCategoryMappingGrid.dataProvider.addRow(fromMappingCateObject);

                        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
                        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd2'), true);
                        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd3'), true);
                        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd4'), true);
                        expCategoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd5'), true);
                    } else {
                        alert('이미 매핑된 카테고리로 추가 매핑이 불가합니다.');
                        return;
                    }
                }
            }
        });
    },
    /**
     * RMS 카테고리 그리드 삭제 ( 체크 )
     */
    delRmsCategory : function() {
        var check = expCategoryMappingGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제할 카테고리를 선택해주세요.");
            return;
        }
        expCategoryMappingGrid.dataProvider.removeRows(check, false);
    }
};

/**
 * 익스프레스 아이콘 이미지
 */
expCategoryMng.img = {
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        expCategoryMng.img.setImg('onIconMobile', null);
        expCategoryMng.img.setImg('offIconMobile', null);
        expCategoryMng.img.setImg('onIconPc', null);
        expCategoryMng.img.setImg('offIconPc', null);
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'onIconMobile':
                if (params) {
                    $('#onIconMobileImgUrlTag').prop('src', params.src);
                    $('#onIconMobileImgUrl').val(params.imgUrl);
                    $('#onIconMobileImgWidth').val(params.width);
                    $('#onIconMobileImgHeight').val(params.height);
                    $('#onIconMobileImgChangeYn').val(params.changeYn);

                    $('.onIconMobileDisplayView').show();
                    $('.onIconMobileDisplayView').siblings('.onIconMobileDisplayReg').hide();
                } else {
                    $('#onIconMobileImgUrlTag').prop('src', '');
                    $('#onIconMobileImgUrl, #onIconMobileImgWidth, #onIconMobileImgHeight').val('');
                    $('#onIconMobileImgChangeYn').val('N');

                    $('.onIconMobileDisplayView').hide();
                    $('.onIconMobileDisplayView').siblings('.onIconMobileDisplayReg').show();
                }
                break;
            case 'offIconMobile':
                if (params) {
                    $('#offIconMobileImgUrlTag').prop('src', params.src);
                    $('#offIconMobileImgUrl').val(params.imgUrl);
                    $('#offIconMobileImgWidth').val(params.width);
                    $('#offIconMobileImgHeight').val(params.height);
                    $('#offIconMobileImgChangeYn').val(params.changeYn);

                    $('.offIconMobileDisplayView').show();
                    $('.offIconMobileDisplayView').siblings('.offIconMobileDisplayReg').hide();
                } else {
                    $('#offIconMobileImgUrlTag').prop('src', '');
                    $('#offIconMobileImgUrl, #offIconMobileImgWidth, #offIconMobileImgHeight').val('');
                    $('#offIconMobileImgChangeYn').val('N');

                    $('.offIconMobileDisplayView').hide();
                    $('.offIconMobileDisplayView').siblings('.offIconMobileDisplayReg').show();
                }
                break;
            case 'onIconPc':
                if (params) {
                    $('#onIconPcImgUrlTag').prop('src', params.src);
                    $('#onIconPcImgUrl').val(params.imgUrl);
                    $('#onIconPcImgWidth').val(params.width);
                    $('#onIconPcImgHeight').val(params.height);
                    $('#onIconPcImgChangeYn').val(params.changeYn);

                    $('.onIconPcDisplayView').show();
                    $('.onIconPcDisplayView').siblings('.onIconPcDisplayReg').hide();
                } else {
                    $('#onIconPcImgUrlTag').prop('src', '');
                    $('#onIconPcImgUrl, #onIconPcImgWidth, #onIconPcImgHeight').val('');
                    $('#onIconPcImgChangeYn').val('N');

                    $('.onIconPcDisplayView').hide();
                    $('.onIconPcDisplayView').siblings('.onIconPcDisplayReg').show();
                }
                break;
            case 'offIconPc':
                if (params) {
                    $('#offIconPcImgUrlTag').prop('src', params.src);
                    $('#offIconPcImgUrl').val(params.imgUrl);
                    $('#offIconPcImgWidth').val(params.width);
                    $('#offIconPcImgHeight').val(params.height);
                    $('#offIconPcImgChangeYn').val(params.changeYn);

                    $('.offIconPcDisplayView').show();
                    $('.offIconPcDisplayView').siblings('.offIconPcDisplayReg').hide();
                } else {
                    $('#offIconPcImgUrlTag').prop('src', '');
                    $('#offIconPcImgUrl, #offIconPcImgWidth, #offIconPcImgHeight').val('');
                    $('#offIconPcImgChangeYn').val('N');

                    $('.offIconPcDisplayView').hide();
                    $('.offIconPcDisplayView').siblings('.offIconPcDisplayReg').show();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#imageFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#imageFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#imageFile');
        let ext = $('#imageFile').get(0).files[0].name.split('.');
        let params = {
            processKey : 'ExpCategoryIconMobile',
            mode : 'IMG'
        };

        if(expCategoryMng.img.setImgType === 'offIconPc' || expCategoryMng.img.setImgType === 'onIconPc') {
            params.processKey = 'ExpCategoryIconPc';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        expCategoryMng.img.setImg(expCategoryMng.img.setImgType, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        expCategoryMng.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        expCategoryMng.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};