/** 온라인 픽업 주문 현황 Main */
var pickupOrderExtractMng = {
    /**
     * init 이벤트
     */
    init: function () {
        pickupOrderExtractMng.bindEvent();
        pickupOrderExtractMng.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-11d", "0");
        deliveryCore.initSearchDateCustom("schStartShipDt", "schEndShipDt", "-7d", "0");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/pickupOrderExtractHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 330);
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!pickupOrderExtractMng.validSearch()) {
                return false;
            }
            pickupOrderExtractMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(pickupOrderExtractMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(pickupOrderExtractMng.excelDownload);
        // 배송일 조회시작 변경 이벤트 : 주문일 조회시작일 -4일 적용, 단 최대 2주 이내로 적용
        $("#schStartShipDt").on("change", function () {
            var schEndDtVal = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);
            var changeVal = moment($("#schStartShipDt").val(), 'YYYY-MM-DD', true).add(-4, "day");
            if (schEndDtVal.diff(changeVal, "week", true) > 2) {
                $("#schStartDt").val(schEndDtVal.add(-2, "week").format("YYYY-MM-DD"));
            } else {
                $("#schStartDt").val(changeVal.format("YYYY-MM-DD"));
            }
        });
        // 배송일 조회종료 변경 이벤트 : 주문일 조회종료일 동일하게 세팅
        $("#schEndShipDt").on("change", function () {
            var schEndShipDtVal = moment($("#schEndShipDt").val(), 'YYYY-MM-DD', true);
            $("#schEndDt").val(schEndShipDtVal.format("YYYY-MM-DD"));
            // 주문종료일 변경 후, 주문일 조회기간 2주가 넘을 경우 주문시작일도 변경
            var schEndDtVal = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);
            var schStartDtVal = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            if (schEndDtVal.diff(schStartDtVal, "week", true) > 2) {
                $("#schStartDt").val(schEndDtVal.add(-2, "week").format("YYYY-MM-DD"));
            }
        });
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStartDtVal = moment(schStartDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);
        var schStartShipDt = $("#schStartShipDt").val();
        var schEndShipDt = $("#schEndShipDt").val();
        var schStartShipDtVal = moment(schStartShipDt, 'YYYY-MM-DD', true);
        var schEndShipDtVal = moment(schEndShipDt, 'YYYY-MM-DD', true);

        if ($.jUtil.isEmpty(schStartDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty(schStartShipDt) || $.jUtil.isEmpty(schEndShipDt)) {
            alert("배송일을 선택해 주세요.");
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("주문일 조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndShipDtVal.diff(schStartShipDtVal, "day", true) < 0) {
            alert("배송일 조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartShipDt").val(schEndShipDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "week", true) > 2 && schEndShipDtVal.diff(schStartShipDtVal, "week", true) > 2) {
            alert("주문일과 배송일 조회기간은 최대 2주일까지 설정 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            $("#schEndShipDt").val(schStartShipDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "week", true) > 2) {
            alert("주문일 조회기간은 최대 2주일까지 설정 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndShipDtVal.diff(schStartShipDtVal, "week", true) > 2) {
            alert("배송일 조회기간은 최대 2주일까지 설정 가능합니다.");
            $("#schEndShipDt").val(schStartShipDtVal.format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    /**
     * 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
     */
    search: function () {
        var data = $("#pickupOrderExtractSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/extract/getPickupOrderList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                pickupOrderExtractGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#pickupOrderExtractSearchForm").resetForm();
        pickupOrderExtractMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(pickupOrderExtractGrid.gridView.getItemCount() === 0) {
            alert("조회 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "픽업주문내역_" + _date.getFullYear() + ("0"+(_date.getMonth()+1)).slice(-2) + ("0"+_date.getDate()).slice(-2);
        // 추출이력 저장
        pickupOrderExtractMng.saveExtractHistory();
        pickupOrderExtractGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/extract/savePickupOrderExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var pickupOrderExtractGrid = {
    gridView: new RealGridJS.GridView("pickupOrderExtractGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pickupOrderExtractGrid.initGrid();
        pickupOrderExtractGrid.initDataProvider();
    },
    initGrid: function () {
        pickupOrderExtractGrid.gridView.setDataSource(pickupOrderExtractGrid.dataProvider);
        pickupOrderExtractGrid.gridView.setStyles(pickupOrderExtractGridBaseInfo.realgrid.styles);
        pickupOrderExtractGrid.gridView.setDisplayOptions(pickupOrderExtractGridBaseInfo.realgrid.displayOptions);
        pickupOrderExtractGrid.gridView.setColumns(pickupOrderExtractGridBaseInfo.realgrid.columns);
        pickupOrderExtractGrid.gridView.setOptions(pickupOrderExtractGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        pickupOrderExtractGrid.dataProvider.setFields(pickupOrderExtractGridBaseInfo.dataProvider.fields);
        pickupOrderExtractGrid.dataProvider.setOptions(pickupOrderExtractGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        pickupOrderExtractGrid.dataProvider.clearRows();
        pickupOrderExtractGrid.dataProvider.setRows(dataList);
    }
};