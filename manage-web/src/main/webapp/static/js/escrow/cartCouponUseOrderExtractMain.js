/** 장바구니 쿠폰 사용 조회 Main */
var cartCouponUseOrderExtractMng = {
    /**
     * init 이벤트
     */
    init: function () {
        cartCouponUseOrderExtractMng.bindEvent();
        cartCouponUseOrderExtractMng.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 쿠폰번호 입력시 패턴체크
        $("#schAddParamList").on("keyup", function() {
            var schAddParamList = $("#schAddParamList").val();
            if (!$.jUtil.isEmpty(schAddParamList)) {
                let pattern = /[^0-9,]/g;
                if (pattern.test(schAddParamList)) {
                    alert('숫자 또는 ","만 입력하세요.');
                    $("#schAddParamList").val("").focus();
                }
                var couponNoArr = schAddParamList.split(',');
                if (couponNoArr.length > 10) {
                    alert('최대 10개 까지만 입력하세요.');
                    $("#schAddParamList").val("").focus();
                }
            }
        });
        // 주문번호 입력시 validation
        $("#schPurchaseOrderNo").on("keyup", function () {
            var schPurchaseOrderNo = $("#schPurchaseOrderNo").val();
            if (!$.jUtil.isEmpty(schPurchaseOrderNo) && !$.jUtil.isAllowInput(schPurchaseOrderNo, ['NUM'])) {
                alert("주문번호는 숫자만 입력가능합니다.");
                $("#schPurchaseOrderNo").val("").focus();
                return false;
            }
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!cartCouponUseOrderExtractMng.validSearch()) {
                return false;
            }
            cartCouponUseOrderExtractMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(cartCouponUseOrderExtractMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(cartCouponUseOrderExtractMng.excelDownload);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStartDtVal = moment(schStartDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);
        var schAddParamList = $("#schAddParamList").val();
        var schPurchaseOrderNo = $("#schPurchaseOrderNo").val();
        if ($.jUtil.isEmpty(schStartDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("주문일을 선택해 주세요.");
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) > 0) {
            alert("조회기간은 1일만 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            return false;
        } else if ($.jUtil.isEmpty(schAddParamList)) {
            alert("쿠폰번호를 입력해 주세요.");
            return false;
        } else if (!$.jUtil.isEmpty(schPurchaseOrderNo) && !$.jUtil.isAllowInput(schPurchaseOrderNo, ['NUM'])) {
            alert("주문번호는 숫자만 입력해주세요.");
            return false;
        }

        if(!$.jUtil.isEmpty(schAddParamList)) {
            $("#schAddParamList").val(schAddParamList.replace(/(?:\r\n|\r|\n)/g, ''));
            let pattern = /[^0-9,]/g;
            if(pattern.test(schAddParamList)) {
                alert('숫자 또는 ","만 입력하세요.');
                $("#schAddParamList").focus();
                return false;
            }
        }
        return true;
    },
    /**
     * 장바구니쿠폰(농활쿠폰) 사용주문 리스트 조회
     */
    search: function () {
        var data = $("#cartCouponUseOrderExtractSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/extract/getCartCouponUseOrderList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                cartCouponUseOrderExtractGrid.setData(dataList);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#cartCouponUseOrderExtractSearchForm").resetForm();
        cartCouponUseOrderExtractMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(cartCouponUseOrderExtractGrid.gridView.getItemCount() === 0) {
            alert("조회 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "장바구니쿠폰사용내역_" + _date.getFullYear() + ("0"+(_date.getMonth()+1)).slice(-2) + ("0"+_date.getDate()).slice(-2);
        var columnAuth = (excelDownloadAuth === "Y");
        // 추출이력 저장
        cartCouponUseOrderExtractMng.saveExtractHistory();
        cartCouponUseOrderExtractGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            allColumns     : columnAuth,
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/extract/saveCartCouponUseOrderExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var cartCouponUseOrderExtractGrid = {
    gridView: new RealGridJS.GridView("cartCouponUseOrderExtractGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cartCouponUseOrderExtractGrid.initGrid();
        cartCouponUseOrderExtractGrid.initDataProvider();
    },
    initGrid: function () {
        cartCouponUseOrderExtractGrid.gridView.setDataSource(cartCouponUseOrderExtractGrid.dataProvider);
        cartCouponUseOrderExtractGrid.gridView.setStyles(cartCouponUseOrderExtractGridBaseInfo.realgrid.styles);
        cartCouponUseOrderExtractGrid.gridView.setDisplayOptions(cartCouponUseOrderExtractGridBaseInfo.realgrid.displayOptions);
        cartCouponUseOrderExtractGrid.gridView.setColumns(cartCouponUseOrderExtractGridBaseInfo.realgrid.columns);
        cartCouponUseOrderExtractGrid.gridView.setOptions(cartCouponUseOrderExtractGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        cartCouponUseOrderExtractGrid.dataProvider.setFields(cartCouponUseOrderExtractGridBaseInfo.dataProvider.fields);
        cartCouponUseOrderExtractGrid.dataProvider.setOptions(cartCouponUseOrderExtractGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        cartCouponUseOrderExtractGrid.dataProvider.clearRows();
        cartCouponUseOrderExtractGrid.dataProvider.setRows(dataList);
    }
};