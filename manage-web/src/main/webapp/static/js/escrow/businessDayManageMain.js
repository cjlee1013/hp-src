/** Main Script */
var businessDayManageMain = {
    year: null,             // 출력 달의 년(YYYY)
    month: null,            // 출력 달의 월(M/MM)
    firstDayOfWeek: null,   // 출력 달의 1일자 요일
    today: null,            // 오늘 날짜 (YYYY-MM-DD)
    selectTdId: null,       // 선택한 td id

    /**
     * init 이벤트
     */
    init: function() {
        businessDayManageMain.bindingEvent();    // binding event
        businessDayManageMain.initSetDate();     // today date setting
        businessDayManageMain.getCalendarData(); // display calendar data
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // click event binding
        $("#prevMonth").bindClick(businessDayManageMain.clickEventBinding, "prevMonth");    // [이전 달] 버튼
        $("#nextMonth").bindClick(businessDayManageMain.clickEventBinding, "nextMonth");    // [다음 달] 버튼
        $("#prevYear").bindClick(businessDayManageMain.clickEventBinding, "prevYear");      // [이전 년도] 버튼
        $("#nextYear").bindClick(businessDayManageMain.clickEventBinding, "nextYear");      // [다음 년도] 버튼
        $("#saveBtn").bindClick(businessDayManageMain.save);                                       // [저장] 버튼
        $("#resetBtn").bindClick(businessDayManageMain.resetBusinessDay);                          // [초기화] 버튼

        // change event binding
        $("#nonDeliveryYn, #nonSettleYn").bindChange(businessDayManageMain.changeEventBinding,"nonHoliday");
        $("#holidayYn").bindChange(businessDayManageMain.changeEventBinding, "holiday");
    },
    /**
     * click event binding
     */
    clickEventBinding: function(gubun) {
        switch (gubun) {
            case "prevMonth" :  // [이전 달] 버튼
                if(businessDayManageMain.year === 2000 && businessDayManageMain.month === 1){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return false;
                }
                if (businessDayManageMain.month === 1) { // [이전 달] 1월일 경우 전년도 12월로 바꿔준다.
                    businessDayManageMain.getCalendarData(businessDayManageMain.year - 1, 12);
                } else {
                    businessDayManageMain.getCalendarData(businessDayManageMain.year, businessDayManageMain.month - 1);
                }
                break;
            case "nextMonth" :  // [다음 달] 버튼
                if (businessDayManageMain.month === 12) { // [다름 달] 12월일 경우 다음 년도 1월로 바꿔준다.
                    businessDayManageMain.getCalendarData(businessDayManageMain.year + 1, 1);
                } else {
                    businessDayManageMain.getCalendarData(businessDayManageMain.year, businessDayManageMain.month + 1);
                }
                break;
            case "prevYear"  :  // [이전 년도] 버튼
                if(businessDayManageMain.year === 2000){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return;
                }
                businessDayManageMain.getCalendarData(businessDayManageMain.year - 1, businessDayManageMain.month);
                break;
            case "nextYear"  :  // [다음 년도] 버튼
                businessDayManageMain.getCalendarData(businessDayManageMain.year + 1, businessDayManageMain.month);
                break;
        }
    },
    /**
     * change event binding
     */
    changeEventBinding: function(gubun) {
        var isCheckHolidayYn = $("#holidayYn").is(":checked");
        var isCheckNonDeliveryYn = $("#nonDeliveryYn").is(":checked");
        var isCheckNonSettleYn = $("#nonSettleYn").is(":checked");

        switch (gubun) {
            case "nonHoliday" :
                // 배송지연 + 정산불가 => 비영업일 체크
                if (!isCheckHolidayYn && isCheckNonDeliveryYn && isCheckNonSettleYn) {
                    $("#holidayYn").prop("checked", true);
                    // 모두 체크된 상태에서 배송지연 또는 정산불가 체크해제 => 비영업일 체크해제
                } else if (isCheckHolidayYn && (!isCheckNonDeliveryYn || !isCheckNonSettleYn)) {
                    $("#holidayYn").prop("checked", false);
                }
                break;
            case "holiday" :
                // 비영업일 체크 => 배송지연 + 정산불가 체크
                if (isCheckHolidayYn && (!isCheckNonDeliveryYn || !isCheckNonSettleYn)) {
                    $("#nonDeliveryYn").prop("checked", true);
                    $("#nonSettleYn").prop("checked", true);
                    // 모두 체크된 상태에서 비영업일 체크해제 => 배송지연 + 정산불가 체크해제
                } else if (!isCheckHolidayYn && isCheckNonDeliveryYn && isCheckNonSettleYn) {
                    $("#holidayYn").prop("checked", false);
                    $("#nonDeliveryYn").prop("checked", false);
                    $("#nonSettleYn").prop("checked", false);
                }
                break;
        }
    },

    /**
     * 오늘 년,월,일 조회 후 세팅
     */
    initSetDate: function(){
        var date = $.jUtil.getCurrentDate();
        var tmpDate = date.split("-");
        businessDayManageMain.year = tmpDate[0];
        businessDayManageMain.month = tmpDate[1];
        businessDayManageMain.today = date;
    },
    /**
     * 캘린더 초기화
     */
    resetCalendar: function() {
        $("#businessCalendar > tbody").empty();
    },
    /**
     * 화면 출력용 캘린더 데이터 조회
     */
    getCalendarData: function (year, month) {
        // 전달 파라미터가 비어있으면 오늘 날짜 기준으로 조회
        if($.jUtil.isEmpty(year) && $.jUtil.isEmpty(month)) {
            year = businessDayManageMain.year;
            month = businessDayManageMain.month;
        }

        CommonAjax.basic({
            url         : "/escrow/businessDay/getCalendarDate.json?schYear=" + year + "&schMonth="+ month,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                if (res.length == 0) {
                    alert("정상적으로 조회되지 않았습니다.\n잠시 후에 다시 시도해 주시길 바랍니다.");
                    return;
                }
                // 캘린더 초기화
                businessDayManageMain.resetCalendar();
                // 전역 date 변수 setting
                businessDayManageMain.year = res.year;
                businessDayManageMain.month = res.month;
                businessDayManageMain.today = res.today;
                businessDayManageMain.firstDayOfWeek = res.firstDayOfWeek;
                // 캘린더 상단 년도,월 출력
                $("#curYear").html(res.year);
                $("#curMonth").html(res.month);
                // 캘린더 draw
                businessDayManageMain.drawCalendar(res.dayList);

                // 영업일 정보 get & draw
                businessDayManageMain.drawBusinessDayList(businessDayManageMain.getBusinessDayList());

                // 기존 선택된 영업일 정보 음영처리
                if(!$.jUtil.isEmpty(businessDayManageMain.selectTdId)) {
                    $("#" + businessDayManageMain.selectTdId).css('background-color', '#f9f9f9');
                }
            }
        })
    },
    /**
     * 캘린더 출력을 위한 태그 create
     */
    drawCalendar: function(data) {
        var tags = '';
        var weekend = 1;
        var dateCnt = 1;

        // '1일' 그리기 전, 전 달 여백 채울 태그 create
        var firstBlankTags = '';
        var firstBlankDateCnt = 0;
        for (firstBlankDateCnt; firstBlankDateCnt<businessDayManageMain.firstDayOfWeek-1; firstBlankDateCnt++) {
            firstBlankTags += "<td></td>";
        }

        for (var i in data) {
            // N주차 데이터 그리기... start
            if (dateCnt % 7 === 1) {
                tags += "<tr name=" + weekend + "week>";
                // 1주차 일 때, 전 달 여백 채울 태그 draw
                if (weekend === 1) {
                    dateCnt = businessDayManageMain.firstDayOfWeek;
                    tags += firstBlankTags;
                }
            }
            // 일자 draw
            tags += "<td id=" + data[i].date + " onclick='businessDayManageMain.displayBusinessDay($(this));'>";
            // 평일은 검정색, 주말은 빨간색으로 draw
            if(data[i].dayOfWeek === 6 || data[i].dayOfWeek === 7) {
                tags += "<div class='dateArea text-red'>";
            } else {
                tags += "<div class='dateArea'>";
            }
            tags += data[i].day;

            // 일자에 종속된 영업일 필드 draw
            tags += "<span name='businessNo' style='display:none;'/>";
            tags += "<span name='holidayYn' style='display:none;'/>";
            tags += "<span name='nonDeliveryYn' style='display:none;'/>";
            tags += "<span name='nonSettleYn' style='display:none;'/>";

            // 캘린더에 노출시킬 라벨 영역 create
            tags += "<div name='businessDayType' class='businessDayType'>";
            tags += "</div></div></td>";

            // N주차 데이터 그리기... 종료
            if (dateCnt % 7 === 0) {
                tags += "</tr>";
                weekend++;
            }
            dateCnt++;
        }
        // 마지막 일자 그린 후, 캘린더 여백 채울 태그 create
        var lastMarginCnt = (weekend*7) - (firstBlankDateCnt+data.length);
        for (var lastBlankDateCnt=0; lastBlankDateCnt<lastMarginCnt; lastBlankDateCnt++) {
            tags += "<td></td>";
        }

        // 최종 draw
        $("#businessCalendar > tbody").append(tags);
    },
    /**
     * 영업일 타입 리스트 조회
     */
    getBusinessDayList: function() {
        CommonAjax.basic({
            url         : "/escrow/businessDay/getBusinessDayList.json?schYear=" + businessDayManageMain.year + "&schMonth="+ businessDayManageMain.month,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                if (res.length == 0) {
                    return;
                }
                businessDayManageMain.drawBusinessDayList(res);
            }
        });
    },
    /**
     * 영업일 정보 리스트 draw
     */
    drawBusinessDayList: function(data) {
        for(var i in data) {
            businessDayManageMain.drawBusinessDay(data[i]);
        }
    },
    /**
     * 영업일 정보 draw
     */
    drawBusinessDay: function(data) {
        var dataBusinessDay = data.businessDay;
        if ($.jUtil.isEmpty(dataBusinessDay)) {
            return;
        }
        dataBusinessDay = dataBusinessDay.split(" ")[0];

        // 관리번호, 비영업일, 배송지연, 정산불가 hidden text setting
        var dataBusinessNo = data.businessNo;
        var dataHolidayYn = data.holidayYn;
        var dataNonDeliveryYn = data.nonDeliveryYn;
        var dataNonSettleYn = data.nonSettleYn;
        $("#"+dataBusinessDay+" span[name='businessNo']").text(dataBusinessNo);
        $("#"+dataBusinessDay+" span[name='holidayYn']").text(dataHolidayYn);
        $("#"+dataBusinessDay+" span[name='nonDeliveryYn']").text(dataNonDeliveryYn);
        $("#"+dataBusinessDay+" span[name='nonSettleYn']").text(dataNonSettleYn);

        // 비영업일, 배송지연, 정산불가 type draw
        if (dataNonDeliveryYn === 'y' && dataNonSettleYn === 'y') {
            $("#"+dataBusinessDay+" div[name='businessDayType']").html("");
            $('<div>', {
                text: '비영업일',
                css: {
                    backgroundColor: '#f54040',
                    padding: '3px 5px',
                    marginBottom: '3px',
                    color: '#fff'
                }
            }).appendTo($("#"+dataBusinessDay+" div[name='businessDayType']"));
        }
        else if (dataNonDeliveryYn === 'y' && dataNonSettleYn === 'n') {
            $("#"+dataBusinessDay+" div[name='businessDayType']").html("");
            $('<div>', {
                text: '배송지연',
                css: {
                    backgroundColor: '#333',
                    padding: '3px 5px',
                    marginBottom: '3px',
                    color: '#fff'
                }
            }).appendTo($("#"+dataBusinessDay+" div[name='businessDayType']"));
        } else if (dataNonDeliveryYn === 'n' && dataNonSettleYn === 'y') {
            $("#"+dataBusinessDay+" div[name='businessDayType']").html("");
            $('<div>', {
                text: '정산불가',
                css: {
                    backgroundColor: '#999',
                    padding: '3px 5px',
                    marginBottom: '3px',
                    color: '#fff'
                }
            }).appendTo($("#"+dataBusinessDay+" div[name='businessDayType']"));
        } else if (dataNonDeliveryYn === 'n' && dataNonSettleYn === 'n') {
            $("#"+dataBusinessDay+" div[name='businessDayType']").html("");
        }
    },
    /**
     * 캘린더에서 영업일 클릭 시, 상세 정보 하단에 노출
     * @param selectDay
     */
    displayBusinessDay: function ($selectDate) {
        // 하단 영업일 정보 영역 초기화
        businessDayManageMain.resetBusinessDay();

        // 선택된 td의 id 저장
        businessDayManageMain.selectTdId = $selectDate.attr("id");
        $selectDate.css('background-color', '#f9f9f9');

        var nonDeliveryYn = $selectDate.find("span[name='nonDeliveryYn']").text();
        var nonSettleYn = $selectDate.find("span[name='nonSettleYn']").text();

        // 영업일 정보 - 영업일 setting
        $("#businessDay").val($selectDate.attr("id"));
        // 영업일 정보 - 일정유형 setting
        if (nonDeliveryYn === 'y' && nonSettleYn === 'y') {
            $("#holidayYn").prop('checked', true);
            $("#nonDeliveryYn").prop('checked', true);
            $("#nonSettleYn").prop('checked', true);
        } else if (nonDeliveryYn === 'y' && nonSettleYn === 'n') {
            $("#nonDeliveryYn").prop('checked', true);
        } else if (nonDeliveryYn === 'n' && nonSettleYn === 'y') {
            $("#nonSettleYn").prop('checked', true);
        }
    },
    /**
     * 하단 영업일 정보 초기화
     */
    resetBusinessDay: function() {
        $("#businessDayManageForm").resetForm();
        $("#" + businessDayManageMain.selectTdId).removeAttr("style");
    },

    /**
     * 데이터 저장 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 영업일 빈 값 check
        if ($.jUtil.isEmpty($("#businessDay").val())) {
            alert("영업일을 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 데이터 저장 (insert, update 분기)
     */
    save: function() {
        // Form 데이터 유효성 체크
        if (!businessDayManageMain.valid()) {
            return;
        }

        // form data 생성
        var formData = new Object();
        formData.businessDay = $("#businessDay").val();
        formData.businessNo = $("#"+formData.businessDay+" span[name='businessNo']").text();
        formData.holidayYn = $("#holidayYn").is(":checked") === true ? "y" : "n";
        formData.nonDeliveryYn = $("#nonDeliveryYn").is(":checked") === true ? "y" : "n";
        formData.nonSettleYn = $("#nonSettleYn").is(":checked") === true ? "y" : "n";

        var url = '';
        var message = '';
        if ($.jUtil.isEmpty(formData.businessNo)) {
            // insert
            message = '저장';
            url = "/escrow/businessDay/saveBusinessDay.json";
        } else {
            // update
            message = '수정';
            url = "/escrow/businessDay/updateBusinessDay.json";
        }
        CommonAjax.basic({
            url         : url,
            data        : JSON.stringify(formData),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function(res) {
                console.log(res);
                if (!$.jUtil.isEmpty(res.data)) {
                    alert("정상적으로 " + message + "되었습니다.");
                    businessDayManageMain.drawBusinessDay(res.data);
                }
            }
        });
    }
};