/** 주문 배치 관리 Main */
var orderBatchManageMng = {
    /**
     * init 이벤트
     */
    init: function () {
        orderBatchManageMng.bindEvent();
        if (!orderBatchManageMng.checkAuth()) {
            $("#saveBtn, #resetBtn, #deleteBtn").hide();
        }
        orderBatchManageMng.manageHistoryGridShow("N");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!orderBatchManageMng.validSearch()) {
                return false;
            }
            orderBatchManageMng.search();
        });
        // 배치타입 영문 입력만 가능 + 프로시저 입력을 위해 언더바 추가
        $("#batchType").on("keyup", function () {
            var inputString = $("#batchType").val();
            if (!$.jUtil.isAllowInput(inputString, ['ENG', 'SPC_1'])) {
                alert("배치타입은 영문과 언더바('_')만 입력가능합니다.");
                $("#batchType").val("").focus();
                return false;
            }
        });
        // 저장 버튼
        $("#saveBtn").bindClick(orderBatchManageMng.save);
        // 초기화 버튼
        $("#resetBtn").bindClick(orderBatchManageMng.resetInfo);
        // 삭제 버튼
        $("#deleteBtn").bindClick(orderBatchManageMng.remove);
    },
    /**
     * 화면 권한 체크 - 사번기준으로 체크
     * -. 송영준, 이효신, 김다정, 박준수, 홍주영, 최헌효
     * -. 2019101160, 2019101291, 2019101230, 2019101263, 2020103022, 2021103507
     */
    checkAuth: function () {
        return empId === "2019101160" || empId === "2019101291" || empId === "2019101230"
            || empId === "2019101263" || empId === "2020103022" || empId === "2021103507";
    },
    /**
     * 마스터 조회조건 validation
     */
    validSearch: function () {
        // 검색어 자리수 확인
        var schKeyword = $("#schKeyword").val();
        if (schKeyword !== "" && schKeyword.length < 2) {
            alert("2글자 이상 검색하세요.");
            return false;
        }
        return true;
    },
    /**
     * 주문배치관리 마스터 조회
     */
    search: function () {
        var data = $("#orderBatchManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/orderBatch/getOrderBatchManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                orderBatchManageGrid.setData(dataList);
                $("#orderBatchManageSearchCnt").html($.jUtil.comma(orderBatchManageGrid.gridView.getItemCount()));
                // 상세정보 초기화
                orderBatchManageMng.resetInfo();
            }
        });
    },
    /**
     * 그리드에서 클릭한 정보를 하단 상세정보에 그려주기
     */
    drawDetail: function (data) {
        $("#batchNo, #historyBatchNo").val(data.batchNo).prop("disabled", true);
        $("#batchType").val(data.batchType);
        $("#batchNm").val(data.batchNm);
        $("#historyBatchNm").val(data.batchNm).prop("disabled", true);
        $("#batchTerm").val(data.batchTerm);
        $("#description").val(data.description);
        $('input:radio[name=useYn]:input[value="'+ data.useYn +'"]').prop("checked", true);
        $("#metaField1").val(data.metaField1);
        $("#metaField2").val(data.metaField2);
        if (orderBatchManageMng.checkAuth()) {
            orderBatchManageMng.manageHistoryGridShow("Y");
            orderBatchHistoryMng.searchHistory();
        }
    },
    /**
     * 히스토리 그리드 보이기/숨기기 관리
     */
    manageHistoryGridShow: function (flag) {
        if ($.jUtil.isNotEmpty(flag) && flag === "Y") {
            $("#historySearchDiv, #historyGridDiv").show();
            deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        } else {
            $("#historySearchDiv, #historyGridDiv").hide();
        }
    },
    /**
     * 데이터 저장 전 데이터 유효성 체크
     */
    valid: function() {
        // 배치타입 입력여부 체크
        if ($.jUtil.isEmpty($("#batchType").val())) {
            alert("배치타입은 필수값입니다. 다시 확인해주세요.");
            return false;
        // 배치명 입력여부 체크
        } else if ($.jUtil.isEmpty($("#batchNm").val())) {
            alert("배치명은 필수값입니다. 다시 확인해주세요.");
            return false;
        // 배치주기 입력여부 체크
        } else if ($.jUtil.isEmpty($("#batchTerm").val())) {
            alert("배치주기는 필수값입니다. 다시 확인해주세요.");
            return false;
        }
        return true;
    },
    /**
     * 주문배치관리 마스터 저장
     */
    save: function() {
        var formData;
        var saveData;
        var batchNo = $("#batchNo").val();

        // Form 데이터 유효성 체크
        if (!orderBatchManageMng.valid()) {
            return false;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        // 저장을 위한 데이터 생성
        if (!$.jUtil.isEmpty(batchNo)) {
            formData = JSON.parse(_F.getFormDataByJson($("#orderBatchManageInputForm")));
            formData.batchNo = batchNo;
            saveData = JSON.stringify(formData);
        } else {
            saveData = _F.getFormDataByJson($("#orderBatchManageInputForm"));
        }

        CommonAjax.basic({
            url         : '/escrow/orderBatch/saveOrderBatchManage.json',
            data        : saveData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                orderBatchManageMng.search();
            }
        });
    },
    /**
     * 상세정보 초기화
     */
    resetInfo: function () {
        $("#orderBatchManageInputForm input[type='text']").val("");
        $("#orderBatchManageInputForm input[type='radio']:input[value='Y']").prop("checked", true);
        orderBatchManageMng.manageHistoryGridShow("N");
    },
    /**
     * 주문배치관리 마스터 삭제
     */
    remove: function () {
        var formData = {};
        var batchNo = $("#batchNo").val();

        if ($.jUtil.isEmpty(batchNo)) {
            alert("삭제할 대상이 없습니다.");
            return false;
        }
        if(!confirm("정말 삭제 하시겠습니까?")) {
            return false;
        }

        // 삭제할 데이터 생성
        formData.batchNo = batchNo;

        CommonAjax.basic({
            url         : "/escrow/orderBatch/deleteOrderBatchManage.json",
            data        : JSON.stringify(formData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("삭제되었습니다.");
                orderBatchManageMng.search();
            }
        });
    },
};

/** 주문 배치 히스토리 manage */
var orderBatchHistoryMng = {
    /**
     * init 이벤트
     */
    init: function () {
        orderBatchHistoryMng.bindEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 히스토리 그리드 검색 버튼
        $("#searchHistoryBtn").on("click", function () {
            // 조회조건 validation
            if (!orderBatchHistoryMng.validHistorySearch()) {
                return false;
            }
            orderBatchHistoryMng.searchHistory();
        });
    },
    /**
     * 히스토리 조회조건 validation
     */
    validHistorySearch: function () {
        // 배치번호 선택여부 체크
        if ($.jUtil.isEmpty($("#historyBatchNo").val())) {
            alert("배치번호를 선택하세요.");
            return false;
        // 조회시작일 입력여부 체크
        } else if ($.jUtil.isEmpty($("#schStartDt").val())) {
            alert("조회시작일이 입력되지 않았습니다.");
            return false;
        // 조회종료일 입력여부 체크
        } else if ($.jUtil.isEmpty($("#schEndDt").val())) {
            alert("조회종료일이 입력되지 않았습니다.");
            return false;
        }
        return true;
    },
    /**
     * 주문배치 히스토리 조회
     */
    searchHistory: function () {
        var batchNo = $("#historyBatchNo").val();
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var batchType = $("#batchType").val();

        // meta_field2 값이 "sp" 이면 LGW 배치이력 조회
        var metaField2 = $("#metaField2").val();
        var urlString = "";
        if ($.jUtil.isNotEmpty(metaField2) && metaField2.toUpperCase() === "SP") {
            urlString = '/escrow/orderBatch/getLgwBatchHistoryList.json?batchType='+batchType+'&schStartDt='+schStartDt+'&schEndDt='+schEndDt;
        } else {
            urlString = '/escrow/orderBatch/getOrderBatchHistoryList.json?batchNo='+batchNo+'&schStartDt='+schStartDt+'&schEndDt='+schEndDt;
        }

        CommonAjax.basic({
            url         : urlString,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                orderBatchHistoryGrid.setData(dataList);
            }
        });
    },
};

/** Grid Script */
var orderBatchManageGrid = {
    gridView: new RealGridJS.GridView("orderBatchManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        orderBatchManageGrid.initGrid();
        orderBatchManageGrid.initDataProvider();
        orderBatchManageGrid.event();
    },
    initGrid: function () {
        orderBatchManageGrid.gridView.setDataSource(orderBatchManageGrid.dataProvider);
        orderBatchManageGrid.gridView.setStyles(orderBatchManageGridBaseInfo.realgrid.styles);
        orderBatchManageGrid.gridView.setDisplayOptions(orderBatchManageGridBaseInfo.realgrid.displayOptions);
        orderBatchManageGrid.gridView.setColumns(orderBatchManageGridBaseInfo.realgrid.columns);
        orderBatchManageGrid.gridView.setOptions(orderBatchManageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        orderBatchManageGrid.dataProvider.setFields(orderBatchManageGridBaseInfo.dataProvider.fields);
        orderBatchManageGrid.dataProvider.setOptions(orderBatchManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        orderBatchManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = orderBatchManageGrid.dataProvider.getJsonRow(index.dataRow);
            orderBatchManageMng.drawDetail(data);
        };
    },
    setData: function (dataList) {
        orderBatchManageGrid.dataProvider.clearRows();
        orderBatchManageGrid.dataProvider.setRows(dataList);
    }
};

var orderBatchHistoryGrid = {
    gridView: new RealGridJS.GridView("orderBatchHistoryGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        orderBatchHistoryGrid.initGrid();
        orderBatchHistoryGrid.initDataProvider();
        orderBatchHistoryGrid.event();
    },
    initGrid: function () {
        orderBatchHistoryGrid.gridView.setDataSource(orderBatchHistoryGrid.dataProvider);
        orderBatchHistoryGrid.gridView.setStyles(orderBatchHistoryGridBaseInfo.realgrid.styles);
        orderBatchHistoryGrid.gridView.setDisplayOptions(orderBatchHistoryGridBaseInfo.realgrid.displayOptions);
        orderBatchHistoryGrid.gridView.setColumns(orderBatchHistoryGridBaseInfo.realgrid.columns);
        orderBatchHistoryGrid.gridView.setOptions(orderBatchHistoryGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        orderBatchHistoryGrid.dataProvider.setFields(orderBatchHistoryGridBaseInfo.dataProvider.fields);
        orderBatchHistoryGrid.dataProvider.setOptions(orderBatchHistoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        orderBatchHistoryGrid.gridView.onDataCellClicked = function (gridView, index) {
            orderBatchHistoryGrid.dataProvider.getJsonRow(index.dataRow);
        };
    },
    setData: function (dataList) {
        orderBatchHistoryGrid.dataProvider.clearRows();
        orderBatchHistoryGrid.dataProvider.setRows(dataList);
    }
};