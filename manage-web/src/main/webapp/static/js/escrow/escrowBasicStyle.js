$(document).ready(function() {
    // main div 가로길이 속성 재배치 (min-w1000 -> fit-content)
    $("div.escrow-main").css("min-width", "fit-content");

    // select box 가로길이 auto 로 먼저 초기화
    $("select").css("width", "auto");
    // select box 가로길이 auto 값 + 'v' 영역범위 가로길이(약40px) 더해서 보기 좋은 최소한의 길이로 만들기
    $("select").each(function() {
        $(this).css("width", $(this).width()+40+"px");
    });

    // input box 입력풋 가로길이 고정
    $("input[type='text']").css("width", "150px");
    $("input[type='number']").css("width", "100px");
});