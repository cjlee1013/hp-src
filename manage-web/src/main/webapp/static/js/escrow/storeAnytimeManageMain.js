/** 점포 운영 관리 Main */
var storeAnytimeMng = {
    /**
     * init 이벤트
     */
    init: function () {
        storeAnytimeMng.bindEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 검색어 특수기호(%\) 입력 제한, 점포코드 숫자만 입력으로 제한
        $("#schKeyword").notAllowInput("keyup", ['SPC_SCH']).on("keyup", function () {
            var schType = $("#schType").val();
            var schKeyword = $("#schKeyword").val();;
            if (schType === "ID" && !$.jUtil.isEmpty(schKeyword) && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
                alert("점포코드는 숫자만 입력가능합니다.");
                $("#schKeyword").val("").focus();
                return false;
            }
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!storeAnytimeMng.validSearch()) {
                return false;
            }
            storeAnytimeMng.search();
        });
        // 검색 초기화 버튼
        $("#searchResetBtn").bindClick(storeAnytimeMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(storeAnytimeMng.excelDownload);
        // 저장 버튼
        $("#saveBtn").bindClick(storeAnytimeMng.saveAnytimeUseYn);
        // 초기화 버튼
        $("#resetBtn").bindClick(storeAnytimeMng.resetDetail);
        // 애니타임 사용여부 radio 클릭 시 - Shift 체크박스 제어
        $('input:radio[name=storeAnytimeUseYn]').on("change", function () {
            if ($("#storeAnytimeUseY").is(":checked")) {
                $(".shiftUse").prop("disabled", false);
            } else {
                $(".shiftUse").prop("disabled", true);
            }
        });
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStoreType = $("#schStoreType").val();
        if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        // 검색어 체크
        var schKeyword = $("#schKeyword").val();
        var schType = $("#schType").val();
        if (!$.jUtil.isEmpty(schKeyword) && schKeyword.length < 2) {
            alert("검색어는 2글자 이상 입력하세요.");
            return false;
        } else if (schType === "ID" && !$.jUtil.isEmpty(schKeyword) && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
            alert("점포코드는 숫자만 입력가능합니다.");
            return false;
        }
        return true;
    },
    /**
     * 점포 애니타임 정보 조회
     */
    search: function () {
        CommonAjax.basic({
            url         : '/escrow/anytime/getStoreAnytimeInfo.json',
            data        : _F.getFormDataByJson($("#storeAnytimeManageSearchForm")),
            method      : "POST",
            contentType : 'application/json; charset=utf-8',
            callbackFunc: function (dataList) {
                console.log(dataList);
                storeAnytimeManageGrid.setData(dataList);
                $("#storeAnytimeManageSearchCnt").html($.jUtil.comma(storeAnytimeManageGrid.gridView.getItemCount()));
                // 상세정보 초기화
                storeAnytimeMng.resetDetail();
                $("#storeId").val("");
                $("#storeNm").html("");
            }
        });
    },
    /**
     * Shift 애니타임 정보 조회
     */
    searchShift: function (storeId) {
        CommonAjax.basic({
            url         : '/escrow/anytime/getStoreAnytimeShiftInfo.json?storeId=' + storeId,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                $(".shiftUse").prop("checked", false);
                for (var shiftInfo of res) {
                    shiftInfo.shiftAnytimeUseYn === "Y" ? $("#shift_"+shiftInfo.shiftId).prop("checked", true) : $("#shift_"+shiftInfo.shiftId).prop("checked", false);
                }
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#storeAnytimeManageSearchForm").resetForm();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(storeAnytimeManageGrid.gridView.getItemCount() === 0) {
            alert("조회 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "점포운영관리_" + _date.getFullYear() + ("0"+(_date.getMonth()+1)).slice(-2) + ("0"+_date.getDate()).slice(-2);
        storeAnytimeManageGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 애니타임 사용여부 저장
     */
    saveAnytimeUseYn: function() {
        var targetStoreId = $("#storeId").val();
        if ($.jUtil.isEmpty(targetStoreId)) {
            alert("저장할 점포를 선택해주세요.");
            return false;
        }

        // 점포 애니타임 사용여부 저장
        var storeData = new Object();
        storeData.storeId = targetStoreId;
        storeData.storeAnytimeUseYn = $("#storeAnytimeUseY").is(":checked") ? "Y" : "N";
        var sendStoreData = JSON.stringify(storeData);
        CommonAjax.basic({
            url         : '/escrow/anytime/saveStoreAnytimeUseYn.json',
            data        : sendStoreData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res > 0) {
                    // Shift 애니타임 사용여부 저장
                    storeAnytimeMng.saveShiftAnytimeUseYn(targetStoreId);
                } else {
                    alert("잠시 후 다시 시도해주세요.")
                    storeAnytimeMng.search();
                }
            }
        });
    },
    /**
     * Shift 애니타임 사용여부 저장
     */
    saveShiftAnytimeUseYn: function (targetStoreId) {
        var shiftList = new Array();
        $(".shiftUse").each(function() {
            var shiftData = new Object();
            shiftData.storeId = targetStoreId;
            shiftData.shiftId = $(this).val();
            shiftData.shiftAnytimeUseYn = $(this).is(":checked") ? "Y" : "N";
            shiftList.push(shiftData);
        });
        var sendShiftDataList = JSON.stringify(shiftList);
        CommonAjax.basic({
            url         : '/escrow/anytime/saveStoreAnytimeShiftUseYn.json',
            data        : sendShiftDataList,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res > 0) {
                    alert("저장하였습니다.");
                }
                storeAnytimeMng.search();
            }
        });
    },
    /**
     * 상세 그리드 초기화 - 점포/Shift 애니타임 사용여부
     */
    resetDetail: function () {
        $('input:radio[name=storeAnytimeUseYn]').prop("checked", false);
        $(".shiftUse").prop("checked", false).prop("disabled", false);
    },
    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawDetail: function (data) {
        // 클릭한 row 의 storeId, storeNm 세팅
        $("#storeId").val(data.storeId);
        $("#storeNm").html("[" + String(data.storeId).lpad(4,"0") + "] " + data.storeNm);
        // 점포 애니타임 사용여부 세팅
        $('input:radio[name=storeAnytimeUseYn]').prop("checked", false);
        $('input:radio[name=storeAnytimeUseYn]:input[value="'+ data.storeAnytimeUseYn +'"]').prop("checked", true);
        // Shift 애니타임 사용여부 조회 및 세팅
        storeAnytimeMng.searchShift(data.storeId);
        // 애니타임 사용여부 Shift 체크박스 제어
        if ($("#storeAnytimeUseY").is(":checked")) {
            // 점포 애니타임 사용일 경우, 체크박스 disable 해제
            $(".shiftUse").prop("disabled", false);
        } else {
            // 점포 애니타임 미사용 또는 설정안한 경우, 체크박스 disable 처리
            $(".shiftUse").prop("disabled", true);
        }
    },
};

/** Grid Script */
var storeAnytimeManageGrid = {
    gridView: new RealGridJS.GridView("storeAnytimeManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        storeAnytimeManageGrid.initGrid();
        storeAnytimeManageGrid.initDataProvider();
        storeAnytimeManageGrid.event();
    },
    initGrid: function () {
        storeAnytimeManageGrid.gridView.setDataSource(storeAnytimeManageGrid.dataProvider);
        storeAnytimeManageGrid.gridView.setStyles(storeAnytimeManageGridBaseInfo.realgrid.styles);
        storeAnytimeManageGrid.gridView.setDisplayOptions(storeAnytimeManageGridBaseInfo.realgrid.displayOptions);
        storeAnytimeManageGrid.gridView.setColumns(storeAnytimeManageGridBaseInfo.realgrid.columns);
        storeAnytimeManageGrid.gridView.setOptions(storeAnytimeManageGridBaseInfo.realgrid.options);
        setColumnLookupOption(storeAnytimeManageGrid.gridView, "storeAnytimeUseYn", $("#anytimeUseYnLookup"));
    },
    initDataProvider: function () {
        storeAnytimeManageGrid.dataProvider.setFields(storeAnytimeManageGridBaseInfo.dataProvider.fields);
        storeAnytimeManageGrid.dataProvider.setOptions(storeAnytimeManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        storeAnytimeManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = storeAnytimeManageGrid.dataProvider.getJsonRow(index.dataRow);
            storeAnytimeMng.drawDetail(data);
        };
    },
    setData: function (dataList) {
        storeAnytimeManageGrid.dataProvider.clearRows();
        storeAnytimeManageGrid.dataProvider.setRows(dataList);
    }
};