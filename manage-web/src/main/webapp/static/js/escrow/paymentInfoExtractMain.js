/** 지불 수단별 판매현황 Main */
var paymentInfoExtractMng = {
    /**
     * init 이벤트
     */
    init: function () {
        paymentInfoExtractMng.bindEvent();
        paymentInfoExtractMng.initDate();
    },
    /**
     * 조회일자 초기화
     */
    initDate: function () {
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "-1d");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 도움말 버튼
        $("#helpBtn").on("click", function () {
            var popupUrl = "/statistics/popup/paymentInfoExtractHelpPop";
            deliveryCore_popup.openPopup(popupUrl, null, 600, 390);
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!paymentInfoExtractMng.validSearch()) {
                return false;
            }
            paymentInfoExtractMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(paymentInfoExtractMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(paymentInfoExtractMng.excelDownload);
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        // 필수값 체크
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStartDtVal = moment(schStartDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);
        if ($.jUtil.isEmpty(schStartDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("검색일을 선택해 주세요.");
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("조회시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "month", true) > 1) {
            alert("조회기간은 최대 1개월 이내만 가능합니다.");
            $("#schEndDt").val(schStartDtVal.format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    /**
     * 결제수단별/보조결제수단 결제정보 리스트 조회
     */
    search: function () {
        var data = $("#paymentInfoExtractSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/extract/getPaymentInfoListByMethod.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                paymentInfoExtractGrid.setData(dataList);
                $("#paymentInfoExtractSearchCnt").html($.jUtil.comma(paymentInfoExtractGrid.gridView.getItemCount()));
                var headerCells = ["payCnt", "payAmt"];
                paymentInfoExtractGrid.setGridSumData(paymentInfoExtractGrid.gridView, headerCells);
            }
        });
        CommonAjax.basic({
            url         : '/escrow/extract/getAssistancePaymentInfoList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                assistancePaymentInfoExtractGrid.setData(dataList);
                var headerCells = ["payCnt", "payAmt"];
                assistancePaymentInfoExtractGrid.setGridSumData(assistancePaymentInfoExtractGrid.gridView, headerCells);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#paymentInfoExtractSearchForm").resetForm();
        paymentInfoExtractMng.initDate();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(paymentInfoExtractGrid.gridView.getItemCount() === 0) {
            alert("조회 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "지불수단별판매현황_" + _date.getFullYear() + ("0"+(_date.getMonth()+1)).slice(-2) + ("0"+_date.getDate()).slice(-2);
        // 추출이력 저장
        paymentInfoExtractMng.saveExtractHistory();
        RealGridJS.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다.",
            exportGrids    : [
                {grid: paymentInfoExtractGrid.gridView, sheetName: "검색결과"},
                {grid: assistancePaymentInfoExtractGrid.gridView, sheetName: "보조결제수단 상세"}
            ]
        });
    },
    /**
     * 추출이력 저장
     */
    saveExtractHistory: function () {
        CommonAjax.basic({
            url         : '/escrow/extract/savePaymentInfoExtractHistory.json',
            data        : null,
            method      : 'GET'
        });
    },
};

/** Grid Script */
var paymentInfoExtractGrid = {
    gridView: new RealGridJS.GridView("paymentInfoExtractGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentInfoExtractGrid.initGrid();
        paymentInfoExtractGrid.initDataProvider();
    },
    initGrid: function () {
        paymentInfoExtractGrid.gridView.setDataSource(paymentInfoExtractGrid.dataProvider);
        paymentInfoExtractGrid.gridView.setStyles(paymentInfoExtractGridBaseInfo.realgrid.styles);
        paymentInfoExtractGrid.gridView.setDisplayOptions(paymentInfoExtractGridBaseInfo.realgrid.displayOptions);
        paymentInfoExtractGrid.gridView.setColumns(paymentInfoExtractGridBaseInfo.realgrid.columns);
        paymentInfoExtractGrid.gridView.setOptions(paymentInfoExtractGridBaseInfo.realgrid.options);
        var mergeCells = ["parentMethodNm", "cardNm", "cardType"];
        paymentInfoExtractGrid.setGridSumRow(paymentInfoExtractGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        paymentInfoExtractGrid.dataProvider.setFields(paymentInfoExtractGridBaseInfo.dataProvider.fields);
        paymentInfoExtractGrid.dataProvider.setOptions(paymentInfoExtractGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        paymentInfoExtractGrid.dataProvider.clearRows();
        paymentInfoExtractGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "statistical"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: {textAlignment: "far", "numberFormat": "#,##0"},
                    expression: "sum"} });
        }
        var calcText = "";
        if (paymentInfoExtractGrid.gridView.getItemCount() > 0) {
            calcText = Math.floor(Number(_gridView.getSummary("payAmt", "sum") / _gridView.getSummary("payCnt", "sum"))).toLocaleString();
        }
        _gridView.setColumnProperty("userUnitPrice", "header", {summary: {text: calcText, styles: {textAlignment: "far", "numberFormat": "#,##0"}}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
};

var assistancePaymentInfoExtractGrid = {
    gridView: new RealGridJS.GridView("assistancePaymentInfoExtractGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        assistancePaymentInfoExtractGrid.initGrid();
        assistancePaymentInfoExtractGrid.initDataProvider();
    },
    initGrid: function () {
        assistancePaymentInfoExtractGrid.gridView.setDataSource(assistancePaymentInfoExtractGrid.dataProvider);
        assistancePaymentInfoExtractGrid.gridView.setStyles(assistancePaymentInfoExtractGridBaseInfo.realgrid.styles);
        assistancePaymentInfoExtractGrid.gridView.setDisplayOptions(assistancePaymentInfoExtractGridBaseInfo.realgrid.displayOptions);
        assistancePaymentInfoExtractGrid.gridView.setColumns(assistancePaymentInfoExtractGridBaseInfo.realgrid.columns);
        assistancePaymentInfoExtractGrid.gridView.setOptions(assistancePaymentInfoExtractGridBaseInfo.realgrid.options);
        var mergeCells = ["parentMethodNm", "cardNm"];
        assistancePaymentInfoExtractGrid.setGridSumRow(assistancePaymentInfoExtractGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        assistancePaymentInfoExtractGrid.dataProvider.setFields(assistancePaymentInfoExtractGridBaseInfo.dataProvider.fields);
        assistancePaymentInfoExtractGrid.dataProvider.setOptions(assistancePaymentInfoExtractGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        assistancePaymentInfoExtractGrid.dataProvider.clearRows();
        assistancePaymentInfoExtractGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드 합계 Row 설정
     */
    setGridSumRow : function (_gridView, _mergeCells) {
        _gridView.setOptions({summaryMode : "statistical"});
        _gridView.setHeader({summary: {visible: true, styles: {background: "#11ff0000", textAlignment: "far"}, mergeCells: _mergeCells}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
    /**
     * 그리드 합계 데이터 세팅
     */
    setGridSumData : function (_gridView, _headerCells) {
        for (var i = 0; i < _headerCells.length; i++) {
            _gridView.setColumnProperty(_headerCells[i], "header", {
                summary: {
                    styles: {textAlignment: "far", "numberFormat": "#,##0"},
                    expression: "sum"} });
        }
        var calcText = "";
        if (assistancePaymentInfoExtractGrid.gridView.getItemCount() > 0) {
            calcText = Math.floor(Number(_gridView.getSummary("payAmt", "sum") / _gridView.getSummary("payCnt", "sum"))).toLocaleString();
        }
        _gridView.setColumnProperty("userUnitPrice", "header", {summary: {text: calcText, styles: {textAlignment: "far", "numberFormat": "#,##0"}}});
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", {summary: {text: "합계", styles: {textAlignment: "center"}}});
    },
};