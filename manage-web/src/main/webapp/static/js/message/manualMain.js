/**
 * 메세지관리 > 발송관리 > 메시지 수동발송관리
 */

var manualMain = {

    init: function () {
        this.event();
        this.initSearchDate('-1d');
        this.initSendDate();
        this.setMode();
        this.initUserAuth();

        this.changeWorkType();
        this.changeTemplateCode();

        commonEditor.initEditor('#contentBodyH', 100, 'Message');
        commonEditor.setHtml('#contentBodyH', '');
    },

    event: function () {
        // 검색 버튼
        $('#searchBtn').on('click', function () {
            manualMain.search();
        });
        // 수정 버튼
        $('#editManualBtn').on('click', function () {
            manualMain.edit();
        });
        // 등록 버튼
        $('#setManualBtn').on('click', function () {
            manualMain.set();
        });
        // 삭제 버튼
        $('#cancelManualBtn').on('click', function () {
            manualMain.cancel();
        });
        // 엑셀 다운로드 버튼
        $('#excelDownloadBtn').on('click', function () {
            manualMain.excelDownload();
        });
        // 검색 폼 초기화
        $('#searchResetBtn').on('click', function () {
            manualMain.searchFormReset();
        });
        // 등록 폼 초기화
        $('#resetBtn').on('click', function () {
            manualMain.setFormReset();
        });
        // 대상자 삭제 버튼
        $('#sendRowDeleteBtn').on('click', function () {
            manualMain.deleteSendList();
        });
        // 일괄등록 버튼
        $('#excelUploadBtn').on('click', function () {
            let target = 'manualMain.setExcelSendList';
            let url = '/common/popup/excelSendListPop?callback=' + target;
            windowPopupOpen(url, target, 1085, 300);
        });
        // 주문회원 검색 버튼
        $('#userSearchBtn').on('click', function () {
            let target = 'manualMain.setOrderSendListForBuyer';
            let url = '/common/popup/orderSendListPop?buyerCallback=' + target + "&shipCallback=manualMain.setOrderSendListForShip";
            windowPopupOpen(url, target, 1261, 550);
        });

        // 본사/점포 고를 떄
        $('#affiliationType').on('change', function () {
            manualMain.getWorkContent();
            manualMain.changeWorkType();
        });
        // 하이퍼 / 클럽 고를 때
        $('#storeType').on('change', function () {
            manualMain.getWorkContent();
            manualMain.changeWorkType();
        });
        // 채널 타입 고를 때
        $('#workType').on('change', function () {
            manualMain.getWorkContent();
            manualMain.changeWorkType();
        });
        // 템플릿 코드 선택 시
        $('#templateCode').on('change', function () {
            manualMain.changeTemplateCode();
        });
    },

    /**
     * 엑셀추출에서 넘어오는 데이터
     * @param list
     */
    setExcelSendList: function (list) {
        let option = {
            startIndex: 0,
            fields: ['identifier', 'toToken', 'maskingToken']
        };
        let sendList = manualMain.loop(
            list, option, 'identifier', 'toToken', 'toToken');
        sendListGrid.setData(sendList);
        $('#sendListCount').html(
            $.jUtil.comma(sendListGrid.dataProvider.getRowCount()));
    },

    /**
     * 주문검색에서 넘어오는 데이터
     * @param list
     */
    setOrderSendListForBuyer: function (list) {
        manualMain.setOrderSendList(list, 'rawBuyerMobileNo', 'buyerMobileNo');
    },
    setOrderSendListForShip: function (list) {
        manualMain.setOrderSendList(list, 'rawShipMobileNo', 'shipMobileNo');
    },
    setOrderSendList: function (list, rawPhoneNoFieldName, phoneNoFieldName) {
        let option = {
            startIndex: 0,
            fields: ['identifier', 'toToken', 'maskingToken']
        };
        let sendList = manualMain.loop(
            list, option, 'userNo', rawPhoneNoFieldName, phoneNoFieldName);
        sendListGrid.setData(sendList);
        $('#sendListCount').html(
            $.jUtil.comma(sendListGrid.dataProvider.getRowCount()));
    },

    loop: function (list, option, identifier, toToken, maskingToken) {
        let sendList = [];
        list.forEach(function (idx) {
            option.values = [idx[identifier], idx[toToken], idx[maskingToken]];
            if (sendListGrid.dataProvider.searchDataRow(option) < 0) {
                sendList.push({
                    identifier: idx[identifier],
                    toToken: idx[toToken],
                    maskingToken: idx[maskingToken]
                });
            }
        });
        return sendList;
    },

    deleteSendList: function () {
        if (confirm("삭제하시겠습니까?")) {
            let rows = sendListGrid.gridView.getCheckedRows(false);
            if (rows.length === 0) {
                alert('삭제할 항목을 선택해주세요.');
                return;
            }
            for (let idx in rows.reverse()) {
                sendListGrid.delData(rows[idx]);
            }
            $('#sendListCount').html(
                $.jUtil.comma(sendListGrid.dataProvider.getRowCount()));
        }
    },

    /**
     * 템플릿 코드가 바뀌는 경우
     * 1. 템플릿을 쓰는 경우
     *      - 하단 폼에 뿌리고 disable
     * 2. 템플릿을 쓰지 않는 경우
     *      - 알림톡 : 그런 경우 없음
     *      - 문자 : 직접 입력 (enable)
     *      - 메일 : 직접 입력 (enable)
     */
    changeTemplateCode: function () {
        let templateCode = $('#templateCode').val();
        let workType = $('#workType').val();
        let body = $('#contentBodyT');
        let callback = $('#callback');
        let subject = $('#contentSubject');

        if (this.isEmpty(templateCode)) {
            manualMain.clearContentBody();
            manualMain.clearContentSubject();
            manualMain.clearCallback();
            manualMain.enableAllEditor();

            if (workType === 'SMS') {
                // 기본 callback 추가
                callback.val('1577-3355');
            } else {
                callback.val('');
            }
        } else {
            CommonAjax.basic({
                url: '/message/admin/content/getContentTemplateCode.json?'
                    + 'templateCode=' + templateCode,
                data: null,
                contentType: 'application/json',
                method: 'GET',
                async: false,
                successMsg: null,
                callbackFunc: function (res) {
                    manualMain.enableAllEditor();
                    if (res.data.workType === 'MAIL') {
                        commonEditor.setHtml(
                            '#contentBodyH', res.data.contentBody);
                        subject.val(res.data.contentSubject);
                        callback.val(res.data.callback);
                        commonEditor.disableEditor('#contentBodyH');
                        subject.attr('disabled', 'disabled');
                        callback.attr('disabled', 'disabled');
                    } else if (res.data.workType === 'SMS') {
                        body.val(res.data.contentBody);
                        subject.val(res.data.contentSubject);
                        callback.val(res.data.callback);
                    } else {
                        // 알림톡
                        body.val(res.data.contentBody);
                        subject.val(res.data.contentSubject);
                        callback.val(res.data.callback);
                        body.attr('disabled', 'disabled');
                        subject.attr('disabled', 'disabled');
                        callback.attr('disabled', 'disabled');
                    }
                }
            });
        }

        // 알림톡인 경우 모든 경우에 후처리
        if (workType === 'ALIMTALK') {
            subject.attr('disabled', 'disabled');
            body.attr('disabled', 'disabled');
            callback.attr('disabled', 'disabled');
        } else {
            // nothing
        }
    },

    /**
     * 채널이 바뀌는 경우 진행 할 내용 (알림톡, 문자, 메일)
     * 템플릿코드가 null로 돌아가므로 sendDate disabled 기본처리
     * SMS 경우 기본 callback 처리
     */
    changeWorkType: function () {
        manualMain.clearContentSubject();
        manualMain.clearContentBody();
        let callback = $('#callback');
        switch ($("#workType").val()) {
            case 'ALIMTALK':
                manualMain.changeBodyEditor(false);
                manualMain.disableAllEditor();
                callback.val('');
                break;
            case 'SMS':
                manualMain.changeBodyEditor(false);
                manualMain.enableAllEditor();
                callback.val('1577-3355');
                break;
            case 'MAIL':
                manualMain.changeBodyEditor(true);
                manualMain.enableAllEditor();
                callback.val('');
                break;
            default:
        }
    },

    disableSendDate: function () {
        CalendarTime.setCalDate('workDt', '0');
        $('#workDt').attr('disabled', true);
    },

    enableSendDate: function () {
        $('#workDt').attr('disabled', false);
    },

    /**
     * 모든 작성 에디터 활성화
     */
    enableAllEditor: function () {
        // 제목 활성
        $('#contentSubject').removeAttr('disabled');
        // 텍스트 본문 활성
        $('#contentBodyT').removeAttr('disabled');
        // 에디터 본문 활성
        commonEditor.clearEditor('#contentBodyH');
        commonEditor.enableEditor('#contentBodyH');
        // 수신자 정보 활성
        $('#callback').removeAttr('disabled');
    },

    /**
     * 모든 작성 에디터의 내용을 지우고 비활성화
     */
    disableAllEditor: function () {
        // 제목 비활성
        manualMain.clearContentSubject();
        $('#contentSubject').attr('disabled', 'disabled');
        // 텍스트 본문 비활성
        manualMain.clearContentBody();
        $('#contentBodyT').attr('disabled', 'disabled');
        // 에디터 본문 비활성
        commonEditor.clearEditor('#contentBodyH');
        commonEditor.disableEditor('#contentBodyH');
        // 수신자 정보 비활성
        $('#callback').attr('disabled', 'disabled');

        // 공통 바디 지우기
        $('#contentBody').val();
    },

    /**
     * 권한 별 화면에 세팅 할 내용
     */
    initUserAuth: function () {
        let storeType = $('#storeType');
        let affiliationType = $('#affiliationType');
        let regId = $('#regId');

        if (isHeadOffice === false) {
            // 점포인 경우 템플릿을 점포것만 고를 수 있도록 세팅
            storeType.val('HYPER');
            affiliationType.val('STORE');
            storeType.attr('disabled', 'disabled');
            affiliationType.attr('disabled', 'disabled');
            // 점포 사용자의 경우 등록자를 자기로만 세팅
            regId.val(empId);
            regId.attr('readonly', true);
        }
        manualMain.getWorkContent();
    },

    /**
     * Froala, TextArea 전환
     */
    changeBodyEditor: function (useHtml) {
        if (useHtml) {
            $('#contentBodyT').hide();
            $('#contentBodyH').show();
        } else {
            $('#contentBodyT').show();
            $('#contentBodyH').hide();
        }
    },

    /**
     * 제목 지우기
     */
    clearContentSubject: function () {
        $('#contentSubject').val('');
    },

    clearCallback: function () {
        $('#callback').val('');
    },

    /**
     * 모든 body clear
     */
    clearContentBody: function () {
        $('#contentBody').val('');
        $('#contentBodyT').val('');
        commonEditor.clearEditor('#contentBodyH');
    },

    /**
     * 채널 별 컨텐츠 리스트 조회 및 세팅
     * 파라미터에 값이 있는 경우 값을 가져오고 세팅,
     * ETC1 에 수동발송 이라고 되어있는 대상만 가져옴
     */
    getWorkContent: function (selected) {
        let wt = $('#workType option:selected').val();
        let at = $('#affiliationType option:selected').val();
        let st = $('#storeType option:selected').val();
        let param = {
            workType: wt,
            affiliationType: at,
            storeType: st,
            etc1: '수동발송'
        };

        CommonAjax.basic({
            url: '/message/admin/content/getWorkContents.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                let templateCode = $('#templateCode');
                templateCode.empty();
                templateCode.append(
                    '<option value="">템플릿 선택안함 (자유입력)</option>');
                let d = res.data;
                if (d.length === 0) {
                    return;
                }
                $.each(d, function (idx, c) {
                    $('#templateCode').append(
                        '<option value="' + c.templateCode + '">' + c.name
                        + '</option>');
                })

                if (selected) {
                    templateCode.val(selected).prop("selected", true);
                    $('#templateCode option')
                    .not(":selected")
                    .attr("disabled", "disabled");
                }
            }
        });
    },

    /**
     * 값이 비었는지 검사
     * @param value is empty return true
     * @returns {boolean}
     */
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },

    /**
     * 날짜폼 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        manualMain.setDate = Calendar.datePickerRange('schStartDate',
            'schEndDate');
        manualMain.setDate.setEndDate(0);
        manualMain.setDate.setStartDate(flag);
        $("#schEndDate").datepicker("option", "minDate",
            $("#schStartDate").val());
    },

    initSendDate: function () {
        manualMain.setDate = CalendarTime.datePicker('workDt',
            {timeFormat: 'HH:mm:ss'});
        $('#workDt').val(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    },

    /**
     * 검색
     */
    search: function () {
        let s = $('#schStartDate').val();
        let e = $('#schEndDate').val();
        if (moment(e).diff(moment(s), 'month') >= 6) {
            alert('최대 6개월까지만 조회 가능합니다.');
            return;
        }
        CommonAjax.basic({
            url: '/message/admin/manual/history/getWorks.json',
            data: _F.getFormDataByJson($('#manualSearchForm')),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                manualListGrid.setData(res.data);
                $('#manualTotalCount').html(
                    $.jUtil.comma(manualListGrid.dataProvider.getRowCount()));
            }
        });
    },

    validation: function () {
        let workName = $('#coupledWorkName').val();
        let desc = $('#description').val();
        let workDt = $('#workDt').val();
        if (manualMain.isEmpty(workName)) {
            alert("작업 명이 없습니다.");
            return false;
        }
        if (manualMain.isEmpty(desc)) {
            alert("설명이 없습니다.");
            return false;
        }
        if (manualMain.isEmpty(workDt)) {
            alert("예약 시간이 올바르지 않습니다.");
            return false;
        }

        if (manualMain.isEmpty($('#templateCode').val())) {
            let callback = $('#callback').val();
            let contentBody = $('#contentBody').val();
            if (manualMain.isEmpty(callback)) {
                alert("발신자정보가 없습니다.");
                return false;
            }
            if (manualMain.isEmpty(contentBody)) {
                alert("본문이 없습니다.");
                return false;
            }
        }
        return true;
    },
    /**
     * 수동 발송 등록
     */
    set: function () {
        let workType = $('#workType').val();
        let contentBody = $('#contentBody');

        if (workType === 'MAIL') {
            contentBody.val(commonEditor.getHtmlData('#contentBodyH'));
        } else {
            contentBody.val($('#contentBodyT').val());
        }

        if (!manualMain.validation()) {
            return;
        }

        let sendList = sendListGrid.gridView.getCheckedRows(false);
        let sendDt = $('#workDt').val();
        let nowDt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

        if (sendList.length === 0) {
            alert("수신자 목록에서 발송 대상자를 선택해주세요.");
            return;
        } else if(sendList.length <= 100000 && sendList.length > 100) {
            if(moment(sendDt).diff(nowDt, "minute", true) < 10) {
                alert("100건 이상 발송 시 10분 이후 예약만 가능합니다.");
                return;
            }
            alert("수신자 대량 업로드 시 몇 분 이상 소요 될 수 있습니다.");
        } else if(sendList.length > 100000) {
            alert("10만건 이상은 발송 할 수 없습니다.");
            return;
        }

        let data = manualMain.toJson($('#manualSetForm'));
        data.bodyArgument = sendListGrid.toList(sendList);
        data.workDt = moment(sendDt).format('YYYY-MM-DDTHH:mm:ss');

        CommonAjax.basic({
            url: '/message/admin/manual/setWorks.json',
            data: JSON.stringify(data),
            method: 'POST',
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                if (res.data.returnCode === 'ERROR') {
                    alert("등록에 실패했습니다. 사유 : " + res.data.message);
                } else {
                    alert(res.data.sendCount + "건 발송 등록 되었습니다.");
                    manualMain.search();
                    manualMain.setFormReset();
                }
            }
        });
    },

    /**
     * 수동 발송 삭제
     */
    cancel: function () {
        if (confirm("취소 하시겠습니까?")) {
            CommonAjax.basic({
                url: '/message/admin/manual/cancelWorks.json',
                data: _F.getFormDataByJson($('#manualSetForm')),
                method: "POST",
                contentType: 'application/json',
                successMsg: null,
                callbackFunc: function (res) {
                    if (res.data.returnCode === "COMPLETED") {
                        alert("취소 되었습니다.");
                    } else {
                        alert("취소 요청에 실패했습니다. 사유 : " + res.data.message);
                    }
                    manualMain.search();
                    manualMain.setFormReset();
                }
            });
        }
    },

    toJson: function (f) {
        var unindexed_array = f.serializeArray();
        var param = {};

        $(unindexed_array).each(function (i, v) {
            if (param[v.name]) {
                if (param[v.name].length < 2) {
                    var tmpArray = [];
                    tmpArray.push(param[v.name]);
                    tmpArray.push(v.value);
                    param[v.name] = tmpArray;
                } else {
                    param[v.name].push(v.value);
                }
            } else {
                param[v.name] = v.value;
            }
        });
        return param;
    },

    /**
     * 검색 폼 초기화
     */
    searchFormReset: function () {
        $("#manualSearchForm").resetForm();
        manualMain.initSearchDate('-1d');
        if (isHeadOffice === false) {
            $('#regId').val(empId);
        }
    },

    /**
     * 등록 폼 초기화
     */
    setFormReset: function () {
        // form reset
        $("#manualSetForm").resetForm();

        // hidden value = null
        $('#manualSendSeq').val('');
        $('#manualSendWorkSeq').val('');
        $('#coupledWorkSeq').val('');

        // html value = null
        /*
        $('#regId').html('');
        $('#chgId').html('');
        $('#regDt').html('');
        $('#chgDt').html('');
        */
        $('#workStatus').html('');
        //$('#templateCode').attr('readOnly', false);

        // selectbox reset
        $('#workType option').removeAttr("disabled");
        $('#templateCode option').removeAttr("disabled");

        // datepicker reset
        //$('#workDt').attr('readOnly', false);

        manualMain.clearContentSubject();
        manualMain.clearContentBody();
        manualMain.getWorkContent();
        manualMain.changeWorkType();

        CalendarTime.setCalDate('workDt', '1d');

        sendListGrid.dataProvider.clearRows();

        // button mode reset
        this.setMode();
    },

    /**
     * Grid 내용 엑셀 다운로드
     */
    excelDownload: function () {
        manualListGrid.excelDownload();
    },

    // 상태 값에 따라 변경 할 id 값
    decideStatus: function (workStatus, id) {
        if (workStatus === 'READY' || workStatus === '발송 요청') {
            $('#' + id).attr('readOnly', false);
        } else {
            $('#' + id).attr('readOnly', true);
        }
    },

    disableStatus: function (workStatus, id) {
        let selector = '#' + id;
        $(selector).val('');
        if (workStatus === 'READY' || workStatus === '발송 요청') {
            $(selector).show();
        } else {
            $(selector).hide();
        }
    },

    // 등록 hide
    // 수정 show
    // 발송취소 show
    editMode: function () {
        $('#setManualBtn').hide();
        //$('#editManualBtn').show();
        $('#cancelManualBtn').show();
        sendListGrid.clearRows();
        $('#sendListDiv').hide();
    },

    // 등록 show
    // 수정 hide
    // 발송취소 hide
    setMode: function () {
        $('#setManualBtn').show();
        //$('#editManualBtn').hide();
        $('#cancelManualBtn').hide();
        sendListGrid.clearRows();
        $('#sendListDiv').show();
    },

    setDetailData: function (data) {
        let workType = $('#workType');
        let workTypeOption = $('#workType option');
        let coupledWorkName = $('#coupledWorkName');
        let subject = $('#contentSubject');
        let body;
        let callback = $('#callback');
        let description = $('#description');
        let workStatus = $('#workStatus');
        let workDt = $('#workDt');

        //hidden start
        $('#manualSendSeq').val(data.manualSendSeq);
        $('#manualSendWorkSeq').val(data.manualSendWorkSeq);
        $('#coupledWorkSeq').val(data.coupledWorkSeq);
        //hidden end

        //workType control start
        workTypeOption.removeAttr("disabled");
        workType.val(data.workType);
        workTypeOption.not(":selected").attr("disabled", "disabled");

        manualMain.changeWorkType();
        if (workType.val() === 'MAIL') {
            commonEditor.setHtml('#contentBodyH', data.body);
            commonEditor.disableEditor('#contentBodyH');
        } else {
            body = $('#contentBodyT');
            body.val(data.body);
            body.attr('disabled', 'disabled');
        }
        //workType control end

        coupledWorkName.val(data.coupledWorkName);
        description.val(data.description);
        workStatus.html(workStatusMap[data.workStatus]);
        workDt.val(moment(data.workDt).format('YYYY-MM-DD HH:mm:ss'));
        callback.val(data.callback);
        subject.val(data.subject);

        subject.attr('disabled', 'disabled');
        callback.attr('disabled', 'disabled');

        // editMode 메소드에서 show 처리 된 버튼을 ready 가 아닌경우 hide
        if (data.workStatus !== 'READY') {
            $('#cancelManualBtn').hide();
        }

        manualMain.getWorkContent(null);
    },

    /**
     * 그리드 선택 이벤트
     * @param selectRowId
     */
    gridRowSelect: function (selectRowId) {
        this.editMode();
        let rowDataJson = manualListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: '/message/admin/manual/history/getDetails.json',
            contentType: 'application/json',
            data: JSON.stringify(rowDataJson),
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                manualMain.setDetailData(res.data);
            }
        });
    }
}

const sendListGrid = {
    gridView: new RealGridJS.GridView("sendListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        sendListGrid.initGrid();
        sendListGrid.initDataProvider();
        sendListGrid.event();
        sendListGridBaseInfo.realgrid.options.checkBar.visible = true;
    },
    initGrid: function () {
        sendListGrid.gridView.setDataSource(sendListGrid.dataProvider);

        sendListGrid.gridView.setStyles(
            sendListGridBaseInfo.realgrid.styles);
        sendListGrid.gridView.setDisplayOptions(
            sendListGridBaseInfo.realgrid.displayOptions);
        sendListGrid.gridView.setColumns(
            sendListGridBaseInfo.realgrid.columns);
        sendListGrid.gridView.setOptions(
            sendListGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        sendListGrid.dataProvider.setFields(
            sendListGridBaseInfo.dataProvider.fields);
        sendListGrid.dataProvider.setOptions(
            sendListGridBaseInfo.dataProvider.options);
    },
    clearRows: function() {
        sendListGrid.dataProvider.clearRows();
    },
    setData: function (list) {
        this.dataProvider.addRows(list);
        // 추가 이벤트 발생 시 전체 체크 처리
        this.gridView.checkAll();
        // 신규선택자만 체크 처리
        /*
        let rowCount = this.dataProvider.getRowCount();
        for(let i = rowCount - 1; i >= rowCount - list.length; i --) {
            this.gridView.checkItem(i, true);
        }
        */
    },
    delData: function (row) {
        this.dataProvider.removeRow(row);
    },
    toList: function (rows) {
        let list = [];
        rows.forEach(function (r) {
            let row = this.dataProvider.getJsonRow(r);
            list.push({
                identifier: row.identifier,
                toToken: row.toToken,
                mappingData: null
            });
        }, this);
        return list;
    },
    event: function () {
    }
};

const manualListGrid = {
    gridView: new RealGridJS.GridView("manualListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        manualListGrid.initGrid();
        manualListGrid.initDataProvider();
        manualListGrid.event();
    },
    initGrid: function () {
        manualListGrid.gridView.setDataSource(manualListGrid.dataProvider);

        manualListGrid.gridView.setStyles(
            manualWorkGridBaseInfo.realgrid.styles);
        manualListGrid.gridView.setDisplayOptions(
            manualWorkGridBaseInfo.realgrid.displayOptions);
        manualListGrid.gridView.setColumns(
            manualWorkGridBaseInfo.realgrid.columns);
        manualListGrid.gridView.setOptions(
            manualWorkGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        manualListGrid.dataProvider.setFields(
            manualWorkGridBaseInfo.dataProvider.fields);
        manualListGrid.dataProvider.setOptions(
            manualWorkGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        manualListGrid.gridView.onDataCellClicked = function (gridView, index) {
            manualMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        manualListGrid.dataProvider.clearRows();
        manualListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (manualListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "수동발송이력_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        manualListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

$(document).ready(function () {
    manualMain.init();
    manualListGrid.init();
    sendListGrid.init();
    CommonAjaxBlockUI.global();
});
