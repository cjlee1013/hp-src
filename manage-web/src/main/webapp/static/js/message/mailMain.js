/**
 * 메세지관리 > 발송관리 > 메일발송조회
 */
const mailMain = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.initSearchDate('0d');
        commonEditor.initEditor('#bodyTemplate', 150, 'Message');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        mailMain.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        mailMain.setDate.setEndDate(0);
        mailMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#chkIncludeSystemSending').on('click', function () {
            $('#schIncludeSystemSending').val($(this).is(":checked") ? 1 : 0);
        });
        $('#searchResetBtn').on('click', function() { mailMain.searchFormReset(); });
        $('#searchBtn').on('click', function() { mailMain.search(); });
        $('#excelDownloadBtn').on('click', function() { mailMain.excelDownload(); });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        mailListGrid.excelDownload();
    },
    /**
     * 알림톡 발송 이력 검색
     */
    search : function() {
        if (mailMain.checkDate() === false) {
            return;
        }

        CommonAjax.basic({url:'/message/admin/mail/history/getWorks.json', data:$('#mailSearchForm').serialize(), method:"POST", successMsg:null, callbackFunc:function(res) {
                mailListGrid.setData(res.data);
                $('#mailTotalCount').html($.jUtil.comma(mailListGrid.dataProvider.getRowCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $("#mailSearchForm").resetForm();
        $('#schIncludeSystemSending').val(0);

        mailMain.initSearchDate('0d');
    },
    gridRowSelect : function(selectRowId) {
        var rowDataJson = mailListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({url:'/message/admin/mail/history/getDetail.json?mailWorkSeq=' + rowDataJson.mailWorkSeq, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                var workDetail = res.data;
                mailMain.setDetailData(workDetail);
            }});
    },
    setDetailData : function (data) {
        commonEditor.enableEditor('#bodyTemplate');
        $('#workName').html(data.mailWorkName);
        $('#description').html(data.description);
        $('#subject').html(data.subject);
        commonEditor.setHtml($('#bodyTemplate'), data.body);
        $('#regDt').html(data.regDt.replace("T", " "));
        $('#regName').html(data.regId);
        $('#chgDt').html(data.chgDt.replace("T", " "));
        $('#chgName').html(data.chgId);
        sendListGrid.setData(data.sendList);
        commonEditor.disableEditor('#bodyTemplate');


        var detailTotalCount = sendListGrid.dataProvider.getRowCount();
        var sendCount = 0;
        var failCount = 0;

        for (var i = 0; i < detailTotalCount; i++) {
            var row = sendListGrid.dataProvider.getJsonRow(i);
            if (row.sendResultCd == "COMPLETED") sendCount++;
            else if (row.sendResultCd == "ERROR") failCount++;
        }

        $('#setCount').html($.jUtil.comma(detailTotalCount));
        $('#sendCount').html($.jUtil.comma(sendCount));
        $('#failCount').html($.jUtil.comma(failCount));
    },
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },
    isNotEmpty: function (value) {
        return !mailMain.isEmpty(value);
    },
    checkDate: function () {
        let startDt = $('#schStartDate');
        let endDt = $('#schEndDate');
        let diff = moment(endDt.val()).diff(startDt.val(), "day", true) + 1;

        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() ||
            !moment(endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("날짜 형식이 맞지 않습니다.");
            return false;
        }

        let isKeyword = mailMain.isNotEmpty($('#schKeyword').val());
        let workStatus = mailMain.isNotEmpty($('#schWorkStatus').val());

        if (isKeyword || workStatus) {
            if (diff > 7) {
                alert("7일 이내만 검색 가능합니다.");
                return false;
            }
        } else {
            if (diff > 3) {
                alert("상세조건을 설정 해 주세요.");
                return false;
            }
        }
        return true;
    }
};

const mailListGrid = {
    gridView : new RealGridJS.GridView("mailListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mailListGrid.initGrid();
        mailListGrid.initDataProvider();
        mailListGrid.event();
    },
    initGrid : function() {
        mailListGrid.gridView.setDataSource(mailListGrid.dataProvider);

        mailListGrid.gridView.setStyles(mailWorkGridBaseInfo.realgrid.styles);
        mailListGrid.gridView.setDisplayOptions(mailWorkGridBaseInfo.realgrid.displayOptions);
        mailListGrid.gridView.setColumns(mailWorkGridBaseInfo.realgrid.columns);
        mailListGrid.gridView.setOptions(mailWorkGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        mailListGrid.dataProvider.setFields(mailWorkGridBaseInfo.dataProvider.fields);
        mailListGrid.dataProvider.setOptions(mailWorkGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        mailListGrid.gridView.onDataCellClicked = function(gridView, index) {
            mailMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        mailListGrid.dataProvider.clearRows();
        mailListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(mailListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "메일발송이력_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mailListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

const sendListGrid = {
    gridView : new RealGridJS.GridView("sendListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        sendListGrid.initGrid();
        sendListGrid.initDataProvider();
    },
    initGrid : function() {
        sendListGrid.gridView.setDataSource(sendListGrid.dataProvider);

        sendListGrid.gridView.setStyles(mailSendListGridBaseInfo.realgrid.styles);
        sendListGrid.gridView.setDisplayOptions(mailSendListGridBaseInfo.realgrid.displayOptions);
        sendListGrid.gridView.setColumns(mailSendListGridBaseInfo.realgrid.columns);
        sendListGrid.gridView.setOptions(mailSendListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        sendListGrid.dataProvider.setFields(mailSendListGridBaseInfo.dataProvider.fields);
        sendListGrid.dataProvider.setOptions(mailSendListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        sendListGrid.dataProvider.clearRows();
        sendListGrid.dataProvider.setRows(dataList);
    }
};

const mail = {
};

$(document).ready(function() {
    mailMain.init();
    mailListGrid.init();
    sendListGrid.init();
    CommonAjaxBlockUI.global();
});
