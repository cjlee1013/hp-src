/**
 * 메세지관리 > 컨텐츠관리 > 템플릿관리
 */
const contentMain = {
    init: function () {
        this.event();
        commonEditor.initEditor('#contentBodyH', 150, 'Message');
        commonEditor.setHtml('#contentBodyH', '');
        this.initTypeAction();
        this.disableBlockYn();
    },

    initTypeAction: function () {
        contentMain.setMode();
        contentMain.changeWorkType();
    },

    event: function () {
        $('#searchResetBtn').on('click', function () {
            contentMain.searchFormReset();
        });
        $('#searchBtn').on('click', function () {
            contentMain.search();
        });
        $('#excelDownloadBtn').on('click', function () {
            contentListGrid.excelDownload();
        });
        $('#setContentBtn').on('click', function () {
            contentMain.set();
        });
        $('#editContentBtn').on('click', function () {
            contentMain.edit();
        });
        $('#resetBtn').on('click', function () {
            contentMain.setFormReset();
        });
        $('#workTypeF').on('change', function () {
            contentMain.changeWorkType();
        });
        $('#deleteAttachmentBtn').on('click', function () {
            contentMain.deleteAttachment();
        });
        $('#blockYn').on('change', function () {
            if ($('#blockYn').val() === 'Y') {
                contentMain.enableBlockYn();
            } else {
                contentMain.disableBlockYn();
            }
        });
    },

    searchFormReset: function () {
        $('#contentSearchForm').resetForm();
    },

    enableBlockYn: function () {
        $('#blockStartTime').attr('disabled', false);
        $('#blockEndTime').attr('disabled', false);
    },

    disableBlockYn: function () {
        $('#blockStartTime').attr('disabled', true);
        $('#blockEndTime').attr('disabled', true);
    },

    /**
     * 하단 폼 초기화 시 진행할 내용
     * 폼내용 전체 리셋 및 hidden 타입 초기화
     */
    setFormReset: function () {
        // form 영역
        $('#contentSetForm').resetForm();
        $('#buttonForm').resetForm();
        $('#mmsAttachForm').resetForm();
        $('#fileId').html('');
        // hidden 영역
        $('#contentSeq').val('');
        $('#contentBody').val('');
        // 첨부파일 영역
        $('#attachment').val('');
        // WorkType이 초기화 되면서 아래 채널별 form을 리셋
        contentMain.changeWorkType();
        // setMode 변경
        contentMain.setMode();
        // Froala 초기화
        commonEditor.clearEditor($('#contentBodyH'));
    },

    search: function () {
        let keywordLength = $('#schKeyword').val().length;
        if (keywordLength > 0 && keywordLength < 2) {
            alert("검색어는 2자 이상 입력해 주세요.");
            return false;
        }

        let data = _F.getFormDataByJson($('#contentSearchForm'));

        CommonAjax.basic({
            url: '/message/admin/content/searchContents.json',
            data: data,
            contentType: 'application/json',
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                contentListGrid.setData(res.data);
                $('#contentTotalCount').html(
                    $.jUtil.comma(contentListGrid.dataProvider.getRowCount()));
            }
        });
    },

    edit: function () {
        // 수정 시 시간 값은 api에서 정하므로 공백 처리
        $('#regDt').val('');
        $('#chgDt').val('');
        // HTML과 TEXT 여부에 따라 실제 contentBody에 넣을 값 선택
        contentMain.setBodyValue();
        // 폼 필수 값 유효성 체크
        if (!contentMain.checkFormValidation('edit')) {
            return;
        }
        // 알림톡의 경우 버튼 데이터가 있는 경우 serialize
        contentMain.serializeButton();
        // SMS의 경우 첨부파일이 있다면 업로드
        contentMain.setAttachment();

        let data = _F.getFormDataByJson($('#contentSetForm'));
        CommonAjax.basic({
            url: '/message/admin/content/editContent.json',
            data: data,
            contentType: 'application/json',
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnStatus === 200 && res.data > 0) {
                    alert("수정 되었습니다.");
                } else {
                    alert("수정에 실패했습니다.");
                }
                // 리스트 새로 호출
                // 검색조건 유지 후 검색 진행
                //contentMain.refreshGrid();
                contentMain.search();
                // 하단 폼 전부 reset
                contentMain.setFormReset();
            },
            errorCallbackFunc: function (res) {
                let data = res.responseJSON.data;
                if (data !== null &&
                    data.includes('SQLIntegrityConstraintViolationException')) {
                    alert('동일한 템플릿 코드가 있습니다. 코드 확인 후 등록바랍니다.');
                } else {
                    alert('에러가 발생하여 등록에 실패했습니다.');
                }
            }
        });
    },

    set: function () {
        // HTML과 TEXT 여부에 따라 실제 contentBody에 넣을 값 선택
        contentMain.setBodyValue();
        // 폼 필수 값 유효성 체크
        if (!contentMain.checkFormValidation('set')) {
            return;
        }
        // 알림톡의 경우 버튼 데이터가 있는 경우 serialize
        contentMain.serializeButton();
        // SMS의 경우 첨부파일이 있다면 업로드
        contentMain.setAttachment();

        let data = _F.getFormDataByJson($('#contentSetForm'));
        CommonAjax.basic({
            url: '/message/admin/content/setContent.json',
            data: data,
            contentType: 'application/json',
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnStatus === 200 && res.data.contentSeq > 0) {
                    alert("등록 되었습니다.");
                } else {
                    alert("등록에 실패했습니다.");
                }
                // 리스트 새로 호출
                contentMain.refreshGrid();
                // 하단 폼 전부 reset
                contentMain.setFormReset();
            },
            errorCallbackFunc: function (res) {
                let data = res.responseJSON.data;
                if (data !== null &&
                    data.includes('SQLIntegrityConstraintViolationException')) {
                    alert('동일한 템플릿 코드가 있습니다. 코드 확인 후 등록바랍니다.');
                } else {
                    alert('에러가 발생하여 등록에 실패했습니다.');
                }
            }
        });
    },

    /**
     * 신규/수정 모드에 따른 form 유효성 체크
     */
    checkFormValidation: function (mode) {
        let workType = $('#workTypeF').val();
        let seq = $('#contentSeq').val();
        let body = $('#contentBody').val();
        let name = $('#name').val();
        let desc = $('#description').val();
        let templateCode = $('#templateCode').val();
        let blockYn = $('#blockYn').val();

        if (workType === '') {
            alert("발송 채널 타입이 없습니다.");
            return false;
        }
        if (!seq && mode === 'edit') {
            alert("컨텐츠 id를 찾을 수 없습니다.");
            return false;
        }
        if (!body) {
            alert("컨텐츠 내용이 없습니다.");
            return false;
        }
        if (!name) {
            alert("컨텐츠 템플릿 명이 없습니다.");
            return false;
        }
        if (!desc) {
            alert("컨텐츠 설명이 없습니다.");
            return false;
        }
        if (!templateCode) {
            alert("템플릿 코드가 없습니다.");
            return false;
        }
        if (blockYn === 'Y') {
            let reg = /([2][0-3]|[01][0-9]):([0-5][0-9])/;
            let bst = $('#blockStartTime').val();
            let bet = $('#blockEndTime').val();

            if (!bst.match(reg) || !bet.match(reg)) {
                alert("차단 시간이 정확하지 않습니다.");
                return false;
            }
        }
        return true;
    },

    /**
     * 리스트 새로고침
     */
    refreshGrid: function () {
        contentMain.searchFormReset();
        contentMain.search();
    },

    /**
     * 컨텐츠 Table row 를 눌렀을 때
     * @param selectRowId selectRowId
     */
    gridRowSelect: function (selectRowId) {
        let data = contentListGrid.dataProvider.getJsonRow(selectRowId);
        CommonAjax.basic({
            url: '/message/admin/content/getContent.json?contentSeq='
                + data.contentSeq,
            data: data,
            contentType: 'application/json',
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                contentMain.setDetailData(res.data);
            }
        });
    },

    /**
     * 하단 폼에 표시
     * @param data data
     */
    setDetailData: function (data) {
        $('#contentSeq').val(data.contentSeq);
        $('#name').val(data.name);
        $('#description').val(data.description);
        $('#workTypeF').val(data.workType);
        $('#affiliationTypeF').val(data.affiliationType);
        $('#storeTypeF').val(data.storeType);
        $('#callback').val(data.callback);
        $('#templateCode').val(data.templateCode);
        $('#blockYn').val(data.blockYn);
        if (data.blockYn === 'Y') {
            contentMain.enableBlockYn();
            $('#blockStartTime').val(data.blockStartTime);
            $('#blockEndTime').val(data.blockEndTime);
        } else {
            $('#blockStartTime').val('');
            $('#blockEndTime').val('');
            contentMain.disableBlockYn();
        }
        $('#etc1').val(data.etc1);
        $('#etc2').val(data.etc2);
        $('#etc3').val(data.etc3);
        $('#etc4').val(data.etc4);
        $('#etc5').val(data.etc5);
        $('#attachment').val(data.attachment);
        $('#regId').val(data.regId);
        $('#regDt').val(data.regDt.replace("T", " "));
        $('#chgId').val(data.chgId);
        $('#chgDt').val(data.chgDt.replace("T", " "));
        $('#contentSubject').val(data.contentSubject);

        if (data.workType === 'MAIL') {
            commonEditor.setHtml($('#contentBodyH'), data.contentBody);
        } else {
            $('#contentBodyT').val(data.contentBody);
        }

        contentMain.editMode();
        contentMain.changeWorkType();
        contentMain.resetButtonParamName();

        if (data.workType === 'ALIMTALK') {
            contentMain.deserializeButton(data.attachment);
        } else if (data.workType === 'SMS') {
            $('#fileId').html(data.attachment);
        } else {

        }
    },

    /**
     * 수정 상태 폼 제어
     */
    editMode: function () {
        $('#regId').attr('readOnly', true);
        $('#chgId').attr('readOnly', true);
        $('#regDt').attr('readOnly', true);
        $('#chgDt').attr('readOnly', true);
        $('#setContentBtn').hide();
        $('#editContentBtn').show();
    },

    /**
     * 등록 상태 폼 제어
     */
    setMode: function () {
        $('#regId').attr('readOnly', true);
        $('#chgId').attr('readOnly', true);
        $('#regDt').attr('readOnly', true);
        $('#chgDt').attr('readOnly', true);
        $('#setContentBtn').show();
        $('#editContentBtn').hide();
    },

    /**
     * 메일의 경우 Froala 에디터 내용을 가져오고
     * 이외의 경우 텍스트박스에서 내용을 가져와 serialize 되도록 form에 세팅
     */
    setBodyValue: function () {
        if ($('#workTypeF').val() === 'MAIL') {
            $('#contentBody').val(commonEditor.getHtmlData('#contentBodyH'));
        } else {
            $('#contentBody').val($('#contentBodyT').val());
        }
    },

    /**
     * 발송 채널 타입이 바뀐 경우 해야 할 액션
     */
    changeWorkType: function () {
        let workType = $('#workTypeF').val();
        // SET1. 메일인 경우 에디터 변경
        if (workType === 'MAIL') {
            $('#html').show();
            $('#text').hide();
        } else {
            $('#text').show();
            $('#html').hide();
        }

        // SET2. 알림톡인 경우 버튼 노출
        if (workType === 'ALIMTALK') {
            $('#buttonForm').show();
        } else {
            $('#buttonForm').hide();
            $('#buttonForm').resetForm();
        }

        // SET3. 문자인 경우 파일첨부 노출
        if (workType === 'SMS') {
            $('#mmsAttachForm').show();
        } else {
            $('#mmsAttachForm').hide();
            $('#mmsAttachForm').resetForm();
        }
    },

    /**
     * 버튼 타입이 바뀐 경우 뒤 따르는 파라미터 명칭을 바꾸고
     * serialize 시 key값으로 사용 될 수 있도록 세팅
     * @param type
     */
    changeButtonParamName: function (type) {
        let index = type.substring(10, 11);
        let val = $('#' + type).val();
        let param1 = $('#buttonParam' + index + '1');
        let param2 = $('#buttonParam' + index + '2');
        // 앱링크에서도 URL 파라미터만 사용하므로 scheme 키는 주석처리하고 웹링크와 같은 파라미터 첨부
        // 롤백
        // if (val === 'WL' || val === 'AL') {
        if (val === 'WL') {
            param1.html('url_mobile');
            param2.html('url_pc');
        } else if (val === 'AL') {
            param1.html('scheme_android');
            param2.html('scheme_ios');
        } else if (val === 'BC') {
            param1.html('chat_extra');
            param2.html('');
        } else if (val === 'BT') {
            param1.html('chat_extra');
            param2.html('char_event');
        } else {
            param1.html('');
            param2.html('');
        }
    },

    /**
     * 버튼 파라미터 이름 초기화
     */
    resetButtonParamName: function () {
        for (let i = 1; i <= 5; i++) {
            $('#buttonType' + i).val('');
            $('#buttonName' + i).val('');
            $('#param' + i + '1').val('');
            $('#param' + i + '2').val('');
            $('#buttonParam' + i + '1').html('');
            $('#buttonParam' + i + '2').html('');
        }
    },

    /**
     * 버튼정보를 모아 serialize 이전에 attachment에 넣기
     */
    serializeButton: function () {
        const attachment = $('#attachment');
        if ($('#workTypeF').val() !== 'ALIMTALK') {
            return;
        }

        let jsonArray = [];

        for (let i = 1; i <= 5; i++) {
            let type = $('#buttonType' + i).val();
            let name = $('#buttonName' + i).val();

            if (!type) {
                continue;
            }

            let data = {
                type: type,
                name: name
            };

            for (let j = 1; j <= 2; j++) {
                let k = $('#buttonParam' + i + j).html();
                let v = $('#param' + i + j).val();
                // 이후 AL 에 scheme 파라미터를 사용하는 경우 이곳에서 추가로직 필요
                if (k !== '') {
                    data[k] = v;
                }
            }
            jsonArray.push(data);
        }
        if (jsonArray.length > 0) {
            let button = {
                button: jsonArray
            };
            attachment.val(JSON.stringify(button));
        } else {
            attachment.val('');
        }
    },

    /**
     * 가져온 버튼정보를 하단 버튼폼에 넣어준다.
     */
    deserializeButton: function (json) {
        if (!json) {
            contentMain.resetButtonParamName();
            return;
        }
        /* structure
        {
            button: [
                {
                    type: type,
                    name: name,
                    param1: param1,
                    param2: param2
                },
                {
                    ...
                }
            ]
        }
        */
        const data = JSON.parse(json);
        const array = data.button;
        for (let i = 1; i <= array.length; i++) {
            let button = array[i - 1];
            $('#buttonType' + i).val(button.type);
            $('#buttonName' + i).val(button.name);

            contentMain.changeButtonParamName('buttonType' + i);

            for (let j = 1; j <= 2; j++) {
                let k = $('#buttonParam' + i + j).html();
                let v = $('#param' + i + j)
                v.val(button[k]);
            }
        }
    },

    /**
     * MMS 첨부파일 추가
     */
    setAttachment: function () {
        if (!file.files[0] || $('#workTypeF').val() !== 'SMS') {
            return;
        }
        let form = jQuery('#mmsAttachForm')[0];
        let formData = new FormData(form);
        formData.append("file", file.files[0]);
        jQuery.ajax({
            url: "/message/admin/content/setAttachment.json",
            type: "POST",
            async: false, // 호출 순서를 위해서 필요
            processData: false,
            contentType: false,
            data: formData,
            success: function (res) {
                if (!res.data) {
                    alert("첨부파일 등록에 실패했습니다.");
                } else {
                    $('#attachment').val(res.data);
                }
            }
        });
    },

    /**
     * MMS 첨부파일 삭제
     */
    deleteAttachment: function () {
        let contentSeq = $('#contentSeq').val();
        let workType = $('#workTypeF').val();

        if (workType !== 'SMS' || contentSeq === 0) {
            alert("유효한 요청이 아닙니다.");
            return;
        }
        CommonAjax.basic({
            url: '/message/admin/content/deleteAttachment.json?contentSeq='
                + contentSeq,
            data: null,
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnStatus === 200 && res.data > 0) {
                    alert("삭제 되었습니다.");
                } else {
                    alert("삭제에 실패했습니다.");
                }
                // 리스트 새로 호출
                contentMain.refreshGrid();
                // 하단 폼 전부 reset
                contentMain.setFormReset();
            }
        });
    }
};

const contentListGrid = {
    gridView: new RealGridJS.GridView("contentListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        contentListGrid.initGrid();
        contentListGrid.initDataProvider();
        contentListGrid.event();
    },
    initGrid: function () {
        contentListGrid.gridView.setDataSource(contentListGrid.dataProvider);
        contentListGrid.gridView.setStyles(
            contentInfoGridBaseInfo.realgrid.styles);
        contentListGrid.gridView.setDisplayOptions(
            contentInfoGridBaseInfo.realgrid.displayOptions);
        contentListGrid.gridView.setColumns(
            contentInfoGridBaseInfo.realgrid.columns);
        contentListGrid.gridView.setOptions(
            contentInfoGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        contentListGrid.dataProvider.setFields(
            contentInfoGridBaseInfo.dataProvider.fields);
        contentListGrid.dataProvider.setOptions(
            contentInfoGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        contentListGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            contentMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        contentListGrid.dataProvider.clearRows();
        contentListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (contentListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "컨텐츠_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        contentListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

const content = {};

$(document).ready(function () {
    contentMain.init();
    contentListGrid.init();
    CommonAjaxBlockUI.global();
});
