/**
 * 메세지관리 > 자동발송관리 > Push 이미지 관리
 */
const pushimageMain = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initSearchDate('-30d');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        pushimageMain.setDate = Calendar.datePickerRange('schStartDate',
            'schEndDate');

        pushimageMain.setDate.setEndDate(0);
        pushimageMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate",
            $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {
        $('#searchResetBtn').on('click', function () {
            pushimageMain.searchFormReset();
        });
        $('#searchBtn').on('click', function () {
            pushimageMain.search();
        });
        $('#registBtn').on('click', function () {
            pushimageMain.set();
        });
        $('#resetBtn').on('click', function () {
            pushimageMain.setFormReset();
        });
    },
    /**
     * Push 이미지 이력 검색
     */
    search: function () {
        var s = $('#schStartDate').val();
        var e = $('#schEndDate').val();
        if (moment(e).diff(moment(s), 'month') >= 1) {
            alert('최대 1개월까지만 조회 가능합니다.');
            return;
        }

        CommonAjax.basic({
            url: '/message/admin/pushimage/getList.json',
            data: $('#pushimageSearchForm').serialize(),
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                pushimageListGrid.setData(res.data);
                $('#pushimageTotalCount').html(
                    $.jUtil.comma(pushimageListGrid.dataProvider.getRowCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset: function () {
        $("#pushimageSearchForm").resetForm();

        pushimageMain.initSearchDate('-30d');
    },
    /**
     * Push 이미지 등록
     */
    set: function () {
        var imageNameVal = $("#imageName").val();

        if (imageNameVal == "") {
            alert("이미지 이름을 선택해 주세요.");
            return;
        }

        var imageFileVal = $("#imageFile").val();

        if (imageFileVal == "") {
            alert("Push 이미지를 선택해 주세요.");
            return;
        }

        var form = jQuery('#pushimageSetForm')[0];
        var formData = new FormData(form);
        jQuery.ajax({
            url: "/message/admin/pushimage/setPushImage.json",
            type: "POST",
            async: false, // 호출 순서를 위해서 필요
            processData: false,
            contentType: false,
            data: formData,
            success: function (res) {
                if (res.data.returnCode === 'ERROR') {
                    alert("등록에 실패했습니다. 사유 : " + res.data.message);
                } else {
                    alert("등록 되었습니다.");
                    pushimageMain.setFormReset();
                    pushimageMain.search();
                }
            }
        });
    },
    /**
     * 등록 폼 초기화
     */
    setFormReset: function () {
        // form reset
        $("#pushimageSetForm").resetForm();
    }
};

const pushimageListGrid = {
    gridView: new RealGridJS.GridView("pushimageListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pushimageListGrid.initGrid();
        pushimageListGrid.initDataProvider();
        pushimageListGrid.event();
    },
    initGrid: function () {
        pushimageListGrid.gridView.setDataSource(pushimageListGrid.dataProvider);

        pushimageListGrid.gridView.setStyles(
            pushimageGridBaseInfo.realgrid.styles);
        pushimageListGrid.gridView.setDisplayOptions(
            pushimageGridBaseInfo.realgrid.displayOptions);
        pushimageListGrid.gridView.setColumns(
            pushimageGridBaseInfo.realgrid.columns);
        pushimageListGrid.gridView.setOptions(
            pushimageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        pushimageListGrid.dataProvider.setFields(
            pushimageGridBaseInfo.dataProvider.fields);
        pushimageListGrid.dataProvider.setOptions(
            pushimageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        pushimageListGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            pushimageMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        pushimageListGrid.dataProvider.clearRows();
        pushimageListGrid.dataProvider.setRows(dataList);
    }
};

$(document).ready(function () {
    pushimageMain.init();
    pushimageListGrid.init();
    CommonAjaxBlockUI.global();
});
