/**
 * 메세지관리 > 발송관리 > SMS발송조회
 */
const smsMain = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initSearchDate('0d');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        smsMain.setDate = Calendar.datePickerRange('schStartDate',
            'schEndDate');

        smsMain.setDate.setEndDate(0);
        smsMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate",
            $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {
        $('#searchResetBtn').on('click', function () {
            smsMain.searchFormReset();
        });
        $('#searchBtn').on('click', function () {
            smsMain.search();
        });
        $('#excelDownloadBtn').on('click', function () {
            smsMain.excelDownload();
        });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function () {
        smsListGrid.excelDownload();
    },
    /**
     * SMS 발송 이력 검색
     */
    search: function () {
        if (smsMain.checkDate() === false) {
            return;
        }

        CommonAjax.basic({
            url: '/message/admin/sms/history/getWorks.json',
            data: $('#smsSearchForm').serialize(),
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                smsListGrid.setData(res.data);
                $('#smsTotalCount').html(
                    $.jUtil.comma(smsListGrid.dataProvider.getRowCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset: function () {
        $("#smsSearchForm").resetForm();

        smsMain.initSearchDate('0d');
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = smsListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: '/message/admin/sms/history/getDetail.json?smsWorkSeq='
                + rowDataJson.smsWorkSeq,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                var workDetail = res.data;
                smsMain.setDetailData(workDetail);
            }
        });
    },

    feedToBr: function (str) {
        return (str !== null && str !== '')
            ? str.replace(/(\n|\r|\r\n)/g, '<br>')
            : '';
    },

    setDetailData: function (data) {
        $('#workName').html(data.smsWorkName);
        $('#description').html(data.description);
        $('#subject').html(smsMain.feedToBr(data.subject));
        $('#bodyTemplate').html(smsMain.feedToBr(data.body));
        $('#regDt').html(data.regDt.replace("T", " "));
        $('#regName').html(data.regId);
        $('#chgDt').html(data.chgDt.replace("T", " "));
        $('#chgName').html(data.chgId);
        sendListGrid.setData(data.sendList);

        var detailTotalCount = sendListGrid.dataProvider.getRowCount();
        var sendCount = 0;
        var failCount = 0;

        for (var i = 0; i < detailTotalCount; i++) {
            var row = sendListGrid.dataProvider.getJsonRow(i);
            if (row.sendResultCd == "COMPLETED") {
                sendCount++;
            } else if (row.sendResultCd == "ERROR") {
                failCount++;
            }
        }

        $('#setCount').html($.jUtil.comma(detailTotalCount));
        $('#sendCount').html($.jUtil.comma(sendCount));
        $('#failCount').html($.jUtil.comma(failCount));

        $('#resendBtn').show();
    },
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },
    isNotEmpty: function (value) {
        return !smsMain.isEmpty(value);
    },
    checkDate: function () {
        let startDt = $('#schStartDate');
        let endDt = $('#schEndDate');
        let diff = moment(endDt.val()).diff(startDt.val(), "day", true) + 1;

        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() ||
            !moment(endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("날짜 형식이 맞지 않습니다.");
            return false;
        }

        let isKeyword = smsMain.isNotEmpty($('#schKeyword').val());
        let workStatus = smsMain.isNotEmpty($('#schWorkStatus').val());

        if (isKeyword || workStatus) {
            if (diff > 7) {
                alert("7일 이내만 검색 가능합니다.");
                return false;
            }
        } else {
            if (diff > 3) {
                alert("상세조건을 설정 해 주세요.");
                return false;
            }
        }
        return true;
    },
    resend: function() {
        let id = $('#getCurrentRow').val();
        if(id >= 0) {
            let row = smsListGrid.dataProvider.getJsonRow(id);
            CommonAjax.basic({
                url: '/message/admin/sms/resend.json?smsWorkSeq=' + row.smsWorkSeq,
                data: null,
                method: 'GET',
                successMsg: null,
                callbackFunc: function (res) {
                    alert(res.data);
                }
            });
        } else {
            alert("정상적으로 선택되지 않았습니다.")
        }
    },
    setCurrentRow: function(id) {
        $('#getCurrentRow').val(id);
    }
};

const smsListGrid = {
    gridView: new RealGridJS.GridView("smsListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        smsListGrid.initGrid();
        smsListGrid.initDataProvider();
        smsListGrid.event();
    },
    initGrid: function () {
        smsListGrid.gridView.setDataSource(smsListGrid.dataProvider);

        smsListGrid.gridView.setStyles(smsWorkGridBaseInfo.realgrid.styles);
        smsListGrid.gridView.setDisplayOptions(
            smsWorkGridBaseInfo.realgrid.displayOptions);
        smsListGrid.gridView.setColumns(smsWorkGridBaseInfo.realgrid.columns);
        smsListGrid.gridView.setOptions(smsWorkGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        smsListGrid.dataProvider.setFields(
            smsWorkGridBaseInfo.dataProvider.fields);
        smsListGrid.dataProvider.setOptions(
            smsWorkGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        smsListGrid.gridView.onDataCellClicked = function (gridView, index) {
            smsMain.gridRowSelect(index.dataRow);
            smsMain.setCurrentRow(index.dataRow);
        };
    },
    setData: function (dataList) {
        smsListGrid.dataProvider.clearRows();
        smsListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (smsListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "SMS발송이력_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        smsListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

const sendListGrid = {
    gridView: new RealGridJS.GridView("sendListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        sendListGrid.initGrid();
        sendListGrid.initDataProvider();
    },
    initGrid: function () {
        sendListGrid.gridView.setDataSource(sendListGrid.dataProvider);

        sendListGrid.gridView.setStyles(
            smsSendListGridBaseInfo.realgrid.styles);
        sendListGrid.gridView.setDisplayOptions(
            smsSendListGridBaseInfo.realgrid.displayOptions);
        sendListGrid.gridView.setColumns(
            smsSendListGridBaseInfo.realgrid.columns);
        sendListGrid.gridView.setOptions(
            smsSendListGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        sendListGrid.dataProvider.setFields(
            smsSendListGridBaseInfo.dataProvider.fields);
        sendListGrid.dataProvider.setOptions(
            smsSendListGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        sendListGrid.dataProvider.clearRows();
        sendListGrid.dataProvider.setRows(dataList);
    }
};

$(document).ready(function () {
    smsMain.init();
    smsListGrid.init();
    sendListGrid.init();
    CommonAjaxBlockUI.global();
});
