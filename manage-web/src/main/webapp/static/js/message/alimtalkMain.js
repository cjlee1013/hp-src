/**
 * 메세지관리 > 발송관리 > 알림톡발송조회
 */
const alimtalkMain = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initSearchDate('0d');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        alimtalkMain.setDate = Calendar.datePickerRange('schStartDate',
            'schEndDate');

        alimtalkMain.setDate.setEndDate(0);
        alimtalkMain.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate",
            $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {
        $('#searchResetBtn').on('click', function () {
            alimtalkMain.searchFormReset();
        });
        $('#searchBtn').on('click', function () {
            alimtalkMain.search();
        });
        $('#excelDownloadBtn').on('click', function () {
            alimtalkMain.excelDownload();
        });
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function () {
        alimtalkListGrid.excelDownload();
    },
    /**
     * 알림톡 발송 이력 검색
     */
    search: function () {
        if (alimtalkMain.checkDate() === false) {
            return;
        }

        CommonAjax.basic({
            url: '/message/admin/alimtalk/history/getWorks.json',
            data: $('#alimtalkSearchForm').serialize(),
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                alimtalkListGrid.setData(res.data);
                $('#alimtalkTotalCount').html(
                    $.jUtil.comma(alimtalkListGrid.dataProvider.getRowCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset: function () {
        $("#smsSearchForm").resetForm();

        smsMain.initSearchDate('0d');
    },
    gridRowSelect: function (selectRowId) {
        var rowDataJson = alimtalkListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: '/message/admin/alimtalk/history/getDetail.json?alimtalkWorkSeq='
                + rowDataJson.alimtalkWorkSeq,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                var workDetail = res.data;
                alimtalkMain.setDetailData(workDetail);
            }
        });
    },

    feedToBr: function (str) {
        return (str !== null && str !== '')
            ? str.replace(/(\n|\r|\r\n)/g, '<br>')
            : '';
    },

    setDetailData: function (data) {
        $('#workName').html(data.alimtalkWorkName);
        $('#description').html(data.description);
        $('#subject').html(alimtalkMain.feedToBr(data.subject));
        $('#bodyTemplate').html(alimtalkMain.feedToBr(data.body));
        $('#regDt').html(data.regDt.replace("T", " "));
        $('#regName').html(data.regId);
        $('#chgDt').html(data.chgDt.replace("T", " "));
        $('#chgName').html(data.chgId);
        sendListGrid.setData(data.sendList);

        var detailTotalCount = sendListGrid.dataProvider.getRowCount();
        var sendCount = 0;
        var failCount = 0;

        for (var i = 0; i < detailTotalCount; i++) {
            var row = sendListGrid.dataProvider.getJsonRow(i);
            if (row.sendResultCd === "COMPLETED") {
                sendCount++;
            } else if (row.sendResultCd === "ERROR") {
                failCount++;
            }
        }

        $('#setCount').html($.jUtil.comma(detailTotalCount));
        $('#sendCount').html($.jUtil.comma(sendCount));
        $('#failCount').html($.jUtil.comma(failCount));
    },
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },
    isNotEmpty: function (value) {
        return !alimtalkMain.isEmpty(value);
    },
    checkDate: function () {
        let startDt = $('#schStartDate');
        let endDt = $('#schEndDate');
        let diff = moment(endDt.val()).diff(startDt.val(), "day", true) + 1;

        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() ||
            !moment(endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("날짜 형식이 맞지 않습니다.");
            return false;
        }

        let isKeyword = alimtalkMain.isNotEmpty($('#schKeyword').val());
        let workStatus = alimtalkMain.isNotEmpty($('#schWorkStatus').val());

        if (isKeyword || workStatus) {
            if (diff > 7) {
                alert("7일 이내만 검색 가능합니다.");
                return false;
            }
        } else {
            if (diff > 3) {
                alert("상세조건을 설정 해 주세요.");
                return false;
            }
        }
        return true;
    }
};

const alimtalkListGrid = {
    gridView: new RealGridJS.GridView("alimtalkListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        alimtalkListGrid.initGrid();
        alimtalkListGrid.initDataProvider();
        alimtalkListGrid.event();
    },
    initGrid: function () {
        alimtalkListGrid.gridView.setDataSource(alimtalkListGrid.dataProvider);

        alimtalkListGrid.gridView.setStyles(
            alimtalkWorkGridBaseInfo.realgrid.styles);
        alimtalkListGrid.gridView.setDisplayOptions(
            alimtalkWorkGridBaseInfo.realgrid.displayOptions);
        alimtalkListGrid.gridView.setColumns(
            alimtalkWorkGridBaseInfo.realgrid.columns);
        alimtalkListGrid.gridView.setOptions(
            alimtalkWorkGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        alimtalkListGrid.dataProvider.setFields(
            alimtalkWorkGridBaseInfo.dataProvider.fields);
        alimtalkListGrid.dataProvider.setOptions(
            alimtalkWorkGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        alimtalkListGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            alimtalkMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        alimtalkListGrid.dataProvider.clearRows();
        alimtalkListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (alimtalkListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "알림톡발송이력_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        alimtalkListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

const sendListGrid = {
    gridView: new RealGridJS.GridView("sendListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        sendListGrid.initGrid();
        sendListGrid.initDataProvider();
    },
    initGrid: function () {
        sendListGrid.gridView.setDataSource(sendListGrid.dataProvider);

        sendListGrid.gridView.setStyles(
            alimtalkSendListGridBaseInfo.realgrid.styles);
        sendListGrid.gridView.setDisplayOptions(
            alimtalkSendListGridBaseInfo.realgrid.displayOptions);
        sendListGrid.gridView.setColumns(
            alimtalkSendListGridBaseInfo.realgrid.columns);
        sendListGrid.gridView.setOptions(
            alimtalkSendListGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        sendListGrid.dataProvider.setFields(
            alimtalkSendListGridBaseInfo.dataProvider.fields);
        sendListGrid.dataProvider.setOptions(
            alimtalkSendListGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        sendListGrid.dataProvider.clearRows();
        sendListGrid.dataProvider.setRows(dataList);
    }
};

const alimtalk = {};

$(document).ready(function () {
    alimtalkMain.init();
    alimtalkListGrid.init();
    sendListGrid.init();
    CommonAjaxBlockUI.global();
});
