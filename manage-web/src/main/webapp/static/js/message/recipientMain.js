/**
 * 메세지관리 > 발송관리 > 문자발송이력
 */
const recipientMain = {
    init: function () {
        this.event();
        this.initDate('0d');
    },
    initDate: function (flag) {
        recipientMain.setDate =
            Calendar.datePickerRange('schStartDate', 'schEndDate');
        recipientMain.setDate.setEndDate(0);
        recipientMain.setDate.setStartDate(flag);
        $("#schEndDate").datepicker(
            "option", "minDate", $("#schStartDate").val());
    },
    event: function () {

    },
    search: function () {
        if(!recipientMain.validation()) {
            alert("수신번호를 입력 해 주세요.");
            return false;
        }

        $('#schKeyword').val(recipientMain.mergeInfo());
        if (recipientMain.checkDate()) {
            let data = _F.getFormDataByJson($("#recipientSearchForm"));
            CommonAjax.basic({
                url: '/message/admin/recipient/search.json',
                data: data,
                method: 'POST',
                contentType: 'application/json',
                successMsg: null,
                callbackFunc: function (res) {
                    recipientListGrid.setData(res);
                    $('#RecipientCount').html($.jUtil.comma(
                        recipientListGrid.dataProvider.getRowCount()));
                }
            });
        }
    },
    getDetail: function (id) {
        let row = recipientListGrid.dataProvider.getJsonRow(id);
        CommonAjax.basic({
            url: '/message/admin/recipient/getDetail.json?listSeq='
                + row.listSeq + '&historySeq=' + row.historySeq + '&workType='
                + row.workType,
            data: null,
            method: 'GET',
            successMsg: null,
            callbackFunc: function (res) {
                $('#subject').val(res.subject);
                $('#body').val(res.body);

                if(row.workType === 'SMS') {
                    $('#resendTr').show();
                } else {
                    $('#resendTr').hide();
                }
            }
        });
    },
    resend: function() {
        let id = $('#getCurrentRow').val();
        if(id >= 0) {
            let row = recipientListGrid.dataProvider.getJsonRow(id);
            CommonAjax.basic({
                url: '/message/admin/recipient/resend.json?smsWorkSeq=' + row.workSeq
                    + '&smsListSeq=' + row.listSeq
                    + '&smsSendHistorySeq=' + row.historySeq
                    + '&workType=' + row.workType,
                data: null,
                method: 'GET',
                successMsg: null,
                callbackFunc: function (res) {
                    alert(res.data);
                }
            });
        } else {
            alert("리스트에서 다시 선택 해 주세요.");
        }
    },
    mergeInfo: function () {
        let k1 = $('#schKeyword1').val();
        let k2 = $('#schKeyword2').val();
        let k3 = $('#schKeyword3').val();

        if (recipientMain.isEmpty(k1) ||
            recipientMain.isEmpty(k2) ||
            recipientMain.isEmpty(k3)) {
            // 셋중 하나라도 비어있는 경우 empty 처리
            return '';
        } else {
            return k1 + '-' + k2 + '-' + k3;
        }
    },
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    },
    isNotEmpty: function (value) {
        return !recipientMain.isEmpty(value);
    },
    checkDate: function () {
        let startDt = $('#schStartDate');
        let endDt = $('#schEndDate');
        let diff = moment(endDt.val()).diff(startDt.val(), "day", true) + 1;

        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() ||
            !moment(endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("날짜 형식이 맞지 않습니다.");
            return false;
        }

        let workType = recipientMain.isNotEmpty($('#workType').val());
        let workStatus = recipientMain.isNotEmpty($('#workStatus').val());
        let isKeyword = recipientMain.isNotEmpty(recipientMain.mergeInfo());

        if (workType || workStatus || isKeyword) {
            if (diff > 7) {
                alert("7일 이내만 검색 가능합니다.");
                return false;
            }
        } else {
            if (diff > 3) {
                alert("상세조건을 설정 해 주세요.");
                return false;
            }
        }
        return true;
    },
    validation: function() {
        // 일자와 수신번호가 반드시 필요
        // 일자는 무조건 들어가므로 수신번호만 체크
        let recipient = recipientMain.mergeInfo();
        return recipientMain.isNotEmpty(recipient);
    },
    reset: function () {
        $('#recipientSearchForm').resetForm();
        recipientMain.initDate('0d');
    },
    setCurrentRow: function(id) {
        $('#getCurrentRow').val(id);
    }
}

const recipientListGrid = {
    gridView: new RealGridJS.GridView("recipientListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        recipientListGrid.initGrid();
        recipientListGrid.initDataProvider();
        recipientListGrid.event();
    },
    initGrid: function () {
        recipientListGrid.gridView.setDataSource(
            recipientListGrid.dataProvider);

        recipientListGrid.gridView.setStyles(
            recipientGridBaseInfo.realgrid.styles);
        recipientListGrid.gridView.setDisplayOptions(
            recipientGridBaseInfo.realgrid.displayOptions);
        recipientListGrid.gridView.setColumns(
            recipientGridBaseInfo.realgrid.columns);
        recipientListGrid.gridView.setOptions(
            recipientGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        recipientListGrid.dataProvider.setFields(
            recipientGridBaseInfo.dataProvider.fields);
        recipientListGrid.dataProvider.setOptions(
            recipientGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        recipientListGrid.gridView.onDataCellClicked =
            function (gridView, index) {
                recipientMain.getDetail(index.dataRow);
                recipientMain.setCurrentRow(index.dataRow);
            };
    },
    setData: function (dataList) {
        recipientListGrid.dataProvider.clearRows();
        recipientListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (recipientListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "수신자_" + _date.getFullYear() + (_date.getMonth() + 1)
            + _date.getDate();

        recipientListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
}

$(document).ready(function () {
    recipientMain.init();
    recipientListGrid.init();
    CommonAjaxBlockUI.global();
});