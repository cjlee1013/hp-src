const appStoreReviewControlMain = {
    /**
     * init 이벤트
     */
    init: function () {
        appStoreReviewControlMain.buttonEvent();
        appStoreReviewControlMain.search('home');
        appStoreReviewControlMain.search('club');
    },

    /**
     * button 이벤트
     */
    buttonEvent: function () {
        // 저장
        $("#saveBtn_home").on('click', function () {
            appStoreReviewControlMain.save('home');
        });
        // 저장
        $("#saveBtn_club").on('click', function () {
            appStoreReviewControlMain.save('club');
        });
    },

    /**
     * 목록 데이터 조회
     */
    search: function (siteType) {
        var seq = $('#seq_' + siteType).val();
        manageAjax('/app/mng/appstore/review/getAppStoreReviewControlData.json?seq=' + seq, 'get', null, function (res) {
            if (res.data) {
                $('#version_' + siteType).val(res.data.version);
                $('#controlYn_' + siteType).val(res.data.controlYn);
            }
        });
    },

    /**
     * 등록/수정
     */
    save: function (siteType) {
        var seqId = '#seq_' + siteType;
        var siteTypeId = '#siteType_' + siteType;
        var versionId = '#version_' + siteType;
        var controlYnId = '#controlYn_' + siteType;

        if ($(versionId).val() == '') {
            alert('버전정보를 입력해 주세요.');
            return;
        }
        if ($(controlYnId).val() == '') {
            alert('제어여부를 선택해 주세요.');
            return;
        }

        var data = JSON.stringify({'seq': $(seqId).val(),
            'siteType': $(siteTypeId).val(),
            'version': $(versionId).val(),
            'controlYn': $(controlYnId).val()});
        manageAjax('/app/mng/appstore/review/setAppStoreReviewControlData.json', 'post',
            data,
            function (res) {
                alert($(siteTypeId).val() + ' 앱버전을 저장 하였습니다.');
            });
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg) {
    var params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = 'application/json; charset=utf-8';

    //content Type
    $.ajaxSetup({contentType: "application/json; charset=utf-8",});
    CommonAjax.basic(params);
}
