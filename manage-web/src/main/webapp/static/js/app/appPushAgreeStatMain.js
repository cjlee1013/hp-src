const appPushAgreeStatMain = {
    /**
     * init 이벤트
     */
    init: function () {
        appPushAgreeStatMain.setMonth();
        appPushAgreeStatMain.buttonEvent();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function () {
        // 검색
        $("#searchBtn").on('click', function () {
            appPushAgreeStatMain.search();
        });
        // 검색 초기화
        $("#initBtn").on('click', function () {
            appPushAgreeStatMain.initSearchYM();
            appPushAgreeStatMain.setMonth();
        });
    },

    /**
     * 목록 데이터 조회
     */
    search: function () {
        const year =  $('#currentYear option:selected').val();
        const month = $('#currentMonth option:selected').val();

        const uri = "/app/mng/adver/push/getAgreeStatList.json?year=" + year
            + "&month=" + month;

        manageAjax(uri, 'get',null, function (res) {
                appPushAgreeStatGrid.setData(res);
                //count
                $('#searchListCount').html(
                    $.jUtil.comma(appPushAgreeStatGrid.gridView.getItemCount()));
            });
    },
    /**
     * 조회기간 년도, 월별 값 설정
     */
    initSearchYM : function () {
        const year = moment().format('YYYY');
        const month = moment().format('MM');

        $('#currentYear').val(year);
        $('#currentMonth').val(month);
    },
    /**
     * 조회기간 셀렉트박스 월별 부분 년도에 맞춰 설정
     */
    setMonth: function () {
        const year =  $('#currentYear option:selected').val();
        const currentMonth = $('#currentMonth');

        const uri = "/app/mng/adver/push/getMonths.json?year=" + year;
        manageAjax(uri, 'get',null, function (res) {
            currentMonth.empty();

            for (let i=0; i< res.length; i++){
                currentMonth.append("<option value='" + res[i].optionValue + "'>"
                    + res[i].optionStr + "</option>");
            }

            currentMonth.focus();
        });
    },
};

const appPushAgreeStatGrid = {
    gridView: new RealGridJS.GridView("appPushAgreeStatGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        appPushAgreeStatGrid.initGrid();
        appPushAgreeStatGrid.initDataProvider();
        appPushAgreeStatGrid.event();
    },
    initGrid: function () {
        appPushAgreeStatGrid.gridView.setDataSource(
            appPushAgreeStatGrid.dataProvider);
        appPushAgreeStatGrid.gridView.setStyles(
            appPushAgreeStatGridBaseInfo.realgrid.styles);
        appPushAgreeStatGrid.gridView.setDisplayOptions(
            appPushAgreeStatGridBaseInfo.realgrid.displayOptions);
        appPushAgreeStatGrid.gridView.setColumns(
            appPushAgreeStatGridBaseInfo.realgrid.columns);
        appPushAgreeStatGrid.gridView.setOptions(
            appPushAgreeStatGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        appPushAgreeStatGrid.dataProvider.setFields(
            appPushAgreeStatGridBaseInfo.dataProvider.fields);
        appPushAgreeStatGrid.dataProvider.setOptions(
            appPushAgreeStatGridBaseInfo.dataProvider.options);
    },
    event: function () {
    },
    setData: function (dataList) {
        appPushAgreeStatGrid.dataProvider.clearRows();
        appPushAgreeStatGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg) {
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = 'application/json; charset=utf-8';

    //content Type
    $.ajaxSetup({contentType: "application/json; charset=utf-8",});
    CommonAjax.basic(params);
}
