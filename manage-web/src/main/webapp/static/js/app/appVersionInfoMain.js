const appVersionMain = {
    /**
     * init 이벤트
     */
    init: function () {
        appVersionMain.buttonEvent();
        appVersionMain.updateApplyDtReset();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function () {
        // 검색 폼 초기화
        $("#searchResetBtn").on('click', function () {
            appVersionMain.searchFormReset();
        });

        // 검색
        $("#searchBtn").on('click', function () {
            appVersionMain.search();
        });

        // 상세 폼 초기화
        $('#registResetBtn').on('click', function () {
            appVersionMain.registFormReset();
        });

        // 앱버전 입력값
        // $('#registVersion').on('blur', function (e) {
        //     appVersionMain.appVersionPattern();
        // });

        // 앱버전정보 등록/수정
        $('#registBtn').on('click', function () {
            appVersionMain.regist();
        });
    },

    /**
     * 검색 폼 초기화
     */
    searchFormReset: function () {
        $("#siteType").val("");
        $("#devicePlatform").val("");
        $("#isUsed").val("");
    },

    /**
     * 상세 폼 초기화
     */
    registFormReset: function () {
        $('#registSeq').val('');
        $('#registSeqView').html('');
        $('#registSiteType').val('HOME').removeAttr('disabled');
        $('#registDevicePlatform').val('ANDROID').removeAttr('disabled');
        $('#registVersion').val('').removeAttr('disabled');
        $('input:radio[name="registForceUpdate"]:input[value="N"]').prop(
            'checked', true);
        $('input:radio[name="registForceUpdate"]').removeAttr('disabled');
        $('input:radio[name="registUpdateNotification"]:input[value="N"]').prop(
            'checked', true);
        $('input:radio[name="registUpdateNotification"]').removeAttr(
            'disabled');
        appVersionMain.updateApplyDtReset();
        $('#registUpdateApplyDt').removeAttr('disabled');
        $('input:radio[name="registIsUsed"]:input[value="true"]').prop(
            'checked',
            true);
        $('input:radio[name="registIsUsed"]').removeAttr('disabled');
        $('#registMemo').val('').removeAttr('disabled');
    },

    /**
     * 상세 폼 데이트피커 초기화
     */
    updateApplyDtReset: function () {
        $('#registUpdateApplyDt').removeAttr('disabled');
        appVersionMain.setDate = CalendarTime.datePicker('registUpdateApplyDt');
        appVersionMain.setDate.setDate(0);
    },

    appVersionPattern: function () {
        var regexp = /^\d{1,2}\.\d{1,2}\.\d{1,2}$/g;
        var version = $('#registVersion').val();
        if (!regexp.test(version)) {
            alert('앱버전은 숫자와 .의 형식으로만 입력하세요.');
            $('#registVersion').val('');
            return false;
        }
        return true;
    },

    /**
     * 목록 데이터 조회
     */
    search: function () {
        const data = $("#searchForm").serialize();
        manageAjax('/app/version/mng/getAppVersionList.json?' + data, 'get',
            null, function (res) {
                appVersionInfoGrid.setData(res);
                //count
                $('#searchListCount').html(
                    $.jUtil.comma(appVersionInfoGrid.gridView.getItemCount()));
            });
    },

    /**
     * 상세 데이터 조회
     */
    detail: function (seq) {
        manageAjax('/app/version/mng/getAppVersion.json?seq=' + seq, 'get',
            null, function (res) {
                $('#registSeq').val(res.seq);
                $('#registSeqView').html(res.seq);
                $('#registSiteType').val(res.siteType).prop('disabled', true);
                $('#registDevicePlatform').val(res.devicePlatform).prop(
                    'disabled', true);
                $('#registVersion').val(res.version).prop('disabled', true);
                $('input:radio[name="registForceUpdate"]:input[value="'
                    + res.forceUpdate + '"]').prop('checked', true);
                $('input:radio[name="registForceUpdate"]').prop('disabled',
                    true);
                $('input:radio[name="registUpdateNotification"]:input[value="'
                    + res.updateNotification + '"]').prop('checked', true).prop(
                    'disabled', true);
                $('input:radio[name="registUpdateNotification"]').prop(
                    'disabled', true);
                $('#registUpdateApplyDt').val(res.updateApplyDt).prop(
                    'disabled', true);
                $('input:radio[name="registIsUsed"]:input[value="' + res.isUsed
                    + '"]').prop('checked', true);
                if (res.isUsedHandler == false) {
                    $('input:radio[name="registIsUsed"]').prop('disabled',
                        true);
                } else {
                    $('input:radio[name="registIsUsed"]').removeAttr(
                        'disabled');
                }
                $('#registMemo').val(res.memo).prop('disabled', true);
            });
    },

    /**
     * 버전정보 등록/수정
     */
    regist: function () {
        if ($('#registSeq').val()) {
            let isUsed = false;
            $('input:radio[name="registIsUsed"]').each(function () {
                isUsed = $(this)[0].hasAttribute('disabled');
            });

            if (isUsed == true) {
                return false;
            }

            manageAjax('/app/version/mng/putAppVersion.json', 'post',
                JSON.stringify({
                    'seq': $('#registSeq').val(),
                    'isUsed': $(
                        'input:radio[name="registIsUsed"]:checked').val()
                }),
                function (res) {
                    alert('수정되었습니다.');
                    appVersionMain.registFormReset();
                    appVersionMain.search();
                });

        } else {

            if (!appVersionMain.registValid()) {
                return false;
            }

            manageAjax('/app/version/mng/setAppVersion.json', 'post',
                JSON.stringify({
                    'siteType': $('#registSiteType').val(),
                    'devicePlatform': $('#registDevicePlatform').val(),
                    'version': $('#registVersion').val(),
                    'forceUpdate': $(
                        'input:radio[name="registForceUpdate"]:checked').val(),
                    'updateNotification': $(
                        'input:radio[name="registUpdateNotification"]:checked').val(),
                    'updateApplyDt': $('#registUpdateApplyDt').val(),
                    'isUsed': $(
                        'input:radio[name="registIsUsed"]:checked').val(),
                    'memo': $('#registMemo').val()
                }),
                function (res) {
                    alert('등록되었습니다.');
                    appVersionMain.registFormReset();
                    appVersionMain.search();
                });

        }

    },

    /**
     * 버전정보 등록 유효성 체크
     */
    registValid: function () {

        // 업데이트 일자 체크
        var nowTime = new Date().getTime();
        var applyDt = $('#registUpdateApplyDt').val().replace(' ', 'T');
        var applyTime = new Date(applyDt).getTime();
        console.log(nowTime, applyTime, applyDt);
        if (applyTime < nowTime) {
            alert("버전 업데이트일은 등록일 이후로 설정해주세요.");
            return false;
        }

        // 앱 버전값 체크
        if (!appVersionMain.appVersionPattern()) {
            return false;
        }

        return true;
    }

};

const appVersionInfoGrid = {
    gridView: new RealGridJS.GridView("appVersionInfoGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        appVersionInfoGrid.initGrid();
        appVersionInfoGrid.initDataProvider();
        appVersionInfoGrid.event();
    },
    initGrid: function () {
        appVersionInfoGrid.gridView.setDataSource(
            appVersionInfoGrid.dataProvider);
        appVersionInfoGrid.gridView.setStyles(
            appVersionMngGridBaseInfo.realgrid.styles);
        appVersionInfoGrid.gridView.setDisplayOptions(
            appVersionMngGridBaseInfo.realgrid.displayOptions);
        appVersionInfoGrid.gridView.setColumns(
            appVersionMngGridBaseInfo.realgrid.columns);
        appVersionInfoGrid.gridView.setOptions(
            appVersionMngGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        appVersionInfoGrid.dataProvider.setFields(
            appVersionMngGridBaseInfo.dataProvider.fields);
        appVersionInfoGrid.dataProvider.setOptions(
            appVersionMngGridBaseInfo.dataProvider.options);
    },
    event: function () {
        appVersionInfoGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            let data = appVersionInfoGrid.dataProvider.getJsonRow(
                index.dataRow);
            appVersionMain.detail(data.seq);

        };
    },
    setData: function (dataList) {
        appVersionInfoGrid.dataProvider.clearRows();
        appVersionInfoGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg) {
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = 'application/json; charset=utf-8';

    //content Type
    $.ajaxSetup({contentType: "application/json; charset=utf-8",});
    CommonAjax.basic(params);
}
