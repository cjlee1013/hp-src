/**
 * 어드민 홈
 */
//홈 공통
var home = {
    generalSelect : {TITLE:"제목", TDESC:"제목+내용"},
    harmSelect : {NMPROD:"제품명", RPTYPE:"유형", INSI:"검사기관", NMMANUFAC:"제조업소", NMSALE:"판매업소"},
    init : function() {
        tabDiv = 'GENERAL';
        home.searchTypeSetting(tabDiv);
        home.event();
    },
    event : function() {
		// 상단 탭 클릭
		$('.tab_vicinity li').on('click', function (){
			$('.tab_vicinity li').removeClass('on');
			$(this).addClass("on");

			tabDiv = $(this).data("type");
			home.searchTypeSetting(tabDiv);
            cnotice.resetDetail();
			switch (tabDiv) {
                case 'GENERAL' :
                    $('#harmGridTotalDiv').hide();
                    $('#cnoticeGridTotalDiv').show();
                    break;
                case 'HARM' :
                    $('#cnoticeGridTotalDiv').hide();
                    $('#cnoticeDetail').hide();
                    $('#harmGridTotalDiv').show();
                    harmManage.gridView.resetSize();
                    break;
            }
		});

        //검색 버튼
        $('#searchBtn').on('click', function(){
            home.search();
        });

        //초기화 버튼
        $('#searchInitBtn').on('click', function(){
            $('#searchType').find('option:first').prop('selected', true);
            $('#searchKeyword').val('');
        });

        //검색어 칸 엔터
        $('#searchKeyword').on("keypress", function (e) {
            if (e.keyCode === 13) {
                home.search();
            }
        });
    },

    //검색
    search : function(){
        switch (tabDiv) {
            case 'GENERAL' :
                cnotice.search();
                break;
            case 'HARM' :
                harmManage.search();
                break;
        }
    },

    //검색어 타입 셋팅
    searchTypeSetting : function(tabDiv){
        var searchType;
        switch(tabDiv) {
            case 'GENERAL' :
                searchType = home.generalSelect;
                break;
            case 'HARM' :
                searchType = home.harmSelect;
                break;
        }
        $('#searchType').html("");
        $.each(searchType, function (val, txt) {
            $('#searchType').append($('<option>', {
                value: val,
                text : txt
            }));
        });
    },
};


//일반공지 관련
var cnotice = {
    gridView : new RealGridJS.GridView("cnoticeGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        cnotice.initGrid();
        cnotice.initDataProvider();
        cnotice.event();
    },
    initGrid : function(){
        cnotice.gridView.setDataSource(cnotice.dataProvider);
        cnotice.gridView.setStyles(cnoticeGridHeadBaseInfo.realgrid.styles);
        cnotice.gridView.setDisplayOptions(cnoticeGridHeadBaseInfo.realgrid.displayOptions);
        cnotice.gridView.setColumns(cnoticeGridHeadBaseInfo.realgrid.columns);
        cnotice.gridView.setOptions(cnoticeGridHeadBaseInfo.realgrid.options);

        cnotice.gridView.setColumnProperty( "noticeTitle", "styles" ,{
            "textAlignment": "near",
            "lineAlignment": "center",
            "iconLocation": "left",     //top, bottom, center, left, right
            "iconAlignment": "center",   //near, center, far  상중하
            "iconOffset": 5 ,           //셀라인과의 여백
            "iconPadding": "0",
        });
        cnotice.gridView.setColumnProperty( "noticeTitle", "renderer" ,{
            "type": "multiIcon",
            "showTooltip": false,
            "textVisible": true,
            "minWidth": 30,
            renderCallback:function(grid, index) {

                var value = cnotice.dataProvider.getJsonRow(index.dataRow);
                var ret = [];
                if (value.topYn == "Y") {
                    ret.push('/static/images/icon_imp.png');
                }
                if (value.newYn == "Y") {
                    ret.push('/static/images/icon_new.png');
                }
                return ret;
            }
        });



    },
    initDataProvider : function() {
        cnotice.dataProvider.setFields(cnoticeGridHeadBaseInfo.dataProvider.fields);
        cnotice.dataProvider.setOptions(cnoticeGridHeadBaseInfo.dataProvider.options);
    },
    event : function() {
        cnotice.gridView.onDataCellClicked = function(gridView, index) {
            cnotice.getCnoticeDetail(index.dataRow);
        };
    },
    setData : function(dataList) {
        cnotice.dataProvider.clearRows();
        cnotice.dataProvider.setRows(dataList);
        //검색결과 건수
        $('#cnoticeSearchCnt').html($.jUtil.comma(cnotice.gridView.getItemCount()));
    },
    //일반공지 상세 조회
    getCnoticeDetail : function (rowdata) {
        cnotice.resetDetail();
        $('#cnoticeDetail').show();
        var rowDataJson = cnotice.dataProvider.getJsonRow(rowdata);

        $("#noticeTitle").html(rowDataJson.noticeTitle);
        $("#noticeDesc").html(rowDataJson.noticeDesc);
        cnotice.getNoticeFileList(rowDataJson.noticeNo);


    },
    resetDetail : function(){
        $("#noticeTitle").html('');
        $("#noticeDesc").html('');
        $("#fileList").html('');
    },
    /**
     * 첨부파일리스트조회
     */
    getNoticeFileList : function(noticeNo) {
        CommonAjax.basic({url:'/manage/notice/getNoticeFileList.json?noticeNo=' + noticeNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                for (var i in res) {
                    if (res[i].useYn == 'Y') {
                        cnotice.appendFile(res[i]);
                    }
                }
            }});

    },
    appendFile : function(f){

        var fileAppend= '<a href="'
            + hmpImgUrl + "/provide/view?processKey=ItemFile&fileId=" + f.fileId
            + '" target="_blank" style="margin-right: 10px;text-decoration: underline;color: #0091ff !important;" ">'
            + f.fileName
        $("#fileList").append(fileAppend);

    },
    //검색
    search : function(){
        CommonAjax.basic({url:'/home/getAdminNoticeList.json?' + $('#searchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                cnotice.resetDetail();
                cnotice.setData(res);

            }});
    }
};

var harmManage = {
    gridView : new RealGridJS.GridView("harmGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        harmManage.initGrid();
        harmManage.initDataProvider();
        harmManage.event();
    },
    initGrid : function(){
        harmManage.gridView.setDataSource(harmManage.dataProvider);
        harmManage.gridView.setStyles(harmGridHeadBaseInfo.realgrid.styles);
        harmManage.gridView.setDisplayOptions(harmGridHeadBaseInfo.realgrid.displayOptions);
        harmManage.gridView.setColumns(harmGridHeadBaseInfo.realgrid.columns);
        harmManage.gridView.setOptions(harmGridHeadBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        harmManage.dataProvider.setFields(harmGridHeadBaseInfo.dataProvider.fields);
        harmManage.dataProvider.setOptions(harmGridHeadBaseInfo.dataProvider.options);
    },
    event : function() {
        harmManage.gridView.onDataCellClicked = function(gridView, index) {

        };
    },
    setData : function(dataList) {
        harmManage.dataProvider.clearRows();
        harmManage.dataProvider.setRows(dataList);
        //검색결과 건수
        $('#harmListCount').html($.jUtil.comma(harmManage.gridView.getItemCount()));
    },
    // 검색
    search: function(){

        CommonAjax.basic({url:'/home/getHarmProductList.json?' + $('#searchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                harmManage.setData(res);

            }});


    }

};



