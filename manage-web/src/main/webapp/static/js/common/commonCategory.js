/**
 * 공통 - 카테고리
 */

var commonCategory = {
	init : function() {
		this.event();
	},
	event : function() {
	},
	/**
	 * 카테고리 셀렉트박스 생성
	 *
	 * @param depth
	 * @param parentCateCd
	 * @param cateCd
	 * @param selector
     */
	setCategorySelectBox : function(depth, parentCateCd, cateCd, selector, callBack, dispYnArr , exceptHdnCate) {
		$(selector).html('<option value="">'+$(selector).data('default')+'</option>');
		if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
			CommonAjax.basic({
				 url: '/common/category/getCategoryForSelectBox.json',
				 data: {
					 depth: depth,
					 parentCateCd: parentCateCd,
                     dispYnArr:dispYnArr //노출여부(값 넘기지 않으면 디폴트 Y#A(전체노출&어드민노출)
				 },
				 method: 'get',
				 callbackFunc: function (resData) {
					 $.each(resData, function (idx, val) {
						 var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
						 $(selector).append('<option value="' + val.cateCd + '" ' + selected  + '>' + val.cateNm + '</option>');
					 });
					 $(selector).css('-webkit-padding-end','30px');

                     if (typeof callBack == 'function') {
                         var callData = {
                             depth:depth,
                             selector : selector,
                             data:resData
                         };
                         callBack(callData);
                     }
				 }
			});
		}
	},
	/**
	 * 카테고리 select box change 이벤트
	 *
	 * @param depth 해당 카테고리 뎁스
	 * @param prefix 셀렉트박스 아이디 prefix
	 */
	changeCategorySelectBox : function(depth, prefix, dispYnArr) {
		var cateCd = $('#'+prefix+depth).val();
		this.setCategorySelectBox(depth+1, cateCd, '', $('#'+prefix+(depth+1)), null, dispYnArr);
		$('#'+prefix+(depth+1)).change();
	},

	/**
	 * 카테고리 select box change 이벤트 ( 커스텀 )
	 *
	 * @param depth 해당 카테고리 뎁스
	 * @param prefix 셀렉트박스 아이디 prefix
	 * @param afterPrefix 셀렉트박스 아이디 afterPrefix ( 대 카테고리 > 중 카테고리 )
	 */
	changeCategoryCustomSelectBox : function(depth, prefix, afterPrefix, dispYnArr) {
		var cateCd = $('#'+prefix).val();
		this.setCategorySelectBox(depth+1, cateCd, '', $('#'+afterPrefix), null, dispYnArr);
		$('#'+afterPrefix).change();
	},
    /**
	 * 카테고리 select box init
	 *
     * @param depth 로딩할 카테고리 뎁스
     * @param prefix 셀렉트박스 아이디 prefix
     */
    initCategorySelectBox : function(depth, prefix, dispYnArr , exceptHdnCate) {

		for(var i=1; i<depth+1; i++) {
            this.setCategorySelectBox(i,'','',$('#'+prefix+i), null, dispYnArr , exceptHdnCate);

		}
		switch(depth) {
			case 4 :
                $('#'+prefix+'3').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(3, prefix, dispYnArr);
                });
            case 3 :
                $('#'+prefix+'2').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(2, prefix, dispYnArr);
                });
            case 2 :
                $('#'+prefix+'1').unbind('change').on('change', function() {
                    commonCategory.changeCategorySelectBox(1, prefix, dispYnArr);
                });
		}
    }
};

$(document).ready(function(){
	commonCategory.init();
});