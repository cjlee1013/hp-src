const UIUtil = {
    /**
     * jquery dialog innerPopup 열기
     * @param url(필수) - 팝업 url
     * @param width(필수) - 너비
     * @param height(필수) - 높이
     * @param title(선택) - 제목
     * @param bgColor(선택) - 배경색
     * @param dragYn(선텍) - 드래그 가능 여부
     * @param opacity(선텍) - 배경 투과율(0~1까지 실수)
     */
    openInnerPopup : function (url, width, height, title, bgColor, dragYn, opacity){
        if(url == undefined || url == null) {
            alert("팝업 url이 입력되지 않았습니다");
            return;
        }

        width = parseInt(width);
        if(isNaN(width) == true){
            alert("너비를 제대로 입력하세요.");
            return;
        }

        height = parseInt(height);
        if(isNaN(height) == true){
            alert("높이를 제대로 입력하세요.");
            return;
        }

        if(title == undefined || title == null) {
            title = "";
        }
        if(bgColor == undefined || bgColor == null) {
            bgColor = "black";
        }
        if(dragYn == undefined || dragYn == "") {
            dragYn = "Y";
        }
        if(opacity == undefined || opacity == "") {
            opacity = "0.6";
        }

        //다이어로그 html을 초기화하고 재생성
        if($("div[role='dialog']").length > 0) {
            $("div[role='dialog']").html("");
        }

        var dialog =
            $('<div></div>')
            .html('<iframe style="border:0px;" src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                width: width,
                height: height,
                title: title,
                draggable : dragYn
            });
        dialog.dialog('open');

        //다이어로그 css 변경
        $('.ui-widget-overlay').css('background', bgColor);
        $('.ui-widget-overlay').css('opacity', opacity);

        if(title == "") {
            $(".ui-dialog-titlebar").hide();
        }

        //배경화면 클릭시 팝업 닫힘
        $('.ui-widget-overlay').bind("click" ,function () {
            $("div[role='dialog']").html("");
        });

    },
    /**
     * jquery dialog innerPopup 닫기
     */
    closeInnerPopup :function (){
       parent.$("div[role='dialog']").html("");
    },

    /**
     * Tab Window 열기
     * @param url(필수) - 팝업 url
     * @param tabName(필수) - 탭 네임
     * @param tabTitle(필수) - 탭 제목
     * @param width(필수) - 탭 너비
     */
    addTabWindow : function(url, tabId, tabName, width) {
        //탭메뉴 추가.
        var subTabber = parent.Tabbar;

        //탭이 이미 있다면 데이터 갱신 후 활성화
        if(subTabber.getAllTabs().indexOf(tabId) != -1) {
            subTabber.tabs(tabId).attachURL(url);
            subTabber.tabs(tabId).setActive();
        }
        else {
            subTabber.addTab(tabId, tabName, width, null, true, true);
            subTabber.tabs(tabId).attachURL(url);
            subTabber.enableAutoReSize();
        }
    },
    /**
     * 부모 탭 바로 옆(우측)에 Tab Window 열기
     * @param url(필수) 팝업 url
     * @param tabId(필수) 탭 아이디
     * @param tabName(필수) 탭 네임
     * @param width(필수) 탭 너비
     * @param parentTabId 부모 탭 아이디
     */
    addTabWindowBesideParent : function(url, tabId, tabName, width, parentTabId) {
        //탭메뉴
        let subTabber = parent.Tabbar;
        let parentTabindex = 0;

        //부모탭 정보 확인
        if (!UIUtil.existsTab(parentTabId)){
            alert("부모 탭(tabId:" + parentTabId + ")이 존재하지 않습니다.");
            return;
        }
        //부모탭의 포지션 정보 가져오기
        subTabber.forEachTab(function (tab) {
            if (tab.getId() === parentTabId) {
                parentTabindex = tab.getIndex();
            }
        });
        //탭이 이미 있다면 데이터 갱신 후 활성화
        if (UIUtil.existsTab(tabId)) {
            subTabber.tabs(tabId).attachURL(url);
            subTabber.tabs(tabId).setActive();
        }else {
            subTabber.addTab(tabId, tabName, width, parentTabindex + 1, true, true);
            subTabber.tabs(tabId).attachURL(url);
            subTabber.enableAutoReSize();
        }
    },
    /**
     * Tab Window 닫기
     * @param tabName(필수) - 탭 네임
     */
    RemoveTabWindow : function(tabId) {
        var subTabber = parent.Tabbar;
        subTabber._removeTab(tabId);
    },

    /**
     * 탭아이디로 탭바에 존재여부를 체크합니다.
     * @param tabId 탭아이디
     * @returns {boolean}
     */
    existsTab: function(tabId) {
        let tabs = parent.Tabbar.getAllTabs();
        return tabs.indexOf(tabId) !== -1;
    },
};