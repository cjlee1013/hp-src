/*!
 * jQuery Validation Plugin v1.16.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2016 Jörn Zaefferer
 * Released under the MIT license
 */
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define(["jquery", "../core/jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

( function() {

	function stripHtml( value ) {

		// Remove html tags and space chars
		return value.replace( /<.[^<>]*?>/g, " " ).replace( /&nbsp;|&#160;/gi, " " )

		// Remove punctuation
		.replace( /[.(),;:!?%#$'\"_+=\/\-“”’]*/g, "" );
	}

	$.validator.addMethod( "maxWords", function( value, element, params ) {
		return this.optional( element ) || stripHtml( value ).match( /\b\w+\b/g ).length <= params;
	}, $.validator.format( "Please enter {0} words or less." ) );

	$.validator.addMethod( "minWords", function( value, element, params ) {
		return this.optional( element ) || stripHtml( value ).match( /\b\w+\b/g ).length >= params;
	}, $.validator.format( "Please enter at least {0} words." ) );

	$.validator.addMethod( "rangeWords", function( value, element, params ) {
		var valueStripped = stripHtml( value ),
			regex = /\b\w+\b/g;
		return this.optional( element ) || valueStripped.match( regex ).length >= params[ 0 ] && valueStripped.match( regex ).length <= params[ 1 ];
	}, $.validator.format( "Please enter between {0} and {1} words." ) );

}() );

// Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
$.validator.addMethod( "extension", function( value, element, param ) {
	param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
	return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
}, $.validator.format( "Please enter a value with a valid extension." ) );

$.validator.addMethod( "integer", function( value, element ) {
	return this.optional( element ) || /^-?\d+$/.test( value );
}, "A positive or negative non-decimal number please" );

$.validator.addMethod( "ipv4", function( value, element ) {
	return this.optional( element ) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test( value );
}, "Please enter a valid IP v4 address." );

$.validator.addMethod( "ipv6", function( value, element ) {
	return this.optional( element ) || /^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test( value );
}, "Please enter a valid IP v6 address." );

$.validator.addMethod( "notEqualTo", function( value, element, param ) {
	return this.optional( element ) || !$.validator.methods.equalTo.call( this, value, element, param );
}, "Please enter a different value, values must not be the same." );

$.validator.addMethod( "nowhitespace", function( value, element ) {
	return this.optional( element ) || /^\S+$/i.test( value );
}, "입력란에 공백은 제거해주시기 바랍니다." );

/**
 * RegExp 형식과 입력 값이 일치하면 true를 리턴함
 *
 *  ex1) $.validator.methods.pattern("AR1004",element,/^AR\d{4}$/)
 *    결과 : true
 *  ex2) $.validator.methods.pattern("BR1004",element,/^AR\d{4}$/)
 *    결과 : false
 *
 * @return Boolean
 */
$.validator.addMethod( "pattern", function( value, element, param ) {
	if ( this.optional( element ) ) {
		return true;
	}
	if ( typeof param === "string" ) {
		param = new RegExp( "^(?:" + param + ")$" );
	}
	return param.test( value );
}, "입력 값은 잘못된 형식입니다." );

/**
 * 한글만 입력
 */
$.validator.addMethod("kor", function(value,element){
return this.optional( element ) || /^[가-힣]+$/.test(value);
}, "한글만 입력가능합니다.");

/**
 * 영문만 입력
 */
$.validator.addMethod("eng", function(value,element){
	return this.optional( element ) || /^[a-zA-Z]+$/.test(value);
}, "영문만 입력가능합니다.");

/**
 * 한글 영문 입력
 */
$.validator.addMethod("kor_eng", function(value,element){
	return this.optional( element ) || /^[가-힣a-zA-Z]+$/.test(value);
}, "한글과 영문만 입력가능합니다.");

/**
 * 소수점 첫째자리 정규식 체크
 */
$.validator.addMethod(
	'tenthsPlaceValue', function (value, element) {
        return this.optional( element ) || /^\d*[.]\d{1}$/.test(value);
	}, '소수점 첫째자리 까지만 입력가능합니다.');

/**
 * 소수점 둘째자리 정규식 체크
 */
$.validator.addMethod(
	'hundredthsPlaceValue', function (value, element) {
        return this.optional( element ) || /^\d*[.]\d{2}$/.test(value);
	}, '소수점 둘째자리 까지만 입력가능합니다.');

/**
 * byte 체크(한글 3byte, 영문 / 숫자는 1byte)
 *
 * @param 입력 가능한 byte 크기
 */
( function() {
	/**
	 * byteLength를 구하는 function
	 * @see http://programmingsummaries.tistory.com/239
	 */
	function utf8ByteLength(s,b,i,c){
		for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
		return b;
	}

	$.validator.addMethod(
		"byteLength", function(value, element, param) {
			var result = true;

			if(utf8ByteLength(value) > param){
				result = false;
			}
			return this.optional(element) || result;
		}, $.validator.format("최대 byte값을 넘었습니다. {0}byte 이하로 입력하세요."));
}() );

/**
 * 하이픈을 포함한 휴대폰 번호체크
 */
$.validator.addMethod('mobile', function(value, element){
	return this.optional(element) || /^01([0|1|6|7|8|9]?)-(\d{3,4})-(\d{4})$/.test(value);
}, "올바른 휴대폰 번호 형식이 아닙니다");

/**
 * 입력허용 문자열 체크
 */
$.validator.addMethod("allowInput", function(value,element,param){
	if ( this.optional( element ) ) {
		return true;
	}
	if (!param || param.length < 1) {
		return false;
	}
	var regx = '';
	var msg = new Array();
	for(var i =0; i < param.length; i++) {
		switch (param[i].toUpperCase()) {
			case 'NUM':
				msg.push('숫자');
				regx += '0-9'; break;
			case 'ENG':
				msg.push('영문');
				regx += 'a-zA-Z'; break;
			case 'KOR':
				msg.push('한글');
				regx += 'ㄱ-ㅎ가-힣'; break;
			default:
				return false; break;
		}
	}
	if (!new RegExp('^[' + regx + '\\s]+$').test(this.elementValue( element ))) {
		var errors = {};
		errors[element.name] =  msg.join(', ') + ' 외의 문자는 입력할 수 없습니다.';
		this.showErrors(errors);
	}
	return true;
}, "허용되지 않은 문자는 입력할 수 없습니다.");


}));