/**
 * 쿠폰조회 공통 팝업
 */
$(document).ready(function() {
    couponPopGrid.init();
    couponPop.init();
    CommonAjaxBlockUI.global();
});

// 쿠폰조회 공통팝업 그리드
var couponPopGrid = {
    gridView : new RealGridJS.GridView("couponPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        couponPopGrid.initCouponPopGrid();
        couponPopGrid.initDataProvider();
        couponPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initCouponPopGrid : function() {
        couponPopGrid.gridView.setDataSource(couponPopGrid.dataProvider);
        couponPopGrid.gridView.setStyles(couponPopGridBaseInfo.realgrid.styles);
        couponPopGrid.gridView.setDisplayOptions(couponPopGridBaseInfo.realgrid.displayOptions);
        couponPopGrid.gridView.setColumns(couponPopGridBaseInfo.realgrid.columns);
        couponPopGrid.gridView.setOptions(couponPopGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(couponPopGrid.gridView, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(couponPopGrid.gridView, "couponType", couponTypeJson);
        setColumnLookupOptionForJson(couponPopGrid.gridView, "status", couponStatusJson);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        couponPopGrid.dataProvider.setFields(couponPopGridBaseInfo.dataProvider.fields);
        couponPopGrid.dataProvider.setOptions(couponPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        couponPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        couponPopGrid.dataProvider.clearRows();
        couponPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        let checkedRows = couponPopGrid.gridView.getCheckedRows(true);
        let couponList = new Array();

        checkedRows.forEach(function (_row) {
            let _data = this.dataProvider.getJsonRow(_row);
            let dataObj = new Object();

            dataObj.couponNo = _data.couponNo;
            dataObj.storeType = _data.storeType;
            dataObj.manageCouponNm = _data.manageCouponNm;
            dataObj.displayCouponNm = _data.displayCouponNm;
            dataObj.applySystem = _data.applySystem;
            dataObj.couponType = _data.couponType;
            dataObj.status = _data.status;
            dataObj.discount = _data.discount;

            couponList.push(dataObj);
        }, this);

        return couponList;
    },
};

// 쿠폰조회
var couponPop = {
    /**
     * 초기화
     */
    init : function() {
        couponPop.event();
        couponPop.couponPopFormReset();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(couponPop.searchCouponList);
        $("#schResetBtn").bindClick(couponPop.couponPopFormReset);
        $("#selectBtn").bindClick(couponPop.checkGridRow);

        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                couponPop.searchCouponList();
            }
        });
    },
    /**
     * 쿠폰조회 공통팝업 form 초기화
     */
    couponPopFormReset : function() {
        $("#couponPopForm").resetForm();

        couponPop.initCalendarDate("-1m");

        // 입력값이 따른 화면 컨트롤
        couponPop.viewControl();
    },
    initCalendarDate : function (flag) {
        couponPop.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");

        couponPop.setDate.setStartDate(flag);
        couponPop.setDate.setEndDate(0);

        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    /**
     * requsetParam에 따른 화면 컨트롤
     */
    viewControl : function() {
        // 쿠폰번호가 있을경우 쿠폰번호 입력 후 수정 못하도록
        if (!$.jUtil.isEmpty(couponNo)) {
            $("#schType").val("COUPONNO");
            $("#schValue").val(couponNo).prop("readOnly", true);
        }
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        // 관리쿠폰명 자리수 확인
        if ($("#schType").val() == "COUPONNM" && $("#schValue").val().trim().length < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            return false;
        }

        return true;
    },
    /**
     * 쿠폰 조회
     */
    searchCouponList : function () {
        // 조회 조건 validation check
        if (couponPop.validCheck()) {
            let couponPopForm = $("#couponPopForm").serialize();

            CommonAjax.basic({
                url: "/common/getCouponPopList.json"
                , data: couponPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    couponPopGrid.setData(res);
                    $("#couponCnt").html(couponPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        let checkedRows = couponPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("쿠폰을 선택해 주세요.");
            return false;
        }

        let checkCouponList = couponPopGrid.checkData();
        eval("opener." + callBackScript + "(checkCouponList);");

        self.close();
    },
};