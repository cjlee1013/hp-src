/**
 * 기획전, 이벤트조회 공통 팝업
 */
$(document).ready(function() {
    promoPopGrid.init();
    promoPop.init();
    CommonAjaxBlockUI.global();
});

// 기획전, 이벤트조회 공통팝업 그리드
var promoPopGrid = {
    gridView : new RealGridJS.GridView("promoPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        promoPopGrid.initPromoPopGrid();
        promoPopGrid.initDataProvider();
        promoPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initPromoPopGrid : function() {
        promoPopGrid.gridView.setDataSource(promoPopGrid.dataProvider);
        promoPopGrid.gridView.setStyles(promoPopGridBaseInfo.realgrid.styles);
        promoPopGrid.gridView.setDisplayOptions(promoPopGridBaseInfo.realgrid.displayOptions);
        promoPopGrid.gridView.setColumns(promoPopGridBaseInfo.realgrid.columns);
        promoPopGrid.gridView.setOptions(promoPopGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(promoPopGrid.gridView, "useYn", useYnJson);
        setColumnLookupOptionForJson(promoPopGrid.gridView, "dispYn", dispYnJson);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        promoPopGrid.dataProvider.setFields(promoPopGridBaseInfo.dataProvider.fields);
        promoPopGrid.dataProvider.setOptions(promoPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        promoPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        promoPopGrid.dataProvider.clearRows();
        promoPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        let checkedRows = promoPopGrid.gridView.getCheckedRows(true);
        let promoList = new Array();

        checkedRows.forEach(function (_row) {
            let _data = this.dataProvider.getJsonRow(_row);
            let dataObj = new Object();

            dataObj.promoNo = _data.promoNo;
            dataObj.promoNm = _data.mngPromoNm;

            promoList.push(dataObj);
        }, this);

        return promoList;
    },
};

// 기획전, 이벤트조회
var promoPop = {
    promoPopTitle : null,

    /**
     * 초기화
     */
    init : function() {
        promoPop.event();
        promoPop.promoPopFormReset();

        if(promoType == "EXH") {
            promoPop.promoPopTitle = "기획전조회";
        } else if (promoType == 'EVENT') {
            promoPop.promoPopTitle = "이벤트조회";
        }

        $("#titlePop").html(promoPop.promoPopTitle);
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(promoPop.searchPromoList);
        $("#schResetBtn").bindClick(promoPop.promoPopFormReset);
        $("#selectBtn").bindClick(promoPop.checkGridRow);

        // 사이트 구분 변경시
        $("input:radio[name='schSiteType']").bindChange(promoPop.view.changeSiteType);

        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                promoPop.searchPromoList();
            }
        });
    },
    /**
     * 기획전, 이벤트조회 공통팝업 form 초기화
     */
    promoPopFormReset : function() {
        $("#promoPopForm").resetForm();

        promoPop.initCalendarDate("-1w");

        // 입력값이 따른 화면 컨트롤
        promoPop.view.initView();
    },
    initCalendarDate : function (flag) {
        promoPop.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");

        promoPop.setDate.setStartDate(flag);
        promoPop.setDate.setEndDate(0);

        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        // 프로모션명 확인
        if ($("#schType").val() == "PROMONM" && $("#schValue").val().trim().length < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            return false;
        }

        return true;
    },
    /**
     * 기획전, 이벤트 조회
     */
    searchPromoList : function () {
        // 조회 조건 validation check
        if (promoPop.validCheck()) {
            let promoPopForm = $("#promoPopForm").serializeArray();
            promoPopForm.push({name:"promoType", value:promoType});

            CommonAjax.basic({
                url: "/common/getPromoPopList.json"
                , data: promoPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    promoPopGrid.setData(res);
                    $("#promoCnt").html(promoPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        let checkedRows = promoPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("기획전, 이벤트을 선택해 주세요.");
            return false;
        }

        let checkPromoList = promoPopGrid.checkData();
        eval("opener." + callBackScript + "(checkPromoList);");

        self.close();
    },
};

promoPop.view = {
    /**
     * requsetParam에 따른 화면 컨트롤
     */
    initView : function() {
        if (!$.jUtil.isEmpty(siteType)) {
            $(".ui.radio").find("input:radio").each(function() {
                if ($(this).val() == siteType) {
                    $(this).prop("checked", true).trigger("change");
                } else {
                    $(this).prop("disabled", true);
                }
            });
        } else {
            promoPop.view.addSelectOption("#schStoreType", $("input:radio[name='schSiteType']:checked").val());
        }

        if (!$.jUtil.isEmpty(storeType)) {
            $("#schStoreType").val("storeType");
        }

        // 기획전, 이벤트번호가 있을경우 기획전, 이벤트번호 입력
        if (!$.jUtil.isEmpty(promoNo)) {
            $("#schType").val("PROMONO");
            $("#schValue").val(promoNo);
        }
    }
    /**
     * 사이트구분 변경 시 상품분류의 점포유형 select box 컨트롤
     * @param obj
     */
    , changeSiteType : function (obj) {
        var siteType = $(obj).val();

        promoPop.view.addSelectOption("#schStoreType", siteType);
    }
    /**
     * select box option 삭제 후 조건에 맞는 option 추가
     * @param obj
     * @param siteType
     * @returns {boolean}
     */
    , addSelectOption : function (obj, siteType) {
        $(obj).find("option").remove();

        var jsonData = storeTypeJson;

        if (jsonData == "") return false;

        $.each(jsonData, function (key, code) {
            var chkOption = 0;
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            var codeRef2 = code.ref2;

            $(obj).find("option").each(function () {
                if ($(this).val() == codeCd) {
                    chkOption++;
                }
            });

            if (chkOption == 0) {
                if (codeRef2 == siteType){
                    $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                }
            }
        });
    }
};