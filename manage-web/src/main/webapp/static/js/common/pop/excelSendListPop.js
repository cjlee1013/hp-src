/**
 * 메세지관리 > 발송관리 > 메시지 수동발송관리 > 엑셀 업로드 팝업
 */
var excelSendListPop = {
    save: function () {
        if (!$('#excels').val()) {
            alert("파일이 없습니다.");
            return;
        }

        jQuery.ajax({
            url: "/message/admin/manual/extractExcel.json",
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: new FormData($('#excelSendListPopForm')[0]),
            success: function (res) {
                if ($.jUtil.isNotEmpty(res.message)) {
                    alert("업로드에 실패했습니다. (" + res.message + ")");
                    self.close();
                }
                if (res.list.length > 0) {
                    eval('opener.' + callBackScript + '(res.list);');
                    self.close();
                } else {
                    alert("추출 된 데이터가 없습니다.");
                    self.close();
                }
            }
        });
    },

    cancel: function () {
        self.close();
    }
}