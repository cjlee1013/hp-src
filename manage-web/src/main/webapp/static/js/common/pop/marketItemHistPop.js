/**
 * 외부연동 상품 연동이력 팝업
 */
$(document).ready(function() {
    CommonAjaxBlockUI.global();
    marketItemHistListPopGrid.init();
    marketItemHistPop.init();

});

//realgrid
var marketItemHistListPopGrid = {
    gridView : new RealGridJS.GridView("marketItemHistListPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        marketItemHistListPopGrid.initGrid();
        marketItemHistListPopGrid.initDataProvider();
        marketItemHistListPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initGrid : function() {
        marketItemHistListPopGrid.gridView.setDataSource(marketItemHistListPopGrid.dataProvider);
        marketItemHistListPopGrid.gridView.setStyles(marketItemHistListPopGridBaseInfo.realgrid.styles);
        marketItemHistListPopGrid.gridView.setDisplayOptions(marketItemHistListPopGridBaseInfo.realgrid.displayOptions);
        marketItemHistListPopGrid.gridView.setColumns(marketItemHistListPopGridBaseInfo.realgrid.columns);
        marketItemHistListPopGrid.gridView.setOptions(marketItemHistListPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        marketItemHistListPopGrid.dataProvider.setFields(marketItemHistListPopGridBaseInfo.dataProvider.fields);
        marketItemHistListPopGrid.dataProvider.setOptions(marketItemHistListPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        marketItemHistListPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            //$('#histDetail').hide();
            marketItemHistPop.setDetail(index.dataRow);
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        marketItemHistListPopGrid.dataProvider.clearRows();
        marketItemHistListPopGrid.dataProvider.setRows(dataList);
    },

};

// 외부연동 이력팝업
var marketItemHistPop = {
    laseSeq: '',
    pageNo: 0,
    /**
     * 초기화
     */
    init: function () {
        marketItemHistPop.event();
        marketItemHistPop.getHist();
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {

        $('#moreHist').on('click', function () {
            marketItemHistPop.getHist(true);
        });
    },

    getHist: function (isMore) {
        CommonAjax.basic({
            url: "/item/market/getMarketItemSpecHistList.json?itemNo=" + itemNo
                + "&partnerId=" + partnerId + "&pageNo="
                + marketItemHistPop.pageNo,
            method: 'get',
            callbackFunc: function (res) {
                if (res.length > 0) {
                    if (isMore) {
                        marketItemHistListPopGrid.dataProvider.addRows(res);
                    } else {
                        marketItemHistListPopGrid.setData(res);
                    }

                    if (res.length > 10) {
                        marketItemHistPop.pageNo++;
                        marketItemHistPop.deleteLastGrid();
                    } else {
                        $('#moreHist').hide();
                        let rowDataJson = marketItemHistListPopGrid.dataProvider.getJsonRow(
                            marketItemHistListPopGrid.gridView.getItemCount() - 1);
                        marketItemHistPop.lastSeq = rowDataJson.id;
                    }
                }
            }
        });
    },

    deleteLastGrid: function () {
        let lastIdx = marketItemHistListPopGrid.gridView.getItemCount() - 1;
        let rowDataJson = marketItemHistListPopGrid.dataProvider.getJsonRow( lastIdx);

        marketItemHistPop.lastSeq = rowDataJson.id;
        marketItemHistListPopGrid.dataProvider.removeRow(lastIdx);
    },

    setDetail: function (rowId) {

        $(".detailArea").show();
        let rowDataJson = marketItemHistListPopGrid.dataProvider.getJsonRow(rowId);

        //spec이 없을 경우 request object를 그냥 보여주도록 합니다.
        $('#specText').text($.jUtil.isEmpty(rowDataJson.spec) ? rowDataJson.request : rowDataJson.spec);
        $('#responseText').text(rowDataJson.response);
    },
}
