/**
 * 사은행사 조회 공통 팝업
 */
$(document).ready(function() {
    ersPromoPopGrid.init();
    ersPromoPop.init();
    CommonAjaxBlockUI.global();
});

// 사은행사 조회 공통팝업 그리드
var ersPromoPopGrid = {
    gridView : new RealGridJS.GridView("ersPromoPopGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        ersPromoPopGrid.initPromoPopGrid();
        ersPromoPopGrid.initDataProvider();
        ersPromoPopGrid.event();
    }
    /**
     * 조회 그리드 설정
     */
    , initPromoPopGrid : function() {
        ersPromoPopGrid.gridView.setDataSource(ersPromoPopGrid.dataProvider);
        ersPromoPopGrid.gridView.setStyles(ersPromoPopGridBaseInfo.realgrid.styles);
        ersPromoPopGrid.gridView.setDisplayOptions(ersPromoPopGridBaseInfo.realgrid.displayOptions);
        ersPromoPopGrid.gridView.setColumns(ersPromoPopGridBaseInfo.realgrid.columns);
        ersPromoPopGrid.gridView.setOptions(ersPromoPopGridBaseInfo.realgrid.options);
    }
    /**
     * 그리드 데이터 설정
     */
    , initDataProvider : function() {
        ersPromoPopGrid.dataProvider.setFields(ersPromoPopGridBaseInfo.dataProvider.fields);
        ersPromoPopGrid.dataProvider.setOptions(ersPromoPopGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        ersPromoPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    }
    /**
     * 조회된 데이터 그리드에 적용
     */
    , setData : function(dataList) {
        ersPromoPopGrid.dataProvider.clearRows();
        ersPromoPopGrid.dataProvider.setRows(dataList);
    }
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    , checkData : function () {
        var checkedRows = ersPromoPopGrid.gridView.getCheckedRows(true);
        var promoList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.eventCd = _data.eventCd;
            dataObj.eventNm = _data.eventNm;

            promoList.push(dataObj);
        }, this);

        return promoList;
    }
};

// 사은행사 조회
var ersPromoPop = {
    ersPromoPopTitle : null

    /**
     * 초기화
     */
    , init : function() {
        ersPromoPop.event();
        ersPromoPop.ersPromoPopFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , event : function() {
        $("#schBtn").bindClick(ersPromoPop.searchPromoList);
        $("#schResetBtn").bindClick(ersPromoPop.ersPromoPopFormReset);
        $("#selectBtn").bindClick(ersPromoPop.checkGridRow);

        // 사이트 구분 변경시
        // $("input:radio[name='schSiteType']").bindChange(ersPromoPop.view.changeSiteType);

        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                ersPromoPop.searchPromoList();
            }
        });
    }
    /**
     * 사은행사 조회 공통팝업 form 초기화
     */
    , ersPromoPopFormReset : function() {
        $("#ersPromoPopForm").resetForm();

        ersPromoPop.initCalendarDate("-2w");

        // 입력값이 따른 화면 컨트롤
        // ersPromoPop.view.initView();
    }
    , initCalendarDate : function (flag) {
        ersPromoPop.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");

        ersPromoPop.setDate.setStartDate(flag);
        ersPromoPop.setDate.setEndDate(0);

        // $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    }
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    , validCheck : function() {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            initCalendarDate(ersPromoSearch, "schStartDt", "schEndDt", "SEARCH", "-1w", "0");
            return false;
        }

        // 3개월 이내 검색
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType == "EVENTNM" && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }

        return true;
    }
    /**
     * 사은행사  조회
     */
    , searchPromoList : function () {
        // 조회 조건 validation check
        if (ersPromoPop.validCheck()) {
            var ersPromoPopForm = $("#ersPromoPopForm").serializeArray();

            CommonAjax.basic({
                url: "/common/getErsPromoPopList.json"
                , data: ersPromoPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    ersPromoPopGrid.setData(res);
                    $("#ersPromoCnt").html(ersPromoPopGrid.dataProvider.getRowCount());
                }
            });
        }
    }
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    , checkGridRow : function () {
        var checkedRows = ersPromoPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("사은행사를 선택해 주세요.");
            return false;
        }

        var checkPromoList = ersPromoPopGrid.checkData();
        eval("opener." + callBackScript + "(checkPromoList);");

        self.close();
    }
};