/**
 * 결제수단(카드) 선택 팝업
 */
$(document).ready(function() {
    cardPopGrid.init();
    cardPop.init();
    CommonAjaxBlockUI.global();
});

var cardPopGrid = {
    gridView : new RealGridJS.GridView("cardPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        cardPopGrid.initCardPopGrid();
        cardPopGrid.initDataProvider();
        cardPopGrid.event();

        if (multiYn == 'N') {
            this.gridView.setCheckBar({exclusive : true});
        } else {
            this.gridView.setCheckBar({exclusive : false});
        }

        if (groupCdNeedYn == "Y") {
            $("#divGroupCd").show();
        } else {
            this.gridView.setColumnProperty("groupCd", "visible", false);
        }
    },
    /**
     * 조회 그리드 설정
     */
    initCardPopGrid : function() {
        cardPopGrid.gridView.setDataSource(cardPopGrid.dataProvider);
        cardPopGrid.gridView.setStyles(cardPopGridBaseInfo.realgrid.styles);
        cardPopGrid.gridView.setDisplayOptions(cardPopGridBaseInfo.realgrid.displayOptions);
        cardPopGrid.gridView.setColumns(cardPopGridBaseInfo.realgrid.columns);
        cardPopGrid.gridView.setOptions(cardPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        cardPopGrid.dataProvider.setFields(cardPopGridBaseInfo.dataProvider.fields);
        cardPopGrid.dataProvider.setOptions(cardPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        cardPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };

        $("#groupCd").allowInput("keyup", ["NUM"], $(this).attr("id"));
    },
    /**
     * 넘어온 데이터 체크선택 셋팅
     */
    getDataItemCheck : function(dataList) {
        if ($.jUtil.isEmpty(dataList)) {
            return;
        }

        console.log(dataList);

        let getPaymentList = JSON.parse(dataList);
        console.log(allPaymentList);

        if (getPaymentList != "" && allPaymentList != "") {
            getPaymentList.forEach(function (value, index, array1) {
                /*
                let option = {
                    startIndex: 0,
                    fields: ["methodCd"],
                    values: [value.methodCd]
                };
                console.log(index);
                console.log(value);

                var resSearch = cardPopGrid.gridView.checkItem(cardPopGrid.gridView.searchItem(option));
                */

                var res = cardPopGrid.dataProvider.searchData({fields: ["methodCd"], value: value.methodCd, wrap: true});
                if (res) {
                    cardPopGrid.gridView.checkItem(res.dataRow);
                    cardPopGrid.dataProvider.setValue(res.dataRow, "groupCd",  value.groupCd);
                }
            });
    }
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        cardPopGrid.dataProvider.clearRows();
        cardPopGrid.dataProvider.setRows(JSON.parse(dataList));

        cardPopGrid.getDataItemCheck(getPaymentList);

        if (setYn == "N") {
            $("#selectBtn, #divGroupCd").hide();
            //cardPopGrid.gridView.setCheckable(false);
        }
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = cardPopGrid.gridView.getCheckedRows(true);
        var paymentList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.methodCd = _data.methodCd;
            dataObj.methodNm = _data.methodNm;
            dataObj.sortSeq = _data.sortSeq;
            dataObj.groupCd = _data.groupCd;

            paymentList.push(dataObj);
        }, this);

        return paymentList;
    }
};

// 카드코드 조회 팝업
var cardPop = {
    /**
     * 초기화
     */
    init : function() {
        cardPop.event();
        cardPop.cardPopFormReset();
        cardPopGrid.setData(allPaymentList);

        //그룹코드 입력된 카드 선택
        $("#checkYn").prop("checked", false);

        /*if (groupCdNeedYn == "Y") {
            $("#divGroupCd").show();
            cardPopGrid.gridView.setColumnProperty("groupCd", "visible", true);
        }*/
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#cardPopForm').resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(cardPop.searchList);
        $("#setGroupCdBtn").bindClick(cardPop.setGroupCdCol);
        $("#delGroupCdBtn").bindClick(cardPop.delGroupCdCol);
        $("#selectBtn").bindClick(cardPop.checkGridRow);
        $('#searchResetBtn').bindClick(cardPop.searchFormReset);
        $('#checkYn').bindChange(cardPop.checkExistGroupCd);

        $("#schType, #schKeyword").keyup(function (e) {
            if (e.keyCode == 13) {
                cardPop.searchList();
            }
        });
    },
    /**
     * 그룹코드 존재하는 카드코드의 그리드 선택
     * */
    checkExistGroupCd : function (obj) {
        var isChecked = $(obj).prop("checked");

        var checkedRows = cardPopGrid.gridView.getCheckedRows(true);
        cardPopGrid.gridView.checkItems(checkedRows, false);

        if (isChecked) {
            var gridDataList = cardPopGrid.dataProvider.getJsonRows(0, -1);
            gridDataList.forEach(function (value, index) {
                if ($.jUtil.isNotEmpty(value.groupCd)) {
                    cardPopGrid.gridView.checkItem(index);
                }
            });
        }
    },
    /**
     * 팝업 form 초기화
     */
    cardPopFormReset : function() {
        $("#cardPopForm").resetForm();
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 검색어 체크
        var schKeyword = $('#schKeyword').val();

        if (schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (schKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    /**
     * 선택된 행 그룹코드 컬럼 셋팅
     */
    setGroupCdCol : function () {
        var groupCd = $("#groupCd").val();

        if ($.jUtil.isEmpty(groupCd)) {
            alert("그룹코드를 입력하세요.");
            return;
        }

        if (groupCd.length < 2 || groupCd.length > 2 || !$.jUtil.isAllowInput(groupCd, ['NUM'])) {
            alert("그룹코드는 두자리 숫자만 입력이 가능합니다.");
            return;
        }

        var checkedRows = cardPopGrid.gridView.getCheckedRows(true);

        checkedRows.forEach(function (_row) {
            cardPopGrid.dataProvider.setValue(_row,"groupCd", groupCd);
        });

        //체크 클리어
        cardPopGrid.gridView.checkItems(checkedRows, false);

        //그룹코드 입력 클리어
        $("#groupCd").val("");
    },
    /**
     * 선택된 행 그룹코드 컬럼 클리어
     */
    delGroupCdCol : function () {
        var checkedRows = cardPopGrid.gridView.getCheckedRows(true);

        checkedRows.forEach(function (_row) {
            cardPopGrid.dataProvider.setValue(_row, "groupCd", "");
        });
    },
    /**
     * 카드리스트 조회
     */
    searchList : function () {
        if (cardPop.validCheck()) {
            var cardPopForm = $("#cardPopForm").serialize();

            CommonAjax.basic({
                url: '/common/cardPopList.json',
                data: cardPopForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    cardPopGrid.setData(res);
                    $("#searchCnt").html(cardPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 그리드 그룹코드 데이터 체크
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = cardPopGrid.checkData();

        if (multiYn != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;

        }

        if ($.jUtil.isEmpty(checkedRows)) {
            alert("카드코드를 선택해 주세요.");
            return false;

        }

        if (groupCdNeedYn == 'Y') {
            var isValidGroupCd = true;

            checkedRows.forEach(function (_row) {
                if ($.jUtil.isEmpty(_row.groupCd)) {
                    isValidGroupCd = false;
                    return false;
                }
            }, this);

            if (!isValidGroupCd) {
                alert("선택한 카드에 대한 그룹코드의 누락이 있습니다.\n확인 후 입력하세요.");
                return false;
            }
        }

        eval("opener." + callBackScript + "(checkedRows);");

        self.close();
    }
};