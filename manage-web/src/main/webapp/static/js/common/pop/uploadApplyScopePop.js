/**
 * 파일 업로드 팝업
 */
$(document).ready(function() {
    fileUploadPop.init();
    CommonAjaxBlockUI.global();
});

var fileUploadPop = {
    /**
     * init 이벤트
     */
    init: function() {
        fileUploadPop.bindingEvent();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#schUploadFile").bindClick(fileUploadPop.searchFile); // 파일찾기 버튼
        $("#selectBtn").bindClick(fileUploadPop.selectFile);        // 선택 버튼
        $("#uploadFile").bindChange(fileUploadPop.changeUploadFile);
        $("#downloadTemplate").bindClick(fileUploadPop.downloadTemplate);
    },
    changeUploadFile: function(obj) {
        var fileInfo = $(obj)[0].files[0];
        var fileExt = fileInfo.name.split(".");
        if(fileInfo == "" || fileInfo == null) {
            alert("파일을 선택해주세요.");
            return false;
        }

        if(fileExt.indexOf("xlsx") < 0) {
            alert("엑셀 (xlsx) 파일만 업로드 가능합니다.");
            return false;
        }

        $("#uploadFileNm").val($(obj)[0].files[0].name);
    },
    downloadTemplate: function() {
        var fileUrl = "";
        var applyScope = $("#applyScope").val();
        var promotionType = $("#promotionType").val();
        if (promotionType == "CARD_DISCOUNT" || promotionType == "RSV_SALE") {
            applyScope = promotionType;
        }

        switch (applyScope) {
            case "ITEM":
                fileUrl = "/static/templates/itemCoupon_template.xlsx";
                break;
            case "SELL":
                fileUrl = "/static/templates/sellerCoupon_template.xlsx";
                break;
            case "CARD_DISCOUNT":
                fileUrl = "/static/templates/cardSales_template.xlsx";
                break;
            case "RSV_SALE":
                fileUrl = "/static/templates/rsv_template.xlsx";
                break;
        }

        if(fileUrl != "") {
            location.href = fileUrl;
        } else {
            alert("양식 파일이 없습니다.");
            return false;
        }
    },
    /**
     * 파일찾기 버튼 클릭 (윈도우 파일 찾기 open)
     */
    searchFile: function() {
        $("#uploadFile").click();
    },
    /**
     * 선택한 파일 정보 불러오기
     */
    getFile: function(_obj) {
        var fileInfo= _obj[0].files[0];
        var fileNm  = fileInfo.name;
        $("#uploadFileNm").val(fileNm);
    },
    /**
     * 선택 버튼, callBack 함수 호출
     */
    selectFile: function() {
        if($("#uploadFile").val() == "") {
            alert("파일을 선택해주세요.");
            return false;
        }

        if(!confirm("선택하시겠습니까?")) return false;

        var applyScope = $("#applyScope").val();
        $("#uploadScopePopForm").ajaxForm({
            url: "/promotion/popup/upload/uploadScopePop.json",
            enctype: "multipart/form-data",
            method: "POST",
            success: function(res) {
                if (res.successYn == null) {
                    //if(res[0].error == applyScope) {
                    alert($("#popTitle").text().trim() + "에서 시스템 오류가 발생하였습니다.");
                } else if (res.successYn == 'N') {
                    if (res.failCount == 0) {
                        alert(CommonErrorMsg.uploadFailMsg);
                    } else {
                        //결과 팝업 호출
                        fileUploadPop.callUploadResultPop(res);
                    }
                } else {
                    eval("opener." + callBackScript + "(res.dataList);");
                }
                console.log(res);
                self.close();
            },
            error: function(resError){
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    } else {
                        alert("오류가 발생하여 완료되지 않았습니다.");
                    }
                } else {
                    alert("정상적으로 처리되지 않았습니다.");
                    console.log(resError);
                    self.close();
                }
                console.log(resError);
                self.close();
            }
        }).submit();
    },
    /**
     * 결과 팝업 호출
     * */
    callUploadResultPop: function(res) {
        var uri = "/promotion/popup/upload/uploadResultPop?uploadCount=" + res.uploadCount + "&successCount=" + res.successCount + "&failCount=" + res.failCount;
        windowPopupOpen(uri, "uploadResultPop", 600, 250);
    }
};