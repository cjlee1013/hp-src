//검색 키워드
let searchString;

const URI = '/common/getAddressListByIsland.json';
const ENTER_KEY_CODE = 13;

//첫번째 페이지
const FIRST_PAGE = 1;
//한 페이지 당 노출되는 게시물 건 수
const DEFAULT_PER_PAGE = 5;
//페이지 리스트에 노출되는 페이지 건 수
const DEFAULT_PAGE_SIZE = 10;

//검색결과 조회 제한 건수
const SEARCH_COUNT_MAX_LIMIT = 1000;
const SEARCH_COUNT_MIN_LIMIT = 51;

//에러 메세지
const ERROR_MESSAGE = "처리 중 오류가 발생하였습니다.\n잠시 후 다시 이용해 주시기 바랍니다.";

/**
 * event 관리
 */
const checkIsland = {
    init: function(){
        $('#zipSearch').focus();
        checkIsland.buttonEvent();
    },
    buttonEvent: function () {
        $('#searchAddr').on("click", '.zipcode, .address', function () {
            const no = $(this).attr('no');
            const zipcode = $('.zipcode[no=' + no + ']').html();
            const gibunAddr = $('.gibun_addr[no=' + no + ']').val();
            const roadAddr = $('.road_addr[no=' + no + ']').val();
            const gibunRep = $('.gibun_rep[no=' + no + ']').val();
            const islandType = $('.is_land_type[no=' + no + ']').val();

            let result = {
                'zipCode': zipcode,
                'gibunAddr': gibunAddr,
                'roadAddr': roadAddr,
                'gibunRep': gibunRep,
                'islandType' : islandType
            };
            let callBack = 'opener.' + callBackScript;
            eval(callBack)(result);
            checkIsland.closeIslandPop();
        });

        $("#zipCodeCancel").on("click", function () {
            checkIsland.closeIslandPop();
        });

        $('#zipSearch').on("keypress", function (e) {
            if (e.keyCode === ENTER_KEY_CODE) {
                checkIsland.searchZipCode();
            }
        });
        // 키 입력 시 label 제거
        $('#zipSearch').on("keydown", function () {
            $(this).siblings("label").fadeOut("fast");
        });

        $("#clearSearch").on("click", function () {
            $('#labelSearch').html('주소를 입력해주세요.');
            $('#zipSearch').val('').focus();
        });

        $('#zipSearch').on("focus", function () {
            $(this).siblings("label").fadeOut("fast");
        });

        $('#zipSearch').on("focusout", function () {
            if ($.jUtil.isEmpty($(this).val())) {
                $(this).siblings("label").fadeIn("fast");
            }
        });
    },
    /**
     * 검색 popup 닫기
     */
    closeIslandPop: function () {
        window.close();
    },
    /**
     * <p>에러 라벨 노출</p>
     * 검색창 하단에 에러 라벨을 노출합니다.
     * @param message 노출할 메세지
     */
    showErrorLabel: function(message) {
        $('#errorLabel').html(
            "<p class=\"text-red\" style='font-size : 12px ;'>" + message + "</p>").show();
     },
    /**
     * 주소 검색
     * @param page 페이지 번호
     * @returns {boolean} 검색결과 노출
     */
    searchZipCode: function(page){
    // 첫번째 페이지 여부 확인
    if (!page) {
        page = FIRST_PAGE;
        searchString = $("#zipSearch").val();
    }

    //에러 라벨 hide 및 초기화
    $('#errorLabel').hide().html('');

    //검색 키워드 유효성 체크
    if (!searchString) {
        checkIsland.showErrorLabel("주소를 입력해주세요.");
        $('#zipSearch').focus();
        return false;
    }

    //set data
    let listText = '';
    let data = {};
    data.page = page;
    data.perPage = DEFAULT_PER_PAGE;
    data.query = encodeURI(searchString);

    addressAjax(URI, 'get', data,
        function (response) {
            let listCnt = 0;

            if (response.data.totalCount > 0) {
                $.each(response.data.dataList, function (Index, row) {
                    listCnt++;
                    listText += '<dl>';
                    listText += '	<dt class="zipcode" no="r' + Index + '">'
                        + '<span class="zipcode">' + row.zipcode + '</span>'
                        + '<span class="island">' + checkIsland.islandTypeToText(row.islandType) + '</span></dt>';
                    listText += '	<dd>';
                    listText += '		<span class="road"><span class="w_name02">도로명</span></span>';
                    listText += '		<span class="island-address first" no="r' + Index + '">'
                        + checkIsland.addressToBoldBySearchKeyWord(row.roadAddrFullTxt, searchString) + '</span>';
                    listText += '	</dd>';
                    listText += '	<dd>';
                    listText += '		<span class="road"><span class="w_name02">지번</span></span>';
                    listText += '		<span class="island-address" no="r' + Index+ '">'
                        + checkIsland.addressToBoldBySearchKeyWord(row.gibunAddrFullTxt, searchString);
                    if ($.jUtil.isNotEmpty(row.gibunRepTxt)) {
                        listText += '<br>' + '[관련지번] '
                            + checkIsland.addressToBoldBySearchKeyWord(row.gibunRepTxt, searchString);
                    }
                    listText += '</span>';

                    listText += '		<input type="hidden" class="gibun_addr" no="r'
                        + Index + '" value="' + row.gibunAddrFullTxt + '">';
                    listText += '		<input type="hidden" class="road_addr" no="r'
                        + Index + '" value="' + row.roadAddrFullTxt + '">';
                    listText += '		<input type="hidden" class="gibun_rep" no="r'
                        + Index + '" value="' + row.gibunRepTxt + '">';
                    listText += '		<input type="hidden" class="is_land_type" no="r'
                        + Index + '" value="' + row.islandType + '">';
                    listText += '	</dd>';
                    listText += '</dl>';

                });

                //검색결과 노출영역
                $('#searchResult').hide().html('');
                if (response.data.totalCount > SEARCH_COUNT_MAX_LIMIT) {
                    $('#searchResult').html(
                        "<div class=\"result-text\"><p class=\"result\">검색결과가 많습니다.</p><p class=\"text\">검색주소를 조금 더 상세히 입력해주세요.</p></div>").show();
                } else if (response.data.totalCount < SEARCH_COUNT_MIN_LIMIT) {
                    $('#searchResult').html(
                        "<div class=\"result-text\"><p class=\"result\">총 <span class=\"text-red\" style=\"vertical-align:baseline;front-weight:bold\">"
                        + response.data.totalCount
                        + "</span>건</p></div>").show();
                } else {
                    $('#searchResult').html(
                        "<div class=\"result-text\"><p class=\"result\">검색결과가 많습니다.</p>"
                        // + "총 <span class=\"text-red\" style=\"vertical-align:baseline;front-weight:bold\">" + response.data.totalCount + "</span>건</p>"
                        + "<p class=\"text\">검색주소를 조금 더 상세히 입력해주세요.</p></div>").show();
                }

                $('#searchAddr').removeClass("no-data");
                $('#searchAddr').removeClass("auto");
                $('#searchAddr').html(listText).animate({scrollTop: 0}).show();

                //페이지 영역
                checkIsland.drawZipcodePager("pagingNavi", response.pagination.page,
                    DEFAULT_PAGE_SIZE, response.pagination.totalPage,
                    checkIsland.searchZipCode);

            } else {
                //errorLable 노출
                checkIsland.showErrorLabel("검색결과가 없습니다.");
                //페이징 영역 지움
                $('#pagingNavi').empty();
            }
        });
    },

    /**
     * 페이징 영역 출력
     * @param id  대상 아이디
     * @param pageNo 페이지 번호
     * @param pageSize 페이지 리스트에 노출되는
     * @param total 전체 게시물 건수
     * @param callback callback Script
     */
    drawZipcodePager: function(id, pageNo, pageSize, total, callback) {
    let pager = $('#' + id);

    //페이징
    let totalPage = parseInt(total);
    let startPage = parseInt((pageNo - 1) / pageSize) * pageSize + 1;
    let endPage = startPage - 1 + pageSize;

    if (totalPage < endPage) {
        endPage = totalPage;
    }

    //페이징 영역 지움
    pager.empty();

    //처음 버튼
    if (totalPage > DEFAULT_PER_PAGE && (pageNo !== FIRST_PAGE)) {
        pager.append($("<a>")
            .attr("href", "javascript:void(0);")
            .addClass("btn-move btn-first")
            .click(function () {
                // alert("처음");
                callback(1);
            })
            .append($("<span>")
                .addClass("sr-only")
                .text("처음")
            )
            .append($("<i>")
                .addClass("fa fa-angle-double-left").text(" ")
            )
        );
    }

    //이전 버튼
    if (pageNo > FIRST_PAGE) {
        let prev_page = pageNo - 1;
        pager.append($("<a>")
            .attr("href", "javascript:void(0);")
            .addClass("btn-move btn-prev")
            .click(function () {
                // alert("이전");
                callback(prev_page);
            })
            .append($("<span>")
                .addClass("sr-only")
                .text("이전")
            )
            .append($("<i>")
                .addClass("fa fa-angle-left").text(" ")
            )
        );
    }
    let pages = [];
    for (let i = startPage; i <= endPage; i++) {
        pages[pages.length] = i;
    }

    //페이지 노출
    let page_obj;
    $.each(pages, function (index, p) {
        if (p == pageNo) {
            page_obj = $("<a>").addClass("link-page").addClass("active");
        } else {
            page_obj = $("<a>").attr("href", "javascript:void(0);").addClass(
                "link-page").click(function () {
                callback(p);
            });
        }

        pager.append(page_obj.text(p));
    });

    //다음 버튼
    if (pageNo < totalPage) {
        let next_page = pageNo + 1;
        pager.append($("<a>")
            .attr("href", "javascript:void(0);")
            .addClass("btn-move btn-next")
            .click(function () {
                // alert("다음");
                callback(next_page);
            })
            .append($("<span>")
                .addClass("sr-only")
                .text("다음")
            )
            .append($("<i>")
                .addClass("fa fa-angle-right").text(" ")
            )
        );
    }

    //마지막 버튼
    if ((totalPage > DEFAULT_PER_PAGE) && (pageNo !== endPage)) {
        pager.append($("<a>")
            .attr("href", "javascript:void(0);")
            .addClass("btn-move btn-last")
            .click(function () {
                // alert("마지막");
                callback(totalPage);
            })
            .append($("<span>")
                .addClass("sr-only")
                .text("마지막")
            )
            .append($("<i>")
                .addClass("fa fa-angle-double-right").text(" ")
            )
        );
    }
    pager.show();
    },
    /**
     * 조회된 검색어 강조
     * @param address
     * @paam searchKeyWord
     * @returns {*|string|void}
     */
    addressToBoldBySearchKeyWord: function(address, searchKeyword) {
        let boldAddress = address;
        return boldAddress.replace(new RegExp(searchKeyword, 'g'),
            '<strong class="result">' + searchKeyword + '</strong>');
    },
    /**
     * 도서산간 유형코드를 텍스트로 출력
     * @param islandType 도서산간 유혀코드
     */
    islandTypeToText: function (islandType) {
        let islandTypeTxt = '';
        if (islandType == 'JEJU') {
            islandTypeTxt = '제주';
        } else if (islandType == 'ISLAND'){
            islandTypeTxt = '도서산간';
        } else {
            islandTypeTxt = '전국';
        }
        return islandTypeTxt;
    }
};


/**
 * commonAjax wrapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function addressAjax(url, method, data, successCallback, successMsg) {
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.contentType = "application/json; charset=utf-8";
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.errorCallbackFunc = function () {
        alert(ERROR_MESSAGE);
    };

    CommonAjax.basic(params);
}