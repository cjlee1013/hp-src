/**
 * 브랜드조회 공통팝업
 */
$(document).ready(function() {
    brandPopGrid.init();
    brandPop.init();
    CommonAjaxBlockUI.global();
});

// 브랜드조회그리드
var brandPopGrid = {
    gridView : new RealGridJS.GridView("brandPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        brandPopGrid.initbrandPopGrid();
        brandPopGrid.initDataProvider();
        brandPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initbrandPopGrid : function() {
        brandPopGrid.gridView.setDataSource(brandPopGrid.dataProvider);
        brandPopGrid.gridView.setStyles(brandPopGridBaseInfo.realgrid.styles);
        brandPopGrid.gridView.setDisplayOptions(brandPopGridBaseInfo.realgrid.displayOptions);
        brandPopGrid.gridView.setColumns(brandPopGridBaseInfo.realgrid.columns);
        brandPopGrid.gridView.setOptions(brandPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        brandPopGrid.dataProvider.setFields(brandPopGridBaseInfo.dataProvider.fields);
        brandPopGrid.dataProvider.setOptions(brandPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        brandPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        brandPopGrid.dataProvider.clearRows();
        brandPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = brandPopGrid.gridView.getCheckedRows(true);
        var brandList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.brandNo = _data.brandNo;
            if (_data.dispNm == 'KOR') {
                dataObj.brandNm = _data.brandNm;
            } else if (_data.dispNm == 'ENG') {
                dataObj.brandNm = _data.brandNmEng;
            }
            dataObj.regNm = _data.regNm;

            brandList.push(dataObj);
        }, this);

        return brandList;
    }
};

// 브랜드조회
var brandPop = {
    /**
     * 초기화
     */
    init : function() {
        brandPop.event();
        brandPop.brandPopFormReset();
        brandPop.initSearchDate('-1y');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        brandPop.setDate = Calendar.datePickerRange('schRegStartDate', 'schRegEndDate');

        brandPop.setDate.setEndDate(0);
        brandPop.setDate.setStartDate(flag);

        $("#schRegEndDate").datepicker("option", "minDate", $("#schRegStartDate").val());
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#brandPopForm').resetForm();
        brandPop.initSearchDate('-1y');
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(brandPop.searchBrandList);
        $("#selectBtn").bindClick(brandPop.checkGridRow);
        $('#searchResetBtn').bindClick(brandPop.searchFormReset);

        $("#searchValue").keyup(function (e) {
            if (e.keyCode == 13) {
                brandPop.searchBrandList();
            }
        });
    },
    /**
     * 공통팝업 form 초기화
     */
    brandPopFormReset : function() {
        $("#brandPopForm").resetForm();
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 검색어 체크
        var searchValue = $('#searchValue').val();

        if (!$.jUtil.isEmpty(searchValue)) {
            if ($.jUtil.isNotAllowInput(searchValue, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchValue').focus();
                return false;
            }
        }
        else {
            alert('검색 키워드를 입력해주세요.');
            return false;
        }

        return true;
    },
    /**
     *  조회
     */
    searchBrandList : function () {
        // 조회 조건 validation check
        if (brandPop.validCheck()) {
            var brandPopForm = $("#brandPopForm").serialize();

            CommonAjax.basic({
                url: '/common/getBrandSearchPopList.json',
                data: brandPopForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    brandPopGrid.setData(res);
                    $("#brandPopSearchCnt").html(brandPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = brandPopGrid.gridView.getCheckedRows(true);

        if (checkedRows.length > 1) {
            alert("하나의 브랜드만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("브랜드를 선택해 주세요.");
            return false;
        }

        var check = brandPopGrid.checkData();
        eval("opener." + callBackScript + "(check);");

        self.close();
    },
};