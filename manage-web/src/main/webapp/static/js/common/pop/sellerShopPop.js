/**
 * 기획전, 이벤트조회 공통 팝업
 */
$(document).ready(function() {
    sellerShopPopGrid.init();
    sellerShopPop.init();
    CommonAjaxBlockUI.global();
});

// 기획전, 이벤트조회 공통팝업 그리드
var sellerShopPopGrid = {
    gridView : new RealGridJS.GridView("sellerShopPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        sellerShopPopGrid.initSellerShopPopGrid();
        sellerShopPopGrid.initDataProvider();
        sellerShopPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initSellerShopPopGrid : function() {
        sellerShopPopGrid.gridView.setDataSource(sellerShopPopGrid.dataProvider);
        sellerShopPopGrid.gridView.setStyles(sellerShopPopGridBaseInfo.realgrid.styles);
        sellerShopPopGrid.gridView.setDisplayOptions(sellerShopPopGridBaseInfo.realgrid.displayOptions);
        sellerShopPopGrid.gridView.setColumns(sellerShopPopGridBaseInfo.realgrid.columns);
        sellerShopPopGrid.gridView.setOptions(sellerShopPopGridBaseInfo.realgrid.options);

    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        sellerShopPopGrid.dataProvider.setFields(sellerShopPopGridBaseInfo.dataProvider.fields);
        sellerShopPopGrid.dataProvider.setOptions(sellerShopPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        sellerShopPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        sellerShopPopGrid.dataProvider.clearRows();
        sellerShopPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = sellerShopPopGrid.gridView.getCheckedRows(true);
        var sellerShopList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.partnerId = _data.partnerId;
            dataObj.partnerNm = _data.partnerNm;
            dataObj.shopNm = _data.shopNm;
            dataObj.shopInfo = _data.shopInfo;

            sellerShopList.push(dataObj);
        }, this);

        return sellerShopList;
    },
};

// 기획전, 이벤트조회
var sellerShopPop = {

    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-10d');

    },
    initSearchDate : function (flag) {
        sellerShopPop.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        sellerShopPop.setDate.setEndDate(0);
        sellerShopPop.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $("#schBtn").bindClick(sellerShopPop.searchSellerShopList);
        $("#schResetBtn").bindClick(sellerShopPop.searchFormReset);
        $("#selectBtn").bindClick(sellerShopPop.checkGridRow);


    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        sellerShopPop.initSearchDate('-10d');
        sellerShopPop.intitView();

    },


    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 6개월 단위
        if (moment($("#searchEndDt").val()).diff($("#searchStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#searchStartDt").val(moment($("#searchEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }


        return true;
    },
    /**
     * 기획전, 이벤트 조회
     */
    searchSellerShopList : function () {
        // 조회 조건 validation check
        if (sellerShopPop.validCheck()) {
            var sellerShopPopForm = $("#sellerShopPopForm").serializeObject(true);


            CommonAjax.basic({
                url: "/common/getSellerShopPopList.json"
                , data: sellerShopPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    sellerShopPopGrid.setData(res);
                    $("#sellerShopCnt").html(sellerShopPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = sellerShopPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("판매업체를 선택해 주세요.");
            return false;
        }

        var checkSellerShopList = sellerShopPopGrid.checkData();
        eval("opener." + callBackScript + "(checkSellerShopList);");

        self.close();
    },
};
