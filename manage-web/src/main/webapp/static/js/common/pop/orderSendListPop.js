/**
 *  메세지관리 > 발송관리 > 메시지 수동발송관리 > 주문회원 검색 팝업
 */
var orderSendListPop = {
    init: function () {
        this.innerInit();
        this.event();
        this.initDate(0);
    },
    initDate: function (d) {
        orderSendListPop.setDate = Calendar.datePicker('orderDt');
        if(d !== null && d !== undefined && d !== '') {
            orderSendListPop.setDate.setDate(d);
        }
    },
    innerInit: function () {
        //$('#orderDt').attr('readOnly', true);
        orderSendListPop.setOrderType();
    },
    close: function () {
        self.close();
    },
    saveAsBuyer: function () {
        orderSendListPop.save(buyerCallback);
    },
    saveAsShip: function () {
        orderSendListPop.save(shipCallback);
    },
    save: function (callbackFunctionName) {
        let rows = orderSendListGrid.gridView.getCheckedRows(false);
        if (rows.length === 0) {
            alert("선택 된 건이 없습니다.");
            return false;
        }
        eval('opener.' + callbackFunctionName + '(orderSendListGrid.toList(rows));');
        orderSendListPop.close();
    },
    reset: function () {
        $('#orderSendListPopForm').resetForm();
        orderSendListPop.initDate(0);
        orderSendListPop.clearShiftSlot();
    },
    openStorePopup: function () {
        let target = 'orderSendListPop.setStoreData';
        let param = '&storeType=HYPER&storeRange=ALL';
        let url = '/common/popup/storePop?callback=' + target;
        windowPopupOpen(url + param, target, 1085, 550);
    },
    setStoreData: function (data) {
        $('#storeId').val(data[0].storeId);
        $('#storeNm').val(data[0].storeNm);
    },
    openMemberPopup: function () {
        let callback = 'orderSendListPop.setMemberData';
        memberSearchPopup(callback);
    },
    setMemberData: function (data) {
        $('#userNo').val(data.userNo);
        $('#buyerNm').val(data.userNm);
    },
    getDayName: function () {
        const week = ['일', '월', '화', '수', '목', '금', '토'];
        return week[new Date($('#orderDt').val()).getDay()];
    },
    setOrderType: function() {
        $('#shiftId').attr('disabled', true);
        $('#slotId').attr('disabled', true);
        $('#driverNm').attr('disabled', true);
    },
    setShipType: function() {
        $('#shiftId').attr('disabled', false);
        $('#slotId').attr('disabled', false);
        $('#driverNm').attr('disabled', false);
    },
    event: function () {
        $('#orderType').on('change', function () {
            let value = $('#orderType').val();
            if (value === 'order') {
                orderSendListPop.setOrderType();
            } else {
                // value === 'ship'
                orderSendListPop.setShipType();
            }
        });
        $('#orderDt').on('change', function () {
            let date = orderSendListPop.getDayName();
            orderSendListPop.makeShift(date);
        });
        $('#shiftId').on('change', function () {
            orderSendListPop.makeSlot($('#shiftId').val());
        });
    },
    makeShift: function (day) {
        let code;
        let shiftId = $('#shiftId');
        orderSendListPop.clearShiftSlot();
        switch (day) {
            case '일':
                code = '1';
                break;
            case '월':
                code = '2';
                break;
            case '화':
                code = '3';
                break;
            case '수':
                code = '4';
                break;
            case '목':
                code = '5';
                break;
            case '금':
                code = '6';
                break;
            case '토':
                code = '7';
                break;
            default:
                orderSendListPop.clearShiftSlot();
                return;
        }
        for (let i = 1; i <= 5; i++) {
            let appendShiftId = code + i + '0';
            shiftId.append('<option value="' + appendShiftId + '">'
                + appendShiftId
                + '</option>');
        }
    },
    makeSlot: function (shiftId) {
        let slotId = $('#slotId');
        slotId.empty();
        slotId.append('<option value="">선택</option>');
        if (shiftId === '' || !shiftId) {
            return;
        }

        for (let i = 1; i <= 2; i++) {
            let appendSlotId = shiftId + i;
            slotId.append('<option value="' + appendSlotId + '">'
                + appendSlotId
                + '</option>')
        }
    },
    clearShiftSlot: function () {
        let slotId = $('#slotId');
        let shiftId = $('#shiftId');
        slotId.empty();
        shiftId.empty();
        slotId.append('<option value="">선택</option>');
        shiftId.append('<option value="">선택</option>');
    },
    search: function () {
        if (orderSendListPop.isEmpty($('#orderDt').val())) {
            alert("날짜를 지정해 주세요");
            return;
        }
        let data = _F.getFormDataByJson($('#orderSendListPopForm'));
        CommonAjax.basic({
            url: '/common/getOrderSendList.json',
            contentType: 'application/json',
            data: data,
            method: 'POST',
            successMsg: null,
            callbackFunc: function (res) {
                orderSendListGrid.setData(res.list);
            }
        });
    },
    isEmpty: function (value) {
        return value === ""
            || value == null
            || (typeof value == "object" && !Object.keys(value).length);
    }
};

const orderSendListGrid = {
    gridView: new RealGridJS.GridView("orderSendListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        orderSendListGrid.initGrid();
        orderSendListGrid.initDataProvider();
        orderSendListGrid.event();
        orderSendListGridBaseInfo.realgrid.options.checkBar.visible = true;
    },
    initGrid: function () {
        orderSendListGrid.gridView.setDataSource(
            orderSendListGrid.dataProvider);

        orderSendListGrid.gridView.setStyles(
            orderSendListGridBaseInfo.realgrid.styles);
        orderSendListGrid.gridView.setDisplayOptions(
            orderSendListGridBaseInfo.realgrid.displayOptions);
        orderSendListGrid.gridView.setColumns(
            orderSendListGridBaseInfo.realgrid.columns);
        orderSendListGrid.gridView.setOptions(
            orderSendListGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        orderSendListGrid.dataProvider.setFields(
            orderSendListGridBaseInfo.dataProvider.fields);
        orderSendListGrid.dataProvider.setOptions(
            orderSendListGridBaseInfo.dataProvider.options);
    },
    setData: function (list) {
        orderSendListGrid.dataProvider.clearRows();
        orderSendListGrid.dataProvider.setRows(list);
    },
    toList: function (rows) {
        let list = [];
        rows.forEach(function (r) {
            list.push(this.dataProvider.getJsonRow(r));
        }, this);
        return list;
    },
    event: function () {
    }
};

$(document).ready(function () {
    orderSendListPop.init();
    orderSendListGrid.init();
    CommonAjaxBlockUI.global();
});