/**
 * 상품조회(옵션포함) 공통 팝업
 */
$(document).ready(function() {
    itemPopOptionGrid.init();
    itemPopOption.init();
    CommonAjaxBlockUI.global();
    commonCategory.setCategorySelectBox(1, "", "", $("#categoryCd"));
});

// 상품조회 공통팝업 그리드
var itemPopOptionGrid = {
    gridView : new RealGridJS.GridView("itemPopOptionGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        itemPopOptionGrid.itemPopOptionGrid();
        itemPopOptionGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정
     */
    itemPopOptionGrid : function() {
        itemPopOptionGrid.gridView.setDataSource(itemPopOptionGrid.dataProvider);
        itemPopOptionGrid.gridView.setStyles(itemPopOptionGridBaseInfo.realgrid.styles);
        itemPopOptionGrid.gridView.setDisplayOptions(itemPopOptionGridBaseInfo.realgrid.displayOptions);
        itemPopOptionGrid.gridView.setColumns(itemPopOptionGridBaseInfo.realgrid.columns);
        itemPopOptionGrid.gridView.setOptions(itemPopOptionGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        itemPopOptionGrid.dataProvider.setFields(itemPopOptionGridBaseInfo.dataProvider.fields);
        itemPopOptionGrid.dataProvider.setOptions(itemPopOptionGridBaseInfo.dataProvider.options);
    },

    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        itemPopOptionGrid.dataProvider.clearRows();
        itemPopOptionGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = itemPopOptionGrid.gridView.getCheckedRows(true);
        var itemList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            itemList.push(_data);
        }, this);

        return itemList;
    },
};

// 상품조회
var itemPopOption = {
    /**
     * 초기화
     */
    init : function() {
        itemPopOption.event();
        itemPopOption.itemPopOptionFormReset();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(itemPopOption.searchItemList);
        $("#schResetBtn").bindClick(itemPopOption.itemPopOptionFormReset);
        $("#selectBtn").bindClick(itemPopOption.checkGridRow);

    },
    /**
     * 상품조회 공통팝업 form 초기화
     */
    itemPopOptionFormReset : function() {
        $("#itemPopOptionForm").resetForm();


        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);
    },

    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {

        if ($("#schType").val() == "ITEMNO" && !$.jUtil.isEmpty($("#schValue").val())) {
            if ($("#schValue").val().trim().length < 9) {
                alert("상품번호는 9자 이상 입력해주세요.");
                return false;
            }
        } else {
            if ($("#schCateCd2").val() == "") {
                alert("중분류까지 선택해 주세요.");
                return false;
            }
        }


        if (!$.jUtil.isEmpty($("#schValue").val()) && $("#schValue").val().trim().length < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            return false;
        }

        return true;
    },
    /**
     * 상품 조회
     */
    searchItemList : function () {
        // 조회 조건 validation check
        if (itemPopOption.validCheck()) {
            var itemPopOptionForm = $("#itemPopOptionForm").serialize();

            if( $("#schType").val() == "ITEMNO" &&  !$.jUtil.isEmpty($("#schValue").val())){
                itemPopOptionForm = new Object();
                itemPopOptionForm.schType = $("#schType").val();
                itemPopOptionForm.schValue = $("#schValue").val();
            }


            CommonAjax.basic({
                url: "/common/getItemOptionPopList.json"
                , data: itemPopOptionForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    itemPopOptionGrid.setData(res);
                    $("#itemPopOptionSearchCnt").html(itemPopOptionGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = itemPopOptionGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("상품을 선택해 주세요.");
            return false;
        }

        var checkItemList = itemPopOptionGrid.checkData();

        var checkItemCount = checkItemList.length;

        for(var i = 0; i< checkItemCount; i++){
            if ( checkItemList[i].optSelUseYn == 'Y' && checkItemList[i].listSplitDispYn == 'N' ) {
                alert ('용도옵션 상품 중 리스트분할 여부가 N인 상품은 레시피에 등록할 수 없습니다.!');
                return false;
            }
        }

        eval("opener." + callBackScript + "(checkItemList);");

        self.close();
    },
};