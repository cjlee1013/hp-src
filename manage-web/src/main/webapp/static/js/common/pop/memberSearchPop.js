const API_MEMBER_SEARCH_URI = "/common/search/members.json";
let result = null;
const RETURN_CODE_SUCCESS = "SUCCESS";

const memberSearchMain = {
    init: function () {
        $("#userNo").focus();
        this.buttonEvent();
    },
    close: function () {
        window.close();
    },
    callbackEvent: function () {
        let callBack = 'opener.' + callBackScript;
        eval(callBack)(result);
        //창 닫기
        memberSearchMain.close();
    }, buttonEvent: function () {
        //조회 폼 초기화
        $("#initBtn").on('click', function(){
            memberSearchMain.reset();
        });
        //검색
        $("#schBtn").on('click', function(){
            memberSearchMain.search();
        });
        //검색 키 이벤트
        $("#userNo, #userNm, #userId, #mobile3").on("keypress", function (e) {
            if (e.keyCode === 13) {
                memberSearchMain.search();
            }
        });

        //휴대폰 번호 이동
        $("#mobile1").on("keyup", function () {
            if ($("#mobile1").val().length === 3) {
                $("#mobile2").focus();
            }
        });
        $("#mobile2").on("keyup", function () {
            if ($("#mobile2").val().length === 4) {
                $("#mobile3").focus();
            }
        });

        //확인 버튼
        $("#selectBtn").on('click', function(){
            if ($.jUtil.isEmpty(result)) {
                alert("회원 정보를 선택해주세요.");
                return false;
            }
            memberSearchMain.callbackEvent();
        });
        // 취소 버튼
        $("#closeBtn").on('click', function(){
            memberSearchMain.close();
        });
    },
    reset: function () {
        $("#userNo, #userNm, #userId, #mobile1, #mobile2, #mobile3").val("");
        result = null;
    },
    search: function () {
        //유효성 체크
        if (this.invalidForm()){
            return false;
        }
        //api 조회 파라미터
        let data = {};
        if (isIncludeUnmasking) {
            data.isIncludeUnmasking = true;
        }

        data.userNo = $("#userNo").val();
        data.userNm = $("#userNm").val();
        data.userId = $("#userId").val();

        ///set mobile
        let mobile = $("#mobile1").val() + $("#mobile2").val() + $("#mobile3").val();
        data.mobile= $.jUtil.phoneFormatter(mobile);

        manageAjax(API_MEMBER_SEARCH_URI, 'POST', JSON.stringify(data), function(res){
            if (res.returnCode !== RETURN_CODE_SUCCESS) {
                alert(res.returnMessage);
            } else {
                result = null;
                memberSearchGrid.setData(res.data);
            }
        });
    },
    invalidForm: function() {
        const userNo = $("#userNo").val();
        const userNm = $("#userNm").val();
        const userId = $("#userId").val();
        const mobile1 = $("#mobile1").val();
        const mobile2 = $("#mobile2").val();
        const mobile3 = $("#mobile3").val();

        if ($.jUtil.isEmpty(userNo) && $.jUtil.isEmpty(userNm) &&
            $.jUtil.isEmpty(userId) && this.isEmptyMobile(mobile1, mobile2, mobile3)) {
            alert("최소 1개의 검색 값을 입력해주세요.");
            $('#userNo').focus();
            return true;
        }

        if (!$.jUtil.isEmpty(userNo) && !$.jUtil.isAllowInput(userNo, ['NUM'])) {
            alert("숫자만 입력 가능합니다.");
            $('#userNo').focus();
            return true;
        }

        if (!$.jUtil.isEmpty(userNm) && userNm.length < 2) {
            alert("두 글자 이상 입력해주세요.");
            $('#userNo').focus();
            return true;
        }

        if ($.jUtil.isNotEmpty(userId) && !$.jUtil.isAllowInput(userId, ['NUM', 'ENG', 'SPC_3'])) {
            alert("영문, 숫자, 일부 특수문자($, *, @, .) 만 사용 가능합니다.");
            $('#userId').focus();
            return true;
        }

        if ($.jUtil.isNotEmpty(userId) && userId.length < 2) {
            alert("두 글자 이상 입력해주세요.");
            $('#userId').focus();
            return true;
        }

        if (this.isNotEmptyMobile(mobile1, mobile2, mobile3) && this.isNotNumTypeMobile(mobile1, mobile2, mobile3)) {
            alert("숫자만 입력 가능합니다.");
            $('#mobile1').focus();
            return true;
        }
        return false;
    },
    isEmptyMobile: function (mobile1, mobile2, mobile3) {
        return ($.jUtil.isEmpty(mobile1) && $.jUtil.isEmpty(mobile2)
            && $.jUtil.isEmpty(mobile3))
    },
    isNotEmptyMobile: function (mobile1, mobile2, mobile3) {
        return !this.isEmptyMobile(mobile1, mobile2, mobile3);
    },
    isNotNumTypeMobile: function (mobile1, mobile2, mobile3) {
        return !($.jUtil.isAllowInput(mobile1, ['NUM']) && $.jUtil.isAllowInput(
            mobile2, ['NUM']) && $.jUtil.isAllowInput(mobile3, ['NUM']));

    },
};

const memberSearchGrid = {
    gridView : new RealGridJS.GridView("memberSearchGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        memberSearchGrid.initGrid();
        memberSearchGrid.initDataProvider();
        memberSearchGrid.event();
    },
    initGrid : function(){
        memberSearchGrid.gridView.setDataSource(memberSearchGrid.dataProvider);
        memberSearchGrid.gridView.setStyles(memberSearchGridBaseInfo.realgrid.styles);
        memberSearchGrid.gridView.setDisplayOptions(memberSearchGridBaseInfo.realgrid.displayOptions);
        memberSearchGrid.gridView.setColumns(memberSearchGridBaseInfo.realgrid.columns);
        memberSearchGrid.gridView.setOptions(memberSearchGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        memberSearchGrid.dataProvider.setFields(memberSearchGridBaseInfo.dataProvider.fields);
        memberSearchGrid.dataProvider.setOptions(memberSearchGridBaseInfo.dataProvider.options);
    }, event : function() {
        memberSearchGrid.gridView.onDataCellClicked = function(gridView, index) {
            result = memberSearchGrid.dataProvider.getJsonRow(index.dataRow);
        };
        memberSearchGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            result = memberSearchGrid.dataProvider.getJsonRow(index.dataRow);
            memberSearchMain.callbackEvent();
        };
    },
    setData : function(dataList) {
        memberSearchGrid.dataProvider.clearRows();
        memberSearchGrid.dataProvider.setRows(dataList);
    },
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType =  "application/json; charset=utf-8";

    CommonAjax.basic(params);
}