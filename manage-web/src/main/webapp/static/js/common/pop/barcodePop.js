/**
 * 쿠폰바코드 선택 팝업
 */
$(document).ready(function() {
    barcodePopGrid.init();
    barcodePop.init();
    CommonAjaxBlockUI.global();
});

// 팝업 그리드
var barcodePopGrid = {
    gridView : new RealGridJS.GridView("barcodePopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        barcodePopGrid.initGrid();
        barcodePopGrid.initDataProvider();
        barcodePopGrid.event();
        this.gridView.setCheckBar({exclusive : true});
    },
    /**
     * 조회 그리드 설정
     */
    initGrid : function() {
        barcodePopGrid.gridView.setDataSource(barcodePopGrid.dataProvider);
        barcodePopGrid.gridView.setStyles(barcodePopGridBaseInfo.realgrid.styles);
        barcodePopGrid.gridView.setDisplayOptions(barcodePopGridBaseInfo.realgrid.displayOptions);
        barcodePopGrid.gridView.setColumns(barcodePopGridBaseInfo.realgrid.columns);
        barcodePopGrid.gridView.setOptions(barcodePopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        barcodePopGrid.dataProvider.setFields(barcodePopGridBaseInfo.dataProvider.fields);
        barcodePopGrid.dataProvider.setOptions(barcodePopGridBaseInfo.dataProvider.options);

        setColumnLookupOptionForJson(barcodePopGrid.gridView, "incUnitCd", incUnitCdJson);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        barcodePopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 넘어온 데이터 체크선택 셋팅
     */
    getDataItemCheck : function(value) {

        if (value != "") {
            let option = {
                startIndex: 0,
                fields: ["couponCd"],
                values: [value]
            };
            barcodePopGrid.gridView.checkItem(barcodePopGrid.gridView.searchItem(option));
        }

        /*if ($.jUtil.isEmpty(dataList)) {
            return;
        }

        console.log(dataList);

        let getBarcodeList = JSON.parse(dataList);
        //console.log(couponBarcodeList);

        if (getBarcodeList != "" && allBarcodeList != "") {
            getBarcodeList.forEach(function (value, index, array1) {
                let option = {
                    startIndex: 0,
                    fields: ["barcode"],
                    values: [value.barcode]
                };
                console.log(index);
                console.log(value);
                barcodePopGrid.gridView.checkItem(barcodePopGrid.gridView.searchItem(option));
            });
        }*/
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        barcodePopGrid.dataProvider.clearRows();
        barcodePopGrid.dataProvider.setRows(JSON.parse(dataList));

        barcodePopGrid.getDataItemCheck(getBarcode);

        if (setYn == "N") {
            //barcodePopGrid.gridView.setCheckable(false);
        }
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = barcodePopGrid.gridView.getCheckedRows(true);
        var selectedList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.couponCd = _data.couponCd;
            dataObj.couponSeq = _data.couponSeq;
            dataObj.incUnitCd = _data.incUnitCd;
            dataObj.couponSdate = _data.couponSdate;
            dataObj.couponEdate = _data.couponEdate;
            selectedList.push(dataObj);
        }, this);

        return selectedList;
    }
};

// 바코드 조회 팝업
var barcodePop = {
    /**
     * 초기화
     */
    init : function() {
        barcodePop.event();
        barcodePopGrid.setData(allBarcodeList);
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        barcodePop.clearAllFilter();
        $("#barcodePopForm").resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#gridSearchBtn").bindClick(barcodePop.gridSearch);
        $('#gridSearchResetBtn').bindClick(barcodePop.searchFormReset);
        $("#selectBtn").bindClick(barcodePop.checkGridRow);

        $("#gridSearchValue").keyup(function (e) {
            if (e.keyCode == 13 && $.jUtil.isNotEmpty($(this).val())) {
                barcodePop.gridSearch();
            }
        });
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 검색어 체크
        var schKeyword = $('#schKeyword').val();

        if (schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (schKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    /**
     * 그리드 리스트에서 검색 조회
     */
    gridSearch : function () {
        barcodePop.filter($("#gridSearchType").val(), $("#gridSearchValue").val());
    },
    /**
     * 적용대상 그리드 내 조회
     * @param searchColumnName
     * @param searchValue
     */
    filter : function(searchColumnName, searchValue) {
        barcodePop.clearAllFilter();

        if (!searchColumnName || !searchValue)
            return;

        var filter = {};
        var filterArr = [];
        var activeFilterArr = [];

        switch(searchColumnName) {
            case "couponCd":
                filter = {name: searchValue, criteria: "value = '" + searchValue + "'"};
                filterArr.push(filter);
                activeFilterArr.push(searchValue);
                break;
            case "trgtSeq":
                filter = {name: searchValue, criteria: "value like '" + searchValue + "%'"};
                filterArr.push(filter);
                activeFilterArr.push(searchValue);
                break;
        }

        barcodePopGrid.gridView.setColumnFilters(searchColumnName, filterArr);
        barcodePopGrid.gridView.activateColumnFilters(searchColumnName, activeFilterArr, true);
    },
    /**
     * 적용대상 필터 제거
     */
    clearAllFilter : function() {
        barcodePopGrid.gridView.clearColumnFilters("couponCd");
        barcodePopGrid.gridView.clearColumnFilters("trgtSeq");
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = barcodePopGrid.gridView.getCheckedRows(true);

        if (checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("쿠폰 바코드를 선택해 주세요.");
            return false;
        }

        var checkedBarcodeOneRow = barcodePopGrid.checkData();

        eval("opener." + callBackScript + "(checkedBarcodeOneRow);");

        self.close();
    }
};