/**
 * 판매업체조회 공통 팝업
 */
$(document).ready(function() {
    partnerPopGrid.init();
    partnerPop.init();
    CommonAjaxBlockUI.global();
});

// 판매업체조회 공통팝업 그리드
var partnerPopGrid = {
    gridView : new RealGridJS.GridView("partnerPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        partnerPopGrid.initPartnerPopGrid();
        partnerPopGrid.initDataProvider();
        partnerPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initPartnerPopGrid : function() {
        partnerPopGrid.gridView.setDataSource(partnerPopGrid.dataProvider);
        partnerPopGrid.gridView.setStyles(partnerPopGridBaseInfo.realgrid.styles);
        partnerPopGrid.gridView.setDisplayOptions(partnerPopGridBaseInfo.realgrid.displayOptions);
        partnerPopGrid.gridView.setColumns(partnerPopGridBaseInfo.realgrid.columns);
        partnerPopGrid.gridView.setOptions(partnerPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        partnerPopGrid.dataProvider.setFields(partnerPopGridBaseInfo.dataProvider.fields);
        partnerPopGrid.dataProvider.setOptions(partnerPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        partnerPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        partnerPopGrid.dataProvider.clearRows();
        partnerPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = partnerPopGrid.gridView.getCheckedRows(true);
        var partnerList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.partnerId =  _data.partnerId;
            if(partnerType == "SELL") {
                dataObj.businessNm = _data.businessNm;
            }
            dataObj.partnerNm = _data.partnerNm;
            dataObj.partnerOwner = _data.partnerOwner;

            partnerList.push(dataObj);
        }, this);

        return partnerList;
    },
    excelDownload: function() {
        if(partnerPopGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "판매업체 리스트_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        partnerPopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 판매업체조회
var partnerPop = {
    /**
     * 초기화
     */
    init : function() {
        if(partnerType == "SELL") {
            $("#titlePop").html("판매업체조회");
        } else {
            $("#titlePop").html("제휴업체 검색");
        }

        partnerPop.event();
        partnerPop.partnerPopFormReset();
        partnerPop.initSearchDate('-1y');
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#partnerPopForm').resetForm();

        // 조회 일자 초기화
        partnerPop.initSearchDate('-1y');
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        partnerPop.setDate = Calendar.datePickerRange('schRegStartDate', 'schRegEndDate');

        partnerPop.setDate.setEndDate(0);
        partnerPop.setDate.setStartDate(flag);

        $("#schRegEndDate").datepicker("option", "minDate", $("#schRegStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(partnerPop.searchPartnerList);
        $("#selectBtn").bindClick(partnerPop.checkGridRow);
        $('#searchResetBtn').bindClick(partnerPop.searchFormReset);
        $("#schPartnerType").bindChange(partnerPop.viewControl);
        $('#excelDownloadBtn').bindClick(partnerPop.excelDownload);

        $("#schPartnerValue, #schKeyword").keyup(function (e) {
            if (e.keyCode == 13) {
                partnerPop.searchPartnerList();
            }
        });
    },
    /**
     * 판매업체조회 공통팝업 form 초기화
     */
    partnerPopFormReset : function() {
        $("#partnerPopForm").resetForm();

        // 입력값이 따른 화면 컨트롤
        partnerPop.viewControl();
    },
    /**
     * requsetParam에 따른 화면 컨트롤
     */
    viewControl : function() {
        // 판매업체ID값이 있을경우 판매업체 조건 항목 변경
        if (!$.jUtil.isEmpty(partnerId)) {
            $("#schType").val("PARTNERID");
            $("#schKeyword").val(partnerId).prop("readOnly", true);
        }
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 날짜 체크
        var startDt = $("#schRegStartDate");
        var endDt = $("#schRegEndDate");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            seller.initSearchDate('-1y');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 검색어 체크
        var schKeyword = $('#schKeyword').val();

        if (schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (schKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    /**
     * 판매업체 조회
     */
    searchPartnerList : function () {
        // 조회 조건 validation check
        if (partnerPop.validCheck()) {
            var partnerPopForm = $("#partnerPopForm").serialize();

            CommonAjax.basic({
                url: '/partner/getSellerList.json',
                data: partnerPopForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    partnerPopGrid.setData(res);
                    $("#partnerPopSearchCnt").html(partnerPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = partnerPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("판매업체를 선택해 주세요.");
            return false;
        }

        var checkPartnerList = partnerPopGrid.checkData();
        eval("opener." + callBackScript + "(checkPartnerList);");

        self.close();
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        partnerPopGrid.excelDownload();
    }
};