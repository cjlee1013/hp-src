/**
 * 익스프레스 이력 팝업
 */
$(document).ready(function() {
    expStoreHistListPopGrid.init();
    expStoreHistPop.init();
});

//list grid
var expStoreHistListPopGrid = {
    gridView : new RealGridJS.GridView("expStoreHistListPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        expStoreHistListPopGrid.initGrid();
        expStoreHistListPopGrid.initDataProvider();
        expStoreHistListPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initGrid : function() {
        expStoreHistListPopGrid.gridView.setDataSource(expStoreHistListPopGrid.dataProvider);
        expStoreHistListPopGrid.gridView.setStyles(expStoreHistListPopGridBaseInfo.realgrid.styles);
        expStoreHistListPopGrid.gridView.setDisplayOptions(expStoreHistListPopGridBaseInfo.realgrid.displayOptions);
        expStoreHistListPopGrid.gridView.setColumns(expStoreHistListPopGridBaseInfo.realgrid.columns);
        expStoreHistListPopGrid.gridView.setOptions(expStoreHistListPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        expStoreHistListPopGrid.dataProvider.setFields(expStoreHistListPopGridBaseInfo.dataProvider.fields);
        expStoreHistListPopGrid.dataProvider.setOptions(expStoreHistListPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        expStoreHistListPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        expStoreHistListPopGrid.dataProvider.clearRows();
        expStoreHistListPopGrid.dataProvider.setRows(dataList);
    },

    excelDownload: function() {
        if(expStoreHistListPopGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "EXPRESS점포_변경이력_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        expStoreHistListPopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};


// 익스프레스 이력 팝업
var expStoreHistPop = {
    /**
     * 초기화
     */
    init : function() {
        expStoreHistPop.event();
        expStoreHistListPopGrid.setData(expStoreHist);
        expStoreHistTotalListPopGrid.setData(expStoreHistTotal);
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {

        $('#excelDownloadBtn').bindClick(expStoreHistPop.excelDownload);

        $("#searchValue").keyup(function (e) {
            if (e.keyCode == 13) {
                expStoreHistPop.searchStoreList();
            }
        });
    },

    excelDownload : function () {
        expStoreHistListPopGrid.excelDownload();
    },
};