/**
 * 상품조회 공통 팝업
 */
$(document).ready(function() {
    itemPopGrid.init();
    itemPop.init();
    CommonAjaxBlockUI.global();
    commonCategory.setCategorySelectBox(1, "", "", $("#categoryCd"));
});

// 상품조회 공통팝업 그리드
var itemPopGrid = {
    gridView : new RealGridJS.GridView("itemPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        itemPopGrid.initItemPopGrid();
        itemPopGrid.initDataProvider();
        itemPopGrid.event();
        if (!$.jUtil.isEmpty(mallType)) {
            if (mallType == 'TD') {
                itemPopGrid.gridView.setColumnProperty(itemPopGrid.gridView.columnByName("businessNm"), "visible", false);
                itemPopGrid.gridView.setColumnProperty(itemPopGrid.gridView.columnByName("partnerId"), "visible", false);
            } else {
                itemPopGrid.gridView.setColumnProperty(itemPopGrid.gridView.columnByName("itemParent"), "visible", false);
                itemPopGrid.gridView.setColumnProperty(itemPopGrid.gridView.columnByName("storeType"), "visible", false);
            }
        }
    },
    /**
     * 조회 그리드 설정
     */
    initItemPopGrid : function() {
        itemPopGrid.gridView.setDataSource(itemPopGrid.dataProvider);
        itemPopGrid.gridView.setStyles(itemPopGridBaseInfo.realgrid.styles);
        itemPopGrid.gridView.setDisplayOptions(itemPopGridBaseInfo.realgrid.displayOptions);
        itemPopGrid.gridView.setColumns(itemPopGridBaseInfo.realgrid.columns);
        itemPopGrid.gridView.setOptions(itemPopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        itemPopGrid.dataProvider.setFields(itemPopGridBaseInfo.dataProvider.fields);
        itemPopGrid.dataProvider.setOptions(itemPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        itemPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            var exclusive = true;

            if (isMulti == 'Y') {
                exclusive = false;
            }

            if (itemPopGrid.gridView.isCheckedRow(index.dataRow)) {
                itemPopGrid.gridView.checkRow(index.dataRow, false, exclusive);
            } else {
                itemPopGrid.gridView.checkRow(index.dataRow, true, exclusive);
            }
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        itemPopGrid.dataProvider.clearRows();
        itemPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = itemPopGrid.gridView.getCheckedRows(true);
        var itemList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.itemNo = _data.itemNo;
            dataObj.itemNm = _data.itemNm1;

            itemList.push(dataObj);
        }, this);

        return itemList;
    },
};

// 상품조회
var itemPop = {
    /**
     * 초기화
     */
    init : function() {
        itemPop.event();
        itemPop.itemPopFormReset();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(itemPop.searchItemList);
        $("#schResetBtn").bindClick(itemPop.itemPopFormReset);
        $("#selectBtn").bindClick(itemPop.checkGridRow);
        $("#schPartnerType").bindChange(itemPop.view.initView)

        $("input:radio[name='schMallType']").on("change", function () {
            itemPop.view.addSelectOption("#schStoreType", $("input:radio[name='schMallType']:checked").val());
        });

        $("#schPartnerValue, #schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                itemPop.searchItemList();
            }
        });
    },
    /**
     * 상품조회 공통팝업 form 초기화
     */
    itemPopFormReset : function() {
        $("#itemPopForm").resetForm();

        // 입력값이 따른 화면 컨트롤
        itemPop.view.initView();

        // 점포 유형값이 있으면 해당 점포 유형 이외의 옵션은 삭제
        if (!$.jUtil.isEmpty(storeType)) {
            if (storeType == "ALL") {
                $("#schStoreType").find("option[value!='']").remove();
            } else {
                $("#schStoreType").find("option[value!=" + storeType + "]").remove();
            }
        }

        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);

        if (!$.jUtil.isEmpty(partnerId)) {
            $('#schPartnerType').val('PARTNERID').attr('readonly', true);
            $('#schPartnerValue').val(partnerId).attr('readonly', true);
        }
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {

        if ($("#schType").val() == "ITEMNO" && !$.jUtil.isEmpty($("#schValue").val())) {
            if ($("#schValue").val().trim().length < 9) {
                alert("상품번호는 9자 이상 입력해주세요.");
                return false;
            }
        } else if ($("#schType").val() == "ITEMPARENT" && !$.jUtil.isEmpty($("#schValue").val())) {
            if ($("#schValue").val().trim().length < 4) {
                alert("parent ID 를 입력해 주세요.");
                return false;
            }
        } else {
            if ($("#schCateCd2").val() == "") {
                alert("중분류까지 선택해 주세요.");
                return false;
            }
        }

        // 거래유형 선택 확인
        if (!$("input:radio[name=schMallType]:checked").prop("checked")) {
            alert("거래유형을 선택해 주세요.");
            return false;
        }

        // 판매업체, 검색어 최소 자리수 확인
        if (!$.jUtil.isEmpty($("#schPartnerValue").val()) && $("#schPartnerValue").val().trim().length < 2) {
            alert("판매업체 키워드는 2자 이상 입력해주세요.");
            return false;
        }

        if (!$.jUtil.isEmpty($("#schValue").val()) && $("#schValue").val().trim().length < 2) {
            alert("검색 키워드는 2자 이상 입력해주세요.");
            return false;
        }

        return true;
    },
    /**
     * 상품 조회
     */
    searchItemList : function () {
        // 조회 조건 validation check
        if (itemPop.validCheck()) {
            var itemPopForm = $("#itemPopForm").serialize();

            CommonAjax.basic({
                url: "/common/getItemPopList.json"
                , data: itemPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    itemPopGrid.setData(res);
                    $("#itemPopSearchCnt").html(itemPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = itemPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("상품을 선택해 주세요.");
            return false;
        }

        var checkItemList = itemPopGrid.checkData();
        eval("opener." + callBackScript + "(checkItemList);");

        self.close();
    },
};

itemPop.view = {
    /**
     * requsetParam에 따른 화면 컨트롤
     */
    initView : function() {
        // 거래유형값이 있을 경우 해당 유형만 선택 가능하도록 변경
        if (!$.jUtil.isEmpty(mallType)) {
            $(".ui.radio").find("input:radio").each(function() {
                if ($(this).val() == mallType) {
                    $(this).prop("checked", true).change();
                } else {
                    $(this).prop("disabled", true);
                    $(this).closest('label').hide();
                }
            });

            if (mallType == 'TD') {
                $('#schPartnerType').closest('tr').hide();
            }
        }

        // 판매업체ID값이 있을경우 판매업체 조건 항목 변경
        if (!$.jUtil.isEmpty(partnerId)) {
            $("#schPartnerType").val("PARTNERID");
            $("#schPartnerValue").val(partnerId).prop("readOnly", true);
        }
    }
    /**
     * select box option 삭제 후 조건에 맞는 option 추가
     * @param obj
     * @param siteType
     * @returns {boolean}
     */
    , addSelectOption : function (obj, mallType) {
        // 전체 를 제외한 나머지 option 제거
        $(obj).find("option[value!='']").remove();

        var jsonData = storeTypeJson;

        $.each(jsonData, function (key, code) {
            var chkOption = 0;
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            var codeRef2 = code.ref2;

            $(obj).find("option").each(function () {
                if ($(this).val() == codeCd) {
                    chkOption++;
                }
            });

            if (chkOption == 0) {
                switch (mallType) {
                    case "TD" :
                        if ($.jUtil.isEmpty(siteType)) {
                            if (codeCd != "DS") {
                                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                                return true;
                            }
                        } else if (siteType == "CLUB") {
                            if (codeRef2 == "CLUB") {
                                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                                return true;
                            }
                        } else {
                            if (codeCd != "DS" && codeRef2 == "HOME") {
                                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                                return true;
                            }
                        }

                        break;
                    case "DS" :
                        if (mallType == codeCd) {
                            $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                            return true;
                        }
                        break;
                    default :
                        $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                        break;
                }
            }
        });
    }
};