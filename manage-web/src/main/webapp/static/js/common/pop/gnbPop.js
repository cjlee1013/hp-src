/**
 * 기획전, 이벤트조회 공통 팝업
 */
$(document).ready(function() {
    gnbPopGrid.init();
    gnbPop.init();
    CommonAjaxBlockUI.global();
});

// 기획전, 이벤트조회 공통팝업 그리드
var gnbPopGrid = {
    gridView : new RealGridJS.GridView("gnbPopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        gnbPopGrid.initGnbPopGrid();
        gnbPopGrid.initDataProvider();
        gnbPopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initGnbPopGrid : function() {
        gnbPopGrid.gridView.setDataSource(gnbPopGrid.dataProvider);
        gnbPopGrid.gridView.setStyles(gnbPopGridBaseInfo.realgrid.styles);
        gnbPopGrid.gridView.setDisplayOptions(gnbPopGridBaseInfo.realgrid.displayOptions);
        gnbPopGrid.gridView.setColumns(gnbPopGridBaseInfo.realgrid.columns);
        gnbPopGrid.gridView.setOptions(gnbPopGridBaseInfo.realgrid.options);

    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        gnbPopGrid.dataProvider.setFields(gnbPopGridBaseInfo.dataProvider.fields);
        gnbPopGrid.dataProvider.setOptions(gnbPopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        gnbPopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        gnbPopGrid.dataProvider.clearRows();
        gnbPopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = gnbPopGrid.gridView.getCheckedRows(true);
        var gnbList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.gnbNo = _data.gnbNo;
            dataObj.dispNm = _data.dispNm ;

            gnbList.push(dataObj);
        }, this);

        return gnbList;
    },
};

// 기획전, 이벤트조회
var gnbPop = {

    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-10d');
        gnbPop.intitView();

    },
    initSearchDate : function (flag) {
        gnbPop.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        gnbPop.setDate.setEndDate(0);
        gnbPop.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $("#schBtn").bindClick(gnbPop.searchGnbList);
        $("#schResetBtn").bindClick(gnbPop.searchFormReset);
        $("#selectBtn").bindClick(gnbPop.checkGridRow);


    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        gnbPop.initSearchDate('-10d');
        gnbPop.intitView();

    },
    /**
     * requsetParam에 따른 화면 컨트롤
     */
    intitView : function(){
        if (!$.jUtil.isEmpty(siteType)) {

            $('input:radio[name="searchSiteType"]').each(function() {
                if ($(this).val() == siteType) {
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("disabled", true);
                }
            });
        }
        if (!$.jUtil.isEmpty(deviceType)) {



            $("#searchDeviceType").find("option").remove();

            var jsonData = gnbDeviceTypeJson;

            if (jsonData == "") return false;

            $.each(jsonData, function (key, code) {
                var codeCd = code.mcCd;
                var codeNm = code.mcNm;


                if (codeCd == deviceType){
                    $("#searchDeviceType").append("<option value=" + codeCd +  " selected >" + codeNm + "</option>");
                }
            });


        }

    },

    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 6개월 단위
        if (moment($("#searchEndDt").val()).diff($("#searchStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#searchStartDt").val(moment($("#searchEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }


        return true;
    },
    /**
     * 기획전, 이벤트 조회
     */
    searchGnbList : function () {
        // 조회 조건 validation check
        if (gnbPop.validCheck()) {
            var gnbPopForm = $("#gnbPopForm").serializeObject(true);


            CommonAjax.basic({
                url: "/common/getGnbPopList.json"
                , data: gnbPopForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    gnbPopGrid.setData(res);
                    $("#gnbCnt").html(gnbPopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = gnbPopGrid.gridView.getCheckedRows(true);

        if (isMulti != "Y" && checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("GNB를 선택해 주세요.");
            return false;
        }

        var checkGnbList = gnbPopGrid.checkData();
        eval("opener." + callBackScript + "(checkGnbList);");

        self.close();
    },
};
