/**
 * 점포조회 공통팝업
 */
$(document).ready(function() {
    storePopGrid.init();
    storePop.init();
    CommonAjaxBlockUI.global();
});

// 점포 공통팝업 그리드
var storePopGrid = {
    gridView : new RealGridJS.GridView("storePopGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        storePopGrid.initStorePopGrid();
        storePopGrid.initDataProvider();
        storePopGrid.event();
    },
    /**
     * 조회 그리드 설정
     */
    initStorePopGrid : function() {
        storePopGrid.gridView.setDataSource(storePopGrid.dataProvider);
        storePopGrid.gridView.setStyles(storePopGridBaseInfo.realgrid.styles);
        storePopGrid.gridView.setDisplayOptions(storePopGridBaseInfo.realgrid.displayOptions);
        storePopGrid.gridView.setColumns(storePopGridBaseInfo.realgrid.columns);
        storePopGrid.gridView.setOptions(storePopGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정
     */
    initDataProvider : function() {
        storePopGrid.dataProvider.setFields(storePopGridBaseInfo.dataProvider.fields);
        storePopGrid.dataProvider.setOptions(storePopGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        storePopGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    /**
     * 조회된 데이터 그리드에 적용
     */
    setData : function(dataList) {
        storePopGrid.dataProvider.clearRows();
        storePopGrid.dataProvider.setRows(dataList);
    },
    /**
     * 그리드에서 선택된 데이터 return
     * @returns {any[]|boolean}
     */
    checkData : function () {
        var checkedRows = storePopGrid.gridView.getCheckedRows(true);
        var storeList = new Array();

        checkedRows.forEach(function (_row) {
            var _data = this.dataProvider.getJsonRow(_row);
            var dataObj = new Object();

            dataObj.storeId =  _data.storeId;
            dataObj.storeNm = _data.storeNm;
            dataObj.storeType =  _data.storeType;
            dataObj.storeTypeNm = _data.storeTypeNm;
            dataObj.storeKind =  _data.storeKind;
            dataObj.storeKindNm = _data.storeKindNm;
            dataObj.originStoreId = _data.originStoreId;

            storeList.push(dataObj);
        }, this);

        return storeList;
    }
};

// 점포조회
var storePop = {
    /**
     * 초기화
     */
    init : function() {
        storePop.event();
        storePop.storePopFormReset();
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#storePopForm').resetForm();
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $("#schBtn").bindClick(storePop.searchStoreList);
        $("#selectBtn").bindClick(storePop.checkGridRow);
        $('#searchResetBtn').bindClick(storePop.searchFormReset);

        $("#searchValue").keyup(function (e) {
            if (e.keyCode == 13) {
                storePop.searchStoreList();
            }
        });
    },
    /**
     * 공통팝업 form 초기화
     */
    storePopFormReset : function() {
        $("#storePopForm").resetForm();
    },
    /**
     * 조회 조건 내용검증
     * @returns {boolean}
     */
    validCheck : function() {
        // 검색어 체크
        var searchValue = $('#searchValue').val();

        if (searchValue != "") {
            if ($.jUtil.isNotAllowInput(searchValue, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchValue').focus();
                return false;
            } else if (searchValue.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    /**
     *  조회
     */
    searchStoreList : function () {
        // 조회 조건 validation check
        if (storePop.validCheck()) {
            var storePopForm = $("#storePopForm").serialize();

            CommonAjax.basic({
                url: '/common/getStoreSearchPopList.json',
                data: storePopForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    storePopGrid.setData(res);
                    $("#storePopSearchCnt").html(storePopGrid.dataProvider.getRowCount());
                }
            });
        }
    },
    /**
     * 선택한 데이터 return
     * @returns {boolean}
     */
    checkGridRow : function () {
        var checkedRows = storePopGrid.gridView.getCheckedRows(true);

        if (checkedRows.length > 1) {
            alert("하나의 항목만 선택할 수 있습니다.");
            return false;
        }

        if (checkedRows.length == 0) {
            alert("점포를 선택해 주세요.");
            return false;
        }

        var checkStore= storePopGrid.checkData();
        eval("opener." + callBackScript + "(checkStore);");

        self.close();
    },
};