/**
 * 쿠폰 일괄발급 파일 업로드 팝업
 */
$(document).ready(function() {
    fileUploadPop.init();
    CommonAjaxBlockUI.global();
});

var fileUploadPop = {
    /**
     * init 이벤트
     */
    init: function() {
        fileUploadPop.bindingEvent();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#schUploadFile").bindClick(fileUploadPop.searchFile); // 파일찾기 버튼
        $("#selectBtn").bindClick(fileUploadPop.selectFile);        // 선택 버튼
        $("#uploadFile").bindChange(fileUploadPop.changeUploadFile);
        $("#downloadTemplate").bindClick(fileUploadPop.downloadTemplate);
    },
    changeUploadFile: function(obj) {
        var fileInfo = $(obj)[0].files[0];
        var fileExt = fileInfo.name.split(".");
        if(fileInfo == "" || fileInfo == null) {
            alert("파일을 선택해주세요.");
            return false;
        }

        if(fileExt.indexOf("xlsx") < 0) {
            alert("엑셀 (xlsx) 파일만 업로드 가능합니다.");
            return false;
        }

        $("#uploadFileNm").val($(obj)[0].files[0].name);
    },
    downloadTemplate: function() {
        var fileUrl = "/static/templates/coupon_issue_template.xlsx";
        location.href = fileUrl;
    },
    /**
     * 파일찾기 버튼 클릭 (윈도우 파일 찾기 open)
     */
    searchFile: function() {
        $("#uploadFile").click();
    },
    /**
     * 선택한 파일 정보 불러오기
     */
    getFile: function(_obj) {
        var fileInfo= _obj[0].files[0];
        var fileNm  = fileInfo.name;
        $("#uploadFileNm").val(fileNm);
    },
    /**
     * 선택 버튼, callBack 함수 호출
     */
    selectFile: function() {
        if($("#uploadFile").val() == "") {
            alert("파일을 선택해주세요.");
            return false;
        }

        if(!confirm("선택 하시겠습니까?")) return false;

        $("#uploadScopePopForm").ajaxForm({
            url: "/promotion/popup/upload/uploadIssueCoupon.json",
            enctype: "multipart/form-data",
            method: "POST",
            success: function(res) {
                if (res.successYn == null) {
                    alert($("#popTitle").text().trim() + "에서 시스템 오류가 발생하였습니다.");
                } else if (res.successYn == 'N') {
                    if (res.failCount == 0) {
                        alert(CommonErrorMsg.uploadFailMsg);
                    } else {
                        //결과 팝업 호출
                        fileUploadPop.callUploadResultPop(res);
                    }
                } else {
                    eval("opener." + callBackScript + "(res.dataList);");
                }
                self.close();
            },
            error: function(resError){
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    } else {
                        alert(CommonErrorMsg.uploadFailMsg);
                    }
                } else {
                    alert(CommonErrorMsg.uploadFailMsg);
                    console.log(resError);
                }
                self.close();
            }
        }).submit();
    },
    /**
     * 결과 팝업 호출
     * */
    callUploadResultPop: function(res) {
        var uri = "/promotion/popup/upload/uploadResultPop?uploadCount=" + res.uploadCount + "&successCount=" + res.successCount + "&failCount=" + res.failCount;
        windowPopupOpen(uri, "uploadResultPop", 600, 300);
    }
};