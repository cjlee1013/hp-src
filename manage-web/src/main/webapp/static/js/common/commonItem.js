/**
 * 공통 - 상품
 */
var commonItem = {

    /**
     * 상품명검색
     */
    getItemName : function(itemNo, callBackFunction){
        if(!$.jUtil.isEmpty(itemNo)) {
            CommonAjax.basic({
                url:'/common/item/getItemNm.json?',
                data:{itemNo: itemNo},
                method:"GET",
                successMsg:null,
                callbackFunc:function(res) {

                    if (typeof callBackFunction == 'function') {
                        callBackFunction(res);
                    }
                }
            });
        }
    }

}