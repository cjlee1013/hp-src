//페이징 그리기
// page = 1 , size = 2 , total = 6 , callback
function drawPaging(id, page, size, total, callback) {
    var pager = $('#' + id);

    //페이지가 1개인 경우 페이징 노출 안함
    if (size >= total) {
        pager.hide();
        return;
    }

    //페이징
    var num_page_group = 10;
    var total_page = parseInt(total / size) + 1;
    var start_page = parseInt((page - 1) / num_page_group) * num_page_group + 1;
    var end_page = start_page - 1 + num_page_group;

    if (total%size == 0 ) {
        total_page--;
    }

    if (total_page < end_page) {
        var end_page = total_page;
    }

    //페이징 영역 지움
    pager.empty();

    //처음 버튼
    if (start_page >  num_page_group) {
        pager.append($("<a>")
            .attr("href", "javascript:(0);")
            .addClass("btn-move btn-first")
            .click(function() {
                // alert("처음");
                callback(1);
            })
                .append($("<span>")
                    .addClass("sr-only")
                    .text("처음")
                )
                .append($("<i>")
                .addClass("fa fa-angle-double-left").text(" ")
                )
        );
    }

    //이전 버튼
    if (start_page > num_page_group) {
        var prev_page = start_page - 1;
        pager.append($("<a>")
            .attr("href", "javascript:(0);")
            .addClass("btn-move btn-prev")
            .click(function() {
                // alert("이전");
                callback(prev_page);
            })
                .append($("<span>")
                .addClass("sr-only")
                    .text("이전")
                )
                .append($("<i>")
                .addClass("fa fa-angle-left").text(" ")
                )
        );
    }
    var pages = [];
    for (var i = start_page; i <= end_page; i++) {
        pages[pages.length] = i;
    }

    //페이지
    var page_obj;
    $.each(pages, function(index, p) {
        if (p == page) {
            page_obj = $("<strong>").addClass("link-page");
        }
        else {
            page_obj = $("<a>").attr("href", "javascript:(0);").addClass("link-page").click(function() {
                callback(p);
            });
        }

        pager.append(page_obj.text(p));
    });

    //다음 버튼
    if (end_page < total_page) {
        var next_page = end_page + 1;
        pager.append($("<a>")
            .attr("href", "javascript:(0);")
            .addClass("btn-move btn-next")
            .click(function() {
                // alert("다음");
                callback(next_page);
            })
                .append($("<span>")
                    .addClass("sr-only")
                    .text("다음")
                )
                .append($("<i>")
                .addClass("fa fa-angle-right").text(" ")
                )
        );
    }

    //마지막 버튼
    if (end_page < total_page) {
        pager.append($("<a>")
            .attr("href", "javascript:(0);")
            .addClass("btn-move btn-last")
            .click(function() {
                // alert("마지막");
                callback(total_page);
            })
                .append($("<span>")
                    .addClass("sr-only")
                    .text("마지막")
                )
                .append($("<i>")
                .addClass("fa fa-angle-double-right").text(" ")
                )
        );
    }

    pager.show();
}