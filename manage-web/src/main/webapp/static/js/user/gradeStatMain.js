const gradeStatMain = {
    /**
     * init 이벤트
     */
    init: function(){
        gradeStatMain.buttonEvent();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){
        // 조회 폼 초기화
        $("#resetBtn").on('click', function(){
            gradeStatMain.reset();
        });

        // 검색 및 데이터 조회
        $("#searchBtn").on('click', function(){
            $("#searchForm").submit();
        });

        // 엑셀다운
        $("#excelDownloadBtn").on('click', function(){
            gradeStatMain.excelDownload();
        });

    },

    /**
     * 검색 폼 초기화
     * 조회기간 셀렉트박스
     * 연속Black등급 체크박스
     */
    reset: function(){
        $('#gradeApplyYm option:last').prop("selected", "selected");
        $('input:checkbox[id="isBlackType"]').prop('checked', false);
    },

    /**
     * 엑셀다운로드
     */
    excelDownload: function() {
        if (gradeStatGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "회원등급통계_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        gradeStatGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    }
};

const gradeStatGrid = {
    gridView : new RealGridJS.GridView("gradeStatGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        gradeStatGrid.initGrid();
        gradeStatGrid.initDataProvider();
    },
    initGrid : function(){
        gradeStatGrid.gridView.setDataSource(gradeStatGrid.dataProvider);
        gradeStatGrid.gridView.setStyles(gradeStatGridBaseInfo.realgrid.styles);
        gradeStatGrid.gridView.setDisplayOptions(gradeStatGridBaseInfo.realgrid.displayOptions);
        gradeStatGrid.gridView.setColumns(gradeStatGridBaseInfo.realgrid.columns);
        gradeStatGrid.gridView.setOptions(gradeStatGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        gradeStatGrid.dataProvider.setFields(gradeStatGridBaseInfo.dataProvider.fields);
        gradeStatGrid.dataProvider.setOptions(gradeStatGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        gradeStatGrid.dataProvider.clearRows();
        //gradeStatGrid.dataProvider.setRows(dataList);
    }
};

