const RETURN_CODE_SUCCESS = "SUCCESS";
const MHC_POINT_SUCCESS_CODE = '0000';
const userInfoDetail = {
    /**
     * init 이벤트
     */
    init: function(){
        this.search();
        this.buttonEvent();
    },
    /**
     * button 이벤트
     */
    buttonEvent: function(){
        //로그인LOG
        $("#loginLogBtn").on('click', function(){
            userInfoDetail.getLoginLog();
        });

        //등급내역
        $(document).on('click', "#gradeHistBtn", function(){
            userInfoDetail.getGradeHistory();
        });
        //MHC 보유포인트 조회
        $(document).on('click', "#totOwnPointBtn", function(){
            userInfoDetail.getTotPoints();
        });

        //계좌 삭제
        $("#delBankAccountBtn").on('click', function(){
            userInfoDetail.deleteBankAccount();
        });
        //배송지 주소 삭제
        $("#delAddressBtn").on('click', function(){
            userInfoDetail.deleteShippingAddress();
        });
        //등급변경
        $(document).on('click', "#userGradeUpdateBtn", function(){
            userInfoDetail.userGradeUpdate();
        });

        //문의내역 조회하기
        $(document).on('click', "#vocCallCenterMainBtn", function(){
            userInfoDetail.vocCallCenterMain();
        });
    },
    /**
     * 회원 상세 조회
     * 회원 번호를 기준으로 상세 조회
     */
    search: function(){
        //상세 초기화
        userInfoDetail.clearDetail();
        //조회
        let userNo = $("#userNo").val();
        manageAjax('/user/userInfo/userInfoDetail.json?userNo=' + userNo,
            'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else {
                    userInfoDetail.setDetail(res.data);
                }
            });
    },
    /**
     * 마케팅 정보 조회
     * @param userId 사용자 아이디
     * @param regDt 가입일자
     */
    agreeInfo: function(userId, regDt){
        manageAjax(
            '/user/userInfo/userAgreeInfo.json?userId=' + userId
            + '&regDt='+ regDt, 'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert("마케팅 정보 조회를 실패하였습니다.");
                } else {
                    userInfoDetail.marketingInfoArea(res.data);
                }
            });
    },
    /**
     * 회원정보 중 계좌번호, 대체카드번호 복호화
     * @param bankAccount
     * @param mhcCardnoEnc
     */
    decryptUserInfo: function(bankAccount, mhcCardnoEnc) {
        let param = {};
        param.bankAccount = bankAccount;
        param.mhcCardnoEnc = mhcCardnoEnc;

        const userDecryptParam = JSON.stringify(param);
        manageAjax(
            '/user/userInfo/decryptUserInfo.json', 'post', userDecryptParam,
            function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    // alert(res.returnMessage);
                    alert("일부 데이터가 정상적으로 조회되지 않았습니다.: 대체카드번호,계좌번호");
                } else {
                    userInfoDetail.setDecryptInfo(res.data);
                }
            });
    },
    /**
     * 복호화 된 값 화면 설정
     * @param res
     */
    setDecryptInfo: function(res) {
        $("#mhcCardno").text(res.mhcCardno);
        $("#bankAccount").text(res.bankAccount);
    },
    /**
     * 상세 정보 clear
     */
    clearDetail: function(){
        // 상세보기 데이터 초기화
        $("#detailUserNo, #userId, #userNm, #birth, #gender, #mobile, #email").html("");
        $("#isCompany, #isUnion, #gradeNm, #isCert, #isAdultCertificate, #isBlackConsumer, #personalOverseaNo, #empId, #regDt, #lastLoginDt, #hyperFirstOrder, #expFirstOrder, #clubFirstOrder").html("");
        $("#isMhcCard, #mhcCardType, #mhcCardType, #mhcCardIssueDt, #ocbYn").html("");
        $("#mhcCardnoHmac").val("");
        $("#bankNm, #bankAccount, #bankOwner").html("");

        // 배송지 목록 그리도 초기화
        shippingGrid.dataProvider.clearRows();
    },
    /**
     * 마케팅 정보 영역 그리기
     * @param res
     */
    marketingInfoArea: function (res) {
        //마케팅 활용동의
        let marketingAgreeAreaHtml = res.marketingAgreeYn;
        if ($.jUtil.isNotEmpty(res.marketingAgreeDt)) {
            marketingAgreeAreaHtml += " (" + res.marketingAgreeDt + ")";
        }
        $("#marketingAgreeArea").html(marketingAgreeAreaHtml);

        //마케팅 정보수신
        const smsAgreeYnHtml = userInfoDetail.setMarketingHtml(res.smsAgreeYn,
            res.smsTreatDt);
        const emailAgreeYnHtml = userInfoDetail.setMarketingHtml(res.emailAgreeYn,
            res.emailTreatDt);
        const dmAgreeYnHtml = userInfoDetail.setMarketingHtml(res.dmAgreeYn,
            res.dmTreatDt);
        const receiptAgreeYnHtml = userInfoDetail.setMarketingHtml(
            res.receiptAgreeYn, res.receiptTreatDt);

        $("#smsAgreeYn").html(smsAgreeYnHtml);
        $("#emailAgreeYn").html(emailAgreeYnHtml);
        $("#dmAgreeYn").html(dmAgreeYnHtml);
        $("#receiptAgreeYn").html(receiptAgreeYnHtml);
    },
    setMarketingHtml: function(agreeYn, treatDt) {
        let resultHtml = "";
        if ($.jUtil.isNotEmpty(agreeYn)) {
            resultHtml += agreeYn;
            if ($.jUtil.isNotEmpty(treatDt)) {
                resultHtml += " (" + treatDt + ")";
            }
        }
        return resultHtml;
    },
    /**
     * set 상세 정보
     * @param res {UserInfoDetailDto}
     */
    setDetail(res) {
        //기본정보
        $("#detailUserNo").text(res.userNo);
        $("#userId").text(res.userId);
        $("#userNm").text(res.userNm);
        $("#birth").text(res.birth);
        $("#gender").text(res.gender);
        $("#mobile").text(res.mobile);
        $("#email").text(res.email);

        //회원정보
        $("#isCompany").text(res.isCompany);
        $("#isUnion").text(res.isUnion);
        //회원등급
        $("#gradeNm").text(res.gradeNm);

        //본인 인증여부
        if (res.isCert === "Y") {
            let text = "<span class='inner'>" + res.isCert + " (" + res.certDt + ")</span>";
            $("#isCert").html(text);
        } else {
            $("#isCert").text(res.isCert);
        }
        //성인 인증여부
        if (res.isAdultCertificate === "Y") {
            let text = "<span id='adultCertificateDt'>" + res.isAdultCertificate + " (" + res.adultCertificateDt + ")</span>";
            $("#isAdultCertificate").html(text);
        } else {
            $("#isAdultCrtificate").text(res.isAdultCrtificate);
        }
        $("#isBlackConsumer").text(res.isBlackConsumer);
        $("#personalOverseaNo").text(res.personalOverseaNo);
        $("#empId").text(res.empId);
        $("#regDt").text(res.regDt);
        $("#lastLoginDt").text(res.lastLoginDt);
        // 최초주문정보
        if (res.firstOrderInfo.hyperFirstOrderYn === "Y") {
            $("#hyperFirstOrder").text(res.firstOrderInfo.hyperFirstOrderYn + " (" +res.firstOrderInfo.hyperFirstOrderDt+ ")");
        } else {
            $("#hyperFirstOrder").text(res.firstOrderInfo.hyperFirstOrderYn);
        }
        if (res.firstOrderInfo.expFirstOrderYn === "Y") {
            $("#expFirstOrder").text(res.firstOrderInfo.expFirstOrderYn + " ("+res.firstOrderInfo.expFirstOrderDt+")");
        } else {
            $("#expFirstOrder").text(res.firstOrderInfo.expFirstOrderYn);
        }
        if (res.firstOrderInfo.clubFirstOrderYn === "Y") {
            $("#clubFirstOrder").text(res.firstOrderInfo.clubFirstOrderYn + " ("+res.firstOrderInfo.clubFirstOrderDt+")");
        } else {
            $("#clubFirstOrder").text(res.firstOrderInfo.clubFirstOrderYn);
        }

        //제휴 정보
        $("#isMhcCard").text(res.isMhcCard);
        $("#mhcCardType").text(res.mhcCardType);
        $("#mhcCardIssueDt").text(res.mhcCardIssueDt);
        $("#mhcCardnoHmac").val(res.mhcCardnoHmac);
        $("#ocbYn").text(res.ocbYn);

        let mhcardButtonHtm = "";
        if (res.isMhcCard === "Y") {
            mhcardButtonHtm = "<span class=\"inner\">"
                + "  <button type=\"button\" id=\"totOwnPointBtn\" class=\"ui button medium gray-dark font-malgun\"\n"
                + "  style=\"float: right\">조회</button>\n"
                + "</span>"
            ;
        }
        $("#mhcPointArea").html(mhcardButtonHtm);

        //계좌 환불 정보
        $("#bankNm").text(res.bankNm);
        $("#bankOwner").text(res.bankOwner);
        $("#bankSeq").val(res.bankSeq);

        //배송지 정보
        shippingGrid.setData(res.shippingAddressList);

        //마케팅 정보 조회
        let marketingRegDt = "";
        marketingRegDt = res.regDt;

        //가입일시 중 시분초 제거하여 조회
        if ($.jUtil.isNotEmpty(marketingRegDt)) {
            marketingRegDt = marketingRegDt.substr(0, 10);
        }        userInfoDetail.agreeInfo(res.userId, marketingRegDt);
        //대체카드번호, 계좌번호 조회
        userInfoDetail.decryptUserInfo(res.bankAccount, res.mhcCardnoEnc);
    },
    /**
     * MHC 보유포인트 조회
     */
    getTotPoints: function() {
        const mhcCardnoHmac = $("#mhcCardnoHmac").val();
        if ($.jUtil.isEmpty(mhcCardnoHmac)) {
            alert("MHC 대체카드번호가 없습니다.");
            return;
        }
        //set body
        let body = {};
        body.mhcCardNoHmac = mhcCardnoHmac;
        const userInfoMhcParam = JSON.stringify(body);

        manageAjax('/user/userInfo/mhcPoint.json',
            'post', userInfoMhcParam, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else if (res.data.returnCode !== MHC_POINT_SUCCESS_CODE) {
                    alert(res.data.returnMessage);
                    return false;
                } else {
                    userInfoDetail.setMhcPointArea(
                        $.jUtil.comma(res.data.totOwnPoint),
                        $.jUtil.comma(res.data.totAvlPoint));
                }
            });
    },
    /**
     * MHC 보유포인트 조회결과 DOM 추가
     * @param totOwnPoint MHC 보유포인트
     * @param totAvlPoint MHC 사용가능 포인트
     */
    setMhcPointArea: function (totOwnPoint, totAvlPoint) {
        let htm = "<span id=\"totOwnPoint\" class=\"text\">"+ totOwnPoint +"</span>\n"
            + "<span class=\"text\" style=\"font-weight: bold\">P</span>\n"
            + "<span class=\"text\">( 사용가능포인트 : </span>\n"
            + "<span id=\"totAvlPoint\" class=\"text\">"+ totAvlPoint +"</span>\n"
            + "<span class=\"text\" style=\"font-weight: bold\"> P</span>\n"
            + "<span class=\"text\"> )</span>\n"
            + "<span class=\"inner\">"
            + "  <button type=\"button\" id=\"totOwnPointBtn\" class=\"ui button medium gray-dark font-malgun\"\n"
            + "  style=\"float: right\">조회</button>\n"
            + "</span>"
        ;
        $("#mhcPointArea").html(htm);
    },
    /**
     * 배송지 주소 삭제
     */
    deleteShippingAddress: function () {
        const checkItems = shippingGrid.gridView.getCheckedItems();

        if(checkItems.length === 0) {
            alert("대상 주소를 선택해주세요.");
            return false;
        }

        if (confirm("삭제 하시겠습니까?")) {
            const resultList = [];
            for (const rowId in checkItems) {
                resultList.push(shippingGrid.gridView.getValue(checkItems[rowId], 'saSeq'));
            }

            manageAjax('/user/userInfo/deleteShippingAddress.json?seq=' + resultList.join(", "),
                'get', null, function (res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        userInfoDetail.search();
                    }
                });
        }
    },
    /**
     * 회원 등급이력
     */
    getGradeHistory: function () {
        let userNo = $("#userNo").val();
        windowPopupOpen("/user/popup/gradeHistory?userNo=" + userNo,
            "userGradeHistory", 1100, 600);
    },
    /**
     * 회원 로그인로그
     */
    getLoginLog: function () {
        let userNo = $("#userNo").val();
        windowPopupOpen("/user/popup/loginLog?userNo=" + userNo,
            "userLoginLog", 1100, 600);
    },
    /**
     * 계좌 삭제
     */
    deleteBankAccount: function() {
        let seq = $("#bankSeq").val();
        if ($.jUtil.isEmpty(seq)) {
            alert('삭제할 계좌가 존재하지 않습니다.');
            return;
        }

        if (confirm("삭제하시겠습니까?")) {
            manageAjax('/user/userInfo/removeRefundAccount.json?seq=' + seq,
                'get', null, function (res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        userInfoDetail.search();
                    }
                });
        }
    },
    /**
     * 회원 등급변경 팝업
     */
    userGradeUpdate: function () {
        const userNo = $("#userNo").val();

        windowPopupOpen("/user/popup/userGradeUpdate?userNo=" + userNo,
            "userGradeUpdate", 1100, 815);
    },

    /**
     * 콜센터문의내역 탭 링크 연결
     */
    vocCallCenterMain: function () {
        const userNo = $("#userNo").val();
        const userNm = $("#userNm").text(); //이름은 화면내에 출력된값을 가져온다
        top.initialTab.addTab('menu_6_2_7', '콜센터문의관리', '/voc/callCenter/callCenterMain?userNo='+userNo+'&userNm='+userNm);
    },
};

const shippingGrid = {
    gridView : new RealGridJS.GridView("shippingGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        shippingGrid.initGrid();
        shippingGrid.initDataProvider();
        shippingGrid.event();
    },
    initGrid : function() {
        shippingGrid.gridView.setDataSource(shippingGrid.dataProvider);
        shippingGrid.gridView.setStyles(shippingAddressGridBaseInfo.realgrid.styles);
        shippingGrid.gridView.setDisplayOptions(shippingAddressGridBaseInfo.realgrid.displayOptions);
        shippingGrid.gridView.setColumns(shippingAddressGridBaseInfo.realgrid.columns);
        shippingGrid.gridView.setOptions(shippingAddressGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        shippingGrid.dataProvider.setFields(shippingAddressGridBaseInfo.dataProvider.fields);
        shippingGrid.dataProvider.setOptions(shippingAddressGridBaseInfo.dataProvider.options);
    },
    event : function() {
        shippingGrid.gridView.onDataCellClicked = function(gridView, index) {
            shippingGrid.gridView.checkItem(index.dataRow,
                !shippingGrid.gridView.isCheckedItem(index.dataRow));
        };
    },
    setData : function(dataList) {
        shippingGrid.dataProvider.clearRows();
        shippingGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = "application/json; charset=utf-8";

    CommonAjax.basic(params);
}