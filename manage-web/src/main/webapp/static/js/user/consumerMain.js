$(document).ready(function () {
    blackConsumerMain.init();
    blackConsumerDetailGrid.init();
    CommonAjaxBlockUI.global();
});

const SAVE_SUCCESS_MSG = "저장하였습니다.";
const RETURN_CODE_SUCCESS = "SUCCESS";
const blackConsumerMain = {
    originUserNm: null,
    originUserId: null,
    /**
     * init
     */
    init: function () {
        $("#memberSchButton").focus();

        this.buttonEvent();
        this.initReleaseDate('0d');
        blackConsumerDetailGrid.disabledBaseInfo(true);
    },
    /**
     * 기본정보 > 구분 > 제한 일자 초기화
     * @param startDate 설정할 시작일
     */
    initReleaseDate: function (startDate) {
        blackConsumerMain.setDate = Calendar.datePickerRange('startDt',
            'endDt');
        blackConsumerMain.setDate.setEndDate(0);
        blackConsumerMain.setDate.setStartDate(startDate);
        blackConsumerMain.setDate.setMinDate('startDt', 0);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    /**
     * button 이벤트
     */
    buttonEvent: function () {
        // 조회 폼 초기화
        $("#initBtn").on('click', function () {
            blackConsumerMain.reset();
        });

        // 검색
        $("#searchBtn").on('click', function () {
            blackConsumerMain.search();
        });
        // 검색 키 이벤트
        $('#userNm').on("keypress", function (e) {
            if (e.keyCode === 13) {
                blackConsumerMain.search();
            }
        });

        //저장
        $("#saveBtn").on('click', function () {
            //일련번호
            let seq = $("#seq").val();
            if ($.jUtil.isEmpty(seq)) {
                blackConsumerDetailGrid.save();
            } else {
                blackConsumerDetailGrid.update();
            }
        });
        //저장폼 초기화
        $("#resetBtn").on('click', function () {
            blackConsumerDetailGrid.reset();
        });

        // 기본정보 > 구분 셀렉트 박스에 따라 입력창 변경
        $("#isRelease").on('change', function () {
            const seq = $('#seq').val();
            if ($.jUtil.isEmpty(seq)) {
                alert("신규 입력 정보는 등록 버튼으로 저장하세요.");
                $("#isRelease").val("true");
                return;
            }
            let type = $('#isRelease option:selected').val();
            //String to boolean
            let booleanType = (type === 'true');
            blackConsumerDetailGrid.hideReasonDetailArea(booleanType);
        });
    },
    /**
     * 검색 폼 초기화
     *
     * 회원명/회원번호 (df 회원명)
     */
    reset: function () {
        $("#searchUserNo, #userNm").val("");

        blackConsumerDetailGrid.dataProvider.clearRows();
        blackConsumerDetailGrid.reset();
        blackConsumerDetailGrid.clearUserInfo();
        blackConsumerDetailGrid.disabledBaseInfo(true);

        blackConsumerMain.originUserId = null;
        blackConsumerMain.originUserNm = null;
    },
    /**
     * 데이터 조회
     */
    search: function () {
        // form 유효성 체크 이벤트
        if (blackConsumerMain.invalidSearch()) {
            return false;
        }
        const userNo = $("#searchUserNo").val();

        manageAjax('/user/blackConsumer/getConsumerList.json?userNo=' + userNo,
            'GET', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                } else {
                    blackConsumerDetailGrid.setData(res.data);
                    blackConsumerDetailGrid.reset();

                    $('#blackConsumerMainGridCount').html($.jUtil.comma(
                        blackConsumerDetailGrid.dataProvider.getRowCount()));

                    blackConsumerDetailGrid.setUserInfo(userNo);

                    blackConsumerDetailGrid.disabledBaseInfo(false);
                }
            });
    },
    /**
     * 검색조건 유효성 체크
     * @returns {boolean}
     */
    invalidSearch: function () {
        const userNm = $("#userNm");
        const userNo = $("#searchUserNo");

        if ($.jUtil.isEmpty(userNm.val()) && $.jUtil.isEmpty(userNo.val())) {
            $.jUtil.alert("회원을 조회하여 대상 회원을 검색해주세요.", "memberSchButton");
            return true;
        }
        return false;
    },
};

//blackConsumerDetailGrid
const blackConsumerDetailGrid = {
    gridView: new RealGridJS.GridView("blackConsumerDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        blackConsumerDetailGrid.initGrid();
        blackConsumerDetailGrid.initDataProvider();
        blackConsumerDetailGrid.event();
    },
    initGrid: function () {
        blackConsumerDetailGrid.gridView.setDataSource(
            blackConsumerDetailGrid.dataProvider);

        blackConsumerDetailGrid.gridView.setStyles(
            consumerHistoryGridBaseInfo.realgrid.styles);
        blackConsumerDetailGrid.gridView.setDisplayOptions(
            consumerHistoryGridBaseInfo.realgrid.displayOptions);
        blackConsumerDetailGrid.gridView.setColumns(
            consumerHistoryGridBaseInfo.realgrid.columns);
        blackConsumerDetailGrid.gridView.setOptions(
            consumerHistoryGridBaseInfo.realgrid.options);

        blackConsumerDetailGrid.gridView.setColumnProperty(
            blackConsumerDetailGrid.gridView.columnByName("isRelease"),
            "dynamicStyles", function (grid, index, value) {
            if (value === "등록") {
                return {foreground: "#FFFF0000"}
            }
        });
    },
    initDataProvider: function () {
        blackConsumerDetailGrid.dataProvider.setFields(
            consumerHistoryGridBaseInfo.dataProvider.fields);
        blackConsumerDetailGrid.dataProvider.setOptions(
            consumerHistoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        blackConsumerDetailGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            let data = blackConsumerDetailGrid.dataProvider.getJsonRow(
                index.dataRow);
            blackConsumerDetailGrid.getConsumer(data.seq);
        };
    },
    setData: function (data) {
        blackConsumerDetailGrid.dataProvider.clearRows();
        blackConsumerDetailGrid.dataProvider.setRows(data);
    },
    /**
     * 기본정보 영역 초기화
     */
    clearBaseInfo: function () {
        $("#seq").val("");
        $("#releaseDt").val("");

        $("#isRelease").val("true");
        $("#reasonType").val("");
        $("#consumerType").val("");
        $("#reasonDetail").val("");
        $("#releaseDetail").val("");

        blackConsumerMain.initReleaseDate("0");
    },
    /**
     * 기본정보 회원명 영역 초기화
     */
    clearUserInfo: function() {
        const userInfo = $("#userInfo");
        userInfo.html("회원을 검색해주세요.");
        userInfo.addClass("text-gray-Lightest");
        $("#userNo").val("");
    },
    /**
     * 기본정보 회원명 영역 설정
     */
    setUserInfo: function(userNo) {
        $("#userNo").val(userNo);
        $("#userInfo").removeClass("text-gray-Lightest");
        $("#userInfo").html(blackConsumerMain.originUserNm + "("
            + blackConsumerMain.originUserId + ")");
    },
    /**
     * 기본정보 영역 데이터 설정
     * @param data 설정할 데이터
     */
    setBaseInfo: function (data) {
        //기본정보 비활성화 처리
        blackConsumerDetailGrid.setDisabledBaseInfo(data.startDt, data.releaseDt);
        //해제영역 노출여부
        blackConsumerDetailGrid.hideReasonDetailArea(data.release);
        //해제사유 비활성화 처리
        blackConsumerDetailGrid.setDisableReleaseArea(data.endDt, data.releaseDt);

        //구분이 '해제'일 경우 disabled 처리
        if (data.release) {
            $('#isRelease').prop("disabled", false);
        } else {
            $('#isRelease').prop("disabled", true);
        }

        $("#userInfo").html(data.userNm + "(" + data.userId + ")");
        $("#userInfo").removeClass("text-gray-Lightest");
        $("#userNo").val(data.userNo);
        $("#seq").val(data.seq);
        $("#isRelease").val(data.release.toString());
        $("#reasonType").val(data.reasonType);
        $("#startDt").val(data.startDt);
        $("#endDt").val(data.endDt);
        $("#consumerType").val(data.consumerType);
        $("#reasonDetail").val(data.reasonDetail);
        $("#releaseDetail").val(data.releaseDetail);
        $("#releaseDt").val(data.releaseDt);
    },
    /**
     * 현재 날짜가 블랙컨슈머 기간 시작 전인지 또는 해제 전인지 체크하여 '기본정보 영역'을 비활성화 처리합니다.
     * @param startDt {string} 제한 시작일
     * @param releaseDt {String} 제한 해제일
     */
    setDisabledBaseInfo: function (startDt, releaseDt) {
        const now = moment();
        if (now.isBefore(startDt, 'day') && $.jUtil.isEmpty(releaseDt)) {
            blackConsumerDetailGrid.disabledBaseInfo(false);
        } else if(now.isBefore(startDt, 'day') && $.jUtil.isNotEmpty(releaseDt)) {
            blackConsumerDetailGrid.disabledBaseInfo(true);
        } else {
            blackConsumerDetailGrid.disabledBaseInfo(true);
        }
    },
    /**
     * 기본정보 영역 비활성화 처리<br/>
     * 파라미터의 값에 따라 기본 정보의 입력폼을 disabled 처리
     * @param bool {boolean} 비활성화 여부
     */
    disabledBaseInfo: function (bool) {
        $("#reasonType").prop("disabled", bool);
        $("#startDt").prop("disabled", bool);
        $("#endDt").prop("disabled", bool);
        $("#consumerType").prop("disabled", bool);
        $("#reasonDetail").prop("disabled", bool);
        $(".dateControl .ui-datepicker-trigger").attr("disabled", bool);
        $('#isRelease').prop("disabled", bool);
        $('#releaseDetail').prop("disabled", bool);

    },
    /**
     *  현재 날짜가 블랙컨슈머 기간 종료 전인지 또는 해제 전인지 체크하여 '해제사유'를 비활성화 처리합니다.
     * @param endDt {String} 종료일
     * @param releaseDt {String} 해제일
     */
    setDisableReleaseArea: function(endDt, releaseDt) {
        const now = moment();
        if (now.isSameOrBefore(endDt, 'day') && $.jUtil.isEmpty(releaseDt)) {
            $('#releaseDetail').prop("disabled", false);
        } else if (now.isSameOrBefore(endDt, 'day') && $.jUtil.isNotEmpty(releaseDt)) {
            $('#releaseDetail').prop("disabled", true);
        } else {
            $('#releaseDetail').prop("disabled", true);
        }
    },
    /**
     * 해제사유 영역 노출제어
     * @param bool {boolean} true일 경우 숨김, false일 경우 해제사유 노출
     */
    hideReasonDetailArea: function (bool) {
        if (bool) {
            $("#releaseDetailArea").hide();
        } else {
            $("#releaseDetailArea").show();
        }
    },
    /**
     * 저장
     * @return {boolean}
     */
    save: function () {
        //유효성 체크
        if (this.invalidSave()) {
            return false;
        }
        if (confirm("저장하시겠습니까?")) {
            //set
            $("#consumerType").attr('disabled', false);
            let data = _F.getFormDataByJson($("#inputForm"));
            //save
            if ($.jUtil.isEmpty(seq)) {
                manageAjax('/user/blackConsumer/addConsumer.json',
                    'POST', data, function (res) {
                        if (res.returnCode !== RETURN_CODE_SUCCESS) {
                            alert(res.returnMessage);
                        } else {
                            alert(SAVE_SUCCESS_MSG);
                            //초기화
                            blackConsumerDetailGrid.reset();
                            //조회
                            blackConsumerMain.search();
                        }
                    });
            }
        }
    },
    /**
     * 수정
     * @return {boolean}
     */
    update: function () {
        if (this.invalidUpdate()) {
            return false;
        }
        if (confirm("저장하시겠습니까?")) {
            //set
            let data = _F.getFormDataByJson($("#inputForm"));

            manageAjax('/user/blackConsumer/modifyConsumer.json',
                'POST', data, function (res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        alert(SAVE_SUCCESS_MSG);
                        blackConsumerDetailGrid.reset();
                        //조회
                        blackConsumerMain.search();
                    }
                });
        }
    },
    /**
     * 기본정보영역 초기화
     */
    reset: function () {
        if ($.jUtil.isNotEmpty($("#userNm").val())
            && $.jUtil.isNotEmpty($("#searchUserNo").val())
            && $.jUtil.isNotEmpty($("#userNo").val()) ) {

            blackConsumerDetailGrid.clearBaseInfo();
            blackConsumerDetailGrid.disabledBaseInfo(false);
            blackConsumerDetailGrid.hideReasonDetailArea(true);
        }
    },
    /**
     * 저장 전 유효성체크
     * @return {boolean} 유효성 체크 통과 실패시 true, 통과할 경우 false
     */
    invalidSave: function () {
        //회원번호
        const userNo = $('#userNo').val();
        if ($.jUtil.isEmpty(userNo)) {
            alert('회원을 조회하여 대상 회원을 검색해주세요.');
            return true;
        }
        // 사유 선택여부
        const reasonType = $('#reasonType option:selected').val();
        if ($.jUtil.isEmpty(reasonType)) {
            alert('사유를 선택해주세요.');
            $('#reasonType').focus();
            return true;
        }
        //권한정지 범위
        const consumerType = $('#consumerType option:selected').val();
        if ($.jUtil.isEmpty(consumerType)) {
            alert('권한정지범위를 선택해주세요.');
            $('#consumerType').focus();
            return true;
        }
        return false;
    },
    /**
     * 수정 전 유효성 체크
     * @return {boolean} 유효성 체크 통과 실패시 true, 통과할 경우 false
     */
    invalidUpdate: function () {
        //구분 - 블랙컨슈머 기간에 따라 등록|해제
        const isRelease = $('#isRelease option:selected').val();
        const releaseDetail = $('#releaseDetail');
        const userNo = $('#userNo').val();

        if (isRelease === "true") {
            //회원번호
            if ($.jUtil.isEmpty(userNo)) {
                alert('회원을 조회하여 대상 회원을 검색해주세요.');
                return true;
            }
            // 사유 선택여부
            if ($.jUtil.isEmpty(reasonType)) {
                alert('사유를 선택해주세요.');
                $('#reasonType').focus();
                return true;
            }
            //권한정지 범위
            const consumerType = $('#consumerType option:selected').val();
            if ($.jUtil.isEmpty(consumerType)) {
                alert('권한정지범위를 선택해주세요.');
                $('#consumerType').focus();
                return true;
            }
        } else {
            if ($.jUtil.isEmpty(releaseDetail.val())) {
                alert('해제사유를 입력해주세요.');
                releaseDetail.focus();
                return true;
            }
            if (releaseDetail.val().length > 100) {
                alert('해제사유는 100자 이내로 입력해주세요.');
                releaseDetail.focus();
                return true;
            }
        }
        return false;
    },
    /**
     * 상세조회
     * @param seq 일련번호
     */
    getConsumer: function (seq) {
        manageAjax('/user/blackConsumer/getConsumer.json?seq=' + seq,
            'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                } else {
                    if ($.jUtil.isNotEmpty(res.data)) {
                        blackConsumerDetailGrid.reset();
                        blackConsumerDetailGrid.setBaseInfo(res.data);
                    }
                }
            });
    },
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg) {
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = "application/json; charset=utf-8";

    CommonAjax.basic(params);
}

/**
 * 검색영역 회원조회 팝업 callback
 * @param res {ResponseObject.data} 조회 응답결과
 */
function callbackMemberSearchPop(res) {
    blackConsumerMain.reset();

    const userNm = $("#userNm");
    userNm.val(res.userNm);
    userNm.focus();
    $("#searchUserNo").val(res.userNo);

    blackConsumerMain.originUserId = res.originUserId;
    blackConsumerMain.originUserNm = res.originUserNm;
}