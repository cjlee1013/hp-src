const RETURN_CODE_SUCCESS = "SUCCESS";
const memberMemoMain = {
    originUserNm: null,
    originUserId: null,
    /**
     * init 이벤트
     */
    init: function(){
        $("#userSchButton").focus();
        memberMemoMain.buttonEvent();
        memberMemoMain.disabledDetail(true);
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){
        // 조회 폼 초기화
        $("#initBtn").on('click', function(){
            memberMemoMain.reset();
        });

        // 검색
        $("#searchBtn").on('click', function(){
            memberMemoMain.search();
        });

        // 저장
        $("#saveBtn").on('click', function(){
            memberMemoMain.addMemberMemo();
        });

        // 저장 초기화
        $("#resetBtn").on('click', function(){
            memberMemoMain.resetInput();
        });
    },
    /**
     * 검색 폼 초기화
     * 회원명/회원번호 (df 회원명)
     */
    reset: function(){
        $("#userNm, #searchUserNo").val("");

        memberMemoGrid.dataProvider.clearRows();

        memberMemoGrid.clearDetail();
        memberMemoGrid.clearUserInfo();
        memberMemoMain.disabledDetail(true);

        memberMemoMain.originUserId = null;
        memberMemoMain.originUserNm = null;
    },
    /**
     * 데이터 조회
     * 1. 검색어가 없을 경우 조회 불가
     * @returns {boolean}
     */
    search: function(){
        // form 유효성 체크
        if (this.invalidSearch()) {
            return false;
        }
        const userNo = $("#searchUserNo").val();

        manageAjax('/user/memberMemo/getMemberMemoList.json?userNo=' + userNo,
            'get', null, function (res) {
            if (res.returnCode !== RETURN_CODE_SUCCESS) {
                alert(res.returnMessage);
            } else {
                memberMemoGrid.setData(res.data);
                //count
                $('#memberMemoListCount').html(
                    $.jUtil.comma(memberMemoGrid.gridView.getItemCount()));

                memberMemoGrid.setUserInfo(userNo);
                memberMemoMain.disabledDetail(false);
            }
        });
    },

    /**
     * 검색조건 유효성 체크
     * @returns {boolean}
     */
    invalidSearch: function(){
        const userNm = $("#userNm");
        const userNo = $("#searchUserNo");

        if ($.jUtil.isEmpty(userNm.val()) && $.jUtil.isEmpty(userNo.val())) {
            $.jUtil.alert("회원을 검색하여 등록할 대상을 선택해주세요.", 'userNm');
            return true;
        }
        return false;
    },
    /**
     * 회원메모 등록
     */
    addMemberMemo: function() {
        const userNo = $("#userNo");
        const content = $("#content");

        if (memberMemoMain.invalidAddMemberMemo(userNo, content)) {
           return false;
        }

        let data = {};
        data.userNo = userNo.val();
        data.content = content.val();

        if (confirm("저장하시겠습니까?")) {
            manageAjax('/user/memberMemo/addMemberMemo.json',
                'post', JSON.stringify(data), function (res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        memberMemoGrid.clearDetail();
                        memberMemoMain.search();
                    }
                });
        }
    },
    invalidAddMemberMemo: function (userNo, content) {
        const seq = $("#seq");
        if ($.jUtil.isNotEmpty(seq.val())) {
            alert('기 등록된 회원메모 입니다.');
            return true;
        }

        if ($.jUtil.isEmpty(userNo.val())) {
            alert('회원을 조회하여 대상 회원을 검색해주세요');
            return true;
        }

        if ($.jUtil.isEmpty(content.val())) {
            $.jUtil.alert('회원메모를 입력해주세요.', 'content');
            return true;
        }
        return false;
    },
    /**
     * 저장 입력내용 초기화
     */
    resetInput: function () {
        if ($.jUtil.isNotEmpty($("#userNm").val())
            && $.jUtil.isNotEmpty($("#searchUserNo").val())
            && $.jUtil.isNotEmpty($("#userNo").val())) {

            memberMemoMain.disabledDetail(false);
            $("#seq").val("");
            $("#content").val("");
        }
    },
    /**
     * 상세정보 영역 비활성화 처리
     * @param bool {boolean} disabled 여부
     */
    disabledDetail: function(bool) {
        $("#content").prop("disabled", bool);
    }
};

const memberMemoGrid = {
    gridView : new RealGridJS.GridView("memberMemoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        memberMemoGrid.initGrid();
        memberMemoGrid.initDataProvider();
        memberMemoGrid.event();
    },
    initGrid : function(){
        memberMemoGrid.gridView.setDataSource(memberMemoGrid.dataProvider);
        memberMemoGrid.gridView.setStyles(memberMemoGridBaseInfo.realgrid.styles);
        memberMemoGrid.gridView.setDisplayOptions(memberMemoGridBaseInfo.realgrid.displayOptions);
        memberMemoGrid.gridView.setColumns(memberMemoGridBaseInfo.realgrid.columns);
        memberMemoGrid.gridView.setOptions(memberMemoGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        memberMemoGrid.dataProvider.setFields(memberMemoGridBaseInfo.dataProvider.fields);
        memberMemoGrid.dataProvider.setOptions(memberMemoGridBaseInfo.dataProvider.options);
    },
    event : function() {
        memberMemoGrid.gridView.onDataCellClicked = function(gridView, index) {
            let data = memberMemoGrid.dataProvider.getJsonRow(index.dataRow);
            memberMemoGrid.clearDetail();
            memberMemoGrid.getMemberMemo(data.seq);
        };
    },
    setData : function(dataList) {
        memberMemoGrid.dataProvider.clearRows();
        memberMemoGrid.dataProvider.setRows(dataList);
    },
    /**
     * 상세조회
     * @param seq 회원메모 일련번호
     */
    getMemberMemo: function (seq) {
        manageAjax('/user/memberMemo/getMemberMemo.json?seq=' + seq,
            'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                } else {
                    memberMemoGrid.setDetail(res.data);
                }
            });
    },
    /**
     * 상세정보 조회결과 바인딩
     * @param data 조회결과
     */
    setDetail: function (data) {
        $("#seq").val(data.seq);
        $("#content").val(data.content);

        memberMemoMain.disabledDetail(true);
    },
    /**
     * 상세정보 초기화
     */
    clearDetail: function () {
        $("#seq").val("");
        $("#content").val("");
    },
    /**
     * 상세정보 회원명 영역 초기화
     */
    clearUserInfo: function() {
        const userInfo = $("#userInfo");
        userInfo.html("회원을 검색해주세요.");
        userInfo.addClass("text-gray-Lightest");
        $("#userNo").val("");
    },
    /**
     * 상세정보 회원명 영역 설정
     */
    setUserInfo: function (userNo) {
        $("#userNo").val(userNo);
        const userInfo = $("#userInfo");
        userInfo.removeClass("text-gray-Lightest");
        userInfo.html(memberMemoMain.originUserNm + "("
            + memberMemoMain.originUserId + ")");
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.contentType = "application/json; charset=utf-8";
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    CommonAjax.basic(params);
}

/**
 * 회원조회 팝업 callback
 * @param res Response
 */
function callbackMemberSearchPop(res) {
    memberMemoMain.reset();

    const userNm = $("#userNm");
    userNm.val(res.userNm);
    userNm.focus();
    $("#searchUserNo").val(res.userNo);

    memberMemoMain.originUserId = res.originUserId;
    memberMemoMain.originUserNm = res.originUserNm;
}