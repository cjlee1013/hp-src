const userInfoMain = {
    /**
     * init 이벤트
     */
    init: function(){
        userInfoMain.buttonEvent();
        //키워드 포커스
        $("#keyword").focus();
        //회원정보관리 메뉴의 탭아이디 설정
        $("#userInfoMainTabId").val(parent.Tabbar.getActiveTab());
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){
        // 조회 폼 초기화
        $("#initBtn").on('click', function(){
            userInfoMain.reset();
        });

        // 검색
        $("#searchBtn").on('click', function(){
            userInfoMain.search();
        });

        // 검색 키 이벤트
        $('#keyword, #mobile3').on("keypress", function (e) {
            if (e.keyCode === 13) {
                userInfoMain.search();
            }
        });

        //휴대폰 번호 이동
        $("#mobile1").on("keyup", function () {
            if ($("#mobile1").val().length === 3) {
                $("#mobile2").focus();
            }
        });
        $("#mobile2").on("keyup", function () {
            if ($("#mobile2").val().length === 4) {
                $("#mobile3").focus();
            }
        });

        // 검색어 셀렉트 박스에 따라 입력창 변경
        $('#searchType').on('change', function(){
            let type = $('#searchType option:selected').val();
            userInfoMain.makeSearchTypeKeyword(type);
        });

    },

    /**
     * 검색 폼 초기화
     * 회원명/회원번호 (df 회원명)
     * 핸드폰 번호 초기화
     * 검색창 초기화
     */
    reset: function(){
        const DEFAULT_SEARCH_TYPE = "USER_NM";
        $("#searchType").val(DEFAULT_SEARCH_TYPE);
        userInfoMain.makeSearchTypeKeyword(DEFAULT_SEARCH_TYPE);
        // $("#keyword").val("");
        //
        // $('#mobile1, #mobile2, #mobile3').val("");
    },
    /**
     * 검색어 셀릭트박스 생성
     * 1. 회원명, 회원번호, 회원 ID, 휴대폰 번호
     * @param type 선택한 셀렉트박스 값
     */
    makeSearchTypeKeyword : function (type) {
        if (type === "MOBILE"){
            $('#searchKeywordSpan').hide();
            $('#mobileSpan').show();
            $('#mobile1').focus();
        } else {
            $('#searchKeywordSpan').show();
            $('#mobileSpan').hide();
            $('#keyword').focus();
        }

        $('#keyword').val("");
        $('#mobile1, #mobile2, #mobile3').val("");
    },
    /**
     * 데이터 조회
     * 1. 검색어가 없을 경우 조회 불가
     * @returns {boolean}
     */
    search: function(){
        // form 유효성 체크
        if (this.invalid()) {
            return false;
        }
        //set data
        let searchType = $('#searchType option:selected').val();
        if (searchType === "MOBILE") {
            let mobile = $("#mobile1").val() + $("#mobile2").val()
                + $("#mobile3").val();
            $("#keyword").val($.jUtil.phoneFormatter(mobile));
        }

        const data = $("#searchForm").serialize();
        manageAjax('/user/userInfo/getUserInfoList.json?' + data, 'get', null, function(res){
            userInfoGrid.setData(res);
            //count
            $('#searchUserMainListCount').html($.jUtil.comma(userInfoGrid.gridView.getItemCount()));
        });
    },

    /**
     * 검색조건 유효성 체크
     * @returns {boolean}
     */
    invalid: function(){
        const searchType = $('#searchType option:selected').val();
        const keyword = $("#keyword");
        const mobile1 = $("#mobile1").val();
        const mobile2 = $("#mobile2").val();
        const mobile3 = $("#mobile3").val();

        if ($.jUtil.isEmpty(keyword.val())
            && this.isEmptyMobile(mobile1, mobile2, mobile3)) {
            alert("회원정보 또는 휴대폰 번호를 입력하세요.");
            keyword.focus();
            return true;
        }

        if (searchType === "USER_NO"  && !$.jUtil.isEmpty(keyword.val())
            && !$.jUtil.isAllowInput(keyword.val(), ['NUM'])) {
            alert("숫자만 입력 가능합니다.");
            keyword.focus();
            return true;
        }

        //회원 명
        if (searchType === "USER_NAME" && !$.jUtil.isEmpty(keyword.val())
            && keyword.val().length < 2) {
            alert("두 글자 이상 입력해주세요");
            keyword.focus();
            return true;
        }

        //회원 아이디 - 영문, 숫자
        if (searchType === "USER_ID" && $.jUtil.isNotEmpty(keyword.val())
            && !$.jUtil.isAllowInput(keyword.val(), ['NUM', 'ENG', 'SPC_3'])) {
            alert("영문, 숫자, 일부 특수문자($, *, @) 만 사용 가능합니다.");
            $('#userId').focus();
            return true;
        }

        //회원 아이디 - 사이즈
        if (searchType === "USER_ID" && $.jUtil.isNotEmpty(keyword.val())
            && keyword.val().length < 2) {
            alert("두 글자 이상 입력해주세요");
            keyword.focus();
            return true;
        }
        //모바일 - 숫자
        if (searchType === "MOBILE" && userInfoMain.isNotEmptyMobile(mobile1, mobile2, mobile3)
            && userInfoMain.isNotNumTypeMobile(mobile1, mobile2, mobile3)) {
            alert("숫자만 입력 가능합니다.");
            $('#mobile1').focus();
            return true;
        }

        return false;
    },

    /**
     * 휴대폰 번호 empty check
     * @param mobile1 핸드폰 번호 첫번째 자리
     * @param mobile2 핸드폰 번호 두번째 자리
     * @param mobile3 핸드폰 번호 세번째 자리
     * @returns {boolean}
     */
    isEmptyMobile: function (mobile1, mobile2, mobile3) {
        return ($.jUtil.isEmpty(mobile1) && $.jUtil.isEmpty(mobile2)
            && $.jUtil.isEmpty(mobile3))
    },

    isNotEmptyMobile: function (mobile1, mobile2, mobile3) {
        return !userInfoMain.isEmptyMobile(mobile1, mobile2, mobile3);
    },

    /**
     * 휴대폰 번호가 숫자형인지 체크
     * @param mobile1 핸드폰 번호 첫번째 자리
     * @param mobile2 핸드폰 번호 두번째 자리
     * @param mobile3 핸드폰 번호 세번째 자리
     * @returns {boolean}
     */
    isNotNumTypeMobile: function (mobile1, mobile2, mobile3) {
        return !($.jUtil.isAllowInput(mobile1, ['NUM'])
            && $.jUtil.isAllowInput(mobile2,['NUM'])
            && $.jUtil.isAllowInput(mobile3, ['NUM']));
    },
};

const userInfoGrid = {
    gridView : new RealGridJS.GridView("userInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        userInfoGrid.initGrid();
        userInfoGrid.initDataProvider();
        userInfoGrid.event();
    },
    initGrid : function(){
        userInfoGrid.gridView.setDataSource(userInfoGrid.dataProvider);
        userInfoGrid.gridView.setStyles(userMainGridBaseInfo.realgrid.styles);
        userInfoGrid.gridView.setDisplayOptions(userMainGridBaseInfo.realgrid.displayOptions);
        userInfoGrid.gridView.setColumns(userMainGridBaseInfo.realgrid.columns);
        userInfoGrid.gridView.setOptions(userMainGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        userInfoGrid.dataProvider.setFields(userMainGridBaseInfo.dataProvider.fields);
        userInfoGrid.dataProvider.setOptions(userMainGridBaseInfo.dataProvider.options);
    },
    event : function() {
        userInfoGrid.gridView.onDataCellClicked = function(gridView, index) {
            let data = userInfoGrid.dataProvider.getJsonRow(index.dataRow);
            let parentTabId = $("#userInfoMainTabId").val();
            UIUtil.addTabWindowBesideParent(
                "/user/userInfo/detail?userNo=" + data.userNo, "userDetailTab",
                "회원정보 상세", 120, parentTabId);
        };
    },
    setData : function(dataList) {
        userInfoGrid.dataProvider.clearRows();
        userInfoGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}
