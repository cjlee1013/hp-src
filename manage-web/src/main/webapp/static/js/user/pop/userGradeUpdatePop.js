const RETURN_CODE_SUCCESS = "SUCCESS";
const userGradeUpdate = {
    /**
     * init 이벤트
     */
    init: function() {
        $("#userGrade").focus();

        userGradeUpdate.buttonEvent();
    },
    /**
     * 버튼 이벤트
     */
    buttonEvent: function(){
        /**
         * 콜센터 문의내역 조회
         */
        $("#getCallCenterInquiriesBtn").on('click', function(){
            userGradeUpdate.search();
        });

        $("#updateUserGradeBtn").on('click', function(){
            userGradeUpdate.update();
        });
    },
    /**
     * 회원 상세 조회
     * 회원 번호를 기준으로 상세 조회
     */
    search: function(){
        //조회
        let data = {};
        data.userNo = $("#userNo").text();

        manageAjax('/user/userInfo/getCallCenterInquiries.json', 'post',
            JSON.stringify(data), function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else {
                    userGradeUpdateGrid.setData(res.data);
                }
            });
    },
    update: function() {
        let data = {};
        data.userNo = $("#userNo").text();
        data.gradeSeq = $('select[name="userGrade"] option:selected').val();
        data.gradeNm = $('select[name="userGrade"] option:selected').text();
        data.orderCnt = $('#orderCnt').val();
        data.orderAmount = $('#orderAmount').val();
        data.reason = $('#reason').val();
        data.callCenterSeq = $('#callCenterSeq').val();

        if (userGradeUpdate.invalidForm(data)) {
            return false;
        }

        manageAjax('/user/userInfo/updateUserGrade.json',
            'post', JSON.stringify(data), function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                } else {
                    alert("회원등급이 변경되었습니다.");

                    userGradeUpdate.clearForm();
                    userGradeUpdateGrid.dataProvider.clearRows();
                }
            });
    },
    invalidForm: function (data) {
        const userGradeSeq = data.gradeSeq;
        const orderCnt = data.orderCnt;
        const orderAmount = data.orderAmount;
        const reason = data.reason;
        const callCenterSeq = data.callCenterSeq;

        if ($.jUtil.isEmpty(userGradeSeq)) {
            alert("등급을 선택하세요.");
            $('#userGrade').focus();
            return true;
        }

        if ($.jUtil.isEmpty(orderCnt)) {
            alert("구매건수를 입력하세요.");
            $('#orderCnt').focus();
            return true;
        }

        if ($.jUtil.isEmpty(orderAmount)) {
            alert("구매금액을 입력하세요.");
            $('#orderAmount').focus();
            return true;
        }

        if ($.jUtil.isEmpty(reason)) {
            alert("사유를 입력하세요.");
            $('#reason').focus();
            return true;
        }

        if ($.jUtil.isEmpty(callCenterSeq)) {
            alert("문의번호를 선택하세요.");
            $('#getCallCenterInquiriesBtn').focus();
            return true;
        }

        if ($.jUtil.isNotEmpty(orderCnt) && !$.jUtil.isAllowInput(orderCnt, ['NUM'])) {
            alert("숫자만 입력 가능합니다.");
            $('#orderCnt').focus();
            return true;
        }

        if ($.jUtil.isNotEmpty(orderAmount) && !$.jUtil.isAllowInput(orderAmount, ['NUM'])) {
            alert("숫자만 입력 가능합니다.");
            $('#orderAmount').focus();
            return true;
        }

        if ($.jUtil.isNotEmpty(reason) && !$.jUtil.isAllowInput(reason, ['NUM', 'ENG', 'KOR'])) {
            alert("한글/영문/숫자만 입력 가능합니다.");
            $('#reason').focus();
            return true;
        }
        return false;
    },
    /**
     * 입력창 초기화
     */
    clearForm: function () {
        $('#userGrade, #gradeSeq, #gradeNm').val('');
        $('#orderCnt, #orderAmount, #reason').val('');
        $('#callCenterReadOnly, #callCenterSeq').val('');
    }
};

const userGradeUpdateGrid = {
    gridView : new RealGridJS.GridView("userGradeUpdateGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        userGradeUpdateGrid.initGrid();
        userGradeUpdateGrid.initDataProvider();
        userGradeUpdateGrid.event();
    },
    initGrid : function(){
        userGradeUpdateGrid.gridView.setDataSource(userGradeUpdateGrid.dataProvider);
        userGradeUpdateGrid.gridView.setStyles(userGradeUpdateGridBaseInfo.realgrid.styles);
        userGradeUpdateGrid.gridView.setDisplayOptions(userGradeUpdateGridBaseInfo.realgrid.displayOptions);
        userGradeUpdateGrid.gridView.setColumns(userGradeUpdateGridBaseInfo.realgrid.columns);
        userGradeUpdateGrid.gridView.setOptions(userGradeUpdateGridBaseInfo.realgrid.options);
        userGradeUpdateGrid.gridView.setCheckBar({visible: true, exclusive: true, showAll: false});
    },
    initDataProvider : function() {
        userGradeUpdateGrid.dataProvider.setFields(userGradeUpdateGridBaseInfo.dataProvider.fields);
        userGradeUpdateGrid.dataProvider.setOptions(userGradeUpdateGridBaseInfo.dataProvider.options);
    },
    event : function() {
        userGradeUpdateGrid.gridView.onDataCellClicked = function(gridView, index) {
            userGradeUpdateGrid.gridView.checkItem(index.dataRow,
                !userGradeUpdateGrid.gridView.isCheckedItem(index.dataRow),
                true, userGradeUpdateGrid.checkEvent(index.dataRow));
        };
        userGradeUpdateGrid.gridView.onItemChecked = function(gridView, index, checked) {
            if (checked) {
                userGradeUpdateGrid.checkEvent(index);
            }
        };
    },
    setData : function(dataList) {
        userGradeUpdateGrid.dataProvider.clearRows();
        userGradeUpdateGrid.dataProvider.setRows(dataList);
    },
    checkEvent: function (selectRowId) {
        const rowDataJson = userGradeUpdateGrid.dataProvider.getJsonRow(selectRowId);
        $('#callCenterReadOnly').val(rowDataJson.key);
        $('#callCenterSeq').val(rowDataJson.key);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;
    params.contentType = "application/json; charset=utf-8";

    //content Type
    CommonAjax.basic(params);
}