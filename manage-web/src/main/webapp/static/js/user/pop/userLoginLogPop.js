const userLoginLog = {
    /**
     * init 이벤트
     */
    init: function () {
        userLoginLog.search();
    },
    /**
     * 회원 상세 조회
     * 회원 번호를 기준으로 상세 조회
     */
    search: function () {
        //조회
        let userNo = $("#userNo").val();
        manageAjax('/user/userInfo/loginLog.json?userNo=' + userNo,
            'get', null, function (res) {
                userLoginLogGrid.setData(res.data);
            });
    },
};

const userLoginLogGrid = {
    gridView: new RealGridJS.GridView("userLoginLogGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        userLoginLogGrid.initGrid();
        userLoginLogGrid.initDataProvider();
        userLoginLogGrid.event();
    },
    initGrid: function () {
        userLoginLogGrid.gridView.setDataSource(userLoginLogGrid.dataProvider);
        userLoginLogGrid.gridView.setStyles(
            userLoginLogGridBaseInfo.realgrid.styles);
        userLoginLogGrid.gridView.setDisplayOptions(
            userLoginLogGridBaseInfo.realgrid.displayOptions);
        userLoginLogGrid.gridView.setColumns(
            userLoginLogGridBaseInfo.realgrid.columns);
        userLoginLogGrid.gridView.setOptions(
            userLoginLogGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        userLoginLogGrid.dataProvider.setFields(
            userLoginLogGridBaseInfo.dataProvider.fields);
        userLoginLogGrid.dataProvider.setOptions(
            userLoginLogGridBaseInfo.dataProvider.options);
    },
    event: function () {
        userLoginLogGrid.gridView.onDataCellClicked = function (gridView,
            index) {
            //NO_OP
        };
    },
    setData: function (dataList) {
        userLoginLogGrid.dataProvider.clearRows();
        userLoginLogGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg) {
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({contentType: "application/json; charset=utf-8",});
    CommonAjax.basic(params);
}
