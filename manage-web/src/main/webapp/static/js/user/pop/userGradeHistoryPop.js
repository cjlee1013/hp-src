const userGradeHistory = {
    /**
     * init 이벤트
     */
    init: function(){
        userGradeHistory.search();
    },
    /**
     * 회원 상세 조회
     * 회원 번호를 기준으로 상세 조회
     */
    search: function(){
        //조회
        let userNo = $("#userNo").val();
        manageAjax('/user/userInfo/gradeHistory.json?userNo=' + userNo,
            'get', null, function (res) {
                userGradeHistoryGrid.setData(res.data);
            });
    },
};

const userGradeHistoryGrid = {
    gridView : new RealGridJS.GridView("userGradeHistoryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        userGradeHistoryGrid.initGrid();
        userGradeHistoryGrid.initDataProvider();
        userGradeHistoryGrid.event();
    },
    initGrid : function(){
        userGradeHistoryGrid.gridView.setDataSource(userGradeHistoryGrid.dataProvider);
        userGradeHistoryGrid.gridView.setStyles(userGradeHistoryGridBaseInfo.realgrid.styles);
        userGradeHistoryGrid.gridView.setDisplayOptions(userGradeHistoryGridBaseInfo.realgrid.displayOptions);
        userGradeHistoryGrid.gridView.setColumns(userGradeHistoryGridBaseInfo.realgrid.columns);
        userGradeHistoryGrid.gridView.setOptions(userGradeHistoryGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        userGradeHistoryGrid.dataProvider.setFields(userGradeHistoryGridBaseInfo.dataProvider.fields);
        userGradeHistoryGrid.dataProvider.setOptions(userGradeHistoryGridBaseInfo.dataProvider.options);
    },
    event : function() {
        userGradeHistoryGrid.gridView.onDataCellClicked = function(gridView, index) {
            //NO_OP
        };
    },
    setData : function(dataList) {
        userGradeHistoryGrid.dataProvider.clearRows();
        userGradeHistoryGrid.dataProvider.setRows(dataList);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}
