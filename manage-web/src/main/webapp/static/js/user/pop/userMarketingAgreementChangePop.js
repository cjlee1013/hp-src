const RETURN_CODE_SUCCESS = "SUCCESS";
const userMarketingAgreementChange = {
    /**
     * init 이벤트
     */
    init: function(){
        userMarketingAgreementChange.search();
        userMarketingAgreementChange.buttonEvent();
    },
    buttonEvent : function() {
        // 저장
        $('#saveBtn').on('click', function(){
            if (confirm("저장하시겠습니까?")) {
                alert("NO-OP");
            }
        });
        // 초기화
        $('#resetBtn').on('click', function(){
            if (confirm("초기화하시겠습니까?")) {
                alert("NO-OP");
            }
        });
    },
    /**
     * 회원 상세 조회
     * 회원 번호를 기준으로 상세 조회
     */
    search: function(){
        //조회
        let userNo = $("#userNo").val();
        manageAjax('/user/userInfo/marketingAgreementChange.json?userNo=' + userNo,
            'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                    return false;
                } else {
                    userMarketingAgreementChange.setData(res.data);
                }
            });
    },
    setData: function (data) {
        $("#userId").text(data.userId);
        $("#userNm").text(data.userNm);
        $("#userStatus").text(data.userStatus);

        data.targetrepYn === "Y" ? $("#targetrepYn").prop("checked", true) : $("#targetrepYn").prop("checked", false);
        data.smsrepYn === "Y" ? $("#smsrepYn").prop("checked", true) : $("#smsrepYn").prop("checked", false);
        data.emailrepYn === "Y" ? $("#emailrepYn").prop("checked", true) : $("#emailrepYn").prop("checked", false);
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}
