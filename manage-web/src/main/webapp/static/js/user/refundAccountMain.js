const RETURN_CODE_SUCCESS = "SUCCESS";
const refundAccountMain = {
    /**
     * init 이벤트
     */
    init: function(){
        refundAccountMain.buttonEvent();
        //키워드 포커스
        $("#keyword").focus();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){
        // 조회 폼 초기화
        $("#initBtn").on('click', function(){
            refundAccountMain.reset();
        });

        // 검색
        $("#searchBtn").on('click', function(){
            refundAccountMain.search();
        });

        // 검색 키 이벤트
        $('#keyword, #mobile3').on("keypress", function (e) {
            if (e.keyCode === 13) {
                refundAccountMain.search();
            }
        });

        //휴대폰 번호 이동
        $("#mobile1").on("keyup", function () {
            if ($("#mobile1").val().length === 3) {
                $("#mobile2").focus();
            }
        });
        $("#mobile2").on("keyup", function () {
            if ($("#mobile2").val().length === 4) {
                $("#mobile3").focus();
            }
        });

        // 검색어 셀렉트 박스에 따라 입력창 변경
        $('#searchType').on('change', function(){
            let type = $('#searchType option:selected').val();
            refundAccountMain.makeSearchTypeKeyword(type);
        });

        //환불계좌 삭제
        $("#rmRefundAccountBtn").on('click', function(){
            refundAccountGrid.removeRefundAccount();
        });
    },
    /**
     * 검색어 셀릭트박스 생성
     * 1. 회원명, 회원번호, 회원 ID, 휴대폰 번호
     * @param type 선택한 셀렉트박스 값
     */
    makeSearchTypeKeyword : function (type) {
        if (type === "MOBILE"){
            $('#searchKeywordSpan').hide();
            $('#mobileAreaSpan').show();
            $('#mobile1').focus();
        } else {
            $('#searchKeywordSpan').show();
            $('#mobileAreaSpan').hide();
            $('#keyword').focus();
        }
        //입력창 clear
        $('#keyword').val("");
        $('#mobile1, #mobile2, #mobile3').val("");
    },

    /**
     * 검색 폼 초기화
     * 회원명/회원번호 (df 회원명)
     * 핸드폰 번호 초기화
     * 검색창 초기화
     */
    reset: function(){
        $("#searchType").val("USER_NM");
        $("#keyword").val("");

        $("#mobile1").val("");
        $("#mobile2").val("");
        $("#mobile3").val("");
    },
    /**
     * 데이터 조회
     * 1. 검색어가 없을 경우 조회 불가
     * @returns {boolean}
     */
    search: function(){
        // form 유효성 체크
        if (this.invalid()) {
            return false;
        }

        //set mobile
        let searchType = $('#searchType option:selected').val();
        if (searchType === "MOBILE") {
            let mobile = $("#mobile1").val() + $("#mobile2").val() + $(
                "#mobile3").val();
            $("#keyword").val($.jUtil.phoneFormatter(mobile));
        }

        const data = $("#searchForm").serialize();
        manageAjax('/user/refundAccount/getRefundAccountList.json?' + data, 'get', null, function(res){
            if (res.returnCode !== RETURN_CODE_SUCCESS) {
                alert(res.returnMessage);
            } else {
                refundAccountGrid.setData(res.data);
                //count
                $('#refundAccountListCount').html($.jUtil.comma(refundAccountGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 검색조건 유효성 체크
     * @returns {boolean}
     */
    invalid: function(){
        const searchType = $('#searchType option:selected').val();
        const keyword = $("#keyword");
        const mobile1 = $("#mobile1").val();
        const mobile2 = $("#mobile2").val();
        const mobile3 = $("#mobile3").val();

        if ($.jUtil.isEmpty(keyword.val())
            && this.isEmptyMobile(mobile1, mobile2, mobile3)) {
            alert("회원정보 또는 휴대폰 번호를 입력하세요.");
            keyword.focus();
            return true;
        }

        if (searchType === "USER_NO"  && !$.jUtil.isEmpty(keyword.val())
            && !$.jUtil.isAllowInput(keyword.val(), ['NUM'])) {
            alert("숫자만 입력 가능합니다.");
            keyword.focus();
            return true;
        }

        //회원 명
        if (searchType === "USER_NM" && !$.jUtil.isEmpty(keyword.val())
            && keyword.val().length < 2) {
            alert("두 글자 이상 입력해주세요");
            keyword.focus();
            return true;
        }

        //회원 아이디 - 영문, 숫자
        if (searchType === "USER_ID" && !$.jUtil.isEmpty(keyword.val())
            && !$.jUtil.isAllowInput(keyword.val(), ['NUM', 'ENG'])) {
            alert("영문, 숫자만 사용 가능합니다.");
            $('#userId').focus();
            return true;
        }

        //회원 아이디 - 사이즈
        if (searchType === "USER_ID" && !$.jUtil.isEmpty(keyword.val())
            && keyword.val().length < 2) {
            alert("두 글자 이상 입력해주세요");
            keyword.focus();
            return true;
        }
        //모바일 - 숫자
        if (searchType === "MOBILE" && refundAccountMain.isNotEmptyMobile(mobile1, mobile2, mobile3)
            && refundAccountMain.isNotNumTypeMobile(mobile1, mobile2, mobile3)) {
            alert("숫자만 입력 가능합니다.");
            $('#mobile1').focus();
            return true;
        }
        return false;
    },

    /**
     * 휴대폰 번호 empty check
     * @param mobile1 핸드폰 번호 첫번째 자리
     * @param mobile2 핸드폰 번호 두번째 자리
     * @param mobile3 핸드폰 번호 세번째 자리
     * @returns {boolean}
     */
    isEmptyMobile: function (mobile1, mobile2, mobile3) {
        return ($.jUtil.isEmpty(mobile1) && $.jUtil.isEmpty(mobile2)
            && $.jUtil.isEmpty(mobile3))
    },

    isNotEmptyMobile: function (mobile1, mobile2, mobile3) {
        return !refundAccountMain.isEmptyMobile(mobile1, mobile2, mobile3);
    },

    /**
     * 휴대폰 번호가 숫자형인지 체크
     * @param mobile1 핸드폰 번호 첫번째 자리
     * @param mobile2 핸드폰 번호 두번째 자리
     * @param mobile3 핸드폰 번호 세번째 자리
     * @returns {boolean}
     */
    isNotNumTypeMobile: function (mobile1, mobile2, mobile3) {
        return !($.jUtil.isAllowInput(mobile1, ['NUM'])
            && $.jUtil.isAllowInput(mobile2,['NUM'])
            && $.jUtil.isAllowInput(mobile3, ['NUM']));
    },
};

const refundAccountGrid = {
    gridView : new RealGridJS.GridView("refundAccountGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        refundAccountGrid.initGrid();
        refundAccountGrid.initDataProvider();
        refundAccountGrid.event();
    },
    initGrid : function(){
        refundAccountGrid.gridView.setDataSource(refundAccountGrid.dataProvider);
        refundAccountGrid.gridView.setStyles(refundAccountGridBaseInfo.realgrid.styles);
        refundAccountGrid.gridView.setDisplayOptions(refundAccountGridBaseInfo.realgrid.displayOptions);
        refundAccountGrid.gridView.setColumns(refundAccountGridBaseInfo.realgrid.columns);
        refundAccountGrid.gridView.setOptions(refundAccountGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        refundAccountGrid.dataProvider.setFields(refundAccountGridBaseInfo.dataProvider.fields);
        refundAccountGrid.dataProvider.setOptions(refundAccountGridBaseInfo.dataProvider.options);
    },
    event : function() {
        refundAccountGrid.gridView.onDataCellClicked = function(gridView, index) {
            let data = refundAccountGrid.dataProvider.getJsonRow(index.dataRow);
            refundAccountGrid.clearDetail();
            refundAccountGrid.getRefundAccount(data.seq);
        };
    },
    setData : function(dataList) {
        refundAccountGrid.dataProvider.clearRows();
        refundAccountGrid.dataProvider.setRows(dataList);
    },
    getRefundAccount: function (seq) {
        manageAjax('/user/refundAccount/getRefundAccount.json?seq=' + seq,
            'get', null, function (res) {
                if (res.returnCode !== RETURN_CODE_SUCCESS) {
                    alert(res.returnMessage);
                } else {
                    refundAccountGrid.setDetail(res.data);
                }
            });
    },
    setDetail: function (data) {
        $("#userNo").html(data.userNo);
        $("#userNm").html(data.userNm);
        $("#userId").html(data.userId);
        $("#bankNm").html(data.bankNm);
        $("#bankAccount").html(data.bankAccount);
        $("#bankOwner").html(data.bankOwner);
        $("#seq").val(data.seq);
    },
    clearDetail: function () {
        $("#userNo").text("");
        $("#userNm").text("");
        $("#userId").text("");
        $("#bankNm").text("");
        $("#bankAccount").text("");
        $("#bankOwner").text("");
        $("#seq").val();
    },
    removeRefundAccount: function() {
        let seq = $("#seq").val();
        if ($.jUtil.isEmpty(seq)) {
            alert('환불계좌를 조회하여 삭제할 대상을 선택해주세요.');
            return;
        }

        if (confirm("삭제하시겠습니까?")) {
            manageAjax('/user/refundAccount/removeRefundAccount.json?seq=' + seq,
                'get', null, function (res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        refundAccountGrid.clearDetail();
                        refundAccountMain.search();
                    }
                });
        }
    }
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    //content Type
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });
    CommonAjax.basic(params);
}
