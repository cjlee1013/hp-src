const shipChangeSlot = {
    init : function () {
        this.bindingEvent();
        this.pageInit();
        orderUtil.setWindowAutoResize();
    },
    pageInit : function () {
        $('#auroraArea, #pickerArea, #anytimeArea').hide();
        shipChangeSlot.orderShipSearch();
        shipChangeSlot.getChangeableInfo();
    },
    bindingEvent : function () {
        $('#saveBtn').on('click', function () {
            const selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id');
            if($.jUtil.isEmpty(selectedInfo)) return ;
            shipChangeSlot.setShipChange(selectedInfo);
        });
    },
    getChangeableInfo : function () {
        const dataParam = {
            'bundleNo' : parameter.bundleNo,
            'purchaseOrderNo' : parameter.purchaseOrderNo
        };
        CommonAjax.basic(
            {
                url:'/order/getShipSlotInfo.json?'.concat(jQuery.param(dataParam)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if(res.returnCode === '0000' || res.returnCode === 'SUCCESS'){
                        $('#slotChangeArea').append(orderUtil.writeToSlotTable(res.data));
                        shipChangeSlot.setRadioButton();
                        orderUtil.setWindowAutoResize();
                    }
                }
            });
    },
    orderShipSearch : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderShipAddrInfo.json?bundleNo=' + parameter.bundleNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToData(res);
                    if(!$.jUtil.isEmpty(res[0].auroraShipMsgCd) || !$.jUtil.isEmpty(res[0].auroraShipMsg)){
                        $('#auroraArea').show();
                    }
                    if(!$.jUtil.isEmpty(res[0].placeNm) || !$.jUtil.isEmpty(res[0].lockerNo) || !$.jUtil.isEmpty(res[0].lockerPasswd)){
                        $('#pickerArea').show();
                    }
                    if(res[0].anytimeSlotYn === 'Y') {
                        $('#anytimeArea').show();
                    }
                    orderUtil.setWindowAutoResize();
                }
            });
    },
    setRadioButton : function (){
        const radioId = [
            ($.jUtil.isEmpty($('#placeNm').text()) ? 'dict' : 'pick'),
            $('#slotShipDt').text().split('-').join(''),
            $('#slotId').text().substr(0, 3),
            $('#slotId').text()
        ].join("_");
        $('#' + radioId).attr("checked", true);
    },
    setShipChange : function (_idInfo) {
        const idInfo = orderUtil.getSplitData(_idInfo);
        const dataParam = {
            'bundleNo' : parameter.bundleNo,
            'purchaseOrderNo' : parameter.purchaseOrderNo,
            'userNo' : parameter.userNo,
            'chgShipDt' : orderUtil.toDateFormatting(idInfo[1]),
            'chgShiftId' : idInfo[2],
            'chgSlotId' : idInfo[3]
        }
        if (confirm("수정된 배송시간을 저장하시겠습니까?")) {
            CommonAjax.basic(
                {
                    url: '/order/modifyShipSlotInfo.json?'.concat($.param(dataParam)),
                    data: null,
                    contentType: 'application/json',
                    method: "GET",
                    successMsg: null,
                    callbackFunc: function (res) {
                        if ("0000,SUCCESS".indexOf(res.returnCode) > -1) {
                            alert("정상적으로 변경되었습니다.");
                            opener.location.reload();
                            self.close();
                        } else {
                            alert(res.returnMessage);
                        }
                    }
                });
        }
    }
};