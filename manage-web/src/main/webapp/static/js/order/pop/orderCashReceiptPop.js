const orderCashReceiptPop = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderCashReceiptInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        var cashReceiptInfo = $('#cashReceiptInfo');
                        if($.jUtil.isNotEmpty(res.data)){
                            if(res.data.cashReceiptUsage === 'NONE'){
                                cashReceiptInfo.html('미발행');
                            } else {
                                cashReceiptInfo.html(orderCashReceiptPop.covertCashType(res.data.cashReceiptType).concat(' : (').concat(res.data.cashReceiptReqNo).concat(')'));
                            }
                        } else {
                            cashReceiptInfo.html('발행내역이 없습니다.');
                        }
                    }
                }
            });
    },
    covertCashType : function (_type) {
        var typeName = '';
        switch (_type) {
            case 'PHONE' :
                typeName = "휴대폰번호";
                break;
            case 'CARD' :
                typeName = "현금영수증카드번호";
                break;
            case 'BUSIN' :
                typeName = "사업자번호";
                break;
            default :
                break;
        }
        return typeName;
    }
};