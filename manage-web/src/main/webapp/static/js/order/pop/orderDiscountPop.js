const orderPromoInfo = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderPromoInfoGrid.init();
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderPromoInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderPromoInfoGrid.setData(res);
                }
        });
    }
};
const orderCouponDiscountInfo = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderCouponInfoGrid.init();
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderCouponDiscountInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderCouponInfoGrid.setData(res);
                    let itemDiscountAmt = 0;
                    let shipDiscountAmt = 0;
                    let cartDiscountAmt = 0;
                    let addDiscountAmt = 0;
                    for(var i=0;i<res.length;i++){
                        if(res[i].discountKind.search("card") > -1 || res[i].discountKind.search("item") > -1){
                            itemDiscountAmt += parseInt(res[i].discountAmt);
                        } else if(res[i].discountKind.search("ship") > -1){
                            shipDiscountAmt += parseInt(res[i].discountAmt);
                        } else if(res[i].discountKind.search("cart") > -1){
                            cartDiscountAmt += parseInt(res[i].discountAmt);
                        } else if(res[i].discountKind.search("add") > -1) {
                            addDiscountAmt += parseInt(res[i].discountAmt);
                        }
                    }
                    if(itemDiscountAmt > 0){
                        $('#itemDiscountAmt').html($.jUtil.comma(itemDiscountAmt));
                    }
                    if(shipDiscountAmt > 0){
                        $('#shipDiscountAmt').html($.jUtil.comma(shipDiscountAmt));
                    }
                    if(cartDiscountAmt > 0){
                        $('#cartDiscountAmt').html($.jUtil.comma(cartDiscountAmt));
                    }
                    if(addDiscountAmt > 0){
                        $('#addDiscountAmt').html($.jUtil.comma(addDiscountAmt));
                    }
                    $('#totalDiscountAmt').html($.jUtil.comma(itemDiscountAmt + shipDiscountAmt + cartDiscountAmt + addDiscountAmt));
                }
            });
    }
};
const orderGiftInfo = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderGiftInfoGrid.init();
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderGiftInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderGiftInfoGrid.setData(res);
                }
            });
    }
};
const orderSaveInfo = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderSaveInfoGrid.init();
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
        };
        CommonAjax.basic(
            {
                url:'/order/orderSaveInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderSaveInfoGrid.setData(res);
                }
            });
    }
};