var NoRcvCommon = {
    pageLoading :function () {
        orderUtil.setWindowAutoResize();
        if ($(document).innerWidth() > 600) {
            orderUtil.setWindowResize((600 - $(document).innerWidth()), 0);
        }
    }
};

var noRcvShipReg = {
    init : function () {
        NoRcvCommon.pageLoading();
        this.bindingEvent();
    },
    validate : function () {
        var noRcvDeclrType = $('#noRcvDeclrType');
        if ($.jUtil.isEmpty(noRcvDeclrType.val())) {
            alert('신고사유를 선택하세요.');
            return false;
        }
        var noRcvDetailReason = $('#noRcvDetailReason');
        if ($.jUtil.isEmpty(noRcvDetailReason.val())) {
            alert('상세 사유를 입력 해 주세요.');
            noRcvDetailReason.focus();
            return false;
        }
        return true;
    },
    setNoRcvShipRegData : function () {
        if (!this.validate()) return;
        var dataParam = {
            'noRcvDeclrType' : $('#noRcvDeclrType').val(),
            'noRcvDetailReason' : $('#noRcvDetailReason').val(),
            'orderItemNo' : noRcvShipParam.orderItemNo,
            'bundleNo' : noRcvShipParam.bundleNo
        };

        CommonAjax.basic({
            url: '/order/addNoReceiveReg.json',
            data: JSON.stringify(dataParam),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                    alert("미수취 등록에 성공하였습니다.");
                    opener.location.reload();
                    self.close();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    bindingEvent : function () {
        $('#noRcvDetailReason').calcTextLength('keyup', '#textCountKr_noRcvDetailReason');
        $('#saveBtn').bindClick(function (){ noRcvShipReg.setNoRcvShipRegData(); });
    }
};

var noRcvShipEdit = {
    init : function () {
        this.pageInit();
        NoRcvCommon.pageLoading();
        this.bindingEvent();
    },
    pageInit : function () {
        this.getNoRcvSearch();
    },
    validate : function () {

        var noRcvProcessType = $('#noRcvProcessType');
        if ($.jUtil.isEmpty(noRcvProcessType.val())) {
            alert('미수취신고 처리 타입을 선택해주세요.');
            return false;
        }
        if(noRcvProcessType.val() === 'C'){
            var noRcvProcessCntnt = $('#noRcvProcessCntnt');
            if ($.jUtil.isEmpty(noRcvProcessCntnt.val())) {
                alert('미수취 신고 처리 내용을 입력 해 주세요.');
                noRcvProcessCntnt.focus();
                return false;
            }
        }
        return true;
    },
    getNoRcvSearch : function () {
        CommonAjax.basic(
            {
                url: '/order/getNoReceiveInfo.json',
                data: JSON.stringify(noRcvShipParam),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToSpan('noRcvProcessModifyForm', res);
                    orderUtil.setWindowAutoResize();
                }
            });
    },
    setNoRcvShipRegData : function () {
        if (!this.validate()) return false;
        var dataParam = {
            'purchaseOrderNo' : noRcvShipParam.purchaseOrderNo,
            'orderItemNo' : noRcvShipParam.orderItemNo,
            'shipNo' : noRcvShipParam.shipNo,
            'bundleNo' : noRcvShipParam.bundleNo,
            'noRcvProcessType' : $('#noRcvProcessType').val(),
            'noRcvProcessCntnt' : $('#noRcvProcessCntnt').val()
        };

        if (confirm('미수취 신고 '.concat(dataParam.noRcvProcessType === 'C' ? '철회를' : '철회요청을').concat(' 하시겠습니까?'))) {
            CommonAjax.basic({
                url: '/order/modifyNoReceiveInfo.json',
                data: JSON.stringify(dataParam),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                        opener.location.reload();
                        self.close();
                    }
                }
            });
        }
    },
    bindingEvent : function () {
        $('#noRcvProcessCntnt').calcTextLength('keyup', '#textCountKr_noRcvProcessCntnt');
        $('#saveBtn').bindClick(function () { noRcvShipEdit.setNoRcvShipRegData(); });
        $('#noRcvProcessType').on('change', function () {
            const noRcvProcessCntntArea = $('#noRcvProcessCntntArea');
            switch ($(this).val()) {
                case 'C' :
                    noRcvProcessCntntArea.show();
                    break;
                case 'W' :
                default :
                    noRcvProcessCntntArea.css('display', 'none');
                    break;
            }
            orderUtil.setWindowAutoResize();
        });
    }
};

var noRcvShipComplete = {
    init : function () {
        this.pageInit();
        NoRcvCommon.pageLoading();
    },
    pageInit : function () {
        this.getNoRcvSearch();
    },
    getNoRcvSearch : function () {
        CommonAjax.basic(
            {
                url: '/order/getNoReceiveInfo.json',
                data: JSON.stringify(noRcvShipParam),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    if (res.noRcvProcessType === 'C') {
                        $('#completeTitle').html("미수취 신고처리 내역");
                    }
                    if (!$.jUtil.isEmpty(res.noRcvProcessCntnt)) {
                        $('#noRcvProcessCntntArea').show();
                    }
                    orderUtil.writeToSpan('noRcvProcessCompleteForm', res);
                    orderUtil.setWindowAutoResize();
                }
            });
    }
};