var invoiceCommon = {
    validate : function (_grid) {
        var invoiceNo = $('#invoiceNo');
        var shipMethod = $('#shipMethod option:selected').text();
        if(shipMethod === '택배배송' ) {
            if($.jUtil.isEmpty($('#dlvCd').val())){
                alert('택배사를 선택해 주세요.');
                return false;
            }

            if($.jUtil.isEmpty(invoiceNo.val().length) || invoiceNo.val().length > 30){
                if(invoiceNo.val().length > 30){
                    alert('송장번호는 30자 이내로 입력하세요.');
                } else {
                    alert('송장번호가 입력되지 않았습니다.');
                }
                invoiceNo.focus();
                return false;
            }

            if(!$.jUtil.isAllowInput(invoiceNo.val(), ['NUM','ENG'])){
                alert("유효하지 않는 운송장 번호 입니다.");
                invoiceNo.focus();
                return false;
            }
        } else if(shipMethod === '직접배송') {
            if($.jUtil.isEmpty($('#scheduleShipDt').val())){
                alert('배송 예정일을 확인하세요.');
                return false;
            }
        }

        var checkedRows = _grid.gridView.getCheckedRows(true);
        if(checkedRows.length === 0) {
            alert('선택한 배송건이 없습니다.');
            return false;
        }
        if(invoiceNo.length > 0) {
            for(var idx in checkedRows) {
                var rowInvoiceNo = _grid.dataProvider.getValue(checkedRows[idx], 'invoiceNo');
                // var checkedOrderItemNo = _grid.dataProvider.getValue(_rowId, 'orderItemNo');
                if(invoiceNo.val() === rowInvoiceNo){
                    alert("이전 송장번호와 동일합니다.");
                    invoiceNo.focus();
                    return false;
                }
            }
        }

        return true;
    },
    actionEvent : function () {
        $('#shipMethod').on("change", function () {

            var inputArea = $('#invoiceInputArea');
            var selectArea = $('#dlvSelectArea');
            var scheduleArea = $('#scheduleInputArea');

            switch (orderUtil.getSplitData($(this).val())[1]) {
                case 'DRCT' :
                    orderUtil.setToggleOffDisplay(inputArea, selectArea);
                    orderUtil.setToggleOnDisplay(scheduleArea);
                    return;
                case 'QUICK' :
                case 'POST' :
                case 'PICK' :
                    orderUtil.setToggleOffDisplay(inputArea, selectArea, scheduleArea);
                    return;
                case 'DLV' :
                default :
                    orderUtil.setToggleOffDisplay(scheduleArea);
                    orderUtil.setToggleOnDisplay(inputArea, selectArea);
                    return;
            }
        });
    },
    communication : function (_bundleNo, _invoiceNo, _dlvCd, _shipMethod, _scheduleShipDt) {
        var parameter = {
            'bundleNo' : _bundleNo,
            'invoiceNo' : _invoiceNo.val(),
            'dlvCd' : _dlvCd.val(),
            'shipMethod' : _shipMethod.val(),
            'scheduleShipDt' : _scheduleShipDt.val()
        };
        if(orderUtil.getSplitData(parameter.shipMethod)[1].toUpperCase() === 'DRCT') {
            parameter.scheduleShipDt = _scheduleShipDt.val();
        }
        return parameter;
    }
};

var orderInvoiceReg = {
    init : function () {
        this.pageInit();
        this.dateInit();
        this.search();
        this.bindingEvent();
        invoiceCommon.actionEvent();
    },
    pageInit : function () {
        orderInvoiceRegGrid.init();
        orderUtil.setWindowAutoResize();
        invoiceCommon.actionEvent();
    },
    dateInit : function () {
        var date = Calendar.datePicker('scheduleShipDt');
        // date.setDate(0);
        // date.setMaxDate('30d');
        // $("#scheduleShipDt").datepicker("option", "minDate", 0);
    },
    search : function () {
        // var formId = $('#groupSearch').find('form').attr('id');
        var invoiceRegParam = {
            'bundleNo' : bundleNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderInvoiceRegInfo.json?'.concat(jQuery.param(invoiceRegParam)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderInvoiceRegGrid.setData(res);
                }
            });
    },
    setInvoiceRegData : function () {
        if(!invoiceCommon.validate(orderInvoiceRegGrid)) return ;

        CommonAjax.basic({
            url:'/order/invoiceReg.json',
            data: JSON.stringify(invoiceCommon.communication(bundleNo, $('#invoiceNo'), $('#dlvCd'), $('#shipMethod'), $('#scheduleShipDt'))),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                    alert("송장등록에 성공하였습니다.");
                    opener.location.reload();
                    self.close();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    bindingEvent : function () {
        $('#saveBtn').bindClick(function() { orderInvoiceReg.setInvoiceRegData(); });
    }
};

var orderInvoiceEdit = {
    init : function () {
        this.pageInit();
        this.dateInit();
        this.search();
        this.bindingEvent();
        invoiceCommon.actionEvent()
    },
    pageInit : function () {
        orderInvoiceEditGrid.init();
        orderUtil.setWindowAutoResize();
        invoiceCommon.actionEvent();
    },
    dateInit : function () {
        var date = Calendar.datePicker('scheduleShipDt');
        // date.setDate(0);
        // date.setMaxDate('30d');
    },
    search : function () {
        CommonAjax.basic({
            url:'/order/getOrderInvoiceEditInfo.json?bundleNo='.concat(bundleNo),
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc: function(res) {
                orderInvoiceEditGrid.setData(res);
            }
        });
    },
    setInvoiceEditData : function () {
        if(!invoiceCommon.validate(orderInvoiceEditGrid)) return ;
        // var orderItemNo = [];
        // for(var rowId of orderInvoiceEditGrid.gridView.getCheckedItems()){
        //     orderItemNo.push(orderInvoiceEditGrid.gridView.getValue(rowId, 'orderItemNo'));
        // }

        CommonAjax.basic({
            url:'/order/invoiceEdit.json',
            data: JSON.stringify(invoiceCommon.communication(bundleNo, $('#invoiceNo'), $('#dlvCd'), $('#shipMethod'), $('#scheduleShipDt'))),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                    opener.location.reload();
                    alert("송장수정에 성공하였습니다.");
                    self.close();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    bindingEvent : function () {
        $('#saveBtn').bindClick(function () { orderInvoiceEdit.setInvoiceEditData(); });
    }
};

var orderShipComplete = {
    init : function () {
        this.pageInit();
        this.search();
        this.bindingEvent();
    },
    pageInit : function () {
        orderShipCompleteGrid.init();
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        var invoiceParam = {
            'bundleNo' : bundleNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderInvoiceCompleteInfo.json?'.concat(jQuery.param(invoiceParam)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderShipCompleteGrid.setData(res);
                }
            });
    },
    bindingEvent : function () {
        $('#saveBtn').bindClick(function() { orderShipComplete.setShipCompleteData(); });
    },
    setShipCompleteData : function () {
        if(!invoiceCommon.validate(orderShipCompleteGrid)) return ;
        var shipNoList = [];
        var items = orderShipCompleteGrid.gridView.getCheckedItems();
        for(var idx in items){
            shipNoList.push(orderShipCompleteGrid.gridView.getValue(items[idx], 'shipNo'));
        }
        var shipComplete = {
            'shipNo' : shipNoList,
            'bundleNo' : bundleNo
        }

        CommonAjax.basic({
            url:'/order/shipComplete.json',
            data: JSON.stringify(shipComplete),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                    opener.location.reload();
                    alert("배송완료에 성공하였습니다.");
                    self.close();
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
};