/**
 * 주문관리 > 취소/교환/환불 상품 정보 리스트
 */
$(document).ready(function () {
    multiShipItemInfoLayout.init();
});

var multiShipItemInfoLayout = {

    /**
     * 초기화
     */
    init: function () {
        multiShipItemInfoLayout.getOrderInfoList();
        multiShipItemInfoLayout.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function () {
        $('#selectAllQty').bindChange(multiShipItemInfoLayout.selectAllQty);

        $('#multiShipList').change(function() {
            if($("input:checkbox[id=selectAllQty]").is(":checked")){
                $("input:checkbox[id=selectAllQty]").prop("checked", false);
            }
            multiShipItemInfoLayout.getOrderInfoList();
        });

    },

    /**
     * 수량 전체선택 이벤트
     * **/
    selectAllQty : function (){
        var selectAllQty = $("input:checkbox[id=selectAllQty]").is(":checked");

        if(selectAllQty){
            $("#itemInfoTable tbody tr").each(function () {
                var claimQty = $(this).find("option:last").val();//최대수량
                $(this).find("select").val(claimQty).attr("selected", "selected");
                $(this).find("select").attr("disabled", "disabled");
            });
        } else {
            $("#itemInfoTable tbody tr").each(function () {
                $(this).find("select").val(0).attr("selected", "selected");
                $(this).find("select").removeAttr("disabled");
            });
        }
    },

    /**
     * 상품정보 리스트 조회
     */
    getOrderInfoList: function () {

        $('#refundMainTable,#syrupTextTag').hide();
        $('#deliveryInfoTable,#deliveryInfoTitle').hide();
        $('#refundTextTable').show();
        $('#piDeductPromoYn,#piDeductDiscountYn,#piDeductShipYn').attr('disabled', true).prop("checked", false);
        $('#oriPiDeductPromoYn,#oriPiDeductDiscountYn,#oriPiDeductShipYn').val('N');

        $('#itemInfoTable tbody tr').remove();
        $('#claimReasonType').val('');
        CommonAjax.basic(
            {
                url: '/order/claim/getMultiShipItemList.json',
                data: JSON.stringify({
                    "purchaseOrderNo": $('#purchaseOrderNo').val(),
                    "multiBundleNo": $('#multiShipList').val()
                }),
                contentType: 'application/json',
                method: 'POST',
                successMsg: null,
                callbackFunc: function (res) {
                    var nonOptItemList = [];//옵션없는 주문정보 리스트

                    if (res.length === 0) {
                        alert('조회된 결과가 없습니다.');
                        window.close();
                        return false;
                    }

                    //orderType : 2 옵션일 경우 itemNm1값이 빈값이면 옵션정보가 없는것으로 간주
                    //주문정보 row에 상품금액/상품 수량 표기
                    for(var i=0;i<res.length;i++){
                        if(res[i].orderType == '2'){
                            if(res[i].itemNm1 == '' || res[i].itemNm1 == null){
                                nonOptItemList.push(res[i]);//옵션이 없는 상품
                            }
                        }
                    }
                    $('#purchaseOrderNo').val(res[0].purchaseOrderNo);
                    $('#titleOrderInfo').text('주문번호 : ' + res[0].purchaseOrderNo + '    |    주문일자 : ' + res[0].orderDt);

                    for(var i=0;i<res.length;i++){
                        var itemInfo = res[i];

                        var tableRow = $('<tr>');
                        var nonOptItemFlag = false;//옵션없는 상품 flag, true : 옵션없음, false : 옵션있음
                        var nonOptItemObject;//옵션없는 상품용 object
                        var selectOrderQty;

                        //옵션이 없는 상품 체크여부
                        for(var j=0;j<nonOptItemList.length;j++){
                            if(itemInfo.orderItemNo == nonOptItemList[j].orderItemNo){
                                nonOptItemFlag = true;
                                nonOptItemObject = nonOptItemList[j];
                            }
                        }
                        switch (itemInfo.orderType) {
                            case '1':
                                var itemInfomation = '';
                                tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text(itemInfo.itemNo));

                                //안내사항 정보 표기
                                //최소구매수량 or 수량별 배송비 차등 수량 값이 있을 경우 표기
                                if(itemInfo.purchaseMinQty > 0 || parseInt(itemInfo.diffQty) > 0){
                                    itemInfomation += "<span class='text' style='font-size: 12px; font-weight: normal'>[안내사항]</span>"
                                    itemInfomation += itemInfo.purchaseMinQty > 0 ? "<span class='text' style='font-size: 12px; font-weight: normal'> 최소구매수량  "+ itemInfo.purchaseMinQty +"개</span>" : ''//최소구매수량 표기
                                    itemInfomation += itemInfo.purchaseMinQty > 0 && itemInfo.diffQty > 0 ? ' / ' : ''
                                    itemInfomation += itemInfo.diffQty > 0 ? "<span class='text' style='font-size: 12px; font-weight: normal'> 수량별 배송비 차등  "+ parseInt(itemInfo.diffQty) +"개</span>" : ''//수량별 배송비 차등 수량 표기
                                }
                                tableRow.append($('<td>').attr('style', 'font-weight: bold; font-size: 14px; overflow:hidden;white-space:nowrap;text-overflow:ellipsis;')
                                .append(!$.jUtil.isEmpty(itemInfo.promoNo) && itemInfo.promoNo != '-' ? itemInfo.promoNm1 + '\n' + '/' + '\n' + itemInfo.promoNo + '<br>' + itemInfo.itemNm1 : itemInfo.itemNm1)//행사정보 표기 여부 확인
                                .append(itemInfomation != '' ? '<br>' + itemInfomation : ''));//안내사항 정보 표기

                                //옵션없는 상품이라 상품금액/신청수량을 표기한다.
                                if(nonOptItemFlag){
                                    nonOptItemObject.purchaseMinQty = itemInfo.purchaseMinQty;//nonOptItemObject에 최소구매수량 값 설정
                                    selectOrderQty = nonOptItemObject.orderQty - nonOptItemObject.claimQty//총주문 개수 - 클레임 건수
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text($.jUtil.comma(nonOptItemObject.orderPrice) + '원'));
                                    multiShipItemInfoLayout.getOrderQtySelectBox(nonOptItemObject, selectOrderQty, tableRow, itemInfo.promoNo, itemInfo.promoNm1);
                                }
                                tableRow.append(
                                    $('<td>').append($('<input type="hidden">').attr("id", itemInfo.itemNo + '_totalOptQty').val(nonOptItemFlag == true ? selectOrderQty : 0))
                                    .append($('<input type="hidden">').attr("id", itemInfo.itemNo + '_totalReqOptQty').val(0))
                                    .append($('<input type="hidden">').attr("id", itemInfo.itemNo + '_purchaseMinQty').val(itemInfo.purchaseMinQty))
                                );
                                break;
                            default :
                                //옵션이 있을경우 다음row에 옵션 표기
                                if(!nonOptItemFlag){
                                    itemInfo.purchaseMinQty = $('#' + itemInfo.itemNo + '_purchaseMinQty').val();
                                    selectOrderQty = itemInfo.orderQty - itemInfo.claimQty//총주문 개수 - 클레임 건수
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text(itemInfo.itemNo));
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 14px').text('\t' + itemInfo.itemNm1));
                                    tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text($.jUtil.comma(itemInfo.orderPrice) + '원'));
                                    multiShipItemInfoLayout.getOrderQtySelectBox(itemInfo, selectOrderQty, tableRow, itemInfo.promoNo , null)

                                    //상품 총 수량 구하기 - 취소/반품시 필요
                                    var sum = parseInt($('#'+itemInfo.itemNo+'_totalOptQty').val());
                                    sum += parseInt(selectOrderQty);
                                    $('#'+itemInfo.itemNo+'_totalOptQty').val(sum);
                                }
                                break;
                        }
                        $('#itemInfoTable > tbody:last').append(tableRow);
                    }
                }
            });
    },

    getOrderQtySelectBox : function(itemInfoObject, selectOrderQty, tableRow, promoNo, promoNm1){
       var select = $('<select></select>').addClass('ui input medium  mg-r-10');

       if(($('#claimPartYn').val() == 'N' && $('#orderCancelYn').val() == 'Y') || selectOrderQty == 0){
           tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 14px; text-align:center').text(selectOrderQty));
       }else{//전체 취소가 아닐 시  신청수량을 select box로 표시
           //신청수량에 표시될 수량 - 총 상품주문 수량에서 클레임처리된 건수를 뺀 수량을 보여준다.
           var purchaseMinQtyCnt = 1;
           var purchaseMinQty = isNaN(itemInfoObject.purchaseMinQty) ? 1 : itemInfoObject.purchaseMinQty;
           select.append( $('<option>').val('0').text('선택'));
           //행사상품 여부 체크
           //행사가 있는 경우 신청 수량선택시 구매한 수량만 선택가능
           if(!$.jUtil.isEmpty(promoNo) && promoNo != '-' && promoNm1 != '행사상품'){
               select.last().append($('<option>').attr('value', selectOrderQty).text(selectOrderQty));
           }else{
               while(purchaseMinQtyCnt <= selectOrderQty){
                   //최소구매수량에 따른 신청 수량 적용
                   if(purchaseMinQtyCnt <= selectOrderQty - purchaseMinQty || selectOrderQty == purchaseMinQtyCnt){
                       var option = $('<option>').attr('value', purchaseMinQtyCnt).text(purchaseMinQtyCnt);
                       select.last().append(option);
                   }
                   purchaseMinQtyCnt++;
               }
           }
           tableRow.append($('<td>').append(select).append($('<input type="hidden">').attr('name', 'orderQtyCompare').val('0')));
       }
       tableRow.append($('<td hidden>').html(itemInfoObject.orderOptNo));
       tableRow.append($('<td hidden>').html(itemInfoObject.orderType));
       tableRow.append($('<td hidden>').html(itemInfoObject.orderItemNo));
       tableRow.append($('<td hidden>').html(itemInfoObject.claimQty));
       tableRow.append($('<td hidden>').html(promoNo));
       tableRow.append($('<td hidden>').html(promoNm1));
    }
}
