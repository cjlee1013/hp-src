/**
 * 주문관리 > 주문관리 -> 취소/환불/교환 팝업 배송정보 layout
 */
$(document).ready(function () {
    deliveryInfoLayout.init();
});

var deliveryInfoLayout = {

    init: function () {
        $("#deliveryInfoTable").find("select,input,radio,button").attr("disabled", true);
        deliveryInfoLayout.bindingEvent();
        claimImg.init();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function () {

        /* 반품비용 결제방법 - 동봉/환불금차감*/
        $('#isEnclose').bindChange(deliveryInfoLayout.setIsEncloseYn);

        /* 상품 발송여부 */
        $("input[name='isPick']").bindChange(deliveryInfoLayout.setDeliveryItemYn);

        $("input:radio[name='isPickReqTd']").bindChange(function() {deliveryInfoLayout.setDeliveryTypeTD('')});

        // 이미지 등록
        $('#imgFile1,#imgFile2,#imgFile3').change(function() {
            claimImg.addImg($(this).attr("id"));
        });
        // 이미지 미리보기
        $(".uploadType").bindClick(claimImg.imagePreview);

        //연락처 - 숫자만 입력
        $("#pickMobileNo_1,#pickMobileNo_2,#pickMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //연락처 - 숫자만 입력
        $("#exchMobileNo_1,#exchMobileNo_2,#exchMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#invoiceNumber").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    },

    /**
     * 반품비용 결제 방법 선택 시 환불예정금액 재조회
     * **/
    setIsEncloseYn: function () {
        refundInfoLayout.checkRefundAmt();
    },

    /**
     * 상품 발송 여부 선택
     * **/
    setDeliveryItemYn: function (isPick) {
        if (typeof isPick == 'object') {
            isPick = $(isPick).val();
        }
        if (isPick == 'Y') {
            $('#courierInfoTable').show();
            $('#isPickReq').hide();
            $("input:radio[name='isPickReq'][value='Y']").prop('checked',true);
        } else {
            $('#courierInfoTable').hide();
            $('#isPickReq').show();
            //$('input:radio[name="isPickReq"]').prop('disabled', false);
        }
    },

    /**
     * 반품배송비 적용
     * **/
    setReturnShipAmt: function (res) {
        $('#deliveryInfoTable,#deliveryInfoTitle,#isEncloseTag,#exchangeIsEncloseTag').show();
        var returnShipAmtText = '';
        var returnShipAmt = 0;
        var whoReasonTag;

        if(res.addShipPrice > 0 || res.addIslandShipPrice > 0 ){
            returnShipAmtText = '('
            if (res.addShipPrice > 0) {
                returnShipAmt += res.addShipPrice;
                returnShipAmtText += '추가 배송비 ' + $.jUtil.comma(res.addShipPrice) + '원'
            }

            if (res.addIslandShipPrice > 0) {
                (res.addShipPrice > 0 ? returnShipAmtText += ' + '
                    : returnShipAmtText += '');
                returnShipAmt += res.addIslandShipPrice;
                returnShipAmtText += '추가 반송도서산간 배송비 ' + $.jUtil.comma(res.addIslandShipPrice) + '원'
            }
            returnShipAmtText += ') |';
        }else{
            returnShipAmtText += ' |';
        }

        switch (res.whoReason) {
            case "B" :
                whoReasonTag = '구매자 책임';
                break
            case "S" :
                whoReasonTag = '판매자 책임';
                $('#isEnclose').val('N');
                $('#isEncloseTag,#exchangeIsEncloseTag').hide();
                break
            case "H" :
                whoReasonTag = '홈플러스 책임';
                break
        }
        $("#deliveryInfoTable").find("select,input,radio,button").attr("disabled", false);
        $('#returnShipAmt').text($.jUtil.comma(returnShipAmt) + '원');
        $('#returnShipAmtText').text(returnShipAmtText);
        $('#whoReasonTag').text(whoReasonTag);

        //반품비용 결제방법 변경시 반품/추가도서산간 배송비 적용에 필요한
        $('#addShipAmt').text(res.addShipPrice);
        $('#addIslandShipAmt').text(res.addIslandShipPrice);

        $('#addShipPrice').text(0);
        $('#addIslandShipPrice').text(0);

        //초기 환불예정금액 구하기
        $('#oriRefundAmt').text(res.refundAmt);
        //초기 차감금액 구하기
        $('#oriDiscountAmt').text(res.discountAmt);
        //반품비용 결제 방법 구하기
        $('#oriIsEnclose').val($('#isEnclose').val());

        //반품비용 결제 방법 초기화
        var isEnclose = $("#isEnclose").val();
        //상품에 동봉함
        if (isEnclose == 'Y') {
            $('#packMsg').text('택배상자 내 반품비용 동봉');
            $('#discountAmt').text($.jUtil.comma(parseInt($('#oriDiscountAmt').text()) - (parseInt($('#addShipAmt').text()) + parseInt($('#addIslandShipAmt').text()))));
            $('#addShipPrice').text(0);
            $('#addIslandShipPrice').text(0);
        //환불금에서 차감
        } else {
            $('#packMsg').text('반품비용 제외 후 환불처리');
            $('#discountAmt').text($.jUtil.comma($('#oriDiscountAmt').text()));
            $('#addShipPrice').text($.jUtil.comma($('#addShipAmt').text()));
            $('#addIslandShipPrice').text($.jUtil.comma($('#addIslandShipAmt').text()));
        }
    },

    /**
     * 반품/교환 배송정보 조회 ajax
     * @param 환불예정금액 조회 대상 주문정보 list
     * **/
    getShippingInfo : function (){
        var url = '';
        if($('#multiShipYn').val() == 'Y'){
            url = '/order/claim/getMultiShipInfo.json?bundleNo=' + $('#bundleNo').val() + '&multiBundleNo=' + $('#multiShipList').val();
        }else{
            url = '/order/claim/getClaimShipInfo.json?bundleNo=' + $('#bundleNo').val();
        }

        CommonAjax.basic(
            {
                url: url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {

                    //orderReturn.jsp, orderExchange.jsp 에서 선언된 변수
                    shipMethod = res.shipMethod;

                    $('#deliveryYn').val(res.deliveryYn);
                    $("input[name$='ReceiverNm']" ).val(res.receiverNm);
                    $("input[name$='MobileNo']" ).val(res.shipMobileNo);
                    $.jUtil.valSplit($("input[name$='MobileNo']" ).val(), '-', 'pickMobileNo');
                    $.jUtil.valSplit($("input[name$='MobileNo']" ).val(), '-', 'exchMobileNo');


                    $("input[name$='ZipCode']" ).val(res.zipCode);
                    $("input[name$='BaseAddr']" ).val(res.baseAddr);
                    $("input[name$='RoadBaseAddr']" ).val(res.roadBaseAddr);
                    $("input[name$='RoadDetailAddr']" ).val(res.roadDetailAddr);
                    $("input[name$='BaseDetailAddr']" ).val(res.roadDetailAddr);
                    $('#storeNm').text("홈플러스 " + res.storeNm);

                    if(!$.jUtil.isEmpty(res.placeNm )){
                        var startTime = res.startTime;
                        var endTime = res.endTime;
                        $('#placeNm').text("(방문위치 : "+ res.placeNm + " / " + "이용시간 " + startTime.substring(0,2) + " : " + startTime.substring(2,4) + " ~ " + endTime.substring(0,2) + " : " + endTime.substring(2,4) +")");
                    }

                    //shipType에 따른 수거방법 화면
                    switch (shipType) {
                        //업체배송
                        case 'DS' :
                            $('#pickWayTD').hide();//
                            $('#pickWayDS').show();
                            break;
                        //직접수령
                        case 'PICK' :
                            $('#pickWayTD,#pickRadioLabel,#pickPlace').show();
                            $('#pickWayDS,#slotChangeArea,#pickShippingInfoRow,#exchShippingInfoRow').hide();
                            if($.jUtil.isEmpty($("input:radio[name='isPickReqTd']:checked").val())){
                                $("input:radio[id='pickRadio'][value='Y']").prop('checked',true);
                            }
                            break;
                        //멀티배송
                        case 'MULTI' :
                            $('#pickWayTD').hide();//
                            $('#pickWayDS').show();
                            break;
                        //자차배송
                        case 'TD' : case 'AURORA' :
                            if(res.shipMethod == 'TD_QUICK') {
                                $('#pickWayTD,#quickRadioLabel').show();
                                $('#pickWayDS,#slotChangeArea').hide();
                                if($.jUtil.isEmpty($("input:radio[name='isPickReqTd']:checked").val())){
                                    $("input:radio[id='quickRadio'][value='Y']").prop('checked',true);
                                }
                            }else{
                                $('#pickWayTD,#tdRadioLabel').show();
                                $('#pickWayDS').hide();
                                if($.jUtil.isEmpty($("input:radio[name='isPickReqTd']:checked").val())){
                                    $("input:radio[id='tdRadio'][value='Y']").prop('checked',true);
                                }
                                deliveryInfoLayout.getChangeableInfo();//수거배성 슬롯정보
                            }
                            break;
                        //그 외 shipMethod를 이용해 적용
                        default   :
                            if(res.shipMethod == 'TD_DRCT'){
                                $('#pickWayTD,#tdRadioLabel').show();
                                $('#pickWayDS').hide();
                                if($.jUtil.isEmpty($("input:radio[name='isPickReqTd']:checked").val())){
                                    $("input:radio[id='tdRadio'][value='Y']").prop('checked',true);
                                }
                                deliveryInfoLayout.getChangeableInfo();//수거배성 슬롯정보
                            }else if(res.shipMethod == 'TD_PICK' || res.shipMethod == 'DS_PICK'){
                                $('#pickWayTD,#pickRadioLabel,#pickPlace').show();
                                $('#pickWayDS,#slotChangeArea,#pickShippingInfoRow,#exchShippingInfoRow').hide();
                                if($.jUtil.isEmpty($("input:radio[name='isPickReqTd']:checked").val())){
                                    $("input:radio[id='pickRadio'][value='Y']").prop('checked',true);
                                }
                            }else{
                                $('#pickWayTD').hide();
                                $('#pickWayDS').show();
                            }
                            break;
                    }
                }
            });
    },

    /**
     * 배송시간 변경 slot
     * **/
    getChangeableInfo : function () {
        var data = {
            'bundleNo' : $('#bundleNo').val(),
            'purchaseOrderNo' : $('#purchaseOrderNo').val()
        }
        CommonAjax.basic(
            {
                url:'/order/getShipSlotInfo.json?'.concat(jQuery.param(data)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if(res.returnCode == '0000' || res.returnCode == 'SUCCESS'){
                        $('#slotChangeArea').empty();

                        // 21.03.22 주문 점포가 폐점 또는 변경 시 슬롯정보를 지우는 경우가 발생
                        // 슬롯정보가 null인 경우 수거 안함으로 기본설정하여 반품/교환처리
                        if($.jUtil.isEmpty(res.data.frontSlotHeader)){
                            $("input:radio[name='isPickReqTd'][value='N']").prop('checked',true);
                            deliveryInfoLayout.setDeliveryTypeTD('N');
                        }else{
                            $('#slotChangeArea').append(orderUtil.writeToSlotTable(res.data))
                        }
                    }
                }
            });
    },

    setDeliveryTypeTD : function(isPickReqTd){

        if($.jUtil.isEmpty(isPickReqTd)){
            isPickReqTd = $("input:radio[name='isPickReqTd']:checked").val();
        }

        if (isPickReqTd == 'Y' && shipType != 'PICK') {
            if(shipMethod == 'TD_QUICK'){
                $('#pickShippingInfoRow,#exchShippingInfoRow').show();
            }else{
                $('#slotChangeArea,#pickShippingInfoRow,#exchShippingInfoRow').show();
            }
        } else {
            $('#slotChangeArea,#pickShippingInfoRow,#exchShippingInfoRow').hide();
        }

        if (isPickReqTd == 'Y' && shipType == 'PICK') {
            $('#pickPlace').show();
        }else{
            $('#pickPlace').hide();
        }
    },
    /**
     * 수거지 우편번호 찾기 callback
     * **/
    setPickZipcode : function(res){
      $("#pickZipCode").val(res.zipCode);
      $("#pickRoadBaseAddr").val(res.roadAddr);
      $("#pickBaseAddr").val(res.gibunAddr);
    },

    /**
     * 배송지 우편번호 찾기 callback
     * **/
    setExchZipcode : function(res){
      $("#exchZipCode").val(res.zipCode);
      $("#exchRoadBaseAddr").val(res.roadAddr);
      $("#exchBaseAddr").val(res.gibunAddr);
    }
}

/**
 * 이벤트 이미지 관련
 */
var claimImg = {
    seq : null

    /**
     * 이미지 초기화
     */
    , init : function () {
        $(".uploadType").each(function (){claimImg.setImg($(this))});
        claimImg.seq = null;
    }
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    , setImg : function (seq, params) {
        $('#imgBtnView' + seq).show();
        $('#imgBtnUpload' + seq).hide();

        $('#fileName'+seq).text(params.fileName);
        $('#uploadFileName'+seq).text(params.imgId);
    },
    /**
     * 업로드 에러 발생시 메세지 처리
     * @param error error
     * @returns {string}
     */
    alertMsg: function (error) {
        var errorMsg = '';

        if (error.indexOf('확장자') > -1) {
            errorMsg = "첨부 가능한 파일형식이 아닙니다.";
        } else {
            errorMsg = error;
        }

        if ($.jUtil.isNotEmpty(errorMsg)) {
            alert(errorMsg);
        }
    },

    /**
     * 이미지 세팅
     * @param obj
     */
    addImg : function () {

        var fileTemp = /(.*?)\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/;

        if ($("#imgFile" + claimImg.seq).get(0).files[0].size > 5242880) {
            return $.jUtil.alert("첨부가능한 최대용량을 초과하였습니다.");
        }
        if (!$("#imgFile" + claimImg.seq).get(0).files[0].name.match(fileTemp)) {
            alert('첨부 가능한 파일형식이 아닙니다.')
            $("#imgFile" + claimImg.seq).val('')
            return false;
        }

        claimImg.setImg(claimImg.seq,{
            'fileName' : $("#imgFile" + claimImg.seq).get(0).files[0].name,
            'imgId': ''
        })
    },

    /**
     * 이미지 업로드
     * @param obj
     */
    uploadImg : function (seq, file) {

        var $uploadForm = $("<form></form>");
        file.clone(true).appendTo($uploadForm);
        file.val('');
        $uploadForm.append('<input type="hidden" name="processKey" value="ClaimAddImage">');
        $uploadForm.append('<input type="hidden" name="mode" value="IMG">');

        $uploadForm.ajaxForm({
            type: "POST",
            url: "/common/upload.json",
            async : false,
            success: function (resData) {
                 for(var i=0;i<resData.fileArr.length;i++){
                    var f = resData.fileArr[i];
                    if (f.hasOwnProperty("error")) {
                        claimImg.alertMsg(f.error);
                    } else {
                        $('#uploadFileName' + seq).text(f.fileStoreInfo.fileId);
                    }
                }
            }
        }).submit();
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function (seq) {

        //21.03.23 사진첨부 3->2->1 순서대로 삭제여부 체크
        switch (seq) {
            case 1 :
                if(!$.jUtil.isEmpty($('#fileName2').text())){
                    return $.jUtil.alert("사진첨부2부터 삭제해주세요.");
                }
                break;
            case 2 :
                if(!$.jUtil.isEmpty($('#fileName3').text())){
                    return $.jUtil.alert("사진첨부3부터 삭제해주세요.");
                }
                break;
        }

        $('#fileName'+seq).text('');
        $('#uploadFileName'+seq).text('');
        $('#imgBtnView' + seq).hide();
        $('#imgBtnUpload' + seq).show();
    }
    /**
     * 이미지 클릭처리
     * @param obj
     */
    , clickFile : function (seq) {
        claimImg.seq = seq;

        //21.03.23 사진첨부 1->2->3 순서대로 등록여부 체크
        switch (claimImg.seq) {
            case 2 :
                if($.jUtil.isEmpty($('#fileName1').text())){
                    return $.jUtil.alert("사진첨부1부터 등록해주세요.");
                }
                break;
            case 3 :
                if($.jUtil.isEmpty($('#fileName2').text())){
                    return $.jUtil.alert("사진첨부2부터 등록해주세요.");
                }
                break;
        }

        $('#imgFile'+seq).click();
    }
};


function setAddrCallBack(res) {

   var type = res.reqType=='1'?'pick' : 'exch';

   $('#'+ type + 'ReceiverNm').text(res.name);
   $('#'+ type + 'MobileNo').text(res.phone);
   $('#'+ type + 'ZipCode').text(res.zipCode);
   $('#'+ type + 'RoadBaseAddr').text(res.addr);
   $('#'+ type + 'RoadDetailAddr').text(res.addrDetail);
   $('#'+ type + 'BaseAddr').text(res.gibunAddr);
   $('#'+ type + 'BaseDetailAddr').text(res.addrDetail);
}