

/**
 *  클레임 요청 데이터 선택
 */
var orderPopupCommon = {

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $('#claimReasonType').bindChange(orderPopupCommon.whoReasonCheck);
    },

    /**
     * 귀책 사유가 구매자인 경우 미차감 체크 불가능 설정
     * **/
    whoReasonCheck : function(){
        var claimReasonType = $('#claimReasonType').val();

        $.each(claimReasonTypeList, function(key, value){
            if(value.mcCd == claimReasonType){
                if(value.whoReason == 'B'){
                    $('#piDeductPromoYn,#piDeductDiscountYn,#piDeductShipYn').attr('disabled', true).prop("checked", false);
                    deductInfoLayout.chgPiDeductPromoYn();
                }else{
                    $('#piDeductPromoYn,#piDeductDiscountYn,#piDeductShipYn').attr('disabled', false);
                }

                //상세사유 필수 여부
                $('#detailReasonRequired').val(value.detailReasonRequired);
            }
        });
    },


    getClaimReqList: function (_readTableId) {
        var claimPreRefundItemList = [];
        var claimPreRefundItem;
        var isMarket = $('#' + _readTableId + ' tr input:checkbox').length > 0;
        $('#' + _readTableId + ' tbody tr').each(function () {
            var orderQty = 0;
            if(isMarket) {
                console.log(orderQty);
                if($(this).find("td").eq(0).find('input:checkbox').length > 0 && $(this).find("td").eq(0).find('input:checkbox').prop("checked")) {
                    orderQty = $(this).find("td").eq(4).html();
                }
                console.log(orderQty);
                if (orderQty > 0) {
                    claimPreRefundItem = {
                        "itemNo": $(this).find("td").eq(1).html(),   //상품번호
                        "orderOptNo": $(this).find("td").eq(5).html(),    //옵션번호
                        "orderItemNo": $(this).find("td").eq(7).html(), //상품주문번호
                        "claimQty": orderQty,                        //신청수량
                    }
                }
            } else {
                orderQty = $('#claimPartYn').val() == 'N' ? $(this).find("td").eq(3).html() : $(this).find("select").eq(0).val();
                if (orderQty > 0) {
                    claimPreRefundItem = {
                      "itemNo": $(this).find("td").eq(0).html(),   //상품번호
                      "orderOptNo": $(this).find("td").eq(4).html(),    //옵션번호
                      "orderItemNo": $(this).find("td").eq(6).html(), //상품주문번호
                      "claimQty": orderQty,                        //신청수량
                    }
                }
            }
            if(orderQty > 0) {
                claimPreRefundItemList.push(claimPreRefundItem);
            }
      });
      return claimPreRefundItemList;
  }
}
