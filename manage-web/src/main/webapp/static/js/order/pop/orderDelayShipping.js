const orderDelayShipping = {
    init : function () {
        this.pageInit();
        this.search();
        this.bindingEvent();
    },
    pageInit : function () {
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'shipNo' : shipNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderDelayShippingInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToSpan("orderDelayShipForm", res);
                }
            });
    },
};