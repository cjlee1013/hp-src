const orderStoreShipMemoPop = {
    init : function () {
        this.pageInit();
        this.search();
        this.bindingEvent();
    },
    pageInit : function () {
        orderUtil.setWindowAutoResize();
    },
    bindingEvent : function (){
        $('#saveBtn').bindClick(function() {orderStoreShipMemoPop.requestShipMemo();});
    },
    search : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
            'bundleNo' : bundleNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderStoreShipMemoInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        if($.jUtil.isNotEmpty(res.data)){
                            $('#shipAddrNo').val(res.data.shipAddrNo);
                            $('#shipMemo').val(res.data.shipMemo);
                        }
                    }

                }
            });
    },
    requestShipMemo : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
            'bundleNo' : bundleNo,
            'shipMemo' : $('#shipMemo').val(),
            'shipAddrNo' : $('#shipAddrNo').val()
        };
        CommonAjax.basic(
            {
                url:'/order/setOrderStoreShipMemoInfo.json',
                data: JSON.stringify(param),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    const returnCode = res['returnCode'];
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        alert("정상적으로 처리되었습니다.");
                        opener.location.reload();
                        self.close();
                    } else {
                        alert(res.returnMessage);
                        self.close();
                    }
                }
            });
    }
};