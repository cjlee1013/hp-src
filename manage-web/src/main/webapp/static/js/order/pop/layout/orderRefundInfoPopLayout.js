/**
 * 주문관리 > 주문관리 -> 취소/환불/교환 팝업 환불정보 layout
 */
$(document).ready(function () {
    refundInfoLayout.init();
});

// 환불정보
var refundInfoLayout = {
    /**
     * 초기화
     */
    init: function () {
        refundInfoLayout.bindingEvent();
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function () {
        $("input[name='refundType']").bindChange(refundInfoLayout.setRefundType);

        // 검색어 특수기호(%\) 입력 제한
        $("#claimReasonDetail").allowInput("keyup", ["KOR", "NUM", "ENG_LOWER", "ENG_UPPER"], $(this).attr("id"));
    },

    /**
     * 환불예정금액 확인
     */
    checkRefundAmt: function () {

        $('input[id$="_totalReqOptQty"]').val(0);

        var claimPreRefundItemList = [];
        var claimPreRefundItem;
        var itemNo;
        var orderType;
        var sum = 0;
        var isMarket = JSON.parse($('#isMarket').val());

        if ($.jUtil.isEmpty($('#claimReasonType').val())) {
            alert(claimTypeName + '사유를 선택해 주세요.');
            return false;
        }

        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = 0;
            var basicNum = 0;
            if(isMarket) {
                basicNum = 1;
            }
            if(isMarket) {
                if ($(this).find("td").eq(0).find('input:checkbox').prop("checked") ) {
                    orderQty = parseInt($(this).find("td").eq(4).html());
                }
            } else {
                orderQty = $(this).find("select").eq(0).val();
            }

            if (orderQty > 0) {
                itemNo = $(this).find("td").eq(0 + basicNum).html();
                orderType = $(this).find("td").eq(5 + basicNum).html();

                claimPreRefundItem = {
                    "itemNo": $(this).find("td").eq(0 + basicNum).html(),   //상품번호
                    "orderOptNo": $(this).find("td").eq(4 + basicNum).html(),    //옵션번호
                    "orderItemNo": $(this).find("td").eq(6 + basicNum).html(), //상품주문번호
                    // "addOptYn":$(this).find("td").eq(7).html(),
                    "claimQty": orderQty,                        //신청수량
                };
                claimPreRefundItemList.push(claimPreRefundItem);

                //상품번호당 요청 수량 구하기
                sum = parseInt($('#' + itemNo + '_totalReqOptQty').val());
                sum += parseInt(orderQty);
                $('#' + itemNo + '_totalReqOptQty').val(sum);

            }
            $(this).find("input[name='orderQtyCompare']").val(orderQty);
        });

        //주문수량 체크

        if (claimPreRefundItemList.length === 0) {
            if(isMarket) {
                alert('상품을 선택해주세요.');
            } else {
                alert('신청수량을 선택해 주세요.');
            }
            return false;
        }
        //환불예정금액 ajax
        refundInfoLayout.getRefundAmt(claimPreRefundItemList);

    },

    /**
     * 전체 주문취소 시 환불 예정금액 확인 함수
     * **/
    allCancelCheckRefundAmt : function(){

        var claimPreRefundItemList = [];
        var claimPreRefundItem;

        if ($.jUtil.isEmpty($('#claimReasonType').val())) {
            alert(claimTypeName + '사유를 선택해 주세요.');
            return false;
        }

        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = $(this).find("td").eq(3).html();
            if (orderQty > 0) {
                claimPreRefundItem = {
                "itemNo": $(this).find("td").eq(0).html(),      //상품번호
                "orderOptNo": $(this).find("td").eq(4).html(),  //옵션번호
                "orderItemNo": $(this).find("td").eq(6).html(), //상품주문번호
                "addOptYn":$(this).find("td").eq(7).html(),     //추가상품여부
                "claimQty": $(this).find("td").eq(3).html()     //신청수량
                };
                claimPreRefundItemList.push(claimPreRefundItem);
            }
        });

        //환불예정금액 ajax
        refundInfoLayout.getRefundAmt(claimPreRefundItemList);
    },

    /**
     * 환불예정금액 호출 ajax
     * @param 환불예정금액 조회 대상 주문정보 list
     * **/
    getRefundAmt : function (claimPreRefundItemList){

        var piDeductPromoYn = $("input:checkbox[id='piDeductPromoYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductDiscountYn = $("input:checkbox[id='piDeductDiscountYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductShipYn = $("input:checkbox[id='piDeductShipYn']").is(":checked") == true ? 'Y' : 'N';

        //반품비용 결제 방법 선택
        //구매자 귀책 시 선택 값으로 설정
        //판매자 귀책 시 service에서 'N'으로 설정
        var encloseYn = $.jUtil.isEmpty($('#isEnclose').val()) == true ? 'N' : $('#isEnclose').val();

        //parameter 세팅
        var param = {
            "purchaseOrderNo": $('#purchaseOrderNo').val(),
            "bundleNo": $('#bundleNo').val(),
            "claimItemList": claimPreRefundItemList,
            "claimReasonType": $('#claimReasonType').val(),
            "piDeductPromoYn" : piDeductPromoYn,
            "piDeductDiscountYn" : piDeductDiscountYn,
            "piDeductShipYn" : piDeductShipYn,
            "encloseYn" : encloseYn,
            "multiShipYn" : $('#multiShipYn').val(),
            "multiBundleNo" : $('#multiShipList').val()
        };

        //환불예정금액 api 호출
        CommonAjax.basic(
            {
                url: '/order/claim/preRefundPrice.json',
                data: JSON.stringify(param),
                contentType: 'application/json',
                method: 'POST',
                callbackFunc: function (res) {
                    //환불예정금액 조회 결과 0 : 성공, 나머지 : 실패
                    if(res.returnValue != '0') {
                        alert('환불예정금액 조회 실패 [' + res.returnMsg + ']');

                        //시럽쿠폰 주문 클레임시 문구 노출
                        //returnValue : 1119 - 시럽쿠폰 사용 주문
                        if(claimTypeName != '교환' && res.returnValue == '1119') {
                            $('#syrupTextTag').show();
                        }
                        return false;
                    }
                    $('#preRefundAmtReturnCode').val(res.returnValue);//환불예정금액 조회 결과 지정
                    $('#refundMainTable').show();
                    $('#refundTextTable,#syrupTextTag').hide();
                    $('#oriRefundAmt').text(res.refundAmt);
                    $('#oriPiDeductPromoYn').val(piDeductPromoYn);
                    $('#oriPiDeductDiscountYn').val(piDeductDiscountYn);
                    $('#oriPiDeductShipYn').val(piDeductShipYn);
                    $('#oriMultiShipList').val($('#multiShipList').val())
                    $('#oriClaimReasonType').val($('#claimReasonType').val());


                    $.each(res, function (key, value) {
                        $('#' + key + '').text($.jUtil.comma(value));
                    });

                    //상품/행사 할인 금액 구하기 [상품할인금액 + 행사할인금액]
                    $('#itemPromoDiscountAmt').text($.jUtil.comma(res.itemDiscountAmt + res.promoDiscountAmt));

                    //추가도서산간배송비 row 노출 여부, 추가도서산간배송비가 추가됐을 경우만 노출
                    if(res.addIslandShipPrice > 0){
                        $('#addIslandShipPriceRow').show();
                    }

                    //임직원할인금액 row 노출 여부, 임직원할인금액이 0원 이상인 경우만 노출
                    if(res.empDiscountAmt > 0){
                        $('#empDiscountAmtRow').show();
                    }

                    //환불,교환 시 배송정보 layout 뇨출
                    if(res.claimType == 'R' || res.claimType == 'X') {
                        deliveryInfoLayout.setReturnShipAmt(res);//반품배송비 적용
                        deliveryInfoLayout.getShippingInfo();//수거/교환 배송정보 조회
                    }

                    //환불예정금액이 0원 아닌 경우 신청버튼 노출
                    $('#reqBtn').show();
                }
            });
    },

    /**
     * 소득공제 제공여부
     */
    setRefundType: function (refundType) {
        if (typeof refundType == 'object') {
            refundType = $(refundType).val();
        }
        if (refundType === 'bk') {
            $('#bankAccountInfoTableTd').show();
        } else {
            $('#bankAccountInfoTableTd').hide();

        }
    }
};