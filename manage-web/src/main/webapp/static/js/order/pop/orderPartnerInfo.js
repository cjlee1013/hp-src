const orderPartnerInfo = {
    init : function () {
        this.pageInit();
        this.search();
    },
    pageInit : function () {
        orderUtil.setWindowAutoResize();
    },
    search : function () {
        const param = {
            'partnerId' : partnerId,
            'bundleNo' : bundleNo,
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderPartnerInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToSpan("orderPartnerInfoForm", res);
                }
            });
    },
};