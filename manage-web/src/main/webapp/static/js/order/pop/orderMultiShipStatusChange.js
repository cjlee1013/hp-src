$(document).ready(function() {
    orderMultiShipStatusChange.init();
    CommonAjaxBlockUI.global();
});

var orderMultiShipStatusChange = {
    init : function () {
        this.pageInit();
        this.bindingEvent();
    },
    pageInit : function (){
        orderUtil.setWindowAutoResize();
    },
    setShipData : function () {
        if (confirm("배송상태를 변경하시겠습니까?")) {
            CommonAjax.basic({
                url:'/order/multiShipStatusChange.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if (res.data === 'true') {
                        opener.location.reload();
                        self.close();
                    } else {
                        alert('배송상태변경에 실패하였습니다.');
                    }
                }
            });
        }
    },
    bindingEvent : function () {
        $('#changeBtn').bindClick(function() { orderMultiShipStatusChange.setShipData(); });
    },
};