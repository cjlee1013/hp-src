/**
 * 주문관리 > 주문관리 -> 취소/환불/교환 팝업 배송정보 layout
 */
$(document).ready(function () {
    deductInfoLayout.init();
});

var deductInfoLayout = {

    init: function () {
        deductInfoLayout.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function () {
        /* 행사 미차감 선택 여부 체크 */
        $('#piDeductPromoYn').bindChange(deductInfoLayout.chgPiDeductPromoYn);
    },

    /**
     * 행사 미차감 선택 여부 체크
     * true : 부분 신청 가능
     * **/
    chgPiDeductPromoYn: function () {
        var piDeductPromoYn = $("input:checkbox[id=piDeductPromoYn]").is(":checked");

        $("#itemInfoTable tbody tr").each(function () {
            var claimQtySave = $(this).find("option:selected").val();
            //체크시
            if (piDeductPromoYn) {
                //select box 마지막 옵션 숫자 여부 체크
                if(!isNaN($(this).find("option:last").val())){
                    var claimQty = $(this).find("option:last").val();//클레임 선택 가능 수량
                    $(this).find("option").remove();//select box 옵션 제거
                    $(this).find("select").append("<option value='0'>선택</option>");
                    //수량 재설정
                    for(var claimCnt = 1; claimCnt <= claimQty; claimCnt++) {
                        if (claimQtySave == claimCnt) {
                            $(this).find("select").append("<option selected value=" + claimCnt + ">" + claimCnt+ "</option>");
                        } else {
                            $(this).find("select").append("<option value=" + claimCnt + ">" + claimCnt+ "</option>");
                        }
                    }
                }
            } else {//체크 해제시, 최대 수량만 선택가능하도록 한다.

                var claimQty = $(this).find("option:last").val();//클레임 선택 가능 수량
                var promoNo = $(this).find("td").eq(8).html();
                var promoNm = $(this).find("td").eq(9).html();
                $(this).find("option").remove();
                $(this).find("select").append("<option value='0'>선택</option>");

                if(!$.jUtil.isEmpty(promoNo) && promoNo != '-' && promoNm != '행사상품'){
                    if (claimQtySave == claimQty) {
                        $(this).find("select").append("<option selected value=" + claimQty + ">" + claimQty + "</option>");
                    } else {
                        $(this).find("select").append("<option value="+claimQty+">"+claimQty+"</option>");
                    }

                }else{//행사 상품이 아닌 경우 부분 수량 체크 가능
                    //수량 재설정
                    for(var claimCnt = 1; claimCnt <= claimQty; claimCnt++) {
                        if (claimQtySave == claimCnt) {
                            $(this).find("select").append("<option selected value=" + claimCnt + ">" + claimCnt + "</option>");

                        } else {
                            $(this).find("select").append("<option value=" + claimCnt + ">" + claimCnt + "</option>");
                        }
                    }
                }
            }
        });
    }

};
