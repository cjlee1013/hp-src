/**
 * 주문관리 > 주문취소 팝업
 */
$(document).ready(function () {
    orderCancel.init();
    CommonAjaxBlockUI.global();
});

var orderCancel = {

    /**
     * 취소요청 유효성 검사
     * **/
    init: function () {
        $('#reqBtn').hide();

        if(claimPartYn == 'N'){
            $('#deductForm').hide();
        }
        orderPopupCommon.bindingEvent();
    },
    validation: function () {

        var piDeductPromoYn = $("input:checkbox[id='piDeductPromoYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductDiscountYn = $("input:checkbox[id='piDeductDiscountYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductShipYn = $("input:checkbox[id='piDeductShipYn']").is(":checked") == true ? 'Y' : 'N';
        var multiShipList = $('#multiShipList').val();

        var oriPiDeductPromoYn = $('#oriPiDeductPromoYn').val();
        var oriPiDeductDiscountYn = $('#oriPiDeductDiscountYn').val();
        var oriPiDeductShipYn = $('#oriPiDeductShipYn').val();
        var oriMultiShipList = $('#oriMultiShipList').val();

        var isMarket = JSON.parse($('#isMarket').val());

        //환불예정금액 확인 유무
        if ($('#preRefundAmtReturnCode').val() != '0' && !isMarket) {
            $('#preRefundInfo').val('');
            alert('환불예정금액을 확인해 주세요');
            return false;
        }

        if(isMarket && $('#claimPartYn').val() == 'Y') {
            var isChecked = false;
            $('#itemInfoTable tbody tr').each(function () {
                if($(this).find("td").eq(0).find('input:checkbox').length > 0 && $(this).find("td").eq(0).find('input:checkbox').prop("checked")) {
                    isChecked = true;
                }
            });
            if(!isChecked){
                alert('상품을 선택해주세요.');
                return false;
            }
        }

        //신청수량 변경유무 확인
        var qtyBoolean = true;
        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = $(this).find("select").eq(0).val();
            var orderQtyCompare = $(this).find("input[name='orderQtyCompare']").val();
            if (orderQty == orderQtyCompare) {
                return true;
            } else {
                qtyBoolean = false;
                return false;
            }
        });
        if (!qtyBoolean && !isMarket) {
            alert('신청수량이 변경됐습니다. 환불예정금액을 다시 확인해 주세요');
            return false;
        }

        //취소 사유/상세 사유 확인
        if ($('#claimReasonType').val() == '' || $('#claimReasonType').val() == null) {
            alert('취소 사유를 선택해주세요');
            return false;
        }

        //사유변경 확인
        if (($('#claimReasonType').val() != $('#oriClaimReasonType').val()) && !isMarket) {
            alert('취소 사유가 변경됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if($('#detailReasonRequired').val() == 'Y'){
            if($.jUtil.isEmpty($('#claimReasonDetail').val())){
                alert('취소 상세사유를 입력해주세요');
                return false;
            }
        }

        if(parseInt($('#refundAmt').text().replace(',','')) < 0) {
            alert('환불예정금액이 0원보다 작아 주문취소가 불가능 합니다.\n취소신청으로 진행 부탁드립니다.');
            return false;
        }

        if(piDeductPromoYn != oriPiDeductPromoYn){
            alert('행사 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(piDeductDiscountYn != oriPiDeductDiscountYn){
            alert('할인 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(piDeductShipYn != oriPiDeductShipYn){
            alert('배송비 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(!$.jUtil.isEmpty(multiShipList)){
            if(multiShipList != oriMultiShipList){
                alert('배송지가 변경됐습니다.\n환불예정금액을 다시 조회해 주세요');
                return false;
            }
        }
        return true;
    },

    /**
     * 취소요청
     * **/
    submit: function () {
        var param;
        var claimPartYn = $('#claimPartYn').val();//전체취소/부분취소 여부
        var orderCancelYnText = orderCancelYn === 'Y'?'주문취소':'취소신청';

        //취소요청 유효성 검사
        if (!orderCancel.validation()) {
            return false;
        }

        //부분취소인 클레임이 전체취소인지 확인
        if(claimPartYn == 'Y'){
            var totalOrderQty = 0;   //총 상품 수량
            var totalOrderReqQty = 0;//신청수량 + 클레임 건수\
            var isMarket = $('#itemInfoTable input:checkbox').length > 0;
            /*
            * 신청수량 + 클레임 수량 구하기
            * */
            $('#itemInfoTable tbody tr').each(function () {
                var basicNum = 0;
                if(isMarket) {
                    basicNum = 1;
                }
                var orderType = $(this).find("td").eq(5 + basicNum).html();
                var itemNo = $(this).find("td").eq(0 + basicNum).html();
                var orderQty = $(this).find("select").eq(0 + basicNum).val();

                if(orderType == 2){
                    totalOrderReqQty += parseInt(orderQty);
                    totalOrderReqQty += isNaN(parseInt($(this).find("td").eq(7 + basicNum).html()) == true ? 0 : parseInt($(this).find("td").eq(7 + basicNum).html()));
                }
                totalOrderQty += isNaN(parseInt($("#" + itemNo + '_totalOptQty').val())) == true ? 0 : parseInt($("#" + itemNo + '_totalOptQty').val());
            });
            /*
            * 총 상품 수량 == 신청수량 + 클레임 건수 같을 경우 claimPartYn(전체 취소 여부) : N 으로 변경한다.
            * */
            if(totalOrderQty == totalOrderReqQty){
               claimPartYn = 'N';
            }
        }

        //parameter 세팅
        param = {
                "purchaseOrderNo": $('#purchaseOrderNo').val(),
                "claimItemList": orderPopupCommon.getClaimReqList('itemInfoTable'),
                "claimReasonType": $('#claimReasonType').val(),
                "cancelType": $('#cancelType').val(),
                "claimType": $('#claimType').val(),
                "bundleNo": $('#bundleNo').val(),
                "claimReasonDetail" : $('#claimReasonDetail').val(),
                "claimPartYn": claimPartYn,
                "orderCancelYn" : $('#orderCancelYn').val(),
                "piDeductPromoYn" : $("input:checkbox[id='piDeductPromoYn']").is(":checked") == true ? 'Y' : 'N',
                "piDeductDiscountYn" : $("input:checkbox[id='piDeductDiscountYn']").is(":checked") == true ? 'Y' : 'N',
                "piDeductShipYn" : $("input:checkbox[id='piDeductShipYn']").is(":checked") == true ? 'Y' : 'N',
                "multiShipYn" : $('#multiShipYn').val()
        };

        if($('#multiShipYn').val() == 'Y'){
            param.multiBundleNo = $('#multiShipList').val();
        }

        if (!confirm(orderCancelYnText + '하시겠습니까?')) {
            return false;
        }
        //파라미터 세팅
        //취소요청
        CommonAjax.basic(
            {
                url: '/order/claim/claimRegister.json',
                data: JSON.stringify(param),
                method: "POST",
                contentType: 'application/json',
                successMsg: null,
                callbackFunc: function (res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1) {
                        alert(orderCancelYnText + ' 완료되었습니다.');
                        opener.location.reload();
                        self.close();
                    }else{
                        alert(orderCancelYnText + '실패 [' + res.returnMessage + ']');
                        return false;
                    }
                }
            });
    }

}