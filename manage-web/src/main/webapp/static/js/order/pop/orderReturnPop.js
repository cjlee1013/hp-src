/**
 * 주문관리 > 반품신청 팝업
 */
$(document).ready(function() {
  orderReturn.init();
  CommonAjaxBlockUI.global();
});

var orderReturn = {
    init : function(){
      $('#reqBtn,#deliveryInfoTitle,#deliveryInfoTable').hide();
      orderPopupCommon.bindingEvent();
    },

    /**
     * 반품신청 유효성 검사
     * **/
    validation: function () {

        var piDeductPromoYn = $("input:checkbox[id='piDeductPromoYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductDiscountYn = $("input:checkbox[id='piDeductDiscountYn']").is(":checked") == true ? 'Y' : 'N';
        var piDeductShipYn = $("input:checkbox[id='piDeductShipYn']").is(":checked") == true ? 'Y' : 'N';
        var multiShipList = $('#multiShipList').val();

        var oriPiDeductPromoYn = $('#oriPiDeductPromoYn').val();
        var oriPiDeductDiscountYn = $('#oriPiDeductDiscountYn').val();
        var oriPiDeductShipYn = $('#oriPiDeductShipYn').val();
        var oriMultiShipList = $('#oriMultiShipList').val();

        var oriIsEnclose = $('#oriIsEnclose').val();
        var isEnclose = $('#isEnclose').val();

        var isMarket = JSON.parse($('#isMarket').val());

        //환불예정금액 확인 유무
        if ($('#preRefundAmtReturnCode').val() != '0') {
            $('#preRefundInfo').val('');
            alert('환불예정금액을 확인해 주세요');
            return false;
        }

        if(isMarket) {
            var isChecked = false;
            $('#itemInfoTable tbody tr').each(function () {
                if($(this).find("td").eq(0).find('input:checkbox').length > 0 && $(this).find("td").eq(0).find('input:checkbox').prop("checked")) {
                    isChecked = true;
                }
            });
            if(!isChecked){
                alert('상품을 선택해주세요.');
                return false;
            }
        }

        //신청수량 변경유무 확인
        var qtyBoolean = true;
        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = $(this).find("select").eq(0).val();
            var orderQtyCompare = $(this).find("input[name='orderQtyCompare']").val();
            if (orderQty == orderQtyCompare) {
                return true;
            } else {
                qtyBoolean = false;
                return false;
            }
        });
        if (!qtyBoolean && !isMarket) {
            alert('신청수량이 변경됐습니다. 환불예정금액을 다시 확인해 주세요');
            return false;
        }

        //반품 사유/상세 사유 확인
        if ($('#claimReasonType').val() == '' || $('#claimReasonType').val() == null) {
            alert('반품 사유를 선택해주세요');
            return false;
        }

        //사유변경 확인
        if ($('#claimReasonType').val() != $('#oriClaimReasonType').val() && !isMarket) {
            alert('반품 사유가 변경됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if($('#detailReasonRequired').val() == 'Y'){
            if($.jUtil.isEmpty($('#claimReasonDetail').val())){
                alert('반품 상세사유를 입력해주세요');
                return false;
            }
        }

        if(!$.jUtil.isEmpty(multiShipList)){
            if(multiShipList != oriMultiShipList && !isMarket){
                alert('배송지가 변경됐습니다.\n환불예정금액을 다시 조회해 주세요');
                return false;
            }
        }

        //환불예정금액 확인
        if (parseInt($('#refundAmt').text().replace(',','')) < 0) {
            alert('환불예정금액이 마이너스(-)가 되어 반품 신청이 불가합니다.\n차감여부와 반품비용 결제방법을 변경해주시기 바랍니다.');
            return false;
        }

        //수거지 이름
        if ($.jUtil.isEmpty($('#pickReceiverNm').val())) {
            alert('받는사람 이름을 입력주세요');
            return false;
        }

        if($('#pickReceiverNm').val().length < 2) {
            alert('받는사람 이름을 2자 이상 입력해주세요.');
            return false;
        }


        //수거지 연락처
        if ($.jUtil.isEmpty($('#pickMobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($('#pickMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#pickMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#pickMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        //수거지 우편번호
        if ($.jUtil.isEmpty($('#pickZipCode').val())) {
            alert('수거지 우편번호를 입력주세요');
            return false;
        }
        //수거지 주소
        if ($.jUtil.isEmpty($('#pickRoadBaseAddr').val())) {
            alert('수거지 주소를 입력주세요');
            return false;
        }
        //수거지 상세주소
        if ($.jUtil.isEmpty($('#pickRoadDetailAddr').val())) {
            alert('수거지 상세주소를 입력주세요');
            return false;
        }
        //상품발송 여부 Y
        if($(':radio[name="isPick"]:checked').val() == 'Y') {
            if ($('#dlvCd').val() == '' || $('#dlvCd').val() == null) {
                alert('택배사를 선택해주세요');
                return false;
            }
            if ($('#invoiceNumber').val() == '' || $('#invoiceNumber').val() == null) {
                alert('송장번호를 입력해주세요');
                return false;
            }
        }

        if(piDeductPromoYn != oriPiDeductPromoYn && !isMarket){
            alert('행사 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(piDeductDiscountYn != oriPiDeductDiscountYn && !isMarket){
            alert('할인 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(piDeductShipYn != oriPiDeductShipYn && !isMarket){
            alert('배송비 미차감 여부가 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        if(oriIsEnclose != isEnclose && !isMarket){
            alert('반품비용 결제방법이 변경 됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }

        return true;
    },

    /**
     * 반품신청 약식 유효성 검사
     * **/
    validationMin : function () {
        var isMarket = JSON.parse($('#isMarket').val());

        //환불예정금액 확인 유무
        if ($('#preRefundAmtReturnCode').val() != '0' && !isMarket) {
            $('#preRefundInfo').val('');
            alert('환불예정금액을 확인해 주세요');
            return false;
        }

        if(isMarket) {
            var isChecked = false;
            $('#itemInfoTable tbody tr').each(function () {
                if($(this).find("td").eq(0).find('input:checkbox').length > 0 && $(this).find("td").eq(0).find('input:checkbox').prop("checked")) {
                    isChecked = true;
                }
            });
            if(!isChecked){
                alert('반품할 대상을 선택해주세요.');
                return false;
            }
        }

        //신청수량 변경유무 확인
        var qtyBoolean = true;
        $('#itemInfoTable tbody tr').each(function () {
            var orderQty = $(this).find("select").eq(0).val();
            var orderQtyCompare = $(this).find("input[name='orderQtyCompare']").val();
            if (orderQty == orderQtyCompare) {
                return true;
            } else {
                qtyBoolean = false;
                return false;
            }
        });
        if (!qtyBoolean && !isMarket) {
            alert('신청수량이 변경됐습니다. 환불예정금액을 다시 확인해 주세요');
            return false;
        }

        //반품 사유/상세 사유 확인
        if ($('#claimReasonType').val() == '' || $('#claimReasonType').val() == null) {
            alert('반품 사유를 선택해주세요');
            return false;
        }

        if($('#detailReasonRequired').val() == 'Y'){
            if($.jUtil.isEmpty($('#claimReasonDetail').val())){
                alert('반품 상세사유를 입력해주세요');
                return false;
            }
        }

        //사유변경 확인
        if ($('#claimReasonType').val() != $('#oriClaimReasonType').val() && !isMarket) {
            alert('반품 사유가 변경됐습니다.\n환불예정금액을 다시 조회해 주세요');
            return false;
        }
        //환불예정금액 확인
        if (parseInt($('#refundAmt').text().replace(',','')) < 0) {
            alert('환불예정금액이 마이너스(-)가 되어 반품 신청이 불가합니다.\n차감여부와 반품비용 결제방법을 변경해주시기 바랍니다.');
            return false;
        }

        return true;
    },


    /**
     * 반품요청
     * **/
    submit: function () {
        var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
        var isPick = $(':radio[name="isPick"]:checked').val(); //수거여부
        $('#pickMobileNo').val($.jUtil.valNotSplit('pickMobileNo', '-'));

        var pickInvoiceNo;//상품 발송 송장번호
        var pickDlvCd;//상품발송 택배사 코드
        var pickShipDt;
        var shiftId;
        var slotId;

        var validSkipVal;

        //수거 안함 선택 시 유효성 검사 skip하기 위해 배송 방법 조회
        if (shipType === 'TD' || shipType === 'AURORA') {
            if(shipMethod == 'TD_QUICK') {
                validSkipVal = 'N'
            }else{
                validSkipVal = $("input:radio[name='isPickReqTd']:checked").val();
            }
        }else if(shipType == 'PICK'){
            validSkipVal = 'N'
        }else if(shipType == 'DS'){
            validSkipVal = 'Y'
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                validSkipVal = $("input:radio[name='isPickReqTd']:checked").val();
            }
        }

        if(validSkipVal != 'N'){
            //취소요청 유효성 검사
            if (!orderReturn.validation()) {
                return false;
            }

            //슬롯 정보 추가
            if (shipType === 'TD' || shipType === 'AURORA') {
                if(shipMethod != 'TD_QUICK') {
                    if ($.jUtil.isEmpty(selectedInfo)) {
                        alert('수거 배송시간을 선택해주세요');
                        return false;
                    }

                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    pickShipDt = orderUtil.toDateFormatting(idInfo[1]);
                    slotId = idInfo[3];
                    shiftId = idInfo[2];
                }
            }else{
                if('TD_DRCT'.indexOf(shipMethod) > -1) {
                    if ($.jUtil.isEmpty(selectedInfo)) {
                        alert('수거 배송시간을 선택해주세요');
                        return false;
                    }

                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    pickShipDt = orderUtil.toDateFormatting(idInfo[1]);
                    slotId = idInfo[3];
                    shiftId = idInfo[2];
                }
            }
        }else{
            if (!orderReturn.validationMin()) {
                return false;
            }
        }

        var isPickReq;
        //수거 안함 여부 체크
        //TD,AURORA,PICK 경우 isPickReqTd radio 버튼 이 'N'인지 여부 체크
        if(shipType === 'TD' || shipType === 'AURORA' || shipType === 'PICK'){
            isPickReq = $("input:radio[name='isPickReqTd']:checked").val() == 'N' ? 'N' : 'Y';
        }else if(shipType === 'DS') {
            isPickReq = $('input:radio[name="isPickReq"]:checked').val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                isPickReq = $("input:radio[name='isPickReqTd']:checked").val() == 'N' ? 'N' : 'Y';
            }else{
                isPickReq = $('input:radio[name="isPickReq"]:checked').val();
            }
        }


        //상품 발송여부 체크
        //상품을 보낸 상태시 택배 정보 추가
        if (isPick === 'Y') {
            pickInvoiceNo = $('#invoiceNumber').val().replace(' ','');
            pickDlvCd = $('#dlvCd').val();
        }

        //deliveryYn : Y = 배송중, 배송중인 경우 호출되는 얼럿
        if($('#deliveryYn').val() == 'Y') {
            if (!confirm('신청한 상품은 배송중 상태입니다.\n배송완료로 상태가 변경됩니다. 반품신청 하시겠습니까?')) {
                return false;
            }
        }else{
            if (!confirm('반품신청 하시겠습니까?')) {
                return false;
            }
        }

        //이미지 업로드
        for (var seq = 1; seq < 4; seq ++) {
            if (!$.jUtil.isEmpty($('#fileName'+seq).text())) {
                claimImg.uploadImg(seq,$('#imgFile'+seq));
            }
        }

        /**
         * 반품요청 파라미터 설정
         * **/
        //파라미터 설정
        var detailParam = {
            'isEnclose' : $('#isEnclose').val(),
            'isPick' : isPick,
            'isPickReq' : isPickReq,
            'pickInvoiceNo' : pickInvoiceNo,
            'pickDlvCd' : pickDlvCd,
            'pickReceiverNm' : $('#pickReceiverNm').val(),
            'pickZipCode' : $('#pickZipCode').val(),
            'pickRoadBaseAddr' : $('#pickRoadBaseAddr').val(),
            'pickRoadDetailAddr' : $('#pickRoadDetailAddr').val(),
            'pickMobileNo' : $('#pickMobileNo').val(),
            'pickBaseAddr' : $('#pickBaseAddr').val(),
            'pickBaseDetailAddr' : $('#pickRoadDetailAddr').val(),
            'shipDt' : pickShipDt,
            'slotId' : slotId,
            'shiftId' : shiftId,
            'uploadFileName' : $('#uploadFileName1').text(),
            'uploadFileName2' : $('#uploadFileName2').text(),
            'uploadFileName3' : $('#uploadFileName3').text(),
            'shipType' : shipType
        };

        //parameter 세팅
        var param = {
            "purchaseOrderNo": $('#purchaseOrderNo').val(),
            "claimItemList": orderPopupCommon.getClaimReqList('itemInfoTable'),
            "claimReasonType": $('#claimReasonType').val(),
            "bundleNo" : $('#bundleNo').val(),
            "claimReasonDetail" : $('#claimReasonDetail').val(),
            "orderCancelYn" : 'N',
            "claimDetail" : detailParam,
            "piDeductPromoYn" : $("input:checkbox[id='piDeductPromoYn']").is(":checked") == true ? 'Y' : 'N',
            "piDeductDiscountYn" : $("input:checkbox[id='piDeductDiscountYn']").is(":checked") == true ? 'Y' : 'N',
            "piDeductShipYn" : $("input:checkbox[id='piDeductShipYn']").is(":checked") == true ? 'Y' : 'N',
            "multiShipYn" : $('#multiShipYn').val()
        };

        if($('#multiShipYn').val() == 'Y'){
            param.multiBundleNo = $('#multiShipList').val();
        }

        //파라미터 세팅
        //반품요청
        CommonAjax.basic(
            {
                url: '/order/claim/claimRegister.json',
                data: JSON.stringify(param),
                method: "POST",
                contentType: 'application/json',
                successMsg: null,
                callbackFunc: function (res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1) {
                        alert('반품신청이 완료되었습니다.')
                        opener.location.reload();
                        self.close();
                    }else{
                        alert('반품신청 실패 [' + res.returnMsg + ']');
                        return false;
                    }
                }
            });
    },
    getParam : function () {
        return param;
    }
};