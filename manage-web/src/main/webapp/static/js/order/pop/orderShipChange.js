$(document).ready(function() {
    orderShipChange.init();
    CommonAjaxBlockUI.global();
});

var orderShipChange = {
    init : function () {
        this.pageInit();
        this.orderShipSearch();
        this.bindingEvent();
    },
    pageInit : function (){
        $('#gibunAddrArea').css('display', 'none');
        $('#auroraArea, #pickerArea').hide();
        orderUtil.setWindowAutoResize();
    },
    roadInit : function (){
        $('#receiverNm').setTextLength('#textCountKr_receiverNm');
        $('#shipMsg').setTextLength('#textCountKr_shipMsg');
        $('#auroraShipMsg').setTextLength('#textCountKr_auroraShipMsg');
        $('#roadDetailAddr').setTextLength('#textCountKr_roadDetailAddr');

        $.jUtil.valSplit($('#shipMobileNo').val(), '-', 'shipMobileNo');
        if($('#baseAddr').text().length > 0){
            $('#gibunAddrArea').show();
        }
        if($.jUtil.isNotEmpty($('#personalOverseaNo').val())){
            $('#personalOverseaNo').removeAttr("readonly");
        }
        this.changeShipMsgUI();
    },
    orderShipSearch : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderShipAddrInfo.json?bundleNo=' + bundleNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToData(res);
                    orderShipChange.roadInit();
                    if(!$.jUtil.isEmpty(res[0].auroraShipMsgCd) || !$.jUtil.isEmpty(res[0].auroraShipMsg)){
                        if(shipType !== 'DS') {
                            $('#auroraArea').show();
                            $('#auroraShipMsgCd').removeAttr("disabled");
                            orderShipChange.changeAuroraShipMsgValue();
                        }
                    }
                    if(!$.jUtil.isEmpty(res[0].placeNm) || !$.jUtil.isEmpty(res[0].lockerNo) || !$.jUtil.isEmpty(res[0].lockerPasswd)){
                        $('#pickerArea').show();
                    }
                    if($.jUtil.isNotEmpty(res[0].safetyNm)){
                        if(/^[0-9_-]*$/.test(res[0].safetyNm) !== false){
                            $('#safetyNm').text(orderUtil.phoneFormatter(res[0].safetyNm));
                        }
                    }
                    if(res[0].shipType === 'SUM'){
                        $('#shipMobileNo_1, #shipMobileNo_2, #shipMobileNo_3, #shipMobileNo, #roadDetailAddr').attr("readonly", true);
                    }
                    if(res[0].orderType === 'ORD_MARKET') {
                        $('#receiverNm, #shipMobileNo_1, #shipMobileNo_2, #shipMobileNo_3, #roadDetailAddr, #shipMsg, #auroraShipMsg').attr("readonly", true);
                        $('#shipMsgCd, #auroraShipMsgCd').prop('disabled', true);
                    }
                    orderUtil.setWindowAutoResize();
                }
        });
    },
    validate : function() {
        const receiverNm = $('#receiverNm');
        if (!receiverNm.val() || receiverNm.val().length < 2 || receiverNm.val().indexOf(' ') > -1) {
            alert("이름이 올바르게 입력되지 않았습니다.\n 2자리 이상 입력해 주세요.");
            receiverNm.focus();
            return false;
        }
        if (receiverNm.val().length > 20) {
            alert("이름은 20자까지만 입력해주세요.");
            receiverNm.focus();
            return false;
        }
        if(!$.jUtil.isAllowInput(receiverNm.val(), ['ENG', 'NUM', 'KOR'])) {
            alert("이름은 특수문자 입력이 불가합니다.");
            receiverNm.focus();
            return false;
        }
        const shipMobileNo = $('#shipMobileNo');
        shipMobileNo.val($.jUtil.valNotSplit('shipMobileNo', '-'));
        if (!orderUtil.chkTelNum(shipMobileNo.val(), true)) {
            $('[id^=shipMobileNo_]').each(function (index) {
                if(index !== 0){
                    if (!$.jUtil.isAllowInput($(this).val(), ['NUM']) || $(this).val().length < 3) {
                        alert("3~4자리 숫자만 사용가능 합니다.");
                        $('#shipMobileNo_' + (index + 1)).focus();
                        return false;
                    }
                }
            });
            return false;
        }
        const shipMsgCd = $('#shipMsgCd');
        const shipMsg = $('#shipMsg');
        const auroraShipMsg = $('#auroraShipMsg');
        if ((shipMsgCd.val() === 'DSM04' | shipMsgCd.val() === 'SSM06')
            && $.jUtil.isEmpty(shipMsg.val())) {
            $.jUtil.alert('배송시 요청사항이 입력되지 않았습니다.', shipMsg);
            return false;
        }
        if(shipMsg.val().length > 50){
            alert("배송시 요청사항은 50자까지 입력할 수 있습니다.");
            shipMsg.focus();
            return false;
        }
        if(auroraShipMsg.val().length > 30){
            alert("공동현관 출입방법은 요청사항은 30자까지 입력할 수 있습니다.");
            shipMsg.focus();
            return false;
        }
        var savePattern = '-a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣.' + '~!@#$%\\^&*\\(\\)-=_+\\{\\}\\[\\]:;\\<\\>?,/\\|/\\s';
        if(!new RegExp('^['.concat(savePattern).concat(']+$')).test(shipMsg.val())){
            alert("배송시 요청사항은 특수문자 입력이 불가합니다.");
            shipMsg.focus();
            return false;
        }
        if(auroraShipMsg.val().length > 0) {
            if(!new RegExp('^['.concat(savePattern).concat(']+$')).test(auroraShipMsg.val())){
                alert("공동현관 출입방법은 특수문자 입력이 불가합니다.");
                auroraShipMsg.focus();
                return false;
            }
        }

        const roadDetailAddr = $('#roadDetailAddr');
        if (!roadDetailAddr.val() || roadDetailAddr.val().length < 1) {
            alert("주소가 입력되지 않았습니다. \n 주소는 100자까지 입력할 수 있습니다.");
            receiverNm.focus();
            return false;
        }
        if(!new RegExp('^[-a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣\\s.]+$').test(roadDetailAddr.val())){
            alert("주소는 특수문자 입력이 불가합니다.");
            roadDetailAddr.focus();
            return false;
        }
        // const personalOverseaNo = $('#personalOverseaNo');
        // if( /^[A-Za-z0-9+]*$/.test(personalOverseaNo.val())){
        //     alert("주소는 특수문자 입력이 불가합니다.");
        //     personalOverseaNo.focus();
        //     return false;
        // }
        return true;
    },
    setShipData : function () {
        if (!orderShipChange.validate()) return false;
        const param = $('#orderAddrChangeForm').serializeObject();
        param.detailAddr = $('#detailAddr').text();
        param.baseAddr = $('#baseAddr').text();
        param.bundleNo = bundleNo;

        if ($.jUtil.isEmpty(param.baseAddr)) {
            param.detailAddr = null;
            param.baseAddr = null;
        }
        if (confirm("배송정보를 수정하시겠습니까?")) {
            CommonAjax.basic({
                url:'/order/shipAddrChange.json',
                data: JSON.stringify(param),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                        opener.location.reload();
                        self.close();
                    }
                }
            });
        }
    },
    bindingEvent : function () {
        $('#shipMsgCd').on('change', function () {
            orderShipChange.changeShipMsgValue();
            orderShipChange.changeShipMsgUI();
        });
        $('#auroraShipMsgCd').on('change', function () {
            orderShipChange.changeAuroraShipMsgValue();
        });
        const roadDetailAddr = $('#roadDetailAddr');
        $('#roadDetailAddr, #receiverNm, #shipMsg, #auroraShipMsg').on('propertychange change paste input', function () {
            var pattern = '-a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣.';
            var alertMsg = '';
            switch ($(this).attr('id')) {
                case 'roadDetailAddr' :
                    alertMsg = '주소는';
                    pattern += '\\s';
                    break;
                case 'shipMsg' :
                    alertMsg = '배송시 요청사항은';
                    // pattern += '~!@#$%^&*()-=_+{}[]:;<>?,/\\|';
                    pattern += '~!@#$%\\^&*\\(\\)-=_+\\{\\}\\[\\]:;\\<\\>?,/\\|/\\s';
                    break;
                case 'auroraShipMsg' :
                    alertMsg = '공동현관 출입방법은';
                    // pattern += '~!@#$%^&*()-=_+{}[]:;<>?,/\\|';
                    pattern += '~!@#$%\\^&*\\(\\)-=_+\\{\\}\\[\\]:;\\<\\>?,/\\|\\s';
                    break;
                case 'receiverNm' :
                    alertMsg = "이름은";
                    break;
            }
            if($(this).val().length > 0) {
                if(!new RegExp('^['.concat(pattern).concat(']+$')).test($(this).val())){
                    alert(alertMsg + ' 특수문자 입력이 불가합니다.');
                    $(this).val($(this).val().replace(new RegExp('[^'.concat(pattern).concat(']'), "gi"), ''));
                    return false;
                }
            }
            if($(this).attr('id') === 'roadDetailAddr') {
                $('#detailAddr').text(roadDetailAddr.val());
                roadDetailAddr.trigger('keyup');
            }
        });
        roadDetailAddr.calcTextLength('keyup', '#textCountKr_roadDetailAddr');
        $('#shipMsg').calcTextLength('keyup', '#textCountKr_shipMsg');
        $('#auroraShipMsg').calcTextLength('keyup', '#textCountKr_auroraShipMsg');
        $('#receiverNm').calcTextLength('keyup', '#textCountKr_receiverNm');
        $('#saveBtn').bindClick(function() { orderShipChange.setShipData(); });
        $('#personalOverseaNo').keyup(function (e) {
            if(!(e.keyCode >= 37 && e.keyCode <= 40)){
                $(this).val($(this).val().replace(/[^a-zA-Z0-9]/gi, ''));
            }
        });
    },
    changeShipMsgValue : function () {
        var selectedData = $('#shipMsgCd option:selected');
        var shipMsg = $('#shipMsg');

        if ($.jUtil.isEmpty(selectedData.val())) {
            shipMsg.val('').trigger('keyup');
            shipMsg.attr("disabled", true);
        } else {
            if (!this.isDirectInput(selectedData)) {
                shipMsg.val(selectedData.text()).trigger('keyup');
                shipMsg.removeAttr("disabled");
            }
        }
    },
    changeAuroraShipMsgValue : function () {
        var selectedData = $('#auroraShipMsgCd option:selected');
        var auroraShipMsg = $('#auroraShipMsg');

        if ($.jUtil.isEmpty(selectedData.val())) {
            auroraShipMsg.val('').trigger('keyup');
            auroraShipMsg.attr("disabled", true);
        } else {
            if (!(selectedData.text().search('자유출입가능') > -1)) {
                auroraShipMsg.trigger('keyup');
                auroraShipMsg.removeAttr("disabled");
                auroraShipMsg.focus();
            } else {
                auroraShipMsg.val('').trigger('keyup');
                auroraShipMsg.attr("disabled", true);
            }
        }
    },
    changeShipMsgUI : function () {
        var selectedData = $('#shipMsgCd option:selected');
        var shipMsg = $('#shipMsg');

        if ($.jUtil.isEmpty(selectedData.val())) {
            shipMsg.prop('maxLength', 100);
        } else {
            if (this.isDirectInput(selectedData)) {
                shipMsg.prop('maxLength', 50);
            } else {
                shipMsg.prop('maxLength', 100);
            }
        }
        $('#textTotalCountKr_shipMsg').text(shipMsg.prop('maxlength'));
    },
    isDirectInput : function (selectedData) {
        return selectedData.text().search('직접입력') > -1 || selectedData.text().search('기타') > -1;
    },
    setZipcode : function(res){
        if(shipAreaType === res.islandType){
            $("#zipCode").val(res.zipCode);
            $("#roadBaseAddr").val(res.roadAddr);
            $('#baseAddr').text(res.gibunAddr);

        } else {
            alert('변경할 수 없는 지역입니다.');
        }
    }
};