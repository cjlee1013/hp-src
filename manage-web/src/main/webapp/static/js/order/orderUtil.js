const orderUtil = {
    calculationOfDate : function (basicDate, flag) {
        var pattern = /([+\-]?[0-9]+)\s*(s|S|h|H|d|D|w|W|m|M|y|Y)?/g;
        var matches = pattern.exec(flag);
        return moment(basicDate).add(matches[1], matches[2])
    },
    calculationOfDateWithFormat : function (_data) {
        var pattern = /([+\-]?[0-9]+)\s*(s|S|h|H|d|D|w|W|m|M|y|Y)?/g;
        var matches = pattern.exec(_data.flag);
        return moment(_data.basicDate).add(matches[1], matches[2]).format(_data.format);
    },
    setLinkTab : function (_tabId, _tabName, _url, _tabSize) {
        var subTabBar = parent.Tabbar;
        if(_tabSize == null){
            _tabSize = 150;
        }
        if(subTabBar.getAllTabs().indexOf(_tabId) !== -1) {  //탭이 이미 있다면 데이터 갱신 후 활성화
            subTabBar.tabs(_tabId).attachURL(_url);
            subTabBar.tabs(_tabId).setActive();
        }
        else {
            subTabBar.addTab(_tabId, _tabName, _tabSize, null, true, true);
            subTabBar.tabs(_tabId).attachURL(_url);
            subTabBar.enableAutoReSize();
        }
    },
    setRealGridColumnLink : function(_gridView, _columnIds, _url, _linkView){
        var _columnArray = _columnIds;
        if(!Array.isArray(_columnIds)){
            _columnArray = [];
            _columnArray = [_columnIds];
        }
        for(var i=0;i<_columnArray.length;i++){
            _gridView.gridView.setColumnProperty(_columnArray[i], "renderer", {
                type : "link",
                url : _url,
                requiredFields : _columnArray[i],
                showUrl : _linkView
            });
        }
    },
    getRealGridColumnValue : function (_gridView, _findText, _rowId) {
        var columnValue = "";
        $.each(_gridView.gridView.getColumns(), function (idx, data) {
            if(data.type === "group"){
                $.each(data.columns, function (_gIdx, _dData) {
                    if(_gridView.gridView.getColumnProperty(_dData, "header").text === _findText){
                        columnValue = _gridView.dataProvider.getJsonRow(_rowId)[_dData.name];
                        return false;
                    }
                });
            }else{
                if(_gridView.gridView.getColumnProperty(data, "header").text === _findText){
                    columnValue = _gridView.dataProvider.getJsonRow(_rowId)[data.name];
                    return false;
                }
            }
        });

        return columnValue;
    },
    writeToSpan : function (_formId, _data) {
        var value = "";
        var spanId = $('#' + _formId).find('span');
        for(var idx = 0; idx < spanId.length; idx++){
            value = _data[spanId[idx].id];
            if($.jUtil.isEmpty(value)){
                value = '-';
            }
            var checkId = spanId[idx].id.toLowerCase();
            if(checkId.indexOf("amt") > -1 || checkId.indexOf("price") > -1){
                if(!isNaN(Number(value))){
                    value = $.jUtil.comma(Number(value));
                }
            }
            if(!$.jUtil.isEmpty(spanId[idx].id)){
                $(spanId[idx]).html(value);
            }
        }
    },
    writeToData : function (_dataArray) {
        if(!Array.isArray(_dataArray)){
            _dataArray = [_dataArray];
        }
        for(var i=0;i<_dataArray.length;i++){
            var keys = Object.keys(_dataArray[i]);

            for(var j=0;j<keys.length;j++){
                var keyValue = $('#' + keys[j]);
                if(keyValue.length){
                    if($('input[id=' + keys[j] + "]").length || $('#' + keys[j] + ' option:selected').length){
                        keyValue.val(_dataArray[i][keys[j]]);
                    } else {
                        keyValue.text(_dataArray[i][keys[j]]);
                    }
                }
            }
        }
    },
    checkBoxChecked : function (_selectedInfo, _allValue){
        var unCheckedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']');
        var checkedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']:checked');

        if(_selectedInfo.val() === _allValue){
            unCheckedBox.each(function (_idx, _value) {
                if(_value.value !== _allValue){
                    this.checked = _selectedInfo.is(':checked');
                }
            });
        } else {
            var checkedValue = checkedBox.map(function () {
                return this.value;
            }).get();
            if(checkedBox.length === (unCheckedBox.length - 1)){
                var isAllCheck = false;
                if(checkedValue.indexOf(_allValue) === -1){
                    isAllCheck = true;
                }
                unCheckedBox.each(function () {
                    if(this.value === _allValue){
                        this.checked = isAllCheck;
                    }
                });
            }
        }
    },
    buttonOnOff : function (_buttonArea, _onButtonIdArray) {
        var buttonInfo = $('#' + _buttonArea).children('button');
        buttonInfo.each(function () {
            if(_onButtonIdArray != null && _onButtonIdArray.indexOf($(this).html()) > -1){
                $(this).removeAttr('disabled');
            } else {
                if($(this).attr('id') !== 'claimAllBtn') {
                    $(this).attr('disabled', true);
                }
            }
        })
    },
    setRealGridCheckable : function (_grid, _compareFieldName) {
        // 비교를 위한 데이터 취득 용
        var compareValue = 0;
        var checkable = '';
        for(var _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
            if(compareValue === _grid.dataProvider.getValue(_idx, _compareFieldName)){
                if(checkable.length !== 0){
                    checkable += ' AND '
                }
                checkable += '(row <> ' + _idx + ')';
            }
            compareValue = _grid.dataProvider.getValue(_idx, _compareFieldName);
        }
        return checkable;
    },
    setRealGridArraysCheckable : function (_grid, _array, _compareFieldName) {
        // 비교를 위한 데이터 취득 용
        var compareValue = 0;
        var checkable = '';
        var notCheckedBundle = _array.join(",");
        for(var _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
            var findValue = _grid.dataProvider.getValue(_idx, _compareFieldName);
            if(compareValue === findValue || notCheckedBundle.indexOf(findValue) > -1){
                if(checkable.length !== 0){
                    checkable += ' AND '
                }
                checkable += '(row <> ' + _idx + ')';
            }
            compareValue = findValue;
        }
        return checkable;
    },
    setRealGridCheckBarDisabled : function (_grid) {
        // 비교를 위한 데이터 취득 용
        var checkable = '';
        for(var _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
            if(checkable.length !== 0){
                checkable += ' AND '
            }
            checkable += '(row <> ' + _idx + ')';
        }
        return checkable;
    },
    getRealGridMergeData : function (_grid, _mergeColumn, _mergeData) {
        var returnMergeData = [];
        for(var _idx = 0; _idx < _grid.dataProvider.getRowCount(); _idx++){
            if(_mergeData === _grid.dataProvider.getValue(_idx, _mergeColumn)){
                returnMergeData[_idx] = _idx;
            }
        }
        return $.grep(returnMergeData, function (n) { return n === 0 || n });
    },
    getWinPopup  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target === 'undefined' || _target == null || _target === '') {
            _target ='popup-search';
        }
        windowPopupOpen(url, _target, _width, _height, 'yes', 'no');
    },
    setRealGridMerge : function (_grid, _mergeColumns, _mergeRule, _mergeCondition) {
        for(var i=0;i<_mergeColumns.length;i++){
            var criteria = {
                criteria : $.jUtil.isEmpty(_mergeCondition) ? "value" : "values['".concat(_mergeCondition).concat("']")
            };
            _grid.gridView.setColumnProperty(_mergeColumns[i], _mergeRule, criteria );
        }
    },
    setRealGridUnMerge : function (_grid, _mergeColumns, _mergeRule) {
        for(var i=0;i<_mergeColumns.length;i++){
            _grid.gridView.setColumnProperty(_mergeColumns[i], _mergeRule, null );
        }
    },
    setRealGridColumnVisible : function (_grid, _visibleColumns) {
        for(var i=0;i<_visibleColumns.length;i++){
            _grid.gridView.setColumnProperty(_visibleColumns[i], "visible", true);
        }
    },
    setRealGridColumnUnVisible : function (_grid, _visibleColumns) {
        for(var i=0;i<_visibleColumns.length;i++){
            _grid.gridView.setColumnProperty(_visibleColumns[i], "visible", false);
        }
    },
    setRealGridChangeHeaderText : function (_grid, _findColumnId, _changeText) {
        var headerInfo = _grid.gridView.getColumnProperty(_findColumnId, "header");
        headerInfo.text = _changeText;
        _grid.gridView.setColumnProperty(_findColumnId, "header", headerInfo);
    },
    setRadioCheck : function () {
        if(arguments.length > 0){
            for(var i=0;i<arguments.length;i++){
                arguments[i].gridView.setCheckBar({"exclusive": "true", "showGroup":"true"});
            }
        }
    },
    setCheckBarHeader : function (_grid, _headerText, _headerImg) {
        for(var i=0;i<_grid.length;i++){
            if(!$.jUtil.isEmpty(_headerText)){
                _grid[i].gridView.setCheckBar({"width" : 40, "showAll" : false, "headText" : _headerText, "footText" : null, "headImageUrl" : null, "footImageUrl": null });
            } else {
                _grid[i].gridView.setCheckBar({"showAll" : "false", "headText" : "null", "footText" : "null", "headImageUrl" : _headerImg, "footImageUrl": "null" });
            }
        }
    },
    getSplitData : function (_data, _separator) {
        return _data.split($.jUtil.isEmpty(_separator) ? (/[,\\|:_\-]/g) : _separator);
    },
    getCamelCaseSplit : function (_camelcaseStr) {
        return this.getSplitData(_camelcaseStr.replace(/([a-zA-Z0-9])(?=[A-Z])/g, '$1|'));
    },
    setWindowResize : function (_x, _y) {
        return window.resizeBy(_x, _y);
    },
    setWindowAutoResize : function () {
        var innerDivHeight = $('div:first').height();
        var width = $( document ).outerWidth(true) - $( window ).outerWidth(true);
        var height = (window.outerHeight - window.innerHeight);
        this.setWindowResize(width, - window.innerHeight);
        this.setWindowResize(width, (innerDivHeight + (height/2)));
    },
    setToggleDisplay : function (_selector, _toggle) {
        // _toggle 이 없는 경우 false
        if($.jUtil.isEmpty(_toggle) ? false : _toggle){
            _selector.show();
        } else {
            _selector.hide();
        }
    },
    setToggleOnDisplay : function () {
        if(arguments.length > 0) {
            for(var i=0;i<arguments.length;i++){
                if(arguments[i].length > 0){
                    this.setToggleDisplay(arguments[i], true);
                }
            }
        }
    },
    setToggleOffDisplay : function () {
        if(arguments.length > 0) {
            for(var i=0;i<arguments.length;i++){
                if(arguments[i].length > 0){
                    this.setToggleDisplay(arguments[i], false);
                }
            }
        }
    },
    writeToSlotTable : function (_slotData) {
        var frontSlotHeader = this.setDictionaryToArray(_slotData.frontSlotHeader);
        var frontSlotMap = this.setDictionaryToArray(_slotData.frontSlotMap);
        var storePickupPlaceList = this.setDictionaryToArray(_slotData.storePickupPlaceList);

        var table = $('<table>');
        var tableColGroup = $('<colgroup>');
        var thead = $('<thead>');
        var tbody = $('<tbody>');
        var tableRow = $('<tr>');

        if(frontSlotHeader.size <= 0){
            alert('배송지 SLOT 정보가 없습니다.');
            return false;
        }
        var dataChecker = 0;
        // table caption 추가
        table.append($('<caption>').html('배송SLOT 테이블'));
        // 배송시간 표시용 col을 추가한다.
        tableColGroup.append($('<col>').attr('width', '220px'));
        tableColGroup.append($('<col>').attr('span', 5).attr('width', '196px'));
        table.append(tableColGroup);

        // 헤더 설정
        $.each(frontSlotHeader, function (key, value) {
            if(dataChecker++ === 0) {
                thead.append($('<th>').html('배송시간대'));
            }
            thead.append($('<th>').html(new Date() === new Date(key) ? '오늘'.concat('(').concat(value).concat(')') : key.concat('(').concat(value).concat(')')));
        });
        // 총 5일치에 대한 슬롯정보 중 모자란 경우 td로 padding
        while(dataChecker++ < 5){
            thead.append($('<td>'));
        }
        table.append(thead);

        var index = 0;
        // 주문마감/가능 여부 표시
        $.each(frontSlotMap, function (key, value) {
            tableRow = $('<tr>');
            tableRow.append($('<th>').text(key).attr('scope', 'row'));
            for(var j=0;j<value.length;j++){
                var valueText = [
                    ($.jUtil.isEmpty(value[j].placeNo) ? '주문' : '픽업'),
                    value[j].slotStatus === 'END' ? '마감' : value[j].slotStatus === 'CLOSE' ? '휴무' : value[j].slotStatus === 'READY' ? '준비중' : value[j].slotStatus === 'DAILY_END' ? '선택불가' : '가능'
                ];
                var td = $('<td>');
                var radioId = [
                    ($.jUtil.isEmpty(value[j].placeNo) ? 'dict' : 'pick'),
                    value[j].shipDt.split('-').join(''),
                    value[j].shiftId,
                    value[j].slotId
                ].join("_");
                if(valueText[1] === '마감' || valueText[1] === '준비중' || valueText[1] === '선택불가' ) {
                    td.attr('class', 'finish').text(valueText[1] === '마감' ? valueText.join('') : valueText[1]);
                    tableRow.append(td);
                    continue;
                }
                if(valueText[1] === '휴무'){
                    if(index === 0){
                        td.attr('rowspan', Object.keys(frontSlotMap).length).attr('class', 'CLOSE').text(valueText[1]);
                        tableRow.append(td);
                    }
                    continue;
                }
                td.append($('<input>').attr('type', 'radio').attr('name', 'changeSlotInfo').attr('id', radioId));
                td.append('&nbsp;');
                // td.append($('<span>').attr('class', 'text').text(valueText));
                td.append($('<label>').attr('for', radioId).attr('class', 'lb-radio').text(valueText.join('')));
                tableRow.append(td);
                //<label for="drct_20200803_2502" class="lb-radio">주문가능</label>
            }
            tbody.append(tableRow);
            index++;
        });
        table.append(tbody);
        return table;
    },
    setDictionaryToArray : function (_dictionary) {
        if(!$.jUtil.isEmpty(_dictionary)){
            var returnMap = {};
            $.each(_dictionary, function (key, value) {
                returnMap[key] = value;
            });
            return returnMap;
        }
        return null;
    },
    sortByStringDesc : function (_data) {
        _data.sort(function ( a, b ) {
            return a > b ? -1 : a < b ? 1 : 0;
        });
        return _data;
    },
    sortByStringAsc : function (_data) {
        _data.sort(function ( a, b ) {
            return a < b ? -1 : a > b ? 1 : 0;
        });
        return _data;
    },
    toDateFormatting : function (_date) {
        return _date.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3');
    },
    phoneFormatter : function (num){
        if(/^[0-9_-]*$/.test(num) === false){
            alert("전화번호에는 문자가 포함될수 없습니다");
            return;
        }

        var formatNum = '';
        num = num.replace(/[^0-9]/g, '');
        if(num.length === 12){
            formatNum = num.replace(/(\d{4})(\d{4})(\d{4})/, '$1-$2-$3');
        }
        else if(num.length==8){
            formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
        }else{
            if(num.indexOf('02')==0){
                formatNum = num.replace(/(\d{2})(\d{3,4})(\d{4})/, '$1-$2-$3');
            }else{
                formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
            }
        }
        return formatNum;
    },
    /**
     * 일반 전화번호 체크
     * barAllowFlag (true/false) : '-' 허용 여부
     **/
    chkTelNum : function(str, barAllowFlag) {
        var regExp;
        if (barAllowFlag) {
            regExp = /^[0-9]{2,4}-[0-9]{3,4}-[0-9]{4}$/;
        } else {
            regExp = /^[0-9]{2,4}[0-9]{3,4}[0-9]{4}$/;
        }
        return regExp.test(str);
    },
    arrayFrom : function (_arrayData) {
        return _arrayData.reduce(function (v1, v2) {
            if(v1.indexOf(v2) < 0) {
                v1.push(v2);
            }
            return v1;
        }, []);
    },
    arrayNullCheck : function (_checkList) {
        if(!Array.isArray(_checkList)){
            _checkList = [_checkList];
        }
        for(var i=0;i<_checkList.length;i++){
            if($.jUtil.isEmpty($('#' + _checkList[i]).val())){
                return _checkList[i];
            }
        }
        return null;
    }
};