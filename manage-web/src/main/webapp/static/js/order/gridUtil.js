var GridUtil = {
    initializeGrid : function (_gridViewName, _gridBaseName ) {
        var gridView = new RealGridJS.GridView(_gridViewName);
        var dataProvider = new RealGridJS.LocalDataProvider();
        var init = function () {
            initGridView();
            initDataProvider();
        };
        var initGridView = function() {
            gridView.setDataSource(dataProvider);
            gridView.setStyles(_gridBaseName.realgrid.styles);
            gridView.setDisplayOptions(_gridBaseName.realgrid.displayOptions);
            gridView.setColumns(_gridBaseName.realgrid.columns);
            gridView.setOptions(_gridBaseName.realgrid.options);
        };
        var initDataProvider = function() {
            dataProvider.setFields(_gridBaseName.dataProvider.fields);
            dataProvider.setOptions(_gridBaseName.dataProvider.options);
        };
        var setData = function (data) {
            dataProvider.clearRows();
            dataProvider.setRows(data);
        };
        var excelDownload = function (fileName) {
            if (gridView.getItemCount() === 0) {
                alert("검색 결과가 없습니다.");
                return false;
            }
            gridView.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: " 엑셀 데이터 추줄 중입니다."
            });
        };

        return {
            gridView: gridView,
            dataProvider: dataProvider,
            init: init,
            setData: setData,
            excelDownload: excelDownload
        };
    },
    initGrid : function () {
        if(arguments.length !== 0){
            for(var i =0; i < arguments.length; i++) {
                arguments[i].init();
            }
        }
    },
    setLinkableColumn : function (_grid, _linkColumn) {
        var linkInfo = {
            type : 'link',
            url : '${'.concat(_linkColumn).concat('}'),
            requiredFields : _linkColumn,
            showUrl : true
        }
        _grid.gridView.setColumnProperty(_linkColumn, "renderer", linkInfo);
    }
};