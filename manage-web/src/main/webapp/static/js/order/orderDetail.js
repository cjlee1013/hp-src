$(document).ready(function() {
    orderDetail.init();
    CommonAjaxBlockUI.global();
});

const orderDetail = {
    init : function () {
        this.gridInit();
        this.pageInit();
        this.bindingEvent();
    },
    pageInit : function () {
        $('#storeShipGridArea, #dlvShipGridArea').hide();
        this.orderUserInfo();
        this.orderPriceInfo();
        this.orderPurchaseInfo();
        this.orderShippingInfo('TD');
        this.orderShippingInfo('DS');
        this.orderClaimInfo();
        this.orderSubstitutionInfo();
        this.orderPaymentInfo();
        this.orderHistoryList();
        },
    gridInit : function () {
        GridUtil.initGrid(orderDetailGrid, orderDetailStoreShipGrid, orderDetailDlvShipGrid, orderClaimGrid, orderPaymentGrid, orderHistoryGrid, orderSubstitutionGrid);
        this.customGrid();
        this.event();
    },
    customGrid: function () {
        orderUtil.setRadioCheck(orderDetailGrid, orderDetailStoreShipGrid, orderDetailDlvShipGrid);
        // 주문정보 그리드 컬럼 Merge
        var sameDataMerge = ['storeType', 'partnerId', 'shipPrice', 'islandShipPrice', 'claimShipPrice', 'claimAddShipPrice', 'claimPrice'];
        var valueSameMerge = [ 'bundleNo', 'orderItemNo', 'totAmt', 'totCartDiscountAmt', 'shipDiscountPrice'];
        orderUtil.setRealGridMerge(orderDetailGrid, sameDataMerge, "mergeRule", "bundleNo");
        orderUtil.setRealGridMerge(orderDetailGrid, ['orderPrice', 'discountAmt', 'promoDiscountAmt'], "mergeRule", "orderItemNo");
        orderUtil.setRealGridMerge(orderDetailGrid, valueSameMerge, "mergeRule");
        // 배송정보 그리드 컬럼 Merge
        orderUtil.setRealGridMerge(orderDetailStoreShipGrid, ['bundleNo'], "mergeRule");
        // 클레임정보 그리드 컬럼 Merge
        orderUtil.setRealGridMerge(orderClaimGrid, ['claimNo'], "mergeRule", 'claimNo');
        // 그리드 링크 렌더링
        orderUtil.setRealGridColumnLink(orderDetailGrid, 'itemNo', 'unnecessary', false);
        orderUtil.setRealGridColumnLink(orderDetailDlvShipGrid, ['invoiceNo', 'delayShipDt', 'partnerNm', 'notReceiveType'], 'unnecessary', false);
        orderUtil.setRealGridColumnLink(orderSubstitutionGrid, 'purchaseOrderNo', 'unnecessary', false);
        orderUtil.setRealGridColumnLink(orderClaimGrid, 'claimBundleNo', 'unnecessary', false)
        // 체크바 타이틀 설정.
        orderUtil.setCheckBarHeader([orderDetailGrid, orderDetailStoreShipGrid, orderDetailDlvShipGrid], "선택", null);
    },
    event: function () {
        //주문정보 그리드
        orderDetailGrid.gridView.onItemChecked = function (grid, itemIndex, checked) {
            const bundleNo = orderDetailGrid.gridView.getValue(itemIndex, 'bundleNo');
            const mergeRows = orderUtil.getRealGridMergeData(orderDetailGrid, 'bundleNo', bundleNo);
            const orderDetailInfo = $('#orderDetailInfo');

            orderDetailInfo.val('');
            orderDetail.claimButton('claimButtonArea', 'NN');
            // 체크가 될 경우에 만
            if(checked){
                let shipStatusList = [];
                let paymentStatus;
                const detailInfo = [ bundleNo, orderDetailGrid.gridView.getValue(itemIndex, 'shipType'), orderDetailGrid.gridView.getValue(itemIndex, 'cmbnClaimYn') ];
                for(var i=0;i<mergeRows.length;i++){
                    paymentStatus = orderDetailGrid.gridView.getValue(mergeRows[i], "paymentStatus");
                    if('P3,P5'.indexOf(paymentStatus) > -1 ) {
                        if((orderDetailGrid.gridView.getValue(mergeRows[i], "itemQty") - orderDetailGrid.gridView.getValue(mergeRows[i], "claimQty")) > 0){
                            shipStatusList.push(orderDetailGrid.gridView.getValue(mergeRows[i], "shipStatus"));
                        }
                    }
                    if(orderDetailGrid.gridView.getValue(mergeRows[i], "claimYn") === 'N'){
                        shipStatusList = [];
                        break;
                    }
                }
                const orderType = $('#orderType');
                if(orderType.val() !== 'ORD_MULTI'){
                    if(!$.jUtil.isEmpty(shipStatusList)){
                        if(orderType.val() === 'ORD_TICKET'){
                            if(shipStatusList.indexOf('D5') > -1 || shipStatusList.indexOf('D4') > -1 || shipStatusList.indexOf('D3') > -1){
                                shipStatusList = ['D1'];
                            } else {
                                if(orderDetail.getTicketRole()){
                                    shipStatusList = ['D1'];
                                } else {
                                    shipStatusList = [];
                                }
                            }
                        } else if($.jUtil.isNotEmpty($('#receiveGiftPhone').text()) && shipStatusList.indexOf('D0') > -1){
                            shipStatusList = ['D1'];
                        } else if(orderType === 'ORD_MARKET') {
                            if(shipStatusList.indexOf('D3') > -1 && $('#marketType').val() === 'N마트') {
                                shipStatusList.splice(shipStatusList.indexOf('D3'), 1);
                            } else if(shipStatusList.indexOf('D4') > -1) {
                                shipStatusList.splice(shipStatusList.indexOf('D4'), 1);
                            } else if(shipStatusList.indexOf('D5') > -1) {
                                shipStatusList.splice(shipStatusList.indexOf('D5'), 1);
                            }
                        }
                    }
                } else {
                    while(shipStatusList.indexOf('D3') > -1) {
                        shipStatusList.splice(shipStatusList.indexOf('D3'), 1);
                    }
                }
                orderDetail.claimButton('claimButtonArea', orderUtil.arrayFrom(shipStatusList));
                orderDetailInfo.val(detailInfo);
            }
        };
        // 주문정보 그리드 링크
        orderDetailGrid.gridView.onDataCellDblClicked = function (grid, index) {
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            var siteType = grid.getValue(index.itemIndex, "siteType");
            if (index.fieldName === "itemNo") {
                if(siteType === 'HOME'){
                    window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
                } else {
                    window.open(theClub + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
                }
            }
        };
        orderClaimGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            if (index.fieldName === "claimBundleNo") {
                const claimTabParam = {
                    'purchaseOrderNo' : grid.getValue(index.itemIndex, "purchaseOrderNo"),
                    'claimNo' : grid.getValue(index.itemIndex, "claimNo"),
                    'claimBundleNo' : grid.getValue(index.itemIndex, "claimBundleNo"),
                    'bundleNo' : grid.getValue(index.itemIndex, "bundleNo"),
                    'claimType' : grid.getValue(index.itemIndex, "claimType")
                };

                var agent = navigator.userAgent.toLowerCase();
                if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
                    window.open("/claim/info/getClaimDetailInfoTab?" + jQuery.param(claimTabParam));
                }else{
                    orderUtil.setLinkTab("claimDetailTab", "클레임정보상세","/claim/info/getClaimDetailInfoTab?" + jQuery.param(claimTabParam) );
                }
            }
        };
        orderSubstitutionGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            // grid.checkRow(index.dataRow);
            const subsPurchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            if (index.fieldName === "purchaseOrderNo")
                var agent = navigator.userAgent.toLowerCase();
                if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
                    window.open("/order/orderManageDetail?purchaseOrderNo=" + subsPurchaseOrderNo);
                }else{
                    orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + subsPurchaseOrderNo );
                }
        };
        // 택배배송정보 그리드
        orderDetailDlvShipGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            // grid.checkRow(index.dataRow);
            const shipNo = grid.getValue(index.itemIndex, "shipNo");
            if (index.fieldName === "invoiceNo") {
                const invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");
                const dlvCd = grid.getValue(index.itemIndex, "dlvCd");
                if(invoiceNo !== '' && ('배송조회,-').indexOf(invoiceNo) < 0){
                    windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + '&invoiceNo=' + invoiceNo, "shipHistoryPop", 649, 744, 0,0);
                }
            } else if(index.fieldName === "delayShipDt"){
                const delayShipDt = grid.getValue(index.itemIndex, "delayShipDt");
                if(!$.jUtil.isEmpty(delayShipDt)){
                    windowPopupOpen("/order/popup/orderDelayShipPop?shipNo=" + shipNo, "orderDelayShipInfoPop", 530, 480, "yes", "yes");
                }
            } else if(index.fieldName === "partnerNm"){
                const partnerId = grid.getValue(index.itemIndex, "partnerId");
                const bundleNo = grid.getValue(index.itemIndex, "bundleNo");
                if(!$.jUtil.isEmpty(partnerId)){
                    windowPopupOpen("/order/popup/orderPartnerInfoPop?partnerId=" + partnerId + "&bundleNo=" + bundleNo, "orderPartnerInfoPop", 700, 480, "yes", "yes");
                }
            } else if(index.fieldName === "notReceiveType"){
                const notReceiveType = grid.getValue(index.itemIndex, 'notReceiveType');
                if(!$.jUtil.isEmpty(notReceiveType)){
                    const _url = '/order/popup/noReceiveShipPop?';
                    const buttonInfo = orderUtil.getSplitData($('#dlvShipInfo').val());
                    const noRcvDataParam = {
                        'orderItemNo' : grid.getValue(index.itemIndex, 'orderItemNo'),
                        'bundleNo' : grid.getValue(index.itemIndex, 'bundleNo'),
                        'userNo' : $('#userNo').text().replace(/[^0-9]/g, ''),
                        'purchaseOrderNo' : $('#purchaseOrderNo').text(),
                        'shipNo' : grid.getValue(index.itemIndex, 'shipNo'),
                        'noRcvPopType' : (notReceiveType === '신고') ? 'EDIT' : 'COMPLETE'
                    };
                    orderUtil.getWinPopup(_url.concat(jQuery.param(noRcvDataParam)), 900, 100, buttonInfo.join(''));
                }
            }
        };
        orderDetailStoreShipGrid.gridView.onItemChecked = function (grid, itemIndex, checked) {
            const bundleNo = orderDetailStoreShipGrid.gridView.getValue(itemIndex, 'bundleNo');
            const mergeRows = orderUtil.getRealGridMergeData(orderDetailStoreShipGrid, 'bundleNo', bundleNo);
            const storeShipInfo = $('#storeShipInfo');
            let noRcvCheck = false;
            // 체크가 될 경우에 만
            if(checked){
                let shipInfo = [bundleNo, 'TD', orderDetailStoreShipGrid.gridView.getValue(itemIndex, "userNo"), $('#purchaseOrderNo').text(), grid.getValue(itemIndex, 'shipAreaType')];
                let shipStatusList = [];
                for(var i=0;i<mergeRows.length;i++){
                    let shipStatus = orderDetailStoreShipGrid.gridView.getValue(mergeRows[i], "shipStatus");
                    const storeKind = orderDetailStoreShipGrid.gridView.getValue(mergeRows[i], "storeKind");
                    const storeType = orderDetailStoreShipGrid.gridView.getValue(mergeRows[i], "storeType");
                    // D1, D2 가 아니거나, DLV인 경우 false
                    if("D1,D2".search(shipStatus) < 0 || storeKind !== 'NOR' || storeType === 'EXP'){
                        shipStatus = '';
                    }
                    shipStatusList.push(shipStatus);
                    storeShipInfo.val(shipInfo);
                }
                if(orderDetailDlvShipGrid.dataProvider.getRowCount() > 0){
                    const totalCount = orderDetailDlvShipGrid.dataProvider.getRowCount();
                    for(let idx = 0; idx < totalCount ; idx++){
                        if("D1,D2".indexOf(orderDetailDlvShipGrid.dataProvider.getJsonRow(idx).shipStatus) === -1){
                            shipStatusList = null;
                            break;
                        }
                    }
                }
                if(!$.jUtil.isEmpty(shipStatusList)){
                    let buttonArray = orderDetail.shipButton(orderUtil.arrayFrom(shipStatusList));
                    if($('#orderType').val() === 'ORD_MARKET') {
                        buttonArray.splice(buttonArray.indexOf('배송시간 수정'), 1);
                    }
                    orderUtil.buttonOnOff('storeShipButtonArea', buttonArray);
                }
            } else {
                // 점포배송 번들번호
                storeShipInfo.val('');
                orderUtil.buttonOnOff('storeShipButtonArea', orderDetail.shipButton('storeShipButtonArea', 'NN'));
            }
        };
        orderDetailDlvShipGrid.gridView.onItemChecked = function (grid, itemIndex, checked) {
            const bundleNo = orderDetailDlvShipGrid.gridView.getValue(itemIndex, 'bundleNo');
            const mergeRows = orderUtil.getRealGridMergeData(orderDetailDlvShipGrid, 'bundleNo', bundleNo);
            const dlvShipInfo = $('#dlvShipInfo');
            const isMultiOrder = $('#orderType').val() === 'ORD_MULTI';
            let notReceiveType;
            // 체크가 될 경우에 만
            if(checked){
                const shipInfo = [
                    isMultiOrder ? orderDetailDlvShipGrid.gridView.getValue(itemIndex, 'multiBundleNo') : bundleNo,
                    isMultiOrder ? 'MULTI' : 'DS',
                    orderDetailDlvShipGrid.gridView.getValue(itemIndex, "orderItemNo"),
                    orderDetailDlvShipGrid.gridView.getValue(itemIndex, "userNo"),
                    $('#purchaseOrderNo').text(),
                    orderDetailDlvShipGrid.gridView.getValue(itemIndex, "shipNo"),
                    grid.getValue(itemIndex, 'shipAreaType'),
                    grid.getValue(itemIndex, 'shipMethod').replace("_", "")
                ];
                let shipStatusList = [];

                for(var i=0;i<mergeRows.length;i++){
                    let shipStatus = orderDetailDlvShipGrid.gridView.getValue(mergeRows[i], "shipStatus");
                    const storeKind = orderDetailDlvShipGrid.gridView.getValue(mergeRows[i], "storeKind");
                    notReceiveType = orderDetailDlvShipGrid.gridView.getValue(mergeRows[i], "notReceiveType");
                    if("NN" === shipStatus) {
                        shipStatus = '';
                    }
                    shipStatusList.push(shipStatus);
                }
                dlvShipInfo.val(shipInfo);

                if(!$.jUtil.isEmpty(shipStatusList)){
                    let buttonArray = orderDetail.shipButton(orderUtil.arrayFrom(shipStatusList));
                    if(!$.jUtil.isEmpty(notReceiveType)){
                        if(buttonArray.indexOf('미수취 신고') > -1){
                            buttonArray.splice(buttonArray.indexOf('미수취 신고'), 1);
                        }
                    }
                    if(!$.jUtil.isEmpty(grid.getValue(itemIndex, 'claimStatus'))
                        && 'C1,C2,C8'.indexOf(grid.getValue(itemIndex, 'claimStatus')) > -1){
                        let shipStatus = grid.getValue(itemIndex, 'shipStatus');
                        if(shipStatus === 'D1' && buttonArray.indexOf('주문확인') > -1){
                            buttonArray.splice(buttonArray.indexOf('주문확인'), 1);
                        } else if(shipStatus === 'D2' && buttonArray.indexOf('송장등록') > -1) {
                            buttonArray.splice(buttonArray.indexOf('송장등록'), 1);
                        }
                    }
                    // 주문타입이 선물셋트 일경우
                    if($('#orderType').val() === 'ORD_MULTI'){
                        let shipStatus = grid.getValue(itemIndex, 'shipStatus');
                        if(buttonArray.indexOf('배송정보 수정') > -1) {
                            buttonArray = ['배송정보 수정'];
                        } else {
                            buttonArray = [];
                            if(shipStatus === 'D3') {
                                buttonArray.push('배송상태변경');
                            }
                        }
                    }
                    if(orderDetailStoreShipGrid.dataProvider.getRowCount() > 0 && (buttonArray.indexOf("배송정보 수정") > -1)){
                        buttonArray.splice(buttonArray.indexOf("배송정보 수정"), 1);
                    }
                    orderUtil.buttonOnOff('dlvShipButtonArea', buttonArray);
                }
            } else {
                dlvShipInfo.val('');
                orderUtil.buttonOnOff('dlvShipButtonArea', orderDetail.shipButton('dlvShipButtonArea', 'NN'));
            }
        };
    },
    orderUserInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailProdSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToSpan("orderDetailUserInfo", res);
                    $('#orderType').val(res['orderType']);
                    const orgPurchaseOrderNo = res['orgPurchaseOrderNo'];
                    if(!$.jUtil.isEmpty(orgPurchaseOrderNo) && orgPurchaseOrderNo !== '-'){
                        $('#orgPurchaseOrderNo').html($("<a></a>").html(orgPurchaseOrderNo));
                    }
                    if(res['receiveGiftNm'] !== '-' && $.jUtil.isEmpty(res['giftAcpDt'])){
                        $('#giftSend').removeAttr("disabled");
                    }
                    if(res['nomemOrderYn'] === 'N') {
                        //회원메모조회
                        orderDetail.getUserMemoInfo(res['userNo']);
                    }
                    if($.jUtil.isNotEmpty(res['marketType']) && res['marketType'] !== '-') {
                        $('#orderSaveBtn').prop('disabled', true);
                        $('#cashReceiptBtn').prop('disabled', true);
                        $('#event').prop('disabled', true);
                        $('#storeSlotChangeBtn').prop('disabled', true);
                    }
                }
            });
    },
    orderPriceInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailPriceSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderUtil.writeToSpan("orderDetailPriceInfo", res);
                }
            });
    },
    orderPaymentInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailPaymentSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderPaymentGrid.setData(res);
                    let totalPaymentAmt = 0;
                    for(var i=0;i<res.length;i++){
                        totalPaymentAmt += parseInt(res[i].amt);
                    }
                    $('#totalPaymentAmt').text($.jUtil.comma(totalPaymentAmt));
                }
            });
    },
    orderPurchaseInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailPurchaseSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderDetailGrid.setData(res);
                    let isAllCancelButton = true;
                    let orderItemQty = 0, claimItemQty = 0;
                    let bundleCheckArray = [];
                    const shipType = res[0].shipType;
                    const cmbnClaimYn = $('#cmbnClaimYn');
                    var isOrderMulti = false;
                    for(var i=0;i<res.length;i++){
                        var data = res[i];
                        orderItemQty += parseInt(data.itemQty);
                        claimItemQty += parseInt(data.claimQty);

                        if(i !== (res.length - 1)){
                            if(data.bundleNo !== res[i].bundleNo){
                                if((orderItemQty - claimItemQty) <= 0 || data.claimYn === 'N'){
                                    bundleCheckArray.push(data.bundleNo);
                                }
                                orderItemQty = 0;
                                claimItemQty = 0;
                            }
                        } else {
                            if((orderItemQty - claimItemQty) <= 0){
                                bundleCheckArray.push(data.bundleNo);
                            }
                        }
                        if(((data.paymentStatus !== 'P3' && data.paymentStatus !== 'P5') || ('D0,D1'.indexOf(data.shipStatus) === -1 && shipType !== 'TICKET') ) || claimItemQty !== 0 || ('D1,D2'.indexOf(data.shipStatus) > -1 && shipType === 'TICKET') || data.claimYn !== 'Y'){
                            isAllCancelButton = false;
                        }
                        if($.jUtil.isEmpty(cmbnClaimYn.val()) || cmbnClaimYn.val() !== 'Y'){
                            cmbnClaimYn.val(data.cmbnClaimYn);
                        }
                        if(!isOrderMulti && $.jUtil.isNotEmpty(data.multiBundleNo)) {
                            isOrderMulti = true;
                        }
                    }
                    if(isOrderMulti) {
                        orderUtil.setRealGridColumnVisible(orderDetailGrid, ['multiBundleNo']);
                        orderUtil.setRealGridUnMerge(orderDetailGrid, ['orderItemNo', 'bundleNo'], "mergeRule");
                        orderUtil.setRealGridMerge(orderDetailGrid, ['multiBundleNo'], "mergeRule");
                    }

                    if(isAllCancelButton){
                        orderDetail.claimButton('claimButtonArea', ['P3']);
                    }
                    orderDetailGrid.gridView.setCheckableExpression(
                        orderUtil.setRealGridArraysCheckable(orderDetailGrid, bundleCheckArray, 'bundleNo'), true
                    );
                }
            });
    },
    orderShippingInfo : function (_mallType) {
        const params = {
            'purchaseOrderNo' : purchaseOrderNo,
            'mallType' : _mallType
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailShipSearch.json?'.concat(jQuery.param(params)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    for(var i=0;i<res.length;i++){
                        if($.jUtil.isNotEmpty(res[i].issuePhoneNo)){
                            res[i].issuePhoneNo = orderUtil.phoneFormatter(res[i].issuePhoneNo);
                        }
                    }
                    if(_mallType === 'TD') {
                        orderDetailStoreShipGrid.setData(res);
                        let isButton = [];
                        let itemQty = 0, _idx = 0, totalItemQty = 0;
                        for(var i=0;i<res.length;i++){
                            var data = res[i];
                            itemQty += parseInt(data.itemQty);
                            totalItemQty += itemQty;
                            if(_idx++ !== (res.length - 1)){
                                if(data.bundleNo !== res[_idx].bundleNo){
                                    if(itemQty === 0){
                                        isButton.push(data.bundleNo);
                                    }
                                    itemQty = 0;
                                }
                            } else {
                                if(itemQty <= 0){
                                    isButton.push(data.bundleNo);
                                }
                            }
                        }
                        orderDetailStoreShipGrid.gridView.setCheckableExpression(
                            orderUtil.setRealGridArraysCheckable(orderDetailStoreShipGrid, isButton, 'bundleNo'), true
                        );
                        if(orderDetailStoreShipGrid.dataProvider.getRowCount() > 0){
                            $('#storeShipGridArea').show();
                            $('#storeShipAddrBtn').val('STORE');
                            $('#storeShipItemMemoBtn').removeAttr("disabled");
                            orderDetail.getStoreShipMemo();
                            orderDetail.getStoreShipMsg();
                        }
                        if(totalItemQty <= 0){
                            $('#storeShipButtonArea').hide();
                        }
                    } else {
                        orderDetailDlvShipGrid.setData(res);
                        let isButton = [];
                        let itemQty = 0, _idx = 0, totalItemQty = 0;
                        var isMultiOrder = false;
                        for(var i=0;i<res.length;i++){
                            var data = res[i];
                            itemQty += parseInt(data.itemQty);
                            totalItemQty += itemQty;
                            if(_idx++ !== (res.length - 1)){
                                if(data.bundleNo !== res[_idx].bundleNo){
                                    if(itemQty === 0){
                                        isButton.push(data.bundleNo);
                                    }
                                    itemQty = 0;
                                }
                            } else {
                                if(itemQty <= 0){
                                    isButton.push(data.bundleNo);
                                }
                            }
                            if(!isMultiOrder && $.jUtil.isNotEmpty(data.multiBundleNo)){
                                isMultiOrder = true;
                            }

                        }
                        if(isMultiOrder) {
                            orderUtil.setRealGridColumnVisible(orderDetailDlvShipGrid, ['multiBundleNo']);
                            orderUtil.setRealGridColumnUnVisible(orderDetailDlvShipGrid, ['notReceiveType']);
                            orderUtil.setRealGridMerge(orderDetailDlvShipGrid, ['multiBundleNo'], "mergeRule");
                            orderUtil.setRealGridChangeHeaderText(orderDetailDlvShipGrid, "partnerNm", "점포");
                            orderDetailDlvShipGrid.gridView.setCheckableExpression(
                                orderUtil.setRealGridArraysCheckable(orderDetailDlvShipGrid, isButton, 'multiBundleNo'), true
                            );
                        } else {
                            orderDetailDlvShipGrid.gridView.setCheckableExpression(
                                orderUtil.setRealGridArraysCheckable(orderDetailDlvShipGrid, isButton, 'bundleNo'), true
                            );
                        }

                        if(orderDetailDlvShipGrid.dataProvider.getRowCount() > 0){
                            $('#dlvShipGridArea').show();
                            $('#dlvShipAddrBtn').val('DLV');
                        }
                        if(totalItemQty <= 0){
                            $('#dlvShipButtonArea').hide();
                        }
                    }
                }
            });
    },
    orderClaimInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderDetailClaimSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderClaimGrid.setData(res);
                }
            });
    },
    orderSubstitutionInfo : function () {
        CommonAjax.basic(
            {
                url:'/order/getOrderSubstitutionSearch.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    orderSubstitutionGrid.setData(res);
                }
            });
    },
    getUserMemoInfo : function (_userNo){
        CommonAjax.basic(
            {
                url:'/order/getUserMemo.json?userNo=' + _userNo.replace(/[^0-9]/g, ''),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if($.jUtil.isNotEmpty(res)){
                        $('#userMemo').val(res[0]['content']);
                    }
                }
            });
    },
    getStoreShipMemo : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo,
            'bundleNo' : orderDetailStoreShipGrid.gridView.getValue(0, 'bundleNo'),
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderStoreShipMemoInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        $('#storeShipItemMemo').val(res.data.shipMemo);
                    }

                }
            });
    },
    getStoreShipMsg : function () {
        const param = {
            'purchaseOrderNo' : purchaseOrderNo
        };
        CommonAjax.basic(
            {
                url:'/order/getOrderStoreShipMsgInfo.json?'.concat(jQuery.param(param)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        $('#storeShipMsg').val(res.data.storeShipMsg);
                    }

                }
            });
    },
    orderHistoryList : function () {
        CommonAjax.basic(
            {
                url: '/order/getOrderHistoryList.json?purchaseOrderNo=' + purchaseOrderNo,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    orderHistoryGrid.setData(res);
                }
            });
    },
    bindingEvent : function () {
        $('#claimAllBtn').bindClick(function () { orderDetail.openClaimPopup('allOrderCancel', 1024, 768); });
        $('#claimBtn').bindClick(function () { orderDetail.openClaimPopup('orderCancel', 1024, 768); });
        $('#claimCancelBtn').bindClick(function () { orderDetail.openClaimPopup('claimCancel', 1024, 768); });
        $('#claimReturnBtn').bindClick(function () { orderDetail.openClaimPopup('claimReturn', 1024, 768); });
        $('#claimExchangeBtn').bindClick(function () { orderDetail.openClaimPopup('claimExchange', 1024, 768); });
        $('#promotion').bindClick(function () { orderUtil.getWinPopup("/order/popup/orderDiscountPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo, "type":"promotion"})), 900, 400, 'orderPromoPop'); });
        $('#coupon').bindClick(function () { orderUtil.getWinPopup("/order/popup/orderDiscountPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo, "type":"coupon"})), 900, 400, 'orderCouponDiscountPop'); });
        $('#event').bindClick(function () { orderUtil.getWinPopup("/order/popup/orderDiscountPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo, "type":"gift"})), 900, 400, 'orderGiftPop'); });
        $('#orderSaveBtn').bindClick(function () { orderUtil.getWinPopup("/order/popup/orderDiscountPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo, "type":"save"})), 900, 400, 'orderSavePop'); });
        $('#giftSend').bindClick( function () { orderDetail.requestPresentReSend(); });
        $('#userMemoBtn').bindClick(function () { orderDetail.openReferencePop('USER_MEMO'); });
        $('#cashReceiptBtn').bindClick(function () { orderUtil.getWinPopup("/order/popup/orderCashReceiptPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo})), 900, 400, 'orderCashReceiptPop'); });
        $('#storeShipItemMemoBtn').bindClick(function () {
            const bundleNo = orderDetailStoreShipGrid.gridView.getValue(0, 'bundleNo');
            orderUtil.getWinPopup("/order/popup/storeShipMemoPop?".concat(jQuery.param({"purchaseOrderNo":purchaseOrderNo, "bundleNo":bundleNo})), 500, 400, 'storeShipMemoPop');
        });
        $('#orgPurchaseOrderNo').bindClick(function (e) {
            const orgPurchaseOrderNo = $('#orgPurchaseOrderNo');
            if(orgPurchaseOrderNo.text() !== '-'){
                return orderDetail.openCombineData();
            }
        });
        // 버튼영역 내 버튼 클릭시
        $('#storeShipGridArea, #dlvShipGridArea').children().find($('button')).on('click', function () {
            const buttonTypeInfo = orderUtil.getCamelCaseSplit($(this).attr('id').replace('Btn', ''));
            orderDetail.openShipPopup(buttonTypeInfo);
        });
    },
    openCombineData : function (){
        const combineOrderNo = $('#orgPurchaseOrderNo').text();
        if(combineOrderNo !== '-' && !$.jUtil.isEmpty(combineOrderNo)){
            var agent = navigator.userAgent.toLowerCase();
            if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
                window.open("/order/orderManageDetail?purchaseOrderNo=" + combineOrderNo);
            }else{
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + combineOrderNo);
            }
        }
    },
    claimButton : function (_buttonArea, _status) {
        let buttonStatus = [];
        for(var i=0;i<_status.length;i++){
            switch (_status[i]) {
                case 'D1' : // 결제완료
                    buttonStatus.push('주문취소');
                    break;
                case 'D2' : // 주문확인(상품준비중)
                    buttonStatus.push('취소신청');
                    break;
                case 'D3' : // 배송중
                    buttonStatus.push('반품신청');
                    buttonStatus.push('교환신청');
                    break;
                case 'D4' : // 배송완료
                case 'D5' : // 구매확정
                    buttonStatus.push('반품신청');
                    buttonStatus.push('교환신청');
                    break;
                case 'P3' :
                    buttonStatus.push('전체주문취소');
                    break;
                default :
                    buttonStatus = null;
                    break;
            }
        }
        orderUtil.buttonOnOff(_buttonArea, buttonStatus);
    },
    shipButton : function (_status) {
        let buttonStatus = [];
        for(var i=0;i<_status.length;i++){
            switch (_status[i]) {
                case 'D1' : // 결제완료
                    buttonStatus.push('배송정보 수정');
                    buttonStatus.push('배송시간 수정');
                    buttonStatus.push('주문확인');
                    break;
                case 'D2' : // 주문확인(상품준비중)
                    buttonStatus.push('배송정보 수정');
                    buttonStatus.push('배송시간 수정');
                    buttonStatus.push('송장등록');
                    break;
                case 'D3' : // 배송중
                    buttonStatus.push('송장수정');
                    buttonStatus.push('배송완료');
                    buttonStatus.push('미수취 신고');
                    break;
                case 'D4' : // 배송완료
                    buttonStatus.push('송장수정');
                    buttonStatus.push('미수취 신고');
                    break;
                case 'P3' :
                case 'D5' : // 구매확정
                default :
                    buttonStatus = null;
                    break;
            }
        }
        return buttonStatus;
    },
    openClaimPopup : function (_claimType, _w, _h) {
        const orderDetailInfo = orderUtil.getSplitData($('#orderDetailInfo').val());
        let _url = '/order/claim/popup/cancelOrder?';
        var multiShipYn = orderDetailInfo[1] === 'MULTI' ? 'Y' : 'N';
        if($.jUtil.isEmpty(orderDetailInfo[1])){
            if($('#orderType').val() === 'ORD_MULTI') {
                multiShipYn = 'Y';
            }
        }
        const _param = {
            'purchaseOrderNo' : purchaseOrderNo,
            'bundleNo' : $.jUtil.isEmpty(orderDetailInfo[0]) ? 0 : orderDetailInfo[0],
            'shipType' : orderDetailInfo[1],
            'multiShipYn' :  multiShipYn
        };
        switch (_claimType) {
            case 'allOrderCancel' :
                _param.bundleNo = 0;
                _param.claimPartYn = 'N';
                _param.orderCancelYn = 'Y';
                if($('#cmbnClaimYn').val() === 'N'){
                    alert("합배송이 있는 주문입니다.\n합배송 주문부터 취소한 후 해당 원주문 취소가 가능합니다.");
                    return false;
                }
                break;
            case 'orderCancel' :
                _param.claimPartYn = 'Y';
                _param.orderCancelYn = 'Y';
                break;
            case 'claimCancel' :
                _param.claimPartYn = 'Y';
                _param.orderCancelYn = 'N';
                break;
            case 'claimReturn' :
                _url = '/order/claim/popup/returnOrder?';
                break;
            case 'claimExchange' :
                _url = '/order/claim/popup/exchangeOrder?';
                break;
            default :
                alert('지정되지 않은 액션입니다.');
                return ;
        }
        orderUtil.getWinPopup(_url.concat(jQuery.param(_param)), _w, _h, _claimType);
    },
    // 배송상태에 따른 버튼 컨트롤
    openShipPopup : function (_buttonTypeInfo) {
        const buttonInfo = orderUtil.getSplitData($('#' + _buttonTypeInfo[0].toLowerCase() + 'ShipInfo').val());
        const dataParam = {
            'pageType'      :   _buttonTypeInfo[_buttonTypeInfo.length - 1].toUpperCase(),
            'bundleNo'      :   buttonInfo[0],
            'shipType'      :   buttonInfo[1],
            'shipAreaType'  :   buttonInfo[1] === 'TD' ? buttonInfo[4] : buttonInfo[6],
            'shipMethod'    :   buttonInfo[buttonInfo.length - 1]
        };
        let _url = '';
        let width = 900;
        let height = 300;
        switch (dataParam.pageType) {
            case 'ADDR' :
                _url = '/order/popup/orderShipChangePop?'.concat(jQuery.param(dataParam));
                break;
            case 'CHECK' :
                if(confirm("주문확인을 하시겠습니까?")){
                    CommonAjax.basic({
                        url: '/order/orderCheck.json?'.concat(jQuery.param(dataParam)),
                        data: null,
                        method: 'GET',
                        callbackFunc: function (res) {
                            if (res.returnCode === 'SUCCESS' || res.returnCode === '0000') {
                                alert("주문확인이 완료 되었습니다.");
                                location.reload();
                            } else {
                                alert(res.returnMessage);
                            }
                        }
                    });
                }
                return ;
            case 'REG' :
            case 'EDIT' :
            case 'COMPLETE' :
                _url = '/order/popup/orderShippingPop?'.concat(jQuery.param(dataParam));
                break;
            case 'RECEIVE' :
                if(buttonInfo[1] !== 'DS') return ;
                const noRcvDataParam = {
                    'orderItemNo' : buttonInfo[2],
                    'bundleNo' : buttonInfo[0],
                    'userNo' : buttonInfo[3].replace(/[^0-9]/g, ''),
                    'purchaseOrderNo' : buttonInfo[4],
                    'shipNo' : buttonInfo[5],
                    'noRcvPopType' : 'REG'
                };
                _url = '/order/popup/noReceiveShipPop?'.concat(jQuery.param(noRcvDataParam));
                break;
            case 'CHANGE' :
                dataParam.userNo = buttonInfo[2].replace(/[^0-9]/g, '');
                dataParam.purchaseOrderNo = buttonInfo[3];
                _url = '/order/popup/orderShipChangeSlotPop?'.concat(jQuery.param(dataParam));
                break;
            case 'STATUS' :
                dataParam.purchaseOrderNo = purchaseOrderNo;
                width = 600;
                _url = '/order/popup/orderMultiShipStatusChangePop?'.concat(jQuery.param(dataParam));
                break;
            default :
                return ;
        }
        orderUtil.getWinPopup(_url, width, height, _buttonTypeInfo.join(''));
    },
    openReferencePop : function (_type) {
        const userNo = $('#userNo').text().replace(/[^0-9]/g, '');
        switch (_type) {
            case 'USER_MEMO' :
                windowPopupOpen("/voc/popup/oneOnOneInquiryMemberMemoPop?userNo=" + userNo, "oneOnOneInquiryMemberMemoPop", 800, 680, 0,0);
                break;
            case 'STORE_ITEM' :
                break;
            case 'STORE_ADMIN' :
                break;
        }
    },
    requestPresentReSend : function () {
        if(confirm("선물하기 재전송을 하시겠습니까?")){
            CommonAjax.basic(
                {
                    url:'/order/getPresentReSend.json?purchaseOrderNo=' + purchaseOrderNo,
                    data: null,
                    contentType: 'application/json',
                    method: "GET",
                    successMsg: null,
                    callbackFunc: function(res) {
                        const returnCode = res['returnCode'];
                        let alertMsg;
                        if(returnCode === 'SUCCESS'){
                            alertMsg = $.validator.format('{0} 로 재발송 되었습니다. ({1}회 발송)', [$('#receiveGiftPhone').text(), res['data']]);
                        } else if(returnCode === 'CS4119') {
                            alertMsg = '재발송 가능 횟수를 초과하였습니다.(최대 5회 가능)';
                            document.location.reload();
                        } else {
                            alertMsg = res['returnMessage'];
                        }
                        alert(alertMsg);
                    }
                });
        }
    },
    getTicketRole : function () {
        let isRole = false;
        CommonAjax.basic(
            {
                url:'/order/getOrderTicketCancelRoleCheck.json?',
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                async: false,
                callbackFunc: function(res) {
                    isRole = res;
                }
            });
        return isRole;
    },
};