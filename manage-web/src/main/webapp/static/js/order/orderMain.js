$(document).ready(function() {
    orderMain.init();
    CommonAjaxBlockUI.global();
});

var orderMain = {
    init : function () {
        this.initGrid();
        this.customGrid();
        this.initSearchDate('-1h');
        this.customGridEvent();
        this.bindingEvent();
        this.resetSearchForm();
        this.initPage();
    },
    initGrid : function () {
        orderManageGrid.init();
    },
    initPage : function (){
        orderMain.setStoreId();
    },
    customGrid : function () {
        var bundleSameDataMerge = ['shipPrice', 'islandShipPrice', 'storeType', 'claimPrice', 'discountAmt', 'totAmt'];
        var valueSameMerge = ['bundleNo', 'purchaseOrderNo'];
        orderUtil.setRealGridMerge(orderManageGrid, bundleSameDataMerge, "mergeRule", "bundleNo");
        orderUtil.setRealGridMerge(orderManageGrid, valueSameMerge, "mergeRule");
        // 그리드 링크 렌더링
        orderUtil.setRealGridColumnLink(orderManageGrid, ['orgPurchaseOrderNo', 'itemNo'], 'unnecessary', false);
    },
    customGridEvent : function () {
        orderManageGrid.gridView.onDataCellDblClicked = function (grid, index) {
            switch (index.fieldName) {
                case 'itemNo' :
                    var itemNo = grid.getValue(index.itemIndex, "itemNo");
                    var storeType = grid.getValue(index.itemIndex, "storeType");
                    var siteType = grid.getValue(index.itemIndex, "siteType");
                    if(siteType === 'HOME'){
                        window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
                    } else {
                        window.open(theClub + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
                    }
                    break;
                default :
                    var purchaseOrderNo = orderUtil.getRealGridColumnValue(orderManageGrid, "주문번호", index.dataRow);
                    orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo);
                    break;
            }
        };
        orderManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            switch (index.fieldName) {
                case 'orgPurchaseOrderNo':
                    var orgPurchaseOrderNo = grid.getValue(index.itemIndex, 'orgPurchaseOrderNo');
                    if(!$.jUtil.isEmpty(orgPurchaseOrderNo)){
                        orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + orgPurchaseOrderNo);
                    }
                    break;
            }
        };
    },
    initSearchDate : function (flag) {
        var defaultTime = new Date();
        orderMain.setDate = CalendarTime.datePickerRange('schStartDt', 'schEndDt', {timeFormat:"HH:mm"});
        orderMain.setDate.setEndDate(moment(defaultTime).format('YYYY-MM-DD HH:mm'));
        orderMain.setDate.setStartDate(
            orderUtil.calculationOfDateWithFormat({
                basicDate : $('#schEndDt').val(),
                flag : flag,
                format : 'YYYY-MM-DD HH:mm'
            })
        );
    },
    bindingEvent : function () {
        $('#searchBtn').bindClick(orderMain.search);
        $('#searchResetBtn').on('click', function () {
            orderMain.resetSearchForm();
        });
        $('#excelDownloadBtn').on('click', function () {
            orderMain.excelDownload();
        });
        $('#schStartDt, #schEndDt').on('change', function (e) {
            orderMain.validTime($(this));
        });
        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(orderMain.openStorePopup);
        //[판매업체] 선택
        $('#schStoreType').bindChange(orderMain.chgStoreType);
        //[구매자 회원/비회원] 선택
        $("input[name='schNonUserYn']").bindChange(orderMain.chgUserYn);

        $("input:checkbox").change(function () {
            var checkBoxName = $(this).attr('name');
            if(!checkBoxName.includes('schReserve')){
                orderUtil.checkBoxChecked($(this), 'ALL');
            } else {
                if($(this).is(":checked")){
                    switch ($(this).attr('id')) {
                        case 'schReserveDrctYn':
                            $('#schReserveDlvYn').prop('checked', false);
                            $('#schReserveDrctNorYn').prop('checked', false);
                            break;
                        case 'schReserveDlvYn' :
                            $('#schReserveDrctYn').prop('checked', false);
                            $('#schReserveDrctNorYn').prop('checked', false);
                            break;
                        case 'schReserveDrctNorYn' :
                            $('#schReserveDlvYn').prop('checked', false);
                            $('#schReserveDrctYn').prop('checked', false);
                            break;
                    }
                }
            }
        });
        $('#schDetailType').change(function (e) {
            if("BUYER_TEL_NO,RECEIVER_TEL_NO".indexOf($(this).val()) > -1){
                $('#schDetailContents').attr('maxlength', 13);
            } else {
                $('#schDetailContents').removeAttr('maxlength');
            }
        });

        $('#schDetailContents').keyup(function (e) {
            if("BUYER_TEL_NO,RECEIVER_TEL_NO".indexOf($('#schDetailType').val()) > -1){
                $(this).val($.jUtil.phoneFormatter($(this).val()));
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            $('#schDetailType').val("PURCHASE");
            orderMain.initSearchDate('-1h');
        });
    },
    validTime : function(_validDate) {
        var validDt = moment(_validDate.val(), 'YYYY-MM-DD HH:mm', true);
        if (!validDt.isValid()) {
            _validDate.val(moment(new Date()).format('YYYY-MM-DD HH:mm'));
            return false;
        }
    },
    valid : function(){
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD HH:mm', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD HH:mm', true);
        var isCheckable = true;
        if (schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD HH:mm"));
            return false;
        }

        var schDetailType = $('#schDetailType');
        if (schDetailType.val() !== 'ALL') {
            if('ITEM,BUYER_TEL_NO,RECEIVER_TEL_NO'.indexOf(schDetailType.val()) > -1){
                if('BUYER_TEL_NO,RECEIVER_TEL_NO'.indexOf(schDetailType.val()) > -1){
                    if (schEndDt.diff(schStartDt, "day", true) > 180) {
                        alert("조회기간은 최대 6개월까지 설정 가능합니다.");
                        $("#schStartDt").val(schEndDt.add(-180, "day").format("YYYY-MM-DD HH:mm"));
                        return false;
                    }
                    if(!$.jUtil.phoneValidate($('#schDetailContents').val(), true)){
                        alert('상세조회 형식(전화번호)이 잘못되었습니다.');
                        return false;
                    }
                }
                if("ITEM".indexOf(schDetailType.val()) > -1){
                    if (schEndDt.diff(schStartDt, "day", true) > 7) {
                        alert("상품번호로 검색 시, 조회기간은 최대 7일(24시간)까지 설정 가능합니다.");
                        $("#schStartDt").val(schEndDt.add(-7, "day").format("YYYY-MM-DD HH:mm"));
                        return false;
                    }
                }
                if("RECEIVER_TEL_NO".indexOf(schDetailType.val()) > -1){
                    if (schEndDt.diff(schStartDt, "day", true) > 3) {
                        alert("수령인 전화번호로 검색 시, 조회기간은 최대 3일(24시간)까지 설정 가능합니다.");
                        $("#schStartDt").val(schEndDt.add(-3, "day").format("YYYY-MM-DD HH:mm"));
                        return false;
                    }
                }
                isCheckable = false;
            }

            if($.jUtil.isNotEmpty($('#schUserSearch').val()) && isCheckable){
                isCheckable = false;
                if (schEndDt.diff(schStartDt, "week", true) > 1) {
                    alert("조회기간은 최대 1주일까지 설정 가능합니다.");
                    $("#schStartDt").val(schEndDt.add(-1, "week").format("YYYY-MM-DD HH:mm"));
                    return false;
                }
            }

            if($.jUtil.isNotEmpty($('#schStoreId').val()) && isCheckable){
                isCheckable = false;
                if (schEndDt.diff(schStartDt, "day", true) > 7) {
                    alert("판매자로 검색 시, 조회기간은 최대 7일(24시간)까지 설정 가능합니다.");
                    $("#schStartDt").val(schEndDt.add(-7, "day").format("YYYY-MM-DD HH:mm"));
                    return false;
                }
            }
        }

        var itemTypeCheck = $('input:checkbox[name="schItemType"]').is(':checked');
        if(itemTypeCheck && isCheckable){
            isCheckable = false;
            if (schEndDt.diff(schStartDt, "day", true) > 1) {
                alert("업체배송상품 검색 시, 조회기간은 최대 1일(24시간)까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-1, "day").format("YYYY-MM-DD HH:mm"));
                return false;
            }
        }

        var schDetailContents = $('#schDetailContents');
        if($.jUtil.isEmpty(schDetailContents.val()) && $.jUtil.isEmpty($('#schStoreId').val()) && $.jUtil.isEmpty($('#schUserSearch').val()) && isCheckable){
            if (schEndDt.diff(schStartDt, "hour", true) > 1) {
                alert("검색조건이 입력되지 않아, 조회기간은 최대 1시간까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-1, "hour").format("YYYY-MM-DD HH:mm"));
                return false;
            }
        }

        // if(isCheckable){
        //     if (schEndDt.diff(schStartDt, "day", true) > 93) {
        //         alert("조회기간은 최대 3개월(93일)까지 설정 가능합니다.");
        //         $("#schStartDt").val(schEndDt.add(-3, "month").format("YYYY-MM-DD HH:mm"));
        //         return false;
        //     }
        // }
        return true;
    },
    search : function () {
        if (!orderMain.valid()) return false;
        var searchFormData = $("form[name=searchForm]").serializeObject();
        var parentMethodCdCheck = $('input:checkbox[name=parentMethodCd]:checked');
        var paymentStatusCheck = $('input:checkbox[name=paymentStatus]:checked');
        if(parentMethodCdCheck.length > 0){
            var list = new Array();
            parentMethodCdCheck.each(function (index, item) {
                list.push($(item).val());
            });
            searchFormData.parentMethodCd = list;
        }
        if(paymentStatusCheck.length > 0){
            var list = [];
            paymentStatusCheck.each(function (index, item) {
                list.push($(item).val());
            });
            if(list.indexOf("ALL") > -1){
                searchFormData.paymentStatus = list[0];
            } else {
                searchFormData.paymentStatus = list.join(",");
            }
        }
        CommonAjax.basic(
            {
                url:'/order/getOrderSearch.json',
                data: JSON.stringify(searchFormData),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    orderManageGrid.setData(res);
                    $('#totalCnt').html(orderManageGrid.dataProvider.getRowCount());
                }
            });
    },
    /**
     * 구매자 회원/비회원 선택
     * **/
    chgUserYn : function(schNonUserYn){
        if (typeof schNonUserYn == 'object') {
            schNonUserYn = $(schNonUserYn).val();
        }
        $("#schUserSearch").val('');
        if(schNonUserYn === 'N'){
            $('#schBuyerInfo').hide();//비회원 selectBox 숨기기
            $('#userSchButton').show();//회원조회 버튼 보이기
            $("#schUserSearch").attr("readonly", true);//회원조회 input readonly로 변경
        }else{
            $('#schBuyerInfo').show();//비회원 selectBox 보이기
            $('#userSchButton').hide();//회원조회 버튼 숨기기
            $("#schUserSearch").attr("readonly", false);//회원조회 input readonly로 헤재
        }

    },
    chgStoreType : function(){
        var storeType = $('#schStoreType').val();
        var schStoreId = $('#schStoreId');
        var schStoreNm = $('#schStoreNm');
        schStoreId.val('');
        schStoreNm.val('');
        if(storeType === 'ALL'){
            $('#schStoreBtn').attr('disabled', true);
        }else{
            $('#schStoreBtn').attr('disabled', false);
        }

        if (storeType === 'DS') {
            schStoreId.prop("placeholder","판매업체ID");
            schStoreNm.prop("placeholder","판매업체명");
        } else {
            schStoreId.prop("placeholder","점포ID");
            schStoreNm.prop("placeholder","점포명");
        }
    },
    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal === "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=orderMain.callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=orderMain.callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },
    excelDownload: function() {
        if (orderManageGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "주문관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        orderManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    callbackMemberSearchPop : function(memberData) {
        $("#schUserSearch").val(memberData.userNo);
    },
    setStoreId: function() {
        if (!$.jUtil.isEmpty(storeId)) {
            $("#schStoreType").attr("disabled",true);
            $("#schStoreBtn").attr("disabled",true);
            $("#schStoreType").val(storeType);
            $("#schStoreId").val(parseInt(storeId));
            $("#schStoreNm").val(storeNm);
        }
    },
};


// /**
//  * 회원조회 팝업 callback
//  * @param res Response
//  */
// function callbackMemberSearchPop(res) {
//     $("#memberSchContents").val(res.userNo);
// }

orderMain.callBack = {
    searchPartner : function(res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    },
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    }
};