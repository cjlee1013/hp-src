/**
 * 기본배분율 관리
 */

/**
 * Real Grid 초기화
 */
var pgDivideManageGrid = {
    realGrid: new RealGridJS.GridView("pgDivideManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pgDivideManageGrid.initGrid();
        pgDivideManageGrid.initDataProvider();
        pgDivideManageGrid.event();
    },
    initGrid: function () {
        pgDivideManageGrid.realGrid.setDataSource(pgDivideManageGrid.dataProvider);
        pgDivideManageGrid.realGrid.setStyles(pgDivideManageGridBaseInfo.realgrid.styles);
        pgDivideManageGrid.realGrid.setDisplayOptions(pgDivideManageGridBaseInfo.realgrid.displayOptions);
        pgDivideManageGrid.realGrid.setColumns(pgDivideManageGridBaseInfo.realgrid.columns);
        pgDivideManageGrid.realGrid.setOptions(pgDivideManageGridBaseInfo.realgrid.options);
        pgDivideManageGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        pgDivideManageGrid.dataProvider.setFields(pgDivideManageGridBaseInfo.dataProvider.fields);
        pgDivideManageGrid.dataProvider.setOptions(pgDivideManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        pgDivideManageGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            pgDivideMng.drawPgDivideManageSearchForm(pgDivideManageGrid.dataProvider.getJsonRow(pgDivideManageGrid.realGrid.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        pgDivideManageGrid.dataProvider.clearRows();
        pgDivideManageGrid.dataProvider.setRows(dataList);
    }
};

var pgDivideMng = {
    init : function() {
        pgDivideMng.buttonEvent();
    },
    /** event binding */
    buttonEvent : function() {
        // 변경이력팝업
        $('#historyBtn').on('click', function(){
            // 선택값 validation
            if (!pgDivideMng.popupValid()) {
                return false;
            }
            pgDivideMng.popup();
        });
        // 저장
        $('#saveDivideRateBtn').on('click', function(){
            // 입력값 validation
            if (!pgDivideMng.valid()) {
                return false;
            }
            if(!confirm("저장 하시겠습니까?")) {
                return false;
            }
            pgDivideMng.update();
        });
        // 초기화
        // $('#searchResetBtn').on('click', function(){
        //     pgDivideMng.reset();
        // });
    },
    /** 조회 */
    search : function() {
        var data = $("#pgDivideManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPgDivideRateList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                //pgDivideMng.convertDataFormat(res);
                pgDivideManageGrid.setData(res);
            }
        });
    },
    /** 변경이력팝업 */
    popup : function() {
        var url = "/pg/popup/pgDivideManageHistoryPop?parentMethodCd="+$("#parentMethodCd").val()+"&siteType="+$("#siteType").val();
        $.jUtil.openNewPopup(url, 1084, 400);
    },
    /** 변경이력팝업 조회 전 필수값 체크 */
    popupValid : function() {
        // 결제수단 선택여부
        if ($.jUtil.isEmpty($("#parentMethodCd").val())) {
            alert("이력조회 대상 사이트, 결제수단을 선택해주세요.");
            return false;
        }
        return true;
    },
    /** 저장 */
    update : function() {
        $("#platform").prop("disabled", false);
        $("#parentMethodCd").prop("disabled", false);
        $("#siteType").prop("disabled", false);
        var saveData = _F.getFormDataByJson($("#pgDivideManageSearchForm"));

        $("#platform").prop("disabled", true);
        $("#parentMethodCd").prop("disabled", true);
        $("#siteType").prop("disabled", true);
        CommonAjax.basic({
            url         : '/pg/pgDivide/savePgDivideRate.json',
            data        : saveData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                pgDivideMng.reset();
                pgDivideMng.search();
            }
        });
    },
    /** 저장 전 입력값 validation */
    valid: function() {
        // 비어있는 값 보정, PG사별 입력된 배분율 합산
        pgDivideMng.correctBlankData();
        var inicisRate = Number($('#inicisRate').val());
        var tosspgRate = Number($('#tosspgRate').val());
        var number = inicisRate + tosspgRate;

        // 그리드 선택여부
        if ($.jUtil.isEmpty($("#parentMethodCd").val())) {
            alert("수정할 결제수단을 선택해주세요.");
            return false;
        // PG사 배분율이 입력되지 않은 경우(= 전체 0)
        } else if (number == 0) {
            alert("배분율이 입력되지 않았습니다. 다시 확인해주세요.");
            return false;
        // 100 이상 입력한 경우
        } else if (inicisRate > 100 || tosspgRate > 100) {
            alert("배분율을 잘못 입력하셨습니다. 다시 확인해주세요.");
            return false;
        // PG사 배분율의 합이 100이 아닌 경우
        } else if (number != 100) {
            alert("전체 배분율의 합은 100 미만/초과될 수 없습니다. 다시 확인해주세요.");
            return false;
        // PG사 상점ID가 입력되지 않은 경우
        } else if (inicisRate > 0 && $.jUtil.isEmpty($("#inicisMid").val())) {
            alert("INICIS 상점이 선택되지 않았습니다. 다시 확인해주세요.");
            return false;
        } else if (tosspgRate > 0 && $.jUtil.isEmpty($("#tosspgMid").val())) {
            alert("TOSSPG 상점이 선택되지 않았습니다. 다시 확인해주세요.");
            return false;
        }
        return true;
    },
    /** 초기화 */
    reset : function() {
        $('#divideSeq').val('');
        $('#siteType').val('').prop("disabled", true);
        $('#platform').val('').prop("disabled", true);
        $('#parentMethodCd').val('').prop("disabled", true);

        $('#inicisMid').val('');
        $('#inicisRate').val('0');
        $('#tosspgMid').val('');
        $('#tosspgRate').val('0');
    },
    /** 그리드 선택내용을 플랫폼 & 디바이스 & 결제수단 선택 영역에 그리기 */
    drawPgDivideManageSearchForm: function(data) {
        console.log(data);

        $('#divideSeq').val(data.divideSeq);
        $('#siteType').val(data.siteType);
        $('#platform').val(data.platform);
        $('#parentMethodCd').val(data.parentMethodCd);
        $('#inicisMid').val(data.inicisMid);
        $('#inicisRate').val(data.inicisRate);
        $('#tosspgMid').val(data.tosspgMid);
        $('#tosspgRate').val(data.tosspgRate);
    },
    // /**
    //  * 그리드에 노출할 포맷 맞추기
    //  * ex) Y->사용, N->사용안함
    //  */
    // convertDataFormat: function(res) {
    //     $.each(res, function(idx, data) {
    //          data.useYn = (data.useYn === "Y" ? "사용" : "사용안함");
    //          data.siteType = (data.siteType === "HOME" ? "홈플러스" : "더클럽");
    //     });
    // },
    /**
     * null, '' 데이터에 대한 보정
     * 배분율 기준으로 보정
     * ex) 배분율이 입력되지 않은 경우 0으로 보정
     */
    correctBlankData : function() {
        if($.jUtil.isEmpty($('#inicisRate').val())){
            $('#inicisRate').val(0);
        }
        if($.jUtil.isEmpty($('#tosspgRate').val())){
            $('#tosspgRate').val(0);
        }
    }
};