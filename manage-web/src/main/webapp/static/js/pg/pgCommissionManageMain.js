/** PG 수수료 관리 Main */
var pgCommissionManageMng = {
    /**
     * init 이벤트
     */
    init: function() {
        pgCommissionManageMng.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+30d");
        deliveryCore.initApplyDateCustom("startDt", "endDt", "0", "0");
        $("#startDt").datepicker("option", "minDate", 0);
        $("#startDt, #decpntProcess, #pgKind, #parentMethodCd, #costPeriod, #costDay, input[name='vatYn']").prop("disabled", false);
        pgCommissionManageMng.changeCostPeriod();
        //결제수단 상세 초기화
        pgCommissionManageMng.changeMethodGrid();
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function() {
        // 상단 검색영역 검색
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!pgCommissionManageMng.validSearch()) {
                return false;
            }
            pgCommissionManageMng.search();
        });
        // 상단 검색영역 초기화
        $("#searchResetBtn").bindClick(pgCommissionManageMng.schReset);
        // 기본정보 초기화
        $("#resetBtn").on("click", function () {
            if(!confirm("초기화 하시겠습니까?")) {
                return false;
            }
            pgCommissionManageMng.resetInfo();
        });
        // PG 수수료 저장
        $("#savePgCommissionBtn").bindClick(pgCommissionManageMng.save);
        // 정산주기 변경 시 정산일 input 제어
        $("#costPeriod").bindChange(pgCommissionManageMng.changeCostPeriod);
        // 결제수단 변경 시 결제수단 그리드 제어
        $("#parentMethodCd").bindChange(pgCommissionManageMng.changeMethodGrid);
        // PG사 변경 시 결제수단 그리드 제어 (실시간-토스 제어를 위해 추가)
        $("#pgKind").bindChange(pgCommissionManageMng.changeMethodGrid);
        // 검색어 특수기호(%\) 입력 제한
        $("#schKeyword").notAllowInput("keyup", ['SPC_SCH']);
        // 실수형 숫자 입력값 제한
        $("[id$=Rate]").on("keypress", function () {
            return pgCommissionManageMng.isNumberKey(event);
        });
        // 정수형 숫자 입력값 제한
        $("[id$=Amt], #costDay").on("keypress", function () {
            return pgCommissionManageMng.isIntNumberKey();
        });
        // 숫자 입력란 한글 입력 방지
        $("[id$=Rate], [id$=Amt], #costDay").on("keyup", function () {
            return pgCommissionManageMng.delKor(event);
        });
    },
    /**
     * 실수형 숫자 입력 검증
     */
    isNumberKey: function (evt) {
        // 키보드 입력값
        var charCode = (evt.which) ? evt.which : event.keyCode;
        // input 값
        var _value = event.srcElement.value;

        // 숫자와 소수점(.)만 입력가능
        if (event.keyCode < 48 || event.keyCode > 57) {
            if (event.keyCode !== 46) {
                alert("숫자와 소수점만 입력가능합니다.");
                return false;
            }
        }
        // 소수점(.)이 두번 이상 나오지 못하게 제한
        var _pattern0 = /^\d*[.]\d*$/;
        if (_pattern0.test(_value)) {
            if (charCode == 46) {
                return false;
            }
        }
        // 소수점 앞 두자리 이하의 숫자만 입력가능
        var _pattern1 = /^\d{2}$/; // 현재 value 값이 2자리 숫자이면 . 만 입력가능
        if (_pattern1.test(_value)) {
            if (charCode != 46 && !_value.endsWith('.')) {
                alert("100 이하의 숫자만 입력가능합니다");
                return false;
            }
        }
        // 소수점 둘째자리까지만 입력가능
        var _pattern2 = /^\d*[.]\d{3}$/;
        if (_pattern2.test(_value)) {
            alert("소수점 셋째자리까지만 입력가능합니다.");
            return false;
        }
        return true;
    },
    /**
     * 정수형 숫자 입력 검증
     */
    isIntNumberKey: function () {
        // 숫자만 입력가능
        if (event.keyCode < 48 || event.keyCode > 57) {
            alert("숫자만 입력가능합니다.");
            return false;
        }
        return true;
    },
    /**
     * 숫자 입력란에 한글 입력 방지
     */
    delKor : function (evt) {
        var objTarget = evt.srcElement || evt.target;
        var _value = event.srcElement.value;
        if(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g.test(_value)) {
            objTarget.value = objTarget.value.replace(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g,'');
            objTarget.value = null;
            return false;
        }
    },
    /**
     * 상단검색영역초기화
     */
    schReset: function() {
        $("#pgCommissionSearchForm select").find('option:first').prop('selected', true);
        $("#pgCommissionSearchForm input[type='text']").val("");
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+30d");
    },
    /**
     * 기본정보 초기화
     */
    resetInfo: function() {
        $("#pgCommissionInputForm select").find('option:first').prop('selected', true);
        $("#pgCommissionInputForm input[type='text']").val("");
        deliveryCore.initApplyDateCustom("startDt", "endDt", "0", "0");
        $("#startDt").datepicker("option", "minDate", 0);
        $("#pgCommissionTargetForm input[type='text']").val("0");
        $("#pgCommissionTargetForm input[type='hidden']").val("");
        $("#startDt, #decpntProcess, #pgKind, #parentMethodCd, #costPeriod, #costDay, input[name='vatYn']").prop("disabled", false);
        pgCommissionManageMng.changeCostPeriod();
        pgCommissionManageMng.changeMethodGrid();
    },
    /**
     * 정산일 input 제어
     */
    changeCostPeriod: function() {
        if ($("#costPeriod").val() === "DAY") {
            $("#costDay").prop("disabled", false);
        } else {
            $("#costDay").prop("disabled", true);
        }
    },
    /**
     * 결제수단 그리드 제어
     */
    changeMethodGrid: function() {
        var selectedMethodCd = $("#parentMethodCd").val();
        $("#pgCommissionTargetForm").find('[id$=Div]').hide();
        $("#"+selectedMethodCd+"Div").show();
        // PG사에 따른 실시간계좌이체 그리드 변경
        if (selectedMethodCd === "RBANK") {
            var selectedPgKind = $("#pgKind").val();
            if (selectedPgKind === "TOSSPG") {
                $("#rbankCreditRateTitle").html("10만원이하");
                $("#rbankOverFstAmtRow").show();
                $("#rbankOverNextAmtRow").show();
            } else {
                $("#rbankCreditRateTitle").html("일반수수료");
                $("#rbankOverFstAmtRow").hide();
                $("#rbankOverNextAmtRow").hide();
            }
        }
    },
    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawDetail: function (data) {
        $("#pgCommissionMngSeq").val(data.pgCommissionMngSeq);
        $('input:radio[name=useYn]:input[value="'+ data.useYn +'"]').prop("checked", true);
        $('input:radio[name=vatYn]:input[value="'+ data.vatYn +'"]').prop("checked", true);
        $("#commissionMngNm").val(data.commissionMngNm);
        $("#pgKind").val(data.pgKind);
        $("#parentMethodCd").val(data.parentMethodCd);
        $("#startDt").datepicker("destroy");
        $("#startDt").val(data.startDt);
        $("#endDt").val(data.endDt);
        $("#costPeriod").val(data.costPeriod);
        $("#costDay").val(data.costDay);
        $("#decpntProcess").val(data.decpntProcess);
        // 수정불가 항목 disable 처리
        $("#startDt, #decpntProcess, #pgKind, #parentMethodCd, #costPeriod, #costDay, input[name='vatYn']").prop("disabled", true);
        pgCommissionManageMng.changeMethodGrid();
        pgCommissionManageMng.searchTarget(data.pgCommissionMngSeq, data.parentMethodCd);
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 1년 초과 검색제한
        if (!deliveryCore.dateValid($("#schFromDt"), $("#schEndDt"), ["isValid", "overOneYearRange"])) {
            return false;
        }

        var schKeyword = $("#schKeyword").val();
        // 검색어 자릿수 확인
        if (schKeyword !== "" && schKeyword.length < 2) {
            alert("2글자 이상 검색하세요.");
            return false;
        }
        return true;
    },
    /**
     * PG 수수료 마스터 데이터 조회
     */
    search: function() {
        var data = $("#pgCommissionSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getPgCommissionMngList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(dataList) {
                console.log(dataList);
                pgCommissionManageGrid.setData(dataList);
                $('#pgCommissionSearchCnt').html($.jUtil.comma(pgCommissionManageGrid.gridView.getItemCount()));
                //기본정보 초기화
                pgCommissionManageMng.resetInfo();
            }
        });
    },
    /**
     * 데이터 저장 전 Form 데이터 유효성 체크
     */
    valid: function() {
        var commissionMngNm = $("#commissionMngNm").val();
        // 제목 입력여부 체크
        if ($.jUtil.isEmpty(commissionMngNm)) {
            alert("제목이 입력되지 않았습니다. 다시 확인해주세요.");
            return false;
        // 적용기간 입력여부 체크
        } else if ($.jUtil.isEmpty($("#startDt").val()) || $.jUtil.isEmpty($("#endDt").val())) {
            alert("적용기간은 필수값입니다. 다시 확인해주세요.");
            return false;
        // PG 입력여부 체크
        } else if ($.jUtil.isEmpty($("#pgKind").val())) {
            alert("PG는 필수값입니다. 다시 확인해주세요.");
            return false;
        // 결제수단 입력여부 체크
        } else if ($.jUtil.isEmpty($("#parentMethodCd").val())) {
            alert("결제수단은 필수값입니다. 다시 확인해주세요.");
            return false;
        // 정산주기 일정산이고, 정산일이 없는경우 체크
        } else if ($("#costPeriod").val() === "DAY" && $.jUtil.isEmpty($("#costDay").val())) {
            alert("정산주기가 일정산일 경우 정산일은 필수값입니다. 다시 확인해주세요.");
            return false;
        }
        // 제목 글자수 체크
        if (commissionMngNm.length > 20) {
            alert("제목은 20글자를 넘을 수 없습니다.");
            return false;
        }
        return true;
    },
    /**
     * PG 수수료 저장
     */
    save: function() {
        // Form 데이터 유효성 체크
        if (!pgCommissionManageMng.valid()) {
            return false;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }
        // 중복체크
        pgCommissionManageMng.checkDuplicateCommission();
    },
    /**
     * 저장 전 중복 체크
     */
    checkDuplicateCommission: function () {
        CommonAjax.basic({
            url: '/pg/paymentMethod/checkDuplicateCommission.json?pgKind='+$("#pgKind").val()+"&parentMethodCd="+$("#parentMethodCd").val()
                +"&startDt="+$("#startDt").val()+"&endDt="+$("#endDt").val()+"&pgCommissionMngSeq="+$("#pgCommissionMngSeq").val(),
            data: null,
            method: "GET",
            callbackFunc: function (res) {
                console.log(res);
                if (res.data > 0) {
                    alert("동일 기간에 등록된 수수료 정책이 존재합니다. 기존 수수료 정책을 확인하세요.");
                    return false;
                } else {
                    // 중복건 없을 시 저장 데이터 세팅 후 저장 실행
                    pgCommissionManageMng.setSaveData();
                }
            }
        });
    },
    /**
     * 저장 데이터 세팅
     */
    setSaveData: function() {
        var targetList = new Array();
        // 수정불가 항목 저장전 상태변경
        $("#startDt, #decpntProcess, #pgKind, #parentMethodCd, #costPeriod, #costDay, input[name='vatYn']").prop("disabled", false);
        var mngData = JSON.parse(_F.getFormDataByJson($("#pgCommissionInputForm")));
        mngData.pgCommissionMngSeq = $("#pgCommissionMngSeq").val();
        var targetData = $('#pgCommissionTargetForm').serializeObject(true);
        // 저장을 위한 데이터 세팅 및 저장 실행
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getDistinctMethodCd.json?parentMethodCd='+$("#parentMethodCd").val(),
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                var methodList = res;
                if (!$.jUtil.isEmpty(methodList)) {
                    for (var method of methodList) {
                        var saveTgt = new Object();
                        var target = targetData[method.methodCd+"List"];
                        if (!$.jUtil.isEmpty(target)) {
                            saveTgt.pgCommissionMngSeq = $("#pgCommissionMngSeq").val();
                            saveTgt.methodCd = method.methodCd;
                            // saveTgt.commissionAmt = "0"; // commissionAmt 추후 필요시 세팅예정.
                            saveTgt.pgCommissionTargetSeq = target[0].pgCommissionTargetSeq;
                            saveTgt.debitCommissionRate = target[0].debitCommissionRate;
                            if($("#parentMethodCd").val() === "PHONE") {
                                saveTgt.creditCommissionRate = $("#PHONECommonRate").val();
                                saveTgt.mobileRefundCommissionAmt = $("#PHONECommonAmt").val();
                            } else {
                                saveTgt.creditCommissionRate = target[0].creditCommissionRate;
                            }
                            if($("#parentMethodCd").val() === "RBANK") {
                                saveTgt.rbankLowCommissionAmt = target[0].rbankLowCommissionAmt;
                                saveTgt.tossRbankFstCommissionAmt = target[0].tossRbankFstCommissionAmt;
                                saveTgt.tossRbankNextCommissionAmt = target[0].tossRbankNextCommissionAmt;
                            }
                            targetList.push(saveTgt);
                        }
                    }
                    if (!$.jUtil.isEmpty(targetList)) {
                        mngData.pgCommissionTargetDtoList = targetList;
                    }
                }
                // 저장 실행
                pgCommissionManageMng.executeSave(mngData);
            }
        });
    },
    /**
     * 데이터 저장 실행
     */
    executeSave: function(saveData) {
        CommonAjax.basic({
            url         : '/pg/paymentMethod/savePgCommission.json',
            data        : JSON.stringify(saveData),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                pgCommissionManageMng.search();
            }
        });
    },
    /**
     * PG 수수료 대상 조회
     */
    searchTarget: function (searchSeq, parentMethodCd) {
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getPgCommissionTargetList.json?pgCommissionMngSeq='+searchSeq,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                var targetList = res;

                if ($.jUtil.isEmpty(targetList)) {
                    $("#pgCommissionTargetForm input[type='text']").val("0");
                    $("#pgCommissionTargetForm input[type='hidden']").val("");
                } else {
                    if (parentMethodCd === "PHONE") {
                        $("#PHONECommonRate").val(targetList[0].creditCommissionRate);
                        $("#PHONECommonAmt").val(targetList[0].mobileRefundCommissionAmt);
                    }
                    for (var i=0; i<targetList.length; i++) {
                        var methodCd = targetList[i].methodCd;
                        var creditCommissionRate = targetList[i].creditCommissionRate;
                        var debitCommissionRate = targetList[i].debitCommissionRate;
                        var mobileRefundCommissionAmt = targetList[i].mobileRefundCommissionAmt;
                        var rbankLowCommissionAmt = targetList[i].rbankLowCommissionAmt;
                        var tossRbankFstCommissionAmt = targetList[i].tossRbankFstCommissionAmt;
                        var tossRbankNextCommissionAmt = targetList[i].tossRbankNextCommissionAmt;
                        var pgCommissionTargetSeq = targetList[i].pgCommissionTargetSeq;

                        $("#"+methodCd+"CreditRate").val(creditCommissionRate); // 카드결제:신용, 간편결제:카드, 휴대폰/실시간:수수료율값
                        $("#"+methodCd+"DebitRate").val(debitCommissionRate); // 카드결제:체크, 간편결제:머니, 휴대폰/실시간:수수료율값
                        $("#"+methodCd+"RefundAmt").val(mobileRefundCommissionAmt); // 휴대폰결제
                        $("#"+methodCd+"LowCommissionAmt").val(rbankLowCommissionAmt); // 실시간계좌이체
                        $("#"+methodCd+"OverFstAmt").val(tossRbankFstCommissionAmt); // 실시간계좌이체(토스만)
                        $("#"+methodCd+"OverNextAmt").val(tossRbankNextCommissionAmt); // 실시간계좌이체(토스만)
                        $("#"+methodCd+"PgCommissionTargetSeq").val(pgCommissionTargetSeq); // 수수료 대상 seq
                    }
                }
            }
        });
    }

};

/** Grid Script */
var pgCommissionManageGrid = {
    gridView: new RealGridJS.GridView("pgCommissionManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pgCommissionManageGrid.initGrid();
        pgCommissionManageGrid.initDataProvider();
        pgCommissionManageGrid.event();
    },
    initGrid: function () {
        pgCommissionManageGrid.gridView.setDataSource(pgCommissionManageGrid.dataProvider);
        pgCommissionManageGrid.gridView.setStyles(pgCommissionManageGridBaseInfo.realgrid.styles);
        pgCommissionManageGrid.gridView.setDisplayOptions(pgCommissionManageGridBaseInfo.realgrid.displayOptions);
        pgCommissionManageGrid.gridView.setColumns(pgCommissionManageGridBaseInfo.realgrid.columns);
        pgCommissionManageGrid.gridView.setOptions(pgCommissionManageGridBaseInfo.realgrid.options);
        setColumnLookupOption(pgCommissionManageGrid.gridView, "pgKind", $("#schPgKind"));
        setColumnLookupOption(pgCommissionManageGrid.gridView, "parentMethodCd", $("#schParentMethodCd"));
        setColumnLookupOption(pgCommissionManageGrid.gridView, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        pgCommissionManageGrid.dataProvider.setFields(pgCommissionManageGridBaseInfo.dataProvider.fields);
        pgCommissionManageGrid.dataProvider.setOptions(pgCommissionManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        pgCommissionManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = pgCommissionManageGrid.dataProvider.getJsonRow(index.dataRow);
            pgCommissionManageMng.drawDetail(data);
        };
    },
    setData: function (dataList) {
        pgCommissionManageGrid.dataProvider.clearRows();
        pgCommissionManageGrid.dataProvider.setRows(dataList);
    }
};