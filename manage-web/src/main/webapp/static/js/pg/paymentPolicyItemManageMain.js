/**
 * 결제정책 상품 관리
 */

// 결제정책 상품 관리 메인 그리드
var paymentPolicyItemGrid = {
    realGrid: new RealGridJS.GridView("paymentPolicyItemGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentPolicyItemGrid.initGrid();
        paymentPolicyItemGrid.initDataProvider();
        paymentPolicyItemGrid.event();
    },
    initGrid: function () {
        paymentPolicyItemGrid.realGrid.setDataSource(paymentPolicyItemGrid.dataProvider);
        paymentPolicyItemGrid.realGrid.setStyles(paymentPolicyItemGridBaseInfo.realgrid.styles);
        paymentPolicyItemGrid.realGrid.setDisplayOptions(paymentPolicyItemGridBaseInfo.realgrid.displayOptions);
        paymentPolicyItemGrid.realGrid.setColumns(paymentPolicyItemGridBaseInfo.realgrid.columns);
        paymentPolicyItemGrid.realGrid.setOptions(paymentPolicyItemGridBaseInfo.realgrid.options);
        paymentPolicyItemGrid.realGrid.setEditOptions();
        setColumnLookupOption(paymentPolicyItemGrid.realGrid, "siteType", $("#schSiteType"));
        setColumnLookupOption(paymentPolicyItemGrid.realGrid, "applyTarget", $("#schApplyTarget"));
        setColumnLookupOption(paymentPolicyItemGrid.realGrid, "pgStoreSet", $("#schPgStoreSet"));
        setColumnLookupOption(paymentPolicyItemGrid.realGrid, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        paymentPolicyItemGrid.dataProvider.setFields(paymentPolicyItemGridBaseInfo.dataProvider.fields);
        paymentPolicyItemGrid.dataProvider.setOptions(paymentPolicyItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        paymentPolicyItemGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            paymentPolicyItemMng.drawPaymentPolicyDetail(paymentPolicyItemGrid.dataProvider.getJsonRow(paymentPolicyItemGrid.realGrid.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        paymentPolicyItemGrid.dataProvider.clearRows();
        paymentPolicyItemGrid.dataProvider.setRows(dataList);
    }
};

var paymentPolicyItemMng = {
    init: function () {
        paymentPolicyItemMng.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+7d");
        paymentPolicyItemMng.manageApplyGrid();
        paymentPolicyItemMng.initCategorySelectBox();
    },
    /** event binding */
    bindEvent: function () {
        // 결제정책 조회
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!paymentPolicyItemMng.validSearch()) {
                return false;
            }
            paymentPolicyItemMng.search();
        });
        // 결제정책 조회 form 초기화
        $("#searchResetBtn").on("click", function () {
            paymentPolicyItemMng.resetSearchForm();
        });
        // 결제정책 상세 form 초기화
        $("#resetPolicyItemBtn").on("click", function () {
            paymentPolicyItemMng.resetDetailForm();
        });
        // 히스토리 조회 팝업
        $("#policyItemHistoryBtn").bindClick(paymentPolicyItemMng.historyPop);
        // 저장
        $("#savePolicyItemBtn").on("click", function () {
            // 입력값 validation
            if (!paymentPolicyItemMng.valid()) {
                return false;
            }
            if (!confirm("저장 하시겠습니까?")) {
                return false;
            }
            paymentPolicyItemMng.save();
        });
    },
    /** 히스토리 조회 팝업 */
    historyPop: function () {
        var policyNo = $("#policyNo").html();
        if ($.jUtil.isEmpty(policyNo)) {
            alert("히스토리 조회할 정책을 선택하세요.");
            return false;
        }
        var applyTarget;
        if ($.jUtil.isEmpty($("#applyTarget").html())) {
            applyTarget = "카테고리";
        } else if ($("#applyTarget").html() === "상품") {
            applyTarget = "상품번호";
        } else {
            applyTarget = $("#applyTarget").html();
        }
        $.jUtil.openNewPopup('/pg/popup/paymentPolicyHistoryPop?policyNo='+policyNo+'&applyTarget='+applyTarget, 1084, 450);
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 1년 초과 검색제한
        if (!deliveryCore.dateValid($("#schFromDt"), $("#schEndDt"), ["isValid", "overOneYearRange"])) {
            return false;
        }

        var schType = $("#schType").val();
        var schKeyword = $("#schKeyword").val();
        // 검색어 자릿수 확인
        if (schType == "policyNm") {
            if (schKeyword != "" && schKeyword.length < 2) {
                alert("관리명은 최소 2자 이상 입력하세요.");
                return false;
            }
        }
        // 검색어 입력제한
        if (schType == "itemNo" || schType == "policyNo") {
            if (schKeyword != "" && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
                alert("상품번호/관리번호는 숫자만 입력해주세요.");
                return false;
            }
        }
        return true;
    },
    /** 결제정책 마스터 조회 */
    search: function () {
        var data = $("#paymentPolicyItemManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPaymentPolicyMngList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                paymentPolicyItemGrid.setData(res);
                $("#mngSearchCnt").html($.jUtil.comma(paymentPolicyItemGrid.realGrid.getItemCount()));
                paymentPolicyItemMng.resetDetailForm();
            }
        });
    },
    /** 결제정책 조회 form 초기화 */
    resetSearchForm: function () {
        $("#paymentPolicyItemManageSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+7d");
    },
    /** 결제정책 상세 form 초기화 */
    resetDetailForm: function () {
        $("#policyNo").html("");
        $("#siteType").html("");
        $("#policyNm").html("");
        $("#applyTarget").html("");
        $("#priority").html("");
        $("#applyStartDt").html("");
        $("#applyEndDt").html("");

        $("#pointUseYn").html("");
        $("#cashReceiptUseYn").html("");
        $("#cardInstallmentUseYn").html("");
        $("#freeInterestUseYn").html("");

        $("#applyTargetTotalCount").html("");
        applyItemGrid.dataProvider.clearRows();
        applyCategoryGrid.dataProvider.clearRows();
        applyAffiliateGrid.dataProvider.clearRows();
        paymentPolicyItemMng.manageApplyGrid();

        exposMethodGrid.treeDataProviders.clearRows();
        $("#pgStoreSet").html("");
        $("[id$='Mid']").html("");
        $("[id$='Rate']").html("");
    },
    /** 카테고리 셀렉트박스 초기화 */
    initCategorySelectBox : function() {
        commonCategory.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#searchCateCd4'));
    },
    /** 그리드 선택내용을 기본정보 항목에 세팅 */
    drawPaymentPolicyDetail: function (data) {
        console.log(data);

        $("#policyNo").html(data.policyNo);
        $("#siteType").html($("#schSiteType option[value='" + data.siteType + "']").text());
        $("#policyNm").html(data.policyNm);
        $("#applyTarget").html($("#schApplyTarget option[value='" + data.applyTarget + "']").text());
        $("#priority").html(data.priority);
        $("#applyStartDt").html(data.applyStartDt);
        $("#applyEndDt").html(data.applyEndDt);

        $("#pointUseYn").html($("#schPointUseYn option[value='" + data.pointUseYn + "']").text());
        $("#cashReceiptUseYn").html($("#schCashReceiptUseYn option[value='" + data.cashReceiptUseYn + "']").text());
        $("#cardInstallmentUseYn").html($("#schCardInstallmentUseYn option[value='" + data.cardInstallmentUseYn + "']").text());
        $("#freeInterestUseYn").html($("#schFreeInterestUseYn option[value='" + data.freeInterestUseYn + "']").text());

        $("#pgStoreSet").html($("#schPgStoreSet option[value='" + data.pgStoreSet + "']").text());
        paymentPolicyItemMng.manageApplyGrid();

        exposMethodMng.search(data.siteType, data.policyNo);
        paymentPolicyItemMng.searchPgDivide(data.policyNo);
        paymentPolicyItemMng.searchPolicyApply(data.policyNo);
    },
    /** PG배분율 조회 */
    searchPgDivide: function (policyNo) {
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPaymentPolicyPgDivideRateList.json?policyNo='+policyNo,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);

                var pgList = res;
                if (!$.jUtil.isEmpty(pgList)) {
                    for(var i=0; i<pgList.length; i++) {
                        var parentMethodCd = pgList[i].parentMethodCd;
                        var inicisMid = pgList[i].inicisMid;
                        var inicisRate = pgList[i].inicisRate;
                        var tosspgMid = pgList[i].tosspgMid;
                        var tosspgRate = pgList[i].tosspgRate;

                        $("#"+parentMethodCd+"inicisMid").html(inicisMid);
                        $("#"+parentMethodCd+"inicisRate").html(inicisRate+"%");
                        $("#"+parentMethodCd+"tosspgMid").html(tosspgMid);
                        $("#"+parentMethodCd+"tosspgRate").html(tosspgRate+"%");
                    }
                } else {
                    $("[id$='Mid']").html("");
                    $("[id$='Rate']").html("");
                }
            }
        });
    },
    /** 결제정책 적용대상 조회 */
    searchPolicyApply: function (policyNo) {
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPaymentPolicyApplyList.json?policyNo='+policyNo,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);

                if ($("#applyTarget").html() == "상품") {
                    applyItemGrid.setData(res);
                    $("#applyTargetTotalCount").html($.jUtil.comma(applyItemGrid.realGrid.getItemCount()));
                    applyCategoryGrid.dataProvider.clearRows();
                    applyAffiliateGrid.dataProvider.clearRows();
                } else if ($("#applyTarget").html() == "카테고리") {
                    applyCategoryGrid.setData(res);
                    $("#applyTargetTotalCount").html($.jUtil.comma(applyCategoryGrid.realGrid.getItemCount()));
                    applyItemGrid.dataProvider.clearRows();
                    applyAffiliateGrid.dataProvider.clearRows();
                } else if ($("#applyTarget").html() == "제휴채널") {
                    applyAffiliateGrid.setData(res);
                    $("#applyTargetTotalCount").html($.jUtil.comma(applyAffiliateGrid.realGrid.getItemCount()));
                    applyItemGrid.dataProvider.clearRows();
                    applyCategoryGrid.dataProvider.clearRows();
                } else {
                    applyItemGrid.dataProvider.clearRows();
                    applyCategoryGrid.dataProvider.clearRows();
                    applyAffiliateGrid.dataProvider.clearRows();
                    $("#applyTargetTotalCount").html("0");
                }
            }
        });
    },
    /** 적용대상 일괄등록 - 엑셀 업로드 팝업 호출 */
    uploadExcel: function () {
        var applyTarget = $("#applyTarget").html();
        if (applyTarget == "상품") {
            $.jUtil.openNewPopup('/pg/popup/paymentPolicyApplyUploadPop?callback=paymentPolicyItemMng.uploadExcelCallback', 500, 360);
        } else {
            alert("적용대상이 상품인 결제정책을 선택하세요.");
            return false;
        }
    },
    /** 엑셀 업로드 결과 반영 - 상품 validation 적용 후 주석해제 및 적용 예정 */
    uploadExcelCallback: function (resDataArr) {
        var resultCount = resDataArr.length;
        var firstResult = resDataArr[0];
        // 엑셀 read 결과가 없을 경우
        if (resultCount == 0) {
            alert("등록할 상품이 없습니다. 엑셀 파일을 다시 확인하시기 바랍니다.");
            return;
        }
        // error 건이 있을 경우 건수 alert
        if (firstResult.errorCount != 0) {
            alert(resDataArr[0].errorCount + "건이 등록 실패했습니다.");
        }
        // 1000건 이상 등록시도 또는 상품전체 validation 실패한 경우
        if (firstResult.itemNo == "error" && !$.jUtil.isEmpty(firstResult.itemNm)) {
            alert(firstResult.itemNm);
            return;
        }

        var insertIndex = applyItemGrid.realGrid.getItemCount();
        for (var i=0; i<resultCount; i++) {
            var applyCd = resDataArr[i].itemNo;
            var applyTargetNm = resDataArr[i].itemNm;
            var policyNo = $("#policyNo").html();
            var applyPriority = 2;
            var data = [applyCd, applyTargetNm, policyNo, applyPriority];

            var checkDuplicate = applyItemGrid.realGrid.searchItem({fields: ["applyCd"], values: [applyCd], wrap: true});
            if (checkDuplicate < 0) {
                applyItemGrid.dataProvider.insertRow(insertIndex, data);
                insertIndex++;
            }
        }
    },
    /** 적용대상 등록 - 상품/카테고리 등록 팝업 호출 */
    addApplyTarget: function () {
        var applyTarget = $("#applyTarget").html();
        if (applyTarget === "상품") {
            $.jUtil.openNewPopup('/common/popup/itemPop?callback=paymentPolicyItemMng.addApplyTargetCallback&isMulti=Y' , 1084, 650);
        } else if (applyTarget === "카테고리") {
            $.jUtil.openNewPopup('/common/popup/treeCategoryPop?callback=paymentPolicyItemMng.addApplyTargetCallback&transDataChildNode=true' , 580, 650);
        } else if (applyTarget === "제휴채널") {
            $.jUtil.openNewPopup('/common/popup/affiliateChannelPop?callback=paymentPolicyItemMng.addApplyTargetCallback&isMulti=Y', 1084, 564);
        } else {
            alert("먼저 결제정책을 선택하세요.");
            return false;
        }
    },
    /** 팝업 조회결과 반영 */
    addApplyTargetCallback: function (resDataArr) {
        var applyTarget = $("#applyTarget").html();
        var resultCount = resDataArr.length;

        if (applyTarget === "상품") {
            if (resultCount > 0) {
                var itemCount = applyItemGrid.realGrid.getItemCount();
                for (var i=0; i<resultCount; i++) {
                    var applyCd = resDataArr[i].itemNo;
                    var applyTargetNm = resDataArr[i].itemNm;
                    var policyNo = $("#policyNo").html();
                    var applyPriority = 2;
                    var data = [applyCd, applyTargetNm, policyNo, applyPriority];

                    var checkDuplicate = applyItemGrid.realGrid.searchItem({fields: ["applyCd"], values: [applyCd], wrap: true});
                    if (checkDuplicate < 0) {
                        applyItemGrid.dataProvider.insertRow(itemCount, data);
                        itemCount++;
                    }
                }
            }
        } else if (applyTarget === "카테고리") { // applyPriority = 가장 하위 카테고리 ID 첫자리 + 2
            if (resultCount > 0) {
                var cateCount = applyCategoryGrid.realGrid.getItemCount();
                for (var i=0; i<resultCount; i++) {
                    var applyCd;
                    var applyTargetNm;
                    var policyNo = $("#policyNo").html();
                    var applyPriority;

                    // 세분류
                    if (!$.jUtil.isEmpty(resDataArr[i].categoryId)) {
                        applyCd = resDataArr[i].categoryId;
                        applyTargetNm = resDataArr[i].categoryNm;
                        applyPriority = Number(resDataArr[i].categoryId.substr(0, 1)) + 2;
                    // 소분류
                    } else if (!$.jUtil.isEmpty(resDataArr[i].categorySId)) {
                        applyCd = resDataArr[i].categorySId;
                        applyTargetNm = resDataArr[i].categorySNm;
                        applyPriority = Number(resDataArr[i].categorySId.substr(0, 1)) + 2;
                    // 중분류
                    } else if (!$.jUtil.isEmpty(resDataArr[i].categoryMId)) {
                        applyCd = resDataArr[i].categoryMId;
                        applyTargetNm = resDataArr[i].categoryMNm;
                        applyPriority = Number(resDataArr[i].categoryMId.substr(0, 1)) + 2;
                    // 대분류
                    } else if (!$.jUtil.isEmpty(resDataArr[i].categoryLId)) {
                        applyCd = resDataArr[i].categoryLId;
                        applyTargetNm = resDataArr[i].categoryLNm;
                        applyPriority = Number(resDataArr[i].categoryLId.substr(0, 1)) + 2;
                    }
                    var data = [applyCd, applyTargetNm, policyNo, applyPriority];

                    var checkDuplicate = applyCategoryGrid.realGrid.searchItem({fields: ["applyCd"], values: [applyCd], wrap: true});
                    if (checkDuplicate < 0) {
                        applyCategoryGrid.dataProvider.insertRow(cateCount, data);
                        cateCount++;
                    }
                }
            }
        } else if (applyTarget === "제휴채널") {
            if (resultCount > 0) {
                var affiCount = applyAffiliateGrid.realGrid.getItemCount();
                for (var i=0; i<resultCount; i++) {
                    var applyCd = resDataArr[i].channelId;
                    var applyTargetNm = resDataArr[i].channelNm;
                    var policyNo = $("#policyNo").html();
                    var applyPriority = 1;
                    var data = [applyCd, applyTargetNm, policyNo, applyPriority];

                    var checkDuplicate = applyAffiliateGrid.realGrid.searchItem({fields: ["applyCd"], values: [applyCd], wrap: true});
                    if (checkDuplicate < 0) {
                        applyAffiliateGrid.dataProvider.insertRow(affiCount, data);
                        affiCount++;
                    }
                }
            }
        }
    },
    /** 적용대상 삭제 */
    deleteApplyTarget: function () {
        var targetGrid = new Object();

        if ($("#applyTarget").html() == "상품") {
            targetGrid = applyItemGrid;
        } else if ($("#applyTarget").html() == "카테고리") {
            targetGrid = applyCategoryGrid;
        } else if ($("#applyTarget").html() == "제휴채널") {
            targetGrid = applyAffiliateGrid;
        } else {
            alert("삭제할 데이터가 없습니다.");
            return false;
        }

        var checkedList = targetGrid.realGrid.getCheckedItems();
        if (checkedList.length === 0) {
            alert("삭제할 데이터를 체크하세요.");
            return false;
        }
        targetGrid.dataProvider.removeRows(checkedList);
    },
    /** 적용대상 엑셀다운 */
    downloadExcel: function() {
        var targetGrid = new Object();
        if ($("#applyTarget").html() == "상품") {
            targetGrid = applyItemGrid;
        } else if ($("#applyTarget").html() == "카테고리") {
            targetGrid = applyCategoryGrid;
        } else if ($("#applyTarget").html() == "제휴채널") {
            targetGrid = applyAffiliateGrid;
        } else {
            alert("먼저 결제정책을 선택하세요.");
            return false;
        }

        if(targetGrid.realGrid.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName =  "적용대상_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        targetGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀을 다운받는 중입니다. 잠시만 기다려 주세요."
        });
    },
    /** 적용대상 그리드 동적관리 */
    manageApplyGrid: function () {
        // 상품
        if ($("#applyTarget").html() === "상품") {
            $("#applyItemGrid").show();
            $("#uploadExcelBtn").show();
            $("#applyCategoryGrid").hide();
            $("#applyAffiliateGrid").hide();
        // 카테고리
        } else if ($("#applyTarget").html() === "카테고리") {
            $("#applyItemGrid").hide();
            $("#uploadExcelBtn").hide();
            $("#applyCategoryGrid").show();
            $("#applyAffiliateGrid").hide();
        // 제휴
        } else if ($("#applyTarget").html() === "제휴채널") {
            $("#applyItemGrid").hide();
            $("#uploadExcelBtn").hide();
            $("#applyCategoryGrid").hide();
            $("#applyAffiliateGrid").show();
        // default
        } else {
            $("#applyItemGrid").show();
            $("#uploadExcelBtn").hide();
            $("#applyCategoryGrid").hide();
            $("#applyAffiliateGrid").hide();
        }
    },
    /** 저장 */
    save: function () {
        var targetGrid = new Object();
        if ($("#applyTarget").html() === "상품") {
            targetGrid = applyItemGrid;
        } else if ($("#applyTarget").html() === "카테고리") {
            targetGrid = applyCategoryGrid;
        } else if ($("#applyTarget").html() === "제휴채널") {
            targetGrid = applyAffiliateGrid;
        }

        var gridRowCount = targetGrid.realGrid.getItemCount();
        if (gridRowCount == 0) {
            alert("저장할 대상이 없습니다.");
            return false;
        }

        var saveData;
        var applyList = new Array();
        var applyObj = new Object();

        for (var i=0; i<gridRowCount; i++) {
            var applyData = new Object();
            var rowId = targetGrid.realGrid.getDataRow(i);
            applyData.policyNo = targetGrid.realGrid.getValue(rowId, "policyNo");
            applyData.applyCd = targetGrid.realGrid.getValue(rowId, "applyCd");
            applyData.applyPriority = targetGrid.realGrid.getValue(rowId, "applyPriority"); // category + 2
            applyData.applyTargetNm = targetGrid.realGrid.getValue(rowId, "applyTargetNm");
            applyList.push(applyData);
        }
        applyObj.policyNo = $("#policyNo").html();
        applyObj.paymentPolicyApplyDtoList = applyList;
        saveData = JSON.stringify(applyObj);

        CommonAjax.basic({
            url         : '/pg/pgDivide/savePaymentPolicyApply.json',
            data        : saveData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                paymentPolicyItemMng.search();
            }
        });
    },
    /** 저장 전 입력값 validation */
    valid: function () {
        if ($.jUtil.isEmpty($("#applyTarget").html())) {
            alert("먼저 결제정책을 선택하세요.");
            return false;
        } else if ($("#applyTarget").html() != "상품" && $("#applyTarget").html() != "카테고리" && $("#applyTarget").html() != "제휴채널") {
            alert("먼저 결제정책을 선택하세요.");
            return false;
        }
        return true;
    },
};

// 적용대상 그리드 : 상품
var applyItemGrid = {
    realGrid: new RealGridJS.GridView("applyItemGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        applyItemGrid.initGrid();
        applyItemGrid.initDataProvider();
        applyItemGrid.event();
    },
    initGrid: function () {
        applyItemGrid.realGrid.setDataSource(applyItemGrid.dataProvider);
        applyItemGrid.realGrid.setStyles(applyItemGridBaseInfo.realgrid.styles);
        applyItemGrid.realGrid.setDisplayOptions(applyItemGridBaseInfo.realgrid.displayOptions);
        applyItemGrid.realGrid.setColumns(applyItemGridBaseInfo.realgrid.columns);
        applyItemGrid.realGrid.setOptions(applyItemGridBaseInfo.realgrid.options);
        applyItemGrid.realGrid.setCheckBar({visible: true, exclusive: false, showAll: true});
        applyItemGrid.realGrid.setColumnProperty("applyCd", "header", {text: "상품번호"});
        applyItemGrid.realGrid.setColumnProperty("applyTargetNm", "header", {text: "상품명"});
    },
    initDataProvider: function () {
        applyItemGrid.dataProvider.setFields(applyItemGridBaseInfo.dataProvider.fields);
        applyItemGrid.dataProvider.setOptions(applyItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        applyItemGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            applyItemGrid.realGrid.checkRow(index, true);
        };
    },
    setData: function (dataList) {
        applyItemGrid.dataProvider.clearRows();
        applyItemGrid.dataProvider.setRows(dataList);
    }
};

// 적용대상 그리드 : 카테고리
var applyCategoryGrid = {
    realGrid: new RealGridJS.GridView("applyCategoryGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        applyCategoryGrid.initGrid();
        applyCategoryGrid.initDataProvider();
        applyCategoryGrid.event();
    },
    initGrid: function () {
        applyCategoryGrid.realGrid.setDataSource(applyCategoryGrid.dataProvider);
        applyCategoryGrid.realGrid.setStyles(applyCategoryGridBaseInfo.realgrid.styles);
        applyCategoryGrid.realGrid.setDisplayOptions(applyCategoryGridBaseInfo.realgrid.displayOptions);
        applyCategoryGrid.realGrid.setColumns(applyCategoryGridBaseInfo.realgrid.columns);
        applyCategoryGrid.realGrid.setOptions(applyCategoryGridBaseInfo.realgrid.options);
        applyCategoryGrid.realGrid.setCheckBar({visible: true, exclusive: false, showAll: true});
        applyCategoryGrid.realGrid.setColumnProperty("applyCd", "header", {text: "카테고리코드"});
        applyCategoryGrid.realGrid.setColumnProperty("applyTargetNm", "header", {text: "카테고리명"});
    },
    initDataProvider: function () {
        applyCategoryGrid.dataProvider.setFields(applyCategoryGridBaseInfo.dataProvider.fields);
        applyCategoryGrid.dataProvider.setOptions(applyCategoryGridBaseInfo.dataProvider.options);
    },
    event: function () {
        applyCategoryGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            applyCategoryGrid.realGrid.checkRow(index, true);
        };
    },
    setData: function (dataList) {
        applyCategoryGrid.dataProvider.clearRows();
        applyCategoryGrid.dataProvider.setRows(dataList);
    }
};

// 적용대상 그리드 : 제휴채널
var applyAffiliateGrid = {
    realGrid: new RealGridJS.GridView("applyAffiliateGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        applyAffiliateGrid.initGrid();
        applyAffiliateGrid.initDataProvider();
        applyAffiliateGrid.event();
    },
    initGrid: function () {
        applyAffiliateGrid.realGrid.setDataSource(applyAffiliateGrid.dataProvider);
        applyAffiliateGrid.realGrid.setStyles(applyAffiliateGridBaseInfo.realgrid.styles);
        applyAffiliateGrid.realGrid.setDisplayOptions(applyAffiliateGridBaseInfo.realgrid.displayOptions);
        applyAffiliateGrid.realGrid.setColumns(applyAffiliateGridBaseInfo.realgrid.columns);
        applyAffiliateGrid.realGrid.setOptions(applyAffiliateGridBaseInfo.realgrid.options);
        applyAffiliateGrid.realGrid.setCheckBar({visible: true, exclusive: false, showAll: true});
        applyAffiliateGrid.realGrid.setColumnProperty("applyCd", "header", {text: "채널ID"});
        applyAffiliateGrid.realGrid.setColumnProperty("applyTargetNm", "header", {text: "채널명"});
    },
    initDataProvider: function () {
        applyAffiliateGrid.dataProvider.setFields(applyAffiliateGridBaseInfo.dataProvider.fields);
        applyAffiliateGrid.dataProvider.setOptions(applyAffiliateGridBaseInfo.dataProvider.options);
    },
    event: function () {
        applyAffiliateGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            applyAffiliateGrid.realGrid.checkRow(index, true);
        };
    },
    setData: function (dataList) {
        applyAffiliateGrid.dataProvider.clearRows();
        applyAffiliateGrid.dataProvider.setRows(dataList);
    }
};

// 노출 결제수단 그리드
var exposMethodGrid = {
    treeView: new RealGridJS.TreeView("exposMethodGrid"),
    treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
    init: function () {
        exposMethodGrid.initGrid();
        exposMethodGrid.initDataProvider();
    },
    initGrid: function () {
        exposMethodGrid.treeView.setDataSource(exposMethodGrid.treeDataProviders);

        exposMethodGrid.treeView.setStyles(exposMethodGridBaseInfo.realgrid.styles);
        exposMethodGrid.treeView.setDisplayOptions(exposMethodGridBaseInfo.realgrid.displayOptions);
        exposMethodGrid.treeView.setColumns(exposMethodGridBaseInfo.realgrid.columns);
        exposMethodGrid.treeView.setOptions(exposMethodGridBaseInfo.realgrid.options);
        exposMethodGrid.treeView.setTreeOptions({lineVisible: false});
        exposMethodGrid.treeView.setSelectOptions({style: "none"});
        exposMethodGrid.treeView.setColumnProperty("methodName", "header", {text: "노출결제수단"});
    },
    initDataProvider: function () {
        exposMethodGrid.treeDataProviders.setFields(exposMethodGridBaseInfo.dataProvider.fields);
        exposMethodGrid.treeDataProviders.setOptions(exposMethodGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        exposMethodGrid.treeDataProviders.clearRows();
        exposMethodGrid.treeDataProviders.fillJsonData({"rows": dataList}, {rows: "rows", icon: "parentCode"});
    }
};

var exposMethodMng = {
    search: function (siteType, policyNo) {
        CommonAjax.basic({
            url: '/pg/pgDivide/getPaymentPolicyMethodTree.json?siteType='+siteType+'&policyNo='+policyNo,
            data: null,
            method: "GET",
            callbackFunc: function (res) {
                exposMethodGrid.setData(res);
                exposMethodGrid.treeView.expandAll();
            }
        });
    },
};