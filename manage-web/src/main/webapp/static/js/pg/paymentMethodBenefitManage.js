/** Main Script */
var paymentMethodBenefitManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        paymentMethodBenefitManageMain.bindingEvent();
        deliveryCore.initSearchDate("-1m", "0");
        deliveryCore.initApplyDate("0", "0");
        $("#applyStartDt").datepicker("option", "minDate", 0); // 오늘 이전 날짜 선택 못하도록 막음
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#schBtn").bindClick(paymentMethodBenefitManageMain.search);                        // 상단 검색영역 검색
        $("#schResetBtn").bindClick(paymentMethodBenefitManageMain.reset, "search");   // 상단 검색영역 초기화
        $("#addPaymentMethodBenefitManageBtn").bindClick(paymentMethodBenefitManageMain.add); // 하단 기본정보 저장
        $("#resetBtn").bindClick(paymentMethodBenefitManageMain.reset, "info");        // 하단 기본정보 초기화
    },

    /**
     * 검색 폼 초기화
     * @param pos : search(상단 검색영역), info(하단 정보영역)
     */
    reset: function(pos) {
        // 상단 검색영역 초기화
        if (pos === "search") {
            $("#paymentMethodBenefitManageSearchForm").resetForm();
            deliveryCore.initSearchDate("-1m", "0");
        }
        // 하단 정보영역 초기화
        else if (pos === "info") {
            if(!confirm("초기화 하시겠습니까?")) {
                return false;
            }
            $("#paymentMethodBenefitManageForm").resetForm();
            deliveryCore.initApplyDate("0", "0");
        }
    },
    /**
     * 데이터 조회
     */
    search: function() {
        if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid"])) {
            return false;
        } else if ($.jUtil.isNotEmpty($("#schBenefitTitle").val()) && $("#schBenefitTitle").val().length < 2) {
            alert("검색어는 최소 2자 이상 입력해주세요.");
            return false;
        }
        var data = $("#paymentMethodBenefitManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getPaymentMethodBenefitManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                paymentMethodBenefitManageGrid.setData(res);
                $('#paymentMethodBenefitManageSearchCnt').html($.jUtil.comma(paymentMethodBenefitManageGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 등록
     */
    add: function() {
        // Form 데이터 유효성 체크
        if (!paymentMethodBenefitManageMain.valid()) {
            return false;
        }
        // 저장여부 확인
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        console.log(_F.getFormDataByJson($("#paymentMethodBenefitManageForm")));
        CommonAjax.basic({
            url         : '/pg/paymentMethod/savePaymentMethodBenefitManage.json',
            data        : _F.getFormDataByJson($("#paymentMethodBenefitManageForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                paymentMethodBenefitManageMain.search();
            }
        });
    },
    /**
     * 데이터 등록 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 사이트
        if ($("#siteType").val() !== "HOME" && $("#siteType").val() !== "CLUB") {
            alert("사이트를 선택해주세요.");
            return false;
        }
        // 결제수단
        else if ($.jUtil.isEmpty($("#parentMethodCd").val())) {
            alert("결제수단을 선택해주세요.");
            return false;
        }
        // 사용여부
        else if ($.jUtil.isEmpty($("input[name='useYn']:checked").val())) {
            alert("사용여부를 선택해주세요.");
            return false;
        }
        // 제목
        else if ($.jUtil.isEmpty($("#benefitTitle").val())) {
            alert("제목을 입력해주세요.");
            return false;
        }
        else if ($("#benefitTitle").val().length > 20) {
            alert("제목은 20자 이내로 입력해주세요.");
            return false;
        }
        // 노출기간
        else if (!deliveryCore.dateValid($("#applyStartDt"), $("#applyEndDt"), ["isValid"])) {
            return false;
        }
        // 혜택안내문구
        else if ($.jUtil.isEmpty($("#benefitMessage1").val())) {
            alert("혜택안내문구를 입력해주세요.");
            return false;
        }
        else if ($("#benefitMessage1").val().length > 20) {
            alert("혜택안내문구는 20자 이내로 입력해주세요.");
            return false;
        }
        else if ($.jUtil.isEmpty($("#benefitMessage2").val()) && $("#benefitMessage2").val().length > 20) {
            alert("혜택안내문구는 20자 이내로 입력해주세요.");
            return false;
        }
        else if ($.jUtil.isEmpty($("#benefitMessage3").val()) && $("#benefitMessage3").val().length > 20) {
            alert("혜택안내문구는 20자 이내로 입력해주세요.");
            return false;
        }
        return true;
    },

    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawPaymentMethodBenefitManageDetail: function (gridData) {
        // 번호
        $("#paymentMethodBenefitManageSeq").val(gridData.paymentMethodBenefitManageSeq);
        // 사이트
        $("#siteType").val(gridData.siteType).prop("selected", true);
        // 결제수단
        $("#parentMethodCd").val(gridData.parentMethodCd).prop("selected", true);
        // 사용여부
        $("#use"+gridData.useYn).prop("checked", true);
        // 제목
        $("#benefitTitle").val(gridData.benefitTitle);
        // 노출기간
        $("#applyStartDt").val(gridData.applyDt.split("~")[0]);
        $("#applyEndDt").val(gridData.applyDt.split("~")[1]);
        // 혜택안내문구
        $("#benefitMessage1").val(gridData.benefitMessage1);
        $("#benefitMessage2").val(gridData.benefitMessage2);
        $("#benefitMessage3").val(gridData.benefitMessage3);
    }
};

/** Grid Script */
var paymentMethodBenefitManageGrid = {
    gridView: new RealGridJS.GridView("paymentMethodBenefitManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentMethodBenefitManageGrid.initGrid();
        paymentMethodBenefitManageGrid.initDataProvider();
        paymentMethodBenefitManageGrid.event();
    },
    initGrid: function () {
        paymentMethodBenefitManageGrid.gridView.setDataSource(paymentMethodBenefitManageGrid.dataProvider);
        paymentMethodBenefitManageGrid.gridView.setStyles(paymentMethodBenefitManageGridBaseInfo.realgrid.styles);
        paymentMethodBenefitManageGrid.gridView.setDisplayOptions(paymentMethodBenefitManageGridBaseInfo.realgrid.displayOptions);
        paymentMethodBenefitManageGrid.gridView.setColumns(paymentMethodBenefitManageGridBaseInfo.realgrid.columns);
        paymentMethodBenefitManageGrid.gridView.setOptions(paymentMethodBenefitManageGridBaseInfo.realgrid.options);

        setColumnLookupOption(paymentMethodBenefitManageGrid.gridView, "siteType", $("#schSiteType"));
        setColumnLookupOption(paymentMethodBenefitManageGrid.gridView, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        paymentMethodBenefitManageGrid.dataProvider.setFields(paymentMethodBenefitManageGridBaseInfo.dataProvider.fields);
        paymentMethodBenefitManageGrid.dataProvider.setOptions(paymentMethodBenefitManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        paymentMethodBenefitManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            paymentMethodBenefitManageMain.drawPaymentMethodBenefitManageDetail(paymentMethodBenefitManageGrid.dataProvider.getJsonRow(paymentMethodBenefitManageGrid.gridView.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        paymentMethodBenefitManageGrid.dataProvider.clearRows();
        paymentMethodBenefitManageGrid.dataProvider.setRows(dataList);
    }
};