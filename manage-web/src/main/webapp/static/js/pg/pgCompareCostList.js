/***
 * PG 대사 > PG 정산대사 조회
 */
var pgCompareCostForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.bindingEvent();
        CommonAjaxBlockUI.global();
    },
    initSearchDate : function (flag) {
        pgCompareCostForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        pgCompareCostForm.setDate.setEndDate('-1d');
        pgCompareCostForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(pgCompareCostForm.search);
        $('#clearBtn').bindClick(pgCompareCostForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(pgCompareCostDiffGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 31) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-31, "day").format("YYYY-MM-DD"));
            return false;
        }

        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var dateType = $("#dateType").val();
        var searchForm = "startDt="+startDt+"&endDt="+endDt + "&dateType=" + dateType;
        var pgKind = $("#pgKind").val();
        if (pgKind != "") {
            searchForm = searchForm + "&pgKind=" + pgKind;
        }
        pgCompareCostSumGrid.startDt = startDt;
        pgCompareCostSumGrid.endDt = endDt;
        pgCompareCostSumGrid.dateType = dateType;
        CommonAjax.basic(
            {
                url:'/pg/pgCompareCost/getSum.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    pgCompareCostSumGrid.setData(res);

                    //그룹헤더컬럼 합계
                    var stringCol = [ "hmpPaymentAmt", "hmpCommissionAmt", "hmpSettleAmt"
                        ,"pgPaymentAmt", "pgCommissionAmt", "pgSettleAmt"
                        ,"diffPaymentAmt", "diffCommissionAmt", "diffSettleAmt"];
                    pgCompareCostForm.setRealGridHeaderSum(pgCompareCostSumGrid.gridView, stringCol);
                }
            });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            pgCompareCostForm.init();
        });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
};
var pgCompareCostSumGrid = {
    gridView : new RealGridJS.GridView("pgCompareCostSumGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgCompareCostSumGrid.initGrid();
        pgCompareCostSumGrid.initDataProvider();
    },
    initGrid : function () {
        pgCompareCostSumGrid.gridView.setDataSource(pgCompareCostSumGrid.dataProvider);
        pgCompareCostSumGrid.gridView.setStyles(pgCompareCostSumBaseInfo.realgrid.styles);
        pgCompareCostSumGrid.gridView.setDisplayOptions(pgCompareCostSumBaseInfo.realgrid.displayOptions);
        pgCompareCostSumGrid.gridView.setColumns(pgCompareCostSumBaseInfo.realgrid.columns);
        pgCompareCostSumGrid.gridView.setOptions(pgCompareCostSumBaseInfo.realgrid.options);
        pgCompareCostSumGrid.gridView.onDataCellClicked = pgCompareCostSumGrid.selectRow;
        var mergeCells = [["pgKind"]];
        SettleCommon.setRealGridSumHeader(pgCompareCostSumGrid.gridView, mergeCells);
    },
    selectRow : function(gridView, data) {
        var row = pgCompareCostSumGrid.dataProvider.getJsonRow(data.dataRow);
        var params = 'startDt='+pgCompareCostSumGrid.startDt+'&endDt='+pgCompareCostSumGrid.endDt+'&pgKind='+row.pgKind+'&dateType='+pgCompareCostSumGrid.dateType;
        pgCompareCostDiffGrid.fileName = "PG정산대사조회_상세내역_" + pgCompareCostSumGrid.startDt+"_"+pgCompareCostSumGrid.endDt;
        CommonAjax.basic(
        {
            url: '/pg/pgCompareCost/getDiff.json?'+params,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                pgCompareCostDiffGrid.setData(res);
                //그룹헤더컬럼 합계
                var stringCol = [ "hmpAmt", "pgAmt", "diffAmt"];
                pgCompareCostForm.setRealGridHeaderSum(pgCompareCostDiffGrid.gridView, stringCol);
            }
        });
    },
    initDataProvider : function() {
        pgCompareCostSumGrid.dataProvider.setFields(pgCompareCostSumBaseInfo.dataProvider.fields);
        pgCompareCostSumGrid.dataProvider.setOptions(pgCompareCostSumBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgCompareCostSumGrid.dataProvider.clearRows();
        pgCompareCostSumGrid.dataProvider.setRows(dataList);
    },
    startDt : "",
    endDt : "",
    dateType : ""
};
var pgCompareCostDiffGrid = {
    gridView : new RealGridJS.GridView("pgCompareCostDiffGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgCompareCostDiffGrid.initGrid();
        pgCompareCostDiffGrid.initDataProvider();
    },
    initGrid : function () {
        pgCompareCostDiffGrid.gridView.setDataSource(pgCompareCostDiffGrid.dataProvider);
        pgCompareCostDiffGrid.gridView.setStyles(pgCompareCostDiffBaseInfo.realgrid.styles);
        pgCompareCostDiffGrid.gridView.setDisplayOptions(pgCompareCostDiffBaseInfo.realgrid.displayOptions);
        pgCompareCostDiffGrid.gridView.setColumns(pgCompareCostDiffBaseInfo.realgrid.columns);
        pgCompareCostDiffGrid.gridView.setOptions(pgCompareCostDiffBaseInfo.realgrid.options);
        var mergeCells = [["pgKind", "pgMid", "basicDt", "payScheduleDt", "pgOid", "pgTid", "flag"]];
        SettleCommon.setRealGridSumHeader(pgCompareCostDiffGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        pgCompareCostDiffGrid.dataProvider.setFields(pgCompareCostDiffBaseInfo.dataProvider.fields);
        pgCompareCostDiffGrid.dataProvider.setOptions(pgCompareCostDiffBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgCompareCostDiffGrid.dataProvider.clearRows();
        pgCompareCostDiffGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgCompareCostDiffGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgCompareCostDiffGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgCompareCostDiffGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};