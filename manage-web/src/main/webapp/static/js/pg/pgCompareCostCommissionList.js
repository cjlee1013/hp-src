/***
 * PG 대사 > PG 수수료대사 조회
 */
var pgCompareCostCommissionForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.bindingEvent();
        CommonAjaxBlockUI.global();
    },
    initSearchDate : function (flag) {
        pgCompareCostCommissionForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        pgCompareCostCommissionForm.setDate.setEndDate('-1d');
        pgCompareCostCommissionForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(pgCompareCostCommissionForm.search);
        $('#clearBtn').bindClick(pgCompareCostCommissionForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(pgCompareCostCommissionGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 31) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-31, "day").format("YYYY-MM-DD"));
            return false;
        }

        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var dateType = $("#dateType").val();
        var flagType = $("#flagType").val();
        var pgKind = $("#pgKind").val();
        var searchForm = "startDt="+startDt+"&endDt="+endDt + "&dateType=" + dateType + "&flagType=" + flagType + "&pgKind=" + pgKind;
        pgCompareCostCommissionGrid.fileName = "PG수수료대사조회_" + startDt+"_"+endDt;
        CommonAjax.basic(
            {
                url:'/pg/pgCompareCost/getCommission.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    pgCompareCostCommissionGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(pgCompareCostCommissionGrid.gridView.getItemCount()));
                    //그룹헤더컬럼 합계
                    var stringCol = [ "hmpAmt", "pgAmt", "diffAmt"];
                    pgCompareCostCommissionForm.setRealGridHeaderSum(pgCompareCostCommissionGrid.gridView, stringCol);
                }
            });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            pgCompareCostCommissionForm.init();
        });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
};
var pgCompareCostCommissionGrid = {
    gridView : new RealGridJS.GridView("pgCompareCostCommissionGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgCompareCostCommissionGrid.initGrid();
        pgCompareCostCommissionGrid.initDataProvider();
    },
    initGrid : function () {
        pgCompareCostCommissionGrid.gridView.setDataSource(pgCompareCostCommissionGrid.dataProvider);
        pgCompareCostCommissionGrid.gridView.setStyles(pgCompareCostCommissionBaseInfo.realgrid.styles);
        pgCompareCostCommissionGrid.gridView.setDisplayOptions(pgCompareCostCommissionBaseInfo.realgrid.displayOptions);
        pgCompareCostCommissionGrid.gridView.setColumns(pgCompareCostCommissionBaseInfo.realgrid.columns);
        pgCompareCostCommissionGrid.gridView.setOptions(pgCompareCostCommissionBaseInfo.realgrid.options);
        var mergeCells = [["pgKind", "pgMid", "basicDt", "payScheduleDt","pgOid", "pgTid", "flag"]];
        SettleCommon.setRealGridSumHeader(pgCompareCostCommissionGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        pgCompareCostCommissionGrid.dataProvider.setFields(pgCompareCostCommissionBaseInfo.dataProvider.fields);
        pgCompareCostCommissionGrid.dataProvider.setOptions(pgCompareCostCommissionBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgCompareCostCommissionGrid.dataProvider.clearRows();
        pgCompareCostCommissionGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgCompareCostCommissionGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgCompareCostCommissionGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgCompareCostCommissionGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};