/**
 * 결제수단 관리
 */

/**
 * Real Grid 초기화
 */
var paymentMethodGrid = {
    realGrid: new RealGridJS.GridView("paymentMethodGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentMethodGrid.initGrid();
        paymentMethodGrid.initDataProvider();
        paymentMethodGrid.event();
    },
    initGrid: function () {
        paymentMethodGrid.realGrid.setDataSource(paymentMethodGrid.dataProvider);
        paymentMethodGrid.realGrid.setStyles(paymentMethodGridBaseInfo.realgrid.styles);
        paymentMethodGrid.realGrid.setDisplayOptions(paymentMethodGridBaseInfo.realgrid.displayOptions);
        paymentMethodGrid.realGrid.setColumns(paymentMethodGridBaseInfo.realgrid.columns);
        paymentMethodGrid.realGrid.setOptions(paymentMethodGridBaseInfo.realgrid.options);
        paymentMethodGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        paymentMethodGrid.dataProvider.setFields(paymentMethodGridBaseInfo.dataProvider.fields);
        paymentMethodGrid.dataProvider.setOptions(paymentMethodGridBaseInfo.dataProvider.options);
    },
    event: function () {
        paymentMethodGrid.realGrid.onDataCellClicked = function () {
            paymentMethodMng.drawInputForm(paymentMethodGrid.dataProvider.getJsonRow(paymentMethodGrid.realGrid.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        paymentMethodGrid.dataProvider.clearRows();
        paymentMethodGrid.dataProvider.setRows(dataList);
    },
    excelDownloadAll: function () {
        // 전체 결제수단 조회
        paymentMethodGrid.dataProvider.clearRows();
        CommonAjax.basic({
            url: "/pg/paymentMethod/getPaymentMethodList.json?schParentMethodCd=ALL&schSiteType=ALL"
            , method: "GET"
            , callbackFunc: function (dataList) {
                paymentMethodGrid.dataProvider.addRows(dataList);
            }
        });

        // 전체 결제수단 엑셀다운
        setTimeout(function () {
            if (paymentMethodGrid.realGrid.getItemCount() == 0) {
                alert("전체 결제수단이 조회되지 않았습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "결제수단관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            paymentMethodGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }, 1500);
    }
};

var paymentMethodMng = {
    init: function () {
        paymentMethodMng.bindEvents();
    },
    /** event binding */
    bindEvents: function () {
        // 저장
        $("#savePaymentMethodBtn").on("click", function () {
            // 입력값 validation
            if (!paymentMethodMng.valid()) {
                return false;
            }
            if(!confirm("저장 하시겠습니까?")) {
                return false;
            }
            paymentMethodMng.insert();
        });
        // 초기화
        $("#searchResetBtn").on("click", function () {
            paymentMethodMng.reset();
        });
        // 엑셀 다운로드
        $("#excelDownloadAllBtn").on("click", function () {
            paymentMethodGrid.excelDownloadAll();
        });
        // 상세결제수단코드 변경 시 중복체크
        $("#methodCd").change(function () {
            if ($.jUtil.isEmpty($("#siteType").val())) {
                alert("사이트를 먼저 선택해주세요.");
                $("#methodCd").val("");
                $('select[name^="siteType"]').focus();
            } else if ($("#orgCode").val() != $("#methodCd").val()) {
                CommonAjax.basic({
                    url: "/pg/paymentMethod/checkDuplicateCode.json?methodCd=" + $("#methodCd").val() + "&siteType=" + $("#siteType").val()
                    , method: "GET"
                    , datatype: "json"
                    , callbackFunc: function (res) {
                        if ($.jUtil.objSize(res) > 0) {
                            alert("중복 코드입니다. 변경해주세요.");
                            $("#methodCd").val($("#orgCode").val());
                            $("#methodCd").focus();
                        }
                    }
                });
            }
        });
        // 사용여부 변경 시 노출순서 값 고정 (9999)
        $("#useYn").change(function () {
            if ($("#useYn").val() == "N") {
                $("#sortSeq").val("9999");
            }
        });

        // 이미지 초기화
        paymentMethodImg.init();
        // 이미지 파일선택 및 업로드
        $('input[name="fileArr"]').change(function() {
            paymentMethodImg.uploadImage();
        });
        // 이미지 삭제 (PC)
        $('#deleteImageBtn').click(function() {
            if (confirm('삭제하시겠습니까?')) {
                paymentMethodImg.deleteImage('pc');
            }
        });
        // 이미지 삭제 (MOBILE)
        $('#deleteImageBtnMobile').click(function() {
            if (confirm('삭제하시겠습니까?')) {
                paymentMethodImg.deleteImage('mobile');
            }
        });
        // 이미지 미리보기
        $('.uploadType').bindClick(paymentMethodImg.previewImage);
    },
    /** 결제수단 조회 */
    search: function (schParentMethodCd, schSiteType) {
        CommonAjax.basic({
            url: "/pg/paymentMethod/getPaymentMethodList.json?schParentMethodCd=" + schParentMethodCd + "&schSiteType=" + schSiteType
            , method: "GET"
            , callbackFunc: function (res) {
                console.log(res);
                paymentMethodGrid.setData(res);
                paymentMethodMng.reset();
                paymentMethodMng.selectGrid(schParentMethodCd, schSiteType);
            }
        });
    },
    /** 결제수단 조회 후 그리드 동적관리 */
    selectGrid: function (parentMethodCd, siteType) {
        $("#parentMethodCd").val(parentMethodCd);
        $("#siteType").val(siteType);

        if (parentMethodCd == "RBANK" || parentMethodCd == "PHONE") {
            $("#imageArea").hide();
            $("#pgCodeArea1").show();
        } else if (parentMethodCd == "KAKAOPAY" || parentMethodCd == "KAKAOMONEY" || parentMethodCd == "NAVERPAY" || parentMethodCd == "SAMSUNGPAY" || parentMethodCd == "TOSS" || parentMethodCd == "PAYCO") {
            $("#imageArea").show();
            $("#pgCodeArea1").hide();
        } else {
            $("#imageArea").show();
            $("#pgCodeArea1").show();
        }
    },
    /** 조회결과 선택 시 하단상세(inputForm) 세팅 */
    drawInputForm: function (data) {
        $("#siteType").val(data.siteType);
        $("#siteType").prop("disabled", true);
        $("#useYn").val(data.useYn);
        $("#platform").val(data.platform);
        $("#sortSeq").val(data.sortSeq);
        $("#methodCd").val(data.methodCd);
        $("#methodCd").prop("disabled", true);
        $("#methodNm").val(data.methodNm);
        $("#inicisCd").val(data.inicisCd);
        $("#tosspgCd").val(data.tosspgCd);

        $("#parentMethodCd").val(data.parentMethodCd);
        $("#pgCertifyKind").val(data.pgCertifyKind);
        $("#orgCode").val(data.methodCd);

        if (!$.jUtil.isEmpty(data.imgUrl)) {
            paymentMethodImg.setImage('pc', {
                'imgUrl'    : data.imgUrl,
                'imgWidth'  : data.imgWidth,
                'imgHeight' : data.imgHight,
                'src'       : apiImgUrl+"/provide/view?processKey="+paymentMethodImg.processKey+"\&fileId="+data.imgUrl,
                'changeYn'  : 'N'
            });
        } else {
            paymentMethodImg.setImage('pc', null, true);
        }

        if (!$.jUtil.isEmpty(data.imgUrlMobile)) {
            paymentMethodImg.setImage('mobile', {
                'imgUrl'    : data.imgUrlMobile,
                'src'       : apiImgUrl+"/provide/view?processKey="+paymentMethodImg.processKey+"\&fileId="+data.imgUrlMobile,
                'changeYn'  : 'N'
            });
        } else {
            paymentMethodImg.setImage('mobile', null, true);
        }
    },
    /** 저장 */
    insert: function () {
        var schParentMethod = $("#parentMethodCd").val();
        var schSiteType = $("#siteType").val();
        $("#siteType").prop("disabled", false);
        $("#methodCd").prop("disabled", false);
        CommonAjax.basic({
            url         : '/pg/paymentMethod/savePaymentMethod.json',
            data        : _F.getFormDataByJson($("#inputForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                paymentMethodMng.search(schParentMethod, schSiteType);
            }
        });
    },
    /** 저장 전 입력값 validation */
    valid: function () {
        // 결제수단 미선택 여부 체크
        if ($.jUtil.isEmpty($("#parentMethodCd").val())) {
            alert("결제수단이 선택되지 않았습니다. 선택 후, 다시 진행해주세요.");
            return false;
        // PG사 코드 미등록 여부 체크
        } else if ($("#parentMethodCd").val() != "KAKAOPAY" && $("#parentMethodCd").val() != "KAKAOMONEY" && $("#parentMethodCd").val() != "NAVERPAY" && $("#parentMethodCd").val() != "SAMSUNGPAY" && $("#parentMethodCd").val() != "TOSS" && $("#parentMethodCd").val() != "PAYCO") {
            if ($.jUtil.isEmpty($("#inicisCd").val()) && $.jUtil.isEmpty($("#tosspgCd").val())) {
                alert("PG사 코드는 1개 이상 입력해야 합니다. 다시 확인해주세요.");
                return false;
            }
        }

        // 상세결제수단 코드 입력여부 체크
        if ($.jUtil.isEmpty($("#methodCd").val())) {
            alert("상세결제수단 코드가 입력되지 않았습니다. 다시 확인해주세요");
            return false;
        // 상세결제수단명 입력여부 체크
        } else if ($.jUtil.isEmpty($("#methodNm").val())) {
            alert("상세결제수단명이 입력되지 않았습니다. 다시 확인해주세요");
            return false;
        // 노출순서 입력여부 체크
        } else if ($.jUtil.isEmpty($("#sortSeq").val())) {
            alert("노출순서가 입력되지 않았습니다. 다시 확인해주세요.");
            return false;
        // 노출순서 값 체크
        } else if ($("#sortSeq").val() < 1) {
            alert("노출순서가 0보다 커야 합니다.");
            return false;
        } else if ($("#sortSeq").val() > 9999) {
            alert("노출순서는 최대 9999 까지 입력해주세요.");
            return false;
        // 노출범위 설정여부 체크
        } else if ($("#useYn").val() == "Y" && $.jUtil.isEmpty($("#platform").val())) {
            alert("노출범위가 설정되지 않았습니다. 활성 상태에서 노출범위는 필수 설정항목 입니다.");
            return false;
        }

        // 이미지 업로드 공통 api 연동안됨. 연동 후 주석 해제 예정.
        // 이미지 관련 validation 체크
        // if ($("#parentMethodCd").val() != "RBANK" && $("#parentMethodCd").val() != "PHONE") {
        //     if ($("#imgUrl").val() == "" || $("#imgUrlMobile").val() == "") {
        //         alert("노출이미지가 등록되지 않았습니다. 노출이미지(PC/모바일)는 필수 항목입니다.");
        //         return false;
        //     }
        // }

        // PG사 코드 없을 경우 "-" 입력
        if ($.jUtil.isEmpty($("#inicisCd").val())) {
            $("#inicisCd").val("-");
        }
        // PG사 코드 없을 경우 "-" 입력
        if ($.jUtil.isEmpty($("#tosspgCd").val())) {
            $("#tosspgCd").val("-");
        }
        return true;
    },
    /** 초기화 */
    reset: function () {
        $("#siteType").val("");
        $("#siteType").prop("disabled", false);
        $("#useYn").val("N");
        $("#platform").val("");
        $("#sortSeq").val("");
        $("#methodCd").val("");
        $("#methodCd").prop("disabled", false);
        $("#methodNm").val("");
        $("#inicisCd").val("");
        $("#tosspgCd").val("");
        $("#orgCode").val("");

        $("#thumbUrl").attr("src", "");
        $("#thumbArea").hide();
        $("#uploadArea").show();
        $("#thumbUrlMobile").attr("src", "");
        $("#thumbAreaMobile").hide();
        $("#uploadAreaMobile").show();
    }
};

var paymentMethodImg = {
    processKey : "UserCreditCardImage",
    imgType : null,

    /** 이미지 초기화 */
    init : function() {
        paymentMethodImg.setImage('pc', null, true);
        paymentMethodImg.setImage('mobile', null, true);
        paymentMethodImg.imgType = null;
    },
    /** 화면 이미지 정보 설정 */
    setImage : function(type, params) {
        switch (type) {
            case 'pc':
                if (params) {
                    $('.imgUrl').val(params.imgUrl);
                    $('.imgWidth').val(params.imgWidth);
                    $('.imgHight').val(params.imgHeight);
                    $('.changeYn').val(params.changeYn);
                    $('#thumbUrl').prop('src', params.src);

                    $('#thumbArea').show();
                    $('#thumbArea').siblings('#uploadArea').hide();
                } else {
                    $('.imgUrl').val('');
                    $('.imgWidth').val('');
                    $('.imgHight').val('');
                    $('.changeYn').val('N');
                    $('#thumbUrl').prop('src', '');

                    $('#thumbArea').hide();
                    $('#thumbArea').siblings('#uploadArea').show();
                }
                break;
            case 'mobile':
                if (params) {
                    $('.imgUrlMobile').val(params.imgUrl);
                    $('.mobileChangeYn').val(params.changeYn);
                    $('#thumbUrlMobile').prop('src', params.src);

                    $('#thumbAreaMobile').show();
                    $('#thumbAreaMobile').siblings('#uploadAreaMobile').hide();
                } else {
                    $('.imgUrlMobile').val('');
                    $('.mobileChangeYn').val('N');
                    $('#thumbUrlMobile').prop('src', '');

                    $('#thumbAreaMobile').hide();
                    $('#thumbAreaMobile').siblings('#uploadAreaMobile').show();
                }
                break;
        }
    },
    /** 이미지 업로드 */
    uploadImage : function() {
        let file = $('#paymentMethodImgFile');
        let fileName;
        if (paymentMethodImg.imgType === "mobile") {
            fileName = $("#siteType").val()+"_"+$("#methodCd").val()+"_MOBILE";
        } else {
            fileName = $("#siteType").val()+"_"+$("#methodCd").val();
        }
        let params = {
            processKey : paymentMethodImg.processKey,
            mode : "IMG",
            fileName : fileName
        };
        // 파일확장자 (필요 시 사용)
        // let ext = $('#paymentMethodImgFile').get(0).files[0].name.split('.');

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];
                    if (f.hasOwnProperty("errors")) {
                        alert(f.error);
                    } else {
                        paymentMethodImg.setImage(paymentMethodImg.imgType, {
                            'imgUrl'    : f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'       : apiImgUrl+"/provide/view?processKey="+paymentMethodImg.processKey+"\&fileId="+f.fileStoreInfo.fileId,
                            'changeYn'  : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /** 이미지 미리보기 팝업 */
    previewImage : function(obj) {
        let imgUrl = $(obj).find('img').prop('src');
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /** 이미지 삭제 버튼처리 (화면 초기화) */
    deleteImage : function(type) {
        paymentMethodImg.setImage(type, null, true);
    },
    /** 이미지 등록을 위한 클릭처리 */
    saveImageFile : function(type) {
        paymentMethodImg.imgType = type;
        $('input[name=fileArr]').click();
    }
};