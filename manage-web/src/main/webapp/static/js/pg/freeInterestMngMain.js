/** Main Script */
var freeInterestMngMain = {
    COPYCARD                : "COPYCARD",   //복사FLAG값 (복사됨)
    NONE                    : "NONE",       //복사FLAG값 (복사안됨)
    /**
     * init 이벤트
     */
    init: function() {
        freeInterestMngMain.bindingEvent();
        freeInterestMngMain.bindingAllowEvent();
        deliveryCore.initSearchDate("-1m", "0");
        deliveryCore.initApplyDate("0", "+1m");
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        // 상단 검색영역 검색
        $("#schBtn").bindClick(freeInterestMngMain.search);
        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(freeInterestMngMain.resetSch);
        // 기본정보 저장
        $("#saveBtn").bindClick(freeInterestMngMain.basicSave);
        // 기본정보 초기화
        $("#resetBtn").bindClick(freeInterestMngMain.resetBasicInfo);
        //상세정보 저장
        $("#saveAllBtn").bindClick(freeInterestDetail.applyCopyAndSave);
        //상세정보 초기화
        $("#resetDetailBtn").bindClick(freeInterestDetail.resetApplyInfo);
        //복사
        $("#copyBtn").bindClick(freeInterestMngMain.copyCard);
        //삭제
        $("#applyDeleteBtn").bindClick(freeInterestDetail.applyDelete);
    },
    /**
     * 복사진행
     */
    copyCard : function() {
        if (freeInterestApplyGrid.dataProvider.getRowCount()<=0) {
            alert("상세정보가 없습니다.");
            return false;
        }
        if (!confirm("기본정보/상세정보를 복사하시겠습니까?")) {
            return false;
        }
        $("#copyFlag").val(freeInterestMngMain.COPYCARD);
        //기본정보 키값 초기화
        $("#freeInterestMngSeq").val("");
        //상세정보 키값 초기화
        $("#applySeq").val("");
        //기본정보 적용기간 초기화
        $("#applyStartDt").val("");
        $("#applyEndDt").val("");
        validCheck.basicInfoVisible(false);

    },
    /**
     * 입력값 제약조건 바인딩
     */
    bindingAllowEvent : function() {
        $("#paymentAmt").allowInput("keyup", ["NUM"], $(this).attr("id"));
    },
    /**
     * 데이터 조회
     */
    search: function() {

        if (!validCheck.search()) {
            return false;
        }

        var data = $("#freeInterestSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getFreeInterestMngList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                freeInterestGrid.setData(res);
                $('#freeInterestSearchCnt').html($.jUtil.comma(freeInterestGrid.gridView.getItemCount()));
                freeInterestApplyGrid.dataProvider.clearRows();
                freeInterestMngMain.resetBasicInfo();
                freeInterestDetail.resetApplyInfo();
                freeInterestMngMain.copyFlagChange(freeInterestMngMain.NONE);
                validCheck.basicInfoVisible(false);
                validCheck.applyInfoVisible(false);
                deliveryCore.initApplyDate("0", "+1m");
            }
        });
    },
    copyFlagChange:function(copyFlag) {
        $("#copyFlag").val(copyFlag);
    },
    /**
     * 검색영역 초기화
     */
    resetSch : function () {
        $("#freeInterestSearchForm").resetForm();
        deliveryCore.initSearchDate("-1m", "0");
    },
    /**
     * 기본정보 초기화
     */
    resetBasicInfo : function() {
        $("#freeInterestForm").resetForm();
        freeInterestMngMain.copyFlagChange(freeInterestMngMain.NONE);
        validCheck.basicInfoVisible(false);
        freeInterestApplyGrid.dataProvider.clearRows();
        freeInterestDetail.resetApplyInfo();
        $('#freeInterestApplyCnt').html("0");
    },
    /**
     * 데이터 등록
     */
    basicSave : function() {
        //저장 유효성 검사
        if (!validCheck.basicSaveValid()) {
            return false;
        }

        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveFreeInterest.json',
            data        : _F.getFormDataByJson($("#freeInterestForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.errors !== undefined) {
                    alert(res.errors[0].detail);
                } else {
                    console.log(res);
                    alert("저장하였습니다.");
                    $("#freeInterestForm").resetForm();
                    deliveryCore.initApplyDate("0", "+1m");
                    freeInterestMngMain.search();
                }
            }
        });
    },
    freeInterestMainSelect : function(data) {
        console.log(data);

        $("#freeInterestMngSeq").val(data.freeInterestMngSeq);
        $("#siteType").val(data.siteType);
        $("#freeInterestNm").val(data.freeInterestNm);
        $('input:radio[name="useYn"][value="'+data.useYn+'"]').trigger('click');
        $("#applyStartDt").val(data.applyStartDt);
        $("#applyEndDt").val(data.applyEndDt);

        //수정불가항목 ReadOnly처리
        validCheck.basicInfoVisible(true);
        //무이자혜택관리상세정보 조회
        freeInterestDetail.detailSearch(data.freeInterestMngSeq);
    }

};

var freeInterestDetail = {

    /**
     * 무이자 대상 카드정보 조회
     * @param freeInterestMngSeq
     */
    detailSearch: function(freeInterestMngSeq) {

        CommonAjax.basic({
            url         : '/pg/paymentMethod/getFreeInterestApplyList.json?freeInterestMngSeq=' + freeInterestMngSeq,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                freeInterestApplyGrid.setData(res);
                $('#freeInterestApplyCnt').html($.jUtil.comma(freeInterestApplyGrid.gridView.getItemCount()));
            },
            errorCallbackFunc   : function() {
                alert("카드상세정보 조회중 오류가 발생하였습니다. 기본정보를 재 선택해 주세요.");
            }
        });
    },
    /**
     * 상세정보초기화
     */
    resetApplyInfo : function() {
        $("#freeInterestApplyForm").resetForm();
        $("#freeInterestApplyForm input[type='hidden']").val("");
        freeInterestMngMain.copyFlagChange(freeInterestMngMain.NONE);
        validCheck.applyInfoVisible(false);
    },
    /**
     * 상세정보 저장
     * 복사내용 저장
     */
    applyCopyAndSave : function() {

        let copyFlag = $("#copyFlag").val();
        if (copyFlag === freeInterestMngMain.COPYCARD) {
            var applyDataArray = new Array();
            var basicData = {};
            if (!validCheck.basicSaveValid()) {
                return false;
            }
            //기본정보Setting
            basicData.siteType = $("#siteType").val();
            basicData.freeInterestNm = $("#freeInterestNm").val();
            basicData.useYn = "Y";
            basicData.applyStartDt = $("#applyStartDt").val();
            basicData.applyEndDt = $("#applyEndDt").val();


            if(!confirm("복사된 내용을 저장 하시겠습니까?")) {
                return false;
            }

            var rtnList = freeInterestApplyGrid.dataProvider.getRows(0,-1);
            rtnList.forEach(function(value,row) {
                var tempData = {};
                tempData.methodCd = value[2];
                tempData.paymentAmt = value[3];
                tempData.freeInterestType = value[4];
                tempData.installmentTerm = value[5];
                applyDataArray.push(tempData);
            });

            var applyData = {};
            applyData.freeInterestDto = basicData;
            applyData.freeInterestApplyDtoList = applyDataArray;

            var jsonData = JSON.stringify(applyData);
            console.log(applyData);
            CommonAjax.basic({
                url         : '/pg/paymentMethod/saveCopyFreeInterest.json',
                data        : jsonData,
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.errors !== undefined) {
                        alert(res.errors[0].detail);
                    } else {
                        console.log(res);
                        alert("복사된 정보를 저장하였습니다.");
                        freeInterestMngMain.search();
                        $("#freeInterestForm").resetForm();
                        $("#freeInterestApplyForm").resetForm();

                    }
                }
            });
        } else {
            freeInterestDetail.applySave();
        }
    },
    /**
     * 상세정보 저장
     */
    applySave: function() {

        // 입력데이타 검증
        if (!validCheck.applySaveValid()) {
            return false;
        }

        if(!confirm("상세정보(복사)를 저장 하시겠습니까?")) {
            return false;
        }

        var applyData = {};
        let freeInterestMngSeq = $("#freeInterestMngSeq").val();

        applyData.applySeq = $("#applySeq").val();
        applyData.methodCd = $("#methodCd").val();
        applyData.paymentAmt = $("#paymentAmt").val();
        applyData.freeInterestType = $("#freeInterestType").val();
        applyData.installmentTerm = $("#installmentTerm").val();
        applyData.freeInterestMngSeq = freeInterestMngSeq;

        var jsonData = JSON.stringify(applyData);

        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveFreeInterestApply.json',
            data        : jsonData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.errors !== undefined) {
                    alert(res.errors[0].detail);
                } else {
                    console.log(res);
                    alert("카드정보를 저장하였습니다.");
                    freeInterestDetail.detailSearch(freeInterestMngSeq);
                    $("#freeInterestApplyForm").resetForm();

                }
            }
        });
    },
    /**
     * 상세정보 삭제
     */
    applyDelete:function(){
        if (freeInterestApplyGrid.dataProvider.getRowCount()<=0) {
            alert("상세정보가 없습니다.");
            return false;
        }
        let freeInterestMngSeq = $("#freeInterestMngSeq").val();
        var current = freeInterestApplyGrid.gridView.getCurrent();
        var gridData = freeInterestApplyGrid.dataProvider.getJsonRow(current.dataRow);
        var deleteData = {};
        deleteData.applySeq = gridData.applySeq;
        deleteData.methodCd = gridData.methodCd;

        if (!confirm("선택하신 카드정보를 삭제 하시겠습니까?")) {
            return false;
        }

        var jsonData = JSON.stringify(deleteData);
        CommonAjax.basic({
            url         : '/pg/paymentMethod/deleteFreeInterestApply.json',
            data        : jsonData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.errors !== undefined) {
                    alert(res.errors[0].detail);
                } else {
                    console.log(res);
                    alert("카드정보를 삭제하였습니다.");
                    freeInterestDetail.detailSearch(freeInterestMngSeq);
                    $("#freeInterestApplyForm").resetForm();

                }
            }
        });


    },
    /**
     * 상세정보 그리드 클릭
     * @param data
     */
    freeInterestApplySelect : function(data) {
        $("#methodCd").val(data.methodCd);
        $("#paymentAmt").val(data.paymentAmt);
        $("#freeInterestType").val(data.freeInterestType);
        $("#installmentTerm").val(data.installmentTerm);
        $("#applySeq").val(data.applySeq);
        //상세정보 변경불가처리
        validCheck.applyInfoVisible(true);
    },
};

//조회/검색 검증
var validCheck = {
    search : function() {
        if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid", "overOneYearRange"])) {
            return false;
        }
        return true;
    },
    /**
     * 데이터 등록 전 Form 데이터 유효성 체크
     */
    basicSaveValid : function() {
        if ($.jUtil.isEmpty($("#siteType").val())) {
            alert("사이트를 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#freeInterestNm").val())) {
            alert("제목을 이력해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("input[name=useYn]:checked").val())) {
            alert("사용여부를 선택해주세요.");
            return false;
        }

        if (!deliveryCore.dateValid($("#applyStartDt"), $("#applyEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        }
        return true;
    },
    /**
     * 적용대상 Form 데이터 유효성 체크
     */
    applySaveValid : function() {
        var installmentRegexp = new RegExp('[^0-9\,]','g');
        if ($.jUtil.isEmpty($("#freeInterestMngSeq").val())) {
            alert("기본정보를 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#methodCd").val())) {
            alert("카드사를 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#paymentAmt").val())) {
            alert("결제금액을 입력해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#freeInterestType").val())) {
            alert("무이자유형을 선택해 주세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#installmentTerm").val())) {
            alert("할부기간을 입력해 주세요.");
            return false;
        } else if (installmentRegexp.test($("#installmentTerm").val())) {
            alert("할부기간에는 숫자(0-9)/콤마(,)만 입력 가능합니다. ");
            return false;
        }

        return true;
    },
    /**
     * 기본정보 Disable/Enable
     */
    basicInfoVisible:function (flag) {
        $("#siteType").prop("disabled",flag);
        $("#applyStartDt").prop("readonly",flag);
        $("#applyEndDt").prop("readonly",flag);
    },
    /**
     * 적용대상 Disable/Enable
     * @param flag
     */
    applyInfoVisible:function (flag) {
        $("#methodCd").prop("disabled",flag);
        $("#freeInterestType").prop("disabled",flag);
    }
};


/** Grid Script */
var freeInterestGrid = {
    gridView: new RealGridJS.GridView("freeInterestGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        freeInterestGrid.initGrid();
        freeInterestGrid.initDataProvider();
        freeInterestGrid.event();
    },
    initGrid: function () {
        freeInterestGrid.gridView.setDataSource(freeInterestGrid.dataProvider);
        freeInterestGrid.gridView.setStyles(freeInterestGridBaseInfo.realgrid.styles);
        freeInterestGrid.gridView.setDisplayOptions(freeInterestGridBaseInfo.realgrid.displayOptions);
        freeInterestGrid.gridView.setColumns(freeInterestGridBaseInfo.realgrid.columns);
        freeInterestGrid.gridView.setOptions(freeInterestGridBaseInfo.realgrid.options);
        setColumnLookupOption(freeInterestGrid.gridView, "siteType", $("#siteType"));
        setColumnLookupOption(freeInterestGrid.gridView, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        freeInterestGrid.dataProvider.setFields(freeInterestGridBaseInfo.dataProvider.fields);
        freeInterestGrid.dataProvider.setOptions(freeInterestGridBaseInfo.dataProvider.options);
    },
    event: function () {
        freeInterestGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = freeInterestGrid.dataProvider.getJsonRow(index.dataRow);
            freeInterestMngMain.freeInterestMainSelect(data);
        };
    },
    setData: function (dataList) {
        freeInterestGrid.dataProvider.clearRows();
        freeInterestGrid.dataProvider.setRows(dataList);
    }
};

/** 무이자Apply Grid **/
var freeInterestApplyGrid = {
    gridView: new RealGridJS.GridView("freeInterestApplyGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        freeInterestApplyGrid.initGrid();
        freeInterestApplyGrid.initDataProvider();
        freeInterestApplyGrid.event();
    },
    initGrid: function () {
        freeInterestApplyGrid.gridView.setDataSource(freeInterestApplyGrid.dataProvider);
        freeInterestApplyGrid.gridView.setStyles(freeInterestApplyGridBaseInfo.realgrid.styles);
        freeInterestApplyGrid.gridView.setDisplayOptions(freeInterestApplyGridBaseInfo.realgrid.displayOptions);
        freeInterestApplyGrid.gridView.setColumns(freeInterestApplyGridBaseInfo.realgrid.columns);
        freeInterestApplyGrid.gridView.setOptions(freeInterestApplyGridBaseInfo.realgrid.options);
        setColumnLookupOption(freeInterestApplyGrid.gridView, "methodCd", $("#methodCd"));
        setColumnLookupOption(freeInterestApplyGrid.gridView, "freeInterestType", $("#freeInterestType"));
    },
    initDataProvider: function () {
        freeInterestApplyGrid.dataProvider.setFields(freeInterestApplyGridBaseInfo.dataProvider.fields);
        freeInterestApplyGrid.dataProvider.setOptions(freeInterestApplyGridBaseInfo.dataProvider.options);
    },
    event: function () {
        freeInterestApplyGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = freeInterestApplyGrid.dataProvider.getJsonRow(index.dataRow);
            freeInterestDetail.freeInterestApplySelect(data);
        };
    },
    setData: function (dataList) {
        freeInterestApplyGrid.dataProvider.clearRows();
        freeInterestApplyGrid.dataProvider.setRows(dataList);
    }
};