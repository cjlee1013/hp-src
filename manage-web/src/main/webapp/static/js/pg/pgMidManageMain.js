/** Main Script */
var pgMidManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        pgMidManageMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#schBtn").bindClick(pgMidManageMain.search);                        // 상단 검색영역 검색
        $("#schResetBtn").bindClick(pgMidManageMain.reset, "search");   // 상단 검색영역 초기화
        $("#addPgMidManageBtn").bindClick(pgMidManageMain.add);                // 하단 기본정보 저장
        $("#resetBtn").bindClick(pgMidManageMain.reset, "info");        // 하단 기본정보 초기화
    },

    /**
     * 검색 폼 초기화
     * @param pos : search(상단 검색영역), info(하단 정보영역)
     */
    reset: function(pos) {
        // 상단 검색영역 초기화
        if (pos === "search") {
            $("#pgMidManageSearchForm").resetForm();
        }
        // 하단 정보영역 초기화
        else if (pos === "info") {
            if(!confirm("초기화 하시겠습니까?")) {
                return false;
            }
            $("#pgMidManageForm").resetForm();
        }
    },
    /**
     * 데이터 조회
     */
    search: function() {
        var data = $("#pgMidManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getPgMidManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                pgMidManageGrid.setData(res);
                $('#pgMidManageSearchCnt').html($.jUtil.comma(pgMidManageGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 등록
     */
    add: function() {
        // Form 데이터 유효성 체크
        if (!pgMidManageMain.valid()) {
            return false;
        }
        // 저장여부 확인
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        CommonAjax.basic({
            url         : '/pg/paymentMethod/savePgMidManage.json',
            data        : _F.getFormDataByJson($("#pgMidManageForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                pgMidManageMain.search();
            }
        });
    },
    /**
     * 데이터 등록 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 1.PG사
        if ($.jUtil.isEmpty($("#pgKind").val())) {
            alert("PG사를 선택해주세요.");
            return false;
        }
        // 2.사용여부
        else if ($.jUtil.isEmpty($("input[name=useYn]:checked").val())) {
            alert("사용여부를 선택해주세요.");
            return false;
        }
        // 3.노출순서
        else if ($.jUtil.isEmpty($("#dispSeq").val())) {
            alert("노출순서 를 입력해주세요.");
            return false;
        }
        else if ($("#dispSeq").val().length > 2) {
            alert("노출순서는 2자 이내로 입력해주세요.");
            return false;
        }
        else if (!$.jUtil.isAllowInput($("#dispSeq").val(), ['NUM'])) {
            alert("노출순서는 숫자만 입력해주세요.");
            return false;
        }
        // 4.상점ID
        else if ($.jUtil.isEmpty($("#pgMid").val())) {
            alert("상점ID 를 입력해주세요.");
            return false;
        }
        else if ($("#pgMid").val().length > 15) {
            alert("상점ID는 15자 이내로 입력해주세요.");
            return false;
        }
        // 5.사용목적
        else if ($.jUtil.isEmpty($("#usePurpose").val())) {
            alert("사용목적을 선택해주세요.");
            return false;
        }
        // 6.사이트 Key
        else if (!$.jUtil.isEmpty($("#siteKey").val())) {
            if ($("#siteKey").val().length > 100) {
                alert("사이트 Key는 100자 이내로 입력해주세요.");
                return false;
            }
            else if (!$.jUtil.isAllowInput($("#siteKey").val(), ['NUM','ENG'])) {
                alert("사이트 Key는 영문과 숫자만 입력해주세요.");
                return false;
            }
        }
        // 7.무이자사용여부
        else if ($.jUtil.isEmpty($("input[name=freeInterestYn]:checked").val())) {
            alert("무이자사용여부를 선택해주세요.");
            return false;
        }
        // 8.비고
        else if (!$.jUtil.isEmpty($("#note").val())) {
            if ($("#note").val().length > 100) {
                alert("비고는 100자 이내로 입력해주세요.");
                return false;
            }
        }
        return true;
    },

    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawPgMidManageDetail: function (gridData) {
        // 1.PG사
        $("#pgKind").val(gridData.pgKind).prop("selected", true);
        // 2.사용여부 (Y:사용/N:사용안함)
        $("#use"+gridData.useYn).prop("checked", true);
        // 3.노출순서
        $("#dispSeq").val(gridData.dispSeq);
        // 4.상점ID
        $("#pgMid").val(gridData.pgMid);
        // 5.사용목적
        $("#usePurpose").val(gridData.usePurpose).prop('selected', true);
        // 6.간편결제 선택여부 (SAMSUNGPAY:삼성페이/KAKAOPAY:카카오카드/KAKAOMONEY:카카오머니/NAVERPAY:네이버페이/TOSS:토스/PAYCO:페이코)
        $("#easyPayTarget").val(gridData.easyPayTarget).prop('selected', true);
        // 7.사이트 Key
        $("#siteKey").val(gridData.siteKey);
        // 8.무이자사용여부 (Y:사용/N:사용안함)
        $("#freeInterest"+gridData.freeInterestYn).prop("checked", true);
        // 9.비고
        $("#note").val(gridData.note);
    }
};

/** Grid Script */
var pgMidManageGrid = {
    gridView: new RealGridJS.GridView("pgMidManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pgMidManageGrid.initGrid();
        pgMidManageGrid.initDataProvider();
        pgMidManageGrid.event();
    },
    initGrid: function () {
        pgMidManageGrid.gridView.setDataSource(pgMidManageGrid.dataProvider);
        pgMidManageGrid.gridView.setStyles(pgMidManageGridBaseInfo.realgrid.styles);
        pgMidManageGrid.gridView.setDisplayOptions(pgMidManageGridBaseInfo.realgrid.displayOptions);
        pgMidManageGrid.gridView.setColumns(pgMidManageGridBaseInfo.realgrid.columns);
        pgMidManageGrid.gridView.setOptions(pgMidManageGridBaseInfo.realgrid.options);

        setColumnLookupOption(pgMidManageGrid.gridView, "useYn", $("#schUseYn"));
        setColumnLookupOption(pgMidManageGrid.gridView, "usePurpose", $("#usePurpose"));
    },
    initDataProvider: function () {
        pgMidManageGrid.dataProvider.setFields(pgMidManageGridBaseInfo.dataProvider.fields);
        pgMidManageGrid.dataProvider.setOptions(pgMidManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        pgMidManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            pgMidManageMain.drawPgMidManageDetail(pgMidManageGrid.dataProvider.getJsonRow(pgMidManageGrid.gridView.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        pgMidManageGrid.dataProvider.clearRows();
        pgMidManageGrid.dataProvider.setRows(dataList);
    }
};