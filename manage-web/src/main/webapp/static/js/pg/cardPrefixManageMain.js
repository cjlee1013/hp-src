/** 카드 PREFIX 관리 Main */
var cardPrefixManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        cardPrefixManageMain.bindEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent : function() {
        //카드프리픽스 입력값 validation
        $("#schCardPrefixNo").allowInput("keyup", ["NUM"]);
        //상단 검색영역검색
        $("#schBtn").bindClick(cardPrefixManageMain.search);
        //상단 검색영역 초기화
        $("#schResetBtn").bindClick(cardPrefixManageMain.schReset);
        //기본정보 초기화
        $("#resetBtn").bindClick(cardPrefixManageMain.resetInfo);
        //기본정보 저장
        $("#updateCardPrefixManageBtn").bindClick(cardPrefixManageMain.update);
    },
    /**
     * 상단검색영역초기화
     */
    schReset:function() {
        $("#cardPrefixManageSearchForm select").find('option:first').prop('selected', true);
        $("#cardPrefixManageSearchForm input[type='text']").val("");
    },
    /**
     * 기본정보 초기화
     */
    resetInfo:function() {
        $("#cardPrefixManageForm select").find('option:first').prop('selected', true);
        $("#cardPrefixManageForm input[type='text']").val("");
        $("#cardPrefixManageForm input[type='hidden']").val("");
        $("#bundleMethodCd").val("");
    },
    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawDetail: function (data) {
        $("#cardPrefixNo").val(data.cardPrefixNo);
        $("#cardKind").val(data.cardKind);
        $("#methodCd").val(data.methodCd);
        $("#cardDetailKind").val(data.cardDetailKind);
    },
    /**
     * 데이터 조회
     */
    search: function() {
        var data = $("#cardPrefixManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/paymentMethod/getCardPrefixManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(dataList) {
                console.log(dataList);
                cardPrefixManageGrid.setData(dataList);
                $('#cardPrefixManageSearchCnt').html($.jUtil.comma(cardPrefixManageGrid.gridView.getItemCount()));
                //기본정보 초기화
                cardPrefixManageMain.resetInfo();
            }
        });
    },
    /**
     * 데이터 저장
     */
    update: function() {
        // Form 데이터 유효성 체크
        if (!cardPrefixManageMain.valid()) {
            return false;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }

        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveCardPrefixManage.json',
            data        : _F.getFormDataByJson($("#cardPrefixManageForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                cardPrefixManageMain.search();
            }
        });
    },
    /**
     * 데이터 저장 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // 1.필수값체크:카드프리픽스
        if ($.jUtil.isEmpty($("#cardPrefixNo").val())) {
            alert("수정할 데이터를 선택해주세요.");
            return false;
        // 2.필수값체크:카드종류
        } else if ($.jUtil.isEmpty($("#cardKind").val())) {
            alert("수정할 데이터를 선택해주세요.");
            return false;
        // 3.변경할 값체크 - 결제수단, 카드상세종류 모두 미선택 시 체크
        } else if ($.jUtil.isEmpty($("#methodCd").val()) && $.jUtil.isEmpty($("#cardDetailKind").val())) {
            alert("선택된 변경할 값이 없습니다. 변경할 결제수단/카드상세종류를 선택해주세요.");
            return false;
        }
        return true;
    },
    /**
     * 결제수단일괄변경 - 화면값 변경
     */
    applyGridMethodCd: function () {
        var checkedList = cardPrefixManageGrid.gridView.getCheckedRows(true);
        if (checkedList.length === 0) {
            alert("변경할 데이터를 체크하세요.");
            return false;
        }
        if ($("#bundleMethodCd").val() === "") {
            alert("입력할 결제수단을 선택하세요.");
            return false;
        }
        for (var i = 0; i < checkedList.length; i++) {
            cardPrefixManageGrid.dataProvider.setValue(checkedList[i], "methodCd", $("#bundleMethodCd").val());
            cardPrefixManageGrid.dataProvider.setValue(checkedList[i], "methodNm", $("#bundleMethodCd").val());
        }
    },
    /**
     * 결제수단일괄변경 - 데이터 저장
     */
    updateBundleMethodCd: function () {
        var checkedList = cardPrefixManageGrid.gridView.getCheckedRows(true);
        if (checkedList.length === 0) {
            alert("변경할 데이터를 체크하세요.");
            return false;
        }
        if ($("#bundleMethodCd").val() === "") {
            alert("저장할 결제수단을 선택하세요.");
            return false;
        }
        if(!confirm("일괄변경 하시겠습니까?")) {
            return false;
        }

        // 일괄저장용 데이터 리스트 생성
        var saveData;
        var dataObj = new Object();
        var bundlePrefixList = new Array();
        for (var i = 0; i < checkedList.length; i++) {
            var cardPrefixNo = cardPrefixManageGrid.dataProvider.getValue(checkedList[i], "cardPrefixNo");
            var cardKind = cardPrefixManageGrid.dataProvider.getValue(checkedList[i], "cardKind");
            var methodCd = cardPrefixManageGrid.dataProvider.getValue(checkedList[i], "methodCd");
            var data = new Object();
            data.cardPrefixNo = cardPrefixNo;
            data.cardKind = cardKind;
            data.methodCd = methodCd;
            bundlePrefixList.push(data);
        }
        dataObj.cardPrefixManageDtoList = bundlePrefixList;
        saveData = JSON.stringify(dataObj);

        // 데이터 저장
        CommonAjax.basic({
            url         : '/pg/paymentMethod/saveBundleCardPrefixMethod.json',
            data        : saveData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                cardPrefixManageMain.search();
            }
        });
    }
};

/** Grid Script */
var cardPrefixManageGrid = {
    gridView: new RealGridJS.GridView("cardPrefixManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        cardPrefixManageGrid.initGrid();
        cardPrefixManageGrid.initDataProvider();
        cardPrefixManageGrid.event();
    },
    initGrid: function () {
        cardPrefixManageGrid.gridView.setDataSource(cardPrefixManageGrid.dataProvider);
        cardPrefixManageGrid.gridView.setStyles(cardPrefixManageGridBaseInfo.realgrid.styles);
        cardPrefixManageGrid.gridView.setDisplayOptions(cardPrefixManageGridBaseInfo.realgrid.displayOptions);
        cardPrefixManageGrid.gridView.setColumns(cardPrefixManageGridBaseInfo.realgrid.columns);
        cardPrefixManageGrid.gridView.setOptions(cardPrefixManageGridBaseInfo.realgrid.options);
        cardPrefixManageGrid.gridView.setCheckBar({visible: true, exclusive: false, showAll: false});
        setColumnLookupOption(cardPrefixManageGrid.gridView, "methodNm", $("#schMethodCd"));
        setColumnLookupOption(cardPrefixManageGrid.gridView, "cardDetailKindNm", $("#schCardDetailKind"));
    },
    initDataProvider: function () {
        cardPrefixManageGrid.dataProvider.setFields(cardPrefixManageGridBaseInfo.dataProvider.fields);
        cardPrefixManageGrid.dataProvider.setOptions(cardPrefixManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        cardPrefixManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = cardPrefixManageGrid.dataProvider.getJsonRow(index.dataRow);
            cardPrefixManageMain.drawDetail(data);
        };
    },
    setData: function (dataList) {
        cardPrefixManageGrid.dataProvider.clearRows();
        cardPrefixManageGrid.dataProvider.setRows(dataList);
    }
};