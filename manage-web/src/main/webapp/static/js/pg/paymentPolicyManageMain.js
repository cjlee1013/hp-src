/**
 * 결제정책 관리
 */

// 결제정책 메인 그리드
var paymentPolicyGrid = {
    realGrid: new RealGridJS.GridView("paymentPolicyGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        paymentPolicyGrid.initGrid();
        paymentPolicyGrid.initDataProvider();
        paymentPolicyGrid.event();
    },
    initGrid: function () {
        paymentPolicyGrid.realGrid.setDataSource(paymentPolicyGrid.dataProvider);
        paymentPolicyGrid.realGrid.setStyles(paymentPolicyGridBaseInfo.realgrid.styles);
        paymentPolicyGrid.realGrid.setDisplayOptions(paymentPolicyGridBaseInfo.realgrid.displayOptions);
        paymentPolicyGrid.realGrid.setColumns(paymentPolicyGridBaseInfo.realgrid.columns);
        paymentPolicyGrid.realGrid.setOptions(paymentPolicyGridBaseInfo.realgrid.options);
        paymentPolicyGrid.realGrid.setEditOptions();
        setColumnLookupOption(paymentPolicyGrid.realGrid, "siteType", $("#schSiteType"));
        setColumnLookupOption(paymentPolicyGrid.realGrid, "applyTarget", $("#applyTarget"));
        setColumnLookupOption(paymentPolicyGrid.realGrid, "pgStoreSet", $("#pgStoreSetShow"));
        setColumnLookupOption(paymentPolicyGrid.realGrid, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        paymentPolicyGrid.dataProvider.setFields(paymentPolicyGridBaseInfo.dataProvider.fields);
        paymentPolicyGrid.dataProvider.setOptions(paymentPolicyGridBaseInfo.dataProvider.options);
    },
    event: function () {
        paymentPolicyGrid.realGrid.onDataCellClicked = function (realGrid, index) {
            paymentPolicyMng.drawPaymentPolicyDetail(paymentPolicyGrid.dataProvider.getJsonRow(paymentPolicyGrid.realGrid.getCurrent().dataRow));
        };
    },
    setData: function (dataList) {
        paymentPolicyGrid.dataProvider.clearRows();
        paymentPolicyGrid.dataProvider.setRows(dataList);
    }
};

var paymentPolicyMng = {
    init: function () {
        paymentPolicyMng.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+7d");
        paymentPolicyMng.initApplyDt();
        paymentPolicyMng.managePgGrid();
    },
    /** event binding */
    bindEvent: function () {
        // 결제정책 조회
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!paymentPolicyMng.validSearch()) {
                return false;
            }
            paymentPolicyMng.search();
        });
        // 결제정책 조회 form 초기화
        $("#searchResetBtn").on("click", function () {
            paymentPolicyMng.resetSearchForm();
        });
        // 결제정책 상세 form 초기화
        $("#resetPolicyDetailBtn").on("click", function () {
            paymentPolicyMng.resetDetailForm();
        });
        // 초기화 후 사이트 선택시 결제수단 조회
        $("#siteType").on("change", function () {
            // 관리번호 없을 시 초기화로 판단
            if (!$.jUtil.isEmpty($("#siteType").val()) && $.jUtil.isEmpty($("#policyNo").val())) {
                methodTreeMng.search();
                exposMethodGrid.treeDataProviders.clearRows();
                $("#pgMidManageForm").resetForm();
                paymentPolicyMng.managePgGrid();
            }
        });
        // 결제정책 관리명 글자수확인
        $("#policyNm").calcTextLength("keyup", "#textCountPolicyNm");
        // 적용기간 상시 체크박스 적용
        $("#unlimitedApplyEndDt").bindClick(paymentPolicyMng.unlimitedApplyEndDt);
        // 노출 결제수단 추가
        $("#methodAddBtn").bindClick(paymentPolicyMng.exposMethodAdd);
        // 노출 결제수단 삭제
        $("#methodRemoveBtn").bindClick(paymentPolicyMng.exposMethodRemove);
        // PG상점설정 변경 시
        $("#pgStoreSetShow").bindChange(paymentPolicyMng.managePgGrid);
        // 복사
        $("#copyPolicyDetailBtn").on("click", function () {
            if (!$.jUtil.isEmpty($("#policyNo").val())) {
                alert("복사되었습니다.");
                paymentPolicyMng.copyDetail();
            } else {
                alert("복사할 정책 선택 후 복사하시기 바랍니다.");
                return false;
            }
        });
        // 저장
        $("#savePolicyDetailBtn").on("click", function () {
            // 입력값 validation
            if (!paymentPolicyMng.valid()) {
                return false;
            }
            if (!confirm("저장 하시겠습니까?")) {
                return false;
            }
            paymentPolicyMng.save();
        });
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 1년 초과 검색제한
        if (!deliveryCore.dateValid($("#schFromDt"), $("#schEndDt"), ["isValid", "overOneYearRange"])) {
            return false;
        }

        var schType = $("#schType").val();
        var schKeyword = $("#schKeyword").val();
        // 검색어 자릿수 확인
        if (schType == "policyNm") {
            if (schKeyword != "" && schKeyword.length < 2) {
                alert("관리명은 최소 2자 이상 입력하세요.");
                return false;
            }
        }
        // 검색어 입력제한
        if (schType == "policyNo") {
            if (schKeyword != "" && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
                alert("관리번호는 숫자만 입력해주세요.");
                return false;
            }
        }
        return true;
    },
    /** 결제정책 마스터 조회 */
    search: function () {
        var data = $("#paymentPolicyManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPaymentPolicyMngList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                paymentPolicyGrid.setData(res);
                $("#paymentPolicyManageSearchCnt").html($.jUtil.comma(paymentPolicyGrid.realGrid.getItemCount()));
                paymentPolicyMng.resetDetailForm();
            }
        });
    },
    /** 결제정책 조회 form 초기화 */
    resetSearchForm: function () {
        $("#paymentPolicyManageSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "0", "+7d");
    },
    /** 결제정책 상세 form 초기화 */
    resetDetailForm: function () {
        $("#paymentPolicyInputForm").resetForm();
        $("#siteType").prop("disabled", false);
        $("#applyTarget").prop("disabled", false);
        $("#pgStoreSetShow").prop("disabled", false);
        paymentPolicyMng.initApplyDt();
        paymentPolicyMng.unlimitedApplyEndDt();
        methodTreeGrid.treeDataProviders.clearRows();
        exposMethodGrid.treeDataProviders.clearRows();
        $("#pgMidManageForm").resetForm();
        paymentPolicyMng.managePgGrid();
    },
    /** 기본정보-적용기간 초기화 */
    initApplyDt: function () {
        deliveryCore.initApplyDate("0", "0");
        $("#applyStartDt").datepicker("option", "minDate", 0);
    },
    /** 기본정보-적용기간 상시 체크박스 설정 */
    unlimitedApplyEndDt: function () {
        if ($("#unlimitedApplyEndDt").prop("checked") == true) {
            $("#applyEndDt").val('2999-12-31').prop("disabled", true);
            $("#applyEndDt").datepicker("option", "disabled", true);
        } else {
            $("#applyEndDt").prop("disabled", false);
            $("#applyEndDt").datepicker("option", "disabled", false);
        }
    },
    /** 그리드 선택내용을 기본정보 항목에 세팅 */
    drawPaymentPolicyDetail: function (data) {
        console.log(data);

        $("#policyNo").val(data.policyNo);
        $("#siteType").val(data.siteType).prop("disabled", true);
        $("#policyNm").val(data.policyNm).setTextLength("#textCountPolicyNm");
        $("#applyTarget").val(data.applyTarget).prop("disabled", true);
        $("#priority").val(data.priority);
        $("#applyStartDt").val(data.applyStartDt);
        $("#applyEndDt").val(data.applyEndDt).prop("disabled", false);
        $("#unlimitedApplyEndDt").prop("checked", false);
        $('input:radio[name=useYn]:input[value="'+ data.useYn +'"]').prop("checked", true);
        $('input:radio[name=paymentMethodGuideYn]:input[value="'+ data.paymentMethodGuideYn +'"]').prop("checked", true);

        $('input:radio[name=pointUseYn]:input[value="'+ data.pointUseYn +'"]').prop("checked", true);
        $('input:radio[name=cashReceiptUseYn]:input[value="'+ data.cashReceiptUseYn +'"]').prop("checked", true);
        $('input:radio[name=cardInstallmentUseYn]:input[value="'+ data.cardInstallmentUseYn +'"]').prop("checked", true);
        $('input:radio[name=freeInterestUseYn]:input[value="'+ data.freeInterestUseYn +'"]').prop("checked", true);

        $("#pgStoreSet").val(data.pgStoreSet);
        $("#pgStoreSetShow").val(data.pgStoreSet).prop("disabled", true);
        paymentPolicyMng.managePgGrid();

        methodTreeMng.search();
    },
    /** PG배분율 조회*/
    searchPgDivide: function () {
        CommonAjax.basic({
            url         : '/pg/pgDivide/getPaymentPolicyPgDivideRateList.json?policyNo='+$("#policyNo").val(),
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);

                var pgList = res;

                for (var i=0; i<pgList.length; i++) {
                    var parentMethodCd = pgList[i].parentMethodCd;
                    var divideSeq = pgList[i].divideSeq;
                    var inicisMid = pgList[i].inicisMid;
                    var inicisRate = pgList[i].inicisRate;
                    var tosspgMid = pgList[i].tosspgMid;
                    var tosspgRate = pgList[i].tosspgRate;

                    $("#"+parentMethodCd+"divideSeq").val(divideSeq);
                    $("#"+parentMethodCd+"inicisMid").val(inicisMid);
                    $("#"+parentMethodCd+"inicisRate").val(inicisRate);
                    $("#"+parentMethodCd+"tosspgMid").val(tosspgMid);
                    $("#"+parentMethodCd+"tosspgRate").val(tosspgRate);
                }
            }
        });
    },
    /** 노출 결제수단 추가 */
    exposMethodAdd: function () {
        var selected = methodTreeGrid.treeView.getSelection();
        if ($.jUtil.isEmpty(selected)) {
            alert("추가할 결제수단을 선택하세요.");
            return false;
        }

        var rowIdList = new Array();
        for (var i=selected.startItem; i<=selected.endItem; i++) {
            var rowId = methodTreeGrid.treeView.getDataRow(i);
            rowIdList.push(rowId);
        }

        for (var j = 0; j < rowIdList.length; j++) {
            // 결제수단 그리드 데이터
            var checkedRowId = rowIdList[j];
            var methodName = methodTreeGrid.treeDataProviders.getValue(checkedRowId, "methodName");
            var methodCode = methodTreeGrid.treeDataProviders.getValue(checkedRowId, "methodCode");
            var parentCode = methodTreeGrid.treeDataProviders.getValue(checkedRowId, "parentCode");
            var depth = methodTreeGrid.treeDataProviders.getValue(checkedRowId, "depth");
            var parentRowId = methodTreeGrid.treeDataProviders.getAncestors(checkedRowId);

            // 노출 결제수단 그리드 데이터
            var searchExposParent = exposMethodGrid.treeDataProviders.searchData({fields: ["methodCode"], value: parentCode, wrap: true});

            if (depth == "2") { // 결제수단 추가 시 상위결제수단 없을 경우 같이 추가
                if ($.jUtil.isEmpty(searchExposParent)) {
                    var parentName = methodTreeGrid.treeDataProviders.getValue(parentRowId, "methodName");
                    exposMethodGrid.treeDataProviders.addChildRow(
                        -1,
                        [parentName, parentCode, "0", "1"],
                        1,
                        true
                    );
                    // 추가한 상위결제수단의 rowId 재검색
                    var newParent = exposMethodGrid.treeDataProviders.searchData({fields: ["methodCode"], value: parentCode, wrap: true});
                    exposMethodGrid.treeDataProviders.insertChildRow(
                        newParent.dataRow, // 자식노드 추가할 부모노드의 rowId
                        exposMethodGrid.treeDataProviders.getChildCount(newParent.dataRow), // 부모노드 아래 마지막 자식노드로 추가
                        [methodName, methodCode, parentCode, depth],
                        2, // 추가할 자식노드의 위치
                        false
                    );
                } else {
                    exposMethodGrid.treeDataProviders.insertChildRow(
                        searchExposParent.dataRow, // 자식노드 추가할 부모노드의 rowId
                        exposMethodGrid.treeDataProviders.getChildCount(searchExposParent.dataRow), // 부모노드 아래 마지막 자식노드로 추가
                        [methodName, methodCode, parentCode, depth],
                        2, // 추가할 자식노드의 위치
                        false
                    );
                }
            }

            // 결제수단 목록에서 삭제
            if (depth == "2") {
                methodTreeGrid.treeDataProviders.removeRow(checkedRowId);
            }
            exposMethodGrid.treeView.expandAll();
        }
        paymentPolicyMng.managePgGrid();
    },
    /** 노출 결제수단 삭제 */
    exposMethodRemove: function () {
        var exposSelected = exposMethodGrid.treeView.getSelection();
        if ($.jUtil.isEmpty(exposSelected)) {
            alert("삭제할 결제수단을 선택하세요.");
            return false;
        }

        var exposRowIdList = new Array();
        for (var i=exposSelected.startItem; i<=exposSelected.endItem; i++) {
            var exposRowId = exposMethodGrid.treeView.getDataRow(i);
            exposRowIdList.push(exposRowId);
        }

        for (var j = 0; j < exposRowIdList.length; j++) {
            // 노출 결제수단 그리드 데이터
            var checkedExposRowId = exposRowIdList[j];
            var methodName = exposMethodGrid.treeDataProviders.getValue(checkedExposRowId, "methodName");
            var methodCode = exposMethodGrid.treeDataProviders.getValue(checkedExposRowId, "methodCode");
            var parentCode = exposMethodGrid.treeDataProviders.getValue(checkedExposRowId, "parentCode");
            var depth = exposMethodGrid.treeDataProviders.getValue(checkedExposRowId, "depth");
            var parentRowId = exposMethodGrid.treeDataProviders.getAncestors(checkedExposRowId);

            // 결제수단 그리드 데이터
            var searchMethodParent = methodTreeGrid.treeDataProviders.searchData({fields: ["methodCode"], value: parentCode, wrap: true});

            if (depth == "2") { // 결제수단 추가 시 상위결제수단 없을 경우 같이 추가
                if ($.jUtil.isEmpty(searchMethodParent)) {
                    var parentName = exposMethodGrid.treeDataProviders.getValue(parentRowId, "methodName");
                    methodTreeGrid.treeDataProviders.addChildRow(
                        -1,
                        [parentName, parentCode, "0", "1"],
                        1,
                        true
                    );
                    // 추가한 상위결제수단의 rowId 재검색
                    var newParent = methodTreeGrid.treeDataProviders.searchData({fields: ["methodCode"], value: parentCode, wrap: true});
                    methodTreeGrid.treeDataProviders.insertChildRow(
                        newParent.dataRow,
                        methodTreeGrid.treeDataProviders.getChildCount(newParent.dataRow),
                        [methodName, methodCode, parentCode, depth],
                        2,
                        false
                    );
                } else {
                    methodTreeGrid.treeDataProviders.insertChildRow(
                        searchMethodParent.dataRow,
                        methodTreeGrid.treeDataProviders.getChildCount(searchMethodParent.dataRow),
                        [methodName, methodCode, parentCode, depth],
                        2,
                        false
                    );
                }
            }

            // 노출 결제수단 목록에서 삭제
            // 상위결제수단은 자식노드가 없을 경우만 삭제, 마지막 자식노드 삭제 시 부모노드도 삭제
            if (depth == "1") {
                if (exposMethodGrid.treeDataProviders.getChildCount(checkedExposRowId) < 1) {
                    exposMethodGrid.treeDataProviders.removeRow(checkedExposRowId);
                    $("#"+methodCode+"inicisMid").val("");
                    $("#"+methodCode+"tosspgMid").val("");
                    $("#"+methodCode+"inicisRate").val("0");
                    $("#"+methodCode+"tosspgRate").val("0");
                }
            } else {
                exposMethodGrid.treeDataProviders.removeRow(checkedExposRowId);

                if (exposMethodGrid.treeDataProviders.getChildCount(parentRowId) < 1) {
                    exposMethodGrid.treeDataProviders.removeRow(parentRowId);
                    $("#"+parentCode+"inicisMid").val("");
                    $("#"+parentCode+"tosspgMid").val("");
                    $("#"+parentCode+"inicisRate").val("0");
                    $("#"+parentCode+"tosspgRate").val("0");
                }
            }
        }
        paymentPolicyMng.managePgGrid();
    },
    /** PG상점설정 후 그리드 동적관리 */
    managePgGrid: function () {
        $("#pgStoreSet").val($("#pgStoreSetShow").val());
        // 해당없음
        if ($("#pgStoreSetShow").val() == "N") {
            $("#pgMidManageForm").resetForm();
            $("[id$='Mid']").prop("disabled", true);
            $("[id$='Rate']").prop("disabled", true);
        // PG배분
        } else if ($("#pgStoreSetShow").val() == "P") {
            $("[id$='Mid']").prop("disabled", true);
            $("[id$='Rate']").prop("disabled", true);
            var rowCount = exposMethodGrid.treeDataProviders.getRowCount();
            for (var i=0; i<rowCount; i++) {
                var rowId = exposMethodGrid.treeView.getDataRow(i);
                var parentCode = exposMethodGrid.treeDataProviders.getValue(rowId, "parentCode");
                $("[id^='"+parentCode+"']").prop("disabled", false);
            }
        }
    },
    /** 복사하기 */
    copyDetail: function () {
        $("#policyNo").val("");
        $("#siteType").prop("disabled", false);
        $("#applyTarget").prop("disabled", false);
        $("#pgStoreSetShow").prop("disabled", false);
        paymentPolicyMng.initApplyDt();
        paymentPolicyMng.unlimitedApplyEndDt();
    },
    /** 저장 */
    save: function () {
        $("#policyNo").prop("disabled", false);
        $("#siteType").prop("disabled", false);
        $("#applyTarget").prop("disabled", false);
        $("#applyEndDt").prop("disabled", false);
        var saveData;
        var formData = new Object();
        var methodList = new Array();
        var pgDivideList = new Array();

        var iter = exposMethodGrid.treeDataProviders.getRowCount();
        for (var i=0; i<iter; i++) {
            var rowId = exposMethodGrid.treeView.getDataRow(i);
            var depth = exposMethodGrid.treeDataProviders.getValue(rowId, "depth");

            if (depth > 1) {
                var parentMethod = exposMethodGrid.treeDataProviders.getValue(rowId, "parentCode");
                var adminMethodCd = exposMethodGrid.treeDataProviders.getValue(rowId, "methodCode");
                var lastIndex = adminMethodCd.lastIndexOf("_");
                var methodCd = null;

                if (lastIndex !== "-1") {
                    if (parentMethod === adminMethodCd.substring(0, lastIndex)) {
                        methodCd = parentMethod;
                    } else {
                        methodCd = adminMethodCd;
                    }
                } else {
                    methodCd = adminMethodCd;
                }

                var methodNm = exposMethodGrid.treeDataProviders.getValue(rowId, "methodName");
                var data = new Object();
                data.policyNo = $("#policyNo").val();
                data.methodCd = methodCd;
                methodList.push(data);

                if ($("#pgStoreSetShow").val() == "P") {
                    var pgData = new Object();
                    pgData.divideNm = $("#policyNm").val();
                    pgData.parentMethodCd = parentMethod;
                    pgData.methodCd = methodCd;
                    pgData.methodNm = methodNm;
                    pgData.inicisMid = $("#"+parentMethod+"inicisMid").val();
                    pgData.inicisRate = $("#"+parentMethod+"inicisRate").val();
                    pgData.tosspgMid = $("#"+parentMethod+"tosspgMid").val();
                    pgData.tosspgRate = $("#"+parentMethod+"tosspgRate").val();
                    pgData.priority = $("#priority").val();
                    pgDivideList.push(pgData);
                }
            }
        }

        if (!$.jUtil.isEmpty(methodList)) {
            formData = JSON.parse(_F.getFormDataByJson($("#paymentPolicyInputForm")));
            formData.paymentPolicyMethodDtoList = methodList;
            if (!$.jUtil.isEmpty(pgDivideList)) {
                formData.pgDivideManageDtoList = pgDivideList;
            }
            saveData = JSON.stringify(formData);
        } else {
            saveData = _F.getFormDataByJson($("#paymentPolicyInputForm"));
        }
        $("#policyNo").prop("disabled", true);

        CommonAjax.basic({
            url         : '/pg/pgDivide/savePaymentPolicyMng.json',
            data        : saveData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                paymentPolicyMng.search();
            }
        });
    },
    /** 저장 전 입력값 validation */
    valid: function () {
        if ($.jUtil.isEmpty($("#siteType").val())) {
            alert("사이트를 선택하세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#policyNm").val())) {
            alert("관리명을 입력하세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#applyTarget").val())) {
            alert("적용대상을 선택하세요.");
            return false;
        } else if ($.jUtil.isEmpty($("#priority").val())) {
            alert("우선순위를 입력하세요.");
            return false;
        }
        if (!$.jUtil.isAllowInput($("#priority").val(), ['NUM'])) {
            alert("우선순위는 숫자만 입력해주세요.");
            return false;
        }
        var priorityVal = $("#priority").val();
        if (priorityVal.length > 3 || Number(priorityVal) > 999) {
            alert("우선순위는 최대 999까지 입력가능합니다.");
            return false;
        }
        if ($("#pgStoreSetShow").val() == "P") {
            var rowCount = exposMethodGrid.treeDataProviders.getRowCount();
            for (var i=0; i<rowCount; i++) {
                var rowId = exposMethodGrid.treeView.getDataRow(i);
                var parentCode = exposMethodGrid.treeDataProviders.getValue(rowId, "parentCode");
                var depth = exposMethodGrid.treeDataProviders.getValue(rowId, "depth");

                if (depth > 1) {
                    var inicisMid = $("#"+parentCode+"inicisMid").val();
                    var tosspgMid = $("#"+parentCode+"tosspgMid").val();
                    var inicisRate = $("#"+parentCode+"inicisRate").val();
                    var tosspgRate = $("#"+parentCode+"tosspgRate").val();

                    if ($.jUtil.isEmpty(inicisMid) || $.jUtil.isEmpty(tosspgMid)) {
                        alert("상점ID를 입력해주세요.");
                        return false;
                    }
                    if ($.jUtil.isEmpty(inicisRate) || $.jUtil.isEmpty(tosspgRate)) {
                        alert("배분율을 입력해주세요.");
                        return false;
                    }
                    if (!$.jUtil.isAllowInput(inicisRate, ['NUM']) || !$.jUtil.isAllowInput(tosspgRate, ['NUM'])) {
                        alert("배분율은 숫자만 입력해주세요.");
                        return false;
                    }
                    if (Number(inicisRate) > 100 || Number(tosspgRate) > 100) {
                        alert("배분율은 100%를 넘을 수 없습니다.");
                        return false;
                    }
                    var numberSum = Number(inicisRate) + Number(tosspgRate);
                    if (numberSum !== 100) {
                        alert("전체 배분율의 합은 100 미만/초과될 수 없습니다. 다시 확인해주세요.");
                        return false;
                    }
                }
            }
        }
        return true;
    },
};

// 결제수단 tree 그리드
var methodTreeGrid = {
    treeView: new RealGridJS.TreeView("methodTreeGrid"),
    treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
    init: function () {
        methodTreeGrid.initGrid();
        methodTreeGrid.initDataProvider();
        methodTreeGrid.event();
    },
    initGrid: function () {
        methodTreeGrid.treeView.setDataSource(methodTreeGrid.treeDataProviders);

        methodTreeGrid.treeView.setStyles(methodTreeGridBaseInfo.realgrid.styles);
        methodTreeGrid.treeView.setDisplayOptions(methodTreeGridBaseInfo.realgrid.displayOptions);
        methodTreeGrid.treeView.setColumns(methodTreeGridBaseInfo.realgrid.columns);
        methodTreeGrid.treeView.setOptions(methodTreeGridBaseInfo.realgrid.options);
        methodTreeGrid.treeView.setTreeOptions({lineVisible: false});
        methodTreeGrid.treeView.setSelectOptions({style: "rows"});
        methodTreeGrid.treeView.setColumnProperty("methodName", "header", {text: "결제수단"});
    },
    initDataProvider: function () {
        methodTreeGrid.treeDataProviders.setFields(methodTreeGridBaseInfo.dataProvider.fields);
        methodTreeGrid.treeDataProviders.setOptions(methodTreeGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 결제수단 그리드 expand 시 (event 순서 제어를 위해 데이터 조회 후 expand 발생)
        methodTreeGrid.treeView.onTreeItemExpanded = function () {
            // 노출 결제수단 조회 및 세팅
            if (!$.jUtil.isEmpty($("#policyNo").val())) {
                exposMethodMng.search();
            }
        }
    },
    setData: function (dataList) {
        methodTreeGrid.treeDataProviders.clearRows();
        methodTreeGrid.treeDataProviders.fillJsonData({"rows": dataList}, {rows: "rows", icon: "parentCode"});
    }
};

var methodTreeMng = {
    search: function () {
        CommonAjax.basic({
            url: '/pg/paymentMethod/getPaymentMethodTree.json?siteType='+$("#siteType").val(),
            data: null,
            method: "GET",
            callbackFunc: function (res) {
                methodTreeGrid.setData(res);
                methodTreeGrid.treeView.expandAll();
            }
        });
    },
};

// 노출 결제수단 그리드
var exposMethodGrid = {
    treeView: new RealGridJS.TreeView("exposMethodGrid"),
    treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
    init: function () {
        exposMethodGrid.initGrid();
        exposMethodGrid.initDataProvider();
        exposMethodGrid.event();
    },
    initGrid: function () {
        exposMethodGrid.treeView.setDataSource(exposMethodGrid.treeDataProviders);

        exposMethodGrid.treeView.setStyles(exposMethodGridBaseInfo.realgrid.styles);
        exposMethodGrid.treeView.setDisplayOptions(exposMethodGridBaseInfo.realgrid.displayOptions);
        exposMethodGrid.treeView.setColumns(exposMethodGridBaseInfo.realgrid.columns);
        exposMethodGrid.treeView.setOptions(exposMethodGridBaseInfo.realgrid.options);
        exposMethodGrid.treeView.setTreeOptions({lineVisible: false});
        exposMethodGrid.treeView.setSelectOptions({style: "rows"});
        exposMethodGrid.treeView.setColumnProperty("methodName", "header", {text: "노출결제수단"});
    },
    initDataProvider: function () {
        exposMethodGrid.treeDataProviders.setFields(exposMethodGridBaseInfo.dataProvider.fields);
        exposMethodGrid.treeDataProviders.setOptions(exposMethodGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 노출 결제수단 그리드 expand 시 (event 순서 제어를 위해 데이터 조회 후 expand 발생)
        exposMethodGrid.treeView.onTreeItemExpanded = function () {
            // 결제수단/노출결제수단 데이터 sync
            exposMethodMng.syncMethodTree();
            // PG배분율 조회
            if ($("#pgStoreSetShow").val() == "P" && !$.jUtil.isEmpty($("#policyNo").val())) {
                paymentPolicyMng.searchPgDivide();
            }
        }
    },
    setData: function (dataList) {
        exposMethodGrid.treeDataProviders.clearRows();
        exposMethodGrid.treeDataProviders.fillJsonData({"rows": dataList}, {rows: "rows", icon: "parentCode"});
    }
};

var exposMethodMng = {
    search: function () {
        CommonAjax.basic({
            url: '/pg/pgDivide/getPaymentPolicyMethodTree.json?siteType='+$("#siteType").val()+'&policyNo='+$("#policyNo").val(),
            data: null,
            method: "GET",
            callbackFunc: function (res) {
                exposMethodGrid.setData(res);
                exposMethodGrid.treeView.expandAll();
                paymentPolicyMng.managePgGrid();
            }
        });
    },
    /** 노출결제수단 조회 및 세팅 후 결제수단 목록에서 제외처리 */
    syncMethodTree: function () {
        var rowCount = exposMethodGrid.treeDataProviders.getRowCount();
        for (var i=0; i<rowCount; i++) {
            var rowId = exposMethodGrid.treeView.getDataRow(i);
            var searchCode = exposMethodGrid.treeDataProviders.getValue(rowId, "methodCode");
            var removeTarget = methodTreeGrid.treeDataProviders.searchData({fields: ["methodCode"], value: searchCode, wrap: true});

            if (!$.jUtil.isEmpty(removeTarget)) {
                var removeTargetDepth = methodTreeGrid.treeDataProviders.getValue(removeTarget.dataRow, "depth");
                if (removeTargetDepth != "1") {
                    methodTreeGrid.treeDataProviders.removeRow(removeTarget.dataRow);
                }
            }
        }
    },
};