/**
 * PG 승인대사 조회
 */

// PG사별 승인 합계내역 그리드
var pgCompareApprovalSumGrid = {
    realGrid: new RealGridJS.GridView("pgCompareApprovalSumGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pgCompareApprovalSumGrid.initGrid();
        pgCompareApprovalSumGrid.initDataProvider();
        pgCompareApprovalSumGrid.event();
    },
    initGrid: function () {
        pgCompareApprovalSumGrid.realGrid.setDataSource(pgCompareApprovalSumGrid.dataProvider);
        pgCompareApprovalSumGrid.realGrid.setStyles(pgCompareApprovalSumGridBaseInfo.realgrid.styles);
        pgCompareApprovalSumGrid.realGrid.setDisplayOptions(pgCompareApprovalSumGridBaseInfo.realgrid.displayOptions);
        pgCompareApprovalSumGrid.realGrid.setColumns(pgCompareApprovalSumGridBaseInfo.realgrid.columns);
        pgCompareApprovalSumGrid.realGrid.setOptions(pgCompareApprovalSumGridBaseInfo.realgrid.options);
        pgCompareApprovalSumGrid.realGrid.setEditOptions();


        pgCompareApprovalSumGrid.realGrid.setOptions({ summaryMode : "statistical" });
        pgCompareApprovalSumGrid.realGrid.setFooter({ visible: true, styles: { background: "#11ff0000", textAlignment: "far" }} );
        pgCompareApprovalSumGrid.realGrid.setColumnProperty(pgCompareApprovalSumGrid.realGrid.getColumns()[0], "footer", { text: "합계", styles: { textAlignment: "center" } });

    },
    initDataProvider: function () {
        pgCompareApprovalSumGrid.dataProvider.setFields(pgCompareApprovalSumGridBaseInfo.dataProvider.fields);
        pgCompareApprovalSumGrid.dataProvider.setOptions(pgCompareApprovalSumGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // pgCompareApprovalSumGrid.realGrid.onDataCellClicked = function (realGrid, index) {
        //     paymentPolicyMng.drawPaymentPolicyDetail(pgCompareApprovalSumGrid.dataProvider.getJsonRow(pgCompareApprovalSumGrid.realGrid.getCurrent().dataRow));
        // };
    },
    setData: function (dataList) {
        pgCompareApprovalSumGrid.dataProvider.clearRows();
        pgCompareApprovalSumGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        setTimeout(function () {
            if (pgCompareApprovalSumGrid.realGrid.getItemCount() == 0) {
                alert("없습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "PG승인대사_PG사별승인합계내역_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            pgCompareApprovalSumGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }, 1500);
    }
};

// 승인 차이내역 그리드
var pgCompareApprovalDiffGrid = {
    realGrid: new RealGridJS.GridView("pgCompareApprovalDiffGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pgCompareApprovalDiffGrid.initGrid();
        pgCompareApprovalDiffGrid.initDataProvider();
        pgCompareApprovalDiffGrid.event();
    },
    initGrid: function () {
        pgCompareApprovalDiffGrid.realGrid.setDataSource(pgCompareApprovalDiffGrid.dataProvider);
        pgCompareApprovalDiffGrid.realGrid.setStyles(pgCompareApprovalDiffGridBaseInfo.realgrid.styles);
        pgCompareApprovalDiffGrid.realGrid.setDisplayOptions(pgCompareApprovalDiffGridBaseInfo.realgrid.displayOptions);
        pgCompareApprovalDiffGrid.realGrid.setColumns(pgCompareApprovalDiffGridBaseInfo.realgrid.columns);
        pgCompareApprovalDiffGrid.realGrid.setOptions(pgCompareApprovalDiffGridBaseInfo.realgrid.options);
        pgCompareApprovalDiffGrid.realGrid.setEditOptions();
    },
    initDataProvider: function () {
        pgCompareApprovalDiffGrid.dataProvider.setFields(pgCompareApprovalDiffGridBaseInfo.dataProvider.fields);
        pgCompareApprovalDiffGrid.dataProvider.setOptions(pgCompareApprovalDiffGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // pgCompareApprovalSumGrid.realGrid.onDataCellClicked = function (realGrid, index) {
        //     paymentPolicyMng.drawPaymentPolicyDetail(pgCompareApprovalSumGrid.dataProvider.getJsonRow(pgCompareApprovalSumGrid.realGrid.getCurrent().dataRow));
        // };
    },
    setData: function (dataList) {
        pgCompareApprovalDiffGrid.dataProvider.clearRows();
        pgCompareApprovalDiffGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        setTimeout(function () {
            if (pgCompareApprovalDiffGrid.realGrid.getItemCount() == 0) {
                alert("없습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "PG승인대사_차이내역_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

            pgCompareApprovalDiffGrid.realGrid.exportGrid({
                type: "excel",
                target: "local",
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }, 1500);
    }
};

var pgCompareApprovalMain = {
    init: function () {
        pgCompareApprovalMain.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1", "-1d");
    },
    /** event binding */
    bindEvent: function () {
        // 결제정책 조회
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!pgCompareApprovalMain.validSearch()) {
                return false;
            }
            pgCompareApprovalMain.search();
        });
        // 결제정책 조회 form 초기화
        $("#searchResetBtn").on("click", function () {
            pgCompareApprovalMain.resetSearchForm();
        });

        $("#sumExcelDownBtn").on("click", function () {
            pgCompareApprovalSumGrid.excelDownload();
        });
        $("#diffExcelDownBtn").on("click", function () {
            pgCompareApprovalDiffGrid.excelDownload();
        });
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 1년 초과 검색제한
        if (!deliveryCore.dateValid($("#schFromDt"), $("#schEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        }
        return true;
    },
    /** 결제정책 마스터 조회 */
    search: function () {
        var data = $("#pgCompareApprovalSearchForm").serialize();
        CommonAjax.basic({
            url         : '/pg/pgCompareApproval/getPgCompareApprovalSumList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                pgCompareApprovalSumGrid.setData(res);
                pgCompareApprovalSumGrid.realGrid.setStyles({
                   body: {
                       cellDynamicStyles: function (grid, index, value) {
                           var columns = ["approvalDiffTotAmt", "cancelDiffTotAmt", "diffSumAmt"];
                           if (columns.indexOf(index.fieldName) > -1) {
                               if (value != 0) {
                                   return {
                                       foreground: "#ffff0000"
                                   }
                               }
                           }
                       }
                   }
                });

                var stringCol = [ "approvalTotAmt", "cancelTotAmt", "sumAmt",
                                  "pgApprovalTotAmt", "pgCancelTotAmt", "pgSumAmt",
                                  "approvalDiffTotAmt", "cancelDiffTotAmt", "diffSumAmt"
                ];
                pgCompareApprovalMain.setGridFooterData(pgCompareApprovalSumGrid.realGrid, stringCol);
                pgCompareApprovalMain.searchDiff(data);
            }
        });
    },

    searchDiff: function (data) {
        CommonAjax.basic({
            url         : '/pg/pgCompareApproval/getPgCompareApprovalDiffList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                pgCompareApprovalDiffGrid.setData(res);
            }
        });
    },

    setGridFooterData : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "footer", {
                styles: { textAlignment: "far", "numberFormat": "#,##0" },
                expression: "sum" });
        }
    },

    /** 결제정책 조회 form 초기화 */
    resetSearchForm: function () {
        $("#pgCompareApprovalSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1", "-1d");

    },
    setSearchDate: function (dateTypCd) {
        if (dateTypCd === "D") {
            deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1d", "-1d");
        } else if (dateTypCd === "W") {
            deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1w", "-1d");
        } else if (dateTypCd === "M") {
            deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-1m", "-1d");
        }
    }
};

