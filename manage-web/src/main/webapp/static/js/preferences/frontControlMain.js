/**
 * 메세지관리 > 발송관리 > 프론트 구성요소 컨트롤
 */
const frontControlMain = {
    /**
     * 초기화
     */
    init: function () {
        this.event();
        this.initLiveSlotOn();
    },
    /**
     * 이벤트 바인딩
     */
    event: function () {
        $("input[name='liveSlotOn'][value='true']").on('click', function () {
            frontControlMain.setLiveSlotOn();
        });
        $("input[name='liveSlotOn'][value='false']").on('click', function () {
            frontControlMain.setLiveSlotOn();
        });
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initLiveSlotOn: function () {
        let liveSlotOnValue = frontControlMain.getLiveSlotOn();
        frontControlMain.displayLiveSlotOn(liveSlotOnValue);
    },

    displayLiveSlotOn: function (liveSlotOnValue) {
        if (liveSlotOnValue) {
            $("input[name='liveSlotOn'][value='true']").prop("checked", true);
        } else {
            $("input[name='liveSlotOn'][value='false']").prop("checked", true);
        }
    },
    getLiveSlotOn: function () {
        CommonAjax.basic({
            url: '/preferences/front/control/getLiveSlotOn.json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnCode == "SUCCESS") {
                    let liveSlotOnValue = res.data.liveSlotOn;
                    frontControlMain.displayLiveSlotOn(liveSlotOnValue);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 페이지를 새로 고쳐주세요.");
                }
            },
            error: function(e) {
                alert("오류가 발생하였습니다! 잠시 후에 페이지를 새로 고쳐주세요.");
            }
        });
    },
    setLiveSlotOn: function () {
        CommonAjax.basic({
            url: '/preferences/front/control/setLiveSlotOn.json',
            data: {"liveSlotOn" : $("input[name='liveSlotOn']:checked").val()},
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                if (res.returnCode == "SUCCESS") {
                    let liveSlotOnValue = res.data.liveSlotOn;
                    frontControlMain.displayLiveSlotOn(liveSlotOnValue);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                    frontControlMain.initLiveSlotOn();
                }
            },
            error: function(e) {
                alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                frontControlMain.initLiveSlotOn();
            }
        });
    }
};

$(document).ready(function () {
    frontControlMain.init();
});
