/**
 * 카드사할인관리
 */
$(document).ready(function() {
    cardDiscountGrid.init();
    cardDiscountApplyGrid.init();
    cardDiscountSearch.init();
    cardDiscount.init();
    CommonAjaxBlockUI.global();
});

var cardDiscountGrid = {
    realGrid : new RealGridJS.GridView("cardDiscountGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        cardDiscountGrid.initGrid();
        cardDiscountGrid.initDataProvider();
        cardDiscountGrid.event();
    },
    initGrid : function() {
        cardDiscountGrid.realGrid.setDataSource(cardDiscountGrid.dataProvider);

        cardDiscountGrid.realGrid.setStyles(cardDiscountGridBaseInfo.realgrid.styles);
        cardDiscountGrid.realGrid.setDisplayOptions(cardDiscountGridBaseInfo.realgrid.displayOptions);
        cardDiscountGrid.realGrid.setColumns(cardDiscountGridBaseInfo.realgrid.columns);
        cardDiscountGrid.realGrid.setOptions(cardDiscountGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(cardDiscountGrid.realGrid, "useYn", useYnJson);
        setColumnLookupOptionForJson(cardDiscountGrid.realGrid, "storeType", storeTypeJson);
    },
    initDataProvider : function() {
        cardDiscountGrid.dataProvider.setFields(cardDiscountGridBaseInfo.dataProvider.fields);
        cardDiscountGrid.dataProvider.setOptions(cardDiscountGridBaseInfo.dataProvider.options);
    },
    event : function() {
        cardDiscountGrid.realGrid.onDataCellClicked = function(gridView, index) {
            cardDiscount.getCardDiscountDetail(cardDiscountGrid.dataProvider.getJsonRow(cardDiscountGrid.realGrid.getCurrent().dataRow).discountNo);
        };
    },
    setData : function(dataList) {
        cardDiscountGrid.dataProvider.clearRows();
        cardDiscountGrid.dataProvider.setRows(dataList);
    },
    excelDownload : function() {
        if (cardDiscountGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "카드상품할인관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        cardDiscountGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var cardDiscountApplyGrid = {
    realGrid : new RealGridJS.GridView("discountApplyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        cardDiscountApplyGrid.initGrid();
        cardDiscountApplyGrid.initDataProvider();
        cardDiscountApplyGrid.event();
    },
    initGrid : function() {
        cardDiscountApplyGrid.realGrid.setDataSource(cardDiscountApplyGrid.dataProvider);

        cardDiscountApplyGrid.realGrid.setStyles(cardDiscountApplyGridBaseInfo.realgrid.styles);
        cardDiscountApplyGrid.realGrid.setDisplayOptions(cardDiscountApplyGridBaseInfo.realgrid.displayOptions);
        cardDiscountApplyGrid.realGrid.setColumns(cardDiscountApplyGridBaseInfo.realgrid.columns);
        cardDiscountApplyGrid.realGrid.setColumns(cardDiscountApplyGridBaseInfo.realgrid.columns);
        var options = cardDiscountApplyGridBaseInfo.realgrid.options;
        options.hideDeletedRows = true;
        cardDiscountApplyGrid.realGrid.setOptions(options);
    },
    initDataProvider : function() {
        cardDiscountApplyGrid.dataProvider.setFields(cardDiscountApplyGridBaseInfo.dataProvider.fields);
        var options = cardDiscountApplyGridBaseInfo.dataProvider.options;
        options.softDeleting = true;
        cardDiscountApplyGrid.dataProvider.setOptions(options);
    },
    event : function() {
        /**
         * 체크한 데이터 삭제
         **/
        $("#deleteApplyScope").on("click", function() {
            cardDiscountApplyGrid.removeCheckData();
        });
    },
    setData : function(dataList) {
        cardDiscountApplyGrid.dataProvider.clearRows();
        cardDiscountApplyGrid.clearAllFilter();
        cardDiscountApplyGrid.dataProvider.setRows(dataList);
    },
    // 그리드 데이터 append
    appendData : function(data) {
        var currentDatas = cardDiscountApplyGrid.dataProvider.getJsonRows(0, -1);

        var createdDataList = cardDiscountApplyGrid.dataProvider.getStateRows("created");
            createdDataList = createdDataList.concat(cardDiscountApplyGrid.dataProvider.getStateRows("none"));

        var createdDatas = new Array();
        createdDataList.forEach(function(row, index) {
            var dataRow = cardDiscountApplyGrid.dataProvider.getJsonRow(row);
            createdDatas.push(dataRow);
        });

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        if (!currentDatas !== undefined && currentDatas.length > 0) {

            cardDiscountApplyGrid.dataProvider.beginUpdate();
            try {
                data.forEach(function(value, index, array1) {

                    if (currentDatas.findIndex(function(currentData) { return currentData.applyCd === value.applyCd; }) == -1) {
                        cardDiscountApplyGrid.dataProvider.addRow(value);
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ['applyCd'],
                            values: [value.applyCd]
                        };

                        var _row = cardDiscountApplyGrid.dataProvider.searchDataRow(options);
                        var rowStatus = cardDiscountApplyGrid.dataProvider.getRowState(_row);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (rowStatus == 'deleted') {

                            console.log(value);
                            var values = [{
                                applyCd: value.applyCd,
                                discountType: value.discountType,
                                discountRate: value.discountRate,
                                discountPrice: value.discountPrice,
                                couponCd: value.couponCd,
                                issueStartDt: value.issueStartDt,
                                issueEndDt: value.issueEndDt,
                                groupCd: value.groupCd
                            }];
                            cardDiscountApplyGrid.dataProvider.updateRows(_row, values);

                            cardDiscountApplyGrid.dataProvider.setRowState(_row, 'updated');

                        } else if (rowStatus == "createAndDeleted") {
                            if (createdDatas.findIndex(function(createdData) { return createdData.applyCd === value.applyCd; }) == -1) {
                                cardDiscountApplyGrid.dataProvider.addRow(value);
                            }
                        }
                        /*
                         // 주석을 풀고 적용하게 되면 저장시에 데이터 추가 필요
                            else  {
                            var values = [{
                                discountType: value.discountType,
                                discountRate: value.discountRate,
                                discountPrice: value.discountPrice,
                                couponCd: value.couponCd
                            }];
                            cardDiscountApplyGrid.dataProvider.updateRows(_row, values);
                        }*/
                    }
                });
            } finally {
                cardDiscountApplyGrid.dataProvider.endUpdate(true);
            }
        } else {
            cardDiscountApplyGrid.dataProvider.addRows(data);
        }
        cardDiscountApplyGrid.discountApplyGridSearchClear();
        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());
    },
    removeCheckData : function() {
        var check = cardDiscountApplyGrid.realGrid.getCheckedRows(true);
        if (check.length == 0) {
            alert("삭제할 대상을 선택해 주세요.");
            return;
        }
        cardDiscountApplyGrid.dataProvider.removeRows(check, false);
        cardDiscountApplyGrid.discountApplyGridSearchClear();
    },
    discountApplyGridSearchClear : function() {
        $('#discountApplyGridSearchValue').val('');

        cardDiscountApplyGrid.clearAllFilter();
    },
    filter : function(searchColumnName, searchValue) {
        /* 사용하는 필터 제거 */
        cardDiscountApplyGrid.clearAllFilter();

        if (!searchColumnName || !searchValue)
            return;

        var filter = {};
        var filterArr = [];
        var activeFilterArr = [];

        switch(searchColumnName) {
            case "applyCd":
                $.each(searchValue, function(i) {
                    var value = searchValue[i];
                    filter = {name: value, criteria: "value = '" + value + "'"};
                    filterArr.push(filter);
                    activeFilterArr.push(value);
                });
                break;

            case "applyNm":
                filter = {name: searchValue, criteria: "value like '" + searchValue + "%'"};
                filterArr.push(filter);
                activeFilterArr.push(searchValue);
                break;
        }

        cardDiscountApplyGrid.realGrid.setColumnFilters(searchColumnName, filterArr);
        cardDiscountApplyGrid.realGrid.activateColumnFilters(searchColumnName, activeFilterArr, true);
    },
    clearAllFilter : function() {
        cardDiscountApplyGrid.realGrid.clearColumnFilters("applyCd");
        cardDiscountApplyGrid.realGrid.clearColumnFilters("applyNm");

        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());
    },
    excelDownload : function() {
        if (cardDiscountApplyGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "즉시할인_상품_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        cardDiscountApplyGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var cardDiscountSearch = {
    schDate : null,
    schDiscountKind : "CARD",
	init : function() {
        cardDiscountSearch.event();
        cardDiscountSearch.initCardDiscountSearch();
	},
    event : function() {
        $("#getCardDiscountSearchBtn").on("click", function() {
            cardDiscountSearch.getCardDiscountSearch();
        });

        $("#initCardDiscountSearchBtn").on("click", function() {
            cardDiscountSearch.initCardDiscountSearch();
        });

        $("#excelDownloadSearchListBtn").on("click", function() {
            cardDiscountGrid.excelDownload();
        });

        $("#setTodayBtn").on("click", function() {
            cardDiscountSearch.initSearchDate("0");
        });

        $("#setOneWeekBtn").on("click", function() {
            cardDiscountSearch.initSearchDate("-1w");
        });

        $("#setOneMonthBtn").on("click", function() {
            cardDiscountSearch.initSearchDate("-1m");
        });
    },
    //즉시할인 조회 검색 폼 - 초기화
    initCardDiscountSearch : function() {
        $("#cardDiscountSearchForm").each(function() {
            this.reset();
        });

        //조회 일자 초기화
        cardDiscountSearch.initSearchDate("0");
    },
    //조회 일자 초기화
    initSearchDate : function(flag) {
        cardDiscountSearch.schDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        cardDiscountSearch.schDate.setEndDate("0");
        cardDiscountSearch.schDate.setStartDate(flag);
    },
    // 내용 검증
    validateCheck : function() {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");

        if (!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 1개월 내로 검색 가능
        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 1) {
            alert("1개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 검색어 자릿수 확인
        if ($("#schType").val() == "discountNm") {
            if ($.jUtil.isNotEmpty($("#schValue").val()) && $("#schValue").val().length < 2) {
                alert("2글자 이상 검색 하세요.");
                return false;
            }
        }

        // 다중검색 조건 확인
        if ($("#schType").val() == "discountNo") {
            var replaceText = $("#schValue").val().replace(/\s/gi, "");
            $("#schValue").val(replaceText);

            if (!/^[0-9,]*$/.test($("#schValue").val())) {
                alert("즉시할인번호로 다중 검색 시 즉시할인번호 사이에 구분자 콤마(',')를 입력해주세요.");
                return;
            }

            var arrSchValueList = $("#schValue").val().split(",");

            if (arrSchValueList.length > 100) {
                alert("즉시할인번호는 최대 100개까지 검색 가능합니다.");
                return false;
            }
        }

        return true;
    },
    //카드사할인조회
    getCardDiscountSearch : function(cardDiscountNo) {
        // 조회 조건 확인
        if (!cardDiscountSearch.validateCheck()) return;

        $("#cardDiscountSearchCnt").html("0");
        cardDiscount.initCardDiscountAll();

        var searchFormData = $("form[name=cardDiscountSearchForm]").serializeArray();
        searchFormData.push({name: "schDiscountKind", value: cardDiscountSearch.schDiscountKind});

        if ($("#schType").val() == "discountNo") {
            searchFormData.push({name: "schValueList", value: $("#schValue").val().split(",")});

            var schValue = searchFormData.find(function(item) {
                return item.name == "schValue";
            });
            schValue.value = "";
        }

        CommonAjax.basic({url:"/discount/getDiscountList.json", data: searchFormData, method: "POST",  callbackFunc:function(res) {
            cardDiscountGrid.setData(res);
            cardDiscountApplyGrid.discountApplyGridSearchClear();
            $("#cardDiscountSearchCnt").html(cardDiscountGrid.dataProvider.getRowCount());

            if (cardDiscountNo !== undefined && cardDiscountNo != "") {

                var rowIndex = cardDiscountGrid.dataProvider.searchDataRow({fields: ['discountNo'], values: [cardDiscountNo]});
                if (rowIndex > -1) {

                    cardDiscountGrid.realGrid.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                    cardDiscountGrid.realGrid.onDataCellClicked();
                }
            }
        }});
    }
};

var cardDiscount = {
    storeList: null,
    paymentList: null,
    issueDate : null,
    issueStartDate : null,
    issueEndDate : null,
    applyGridHeader : null,
    copyYn : "N",

    init : function() {
        cardDiscount.event();
        cardDiscount.initCardDiscountAll();

        cardDiscount.applyGridHeader = new Object();
        cardDiscount.applyGridHeader.header1 = "상품번호";
        cardDiscount.applyGridHeader.header2 = "저장상품명";
        cardDiscount.applyGridHeader.header3 = "상품명";
    },
    event : function() {
        $("#discountNm").calcTextLength("keyup", "#discountNmLen");
        $("#displayDiscountNm").calcTextLength("keyup", "#displayDiscountNmLen");

        //초기화
        $("#initCardDiscountAll").on("click", function() {
            if (!confirm("초기화 하시겠습니까?")) return false;
            cardDiscount.initCardDiscountAll();
        });

        //복사
        $("#copyCardDiscount").on("click", function() {
            if ($.jUtil.isEmpty($("#discountNo").val())) {
                alert("복사할 대상이 없습니다.");
                return;
            }

            if (confirm("현재 정보를 복사하시겠습니까?")) {
                cardDiscount.copyCardDiscount();
            }
        });

        //즉시할인  저장, 수정
        $("#setCardDiscount").on("click", function() {
            cardDiscount.setCardDiscount($(this));
        });

        //점포유형
        $("#storeType").on("change", function() {
            cardDiscount.chgStoreType($(this));
        });

        $("#popupApplySearch").click(function(){
            cardDiscount.callScopePopup($(this));
        });

        $("#popupApplyAllSearch").bindClick(cardDiscount.callUploadScopePopup);
        $("#applyGridExcelDownloadBtn").bindClick(cardDiscountApplyGrid.excelDownload);
        $('#discountApplyGridSearchType').bindChange(cardDiscountApplyGrid.discountApplyGridSearchClear);
        $('#discountApplyGridSearchBtn').bindClick(cardDiscount.discountApplyGridSearch);

        $('#discountApplyGridSearchInitBtn').on('click', function() {
            $('#discountApplyGridSearchType').val('applyCd');
            cardDiscountApplyGrid.discountApplyGridSearchClear();
        });

        $("#storeInfoBtn").bindClick(cardDiscount.setStorePop);
        $("#cardInfoBtn").bindClick(cardDiscount.setCardPop);

        $("#groupCd").allowInput("keyup", ["NUM"], $(this).attr("id"));
        $("#setGroupCdBtn").bindClick(cardDiscount.setGroupCd);
    },
    /**
     * 그룹코드 컬럼에 셋팅
     */
    setGroupCd : function() {
        var chgYn = $("#chgYn").val();
        var groupCd = $("#groupCd").val();

        if (chgYn == 'N') {
            return;
        }

        if ($.jUtil.isEmpty(groupCd)) {
            alert("카드그룹코드를 입력해주세요");
            $("#groupCd").focus();
            return;
        }

        if (groupCd.length < 2 || groupCd.length > 2 || !$.jUtil.isAllowInput(groupCd, ['NUM'])) {
            alert("그룹코드는 1~99까지만 입력가능합니다.");
            return;
        }

        var checkedRows = cardDiscountApplyGrid.realGrid.getCheckedRows(true);

        if ($.jUtil.isEmpty(checkedRows)) {
            alert("그룹코드를 지정할 상품을 선택해 주세요.");
            return;
        }

        checkedRows.forEach(function (_row) {
            cardDiscountApplyGrid.dataProvider.setValue(_row,"groupCd", groupCd);
        });

        //체크 클리어
        cardDiscountApplyGrid.realGrid.checkItems(checkedRows, false);

        //그룹코드 입력 클리어
        $("#groupCd").val("");
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(data) {
        var storeType = $('#storeType').val();
        var chgYn = $("#chgYn").val();

        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));
        $('.storePop input[name="setYn"]').val(chgYn);

        var url = "/common/popup/storeSelectPop";
        var target = "storeSelectPop";
        windowPopupOpen(url, target, 600, 470);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    openCardPop : function(data) {
        var storeType = $("#storeType").val();
        var siteType = getSiteType(storeType);
        var chgYn = $("#chgYn").val();


        $('.cardPop input[name="siteType"]').val(siteType);
        $('.cardPop input[name="paymentList"]').val(JSON.stringify(data));
        $('.cardPop input[name="setYn"]').val(chgYn);

        var url = "/common/popup/cardPop";
        var target = "cardPop";
        windowPopupOpen(url, target, 470, 475);

        var frm = document.getElementById('cardSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    /**
     * 카드 결제수단 팝업 call back
     */
    cardPopupCallback: function(checkedPaymentList) {
        var resCount = checkedPaymentList.length;
        cardDiscount.paymentList = checkedPaymentList;

        $('#cardCnt').html(resCount);
    },
    setCardPop : function() {
        console.log(cardDiscount.paymentList);
        if ($.jUtil.isEmpty(cardDiscount.paymentList)) {
            cardDiscount.openCardPop();
        } else {
            cardDiscount.openCardPop(cardDiscount.paymentList);
        }
    },
    setStorePop : function() {
        if ($.jUtil.isEmpty(cardDiscount.storeList)) {
            cardDiscount.openStoreInfoPopup();
        } else {
            cardDiscount.openStoreInfoPopup(cardDiscount.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        var resCount = res.storeList.length;

        cardDiscount.storeList = res.storeList;
        $('#storeCnt').html(resCount);
    },
    initCardDiscountAll : function() {
        //폼 초기화
        cardDiscount.formElementControl(false);

        $("#cardDiscountForm").each(function() {
            this.reset();
        });

        $("#discountNmLen, #displayDiscountNmLen, #storeCnt, #cardCnt").html("0");
        $("#chgYn").val("");
        $("#groupCd").val("");
        cardDiscount.storeList = null;
        cardDiscount.paymentList = null;
        cardDiscount.initDetailDate();

        $("#storeType").trigger("change");

        // 적용대상 그리드 초기화
        cardDiscountApplyGrid.dataProvider.clearRows();

        cardDiscount.setScopeCount(cardDiscountApplyGrid.realGrid.getItemCount());
        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());

    },
    // 초기화
    initDetailDate : function() {
        cardDiscount.setDate = CalendarTime.datePickerRange('issueStartDt', 'issueEndDt', {timeFormat:'00:00'}, true, {timeFormat:'23:59'});

        cardDiscount.setDate.setStartDate(0);
        cardDiscount.setDate.setEndDateByTimeStamp("1w");
    },
    setCardDiscount : function(obj) {
        if (!cardDiscount.validateCheck()) return;

        var discountNo = $("#discountNo").val();
        var type = "등록";
        var confirmMsg = "즉시할인을 저장하시겠습니까?";
        var successMsg = "저장 되었습니다.";
        if (discountNo > 0 || discountNo != "") {
            confirmMsg = "즉시할인을 수정하시겠습니까?";
            successMsg = "수정 되었습니다.";
            type = "수정";
        }
        confirmMsg += "\n\n저장 데이터가 많으면 시간이 소요됩니다.\n작업중이던 데이터는 5분 뒤 정상 저장되었는지 반드시 확인해 주시기 바랍니다.";

        if (confirm(confirmMsg)) {
            var cardDiscountForm = $("form[name=cardDiscountForm]").serializeArray();

            // RequestBody용 parameterObject
            var paramObject = {
                createApplyScopeList : [],
                deleteApplyScopeList : []
            };

            $.map(cardDiscountForm, function(value, index) {
                paramObject[value.name] = value.value;
            });

            paramObject.storeList = cardDiscount.storeList;
            paramObject.paymentList = cardDiscount.paymentList;

            var createdDataRowList = cardDiscountApplyGrid.dataProvider.getStateRows("created");
            createdDataRowList = createdDataRowList.concat(cardDiscountApplyGrid.dataProvider.getStateRows("updated"));
            var deletedDataRowList = cardDiscountApplyGrid.dataProvider.getStateRows("deleted");

            createdDataRowList.forEach(function(_row, index) {
                var _data = cardDiscountApplyGrid.dataProvider.getJsonRow(_row);
                _data.applyScope = paramObject.applyScope;
                paramObject.createApplyScopeList.push(_data);
            });

            deletedDataRowList.forEach(function(_row, index) {
                var _data = cardDiscountApplyGrid.dataProvider.getJsonRow(_row);
                _data.applyScope = paramObject.applyScope;
                paramObject.deleteApplyScopeList.push(_data);
            });

            $.ajax({
                url         : '/discount/setDiscount.json',
                data        : JSON.stringify(paramObject),
                method      : 'POST',
                dataType    : 'json',
                contentType : 'application/json; charset=utf-8',
                success     : function(res) {
                    cardDiscount.setCallback(res, type, successMsg);
                },
                error : function(resError) {
                    if (resError.status == 401) {
                        alert(CommonErrorMsg.sessionErrorMsg);
                        windowPopupOpen("/login/popup", "loginPop", 400, 481);
                    } else {
                        console.log(resError);
                        if (resError.responseJSON != null) {
                            if (resError.responseJSON.returnMessage !== undefined) {
                                alert(resError.responseJSON.returnMessage);
                            } else {
                                alert(CommonErrorMsg.dfErrorMsg);
                            }
                        }
                    }
                },
                fail : function() {
                    alert(CommonErrorMsg.failMsg);
                }
            });
        }
    },
    setCallback : function(res, type, successMsg) {
        alert(successMsg);

        // 조회
        if (cardDiscount.copyYn == "Y") {
            // 초기화
            cardDiscount.initCardDiscountAll();

            $("#schType").val("discountNo");
            $("#schValue").val(res.discountNo);

            cardDiscount.copyYn = "N";
        }

        cardDiscountSearch.getCardDiscountSearch(res.discountNo);
    },
    getCardDiscountDetail : function(discountNo) {
        cardDiscount.initCardDiscountAll();

        CommonAjax.basic({url:"/discount/getDiscountDetail.json", data : {"discountNo":discountNo}, method : "POST", successMsg:null, callbackFunc:function(res) {
            cardDiscount.detailCallback(res, "set");
        }});
    },
    detailCallback : function(res, detailType) {

        $("#discountNm").val(res.discountNm).setTextLength("#discountNmLen");
        $("#displayDiscountNm").val(res.displayDiscountNm).setTextLength("#displayDiscountNmLen");

        $("#storeType").val(res.storeType).change();
        $("#discountKind").val(res.discountKind).change();

        $("#useYn").val(res.useYn);

        //input 값 입력
        $("#cardDiscountForm").find("input").each(function(index) {
            var formElementId = $(this).attr("id");
            if (formElementId != undefined) {
                $.each(res, function(key, value) {
                    if (formElementId == key) {
                        $("#" + formElementId).val(value);
                    }
                });
            }
        });

        $("#chgYn").val(res.chgYn);

        if (detailType == "set") {
            if ($("#chgYn").val() == "N") {
                cardDiscount.formElementControl(true);
            }
        } else {
            $("#discountNo").val("");
        }

        //점포코드
        cardDiscount.storeList = res.storeList;
        //결제수단
        cardDiscount.paymentList = res.paymentList;

        if (!$.jUtil.isEmpty(res.storeList)) {
            $("#storeCnt").html(res.storeList.length);
        }

        if (!$.jUtil.isEmpty(res.paymentList)) {
            $("#cardCnt").html(res.paymentList.length);
        }

        cardDiscountApplyGrid.setData(res.applyScopeList);
        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());
        cardDiscount.setScopeCount(cardDiscountApplyGrid.realGrid.getItemCount());
        cardDiscountApplyGrid.discountApplyGridSearchClear();
    },
    callScopePopup : function(obj) {
        var callback = "cardDiscount.callbackApplySearch";
        var popWidth = "1084";
        var popHeight = "650";
        var storeType = $('#storeType').val();
        var mallType = getMallType(storeType);
        var siteType = getSiteType(storeType);

        var uri = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + siteType + "&storeType=" + storeType;
        var target = "itemPopup";

        windowPopupOpen(uri, target, popWidth, popHeight);
    },
    setApplyObject : function(callbackDataArray) {
        var applyDataArray = new Array();

        for(var i in callbackDataArray) {
            var applyDataObj = new Object();
            applyDataObj.applyCd = callbackDataArray[i].itemNo;
            applyDataObj.applyNm = callbackDataArray[i].itemNm;
            applyDataObj.discountType = callbackDataArray[i].discountType;
            applyDataObj.discountRate = callbackDataArray[i].discountRate;
            applyDataObj.discountPrice = callbackDataArray[i].discountPrice;
            applyDataObj.couponCd = callbackDataArray[i].couponCd;
            applyDataObj.issueStartDt = callbackDataArray[i].issueStartDt;
            applyDataObj.issueEndDt = callbackDataArray[i].issueEndDt;
            applyDataObj.groupCd = callbackDataArray[i].groupCd;

            applyDataArray.push(applyDataObj);
        }

        return applyDataArray;
    },
    callbackApplySearch : function(callbackDataArray) {
        var applyDataArray = cardDiscount.setApplyObject(callbackDataArray);

        cardDiscountApplyGrid.appendData(applyDataArray);
        cardDiscountApplyGrid.discountApplyGridSearchClear();
        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());
    },
    chgStoreType : function(obj) {
        if ($(obj).val() == STORE_TYPE_DS) {
            $("#storeInfoBtn").prop("disabled", true);
        } else {
            $("#storeInfoBtn").prop("disabled", false);
        }

        cardDiscount.storeList = null;
        cardDiscount.paymentList = null;

        $('#storeCnt, #cardCnt').html('0');
    }
    ,
    setScopeCount : function(cnt) {
        $("#applyScopeCnt").html(cnt + "건)");
    },
    setDiscountApplySearchCount : function(cnt) {
        var str = "(" + cnt + "/";
        $('#applyScopeGridCount').html(str);
    },
    callUploadScopePopup : function() {
        //적용일자 validation
        if (!cardDiscount.validCheckIssuePeriod()) {
            return;
        }

        var popWidth = "600";
        var popHeight = "250";
        var applyScope = "ITEM";
        var storeType = $("#storeType").val();
        var issueStartDt = $("#issueStartDt").val();
        var issueEndDt = $("#issueEndDt").val();
        var callback = "cardDiscount.callbackApplySearch";
        var uri = "/promotion/popup/upload/uploadApplyScopePop?promotionType=CARD_DISCOUNT&discountNo=" + $("#discountNo").val() + "&applyScope=" + applyScope
            + "&storeType=" + storeType + "&issueStartDt=" + issueStartDt + "&issueEndDt=" + issueEndDt + "&callBackScript=" + callback;

        windowPopupOpen(uri, "uploadApplyScopePop", popWidth, popHeight);
    },
    validCheckIssuePeriod : function() {
        var nowDt = moment().format("YYYY-MM-DD 00:00");
        var issueStartDt = $("#issueStartDt").val();
        var issueEndDt = $("#issueEndDt").val();

        if (!moment(issueStartDt,'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("적용기간 날짜 형식이 맞지 않습니다.");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        if (!moment(issueEndDt,'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("적용기간 날짜 형식이 맞지 않습니다.");
            $("#issueEndDt").val(nowDt);
            return false;
        }

        if (moment(issueEndDt).diff(issueStartDt, "day", true) > 31) {
            alert("적용기간은 31일을 초과할 수 없습니다.");
            return false;
        }

        if (moment(issueEndDt).diff(issueStartDt, "day", true) < 0) {
            alert("적용기간 종료일은 적용기간 시작일보다 빠를 수 없습니다.");
            return false;
        }

        if ($.jUtil.isEmpty($("#discountNo").val())) {
            if (moment(issueStartDt).diff(nowDt, "day", true) < 0) {
                alert("적용기간 시작일은 과거일을 선택할 수 없습니다.");
                $("#issueStartDt").val(nowDt);
                return false;
            }
        }

        return true;
    }
    ,
    validateCheck : function() {
        var nowDt = moment().format("YYYY-MM-DD 00:00");
        var issueStartDt = $("#issueStartDt").val();
        var issueEndDt = $("#issueEndDt").val();
        var discountNm = $("#discountNm").val();
        var displayDiscountNm = $("#displayDiscountNm").val();
        var useYn = $("#useYn").val();
        var returnFlag = true;
        var limitQty = 50000;

        if ($.jUtil.isEmpty(discountNm)) {
            alert("관리할인명을 입력하세요.");
            return false;
        }

        if (discountNm.length > 60) {
            alert("관리할인명은 최대 60자까지 입력 가능합니다.");
            return false;
        }

        if ($.jUtil.isEmpty(displayDiscountNm)) {
            alert("전시할인명을 입력하세요.");
            return false;
        }

        if (displayDiscountNm.length > 60) {
            alert("전시할인명은 최대 60자까지 입력 가능합니다.");

        }

        // 적용기간에 대한 valid check
        if (!this.validCheckIssuePeriod()) {
            return false;
        }

        if ($("#storeType").val() != 'DS' && $.jUtil.isEmpty(cardDiscount.storeList)) {
            alert("점포를 등록하세요.");
            return false;
        }

        if ($.jUtil.isEmpty(cardDiscount.paymentList)) {
            alert("할인카드를 등록하세요.");
            return false;
        }

        if (useYn == "Y") {
            var list = cardDiscountApplyGrid.dataProvider.getStateRows("created");
            list = list.concat(cardDiscountApplyGrid.dataProvider.getStateRows("updated"));
            list = list.concat(cardDiscountApplyGrid.dataProvider.getStateRows("none"));

            // 그리드에 상품수가 50,000개를 초과하는 경우
            if (list.length > limitQty) {
                alert("할인 대상 상품은 최대 50,000개를 초과하여 등록할 수 없습니다.");
                return false;
            }

            var isValidGroupCd = true;

            list.forEach(function (_row, index) {
                var groupCd = cardDiscountApplyGrid.dataProvider.getJsonRow(_row).groupCd;

                if ($.jUtil.isEmpty(groupCd)) {
                    isValidGroupCd = false;
                    return false;
                }
            });

            if (!isValidGroupCd) {
                alert("그룹코드의 누락이 있습니다.\n그리드 확인 후 입력하세요.");
                return false;
            }

            var isValidApplyPeriod = true;
            var issueStartDt = $("#issueStartDt").val();
            list.forEach(function (_row, index) {
                var curRow = cardDiscountApplyGrid.dataProvider.getJsonRow(_row);
                var  apply_issueStartDt = moment(curRow.issueStartDt).format("YYYY-MM-DD") + " 00:00";
                var  apply_issueEndDt = moment(curRow.issueEndDt).format("YYYY-MM-DD") + " 23:59";

                if ($.jUtil.diffDate(issueStartDt, apply_issueStartDt, "D") < 0 || $.jUtil.diffDate(apply_issueEndDt, issueEndDt, "D") < 0) {
                    isValidApplyPeriod = false;
                    return false;
                }
            });

            if (!isValidApplyPeriod) {
                alert("적용 상품 목록의 시작일, 종료일은 적용기간내로 설정하여야 합니다.\n확인하십시오.");
                return false;
            }
        }

        return returnFlag;
    },
    formElementControl : function(flag) {
        $("#cardDiscountForm").find("select").each(function() {
            if (flag) {
                if ($(this).attr("isUpdate") == "N") {
                    removeAllSelectOption($(this));
                } else {
                    addSelectOption($(this))
                }
            } else addSelectOption($(this));
        });

        $("#cardDiscountForm").find("input:text").each(function() {
            var formElementId = $(this).attr("id");
            if (flag) {
                $("#" + formElementId).attr("readonly", ($(this).attr("isUpdate") == "N" ? true : false));
            } else {
                $("#" + formElementId).attr("readonly", flag);
            }
        });

        $("#groupCd").attr("disabled", flag); //카드그룹코드
        $("#discountNo").attr("readonly", true);
        $("#issueDtDiv .ui-datepicker-trigger").attr("disabled", flag);
    },
    copyCardDiscount : function() {
        var discountNo = $("#discountNo").val();
        cardDiscount.initCardDiscountAll();

        CommonAjax.basic({url: "/discount/getDiscountDetail.json", data: {"discountNo": discountNo, "copy":true}, method: "POST", successMsg: null, callbackFunc : function(res) {
            //복사시 적용기간 초기화
            res.issueStartDt = "";
            res.issueEndDt = "";

            cardDiscount.copyYn = "Y";
            cardDiscount.detailCallback(res, "copy");
            cardDiscount.setScopeCount("0");
        }});
    },
    // 적용 정보 그리드 내 조회
    discountApplyGridSearch : function() {
        var gridSearchValue = $("#discountApplyGridSearchValue").val();

        if ($('#discountApplyGridSearchType').val() == "applyCd") {
            var replaceText = gridSearchValue.replace(/\s/gi, "");
            $("#discountApplyGridSearchValue").val(replaceText);

            if (!/^[0-9,]*$/.test(replaceText)) {
                alert('상품번호로 다중 검색 시 상품번호 사이에 구분자 콤마(“,”)를 입력해주세요.');
                return;
            }

            var arrSchValueList = replaceText.split(",");

            if (arrSchValueList.length > 1000) {
                alert("상품번호는 최대 1,000개까지 검색 가능합니다.");
                return;
            }

            gridSearchValue = arrSchValueList;
        }

        cardDiscountApplyGrid.filter($('#discountApplyGridSearchType').val(), gridSearchValue);

        cardDiscount.setDiscountApplySearchCount(cardDiscountApplyGrid.realGrid.getItemCount());
    }
};

function addSelectOption(obj){
    var jsonData = "";
    var defaultOption = "";

    switch ($(obj).attr("id")) {
        case "storeType":
            jsonData = storeTypeJson;
            break;
    }

    if (jsonData == "") return false;

    $(obj).find("option").remove();

    if (defaultOption != "") $(obj).prepend(defaultOption);
    $.each(jsonData, function(key, code) {
        var chkOption = 0;
        var codeCd = code.mcCd;
        var codeNm = code.mcNm;
        var codeRef1 = code.ref1;
        var codeRef3 = code.ref3;

        $(obj).find("option").each(function() {
            if ($(this).val() == codeCd) {
                chkOption++;
            }
        });

        if (chkOption == 0) {
            if ($(obj).attr("id") != "storeType" || ($(obj).attr("id") == "storeType" && codeRef3 != 'VIRTUAL')) {
                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
            }
        }
    });
}