/**
 * 쿠폰발급관리
 */
$(document).ready(function() {
    couponGrid.init();
    couponIssueMngGrid.init();
    couponSearch.init();
    couponIssueMng.init();
    CommonAjaxBlockUI.global();
});

/**
 * Real Grid 초기화
 */
var couponGrid = {
    realGrid : new RealGridJS.GridView("couponGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        couponGrid.initGrid();
        couponGrid.initDataProvider();
        couponGrid.event();
    },
    initGrid : function() {
        couponGrid.realGrid.setDataSource(couponGrid.dataProvider);

        couponGrid.realGrid.setStyles(couponIssueMngCouponGridBaseInfo.realgrid.styles);
        couponGrid.realGrid.setDisplayOptions(couponIssueMngCouponGridBaseInfo.realgrid.displayOptions);
        couponGrid.realGrid.setColumns(couponIssueMngCouponGridBaseInfo.realgrid.columns);
        couponGrid.realGrid.setOptions(couponIssueMngCouponGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(couponGrid.realGrid, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "couponType", couponTypeJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "status", couponStatusJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "purpose", couponPurposeJson);

        couponGrid.realGrid.setIndicator({visible:true});
        couponGrid.realGrid.setColumnProperty("purchaseMin", "visible", false);
        couponGrid.realGrid.setColumnProperty("discountMax", "visible", false);
        couponGrid.realGrid.setColumnProperty("applySystem", "visible", false);
        couponGrid.realGrid.setColumnProperty("purpose", "visible", false);
        couponGrid.realGrid.setColumnProperty("shareYn", "visible", false);
        couponGrid.realGrid.setColumnProperty("departCd", "visible", false);
        couponGrid.realGrid.setColumnProperty("regNm", "visible", false);
        couponGrid.realGrid.setColumnProperty("regDt", "visible", false);
        couponGrid.realGrid.setColumnProperty("chgNm", "visible", false);
        couponGrid.realGrid.setColumnProperty("chgDt", "visible", false);
    },
    initDataProvider : function() {
        couponGrid.dataProvider.setFields(couponIssueMngCouponGridBaseInfo.dataProvider.fields);
        couponGrid.dataProvider.setOptions(couponIssueMngCouponGridBaseInfo.dataProvider.options);
    },
    event : function() {
        couponGrid.realGrid.onDataCellClicked = function(gridView, index) {
            couponIssueMng.submitCouponIssueHistory(index.dataRow);
        };
    },
    setData : function(dataList) {
        couponGrid.dataProvider.clearRows();
        couponGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(couponGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰발급관리_쿠폰목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        couponGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            lookupDisplay: true,
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var couponIssueMngGrid = {
    realGrid : new RealGridJS.GridView("couponIssueMngGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        couponIssueMngGrid.initGrid();
        couponIssueMngGrid.initDataProvider();
    },
    initGrid : function() {
        couponIssueMngGrid.realGrid.setDataSource(couponIssueMngGrid.dataProvider);

        couponIssueMngGrid.realGrid.setStyles(couponIssueMngGridBaseInfo.realgrid.styles);
        couponIssueMngGrid.realGrid.setDisplayOptions(couponIssueMngGridBaseInfo.realgrid.displayOptions);
        couponIssueMngGrid.realGrid.setColumns(couponIssueMngGridBaseInfo.realgrid.columns);
        couponIssueMngGrid.realGrid.setOptions(couponIssueMngGridBaseInfo.realgrid.options);

        couponIssueMngGrid.realGrid.setIndicator({visible:true});
        couponIssueMngGrid.realGrid.setColumnProperty("orderPurchaseNo", "visible", false);
        couponIssueMngGrid.realGrid.setColumnProperty("cancelYn", "visible", false);
        couponIssueMngGrid.realGrid.setColumnProperty("preIssueNo", "visible", false);
        couponIssueMngGrid.realGrid.setColumnProperty("preIssueNo", "visible", false);
    },
    initDataProvider : function() {
        couponIssueMngGrid.dataProvider.setFields(couponIssueMngGridBaseInfo.dataProvider.fields);
        couponIssueMngGrid.dataProvider.setOptions(couponIssueMngGridBaseInfo.dataProvider.options)
    },
    setData : function(dataList) {
        couponIssueMngGrid.dataProvider.clearRows();
        couponIssueMngGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(couponIssueMngGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰발급관리_발급회원목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        couponIssueMngGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            lookupDisplay: true,
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var couponSearch = {
    selectCouponRowId : 0,
    schDate : null,     // 캘린더 : 검색 폼 > 등록기간
    init : function() {
        this.event();
        this.initCouponSearch();
    },
    event : function() {
        $("#getCouponSearchBtn").on("click", function() {
            couponSearch.getCouponSearch();
        });

        $("#initCouponSearchBtn").on("click", function() {
            couponSearch.initCouponSearch();
        });

        $("#excelDownloadCouponListBtn").on("click", function() {
            couponGrid.excelDownload();
        });

        $("#setTodayBtn").on("click", function() {
            couponSearch.initSearchDate("0");
        });

        $("#setOneWeekBtn").on("click", function() {
            couponSearch.initSearchDate("-1w");
        });

        $("#setOneMonthBtn").on("click", function() {
            couponSearch.initSearchDate("-1m");
        });

        $("#setThreeMonthBtn").on("click", function() {
            couponSearch.initSearchDate("-3m");
        });
    },
    //쿠폰 조회 검색 폼 - 초기화
    initCouponSearch : function() {
        $("#couponSearchForm").each(function () {
            this.reset();
        });

        //조회 일자 초기화
        couponSearch.initSearchDate("0");
        //couponIssueMng.initCouponIssueHistAll();
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        couponSearch.schDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        couponSearch.schDate.setEndDate(0);
        couponSearch.schDate.setStartDate(flag);
    },
    // 내용 검증
    validateCheck : function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var returnFlag = true;
        // 1년 단위
        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            returnFlag = false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            returnFlag = false;
        }

        if(startDt == "Invalid Date" || endDt == "Invalid Date") {
            alert("날짜 형식이 맞지 않습니다");
            returnFlag = false;
        }

        // 검색어 자릿수 확인
        if($("#schType").val() == "couponNm") {
            if ($("#schValue").val() != "" && $("#schValue").val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력하세요.");
                returnFlag = false;
            }
        }

        return returnFlag;
    },
    //쿠폰조회
    getCouponSearch : function(func) {
        // 조회 조건 확인
        if (!couponSearch.validateCheck()) return;

        couponIssueMng.initCouponIssueHistAll();

        $("#couponSearchCnt").html("0");

        var searchFromData = $("form[name=couponSearchForm]").serialize();

        CommonAjax.basic({url:"/coupon/getCouponList.json?" + searchFromData, method : "GET",  callbackFunc:function(res) {
            couponGrid.setData(res);
            $("#couponSearchCnt").html(couponGrid.dataProvider.getRowCount());
        }});
    }
};

var couponIssueMng = {
    couponNo : null,
    couponStatus : null,
    couponIssueLimit : null,
    couponMaxCount : 0,
    couponIssueCount : 0,
    schDate : null,
    firstBuyLimit : null,

    init: function () {
        couponIssueMng.event();
        couponIssueMng.initCouponIssueHistAll();
    },
    event: function () {
        //초기화
        $("#initCouponIssueHistSearchBtn").on("click", function() {
            couponIssueMng.initCouponIssueHistAll();
        });

        $("#getCouponIssueHistSearchBtn").on("click", function() {
            couponIssueMng.getCouponIssueHist();
        });

        $("#excelDownloadCouponHistListBtn").on("click", function() {
           couponIssueMngGrid.excelDownload();
        });

        $("#setMember, #srchMember").on("click", function() {
            couponIssueMng.callMemberSearchPop($(this));
        });

        $("#setAllMember").on("click", function() {
            couponIssueMng.callUploadPop();
        });

        $("#withdrawCoupon").on("click", function() {
            couponIssueMng.setWithdrawCoupon();
        });

        $("#setHistTodayBtn").on("click", function() {
            couponIssueMng.initCouponHistDate("0");
        });

        $("#setHistTodayBtn").on("change", function() {
            couponIssueMng.chgHistSchType($(this));
        });

        $("#histSchType").bindChange(couponIssueMng.chgHistSchType);
    },
    chgHistSchType : function(obj) {
        var value = $(obj).val();

        $("#histSchValue").val("");

        if (value == 'userNo') {
            $("#randomNoSrchControl").hide();
            $("#schValueDetail").val("");
            $("#memberSrchControl").show();
        } else if (value == 'randomNo') {
            $("#memberSrchControl").hide();
            $("#memberId, #memberNm").val("");
            $("#randomNoSrchControl").show();
        }
    }
    ,
    initCouponHistDate : function (flag) {
        couponIssueMng.schDate = Calendar.datePickerRange("histSchStartDt", "histSchEndDt");
        couponIssueMng.schDate.setEndDate(0);
        couponIssueMng.schDate.setStartDate(flag);
    },
    initCouponIssueHistAll : function() {
        couponIssueMng.couponNo = null;

        $("#couponIssueHistSearchForm").each(function () {
            this.reset();
        });

        couponIssueMng.initCouponHistDate("0");
        $("#histSchValue").val("");
        $("#couponIssueHistSearchCnt").html("0");
        $("#subCouponNo").html("");
        // $("#couponStat").html("");
        $("#allIssuedSearchPopup").hide();

        $("#histSchType").change();

        couponIssueMngGrid.dataProvider.clearRows();
    },
    submitCouponIssueHistory : function(selectRowId) {
        couponIssueMng.initCouponIssueHistAll();
        couponSearch.selectCouponRowId = selectRowId;
        couponIssueMng.couponNo         = couponGrid.dataProvider.getValue(selectRowId, "couponNo");
        couponIssueMng.couponStatus     = couponGrid.dataProvider.getValue(selectRowId, "status");
        var couponType = couponGrid.dataProvider.getValue(selectRowId, "couponType");

        //일괄등록 버튼 활성화
        $('#setAllMember').show();
        $("#subCouponNo").text(couponIssueMng.couponNo);

        if (couponType == COUPON_TYPE_CART) {
            couponIssueMngGrid.realGrid.setColumnProperty("randomNo", "visible", true);
        } else {
            couponIssueMngGrid.realGrid.setColumnProperty("randomNo", "visible", false);
        }
        couponIssueMng.getCouponIssueHist();
    },
    getCouponIssueHist : function() {
        if(couponIssueMng.couponNo == null) {
            alert("쿠폰을 선택하세요.");
            return false;
        }

        // 조회기간 체크
        if (moment($("#histSchEndDt").val()).diff($("#histSchStartDt").val(), "day", true) > 7) {
            alert("7일간만 검색 가능합니다.");
            $("#histSchStartDt").val(moment($("#histSchEndDt").val()).add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }

        $("#couponIssueHistSearchCnt").html("0");
        $("#subCouponNo").html("");
        $("#couponStat").html("");

        if ($("#histSchType").val() == 'randomNo') {
            $("#histSchValue").val($.trim($("#schValueDetail").val()));
        }

        couponIssueMngGrid.dataProvider.clearRows();
        //$("#allIssuedSearchPopup").show();

        $("#subCouponNo").html(couponIssueMng.couponNo);

        var params = {
            "couponNo":couponIssueMng.couponNo
            , "histSchType":$("#histSchType").val()
            , "histSchValue":$("#histSchValue").val()
            , "histSchStartDt":$("#histSchStartDt").val()
            , "histSchEndDt":$("#histSchEndDt").val()
            , "schPageType":$("#schPageType").val()
        };

        CommonAjax.basic({
            url : "/coupon/getCouponIssueHistList.json?" + $.param(params),
            method : 'get',
            callbackFunc : function(resData) {
                if ( resData == null ) {
                    if (true) {
                        alert('정상적으로 조회되지 않았습니다.');
                    }
                } else {
                    /*couponIssueMng.couponIssueCount = resData.issueCount;
                    var couponStat = "[실제발급수: "+ resData.issueCount +"]";

                    $("#couponStat").html(couponStat);*/
                    $("#couponIssueHistSearchCnt").html(resData.couponIssueHistList.length);
                    couponIssueMngGrid.dataProvider.clearRows();
                    couponIssueMngGrid.setData(resData.couponIssueHistList);
                }
            }
        });
    },
    callMemberSearchPop : function(obj) {
        var memberSearchCallback = "";
        if($(obj).attr("id") == "setMember") {
            if (couponIssueMng.couponNo == "" || couponIssueMng.couponNo == null) {
                alert("발급할 쿠폰을 선택하세요.");
                return;
            }

            if (this.couponStatus != "ISSUED") {
                alert("발급할 수 없는 쿠폰입니다.");
                return;
            }

            if (couponIssueMng.couponIssueLimit == "Y") {
                if (couponIssueMng.couponIssueCount >= couponIssueMng.couponLimitCount) {
                    alert("총 발급매수 수량 제한으로 쿠폰을 발급 할 수 없습니다.");
                    return false;
                }
            }

            memberSearchCallback = "couponIssueMng.callbackMemberSetPop";
        }
        else {
            if (couponIssueMng.couponNo == "" || couponIssueMng.couponNo == null) {
                alert("조회할 쿠폰을 선택하세요.");
                return;
            }

            memberSearchCallback = "couponIssueMng.callbackMemberSearchPop";
        }
        memberSearchPopup(memberSearchCallback);
    },
    callbackMemberSetPop : function(memberData) {
        var params = {
            "couponNo" : couponIssueMng.couponNo
            , "userNo" : memberData.userNo
            , "userId" : memberData.userId
            , "userNm" : memberData.userNm
            , "mhcUcid" : memberData.mhcUcid
        };

        //if (confirm(memberData.userNm +" ("+ memberData.userId +") 에게 쿠폰을 발급하시겠습니까?")) {
            CommonAjax.basic({url:"/coupon/setIssueCoupon.json", data : $.param(params), method : "POST", successMsg:"쿠폰이 발급되었습니다."
                , callbackFunc:function(res) {
                    couponIssueMng.setCallback();
                }, errorCallbackFunc:function(resError) {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.returnMessage !== undefined) {
                            // 에러메세지
                            alert(resError.responseJSON.returnMessage);
                        }
                        else if (resError.responseJSON.data !== undefined) {
                            // api 에러메세지
                            alert(resError.responseJSON.data.result);
                        }
                        else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    }
                    return false;
                }
            });
        //}
    },
    callbackMemberSearchPop : function(memberData) {
        $("#histSchValue").val(memberData.userNo);
        $("#memberId").val(memberData.userId);
        $("#memberNm").val(memberData.userNm);

        couponIssueMng.initCouponHistDate("-7d");
        couponIssueMng.getCouponIssueHist();
    },
    callUploadPop : function() {
        if(couponIssueMng.couponNo == null) {
            alert("발급할 쿠폰을 선택하세요.");
            return false;
        }

        var popWidth = "600";
        var popHeight = "250";
        var callback = "couponIssueMng.setCallback";
        var uri = "/coupon/popup/uploadIssueCouponPop?callback="+ callback +"&couponNo="+ couponIssueMng.couponNo;

        if(uri != "") {
            windowPopupOpen(uri, "setAllMember", popWidth, popHeight);
        }
    },
    setWithdrawCoupon : function() {
        if ($.jUtil.isEmpty(couponIssueMng.couponNo) || couponIssueMng.couponNo == 0) {
            alert("쿠폰을 선택하세요.");
            return;
        }

        var checkedRows = couponIssueMngGrid.realGrid.getCheckedRows(true);

        if (checkedRows.length <= 0) {
            alert("회수할 대상을 선택해 주세요.");
            return;
        }

        if(checkedRows.length > 5000) {
            alert("최대 5,000건씩 선택 및 회수 가능합니다.(현재 " + $.jUtil.comma(checkedRows.length) + "건 선택)");
            return;
        }

        if (confirm("선택한 회원의 쿠폰을 회수 하시겠습니까?")) {
            var paramObject = {
                couponNo : couponIssueMng.couponNo,
                issueNoArr : []
            };

            checkedRows.forEach( function(rowId, i){
                if (couponIssueMngGrid.dataProvider.getValue(rowId, "withdrawYn") != "Y" && couponIssueMngGrid.dataProvider.getValue(rowId, "couponUseYn") != "Y") {
                    paramObject.issueNoArr.push(couponIssueMngGrid.dataProvider.getValue(rowId, "issueNo"));
                }
            });

            if (paramObject.issueNoArr.length > 0) {
                $.ajax({
                    url         : '/coupon/setWithdrawCoupon.json',
                    data        : JSON.stringify(paramObject),
                    method      : 'POST',
                    dataType    : 'json',
                    contentType : 'application/json; charset=utf-8',
                    success     : function (res) {
                        alert("쿠폰이 회수되었습니다.");
                        couponIssueMng.setCallback();
                    },
                    error: function(resError) {
                        // SessionNotFoundException 발생 시 httpStatus(401)를 확인하여 로그인 페이지 팝업오픈
                        if (resError.status == 401) {
                            alert(CommonErrorMsg.sessionErrorMsg);
                            windowPopupOpen("/login/popup", "loginPop", 400, 481);
                        } else {
                            if (resError.responseJSON != null) {
                                if (resError.responseJSON.returnMessage !== undefined) {
                                    // 에러메세지
                                    alert(resError.responseJSON.returnMessage);
                                }
                                else if (resError.responseJSON.data !== undefined) {
                                    // api 에러메세지
                                    alert(resError.responseJSON.data.result);
                                }
                                else {
                                    // df 에러메세지
                                    alert(CommonErrorMsg.dfErrorMsg);
                                }
                            }
                        }
                    },
                    fail: function () {
                        alert(CommonErrorMsg.failMsg);
                    }
                });
            } else {
                alert("유효한 회수 대상이 없습니다.");
            }
        }
    },
    setCallback : function() {
        couponIssueMng.submitCouponIssueHistory(couponSearch.selectCouponRowId);
    }
};