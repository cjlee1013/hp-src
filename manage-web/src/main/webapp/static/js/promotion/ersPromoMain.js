/**
 * 프로모션관리 > 행사관리 > 사은행사관리 메인 js
 */
$(document).ready(function () {
    ersPromoGrid.init();
    ersPromoIntervalGrid.init();
    ersPromoStoreGrid.init();
    ersPromoItemGrid.init();
    CommonAjaxBlockUI.global();
    ersPromoSearch.init();
    ersPromo.init();
});

var ersPromoSearch = {
    init : function () {
        ersPromoSearch.searchFormReset();
        ersPromoSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#ersPromoSearchForm").resetForm();

        // 조회 기간 초기화
        initCalendarDate(ersPromoSearch, "schStartDt", "schEndDt", "SEARCH", "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(ersPromoSearch.searchErsPromoList);
        $("#schResetBtn").bindClick(ersPromoSearch.searchFormReset);
        $("#excelDownloadBtn").bindClick(ersPromoGrid.excelDownload);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                ersPromoSearch.searchErsPromoList();
            }
        });
    }
    /**
     * 점포검색 팝업
     */
    , openStorePopup : function () {
        var target = "ersStorePopup";
        var callback = "ersPromoSearch.storePopupCallback";
        var url = "/common/popup/storePop?callback=" + callback + "&isMulti=N";

        var popWidth = "1100";
        var popHeight = "620";

        windowPopupOpen(url, target, popWidth, popHeight);
    }
    /**
     * 점포검색 팝업 콜백
     * @param res
     */
    , storePopupCallback : function (res) {
        if (!$.jUtil.isEmpty(res)) {
            $("#schStoreId").val(res[0].storeId);
            $("#schStoreNm").val(res[0].storeNm);
        }
    }
    , searchValidCheck : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            initCalendarDate(ersPromoSearch, "schStartDt", "schEndDt", "SEARCH", "-1w", "0");
            return false;
        }

        // 3개월 이내 검색
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType == "EVENTNM" && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }

        return true;
    }
    /**
     * 사은행사 정보 조회
     */
    , searchErsPromoList : function (eventCd) {
        // 검색조건 validation check
        if (ersPromoSearch.searchValidCheck()) {
            // 사은행사 정보 조회 개수 초기화
            $("#ersPromoTotalCount").html("0");

            // 사은행사 상세 초기화
            ersPromo.ersPromoFormReset();

            // 조회
            var ersPromoSearchForm = $("#ersPromoSearchForm").serialize();

            CommonAjax.basic({
                url: "/event/getErsPromoList.json"
                , data: ersPromoSearchForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    ersPromoGrid.setData(res);

                    $("#ersPromoTotalCount").html($.jUtil.comma(ersPromoGrid.dataProvider.getRowCount()));

                    if (eventCd !== undefined && eventCd != "") {
                        var rowIndex = ersPromoGrid.dataProvider.searchDataRow({fields: ["eventCd"], values: [eventCd]});

                        if (rowIndex > -1) {
                            ersPromoGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                            ersPromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

var ersPromo = {
    selectEventCd : null

    /**
     * 초기화
     */
    , init : function () {
        ersPromo.bindingEvent();
        ersPromo.ersPromoFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setEventBtn").bindClick(ersPromo.setErsPromo);          // 저장
        $("#resetBtn").bindClick(ersPromo.ersPromoFormReset);       // 초기화
        $("#purchaseGuide").calcTextLength("keyup", "#purchaseGuideLen");
    }
    /**
     * 사은행사 상세 내용 초기화
     */
    , ersPromoFormReset : function () {
        $("#ersPromoForm").resetForm();

        ersPromo.selectEventCd = null;

        // span 초기화
        $("#eventCd, #eventNm, #eventTargetType, #eventGiveType, #receiptSumYn, #tenantYn, #multipleYn, #ticketAmtPrtYn, #customerInfoPrtYn, #saleType, #partName").html("");
        $("#eventDate, #giveDate, #returnDate, #confirmYn, #confirmDt, #confirmUser, #cancelFlag, #cancelDt, #cancelUser, #reconfirmYn, #fundRate, #dcYn, #visaRfEventFlag, #receiptPrtTitle").html("");
        $("#receiptMsg1, #receiptMsg2, #receiptMsg3, #receiptMsg4, #receiptMsg5, #receiptMsg6, #receiptMsg7, #receiptMsg8, #receiptMsg9, #receiptMsg10, #receiptMsg11, #receiptMsg12, #receiptMsg13, #receiptMsg14, #receiptMsg15").html("");
        $("#intervalCount, #storeCount, #itemCount, #cardCount, #purchaseGuideLen").html("0");

        // 그리드 초기화
        ersPromoIntervalGrid.dataProvider.clearRows();
        ersPromoStoreGrid.dataProvider.clearRows();
        ersPromoItemGrid.dataProvider.clearRows();
        // ersPromoCardGrid.dataProvider.clearRows();
    }
    /**
     * 사은행사 상세 조회
     */
    , getErsPromoDetail : function (eventCd) {
        // 사은행사 상세 초기화
        ersPromo.ersPromoFormReset();

        ersPromo.selectEventCd = eventCd;

        CommonAjax.basic({
            url:"/event/getErsPromoDetail.json?eventCd="+eventCd
            , method : "GET"
            , callbackFunc : function(res) {
                ersPromo.detailCallback(res);
            }
        });
    }
    , detailCallback : function (res) {
        if (!$.jUtil.isEmpty(res)) {
            var dataRow = ersPromoGrid.gridView.getCurrent().dataRow;
            var gridRow = ersPromoGrid.dataProvider.getJsonRow(dataRow);
            var values = ersPromoGrid.gridView.getDisplayValuesOfRow(dataRow);   // lookup 데이터 가져오기 위해 displayValue 값 가져옴.

            $("#eventCd").html(res.eventCd);
            $("#eventNm").html(res.eventNm);
            $("#receiptSumYn").html(values.receiptSumYn);
            $("#tenantYn").html(res.tenantYn);
            $("#multipleYn").html(values.multipleYn);
            $("#ticketAmtPrtYn").html(res.ticketAmtPrtYn);
            $("#customerInfoPrtYn").html(values.customerInfoPrtYn);
            $("#saleType").html(values.saleType);
            $("#confirmDt").html(res.confirmDt);
            $("#confirmUser").html(res.confirmUser);
            $("#partName").html(res.partName);
            $("#cancelFlag").html(values.cancelFlag);
            $("#cancelDt").html(res.cancelDt);
            $("#cancelUser").html(res.cancelUser);
            $("#reconfirmYn").html(res.reconfirmYn);
            $("#fundRate").html(res.fundRate);
            $("#dcYn").html(res.dcYn);
            $("#receiptMsg1").html(res.receiptMsg1);
            $("#receiptMsg2").html(res.receiptMsg2);
            $("#receiptMsg3").html(res.receiptMsg3);
            $("#receiptMsg4").html(res.receiptMsg4);
            $("#receiptMsg5").html(res.receiptMsg5);
            $("#receiptMsg6").html(res.receiptMsg6);
            $("#receiptMsg7").html(res.receiptMsg7);
            $("#receiptMsg8").html(res.receiptMsg8);
            $("#receiptMsg9").html(res.receiptMsg9);
            $("#receiptMsg10").html(res.receiptMsg10);
            $("#receiptMsg11").html(res.receiptMsg11);
            $("#receiptMsg12").html(res.receiptMsg12);
            $("#receiptMsg13").html(res.receiptMsg13);
            $("#receiptMsg14").html(res.receiptMsg14);
            $("#receiptMsg15").html(res.receiptMsg15);

            $("#eventTargetType").html(values.eventTargetType);
            $("#eventGiveType").html(values.eventGiveType);
            $("#receiptPrtTitle").html(res.receiptPrtTitle);
            $("#confirmYn").html(values.confirmFlag);
            $("#visaRfEventFlag").html(res.visaRfEventFlag);

            $("#eventDate").html(res.eventStDate + " ~ " + res.eventEdDate);
            $("#giveDate").html(res.giveStDate + " ~ " + res.giveEdDate);
            $("#returnDate").html(res.returnStDate + " ~ " + res.returnEdDate);
            $("#useYn").val(res.useYn);
            $("#purchaseGuide").val(res.purchaseGuide).setTextLength("#purchaseGuideLen");

            if (!$.jUtil.isEmpty(res.intervalList)) {
                ersPromoIntervalGrid.setData(res.intervalList);
                $("#intervalCount").html(res.intervalList.length);
            }

            if (!$.jUtil.isEmpty(res.storeList)) {
                ersPromoStoreGrid.setData(res.storeList);
                $("#storeCount").html(res.storeList.length);
            }

            if (!$.jUtil.isEmpty(res.itemList)) {
                ersPromoItemGrid.setData(res.itemList);
                $("#itemCount").html(res.itemList.length);
            }
        }
    }
    /**
     * 일괄 사용여부 버튼
     * @param useFlag
     */
    , massOnlineUseBtn : function (useFlag) {
        var eventCdList = ersPromoGrid.getCheckEventCd();

        if (eventCdList.length == 0) {
            alert("일괄적용할 사은행사를 선택하세요.");
            return;
        }

        var msg = "";

        switch (useFlag) {
            case "Y" :
                msg = "일괄 사용으로 변경하시겠습니까?";
                break;
            case "N" :
                msg = "일괄 사용안함으로 변경하시겠습니까?";
                break;
        }

        if (confirm(msg)) {
            // setErsPromoMassOnlineUseYn
            var dataParam = {
                "eventCdList" : eventCdList
                , "useYn" : useFlag
            };

            CommonAjax.basic({
                url: "/event/setErsPromoMassOnlineUseYn.json"
                , data: JSON.stringify(dataParam)
                , method: "POST"
                , contentType: "application/json; charset=utf-8"
                , successMsg: "사용여부 적용이 완료되었습니다."
                , callbackFunc: function () {
                    ersPromoSearch.searchErsPromoList();
                }
            });
        }
    }
    /**
     * 사은행사 저장
     */
    , setErsPromo : function () {
        if (ersPromo.setPromoValid()) {
            if (confirm("수정하시겠습니까?")) {
                // 사은행사 정보
                var dataParam = {
                    "eventCd": ersPromo.selectEventCd,
                    "useYn": $("#useYn").val(),
                    "purchaseGuide": $("#purchaseGuide").val()
                };

                CommonAjax.basic({
                    url: "/event/setErsPromo.json"
                    , data: JSON.stringify(dataParam)
                    , method: "POST"
                    , contentType: "application/json; charset=utf-8"
                    , successMsg: "수정되었습니다."
                    , callbackFunc: function () {
                        ersPromoSearch.searchErsPromoList(ersPromo.selectEventCd);
                    }
                });
            }
        }
    }
    /**
     * 사은행사 저장 시 유효성 검사
     * @returns {*|boolean|boolean}
     */
    , setPromoValid : function () {
        if ($.jUtil.isEmpty(ersPromo.selectEventCd)) {
            return $.jUtil.alert("선택된 사은행사가 없습니다.");
        }

        if ($.jUtil.isEmpty($("#purchaseGuide").val()) || $("#purchaseGuide").val().length < 10) {
            return $.jUtil.alert("구매 가이드를 10자 이상 입력해 주세요.");
        }

        return true;
    }
};