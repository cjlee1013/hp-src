/**
 * 기획전관리, 이벤트관리 에서 사용, 그리드 관련 컨트롤 분리
 * */

// 기획전관리 조회 그리드
var exhPromoGrid = {
    gridView : new RealGridJS.GridView("exhPromoGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        exhPromoGrid.initExhPromoGrid();
        exhPromoGrid.initDataProvider();
        exhPromoGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initExhPromoGrid : function() {
        exhPromoGrid.gridView.setDataSource(exhPromoGrid.dataProvider);
        exhPromoGrid.gridView.setStyles(exhGridBaseInfo.realgrid.styles);
        exhPromoGrid.gridView.setDisplayOptions(exhGridBaseInfo.realgrid.displayOptions);
        exhPromoGrid.gridView.setColumns(exhGridBaseInfo.realgrid.columns);
        exhPromoGrid.gridView.setOptions(exhGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(exhPromoGrid.gridView, "couponUseYn", useYnJson);
        setColumnLookupOptionForJson(exhPromoGrid.gridView, "dispYn", dispYnJson);
        setColumnLookupOptionForJson(exhPromoGrid.gridView, "useYn", useYnJson);
        setColumnLookupOptionForJson(exhPromoGrid.gridView, "siteType", siteTypeJson);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        exhPromoGrid.dataProvider.setFields(exhGridBaseInfo.dataProvider.fields);
        exhPromoGrid.dataProvider.setOptions(exhGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        exhPromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = exhPromoGrid.dataProvider.getJsonRow(exhPromoGrid.gridView.getCurrent().dataRow);
            exhPromo.getExhPromoDetail(gridRow.promoNo);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        exhPromoGrid.dataProvider.clearRows();
        exhPromoGrid.dataProvider.setRows(dataList);
    }
};

// 쿠폰 조회 그리드
var couponGrid = {
    gridView : new RealGridJS.GridView("couponGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        couponGrid.initExhCouponGrid();
        couponGrid.initDataProvider();
        couponGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initExhCouponGrid : function() {
        couponGrid.gridView.setDataSource(couponGrid.dataProvider);
        couponGrid.gridView.setStyles(exhCouponGridBaseInfo.realgrid.styles);
        couponGrid.gridView.setDisplayOptions(exhCouponGridBaseInfo.realgrid.displayOptions);
        couponGrid.gridView.setColumns(exhCouponGridBaseInfo.realgrid.columns);
        couponGrid.gridView.setOptions(exhCouponGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(couponGrid.gridView, "couponType", couponTypeJson);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        couponGrid.dataProvider.setFields(exhCouponGridBaseInfo.dataProvider.fields);
        couponGrid.dataProvider.setOptions(exhCouponGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        couponGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        couponGrid.dataProvider.clearRows();
        couponGrid.dataProvider.setRows(dataList);

        exhPromo.setCouponCnt();
    }
    /**
     * 그리드에 데이터 추가
     * @param data
     */
    , appendData : function(data) {
        var currentDatas = couponGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        var couponCnt = couponGrid.dataProvider.getRowCount();

        couponGrid.dataProvider.beginUpdate();
        try {
            data.forEach(function (value, index, array1) {
                if (couponCnt < 10) {
                    if (currentDatas.findIndex(function (currentData) {
                        return currentData.couponNo === value.couponNo;
                    }) == -1) {
                        couponGrid.dataProvider.addRow(value);
                        couponCnt++;
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ["couponNo"],
                            values: [value.couponNo]
                        };

                        var _row = couponGrid.dataProvider.searchDataRow(options);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (couponGrid.dataProvider.getRowState(_row) == "deleted") {
                            couponGrid.dataProvider.setRowState(_row, "none");
                        }
                    }
                } else {
                    return false;
                }
            });
        } finally {
            couponGrid.dataProvider.endUpdate(true);
        }

        exhPromo.setCouponCnt();
    }
    /**
     * 선택한 row 삭제
     */
    , removeCheckData : function() {
        var check = couponGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        couponGrid.dataProvider.removeRows(check, false);

        exhPromo.setCouponCnt();
    }
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    , moveRow : function(obj) {
        var checkedRows = couponGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 쿠폰을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(couponGrid, $(obj).attr("key"), checkedRows);
    }
};

// 상품 분류 조회 그리드
var themeGrid = {
    selectedRowId : null

    , gridView : new RealGridJS.GridView("themeGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        themeGrid.initExhCouponGrid();
        themeGrid.initDataProvider();
        themeGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initExhCouponGrid : function() {
        themeGrid.gridView.setDataSource(themeGrid.dataProvider);
        themeGrid.gridView.setStyles(exhThemeGridBaseInfo.realgrid.styles);
        themeGrid.gridView.setDisplayOptions(exhThemeGridBaseInfo.realgrid.displayOptions);
        themeGrid.gridView.setColumns(exhThemeGridBaseInfo.realgrid.columns);
        themeGrid.gridView.setOptions(exhThemeGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(themeGrid.gridView, "themeDispYn", dispYnJson);
        setColumnLookupOption(themeGrid.gridView, "storeType", $("#storeType"));
        setColumnLookupOption(themeGrid.gridView, "sortKind", $("#sortKind"));
        setColumnLookupOption(themeGrid.gridView, "pcTemplate", $("#pcTemplate"));
        setColumnLookupOption(themeGrid.gridView, "mobileTemplate", $("#mobileTemplate"));
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        themeGrid.dataProvider.setFields(exhThemeGridBaseInfo.dataProvider.fields);
        themeGrid.dataProvider.setOptions(exhThemeGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        themeGrid.gridView.onDataCellClicked = function(gridView, index) {
            themeGrid.selectedRowId = themeGrid.gridView.getCurrent().dataRow;
            var gridRow = themeGrid.dataProvider.getJsonRow(themeGrid.selectedRowId);
            exhPromo.theme.getTheme(gridRow);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        themeGrid.dataProvider.clearRows();
        themeGrid.dataProvider.setRows(dataList);

        exhPromo.theme.setThemeCnt();
    }
    /**
     * 선택한 row 삭제
     */
    , removeCheckData : function() {
        var check = themeGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        themeGrid.dataProvider.removeRows(check, false);

        exhPromo.theme.setThemeCnt();
    }
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    , moveRow : function(obj) {
        var checkedRows = themeGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품분류를 선택해 주세요.");
            return false;
        }

        realGridMoveRow(themeGrid, $(obj).attr("key"), checkedRows);
    }
    , getRowId : function (themeNo, tempThemeNo) {
        var rowId;
        var options;

        if (!$.jUtil.isEmpty(themeNo)) {
            options = {
                startIndex: 0,
                fields: ["themeNo"],
                values: [themeNo]
            };
        } else {
            options = {
                startIndex: 0,
                fields: ["tempThemeNo"],
                values: [tempThemeNo]
            };
        }

        themeGrid.selectedRowId = themeGrid.dataProvider.searchDataRow(options);
    }
};

// 상품 조회 그리드
var itemGrid = {
    gridView : new RealGridJS.GridView("itemGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        itemGrid.initExhCouponGrid();
        itemGrid.initDataProvider();
        itemGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initExhCouponGrid : function() {
        itemGrid.gridView.setDataSource(itemGrid.dataProvider);
        itemGrid.gridView.setStyles(exhItemGridBaseInfo.realgrid.styles);
        itemGrid.gridView.setDisplayOptions(exhItemGridBaseInfo.realgrid.displayOptions);
        itemGrid.gridView.setColumns(exhItemGridBaseInfo.realgrid.columns);
        itemGrid.gridView.setOptions(exhItemGridBaseInfo.realgrid.options);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        itemGrid.dataProvider.setFields(exhItemGridBaseInfo.dataProvider.fields);
        itemGrid.dataProvider.setOptions(exhItemGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        itemGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        itemGrid.dataProvider.clearRows();
        itemGrid.dataProvider.setRows(dataList);

        exhPromo.theme.setItemCnt();
    }
    /**
     * 그리드에 데이터 추가
     * @param data
     */
    , appendData : function(data) {
        var currentDatas = itemGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        var itemCnt = itemGrid.dataProvider.getRowCount();

        itemGrid.dataProvider.beginUpdate();

        try {
            data.forEach(function (value, index, array1) {
                if (itemCnt < 300) {
                    if (currentDatas.findIndex(function (currentData) {
                        return currentData.itemNo === value.itemNo;
                    }) == -1) {
                        itemGrid.dataProvider.addRow(value);
                        itemCnt++;
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ["itemNo"],
                            values: [value.itemNo]
                        };

                        var _row = itemGrid.dataProvider.searchDataRow(options);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (itemGrid.dataProvider.getRowState(_row) == "deleted") {
                            itemGrid.dataProvider.setRowState(_row, "none");
                        }
                    }
                } else {
                    return false;
                }
            });
        } finally {
            itemGrid.dataProvider.endUpdate(true);
        }

        exhPromo.theme.setItemCnt();
    }
    /**
     * 선택한 row 삭제
     */
    , removeCheckData : function () {
        var check = itemGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        itemGrid.dataProvider.removeRows(check, false);
        exhPromo.theme.setItemCnt();
    }
    , removeAll : function () {
        itemGrid.dataProvider.clearRows();
        exhPromo.theme.setItemCnt();
    }
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    , moveRow : function(obj) {
        var checkedRows = itemGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(itemGrid, $(obj).attr("key"), checkedRows);
    }
    /**
     * 그리드 엑셀 다운로드
     * @returns {boolean}
     */
    , excelDownload: function(promoNo) {
        if(itemGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "기획전관리_상품내역(기획전번호-" + promoNo + ")_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        itemGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};