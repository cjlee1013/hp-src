/**
 * 프로모션관리 > 행사관리 > 덤 행사 관리 그리드 js
 */

// 조회 그리드
var etcPromoGrid = {
    gridView : new RealGridJS.GridView("etcPromoGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        etcPromoGrid.initEtcPromoGrid();
        etcPromoGrid.initDataProvider();
        etcPromoGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initEtcPromoGrid : function() {
        etcPromoGrid.gridView.setDataSource(etcPromoGrid.dataProvider);
        etcPromoGrid.gridView.setStyles(etcPromoGridBaseInfo.realgrid.styles);
        etcPromoGrid.gridView.setDisplayOptions(etcPromoGridBaseInfo.realgrid.displayOptions);
        etcPromoGrid.gridView.setColumns(etcPromoGridBaseInfo.realgrid.columns);
        etcPromoGrid.gridView.setOptions(etcPromoGridBaseInfo.realgrid.options);

        setColumnLookupOption(etcPromoGrid.gridView, "storeType", $("#storeType"));
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        etcPromoGrid.dataProvider.setFields(etcPromoGridBaseInfo.dataProvider.fields);
        etcPromoGrid.dataProvider.setOptions(etcPromoGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        etcPromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = etcPromoGrid.dataProvider.getJsonRow(etcPromoGrid.gridView.getCurrent().dataRow);
            etcPromo.getEtcPromoDetail(gridRow.etcNo);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        etcPromoGrid.dataProvider.clearRows();
        etcPromoGrid.dataProvider.setRows(dataList);
    }
};

// 상품 그리드
var etcPromoItemGrid = {
    gridView : new RealGridJS.GridView("etcPromoItemGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        etcPromoItemGrid.initEtcPromoItemGrid();
        etcPromoItemGrid.initDataProvider();
        etcPromoItemGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initEtcPromoItemGrid : function() {
        etcPromoItemGrid.gridView.setDataSource(etcPromoItemGrid.dataProvider);
        etcPromoItemGrid.gridView.setStyles(etcPromoItemGridBaseInfo.realgrid.styles);
        etcPromoItemGrid.gridView.setDisplayOptions(etcPromoItemGridBaseInfo.realgrid.displayOptions);
        etcPromoItemGrid.gridView.setColumns(etcPromoItemGridBaseInfo.realgrid.columns);
        etcPromoItemGrid.gridView.setOptions(etcPromoItemGridBaseInfo.realgrid.options);

        setColumnLookupOption(etcPromoItemGrid.gridView, "dispYn", $("#dispYn"));
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        etcPromoItemGrid.dataProvider.setFields(etcPromoItemGridBaseInfo.dataProvider.fields);
        etcPromoItemGrid.dataProvider.setOptions(etcPromoItemGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        etcPromoItemGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = etcPromoItemGrid.dataProvider.getJsonRow(etcPromoItemGrid.gridView.getCurrent().dataRow);
            etcPromo.setItemInfo(gridRow);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        etcPromoItemGrid.dataProvider.clearRows();
        etcPromoItemGrid.dataProvider.setRows(dataList);

        etcPromo.setItemCnt();
    }
    /**
     * 그리드에 데이터 추가
     * @param data
     */
    , appendData : function(data, flag) {
        var currentDatas = etcPromoItemGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        etcPromoItemGrid.dataProvider.beginUpdate();

        try {
            data.forEach(function (value, index, array1) {
                if (currentDatas.findIndex(function (currentData) {
                    return currentData.itemNo === value.itemNo;
                }) == -1) {
                    // 전시여부 - 전시함 default setting
                    if (flag == "POP") {
                        value.dispYn = "Y";
                    }

                    etcPromoItemGrid.dataProvider.addRow(value);
                } else {
                    var options = {
                        startIndex: 0,
                        fields: ["itemNo"],
                        values: [value.itemNo]
                    };

                    var _row = etcPromoItemGrid.dataProvider.searchDataRow(options);

                    // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                    if (etcPromoItemGrid.dataProvider.getRowState(_row) == "deleted") {
                        etcPromoItemGrid.dataProvider.setRowState(_row, "none");
                    }

                    if (flag == "POP") {
                        value.dispYn = "Y";
                    }

                    etcPromoItemGrid.dataProvider.setValue(_row, "dispYn", value.dispYn);
                }
            });
        } finally {
            etcPromoItemGrid.dataProvider.endUpdate(true);
        }

        etcPromo.setItemCnt();
    }
    , getGridStateData : function (state) {
        var itemList = etcPromoItemGrid.dataProvider.getStateRows(state);
        var returnList = new Array();

        itemList.forEach(function(_row, index) {
            var _data = etcPromoItemGrid.dataProvider.getJsonRow(_row);
            var itemObj = new Object();

            itemObj.itemNo = _data.itemNo;
            itemObj.dispYn = _data.dispYn;

            returnList.push(itemObj);
        });

        return returnList;
    }
};