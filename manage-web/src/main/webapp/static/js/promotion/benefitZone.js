/**
 * 사이트관리 > 전문관관리 > 혜택존 전시 관리 메인 js
 */
$(document).ready(function () {
    benefitZoneGrid.init();
    CommonAjaxBlockUI.global();
    benefitZoneSearch.init();
    benefitZone.init();
});

// 그리드
var benefitZoneGrid = {
    gridView : new RealGridJS.GridView("benefitZoneGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        benefitZoneGrid.initBenefitZoneGrid();
        benefitZoneGrid.initDataProvider();
        benefitZoneGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initBenefitZoneGrid : function() {
        benefitZoneGrid.gridView.setDataSource(benefitZoneGrid.dataProvider);
        benefitZoneGrid.gridView.setStyles(benefitZoneGridBaseInfo.realgrid.styles);
        benefitZoneGrid.gridView.setDisplayOptions(benefitZoneGridBaseInfo.realgrid.displayOptions);
        benefitZoneGrid.gridView.setColumns(benefitZoneGridBaseInfo.realgrid.columns);
        benefitZoneGrid.gridView.setOptions(benefitZoneGridBaseInfo.realgrid.options);

        setColumnLookupOption(benefitZoneGrid.gridView, "dispLoc", $("#dispLoc"));
        setColumnLookupOption(benefitZoneGrid.gridView, "deviceType", $("#deviceType"));
        setColumnLookupOption(benefitZoneGrid.gridView, "dispYn", $("#dispYn"));
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        benefitZoneGrid.dataProvider.setFields(benefitZoneGridBaseInfo.dataProvider.fields);
        benefitZoneGrid.dataProvider.setOptions(benefitZoneGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        benefitZoneGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = benefitZoneGrid.dataProvider.getJsonRow(benefitZoneGrid.gridView.getCurrent().dataRow);
            benefitZone.getBenefitZoneDetail(gridRow.benefitNo);
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        benefitZoneGrid.dataProvider.clearRows();
        benefitZoneGrid.dataProvider.setRows(dataList);
    },
};

var benefitZoneSearch = {
    init : function () {
        benefitZoneSearch.searchFormReset();
        benefitZoneSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();

        // 조회 기간 초기화
        benefitZone.calendar.initCalendarDate(benefitZoneSearch, "schStartDt", "schEndDt", "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(benefitZoneSearch.searchBenefitZoneList);
        $("#schResetBtn").bindClick(benefitZoneSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                benefitZoneSearch.searchBenefitZoneList();
            }
        });
    }
    /**
     * 혜택존 정보 조회
     */
    , searchBenefitZoneList : function (benefitNo) {
        // 검색조건 validation check
        if (benefitZone.valid.searchValid()) {
            // 혜택존 정보 조회 개수 초기화
            $("#benefitTotalCnt").html("0");

            // 조회
            var searchForm = $("#searchForm").serialize();

            CommonAjax.basic({
                url : "/benefitZone/getBenefitZoneList.json"
                , data : searchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    benefitZoneGrid.setData(res);

                    $("#benefitTotalCnt").html($.jUtil.comma(benefitZoneGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(benefitNo)) {
                        var rowIndex = benefitZoneGrid.dataProvider.searchDataRow({fields : ["benefitNo"], values : [benefitNo]});

                        if (rowIndex > -1) {
                            benefitZoneGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            benefitZoneGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

var benefitZone = {
    selectBenefitNo : null
    , uploadType : 'IMG'

    /**
     * 혜택존 상세 초기화
     */
    , init : function () {
        benefitZone.bindingEvent();
        benefitZone.benefitZoneFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(benefitZone.setBenefitZone);             // 저장
        $("#resetBtn").bindClick(benefitZone.benefitZoneFormReset);     // 초기화
        $("#siteType").bindChange(benefitZone.view.changeSiteType);     // 사이트구분 변경 시
        $("#deviceType").bindChange(benefitZone.view.changeDeviceType); // 디바이스 변경 시
        $("#dispLoc").bindChange(benefitZone.view.changeDispLoc);       // 전시위치 변경 시
        $("#linkType").bindChange(benefitZone.view.chagneLinkInfo);     // 링크 유형 변경 시
        $("#storeType").bindChange(benefitZone.view.changeStoreType);   // 점포유형 유형 변경 시
        $("#dlvStoreYn").bindClick(benefitZone.valid.dlvStoreYnValid);  // 택배점 설정 선택 시
        $("#getLinkInfoBtn").bindClick(benefitZone.linkPop);            // 링크 조회

        // 이미지 등록
        $("input[name='fileArr']").bindChange(benefitZone.img.addImg);

        // 이미지 삭제
        $("#bannerImgDelBtn").click(function () {
            if (confirm("삭제하시겠습니까?")) {
                benefitZone.img.deleteImg($(this).siblings(".uploadType"));
            }
        });

        // 이미지 미리보기
        $(".uploadType").bindClick(benefitZone.img.imagePreview);

        // 혜택존 전시순서 숫자만 입력
        $("#priority").allowInput("keyup", ["NUM"], $(this).attr("id"));

        // 입력 문자 길이 계산
        $("#bannerNm").calcTextLength("keyup", "#bannerNmLen");
    }
    /**
     * 혜택존 상세 내용 초기화
     */
    , benefitZoneFormReset : function () {
        $("#benefitZoneForm").resetForm();

        // 전시 기간 초기화
        benefitZone.calendar.initCalendarDate(benefitZoneSearch, "dispStartDt", "dispEndDt", "1d", "15d");

        // span 초기화
        $("#benefitZoneTotalCnt").html("0");
        $("#bannerNmLen").html("0");

        // 사이트 구분에 따른 링크유형 컨트롤
        $("#siteType").trigger("change");

        // 디바이스에 따른 전시위치 컨트롤
        $("#deviceType").trigger("change");

        // 링크 유형에 따른 UI 변경
        $("#linkType").trigger("change");

        // 혜택존 번호 초기화
        benefitZone.selectBenefitNo = null;

        // 이미지 초기화
        benefitZone.img.init();
    }
    /**
     * 링크 조회
     */
    , linkPop : function () {
        var callback = "callback=" + "benefitZone.callbackLinkInfo";
        var uri;
        var popWidth = "1084";
        var popHeight;
        var target;

        switch ($("#linkType").val()) {
            case "EXH" :
            case "EVENT" :
                uri = "/common/popup/promoPop?" + callback + "&promoType=" + $("#linkType").val() + "&siteType=" + $("#siteType").val() + "&isMulti=N";

                if (!$.jUtil.isEmpty($("#linkNo").val())) {
                    uri += "&promoNo=" + $("#linkNo").val();
                }

                popWidth = "1084";
                popHeight = "570";
                target = "promoPop";
                break;
            case "ITEM_DS" :
            case "ITEM_TD" :
                var mallType, storeType;

                if ($("#linkType").val() == "ITEM_DS") {
                    mallType = storeType = "DS";
                } else {
                    mallType = "TD";
                    storeType = $("#storeType").val();
                }

                uri = "/common/popup/itemPop?" + callback + "&isMulti=N&mallType=" + mallType + "&siteType=" + $("#siteType").val() + "&storeType=" + storeType;
                popHeight = "650";
                target = "promoItemPopup";
                break;
        }

        windowPopupOpen(uri, target, popWidth, popHeight);
    }
    /**
     * 링크 검색 팝업 콜백 (공통팝업)
     * @param promotion
     */
    , callbackLinkInfo : function (promotion) {
        var linkType = $("#linkType").val();

        switch (linkType) {
            case "EXH" :
            case "EVENT" :
                $("#linkNo").val(promotion[0].promoNo);
                $("#linkNm").val(promotion[0].promoNm);
                break;
            case "ITEM_DS" :
            case "ITEM_TD" :
                $("#linkNo").val(promotion[0].itemNo);
                $("#linkNm").val(promotion[0].itemNm);
                break;
        }
    }
    , getBenefitZoneDetail : function (benefitNo) {
        // 혜택존 상세 초기화
        benefitZone.benefitZoneFormReset();

        benefitZone.selectBenefitNo = benefitNo;

        CommonAjax.basic({
            url:"/benefitZone/getBenefitZoneDetail.json"
            , data : {benefitNo : benefitNo}
            , method : "GET"
            , callbackFunc : function (res) {
                benefitZone.detailCallback(res);
            }
        });
    }
    /**
     * 혜택존 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param benefitInfo
     * @returns {boolean}
     */
    , detailCallback : function (benefitInfo) {
        if ($.jUtil.isEmpty(benefitInfo)) {
            return true;
        }

        // 기본정보
        $("#siteType").val(benefitInfo.siteType).trigger("change");
        $("#deviceType").val(benefitInfo.deviceType);
        $("#dispLoc").val(benefitInfo.dispLoc).trigger("change");
        $("#priority").val(benefitInfo.priority);
        $("#dispStartDt").val(benefitInfo.dispStartDt);
        $("#dispEndDt").val(benefitInfo.dispEndDt);
        $("#bannerNm").val(benefitInfo.bannerNm).setTextLength("#bannerNmLen");
        $("#dispYn").val(benefitInfo.dispYn);

        // 링크 유형에 따른 UI 변경
        $("#linkType").val(benefitInfo.linkType).trigger("change");

        switch (benefitInfo.linkType) {
            case "URL" :
                $("#urlPath").val(benefitInfo.linkInfo);
                break;
            case "NONE" :
                break;
            default :
                if (benefitInfo.linkType == "ITEM_TD") {
                    $("#storeType").val(benefitInfo.storeType).trigger("change");
                    $("#dlvStoreYn").prop("checked", benefitInfo.dlvStoreYn == "Y");
                    $("#dlvStoreId").val(benefitInfo.dlvStoreId);
                }

                $("#linkNo").val(benefitInfo.linkInfo);
                $("#linkNm").val(benefitInfo.linkNm);
                break;
        }

        // 배너이미지
        for (var bnIdx in benefitInfo.imgList) {
            var imgData = benefitInfo.imgList[bnIdx];

            benefitZone.setImgDisplayData(imgData);
        }
    }
    /**
     * 혜택존 상세 - 이미지 영역 데이터 채우기
     * @param imgData
     */
    , setImgDisplayData : function (imgData) {
        var processKey = benefitZone.img.getProcessKey();

        // 파일명, 사이즈 등은 임시로 넣음
        benefitZone.img.setImg($("#bannerImg"), {
            "imgUrl" : imgData.imgUrl
            , "imgWidth" : imgData.imgWidth
            , "imgHeight" : imgData.imgHeight
            , "src" : hmpImgUrl + "/provide/view?processKey=" + processKey + "\&fileId=" + imgData.imgUrl
            , "changeYn" : "N"
        });
    }
    /**
     * 혜택존 저장
     */
    , setBenefitZone : function () {
        if (benefitZone.valid.setBenefitZoneValid()) {
            var benefitZoneForm = $("#benefitZoneForm").serializeObject(true);

            // 혜택존 번호
            benefitZoneForm.benefitNo = $.jUtil.isEmpty(benefitZone.selectBenefitNo) ? null : Number(benefitZone.selectBenefitNo);

            // 이미지 디바이스 타입
            benefitZoneForm.imgList[0].deviceType = $("#deviceType").val();

            // 링크 정보
            switch ($("#linkType").val()) {
                case "URL" :
                    benefitZoneForm.linkInfo = $("#urlPath").val();
                    break;
                case "NONE" :
                    benefitZoneForm.linkInfo = null;
                    break;
                default :
                    benefitZoneForm.linkInfo = $("#linkNo").val();
                    break;
            }

            // 혜택존 저장 호출
            CommonAjax.basic({
                url : "/benefitZone/setBenefitZone.json"
                , data : JSON.stringify(benefitZoneForm)
                , method : "POST"
                , contentType: 'application/json'
                , successMsg : "저장되었습니다."
                , callbackFunc : function (res) {
                    benefitZoneSearch.searchBenefitZoneList(res);
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        benefitZoneSearch.searchBenefitZoneList(benefitZone.selectBenefitNo);
                    }
                }
            });
        }
    }
};

benefitZone.calendar = {
    initCalendarDate : function (targetObj, startId, endId, startFlag, endFlag) {
        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        targetObj.setDate = Calendar.datePickerRange(startId, endId);
        targetObj.setDate.setStartDate(startFlag);
        targetObj.setDate.setEndDate(endFlag);

        endObj.datepicker("option", "minDate", startObj.val());
    }
};

benefitZone.view = {
    /**
     * 사이트구분 변경 시 링크의 점포유형 select box 컨트롤
     * @param obj
     */
    changeSiteType : function (obj) {
        var siteType = $(obj).val();

        benefitZone.view.addSelectOption($("#linkType"), siteType);

        // 링크유형 변경으로 인한 UI 컨트롤
        $("#linkType").trigger("change");
    }
    , changeDeviceType : function (obj) {
        var deviceType = $(obj).val();

        benefitZone.view.addSelectOption($("#dispLoc"), deviceType);
    }
    /**
     * select box option 삭제 후 조건에 맞는 option 추가
     * @param obj
     * @param siteType
     * @returns {boolean}
     */
    , addSelectOption : function (obj, typeCode) {
        var jsonData;
        var objId = $(obj).attr("id");

        switch (objId) {
            case "linkType" :
                jsonData = linkTypeJson
                break;
            case "dispLoc" :
                jsonData = dispLocJson
                break;
            case "storeType" :
                jsonData = storeTypeJson
                break;
        }

        if (jsonData == "") return false;

        $(obj).find("option").remove();

        $.each(jsonData, function (key, code) {
            var chkOption = 0;
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            var codeRef2 = code.ref2;

            $(obj).find("option").each(function () {
                if ($(this).val() == codeCd) {
                    chkOption++;
                }
            });

            if (chkOption == 0) {
                if (objId == "storeType") {
                    // 점포상품일 경우만 해당하므로 DS 코드는 제거
                    if (codeRef2 == typeCode && codeCd != "DS"){
                        $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                    }
                } else {
                    if (codeRef1 == "ALL" || codeRef1 == typeCode){
                        $(obj).append("<option value=" + codeCd + ">" + codeNm + "</option>");
                    }
                }
            }
        });

        if (objId == "dispLoc"){
            $(obj).trigger("change");
        }
    }
    /**
     * 링크유형 변경 시 UI 변경
     */
    , chagneLinkInfo : function (obj) {
        var linkType = $(obj).val();

        switch (linkType) {
            case "URL" :
                $("#storeType").val("").hide();
                $("#linkNo").val("").hide();
                $("#linkNm").val("").hide();
                $("#getLinkInfoBtn").hide();
                $("#urlPath").show();
                $("#storeChecklb").val("").hide();
                break;
            case "NONE" :
                $("#storeType").val("").hide();
                $("#linkNo").val("").hide();
                $("#linkNm").val("").hide();
                $("#getLinkInfoBtn").hide();
                $("#urlPath").val("").hide();
                $("#storeChecklb").val("").hide();
                break;
            case "ITEM_TD" :
                $("#storeType").val("").show();
                $("#linkNo").val("").show();
                $("#linkNm").val("").show();
                $("#getLinkInfoBtn").show();
                $("#urlPath").val("").hide();
                $("#storeChecklb").val("").show();

                benefitZone.view.selectItemTDType();
                break;
            default :
                $("#storeType").val("").hide();
                $("#linkNo").val("").show();
                $("#linkNm").val("").show();
                $("#getLinkInfoBtn").show();
                $("#urlPath").val("").hide();
                $("#storeChecklb").val("").hide();
                break;
        }
    }
    /**
     * 점포상품 링크 선택시 - 점포 유형 select box, 택배점포 select box 설정
     */
    , selectItemTDType : function () {
        // 사이트 유형 별 점포 유형
        benefitZone.view.addSelectOption($("#storeType"), $("#siteType").val());

        // 점포 유형 별 택배점포
        benefitZone.view.changeStoreType();
    }
    /**
     * 점포유형 변경 시 - 택배점포 설정
     */
    , changeStoreType : function () {
        $("#dlvStoreId").find("option").remove();
        $("#dlvStoreId").append("<option value=''>택배점</option>");
        $("#linkNo, #linkNm").val("");

        if ($("#storeType").val() == "HYPER" || $("#storeType").val() == "CLUB") {
            var jsonData = dlvStoreListJson;

            if (jsonData == "") return false;

            $.each(jsonData, function (key, code) {
                var chkOption = 0;
                var storeType = code.storeType;
                var codeCd = code.storeId;
                var codeNm = code.storeNm;

                $("#dlvStoreId").find("option").each(function () {
                    if ($(this).val() == codeCd) {
                        chkOption++;
                    }
                });

                if (chkOption == 0) {
                    if (storeType == $("#storeType").val()) {
                        $("#dlvStoreId").append("<option value=" + codeCd + ">" + codeNm + "</option>");
                    }
                }
            });
        }
    }
    , changeDispLoc : function (obj) {
        var dispLoc = $(obj).val();

        switch (dispLoc) {
            case "TOP" :
                var deviceType = $("#deviceType").val();

                if (deviceType == "PC") {
                    $("#imgSapn").html("1200 X 450");
                    $("#imgSizeSapn").html("5");
                } else {
                    $("#imgSapn").html("750 X 460");
                    $("#imgSizeSapn").html("2");
                }

                break;
            case "BOTT_L" :
                $("#imgSapn").html("380 X 230");
                $("#imgSizeSapn").html("2");
                break;
            case "BOTT_R" :
                $("#imgSapn").html("380 X 230");
                $("#imgSizeSapn").html("2");
                break;
        }
    }
};

/**
 * 혜택존 유효성 검사 관련
 */
benefitZone.valid = {
    searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'BENEFITNO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    }
    /**
     * 택배점 설정 검사
     */
    , dlvStoreYnValid : function(obj){
        if($(obj).is(":checked")){
            var storeTyep = $("#storeType").val();
            var linkNo = $("#linkNo").val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB' ){
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                return false;
            }

            if ($.jUtil.isEmpty(linkNo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                return false;
            }
        } else{
            benefitZone.view.changeStoreType();
        }
    }
    , setBenefitZoneValid : function () {
        if ($.jUtil.isEmpty($("#bannerNm").val())) {
            return $.jUtil.alert("배너명을 입력해주세요.");
        }

        var nowDt = moment().format("YYYY-MM-DD");

        // 전시시작기간
        if (!moment($("#dispStartDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("전시시작 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#dispEndDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("전시종료 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 링크 정보 확인
        if ($("#linkType").val() != "NONE") {
            switch ($("#linkType").val()) {
                case "URL" :
                    if ($.jUtil.isEmpty($("#urlPath").val())) {
                        return $.jUtil.alert("링크 URL이 입력되지 않았습니다.")
                    }
                    break;
                default :
                    if ($.jUtil.isEmpty($("#linkNo").val())) {
                        return $.jUtil.alert("링크정보를 선택해주세요.")
                    }
                    break;
            }
        }

        // 적용시스템 선택에 따른 이미지 등록 여부 확인
        if (($("#dlvStoreYn").is(":checked")) && $.jUtil.isEmpty($("#dlvStoreId").val())) {
            return $.jUtil.alert("택배점을 선택해주세요.");
        }

        // 이미지 확인
        if ($("input[name='imgList[].imgUrl']:input[value!='']").length < 1) {
            return $.jUtil.alert("이미지를 등록해 주세요.");
        }

        return true;
    }
};

/**
 * 혜택존 이미지 관련
 */
benefitZone.img = {
    imgDiv : null

    /**
     * 이미지 초기화
     */
    , init : function () {
        $(".uploadType").each(function (){benefitZone.img.setImg($(this))});
        benefitZone.img.imgDiv = null;
    }
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    , setImg : function (obj, params) {
        if (!$.jUtil.isEmpty(benefitZone.img.imgDiv)) {
            benefitZone.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        }

        if (params) {
            obj.find(".imgUrl").val(params.imgUrl);
            // obj.find(".imgNm").val(params.imgNm);
            obj.find(".imgWidth").val(params.imgWidth);
            obj.find(".imgHeight").val(params.imgHeight);
            obj.find(".imgUrlTag").prop("src", params.src);

            obj.parents(".imgDisplayView").show();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").hide();
        } else {
            obj.find(".imgUrl").val("");
            // obj.find(".imgNm").val("");
            obj.find(".imgWidth").val("");
            obj.find(".imgHeight").val("");
            obj.find(".imgUrlTag").prop("src", "");

            obj.parents(".imgDisplayView").hide();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").show();
        }
    }
    /**
     * 이미지 processKey
     * @returns {string}
     */
    , getProcessKey : function () {
        var disploc = $("#dispLoc").val();
        var processKey;

        switch (disploc) {
            case "TOP" :
                var deviceType = $("#deviceType").val();

                if (deviceType == "PC") {
                    processKey = "BenefitPCTopBanner";
                } else {
                    processKey = "BenefitMobileTopBanner";
                }
                break;
            case "BOTT_L" :
            case "BOTT_R" :
                processKey = "BenefitPCBottomBanner";
                break;
        }

        return processKey;
    }
    /**
     * 이미지 업로드
     * @param obj
     */
    , addImg : function () {
        var imgSize;

        if ($("#deviceType").val() == "PC" && $("#dispLoc").val() == "TOP") {
            imgSize = 5242880;
        } else {
            imgSize = 2097152;
        }

        if ($("#deviceType").val() == "PC" && $("#dispLoc").val() == "TOP" && $("#imgFile").get(0).files[0].size > 5242880) {
            return $.jUtil.alert("용량을 초과하였습니다.\n" + $("#imgSizeSapn").text() + "MB 이하로 업로드해주세요.");
        } else if ($("#imgFile").get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var processKey = benefitZone.img.getProcessKey();

        /**
         * @type {jQuery.fn.init|jQuery|HTMLElement}
         */
        var file = $("#imgFile");
        var params = {
            processKey : processKey,
            mode : "IMG"
        };

        var ext = $("#imgFile").get(0).files[0].name.split(".");

        CommonAjax.upload({
            file : file,
            data : params,
            callbackFunc : function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        benefitZone.img.setImg(benefitZone.img.imgDiv, {
                            "imgUrl" : f.fileStoreInfo.fileId
                            , "imgWidth" : f.fileStoreInfo.width
                            , "imgHeight" : f.fileStoreInfo.height
                            , "src"   : hmpImgUrl + "/provide/view?processKey=" + processKey + "\&fileId=" + f.fileStoreInfo.fileId
                            , "ext"   : ext[1]
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    }
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    , imagePreview : function (obj) {
        var imgUrl = $(obj).find("img").prop("src");
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function (obj) {
        benefitZone.img.setImg(obj, null);
    }
    /**
     * 이미지 클릭처리
     * @param obj
     */
    , clickFile : function (obj) {
        benefitZone.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        $("input[name=fileArr]").click();
    }
};