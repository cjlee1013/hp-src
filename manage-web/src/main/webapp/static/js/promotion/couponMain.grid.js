/**
 * 쿠폰관리(couponMain.jsp) 에서 사용, 그리드 관련 컨트롤 분리
 * */

// 쿠폰관리 조회 그리드
var couponListGrid = {
    gridView : new RealGridJS.GridView("couponListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        couponListGrid.initCouponListGrid();
        couponListGrid.initDataProvider();
        couponListGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initCouponListGrid : function() {
        couponListGrid.gridView.setDataSource(couponListGrid.dataProvider);
        couponListGrid.gridView.setStyles(couponListGridBaseInfo.realgrid.styles);
        couponListGrid.gridView.setDisplayOptions(couponListGridBaseInfo.realgrid.displayOptions);
        couponListGrid.gridView.setColumns(couponListGridBaseInfo.realgrid.columns);
        couponListGrid.gridView.setOptions(couponListGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(couponListGrid.gridView, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(couponListGrid.gridView, "couponType", couponTypeJson);
        setColumnLookupOptionForJson(couponListGrid.gridView, "status", couponStatusJson);
        setColumnLookupOptionForJson(couponListGrid.gridView, "purpose", couponPurposeJson);
        setColumnLookupOptionForJson(couponListGrid.gridView, "shareYn", useYnJson);
        setColumnLookupOptionForJson(couponListGrid.gridView, "departCd", couponCmsUnitJson);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        couponListGrid.dataProvider.setFields(couponListGridBaseInfo.dataProvider.fields);
        couponListGrid.dataProvider.setOptions(couponListGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        couponListGrid.gridView.onDataCellClicked = function(gridView, index) {
            coupon.copyYn = "N";
            coupon.getCouponDetail(couponListGrid.dataProvider.getJsonRow(couponListGrid.gridView.getCurrent().dataRow).couponNo, "N");
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        couponListGrid.dataProvider.clearRows();
        couponListGrid.dataProvider.setRows(dataList);
    },
    /**
     * 조회 그리드 엑셀 다운로드
     * @returns {boolean}
     */
    excelDownload: function() {
        if(couponListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        couponListGrid.gridView.exportGrid({
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 적용대상 그리드
var applyGrid = {
    gridView : new RealGridJS.GridView("applyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        applyGrid.initApplyGrid();
        applyGrid.initDataProvider();
    },
    /**
     * 적용대상 그리드 설정 초기화
     */
    initApplyGrid : function() {
        applyGrid.gridView.setDataSource(applyGrid.dataProvider);
        applyGrid.gridView.setStyles(applyGridBaseInfo.realgrid.styles);
        applyGrid.gridView.setDisplayOptions(applyGridBaseInfo.realgrid.displayOptions);
        applyGrid.gridView.setColumns(applyGridBaseInfo.realgrid.columns);
        var options = applyGridBaseInfo.realgrid.options;
        options.hideDeletedRows = true;
        applyGrid.gridView.setOptions(options);
    },
    /**
     * 적용대상 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        applyGrid.dataProvider.setFields(applyGridBaseInfo.dataProvider.fields);
        var options = applyGridBaseInfo.dataProvider.options;
        options.softDeleting = true;
        applyGrid.dataProvider.setOptions(options);
    },
    /**
     * 조회된 데이터 그리드에 적용
     * @param dataList
     */
    setData : function(dataList) {
        applyGrid.dataProvider.clearRows();
        applyGrid.clearAllFilter();
        applyGrid.dataProvider.setRows(dataList);
    },
    /**
     * 적용대상 그리드에 데이터 추가
     * @param data
     */
    appendData : function(data) {
        var currentDatas = applyGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        if (!currentDatas !== undefined && currentDatas.length > 0) {
            applyGrid.dataProvider.beginUpdate();
            try {
                data.forEach(function(value, index, array1) {
                    if (currentDatas.findIndex(function(currentData) { return currentData.applyCd === value.applyCd; }) == -1) {
                        applyGrid.dataProvider.addRow(value);
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ['applyCd'],
                            values: [value.applyCd]
                        };

                        var _row = applyGrid.dataProvider.searchDataRow(options);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (applyGrid.dataProvider.getRowState(_row) == 'deleted') {
                            applyGrid.dataProvider.setRowState(_row, 'none');
                        }
                    }
                });
            } finally {
                applyGrid.dataProvider.endUpdate(true);
            }
        } else {
            applyGrid.dataProvider.addRows(data);
        }
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = applyGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        applyGrid.dataProvider.removeRows(check, false);
    },
    /**
     * 적용대상 그리드 내 조회
     * @param searchColumnName
     * @param searchValue
     */
    filter : function(searchColumnName, searchValue) {
        // 사용하는 필터 제거active
        applyGrid.clearAllFilter();

        if (!searchColumnName || !searchValue)
            return;

        var filter = {};
        var filterArr = [];
        var activeFilterArr = [];

        switch(searchColumnName) {
            case "applyCd":
                $.each(searchValue, function(i) {
                    var value = searchValue[i];
                    filter = {name: value, criteria: "value = '" + value + "'"};
                    filterArr.push(filter);
                    activeFilterArr.push(value);
                });
                break;
            case "applyNm":
                filter = {name: searchValue, criteria: "value like '" + searchValue + "%'"};
                filterArr.push(filter);
                activeFilterArr.push(searchValue);
                break;
        }

        applyGrid.gridView.setColumnFilters(searchColumnName, filterArr);
        applyGrid.gridView.activateColumnFilters(searchColumnName, activeFilterArr, true);
    },
    /**
     * 적용대상 필터 제거
     */
    clearAllFilter : function() {
        applyGrid.gridView.clearColumnFilters("applyCd");
        applyGrid.gridView.clearColumnFilters("applyNm");
    },
    /**
     * 적용대상 그리드 엑셀다운로드
     * @returns {boolean}
     */
    excelDownload : function() {
        if (applyGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰_" + $("#applyScope").val().text() + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        applyGrid.gridView.exportGrid({
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 제외상품 그리드
var exceptGrid = {
    gridView : new RealGridJS.GridView("exceptGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        exceptGrid.initExceptGrid();
        exceptGrid.initDataProvider();
    },
    setDisplay : function() {
        var applyScope = $("#applyScope").val();
        var storeType = $("#storeType").val();
        var couponType = $("#couponType").val();

        if (couponType != COUPON_TYPE_SHIP && (applyScope == COUPON_APPLY_SCOPE_ALL || applyScope == COUPON_APPLY_SCOPE_CATE || applyScope == COUPON_APPLY_SCOPE_SELL)) {
            $("#exceptGridArea").show();
            this.gridView.resetSize();
        } else {
            $("#exceptGridArea").hide();
            exceptGrid.dataProvider.clearRows();
        }
    },
    /**
     * 적용대상 그리드 설정 초기화
     */
    initExceptGrid : function() {
        exceptGrid.gridView.setDataSource(exceptGrid.dataProvider);
        exceptGrid.gridView.setStyles(exceptGridBaseInfo.realgrid.styles);
        exceptGrid.gridView.setDisplayOptions(exceptGridBaseInfo.realgrid.displayOptions);
        exceptGrid.gridView.setColumns(exceptGridBaseInfo.realgrid.columns);
        var options = exceptGridBaseInfo.realgrid.options;
        options.hideDeletedRows = true;
        exceptGrid.gridView.setOptions(options);
    },
    /**
     * 적용대상 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        exceptGrid.dataProvider.setFields(exceptGridBaseInfo.dataProvider.fields);
        var options = exceptGridBaseInfo.dataProvider.options;
        options.softDeleting = true;
        exceptGrid.dataProvider.setOptions(options);
    },
    /**
     * 조회된 데이터 그리드에 적용
     * @param dataList
     */
    setData : function(dataList) {
        exceptGrid.dataProvider.clearRows();
        exceptGrid.clearAllFilter();
        exceptGrid.dataProvider.setRows(dataList);
    },
    /**
     * 제외 상품 그리드에 데이터 추가
     * @param data
     */
    appendData : function(data) {
        var currentDatas = exceptGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        if (!currentDatas !== undefined && currentDatas.length > 0) {

            exceptGrid.dataProvider.beginUpdate();
            try {
                data.forEach(function(value, index, array1) {
                    if (currentDatas.findIndex(function(currentData) { return currentData.itemNo === value.itemNo; }) == -1) {
                        exceptGrid.dataProvider.addRow(value);
                    } else {
                        var options = {
                            startIndex: 0,
                            fields: ['itemNo'],
                            values: [value.itemNo]
                        };

                        var _row = exceptGrid.dataProvider.searchDataRow(options);

                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (exceptGrid.dataProvider.getRowState(_row) == 'deleted') {
                            exceptGrid.dataProvider.setRowState(_row, 'none');
                        }
                    }
                });
            } finally {
                exceptGrid.dataProvider.endUpdate(true);
            }
        } else {
            exceptGrid.dataProvider.addRows(data);
        }
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = exceptGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        exceptGrid.dataProvider.removeRows(check, false);
    },
    /**
     * 적용대상 그리드 내 조회
     * @param searchColumnName
     * @param searchValue
     */
    filter : function(searchColumnName, searchValue) {
        // 사용하는 필터 제거
        exceptGrid.clearAllFilter();

        if (!searchColumnName || !searchValue)
            return;

        var filter = {};
        var filterArr = [];
        var activeFilterArr = [];

        switch(searchColumnName) {
            case "itemNo":
                $.each(searchValue, function(i) {
                    var value = searchValue[i];
                    filter = {name: value, criteria: "value = '" + value + "'"};
                    filterArr.push(filter);
                    activeFilterArr.push(value);
                });
                break;
            case "itemNm":
                filter = {name: searchValue, criteria: "value like '" + searchValue + "%'"};
                filterArr.push(filter);
                activeFilterArr.push(searchValue);
                break;
        }

        exceptGrid.gridView.setColumnFilters(searchColumnName, filterArr);
        exceptGrid.gridView.activateColumnFilters(searchColumnName, activeFilterArr, true);
    },
    /**
     * 제외상품 그리드 필터 제거
     */
    clearAllFilter : function() {
        exceptGrid.gridView.clearColumnFilters("itemNo");
        exceptGrid.gridView.clearColumnFilters("itemNm");
    },
    /**
     * 제외상품 그리드 엑셀다운로드
     * @returns {boolean}
     */
    excelDownload : function() {
        if (exceptGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "쿠폰_제외상품_" + _date.getFullYear() + month + date;

        exceptGrid.gridView.exportGrid({
            type: "excel",
            lookupDisplay: true,
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};
