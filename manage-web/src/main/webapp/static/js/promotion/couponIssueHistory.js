/**
 * 쿠폰사용현황
 */
$(document).ready(function() {
    couponGrid.init();
    couponIssueHistGrid.init();
    couponSearch.init();
    couponIssueHist.init();
    CommonAjaxBlockUI.global();
});

/**
 * Real Grid 초기화
 */
var couponGrid = {
    realGrid : new RealGridJS.GridView("couponGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        couponGrid.initGrid();
        couponGrid.initDataProvider();
        couponGrid.event();
    },
    initGrid : function() {
        couponGrid.realGrid.setDataSource(couponGrid.dataProvider);

        couponGrid.realGrid.setStyles(couponListGridBaseInfo.realgrid.styles);
        couponGrid.realGrid.setDisplayOptions(couponListGridBaseInfo.realgrid.displayOptions);
        couponGrid.realGrid.setColumns(couponListGridBaseInfo.realgrid.columns);
        couponGrid.realGrid.setOptions(couponListGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(couponGrid.realGrid, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "couponType", couponTypeJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "status", couponStatusJson);
        setColumnLookupOptionForJson(couponGrid.realGrid, "purpose", couponPurposeJson);

        couponGrid.realGrid.setIndicator({visible:true});
        couponGrid.realGrid.setColumnProperty("purchaseMin", "visible", false);
        couponGrid.realGrid.setColumnProperty("discountMax", "visible", false);
        couponGrid.realGrid.setColumnProperty("applySystem", "visible", false);
        couponGrid.realGrid.setColumnProperty("purpose", "visible", false);
        couponGrid.realGrid.setColumnProperty("shareYn", "visible", false);
        couponGrid.realGrid.setColumnProperty("departCd", "visible", false);
        couponGrid.realGrid.setColumnProperty("regNm", "visible", false);
        couponGrid.realGrid.setColumnProperty("regDt", "visible", false);
        couponGrid.realGrid.setColumnProperty("chgNm", "visible", false);
        couponGrid.realGrid.setColumnProperty("chgDt", "visible", false);
    },
    initDataProvider : function() {
        couponGrid.dataProvider.setFields(couponListGridBaseInfo.dataProvider.fields);
        couponGrid.dataProvider.setOptions(couponListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        couponGrid.realGrid.onDataCellClicked = function(gridView, index) {
            couponIssueHist.submitCouponIssueHistory(index.dataRow);
        };
    },
    setData : function(dataList) {
        couponGrid.dataProvider.clearRows();
        couponGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(couponGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰사용조회_쿠폰목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        couponGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var couponIssueHistGrid = {
    realGrid : new RealGridJS.GridView("couponIssueHistGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        couponIssueHistGrid.initGrid();
        couponIssueHistGrid.initDataProvider();
        couponIssueHistGrid.bindingEvent();
    },
    initGrid : function() {
        couponIssueHistGrid.realGrid.setDataSource(couponIssueHistGrid.dataProvider);

        /* RealGrid Header Summary 추가 */
        /*
        couponIssueHistGrid.realGrid.setIndicator({footText:"합계"});
        var mergeCells = couponUseHistoryGridBaseInfo.realgrid.columns.map(function(v, i){return v.name}).splice(0,7).reverse();
        couponIssueHistGrid.realGrid.setHeader({summary: {visible: true, mergeCells:mergeCells}});
        couponUseHistoryGridBaseInfo.realgrid.columns[6].header["summary"] = {
                                                                               "styles": {
                                                                                   "paddingRight": 5,
                                                                                   "textAlignment": "far",
                                                                                   "numberFormat": "#,###",
                                                                                   "suffix": "원"
                                                                               },
                                                                               "expression": "sum"
                                                                           };



        couponUseHistoryGridBaseInfo.realgrid.styles.indicator = {summary:{textAlignment:"center"}};
        */
        couponIssueHistGrid.realGrid.setStyles(couponUseHistoryGridBaseInfo.realgrid.styles);
        couponIssueHistGrid.realGrid.setDisplayOptions(couponUseHistoryGridBaseInfo.realgrid.displayOptions);
        couponIssueHistGrid.realGrid.setColumns(couponUseHistoryGridBaseInfo.realgrid.columns);
        couponIssueHistGrid.realGrid.setOptions(couponUseHistoryGridBaseInfo.realgrid.options);

        couponIssueHistGrid.realGrid.setIndicator({visible:true});
        couponIssueHistGrid.realGrid.setColumnProperty("couponUseYn", "visible", false);
        couponIssueHistGrid.realGrid.setColumnProperty("randomNo", "visible", false);
        couponIssueHistGrid.realGrid.setColumnProperty("withdrawYn", "visible", false);
        couponIssueHistGrid.realGrid.setColumnProperty("preIssueNo", "visible", false);
        couponIssueHistGrid.realGrid.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});
        couponIssueHistGrid.realGrid.setColumnProperty("chgNm", "visible", false);
        couponIssueHistGrid.realGrid.setColumnProperty("chgDt", "visible", false);
        couponIssueHistGrid.realGrid.setCheckBar({ visible: false });
    },
    initDataProvider : function() {
        couponIssueHistGrid.dataProvider.setFields(couponUseHistoryGridBaseInfo.dataProvider.fields);
        couponIssueHistGrid.dataProvider.setOptions(couponUseHistoryGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        couponIssueHistGrid.dataProvider.clearRows();
        couponIssueHistGrid.dataProvider.setRows(dataList);
    },
    bindingEvent: function() {
        couponIssueHistGrid.realGrid.onLinkableCellClicked = function (grid, index, url) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            if (index.fieldName == "purchaseOrderNo") {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
            }
        };
    },
    excelDownload: function() {
        if(couponIssueHistGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰사용조회_사용회원목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        couponIssueHistGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var couponSearch = {
    selectCouponRowId : 0,
    schDate : null,     // 캘린더 : 검색 폼 > 등록기간
	init : function() {
        this.event();
        this.initCouponSearch();
	},
    event : function() {
        $("#getCouponSearchBtn").on("click", function() {
            couponSearch.getCouponSearch();
        });

        $("#initCouponSearchBtn").on("click", function() {
            couponSearch.initCouponSearch();
        });

        $("#excelDownloadCouponListBtn").on("click", function() {
            couponGrid.excelDownload();
        });

        $("#setTodayBtn").on("click", function() {
            couponSearch.initSearchDate("0");
        });

        $("#setOneWeekBtn").on("click", function() {
            couponSearch.initSearchDate("-1w");
        });

        $("#setOneMonthBtn").on("click", function() {
            couponSearch.initSearchDate("-1m");
        });

        $("#setThreeMonthBtn").on("click", function() {
            couponSearch.initSearchDate("-3m");
        });
    },
    //쿠폰 조회 검색 폼 - 초기화
    initCouponSearch : function() {
        $("#couponSearchForm").each(function () {
            this.reset();
        });

        //조회 일자 초기화
        couponSearch.initSearchDate("0");

        couponIssueHist.initCouponIssueHistAll();
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        couponSearch.schDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        couponSearch.schDate.setEndDate("0");
        couponSearch.schDate.setStartDate(flag);
    },
    // 내용 검증
    validateCheck : function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var returnFlag = true;
        // 1년 단위
        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            returnFlag = false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            returnFlag = false;
        }

        if(startDt == "Invalid Date" || endDt == "Invalid Date") {
            alert("날짜 형식이 맞지 않습니다");
            returnFlag = false;
        }

        // 검색어 자릿수 확인
        if($("#schType").val() == "couponNm") {
            if ($("#schValue").val() != "" && $("#schValue").val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력하세요.");
                returnFlag = false;
            }
        }

        return returnFlag;
    },
    //쿠폰조회
    getCouponSearch : function(func) {
        // 조회 조건 확인
        if (!couponSearch.validateCheck()) return;

        couponIssueHist.initCouponIssueHistAll();

        $("#couponSearchCnt").html("0");

        var searchFromData = $("form[name=couponSearchForm]").serialize();

        CommonAjax.basic({url:"/coupon/getCouponList.json?" + searchFromData, method : "GET",  callbackFunc:function(res) {
            couponGrid.setData(res);
            $("#couponSearchCnt").html(couponGrid.dataProvider.getRowCount());
        }});
    }
};

var couponIssueHist = {
    couponNo : null,
    schDate : null,

    init: function () {
        this.event();
        this.initCouponIssueHistAll();
    },
    event: function () {
        //초기화
        $("#initCouponIssueHistSearchBtn").on("click", function() {
            couponIssueHist.initCouponIssueHistAll();
        });

        $("#getCouponIssueHistSearchBtn").on("click", function() {
            couponIssueHist.getCouponIssueHist();
        });

        $("#excelDownloadCouponHistListBtn").on("click", function() {
            couponIssueHistGrid.excelDownload();
        });

        $("#schMember").on("click", function() {
            couponIssueHist.callMemberSearchPop($(this));
        });
    },
    initCouponHistDate : function (flag) {
        couponIssueHist.schDate = Calendar.datePickerRange("histSchStartDt", "histSchEndDt");
        couponIssueHist.schDate.setEndDate(0);
        couponIssueHist.schDate.setStartDate(flag);
    },
    initCouponIssueHistAll : function() {
        couponIssueHist.couponNo = null;

        $("#couponIssueHistSearchForm").each(function () {
            this.reset();
        });

        couponIssueHist.initCouponHistDate(0);
        $("#histSchValue").val("");
        $("#couponIssueHistSearchCnt").html("0");
        $("#subCouponNo").html("");
        $("#couponStat").html("");

        couponIssueHistGrid.dataProvider.clearRows();
    },
    submitCouponIssueHistory : function(selectRowId) {
        couponIssueHist.initCouponIssueHistAll();

        couponSearch.selectCouponRowId = selectRowId;
        couponIssueHist.couponNo = couponGrid.dataProvider.getValue(selectRowId, "couponNo");

        $("#subCouponNo").text(couponIssueHist.couponNo);

        this.getCouponIssueHist();
    },
    getCouponIssueHist : function() {
        if(couponIssueHist.couponNo == null) {
            alert("쿠폰을 선택하세요.");
            return false;
        }

        // 조회기간 체크
        if (moment($("#histSchEndDt").val()).diff($("#histSchStartDt").val(), "day", true) > 7) {
            alert("7일 이내만 검색 가능합니다.");
            $("#histSchStartDt").val(moment($("#histSchEndDt").val()).add(-7, "day").format("YYYY-MM-DD"));
            return false;
        }

        $("#couponIssueHistSearchCnt").html("0");
        $("#subCouponNo").html("");
        $("#couponStat").html("");

        couponIssueHistGrid.dataProvider.clearRows();

        $("#subCouponNo").text(couponIssueHist.couponNo);

        var params = {
            "couponNo":couponIssueHist.couponNo
            , "histSchType":"userNo"
            , "histSchValue":$("#histSchValue").val()
            , "histSchStartDt":$("#histSchStartDt").val()
            , "histSchEndDt":$("#histSchEndDt").val()
            , "schPageType":$("#schPageType").val()
        };

        couponIssueHistGrid.dataProvider.clearRows();

        //사용내역조회 리스트 10만개 제한
        var LIMIT_LIST_COUNT = 100000;

        CommonAjax.basic({
            url : "/coupon/getCouponIssueHistList.json?" + $.param(params),
            method : 'get',
            callbackFunc : function(resData) {
                if ( resData == null ) {
                    if (true) {
                        alert('정상적으로 조회되지 않았습니다.');
                    }
                } else {
                    var resultCount = resData.couponIssueHistListCount;

                    /*if (resultCount > LIMIT_LIST_COUNT) {
                        alert("검색결과 수가 많아, 10만건만 출력되었습니다.");
                    }

                    // var couponStat = "[실제발급수: "+ resData.issueCount +"] [사용건수: "+ resData.useIssueCount +"] [사용금액: "+ resData.sumDiscountprice +"]";
                    var couponStat = "[실제발급수: "+ resData.issueCount +"]";
                    $("#couponStat").html(couponStat);
                    */

                    $("#couponIssueHistSearchCnt").html(resultCount);

                    couponIssueHistGrid.dataProvider.clearRows();
                    couponIssueHistGrid.setData(resData.couponIssueHistList);
                }
            }
        });
    },
    callMemberSearchPop : function(obj) {
        var memberSearchCallback = "couponIssueHist.callbackMemberSearchPop";
        if (couponIssueHist.couponNo == "" || couponIssueHist.couponNo == null) {
            alert("조회할 쿠폰을 선택하세요.");
            return;
        }
        memberSearchPopup(memberSearchCallback);
    },
    callbackMemberSearchPop : function(memberData) {
        $("#histSchValue").val(memberData.userNo);
        $("#memberId").val(memberData.userId);
        $("#memberNm").val(memberData.userNm);

        couponIssueHist.getCouponIssueHist();
    }
};