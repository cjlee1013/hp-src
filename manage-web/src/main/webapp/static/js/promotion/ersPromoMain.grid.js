/**
 * 사은행사관리(ersPromoMain.jsp) 에서 사용, 그리드 관련 컨트롤 분리
 * */

// 사은행사관리 조회 그리드
var ersPromoGrid = {
    gridView : new RealGridJS.GridView("ersPromoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        ersPromoGrid.initErsPromoGrid();
        ersPromoGrid.initDataProvider();
        ersPromoGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initErsPromoGrid : function() {
        ersPromoGrid.gridView.setDataSource(ersPromoGrid.dataProvider);
        ersPromoGrid.gridView.setStyles(ersPromoGridBaseInfo.realgrid.styles);
        ersPromoGrid.gridView.setDisplayOptions(ersPromoGridBaseInfo.realgrid.displayOptions);
        ersPromoGrid.gridView.setColumns(ersPromoGridBaseInfo.realgrid.columns);
        ersPromoGrid.gridView.setOptions(ersPromoGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(ersPromoGrid.gridView, "eventTargetType", eventTargetTypeJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "eventGiveType", eventGiveTypeJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "receiptSumYn", applyFlagJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "multipleYn", applyFlagJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "customerInfoPrtYn", applyYnJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "saleType", saleTypeJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "confirmFlag", confirmYnJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "cancelFlag", useYnJson);
        setColumnLookupOptionForJson(ersPromoGrid.gridView, "useYn", useYnJson);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        ersPromoGrid.dataProvider.setFields(ersPromoGridBaseInfo.dataProvider.fields);
        ersPromoGrid.dataProvider.setOptions(ersPromoGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        ersPromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            ersPromo.getErsPromoDetail(ersPromoGrid.dataProvider.getJsonRow(ersPromoGrid.gridView.getCurrent().dataRow).eventCd);
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        ersPromoGrid.dataProvider.clearRows();
        ersPromoGrid.dataProvider.setRows(dataList);
    },
    /**
     * 조회 그리드 엑셀 다운로드
     * @returns {boolean}
     */
    excelDownload: function() {
        if(ersPromoGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        ersPromoGrid.gridView.exportGrid({
            type: "excel"
            , target: "local"
            , lookupDisplay: true
            , fileName: fileName + ".xlsx"
            , showProgress: true
            , progressMessage: "엑셀 Export중입니다."
        });
    }
    /**
     * 선택된 사은행사번호 가져오기
     * @returns {any[]}
     */
    , getCheckEventCd : function () {
        var eventCdList = new Array();

        var checkRows = ersPromoGrid.gridView.getCheckedRows(true);

        for (var idx = 0; idx < checkRows.length; idx++) {
            var eventCd = ersPromoGrid.dataProvider.getValue(checkRows[idx], "eventCd");

            eventCdList.push(eventCd);
        }

        return eventCdList;
    }
};

// 사은행사 할인 구간 정보 그리드
var ersPromoIntervalGrid = {
    gridView : new RealGridJS.GridView("ersPromoIntervalGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        ersPromoIntervalGrid.initErsPromoIntervalGrid();
        ersPromoIntervalGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initErsPromoIntervalGrid : function() {
        ersPromoIntervalGrid.gridView.setDataSource(ersPromoIntervalGrid.dataProvider);
        ersPromoIntervalGrid.gridView.setStyles(ersPromoIntervalGridBaseInfo.realgrid.styles);
        ersPromoIntervalGrid.gridView.setDisplayOptions(ersPromoIntervalGridBaseInfo.realgrid.displayOptions);
        ersPromoIntervalGrid.gridView.setColumns(ersPromoIntervalGridBaseInfo.realgrid.columns);
        ersPromoIntervalGrid.gridView.setOptions(ersPromoIntervalGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(ersPromoIntervalGrid.gridView, "giveType", giftGiveTypeJson);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        ersPromoIntervalGrid.dataProvider.setFields(ersPromoIntervalGridBaseInfo.dataProvider.fields);
        ersPromoIntervalGrid.dataProvider.setOptions(ersPromoIntervalGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        ersPromoIntervalGrid.dataProvider.clearRows();
        ersPromoIntervalGrid.dataProvider.setRows(dataList);
    },
};

// 사은행사 적용점포 그리드
var ersPromoStoreGrid = {
    gridView : new RealGridJS.GridView("ersPromoStoreGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        ersPromoStoreGrid.initErsPromoStoreGrid();
        ersPromoStoreGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initErsPromoStoreGrid : function() {
        ersPromoStoreGrid.gridView.setDataSource(ersPromoStoreGrid.dataProvider);
        ersPromoStoreGrid.gridView.setStyles(ersPromoStoreGridBaseInfo.realgrid.styles);
        ersPromoStoreGrid.gridView.setDisplayOptions(ersPromoStoreGridBaseInfo.realgrid.displayOptions);
        ersPromoStoreGrid.gridView.setColumns(ersPromoStoreGridBaseInfo.realgrid.columns);
        ersPromoStoreGrid.gridView.setOptions(ersPromoStoreGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        ersPromoStoreGrid.dataProvider.setFields(ersPromoStoreGridBaseInfo.dataProvider.fields);
        ersPromoStoreGrid.dataProvider.setOptions(ersPromoStoreGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        ersPromoStoreGrid.dataProvider.clearRows();
        ersPromoStoreGrid.dataProvider.setRows(dataList);
    },
};


// 사은행사 적용점포 그리드
var ersPromoItemGrid = {
    gridView : new RealGridJS.GridView("ersPromoItemGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        ersPromoItemGrid.initErsPromoStoreGrid();
        ersPromoItemGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initErsPromoStoreGrid : function() {
        ersPromoItemGrid.gridView.setDataSource(ersPromoItemGrid.dataProvider);
        ersPromoItemGrid.gridView.setStyles(ersPromoItemGridBaseInfo.realgrid.styles);
        ersPromoItemGrid.gridView.setDisplayOptions(ersPromoItemGridBaseInfo.realgrid.displayOptions);
        ersPromoItemGrid.gridView.setColumns(ersPromoItemGridBaseInfo.realgrid.columns);
        ersPromoItemGrid.gridView.setOptions(ersPromoItemGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        ersPromoItemGrid.dataProvider.setFields(ersPromoItemGridBaseInfo.dataProvider.fields);
        ersPromoItemGrid.dataProvider.setOptions(ersPromoItemGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        ersPromoItemGrid.dataProvider.clearRows();
        ersPromoItemGrid.dataProvider.setRows(dataList);
    },
};


// // 사은행사 적용점포 그리드
// var ersPromoCardGrid = {
//     gridView : new RealGridJS.GridView("ersPromoCardGrid"),
//     dataProvider : new RealGridJS.LocalDataProvider(),
//
//     init : function() {
//         ersPromoCardGrid.initErsPromoStoreGrid();
//         ersPromoCardGrid.initDataProvider();
//     },
//     /**
//      * 조회 그리드 설정 초기화
//      */
//     initErsPromoStoreGrid : function() {
//         ersPromoCardGrid.gridView.setDataSource(ersPromoCardGrid.dataProvider);
//         ersPromoCardGrid.gridView.setStyles(ersPromoCardGridBaseInfo.realgrid.styles);
//         ersPromoCardGrid.gridView.setDisplayOptions(ersPromoCardGridBaseInfo.realgrid.displayOptions);
//         ersPromoCardGrid.gridView.setColumns(ersPromoCardGridBaseInfo.realgrid.columns);
//         ersPromoCardGrid.gridView.setOptions(ersPromoCardGridBaseInfo.realgrid.options);
//     },
//     /**
//      * 그리드 데이터 설정 초기화
//      */
//     initDataProvider : function() {
//         ersPromoCardGrid.dataProvider.setFields(ersPromoCardGridBaseInfo.dataProvider.fields);
//         ersPromoCardGrid.dataProvider.setOptions(ersPromoCardGridBaseInfo.dataProvider.options);
//     },
//     /**
//      * 조회된 데이터 그리드에 설정
//      */
//     setData : function(dataList) {
//         ersPromoCardGrid.dataProvider.clearRows();
//         ersPromoCardGrid.dataProvider.setRows(dataList);
//     },
// };