/**
 * 프로모션관리 > 이벤트 > 이벤트 메인 js
 */
$(document).ready(function () {
    eventPromoGrid.init();
    commonEditor.initEditor("#pcEditor", 300, "PromotionImage");
    commonEditor.initEditor("#mobileEditor", 300, "PromotionImage");
    CommonAjaxBlockUI.global();
    eventPromoSearch.init();
    eventPromo.init();
});

/**
 * 이벤트 그리드
 */
var eventPromoGrid = {
    gridView : new RealGridJS.GridView("eventPromoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        eventPromoGrid.initEventPromoGrid();
        eventPromoGrid.initDataProvider();
        eventPromoGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initEventPromoGrid : function() {
        eventPromoGrid.gridView.setDataSource(eventPromoGrid.dataProvider);
        eventPromoGrid.gridView.setStyles(eventGridBaseInfo.realgrid.styles);
        eventPromoGrid.gridView.setDisplayOptions(eventGridBaseInfo.realgrid.displayOptions);
        eventPromoGrid.gridView.setColumns(eventGridBaseInfo.realgrid.columns);
        eventPromoGrid.gridView.setOptions(eventGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(eventPromoGrid.gridView, "dispYn", dispYnJson);
        setColumnLookupOptionForJson(eventPromoGrid.gridView, "useYn", useYnJson);
        setColumnLookupOptionForJson(eventPromoGrid.gridView, "siteType", siteTypeJson);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        eventPromoGrid.dataProvider.setFields(eventGridBaseInfo.dataProvider.fields);
        eventPromoGrid.dataProvider.setOptions(eventGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        eventPromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = eventPromoGrid.dataProvider.getJsonRow(eventPromoGrid.gridView.getCurrent().dataRow);
            eventPromo.getEventPromoDetail(gridRow.promoNo);
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        eventPromoGrid.dataProvider.clearRows();
        eventPromoGrid.dataProvider.setRows(dataList);
    },
};

/**
 * 이벤트 조회 영역
 */
var eventPromoSearch = {
    init : function () {
        eventPromoSearch.searchFormReset();
        eventPromoSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#eventPromoSearchForm").resetForm();

        // 조회 기간 초기화
        initCalendarDate(eventPromoSearch, "schStartDt", "schEndDt", SEARCH_AREA, "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(eventPromoSearch.searchEventPromoList);
        $("#schResetBtn").bindClick(eventPromoSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                eventPromoSearch.searchEventPromoList();
            }
        });
    }
    /**
     * 이벤트 정보 조회
     */
    , searchEventPromoList : function (promoNo) {
        // 검색조건 validation check
        if (eventPromo.valid.searchValid()) {
            // 이벤트 정보 조회 개수 초기화
            $("#eventTotalCnt").html("0");

            // 조회
            var eventPromoSearchForm = $("#eventPromoSearchForm").serializeObject();
            eventPromoSearchForm.schPromoType = $("#promoType").val();

            CommonAjax.basic({
                url : "/promotion/getPromoList.json"
                , data : eventPromoSearchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    eventPromoGrid.setData(res);

                    $("#eventTotalCnt").html($.jUtil.comma(eventPromoGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(promoNo)) {
                        var rowIndex = eventPromoGrid.dataProvider.searchDataRow({fields : ["promoNo"], values : [promoNo]});

                        if (rowIndex > -1) {
                            eventPromoGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            eventPromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

/**
 * 이벤트 정보 영역
 */
var eventPromo = {
    selectPromoNo : null
    , uploadType : 'IMG'

    /**
     * 이벤트 상세 초기화
     */
    , init : function () {
        eventPromo.bindingEvent();
        eventPromo.eventPromoFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(eventPromo.setEventPromo);               // 저장
        $("#resetBtn").bindClick(eventPromo.eventPromoFormReset, "");       // 초기화

        // 사이트 구분 변경시
        $("input:radio[name='siteType']").bindChange(eventPromo.view.changeSiteType);

        // 이미지 등록
        $("input[name='fileArr']").bindChange(eventPromo.img.addImg);

        // 이미지 삭제
        $("#benefitPCImgDelBtn, #benefitMOImgDelBtn, #searchPCImgDelBtn, #searchMOImgDelBtn").click(function () {
            if (confirm("삭제하시겠습니까?")) {
                eventPromo.img.deleteImg($(this).siblings(".uploadType"));
            }
        });

        // 이미지 미리보기
        $(".uploadType").bindClick(eventPromo.img.imagePreview);

        // 카테고리 이벤트 노출 우선순위 숫자만 입력
        $("#searchPriority").allowInput("keyup", ["NUM"], $(this).attr("id")).bindChange(eventPromo.valid.priorityCheck);

        // 입력 문자 길이 계산
        $("#mngPromoNm").calcTextLength("keyup", "#mngPromoNmLen");
        $("#dispPromoNm").calcTextLength("keyup", "#dispPromoNmLen");
        $("#appTopTitle").calcTextLength("keyup", "#topTitleLen");
        $("#searchKeyword").calcTextLength("keyup", "#keywordLen");
    }
    /**
     * 이벤트 상세 내용 초기화
     */
    , eventPromoFormReset : function (siteType) {
        $("#eventPromoForm").resetForm();
        eventPromo.selectPromoNo = null;

        // 사이트 구분 변경으로 인한 초기화 시
        if (!$.jUtil.isEmpty(siteType)) {
            $("input:radio[name='siteType']:input[value='" + siteType + "']").prop("checked", true);
        } else {
            eventPromo.view.siteTypeView();
        }

        // 전시 기간 초기화
        initCalendarDate(eventPromo, "dispStartDt", "dispEndDt", FORM_AREA, "1d", "15d");

        // span 초기화
        $("#promoNo").html("");
        $("#mngPromoNmLen").html("0");
        $("#dispPromoNmLen").html("0");
        $("#keywordLen").html("0");

        // 이미지 초기화
        eventPromo.img.init();

        // 에디터 초기화
        commonEditor.setHtml("#pcEditor", "");
        commonEditor.setHtml("#mobileEditor", "");
    }
    /**
     * 이벤트 상세 조회
     */
    , getEventPromoDetail : function (promoNo) {
        // 이벤트 상세 초기화
        eventPromo.eventPromoFormReset();

        eventPromo.selectPromoNo = promoNo;

        CommonAjax.basic({
            url:"/promotion/getPromoDetail.json"
            , data : {promoNo : promoNo}
            , method : "GET"
            , callbackFunc : function (res) {
                eventPromo.detailCallback(res);
            }
        });
    }
    /**
     * 이벤트 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param eventInfo
     * @returns {boolean}
     */
    , detailCallback : function (eventInfo) {
        if ($.jUtil.isEmpty(eventInfo)) {
            return true;
        }

        // 기본정보
        $("#promoNo").html(eventInfo.promoNo);
        $("#mngPromoNm").val(eventInfo.mngPromoNm).setTextLength("#mngPromoNmLen");
        $("#dispPromoNm").val(eventInfo.dispPromoNm).setTextLength("#dispPromoNmLen");
        $("#dispStartDt").val(eventInfo.dispStartDt);
        $("#dispEndDt").val(eventInfo.dispEndDt);
        $("#appTopTitle").val(eventInfo.appTopTitle).setTextLength("#topTitleLen");
        $("#searchKeyword").val(eventInfo.searchKeyword).setTextLength("#keywordLen");
        $("#searchPriority").val(eventInfo.searchPriority);

        // 기본정보 > 적용시스템, 노출영역
        $("#pcUseYn").prop("checked", eventInfo.pcUseYn == "Y");
        $("#appUseYn").prop("checked", eventInfo.appUseYn == "Y");
        $("#mwebUseYn").prop("checked", eventInfo.mwebUseYn == "Y");

        $("#dispSearchYn").prop("checked", eventInfo.dispSearchYn == "Y");

        // 기본정보 > 사이트구분, 전시, 사용 여부
        $("input:radio[name='siteType']:input[value='" + eventInfo.siteType + "']").prop("checked", true).trigger("change");
        $("input:radio[name='dispYn']:input[value='" + eventInfo.dispYn + "']").prop("checked", true).trigger("change");
        $("input:radio[name='useYn']:input[value='" + eventInfo.useYn + "']").prop("checked", true).trigger("change");

        // 배너이미지
        for (var bnIdx in eventInfo.imgList) {
            var imgData = eventInfo.imgList[bnIdx];

            eventPromo.setImgDisplayData(imgData);
        }

        if (eventInfo.pcUseYn == "Y") {
            commonEditor.setHtml("#pcEditor", eventInfo.pcText);
        }

        if (eventInfo.appUseYn == "Y" || eventInfo.mwebUseYn == "Y") {
            commonEditor.setHtml("#mobileEditor", eventInfo.mobileText);
        }
    }
    /**
     * 이벤트 상세 - 이미지 영역 데이터 채우기
     */
    , setImgDisplayData : function (imgData) {
        var obj = "";
        var deviceType = imgData.deviceType == "PC" ? imgData.deviceType : "MO";
        var imgType = deviceType + "_" + imgData.imgType;
        var processKey = eventPromo.img.getProcessKey(imgType);

        switch (imgData.imgType) {
            case "BENEFIT" :
                obj = "#benefit" + deviceType + "Img";
                break;
            case "SEARCH" :
                obj = "#search" + deviceType + "Img";
                break;
        }

        // 파일명, 사이즈 등은 임시로 넣음
        eventPromo.img.setImg($(obj), {
            "imgUrl" : imgData.imgUrl
            , "imgWidth" : imgData.imgWidth
            , "imgHeight" : imgData.imgHeight
            , "src" : (processKey == "PromotionImage" ? hmpImgFrontUrl : hmpImgUrl) + "/provide/view?processKey=" + processKey + "\&fileId=" + imgData.imgUrl
            , "changeYn" : "N"
        });
    }
    /**
     * 이벤트 저장
     */
    , setEventPromo : function () {
        if (eventPromo.valid.setPromoValid()) {
            var eventPromoForm = $("#eventPromoForm").serializeObject(true);

            // 이미지 유효성 검사는 여기서
            for (var imgIdx in eventPromoForm.imgList) {
                var imgInfo = eventPromoForm.imgList[imgIdx];
                // 1. 배너 이미지 확인 - 혜택존은 필수
                // 3. 나머지 배너 이미지는 노출영역 체크 확인
                if (imgInfo.imgType == "BENEFIT" && $.jUtil.isEmpty(imgInfo.imgUrl)) {
                    return $.jUtil.alert("혜택존 배너 이미지를 등록해 주세요.");
                } else {
                    if (imgInfo.imgType == "SEARCH" && $("#dispSearchYn").is(":checked") && $.jUtil.isEmpty(imgInfo.imgUrl)) {
                        return $.jUtil.alert("검색결과 배너 이미지를 등록해 주세요.");
                    }
                }
            }

            // 이벤트 번호
            eventPromoForm.promoNo = Number($("#promoNo").text());

            // 이벤트내용 에디터
            eventPromoForm.pcType      = "EDIT";
            eventPromoForm.mobileType  = "EDIT";
            eventPromoForm.pcText      = commonEditor.getHtmlData("#pcEditor");
            eventPromoForm.mobileText  = commonEditor.getHtmlData("#mobileEditor");

            // 이벤트 저장 호출
            CommonAjax.basic({
                url : "/promotion/setPromotion.json"
                , data : JSON.stringify(eventPromoForm)
                , method : "POST"
                , contentType: 'application/json'
                , successMsg : "이벤트 등록/수정이 완료되었습니다"
                , callbackFunc : function (res) {
                    // 신규 등록시 상세 초기화
                    if ($.jUtil.isEmpty(eventPromo.selectPromoNo)) {
                        eventPromo.eventPromoFormReset();
                        eventPromoSearch.searchEventPromoList();
                    } else {
                        eventPromoSearch.searchEventPromoList(res);
                    }
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        eventPromo.eventPromoFormReset();
                        eventPromoSearch.searchEventPromoList();
                    }
                }
            });
        }
    }
};

/**
 * 이벤트 화면 컨트롤 관련
 */
eventPromo.view = {
    /**
     * 사이트구분 변경 시 : 등록 - 전체초기화, 수정 - 변경불가
     * @param obj
     */
    changeSiteType : function (obj) {
        if ($.jUtil.isEmpty(eventPromo.selectPromoNo)) {
            eventPromo.eventPromoFormReset($(obj).val());
        } else {
            eventPromo.view.siteTypeView("SET");
        }
    }
    /**
     * 사이트구분 항목 비활성화
     * - 초기화 시 : 전체 활성화
     * - 상세 검색 시 : 미선택 항목 비활성화
     * @param flag
     */
    , siteTypeView : function (flag) {
        var checked = false;

        $("input:radio[name='siteType']").each(function () {
            if (flag == "SET") {
                checked = !$(this).prop("checked");
            }

            $(this).prop("disabled", checked);
        });
    }
};

/**
 * 이벤트 이미지 관련
 */
eventPromo.img = {
    imgDiv : null

    /**
     * 이미지 초기화
     */
    , init : function () {
        $(".uploadType").each(function (){eventPromo.img.setImg($(this))});
        eventPromo.img.imgDiv = null;
        commonEditor.inertImgurl = null;
    }
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    , setImg : function (obj, params) {
        if (!$.jUtil.isEmpty(eventPromo.img.imgDiv)) {
            eventPromo.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        }

        if (params) {
            obj.find(".imgUrl").val(params.imgUrl);
            obj.find(".imgWidth").val(params.imgWidth);
            obj.find(".imgHeight").val(params.imgHeight);
            obj.find(".changeYn").val(params.changeYn);
            obj.find(".imgUrlTag").prop("src", params.src);

            obj.parents(".imgDisplayView").show();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").hide();
        } else {
            obj.find(".imgUrl").val("");
            obj.find(".imgWidth").val("");
            obj.find(".imgHeight").val("");
            obj.find(".changeYn").val("N");
            obj.find(".imgUrlTag").prop("src", "");

            obj.parents(".imgDisplayView").hide();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").show();
        }
    }
    /**
     * 이미지 processKey
     * @returns {string}
     */
    , getProcessKey : function (imgType) {
        var processKey;

        switch (imgType) {
            case "PC_BENEFIT" :
                processKey = "PromoBenefitPCBanner";
                break;
            case "MO_BENEFIT" :
                processKey = "PromoBenefitMobileBanner";
                break;
            case "PC_SEARCH" :
                processKey = "PromoSearchPCBanner";
                break;
            case "MO_SEARCH" :
                processKey = "PromoSearchMobileBanner";
                break;
            default :
                processKey = "PromotionImage";
                break;
        }

        return processKey;
    }
    /**
     * 이미지 업로드
     * @param obj
     */
    , addImg : function () {
        if ($("#imgFile").get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($("#imgFile").get(0).files[0].name.length > 30) {
            return $.jUtil.alert("파일명은 30자(확장자포함) 이내로 입력 가능합니다.");
        }

        var imgType = eventPromo.img.imgDiv.data("type");
        var processKey = eventPromo.img.getProcessKey(imgType);

        // switch (imgType) {
        //     case "PC_BENEFIT" :
        //         params.processKey = "PromoBenefitPCBanner";
        //         break;
        //     case "MO_BENEFIT" :
        //         params.processKey = "PromoBenefitMobileBanner";
        //         break;
        //     case "PC_SEARCH" :
        //         params.processKey = "PromoSearchPCBanner";
        //         break;
        //     case "MO_SEARCH" :
        //         params.processKey = "PromoSearchMobileBanner";
        //         break;
        //     default :
        //         params.processKey = "PromotionImage";
        //         break;
        // }

        var file = $("#imgFile");
        var params = {
            processKey : processKey,
            mode : "IMG"
        };

        var ext = $("#imgFile").get(0).files[0].name.split(".");

        CommonAjax.upload({
            file : file,
            data : params,
            callbackFunc : function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        eventPromo.img.setImg(eventPromo.img.imgDiv, {
                            "imgUrl" : f.fileStoreInfo.fileId
                            , "imgWidth" : f.fileStoreInfo.width
                            , "imgHeight" : f.fileStoreInfo.height
                            , "src"   : (processKey == "PromotionImage" ? hmpImgFrontUrl : hmpImgUrl) + "/provide/view?processKey=" + processKey + "\&fileId=" + f.fileStoreInfo.fileId
                            , "ext"   : ext[1]
                            , "changeYn" : "Y"
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    }
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    , imagePreview : function (obj) {
        var imgUrl = $(obj).find("img").prop("src");
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function (obj) {
        eventPromo.img.setImg(obj, null);
    }
    /**
     * 이미지 클릭처리
     * @param obj
     */
    , clickFile : function (obj) {
        eventPromo.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        $("input[name=fileArr]").click();
    }
};

/**
 * 이벤트 유효성 검사 관련
 * @type {{themeValid: eventPromo.valid.themeValid}}
 */
eventPromo.valid = {
    searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'PROMONO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }

        return true;
    }
    , priorityCheck : function (obj) {
        var priority = $(obj).val();

        if (priority < 1 || priority >= 10) {
            $(obj).val("");
            return $.jUtil.alert("우선순위는 1~9까지 입력가능합니다.");
        };
    }
    , setPromoValid : function () {
        if ($.jUtil.isEmpty($("#mngPromoNm").val())) {
            return $.jUtil.alert("관리이벤트명을 입력해주세요.");
        }

        if ($.jUtil.isEmpty($("#dispPromoNm").val())) {
            return $.jUtil.alert("전시이벤트명을 입력해주세요.");
        }

        var nowDt = moment().format("YYYY-MM-DD HH:mm");

        // 전시시작기간
        if (!moment($("#dispStartDt").val(),'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("전시기간 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#dispEndDt").val(),'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("전시종료 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 적용시스템 1개 이상 적용 확인
        if (!(($("#pcUseYn").is(":checked")) || ($("#appUseYn").is(":checked")) || ($("#mwebUseYn").is(":checked")))) {
            alert("적용시스템을 선택해주세요");
            return false;
        }

        // 적용시스템 선택에 따른 이벤트내용등록 여부 확인
        if (($("#pcUseYn").is(":checked"))) {
            if ($.jUtil.isEmpty(commonEditor.getHtmlData("#pcEditor"))) {
                return $.jUtil.alert("PC 이벤트 내용을 입력해주세요.");
            }
        }

        // 적용시스템 선택에 따른 이벤트내용등록 여부 확인
        if ($("#appUseYn").is(":checked") || $("#mwebUseYn").is(":checked")) {
            if ($.jUtil.isEmpty(commonEditor.getHtmlData("#mobileEditor"))) {
                return $.jUtil.alert("MWeb/APP 이벤트 내용을 입력해주세요.");
            }
        }

        return true;
    }
};