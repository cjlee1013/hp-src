/**
 * 프로모션관리 > 행사관리 > 마일리지 행사 관리 그리드 js
 */

// 조회 그리드
var mileagePromoGrid = {
    gridView : new RealGridJS.GridView("mileagePromoGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        mileagePromoGrid.initMileagePromoGrid();
        mileagePromoGrid.initDataProvider();
        mileagePromoGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initMileagePromoGrid : function() {
        mileagePromoGrid.gridView.setDataSource(mileagePromoGrid.dataProvider);
        mileagePromoGrid.gridView.setStyles(mileagePromoGridBaseInfo.realgrid.styles);
        mileagePromoGrid.gridView.setDisplayOptions(mileagePromoGridBaseInfo.realgrid.displayOptions);
        mileagePromoGrid.gridView.setColumns(mileagePromoGridBaseInfo.realgrid.columns);
        mileagePromoGrid.gridView.setOptions(mileagePromoGridBaseInfo.realgrid.options);

        setColumnLookupOption(mileagePromoGrid.gridView, "useYn", $("#useYn"));
        setColumnLookupOption(mileagePromoGrid.gridView, "departCd", $("#departCd"));
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        mileagePromoGrid.dataProvider.setFields(mileagePromoGridBaseInfo.dataProvider.fields);
        mileagePromoGrid.dataProvider.setOptions(mileagePromoGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        mileagePromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = mileagePromoGrid.dataProvider.getJsonRow(mileagePromoGrid.gridView.getCurrent().dataRow);
            mileagePromo.getMileagePromoDetail(gridRow.mileageNo);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        mileagePromoGrid.dataProvider.clearRows();
        mileagePromoGrid.dataProvider.setRows(dataList);
    }
};

// 상품 그리드
var mileageItemGrid = {
    gridView : new RealGridJS.GridView("mileageItemGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        mileageItemGrid.initMileageItemGrid();
        mileageItemGrid.initDataProvider();
        mileageItemGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initMileageItemGrid : function() {
        mileageItemGrid.gridView.setDataSource(mileageItemGrid.dataProvider);
        mileageItemGrid.gridView.setStyles(mileageItemGridBaseInfo.realgrid.styles);
        mileageItemGrid.gridView.setDisplayOptions(mileageItemGridBaseInfo.realgrid.displayOptions);
        mileageItemGrid.gridView.setColumns(mileageItemGridBaseInfo.realgrid.columns);

        var options = mileageItemGridBaseInfo.realgrid.options;
        options.hideDeletedRows = true;
        mileageItemGrid.gridView.setOptions(options);
        // mileageItemGrid.gridView.setOptions(mileageItemGridBaseInfo.realgrid.options);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        mileageItemGrid.dataProvider.setFields(mileageItemGridBaseInfo.dataProvider.fields);

        var options = mileageItemGridBaseInfo.dataProvider.options;
        options.softDeleting = true;
        mileageItemGrid.dataProvider.setOptions(options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        mileageItemGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = mileageItemGrid.dataProvider.getJsonRow(mileageItemGrid.gridView.getCurrent().dataRow);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        mileageItemGrid.dataProvider.clearRows();
        mileageItemGrid.dataProvider.setRows(dataList);

        mileagePromo.setItemCnt(0);
    }
    /**
     * 그리드에 데이터 추가
     * @param data
     */
    , appendData : function(data, flag) {
        var currentDatas = mileageItemGrid.dataProvider.getJsonRows(0, -1);

        // 기존 데이터가 있을시 데이터 중복 제거 진행
        mileageItemGrid.dataProvider.beginUpdate();

        try {
            data.forEach(function (value, index, array1) {
                if (currentDatas.findIndex(function (currentData) {
                    return currentData.itemNo === value.itemNo;
                }) == -1) {
                    // 일괄등록 일 경우 전시여부가 'Y'인 경우에만 추가
                    if (flag == "POP" || (flag == "EXCEL" && value.dispYn == "Y")) {
                        mileageItemGrid.dataProvider.addRow(value);
                    }
                } else {
                    var options = {
                        startIndex: 0,
                        fields: ["itemNo"],
                        values: [value.itemNo]
                    };

                    // 해당 상품번호를 가지고 있는 datarow를 가져온다
                    var _row = mileageItemGrid.dataProvider.searchDataRow(options);

                    if (value.dispYn == "N") {
                        mileageItemGrid.dataProvider.setRowState(_row, "deleted");
                    } else {
                        // 삭제 예정인 상품을 추가할 경우 상태 값을 none으로 변경
                        if (mileageItemGrid.dataProvider.getRowState(_row) == "deleted") {
                            mileageItemGrid.dataProvider.setRowState(_row, "none");
                        }
                    }
                }
            });
        } finally {
            mileageItemGrid.dataProvider.endUpdate(true);
        }

        mileagePromo.setItemCnt(0);
    }
    , getGridStateData : function (state) {
        var itemList = mileageItemGrid.dataProvider.getStateRows(state);
        var returnList = new Array();

        itemList.forEach(function(_row, index) {
            var _data = mileageItemGrid.dataProvider.getJsonRow(_row);
            returnList.push(_data.itemNo);
        });

        return returnList;
    }
    /**
     * 선택한 row 삭제
     */
    , removeCheckData : function() {
        var check = mileageItemGrid.gridView.getCheckedRows(true);
        var checkLen = check.length;

        if (checkLen == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }

        mileageItemGrid.dataProvider.removeRows(check, false);

        mileagePromo.setItemCnt(checkLen);
    }
};