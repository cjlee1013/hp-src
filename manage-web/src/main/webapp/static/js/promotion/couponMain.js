/**
 * 이벤트관리 > 쿠폰관리 > 쿠폰관리 메인 js
 */
$(document).ready(function() {
    couponListGrid.init();
    applyGrid.init();
    exceptGrid.init();
    couponSearch.init();
    coupon.init();
    CommonAjaxBlockUI.global();
    commonCategory.setCategorySelectBox(1, "", "", $("#categoryCd"));
});

// 내용검증
var validCheck = {
    /**
     * 검색 조건 내용 검증
     */
    search: function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }

        // 1개월 단위
        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 1) {
            alert("1개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (schType == 'COUPONNM' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    },
    /**
     * 임시저장 시 쿠폰 설정 내용 검증
     */
    setTempStatus: function() {
        var manageCouponNm = $("#manageCouponNm").val();
        var manageCouponNmLen = $("#manageCouponNm").attr("maxLength");

        // 쿠폰명 자릿수 체크
        if ($.jUtil.isEmpty(manageCouponNm)) {
            alert("관리쿠폰명을 입력해주세요.");
            return false;
        }

        if (manageCouponNm.length > manageCouponNmLen) {
            alert("관리쿠폰명은 최대 "+ manageCouponNmLen + "자까지 입력 가능합니다.");
            return false;
        }

        if ($("#purpose").val() == PURPOSE_RE_PURCHASE) {
            var purchaseStdDt = $("#purchaseStdDt").val();

            if ($.jUtil.isNotEmpty(purchaseStdDt) && !moment(purchaseStdDt,'YYYY-MM-DD',true).isValid()) {
                alert("구매기준일 날짜 형식이 맞지 않습니다.(YYYY-MM-DD)");

                $("#purchaseStdDt").val('');
                return false;
            }
        }

        var issueTimeYn = $("#issueTimeYn").val();
        var issueStartTime = $("#issueStartTime").val();
        var issueEndTime = $("#issueEndTime").val();

        if (issueTimeYn == 'Y') {
            if (!moment(issueStartTime, 'HH:mm', true).isValid()) {
                alert("발급시간대(시작) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            } else if (!moment(issueEndTime, 'HH:mm', true).isValid()) {
                alert("발급시간대(종료) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            }
        }

        var shipSlotYn = $("#shipSlotYn").val();
        var slotStartTime = $("#slotStartTime").val();
        var slotEndTime = $("#slotEndTime").val();

        if (shipSlotYn == 'Y') {
            if (!moment(slotStartTime, 'HH:mm', true).isValid()) {
                alert("배송Slot시간대(시작) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            } else if (!moment(slotEndTime, 'HH:mm', true).isValid()) {
                alert("배송Slot시간대(종료) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            }
        }

        return true;
    }
    ,
    /**
     * 쿠폰 설정 내용 검증
     */
    set: function (btnId) {
        // 임시저장 상태 아니거나 적용범위가 전체가 아닐시 적용대상 존재 여부 체크
        var couponType = $("#couponType").val();
        var storeType = $("#storeType").val();
        var nowDtValid = moment().format("YYYY-MM-DD HH:00");
        var nowDt = moment().format("YYYY-MM-DD HH:mm");
        var nowMonth = moment().format("YYYY-MM");
        var issueStartDt = $("#issueStartDt").val();
        var issueEndDt = $("#issueEndDt").val();
        var discountType = $("#discountType").val();
        var validType = $("#validType").val();
        var validDay = $("#validDay").val();
        var validStartDt = $("#validStartDt").val();
        var validEndDt = $("#validEndDt").val();
        var randomNoIssueYn = $("#randomNoIssueYn").val();
        var gradeCouponYn = $("#gradeCouponYn").val();
        var applyScope = $("#applyScope").val();
        var purpose = $("#purpose").val();

        if ($.jUtil.isEmpty(applyScope)) {
            alert("적용상품범위를 선택하세요.");
            return false;
        }

        if (applyScope != COUPON_APPLY_SCOPE_ALL) {
            if (applyGrid.gridView.getItemCount() == 0) {
                alert("적용대상을 등록해 주세요.");
                return false;
            }
        }

        if ($("#storeType").val() != STORE_TYPE_DS && $.jUtil.isEmpty(coupon.storeList)) {
            alert("점포를 등록하세요.");
            return false;
        }

        if ((couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_DUPL)
            && $.jUtil.isNotEmpty($("#paymentType").val()) && $.jUtil.isEmpty(coupon.paymentList)) {
            alert("상세결제수단을 등록하세요.");
            return false;
        }

        var manageCouponNm = $("#manageCouponNm").val();
        var manageCouponNmLen = $("#manageCouponNm").attr("maxLength");

        var displayCouponNm = $("#displayCouponNm").val();
        var displayCouponNmLen = $("#displayCouponNm").attr("maxLength");

        // 쿠폰명 자릿수 체크
        if ($.jUtil.isEmpty(manageCouponNm)) {
            alert("관리쿠폰명을 입력해주세요.");
            return false;
        }

        if (manageCouponNm.length > manageCouponNmLen) {
            alert("관리쿠폰명은 최대 "+ manageCouponNmLen + "자까지 입력 가능합니다.");
            return false;
        }

        if ($.jUtil.isEmpty(displayCouponNm)) {
            alert("전시쿠폰명을 입력해주세요.");
            return false;
        }

        if (displayCouponNm.length > displayCouponNmLen) {
            alert("전시쿠폰명은 " + displayCouponNmLen + "자까지 입력 가능합니다.");
            return false;
        }

        if (storeType != STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_IMME) && $("#barcode").val().length != "13") {
            alert("바코드를 입력하세요.");
            return false;
        }

        //적용시스템은 하나이상 선택하여야 합니다.
        if($("input:checkbox[class=chkApplySystem]:checked").length == 0) {
            alert("적용시스템은 최소 1개의 항목을 선택해주세요.");
            return false;
        }

        //재구매유도(웰컴)
        if (purpose == PURPOSE_RE_PURCHASE) {
            var purchaseStdDt = $("#purchaseStdDt").val();

            if ($.jUtil.isEmpty(purchaseStdDt)) {
                alert("구매기준일을 입력하세요.");
                return false;
            }

            if (!moment(purchaseStdDt,'YYYY-MM-DD',true).isValid()) {
                alert("구매기준일 날짜 형식이 맞지 않습니다.(YYYY-MM-DD)");
                $("#purchaseStdDt").val('');
                return false;
            }
        }

        var purchaseMin = $("#purchaseMin").val();
        if (discountType == COUPON_DISCOUNT_TYPE_PRICE) {
            var discountPrice = $("#discountPrice").val();

            if ($.jUtil.isEmpty(discountPrice) || discountPrice < 10) {
                alert("할인금액은 10원 이상 등록할 수 있습니다.");
                return false;
            }

            if (discountPrice.substr(discountPrice.length - 1) > 0) {
                alert("할인금액의 원단위는 반드시 0이어야 합니다.");
                return false;
            }

            var returnFlag = limitNumber("long", discountPrice);
            if (!returnFlag) {
                alert("할인금액은 999,999,990원을 넘을 수 없습니다.");
                return false;
            }

            if ($.jUtil.isEmpty(purchaseMin)) {
                alert("최소구매금액을 입력하세요.");
                return false;
            }

            if (purchaseMin.substr(purchaseMin.length - 1) > 0) {
                alert("최소구매금액의 원단위는 반드시 0이어야 합니다.");
                return false;
            }

            if (storeType == STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_DUPL) && (Number(purchaseMin) < Number(discountPrice))) {
                alert("최소구매금액은 할인금액보다 작을 수 없습니다.");
                return false;
            }
        } else {
            var discountRate = $("#discountRate").val();
            var discountMax = $("#discountMax").val();

            if ($.jUtil.isEmpty(discountRate) || discountRate == "0") {
                alert("할인율은 1% 이상 등록할 수 있습니다.");
                return false;
            }

            if ($.jUtil.isEmpty(discountMax)) {
                alert("최대할인금액을 입력하세요.");
                return false;
            }

            if (discountMax.substr(discountMax.length - 1) > 0) {
                alert("최대할인금액의 원단위는 반드시 0이어야 합니다.");
                return false;
            }

            var returnFlag = limitNumber("share", discountRate);
            if (returnFlag) {
                returnFlag = limitNumber("long", discountMax);
                if (!returnFlag) {
                    alert("최대할인금액은 999,999,990원을 넘을 수 없습니다.");
                    return false;
                }
            } else {
                alert("할인율은 100%를 넘을 수 없습니다.");
                return false;
            }

            if ($.jUtil.isEmpty(purchaseMin)) {
                alert("최소구매금액을 입력하세요.");
                return false;
            }
        }

        //분담설정
        if ($("#shareYn").val() == "Y") {
            var sumShare = Number($("#shareHome").val()) + Number($("#shareSeller").val()) + Number($("#shareCard").val());

            if (sumShare != 100) {
                alert("분담설정은 합이 100이어야 합니다.");
                return false;
            }
        }

        if (gradeCouponYn == "Y") {
            var gradeCouponMonth = $("#gradeCouponMonth").val();
            var userGradeSeq = $("#userGradeSeq").val();

            if ($.jUtil.isEmpty(gradeCouponMonth)) {
                alert("등급쿠폰이 적용될 연월을 선택하세요.");
                return false;
            }

            if (!moment(gradeCouponMonth,'YYYY-MM',true).isValid()) {
                alert("등급쿠폰 연월 형식을 확인하세요(YYYY-MM");
                return false;
            }

            if ($.jUtil.diffDate(nowMonth, gradeCouponMonth, "D") <= 0) {
                alert("등급쿠폰의 연월은 익월부터 설정 가능합니다.");
                return false;
            }

            if ($.jUtil.isEmpty(userGradeSeq)) {
                alert("등급쿠폰의 등급을 설정하세요.");
                return false;
            }
        }

        //발급기간
        if (!moment(issueStartDt,'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("발급기간 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        if (!moment(issueEndDt,'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("발급기간 날짜 형식이 맞지 않습니다");
            $("#issueEndDt").val(nowDt);
            return false;
        }

        issueStartDt = $("#issueStartDt").val();
        issueEndDt = $("#issueEndDt").val();

        if ((btnId == "issueBtn" || btnId =="setCouponBtn") && $.jUtil.diffDate(nowDtValid, issueStartDt, "D") < 0) {
            alert("발급일자 시작일은 과거일을 선택할 수 없습니다.");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        if ($.jUtil.diffDate(issueStartDt, issueEndDt, "D") < 0) {
            alert("발급일자 종료일은 시작일 보다 빠를 수 없습니다.");
            $("#issueStartDt").val(issueEndDt);
            return false;
        }

        if ($.jUtil.diffDate(issueStartDt, issueEndDt, "D") > 365) {
            alert("발급기간은 1년을 초과할 수 없습니다.");
            $("#issueStartDt").val(issueEndDt);
            return false;
        }

        if ($.jUtil.diffDate((purchaseStdDt + " 00:00"), issueStartDt, "D") < 0) {
            alert("구매기준일은 발급일 이후로 입력이 불가능합니다.");
            $("#purchaseStdDt").val('');
            return false;
        }

        //난수쿠폰 사용 여부
        if (randomNoIssueYn == "Y") {
            var randomNoType = $("#randomNoType").val();
            var issueLimitType = $("#issueLimitType").val();
            var randomNo = $("#randomNo").val();

            if (randomNoType == 'MULTI') {
                if ($.jUtil.isEmpty(randomNo)) {
                    alert("다회성 난수쿠폰번호를 입력하세요.");
                    return false;
                }

                if (randomNo.length < 3 || randomNo.length > 12) {
                    alert("난수쿠폰번호는 3~12자리까지 입력가능합니다.");
                    return false;
                }

                if (!$.jUtil.isAllowInput(randomNo, ['NUM', 'ENG', 'SHARP'])){
                    alert("난수쿠폰번호는 '#', 숫자, 영문이외에는 입력할 수 없습니다.");
                    return false;
                }
            }

            if (issueLimitType != "PERIOD") {
                alert("난수쿠폰 발행 시 총 발급수량은 [기간]선택만 가능합니다.");
                return false;
            }
        }

        if (couponType != COUPON_TYPE_IMME) {
            // 적용기간이 '발급후적용' 일 경 시간체크
            if (validType == COUPON_VALID_TYPE_DATE) {
                if ($.jUtil.isEmpty(validDay)) {
                    alert("적용기간을 입력하세요.");
                    return false;
                }

                if (!limitNumber(validDay, "date")) {
                    alert("적용기간이 '발급일로부터'일 경우 최대 365일까지 가능합니다.");
                    return false;
                }
            } else { //PERIOD
                if (!moment(validStartDt, 'YYYY-MM-DD HH:mm', true).isValid()) {
                    alert("적용기간 날짜 형식이 맞지 않습니다.");
                    $("#validStartDt").val(nowDt);
                    return false;
                }

                validStartDt = $("#validStartDt").val();
                validEndDt = $("#validEndDt").val();

                if ((btnId == "issueBtn" || btnId =="setCouponBtn") && $.jUtil.diffDate(nowDtValid, validStartDt, "D") < 0) {
                    alert("적용기간 시작일은 과거일을 선택할 수 없습니다.");
                    $("#validStartDt").val(nowDt);
                    return false;
                }

                if ($.jUtil.diffDate(validStartDt, validEndDt, "D") < 0) {
                    alert("적용기간 종료일은 시작일 보다 빠를 수 없습니다.");
                    $("#issueStartDt").val(validEndDt);
                    return false;
                }

                if ($.jUtil.diffDate(issueStartDt, validStartDt, "D") < 0) {
                    alert("적용기간은 발급기간 보다 빠를 수 없습니다.");
                    $("#validStartDt").val($("#issueStartDt").val());
                    return false;
                }

                if ($.jUtil.diffDate(issueEndDt, validEndDt, "D") < 0) {
                    alert("적용기간은 발급기간 보다 빠를 수 없습니다.");
                    $("#validEndDt").val($("#issueEndDt").val());
                    return false;
                }
            }
        }

        var issueTimeYn = $("#issueTimeYn").val();
        var issueStartTime = $("#issueStartTime").val();
        var issueEndTime = $("#issueEndTime").val();

        if (issueTimeYn == 'Y') {
            if (!moment(issueStartTime, 'HH:mm', true).isValid()) {
                alert("발급시간대(시작) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            } else if (!moment(issueEndTime, 'HH:mm', true).isValid()) {
                alert("발급시간대(종료) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            }

            if (issueStartTime > issueEndTime) {
                alert("발급시간대 시작시간은 종료시간 보다 빠를 수 없습니다.");
                return false;
            }
        }

        var shipSlotYn = $("#shipSlotYn").val();
        var slotStartTime = $("#slotStartTime").val();
        var slotEndTime = $("#slotEndTime").val();

        if (shipSlotYn == 'Y') {
            if (!moment(slotStartTime, 'HH:mm', true).isValid()) {
                alert("배송Slot시간대(시작) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            } else if (!moment(slotEndTime, 'HH:mm', true).isValid()) {
                alert("배송Slot시간대(종료) 형식이 맞지 않습니다. 시:분만 입력 가능합니다.");
                return false;
            }

            if (slotStartTime > slotEndTime) {
                alert("배송Slot시간대 시작시간은 종료시간 보다 빠를 수 없습니다.");
                return false;
            }
        }

        /*if (shipSlotYn == 'Y' && couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_SHIP) {
           if($("input:checkbox[class=chkSlotDay]:checked").length == 0) {
               alert("배송 Slot 적용요일을 선택하세요.");
               return false;
           }
        }*/

        var issueLimitType = $("#issueLimitType").val();
        var issueMaxCnt = $("#issueMaxCnt").val();

        if (issueLimitType != "UNLIMIT" && issueMaxCnt == 0) {
            alert("총 발급매수를 입력하세요.");
            return false;
        } else if (issueMaxCnt > 1000000) {
            alert("총 발급매수는 100만까지 입력가능합니다.");
            return false;
        }

        if (couponType != COUPON_TYPE_IMME) {
            var limitPerCnt = $("#limitPerCnt").val();
            if (limitPerCnt == '' || parseInt(limitPerCnt) == 0 ) {
                alert("1일 발급수량을 입력하세요.");
                return false;
            }

            if (parseInt(limitPerCnt) > 99 ) {
                alert("1인 발급수량은 최대 99까지 입력이 가능 합니다.");
                return false;
            }
        }

        var limitPerDayYn = $("#limitPerDayYn").val();
        var limitPerDayCnt = $("#limitPerDayCnt").val();

        if (limitPerDayYn == 'Y') {
            if (limitPerDayCnt == '' || limitPerDayCnt == 0) {
                alert("1인 1일 발급수량을 입력하세요.");
                return false;
            }

            if (parseInt(limitPerDayCnt) > 99) {
                alert("1인 1일 발급수량은 최대 99까지 입력이 가능 합니다.");
                return false;
           }
        }

        return true;
    }
};

// 달력 초기화
var calendarInit = {
    searchPeriod : function (flag) {
        couponSearch.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");

        couponSearch.setDate.setStartDate(flag);
        couponSearch.setDate.setEndDate(0);
    },
    issuePeriod : function (flag) {
        coupon.setIssueDate = CalendarTime.datePickerRange("issueStartDt", "issueEndDt", {timeFormat: 'HH:mm'});

        coupon.setIssueDate.setStartDate(0);
        coupon.setIssueDate.setEndDateByTimeStamp(flag);
    },
    validPeriod : function (flag) {
        coupon.setValidDate = CalendarTime.datePickerRange("validStartDt", "validEndDt", {timeFormat: 'HH:mm'});

        coupon.setValidDate.setStartDate(0);
        coupon.setValidDate.setEndDateByTimeStamp(flag);
    },
    purchaseStdDt : function () {
        coupon.setPurchaseStdDt = Calendar.datePicker("purchaseStdDt");
    },
    gradeCouponMonth : function () {
        monthConfig.onSelect = function (dataText, inst) {
            var firstDate = dataText + "-01 00:00";
            var lastDate = moment(dataText + "-01").endOf('month').format('YYYY-MM-DD 23:59');

            $("#issueStartDt, #validStartDt").val(firstDate);
            $("#issueEndDt, #validEndDt").val(lastDate);
        };

        $("#gradeCouponMonth").monthpicker(monthConfig);
    }
};

// 쿠폰관리 조회
var couponSearch = {
    setDate: null,
    init : function () {
        couponSearch.searchFormReset();
        couponSearch.bindingEvent();
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $("#couponSearchForm").resetForm();

        // 조회 기간 초기화
        calendarInit.searchPeriod( "0");
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#schBtn").bindClick(couponSearch.searchCouponList);
        $("#schResetBtn").bindClick(couponSearch.searchFormReset);
        $("#excelDownloadBtn").bindClick(couponListGrid.excelDownload);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                couponSearch.searchCouponList();
            }
        });
    },
    /**
     * 쿠폰 정보 조회
     */
    searchCouponList : function (couponNo) {
        // 검색조건 validation check
        if (validCheck.search()) {
            // 쿠폰 정보 조회 개수 초기화
            $("#couponTotalCount").html("0");

            // 쿠폰 상세 초기화
            coupon.couponFormReset();

            // 조회
            var couponSearchForm = $("#couponSearchForm").serialize();

            CommonAjax.basic({
                url: "/coupon/getCouponList.json"
                , data: couponSearchForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    couponListGrid.setData(res);
                    $("#couponTotalCount").html(couponListGrid.dataProvider.getRowCount());

                    if (couponNo !== undefined && $.jUtil.isNotEmpty(couponNo)) {
                        var rowIndex = couponListGrid.dataProvider.searchDataRow({fields: ["couponNo"], values: [couponNo]});

                        if (rowIndex > -1) {
                            couponListGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                            couponListGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    },
};

// 쿠폰관리
var coupon = {
    setIssueDate : null,
    setValidDate : null,
    copyYn : "N",
    detailOriginalData : null,
    storeList : null,
    paymentList : null,
    barcode : null,
    /**
     * 초기화
     */
    init : function() {
        coupon.bindingEvent();
        coupon.couponFormReset();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#displayItYn, #displayCzYn").bindChange(coupon.changeDisplayYn);    //노출설정
        $("#setCouponBtn").bindClick(coupon.setCouponData);         // 저장
        $("#copyBtn").bindClick(coupon.setCopyCoupon);              // 복사
        $("#resetBtn").bindClick(coupon.couponFormReset);           // 초기화
        $("#getStoreInfoBtn").bindClick(coupon.setStorePop); // 점포선택
        $("#cardInfoBtn").bindClick(coupon.setCardPop);
        $("#shareHomeYn, #shareSellerYn, #shareCardYn").bindClick(coupon.checkShare);   // 분담 설정 선택
        $("#deleteApply").bindClick(coupon.removeApplyGridData);    // 적용대상 삭제
        $("#popupApplySearch").bindClick(coupon.openPopup);             // 적용대상 등록
        $("#pullCmsProduct").bindClick(coupon.getCmsCouponProdList);             // 적용대상 등록
        $("#applyExcelDownloadBtn").bindClick(applyGrid.excelDownload); // 적용대상 엑셀 다운로드
        $("#applyGridSearchBtn").bindClick(coupon.applyGridSearch);     // 적용대상 그리드 내 검색
        $("#applyGridSearchInitBtn").bindClick(coupon.applyGridSearchClear);    // 적용대상 그리드 내 검색 초기화
        $("#applyGridSearchValue").keyup(function (e) {
            if (e.keyCode == 13) {
                coupon.applyGridSearch();
            }
        });
        $("#popupApplyAllSearch").bindClick(coupon.callUploadScopePopup);
        $("#storeType").bindChange(coupon.changeStoreType);         // 점포유형
        $("#couponType").bindChange(coupon.changeCouponType);       // 쿠폰종류
        $("#discountType").bindChange(coupon.changeDiscountType);   // 할인방법
        $("#shareYn").bindChange(coupon.changeShareYn);             // 분담설정여부
        $("#issueLimitType").bindChange(coupon.changeIssueLimitType);  // 총발급수량 제한
        $("#validType").bindChange(coupon.changeValidType);         // 유효기간타입
        $("#limitPerDayYn, #issueTimeYn, #shipSlotYn").bindChange(coupon.changeDisplayStyle); // 1인 요일별 발급수량, 발급시간대, 적용시간대, 배송Slot시간
        $("#paymentType").bindChange(coupon.changePaymentType);
        $("#applyScope").bindChange(coupon.changeApplyScope);        // 적용 범위
        $("#barcodePopBtn").bindClick(coupon.setBarcodePop);
        $("#cartBarcode").bindChange(coupon.changeCartBarcode);

        $("#manageCouponNm").calcTextLength("keyup", "#manageCouponNmLen");
        $("#displayCouponNm").calcTextLength("keyup", "#displayCouponNmLen");

        $("#discountPrice, #discountRate, #discountMax, #purchaseMin, #shareHome, #shareSeller, #shareCard, #validDay, #issueMaxCnt, #limitPerCnt, #limitPerDayCnt").allowInput("keyup", ["NUM"], $(this).attr("id"));

        $("#purpose").bindChange(coupon.changePurpose);
        $("#gradeCouponYn").bindChange(coupon.changeGradeCouponYn);
        $("#randomNoIssueYn").bindChange(coupon.changeRandomNoIssueYn);
        $("#randomNoType").bindChange(coupon.changeRandomNoType);
        $("#randomListDownloadBtn").bindClick(coupon.randomListDownload);

        // 쿠폰 제외상품 관리 그리드 컨트롤 관련 이벤트
        $("#popupExceptApplyAllSearch").bindClick(coupon.callUploadScopePopup);
        $("#deleteExcept").bindClick(coupon.removeExceptGridData);             // 제외상품 삭제
        $("#popupExceptSearch").bindClick(coupon.exceptItemPopup);             // 제외상품 등록
        $("#exceptExcelDownloadBtn").bindClick(exceptGrid.excelDownload);    // 제외상품 엑셀 다운로드
        $("#exceptGridSearchBtn").bindClick(coupon.exceptGridSearch);          // 제외상품 그리드 내 검색
        $("#exceptGridSearchInitBtn").bindClick(coupon.exceptGridSearchClear); // 제외상품 그리드 내 검색 초기화
        $("#exceptGridSearchValue").keyup(function (e) {
            if (e.keyCode == 13) {
                coupon.exceptGridSearch();
            }
        });

        $("#issueBtn, #stopBtn, #withdrawBtn, #delBtn").on("click", function() {
            coupon.setCouponStatus($(this).attr("id"));
        });

        $("#storeInfoBtn").bindClick(coupon.setStorePop); // 점포선택

        //구매유도일
        calendarInit.purchaseStdDt();
    },
    /**
     * 노출설정에 대한 얼럿
     * */
    changeDisplayYn : function (obj) {
        var id = $(obj).attr("id");
        var isChecked = $(obj).prop("checked");

        if (id =='displayItYn' && !isChecked) {
            alert("상품상세 화면에 노출되지 않습니다.");
        } else if (id =='displayCzYn' && !isChecked) {
            alert("쿠폰존에 노출되지 않습니다.");
        }
    }
    ,
    /**
     * 쿠폰 상세 내용 초기화
     */
    couponFormReset : function () {
        coupon.formElementControl(false);

        $("#couponForm").resetForm();
        $("#couponForm input[type='hidden']").val("");

        // span 초기화
        $("#manageCouponNmLen, #displayCouponNmLen").html("0");
        $("#offlineCouponNo").html("");
        $("#storeCnt").html("0");

        // 원본 데이터 초기화
        coupon.detailOriginalData = null;
        coupon.storeList = null;
        coupon.paymentList = null;
        coupon.barcode = null;
        coupon.copyYn = "N";

        // hidden 초기화
        $("#status").val("TEMP");
        $("#statusNm").text("");

        // 이벤트 호출
        $("#storeType").change();
        $("#couponType").change();
        $("#discountType").change();
        $("#shareYn").change();
        $("#validType").change();
        $("#limitPerDayYn, #issueTimeYn, #shipSlotYn").change();
        $("#paymentType").change();
        $("#applyScope").change();

        coupon.setBtnControl("", "");
    },
    /**
     * 점포유형 선택 변경 시
     * @param obj
     */
    changeStoreType : function(obj) {
        var couponTypeCd = $('#couponType').val();
        var storeTypeCd = $(obj).val();
        var storeType = $('#storeType');
        var applyScope = $("#applyScope");
        var displayItYn = $("#displayItYn");
        var displayCzYn = $("#displayCzYn");
        var status = $("#status").val();
        var discountType = $("#discountType");
        var minPurchase = $("#minPurchase");
        var shareYn = $("#shareYn");
        var validType = $("#validType");
        var issueLimitType = $("#issueLimitType");
        var itemDuplYn = $("#itemDuplYn");
        var limitPerDayYn = $("#limitPerDayYn");
        var limitPerCnt = $("#limitPerCnt");
        var issueTimeYn = $("#issueTimeYn");
        var shipSlotYn = $("#shipSlotYn");
        var combineOrderAvailYn = $("#combineOrderAvailYn");
        var pickupAvailYn = $("#pickupAvailYn");
        var paymentType = $("#paymentType");
        var randomNoIssueYn = $("#randomNoIssueYn");
        var randomNoType = $("#randomNoType");
        var departCd = $("#departCd");
        var cartBarcode = $("#cartBarcode");
        var gradeCouponYn = $("#gradeCouponYn");
        var deactivateAttr ;

        //addSelectOption(couponType);
        addSelectOption(shareYn);
        addSelectOption(validType);
        addSelectOption(discountType);
        addSelectOption(limitPerDayYn);
        addSelectOption(issueTimeYn);
        addSelectOption(shipSlotYn);
        addSelectOption(itemDuplYn);
        addSelectOption(issueLimitType);
        addSelectOption(combineOrderAvailYn);
        addSelectOption(pickupAvailYn);
        addSelectOption(applyScope);
        addSelectOption(paymentType);
        addSelectOption(randomNoIssueYn);
        addSelectOption(randomNoType);
        addSelectOption(departCd);
        addSelectOption(cartBarcode);
        addSelectOption(gradeCouponYn);

        if (storeTypeCd == STORE_TYPE_DS) {
            departCd.val("33004");
            $("#storeInfoBtn").prop("disabled", true);
        } else {
            if (couponTypeCd == COUPON_TYPE_CART || couponTypeCd == COUPON_TYPE_DUPL || couponTypeCd == COUPON_TYPE_SHIP ) {
                departCd.val("31");
            } else if (couponTypeCd == COUPON_TYPE_ITEM || couponTypeCd == COUPON_TYPE_IMME) {
                departCd.val("01");
            }

            $("#storeInfoBtn").prop("disabled", false);
        }

        coupon.storeList = null;
        $("#storeCnt").html("0");

        $("#barcode, #barcodeCouponSeq").val("");

        //할인방법
        $(discountType).prop("disabled", false);
        //정액/정률 시 값
        $("#discountRate, #discountPrice").prop("disabled", false);
        //적용시스템
        $("#applyPcYn, #applyAppYn, #applyMwebYn").prop("checked", true);

        switch (couponTypeCd) {
            case COUPON_TYPE_DUPL:
                deactivateAttr = "noDupl";

                if (storeTypeCd == STORE_TYPE_DS) {
                    removeSelectOption(randomNoIssueYn, "Y");
                    removeSelectOption(issueTimeYn, "Y");
                    removeSelectOption(applyScope, COUPON_APPLY_SCOPE_ALL);
                } else {
                    removeSelectOption(applyScope, COUPON_APPLY_SCOPE_SELL);
                    $("#cartBarcode").show();
                    $("#barcodeInputLine").hide();
                    $("#cartBarcode").change();
                }

                //노출설정
                $("#displayItYn, #displayCzYn").prop("checked", false);
                //쿠폰 바코드 조회
                $("#barcodePopBtn").prop("disabled", true);

                //1인 발급수량
                limitPerCnt.val("1");

                if (storeTypeCd == STORE_TYPE_EXP || storeTypeCd == STORE_TYPE_DS) {
                    removeSelectOption(shipSlotYn, "Y");
                    removeSelectOption(combineOrderAvailYn, "Y");
                    removeSelectOption(pickupAvailYn, "Y");
                } else {
                    //합주문 가능여부
                    combineOrderAvailYn.val("Y");
                    //팍업 가능여부
                    pickupAvailYn.val("Y");
                }

                removeSelectOption(shareYn, "Y");
                removeSelectOption(itemDuplYn, "N");
                removeSelectOption(gradeCouponYn, "Y");

                break;
            case COUPON_TYPE_CART:
                deactivateAttr = "noCart";
                removeSelectOption(shareYn, "Y");
                removeSelectOption(applyScope, COUPON_APPLY_SCOPE_SELL);

                $("#cartBarcode").show();
                $("#barcodeInputLine").hide();
                //노출설정
                $("#displayItYn, #displayCzYn").prop("checked", false);
                //쿠폰 바코드 조회
                $("#barcodePopBtn").prop("disabled", true);

                //1인 발급수량
                limitPerCnt.val("1");

                if (storeTypeCd == STORE_TYPE_EXP) {
                    removeSelectOption(shipSlotYn, "Y");
                    removeSelectOption(combineOrderAvailYn, "Y");
                    removeSelectOption(pickupAvailYn, "Y");
                } else {
                    //합주문 가능여부
                    combineOrderAvailYn.val("Y");
                    //팍업 가능여부
                    pickupAvailYn.val("Y");
                }
                $("#cartBarcode").change();

                break;
            case COUPON_TYPE_ITEM:
                deactivateAttr = "noItem";

                if (storeTypeCd != STORE_TYPE_DS) {
                    removeSelectExceptOption(applyScope, COUPON_APPLY_SCOPE_ITEM);
                    //쿠폰 바코드 조회 버튼
                    $("#barcodePopBtn").prop("disabled", false);

                    //할인방법
                    $("#discountType").prop("disabled", true);

                    //분담설정(설정안함 고정)
                    removeSelectOption(shareYn, "Y");
                } else {
                    removeSelectOption(applyScope, COUPON_APPLY_SCOPE_ALL);
                    //쿠폰 바코드 조회 버튼
                    $("#barcodePopBtn").prop("disabled", true);
                    //할인방법
                    $("#discountType").prop("disabled", false);
                }

                if (storeTypeCd == STORE_TYPE_HYPER) {
                    removeSelectOption(combineOrderAvailYn, "N");
                    removeSelectOption(pickupAvailYn, "N");
                } else {
                    removeSelectOption(combineOrderAvailYn, "Y");
                    removeSelectOption(pickupAvailYn, "Y");
                }

                removeSelectOption(gradeCouponYn, "Y");
                removeSelectOption(randomNoIssueYn, "Y");
                removeSelectOption(itemDuplYn, "Y");
                removeSelectOption(issueTimeYn, "Y");
                removeSelectOption(shipSlotYn, "Y");

                //노출설정
                $("#displayItYn, #displayCzYn").prop("checked", true);

                //1인 발급수량
                limitPerCnt.val("1");
                break;
            case COUPON_TYPE_IMME:
                deactivateAttr = "noImme";

                removeSelectOption(gradeCouponYn, "Y");
                removeSelectOption(itemDuplYn, "Y");
                removeSelectOption(validType, "DATE");
                removeSelectOption(shareYn, "Y");
                removeSelectOption(issueTimeYn, "Y");
                removeSelectOption(limitPerDayYn, "Y");
                removeSelectOption(shipSlotYn, "Y");
                removeSelectOption(randomNoIssueYn, "Y");
                removeSelectExceptOption(issueLimitType, "UNLIMIT");
                removeSelectExceptOption(applyScope, COUPON_APPLY_SCOPE_ITEM);

                if (storeTypeCd == STORE_TYPE_HYPER) {
                    removeSelectOption(combineOrderAvailYn, "N");
                    removeSelectOption(pickupAvailYn, "N");
                } else {
                    removeSelectOption(combineOrderAvailYn, "Y");
                    removeSelectOption(pickupAvailYn, "Y");
                }

                //노출설정
                $("#displayItYn, #displayCzYn").prop("checked", false);
                //쿠폰 바코드 조회
                $("#barcodePopBtn").prop("disabled", false);
                //할인방법
                $("#discountType").prop("disabled", true);

                $("#validStartDt, #validEndDt").val("");

                //1인 발급수량
                limitPerCnt.val("");

                break;
            case COUPON_TYPE_SHIP:
                deactivateAttr = "noShip";

                if (storeTypeCd != STORE_TYPE_DS) {
                    removeSelectExceptOption(applyScope, COUPON_APPLY_SCOPE_ALL);
                } else {
                    removeSelectOption(gradeCouponYn, "Y");
                    removeSelectExceptOption(applyScope, COUPON_APPLY_SCOPE_SELL);
                }

                if (storeTypeCd == STORE_TYPE_EXP || storeTypeCd == STORE_TYPE_DS) {
                    removeSelectOption(shipSlotYn, "Y");
                }

                removeSelectOption(itemDuplYn, "N");
                removeSelectOption(shareYn, "Y");
                removeSelectOption(issueTimeYn, "Y");
                removeSelectOption(combineOrderAvailYn, "Y");
                removeSelectOption(pickupAvailYn, "Y");
                removeSelectOption(randomNoIssueYn, "Y");
                removeSelectExceptOption(discountType, COUPON_DISCOUNT_TYPE_RATE);

                //노출설정
                $("#displayItYn, #displayCzYn").prop("checked", false);
                //쿠폰 바코드 조회
                $("#barcodePopBtn").prop("disabled", true);

                //1인 발급수량
                limitPerCnt.val("1");

                break;
        }
        couponTypeControl.deactivateControl(deactivateAttr);
        applyScope.trigger("change");

        if (!(couponTypeCd == COUPON_TYPE_CART || (couponTypeCd == COUPON_TYPE_DUPL && storeTypeCd != STORE_TYPE_DS))) {
            $("#cartBarcode").hide();
            $("#barcodeInputLine").show();

            //발급요일
            $(".dayChk").prop("checked", false);

            removeSelectExceptOption(paymentType, '');
        }

        // 발급기간 초기화
        calendarInit.issuePeriod("1w");

        if (couponTypeCd == COUPON_TYPE_IMME) {
            //적용시스템
            $("#applyPcYn, #applyAppYn, #applyMwebYn").prop("checked", true).attr("onClick", "return false;");
        } else {
            calendarInit.validPeriod("1w");
        }

        //등급쿠폰 기간
        calendarInit.gradeCouponMonth();

        $("#shareYn").val("N").change();
        randomNoType.change();
        $("#purchaseMin").val("0");
        issueTimeYn.val("N").change();
        shipSlotYn.val("N").change();
        discountType.change();
        issueLimitType.change();
        limitPerDayYn.val("N").change();
        paymentType.val("").change();
        randomNoIssueYn.val("N").change();
        gradeCouponYn.val("N").change();
        validType.change();
    },
    changeCouponType : function(obj) {
        var couponTypeCd = $(obj).val();
        var storeType = $("#storeType");

        addSelectOption(storeType);
        if (couponTypeCd == COUPON_TYPE_CART || couponTypeCd == COUPON_TYPE_IMME) {
            removeSelectOption(storeType, STORE_TYPE_DS);
        }

        $(storeType).change();
    },
    changeCartBarcode : function(obj) {
        var departCd = $(obj).find('option:selected').data('dept');
        $("#departCd").val(departCd);
    },
    changePaymentType : function(obj) {
        var paymentType = $(obj).val();

        if (paymentType == 'CARD') {
            $("#thDetailPayment, #tdDetailPayment").show();
            $("#tdPayment").prop("colspan", 1);
            $("#cardInfoBtn").text("카드종류");
        } else if (paymentType == 'EASYPAY') {
            $("#thDetailPayment, #tdDetailPayment").show();
            $("#tdPayment").prop("colspan", 1);
            $("#cardInfoBtn").text("간편결제");
        } else {
            $("#thDetailPayment, #tdDetailPayment").hide();
            $("#tdPayment").prop("colspan", 3);
        }
        coupon.paymentList = null;
        $('.cardPop input[name="paymentList"]').val("");
        $("#cardCnt").text("0");
    },
    /**
     * 할인방법 선택 변경시 - 할인금액/할인율, 최대할인금액 초기화
     * @param obj
     */
    changeDiscountType : function(obj) {
        var storeType = $("#storeType").val();
        var couponType = $("#couponType").val();

        //정액/정률 시 값
        if (!(storeType == STORE_TYPE_DS || couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_DUPL) || (storeType == STORE_TYPE_DS && couponType == COUPON_TYPE_SHIP)) {
            $("#discountRate, #discountPrice").prop("readonly", true);
        } else {
            $("#discountRate, #discountPrice").prop("readonly", false);
        }

        if ($(obj).val() == COUPON_DISCOUNT_TYPE_PRICE) { // 정액
            $("#discountPriceSpan").show();
            $("#discountRateSpan").hide();
            $("#discountRate, #discountMax").val("");
        } else {
            $("#discountPriceSpan").hide();
            $("#discountRateSpan").show();
            $("#discountPrice").val("");
            if ($("#couponType").val() == COUPON_TYPE_SHIP) {
                $("#discountRate").val("100");
                $("#discountMax").val("999999990");
            } else {
                $("#discountRate, #discountMax").val("");
            }
        }
    },
    /**
     * 분담여부 선택 변경시 - 분담방법 초기화
     * @param obj
     */
    changeShareYn : function(obj) {
        var shareChkObj = $("input[name^=share]");

        if ($("storeType").val() != STORE_TYPE_DS) {
            $("#shareHome").val("100");
            $("#shareSeller, #shareCard").val("0");
        } else {
            $("#shareSeller").val("100");
            $("#shareHome, #shareCard").val("0");
        }

        if ($(obj).val() == "Y") {
            $("#shareSpan").show();
            shareChkObj.val("0");
        } else {
            $("#shareSpan").hide();
        }

        // 분담여부를 변경하면 분담설정값들을 초기화함.
        shareChkObj.prop({"checked":false, "readonly":true});
    },
    /**
     * 각 분담방법 체크 변경시 - 해당 분담률 설정 변경
     * @param obj
     */
    checkShare : function(obj) {
        var status = $("#status").val();

        if (status == "TEMP" || $.jUtil.isEmpty(status)) {
            var chkVal = $(obj).is(":checked");
            var objId = $(obj).attr("id");
            var shareId = "#" + objId.toString().substr(0, objId.toString().lastIndexOf("Yn"))

            $(shareId).val((chkVal ? $(shareId).val() : "0"));
            $(shareId).prop("readonly", (chkVal ? false : true));
        }
    },
    /**
     *
     * @param obj
     */
    changeIssueLimitType : function(obj) {
        if ($(obj).val() == 'UNLIMIT') {
            $("#issueMaxSpan").hide();
            $("#issueMaxCnt").val("");
        } else {
            $("#issueMaxSpan").show();
        }
    },
    /**
     * 유효기간 타입 변경 시 - 타입별 UI 제어
     * @param obj
     */
    changeValidType : function(obj) {
        if ($(obj).val() == COUPON_VALID_TYPE_PERIOD) {
            $("#periodSpan").show();
            $("#dateSpan").hide();
        } else {
            $("#periodSpan").hide();
            $("#dateSpan").show();
        }
    },
    /**
     * 1인 요일별 발급수량, 발급시간대, 적용시간대, 배송Slot시간 사용여부 변경 시 - 각 입력항목 UI 제어
     * @param obj
     */
    changeDisplayStyle : function(obj) {
        var spanId = "#" + $(obj).attr("id").toString().replace("Yn", "Span");

        if ($(obj).val() == "Y") {
            $(spanId).show();

            var couponType = $("#couponType").val();

            if ($(obj).attr("id") == "shipSlotYn" && (couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_SHIP || couponType == COUPON_TYPE_DUPL)) {
                $("input:checkbox[class=chkSlotDay]").prop("checked", true).attr("onClick", "return true;");
            }
        } else {
            $(spanId).hide();
            $(spanId + " input").val("");
            if ($(obj).attr("id") == "shipSlotYn") {
                $("input:checkbox[class=chkSlotDay]").prop("checked", false).attr("onClick", "return false;");
            }
        }
    },
    changePurpose : function (obj) {
        var purpose = $(obj).val();

        //재구미유도(웰컴)
        if (purpose == PURPOSE_RE_PURCHASE) {
            $("#spanPurchaseStdDt").show();
        } else {
            $("#spanPurchaseStdDt").hide();
            $("#purchaseStdDt").val("");
        }
    },
    changeRandomNoIssueYn : function(obj) {
        var randomNoIssueYn = $(obj).val();
        var couponType = $("#couponType").val();

        $("#randomNo").val("");

        if (randomNoIssueYn == "Y") {
            $(".randomInfo").show();
            $("#randomNoType").change();

            $("#displayItYn, #displayCzYn").prop("checked", false).attr("onClick", "return false;");
            removeSelectExceptOption($("#issueLimitType"), "PERIOD");
        } else {
            $(".randomInfo").hide();
            if (couponType == COUPON_TYPE_IMME) {
                $("#displayItYn, #displayCzYn").attr("onClick", "return false;");
                removeSelectExceptOption($("#issueLimitType"), "UNLIMIT");
            } else {
                $("#displayItYn, #displayCzYn").attr("onClick", "return true;");
                addSelectOption($("#issueLimitType"));
            }
        }
        $("#issueLimitType").change();
    },
    randomListDownload : function(obj) {
        var couponNo = $("#couponNo").val();
        var couponType = $("#couponType").val();
        var status = $("#status").val();

        if (!(couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_DUPL) || status == "TEMP" || $.jUtil.isEmpty(couponNo)) {
            return;
        }
        $('#excelDownloadFrame').attr('src', "/coupon/downloadCouponRandom.json?couponNo=" + couponNo);
    },
    changeRandomNoType : function(obj) {
        var randomNoType = $(obj).val();
        $("#randomNo").val("");

        if (randomNoType == "ONCE") {
            $("#randomListDownloadBtn, #radomNotiText").show();
            $("#randomNo").hide();

            if ($("#randomListDownAvailYn").val() == 'Y') {
                $("#randomListDownloadBtn").prop("disabled", false);
            } else {
                $("#randomListDownloadBtn").prop("disabled", true);
            }
        } else {
            $("#randomListDownloadBtn, #radomNotiText").hide();
            $("#randomNo").show();
        }
    },
    changeGradeCouponYn : function(obj) {
        var gradeCouponYn = $(obj).val();
        var couponType = $("#couponType").val();
        var storeType = $("#storeType").val();

        $("#gradeCouponMonth").val("").prop("readonly", true);
        $("#userGradeSeq").val("");

        addSelectOption($("#userGradeSeq"));
        addSelectOption($("#validType"));

        var purpose = $("#purpose");
        addSelectOption($("#purpose"));

        if (gradeCouponYn == "Y") {
            $("#targetMonth").show();
            removeSelectExceptOption($("#validType"), "PERIOD");
            removeSelectOption($("#randomNoIssueYn"), "Y");
            removeSelectExceptOption($("#purpose"), "43040");
            $("#issueStartDt, #issueEndDt, #validStartDt, #validEndDt").prop("readonly", true);
            $(".ui-datepicker-trigger").attr("disabled", true);
        } else {
            $("#targetMonth").hide();

            if (couponType == COUPON_TYPE_IMME) {
                removeSelectOption($("#validType"), "DATE");
                removeSelectOption(purpose, "CRM");
                removeSelectOption(purpose, "REPURCHASE");
                removeSelectOption(purpose, "MHC");
                removeSelectOption(purpose, "EASY");
            } else {
                $("#issueStartDt, #issueEndDt, #validStartDt, #validEndDt").prop("readonly", false);
                $(".ui-datepicker-trigger").attr("disabled", false);
            }

            removeSelectExceptOption($("#userGradeSeq"), '');

            if (couponType == COUPON_TYPE_CART || (couponType == COUPON_TYPE_DUPL && storeType != STORE_TYPE_DS)) {
                addSelectOption($("#randomNoIssueYn"));
            }
        }

        $("#validType").change();
        $("#randomNoIssueYn").val('N').change();
        $("#purpose").change();
    },
    /**
     * 적용 범위 변경 시 - 적용 대상 그리드 header 설정 변경 및 적용 상품 정보 영역 초기화
     * @param obj
     */
    changeApplyScope : function(obj) {
        var applyScope = $(obj).val();

        var $applyGridSearchType = $("#applyGridSearchType");
        var $applyGridSearchValue = $("#applyGridSearchValue");

        // 적용대상 그리드 검색 영역 초기화
        $applyGridSearchType.find("option").remove();
        $applyGridSearchValue.val("");

        var applyCdTxt, applyNmTxt = "";

        switch (applyScope) {
            default :
            case "" :
            case COUPON_APPLY_SCOPE_ALL :
                break;
            case COUPON_APPLY_SCOPE_ITEM:
                $("#popupApplySearch").text("상품 등록");
                applyCdTxt  = "상품번호";
                applyNmTxt  = "상품명";
                break;
            case COUPON_APPLY_SCOPE_SELL:
                $("#popupApplySearch").text("판매자 등록");
                applyCdTxt  = "판매자ID";
                applyNmTxt  = "판매자명";
                break;
            case COUPON_APPLY_SCOPE_CATE:
                $("#popupApplySearch").text("카테고리 등록");
                applyCdTxt  = "카테고리번호";
                applyNmTxt  = "카테고리명";
                break;
        }

        // 적용대상 그리드 초기화
        applyGrid.dataProvider.clearRows();
        // 적용대상 건수
        coupon.setApplyGridCnt(applyGrid.dataProvider.getRowCount());
        //coupon.setApplyGridSearchCnt(applyGrid.gridView.getItemCount());
        coupon.applyGridSearchClear();

        // 적용 버튼 컨트롤
        coupon.setScopeBtnControl($("#status").val());

        // 적용 대상이 전체인 경우 UI 노출 컨트롤
        if (applyScope == COUPON_APPLY_SCOPE_ALL || applyScope == "") {
            $(".noNeedAllScope").hide();
        } else {
            $(".noNeedAllScope").show();
            applyGrid.gridView.resetSize();
        }

        // 제외상품 그리드 display
        exceptGrid.setDisplay();

        // 적용대상 그리드 초기화
        exceptGrid.dataProvider.clearRows();

        coupon.setExceptGridCnt(exceptGrid.dataProvider.getRowCount());
        //coupon.setExceptGridSearchCnt(applyGrid.gridView.getItemCount());
        coupon.exceptGridSearchClear();

        if (applyScope == COUPON_APPLY_SCOPE_ALL || applyScope == "") {
            return;
        }

        applyGrid.gridView.setColumnProperty("applyCd", "header", applyGrid.gridView.getColumnProperty("applyCd", "header").text = applyCdTxt);
        applyGrid.gridView.setColumnProperty("applyNm", "header", applyGrid.gridView.getColumnProperty("applyNm", "header").text = applyNmTxt);
        applyGrid.gridView.setColumnProperty("regNm", "header", applyGrid.gridView.getColumnProperty("regNm", "header").text = "등록자");
        applyGrid.gridView.setColumnProperty("regDt", "header", applyGrid.gridView.getColumnProperty("regDt", "header").text = "등록일");
        $applyGridSearchType.append("<option value='applyCd'>" + applyCdTxt + "</option>");
        $applyGridSearchType.append("<option value='applyNm'>" + applyNmTxt + "</option>");

        // 적용대상 원본 데이터가 있을시 원본데이터로 복구
        if (coupon.detailOriginalData != null) {
            if (coupon.detailOriginalData.applyScope == applyScope) {
                applyGrid.setData(coupon.detailOriginalData.applyList);
                exceptGrid.setData(coupon.detailOriginalData.exceptItemList);
            }
        }
    },
    /**
     * 적용 대상 그리드 내 조회
     */
    applyGridSearch : function() {
        var gridSearchValue = $("#applyGridSearchValue").val();

        if ($("#applyGridSearchType").val() == "applyCd") {
            var replaceText = gridSearchValue.replace(/\s/gi, "");
            $("#applyGridSearchValue").val(replaceText);

            if (!/^[a-zA-Z0-9,]*$/.test(replaceText)) {
                alert("상품번호/카테고리번호/판매자ID로 다중 검색 시 번호 사이에 구분자 콤마(',')를 입력해주세요.");
                return;
            }

            var arrSchValueList = replaceText.split(",");

            if (arrSchValueList.length > 1000) {
                alert("상품번호/카테고리번호/판매자ID는 최대 1,000개까지 검색 가능합니다.");
                return;
            }

            gridSearchValue = arrSchValueList;
        }

        applyGrid.filter($("#applyGridSearchType").val(), gridSearchValue);

        coupon.setApplyGridSearchCnt(applyGrid.gridView.getItemCount());
    },
    /**
     * 적용 대상 그리드 내 조회 초기화
     */
    applyGridSearchClear : function() {
        $("#applyGridSearchValue").val("");

        applyGrid.clearAllFilter();
        coupon.setApplyGridSearchCnt(applyGrid.gridView.getItemCount());
    },
    /**
     * 적용 대상 건수 노출
     * @param cnt
     */
    setApplyGridCnt : function(cnt) {
        $("#applyCnt").html(cnt + "건)");
    },
    /**
     * 적용 대상 그리드의 데이터 검색 건수 노출
     * @param cnt
     */
    setApplyGridSearchCnt : function(cnt) {
        $("#applySearchCount").html("(" + cnt + "/");
    },
    /**
     * 적용 대상 그리드 검색 관련 초기화
     */
    applyGridSearchClear : function() {
        $("#applyGridSearchValue").val("");

        applyGrid.clearAllFilter();
        coupon.setApplyGridSearchCnt(applyGrid.gridView.getItemCount());
    },
    /**
     * 적용 범위별 등록 버튼 제어
     * @param status
     */
    setScopeBtnControl : function(status) {
        var deleteApplyScope = $("#deleteApply");           // 삭제
        var popupApplySearch = $("#popupApplySearch");      // 등록
        var popupApplyAllSearch = $("#popupApplyAllSearch");
        var pullCmsProduct = $("#pullCmsProduct");        // CMS 쿠폰 상품 리스트
        var barcodePopBtn = $("#barcodePopBtn");        // CMS 바코드 조회
        var excelDownloadBtn = $("#applyExcelDownloadBtn"); // 엑셀다운

        var deleteExcept = $("#deleteExcept");              // 제외상품 그리드 삭제
        var popupExceptSearch = $("#popupExceptSearch");    // 제외상품 그리드 등록
        var popupExceptApplyAllSearch = $("#popupExceptApplyAllSearch");    // 일괄 제외상품 그리드 등록

        var couponType = $("#couponType").val();
        var applyScope = $("#applyScope").val();
        var storeType = $("#storeType").val();

        switch (status) {
            case "STOP":
            case "COMP":
                deleteApplyScope.hide();
                popupApplySearch.hide();
                popupApplyAllSearch.hide();
                barcodePopBtn.hide();
                pullCmsProduct.hide();
                deleteExcept.hide();
                popupExceptSearch.hide();
                popupExceptApplyAllSearch.hide();
                break;
            case "ISSUED":
                if (applyScope != COUPON_APPLY_SCOPE_ALL && applyScope != null) {
                    deleteApplyScope.show();
                    if (storeType != STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_IMME)) {
                        barcodePopBtn.hide()
                        pullCmsProduct.hide();
                        popupApplyAllSearch.hide();
                        popupApplySearch.hide();
                    } else if (applyScope == COUPON_APPLY_SCOPE_CATE) {
                        barcodePopBtn.hide();
                        pullCmsProduct.hide();
                        popupApplyAllSearch.hide();
                        popupApplySearch.show();
                    } else {
                        barcodePopBtn.hide();
                        pullCmsProduct.hide();
                        popupApplyAllSearch.show();
                        popupApplySearch.show();
                    }
                } else {
                    deleteApplyScope.hide();
                    barcodePopBtn.hide();
                    pullCmsProduct.hide();
                    popupApplySearch.hide();
                    popupApplyAllSearch.hide();
                }
                deleteExcept.show();
                popupExceptSearch.show();
                popupExceptApplyAllSearch.show();

                break;
            case "":
            case "TEMP":
                if (applyScope != COUPON_APPLY_SCOPE_ALL && applyScope != null) {
                    deleteApplyScope.show();
                    if (storeType != STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_IMME)) {
                        barcodePopBtn.show();
                        pullCmsProduct.show();
                        popupApplyAllSearch.hide();
                        popupApplySearch.hide();
                    } else if (applyScope == COUPON_APPLY_SCOPE_CATE) {
                        barcodePopBtn.hide()
                        pullCmsProduct.hide();
                        popupApplyAllSearch.hide();
                        popupApplySearch.show();
                    } else {
                        barcodePopBtn.hide();
                        pullCmsProduct.hide();
                        popupApplyAllSearch.show();
                        popupApplySearch.show();
                    }
                } else {
                    deleteApplyScope.hide();
                    barcodePopBtn.hide();
                    pullCmsProduct.hide();
                    popupApplySearch.hide();
                    popupApplyAllSearch.hide();
                }
                deleteExcept.show();
                popupExceptSearch.show();
                popupExceptApplyAllSearch.show();

                break;
           /* default:
                if (applyScope != COUPON_APPLY_SCOPE_ALL && applyScope != null) {
                    deleteApplyScope.show();
                    if (storeType != STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_IMME)) {
                        //barcodePopBtn.show()
                        //pullCmsProduct.show();
                        popupApplyAllSearch.hide();
                        popupApplySearch.hide();
                    } else if (applyScope == COUPON_APPLY_SCOPE_CATE) {
                        //barcodePopBtn.hide()
                        //pullCmsProduct.hide();
                        popupApplyAllSearch.hide();
                        popupApplySearch.show();
                    } else {
                        //barcodePopBtn.hide()
                        //pullCmsProduct.hide();
                        popupApplyAllSearch.show();
                        popupApplySearch.show();
                    }
                } else {
                    deleteApplyScope.hide();
                    //barcodePopBtn.hide();
                    //pullCmsProduct.hide();
                    popupApplySearch.hide();
                    popupApplyAllSearch.hide();
                }
                deleteExcept.show();
                popupExceptSearch.show();
                popupExceptApplyAllSearch.show();
                break;*/
        }

        /*if ($("#applyScope").val() == COUPON_APPLY_SCOPE_ALL) {
            excelDownloadBtn.hide();
        } else {
            excelDownloadBtn.show();
        }*/
    },
    removeApplyGridData : function() {
        applyGrid.removeCheckData();
        coupon.applyGridSearchClear();
    },

    /**
     * 제외상품 리스트 그리드 내 조회
     */
    exceptGridSearch : function() {
        var gridSearchValue = $("#exceptGridSearchValue").val();

        if ($("#exceptGridSearchType").val() == "itemNo") {
            var replaceText = gridSearchValue.replace(/\s/gi, "");
            $("#exceptGridSearchValue").val(replaceText);

            if (!/^[a-zA-Z0-9,]*$/.test(replaceText)) {
                alert("상품번호 다중 검색 시 번호 사이에 구분자 콤마(',')를 입력해주세요.");
                return;
            }

            var arrSchValueList = replaceText.split(",");

            if (arrSchValueList.length > 1000) {
                alert("상품번호는 최대 1,000개까지 검색 가능합니다.");
                return;
            }

            gridSearchValue = arrSchValueList;
        }

        exceptGrid.filter($("#exceptGridSearchType").val(), gridSearchValue);

        coupon.setExceptGridSearchCnt(exceptGrid.gridView.getItemCount());
    },
    /**
     * 제외 상품 그리드 내 조회 초기화
     */
    exceptGridSearchClear : function() {

        $("#exceptGridSearchValue").val("");

        exceptGrid.clearAllFilter();
        coupon.setExceptGridSearchCnt(exceptGrid.gridView.getItemCount());
    },
    /**
     * 쿠폰바코드 조회 팝업
     */
    setBarcodePop : function() {
        console.log(coupon.barcode);
        if ($.jUtil.isEmpty(coupon.barcode)) {
            coupon.openBarcodePop();
        } else {
            coupon.openBarcodePop(coupon.barcode);
        }
    }
    ,
    openBarcodePop : function(data) {
        var storeType = $("#storeType").val();
        var getBarcode = $("#barcode").val();// coupon.barcode;
        var coyn;

        switch ($("#storeType").val()) {
            case STORE_TYPE_HYPER:
                coyn = 'RY';
                break;
            case STORE_TYPE_CLUB:
                coyn = 'RN';
                break;
            case STORE_TYPE_EXP:
                coyn = 'R';
                break;
        }

        $("#coyn").val(coyn);
        $('.barcodePop input[name="storeType"]').val(storeType);
        $('.barcodePop input[name="getBarcode"]').val(getBarcode);
        $('.barcodePop input[name="barcodeList"]').val(JSON.stringify(data));

        var status = $("#status").val();
        if ($.jUtil.isEmpty(status) || status == "TEMP") {
            $('.barcodePop input[name="setYn"]').val('Y');
        } else {
            $('.barcodePop input[name="setYn"]').val('N');
        }

        var url = "/common/popup/barcodePop";
        var target = "barcodePop";
        windowPopupOpen(url, target, 1084, 470);

        var frm = document.getElementById('barcodeSelectPopForm');
        frm.target = target;

        console.log($('.barcodePop input[name="setYn"]').val());
        frm.submit();
    },
    openCardPop : function(data) {
        var storeType = $("#storeType").val();
        var siteType = getSiteType(storeType);
        var paymentMethodType = $("#paymentType").val();

        $('.cardPop input[name="paymentMethodType"]').val(paymentMethodType);
        $('.cardPop input[name="siteType"]').val(siteType);
        $('.cardPop input[name="paymentList"]').val(JSON.stringify(data));

        if (paymentMethodType == 'CARD') {
            $('.cardPop input[name="multiYn"]').val('Y');
        } else {
            $('.cardPop input[name="multiYn"]').val('N');
        }

        if ($("#chgYn").val() == 'Y') {
            $('.cardPop input[name="setYn"]').val('N');
        } else {
            $('.cardPop input[name="setYn"]').val('Y');
        }

        var url = "/common/popup/cardPop";
        var target = "cardPop";
        windowPopupOpen(url, target, 470, 430);

        var frm = document.getElementById('cardSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    /**
     * 쿠폰 바코드 팝업 call back
     */
    barcodePopupCallback: function(data) {
        //적용목록 삭제상태로 삭제
        if ($("#barcode").val() != data[0].couponCd) {
            var allList = applyGrid.dataProvider.getRows(0, -1);
            allList.forEach(function (_row, row) {
                applyGrid.dataProvider.removeRow(row);
            });
        }

        coupon.barcode = data[0].couponCd;
        $("#barcode").val(data[0].couponCd);
        $("#barcodeCouponSeq").val(data[0].couponSeq);

        /*if ($.jUtil.isNotEmpty(data[0].incUnitCd)) {
            $("#departCd").val(data[0].incUnitCd);
        }*/
        //바코드 팝업을 통해 가져오는 바코드의 경우 귀속 부서 '01'로 고정
        $("#departCd").val("01");

        if ($.jUtil.isNotEmpty(data[0].couponSdate)) {
            $("#issueStartDt").val(data[0].couponSdate + " 06:00");
        }

        if ($.jUtil.isNotEmpty(data[0].couponEdate)) {
            $("#issueEndDt").val(data[0].couponEdate + " 23:59");
        }

        //정액/정률 컨트롤
        var discountType = $("#discountType");
        addSelectOption(discountType);

        removeSelectOption(discountType, COUPON_DISCOUNT_TYPE_PRICE);
        $(discountType).change();
    },
    /**
     * 제외 상품 건수 노출
     * @param cnt
     */
    setExceptGridCnt : function(cnt) {
        $("#exceptCnt").html(cnt + "건)");
    },
    /**
     * 제외상품 그리드의 데이터 검색 건수 노출
     * @param cnt
     */
    setExceptGridSearchCnt : function(cnt) {
        $("#exceptSearchCount").html("(" + cnt + "/");
    },
    removeExceptGridData : function() {
        exceptGrid.removeCheckData();
        coupon.exceptGridSearchClear();
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(data) {
        var storeType = $('#storeType').val();
        var status = $('#status').val();

        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if (status == "" || status == "TEMP" || status == "READY" || status == "ISSUED") {
            $('.storePop input[name="setYn"]').val('Y');
        } else {
            $('.storePop input[name="setYn"]').val('N');
        }

        var url = "/common/popup/storeSelectPop";
        var target = "storeSelectPop";
        windowPopupOpen(url, target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    setStorePop : function() {
        coupon.openStoreInfoPopup($('#storeListJson').val());

        if ($.jUtil.isEmpty(coupon.storeList)) {
            coupon.openStoreInfoPopup();
        } else {
            coupon.openStoreInfoPopup(coupon.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        var resCount = res.storeList.length;

        coupon.storeList = res.storeList;
        $('#storeCnt').html(resCount);
    },
    /**
     * 카드 결제수단 팝업 call back
     */
    cardPopupCallback: function(checkedPaymentList) {
        var resCount = checkedPaymentList.length;
        coupon.paymentList = checkedPaymentList;

        $('#cardCnt').html(resCount);
    },
    setCardPop : function() {
        console.log(coupon.paymentList);
        if ($.jUtil.isEmpty(coupon.paymentList)) {
            coupon.openCardPop();
        } else {
            coupon.openCardPop(coupon.paymentList);
        }
    },
    callUploadScopePopup : function(obj) {
        var popWidth = "600";
        var popHeight = "250";
        var uri = "";
        var callback = "";
        var applyScope = $("#applyScope").val();

        if($.jUtil.isEmpty(applyScope)) {
            alert("적용상품 범위를 선택하세요.");
            return false;
        }

        switch ($(obj).attr("id")) {
            case "popupApplyAllSearch":
                callback = "coupon.applyPopupCallback";
                uri = "/promotion/popup/upload/uploadApplyScopePop?promotionType=COUPON&couponNo=" + $("#couponNo").val() + "&applyScope=" + applyScope + "&callBackScript=" + callback;
                break;
            case "popupExceptApplyAllSearch":
                callback = "coupon.exceptPopupCallback";
                uri = "/promotion/popup/upload/uploadApplyScopePop?promotionType=COUPON&couponNo=" + $("#couponNo").val() + "&applyScope=ITEM&exceptScope=ITEM&callBackScript=" + callback;
                break;
            default:
                break;
        }

        if (uri != "") {
            windowPopupOpen(uri, "uploadScopePop", popWidth, popHeight);
        } else {
            alert("일괄등록을 할 수 없습니다. 관리자에 문의하세요.");
        }
    },
    /**
     * 적용 범위별 공통팝업 호출
     */
    openPopup : function() {
        var applyScope = $("#applyScope").val();
        var storeType = $("#storeType").val();
        var mallType = getMallType(storeType);
        var siteType = getSiteType(storeType);

        var url = "";
        var target = "";
        var callback = "coupon.applyPopupCallback";

        var popWidth = "1084";
        var popHeight = "555";

        var alertMsg = "적용상품범위가 전체입니다.";

        if ($.jUtil.isEmpty(applyScope)) {
            alert("적용상품범위를 선택하세요.");
            return true;
        }

        switch (applyScope) {
            case COUPON_APPLY_SCOPE_ITEM:
                target = "itemPopup";
                url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + siteType + "&storeType=" + storeType;
                popHeight = "650";
                break;
            case COUPON_APPLY_SCOPE_SELL:
                target = "partnerPop";
                url = "/common/popup/partnerPop?callback=" + callback + "&isMulti=Y&partnerType=SELL";
                popWidth = "1080";
                popHeight = "530";
                break;
            case COUPON_APPLY_SCOPE_CATE:
                target = "catePopup";
                url = "/common/popup/treeCategoryPop?callback=" + callback;
                popWidth = "580";
                popHeight = "650";
                break;
            default:
                break;
        }

        if (url != "") {
            windowPopupOpen(url, target, popWidth, popHeight);
        } else {
            alert(alertMsg);
        }
    },
    /**
     * 공통팝업 콜백 - 선택한 데이터 적용 대상 그리드에 추가
     * @param resDataArr
     */
    applyPopupCallback : function(callbackDataArray) {
        var applyDataArray = coupon.setApplyObject(callbackDataArray);

        applyGrid.appendData(applyDataArray);
        coupon.applyGridSearchClear();
        coupon.setApplyGridSearchCnt(applyGrid.gridView.getItemCount());
    },
    /**
     * 제외상품 선택을 위한 상품팝업 호출
     */
    exceptItemPopup : function() {
        var storeType = $("#storeType").val();
        var mallType = getMallType(storeType);
        var siteType = getSiteType(storeType);

        var callback = "coupon.exceptPopupCallback";
        var target = "itemPopup";
        var url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + siteType + "&storeType=" + storeType;

        var popWidth = "1084";
        var popHeight = "650";

        windowPopupOpen(url, target, popWidth, popHeight);
    },
    /**
     * 공통팝업 콜백 - 선택한 데이터 제외 상품 그리드에 추가
     * @param resDataArr
     */
    exceptPopupCallback : function(resDataArr) {
        var exceptDataArray = coupon.setExceptObject(resDataArr);

        exceptGrid.appendData(exceptDataArray);
        coupon.exceptGridSearchClear();
        coupon.setExceptGridSearchCnt(exceptGrid.gridView.getItemCount());
    },
    /**
     * 각 적용범위 별 선택 데이터 적용 대상 그리드에 추가할 수 있도록 설정 변경
     * @param resDataArr
     * @returns {any[]}
     */
    setApplyObject : function(resDataArr) {
        var applyDataArray = new Array();
        var applyScope = $("#applyScope").val();

        if (!Array.isArray(resDataArr)) {
            if (applyScope == COUPON_APPLY_SCOPE_SELL) {
                var applyDataObj = new Object();
                applyDataObj.applyCd = resDataArr.partnerId;
                applyDataObj.applyNm = resDataArr.partnerNm;
                applyDataObj.applyNmDisp = resDataArr.partnerNm;

                applyDataArray.push(applyDataObj);
            }
        } else {
            for (var i in resDataArr) {
                var applyDataObj = new Object();

                switch (applyScope) {
                    case COUPON_APPLY_SCOPE_ITEM:
                        applyDataObj.applyCd = resDataArr[i].itemNo;
                        applyDataObj.applyNm = resDataArr[i].itemNm;
                        applyDataObj.applyNmDisp = resDataArr[i].itemNm;
                        break;
                    case COUPON_APPLY_SCOPE_SELL:
                        applyDataObj.applyCd = resDataArr[i].partnerId;
                        applyDataObj.applyNm = resDataArr[i].partnerNm;
                        applyDataObj.applyNmDisp = resDataArr[i].partnerNm;
                        break;
                    case COUPON_APPLY_SCOPE_CATE:
                        applyDataObj.applyCd = resDataArr[i].categoryId;

                        var categoryNm = resDataArr[i].categoryNm;
                        if (resDataArr[i].categorySNm != "") categoryNm = resDataArr[i].categorySNm + " > " + categoryNm;
                        if (resDataArr[i].categoryMNm != "") categoryNm = resDataArr[i].categoryMNm + " > " + categoryNm;
                        if (resDataArr[i].categoryLNm != "") categoryNm = resDataArr[i].categoryLNm + " > " + categoryNm;

                        applyDataObj.applyNm = resDataArr[i].categoryNm;
                        applyDataObj.applyNmDisp = categoryNm;
                        break;
                    default:
                        break;
                }

                applyDataArray.push(applyDataObj);
            }
        }

        return applyDataArray;
    },
    /**
     * 제외 상품 그리드에 추가할 수 있도록 설정 변경
     * @param resDataArr
     * @returns {any[]}
     */
    setExceptObject : function(resDataArr) {
        var exceptDataArray = new Array();

        for (var i in resDataArr) {
            var exceptDataObj = new Object();

            exceptDataObj.itemNo = resDataArr[i].itemNo;
            exceptDataObj.itemNm = resDataArr[i].itemNm;

            exceptDataArray.push(exceptDataObj);
        }

        return exceptDataArray;
    },
    /**
     * 버튼 컨트롤
     * @param status
     * @param withdrawYn
     */
    setBtnControl : function(status, withdrawYn) {
        var delBtn = $("#delBtn");
        var setCouponBtn = $("#setCouponBtn");
        var withdrawBtn = $("#withdrawBtn");
        var issueBtn = $("#issueBtn");
        var stopBtn = $("#stopBtn");
        var applySearch = $("#popupApplySearch");
        var applyAllSearch = $("#popupApplyAllSearch");

        var barcodePopBtn = $("#barcodePopBtn");
        var pullCmsProduct = $("#pullCmsProduct");

        var storeType = $("#storeType").val();
        var couponType = $("#couponType").val();

        barcodePopBtn.hide()
        pullCmsProduct.hide();
        switch (status) {
            case "TEMP":
                delBtn.hide();          //쿠폰 삭제 버튼 노출 안함.
                //delBtn.show();          //쿠폰 삭제
                setCouponBtn.show();    //쿠폰 등록
                withdrawBtn.hide();     //쿠폰 회수
                issueBtn.show();        //쿠폰 발급
                stopBtn.hide();         //쿠폰 중단

                if (storeType != STORE_TYPE_DS && (couponType == COUPON_TYPE_ITEM || couponType == COUPON_TYPE_IMME)) {
                    barcodePopBtn.show();
                    pullCmsProduct.show();
                }
                applySearch.show();     //적용대상 등록
                applyAllSearch.show();  //적용대상 일괄등록
                break;
            case "READY":
                delBtn.hide();          //쿠폰 삭제
                setCouponBtn.show();    //쿠폰 등록 (복사)
                withdrawBtn.hide();     //쿠폰 회수
                issueBtn.hide();        //쿠폰 발급
                stopBtn.show();         //쿠폰 중단

                applySearch.show();     //적용대상 등록
                applyAllSearch.show();  //적용대상 일괄등록
                break;
            case "ISSUED":
                delBtn.hide();          //쿠폰 삭제
                setCouponBtn.show();    //쿠폰 등록
                withdrawBtn.hide();     //쿠폰 회수
                issueBtn.hide();        //쿠폰 발급
                stopBtn.show();         //쿠폰 중단

                applySearch.hide();     //적용대상 등록
                applyAllSearch.hide();  //적용대상 일괄등록
                break;
            case "STOP":
                delBtn.hide();     //쿠폰 삭제
                setCouponBtn.show();     //쿠폰 등록
                (withdrawYn == "Y" ? withdrawBtn.hide() : withdrawBtn.show());//쿠폰 회수
                issueBtn.hide();   //쿠폰 발급
                stopBtn.hide();    //쿠폰 중단

                applySearch.hide();     //적용대상 등록
                applyAllSearch.hide();  //적용대상 일괄등록
                break;
            case "COMP":
                delBtn.hide();     //쿠폰 삭제
                setCouponBtn.show();     //쿠폰 등록
                withdrawBtn.hide();//쿠폰 회수
                issueBtn.hide();   //쿠폰 발급
                stopBtn.hide();    //쿠폰 중단

                applySearch.hide();     //적용대상 등록
                applyAllSearch.hide();  //적용대상 일괄등록
                break;
            default:
                delBtn.hide();          //쿠폰 삭제
                setCouponBtn.show();    //쿠폰 등록
                withdrawBtn.hide();     //쿠폰 회수
                issueBtn.hide();        //쿠폰 발급
                stopBtn.hide();         //쿠폰 중단

                applySearch.hide();     //적용대상 등록
                applyAllSearch.hide();  //적용대상 일괄등록
                break;
        }
        this.setScopeBtnControl(status);
    },
    setCallback : function(res) {
        // 초기화
        // coupon.init();

        // 조회
        couponSearch.searchCouponList(res);
    },
    /**
     * 결제수단 조회
     * @param siteType
     */
    getMethodList : function(couponType, storeType) {
        var siteType = getSiteType(storeType);

        $("#methodCd").html('<option value="">선택안함</option>');
        if (!(couponType == COUPON_TYPE_CART || couponType == COUPON_TYPE_DUPL)) {
            return;
        }

        CommonAjax.basic({
            url:"/coupon/getMethodList.json?siteType="+siteType,
            method : "GET",
            callbackFunc : function(data) {
                if (data != null) {
                    $.each(data, function (i, methodData) {
                        $("#methodCd").append("<option value='" + methodData.methodCd + "'>" + methodData.methodNm + "</option>");
                    });
                }
            }
        });
    }
    ,/**
     * CMS 쿠폰 리스트 조회
     */
    getCmsCouponProdList : function() {
        var barcodeCouponSeq = $("#barcodeCouponSeq").val();

        if ($.jUtil.isEmpty(barcodeCouponSeq)) {
            alert("쿠폰바코드를 확인하세요.");
            return;
        }

        var coyn;

        switch ($("#storeType").val()) {
            case STORE_TYPE_HYPER:
                coyn = 'RY';
                break;
            case STORE_TYPE_CLUB:
                coyn = 'RN';
                break;
            case STORE_TYPE_EXP:
                coyn = 'R';
                break;
        }

        CommonAjax.basic({
            url:"/coupon/getCmsCouponProdList.json?couponSeq=" + barcodeCouponSeq + "&coyn=" + coyn,
            method : "GET",
            callbackFunc : function(data) {
                //할인방법
                var discountType = $("#discountType");

                if ($.jUtil.isNotEmpty(data)) {
                    applyGrid.appendData(data);
                    //정액 정률 세팅

                    var itemType = data[0].itemType;
                    var couponValue = data[0].couponValue;

                    addSelectOption(discountType);
                    $(discountType).val(itemType).change();
                    removeSelectExceptOption(discountType, itemType);

                    if (itemType == COUPON_DISCOUNT_TYPE_RATE) {
                        $("#discountRate").val(couponValue);
                    } else {
                        $("#discountPrice").val(couponValue);
                    }
                } else  {
                    //addSelectOption(discountType);
                    //alert("데이터가 없습니다.");
                }
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            }
        });

    },
    /**
     * 쿠폰 기본정보 복사
     **/
    setCopyCoupon : function() {
        var couponNo = $("#couponNo").val();

        if ($.jUtil.isEmpty(couponNo)) {
            alert("복사할 대상이 없습니다.");
            return;
        }

        if (!confirm("쿠폰 정보를 복사하시겠습니까?")) {
            return;
        }

        coupon.couponFormReset();

        CommonAjax.basic({url: "/coupon/getCouponDetail.json", data: {"couponNo": couponNo, "copyYn": "Y"}, method: "GET", successMsg: null, callbackFunc : function(res) {
            //복사 시 적용기간 초기화
            res.couponNo = "";
            res.status = "";
            res.statusNm = "";
            res.validStartDt = "";
            res.validEndDt = "";
            res.randomNo = "";

            if (res.storeType != STORE_TYPE_DS && (res.couponType == COUPON_TYPE_ITEM || res.couponType == COUPON_TYPE_IMME)) {
                res.barcode = "";
            }

            coupon.copyYn = "Y";
            coupon.detailCallback(res);
            //coupon.setScopeCount("0");
        }});
    },
    /**
     * 쿠폰 조회 그리드 선택 시 - 해당 쿠폰 상세 정보 조회
     * @param couponNo
     */
    getCouponDetail : function(couponNo) {
        // 쿠폰 상세 초기화
        coupon.couponFormReset();

        CommonAjax.basic({
            url:"/coupon/getCouponDetail.json?couponNo="+couponNo+"&copyYn=N"
            , method : "GET"
            , callbackFunc : function(couponData) {
                coupon.copyYn = "N";
                coupon.detailCallback(couponData);
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        // api 에러메세지
                        alert(resError.responseJSON.returnMessage);
                    }
                }

                couponSearch.searchCouponList();
            }
        });
    },
    /**
     * 쿠폰 상세 정보 조회 콜백 - 데이터 각 항목에 적용
     * @param couponData
     */
    detailCallback : function(couponData) {
        // 원본 데이터 값 설정
        coupon.detailOriginalData = couponData;

        if (couponData.copyYn == 'Y') {
            $("#couponNo").val("");
        } else {
            $("#couponNo").val(couponData.couponNo);
        }

        $("#couponType").val(couponData.couponType).change();
        $("#storeType").val(couponData.storeType).change();
        $("#status").val(couponData.status);
        $("#statusNm").html(couponData.statusNm);
        $("#manageCouponNm").val(couponData.manageCouponNm).setTextLength("#manageCouponNmLen");
        $("#offlineCouponNo").html(couponData.offlineCouponNo);
        $("#displayCouponNm").val(couponData.displayCouponNm).setTextLength("#displayCouponNmLen");
        $("#departCd").val(couponData.departCd);

        // 노출영역
        $("#displayItYn").prop("checked", couponData.displayItYn == "Y");
        $("#displayCzYn").prop("checked", couponData.displayCzYn == "Y");

        $("#itemDuplYn").val(couponData.itemDuplYn).change();

        // 바코드
        if (couponData.couponType == COUPON_TYPE_CART || couponData.couponType == COUPON_TYPE_DUPL) {
            $("#cartBarcode").val(couponData.barcode);
        } else {
            $("#barcode").val(couponData.barcode);
        }
        coupon.barcocde = couponData.barcode;

        // 바코드 쿠폰 시퀀스
        $("#barcodeCouponSeq").val(couponData.barcodeCouponSeq);

        // 할인방법
        $("#discountType").val(couponData.discountType).change();
        $("#discountPrice").val(couponData.discountPrice);
        $("#discountRate").val(couponData.discountRate);
        $("#discountMax").val(couponData.discountMax);
        $("#purchaseMin").val(couponData.purchaseMin);

        // 분담설정
        $("#shareYn").val(couponData.shareYn).change();

        var isShareHome = couponData.shareHome > 0;
        var isShareSeller = couponData.shareSeller > 0;
        var isShareCard = couponData.shareCard > 0;

        $("#shareHomeYn").prop("checked", isShareHome);
        $("#shareHome").val(couponData.shareHome).prop("readonly", !isShareHome);

        $("#shareSellerYn").prop("checked", isShareSeller);
        $("#shareSeller").val(couponData.shareSeller).prop("readonly", !isShareSeller);

        $("#shareCardYn").prop("checked", isShareCard);
        $("#shareCard").val(couponData.shareCard).prop("readonly", !isShareCard);

        // 적용시스템
        $("#applyPcYn").prop("checked", couponData.applyPcYn == "Y");
        $("#applyAppYn").prop("checked", couponData.applyAppYn == "Y");
        $("#applyMwebYn").prop("checked", couponData.applyMwebYn == "Y");

        //등급쿠폰
        $("#gradeCouponYn").val(couponData.gradeCouponYn).change();
        $("#gradeCouponMonth").val(couponData.gradeCouponMonth);
        $("#userGradeSeq").val(couponData.userGradeSeq);

        //난수번호
        $("#randomNoIssueYn").val(couponData.randomNoIssueYn).change();
        //일회성 난수 쿠폰 엑셀다운로드 가능 여부
        $("#randomListDownAvailYn").val(couponData.randomListDownAvailYn);

        if (couponData.randomNoIssueYn == "Y") {
            $("#randomNoType").val(couponData.randomNoType).change();

            if (couponData.randomNoType == 'MULTI') {
                $("#randomNo").val(couponData.randomNo);
            } else {
                $("#randomListDownAvailYn").val(couponData.randomListDownAvailYn).change();
            }
        }

        //구매목적
        $("#purpose").val(couponData.purpose).change();

        //구매기준
        $("#purchaseStdDt").val(couponData.purchaseStdDt);

        //적용기간 타입
        $("#validType").val(couponData.validType).change();

        if (coupon.copyYn == 'Y') {
            // 발급기간 초기화
            couponData.issueStartDt = moment(new Date).format("YYYY-MM-DD 00:00");
            if ($.jUtil.diffDate(couponData.issueStartDt, couponData.issueEndDt, "D") > 0) {
                $("#issueStartDt").val(couponData.issueStartDt);
                $("#issueEndDt").val(couponData.issueEndDt);
            }

            if (couponData.couponType != COUPON_TYPE_IMME && couponData.validType == "PERIOD") {
                calendarInit.validPeriod("1w");
            }

            if (couponData.gradeCouponYn == 'Y' && $.jUtil.isNotEmpty(couponData.gradeCouponMonth)) {
                monthConfig.onSelect(couponData.gradeCouponMonth);
            }
        } else {
            $("#issueStartDt").val(couponData.issueStartDt);
            $("#issueEndDt").val(couponData.issueEndDt);

            $("#validStartDt").val(couponData.validStartDt);
            $("#validEndDt").val(couponData.validEndDt);
        }

        $("#issueLimitType").val(couponData.issueLimitType).change();
        $("#issueMaxCnt").val(couponData.issueMaxCnt);
        $("#limitPerCnt").val(couponData.limitPerCnt);
        $("#limitPerDayYn").val(couponData.limitPerDayYn).change();
        $("#limitPerDayCnt").val(couponData.limitPerDayCnt);
        $("#issueTimeYn").val(couponData.issueTimeYn).change();
        $("#issueStartTime").val(couponData.issueStartTime);
        $("#issueEndTime").val(couponData.issueEndTime);
        $("#validDay").val(couponData.validDay);

        // 배송Slot시간
        $("#shipSlotYn").val(couponData.shipSlotYn).change();

        if (couponData.shipSlotYn == "Y") {
            $("#slotStartTime").val(couponData.slotStartTime);
            $("#slotEndTime").val(couponData.slotEndTime);
        }

        // 적용요일
        $("#validMon").prop("checked", couponData.validMon == "Y");
        $("#validTue").prop("checked", couponData.validTue == "Y");
        $("#validWed").prop("checked", couponData.validWed == "Y");
        $("#validThu").prop("checked", couponData.validThu == "Y");
        $("#validFri").prop("checked", couponData.validFri == "Y");
        $("#validSat").prop("checked", couponData.validSat == "Y");
        $("#validSun").prop("checked", couponData.validSun == "Y");

        // 합주문 가능여부
        $("#combineOrderAvailYn").val(couponData.combineOrderAvailYn);

        // 픽업 가능여부
        $("#pickupAvailYn").val(couponData.pickupAvailYn);

        //쿠폰 점포 리스트
        coupon.storeList = couponData.storeList;

        if (!$.jUtil.isEmpty(couponData.storeList)) {
            $("#storeCnt").html(couponData.storeList.length);
        }

        //결제 수단 리스트
        $("#paymentType").val(couponData.paymentType).change();
        coupon.paymentList = couponData.paymentList;

        if (!$.jUtil.isEmpty(couponData.paymentList)) {
            $("#cardCnt").html(couponData.paymentList.length);
        }

        // 적용 정보
        $("#applyScope").val(couponData.applyScope).change();

        // 적용 대상 리스트
        applyGrid.setData(couponData.applyList);

        // 적용대상 건수
        coupon.setApplyGridCnt(applyGrid.dataProvider.getRowCount());
        coupon.applyGridSearchClear();

        // 제외상품 그리드 display
        exceptGrid.setDisplay();

        // 제외 상품 대상 리스트, 건 수
        exceptGrid.setData(couponData.exceptItemList);
        coupon.setExceptGridCnt(exceptGrid.dataProvider.getRowCount());
        coupon.exceptGridSearchClear();

        if (coupon.copyYn == "N") {
            if (couponData.status == "ISSUED" || couponData.status == "COMP" || couponData.status == "STOP") {
                coupon.formElementControl(true);
            } else if (couponData.status == "READY") {
                removeAllSelectOption($("#couponType"));
                removeAllSelectOption($("#storeType"));
                removeAllSelectOption($("#gradeCouponYn"));
                removeAllSelectOption($("#randomNoIssueYn"));
                removeAllSelectOption($("#randomNoType"));
                removeAllSelectOption($("#applyScope"));
            }
            coupon.setBtnControl(couponData.status, couponData.withdrawYn);
        } else {
            coupon.setBtnControl("", "");
        }
    },
    formElementControl : function(flag) {
        var onclickStr = "";
        if (flag) onclickStr = "return false;";

        $("#couponForm").find("select").each(function() {
            var formElementId = $(this).attr("id");
            if (formElementId == "departCd") {
                $("#" + formElementId).prop("disabled", true);
            } else {
                if (flag) removeAllSelectOption($(this));
                else addSelectOption($(this));
            }
        });

        $("#couponForm").find("input:text").each(function() {
            var formElementId = $(this).attr("id");

            if (formElementId != "couponNo") {
                if (flag) {
                    $("#" + formElementId).attr("readonly", ($(this).attr("isUpdate") == "true" ? false : true));
                } else {
                    $("#" + formElementId).attr("readonly", flag);
                }
            } else {
                $(this).attr("readonly", true);
            }
        });

        $("#couponForm").find("input:checkbox").each(function() {
            var formElementId = $(this).attr("id");
            if (formElementId != undefined) {
                if($("#" + formElementId).attr("isUpdate") != "true") {
                    $("#" + formElementId).attr("onClick", onclickStr);
                }
            }
        });

        $(".dateControl .ui-datepicker-trigger").attr("disabled", flag);
        if ($("#gradeCouponYn").val() == 'Y') {
            $(".ui-monthpicker-trigger").attr("disabled", flag);
        }
    },
    setImmeValidPeriod : function () {
        $("#validStartDt").val($("#issueStartDt").val());
        $("#validEndDt").val($("#issueEndDt").val());
    },
    /**
     * 쿠폰 정보 저장
     */
    setCouponData : function (obj) {
        var couponNo = $("#couponNo").val();
        var status = $("#status").val();
        var confirmMsg = "쿠폰을 저장하시겠습니까?";
        var successMsg = "임시저장 되었습니다.";

        if ($.jUtil.isNotEmpty(couponNo) && couponNo > 0) {
            confirmMsg = "쿠폰을 수정하시겠습니까?";
            successMsg = "수정 되었습니다.";

            if (status == "STOP" || status == "COMP") {
                alert("발급중단/발급종료 상태일때는 수정 할 수 없습니다.");
                return false;
            }
        } else {
            $("#status").val("TEMP");
        }

        if ($("#couponType").val() == COUPON_TYPE_IMME ) {
            coupon.setImmeValidPeriod();
        }

        status = $("#status").val();

        if (status == "TEMP") {
            if (!validCheck.setTempStatus()) {
                return false;
            }
        } else if (status == "READY") {
            if (!validCheck.set("setCouponBtn")) {
                return false;
            }
        }

        var applyPcYn = $("#applyPcYn");
        var applyAppYn = $("#applyAppYn");
        var applyMwebYn = $("#applyMwebYn");
        (applyPcYn.is(":checked") ? applyPcYn.val("Y") : applyPcYn.val("N"));
        (applyAppYn.is(":checked") ? applyAppYn.val("Y") : applyAppYn.val("N"));
        (applyMwebYn.is(":checked") ? applyMwebYn.val("Y") : applyMwebYn.val("N"));

        var displayItYn = $("#displayItYn");
        var displayCzYn = $("#displayCzYn");
        (displayItYn.is(":checked") ? displayItYn.val("Y") : displayItYn.val("N"));
        (displayCzYn.is(":checked") ? displayCzYn.val("Y") : displayCzYn.val("N"));

        // 쿠폰 정보
        var couponForm = $("#couponForm").serializeArray();

        // RequestBody용 parameterObject
        var paramObject = {
            createApplyList : [],
            updateApplyList : [],
            deleteApplyList : []
        };

        $.map(couponForm, function(value, index) {
            paramObject[value.name] = value.value;
        });

        paramObject.discountType  = $("#discountType").val();
        paramObject.departCd = $("#departCd").val();
        paramObject.storeList = coupon.storeList;
        paramObject.paymentList = coupon.paymentList;
        paramObject.applyScope = $("#applyScope").val();

        // 적용대상 셋팅
        var createdDataList = applyGrid.dataProvider.getStateRows("created");
        var updatedDataList = applyGrid.dataProvider.getStateRows("none");
        var deletedDataList = applyGrid.dataProvider.getStateRows("deleted");

        paramObject.createApplyList = getGridStateData(applyGrid, createdDataList);
        paramObject.updateApplyList = getGridStateData(applyGrid, updatedDataList);
        paramObject.deleteApplyList = getGridStateData(applyGrid, deletedDataList);

        // 제외상품 정보
        var createdExceptDataList = exceptGrid.dataProvider.getStateRows("created");
        var deletedExceptDataList = exceptGrid.dataProvider.getStateRows("deleted");
        paramObject.createExceptItemList = getExceptGridStateData(exceptGrid, createdExceptDataList);
        paramObject.deleteExceptItemList = getExceptGridStateData(exceptGrid, deletedExceptDataList);

        // 적용범위 변경에 따른 적용대상/제외상품 정보 삭제
        if ($.jUtil.isNotEmpty(coupon.detailOriginalData) && coupon.detailOriginalData.applyScope != paramObject.applyScope) {
            //적용대상
            if ($.jUtil.isNotEmpty(coupon.detailOriginalData.applyList)) {
                coupon.detailOriginalData.applyList.forEach(function (value, index) {
                    paramObject.deleteApplyList.push(value.applyCd);
                });
            }
            //제외대상상품
            if ($.jUtil.isNotEmpty(coupon.detailOriginalData.exceptItemList)) {
                coupon.detailOriginalData.exceptItemList.forEach(function (value, index) {
                    paramObject.deleteExceptItemList.push(value.itemNo);
                });
            }
        }

        // 적용 점포 리스트
        paramObject.createStoreList = createStoreList;
        paramObject.deleteStoreList = deleteStoreList;

        $.ajax({
            url: "/coupon/setCoupon.json"
            , data        : JSON.stringify(paramObject)
            , method      : "POST"
            , dataType    : "json"
            , contentType : "application/json; charset=utf-8"
            , success     : function(couponNo) {
                alert(successMsg);
                couponSearch.searchCouponList(couponNo);
            }
            , error : function(resError) {
                if (resError.status == 401) {
                    alert(CommonErrorMsg.sessionErrorMsg);
                    windowPopupOpen("/login/popup", "loginPop", 400, 481);
                } else {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.returnMessage !== undefined) {
                            alert(resError.responseJSON.returnMessage);
                        } else {
                            alert(CommonErrorMsg.dfErrorMsg);
                        }

                        //이미 발급이 시작된 쿠폰으로 쿠폰정보 재조회합니다.
                        if (resError.responseJSON.returnCode == '8056') {
                            var rowIndex = couponListGrid.dataProvider.searchDataRow({fields: ["couponNo"], values: [couponNo]});

                            if (rowIndex > -1) {
                                couponListGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                                couponListGrid.gridView.onDataCellClicked();
                            }
                        }
                    }
                }
            }
            , fail : function() {
                alert(CommonErrorMsg.failMsg);
            }
        });

    },
    setCouponStatus : function(btnId) {
        if ($("#couponType").val() == COUPON_TYPE_IMME) {
            coupon.setImmeValidPeriod();
        }

        if (btnId != "delBtn") {
            if (!validCheck.set(btnId)) return;
        }

        var alertMsg = "";
        var confirmMsg = "";
        var successMsg = "";
        var timeoutMsg = "저장 데이터가 많아 시간이 소요됩니다.\n작업 중이던 데이터는 5분 뒤 정상 저장되었는지 반드시 확인해 주시기 바랍니다.";
        var compCouponStatus = "";
        var couponStatus = "";
        var withdrawYn = "N";
        var useYn = "Y";
        var discountType = $("#discountType").val();

        switch (btnId) {
            case "delBtn":
                alertMsg = "임시저장 상태일때만 삭제 가능 합니다.";
                confirmMsg = "쿠폰을 삭제하시겠습니까?";
                successMsg = "쿠폰이 삭제되었습니다.";
                couponStatus = "TEMP";
                compCouponStatus = "TEMP";
                useYn = "N";
                break;
            case "withdrawBtn":
                alertMsg = "중단 상태일때만 회수가 가능 합니다.";
                confirmMsg = "쿠폰을 회수 하시겠습니까?\n회수시 고객에게 발급된 쿠폰이 회수 처리 됩니다.\n다만 이미 사용한 고객에게는 영향을 미치지 않으며\n다시 상태를 변경할 수 없습니다.";
                successMsg = "쿠폰이 회수되었습니다.";
                couponStatus = "STOP";
                compCouponStatus = "STOP";
                withdrawYn = "Y";
                break;
            case "issueBtn":
                alertMsg = "임시저장 상태일때만 발급이 가능 합니다.";

                if (discountType == COUPON_DISCOUNT_TYPE_PRICE) {
                    confirmMsg = "할인액이 " + $.jUtil.comma($("#discountPrice").val()) + "원으로 설정되었습니다. \n현재 정보로 쿠폰을 발급하시겠습니까?";
                } else if (discountType == COUPON_DISCOUNT_TYPE_RATE) {
                    confirmMsg = "현재 정보로 쿠폰을 발급하시겠습니까?";
                } else {
                    alert("할인방법이 선택되지 않았습니다. 할인방법을 확인해 주세요");
                    return;
                }

                successMsg = "쿠폰이 발급되었습니다.";
                couponStatus = "ISSUED";
                compCouponStatus = "TEMP";
                break;
            case "stopBtn":
                alertMsg = "발급대기, 발급 상태일때만 중단이 가능 합니다.";
                confirmMsg = "쿠폰 발급을 중단 하시겠습니까?\n쿠폰 발급 중단시 재발급이 불가능합니다.";
                successMsg = "쿠폰이 발급중단 되었습니다.";
                couponStatus = "STOP";
                break;
            default:
                break;
        }

        var couponNo = $("#couponNo").val();
        var status = $("#status").val();
        var applyScope = $("#applyScope").val();

        if ($.jUtil.isEmpty(couponNo)) {
            alert("쿠폰을 선택하세요.");
            return true;
        }

        if (btnId == "stopBtn") {
            if (status != "READY" && status != "ISSUED") {
                alert(alertMsg);
                return true;
            } else {
                compCouponStatus = status;
            }
        }

        if (couponStatus == "") return true;

        if (status != compCouponStatus) {
            alert(alertMsg);
            return true;
        }

        // 적용범위가 전체가 아닐시 적용대상 존재 여부 체크
        if (status != "TEMP" && $.jUtil.isNotEmpty(applyScope) && applyScope != COUPON_APPLY_SCOPE_ALL && btnId !="delBtn") {
            if (applyGrid.gridView.getItemCount() == 0) {
                alert("적용대상을 등록해 주세요.");
                return false;
            }
        }

        if (!confirm(confirmMsg)) return false;

        var applyPcYn = $("#applyPcYn");
        var applyAppYn = $("#applyAppYn");
        var applyMwebYn = $("#applyMwebYn");
        (applyPcYn.is(":checked") ? applyPcYn.val("Y") : applyPcYn.val("N"));
        (applyAppYn.is(":checked") ? applyAppYn.val("Y") : applyAppYn.val("N"));
        (applyMwebYn.is(":checked") ? applyMwebYn.val("Y") : applyMwebYn.val("N"));

        var displayItYn = $("#displayItYn");
        var displayCzYn = $("#displayCzYn");
        (displayItYn.is(":checked") ? displayItYn.val("Y") : displayItYn.val("N"));
        (displayCzYn.is(":checked") ? displayCzYn.val("Y") : displayCzYn.val("N"));

        $("#status").val(couponStatus);

        var couponForm = $("form[name=couponForm]").serializeArray();
        var paramObject = {
            createApplyList : [],
            updateApplyList : [],
            deleteApplyList : []
        };

        $.map(couponForm, function(value, index) {
            if (value.name == "discountPrice") {
                value.value = $("#discountPrice").val().replace(/,/g, '');
            }
            if (value.name == "discountMax") {
                value.value = $("#discountMax").val().replace(/,/g, '');
            }
            if (value.name == "purchaseMin") {
                value.value = $("#purchaseMin").val().replace(/,/g, '');
            }

            paramObject[value.name] = value.value;
        });

        paramObject.withdrawYn = withdrawYn;
        paramObject.useYn = useYn;
        paramObject.discountType = discountType;
        paramObject.departCd = $("#departCd").val();
        paramObject.storeList = coupon.storeList;
        paramObject.paymentList = coupon.paymentList;

        var createdDataList = applyGrid.dataProvider.getStateRows("created");
        var updatedDataList = applyGrid.dataProvider.getStateRows("none");
        var deletedDataList = applyGrid.dataProvider.getStateRows("deleted");

        paramObject.createApplyList = getGridStateData(applyGrid, createdDataList);
        paramObject.updateApplyList = getGridStateData(applyGrid, updatedDataList);
        paramObject.deleteApplyList = getGridStateData(applyGrid, deletedDataList);

        // 적용범위 변경에 따른 적용대상 정보 삭제
        if (coupon.detailOriginalData != null && coupon.detailOriginalData.applyList != null && coupon.detailOriginalData.applyScope != paramObject.applyScope) {
            coupon.detailOriginalData.applyList.forEach(function(value, index) {
                paramObject.deleteApplyList.push(value.applyCd);
            });
        }

        // 제외상품 정보
        var createdExceptDataList = exceptGrid.dataProvider.getStateRows("created");
        var deletedExceptDataList = exceptGrid.dataProvider.getStateRows("deleted");

        paramObject.createExceptItemList = getExceptGridStateData(exceptGrid, createdExceptDataList);
        paramObject.deleteExceptItemList = getExceptGridStateData(exceptGrid, deletedExceptDataList);

        $.ajax({
            url         : '/coupon/setCouponStatus.json',
            data        : JSON.stringify(paramObject),
            method      : 'POST',
            dataType    : 'json',
            contentType : 'application/json; charset=utf-8',
            success     : function(res) {
                alert(successMsg);
                coupon.setCallback(res);
            },
            error : function(resError) {
                if (resError.status == 401) {
                    alert(CommonErrorMsg.sessionErrorMsg);
                    windowPopupOpen("/login/popup", "loginPop", 400, 481);
                } else if (resError.status == 408) {
                    alert(timeoutMsg);
                    location.reload();
                } else {
                    $("#status").val(compCouponStatus);
                    if (resError.responseJSON != null) {
                        //에러시 원래 상태로 변경
                        if (resError.responseJSON.returnMessage !== undefined) {
                            // api 에러메세지
                            alert(resError.responseJSON.returnMessage);
                        } else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    }
                    return false;
                }
            },
            fail : function() {
                $("#status").val(compCouponStatus);
                alert(CommonErrorMsg.failMsg);
            }
        });
    },
};

function addSelectOption(obj) {
    var couponType = $("#couponType").val();
    var jsonData = "";
    var defaultOption = "";
    var appendOption = "";
    var objId = $(obj).attr("id");
    switch (objId) {
        case "storeType":
            jsonData = storeTypeJson;
            break;
        case "purpose":
            jsonData = couponPurposeJson;
            break;
        case "couponType":
            jsonData = couponTypeJson;
            break;
        case "discountType":
            jsonData = couponDiscountTypeJson;
            break;
        case "shareYn":
            jsonData = shareYnJson;
            break;
        case "issueLimitType":
            jsonData = issueLimitTypeJson;
            break;
        case "validType":
            jsonData = couponValidTypeJson;
            break;
        case "applyScope":
            jsonData = couponApplyScopeJson;
            defaultOption = "<option value=''>선택하세요</option>";
            break;
        case "itemDuplYn":
            jsonData = couponDuplYnJson;
            break;
        case "departCd":
            jsonData = couponCmsUnitJson;
            break;
        case "limitPerDayYn":
            jsonData = useYnJson;
            break;
        case "issueTimeYn":
            jsonData = useYnJson;
            break;
        case "shipSlotYn":
            jsonData = useYnJson;
            break;
        case "randomNoIssueYn":
            jsonData = useYnJson;
            break;
        case "combineOrderAvailYn":
            jsonData = availYnJson;
            break;
        case "pickupAvailYn":
            jsonData = availYnJson;
            break;
        case "cartBarcode":
            jsonData = couponCartBarcodeJson;
            break;
        case "randomNoType":
            jsonData = couponRandomTypeJson;
            break;
        case "paymentType":
            jsonData = couponPaymentTypeJson;
            defaultOption = "<option value=''>선택안함</option>";
            break;
        case "methodCd":
            jsonData = couponMethodListJson;
            defaultOption = "<option value=''>선택안함</option>";
            break;
        case "gradeCouponYn":
            jsonData = useYnJson;
            break;
        case "userGradeSeq":
            jsonData = userGradeSeqJson;
            defaultOption = "<option value=''>선택하세요</option>";
            break;
    }

    if (jsonData == "") return false;

    $(obj).find("option").remove();

    if (defaultOption != "") $(obj).prepend(defaultOption);

    $.each(jsonData, function(key, code) {
        var chkOption = 0;
        var codeCd = code.mcCd;
        var codeNm = code.mcNm;
        var codeRef1 = code.ref1;
        var codeRef2 = code.ref2;
        var codeRef3 = code.ref3;

        if (objId == "methodCd") {
            codeCd = code.methodCd;
            codeNm = code.methodNm;
        }

        $(obj).find("option").each(function() {
            if ($(this).val() == codeCd) {
                chkOption++;
            }
        });

        if (chkOption == 0) {
            if (objId == "cartBarcode") {
                if (codeRef2 == couponType) {
                    $(obj).append("<option value=" + codeCd + ' data-dept=' + codeRef1 + ">" + codeNm + "</option>");
                }
            } else if ((objId != "storeType" || (objId == "storeType" && codeRef3 != 'VIRTUAL')) && (objId != "userGradeSeq" || (objId == "userGradeSeq" && codeCd != '11110'))) {
                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
            }
        }
    });

    if (appendOption != "") $(obj).append(appendOption);
}

/**
 * 쿠폰 타입 컨트롤
 * */

var couponTypeControl = {
    couponType : null,

    deactivateControl: function(attrName){
        var onclickStr = "return false;";
        var storeType = $("#storeType").val();

        if ($.jUtil.isEmpty(attrName)) {
            return;
        }

        attrName = "[" + attrName + "]";

        $("#couponForm").find("select").each(function() {
            var objId = $(this).attr("id");

            /*if (objId != "couponType" && objId != "grpDiscountType" && objId != "shareYn"
                && objId != "slotStartTime" && objId != "slotEndTime" && objId != "applyScope") {*/
            /*if (objId == "discountType") {
                if ($(this).is(attrName)) {
                    //removeAllSelectOption($(this));
                    //$(this).prop("disabled", true);
                    $(this).attr("onClick", "return false;");
                } else {
                    //addSelectOption($(this));
                    //$(this).prop("disabled", false);
                    $(this).attr("onClick", "return true;");
                }
            }*/
        });

        $("#couponForm").find("input:text").each(function() {
            if ($(this).attr("id") != "couponNo") {
                if ($(this).attr("id") == "purchaseMin" && storeType == STORE_TYPE_DS) {  // DS, 최소구매금액
                    $(this).attr("readonly", false);
                } else if ($(this).is(attrName)) {
                    $(this).attr("readonly", true);
                } else {
                    $(this).attr("readonly", false);
                }
            } else {
                $(this).attr("readonly", true);
            }
        });

        $("#couponForm").find("input:checkbox").each(function() {
            if ($(this).is(attrName)) {
                //$(this).prop("checked", false).attr("onClick", onclickStr);
                $(this).attr("onClick", onclickStr);
            } else {
                $(this).attr("onClick", "return true;");
            }
        });
    }
};
