/**
 * 프로모션관리 > 행사관리 > 덤 행사 관리 메인 js
 */
$(document).ready(function () {
    etcPromoGrid.init();
    etcPromoItemGrid.init();
    CommonAjaxBlockUI.global();
    etcPromoSearch.init();
    etcPromo.init();
});

var etcPromoSearch = {
    init : function () {
        etcPromoSearch.searchFormReset();
        etcPromoSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();

        // 조회 기간 초기화
        etcPromo.calendar.initCalendarDate(etcPromoSearch, "schStartDt", "schEndDt", "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(etcPromoSearch.searchEtcPromoList);
        $("#schResetBtn").bindClick(etcPromoSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                etcPromoSearch.searchEtcPromoList();
            }
        });
    }
    /**
     * 덤 행사 정보 조회
     */
    , searchEtcPromoList : function (etcNo) {
        // 검색조건 validation check
        if (etcPromo.valid.searchValid()) {
            // 덤 행사 정보 조회 개수 초기화
            $("#etcPromoTotalCnt").html("0");

            // 조회
            var searchForm = $("#searchForm").serialize();

            CommonAjax.basic({
                url : "/event/getEtcPromoList.json"
                , data : searchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    etcPromoGrid.setData(res);

                    $("#etcPromoTotalCnt").html($.jUtil.comma(etcPromoGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(etcNo)) {
                        var rowIdx = etcPromoGrid.dataProvider.searchDataRow({fields : ["etcNo"], values : [etcNo]});

                        if (rowIdx > -1) {
                            etcPromoGrid.gridView.setCurrent({dataRow : rowIdx, fieldIndex : 1});
                            etcPromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

var etcPromo = {
    selectEtcNo : null
    , storeList : []

    /**
     * 덤 행사 상세 초기화
     */
    , init : function () {
        etcPromo.bindingEvent();
        etcPromo.etcPromoFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(etcPromo.setEtcPromo);           // 저장
        $("#resetBtn").bindClick(etcPromo.etcPromoFormReset);   // 초기화
        $("#storePopBtn").bindClick(etcPromo.getStorePopup);    // 점포선택

        // 입력 문자 길이 계산
        $("#etcMngNm").calcTextLength("keyup", "#etcMngNmLen");
        $("#etcInfo").calcTextLength("keyup", "#etcInfoLen");
    }
    /**
     * 덤 행사 상세 내용 초기화
     */
    , etcPromoFormReset : function () {
        $("#etcPromoForm").resetForm();

        // 전시 기간 초기화
        etcPromo.calendar.initCalendarDate(etcPromoSearch, "promoStartDt", "promoEndDt", "1d", "15d");

        // span 초기화
        $("#etcPromoTotalCnt").html("0");
        $("#etcMngNmLen").html("0");
        $("#etcInfoLen").html("0");
        $("#itemCnt").html("0");

        // 덤 행사 번호 초기화
        etcPromo.selectEtcNo = null;

        // 덤 행사 점포 정보 초기화
        etcPromo.storeList = [];

        // 적용상품 그리드 초기화
        etcPromoItemGrid.dataProvider.clearRows();
    }
    , getEtcPromoDetail : function (etcNo) {
        // 덤 행사 상세 초기화
        etcPromo.etcPromoFormReset();

        etcPromo.selectEtcNo = etcNo;

        CommonAjax.basic({
            url:"/event/getEtcPromoDetail.json"
            , data : {etcNo : etcNo}
            , method : "GET"
            , callbackFunc : function (res) {
                etcPromo.detailCallback(res);
            }
        });
    }
    /**
     * 덤 행사 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param etcPromoInfo
     * @returns {boolean}
     */
    , detailCallback : function (etcPromoInfo) {
        if ($.jUtil.isEmpty(etcPromoInfo)) {
            return true;
        }

        // 행사상세
        $("#storeType").val(etcPromoInfo.storeType);
        $("#etcMngNm").val(etcPromoInfo.etcMngNm).setTextLength("#etcMngNmLen");
        $("#etcInfo").val(etcPromoInfo.etcInfo).setTextLength("#etcInfoLen");
        $("#promoStartDt").val(etcPromoInfo.promoStartDt);
        $("#promoEndDt").val(etcPromoInfo.promoEndDt);
        $("#useYn").val(etcPromoInfo.useYn);
        $("input:radio[name='dispStoreType']:input[value='" + etcPromoInfo.dispStoreType + "']").prop("checked", true);

        if (!$.jUtil.isEmpty(etcPromoInfo.storeList)) {
            etcPromo.storeList = etcPromoInfo.storeList;
        }

        if (!$.jUtil.isEmpty(etcPromoInfo.itemList)) {
            etcPromoItemGrid.setData(etcPromoInfo.itemList);
        }
    }
    /**
     * 점포 선택 버튼 클릭시 팝업 호출
     */
    , getStorePopup : function() {
        if ($.jUtil.isEmpty(etcPromo.storeList)) {
            etcPromo.popup.openStorePopup();
        } else {
            etcPromo.popup.openStorePopup(etcPromo.storeList);
        }
    }
    /**
     * 적용 상품 수 설정
     */
    , setItemCnt : function () {
        $("#itemCnt").html(etcPromoItemGrid.dataProvider.getRowCount());
    }
    /**
     * 선택한 상품 정보 setting
     * @param gridRow
     */
    , setItemInfo : function (gridRow) {
        $("#itemNo").val(gridRow.itemNo);
        $("#itemNm").val(gridRow.itemNm);
        $("#dispYn").val(gridRow.dispYn);
    }
    /**
     * 선택한 상품 전시여부 적용
     */
    , setItemDispYn : function () {
        var rowIdx = etcPromoItemGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [$("#itemNo").val()]});
        etcPromoItemGrid.gridView.setValue(rowIdx, "dispYn", $("#dispYn").val());
    }
    /**
     * 저장
     */
    , setEtcPromo : function () {
        if (etcPromo.valid.setEtcPromoValid()) {
            var etcPromoForm = $("#etcPromoForm").serializeObject(true);

            // 덤 행사 번호
            etcPromoForm.etcNo = $.jUtil.isEmpty(etcPromo.selectEtcNo) ? null : Number(etcPromo.selectEtcNo);

            // 노출 점포 유형이 PART일 경우 점포 정보
            if ($("input:radio[name='dispStoreType']:checked").val() == "PART") {
                var storeIdList = new Array();

                for (var idx in etcPromo.storeList) {
                    storeIdList.push(etcPromo.storeList[idx].storeId);
                }

                etcPromoForm.storeIdList = storeIdList;
            } else {
                etcPromoForm.storeIdList = [];
            }

            // 적용 상품 정보 (insert : 등록, update : 수정)
            etcPromoForm.insertItemList = etcPromoItemGrid.getGridStateData("created");
            etcPromoForm.updateItemList = etcPromoItemGrid.getGridStateData("updated");

            // 덤 행사 저장 호출
            CommonAjax.basic({
                url : "/event/setEtcPromo.json"
                , data : JSON.stringify(etcPromoForm)
                , method : "POST"
                , contentType: "application/json"
                , successMsg : "저장되었습니다."
                , callbackFunc : function (res) {
                    etcPromoSearch.searchEtcPromoList(res);
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        etcPromoSearch.searchEtcPromoList(etcPromo.selectEtcNo);
                    }
                }
            });
        }
    }
};

etcPromo.calendar = {
    initCalendarDate : function (targetObj, startId, endId, startFlag, endFlag) {
        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        targetObj.setDate = Calendar.datePickerRange(startId, endId);
        targetObj.setDate.setStartDate(startFlag);
        targetObj.setDate.setEndDate(endFlag);

        endObj.datepicker("option", "minDate", startObj.val());
    }
};

/**
 * 팝업관련
 */
etcPromo.popup = {
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStorePopup : function(data) {
        var storeType = $("#storeType").val();

        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        var target = "etcStorePop";
        windowPopupOpen("/common/popup/storeSelectPop", target, 480, 450);

        var frm = document.getElementById("storePopForm");
        frm.target = target;

        frm.submit();
    }
    , storePopCallback : function (resDataArr) {
        etcPromo.storeList = resDataArr.storeList;
    }
    /**
     * 상품 공통팝업
     */
    , openItemPopup : function () {
        var target = "etcItemPopup";
        var callback = "etcPromo.popup.itemPopCallback";
        var storeType = $("#storeType").val();
        var mallType;

        if (storeType == "DS") {
            mallType = "DS";
        } else {
            mallType = "TD";
        }

        var url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + $("#storeType").find("option:selected").data("sitetype") + "&storeType=" + $("#storeType").val();

        windowPopupOpen(url, target, 1084, 650);
    }
    /**
     * 상품 공통팝업 콜백
     *  - 상품번호로 상세정보 조회
     * @param resDataArr
     */
    , itemPopCallback : function (resDataArr) {
        etcPromoItemGrid.appendData(resDataArr, "POP");
    }
    /**
     * 상품 일괄등록 팝업
     */
    , openItemReadPopup : function () {
        var target = "etcExcelPop";
        var callback = "etcPromo.popup.itemExcelPopCallback";
        var url = "/promotion/popup/upload/excelReadPop?fileType=" + $("#promoType").val() + "&callback=" + callback + "&templateUrl=/static/templates/etcPromoItem_template.xlsx";

        windowPopupOpen(url, target, 600, 350);
    }
    /**
     * 상품 일괄등록 팝업 콜백
     */
    , itemExcelPopCallback : function (resDataArr) {
        etcPromoItemGrid.appendData(resDataArr, "EXCEL");
    }
};


/**
 * 덤 행사 유효성 검사 관련
 */
etcPromo.valid = {
    searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'ETCNO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    }
    , setEtcPromoValid : function () {
        if ($.jUtil.isEmpty($("#etcMngNm").val())) {
            return $.jUtil.alert("덤 관리명을 입력해주세요.");
        }

        if ($.jUtil.isEmpty($("#etcInfo").val())) {
            return $.jUtil.alert("덤 행사정보를 입력해주세요.");
        }

        var nowDt = moment().format("YYYY-MM-DD");

        // 전시시작기간
        if (!moment($("#promoStartDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("행사시작 날짜 형식이 맞지 않습니다");
            $("#promoStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#promoEndDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("행사종료 날짜 형식이 맞지 않습니다");
            $("#promoEndDt").val(nowDt);
            return false;
        }

        // 노출 점포 설정일 시 점포 선택값 확인
        if ($("input:radio[name='dispStoreType']:checked").val() == "PART" && $.jUtil.isEmpty(etcPromo.storeList)) {
            return $.jUtil.alert("노출 점포를 선택해주세요.");
        }

        // 적용상품 확인
        if (etcPromoItemGrid.dataProvider.getRowCount() <= 0) {
            return $.jUtil.alert("상품을 선택해주세요.");
        }

        return true;
    }
};