/**
 * 프로모션관리 > 행사관리 > 마일리지 행사 관리 메인 js
 */
$(document).ready(function () {
    mileagePromoGrid.init();
    mileageItemGrid.init();
    CommonAjaxBlockUI.global();
    mileagePromoSearch.init();
    mileagePromo.init();
});

var mileagePromoSearch = {
    init : function () {
        mileagePromoSearch.searchFormReset();
        mileagePromoSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();

        // 조회 기간 초기화
        mileagePromo.view.initCalendarDate(mileagePromoSearch, "schStartDt", "schEndDt", "SEARCH", "-1w", "0", "");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(mileagePromoSearch.searchMileagePromoList);
        $("#schResetBtn").bindClick(mileagePromoSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                mileagePromoSearch.searchMileagePromoList();
            }
        });
    }
    /**
     * 마일리지 행사 정보 조회
     */
    , searchMileagePromoList : function (mileageNo) {
        // 검색조건 validation check
        if (mileagePromo.valid.searchValid()) {
            // 마일리지 행사 정보 조회 개수 초기화
            $("#mileagePromoTotalCnt").html("0");

            // 조회
            var searchForm = $("#searchForm").serialize();

            CommonAjax.basic({
                url : "/event/getMileagePromoList.json"
                , data : searchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    mileagePromoGrid.setData(res);

                    $("#mileagePromoTotalCnt").html($.jUtil.comma(mileagePromoGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(mileageNo)) {
                        var rowIdx = mileagePromoGrid.dataProvider.searchDataRow({fields : ["mileageNo"], values : [mileageNo]});

                        if (rowIdx > -1) {
                            mileagePromoGrid.gridView.setCurrent({dataRow : rowIdx, fieldIndex : 1});
                            mileagePromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

var mileagePromo = {
    selectMileageNo : null
    , storeList : []
    , paymentList : []

    /**
     * 마일리지 행사 상세 초기화
     */
    , init : function () {
        mileagePromo.bindingEvent();
        mileagePromo.mileagePromoFormReset();

        // 전시 기간 초기화
        mileagePromo.view.initSetDateTime("15d");
        $("#promoStartDt, #promoEndDt").val("").prop("disabled", true);
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(mileagePromo.setMileagePromo);           // 저장
        $("#resetBtn").bindClick(mileagePromo.mileagePromoFormReset);   // 초기화
        $("#siteType").bindChange(mileagePromo.view.changeSiteType);    // 사이트구분 변경시 -> 사이트구분 hidden으로 변경. but 하위 컨트롤을 위해 남겨둠
        $("#earnType").bindChange(mileagePromo.view.changeEarnType);    // 적립방법 변경시
        $("#storeType").bindChange(mileagePromo.view.changeStoreType);  // 적립타입 > 점포유형 변경시
        $("input:radio[name='dispStoreType']").bindChange(mileagePromo.view.changeDispStoreType);   // 점포별 설정 변경시
        $("#storePopBtn").bindClick(mileagePromo.getStorePopup);        // 점포선택
        $("#itemPopBtn").bindClick(mileagePromo.popup.openItemPopup);   // 상품팝업
        $("#itemExcelUpBtn").bindClick(mileagePromo.popup.openItemReadPopup);   // 상품일괄등록
        $("#itemDelBtn").bindClick(mileageItemGrid.removeCheckData);    // 상품삭제
        $("#searchErsPromoBtn").bindClick(mileagePromo.popup.openErsPromoPopup);   // 자동설정 (사은행사 팝업)
        $("#cardInfoBtn").bindClick(mileagePromo.popup.openCardPopup);

        // 입력 문자 길이 계산
        $("#mileageMngNm").calcTextLength("keyup", "#mileageMngNmLen");
        // $("#mileageTypeName").calcTextLength("keyup", "#mileageTypeNmLen");
        $("#displayMessage").calcTextLength("keyup", "#displayMessageLen");
        $("#purchaseGuide").calcTextLength("keyup", "#purchaseGuideLen");

        // 숫자만 입력
        $("#earnAmount, #earnAmountMax, #purchaseMin, #expireDayInp").allowInput("keyup", ["NUM"], $(this).attr("id"));
        $("#earnPercent").allowInput("keyup", ["NUM", "DOT"], $(this).attr("id"));

        // 정률 입력 시 자리수 확인
        $("#earnPercent").keyup(function (e) {
            var earnPercent = $(this).val();

            if (!mileagePromo.valid.isPercentValid(earnPercent)) {
                var percent = earnPercent.substr(0,earnPercent.length - 1);
                $(this).val(percent);
            }
        });
    }
    /**
     * 마일리지 행사 상세 내용 초기화
     */
    , mileagePromoFormReset : function () {
        $("#mileagePromoForm").resetForm();

        // span 초기화
        $("#mileageNo").html("");
        $("#mileageMngNmLen").html("0");
        $("#itemCnt").html("0");
        $("#mileageTypeNmLen").html("0");
        $("#displayMessageLen").html("0");
        $("#storeCnt").html("0");
        $("#cardCnt").html("0");
        $("#purchaseGuideLen").html("0");

        // 최소구매금액 초기화
        $("#purchaseMin").val("");

        // 마일리지 행사 번호 초기화
        mileagePromo.selectMileageNo = null;

        // 마일리지 행사 점포 정보 초기화
        mileagePromo.storeList = [];

        // 마일리지 행사 카드 결제 정보 초기화
        mileagePromo.paymentList = [];

        // 적용상품 그리드 초기화
        mileageItemGrid.dataProvider.clearRows();

        // 사이트구분에 따른 마일리지 적립 점포유형 변경
        $("#siteType").trigger("change");

        // 적립 유형에 따른 UI 변경
        $("#earnType").trigger("change");

        // 마일리지 유효기간 초기화
        $("#expireDayInp").prop("disabled", false);
    }
    , getMileagePromoDetail : function (mileageNo) {
        // 마일리지 행사 상세 초기화
        mileagePromo.mileagePromoFormReset();

        mileagePromo.selectMileageNo = mileageNo;

        CommonAjax.basic({
            url:"/event/getMileagePromoDetail.json"
            , data : {mileageNo : mileageNo}
            , method : "GET"
            , callbackFunc : function (res) {
                mileagePromo.detailCallback(res);
            }
        });
    }
    /**
     * 마일리지 행사 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param mileagePromoInfo
     * @returns {boolean}
     */
    , detailCallback : function (mileageInfo) {
        if ($.jUtil.isEmpty(mileageInfo)) {
            return true;
        }

        // 행사상세
        $("#mileageNo").html(mileageInfo.mileageNo);
        $("#siteType").val(mileageInfo.siteType).trigger("change");
        $("#mileageMngNm").val(mileageInfo.mileageMngNm).setTextLength("#mileageMngNmLen");
        $("#promoStartDt").val(mileageInfo.promoStartDt);
        $("#promoEndDt").val(mileageInfo.promoEndDt);
        // $("#promoEndDt").datepicker("option", "minDate", $("#promoStartDt").val());

        var earnType = mileageInfo.earnType;
        $("#earnType").val(earnType).trigger("change");
        if (earnType == "RATE") {
            $("#earnPercent").val(mileageInfo.earnPercent);
            $("#earnAmountMax").val(mileageInfo.earnAmountMax);
        } else {
            $("#earnAmount").val(mileageInfo.earnAmount);
        }

        $("#departCd").val(mileageInfo.departCd);
        $("#purchaseMin").val(mileageInfo.purchaseMin);
        $("#mileageTypeNm").val(mileageInfo.mileageTypeNm);

        $("#storeType").val(mileageInfo.storeType).trigger("change");
        $("#displayMessage").val(mileageInfo.displayMessage).setTextLength("#displayMessageLen");
        $("#expireDayInp").val(mileageInfo.expireDayInp).prop("disabled", true);
        $("input:radio[name='dispStoreType']:input[value='" + mileageInfo.dispStoreType + "']").prop("checked", true).trigger("change");
        $("#useYn").val(mileageInfo.useYn);
        $("#purchaseGuide").val(mileageInfo.purchaseGuide).setTextLength("#purchaseGuideLen");

        // 점포정보
        if (!$.jUtil.isEmpty(mileageInfo.storeList)) {
            $("#storeCnt").html(mileageInfo.storeList.length);
            mileagePromo.storeList = mileageInfo.storeList;
        }

        // 상품정보
        if (!$.jUtil.isEmpty(mileageInfo.itemList)) {
            mileageItemGrid.setData(mileageInfo.itemList);
        }

        // 결제수단
        if (!$.jUtil.isEmpty(mileageInfo.paymentList)) {
            mileagePromo.paymentList = mileageInfo.paymentList;
            $("#cardCnt").html(mileageInfo.paymentList.length);
        }
    }
    /**
     * 점포 선택 버튼 클릭시 팝업 호출
     */
    , getStorePopup : function() {
        if ($.jUtil.isEmpty(mileagePromo.storeList)) {
            mileagePromo.popup.openStorePopup();
        } else {
            mileagePromo.popup.openStorePopup(mileagePromo.storeList);
        }
    }
    /**
     * 적용 상품 수 설정
     */
    , setItemCnt : function (rmCnt) {
        $("#itemCnt").html(mileageItemGrid.dataProvider.getRowCount() - rmCnt);
    }
    /**
     * 사은행사 정보 상세 조회
     * @param eventCd
     */
    , getErsPromoDetail : function (eventCd) {
        CommonAjax.basic({
            url:"/event/getMileageErsPromoDetail.json?eventCd="+eventCd
            , method : "GET"
            , callbackFunc : function(res) {
                mileagePromo.ersPromoDetailCallback(res);
            }
        });
    }
    , ersPromoDetailCallback : function (res) {
        if (!$.jUtil.isEmpty(res)) {
            $("#mileageMngNm").val(res.eventNm).trigger("keyup");    // 행사명
            $("#promoStartDt").val(res.eventStDate);    // 행사 시작일
            $("#promoEndDt").val(res.eventEdDate);      // 행사 종료일

            // 최소구매금액 - 하한금액
            if (Number(res.eventTargetType) > 20 && Number(res.eventTargetType) < 30) {
                $("#purchaseMin").val(res.lowAmt);
            }

            // 적립방법
            $("#earnType").val(res.giveType).trigger("change");

            if (res.giveType == "PRICE") {
                $("#earnAmount").val(res.ticketAmt);
            } else {
                $("#earnPercent").val(res.ticketAmt);
            }

            // 점포설정 -> 노출 점포 설정으로 변경
            $("input:radio[name='dispStoreType']:input[value='PART']").prop("checked", true).trigger("change");
            if (!$.jUtil.isEmpty(res.storeList)) {
                mileagePromo.storeList = res.storeList;
                $("#storeCnt").html(res.storeList.length);
            }

            if (!$.jUtil.isEmpty(res.itemList)) {
                // mileageItemGrid.setData(res.itemList);
                mileageItemGrid.appendData(res.itemList, "POP");
            }
        }
    }
    /**
     * 저장
     */
    , setMileagePromo : function () {
        if (mileagePromo.valid.setMileagePromoValid()) {
            var mileagePromoForm = $("#mileagePromoForm").serializeObject(true);

            // 마일리지 행사기간
            mileagePromoForm.promoStartDt = $("#promoStartDt").val();
            mileagePromoForm.promoEndDt = $("#promoEndDt").val();

            // 마일리지 행사 번호
            mileagePromoForm.mileageNo = $.jUtil.isEmpty(mileagePromo.selectMileageNo) ? null : Number(mileagePromo.selectMileageNo);

            // 노출 점포 유형이 PART일 경우 점포 정보
            if ($("input:radio[name='dispStoreType']:checked").val() == "PART") {
                var storeIdList = new Array();

                for (var idx in mileagePromo.storeList) {
                    storeIdList.push(mileagePromo.storeList[idx].storeId);
                }

                mileagePromoForm.storeIdList = storeIdList;
            } else {
                mileagePromoForm.storeIdList = [];
            }

            // 마일리지 코드
            mileagePromoForm.mileageCode = $("#storeType").find('option:selected').data("ref3");

            // 적용 상품 정보 (insert : 등록, delete : 삭제)
            mileagePromoForm.createItemList = mileageItemGrid.getGridStateData("created");
            mileagePromoForm.deleteItemList = mileageItemGrid.getGridStateData("deleted");

            // 마일리지 결제수단 카드 정보
            mileagePromoForm.paymentList = mileagePromo.paymentList;

            // 마일리지 행사 저장 호출
            CommonAjax.basic({
                url : "/event/setMileagePromo.json"
                , data : JSON.stringify(mileagePromoForm)
                , method : "POST"
                , contentType: "application/json"
                , successMsg : "저장되었습니다."
                , callbackFunc : function (res) {
                    mileagePromoSearch.searchMileagePromoList(res);
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        mileagePromoSearch.searchMileagePromoList(mileagePromo.selectMileageNo);
                    }
                }
            });
        }
    }
};

mileagePromo.view = {
    initCalendarDate : function(targetObj, startId, endId, flag, startFlag, endFlag, timeFormat) {
        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        if (flag == SEARCH_AREA) {
            targetObj.setDate = Calendar.datePickerRange(startId, endId);
            targetObj.setDate.setEndDate(endFlag);
            targetObj.setDate.setStartDate(startFlag);
        } else {
            targetObj.setDate = CalendarTime.datePickerRange(startId, endId, {timeFormat: timeFormat});
            targetObj.setDate.setEndDateByTimeStamp(endFlag);
            targetObj.setDate.setStartDate(startFlag);
        }
    }
    , initSetDateTime : function (flag) {
        // itemMain.util.initDateTime('10d', 'saleStartDt', 'saleEndDt', "HH:00:00", "HH:59:59");
        mileagePromo.setDateTime = CalendarTime.datePickerRange("promoStartDt", "promoEndDt", {timeFormat:"HH:00:00"}, true, {timeFormat:"HH:59:59"}, true);

        mileagePromo.setDateTime.setEndDateByTimeStamp(flag);
        mileagePromo.setDateTime.setStartDate("1d");

        $("#promoEndDt").datepicker("option", "minDate", $("#promoStartDt").val());
    }
    /**
     * 사이트구분 변경시
     */
    , changeSiteType : function () {
        var siteType = $("#siteType").val();
        mileagePromo.view.addSelectOption("#storeType", siteType);

        // 점포별 설정 초기화
        mileagePromo.view.changeStoreType("INIT");

        // 최초 저장 이후 변경 불가하도록
        if ($.jUtil.isEmpty(mileagePromo.selectMileageNo)) {
            $("#siteType").prop("disabled", false);
        } else {
            $("#siteType").prop("disabled", true);
        }
    }
    /**
     * 적립방법 변경시
     */
    , changeEarnType : function () {
        var earnType = $("#earnType").val();

        switch (earnType) {
            case "RATE" :
                $("#earnPriceSpan").hide();
                $("#earnPercentSpan").show();
                break;
            case "PRICE" :
            default :
                $("#earnPriceSpan").show();
                $("#earnPercentSpan").hide();
                break;
        }
    }
    /**
     * 적립타입생성 > 점포유형 변경 시 점포별 설정 변경
     * - HYPER / CLUB 일 경우만 노출점포 설정 가능 (DS일 경우 비활성화)
     * - 초기화 시 선택한 노출점포설정 초기화
     */
    , changeStoreType : function (flag) {
        var storeType = $("#storeType").val();
        var mileageCd = $("#storeType").find('option:selected').data("ref3");

        // 마일리지 점포유형에 따른 연동코드 노출
        $("#mileageCode").html(mileageCd);

        if (storeType == "DS" || flag == "INIT") {
            $("input:radio[name='dispStoreType']:input[value='ALL']").prop("checked", true).trigger("change");

            if (flag == "INIT") {
                mileagePromo.storeList = [];
            }

            $("#storeCnt").html("0");
        }

        var checked = false;

        $("input:radio[name='dispStoreType']").each(function () {
            if (storeType == "DS") {
                checked = !$(this).prop("checked");
            }

            $(this).prop("disabled", checked);
        });

        // 최초 저장 이후 변경 불가하도록
        if ($.jUtil.isEmpty(mileagePromo.selectMileageNo)) {
            $("#storeType").prop("disabled", false);
        } else {
            $("#storeType").prop("disabled", true);
        }
    }
    /**
     * 점포별 설정 변경 시 점포선택 버튼 컨트롤
     * - 전체(ALL) : 점포선택 버튼 활성화
     * - 노출 점포설정(PART) : 점포선택 버튼 비활성화
     */
    , changeDispStoreType : function () {
        if ($("input:radio[name='dispStoreType']:checked").val() == "ALL") {
            $("#storePopBtn").prop("disabled", true);
            $("#storeCnt").html("0");
        } else {
            $("#storePopBtn").prop("disabled", false);
            $("#storeCnt").html(mileagePromo.storeList.length);
        }
    }
    /**
     * select box option 삭제 후 조건에 맞는 option 추가
     * @param obj
     * @param siteType
     * @returns {boolean}
     */
    , addSelectOption : function (obj, siteType) {
        var jsonData = "";
        var defaultOption = "";

        switch ($(obj).attr("id")) {
            case "storeType":
                jsonData = storeTypeJson;
                break;
        }

        if (jsonData == "") return false;

        $(obj).find("option").remove();

        if (defaultOption != "") $(obj).prepend(defaultOption);

        $.each(jsonData, function (key, code) {
            var chkOption = 0;
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            var codeRef2 = code.ref2;
            var codeRef3 = code.ref3;

            $(obj).find("option").each(function () {
                if ($(this).val() == codeCd) {
                    chkOption++;
                }
            });

            if (chkOption == 0) {
                if ($(obj).attr("id") == "storeType") {
                    if (codeRef2 == siteType){
                        $(obj).append("<option value=" + codeCd + " data-ref3=" + codeRef3 + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                    }
                } else {
                    $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                }
            }
        });
    }
};

/**
 * 팝업관련
 */
mileagePromo.popup = {
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStorePopup : function(data) {
        var storeType = $("#storeType").val();

        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        var target = "mileageStorePop";
        windowPopupOpen("/common/popup/storeSelectPop", target, 610, 450);

        var frm = document.getElementById("storePopForm");
        frm.target = target;

        frm.submit();
    }
    , storePopCallback : function (resDataArr) {
        mileagePromo.storeList = resDataArr.storeList;
        $("#storeCnt").html(mileagePromo.storeList.length);
    }
    /**
     * 상품 공통팝업
     */
    , openItemPopup : function () {
        var target = "mileageItemPopup";
        var callback = "mileagePromo.popup.itemPopCallback";
        var storeType = $("#storeType").val();
        var mallType;

        if (storeType == "DS") {
            mallType = "DS";
        } else {
            mallType = "TD";
        }

        var url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + $("#siteType").val() + "&storeType=" + storeType;

        windowPopupOpen(url, target, 1084, 650);
    }
    /**
     * 상품 공통팝업 콜백
     *  - 상품번호로 상세정보 조회
     * @param resDataArr
     */
    , itemPopCallback : function (resDataArr) {
        mileageItemGrid.appendData(resDataArr, "POP");
    }
    /**
     * 상품 일괄등록 팝업
     */
    , openItemReadPopup : function () {
        var target = "mileageExcelPop";
        var callback = "mileagePromo.popup.itemExcelPopCallback";
        var url = "/promotion/popup/upload/excelReadPop?fileType=" + $("#promoType").val() + "&callback=" + callback + "&templateUrl=/static/templates/etcPromoItem_template.xlsx";

        windowPopupOpen(url, target, 600, 350);
    }
    /**
     * 상품 일괄등록 팝업 콜백
     */
    , itemExcelPopCallback : function (resDataArr) {
        mileageItemGrid.appendData(resDataArr, "EXCEL");
    }
    /**
     * 사은행사 검색 팝업
     */
    , openErsPromoPopup : function () {
        var target = "mileageErsPop";
        windowPopupOpen("/common/popup/ersPromoPop", target, 1084, 610);

        var frm = document.getElementById("ersPromoPopForm");
        frm.target = target;

        frm.submit();
    }
    /**~
     * 사은행사 조회 팝업 콜백 (공통팝업)
     * @param ersPromo
     */
    , ersPopCallback : function (checkPromoList) {
        // var eventCd = ersPromo[0].eventCd;

        // 사은행사 상세 정보 조회 호출
        mileagePromo.getErsPromoDetail(checkPromoList[0].eventCd);
    }
    /**
     * 카드선택 팝업
     */
    , openCardPopup : function() {
        var storeType = $("#storeType").val();
        var siteType = getSiteType(storeType);
        var chgYn = $("#chgYn").val();


        $(".cardPop input[name='siteType']").val(siteType);
        $(".cardPop input[name='paymentList']").val(JSON.stringify(mileagePromo.paymentList));
        $(".cardPop input[name='setYn']").val(chgYn);

        var target = "mileageCardPopup";
        windowPopupOpen("/common/popup/cardPop", target, 470, 425);

        var frm = document.getElementById("cardSelectPopForm");
        frm.target = target;

        frm.submit();
    }
    /**
     * 카드 결제수단 팝업 call back
     */
    , cardPopCallback: function(checkedPaymentList) {
        var resCnt = checkedPaymentList.length;
        mileagePromo.paymentList = checkedPaymentList;

        $("#cardCnt").html(resCnt);
    }
};


/**
 * 마일리지 행사 유효성 검사 관련
 */
mileagePromo.valid = {
    /**
     * 소수점 1자리까지 퍼센트 입력 검사
     * @param val
     * @returns {boolean}
     */
    isPercentValid : function(val) {
        if(val == "") return false;

        var _pattern = /^(\d{1,3}([.]\d{0,1})?)?$/;

        if (!_pattern.test(val)) {
            return false;
        } else {
            if(Math.ceil(val) > 100) return false;
        }

        return true;
    }
    , searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'MILEAGENO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    }
    /**
     * 저장 시 유효성 검사
     */
    , setMileagePromoValid : function () {
        var mileageMngNm = $("#mileageMngNm").val();
        if ($.jUtil.isEmpty(mileageMngNm)) {
            return $.jUtil.alert("마일리지 관리명을 입력해주세요.", "mileageMngNm");
        }

        if(mileageMngNm.length > 20) {
            return $.jUtil.alert("마일리지 관리명은 최대 20자리까지 입력이 가능합니다.", "mileageMngNm");
        }

        var nowDt = moment().format("YYYY-MM-DD HH:mm:ss");

        // 전시시작기간
        if (!moment($("#promoStartDt").val(), "YYYY-MM-DD HH:mm:ss", true).isValid()) {
            alert("행사시작 날짜 형식이 맞지 않습니다");
            $("#promoStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#promoEndDt").val(), "YYYY-MM-DD HH:mm:ss", true).isValid()) {
            alert("행사종료 날짜 형식이 맞지 않습니다");
            $("#promoEndDt").val(nowDt);
            return false;
        }

        var earnType        = $("#earnType").val();
        var earnPercent     = $("#earnPercent").val();
        var earnAmount      = $("#earnAmount").val();
        var earnAmountMax   = $("#earnAmountMax").val();
        var purchaseMin     = $("#purchaseMin").val();
        var departCd        = $("#departCd").val();

        // 적립방법별
        switch (earnType) {
            case "RATE" :
                if ($.jUtil.isEmpty(earnPercent) || Number(earnPercent) <= 0) {
                    return $.jUtil.alert("적립률을 입력해 주세요.", "earnPercent");
                }

                if(mileagePromo.valid.isPercentValid(earnPercent) == false) {
                    return $.jUtil.alert("적립률은 100을 넘을 수 없습니다.", "earnPercent");
                }

                if ($.jUtil.isEmpty(earnAmountMax) || Number(earnAmountMax) <= 0) {
                    return $.jUtil.alert("최대적립금액을 입력해 주세요.", "earnAmountMax");
                }

                if (Number(earnAmountMax) > 999999999) {
                    return $.jUtil.alert("최대적립금액은 999,999,999을 넘을 수 없습니다.", "earnAmountMax");
                }
                break;
            case "PRICE" :
                if ($.jUtil.isEmpty($("#earnAmount").val()) || Number(earnAmount) < 1) {
                    return $.jUtil.alert("적립금액을 입력해 주세요.", "earnAmount");
                }

                if (Number(earnAmountMax) > 999999999) {
                    return $.jUtil.alert("적립금액은 999,999,999을 넘을 수 없습니다.", "earnAmountMax");
                }
                break;
        }

        // 주관부서
        if ($.jUtil.isEmpty(departCd)) {
            return $.jUtil.alert("주관부서를 선택하세요.", "departCd");
        }

        // 최소구매금액
        if (Number(purchaseMin) <= 0) {
            return $.jUtil.alert("최소구매금액을 입력해주세요.", "purchaseMin");
        }

        if (Number(purchaseMin.substr(purchaseMin.length - 1)) > 0) {
            return $.jUtil.alert("최소구매금액의 원단위는 반드시 0이어야 합니다.", "purchaseMin");
        }

        if (Number(purchaseMin) > 99999990) {
            return $.jUtil.alert("최소구매금액은 99,999,990을 넘을 수 없습니다.", "purchaseMin");
        }

        // 노출 점포 설정일 시 점포 선택값 확인
        if ($("input:radio[name='dispStoreType']:checked").val() == "PART" && $.jUtil.isEmpty(mileagePromo.storeList)) {
            return $.jUtil.alert("노출 점포를 선택해주세요.", "dispStoreType");
        }

        if ($.jUtil.isEmpty($("#storeType").val())) {
            return $.jUtil.alert("마일리지 점포유형을 선택하세요.", "storeType");
        }

        var displayMessage = $("#displayMessage").val();
        if ($.jUtil.isEmpty(displayMessage)) {
            return $.jUtil.alert("마일리지 적립사유를 입력해주세요.", "displayMessage");
        }

        if (displayMessage.length > 20) {
            return $.jUtil.alert("마일리지 적립사유는 최대 20자리까지 입력이 가능합니다.", "displayMessage");
        }

        if ($.jUtil.isEmpty($("#expireDayInp").val())) {
            return $.jUtil.alert("마일리지 유효기간을 입력해주세요.", "expireDayInp");
        }

        if ($("#expireDayInp").val() < 1 || $("#expireDayInp").val() > 365) {
            return $.jUtil.alert("마일리지 유효기간은 1일 ~ 365일 까지 등록 가능합니다.", "expireDayInp");
        }

        // 구매가이드 입력 확인
        var purchaseGuide = $("#purchaseGuide").val();
        if ($.jUtil.isEmpty(purchaseGuide)) {
            return $.jUtil.alert("구매가이드를 입력해주세요.", "purchaseGuide");
        }

        if (purchaseGuide.length > 150) {
            return $.jUtil.alert("구매가이드는 최대 150자리까지 입력이 가능합니다.", "purchaseGuide");
        }

        // 적용상품 확인
        var createSize = mileageItemGrid.getGridStateData("created").length + mileageItemGrid.getGridStateData("none").length;
        if (createSize <= 0) {
            return $.jUtil.alert("상품을 선택해주세요.");
        }

        return true;
    }
};