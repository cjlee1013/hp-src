/**
 * 점포행사관리(rmsPromoMain.jsp) 에서 사용, 그리드 관련 컨트롤 분리
 * */

// 점포행사관리 조회 그리드
var rmsPromoGrid = {
    gridView : new RealGridJS.GridView("rmsPromoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        rmsPromoGrid.initRmsPromoGrid();
        rmsPromoGrid.initDataProvider();
        rmsPromoGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initRmsPromoGrid : function() {
        rmsPromoGrid.gridView.setDataSource(rmsPromoGrid.dataProvider);
        rmsPromoGrid.gridView.setStyles(rmsPromoGridBaseInfo.realgrid.styles);
        rmsPromoGrid.gridView.setDisplayOptions(rmsPromoGridBaseInfo.realgrid.displayOptions);
        rmsPromoGrid.gridView.setColumns(rmsPromoGridBaseInfo.realgrid.columns);
        rmsPromoGrid.gridView.setOptions(rmsPromoGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(rmsPromoGrid.gridView, "eventKind", eventKindJson);
        setColumnLookupOptionForJson(rmsPromoGrid.gridView, "state", eventStateJson);
        setColumnLookupOptionForJson(rmsPromoGrid.gridView, "changeType", changeTypeJson);
        setColumnLookupOptionForJson(rmsPromoGrid.gridView, "xoutYn", xoutApplyFlagJson);
        setColumnLookupOption(rmsPromoGrid.gridView, "useYn", $("#useYn"));
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        rmsPromoGrid.dataProvider.setFields(rmsPromoGridBaseInfo.dataProvider.fields);
        rmsPromoGrid.dataProvider.setOptions(rmsPromoGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        rmsPromoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = rmsPromoGrid.dataProvider.getJsonRow(rmsPromoGrid.gridView.getCurrent().dataRow);
            rmsPromo.getRmsPromoDetail(gridRow.rpmPromoCompDetailId, gridRow.thresholdId);
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        rmsPromoGrid.dataProvider.clearRows();
        rmsPromoGrid.dataProvider.setRows(dataList);
    },
    /**
     * 조회 그리드 엑셀 다운로드
     * @returns {boolean}
     */
    excelDownload: function() {
        if(rmsPromoGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "쿠폰관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        rmsPromoGrid.gridView.exportGrid({
            type: "excel"
            , target: "local"
            , lookupDisplay: true
            , fileName: fileName + ".xlsx"
            , showProgress: true
            , progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 점포행사 할인 구간 정보 그리드
var rmsPromoIntervalGrid = {
    gridView : new RealGridJS.GridView("rmsPromoIntervalGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        rmsPromoIntervalGrid.initRmsPromoIntervalGrid();
        rmsPromoIntervalGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initRmsPromoIntervalGrid : function() {
        rmsPromoIntervalGrid.gridView.setDataSource(rmsPromoIntervalGrid.dataProvider);
        rmsPromoIntervalGrid.gridView.setStyles(rmsPromoIntervalGridBaseInfo.realgrid.styles);
        rmsPromoIntervalGrid.gridView.setDisplayOptions(rmsPromoIntervalGridBaseInfo.realgrid.displayOptions);
        rmsPromoIntervalGrid.gridView.setColumns(rmsPromoIntervalGridBaseInfo.realgrid.columns);
        rmsPromoIntervalGrid.gridView.setOptions(rmsPromoIntervalGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        rmsPromoIntervalGrid.dataProvider.setFields(rmsPromoIntervalGridBaseInfo.dataProvider.fields);
        rmsPromoIntervalGrid.dataProvider.setOptions(rmsPromoIntervalGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        rmsPromoIntervalGrid.dataProvider.clearRows();
        rmsPromoIntervalGrid.dataProvider.setRows(dataList);
    },
};

// 점포행사 적용점포 그리드
var rmsPromoStoreGrid = {
    gridView : new RealGridJS.GridView("rmsPromoStoreGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        rmsPromoStoreGrid.initRmsPromoStoreGrid();
        rmsPromoStoreGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initRmsPromoStoreGrid : function() {
        rmsPromoStoreGrid.gridView.setDataSource(rmsPromoStoreGrid.dataProvider);
        rmsPromoStoreGrid.gridView.setStyles(rmsPromoStoreGridBaseInfo.realgrid.styles);
        rmsPromoStoreGrid.gridView.setDisplayOptions(rmsPromoStoreGridBaseInfo.realgrid.displayOptions);
        rmsPromoStoreGrid.gridView.setColumns(rmsPromoStoreGridBaseInfo.realgrid.columns);
        rmsPromoStoreGrid.gridView.setOptions(rmsPromoStoreGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(rmsPromoStoreGrid.gridView, "storeType", storeTypeJson);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        rmsPromoStoreGrid.dataProvider.setFields(rmsPromoStoreGridBaseInfo.dataProvider.fields);
        rmsPromoStoreGrid.dataProvider.setOptions(rmsPromoStoreGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        rmsPromoStoreGrid.dataProvider.clearRows();
        rmsPromoStoreGrid.dataProvider.setRows(dataList);
    },
};


// 점포행사 적용점포 그리드
var rmsPromoItemGrid = {
    gridView : new RealGridJS.GridView("rmsPromoItemGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        rmsPromoItemGrid.initRmsPromoStoreGrid();
        rmsPromoItemGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initRmsPromoStoreGrid : function() {
        rmsPromoItemGrid.gridView.setDataSource(rmsPromoItemGrid.dataProvider);
        rmsPromoItemGrid.gridView.setStyles(rmsPromoItemGridBaseInfo.realgrid.styles);
        rmsPromoItemGrid.gridView.setDisplayOptions(rmsPromoItemGridBaseInfo.realgrid.displayOptions);
        rmsPromoItemGrid.gridView.setColumns(rmsPromoItemGridBaseInfo.realgrid.columns);
        rmsPromoItemGrid.gridView.setOptions(rmsPromoItemGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        rmsPromoItemGrid.dataProvider.setFields(rmsPromoItemGridBaseInfo.dataProvider.fields);
        rmsPromoItemGrid.dataProvider.setOptions(rmsPromoItemGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        rmsPromoItemGrid.dataProvider.clearRows();
        rmsPromoItemGrid.dataProvider.setRows(dataList);
    },
};


// 점포행사 적용점포 그리드
var rmsPromoDiscItemGrid = {
    gridView : new RealGridJS.GridView("rmsPromoDiscItemGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        rmsPromoDiscItemGrid.initRmsPromoStoreGrid();
        rmsPromoDiscItemGrid.initDataProvider();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initRmsPromoStoreGrid : function() {
        rmsPromoDiscItemGrid.gridView.setDataSource(rmsPromoDiscItemGrid.dataProvider);
        rmsPromoDiscItemGrid.gridView.setStyles(rmsPromoDiscItemGridBaseInfo.realgrid.styles);
        rmsPromoDiscItemGrid.gridView.setDisplayOptions(rmsPromoDiscItemGridBaseInfo.realgrid.displayOptions);
        rmsPromoDiscItemGrid.gridView.setColumns(rmsPromoDiscItemGridBaseInfo.realgrid.columns);
        rmsPromoDiscItemGrid.gridView.setOptions(rmsPromoDiscItemGridBaseInfo.realgrid.options);
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        rmsPromoDiscItemGrid.dataProvider.setFields(rmsPromoDiscItemGridBaseInfo.dataProvider.fields);
        rmsPromoDiscItemGrid.dataProvider.setOptions(rmsPromoDiscItemGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        rmsPromoDiscItemGrid.dataProvider.clearRows();
        rmsPromoDiscItemGrid.dataProvider.setRows(dataList);
    },
};