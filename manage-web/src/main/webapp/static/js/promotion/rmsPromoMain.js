/**
 * 프로모션관리 > 행사관리 > 점포행사관리 메인 js
 */
$(document).ready(function() {
    rmsPromoGrid.init();
    rmsPromoIntervalGrid.init();
    rmsPromoStoreGrid.init();
    rmsPromoItemGrid.init();
    rmsPromoDiscItemGrid.init();
    CommonAjaxBlockUI.global();
    rmsPromoSearch.init();
    rmsPromo.init();
});

var rmsPromoSearch = {
    init : function () {
        rmsPromoSearch.searchFormReset();
        rmsPromoSearch.bindingEvent();
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $("#rmsPromoSearchForm").resetForm();

        // 조회 기간 초기화
        rmsPromoSearch.initCalendarDate("-1w");
    },
    /**
     * 달력 초기화
     * @param flag
     */
    initCalendarDate : function (flag) {
        rmsPromoSearch.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        rmsPromoSearch.setDate.setStartDate(flag);
        rmsPromoSearch.setDate.setEndDate(0);

        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#schBtn").bindClick(rmsPromoSearch.searchRmsPromoList);
        $("#schResetBtn").bindClick(rmsPromoSearch.searchFormReset);
        $("#excelDownloadBtn").bindClick(rmsPromoGrid.excelDownload);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                rmsPromoSearch.searchRmsPromoList();
            }
        });
    },
    searchValidCheck : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            rmsPromoSearch.initCalendarDate("-1w");
            return false;
        }

        // 10일 이내 검색
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) > 10) {
            alert("10일 이내만 검색 가능합니다.");
            return false;
        }


        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType == "NAME" && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }

        return true;
    },
    /**
     * 점포검색 팝업
     */
    openStorePopup : function () {
        var target = "rmsStorePopup";
        var callback = "rmsPromoSearch.storePopupCallback";
        var url = "/common/popup/storePop?callback=" + callback + "&isMulti=N";

        var popWidth = "1100";
        var popHeight = "620";

        windowPopupOpen(url, target, popWidth, popHeight);
    },
    /**
     * 점포검색 팝업 콜백
     * @param res
     */
    storePopupCallback : function (res) {
        if (!$.jUtil.isEmpty(res)) {
            $("#schStoreId").val(res[0].storeId);
            $("#schStoreNm").val(res[0].storeNm);
        }
    },
    /**
     * 점포행사 정보 조회
     */
    searchRmsPromoList : function (detailId) {
        // 검색조건 validation check
        if (rmsPromoSearch.searchValidCheck()) {
            // 점포행사 정보 조회 개수 초기화
            $("#rmsPromoTotalCount").html("0");

            // 점포행사 상세 초기화
            rmsPromo.rmsPromoFormReset();

            // 조회
            var rmsPromoSearchForm = $("#rmsPromoSearchForm").serialize();

            CommonAjax.basic({
                url: "/event/getRmsPromoList.json"
                , data: rmsPromoSearchForm
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    rmsPromoGrid.setData(res);

                    $("#rmsPromoTotalCount").html($.jUtil.comma(rmsPromoGrid.dataProvider.getRowCount()));

                    if (detailId !== undefined && detailId != "") {
                        var rowIndex = rmsPromoGrid.dataProvider.searchDataRow({fields: ["rpmPromoCompDetailId"], values: [detailId]});

                        if (rowIndex > -1) {
                            rmsPromoGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                            rmsPromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    },
};

var rmsPromo = {
    selectDetailId : null,

    /**
     * 초기화
     */
    init : function() {
        rmsPromo.bindingEvent();
        rmsPromo.rmsPromoFormReset();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#setEventBtn").bindClick(rmsPromo.setRmsPromo);          // 저장
        $("#resetBtn").bindClick(rmsPromo.rmsPromoFormReset);       // 초기화
    },
    /**
     * 점포행사 상세 내용 초기화
     */
    rmsPromoFormReset : function () {
        $("#rmsPromoForm").resetForm();

        rmsPromo.selectDetailId = null;

        // span 초기화
        $("#promoDisplayId, #compDisplayId, #rpmPromoCompDetailId, #name, #eventKind, #purchaseMin, #discountInfo, #eventDt, #state, #xoutYn").html("");
        $("#intervalCount, #storeCount, #itemCount, #discItemCount").html("0");

        // 그리드 초기화
        rmsPromoIntervalGrid.dataProvider.clearRows();
        rmsPromoStoreGrid.dataProvider.clearRows();
        rmsPromoItemGrid.dataProvider.clearRows();
        rmsPromoDiscItemGrid.dataProvider.clearRows();
    },
    /**
     * 점포행사 상세 조회
     */
    getRmsPromoDetail : function (detailId, thresholdId) {
        // 점포행사 상세 초기화
        rmsPromo.rmsPromoFormReset();

        rmsPromo.selectDetailId = detailId;

        var dataParam = {"rpmPromoCompDetailId":detailId, "thresholdId":thresholdId};

        CommonAjax.basic({
            url:"/event/getRmsPromoDetail.json"
            , data : dataParam
            , method : "POST"
            , callbackFunc : function(res) {
                rmsPromo.detailCallback(res);
            }
        });
    },
    /**
     * 상세 조회 콜백 - 조회된 데이터 각 항목에 적용
     * @param res
     */
    detailCallback : function (res) {
        if (!$.jUtil.isEmpty(res)) {
            var dataRow = rmsPromoGrid.gridView.getCurrent().dataRow;
            var gridRow = rmsPromoGrid.dataProvider.getJsonRow(dataRow);
            var values = rmsPromoGrid.gridView.getDisplayValuesOfRow(dataRow);   // lookup 데이터 가져오기 위해 displayValue 값 가져옴.

            $("#promoDisplayId").html(gridRow.promoDisplayId);
            $("#compDisplayId").html(gridRow.compDisplayId);
            $("#rpmPromoCompDetailId").html(gridRow.rpmPromoCompDetailId);
            $("#name").html(gridRow.name);
            // $("#eventKind").html(gridRow.eventKind);
            $("#eventKind").html(values.eventKind);
            $("#purchaseMin").html(gridRow.purchaseMin);
            $("#discountInfo").html(values.changeType + " | " + gridRow.discount);
            $("#eventDt").html(values.startDate + " ~ " + values.endDate);
            $("#state").html(values.state);
            $("#useYn").val(gridRow.useYn);
            $("#xoutYn").html(values.xoutYn);

            if (!$.jUtil.isEmpty(res.intervalList)) {
                rmsPromoIntervalGrid.setData(res.intervalList);
                $("#intervalCount").html(res.intervalList.length);
            }

            if (!$.jUtil.isEmpty(res.storeList)) {
                rmsPromoStoreGrid.setData(res.storeList);
                $("#storeCount").html(res.storeList.length);
            }

            if (!$.jUtil.isEmpty(res.itemList)) {
                rmsPromoItemGrid.setData(res.itemList);
                $("#itemCount").html(res.itemList.length);
            }

            if (!$.jUtil.isEmpty(res.discItemList)) {
                rmsPromoDiscItemGrid.setData(res.discItemList);
                $("#discItemCount").html(res.discItemList.length);
            }
        }
    },
    /**
     * 온라인 사용여부 수정
     */
    setRmsPromo : function () {
        if (confirm("수정하시겠습니까?")) {
            // 점포행사 정보
            var rmsPromoForm = $("#rmsPromoForm").serializeObject();
            rmsPromoForm.rpmPromoCompDetailId = rmsPromo.selectDetailId;

            CommonAjax.basic({
                url: "/event/setRmsPromo.json"
                , data: JSON.stringify(rmsPromoForm)
                , method: "POST"
                , contentType: "application/json; charset=utf-8"
                , successMsg: "수정되었습니다."
                , callbackFunc: function () {
                    rmsPromoSearch.searchRmsPromoList(rmsPromo.selectDetailId);
                }
            });
        }
    }
};