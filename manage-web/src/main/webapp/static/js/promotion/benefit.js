// 쿠폰 코드 상수화

/** 쿠폰 타입 코드 (coupon_type) */
const COUPON_TYPE_CART  = "1"; // 장바구니 쿠폰
const COUPON_TYPE_SHIP  = "2"; // 배송비 쿠폰
const COUPON_TYPE_ITEM  = "3"; // 상품 쿠폰
const COUPON_TYPE_GRP  = "6"; // 그룹 상품 쿠폰
const COUPON_TYPE_IMME  = "99"; // 즉시 할인 쿠폰
const COUPON_TYPE_DUPL  = "11"; // 추가중복쿠폰

/** 쿠폰 유효기간 타입 (coupon_valid_type) */
const COUPON_VALID_TYPE_DATE  = "DATE"; // 발급일부터
const COUPON_VALID_TYPE_PERIOD  = "PERIOD"; // 기간

/** 쿠폰 할인 타입 (coupon_discount_type) */
const COUPON_DISCOUNT_TYPE_RATE = "1"; // 정률
const COUPON_DISCOUNT_TYPE_PRICE = "2"; // 정액

/** 그룹 상품 할인*/
const GRP_COUPON_DISCOUNT_TYPE_AMT = "20"; // 금액
const GRP_COUPON_DISCOUNT_TYPE_QTY = "10"; // 수량

/** 점포유형(store_type) */
const STORE_TYPE_DS = 'DS';
const STORE_TYPE_HYPER = 'HYPER';
const STORE_TYPE_EXP = 'EXP';
const STORE_TYPE_CLUB = 'CLUB';

/** 쿠폰행사목적(purpose)*/
const PURPOSE_RE_PURCHASE = 'REPURCHASE';

/** 적용범위 (coupon_apply_scope) */
const COUPON_APPLY_SCOPE_ALL = "ALL";
const COUPON_APPLY_SCOPE_SELL = "SELL";
const COUPON_APPLY_SCOPE_ITEM = "ITEM";
const COUPON_APPLY_SCOPE_CATE = "CATE";

/** 화면 영역 **/
const SEARCH_AREA = "SEARCH";
const FORM_AREA = "FORM";

/**
 * Monthpicker 설정
 */
const monthConfig = {
    dateFormat: 'yy-mm',
    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    openOnFocus: true,
    showOn: "button",
    buttonImage : '/static/images/ui-kit/ui-input-calendar-sm.png',
    buttonText: "날짜를 선택하세요",
    minDate : 1,
    onSelect: function (dataText, inst) {
    }
};

// 숫자 제한 체크
function limitNumber(data, numberType) {
    switch (numberType) {
        case "bigint":
            if(Number(data) > 99999990) {
                return false;
            }
            break;
        case "int":
            if(Number(data) > 9999990) {
                return false;
            }
            break;
        case "share":
            if(Number(data) > 100) {
                return false;
            }
            break;
        case "rate":
            if(Number(data) > 70) {
                return false;
            }
            break;
        case "long":
            if(Number(data) > 999999990) {
                return false;
            }
            break;
        case "date":
            if(Number(data) > 365) {
                return false;
            }
            break;
        case "max":
            if(Number(data) > 999990) {
                return false;
            }
        case "year":
            if(Number(data) >= 100) {
                return false;
            }
            break;
    }

    return true;
}

//숫자와 ','만 입력 가능하도록 체크
function onlyNumber(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;

    if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 || keyID == 188) {
        return;
    } else {
        return false;
    }
}

//숫자와 ','만 입력 가능하도록 체크
function removeChar(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;

    if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 || keyID == 188) {
        return;
    } else {
        event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
}

// select box 옵션 전체 제거 (선택된 옵션 제외)
function removeAllSelectOption(obj){
    $.each(obj, function() {
        $(this).find("option").not(":selected").remove();
    });
}

// select box 의 옵션으로 제거
function removeSelectOption(obj, val){
    $(obj).find("option[value='"+ val +"']").remove();
}

//
function removeSelectExceptOption(obj, val) {
    $(obj).find("option[value!='" + val + "']").remove();
}

//콤마(,)제거
function removeComma(event) {
    if(event.target.value.indexOf(",")){
        event.target.value = event.target.value.replace(/[,]/g, "");
    }
}
//하이픈(-)제거
function removeHyphen(event) {
    if(event.target.value.indexOf("-")){
        event.target.value = event.target.value.replace(/[-]/g, "");
    }
}
// 적용 대상 그리드 ROW 상태별 데이터 가져오기
function getGridStateData(gridObj, dataList) {
    var returnList = new Array();

    dataList.forEach(function(_row, index) {
        var _data = gridObj.dataProvider.getJsonRow(_row);
        returnList.push(_data.applyCd);
    });

    return returnList;
}

// 쿠폰 제외 상품 그리드 ROW 상태별 데이터 가져오기
function getExceptGridStateData(gridObj, dataList) {
    var returnList = new Array();

    dataList.forEach(function(_row, index) {
        var _data = gridObj.dataProvider.getJsonRow(_row);
        returnList.push(_data.itemNo);
    });

    return returnList;
}

// 달력 초기화
function initCalendarDate(targetObj, startId, endId, form, startFlag, endFlag) {
    let startObj= $("#"+startId);
    let endObj  = $("#"+endId);

    if (form == SEARCH_AREA) {
        targetObj.setDate = Calendar.datePickerRange(startId, endId);
        targetObj.setDate.setEndDate(endFlag);
        targetObj.setDate.setStartDate(startFlag);
    } else {
        targetObj.setDate = CalendarTime.datePickerRange(startId, endId, {timeFormat: "HH:mm"});
        targetObj.setDate.setEndDateByTimeStamp(endFlag);
        targetObj.setDate.setStartDate(startFlag);
    }

    // endObj.datepicker("option", "minDate", startObj.val());
}

/**
 * 관련카테고리 태그 추출
 * @returns {[]}
 */
function getCateList () {
    let cateCdList = new Array();

    $("[id^='cateCd_']").each(function() {
        cateCdList.push($(this).data("cate-cd"));
    });

    return cateCdList;
}

/**
 * 점포유형으로 사이트 타입 얻기
 * @param storeType
 * @returns {string}
 */
function getSiteType(storeType) {
    var siteType = "HOME";

    if (storeType == STORE_TYPE_CLUB) {
        siteType = "CLUB";
    }

    return siteType;
}

/**
 * 점포유형으로 몰타입 얻기
 * @param storeType
 * @returns {string}
 */
function getMallType(storeType) {
    var mallType = "TD";

    if (storeType == STORE_TYPE_DS) {
        mallType = "DS";
    }

    return mallType;
}