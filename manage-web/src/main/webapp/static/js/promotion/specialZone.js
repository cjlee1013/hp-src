/**
 * 사이트관리 > 전문관관리 > 전문관 전시 관리 메인 js
 */
$(document).ready(function () {
    specialZoneGrid.init();
    CommonAjaxBlockUI.global();
    specialZoneSearch.init();
    specialZone.init();
});

// 그리드
var specialZoneGrid = {
    gridView : new RealGridJS.GridView("specialZoneGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        specialZoneGrid.initSpecialZoneGrid();
        specialZoneGrid.initDataProvider();
        specialZoneGrid.event();
    },
    /**
     * 조회 그리드 설정 초기화
     */
    initSpecialZoneGrid : function() {
        specialZoneGrid.gridView.setDataSource(specialZoneGrid.dataProvider);
        specialZoneGrid.gridView.setStyles(specialZoneGridBaseInfo.realgrid.styles);
        specialZoneGrid.gridView.setDisplayOptions(specialZoneGridBaseInfo.realgrid.displayOptions);
        specialZoneGrid.gridView.setColumns(specialZoneGridBaseInfo.realgrid.columns);
        specialZoneGrid.gridView.setOptions(specialZoneGridBaseInfo.realgrid.options);

        setColumnLookupOption(specialZoneGrid.gridView, "useYn", $("#useYn"));
    },
    /**
     * 그리드 데이터 설정 초기화
     */
    initDataProvider : function() {
        specialZoneGrid.dataProvider.setFields(specialZoneGridBaseInfo.dataProvider.fields);
        specialZoneGrid.dataProvider.setOptions(specialZoneGridBaseInfo.dataProvider.options);
    },
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    event : function() {
        // 그리드 선택
        specialZoneGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = specialZoneGrid.dataProvider.getJsonRow(specialZoneGrid.gridView.getCurrent().dataRow);
            specialZone.getSpecialZoneDetail(gridRow.specialNo);
        };
    },
    /**
     * 조회된 데이터 그리드에 설정
     */
    setData : function(dataList) {
        specialZoneGrid.dataProvider.clearRows();
        specialZoneGrid.dataProvider.setRows(dataList);
    },
};

var specialZoneSearch = {
    init : function () {
        specialZoneSearch.searchFormReset();
        specialZoneSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();

        // 조회 기간 초기화
        specialZone.calendar.initCalendarDate(specialZoneSearch, "schStartDt", "schEndDt", "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(specialZoneSearch.searchSpecialZoneList);
        $("#schResetBtn").bindClick(specialZoneSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                specialZoneSearch.searchSpecialZoneList();
            }
        });
    }
    /**
     * 전문관 정보 조회
     */
    , searchSpecialZoneList : function (specialNo) {
        // 검색조건 validation check
        if (specialZone.valid.searchValid()) {
            // 전문관 정보 조회 개수 초기화
            $("#specialTotalCnt").html("0");

            // 조회
            var searchForm = $("#searchForm").serialize();

            CommonAjax.basic({
                url : "/specialZone/getSpecialZoneList.json"
                , data : searchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    specialZoneGrid.setData(res);

                    $("#specialTotalCnt").html($.jUtil.comma(specialZoneGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(specialNo)) {
                        var rowIndex = specialZoneGrid.dataProvider.searchDataRow({fields : ["specialNo"], values : [specialNo]});

                        if (rowIndex > -1) {
                            specialZoneGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            specialZoneGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

var specialZone = {
    selectSpecialNo : null
    , uploadType : 'IMG'

    /**
     * 전문관 상세 초기화
     */
    , init : function () {
        specialZone.bindingEvent();
        specialZone.specialZoneFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(specialZone.setSpecialZone);             // 저장
        $("#resetBtn").bindClick(specialZone.specialZoneFormReset);     // 초기화
        $("#promoPopBtn").bindClick(specialZone.linkPop);            // 링크 조회

        // 이미지 등록
        $("input[name='fileArr']").bindChange(specialZone.img.addImg);

        // 이미지 삭제
        $("#pcImgDelBtn, #mobileImgDelBtn").click(function () {
            if (confirm("삭제하시겠습니까?")) {
                specialZone.img.deleteImg($(this).siblings(".uploadType"));
            }
        });

        // 이미지 미리보기
        $(".uploadType").bindClick(specialZone.img.imagePreview);

        // 전문관 전시순서 숫자만 입력
        $("#priority").allowInput("keyup", ["NUM"], $(this).attr("id"));

        // 입력 문자 길이 계산
        $("#bannerNm").calcTextLength("keyup", "#bannerNmLen");
    }
    /**
     * 전문관 상세 내용 초기화
     */
    , specialZoneFormReset : function () {
        $("#specialZoneForm").resetForm();

        // 전시 기간 초기화
        specialZone.calendar.initCalendarDate(specialZoneSearch, "dispStartDt", "dispEndDt", "1d", "15d");

        // span 초기화
        $("#bannerNmLen").html("0");

        // 전문관 번호 초기화
        specialZone.selectSpecialNo = null;

        // 이미지 초기화
        specialZone.img.init();
    }
    /**
     * 링크 조회
     */
    , linkPop : function () {
        var callback = "callback=" + "specialZone.callbackLinkInfo";
        var popWidth = "1084";
        var popHeight = "570";
        var target = "promoPop";
        var uri = "/common/popup/promoPop?" + callback + "&promoType=EXH&siteType=HOME&isMulti=N&promoNo=" + $("#promoNo").val();

        windowPopupOpen(uri, target, popWidth, popHeight);
    }
    /**
     * 링크 검색 팝업 콜백 (공통팝업)
     * @param promotion
     */
    , callbackLinkInfo : function (promotion) {
        $("#promoNo").val(promotion[0].promoNo);
        $("#promoNm").val(promotion[0].promoNm);
    }
    , getSpecialZoneDetail : function (specialNo) {
        // 전문관 상세 초기화
        specialZone.specialZoneFormReset();

        specialZone.selectSpecialNo = specialNo;

        CommonAjax.basic({
            url:"/specialZone/getSpecialZoneDetail.json"
            , data : {specialNo : specialNo}
            , method : "GET"
            , callbackFunc : function (res) {
                specialZone.detailCallback(res);
            }
        });
    }
    /**
     * 전문관 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param specialInfo
     * @returns {boolean}
     */
    , detailCallback : function (specialInfo) {
        if ($.jUtil.isEmpty(specialInfo)) {
            return true;
        }

        // 기본정보
        $("#priority").val(specialInfo.priority);
        $("#dispStartDt").val(specialInfo.dispStartDt);
        $("#dispEndDt").val(specialInfo.dispEndDt);
        $("#bannerNm").val(specialInfo.bannerNm).setTextLength("#bannerNmLen");
        $("#useYn").val(specialInfo.useYn);
        $("#promoNo").val(specialInfo.promoNo);
        $("#promoNm").val(specialInfo.promoNm);

        // 배너이미지
        for (var bnIdx in specialInfo.imgList) {
            var imgData = specialInfo.imgList[bnIdx];

            specialZone.setImgDisplayData(imgData);
        }
    }
    /**
     * 전문관 상세 - 이미지 영역 데이터 채우기
     * @param imgData
     */
    , setImgDisplayData : function (imgData) {
        var obj = "";

        switch (imgData.deviceType) {
            case "PC" :
                obj = "#pcImg";
                break;
            case "MOBILE" :
                obj = "#mobileImg";
                break;
        }

        var processKey = specialZone.img.getProcessKey(imgData.deviceType);

        // 파일명, 사이즈 등은 임시로 넣음
        specialZone.img.setImg($(obj), {
            "imgUrl" : imgData.imgUrl
            , "src" : hmpImgUrl + "/provide/view?processKey=" + processKey + "\&fileId=" + imgData.imgUrl
            , "changeYn" : "N"
        });
    }
    , setSpecialZone : function () {
        if (specialZone.valid.setSpecialZoneValid()) {
            var specialZoneForm = $("#specialZoneForm").serializeObject(true);

            // 전문관 번호
            specialZoneForm.specialNo = $.jUtil.isEmpty(specialZone.selectSpecialNo) ? null : Number(specialZone.selectSpecialNo);

            // 전문관 저장 호출
            CommonAjax.basic({
                url : "/specialZone/setSpecialZone.json"
                , data : JSON.stringify(specialZoneForm)
                , method : "POST"
                , contentType: 'application/json'
                , successMsg : "저장되었습니다."
                , callbackFunc : function (res) {
                    specialZoneSearch.searchSpecialZoneList(res);
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        specialZoneSearch.searchSpecialZoneList(specialZone.selectSpecialNo);
                    }
                }
            });
        }
    }
};

specialZone.calendar = {
    initCalendarDate : function (targetObj, startId, endId, startFlag, endFlag) {
        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        targetObj.setDate = Calendar.datePickerRange(startId, endId);
        targetObj.setDate.setStartDate(startFlag);
        targetObj.setDate.setEndDate(endFlag);

        endObj.datepicker("option", "minDate", startObj.val());
    }
};

/**
 * 전문관 유효성 검사 관련
 */
specialZone.valid = {
    searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'SPECIALNO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    }
    , setSpecialZoneValid : function () {
        if ($.jUtil.isEmpty($("#bannerNm").val())) {
            return $.jUtil.alert("배너명을 입력해주세요.");
        }

        var nowDt = moment().format("YYYY-MM-DD");

        // 전시시작기간
        if (!moment($("#dispStartDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("전시시작 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#dispEndDt").val(), 'YYYY-MM-DD', true).isValid()) {
            alert("전시종료 날짜 형식이 맞지 않습니다");
            $("#issueStartDt").val(nowDt);
            return false;
        }

        if ($("#promoNo").val().length <= 0) {
            return $.jUtil.alert("기획전을 선택하세요");
        }

        // 이미지 확인
        var imgCnt = 2;

        if ($("input[name='imgList[].imgUrl']:input[value!='']").length < imgCnt) {
            return $.jUtil.alert("이미지를 등록해 주세요.");
        }

        return true;
    }
};

/**
 * 전문관 이미지 관련
 */
specialZone.img = {
    imgDiv : null

    /**
     * 이미지 초기화
     */
    , init : function () {
        $(".uploadType").each(function (){specialZone.img.setImg($(this))});
        specialZone.img.imgDiv = null;
    }
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    , setImg : function (obj, params) {
        if (!$.jUtil.isEmpty(specialZone.img.imgDiv)) {
            specialZone.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        }

        if (params) {
            obj.find(".imgUrl").val(params.imgUrl);
            obj.find(".imgUrlTag").prop("src", params.src);

            obj.parents(".imgDisplayView").show();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").hide();
        } else {
            obj.find(".imgUrl").val("");
            obj.find(".imgUrlTag").prop("src", "");

            obj.parents(".imgDisplayView").hide();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").show();
        }
    }
    /**
     * 이미지 processKey
     * @returns {string}
     */
    , getProcessKey : function (deviceType) {
        var processKey;

        switch (deviceType) {
            case "PC" :
                processKey = "SpecialZonePCBanner";
                break;
            case "MOBILE" :
                processKey = "SpecialZoneMobileBanner";
                break;
        }

        return processKey;
    }
    /**
     * 이미지 업로드
     * @param obj
     */
    , addImg : function () {
        if ($("#imgFile").get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($("#imgFile").get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var deviceType = specialZone.img.imgDiv.data("type");
        var processKey = specialZone.img.getProcessKey(deviceType);

        /**
         * @type {jQuery.fn.init|jQuery|HTMLElement}
         */
        var file = $("#imgFile");
        var params = {
            processKey : processKey,
            mode : "IMG"
        };

        var ext = $("#imgFile").get(0).files[0].name.split(".");

        CommonAjax.upload({
            file : file,
            data : params,
            callbackFunc : function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        specialZone.img.setImg(specialZone.img.imgDiv, {
                            "imgUrl" : f.fileStoreInfo.fileId
                            , "src"   : hmpImgUrl + "/provide/view?processKey=" + processKey + "\&fileId=" + f.fileStoreInfo.fileId
                            , "imgWidth" : f.fileStoreInfo.width
                            , "imgHeight" : f.fileStoreInfo.height
                            , "ext"   : ext[1]
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    }
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    , imagePreview : function (obj) {
        var imgUrl = $(obj).find("img").prop("src");
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function (obj) {
        specialZone.img.setImg(obj, null);
    }
    /**
     * 이미지 클릭처리
     * @param obj
     */
    , clickFile : function (obj) {
        specialZone.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        $("input[name=fileArr]").click();
    }
};