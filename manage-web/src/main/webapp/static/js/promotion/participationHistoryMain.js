const RETURN_CODE_SUCCESS = "SUCCESS";
const participationHistoryMain = {
    /**
     * init 이벤트
     */
    init: function(){
        $("#eventCode").focus();
        participationHistoryMain.buttonEvent();
    },

    /**
     * button 이벤트
     */
    buttonEvent: function(){
        // 조회 폼 초기화
        $("#initBtn").on('click', function(){
            participationHistoryMain.reset();
        });

        // 검색
        $("#searchBtn").on('click', function(){
            participationHistoryMain.search();
        });
    },
    /**
     * 검색 폼 초기화
     * 이벤트코드, 회원명, 회원번호
     */
    reset: function(){
        $("#eventCode").val("");
        $("#userNm, #searchUserNo").val("");
        participationHistoryGrid.dataProvider.clearRows();
    },
    /**
     * 데이터 조회
     * 1. 검색어가 없을 경우 조회 불가
     * @returns {boolean}
     */
    search: function(){
        // form 유효성 체크
        if (this.invalidSearch()) {
            return false;
        }
        const eventCode = $('#eventCode option:selected').val();
        const userNo = $("#searchUserNo").val();

        let data = {};
        data.eventCode = eventCode;
        data.userNo = userNo;

        manageAjax('/event/participation/getHistory.json',
            'post', JSON.stringify(data), function (res) {
            console.log(res);
            if (res.returnCode !== RETURN_CODE_SUCCESS) {
                alert(res.returnMessage);
            } else {
                participationHistoryGrid.setData(res.data);
                //count
                $('#gridCount').html(
                    $.jUtil.comma(participationHistoryGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 검색조건 유효성 체크
     * @returns {boolean}
     */
    invalidSearch: function(){
        const eventCode = $('#eventCode option:selected').val();
        const userNm = $("#userNm");
        const userNo = $("#searchUserNo");

        if ($.jUtil.isEmpty(eventCode)) {
            $.jUtil.alert("이벤트를 선택하세요.", 'eventCode');
            return true;
        }

        if ($.jUtil.isEmpty(userNm.val()) && $.jUtil.isEmpty(userNo.val())) {
            $.jUtil.alert("회원을 검색하여 등록할 대상을 선택해주세요.", 'userNm');
            return true;
        }
        return false;
    },
};

const participationHistoryGrid = {
    gridView : new RealGridJS.GridView("participationHistoryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function(){
        participationHistoryGrid.initGrid();
        participationHistoryGrid.initDataProvider();
        participationHistoryGrid.event();
    },
    initGrid : function(){
        participationHistoryGrid.gridView.setDataSource(participationHistoryGrid.dataProvider);
        participationHistoryGrid.gridView.setStyles(participationHistoryGridBaseInfo.realgrid.styles);
        participationHistoryGrid.gridView.setDisplayOptions(participationHistoryGridBaseInfo.realgrid.displayOptions);
        participationHistoryGrid.gridView.setColumns(participationHistoryGridBaseInfo.realgrid.columns);
        participationHistoryGrid.gridView.setOptions(participationHistoryGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        participationHistoryGrid.dataProvider.setFields(participationHistoryGridBaseInfo.dataProvider.fields);
        participationHistoryGrid.dataProvider.setOptions(participationHistoryGridBaseInfo.dataProvider.options);
    },
    event : function() {
        participationHistoryGrid.gridView.onDataCellClicked = function(gridView, index) {
        };
    },
    setData : function(dataList) {
        participationHistoryGrid.dataProvider.clearRows();
        participationHistoryGrid.dataProvider.setRows(dataList);
    },
};

/**
 * commonAjax wapping
 *
 * @param url
 * @param method
 * @param data
 * @param successCallback
 */
function manageAjax(url, method, data, successCallback, successMsg){
    let params = {};
    params.data = data;
    params.url = url;
    params.method = method;
    params.contentType = "application/json; charset=utf-8";
    params.callbackFunc = successCallback;
    params.successMsg = successMsg;

    CommonAjax.basic(params);
}

/**
 * 회원조회 팝업 callback
 * @param res Response
 */
function callbackMemberSearchPop(res) {
    $("#userNm, #searchUserNo").val("");

    const userNm = $("#userNm");
    userNm.val(res.userNm);
    userNm.focus();
    $("#searchUserNo").val(res.userNo);
}