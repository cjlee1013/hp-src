/**
 * 할인 사용 현황
 */
$(document).ready(function() {
    discountGrid.init();
    discountHistoryGrid.init();
    discountSearch.init();
    discountHistory.init();
});

var discountGrid = {
    realGrid : new RealGridJS.GridView("discountGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        discountGrid.initGrid();
        discountGrid.initDataProvider();
        discountGrid.event();
    },
    initGrid : function() {
        discountGrid.realGrid.setDataSource(discountGrid.dataProvider);

        discountGrid.realGrid.setStyles(discountHistoryDiscountGridBaseInfo.realgrid.styles);
        discountGrid.realGrid.setDisplayOptions(discountHistoryDiscountGridBaseInfo.realgrid.displayOptions);
        discountGrid.realGrid.setColumns(discountHistoryDiscountGridBaseInfo.realgrid.columns);
        discountGrid.realGrid.setOptions(discountHistoryDiscountGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(discountGrid.realGrid, "useYn", useYnJson);
        setColumnLookupOptionForJson(discountGrid.realGrid, "storeType", storeTypeJson);
        setColumnLookupOptionForJson(discountGrid.realGrid, "discountKind", discountKindJson);

        discountGrid.realGrid.setIndicator({visible:true});
        discountGrid.realGrid.setColumnProperty("discountKind", "visible", true);
        discountGrid.realGrid.setColumnProperty("regNm", "visible", false);
        discountGrid.realGrid.setColumnProperty("regDt", "visible", false);
        discountGrid.realGrid.setColumnProperty("chgNm", "visible", false);
        discountGrid.realGrid.setColumnProperty("chgDt", "visible", false);
    },
    initDataProvider : function() {
        discountGrid.dataProvider.setFields(discountHistoryDiscountGridBaseInfo.dataProvider.fields);
        discountGrid.dataProvider.setOptions(discountHistoryDiscountGridBaseInfo.dataProvider.options);
    },
    event : function() {
        discountGrid.realGrid.onDataCellClicked = function(gridView, index) {
            discountHistory.submitDiscountHistory(index.dataRow);
        };
    },
    setData : function(dataList) {
        discountGrid.dataProvider.clearRows();
        discountGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(discountGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "할인사용조회_할인정보목록_" + _date.getFullYear() + month + date;

        discountGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            lookupDisplay: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var discountHistoryGrid = {
    realGrid : new RealGridJS.GridView("discountHistoryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        discountHistoryGrid.initGrid();
        discountHistoryGrid.initDataProvider();
    },
    initGrid : function() {
        discountHistoryGrid.realGrid.setDataSource(discountHistoryGrid.dataProvider);

        /* RealGrid Header Summary 추가 */
        /*
        discountHistoryGrid.realGrid.setIndicator({footText:"합계"});
        var mergeCells = discountHistoryGridBaseInfo.realgrid.columns.map(function(v, i){return v.name}).splice(0,6).reverse();
        discountHistoryGrid.realGrid.setHeader({summary: {visible: true, mergeCells:mergeCells}});
        discountHistoryGridBaseInfo.realgrid.columns[5].header["summary"] = {
                                                                                "styles": {
                                                                                    "paddingRight": 5,
                                                                                    "textAlignment": "far",
                                                                                    "numberFormat": "#,###",
                                                                                    "suffix": "원"
                                                                                },
                                                                                "expression": "sum"
                                                                            };

        discountHistoryGridBaseInfo.realgrid.styles.indicator = {summary:{textAlignment:"center"}};
        */
        discountHistoryGrid.realGrid.setStyles(discountHistoryGridBaseInfo.realgrid.styles);
        discountHistoryGrid.realGrid.setDisplayOptions(discountHistoryGridBaseInfo.realgrid.displayOptions);
        discountHistoryGrid.realGrid.setColumns(discountHistoryGridBaseInfo.realgrid.columns);
        discountHistoryGrid.realGrid.setOptions(discountHistoryGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        discountHistoryGrid.dataProvider.setFields(discountHistoryGridBaseInfo.dataProvider.fields);
        discountHistoryGrid.dataProvider.setOptions(discountHistoryGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        discountHistoryGrid.dataProvider.clearRows();
        discountHistoryGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(discountHistoryGrid.realGrid.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var month = ((_date.getMonth() + 1) < 10) ? ('0' + (_date.getMonth() + 1)) : (_date.getMonth() + 1);
        var date = (_date.getDate() < 10) ? ('0' +_date.getDate()) : _date.getDate();
        var fileName =  "할인사용조회_사용할인목록_" + _date.getFullYear() + month + date;

        discountHistoryGrid.realGrid.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            lookupDisplay: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var discountSearch = {
    selectDiscountRowId : 0,
    schDate: null,
    init : function() {
        this.event();
        this.initDiscountSearch();
    },
    event : function () {
        $("#initDiscountSearchBtn").on("click", function(){
            discountSearch.initDiscountSearch();
        });

        $("#setTodayBtn").on("click", function() {
            discountSearch.initSearchDate("0");
        });

        $("#setOneWeekBtn").on("click", function() {
            discountSearch.initSearchDate("-1w");
        });

        $("#setOneMonthBtn").on("click", function() {
            discountSearch.initSearchDate("-1m");
        });

        $("#getDiscountSearchBtn").on("click", function(){
            discountSearch.getDiscountSearch();
        });

        //검색 엑셀 다운로드 버튼 클릭
        $('#excelDownloadDiscountListBtn').click(function() {
            discountGrid.excelDownload();
        });
    },
    // 할인 조회 검색 폼 - 초기화
    initDiscountSearch : function() {
        $("#discountSearchForm").each(function () {
            this.reset();
        });

        // 조회 일자 초기화
        discountSearch.initSearchDate("0");

        discountHistory.initDiscountHistAll();
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        discountSearch.schDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        discountSearch.schDate.setEndDate("0");
        discountSearch.schDate.setStartDate(flag);
    },
    // 내용 검증
    validateCheck : function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var returnFlag = true;

        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if(startDt == "Invalid Date" || endDt == "Invalid Date") {
            alert("날짜 형식이 맞지 않습니다");
            return false;
        }

        // 검색어 자릿수 확인
        if($("#schType").val() == "discountNm") {
            if ($("#schValue").val() != "" && $("#schValue").val().length < 2) {
                alert("2글자 이상 검색 하세요.");
                return false;
            }
        }

        return returnFlag;
    },
    // 할인 조회
    getDiscountSearch : function() {
        if (!discountSearch.validateCheck()) return;

        discountHistory.initDiscountHistAll();

        $("#discountSearchCnt").html("0");

        var discountSearchForm = $("form[name=discountSearchForm]").serialize();

        CommonAjax.basic({url:"/discount/getDiscountList.json", data: discountSearchForm, method : "POST",  callbackFunc:function(res) {
            discountGrid.setData(res);
            $("#discountSearchCnt").html(discountGrid.dataProvider.getRowCount());
        }});
    }
};

var discountHistory = {
    discountNo : null,
    schDate : null,

    init: function () {
        this.event();
        this.initDiscountHistAll();
    },
    event: function () {
        //초기화
        $("#initDiscountHistSearchBtn").on("click", function() {
            discountHistory.initDiscountHistAll();
        });

        $("#getDiscountHistSearchBtn").on("click", function() {
            discountHistory.getDiscountHist();
        });

        $("#excelDownloadDiscountHistListBtn").on("click", function() {
            discountHistoryGrid.excelDownload();
        });

        $("#schMember").on("click", function() {
            discountHistory.callMemberSearchPop($(this));
        });
    },
    initDiscountHistDate : function () {
        discountHistory.schDate = Calendar.datePickerRange("useSchStartDt", "useSchEndDt");
        discountHistory.schDate.setEndDate(0);
        discountHistory.schDate.setStartDate("0d");
    },
    initDiscountHistAll : function() {
        discountHistory.couponNo = null;

        $("#discountHistSearchForm").each(function () {
            this.reset();
        });

        discountHistory.initDiscountHistDate();
        $("#histSchValue").val("");
        $("#discountUseSearchCnt").html("0");
        $("#subDiscountNo").html("");
        $("#discountStat").html("");

        discountHistoryGrid.dataProvider.clearRows();
    },
    submitDiscountHistory : function(selectRowId) {
        discountHistory.initDiscountHistAll();

        discountSearch.selectDiscountRowId = selectRowId;

        discountHistory.discountNo = discountGrid.dataProvider.getValue(selectRowId, "discountNo");
        $("#subdiscountNo").text(discountHistory.discountNo);

        this.getDiscountHist();
    },
    getDiscountHist : function() {
        if(discountHistory.discountNo == null) {
            alert("할인을 선택하세요.");
            return false;
        }

        // 조회기간 체크
        if (moment($("#useSchEndDt").val()).diff($("#useSchStartDt").val(), "day", true) > 3) {
            alert("3일 이내만 검색 가능합니다.");
            $("#useSchStartDt").val(moment($("#useSchEndDt").val()).add(-3, "day").format("YYYY-MM-DD"));
            return false;
        }

        /*// 검색어 자릿수 확인
        if($("#histSchType").val() != "" && $.trim($("#histSchType").val()).length < 2) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }*/

        $("#discountUseSearchCnt").html("0");
        $("#subDiscountNo").html("");
        $("#discountStat").html("");

        discountHistoryGrid.dataProvider.clearRows();

        $("#subDiscountNo").text(discountHistory.discountNo);

        var params = {
            "discountNo":discountHistory.discountNo
            , "userNo":$("#userNo").val()
            , "startDt":$("#useSchStartDt").val()
            , "endDt":$("#useSchEndDt").val()
        };

        discountHistoryGrid.dataProvider.clearRows();

        //사용내역조회 리스트 10만개 제한
        var LIMIT_LIST_COUNT = 100000;

        CommonAjax.basic({
            url : "/discount/getDiscountUseInfoList.json",
            data : params,
            method : 'POST',
            callbackFunc : function(resData) {
                if ( resData == null ) {
                    if (true) {
                        alert('정상적으로 조회되지 않았습니다.');
                    }
                } else if ( resData.discountUseList.length <= 0 ) {
                    if (true) {
                        //alert('조회결과가 없습니다.');
                    }
                } else {
                    var resultCount = resData.discountUseListCount;
                    if (resultCount > LIMIT_LIST_COUNT) {
                        alert("검색결과 수가 많아, 10만건만 출력되었습니다.");
                    }

                    var discountStat = "[사용건수: "+ resData.discountCount +"] [사용금액: "+ resData.sumDiscountAmt +"]";
                     $("#discountStat").html(discountStat);

                    $("#discountUseSearchCnt").html(resData.discountCount);

                    discountHistoryGrid.dataProvider.clearRows();
                    discountHistoryGrid.setData(resData.discountUseList);
                }
            }
        });
    },
    callMemberSearchPop : function(obj) {
        var memberSearchCallback = "discountHistory.callbackMemberSearchPop";
        if ($.jUtil.isEmpty(discountHistory.discountNo)) {
            alert("조회할 할인을 선택하세요.");
            return;
        }
        memberSearchPopup(memberSearchCallback);
    },
    callbackMemberSearchPop : function(memberData) {
        $("#userNo").val(memberData.userNo);
        $("#userId").val(memberData.userId);
        $("#userNm").val(memberData.userNm);

        //discountHistory.getDiscountHist();
    }
};