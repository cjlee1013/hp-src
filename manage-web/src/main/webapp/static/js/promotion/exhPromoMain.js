/**
 * 프로모션관리 > 기획전 > 기획전 메인 js
 */
$(document).ready(function () {
    exhPromoGrid.init();
    couponGrid.init();
    themeGrid.init();
    itemGrid.init();
    CommonAjaxBlockUI.global();
    exhPromoSearch.init();
    exhPromo.init();
});

/**
 * 기획전 조회 영역
 */
var exhPromoSearch = {
    init : function () {
        exhPromoSearch.searchFormReset();
        exhPromoSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#exhPromoSearchForm").resetForm();

        // 조회 기간 초기화
        initCalendarDate(exhPromoSearch, "schStartDt", "schEndDt", SEARCH_AREA, "-1w", "0");
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(exhPromoSearch.searchExhPromoList);
        $("#schResetBtn").bindClick(exhPromoSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                exhPromoSearch.searchExhPromoList();
            }
        });
    }
    /**
     * 기획전 정보 조회
     */
    , searchExhPromoList : function (promoNo) {
        // 검색조건 validation check
        if (exhPromo.valid.searchValid()) {
            // 기획전 정보 조회 개수 초기화
            $("#exhTotalCnt").html("0");

            // 조회
            var exhPromoSearchForm = $("#exhPromoSearchForm").serializeObject();
            exhPromoSearchForm.schPromoType = $("#promoType").val();

            CommonAjax.basic({
                url : "/promotion/getPromoList.json"
                , data : exhPromoSearchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    exhPromoGrid.setData(res);

                    $("#exhTotalCnt").html($.jUtil.comma(exhPromoGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(promoNo)) {
                        var rowIndex = exhPromoGrid.dataProvider.searchDataRow({fields : ["promoNo"], values : [promoNo]});

                        if (rowIndex > -1) {
                            exhPromoGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            exhPromoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

/**
 * 기획전 정보 영역
 */
var exhPromo = {
    selectPromoNo : null
    , uploadType : 'IMG'

    /**
     * 기획전 상세 초기화
     */
    , init : function () {
        exhPromo.bindingEvent();

        commonEditor.initEditor("#pcEditor", 300, "PromotionImage");
        commonEditor.initEditor("#mobileEditor", 300, "PromotionImage");
        commonEditor.initEditor("#couponEditor", 300, "PromotionImage");

        exhPromo.exhPromoFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(exhPromo.setExhPromo);                       // 저장
        $("#resetBtn").bindClick(exhPromo.exhPromoFormReset, "");    // 초기화
        $("#itemExcelDownBtn").bindClick(exhPromo.itemExcelDown);           // 상품내역 엑셀 다운로드
        $("#itemGridSearchBtn").bindClick(exhPromo.theme.searchItemGridData);          // 상품내역 등록상품 검색

        // 사이트 구분 변경시
        $("#siteType").bindChange(exhPromo.changeSiteType);  // 사이트 구분 변경 시 초기화 호출
        $("input:checkbox[name='expYn']").bindChange(exhPromo.view.checkExpYn);

        // 선택된 관련카테고리 삭제
        $("#selectedCates").on("click", ".close", function () {
            $(this).closest(".span-button").remove();

            var getCateCnt = Number($("#getCateCnt").text()) - 1;
            $("#getCateCnt").html(getCateCnt);
        });

        // 이미지 등록
        $("input[name='fileArr']").bindChange(exhPromo.img.addImg);

        // 이미지 삭제
        $("#benefitPCImgDelBtn, #benefitMOImgDelBtn, #catePCImgDelBtn, #cateMOImgDelBtn, #searchPCImgDelBtn, #searchMOImgDelBtn, #pcImgDelBtn, #mobileImgDelBtn").click(function () {
            if (confirm("삭제하시겠습니까?")) {
                exhPromo.img.deleteImg($(this).siblings(".uploadType"));
            }
        });

        // 이미지 미리보기
        $(".uploadType").bindClick(exhPromo.img.imagePreview);

        // 상단영역별 선택 변경시
        $("input:radio[name='pcType'],input:radio[name='mobileType']").bindChange(exhPromo.view.changeTopDiv);

        // 쿠폰 사용여부 변경시
        $("input:radio[name='couponUseYn']").bindChange(exhPromo.view.changeCouponArea);

        // 쿠폰 주의사항 사용여부 변경시
        $("input:radio[name='couponCautionYn']").bindChange(exhPromo.view.changeCouponEditDiv);

        // 카테고리, 검색키워드, 혜택존 기획전 노출 우선순위 숫자만 입력
        $("#priority, #searchPriority, #mainPriority").allowInput("keyup", ["NUM"], $(this).attr("id")).bindChange(exhPromo.valid.priorityCheck, $(this).attr("id"));

        // 상품분류 등록상품 검색 숫자만 입력
        $("#itemGridSearchValue").allowInput("keyup", ["NUM"], $(this).attr("id")).bindChange(exhPromo.theme.itemNoCheck);

        // 점포유형 변경시
        $("#storeType").bindChange(exhPromo.theme.changeStoreType);

        // 입력 문자 길이 계산
        $("#mngPromoNm").calcTextLength("keyup", "#mngPromoNmLen");
        $("#dispPromoNm").calcTextLength("keyup", "#dispPromoNmLen");
        $("#appTopTitle").calcTextLength("keyup", "#topTitleLen");
        $("#searchKeyword").calcTextLength("keyup", "#keywordLen");
        $("#themeNm").calcTextLength("keyup", "#themeNmLen");
    }
    /**
     * 기획전 상세 내용 초기화
     */
    , exhPromoFormReset : function (siteType) {
        $("#exhPromoForm").resetForm();
        exhPromo.selectPromoNo = null;

        // 전시 기간 초기화
        initCalendarDate(exhPromo, "dispStartDt", "dispEndDt", FORM_AREA, "1d", "15d");

        // span 초기화
        $("#promoNo").html("");
        $("#mngPromoNmLen").html("0");
        $("#dispPromoNmLen").html("0");
        $("#topTitleLen").html("0");
        $("#getCateCnt").html("0");
        $("#keywordLen").html("0");
        $("#couponCnt").html("0");
        $("#themeCnt").html("0");
        $("#themeNmLen").html("0");

        $("#selectedCates").html("");

        // 이미지 초기화
        exhPromo.img.init();

        // 에디터 초기화
        commonEditor.setHtml("#pcEditor", "");
        commonEditor.setHtml("#mobileEditor", "");
        commonEditor.setHtml("#couponEditor", "");

        // 그리드 초기화
        couponGrid.dataProvider.clearRows();
        themeGrid.dataProvider.clearRows();

        // 상품영역 초기화 호출
        exhPromo.theme.setThemeInit("ALL");

        $("input:radio[name='pcType']:input[value='" + $("input:radio[name='pcType']:checked").val()+"']").trigger("change");
        $("input:radio[name='mobileType']:input[value='" + $("input:radio[name='mobileType']:checked").val()+"']").trigger("change");
        $("input:radio[name='couponUseYn']:input[value='" + $("input:radio[name='couponUseYn']:checked").val()+"']").trigger("change");
        $("input:radio[name='couponCautionYn']:input[value='" + $("input:radio[name='couponCautionYn']:checked").val()+"']").trigger("change");

        // 사이트구분 초기화
        exhPromo.view.addSelectOption("#siteType", "INIT");

        // 사이트 구분 변경으로 인한 초기화 시
        if (!$.jUtil.isEmpty(siteType)) {
            $("#siteType").val(siteType);
        }

        exhPromo.view.siteTypeViewCtrl();
    }
    /**
     * 상품내역 엑셀 다운로드
     */
    , itemExcelDown : function () {
        itemGrid.excelDownload(exhPromo.selectPromoNo);
    }
    /**
     * 사이트구분 변경 시
     * @param flag
     */
    , changeSiteType : function () {
        var siteType = $("#siteType").val();

        exhPromo.exhPromoFormReset(siteType);
    }
    /**
     * 관련카테고리 태그 생성
     * @param cateCd
     * @param cateNm
     */
    , makeCateTag : function (cateCd, cateNm) {
        var selectedId = "cateCd_" + cateCd;

        $("#selectedCates").append("<span id='" + selectedId + "' class='ui span-button mg-l-5 mg-b-5' data-cate-cd='" + cateCd + "'># " + cateNm + " <span class='text close'>[X]</span></span>");
    }
    /**
     * 기획전 상세 조회
     */
    , getExhPromoDetail : function (promoNo) {
        // 기획전 상세 초기화
        exhPromo.exhPromoFormReset();

        exhPromo.selectPromoNo = promoNo;

        CommonAjax.basic({
            url:"/promotion/getPromoDetail.json"
            , data : {promoNo : promoNo}
            , method : "GET"
            , callbackFunc : function (res) {
                exhPromo.detailCallback(res);
            }
        });
    }
    /**
     * 기획전 상세 조회 콜백 - 상세내용 각 항목에 적용
     * @param exhInfo
     * @returns {boolean}
     */
    , detailCallback : function (exhInfo) {
        if ($.jUtil.isEmpty(exhInfo)) {
            return true;
        }

        // 기본정보
        $("#promoNo").html(exhInfo.promoNo);
        $("#mngPromoNm").val(exhInfo.mngPromoNm).setTextLength("#mngPromoNmLen");
        $("#dispPromoNm").val(exhInfo.dispPromoNm).setTextLength("#dispPromoNmLen");
        $("#dispStartDt").val(exhInfo.dispStartDt);
        $("#dispEndDt").val(exhInfo.dispEndDt);
        $("#appTopTitle").val(exhInfo.appTopTitle).setTextLength("#topTitleLen");
        $("#priority").val(exhInfo.priority);
        $("#searchKeyword").val(exhInfo.searchKeyword).setTextLength("#keywordLen");
        $("#searchPriority").val(exhInfo.searchPriority);
        $("#mainPriority").val(exhInfo.mainPriority);

        // 기본정보 > 적용시스템, 노출영역
        $("#pcUseYn").prop("checked", exhInfo.pcUseYn == "Y");
        $("#appUseYn").prop("checked", exhInfo.appUseYn == "Y");
        $("#mwebUseYn").prop("checked", exhInfo.mwebUseYn == "Y");

        $("#dispMainYn").prop("checked", exhInfo.dispMainYn == "Y");
        $("#dispCategoryYn").prop("checked", exhInfo.dispCategoryYn == "Y");
        $("#dispSearchYn").prop("checked", exhInfo.dispSearchYn == "Y");

        // 기본정보 > 사이트구분, 전시, 사용 여부
        $("#siteType").val(exhInfo.siteType);
        $("#expYn").prop("checked", exhInfo.expYn == "Y").change();
        exhPromo.view.addSelectOption("#siteType", exhInfo.siteType);

        $("input:radio[name='dispYn']:input[value='" + exhInfo.dispYn + "']").prop("checked", true).trigger("change");
        $("input:radio[name='useYn']:input[value='" + exhInfo.useYn + "']").prop("checked", true).trigger("change");

        // 기본정보 > 관련 카테고리
        var getCateCnt = 0;
        for (var cateIdx in exhInfo.cateList) {
            exhPromo.makeCateTag(exhInfo.cateList[cateIdx].cateCd, exhInfo.cateList[cateIdx].cateNm);
            getCateCnt++;
        }

        $("#getCateCnt").html(getCateCnt);

        // 이미지
        for (var bnIdx in exhInfo.imgList) {
            var imgData = exhInfo.imgList[bnIdx];
            var deviceType = imgData.deviceType == "PC" ? imgData.deviceType : "MO";

            var obj = "";

            switch (imgData.imgType) {
                case "BENEFIT" :
                    obj = "#benefit" + deviceType + "Img";
                    break;
                case "CATE" :
                    if (exhInfo.dispCategoryYn == "Y") {
                        obj = "#cate" + deviceType + "Img";
                    }
                    break;
                case "SEARCH" :
                    if (exhInfo.dispSearchYn == "Y") {
                        obj = "#search" + deviceType + "Img";
                    }
                    break;
                case "TOP" :
                    if ((exhInfo.pcType != "EDIT" && deviceType == "PC" && exhInfo.pcUseYn == "Y") ||
                        (exhInfo.mobileType != "EDIT" && (deviceType == "MO" && (exhInfo.appUseYn == "Y" || exhInfo.mwebUseYn == "Y")))) {
                        obj = "#top" + deviceType + "Img";
                    }
                    break;
            }

            if (!$.jUtil.isEmpty(obj)) {
                var imgType = deviceType + "_" + imgData.imgType;
                var processKey = exhPromo.img.getProcessKey(imgType);

                // 파일명, 사이즈 등은 임시로 넣음
                exhPromo.img.setImg($(obj), {
                    "imgUrl" : imgData.imgUrl
                    , "imgWidth" : imgData.imgWidth
                    , "imgHeight" : imgData.imgHeight
                    , "src" : (processKey == "PromotionImage" ? hmpImgFrontUrl : hmpImgUrl) + "/provide/view?processKey=" + processKey + "\&fileId=" + imgData.imgUrl
                    , "changeYn" : "N"
                });
            }
        }
        // 상단영역
        $("input:radio[name='pcType']:input[value='" + exhInfo.pcType + "']").prop("checked", true).trigger("change");
        $("input:radio[name='mobileType']:input[value='" + exhInfo.mobileType + "']").prop("checked", true).trigger("change");

        if (exhInfo.pcType == "EDIT") {
            commonEditor.setHtml("#pcEditor", exhInfo.pcText);
        }

        if (exhInfo.mobileType == "EDIT") {
            commonEditor.setHtml("#mobileEditor", exhInfo.mobileText);
        }

        if (!$.jUtil.isEmpty(exhInfo.bgColor)) {
            $("#bgColor").val(exhInfo.bgColor);
        }

        // 쿠폰영역
        $("input:radio[name='couponUseYn']:input[value='" + exhInfo.couponUseYn + "']").prop("checked", true).trigger("change");
        $("input:radio[name='couponCautionYn']:input[value='" + exhInfo.couponCautionYn + "']").prop("checked", true).trigger("change");
        $("#couponTemplate").val(exhInfo.couponTemplate);

        if (!$.jUtil.isEmpty(exhInfo.couponList)) {
            couponGrid.setData(exhInfo.couponList);
        }

        commonEditor.setHtml("#couponEditor", exhInfo.couponCautionText);

        // 상품분류
        themeGrid.setData(exhInfo.themeList);

        // 상품분류별 상품내역
        exhPromo.theme.itemList = exhInfo.themeItemList;
    }
    /**
     * 쿠폰 수 설정
     */
    , setCouponCnt : function () {
        $("#couponCnt").html($.jUtil.comma(couponGrid.dataProvider.getRowCount()));
    }
    /**
     * 기획전 저장
     */
    , setExhPromo : function () {
        if (exhPromo.valid.setPromoValid()) {
            // var exhPromoForm = $("#exhPromoForm").serializeArray();
            var exhPromoForm = $("#exhPromoForm").serializeObject(true);

            // 이미지 유효성 검사는 여기서
            for (var imgIdx in exhPromoForm.imgList) {
                var imgInfo = exhPromoForm.imgList[imgIdx];
                // 1. 배너 이미지 확인 - 혜택존은 필수
                // 2. 상단영역 이미지 확인 -
                // 3. 나머지 배너 이미지는 노출영역 체크 확인
                if (imgInfo.imgType == "BENEFIT" && $.jUtil.isEmpty(imgInfo.imgUrl)) {
                    return $.jUtil.alert("혜택존 배너 이미지를 등록해 주세요.");
                } else if (imgInfo.imgType == "TOP") {
                    if (($("#pcUseYn").is(":checked") && $("input:radio[name='pcType']:checked").val() != "EDIT" && imgInfo.deviceType == "PC" && $.jUtil.isEmpty(imgInfo.imgUrl))) {
                        return $.jUtil.alert("상단영역 PC 이미지를 등록해 주세요.");
                    } else if ((($("#appUseYn").is(":checked") || $("#mwebUseYn").is(":checked")) && $("input:radio[name='mobileType']:checked").val() != "EDIT" && imgInfo.deviceType == "MOBILE" && $.jUtil.isEmpty(imgInfo.imgUrl))) {
                        return $.jUtil.alert("상단영역 모바일 이미지를 등록해 주세요.");
                    }
                } else {
                    if (imgInfo.imgType == "CATE" && $("#dispCategoryYn").is(":checked") && $.jUtil.isEmpty(imgInfo.imgUrl)) {
                        return $.jUtil.alert("카테고리 기획전 배너 이미지를 등록해 주세요.");
                    } else if (imgInfo.imgType == "SEARCH" && $("#dispSearchYn").is(":checked") && $.jUtil.isEmpty(imgInfo.imgUrl)) {
                        return $.jUtil.alert("검색결과 배너 이미지를 등록해 주세요.");
                    }
                }
            }

            // 기획전 번호
            exhPromoForm.promoNo = Number($("#promoNo").text());

            // 관련 카테고리
            exhPromoForm.cateList = getCateList();

            // 상단영역 에디터
            exhPromoForm.pcText      = commonEditor.getHtmlData("#pcEditor");
            exhPromoForm.mobileText  = commonEditor.getHtmlData("#mobileEditor");

            // 쿠폰
            var couponList = couponGrid.dataProvider.getJsonRows();

            var couponInfoList = new Array();
            for (var cpIdx in couponList) {
                var dataInfo = {couponNo : couponList[cpIdx].couponNo, priority : parseInt(cpIdx) + 1}
                couponInfoList.push(dataInfo);
            }

            exhPromoForm.couponList = couponInfoList;
            exhPromoForm.couponCautionText = commonEditor.getHtmlData("#couponEditor");

            // 상품분류
            var themeList = themeGrid.dataProvider.getJsonRows();
            for (var tmIdx in themeList) {
                themeList[tmIdx].priority = parseInt(tmIdx) + 1;
            }

            exhPromoForm.themeList = themeList;

            // 상품내역
            var themeItemList = new Array();
            for (var idx in exhPromo.theme.itemList) {
                var tempTheme = exhPromo.theme.itemList[idx];
                var tempItemList = tempTheme.itemList;
                var itemList = new Array();

                // 임시 분류 번호 비교하여 데이터 가져오기
                for (var tmIdx in themeList) {
                    if (themeList[tmIdx].tempThemeNo == tempTheme.tempThemeNo) {
                        for (var itemIdx in tempItemList) {
                            var dataInfo = {itemNo : tempItemList[itemIdx].itemNo, priority : parseInt(itemIdx) + 1}
                            itemList.push(dataInfo);
                        }

                        var itemInfo = {themeNo : exhPromo.theme.itemList[idx].themeNo, tempThemeNo : exhPromo.theme.itemList[idx].tempThemeNo, itemList : itemList};
                        themeItemList.push(itemInfo);
                        break;
                    }
                }
            }

            exhPromoForm.themeItemList = themeItemList;

            // 기획전 저장 호출
            CommonAjax.basic({
                url : "/promotion/setPromotion.json"
                , data : JSON.stringify(exhPromoForm)
                , method : "POST"
                , contentType: 'application/json'
                , successMsg : "기획전 등록/수정이 완료되었습니다"
                , callbackFunc : function (res) {
                    // 신규 등록시 상세 초기화
                    if ($.jUtil.isEmpty(exhPromo.selectPromoNo)) {
                        exhPromo.exhPromoFormReset();
                        exhPromoSearch.searchExhPromoList();
                    } else {
                        exhPromoSearch.searchExhPromoList(res);
                    }
                }
                , errorCallbackFunc : function (resError) {
                    alert(resError.responseJSON.returnMessage);

                    if (resError.responseJSON.returnCode == "9010") {
                        exhPromo.exhPromoFormReset();
                        exhPromoSearch.searchExhPromoList();
                    }
                }
            });
        }
    }
};

/**
 * 기획전 화면 컨트롤 관련
 */
exhPromo.view = {
    /**
     * 상단영역 유형 변경시 이미지 or 에디터 노출 제어
     * @param obj
     */
    changeTopDiv : function (obj) {
        var divType = $(obj).data("type");
        var topType = $(obj).val();

        switch (topType) {
            case "WIDE" :
                $("#pcImgDiv").show();
                $("#bgColorDiv").show();
                $("#pcEditDiv").hide();
                break;
            case "EDIT" :
                $("#" + divType + "ImgDiv").hide();
                $("#" + divType + "EditDiv").show();

                if (divType == "pc") {
                    $("#bgColorDiv").hide();
                }
                break;
            default :
                $("#" + divType + "ImgDiv").show();
                $("#" + divType + "EditDiv").hide();

                if (divType == "pc") {
                    $("#bgColorDiv").hide();
                }
                break;
        }
    }
    /**
     * 쿠폰영역 사용여부 항목 변경시 노출 제어
     * @param obj
     */
    , changeCouponArea : function (obj) {
        var useYn = $(obj).val();

        switch (useYn) {
            case "Y" :
                $("#couponAreaTr").show();
                break;
            case "N" :
                $("#couponAreaTr").hide();
                break;
        }
    }
    /**
     * 쿠폰영역 주의사항 항목 변경시 에디터 노출 제어
     * @param obj
     */
    , changeCouponEditDiv : function (obj) {
        var useYn = $(obj).val();

        switch (useYn) {
            case "Y" :
                $("#couponEditDiv").show();
                break;
            case "N" :
                $("#couponEditDiv").hide();
                break;
        }
    }
    /**
     * 사이트구분 변경 시 상품분류의 점포유형 select box 컨트롤
     * @param obj
     */
    , siteTypeViewCtrl : function (flag) {
        var siteType = $("#siteType").val();

        // home 일 경우 express용으로 사용할지 여부 체크 노출
        switch (siteType) {
            case "CLUB" :
                $("#siteTypeLb").hide();
                break;
            default :
                $("#siteTypeLb").show();
                break;
        }

        exhPromo.view.checkExpYn();
    }
    /**
     * 사이트구분이 '홈플러스' 일 경우 express용 인지 check 여부에 따라 노출영역 컨트롤
     */
    , checkExpYn : function () {
        var isChecked = $("#expYn").is(":checked");

        // 기획전 상세정보 일 경우 체크박스 숨김
        if ($.jUtil.isEmpty(exhPromo.selectPromoNo)) {
            $("#siteTypeLb").show();
            $("#expYnSpan").hide();
        } else {
            $("#siteTypeLb").hide();

            if (isChecked) {
                $("#expYnSpan").show();
            }
        }

        // express 용 체크했을 경우 혜택존, 카테고리기획전, 검색결과에 해당 기획전 노출되지 않도록 disabled 해 준다.
        if (isChecked) {
            $("#dispMainYn, #dispCategoryYn, #dispSearchYn").prop("checked", false).prop("disabled", true);
        } else {
            $("#dispMainYn, #dispCategoryYn, #dispSearchYn").prop("disabled", false);
            if ($.jUtil.isEmpty(exhPromo.selectPromoNo)) {
                $("#dispMainYn").prop("checked", true);
            }
        }

        exhPromo.view.addSelectOption("#storeType", $("#siteType").val());
    }
    /**
     * select box option 삭제 후 조건에 맞는 option 추가
     * @param obj
     * @param flag
     * @returns {boolean}
     */
    , addSelectOption : function (obj, flag) {
        var jsonData = "";
        var defaultOption = "";
        var objId = $(obj).attr("id");
        var isExpYn = $("#expYn").is(":checked");

        switch (objId) {
            case "siteType":
                jsonData = siteTypeJson;
                break;
            case "storeType":
                jsonData = storeTypeJson;
                break;
        }

        if (jsonData == "") return false;

        $(obj).find("option").remove();

        if (defaultOption != "") $(obj).prepend(defaultOption);

        $.each(jsonData, function (key, code) {
            var chkOption = 0;
            var codeCd = code.mcCd;
            var codeNm = code.mcNm;
            var codeRef1 = code.ref1;
            var codeRef2 = code.ref2;

            $(obj).find("option").each(function () {
                if ($(this).val() == codeCd) {
                    chkOption++;
                }
            });

            if (chkOption == 0) {
                switch (objId) {
                    case "storeType" :
                        if (codeRef2 == flag){
                            if ((isExpYn && codeCd == "EXP") || (!isExpYn && codeCd != "EXP")) {
                                $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                            }
                        }
                        break;
                    case "siteType" :
                        if (flag == "INIT" || codeCd == flag) {
                            $(obj).append("<option value=" + codeCd + (codeRef1 == 'df' ? ' selected' : '') + ">" + codeNm + "</option>");
                        }

                        break;
                }

            }
        });
    }
};

/**
 * 기획전 팝업 관련
 */
exhPromo.popup = {
    /**
     * 공통팝업 호출
     */
    openPopup : function (flag) {
        var url = "";
        var target = "";
        var callback = "";

        var popWidth = "1084";
        var popHeight = "555";
        var storeType = $("#storeType").val();

        switch (flag) {
            case "ITEM" :
                var mallType;

                if (storeType == "DS") {
                    mallType = "DS";
                } else {
                    mallType = "TD";
                }

                target = "exhItemPopup";
                callback = "exhPromo.popup.itemPopCallback";
                url = "/common/popup/itemPop?callback=" + callback + "&isMulti=Y&mallType=" + mallType + "&siteType=" + $("#siteType").val() + "&storeType=" + $("#storeType").val();
                popHeight = "650";
                break;
            case "CATE" :
                target = "exhCatePop";
                callback = "exhPromo.popup.catePopCallback";
                url = "/common/popup/treeCategoryPop?callback=" + callback + "&stepFlag=Middle&tsChecked=true";
                popWidth = "580";
                popHeight = "650";
                break;
            case "COUPON" :
                var couponNo = "";

                target = "exhCouponPop";
                callback = "exhPromo.popup.couponPopCallback";
                url = "/common/popup/couponPop?callback=" + callback + "&isMulti=Y&storeType=" + storeType + "&couponNo=" + couponNo;
                popHeight = "650";
                break;
            case "EXCEL" :
                target = "exhExcelPop";
                callback = "exhPromo.popup.excelPopCallback";
                url = "/promotion/popup/upload/excelReadPop?fileType=" + $("#promoType").val() + "&callback=" + callback + "&templateUrl=/static/templates/promoItem_template.xlsx";
                popWidth = "600";
                popHeight = "350";
                break;
            default:
                break;
        }

        if (url != "") {
            windowPopupOpen(url, target, popWidth, popHeight);
        } else {
            alert(alertMsg);
        }
    }
    /**
     * 카테고리 공통팝업 콜백
     *  - 카테고리태그 설정 (선택한 태그)
     * @param cateCd
     * @param cateNm
     */
    , catePopCallback : function (resDataArr) {
        var getCateCnt = Number($("#getCateCnt").text());

        for (var i in resDataArr) {
            var cateCd = resDataArr[i].categoryId;
            var cateNm = "";

            // 중분류 까지만 가져오지만 배열에서는 소/세 분류에 넣어짐. 이건 나중에 해결해 보자
            if (resDataArr[i].categoryNm != "") {
                cateNm = resDataArr[i].categoryNm;
            }

            if (resDataArr[i].categorySNm != "") {
                cateNm = resDataArr[i].categorySNm + " > " + cateNm;
            } else {
                continue;
            }

            var selectedId = "m" + cateCd;

            if ($("#" + selectedId).length == 0) {
                exhPromo.makeCateTag(cateCd, cateNm);

                getCateCnt++;

                if (getCateCnt == 30) {
                    alert("관련카테고리는 30개까지 적용 가능합니다.\n");
                    break;
                }
            }
        }

        $("#getCateCnt").html(getCateCnt);
    }
    /**
     * 상품 공통팝업 콜백
     *  - 상품번호로 상세정보 조회
     */
    , itemPopCallback : function (resDataArr) {
        var itemCnt = itemGrid.dataProvider.getRowCount();

        if (itemCnt < 300) {
            if (resDataArr.length + itemCnt > 300) {
                alert("상품내역은 300개까지 등록됩니다.");
            }

            exhPromo.popup.getItemNoList(resDataArr);
        } else {
            alert("상품내역은 300개까지 등록할 수 있습니다.");
        }
    }
    /**
     * 쿠폰 공통팝업 콜백
     */
    , couponPopCallback : function (resDataArr) {
        var couponCnt = couponGrid.dataProvider.getRowCount();

        if (couponCnt < 10) {
            if (resDataArr.length + couponCnt > 10) {
                alert("쿠폰은 10개까지 등록됩니다.");
            }

            couponGrid.appendData(resDataArr);
        } else {
            alert("쿠폰은 10개까지 등록할 수 있습니다.");
        }
    }
    /**
     * 상품 일괄등록 팝업 콜백
     * - 데이터가 있으면 기존 상품내역 삭제 후 그리드에 추가
     */
    , excelPopCallback : function (resDataArr) {
        if ($.jUtil.isEmpty(resDataArr)) {
            return $.jUtil.alert("상품정보가 없습니다.")
        } else {
            exhPromo.popup.getItemNoList(resDataArr);
        }
    }
    /**
     * 상품번호 걸러서 상품정보 조회호출
     */
    , getItemNoList : function (resDataArr) {
        var itemNoList = new Array();

        for (var idx in resDataArr) {
            itemNoList.push(resDataArr[idx].itemNo);
        }

        exhPromo.theme.getItemInfo(itemNoList);
    }
};

/**
 * 기획전 이미지 관련
 */
exhPromo.img = {
    imgDiv : null

    /**
     * 이미지 초기화
     */
    , init : function () {
        $(".uploadType").each(function (){exhPromo.img.setImg($(this))});
        exhPromo.img.imgDiv = null;
        commonEditor.inertImgurl = null;
    }
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    , setImg : function (obj, params) {
        if (!$.jUtil.isEmpty(exhPromo.img.imgDiv)) {
            exhPromo.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        }

        if (params) {
            obj.find(".imgUrl").val(params.imgUrl);
            obj.find(".imgWidth").val(params.imgWidth);
            obj.find(".imgHeight").val(params.imgHeight);
            obj.find(".changeYn").val(params.changeYn);
            obj.find(".imgUrlTag").prop("src", params.src);

            obj.parents(".imgDisplayView").show();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").hide();
        } else {
            obj.find(".imgUrl").val("");
            obj.find(".imgWidth").val("");
            obj.find(".imgHeight").val("");
            obj.find(".changeYn").val("N");
            obj.find(".imgUrlTag").prop("src", "");

            obj.parents(".imgDisplayView").hide();
            obj.parents(".imgDisplayView").siblings(".imgDisplayReg").show();
        }
    }
    /**
     * 이미지 processKey
     * @returns {string}
     */
    , getProcessKey : function (imgType) {
        var processKey;

        switch (imgType) {
            case "PC_BENEFIT" :
                processKey = "PromoBenefitPCBanner";
                break;
            case "MO_BENEFIT" :
                processKey = "PromoBenefitMobileBanner";
                break;
            case "PC_CATE" :
                processKey = "PromoCatePCBanner";
                break;
            case "MO_CATE" :
                processKey = "PromoCateMobileBanner";
                break;
            case "PC_SEARCH" :
                processKey = "PromoSearchPCBanner";
                break;
            case "MO_SEARCH" :
                processKey = "PromoSearchMobileBanner";
                break;
            case "PC_TOP" :
                processKey = "PromoPCTop";
                break;
            case "MO_TOP" :
                processKey = "PromoMobileTop";
                break;
            default :
                processKey = "PromotionImage";
                break;
        }

        return processKey;
    }
    /**
     * 이미지 업로드
     * @param obj
     */
    , addImg : function () {
        var imgType = exhPromo.img.imgDiv.data("type");

        if (imgType != "PC_TOP" && imgType != "MO_TOP" && $("#imgFile").get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($("#imgFile").get(0).files[0].size > 5242880) {
            return $.jUtil.alert("용량을 초과하였습니다.\n5MB 이하로 업로드해주세요.");
        } else if ($("#imgFile").get(0).files[0].name.length > 30) {
            return $.jUtil.alert("파일명은 30자(확장자포함) 이내로 입력 가능합니다.");
        }

        var processKey = exhPromo.img.getProcessKey(imgType);

        var file = $("#imgFile");
        var params = {
            processKey : processKey,
            mode : "IMG"
        };

        var ext = $("#imgFile").get(0).files[0].name.split(".");

        CommonAjax.upload({
            file : file,
            data : params,
            callbackFunc : function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        exhPromo.img.setImg(exhPromo.img.imgDiv, {
                            "imgUrl" : f.fileStoreInfo.fileId
                            , "imgWidth" : f.fileStoreInfo.width
                            , "imgHeight" : f.fileStoreInfo.height
                            , "src"   : (processKey == "PromotionImage" ? hmpImgFrontUrl : hmpImgUrl) + "/provide/view?processKey=" + processKey + "\&fileId=" + f.fileStoreInfo.fileId
                            , "ext"   : ext[1]
                            , "changeYn" : "Y"
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    }
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    , imagePreview : function (obj) {
        var imgUrl = $(obj).find("img").prop("src");
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    }
    /**
     * 이미지 삭제 (초기화)
     */
    , deleteImg : function (obj) {
        exhPromo.img.setImg(obj, null);
    }
    /**
     * 이미지 클릭처리
     * @param obj
     */
    , clickFile : function (obj) {
        exhPromo.img.imgDiv = obj.parents(".preview-image").find(".uploadType");
        $("input[name=fileArr]").click();
    }
};

/**
 * 기획전 상품 분류 관련
 */
exhPromo.theme = {
    itemList : {}
    , tempNo : 1
    , storeType : null

    /**
     * 상품 분류 영역 초기화
     * @param flag
     */
    , setThemeInit : function (flag) {
        if (flag == "BTN") {
            if (!confirm("입력한 상품분류 내용이 초기화됩니다. 진행하시겠습니까?")) {
                return;
            }
        }

        $("#setThemeBtn").text("추가");
        themeGrid.selectedRowId = null;

        $("#themeNo").val("");
        $("#tempThemeNo").val("");
        $("#themeNm").val("");
        $("#itemGridSearchValue").val("");
        itemGrid.dataProvider.clearRows();
        $("#itemCnt").html("0");

        if (flag == "ALL") {
            exhPromo.theme.itemList = new Array();
            exhPromo.theme.tempNo = 1;
        } else {
            $("input:radio[name='themeDispYn']:input[value='Y']").prop("checked", true);

            $("#themeArea").find("select").each(function () {
                $(this).find("option:eq(0)").prop("selected", true).trigger("change");
            });
        }

        exhPromo.theme.storeType = $("#storeType").val();

        // 상품분류 전시기간 초기화
        initCalendarDate(exhPromo.theme, "themeDispStartDt", "themeDispEndDt", FORM_AREA, "0d", "1m");
    }
    /**
     * 상품 분류 추가 / 수정
     */
    , setThemeBtn : function () {
        if (exhPromo.valid.themeValid()) {
            var themeNo = $("#themeNo").val();
            var tempThemeNo = themeGrid.selectedRowId == null ? exhPromo.theme.getTempNo() : $("#tempThemeNo").val();

            // theme grid row id 확인 - 상품분류 우선선위가 변경되면 기존 selectedRowId와 다를 수 있어서 필요
            if (themeGrid.selectedRowId != null) {
                themeGrid.getRowId(themeNo, tempThemeNo);
            }

            var values = {
                themeNo             : themeNo
                , tempThemeNo       : tempThemeNo
                , themeNm           : $("#themeNm").val()
                , themeDispYn       : $("input:radio[name='themeDispYn']:checked").val()
                , storeType         : $("#storeType").val()
                , itemCnt           : itemGrid.dataProvider.getRowCount()
                , sortKind          : $("#sortKind").val()
                , pcTemplate        : $("#pcTemplate").val()
                , mobileTemplate    : $("#mobileTemplate").val()
                , themeDispStartDt  : $("#themeDispStartDt").val()
                , themeDispEndDt    : $("#themeDispEndDt").val()
            };

            // 추가
            if (themeGrid.selectedRowId == null) {
                themeGrid.dataProvider.addRow(values);

                var idx = exhPromo.theme.itemList.length == undefined ? 0 : exhPromo.theme.itemList.length;
                exhPromo.theme.itemList[idx] = {themeNo: values.themeNo, tempThemeNo: values.tempThemeNo, itemList: itemGrid.dataProvider.getJsonRows()};

                exhPromo.theme.setThemeCnt();
            } else {
                themeGrid.dataProvider.updateRows(themeGrid.selectedRowId, [values]);

                // var tempThemeNo = $("#tempThemeNo").val();
                var itemListSize = exhPromo.theme.itemList.length;

                if (itemListSize > 0) {
                    for (var idx in exhPromo.theme.itemList) {
                        var itemTempNo = exhPromo.theme.itemList[idx].tempThemeNo;

                        if (tempThemeNo == itemTempNo) {
                            exhPromo.theme.itemList[idx] = {themeNo: values.themeNo, tempThemeNo: values.tempThemeNo, itemList: itemGrid.dataProvider.getJsonRows()};
                            break;
                        }
                    }
                } else {
                    exhPromo.theme.itemList[itemListSize] = {themeNo: values.themeNo, tempThemeNo: values.tempThemeNo, itemList: itemGrid.dataProvider.getJsonRows()};
                }
            }

            if (confirm("변경되었습니다.\n상품내역 초기화 하시겠습니까?")) {
                exhPromo.theme.setThemeInit();
            } else {
                exhPromo.theme.selectTempTheme(themeNo, tempThemeNo);
            }
        }
    }
    /**
     * 상품분류 추가/수정 후 변경된 항목 재노출
     * @param themeNo
     * @param tempThemeNo
     */
    , selectTempTheme : function (themeNo, tempThemeNo) {
        exhPromo.theme.setThemeInit();

        themeGrid.getRowId(themeNo, tempThemeNo);

        themeGrid.gridView.setCurrent({dataRow : themeGrid.selectedRowId, fieldIndex : 1});
        themeGrid.gridView.onDataCellClicked();
    }
    /**
     * 상품분류 임시 번호 가져오기
     * @returns {string}
     */
    , getTempNo : function () {
        var tempThemeNo = "TEMP_" + exhPromo.theme.tempNo++;
        return tempThemeNo;
    }
    /**
     * 상품분류 선택 시 상세 정보 설정
     * @param rowData
     */
    , getTheme : function (rowData) {
        if (rowData) {
            $("#setThemeBtn").text("수정");
            $("#themeNo").val(rowData.themeNo);
            $("#tempThemeNo").val(rowData.tempThemeNo);
            $("#themeNm").val(rowData.themeNm).setTextLength("#themeNmLen");
            $("input:radio[name='themeDispYn']:input[value='" + rowData.themeDispYn + "']").prop("checked", true);
            $("#storeType").val(rowData.storeType);
            $("#itemCnt").val(rowData.itemCnt);
            $("#sortKind").val(rowData.sortKind);
            $("#pcTemplate").val(rowData.pcTemplate);
            $("#mobileTemplate").val(rowData.mobileTemplate);

            $("#themeDispStartDt").val(rowData.themeDispStartDt);
            $("#themeDispEndDt").val(rowData.themeDispEndDt);

            for (var idx in exhPromo.theme.itemList) {
                var itemTempNo = exhPromo.theme.itemList[idx].tempThemeNo;

                if (rowData.tempThemeNo == itemTempNo) {
                    itemGrid.setData(exhPromo.theme.itemList[idx].itemList);
                    exhPromo.theme.setItemCnt();
                    break;
                }
            }

            // 점포유형 정보 저장
            exhPromo.theme.storeType = rowData.storeType;
        }
    }
    /**
     * 상품 팝업에서 선택 후 상품정보 조회
     * @param itemNoList
     */
    , getItemInfo : function (itemNoList) {
        var paramObject = {
            itemNoList : itemNoList
            , storeType : $("#storeType").val()
        };

        $.ajax({
            url : "/promotion/getItemList.json"
            , data : JSON.stringify(paramObject)
            // , data : itemNoList
            , method : "POST"
            , contentType : "application/json; charset=utf-8"
            , success     : function (res) {
                // 엑셀에 등록된 상품 순으로 정렬
                var itemInfoArr = new Array();

                for (var pri = 0; pri < itemNoList.length; pri++) {
                    var itemNo = itemNoList[pri];

                    for (var idx = 0; idx < res.length; idx++) {
                        if (itemNo == res[idx].itemNo) {
                            itemInfoArr.push(res[idx]);
                            break;
                        }
                    }
                }

                itemGrid.appendData(itemInfoArr);
            }
            , error : function (resError) {
                // SessionNotFoundException 발생 시 httpStatus(401)를 확인하여 로그인 페이지 팝업오픈
                if (resError.status == 401) {
                    alert(CommonErrorMsg.sessionErrorMsg);
                    windowPopupOpen("/login/popup", "loginPop", 400, 481);
                } else {
                    if (resError.responseJSON != null) {
                        if (resError.responseJSON.errors !== undefined) {
                            // 에러메세지
                            alert(resError.responseJSON.errors[0].detail);
                        } else if (resError.responseJSON.data !== undefined) {
                            // api 에러메세지
                            alert(resError.responseJSON.data.result);
                        } else {
                            // df 에러메세지
                            alert(CommonErrorMsg.dfErrorMsg);
                        }
                    }
                }
            }
            , fail : function () {
                alert(CommonErrorMsg.failMsg);
            }
        });
    }
    /**
     * 분류 수 설정
     */
    , setThemeCnt : function () {
        $("#themeCnt").html($.jUtil.comma(themeGrid.dataProvider.getRowCount()));
    }
    /**
     * 상품 수 설정
     */
    , setItemCnt : function () {
        $("#itemCnt").html($.jUtil.comma(itemGrid.dataProvider.getRowCount()));
    }
    , itemNoCheck : function () {
        var itemNo = $("#itemGridSearchValue").val();

        if (!$.jUtil.isAllowInput(itemNo, ["NUM"]) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", "itemGridSearchValue");
        }

        return true;
    }
    /**
     * 그리드 상품 조회
     */
    , searchItemGridData : function () {
        if (itemGrid.dataProvider.getRowCount() < 1) {
            return alert("검색 가능한 상품이 없습니다.");
        }

        var itemNo = $("#itemGridSearchValue").val();

        if (exhPromo.theme.itemNoCheck() && !$.jUtil.isEmpty(itemNo)) {
            var rowIndex = itemGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                itemGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 0});
            } else {
                return $.jUtil.alert("검색된 상품이 없습니다.", "itemGridSearchValue");
            }
        }
    }
    /**
     * 점포유형 변경 시 선택한 상품의 점포유형과 상이한지 확인
     */
    , changeStoreType : function () {
        var storeType = $("#storeType").val();
        var itemListLen = itemGrid.dataProvider.getRowCount();

        if (storeType != exhPromo.theme.storeType && itemListLen > 0) {
            if (confirm("점포유형 변경시 등록한 상품내역이 초기화 됩니다.\n변경하시겠습니까?")) {
                itemGrid.dataProvider.clearRows();
                exhPromo.theme.setItemCnt();
                exhPromo.theme.storeType = storeType;
            } else {
                $("#storeType").val(exhPromo.theme.storeType);
            }
        } else {
            exhPromo.theme.storeType = storeType;
        }

    }
};

/**
 * 기획전 유효성 검사 관련
 */
exhPromo.valid = {
    searchValid : function () {
        if (!moment($("#schStartDt").val(),'YYYY-MM-DD',true).isValid() || !moment($("#schEndDt").val(),'YYYY-MM-DD',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 6개월 단위
        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            $("#schStartDt").val(moment($("#schEndDt").val()).add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }

        if (moment($("#schEndDt").val()).diff($("#schStartDt").val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#schStartDt").val($("#schEndDt").val());
            return false;
        }

        var schType = $("#schType").val();
        var schValue = $.trim($("#schValue").val());

        if (schType != 'PROMONO' && !$.jUtil.isEmpty(schValue) && (schValue.length < 2)) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }

        return true;
    }
    , priorityCheck : function (id) {
        var priority = $(this).val();
        var maxPriority;

        if (id == "mainPriority") {
            maxPriority = 99;
        } else {
            maxPriority = 9;
        }

        if (priority < 1 || priority > maxPriority) {
            $(obj).val("");
            return $.jUtil.alert("우선순위는 1~" + maxPriority + "까지 입력가능합니다.");
        };
    }
    , themeValid : function () {
        // 30개 등록 제한
        if (themeGrid.selectedRowId == null && themeGrid.dataProvider.getRowCount() >= 30) {
            alert("상품 분류는 30개까지 등록 가능합니다.");
            return false;
        }

        // 분류명
        if ($.jUtil.isEmpty($.trim($("#themeNm").val()))) {
            return $.jUtil.alert("분류명을 입력해주세요.", "themeNm");
        }

        var nowDt = moment().format("YYYY-MM-DD HH:mm");

        // 전시기간 확인
        if (!moment($("#themeDispStartDt").val(), 'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("상품분류 전시시작일 날짜 형식이 맞지 않습니다");
            $("#themeDispStartDt").val(nowDt);
            return false;
        }

        if (!moment($("#themeDispEndDt").val(), 'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("상품분류 전시종료일 날짜 형식이 맞지 않습니다");
            $("#themeDispEndDt").val(nowDt);
            return false;
        }

        // 상품내역 확인
        if (itemGrid.dataProvider.getRowCount() == 0) {
            alert("상품이 선택되지 않았습니다.");
            return false;
        }

        return true;
    }
    , setPromoValid : function () {
        if ($.jUtil.isEmpty($("#mngPromoNm").val())) {
            return $.jUtil.alert("관리기획전명을 입력해주세요.", "mngPromoNm");
        }

        if ($.jUtil.isEmpty($("#dispPromoNm").val())) {
            return $.jUtil.alert("전시기획전명을 입력해주세요.", "dispPromoNm");
        }

        var nowDt = moment().format("YYYY-MM-DD HH:mm");

        // 전시시작기간
        if (!moment($("#dispStartDt").val(), 'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("전시기간 날짜 형식이 맞지 않습니다");
            $("#dispStartDt").val(nowDt);
            return false;
        }

        // 전시종료기간
        if (!moment($("#dispEndDt").val(), 'YYYY-MM-DD HH:mm',true).isValid()) {
            alert("전시종료 날짜 형식이 맞지 않습니다");
            $("#dispEndDt").val(nowDt);
            return false;
        }

        // 적용시스템 1개 이상 적용 확인
        if (!(($("#pcUseYn").is(":checked")) || ($("#appUseYn").is(":checked")) || ($("#mwebUseYn").is(":checked")))) {
            alert("적용시스템을 선택해주세요");
            return false;
        }

        // 쿠폰 사용여부에 따른 쿠폰 존재 확인
        if ($("input:radio[name='couponUseYn']:checked").val() == "Y" && couponGrid.dataProvider.getRowCount() <= 0) {
            return $.jUtil.alert("사용할 쿠폰을 선택해 주세요.");
        }

        // 상품분류 & 내역 존재 여부
        if (themeGrid.dataProvider.getRowCount() <= 0) {
            return $.jUtil.alert("상품분류를 등록해 주세요.");
        }

        if ($.jUtil.isEmpty(exhPromo.theme.itemList)) {
            alert("상품이 등록되지 않았습니다.");
            return false;
        }

        return true;
    }
};