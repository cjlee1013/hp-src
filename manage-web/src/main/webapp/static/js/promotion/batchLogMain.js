/**
 * 시스템관리 > 모니터링 > 배치 모니터링 메인 js
 */
$(document).ready(function () {
    ideBatchInfoGrid.init();
    batchLogGrid.init();
    batchLogSearch.init();
    batchLog.init();
    CommonAjaxBlockUI.global();
});

/**
 * 배치 로그 그리드
 */
var ideBatchInfoGrid = {
    gridView : new RealGridJS.GridView("ideBatchInfoGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        ideBatchInfoGrid.initIdeBatchInfoGrid();
        ideBatchInfoGrid.initDataProvider();
        ideBatchInfoGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initIdeBatchInfoGrid : function() {
        ideBatchInfoGrid.gridView.setDataSource(ideBatchInfoGrid.dataProvider);
        ideBatchInfoGrid.gridView.setStyles(ideBatchInfoGridBaseInfo.realgrid.styles);
        ideBatchInfoGrid.gridView.setDisplayOptions(ideBatchInfoGridBaseInfo.realgrid.displayOptions);
        ideBatchInfoGrid.gridView.setColumns(ideBatchInfoGridBaseInfo.realgrid.columns);
        ideBatchInfoGrid.gridView.setOptions(ideBatchInfoGridBaseInfo.realgrid.options);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        ideBatchInfoGrid.dataProvider.setFields(ideBatchInfoGridBaseInfo.dataProvider.fields);
        ideBatchInfoGrid.dataProvider.setOptions(ideBatchInfoGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        ideBatchInfoGrid.gridView.onDataCellClicked = function(gridView, index) {
            var gridRow = ideBatchInfoGrid.dataProvider.getJsonRow(ideBatchInfoGrid.gridView.getCurrent().dataRow);
            batchLog.getIdeBatchDetail(gridRow);
        };
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        ideBatchInfoGrid.dataProvider.clearRows();
        ideBatchInfoGrid.dataProvider.setRows(dataList);
    }
};

var batchLogGrid = {
    gridView : new RealGridJS.GridView("batchLogGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        batchLogGrid.initBatchLogGrid();
        batchLogGrid.initDataProvider();
        batchLogGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initBatchLogGrid : function() {
        batchLogGrid.gridView.setDataSource(batchLogGrid.dataProvider);
        batchLogGrid.gridView.setStyles(batchLogGridBaseInfo.realgrid.styles);
        batchLogGrid.gridView.setDisplayOptions(batchLogGridBaseInfo.realgrid.displayOptions);
        batchLogGrid.gridView.setColumns(batchLogGridBaseInfo.realgrid.columns);
        batchLogGrid.gridView.setOptions(batchLogGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(batchLogGrid.gridView, "status", batchStatusJson);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        batchLogGrid.dataProvider.setFields(batchLogGridBaseInfo.dataProvider.fields);
        batchLogGrid.dataProvider.setOptions(batchLogGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
        // 그리드 선택
        batchLogGrid.gridView.onDataCellClicked = function(gridView, index) {
        }
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        batchLogGrid.dataProvider.clearRows();
        batchLogGrid.dataProvider.setRows(dataList);
    }
};

var batchLogSearch = {
    init : function () {
        batchLogSearch.searchFormReset();
        batchLogSearch.bindingEvent();
    }
    /**
     * 검색폼 초기화
     */
    , searchFormReset : function () {
        $("#searchForm").resetForm();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#schBtn").bindClick(batchLogSearch.searchLogList);
        $("#schResetBtn").bindClick(batchLogSearch.searchFormReset);
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                batchLogSearch.searchEtcPromoList();
            }
        });
    }
    /**
     * 달력 초기화
     * @param flag
     */
    , initCalendarDate : function (flag) {
        batchLogSearch.setDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        batchLogSearch.setDate.setStartDate(flag);
        batchLogSearch.setDate.setEndDate(0);

        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    }
    , searchValid : function () {
        var schValue = $.trim($("#schValue").val());

        if (!$.jUtil.isEmpty(schValue) && schValue.length < 2) {
            alert("2글자 이상 검색 하세요.");
            return false;
        }
        return true;
    }
    /**
     * 배치 로그 조회
     */
    , searchLogList : function (seqNo) {
        // 검색조건 validation check
        if (batchLogSearch.searchValid()) {
            // 배치 로그 조회 개수 초기화
            $("#ideBatchTotalCnt").html("0");

            // 조회
            var searchForm = $("#searchForm").serialize();

            CommonAjax.basic({
                url : "/monitering/batchLog/getIdeBatchList.json"
                , data : searchForm
                , method : "GET"
                , successMsg : null
                , callbackFunc : function (res) {
                    ideBatchInfoGrid.setData(res);

                    $("#ideBatchTotalCnt").html($.jUtil.comma(ideBatchInfoGrid.dataProvider.getRowCount()));

                    if (!$.jUtil.isEmpty(seqNo)) {
                        var rowIndex = ideBatchInfoGrid.dataProvider.searchDataRow({fields : ["seqNo"], values : [seqNo]});

                        if (rowIndex > -1) {
                            ideBatchInfoGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                            ideBatchInfoGrid.gridView.onDataCellClicked();
                        }
                    }
                }
            });
        }
    }
};

/**
 *  IDE 배치 상세
 */
var batchLog = {
    init : function () {
        batchLog.batchLogFormReset();
        batchLog.bindingEvent();
    }
    /**
     * 배치모니터링 초기화
     */
    , batchLogFormReset : function () {
        $("#batchLogForm").resetForm();

        $("#seq").html("");
        $("#batchNmLen").html("0");
        $("#batchMngNmLen").html("0");
        $("#batchInfoLen").html("0");
        $("#batchLogCnt").html("0");

        // 그리드 초기화
        batchLogGrid.dataProvider.clearRows();
    }
    /**
     * 이벤트 바인딩
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(batchLog.setBatchLog);
        $("#resetBtn").bindClick(batchLog.batchLogFormReset);

        // 입력 문자 길이 계산
        $("#batchNm").calcTextLength("keyup", "#batchNmLen");
        $("#batchMngNm").calcTextLength("keyup", "#batchMngNmLen");
        $("#batchInfo").calcTextLength("keyup", "#batchInfoLen");
    }
    /**
     * IDE 배치 상세 정보 조회
     * @param seq
     */
    , getIdeBatchDetail : function (gridRow) {
        // 상세 초기화
        batchLog.batchLogFormReset();

        // 값 설정
        $("#seq").html(gridRow.seq);
        $("#batchNm").val(gridRow.batchNm);
        $("#batchMngNm").val(gridRow.batchMngNm);
        $("#batchInfo").val(gridRow.batchInfo);
        $("#batchSchedule").val(gridRow.batchSchedule);
        $("#useYn").val(gridRow.useYn);

        $("#batchLogCnt").html(0);

        CommonAjax.basic({
            url:"/monitering/batchLog/getLogList.json"
            , data : {schValue : gridRow.batchNm}
            , method : "GET"
            , callbackFunc : function (res) {
                batchLogGrid.setData(res);
                $("#batchLogCnt").html($.jUtil.comma(batchLogGrid.dataProvider.getRowCount()));
            }
        });
    }
    , setBatchLog : function () {
        var batchLogForm = $("#batchLogForm").serializeObject(true);

        // 일련번호
        if (!$.jUtil.isEmpty($("#seq").text())) {
            batchLogForm.seq = Number($("#seq").text());
        }

        CommonAjax.basic({
            url:"/monitering/batchLog/setIdeBatchInfo.json"
            , data : JSON.stringify(batchLogForm)
            , method : "POST"
            , contentType: "application/json"
            , successMsg : "배치 모니터링 정보 등록/수정이 완료되었습니다"
            , callbackFunc : function (res) {
                batchLogSearch.searchLogList(res);
            }
            , errorCallbackFunc : function (resError) {
                alert(resError.responseJSON.returnMessage);
            }
        });
    }
};

/**
 * 팝업 관련
 */
batchLog.popup = {
    /**
     * 팝업 호출
     */
    openPopup : function () {
        var batchNm = $("#batchNm").val();

        if (batchNm != "") {
            var target = "batchLogDetailPopup";
            var url = "/promotion/popup/batchLog/detailPop?batchNm=" + $("#batchNm").val();

            var popWidth = "1084";
            var popHeight = "606";
            windowPopupOpen(url, target, popWidth, popHeight);
        } else {
            alert("조회할 배치를 선택해주세요.");
        }
    }
};