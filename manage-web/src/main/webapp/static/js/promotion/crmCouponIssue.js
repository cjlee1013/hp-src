/**
 * CRM쿠폰 대량 발급
 */
$(document).ready(function() {
    crmCouponGrid.init();
    uploadListGrid.init();
    crmCouponSearch.init();
    crmCoupon.init();
});

/**
 * Real Grid 초기화
 */
var crmCouponGrid = {
    gridView : new RealGridJS.GridView("crmCouponGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        crmCouponGrid.initGrid();
        crmCouponGrid.initDataProvider();
        crmCouponGrid.event();
    },
    initGrid : function() {
        crmCouponGrid.gridView.setDataSource(crmCouponGrid.dataProvider);

        crmCouponGrid.gridView.setStyles(crmCouponGridBaseInfo.realgrid.styles);
        crmCouponGrid.gridView.setDisplayOptions(crmCouponGridBaseInfo.realgrid.displayOptions);
        crmCouponGrid.gridView.setColumns(crmCouponGridBaseInfo.realgrid.columns);
        crmCouponGrid.gridView.setOptions(crmCouponGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(crmCouponGrid.gridView, "couponType", couponTypeJson);
        setColumnLookupOptionForJson(crmCouponGrid.gridView, "status", couponStatusJson);
        setColumnLookupOptionForJson(crmCouponGrid.gridView, "departCd", dpartCdJson);
    },
    initDataProvider : function() {
        crmCouponGrid.dataProvider.setFields(crmCouponGridBaseInfo.dataProvider.fields);
        crmCouponGrid.dataProvider.setOptions(crmCouponGridBaseInfo.dataProvider.options);
    },
    event : function() {
        crmCouponGrid.gridView.onDataCellClicked = function(gridView, index) {
            crmCoupon.crmCouponIssueDetail(crmCouponGrid.dataProvider.getJsonRow(crmCouponGrid.gridView.getCurrent().dataRow));
        };
    },
    setData : function(dataList) {
        crmCouponGrid.dataProvider.clearRows();
        crmCouponGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(crmCouponGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "CRM쿠폰대량발급_쿠폰목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        crmCouponGrid.gridView.exportGrid({
            type: "excel"
            , target: "local"
            , lookupDisplay: true
            , fileName: fileName + ".xlsx"
            , showProgress: true
            , progressMessage: "엑셀 Export중입니다."
        });
    }
};


/**
 * 업로드 파일 리스트 Grid
 */
var uploadListGrid = {
    gridView : new RealGridJS.GridView("uploadListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        uploadListGrid.initGrid();
        uploadListGrid.initDataProvider();
        uploadListGrid.event();
    },
    initGrid : function() {
        uploadListGrid.gridView.setDataSource(uploadListGrid.dataProvider);

        uploadListGrid.gridView.setStyles(uploadListGridBaseInfo.realgrid.styles);
        uploadListGrid.gridView.setDisplayOptions(uploadListGridBaseInfo.realgrid.displayOptions);
        uploadListGrid.gridView.setColumns(uploadListGridBaseInfo.realgrid.columns);
        uploadListGrid.gridView.setOptions(uploadListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        uploadListGrid.dataProvider.setFields(uploadListGridBaseInfo.dataProvider.fields);
        uploadListGrid.dataProvider.setOptions(uploadListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        uploadListGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column) {
            var rowData = uploadListGrid.dataProvider.getJsonRow(itemIndex);
            var couponNo = rowData.couponNo;
            var couponFileNo = rowData.couponFileNo;

            crmCoupon.setCouponFileStop(couponNo, couponFileNo);
        };
    },
    setData : function(dataList) {
        uploadListGrid.dataProvider.clearRows();
        uploadListGrid.dataProvider.setRows(dataList);
    }
};

/**
 * CRM 쿠폰 검색
 */
var crmCouponSearch = {
    schDate : null,     // 캘린더 : 검색 폼 > 등록기간

    init : function() {
        crmCouponSearch.bindingEvent();
        crmCouponSearch.initCouponSearch();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#schBtn").bindClick(crmCouponSearch.getCouponSearch);            // 검색
        $("#schResetBtn").bindClick(crmCouponSearch.initCouponSearch);      // 초기화
        $("#excelDownloadBtn").bindClick(crmCouponGrid.excelDownload);      // 다운로드
        $("#setTodayBtn").bindClick(crmCouponSearch.initSearchDate, "0");          // 오늘
        $("#setOneWeekBtn").bindClick(crmCouponSearch.initSearchDate, "-1w");      // 일주일
        $("#setOneMonthBtn").bindClick(crmCouponSearch.initSearchDate, "-1m");     // 한달
        $("#setThreeMonthBtn").bindClick(crmCouponSearch.initSearchDate, "-3m");   // 3달
    },
    /**
     * 검색폼 초기화
     */
    initCouponSearch : function() {
        $("#crmCouponSearchForm").resetForm();

        // 조회 일자 초기화
        crmCouponSearch.initSearchDate("-1w");
        crmCoupon.crmCouponFormReset();
    },
    /**
     * 조회 달력 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        crmCouponSearch.schDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        crmCouponSearch.schDate.setEndDate(0);
        crmCouponSearch.schDate.setStartDate(flag);
    },
    /**
     * 검색 조건 검증
     * @returns {boolean}
     */
    validateCheck : function () {
        var startDt = $("#schStartDt");
        var endDt = $("#schEndDt");
        var returnFlag = true;

        // 1년 단위
        if ($.jUtil.diffDate(startDt.val(), endDt.val(), "M") > 6) {
            alert("6개월 이내만 검색 가능합니다.");

            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            returnFlag = false;
        }

        // 종료일 확인
        if ($.jUtil.diffDate(startDt.val(), endDt.val(), "D") < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");

            startDt.val(endDt.val());
            returnFlag = false;
        }

        if (startDt == "Invalid Date" || endDt == "Invalid Date") {
            alert("날짜 형식이 맞지 않습니다");
            returnFlag = false;
        }

        // 검색어 자릿수 확인
        if ($("#schType").val() == "couponNm") {
            if (!$.jUtil.isEmpty($("#schValue").val()) && $("#schValue").val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력하세요.");
                returnFlag = false;
            }
        }

        return returnFlag;
    },
    /**
     * CRM쿠폰 조회
     * @param func
     */
    getCouponSearch : function(couponNo) {
        // 조회 조건 확인
        if (!crmCouponSearch.validateCheck()) return;

        crmCoupon.crmCouponFormReset();

        $("#couponSearchCnt").html("0");

        var searchFromData = $("form[name=crmCouponSearchForm]").serialize();

        CommonAjax.basic({
            url: "/coupon/getCouponList.json"
            , data: searchFromData
            , method: "GET"
            , successMsg: null
            , callbackFunc: function (res) {
                crmCouponGrid.setData(res);

                $("#couponSearchCnt").html(crmCouponGrid.dataProvider.getRowCount());

                if (couponNo !== undefined && $.jUtil.isNotEmpty(couponNo)) {
                    var rowIndex = crmCouponGrid.dataProvider.searchDataRow({fields: ["couponNo"], values: [couponNo]});

                    if (rowIndex > -1) {
                        crmCouponGrid.gridView.setCurrent({dataRow: rowIndex, fieldIndex: 1});
                        crmCouponGrid.gridView.onDataCellClicked(rowIndex);
                    }
                }
            }
        });
    }
};

/**
 * CRM Coupon File 등록
 */
var crmCoupon = {
    /**
     * 상세 최초 호출
     */
    init : function () {
        crmCoupon.bindingEvent();
        crmCoupon.crmCouponFormReset();
    }
    /**
     * 이벤트 연결
     */
    , bindingEvent : function () {
        $("#setBtn").bindClick(crmCoupon.uploadExcelFile);              // 등록
        $("#resetBtn").bindClick(crmCoupon.crmCouponFormReset);         // 초기화
        $("#schFileBtn").bindClick(crmCoupon.openFileSearch);           // 파일 찾기
        $("#uploadFile").bindChange(crmCoupon.changeFileSearch);        // 파일 선택 후
        $("#templateDownBtn").bindClick(crmCoupon.downloadTemplate);    // 엑셀 양식 다운로드
        $("#schValue").keyup(function (e) {
            if (e.keyCode == 13) {
                crmCouponSearch.getCouponSearch();
            }
        });
    }
    /**
     * 상세 초기화
     */
    , crmCouponFormReset : function() {
        $("#crmCouponForm").resetForm();

        $("#couponNo").val("");
        $("#couponFileNo").val("");
        $("#fileUrl").val("");
        $("#couponNoSpan").html("");
        $("#uploadFileNm").html("");

        //그리드 클리어
        uploadListGrid.dataProvider.clearRows();
    },
    /**
     * 상세 조회 - 등록된 파일 조회
     * @param selectRowId
     */
    crmCouponIssueDetail : function (row) {
        crmCoupon.crmCouponFormReset();

        /*var couponNo = crmCouponGrid.dataProvider.getValue(selectRowId, "couponNo");
        var couponNoSpan = "[" + crmCouponGrid.dataProvider.getValue(selectRowId, "couponNo") + "] " + crmCouponGrid.dataProvider.getValue(selectRowId, "manageCouponNm");
        var status = crmCouponGrid.dataProvider.getValue(selectRowId, "status");*/

        var couponNo = row.couponNo;
        var couponNoSpan = "[" + row.couponNo + "] " + row.manageCouponNm;

        $("#couponNo").val(couponNo);
        $("#couponNoSpan").html(couponNoSpan);

        var buttonInfo = {
            buttonCnt: 1,
            buttonName: "cancelButton",
            buttonImg: [{
                name: "readyStatus",
                img: "/static/images/promotion/btn_crmBatchCancel.png",
                width: 70
            }],
            buttonGap: 0,
            buttonMargin: 0,
            buttonCombine: ["readyStatus"],
            value: ["standBy"],
            isValueNotOption: false
        };

        var targetColumn = "cancelProc";

        CommonAjax.basic({
            url:"/coupon/getCrmCouponFileList.json?"
            , data:{couponNo:couponNo}
            , method : "GET"
            , callbackFunc:function(res) {
                uploadListGrid.setData(res);

                //버튼생성
                $.each(res, function (_rowId, _data) {
                    if (_data.batchStatus == 'B') {
                        uploadListGrid.dataProvider.setValue(_rowId, targetColumn, "standBy");
                    }
                });

                var listCount = uploadListGrid.gridView.getItemCount();
                if (listCount > 0) {
                    setRealGridCellButton(uploadListGrid.gridView, buttonInfo, targetColumn);
                }

                //저장버튼 노출
                if (listCount >= 3) {
                    $("#setBtn").hide();
                } else {
                    $("#setBtn").show();
                }

            }
        });

        // 발급중인 상태에 대해서만 등록버튼 노출되도록
        if (status == "ISSUED") {
            $("#setCrmCouponBtn").show();
        } else {
            $("#setCrmCouponBtn").hide();
        }
    },
    /**
     * 일괄등록 버튼 선택
     */
    openFileSearch : function(){
        $("#uploadFile").trigger("click");
    },
    /**
     * 파일 선택 후
     */
    changeFileSearch : function(obj){
        $("#uploadFileNm").html("");

        var fileInfo = $(obj)[0].files[0];

        if (!fileInfo) {
            alert("파일을 선택해주세요.");
            return;
        }

        var fileExt = fileInfo.name.split(".")[1];

        if (fileExt.indexOf("xlsx") < 0 || fileExt.indexOf("xls") < 0) {
            alert("엑셀 (.xlsx/.xls) 파일만 업로드 가능합니다.");
            $("#uploadFile").val("");
            return;
        }

        if ($("#uploadFile")[0].files[0].name.length > 50){ // 확장자 길이 감안
            alert("엑셀 파일명은 최대 45자를 넘을 수 없습니다. 파일명을 다시 확인해주세요.");
            $("#uploadFile").val("");
            return;
        }

        $("#uploadFileNm").html($("#uploadFile")[0].files[0].name);
    },
    /**
     * 등록버튼 클릭시 - 엑셀 파일 업로드 먼저 실행 후 등록 저리
     * @returns {boolean}
     */
    uploadExcelFile : function(){
        var couponNo = $("#couponNo").val();

        if (couponNo == "") {
            alert("쿠폰을 선택하세요.");
            return true;
        }

        if (!$("#uploadFile").val() || !$("#uploadFileNm").html()){
            alert("파일을 선택해주세요.");
            return;
        }

        var rowId = crmCouponGrid.gridView.getCurrent().dataRow;
        var status = crmCouponGrid.dataProvider.getValue(rowId, "status");

        if (status != "ISSUED") {
            alert("쿠폰 일괄발급은 쿠폰상태가 발급중인 쿠폰만 등록 가능합니다.");
            return;
        }

        var issueLimitType = crmCouponGrid.dataProvider.getValue(rowId, "issueLimitType");
        var strMsg = issueLimitType == "UNLIMIT" ? "최종등록 파일의 회원정보로 업데이트 처리 됩니다. 등록하시겠습니까?" : "총 발급매수가 제한 된 쿠폰입니다. 첨부하신 파일로 등록하시겠습니까?";

        if (confirm(strMsg)) {
            crmCoupon.file.uploadFile();
            // crmCoupon.setCrmCoupon();
        }
    }
    /**
     * 엑셀 양식 다운로드
     * @returns {boolean}
     */
    , downloadTemplate : function () {
        var fileUrl = "/static/templates/crmCoupon_template.xlsx";

        if (fileUrl != "") {
            location.href = fileUrl;
        } else {
            alert("양식 파일이 없습니다.");
            return false;
        }
    }
    /**
     * crm쿠폰 대량발급 저장
     */
    , setCrmCoupon : function () {
        var crmCouponForm = $("#crmCouponForm").serializeObject(true);
        crmCouponForm.fileNm = $("#uploadFileNm").text();

        // crm 쿠폰 대량발급 저장
        CommonAjax.basic({
            url : "/coupon/setCrmCouponFile.json"
            , data : JSON.stringify(crmCouponForm)
            , method : "POST"
            , contentType: 'application/json'
            , successMsg : "CRM 쿠폰 대량발급 파일 등록/수정이 완료되었습니다"
            , callbackFunc : function (res) {
                if (!$.jUtil.isEmpty(res)) {
                    $("#couponFileNo").val(res);
                    crmCouponGrid.gridView.onDataCellClicked();
                }
            }
            , errorCallbackFunc : function(resError) {
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage !== undefined) {
                        alert(resError.responseJSON.returnMessage);
                    }
                }
            }
        });
    }
    /**
     * crm쿠폰 대량발급 파일 중단 상태로 변경
     */
    , setCouponFileStop : function (couponNo, couponFileNo) {
        var paramsObj = {couponNo: couponNo, couponFileNo: couponFileNo};
        CommonAjax.basic({
            url : "/coupon/setCouponFileStop.json"
            , data : JSON.stringify(paramsObj)
            , method : "POST"
            , contentType: 'application/json'
            , callbackFunc : function (res) {
                if (res < 0) {
                    alert("취소 처리가 되지 않았습니다.\n재조회 후 상태를 다시 확인하세요.");
                } else {
                    alert("취소 처리되었습니다.")
                }
                crmCouponGrid.gridView.onDataCellClicked();
            }
        });
    }
};

crmCoupon.file = {
    /**
     * 파일 업로드 클릭 이벤트
     * @param obj
     */
    clickFile : function() {
        $("input[name=fileArr]").click();
    }
    /**
     * 파일엄로드
     */
    , uploadFile : function() {
        var file = $("#uploadFile");
        var params = {
            processKey : "CrmCouponFile"
            , mode : "FILE"
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";

                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        $("#fileUrl").val(f.fileStoreInfo.fileId);
                    }
                }

                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                    return false;
                }

                // 저장 호출
                crmCoupon.setCrmCoupon();
            }
        });
    }
};