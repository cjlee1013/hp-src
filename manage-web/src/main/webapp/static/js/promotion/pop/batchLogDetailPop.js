/**
 * 배치 로그 상세 조회 팝업
 */
$(document).ready(function() {
    batchLogDetailGrid.init();
    batchLogDetail.init();
    CommonAjaxBlockUI.global();
});

var batchLogDetailGrid = {
    gridView : new RealGridJS.GridView("batchLogDetailGrid")
    , dataProvider : new RealGridJS.LocalDataProvider()

    , init : function() {
        batchLogDetailGrid.initBatchLogDetailGrid();
        batchLogDetailGrid.initDataProvider();
        batchLogDetailGrid.event();
    }
    /**
     * 조회 그리드 설정 초기화
     */
    , initBatchLogDetailGrid : function() {
        batchLogDetailGrid.gridView.setDataSource(batchLogDetailGrid.dataProvider);
        batchLogDetailGrid.gridView.setStyles(batchLogDetailGridBaseInfo.realgrid.styles);
        batchLogDetailGrid.gridView.setDisplayOptions(batchLogDetailGridBaseInfo.realgrid.displayOptions);
        batchLogDetailGrid.gridView.setColumns(batchLogDetailGridBaseInfo.realgrid.columns);
        batchLogDetailGrid.gridView.setOptions(batchLogDetailGridBaseInfo.realgrid.options);

        setColumnLookupOptionForJson(batchLogDetailGrid.gridView, "status", batchStatusJson);
    }
    /**
     * 그리드 데이터 설정 초기화
     */
    , initDataProvider : function() {
        batchLogDetailGrid.dataProvider.setFields(batchLogDetailGridBaseInfo.dataProvider.fields);
        batchLogDetailGrid.dataProvider.setOptions(batchLogDetailGridBaseInfo.dataProvider.options);
    }
    /**
     * 조회 그리드 콜백 이벤트 설정
     */
    , event : function() {
    }
    /**
     * 조회된 데이터 그리드에 설정
     */
    , setData : function(dataList) {
        batchLogDetailGrid.dataProvider.clearRows();
        batchLogDetailGrid.dataProvider.setRows(dataList);
    }
};

// 조회
var batchLogDetail = {
    /**
     * 초기화
     */
    init : function() {
        batchLogDetail.event();
        batchLogDetail.batchDetailPopFormReset();
    }
    /**
     * 이벤트 바인딩
     */
    , event : function() {
        $("#schBtn").bindClick(batchLogDetail.searchList);
        $("#schResetBtn").bindClick(batchLogDetail.promoPopFormReset);
    }
    , initCalendar : function(flag) {
        batchLogDetail.setDate = Calendar.datePicker("schStartDt");
        batchLogDetail.setDate.setDate(flag);
    }
    /**
     * 팝업 form 초기화
     */
    , batchDetailPopFormReset : function() {
        $("#batchDetailPopForm").resetForm();

        $("#batchLogDetailCnt").html("0");

        batchLogDetail.initCalendar("0");
    }
    /**
     * 조회
     */
    , searchList : function () {
        $("#batchLogDetailCnt").html("0");

        CommonAjax.basic({
            url : "/promotion/popup/batchLog/getBatchLogInfoList.json"
            , data : {schStartDt : $("#schStartDt").val(), schValue : batchNm}
            , method : "GET"
            , callbackFunc : function (res) {
                batchLogDetailGrid.setData(res);

                $("#batchLogDetailCnt").html($.jUtil.comma(batchLogDetailGrid.dataProvider.getRowCount()));
            }
        });
    }
};