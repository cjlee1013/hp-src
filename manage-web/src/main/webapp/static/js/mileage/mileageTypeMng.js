/***
 * 마일리지 조회 리스트 그리드 설정
 * @type
 */
var mileageTypeMngGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mileageTypeMngGrid.initGrid();
        mileageTypeMngGrid.initDataProvider();
        mileageTypeMngGrid.event();
    },
    initGrid : function () {
        mileageTypeMngGrid.gridView.setDataSource(mileageTypeMngGrid.dataProvider);

        mileageTypeMngGrid.gridView.setStyles(mileageListBaseInfo.realgrid.styles);
        mileageTypeMngGrid.gridView.setDisplayOptions(mileageListBaseInfo.realgrid.displayOptions);
        mileageTypeMngGrid.gridView.setColumns(mileageListBaseInfo.realgrid.columns);
        mileageTypeMngGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);
        mileageTypeMngGrid.gridView.setCheckBar({ visible: false });
        mileageTypeMngGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        mileageTypeMngGrid.dataProvider.setFields(mileageListBaseInfo.dataProvider.fields);
        mileageTypeMngGrid.dataProvider.setOptions(mileageListBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        mileageTypeMngGrid.gridView.onDataCellClicked = function(gridView, index) {
            mileageTypeMngGrid.selectRow(index.dataRow)
        };
    },
    selectRow : function(selectRowId) {
        var rowDataJson = mileageTypeMngGrid.dataProvider.getJsonRow(selectRowId);
        var searchFormData = "mileageTypeNo=" + rowDataJson.mileageTypeNo;

        CommonAjax.basic(
            {
                url:'/mileage/type/getMileageTypeDetail.json?' + searchFormData,
                data: null,
                successMsg: null,
                method: "GET",
                callbackFunc: function(res) {
                    console.log(res);
                    $("#mileageTypeNo").val(res.mileageTypeNo);
                    $("#schMileageKind").val(res.mileageKind);
                    $("#mileageTypeName").val(res.mileageTypeName);
                    $("#schStoreType").val(res.storeType);
                    $("#displayMessage").val(res.displayMessage);
                    $("#mileageTypeExplain").val(res.mileageTypeExplain);
                    $("#regStartDt").val(res.regStartDt);
                    $("#regEndDt").val(res.regEndDt);
                    $("#useStartDt").val(res.useStartDt);
                    $("#useEndDt").val(res.useEndDt);
                    $("#expireType").val(res.expireType);
                    $("#expireDayInp").val(res.expireDayInp);
                    $("#mileageCode").val(res.mileageCode);
                    $("input:radio[name=useYn]:input[value=" + res.useYn + "]").prop("checked", true);
                    mileageTypeMng.initExpireType();
                }
            });

    },
    setData : function(dataList) {
        mileageTypeMngGrid.dataProvider.clearRows();
        mileageTypeMngGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(mileageTypeMngGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지타입관리_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mileageTypeMngGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var mileageTypeMng = {
    init : function () {
        this.initRegDate('0d');
        this.initUseDate('0d');
        mileageTypeMng.initExpireType();
    },
    initRegDate : function (flag) {
        mileageTypeMng.setDate = Calendar.datePickerRange('regStartDt', 'regEndDt');
        mileageTypeMng.setDate.setEndDate(0);
        mileageTypeMng.setDate.setStartDate(flag);
        $("#regEndDt").datepicker("option", "minDate", $("#regStartDt").val());
    },
    initUseDate : function (flag) {
        mileageTypeMng.setDate = Calendar.datePickerRange('useStartDt', 'useEndDt');
        mileageTypeMng.setDate.setEndDate(0);
        mileageTypeMng.setDate.setStartDate(flag);
        $("#useEndDt").datepicker("option", "minDate", $("#useStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(mileageTypeMng.search);
        $('#saveBtn').bindClick(mileageTypeMng.save);
        $('#excelDownloadBtn').bindClick(mileageTypeMngGrid.excelDownload);
        $('#resetRegBtn').bindClick(mileageTypeMng.resetRegForm);
        $('#searchResetBtn').bindClick(mileageTypeMng.resetSearchForm);
        $('#expireType').bind('change', mileageTypeMng.initExpireType);
        $("#mileageCode").allowInput("keyup", ["NUM", "ENG_LOWER", "ENG_UPPER"], $(this).attr("id"));

        $('#expireDayInp').bind('input', function () {
            var value = $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                $(this).focus();
                return;
            }
        });
    },
    initExpireType : function () {
        if($('#expireType').val() == "TT") {
            $('#expireDayInpSpan').hide();
            $('#useDtSpan').show();
        } if($('#expireType').val() == "TR") {
            $('#expireDayInpSpan').show();
            $('#useDtSpan').hide();
        }
    },
    checkValid : function() {
        if ($.jUtil.isEmpty($("#schStoreType").val())) {
            alert("점포 유형을 선택해주세요");
            $("#schStoreType").focus();
            return false;
        }

        if ($.jUtil.isEmpty($("#mileageTypeName").val())) {
            alert("마일리지 유형을 입력해주세요.");
            $("#mileageTypeName").focus();
            return false;
        } else {
            if ($("#mileageTypeName").val().length >= 20) {
                alert("마일리지 유형은 20자내로 입력해주세요.");
                $("#mileageTypeName").focus();
                return false;
            }
        }

        if ($.jUtil.isEmpty($("#schMileageKind").val())) {
            alert("마일리지 종류를 선택해주세요");
            $("#schMileageKind").focus();
            return false;
        }

        if ($("#displayMessage").val().length >= 20) {
            alert("고객 노출 문구는 20자내로 입력해주세요.");
            $("#displayMessage").focus();
            return false;
        }

        if ($("#mileageCode").val().length >= 20) {
            alert("마일리지 코드는 20자내로 입력해주세요.");
            $("#mileageCode").focus();
            return false;
        }

        if ($("#mileageTypeExplain").val().length >= 50) {
            alert("마일리지 타입 설명은 50자내로 입력해주세요.");
            $("#mileageTypeExplain").focus();
            return false;
        }

        var schStartDt = moment($("#regStartDt").val(), 'YYYY-MM-DD', true);
        const schEndDt = moment($("#regEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("마일리지 등록일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 마일리지 등록 시작일은 종료일보다 클 수 없습니다.");
            $("#regStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        if ($("#expireType").val() == "TT") { //기간일로부터
            var useStartDt = moment($("#useStartDt").val(), 'YYYY-MM-DD', true);
            var useEndDt = moment($("#useEndDt").val(), 'YYYY-MM-DD', true);

            if (!useStartDt.isValid() || !useEndDt.isValid()) {
                alert("마일리지 등록일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if (useEndDt.diff(useStartDt, "day", true) < 0) {
                alert("조회기간의 마일리지 등록 시작일은 종료일보다 클 수 없습니다.");
                $("#useStartDt").val(useEndDt.format("YYYY-MM-DD"));
                return false;
            }
        } else {
            if ($("#expireDayInp").val().length <= 0) {
                alert("마일리지 유효기간을 입력해주세요.");
                $("#expireDayInp").focus();
                return false;
            }
        }
        return true;
    },
    search : function () {
        var searchFormData = $("form[name=searchForm]").serialize() + "&schDetailCode="+ $("#schDetailCode").val();

        CommonAjax.basic(
            {
                url:'/mileage/type/getMileageTypeList.json?' + searchFormData,
                data: null,
                successMsg: null,
                method: "GET",
                callbackFunc: function(res) {
                    mileageTypeMngGrid.setData(res);
                    $('#mileageListTotalCount').html($.jUtil.comma(mileageTypeMngGrid.gridView.getItemCount()));
                }
            });
    },
    save : function () {
        if(!mileageTypeMng.checkValid()) {
            return false;
        }
        var confirmMsg = ($("#mileageTypeNo").val() == "")? "등록하시겠습니까?" : "수정하시겠습니까?";
        var resultMsg = ($("#mileageTypeNo").val() == "")? "등록하였습니다." : "수정하였습니다.";
        if(!confirm(confirmMsg)) return false;

        var _url;
        if($.jUtil.isEmpty($("#mileageTypeNo").val())){
            _url = '/mileage/type/addMileageType.json';
        } else {
            _url = '/mileage/type/modifyMileageType.json';
        }

        CommonAjax.basic(
            {
                url: _url,
                data: _F.getFormDataByJson($("#registerForm")),
                contentType: "application/json; charset=utf-8",
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    if("0000,SUCCESS".indexOf(res.returnCode) > -1){
                        alert(resultMsg);
                        mileageTypeMng.resetRegForm();
                        mileageTypeMng.search();
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
    },
    resetRegForm : function () {
        $("#registerForm").each(function () {
            this.reset();
            mileageTypeMng.init();
            $('#mileageTypeNo').val('');
        });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
        });
    }
};