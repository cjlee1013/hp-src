/***
 * 마일리지 지급/회수관리 그리드 설정
 */
var mileageReqListGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        mileageReqListGrid.initGrid();
        mileageReqListGrid.initDataProvider();
        mileageReqListGrid.event();
    },
    initGrid : function () {
        mileageReqListGrid.gridView.setDataSource(mileageReqListGrid.dataProvider);
        mileageReqListGrid.gridView.setStyles(mileageListBaseInfo.realgrid.styles);
        mileageReqListGrid.gridView.setDisplayOptions(mileageListBaseInfo.realgrid.displayOptions);
        mileageReqListGrid.gridView.setColumns(mileageListBaseInfo.realgrid.columns);
        mileageReqListGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);
    },
    initDataProvider : function () {
        mileageReqListGrid.dataProvider.setFields(mileageListBaseInfo.dataProvider.fields);
        mileageReqListGrid.dataProvider.setOptions(mileageListBaseInfo.dataProvider.options);
    },
    event : function () {
        // 그리드 선택
        mileageReqListGrid.gridView.onDataCellClicked = function(gridView, index) {
            mileageReqListGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        mileageReqListGrid.dataProvider.clearRows();
        mileageReqListGrid.dataProvider.setRows(dataList);
    },
    selectRow : function (selectRowId) {
        mileageDetailGrid.clearRow();
        mileageDetailGrid.deleteData = new Array();
        var rowDataJson = mileageReqListGrid.dataProvider.getJsonRow(selectRowId);
        $('#mileageReqNo').val(rowDataJson.mileageReqNo);
        $('#completeDt').val(rowDataJson.completeDt);
        $("#requestStatus").val(rowDataJson.requestStatus);

        mileageReqListGrid.setBasicData();
        mileageReqListGrid.setDetailData();
        mileageReqListGrid.setGridEdit(rowDataJson.requestStatus);

    },
    setGridEdit  : function (_status) {
        if (_status == "대기") {
            //그리드 에디터화
            mileageDetailGrid.gridView.setEditOptions({
                editable      : true,
                appendable    : true,
                insertable    : true,
                updatable     : true,
                deletable     : true,
                hideDeletedRows: true,
                readOnly      : false
            });
            $("#addSingle, #addBulk, #deleteAll").attr("disabled", false);
        } else {
            mileageDetailGrid.gridView.setEditOptions({
                editable      : false,
                appendable    : false,
                insertable    : false,
                updatable     : false,
                deletable     : false,
                softDeleting  : false,
                hideDeletedRows: false,
                readOnly      : true
            });
            $("#addSingle, #addBulk, #deleteAll").attr("disabled", true);
        }
    },
    setBasicData : function () {
        $("#directlyYn").removeAttr("disabled");
        var selectRowData = "mileageReqNo="+ $('#mileageReqNo').val();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestBasic.json?'+ selectRowData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    $("#mileageTypeNo").val(res.mileageTypeNo);
                    $("#mileageTypeName").val(res.mileageTypeName);
                    $("#requestTypeSelect").val("03,04,05".indexOf(res.requestType) > -1 ? "02" : res.requestType);
                    $("#requestType").val(res.requestType);
                    $("#processDt").val(res.processDt);
                    $("#requestReason").val(res.requestReason);
                    $("#displayMessage").val(res.displayMessage);
                    $("#useYn").val(res.useYn);
                    $("#requestMessage").val(res.requestMessage);
                    mileageReqList.setRequestBtn();
                    mileageReqList.setRequestTypeName();
                    $("#requestTypeSelect").attr("disabled", "disabled");
                    if(res.directlyYn === 'Y'){
                        $("#mileageTypeName, #useYn, #mileageTypeName, #searchMileageTypeBtn").attr("disabled", "disabled");
                    }
                    mileageDetailGrid.clearFlag = false;
                }
            });
    },
    setDetailData : function () {
        mileageDetailGrid.dataProvider.clearRows();
        var selectRowData = "mileageReqNo="+ $('#mileageReqNo').val();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestDetail.json?'+ selectRowData,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    $("#regUserNo").val(res.userNo);
                    $("#mileageDetailStatus").val(res.mileageDetailStatus);
                    $("#detailMessage").val(res.detailMessage);
                    $("#mileageReqDetailNo").val(res.mileageReqDetailNo);
                    mileageDetailGrid.setData(res);
                }
            });
    },
    excelDownload : function () {
        if(mileageReqListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마일리지지급회수내역_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        mileageReqListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 마일리지 지급/회수 대상 회원 그리드
 */
var mileageDetailGrid = {
    gridView : new RealGridJS.GridView("mileageDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    deleteData : new Array(),
    clearFlag : false,
    init : function () {
        mileageDetailGrid.initGrid();
        mileageDetailGrid.initDataProvider();
        mileageDetailGrid.event();
    },
    initGrid : function () {
        mileageDetailGrid.gridView.setDataSource(mileageDetailGrid.dataProvider);
        mileageDetailGrid.gridView.setStyles(mileageDetailBaseInfo.realgrid.styles);
        mileageDetailGrid.gridView.setDisplayOptions(mileageDetailBaseInfo.realgrid.displayOptions);
        mileageDetailGrid.gridView.setOptions(mileageDetailBaseInfo.realgrid.options);
        mileageDetailGrid.gridView.setIndicator({ visible: true });

        //그리드 에디터화
        mileageDetailGrid.gridView.setEditOptions({
            editable      : true,
            appendable    : true,
            insertable    : true,
            updatable     : true,
            deletable     : true,
            softDeleting  : false,
            hideDeletedRows: true,
            readOnly      : false
        });

        //MILEAGE_REQUEST_DETAIL_STATUS
        var values = ["대기"];
        var gridColumns = [
            {
                name: "maskingUserNo",
                fieldName: "maskingUserNo",
                readOnly : true,
                dataType: "text",
                header: {
                    text: "회원번호"
                }
            },
            /*{
                name: "userNm",
                fieldName: "userNm",
                readOnly : true,
                dataType: "text",
                header: {
                    text: "회원이름"
                }
            },*/
            {
                name: "mileageAmt",
                fieldName: "mileageAmt",
                dataType: "number",
                header: {
                    text: "금액"
                }
            },
            {
                name: "mileageDetailStatus",
                fieldName: "mileageDetailStatus",
                editor:{
                    type: "dropDown",
                    dropDownCount: 4,
                    dropDownPosition: "button",
                    partialMatch: "true"
                },
                values : values,
                labels : values,
                readOnly : true,
                header: {
                    text: "상태"
                },
                defaultValue: values
            },
            {
                name: "detailMessage",
                fieldName: "detailMessage",
                dataType: "text",
                readOnly : true,
                header: {
                    text: "비고"
                }
            },
            {
                name: "userNo",
                fieldName: "userNo",
                readOnly : true,
                visible: false,
                dataType: "text",
                header: {
                    text: "회원번호"
                }
            },
        ]

        mileageDetailGrid.gridView.setColumns(gridColumns); //edit columns

        //복사시 콤마 제거
        mileageDetailGrid.gridView.setPasteOptions({ numberChars: ","});


        mileageDetailGrid.dataProvider.setOptions({
            softDeleting: false,
        });
    },
    initDataProvider : function () {
        mileageDetailGrid.dataProvider.setFields(mileageDetailBaseInfo.dataProvider.fields);
        mileageDetailGrid.dataProvider.setOptions(mileageDetailBaseInfo.dataProvider.options);
    },
    event : function () {
        //줄번호 선택
        mileageDetailGrid.gridView.onIndicatorCellDblClicked =  function (gridView, index) {
            mileageDetailGrid.gridView.cancel();
            if ($("#requestStatus").val() != "대기") return false;

            if (!confirm('선택한 행을 삭제하시겠습니까?')) return false;
            var selectedRow = mileageDetailGrid.dataProvider.getJsonRow(index);
            selectedRow.useYn = 'N';
            mileageDetailGrid.deleteData.push(selectedRow);
            mileageDetailGrid.dataProvider.removeRow(index); //행 삭제
        };
        mileageDetailGrid.gridView.onValidateColumn = function(grid, column, inserting, value) {
            var error = {};
            if (column.fieldName === "mileageAmt") {
                if (value <= 0) {
                    alert("0보다 큰 금액을 입력해주세요.");
                } else if (value > 10000000) {
                    alert("10,000,000보다 작은 금액을 입력해주세요.");
                }
            }
            return error;
        }
    },
    setData : function(data) {
        mileageDetailGrid.dataProvider.clearRows();
        mileageDetailGrid.dataProvider.setRows(data);
    },
    clearRow : function () {
        if($("#mileageReqNo").val() != "" && $("#requestStatus").val() == "대기") {
            mileageDetailGrid.clearFlag = true;
            var _data = mileageDetailGrid.dataProvider.getJsonRows();
            for(var idx in _data) {
                _data[idx].useYn = "N";
                delete _data[idx].maskingUserNo;
                mileageDetailGrid.deleteData.push(_data[idx]);
            }
        }

        mileageDetailGrid.gridView.cancel();
        mileageDetailGrid.dataProvider.clearRows();
        $('#uploadFileNm, #uploadFile').val("");
    },
    excelDownload : function () {
        if(mileageDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var today = new Date();
        var fileName = "마일리지대상회원_"+ moment(today, 'YYYY-MM-DD', true).format("YYYYMMDD");

        mileageDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            allColumns : false,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            indicator: "hidden"
        });
    },
};
var mileageReqList = {
    init : function () {
        this.initSearchDate('0d');
        this.initProcessDate();
        this.initPage();
    },
    initPage : function () {
        $('#schRequestType').val('01');
        $('#mileageKind').val('03');

        $("#directlyYn").removeAttr("disabled");
    },
    initSearchDate : function (flag) {
        mileageReqList.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        mileageReqList.setDate.setEndDate(0);
        mileageReqList.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initProcessDate : function () {
        mileageReqList.setDate = Calendar.datePickerRange('processDt', 'processDt');
        mileageReqList.setDate.setEndDate(0);
        mileageReqList.setDate.setStartDate(0);
        $("#processDt").datepicker("option", "minDate", $("#processDt").val());
    },
    bindingEvent : function () {
        $('#searchBtn').bindClick(mileageReqList.search);
        $('#searchResetBtn').bindClick(mileageReqList.resetSearchForm);
        $('#excelDownloadBtn').bindClick(mileageReqListGrid.excelDownload);
        $('#saveBtn').bindClick(mileageReqList.save);
        $('#resetRegBtn').bindClick(mileageReqList.resetRegForm);
        $('#searchMileageTypeBtn').bindClick(mileageReqList.searchMileageTypePop);

        $('#addBulk').bindClick(mileageReqList.uploadMileageRequestExcelPop);
        $('#deleteAll').bindClick(mileageDetailGrid.clearRow);
        $('#directlyYn').bindClick(mileageReqList.checkDirectExec);
        $('#userExcelDownloadBtn').bindClick(mileageDetailGrid.excelDownload);
        $('#requestTypeSelect').bindChange(mileageReqList.setRequestTypeName);
    },
    setRequestTypeName : function () {
        if ($('#requestTypeSelect').val() == "02") {
            $("td[name=requestReasonTd]").attr('colspan',4);
            $("th[name=requestTypeTd]").hide();
            $("td[name=requestTypeTd]").hide();
        } else {
            $("td[name=requestReasonTd]").removeAttr("colspan");
            $("th[name=requestTypeTd]").show();
            $("td[name=requestTypeTd]").show();
        }
    },
    search : function () {
        if($("#schDetailCode").val() == 'MILEAGE_REQ_NO' && $('#schDetailDesc').val() != "" && !$.jUtil.isAllowInput($('#schDetailDesc').val(), ['NUM'])){
            alert('번호 검색 시 숫자만 입력이 가능합니다. ');
            $("#schDetailDesc").focus();
            return false;
        }

        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "day", true) > 0) {
            alert("조회기간은 1일단위로만 가능합니다.");
            $("#schEndDt").val(schStartDt.format("YYYY-MM-DD"));
            return false;
        }
        // if (schEndDt.diff(schStartDt, "month", true) > 6) {
        //     alert("조회기간은 최대 6개월 범위까지만 가능합니다.");
        //     $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
        //     return false;
        // }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        CommonAjaxBlockUI.startBlockUI();
        var searchFormData = $("form[name=searchForm]").serialize();
        CommonAjax.basic(
            {
                url:'/mileage/payment/getMileageRequestList.json?'+ searchFormData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    mileageReqListGrid.setData(res);
                    $('#mileageListTotalCount').html($.jUtil.comma(mileageReqListGrid.gridView.getItemCount()));
                    CommonAjaxBlockUI.stopBlockUI();
                },
                error: function() {
                    alert('오류가 발생하였습니다.');
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    checkDirectExec: function () {
        if($("#directlyYn").is(":checked")) {
            $("#directlyYn").val("Y");
            $("#processDt").prop("disabled",true).attr("readonly",true);
        } else {
            $("#directlyYn").val("N");
            $("#processDt").removeAttr("disabled").removeAttr("readonly");
        }
    },
    checkValid : function () {
        var processDate = moment($("#processDt").val(), 'YYYY-MM-DD', true);
        if(!processDate.isValid()){
            alert("실행일자가 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            $("#processDt").focus();
            return false;
        }

        if ($.jUtil.isEmpty($("#requestReason").val())) {
            alert("사유를 입력해주세요.");
            $("#requestReason").focus();
            return false;
        } else {
            if ($("#requestReason").val().length > 30) {
                alert("사유는 30자내로 입력해주세요.");
                $("#requestReason").focus();
                return false;
            }
        }

        if ($("#requestMessage").val().length >= 200) {
            alert("상세 사유는 200자내로 입력해주세요.");
            $("#requestMessage").focus();
            return false;
        }

        if ($("#requestType").val()=="01" && $.jUtil.isEmpty($("#mileageTypeNo").val())) { //지급일때만
            alert("마일리지 유형을 선택해주세요.");
            mileageReqList.searchMileageTypePop();
            return false;
        }

        mileageDetailGrid.gridView.commit(true);
        /*if (mileageDetailGrid.gridView.getItemCount() == 0) {
            alert("대상 회원을 입력해주세요.");
            return false;
        }*/

        // if($("#mileageReqNo").val() == "" && $("#directlyYn").is(":checked") && mileageDetailGrid.gridView.getItemCount() > 1000) {
        //     alert("즉시실행은 등록건수가 1천건 이하만 가능합니다. 즉시실행을 체크가 해지됩니다.");
        //     $("#directlyYn").prop("checked", false).attr("disabled",true);
        //     return false;
        // }
        return true;
    },
    addUserInfo : function (res) {
        var values = { "maskingUserNo": res.userNo, "userNo" : res.userNo, "mileageAmt": 0, "mileageDetailStatus": "대기" };
        mileageDetailGrid.gridView.cancel();
        mileageDetailGrid.dataProvider.addRow(values);
    },
    /**
     * 등록/수정데이터 생성
     */
    setEditGridData : function (_dataProvider, _data) {
        var mileageReqDetailList = [];
        var returnData;
        var mileageReqNo = $("#mileageReqNo").val();

        var indexCompare = [RealGridJS.RowState.NONE];
        for(var idx in _data) {
            var state = _dataProvider.getRowState(idx);
            if(_data[idx].mileageAmt == null || _data[idx].mileageAmt == "" || _data[idx].mileageAmt <= 0) {
                return false;
            }

            _data[idx].useYn = "Y";
            if (mileageReqNo == "") {
                delete _data[idx].mileageReqNo;
                //delete _data[idx].mileageDetailStatus;

                if (_data[idx].detailMessage == "" || _data[idx].detailMessage == null) {
                    //등록
                    mileageReqDetailList.push(_data[idx]);
                }
            } else {
                _data[idx].mileageReqNo = mileageReqNo;
                if (RealGridJS.RowState.CREATED == state
                    || (RealGridJS.RowState.NONE == state && $('#uploadFileNm').val() != "" && $('#uploadFileNm').val() != null) ) {
                    _data[idx].useYn = "Y";
                    mileageReqDetailList.push(_data[idx]);
                } else if (RealGridJS.RowState.UPDATED == state) {
                    return "0";
                }

            }
            delete _data[idx].mileageReqDetailNo;
            delete _data[idx].maskingUserNo;
            delete _data[idx].detailMessage;
            //delete _data[idx].regId;
        }

        if (mileageReqNo !="" && mileageDetailGrid.deleteData.length > 0) {
            for(var idx in mileageDetailGrid.deleteData) {
                mileageReqDetailList.push(mileageDetailGrid.deleteData[idx]);
            }
        }

        if ( mileageReqNo != "") {
            returnData = {
                "mileageReqNo": mileageReqNo,
                "mileageRequestDto" : {
                    "mileageTypeNo": $("#mileageTypeNo").val(),
                    "requestType": $("#requestType").val(),
                    "requestReason": $("#requestReason").val(),
                    "requestMessage": $("#requestMessage").val(),
                    "processDt": $("#processDt").val(),
                    "useYn": $("#useYn").val(),
                    "directlyYn": $("#directlyYn").val()
                }, "mileageReqDetailList": mileageReqDetailList };
        } else {
            returnData = {
                "mileageRequestDto" : {
                    "mileageTypeNo": $("#mileageTypeNo").val(),
                    "requestType": $("#requestTypeSelect").val(),
                    "requestReason": $("#requestReason").val(),
                    "requestMessage": $("#requestMessage").val(),
                    "processDt": $("#processDt").val(),
                    "useYn": $("#useYn").val(),
                    "directlyYn": $("#directlyYn").val()
                }, "mileageReqDetailList": mileageReqDetailList };
        }

        return returnData;
    },
    save : function () {
        if (!mileageReqList.checkValid()) return false;
        //데이터 생성
        var _data = mileageReqList.setEditGridData(mileageDetailGrid.dataProvider, mileageDetailGrid.dataProvider.getJsonRows(0, -1));
        if (!_data) {
            alert("마일리지 금액을 확인해주세요.");
            return false;
        } else if (_data == null || _data == "") {
            return false;
        } else if (_data === "0") {
            alert('이미 등록된 금액은 수정이 불가합니다.');
            mileageReqListGrid.setDetailData();
            return false;
        }

        //console.log(JSON.stringify(_data));
        var confirmMsg = ($("#mileageReqNo").val() == "")? "등록하시겠습니까?" : "수정하시겠습니까?";
        if(!confirm(confirmMsg)) return false;
        ($("#mileageReqNo").val() == "")? mileageReqList.add(_data) : mileageReqList.modify(_data);
        mileageDetailGrid.clearFlag = true;

    },
    addExcelData : function (_data) {
        CommonAjaxBlockUI.startBlockUI();
        $("#directlyYn").removeAttr("disabled");
        mileageDetailGrid.gridView.cancel();
        mileageDetailGrid.dataProvider.fillJsonData(JSON.stringify(_data), {fillMode: 'append'});
        mileageDetailGrid.gridView.setDataSource(mileageDetailGrid.dataProvider);
        CommonAjaxBlockUI.stopBlockUI();
    },
    add : function (_data) {
        CommonAjaxBlockUI.startBlockUI();
        CommonAjax.basic(
            {
                url:'/mileage/payment/setMileageRequest.json',
                data: JSON.stringify(_data),
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    alert('등록되었습니다.');
                    CommonAjaxBlockUI.stopBlockUI();
                    mileageDetailGrid.deleteData = new Array();
                    mileageReqList.resetRegForm();
                    mileageReqList.search();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    CommonAjaxBlockUI.stopBlockUI();
                    alert('오류가 발생하였습니다.');
                    console.log(thrownError);
                }
            });
    },
    modify : function (_data) {
        CommonAjaxBlockUI.startBlockUI();
        CommonAjax.basic(
            {
                url:'/mileage/payment/modifyMileageRequest.json',
                data: JSON.stringify(_data),
                contentType: 'application/json',
                method: "POST",
                timeout: 3000000,
                successMsg: null,
                success: function(res) {
                    CommonAjaxBlockUI.stopBlockUI();
                },
                callbackFunc: function(res) {
                    alert('수정되었습니다.');
                    CommonAjaxBlockUI.stopBlockUI();
                    mileageDetailGrid.deleteData = new Array();
                    mileageReqList.resetRegForm();
                    mileageReqList.search();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    CommonAjaxBlockUI.stopBlockUI();
                    alert('오류가 발생하였습니다.');
                    console.log(thrownError);
                }
            });
    },
    setRequestBtn: function () {
        if($("#directlyYn").val()=="Y") {
            $("#directlyYn").prop("checked",true);
            $("#processDt").prop("disabled",true).attr("readonly",true);
        } else {
            $("#directlyYn").prop("checked",false);
            $("#processDt").removeAttr("disabled").removeAttr("readonly");
        }
    },
    resetRegForm : function () {
        $("form[name=registerForm]").each(function () {
            this.reset();
            mileageReqList.initProcessDate('0d');
            mileageReqList.checkDirectExec();
            mileageReqList.setRequestTypeName();
        });
        $('#requestType, #uploadFileNm, #uploadFile, #mileageReqNo').val('');
        $("#requestTypeSelect, #mileageTypeName, #useYn, #mileageTypeName, #searchMileageTypeBtn, #addSingle, #addBulk, #deleteAll, #directlyYn").prop("disabled",false);
        mileageDetailGrid.gridView.cancel();
        mileageDetailGrid.init();
        mileageDetailGrid.deleteData = new Array();
        mileageDetailGrid.clearFlag = false;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            mileageReqList.init();
            mileageReqListGrid.init();
            mileageReqList.resetRegForm();
        });
    },
    searchMileageTypePop : function () {
        var url = '/mileage/popup/mileageTypeNamePop?mileageTypeNo=' + $("#mileageTypeNo").val() +"&mileageTypeName="+ $("#mileageTypeName").val();
        mileageReqList.windowOpen(url, 600, 430, 'mileageTypePop');
    },
    uploadMileageRequestExcelPop : function () {
        var url = '/mileage/popup/mileageRequestExcelPop';
        mileageReqList.windowOpen(url, 633, 255, 'mileageExcelUploadPop');
    },
    windowOpen  : function (url, _width, _height, _target) {
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);
        if(_target == 'undefined' || _target == null || _target == '') {
            _target ='popup-search';
        }
        void(window.open(url, _target, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top ));
    },
};