/***
 * 마일리지 통계 그리드 설정
 */
var dailyMileageSumGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyMileageSumGrid.initGrid();
        dailyMileageSumGrid.initDataProvider();
        dailyMileageSumGrid.event();
    },
    initGrid : function () {
        dailyMileageSumGrid.gridView.setDataSource(dailyMileageSumGrid.dataProvider);
        dailyMileageSumGrid.gridView.setStyles(mileageListBaseInfo.realgrid.styles);
        dailyMileageSumGrid.gridView.setDisplayOptions(mileageListBaseInfo.realgrid.displayOptions);
        dailyMileageSumGrid.gridView.setColumns(mileageListBaseInfo.realgrid.columns);
        dailyMileageSumGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);

        //컬럼 그룹핑 너비 조정
        dailyMileageSumGrid.gridView.setColumnProperty("적립 (+)", "displayWidth", 160);
        dailyMileageSumGrid.gridView.setColumnProperty("차감 (-)", "displayWidth", 160);
    },
    initDataProvider : function() {
        dailyMileageSumGrid.dataProvider.setFields(mileageListBaseInfo.dataProvider.fields);
        dailyMileageSumGrid.dataProvider.setOptions(mileageListBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        dailyMileageSumGrid.dataProvider.clearRows();
        dailyMileageSumGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailyMileageSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = "마일리지통계_"+ moment(new Date(), 'YYYY-MM-DD', true).format("YYYYMMDD");
        dailyMileageSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 마일리지 타입별 통계 그리드 설정
 */
var dailyMileageDetailGrid = {
    gridView : new RealGridJS.GridView("mileageDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyMileageDetailGrid.initGrid();
        dailyMileageDetailGrid.initDataProvider();
    },
    initGrid : function () {
        dailyMileageDetailGrid.gridView.setDataSource(dailyMileageDetailGrid.dataProvider);
        dailyMileageDetailGrid.gridView.setStyles(mileageDetailBaseInfo.realgrid.styles);
        dailyMileageDetailGrid.gridView.setDisplayOptions(mileageDetailBaseInfo.realgrid.displayOptions);
        dailyMileageDetailGrid.gridView.setColumns(mileageDetailBaseInfo.realgrid.columns);
        dailyMileageDetailGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);

        //컬럼 그룹핑 너비 조정
        dailyMileageDetailGrid.gridView.setColumnProperty("적립 (+)", "displayWidth", 100);
        dailyMileageDetailGrid.gridView.setColumnProperty("차감 (-)", "displayWidth", 100);
    },
    initDataProvider : function() {
        dailyMileageDetailGrid.dataProvider.setFields(mileageDetailBaseInfo.dataProvider.fields);
        dailyMileageDetailGrid.dataProvider.setOptions(mileageDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dailyMileageDetailGrid.dataProvider.clearRows();
        dailyMileageDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailyMileageDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = "마일리지타입별통계_"+ moment(new Date(), 'YYYY-MM-DD', true).format("YYYYMMDD");
        dailyMileageDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

var dailyMileageSum = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
    },
    initSearchDate : function (flag) {
        dailyMileageSum.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailyMileageSum.setDate.setEndDate(0);
        dailyMileageSum.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailyMileageSum.search);
        $('#searchResetBtn').bindClick(dailyMileageSum.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailyMileageSumGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(dailyMileageDetailGrid.excelDownload);
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val() +'_'+$("#endYear").val() + $("#endMonth").val();
        }
        return excelFileName;
    },
    checkValid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                SettleCommon.setMonth(year,'endMonth');
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                SettleCommon.setMonth(year,'endMonth');
                $("#endMonth").val(month);
                return false;
            }

        }
        return true;
    },
    search : function () {
        if(!dailyMileageSum.checkValid()) return false;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = $("form[name=searchForm]").serialize();

        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYY-MM-DD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYY-MM-DD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;
        CommonAjax.basic({
            url:'/mileage/stat/getDailyMileageSummary.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                dailyMileageSumGrid.setData(res);
            }
        });

        CommonAjax.basic({
            url:'/mileage/stat/getDailyMileageTypeSummary.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                dailyMileageDetailGrid.setData(res);
            }
        });

        CommonAjaxBlockUI.stopBlockUI();

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            dailyMileageSum.init();
        });
    }
};