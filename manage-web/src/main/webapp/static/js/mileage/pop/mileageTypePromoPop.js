var mileageTypePromoPop = {
    init : function () {
        this.pageInit();
        this.event();
        $('#storeType').val(storeType);
        $('#storeTypeNm').text(storeType);
    },
    pageInit : function () {
        mileageTypeGrid.init();
        orderUtil.setWindowAutoResize();
        orderUtil.setRadioCheck(mileageTypeGrid);
        orderUtil.setCheckBarHeader([mileageTypeGrid], "선택", null);
    },
    search : function () {
        var searchFormData = $("form[name=mileageTypePopForm]").serialize();
        CommonAjax.basic(
            {
                url:'/mileage/type/getMileagePromoTypePopInfo.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                callbackFunc: function(res) {
                    mileageTypeGrid.setData(res);
                    $('#totalCnt').html(mileageTypeGrid.dataProvider.getRowCount());
                }
        });
    },
    checkRow : function () {
        var checkedRows = mileageTypeGrid.gridView.getCheckedRows(true);
        if($.jUtil.isEmpty(checkedRows)){
            alert('마일리지 타입을 선택해 주세요.');
            return false;
        }
        var returnData = mileageTypeGrid.dataProvider.getJsonRow(0);
        eval("opener." + callBackScript + "(returnData);");
        self.close();
    },
    event : function () {
        $("#selectBtn").bindClick(mileageTypePromoPop.checkRow);
        $("#searchBtn").bindClick(mileageTypePromoPop.search);
    }
};