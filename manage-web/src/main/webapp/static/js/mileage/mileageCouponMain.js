/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
    mileageCouponManage.init();
    CommonAjaxBlockUI.global();
});

// 클레임관리
var mileageCouponManage = {
    init: function () {
      deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
      deliveryCore.initSearchDateCustom("issueStartDt", "issueEndDt", "0", "1w");
      this.bindingEvent();
      this.pageInit();
    },
    pageInit : function (){
        GridUtil.initGrid(mileageCouponGrid, mileageCouponUserGrid, mileageCouponDownGrid);
        this.customGrid();
        $('#mileageCouponDownGrid').hide();
    },
    customGrid : function (){
        // 선택
        mileageCouponGrid.gridView.onDataCellClicked = function(gridView, index) {
            mileageCouponManage.searchManageInfo(mileageCouponGrid.dataProvider.getJsonRow(index.dataRow).couponManageNo);
            $('#issueYn').val(mileageCouponGrid.dataProvider.getJsonRow(index.dataRow).issueYn);
        };
        mileageCouponUserGrid.gridView.onItemChecked = function (gridView, itemIndex, checked) {
            if(checked){
                if(gridView.getValue(itemIndex, "recoveryYn") === 'Y') {
                    alert('회수된 번호입니다. 다시 상태값을 변경할 수 없습니다.');
                    gridView.checkItem(itemIndex, false);
                }
            }
        };
        mileageCouponUserGrid.gridView.onItemAllChecked = function (gridView, checked) {
            var isErrCheck = false;
            if(checked){
                for(var itemIndex = 0; itemIndex < gridView.getItemCount(); itemIndex ++){
                    if(gridView.getValue(itemIndex, "recoveryYn") === 'Y') {
                        gridView.checkItem(itemIndex, false);
                        isErrCheck = true;
                    }
                }
                if(isErrCheck){
                    alert('회수된 난수번호를 선택하셨습니다.\n회수된 번호는 다시 상태값을 변경할 수 없습니다.');
                }
            }
        };

    },
    initSearchDate : function (flag) {
        mileageCouponManage.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        mileageCouponManage.setDate.setEndDate(0);
        mileageCouponManage.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    searchReset: function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
        });
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },
    regReset: function () {
        if(!confirm('초기화 하시겠습니까?')){
            return false;
        }
        $("form[name=registerForm]").each(function() {
            this.reset();
        });
        deliveryCore.initSearchDateCustom("issueStartDt", "issueEndDt", "0", "1w");
        $('#couponManageNo').val('').trigger('change');
        $('#couponType').trigger('change');
    },
    bindingEvent: function () {
        // [검색] 버튼
        $("#searchBtn").bindClick(mileageCouponManage.search);
        // [검색 초기화] 버튼
        $("#searchResetBtn").bindClick(mileageCouponManage.searchReset);
        //[난수종류] 선택
        $('#couponType').bindChange(mileageCouponManage.chgRandomType);
        // [초기화] 버튼
        $("#resetRegBtn").bindClick(mileageCouponManage.regReset);
        // 회수 버튼
        $("#recoveryBtn").bindClick(mileageCouponManage.returnCoupon);
        // 마일리지 타입 유형 조회
        $('#searchMileageTypeBtn').bindClick(mileageCouponManage.searchMileageTypePop);
        // 난수관리 다운로드
        $('#excelDownBtn').bindClick(() => { mileageCouponManage.excelDownload(mileageCouponGrid, "마일리지_난수관리_조회"); });
        // 일회성 쿠폰 정보 다운로드
        $('#couponExcelDownBtn').bindClick( () => { mileageCouponManage.couponOneTimeDownLoad(); });
        $('#saveBtn').bindClick(mileageCouponManage.saveCouponInfo);
        //난수발급수량, 1인 발급수량, 지급마일리지 - 숫자만 입력
        $("#issueQty,#mileageAmt,#personalIssueQty").allowInput("keyup", ["NUM", "NOT_SPACE"], $(this).attr("id"));
        $('#couponManageNo').on('change', function () {
            if($.jUtil.isNotEmpty($(this).val())){
                if($('#issueYn').val() === 'Y'){
                    $('#couponExcelDownBtn').removeAttr('disabled');
                }
                $('#saveBtn').html('수정');
                $('#issueStartDt, #issueEndDt, #couponType, #issueQty, #personalIssueType, #personalIssueQty, #mileageAmt, #mileageTypeNo, #mileageTypeName').attr('readonly', true);
                $( "#issueStartDt, #issueEndDt" ).datepicker( "option", "disabled", true );
            } else {
                $('#couponExcelDownBtn').attr('disabled', true);
                $('#saveBtn').html('저장');
                $('#issueStartDt, #issueEndDt, #couponType, #issueQty, #personalIssueType, #personalIssueQty, #mileageAmt, #mileageTypeNo, #mileageTypeName').removeAttr('readonly');
                $( "#issueStartDt, #issueEndDt" ).datepicker( "option", "disabled", false );
            }
        });
    },
    validate : function () {
        const schStartDt = $("#schStartDt");
        const schEndDt = $("#schEndDt");
        var startDt = moment(schStartDt.val(), 'YYYY-MM-DD', true);
        var endDt = moment(schEndDt.val(), 'YYYY-MM-DD', true);

        if (!startDt.isValid() || !endDt.isValid()) {
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }

        if (endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            schStartDt.val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        if ($.jUtil.isEmpty($('#schDetailDesc').val())) {
            if (endDt.diff(startDt, "month", true) > 2) {
                alert("검색조건이 입력되지 않아,\n조회기가은 최대 2개월까지 설정 가능합니다.");
                schStartDt.val(endDt.add(-2, "month").format("YYYY-MM-DD"));
                return false;
            }
        } else {
            if (endDt.diff(startDt, "month", true) > 6) {
                alert("조회기간은 최대 6개월 범위까지만 가능합니다.");
                schStartDt.val(endDt.add(-6, "month").format("YYYY-MM-DD"));
                return false;
            }
        }
        return true;
    },
    saveValidate : function () {
        const issueStartDt = $("#issueStartDt");
        const issueEndDt = $("#issueEndDt");
        var startDt = moment(issueStartDt.val(), 'YYYY-MM-DD', true);
        var endDt = moment(issueEndDt.val(), 'YYYY-MM-DD', true);
        if (!startDt.isValid() || !endDt.isValid()) {
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            issueStartDt.val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        var checkData = orderUtil.arrayNullCheck(['couponNm', 'issueQty', 'mileageAmt', 'personalIssueQty', 'mileageTypeNo', 'mileageTypeName', 'displayMessage']);
        if($.jUtil.isNotEmpty(checkData)){
            var checkStr = '';
            switch (checkData) {
                case 'couponNm' : checkStr = '제목'; break;
                case 'issueQty' : checkStr = '발급수량'; break;
                case 'mileageAmt' : checkStr = '마일리지금액'; break;
                case 'personalIssueQty' : checkStr = '1인당 발급수량'; break;
                case 'mileageTypeNo' :
                case 'mileageTypeName' :
                    checkStr = '마일리지타입';
                    break;
                case 'displayMessage' : checkStr = '고객노출문구'; break;
            }
            alert(checkStr + '을 입력해주세요.');
            $('#' + checkData).focus();
            return false;
        }
        const personalIssueQty = $('#personalIssueQty');
        const issueQty = $('#issueQty');
        const couponType = $('#couponType');
        const couponNo = $('#couponNo');
        if(parseInt(issueQty.val()) > 10000000){
            alert('발급수량은 최대 ' + $.jUtil.comma(10000000) + '을 초과할 수 없습니다.');
            issueQty.focus();
            return false;
        }
        if(parseInt(personalIssueQty.val()) > parseInt(issueQty.val())){
            alert('1인 발급수량은 최대 발급수량을 초과할 수 없습니다.');
            personalIssueQty.focus();
            return false;
        }

        if(couponType.val() == 'MUL'){
            if($.jUtil.isEmpty(couponNo.val())){
                alert('일련번호를 입력해주세요.');
                couponNo.focus();
                return false;
            }
        }
        return true;
    },
    search : function () {
        var data = _F.getFormDataByJson($("form[name=searchForm]"));
        if(!mileageCouponManage.validate()) return false;
        CommonAjax.basic({
            url: '/mileage/coupon/getMileageCouponManageList.json',
            data: data,
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function (res) {
                if("0000".indexOf(res.returnCode) > -1){
                    mileageCouponGrid.setData(res.data);
                    $('#searchCnt').html($.jUtil.comma(mileageCouponGrid.gridView.getItemCount()));
                } else {
                    alert(res.returnMessage);
                }
            }
        });
    },
    searchManageInfo : function(_couponManageNo) {
        var searchFormData = "couponManageNo=" + _couponManageNo;
        CommonAjax.basic(
            {
                url: '/mileage/coupon/getMileageCouponManageInfo.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                successMsg: null,
                method: "GET",
                callbackFunc: function(res) {
                    if("0000".indexOf(res.returnCode) > -1){
                        orderUtil.writeToData(res.data);
                        const couponManageNo = $('#couponManageNo');
                        if($.jUtil.isNotEmpty(couponManageNo.val())){
                            couponManageNo.trigger('change');
                            $('#couponType').trigger('change');
                            $('#couponNo').attr('disabled', true);
                            $('input:radio[name=useYn]:input[value=' + res.data.useYn + ']').attr('checked', true);
                        }
                        mileageCouponManage.searchUserInfo(_couponManageNo);
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });

    },
    searchUserInfo : function(_couponManageNo) {
        var searchFormData = "couponManageNo=" + _couponManageNo;
        CommonAjax.basic(
            {
                url: '/mileage/coupon/getMileageCouponUserList.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                successMsg: null,
                method: "GET",
                callbackFunc: function(res) {
                    if("0000".indexOf(res.returnCode) > -1){
                        mileageCouponUserGrid.setData(res.data);
                        $('#searchCnt').html($.jUtil.comma(mileageCouponGrid.gridView.getItemCount()));
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });

    },
    searchMileageTypePop : function () {
        orderUtil.getWinPopup(
            "/mileage/popup/mileageTypeNamePop?".concat(jQuery.param({ "mileageTypeNo" : $("#mileageTypeNo").val(), "mileageTypeName" : $("#mileageTypeName").val() })),
            600,
            430,
            'mileageTypePop'
        );
    },
    saveCouponInfo : function () {
        const couponMangeNo = $('#couponManageNo');
        var typeStr = '저장';
        var _url = '/mileage/coupon/setMileageCoupon.json';
        if($.jUtil.isNotEmpty(couponMangeNo.val())){
            typeStr = '수정';
            _url = '/mileage/coupon/modifyMileageCoupon.json';
        }
        if (!confirm(typeStr + "하시겠습니까?")) {
            return false;
        }
        if(!mileageCouponManage.saveValidate()) return false;
        var formData = $("form[name=registerForm]").serializeObject();
        CommonAjax.basic(
            {
                url: _url,
                data: JSON.stringify(formData),
                contentType: 'application/json',
                successMsg: null,
                method: "POST",
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        location.reload();
                    }
                }
            });
    },
    returnCoupon : function (){
        var checkedItems = mileageCouponUserGrid.gridView.getCheckedItems();
        if(checkedItems.length === 0){
            alert("회수할 대상을 선택해주세요.");
            return false;
        }
        var param = {
            couponManageNo : $('#couponManageNo').val(),
            mileageTypeNo : $('#mileageTypeNo').val(),
            couponType : $('#couponType').val(),
            mileageAmt : $('#mileageAmt').val(),
            infoList : null
        };
        var userInfo = new Array();
        for(let i in checkedItems) {
            const rowData = mileageCouponUserGrid.gridView.getValues(checkedItems[i]);
            userInfo.push(rowData);
        }
        param.infoList = userInfo;
        if (!confirm("마일리지를 회수 하시겠습니까?\n회수 시 고객에게 발급된 난수번호가 회수 처리 됩니다.\n다만 이미 사용한 고객에게는 영향을 미치지 않으며, 다시 상태를 변경할 수 없습니다.")) {
            return false;
        }
        CommonAjax.basic(
            {
                url: '/mileage/coupon/returnMileageCoupon.json',
                data: JSON.stringify(param),
                contentType: 'application/json',
                successMsg: null,
                method: "POST",
                callbackFunc: function(res) {
                    alert(res.returnMessage);
                    if('0000,SUCCESS'.indexOf(res.returnCode) > -1){
                        location.reload();
                    }
                }
            });

    },
    chgRandomType: function () {
        const couponType = $('#couponType');
        if(couponType.val() == 'ONE'){
            $('#couponExcelDownBtn').show();
            $('#couponNoArea').hide();
        } else {
            $('#couponExcelDownBtn').hide();
            $('#couponNoArea').show();
        }
    },
    couponOneTimeDownLoad : function (){
        var searchFormData = "couponManageNo=" + $('#couponManageNo').val();
        CommonAjax.basic(
            {
                url: '/mileage/coupon/getMileageCouponDownList.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                successMsg: null,
                method: "GET",
                callbackFunc: function(res) {
                    if("0000".indexOf(res.returnCode) > -1){
                        mileageCouponDownGrid.setData(res.data);
                        mileageCouponManage.excelDownload(mileageCouponDownGrid, "마일리지쿠폰_".concat($('#couponNm').val()).concat("_일회성"))
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
    },
    excelDownload: function(_grid, _fileName) {
        if (_grid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  _fileName + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        _grid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
};