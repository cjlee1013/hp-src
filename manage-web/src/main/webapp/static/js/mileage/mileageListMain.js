/***
 * 마일리지 조회 리스트 그리드 설정
 * @type
 */
var mileageListGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mileageListGrid.initGrid();
        mileageListGrid.initDataProvider();
        mileageListGrid.event();
    },
    initGrid : function () {
        mileageListGrid.gridView.setDataSource(mileageListGrid.dataProvider);
        mileageListGrid.gridView.setStyles(mileageListBaseInfo.realgrid.styles);

        mileageListGrid.gridView.setDisplayOptions(mileageListBaseInfo.realgrid.displayOptions);
        mileageListGrid.gridView.setColumns(mileageListBaseInfo.realgrid.columns);
        mileageListGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        mileageListGrid.dataProvider.setFields(mileageListBaseInfo.dataProvider.fields);
        mileageListGrid.dataProvider.setOptions(mileageListBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
        mileageListGrid.dataProvider.clearRows();
        mileageListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(mileageListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = "마일리지내역_"+$("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        mileageListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};

var mileageList = {
    init : function () {
        this.initSearchDate('0d');
        $("#mileageKind").val("A");
        $("#userNo").val("");
    },
    initSearchDate : function (flag) {
        mileageList.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        mileageList.setDate.setEndDate(0);
        mileageList.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(mileageList.search);
        $('#searchResetBtn').bindClick(mileageList.resetSearchForm);
        $('#excelDownloadBtn').bindClick(mileageListGrid.excelDownload);
        $('#userNo').bindClick(function () {
            memberSearchPopup('mileageList.callbackMemberSearchPop');
        });
        $('#userNo').bind('input', function () {
            var value	= $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).focus();
                return;
            }
        });
    },
    checkValid : function() {
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }

        const mileageCategory = $('#mileageCategory');
        const userNo = $('#userNo');

        if($.jUtil.isEmpty(userNo.val())){
            if (schEndDt.diff(schStartDt, "month", true) > 1) {
                alert("검색조건이 입력되지 않아,\n조회기가은 최대 1개월까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }
        } else {
            if (schEndDt.diff(schStartDt, "month", true) > 6) {
                alert("조회기간은 최대 6개월 범위까지만 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-6, "month").format("YYYY-MM-DD"));
                return false;
            }
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        if($.jUtil.isNotEmpty(userNo.val()) && !$.jUtil.isAllowInput(userNo.val(), ['NUM'])){
            alert("회원번호는 숫자만 입력 가능합니다");
            userNo.focus();
            return false;
        }

        if($.jUtil.isEmpty(userNo.val())){
            alert('회원을 조회해주세요.');
            userNo.focus().trigger('click');
            return false;
        }

        return true;
    },
    search : function () {
        if(!mileageList.checkValid()) return false;
        CommonAjaxBlockUI.startBlockUI();

        var mileCategoryList = new Array();
        $("input[name='mileageCategory']:checked").each(function(){
           mileCategoryList.push($(this).val());
        });

        var searchFormData = $("form[name=searchForm]").serialize();
        CommonAjax.basic(
            {
                url:'/mileage/history/getMileageList.json?' + searchFormData,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    mileageListGrid.setData(res.mileageHistoryList);
                    $('#mileageAmt').html($.jUtil.comma(res.mileageAmt));
                    $('#expiredMileageAmt').html($.jUtil.comma(res.expiredMileageAmt));
                    $('#mileageListTotalCount').html($.jUtil.comma(mileageListGrid.gridView.getItemCount()));
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            mileageList.init();
        });
    },
    callbackMemberSearchPop : function(memberData) {
        $("#userNo").val(memberData.userNo);
    },
};