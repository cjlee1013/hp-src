/***
 * 마일리지 유형 조회 팝업 그리드
 * @type
 */

var mileageTypePopGrid = {
    gridView : new RealGridJS.GridView("mileageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        mileageTypePopGrid.initGrid();
        mileageTypePopGrid.initDataProvider();
        mileageTypePopGrid.event();
    },
    initGrid : function () {
        mileageTypePopGrid.gridView.setDataSource(mileageTypePopGrid.dataProvider);

        mileageTypePopGrid.gridView.setStyles(mileageListBaseInfo.realgrid.styles);
        mileageTypePopGrid.gridView.setDisplayOptions(mileageListBaseInfo.realgrid.displayOptions);
        mileageTypePopGrid.gridView.setColumns(mileageListBaseInfo.realgrid.columns);
        mileageTypePopGrid.gridView.setOptions(mileageListBaseInfo.realgrid.options);
        mileageTypePopGrid.gridView.setCheckBar({ visible: false });
        mileageTypePopGrid.gridView.setSelectOptions({
            style: 'rows'
        });
    },
    initDataProvider : function() {
        mileageTypePopGrid.dataProvider.setFields(mileageListBaseInfo.dataProvider.fields);
        mileageTypePopGrid.dataProvider.setOptions(mileageListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        mileageTypePopGrid.dataProvider.clearRows();
        mileageTypePopGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        mileageTypePopGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            mileageTypePopGrid.selectRow(index.dataRow);
        };
    },
    selectRow: function (selectRowId) {
        var rowDataJson = mileageTypePopGrid.dataProvider.getJsonRow(selectRowId);
        $('#mileageTypeNo').val(rowDataJson.mileageTypeNo);
        $('#mileageTypeName').val(rowDataJson.mileageTypeName);
        mileageTypePop.select();
    },
};

var mileageTypePop = {
    init : function () {
        mileageTypePop.search();
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(mileageTypePop.search);
        $('#searchResetBtn').bindClick(mileageTypePop.resetSearchForm);
        $('#selectBtn').bindClick(mileageTypePop.select);
    },
    search : function () {
        CommonAjaxBlockUI.startBlockUI();
        var searchFormData = $("#mileageTypeName").val();

        CommonAjax.basic({
            url: "/mileage/type/getMileageTypeList.json?storeType=A&useYn=Y&mileageKind=A&schDetailCode=MILEAGE_TYPE_NAME&schDetailDesc="+ searchFormData,
            contentType: 'application/json',
            data: null,
            method: "GET",
            callbackFunc:function(res) {
                mileageTypePopGrid.setData(res);
                CommonAjaxBlockUI.stopBlockUI();
            }
        });
    },
    select : function () {
        if ($('#mileageTypeNo').val()=="" || $('#mileageTypeName').val() =="") {
            alert("마일리지 유형을 선택해주세요.");
            return false;
        }
        $('#mileageTypeName', opener.document).val($('#mileageTypeName').val());
        $('#mileageTypeNo', opener.document).val($('#mileageTypeNo').val());
        self.close();
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
        });
    },
}