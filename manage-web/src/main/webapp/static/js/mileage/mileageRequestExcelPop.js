/***
 * 마일리지 지급/회수관리 팝업 그리드 설정
 */
var mileageDetailGrid = {
    gridView : new RealGridJS.GridView("mileageDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function () {
        mileageDetailGrid.initGrid();
        mileageDetailGrid.initDataProvider();
        mileageDetailGrid.event();
    },
    initGrid : function () {
        mileageDetailGrid.gridView.setDataSource(mileageDetailGrid.dataProvider);
        mileageDetailGrid.gridView.setStyles(mileageDetailBaseInfo.realgrid.styles);
        mileageDetailGrid.gridView.setDisplayOptions(mileageDetailBaseInfo.realgrid.displayOptions);
        mileageDetailGrid.gridView.setColumns(mileageDetailBaseInfo.realgrid.columns);
        mileageDetailGrid.gridView.setOptions(mileageDetailBaseInfo.realgrid.options);
    },
    initDataProvider : function () {
        mileageDetailGrid.dataProvider.setFields(mileageDetailBaseInfo.dataProvider.fields);
        mileageDetailGrid.dataProvider.setOptions(mileageDetailBaseInfo.dataProvider.options);
    },
    event : function () {
    },
    setData : function(dataList) {
        mileageDetailGrid.dataProvider.clearRows();
        mileageDetailGrid.dataProvider.setRows(dataList);
    },
    excelSample : function() {
        var fileName = "일괄처리 엑셀양식";

        SettleCommon.setRealGridColumnVisible(mileageDetailGrid.gridView, [SettleCommon.getRealGridColumnName(mileageDetailGrid.gridView, "고객명")], false);
        SettleCommon.setRealGridColumnVisible(mileageDetailGrid.gridView, [SettleCommon.getRealGridColumnName(mileageDetailGrid.gridView, "비고")], false);

        var _dataProvider = mileageDetailGrid.dataProvider.getRows(0, -1);
        mileageDetailGrid.dataProvider.clearRows();
        mileageDetailGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            count: 0,
            fileName: fileName + ".xlsx",
            allColumns : false,
            progressMessage: "엑셀 Export중입니다."
        });
        mileageDetailGrid.dataProvider.setRows(_dataProvider);
    },
    excelDownload : function () {
        if(mileageDetailGrid.gridView.getItemCount() == 0) {
            alert("실패건이 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "일괄등록실패목록_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        SettleCommon.setRealGridColumnVisible(mileageDetailGrid.gridView, [SettleCommon.getRealGridColumnName(mileageDetailGrid.gridView, "비고")], false);
        mileageDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            indicator: "hidden"
        });
        SettleCommon.setRealGridColumnVisible(mileageDetailGrid.gridView, [SettleCommon.getRealGridColumnName(mileageDetailGrid.gridView, "비고")], true);
    },
};
var mileageReqList = {
    init : function () {

    },
    bindingEvent : function () {
        $('#saveBtn').bindClick(mileageReqList.save);
        $('#btnUploadFile').bindClick(mileageReqList.openFileSearch);
        $('#uploadFile').bindChange(mileageReqList.changeFileSearch);

        $("#closeBtn").on("click", function (data) {
        });
    },
    // 파일 찾기 버튼 선택
    openFileSearch : function() {
        $('#uploadFile').trigger('click');
    },
    changeFileSearch : function() {
        var fileInfo = $('#uploadFile')[0].files[0];
        if (!fileInfo) {
            alert('업로드 대상 파일을 찾아주세요.');
            return false;
        }

        var fileExt = fileInfo.name.split(".")[1];
        if (fileExt.indexOf("xlsx") < 0 && fileExt.indexOf("xls") < 0) {
            alert("일괄등록은 지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            $('#uploadFile').val('');
            return false;
        }
        $("#uploadFileNm").val($('#uploadFile')[0].files[0].name);
    },
    save : function () {
        if (!confirm("일괄 등록하시겠습니까?")) return false;

        CommonAjaxBlockUI.startBlockUI();
        $('form[name="fileUploadForm"]').ajaxForm({
            url: '/mileage/payment/uploadExcel.json',
            enctype: 'multipart/form-data',
            contentType: false,
            method: "POST",
            processData: false,
            cache: false,
            successMsg: null,
            success: function(res) {
                mileageDetailGrid.dataProvider.fillJsonData(res.data.failList, {fillMode: "append"});
                mileageDetailGrid.gridView.setDataSource(mileageDetailGrid.dataProvider);

                mileageReqList.resultTemplate(res);

                if (res.data.failCnt > 0) {
                    mileageDetailGrid.excelDownload();
                }
                CommonAjaxBlockUI.stopBlockUI();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('오류가 발생하였습니다.');
                console.log(thrownError);
                CommonAjaxBlockUI.stopBlockUI();
            }
        }).submit();
    },
    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(res) {
        var innerHtml = [];
        var message = '<br/>';
        if (res.data.failCnt > 0) {
            message = '- 등록에 실패하였습니다. 업로드할 엑셀파일에 오류가 없는지 확인하시고 다시 한 번 등록해주세요.';
        }
        innerHtml.push('<script>');
        innerHtml.push('$("#closeBtn").on("click", function () {');
        innerHtml.push("$(opener.location).attr('href', 'javascript: mileageReqList.addExcelData("+ JSON.stringify(res.data.successList) +");');");
        innerHtml.push('self.close();');
        innerHtml.push('});</script>');

        innerHtml.push('<div class="com wrap-title has-border">');
        innerHtml.push('    <h2 class="title font-malgun">일괄처리</h2>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-10">');
        innerHtml.push('    <ul>');
        innerHtml.push('        <li style="font-size:15px;">'+ message + '</li>');
        innerHtml.push('    </ul>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-15" style="text-align: center">');
        innerHtml.push('    <span style="font-size:15px;">등록 <span style="font-weight:bold;width:100px;display:inline-block;">'+ $.jUtil.comma(res.data.totalCnt) + '</span>');
        innerHtml.push('        성공 <span style="color:#6495ed;font-weight:bold;width:100px;display:inline-block;">'+ $.jUtil.comma(res.data.successCnt) + '</span>');
        innerHtml.push('        실패 <span style="color:#dc143c;font-weight:bold;width:100px;display:inline-block;">'+ $.jUtil.comma(res.data.failCnt) + '</span></span>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="ui center-button-group">');
        innerHtml.push('    <span class="inner">');
        innerHtml.push('        <button id="closeBtn" class="ui button large font-malgun">확인</button>');
        innerHtml.push('    </span>');
        innerHtml.push('</div>');

        $('#divPop').html(innerHtml.join(''));
    }
};