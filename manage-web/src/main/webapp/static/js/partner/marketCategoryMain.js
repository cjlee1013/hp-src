
$(document).ready(function() {
    marketCategoryListGrid.init();
    marketCategory.init();
    commonCategory.init();
    CommonAjaxBlockUI.global();
});

// 마켓카테고리맵핑
var marketCategoryListGrid = {

    gridView : new RealGridJS.GridView("marketCategoryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        marketCategoryListGrid.initGrid();
        marketCategoryListGrid.initDataProvider();
        marketCategoryListGrid.event();

        commonCategory.setCategorySelectBox(1, '', '', $('#schCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#schCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#schCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#schCateCd4'));

        commonCategory.setCategorySelectBox(1, '', '', $('#selectCateCd1'));
        commonCategory.setCategorySelectBox(2, '', '', $('#selectCateCd2'));
        commonCategory.setCategorySelectBox(3, '', '', $('#selectCateCd3'));
        commonCategory.setCategorySelectBox(4, '', '', $('#selectCateCd4'));

        marketCategory.setMarketCategorySelectBox(1,'','', $('#elevenStCateCd1'), $('#elevenSt').val());
        marketCategory.setMarketCategorySelectBox(2,'','', $('#elevenStCateCd2'), $('#elevenSt').val());
        marketCategory.setMarketCategorySelectBox(3,'','', $('#elevenStCateCd3'), $('#elevenSt').val());
        marketCategory.setMarketCategorySelectBox(4,'','', $('#elevenStCateCd4'), $('#elevenSt').val());

        /*
        marketCategory.setMarketCategorySelectBox(1,'','', $('#gmarketCateCd1'), $('#gmarket').val());
        marketCategory.setMarketCategorySelectBox(2,'','', $('#gmarketCateCd2'), $('#gmarket').val());
        marketCategory.setMarketCategorySelectBox(3,'','', $('#gmarketCateCd3'), $('#gmarket').val());

        marketCategory.setMarketCategorySelectBox(1,'','', $('#auctionCateCd1'), $('#auction').val());
        marketCategory.setMarketCategorySelectBox(2,'','', $('#auctionCateCd2'), $('#auction').val());
        marketCategory.setMarketCategorySelectBox(3,'','', $('#auctionCateCd3'), $('#auction').val());
        marketCategory.setMarketCategorySelectBox(4,'','', $('#auctionCateCd4'), $('#auction').val());
         */

        marketCategory.setMarketCategorySelectBox(1,'','', $('#naverCateCd1'), $('#naver').val());
        marketCategory.setMarketCategorySelectBox(2,'','', $('#naverCateCd2'), $('#naver').val());
        marketCategory.setMarketCategorySelectBox(3,'','', $('#naverCateCd3'), $('#naver').val());
        marketCategory.setMarketCategorySelectBox(4,'','', $('#naverCateCd4'), $('#naver').val());

    },
    initGrid : function() {
        marketCategoryListGrid.gridView.setDataSource(marketCategoryListGrid.dataProvider);

        marketCategoryListGrid.gridView.setStyles(marketCategoryListGridBaseInfo.realgrid.styles);
        marketCategoryListGrid.gridView.setDisplayOptions(marketCategoryListGridBaseInfo.realgrid.displayOptions);
        marketCategoryListGrid.gridView.setColumns(marketCategoryListGridBaseInfo.realgrid.columns);
        marketCategoryListGrid.gridView.setOptions(marketCategoryListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        marketCategoryListGrid.dataProvider.setFields(marketCategoryListGridBaseInfo.dataProvider.fields);
        marketCategoryListGrid.dataProvider.setOptions(marketCategoryListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        marketCategoryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            marketCategory.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        marketCategoryListGrid.dataProvider.clearRows();
        marketCategoryListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketCategoryListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "마켓연동카테고리맵핑_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        marketCategoryListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }

};

// 마켓연동 카테고리 맵핑
var marketCategory = {
    //이베이 카테고리 주석처리 21.11
    //partner : 'gmarket,auction,elevenSt,naver', //마켓연동업체
    partner : 'elevenSt,naver',
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(marketCategory.search);
        $('#excelDownloadBtn').bindClick(marketCategory.excelDownload);
        $('#resetBtn').bindClick(marketCategory.resetForm);
        $('#searchResetBtn').bindClick(marketCategory.resetSearchForm);
        $('#setMarketCategoryBtn').bindClick(marketCategory.setMarketCategoryMap);

        $('.applyYn').on('change', function() {
            marketCategory.changeApplyYn($(this));
        });
        $('.dcate').on('change', function() {
            marketCategory.chagneMarketDCateSelectBox($(this));
        });
     },
    /**
     * 연동 여부 변경
     * @param obj
     */
    changeApplyYn : function (obj) {

        let id = $(obj).data('cate');
        if($(obj).val() == 'N' ) {
            $('select[id^=' + id + ']').prop('disabled', true);
            $("#"+id+"1").val('').change();
        } else {
            $("select[id^=" + id + "]").prop('disabled', false);
        }
    },
    /**
     * 마켓연동 카테고리 맵핑 수정/등록
     */
    setMarketCategoryMap : function () {

        if ($.jUtil.isEmpty($('#selectCateCd4').val())) {
            alert('카테고리를 선택해주세요.');
            $('#selectCateCd1').focus();
            return false;
        }

        let marketCategoryMapList = new Array();
        let prefix =  marketCategory.partner.split(',');

        for ( let i in prefix ) {
            let partnerId = $('#' + prefix[i]).val();
            if ($.jUtil.isEmpty($('#'+ prefix[i] +'ApplyYn').val())) {
                return $.jUtil.alert("연동여부를 선택해주세요" , prefix[i]+'ApplyYn');
            }

            if ( ($('#'+prefix[i]+'ApplyYn').val() == 'Y' || $('#'+prefix[i]+'ApplyYn').val() == 'P')
                && $.jUtil.isEmpty($('#' + partnerId + 'LeafCateCd').val())) {
                return $.jUtil.alert("카테고리를 선택해주세요" , prefix[i]+'CateCd1');
            }

            //저장대상
            marketCategoryMapList.push(marketCategory.setMarketCate( prefix[i]));
        }

        let form = $('#marketCategoryMapForm').serializeObject();
        form.mapList = marketCategoryMapList;

        CommonAjax.basic({
            url: '/partner/setMarketCategoryMap.json',
            data: JSON.stringify(form),
            method: "POST",
            contentType: 'application/json',
            callbackFunc: function (res) {
                marketCategory.search();
                alert(res.returnMsg);
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 마켓연동 카테고리 저장
     * @param prefix
     * @returns {Object}
     */
    setMarketCate : function(prefix) {

        let data = new Object();

        let partnerId = $('#'+prefix).val();
        let marketCateCd = $('#' + partnerId + 'LeafCateCd').val();

        data.dcateCd = $('#selectCateCd4').val();
        data.applyYn = $('#'+prefix+'ApplyYn').val();

        data.partnerId = partnerId;
        data.marketCateCd = marketCateCd;

        return data;
    },
    /**
     * 마켓연동 카테고리 조회
     * @returns {boolean}
     */
    search : function() {

        CommonAjax.basic({
            url: '/partner/getMarketCategoryMapList.json?' + $('#marketCategorySearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                marketCategoryListGrid.setData(res);
                $('#marketCategoryTotalCount').html($.jUtil.comma(marketCategoryListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 마켓연동 카테고리 엑셀 다운로드
     */
    excelDownload : function() {
        marketCategoryListGrid.excelDownload();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        marketCategory.resetForm();

        let rowDataJson = marketCategoryListGrid.dataProvider.getJsonRow(selectRowId);
        $('#isMod').val('Y');

        //홈플러스 카테고리
        if (!$.jUtil.isEmpty(rowDataJson.dcateCd)){
            CommonAjax.basic({
                url: '/common/category/getCategoryDetail.json?depth=4&cateCd=' + rowDataJson.dcateCd,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    commonCategory.setCategorySelectBox(1, '', res.lcateCd, $('#selectCateCd1'));
                    commonCategory.setCategorySelectBox(2, res.lcateCd, res.mcateCd, $('#selectCateCd2'));
                    commonCategory.setCategorySelectBox(3, res.mcateCd, res.scateCd, $('#selectCateCd3'));
                    commonCategory.setCategorySelectBox(4, res.scateCd, res.dcateCd, $('#selectCateCd4'));

                    marketCategory.changeElevenStCategory(rowDataJson.elevenCateCd, rowDataJson.elevenApplyYn);
                    marketCategory.changeNaverCategory(rowDataJson.naverCateCd, rowDataJson.naverApplyYn);

                    //이베이 카테고리 주석처리 21.11
                    //marketCategory.changeGmarketCategory(rowDataJson.gmarketCateCd, rowDataJson.gmarketApplyYn);
                    //marketCategory.changeAuctionCategory(rowDataJson.auctionCateCd, rowDataJson.auctionApplyYn);

                }});
        }
    },
    /**
     * 지마켓 카테고리 디테일 조회
     * @param cateCd
     */
    //이베이 카테고리 주석처리 21.11
    /* changeGmarketCategory : function (cateCd, applyYn) {

        let gmarket = $('#gmarket').val();

        if (!$.jUtil.isEmpty(cateCd) && applyYn != "N"){
            $('#' + gmarket + 'LeafCateCd').val(cateCd);
            CommonAjax.basic({
                url: '/partner/getMarketCategoryDetail.json?marketCateCd=' + cateCd + "&partnerId=" + gmarket,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    marketCategory.setMarketCategorySelectBox(1, '', res.marketLcateCd, $('#gmarketCateCd1'), gmarket);
                    marketCategory.setMarketCategorySelectBox(2, res.marketLcateCd, res.marketMcateCd, $('#gmarketCateCd2'), gmarket);
                    marketCategory.setMarketCategorySelectBox(3, res.marketMcateCd, res.marketScateCd, $('#gmarketCateCd3'), gmarket);
                }});


        }else{
            $('#gmarketCateCd1').val('').change();
        }
        $('#gmarketApplyYn').val(applyYn).trigger('change');
    },*/
    /**
     * 옥션 카테고리 디테일 조회
     * @param cateCd
     */
    //이베이 카테고리 주석처리 21.11
    /*
    changeAuctionCategory : function (cateCd, applyYn) {

        let auction = $('#auction').val();
        if (!$.jUtil.isEmpty(cateCd) && applyYn != "N"){

            $('#'+ auction + 'LeafCateCd').val(cateCd);
            CommonAjax.basic({
                url: '/partner/getMarketCategoryDetail.json?marketCateCd=' + cateCd + "&partnerId=" + auction,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    marketCategory.setMarketCategorySelectBox(1, '', res.marketLcateCd, $('#auctionCateCd1'), auction);
                    marketCategory.setMarketCategorySelectBox(2, res.marketLcateCd, res.marketMcateCd, $('#auctionCateCd2'), auction);
                    marketCategory.setMarketCategorySelectBox(3, res.marketMcateCd, res.marketScateCd, $('#auctionCateCd3'), auction);
                    marketCategory.setMarketCategorySelectBox(4, res.marketScateCd, res.marketDcateCd, $('#auctionCateCd4'), auction);

                }});
        }else{
            $('#auctionCateCd1').val('').change();
        }
        $('#auctionApplyYn').val(applyYn).trigger('change');
    },
    */

    /**
     * 11번가 카테고리 디테일 조회
     * @param cateCd
     */
    changeElevenStCategory : function (cateCd, applyYn) {
        let elevenSt = $('#elevenSt').val();
        if (!$.jUtil.isEmpty(cateCd) && applyYn != "N"){
            $('#'+ elevenSt + 'LeafCateCd').val(cateCd);

            CommonAjax.basic({
                url: '/partner/getMarketCategoryDetail.json?marketCateCd=' + cateCd + "&partnerId=" + elevenSt,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    marketCategory.setMarketCategorySelectBox(1, '', res.marketLcateCd, $('#elevenStCateCd1'), elevenSt);
                    marketCategory.setMarketCategorySelectBox(2, res.marketLcateCd, res.marketMcateCd, $('#elevenStCateCd2'), elevenSt);
                    marketCategory.setMarketCategorySelectBox(3, res.marketMcateCd, res.marketScateCd, $('#elevenStCateCd3'), elevenSt);
                    marketCategory.setMarketCategorySelectBox(4, res.marketScateCd, res.marketDcateCd, $('#elevenStCateCd4'), elevenSt);

                }});

        }else{
            $('#elevenStCateCd1').val('').change();
        }
        $('#elevenStApplyYn').val(applyYn).trigger('change');
    },
    /**
     * 네이버 카테고리 디테일 조회
     * @param cateCd
     */
    changeNaverCategory : function (cateCd, applyYn) {

        let naver = $('#naver').val();
        if (!$.jUtil.isEmpty(cateCd) && applyYn != "N" ){
            $('#'+ naver + 'LeafCateCd').val(cateCd);
            CommonAjax.basic({
                url: '/partner/getMarketCategoryDetail.json?marketCateCd=' + cateCd + "&partnerId=" + naver,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    marketCategory.setMarketCategorySelectBox(1, '', res.marketLcateCd, $('#naverCateCd1'), naver);
                    marketCategory.setMarketCategorySelectBox(2, res.marketLcateCd, res.marketMcateCd, $('#naverCateCd2'), naver);
                    marketCategory.setMarketCategorySelectBox(3, res.marketMcateCd, res.marketScateCd, $('#naverCateCd3'), naver);
                    marketCategory.setMarketCategorySelectBox(4, res.marketScateCd, res.marketDcateCd, $('#naverCateCd4'), naver);

                }});

        }else{
            $('#naverCateCd1').val('').change();
        }
        $('#naverApplyYn').val(applyYn).trigger('change');
    },
    
     /**
     * 검색폼 초기화
     */
    resetSearchForm : function() {
        $('#schCateCd1').val('').change();
        $('#schRegYn').val('');
    },
    /**
     * 등록폼 초기화
     */
    resetForm : function() {

        $('#selectCateCd1').val('').change();

        let prefix =  marketCategory.partner.split(',');
        for ( let i in prefix ) {
            $('#'+prefix[i]+"CateCd1").val('').change();
            $('#'+prefix[i]+"ApplyYn").val('').trigger('change');
            $('#coop'+prefix[i]+'LeafCateCd').val('');
        }

        $('#isMod').val('N');

    },
    /**
     * 마켓카테고리 Depth 별 조회
     * @param depth
     * @param parentCateCd
     * @param cateCd
     * @param selector
     * @param partnerId
     */
    setMarketCategorySelectBox : function(depth, parentCateCd, cateCd, selector, partnerId ) {
        $(selector).html('<option value="">'+$(selector).data('default')+'</option>');
        if (((parentCateCd == "" && depth == 1) || ((parentCateCd && depth > 1)))) {
            CommonAjax.basic({
                url: '/partner/getMarketCategoryForSelectBox.json',
                data: {
                    depth: depth,
                    pCateCd: parentCateCd,
                    cateCd: '',
                    partnerId: partnerId
                },
                method: 'get',
                callbackFunc: function (resData) {
                    $.each(resData, function (idx, val) {
                        var selected = (cateCd == val.cateCd) ? 'selected="selected"' : ''
                        $(selector).append('<option value="' + val.cateCd + '-' + val.leafYn+ '" ' + selected  + '>' + val.cateNm + '</option>');
                    });
                    $(selector).css('-webkit-padding-end','30px');

                    if (typeof callBack == 'function') {
                        var callData = {
                            depth:depth,
                            selector : selector,
                            data:resData
                        };
                        callBack(callData);
                    }
                }
            });
        }
    },
    /**
    /**
     * 마켓 카테고리 select box change 이벤트
     *
     * @param depth 해당 카테고리 뎁스
     * @param prefix 셀렉트박스 아이디 prefix
     */
    changeMarketCategorySelectBox : function(depth, prefix, partnerId) {

        let cateCd = $('#' + prefix + depth).val();
        let cateCdVal = cateCd.split("-");

        $('#'+partnerId +'LeafCateCd').val('');

        marketCategory.setMarketCategorySelectBox(depth + 1, cateCdVal[0], '', $('#' + prefix + (depth + 1)), partnerId);
        $('#' + prefix + (depth + 1)).change();

        if(cateCdVal[1] == 'Y') {
            $('#'+partnerId +'LeafCateCd').val(cateCdVal[0]);
        }
    },

    /**
     * 마켓카테고리 세분류 select box change 이벤트
     * @param obj
     */
    chagneMarketDCateSelectBox : function (obj) {

        let id = obj.data('id');
        let cateCdVal = obj.val().split("-");

        $('#'+id +'LeafCateCd').val('');

        if(cateCdVal[1] == 'Y') {
            $('#'+id +'LeafCateCd').val(cateCdVal[0]);
        }
    }

};


