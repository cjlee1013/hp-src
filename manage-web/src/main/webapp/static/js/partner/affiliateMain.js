/**
 * 제휴업체 등록/수정
 */
$(document).ready(function() {
    affiliateListGrid.init();
    affiliate.init();
    CommonAjaxBlockUI.global();
    commonCategory.setCategorySelectBox(1, '', '', $('#categoryCd'));
});

// 제휴업체관리 그리드
var affiliateListGrid = {
    gridView : new RealGridJS.GridView("affiliateListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    isSelectedAffiliate : false,
    init : function() {
        affiliateListGrid.initGrid();
        affiliateListGrid.initDataProvider();
        affiliateListGrid.event();
    },
    initGrid : function() {
        affiliateListGrid.gridView.setDataSource(affiliateListGrid.dataProvider);

        affiliateListGrid.gridView.setStyles(affiliateListGridBaseInfo.realgrid.styles);
        affiliateListGrid.gridView.setDisplayOptions(affiliateListGridBaseInfo.realgrid.displayOptions);
        affiliateListGrid.gridView.setColumns(affiliateListGridBaseInfo.realgrid.columns);
        affiliateListGrid.gridView.setOptions(affiliateListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        affiliateListGrid.dataProvider.setFields(affiliateListGridBaseInfo.dataProvider.fields);
        affiliateListGrid.dataProvider.setOptions(affiliateListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        affiliateListGrid.gridView.onDataCellClicked = function(gridView, index) {
            affiliate.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        affiliateListGrid.dataProvider.clearRows();
        affiliateListGrid.dataProvider.setRows(dataList);
    },
    excelDownload : function() {
        if(affiliateListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "제휴업체 관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        affiliateListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 업체관리
var affiliate = {
    isCheckPartnerId : false,   // 중복확인 여부
    mngCnt : 2,                 // 담당자 입력개수
    isMod : false,              // 수정여부

    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.searchFormReset();
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        affiliate.setDate = Calendar.datePickerRange('schRegStartDate', 'schRegEndDate');

        affiliate.setDate.setEndDate(0);
        affiliate.setDate.setStartDate(flag);

        $("#schRegEndDate").datepicker("option", "minDate", $("#schRegStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $("input[name='affiliateInfo.cultureDeductionYn']").bindChange(affiliate.setCultureDeductionYn);
        $("input[id^=sameAffiManager]").bindClick(affiliate.setSameAffiMng);
        $('#searchResetBtn').bindClick(affiliate.searchFormReset);
        $('#setAffiliateBtn').bindClick(affiliate.setAffiliate);
        $('#resetBtn').bindClick(affiliate.resetForm, true);
        $('#imageUploadBtn').bindClick(affiliate.setImage);
        $('#deleteImageBtn').bindClick(affiliate.unsetImage);
        $('#excelDownloadBtn').bindClick(affiliate.excelDownload);
        $('.uploadType').bindClick(affiliate.imagePreview);
        $('#searchBtn').bindClick(affiliate.search);
        $('#partnerStatusBtn').bindClick(affiliate.getPartnerStatusPop); //회원상태
        $('#getPartnerNoBtn').bindClick(affiliate.getPartnerNoPop);
        $('#getBizCateCd').bindClick(affiliate.getBizCateCd);
        $('#getPartnerPwPop').bindClick(affiliate.getPartnerPwPop);

        $("#schKeyword").notAllowInput('keyup', ['SPC_SCH']);

        // 종목 추가
        $(document).on("click", "li[id^=categorySearchList_]", function() {
            affiliate.addSchCategory(this);
        });

        // 종목 삭제
        $(document).on("click", "button[id^=categoryDelete_]", function(){
            affiliate.delSchCategory(this);
        });

    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        var imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        affiliateListGrid.excelDownload();
    },
    /**
     * 제휴업체 관리 검색
     */
    search : function() {
        if (affiliate.valid.search()) {

            var affiliateSearchForm = $('#affiliateSearchForm').serialize();
            CommonAjax.basic({
                url: '/partner/getAffiliateList.json',
                data: affiliateSearchForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    affiliate.resetForm();
                    affiliateListGrid.setData(res);
                    $('#affiliateTotalCount').html($.jUtil.comma(affiliateListGrid.gridView.getItemCount()));
                }
            });
        }
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schKeyword, #schOperatorType, #schAffiliateType, #schPartnerStatus').val('');
        $('#schType').val('affiliateNm');
        // 조회 일자 초기화
        affiliate.initSearchDate('-1y');

    },
    /**
     * 판매업체 관리 입력/수정 폼 초기화
     */
    resetForm : function(isClick) {
        $("#affiliateForm").each(function () {
            this.reset();
        });

        $('[id^="seq"]').val('');
        $('#partnerStatus').val('NORMAL');
        $('[id^="bizCateCdDiv_"]').remove();
        $('#partnerId, #operatorType, #partnerNo_1, #partnerNo_2, #partnerNo_3, #businessConditions,#partnerOwner, #affiliateNm, #bizCateCd, #bizCateCdNm').val('');
        $('#partnerId').prop('readonly', false);
        affiliate.isMod = false;
        if (isClick) {
            $('#checkPartnerId').show();
            affiliateListGrid.isSelectedAffiliate = false;
        }
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        affiliate.resetForm(false);
        affiliateListGrid.isSelectedAffiliate = true;

        const rowDataJson = affiliateListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url : "/partner/getAffiliate.json?partnerId=" + rowDataJson.partnerId ,
            method : "GET" ,
            callbackFunc : function (affiliateDto) {
                $('#partnerId').val(rowDataJson.partnerId.replace('affi', ''));
                $('#operatorType').val(affiliateDto.operatorType);
                $('#affiliateType').val(affiliateDto.affiliateType);
                $('#affiliateNm').val(affiliateDto.affiliateNm);
                $('#partnerNm').val(affiliateDto.partnerNm);
                $('#partnerOwner').val(affiliateDto.partnerOwner);
                $('#partnerStatus').val(affiliateDto.partnerStatus);
                $.jUtil.valSplit(affiliateDto.partnerNo, '-', 'partnerNo');
                $('#communityNotiNo').val(affiliateDto.communityNotiNo);
                $('#businessConditions').val(affiliateDto.businessConditions);
                $('#bizCateCd').val(affiliateDto.bizCateCd);
                $('#bizCateCdNm').val(affiliateDto.bizCateCdNm);
                $('#zipcode').val(affiliateDto.zipcode);
                $('#addr1').val(affiliateDto.addr1);
                $('#addr2').val(affiliateDto.addr2);

                $('#depositor').val(affiliateDto.depositor);

                affiliate.addSchCategoryHtml(affiliateDto.bizCateCd, affiliateDto.bizCateCdNm);

                affiliateDto.managerList.forEach(function (item, index, array) {
                    let tagId = $('.managerDiv[data-mng-cd="' + item.mngCd + '"]');
                    tagId.find('#seq' + index).val(item.seq);
                    tagId.find('#mngNm' + index).val(item.mngNm);
                    $.jUtil.valSplit(item.mngMobile, '-', ('mngMobile' + index));
                    if (_V.chkCellPhoneNum(item.mngPhone, true) || _V.chkTelNum(item.mngPhone, true)) {
                        $.jUtil.valSplit(item.mngPhone, '-', ('mngPhone' + index));
                    }
                    tagId.find('#mngEmail' + index).val(item.mngEmail);
                });

                $('#partnerId').prop('readonly', true);
                affiliate.isMod = true;
                $('#checkPartnerId').hide();
            }
        });
    },
    /**
     * 제휴담당자와 동일 담당자 정보 셋팅
     * @param obj
     */
    setSameAffiMng : function(obj) {
        if ($(obj).prop('checked')) {
            if (!$.trim($('#mngNm0').val()) || !$.trim($('#mngEmail0').val()) ||
                !$.trim($('#mngMobile0_1').val()) || !$.trim($('#mngMobile0_2').val()) || !$.trim($('#mngMobile0_3').val())) {
                alert('제휴담당자를 입력해 주세요.');
                $(obj).prop('checked', false);
            } else {
                var index = $(obj).prop('id').substr($(obj).prop('id').length-1, 1);
                $('#mngNm'+index).val($('#mngNm0').val());
                $('#mngMobile'+index+'_1').val($('#mngMobile0_1').val());
                $('#mngMobile'+index+'_2').val($('#mngMobile0_2').val());
                $('#mngMobile'+index+'_3').val($('#mngMobile0_3').val());
                $('#mngPhone'+index+'_1').val($('#mngPhone0_1').val());
                $('#mngPhone'+index+'_2').val($('#mngPhone0_2').val());
                $('#mngPhone'+index+'_3').val($('#mngPhone0_3').val());
                $('#mngEmail'+index).val($('#mngEmail0').val());
            }
        }
    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setZipcode : function (res) {
        $("#zipcode").val(res.zipCode);
        $("#addr1").val(res.roadAddr);
        $("#addr2").val('');

    },
    /**
     * 제휴업체 등록
     */
    setAffiliate : function() {
        if (affiliate.valid.set()) {
            $('#phone1').val($.jUtil.valNotSplit('phone1', '-'));
            $('#phone2').val($.jUtil.valNotSplit('phone2', '-'));
            $('#infoCenter').val($.jUtil.valNotSplit('infoCenter', '-'));

            $('#partnerNo').val($('#partnerNo_1').val() + "-" + $('#partnerNo_2').val() + "-" + $('#partnerNo_3').val());

            for(var i=0; i < affiliate.mngCnt; i++) {
                $('#mngMobile' + i).val($.jUtil.valNotSplit('mngMobile' + i, '-'));
                $('#mngPhone' + i).val($.jUtil.valNotSplit('mngPhone' + i, '-'));
            }

            var affiliateForm = $('#affiliateForm').serializeArray();
            affiliateForm.push({name:'isMod', value: affiliate.isMod}); // 수정여부
            affiliateForm[0].value = 'affi' + affiliateForm[0].value;

            CommonAjax.basic({
                url : "/partner/setAffiliate.json",
                data : affiliateForm,
                method : "POST",
                callbackFunc : function (res) {
                    alert(res.returnMsg);
                    affiliate.resetForm(true);
                    affiliate.searchFormReset();
                    affiliate.search();
                }
            });
        }
    },
    /**
     * 판매자 회원상태 > 회원상태 팝업
     */
    getPartnerStatusPop : function() {
        if (!$.jUtil.isEmpty($("#partnerId").val())) {
            windowPopupOpen("/partner/popup/getPartnerStatusPop?partnerId="
                + 'affi' + $("#partnerId").val() + "&callback="
                + "affiliate.callbackPartnerStatus" + "&partnerType="
                + "affiliate"
                , "partnerStatus", "780", "700", "yes", "no");
        }
    },
    /**
     * 판매자 사업자정보 > 사업자 등록번호 조회
     */
    getPartnerNoPop : function() {
        windowPopupOpen("/partner/popup/getPartnerNoPop?partnerType=AFFI&callback=" + "affiliate.callbackPartnerNoChk"
            , "partnerNocheck", "480", "183", "yes", "no");
    },
    /**
     * 판매자 사업자정보 > 사업자 정보 입력 ( 콜백 )
     */
    callbackPartnerNoChk : function(data) {
        $('#partnerNm').val(data.partnerNm);
        $('#partnerOwner').val(data.partnerOwner);
        $('#partnerNo_1').val(data.partnerNo.substring(0, 3));
        $('#partnerNo_2').val(data.partnerNo.substring(3, 5));
        $('#partnerNo_3').val(data.partnerNo.substring(5));
    },
    /**
     * PW 관리 팝업창
     */
    getPartnerPwPop : function () {
        if(!affiliateListGrid.isSelectedAffiliate) {
            alert('업체를 먼저 선택해 주시기 바랍니다.');
            return false;
        }
        windowPopupOpen("/partner/popup/getAffiliatePwPop?partnerId=" + 'affi' + $("#partnerId").val() + "&businessNm=" + $("#partnerNm").val()
            , "sendPartnerInfo", "460", "400", "yes", "no");
    },
    /**
     * 종목 조회
     */
    getBizCateCd : function() {
        CommonAjax.basic({
            url: "/partner/getBizCateCdList.json",
            data: {
                codeNm: $("#bizCateCdNm").val()
            },
            method: "GET",
            callbackFunc: function (res) {
                var resultCount = res.length;

                if (resultCount == 0) {
                    $("#searchCategory").html("");
                    $("#searchCategory").append(
                        "<li class='ui-menu-item-wrapper'>검색결과가 없습니다.</li>");
                } else {
                    $("#searchCategory").html("");

                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];

                        $("#searchCategory").append(
                            "<li class='ui-menu-item-wrapper' id='categorySearchList_"
                            + data.mcCd + "'>" + data.mcNm + "</li>");
                    }
                }
                $("#searchCategory").show();
            }
        });
    },
    /**
     * 종목 추가
     * @param obj
     */
    addSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];
        var categoryCdNm = $(obj).html();

        affiliate.addSchCategoryHtml(categoryCd, categoryCdNm);
        $("#searchCategory").hide();
    },
    /**
     * 종목 추가 html
     * @param categoryCd
     * @param categoryCdNm
     * @returns {boolean}
     */
    addSchCategoryHtml : function(categoryCd, categoryCdNm) {
        $('#bizCateCd').val(categoryCd);
        $('#bizCateCdNm').val(categoryCdNm);

        if ($("#bizCateCdDiv_" + categoryCd).length > 0) {
            $("#bizCateCdDiv_" + categoryCd).find(".categorySetDtoUseYn").val("Y");
            $("#bizCateCdDiv_" + categoryCd).show();
        } else {
            var html = "<span id='bizCateCdDiv_" + categoryCd + "' class='text-normal'>";
            html += "<span class='mg-r-10 mg-l-10 mg-t-10'>" + categoryCdNm + "</span>";
            html += "<button type='button' class='ui button small delete-thumb' id='categoryDelete_" + categoryCd + "'>x</button>";
            html += "</span>";

            $("#bizCateCdListId").html(html);
        }
    },
    /**
     * 종목 삭제
     * @param obj
     */
    delSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];

        $("#bizCateCdDiv_" + categoryCd).find(".categorySetDtoUseYn").val("N");
        $("#bizCateCdDiv_" + categoryCd).hide();
    }
};

affiliate.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#schRegStartDate");
        var endDt = $("#schRegEndDate");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            affiliate.initSearchDate('-1y');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 검색어 체크
        var schKeyword = $('#schKeyword').val();

        if (schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (schKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        return true;
    },
    set : function() {
        var isValid = true;
        var checkObj = {
            affiliateNm: {   //판매업체명
                types: ['KOR', 'ENG', 'NUM'],
                min: 2,
                max: 50
            },
            partnerOwner: { //대표자명
                types: ['KOR', 'ENG'],
                min: 2,
                max: 50
            },
            businessConditions: { //업태
                types: ['KOR', 'ENG'],
                min: 2,
                max: 50
            }
        };

        var partnerId = $('#partnerId').val();
        if(partnerId.includes('affi')) {
            alert("affi가 포함된 ID는 사용 할 수 없습니다.");
            isValid = false;
        }
        if(!affiliateListGrid.isSelectedAffiliate) {
            if (partnerId.length < 1 || partnerId.length > 8) {
                alert("1~8자의 영문 소문자, 숫자만 사용가능합니다.");
                isValid = false;
            }
        }

        for (var id in checkObj) {
            var checkVal = $('#' + id).val();
            if (!$.jUtil.isAllowInput(checkVal, checkObj[id].types) || checkVal.length < checkObj[id].min || checkVal.length > checkObj[id].max) {
                alert(checkObj[id].min + "~" + checkObj[id].max + "자의 " + $.jUtil.getAllowInputMessage(checkObj[id].types));
                $('#' + id).focus();
                isValid = false;
                break;
            }
        }

        if($.jUtil.isEmpty($('#affiliateType').val())) {
            alert('제휴 유형을 선택해 주세요.');
            $('#affiliateType').focus();
            return false;
        }

        if (!isValid) {
            return false;
        } else {
            return true;
        }
    },
    /**
     * 판매자 기본정보 > 아이디 중복확인
     */
    checkPartnerId : function() {
        let partnerId = $('#partnerId').val();

        if ($.jUtil.isEmpty(partnerId)) {
            alert('사용할 아이디를 입력해주세요.');
            return;
        }

        if(partnerId.includes('affi')) {
            alert("affi가 포함된 ID는 사용 할 수 없습니다.");
            $("#partnerId").val('').focus();
            return;
        }

        if (!$.jUtil.isAllowInput(partnerId, ['ENG_LOWER', 'NUM']) || partnerId.length < 1 || partnerId.length > 8) {
            alert("1~8자의 영문 소문자, 숫자만 사용가능합니다.");
            $("#partnerId").focus();
            return;
        }

        partnerId = 'affi' + partnerId;
        affiliate.isCheckPartnerId = false;

        CommonAjax.basic({
            url: "/partner/getAffiliateIdCheck.json",
            data: { partnerId: partnerId, partnerType: 'AFFI' },
            method: "GET",
            callbackFunc: function (res) {
                if (res.returnCnt == 0) {
                    affiliate.isCheckPartnerId = true;
                    alert("사용 가능한 아이디입니다.");
                    $('#partnerId').prop('readonly', true);
                }
            },
            errorCallbackFunc : function(resError) {
                alert(resError.responseJSON.returnMessage);
                $("#partnerId").val("").prop('readonly', true).focus();
            }
        });
    },
};