/**
 * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
 */
$(document).ready(function() {
    sellerShopListGrid.init();
    sellerShop.init();
    CommonAjaxBlockUI.global();
});

// sellerShop 그리드
var sellerShopListGrid = {
  gridView : new RealGridJS.GridView("sellerShopListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
      sellerShopListGrid.initGrid();
      sellerShopListGrid.initDataProvider();
      sellerShopListGrid.event();
  },
  initGrid : function() {
      sellerShopListGrid.gridView.setDataSource(sellerShopListGrid.dataProvider);
      sellerShopListGrid.gridView.setStyles(sellerShopListGridBaseInfo.realgrid.styles);
      sellerShopListGrid.gridView.setDisplayOptions(sellerShopListGridBaseInfo.realgrid.displayOptions);
      sellerShopListGrid.gridView.setColumns(sellerShopListGridBaseInfo.realgrid.columns);
      sellerShopListGrid.gridView.setOptions(sellerShopListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
      sellerShopListGrid.dataProvider.setFields(sellerShopListGridBaseInfo.dataProvider.fields);
      sellerShopListGrid.dataProvider.setOptions(sellerShopListGridBaseInfo.dataProvider.options);
  },
  event : function() {
      // 그리드 선택
      sellerShopListGrid.gridView.onDataCellClicked = function(gridView, index) {
          sellerShop.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
      sellerShopListGrid.dataProvider.clearRows();
      sellerShopListGrid.dataProvider.setRows(dataList);
  }
};

// sellerShop 관리
var sellerShop = {
    isCheckShopNm : false // 중복확인 여부 ( 셀러샵 이름 )
    , isCheckShopUrl : false // 중복확인 여부 ( 셀러샵 URL )
    /**
     * 초기화
     */
    , init : function() {
        this.bindingEvent();
        this.initSearchDate('-10d');
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        sellerShop.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        sellerShop.setDate.setEndDate(0);
        sellerShop.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(sellerShop.search); //sellerShop 검색
        $('#searchResetBtn').bindClick(sellerShop.searchFormReset); //sellerShop 검색폼 초기화
        $('#setBtn').bindClick(sellerShop.set); //sellerShop 등록,수정
        $('#resetBtn').bindClick(sellerShop.resetForm); //sellerShop 입력폼 초기화
        $('#shopInfo').calcTextLength('keyup', '#shopInfoCnt');

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            sellerShop.img.addImg();
        });

        // 등록 > 프로필 이미지 삭제
        $('#profileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                sellerShop.img.deleteImg('profile');
            }
        });

        // 등록 > 로고 이미지 삭제
        $('#logoDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                sellerShop.img.deleteImg('logo');
            }
        });
    },
    /**
     * sellerShop 검색
     */
    search : function() {
        if(!sellerShop .valid.search()){
            return;
        }

        CommonAjax.basic({
            url : '/partner/getSellerShopList.json?' + $('#sellerShopSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                sellerShop.resetForm();
                sellerShopListGrid.setData(res);
                $('#sellerShopTotalCount').html($.jUtil.comma(sellerShopListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        sellerShop.initSearchDate('-10d');
        $('#searchUseYn, #searchType, #searchKeyword').val('');
    },
    /**
     * Url 정보 생성
     */
    setHttpUrl : function(data) {
        $('#pcHttpUrl, #mobileHttpUrl').text(data);
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = sellerShopListGrid.dataProvider.getJsonRow(selectRowId);

        $('#schPartnerId').val(rowDataJson.partnerId);
        $('#partnerId').val(rowDataJson.partnerId);
        $('#schBusinessNm').val(rowDataJson.businessNm);
        $('#schPartnerNm').val(rowDataJson.partnerNm);
        $('#btn_schPartner').hide();


        sellerShop.img.setImg('profile', {
            'imgUrl': rowDataJson.shopProfileImg,
            'src'   : hmpImgUrl + "/provide/view?processKey=SellerProfileImage&fileId=" + rowDataJson.shopProfileImg,
            'changeYn' : 'Y'
        });

        sellerShop.img.setImg('logo', {
            'imgUrl': rowDataJson.shopLogoImg,
            'src'   : hmpImgUrl + "/provide/view?processKey=SellerLogoImage&fileId=" + rowDataJson.shopLogoImg,
            'changeYn' : 'Y'
        });

        sellerShop.isCheckShopNm = false;
        $('#shopNm').attr('readonly', false);
        $('#shopNm').val(rowDataJson.shopNm);
        sellerShop.isCheckShopUrl = false;
        $('#shopUrl').attr('readonly', false);
        $('#shopUrl').val(rowDataJson.shopUrl);
        $('#pcHttpUrl, #mobileHttpUrl').text(rowDataJson.shopUrl);
        $('#shopInfo').val(rowDataJson.shopInfo);
        $('#shopInfoCnt').html(rowDataJson.shopInfo.length);
        $('input[name="csUseYn"][value="' + rowDataJson.csUseYn + '"]').prop('checked', true);
        $('#csInfo').val(rowDataJson.csInfo);
        $('input[name="useYn"][value="' + rowDataJson.useYn + '"]').prop('checked', true);
    },
    /**
     * 판매업체 조회 팝업창
     */
    getPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback="
            + callBack + "&partnerType="
            + "SELL"
            , "partnerStatus", "1080", "600", "yes", "no");
    },
    /**
     * 판매업체 조회 팝업창 Callback
     */
    schPartner : function(res) {
        CommonAjax.basic({
            url : "/partner/getSellerShopItemCnt.json",
            data : { partnerId : res[0].partnerId },
            method : "GET",
            callbackFunc : function (data) {
                if(data.returnCnt > 19) { /* 정상적으로 세팅이 되는 경우 */
                    $('#schPartnerId').val(res[0].partnerId);
                    $('#partnerId').val(res[0].partnerId);
                    $('#schBusinessNm').val(res[0].businessNm);
                    $('#schPartnerNm').val(res[0].partnerNm);
                    $('#btn_schPartner').hide();
                } else if ( data.returnCnt == -999  ) {
                    alert('등록된 셀러샵이 있습니다.');
                    return;
                } else {
                    alert('해당 판매업체는 상품 개수 부족으로 선택하실 수 없습니다. 유효 상품 수 [' + data.returnCnt + '] 개');
                    return;
                }
            }
        });
    },
    /**
     * 셀러샵 이름 OR 셀러샵 URL 중복확인
     */
    validation : function(type) {
        const partnerId = $('#partnerId').val();
        if($.jUtil.isEmpty(partnerId)) {
            $('#schPartnerId').focus();
            alert('판매업체 정보를 입력해 주세요.');
            return;
        }

        let checkKeyword;
        switch (type) {
            case 'shopNm' :
                checkKeyword = $('#shopNm').val();
                if(!$.jUtil.isAllowInput(checkKeyword, ['ENG', 'NUM', 'KOR', 'SPC_1']) || checkKeyword.length > 10) {
                    alert("최대 10자리의 한글, 영문, 숫자, 특수문자 사용가능합니다.");
                    $("#shopNm").focus();
                    return;
                } else if(sellerShop.isCheckShopNm == true) {
                    alert("이미 중복 확인이 완료 되었습니다.");
                    return;
                }
                break;
            case 'shopUrl' :
                checkKeyword = $('#shopUrl').val();
                if(!$.jUtil.isAllowInput(checkKeyword, ['ENG_LOWER', 'NUM', 'NOT_SPACE']) || checkKeyword.length > 12) {
                    alert("최대 12자리의 영문 소문자, 숫자만 사용가능합니다.");
                    $("#shopUrl").focus();
                    return;
                } else if(sellerShop.isCheckShopUrl == true) {
                    alert("이미 중복 확인이 완료 되었습니다.");
                    return;
                }
                break;
        }

        CommonAjax.basic({
            url : "/partner/setSellerShopValidation.json",
            data : { type : type, checkKeyword : encodeURI(checkKeyword), partnerId : partnerId },
            method : "GET",
            callbackFunc : function (res) {
                switch (type) {
                    case 'shopNm' :
                        if (res.returnCnt == 0) {
                            sellerShop.isCheckShopNm = true;
                            $('#shopNm').attr('readonly', true);
                            alert("사용 가능합니다.");
                        } else {
                            alert("이미 사용 중인 셀러샵 이름(닉네임) 입니다.");
                        }
                        break;
                    case 'shopUrl' :
                        if (res.returnCnt == 0) {
                            sellerShop.isCheckShopUrl = true;
                            $('#shopUrl').attr('readonly', true);
                            alert("사용 가능합니다.");
                        } else {
                            alert("이미 사용 중인 셀러샵 URL 입니다.");
                        }
                        break;
                }
            }
        });
    },
    /**
     * sellerShop 등록/수정
     */
    set : function() {
        if(!sellerShop.valid.set()) {
            return;
        }

        if($('#shopLogoImgUrl')) {
            $('#shopLogoYn').val('Y');
        } else {
            $('#shopLogoYn').val('N');
        }

        CommonAjax.basic({
            url:'/partner/setSellerShop.json',
            data : JSON.stringify($('#sellerShopSetForm').serializeObject())
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                sellerShop.resetForm();
                sellerShop.searchFormReset();
                sellerShop.search();
            }
        });
    },
    /**
     * sellerShop 입력/수정 폼 초기화
     */
    resetForm : function() {
        sellerShop.isCheckShopNm = false;
        sellerShop.isCheckShopUrl = false;
        $('#partnerId, #schPartnerId, #schBusinessNm, #schPartnerNm, #shopNm, #shopUrl, #shopInfo, #csInfo').val('');
        $('#shopInfoCnt').html('0');
        $('#btn_schPartner').show();
        $('#shopNm').attr('readonly', false);
        $('#shopUrl').attr('readonly', false);
        $('#pcHttpUrl, #mobileHttpUrl').text('');
        $('input[name="csUseYn"][value="N"]').prop('checked', true);
        $('input[name="useYn"][value="Y"]').prop('checked', true);
        sellerShop.img.init();
    }
};
    /**
     * sellerShop 검색,입력,수정 validation check
     */
sellerShop.valid = {
    search : function () {
        const startDt = $("#searchStartDt");
        const endDt = $("#searchEndDt");
        const searchKeyword = $("#searchKeyword").val();
        const searchType = $("#searchType").val();

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        if(!$.jUtil.isEmpty(searchType) && searchKeyword.length < 2) {
            $('#searchKeyword').focus();
            alert('검색 키워드는 2자 이상 입력해주세요.');
            return false;
        }
        if(searchKeyword != "") {
            if($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if(searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
      return true;
    },
    set : function () {
        if($.jUtil.isEmpty($('#partnerId').val())) {
            $('#schPartnerId').focus();
            alert('판매업체 정보를 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#shopNm').val())) {
            $('#shopNm').focus();
            alert('셀러샵 이름을 입력해 주세요.');
            return false;
        } else {
            if(!sellerShop.isCheckShopNm) {
                $('#shopNm').focus();
                alert('중복 체크를 진행해 주세요.');
                return false;
            }
        }
        if($.jUtil.isEmpty($('#shopUrl').val())) {
            $('#shopUrl').focus();
            alert('셀러샵 URL 입력해 주세요.');
            return false;
        } else {
            if(!sellerShop.isCheckShopUrl) {
                $('#shopUrl').focus();
                alert('중복 체크를 진행해 주세요.');
                return false;
            }
        }
        if($("input:radio[name='csUseYn']:checked").val() == 'Y' && $.jUtil.isEmpty($('#csInfo').val())) {
            $('#csInfo').focus();
            alert('고객센터 정보를 입력해 주세요.');
            return false;
        }
      return true;
    }
};

/**
 * 셀러샵관리 이미지
 */
sellerShop.img = {
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        sellerShop.img.setImg('profile', null);
        sellerShop.img.setImg('logo', null);
        commonEditor.inertImgurl = null;
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'profile':
                if (params) {
                    $('#profileImgUrlTag').prop('src', params.src);
                    $('#shopProfileImg').val(params.imgUrl);
                    $('#profileChangeYn').val(params.changeYn);

                    $('.profileDisplayView').show();
                    $('.profileDisplayView').siblings('.profileDisplayReg').hide();
                } else {
                    $('#profileImgUrlTag').prop('src', '');
                    $('#shopProfileImg').val('');
                    $('#profileChangeYn').val('N');

                    $('.profileDisplayView').hide();
                    $('.profileDisplayView').siblings('.profileDisplayReg').show();
                }
                break;
            case 'logo':
                if (params) {
                    $('#logoImgUrlTag').prop('src', params.src);
                    $('#shopLogoImg').val(params.imgUrl);
                    $('#logoChangeYn').val(params.changeYn);

                    $('.logoDisplayView').show();
                    $('.logoDisplayView').siblings('.logoDisplayReg').hide();
                } else {
                    $('#logoImgUrlTag').prop('src', '');
                    $('#shopLogoImg').val('');
                    $('#logoChangeYn').val('N');

                    $('.logoDisplayView').hide();
                    $('.logoDisplayView').siblings('.logoDisplayReg').show();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#imageFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#imageFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#imageFile');
        let ext = $('#imageFile').get(0).files[0].name.split('.');
        let params = {
            processKey : 'SellerProfileImage',
            mode : 'IMG'
        };

        if(sellerShop.img.setImgType === 'logo') {
            params.processKey = 'SellerLogoImage';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        sellerShop.img.setImg(sellerShop.img.setImgType, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        let imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        sellerShop.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        sellerShop.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};