/**
 * 판매업체 관리/제조사관리 > 판매업체 관리 관리
 */
$(document).ready(function() {
    if (popYn != 'Y') {
        sellerListGrid.init();
    }
    shipPolicyListGrid.init();
    shipPolicy.init();
    CommonAjaxBlockUI.global();
});

var partnerSellerInfo = '';
var shipPolicyListGridNm = 'shipPolicyListGrid';

if (popYn != 'Y') {
    // 판매업체관리 그리드
    var sellerListGrid = {
        gridView : new RealGridJS.GridView("sellerListGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            sellerListGrid.initGrid();
            sellerListGrid.initDataProvider();
            sellerListGrid.event();
        },
        initGrid : function() {
            sellerListGrid.gridView.setDataSource(sellerListGrid.dataProvider);

            sellerListGrid.gridView.setStyles(sellerListGridBaseInfo.realgrid.styles);
            sellerListGrid.gridView.setDisplayOptions(sellerListGridBaseInfo.realgrid.displayOptions);
            sellerListGrid.gridView.setColumns(sellerListGridBaseInfo.realgrid.columns);
            sellerListGrid.gridView.setOptions(sellerListGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            sellerListGrid.dataProvider.setFields(sellerListGridBaseInfo.dataProvider.fields);
            sellerListGrid.dataProvider.setOptions(sellerListGridBaseInfo.dataProvider.options);
        },
        event : function() {
            sellerListGrid.gridView.onDataCellClicked = function(gridView, index) {
                shipPolicy.sellerGridRowSelect(index.dataRow, true);
            };
        },
        setData : function(dataList) {
            sellerListGrid.dataProvider.clearRows();
            sellerListGrid.dataProvider.setRows(dataList);
        }
    };
} else {
    if ($.jUtil.isEmpty(partnerSeller)) {
        window.close();
    }

    partnerSellerInfo = JSON.parse(partnerSeller);
    shipPolicyListGridNm = 'shipPolicyListGridPop';
    $('#setShipPolicyBtn').closest('span').append('<button type="button" class="ui button xlarge white font-malgun" onclick="window.close();">닫기</button>');
}

//배송정책 그리드
var shipPolicyListGrid = {

    gridView : new RealGridJS.GridView(shipPolicyListGridNm),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        shipPolicyListGrid.initGrid();
        shipPolicyListGrid.initDataProvider();
        shipPolicyListGrid.event();
    },
    initGrid : function () {
        shipPolicyListGrid.gridView.setDataSource(shipPolicyListGrid.dataProvider);

        shipPolicyListGrid.gridView.setStyles(shipPolicyListGridBaseInfo.realgrid.styles);
        shipPolicyListGrid.gridView.setDisplayOptions(shipPolicyListGridBaseInfo.realgrid.displayOptions);
        shipPolicyListGrid.gridView.setColumns(shipPolicyListGridBaseInfo.realgrid.columns);
        shipPolicyListGrid.gridView.setOptions(shipPolicyListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function () {
        shipPolicyListGrid.dataProvider.setFields(shipPolicyListGridBaseInfo.dataProvider.fields);
        shipPolicyListGrid.dataProvider.setOptions(shipPolicyListGridBaseInfo.dataProvider.options);
    },
    event : function () {
        shipPolicyListGrid.gridView.onDataCellClicked = function(gridView, index) {
            shipPolicy.shipPolicyGridRowSelect(index.dataRow);
        };
    },
    setData : function (dataList) {
        shipPolicyListGrid.dataProvider.clearRows();
        shipPolicyListGrid.dataProvider.setRows(dataList);
    }
};

// 배송정책관리
shipPolicy = {
    /**
     * 초기화
     */
    init: function () {
        this.resetForm();
        this.bindingEvent();
        this.resetSearchForm();
        shipPolicy.initSearchDate("-700d");

        if (popYn == 'Y') {
            shipPolicy.sellerGridRowSelect(null, false,  partnerSellerInfo.partnerId, 0, partnerSellerInfo.partnerNm);
        }
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate: function (flag) {
        shipPolicy.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');
        shipPolicy.setDate.setEndDate(0)
        shipPolicy.setDate.setStartDate(flag);
        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent: function () {

        $('#shipFee, #claimShipFee, #extraIslandFee, #extraJejuFee, #diffQty, #freeCondition').allowInput("keyup", ["NUM"]);

        $('#searchResetBtn').bindClick(shipPolicy.resetSearchForm);
        $('#searchBtn').bindClick(shipPolicy.searchSeller);

        $('#resetBtn').bindClick(shipPolicy.resetFormConfirm);
        $('#setShipPolicyBtn').bindClick(shipPolicy.setShipPolicy);
        $("#shipArea").bindChange(shipPolicy.changeShipArea);

        $("input[id^=samePartnerAddr]").bindClick(shipPolicy.setSamePartnerAddr);
        $("input[id^=sameReleaseAddr]").bindClick(shipPolicy.setSameReleseAddr);

        $("input[name='shipKind']").on('change', function() {
            if ( $('#storeId').val() == '' || $('#storeId').val() == "0" ) {
                shipPolicy.changeShipKindDS($(this).val());
            }else{
                shipPolicy.changeShipKindTD($(this).val());
            }
        });

        $('.shipMethod').on('change', function() {
            $('#shipMethod').val($(this).val());
        });

        $("input[name='shipType']").on('change', function() {
            if ( $('#storeId').val() == '' || $('#storeId').val() == "0" ) {
                shipPolicy.changeShipTypeDS($(this).val());
            }
        });

        $("input[name='diffYn']").on('change', function() {
            if ( $('#storeId').val() == '' || $('#storeId').val() == "0" ) {
                shipPolicy.changeDiffYn($(this).val(), true );
            }else {
                shipPolicy.changeDiffYn($(this).val(), false );
            }
        });

        $("input[name='schPartnerType']").bindChange(shipPolicy.changeShipMethod);

        //출고기한 변경 이벤트
        $('#releaseDay').on('change', function(){
            shipPolicy.getReleaseDay($(this).val());
        });
    },
    /**
     * 배송정책 초기화 Confirm
     * @returns {boolean}
     */
    resetFormConfirm : function () {

        if(!confirm("초기화 후 신규정책으로 등록이 가능합니다. 입력정보를 초기화하시겠습니까?")) {
            return false;
        }
        shipPolicy.resetForm();
    },
    /**
     * 배송정책 등록 초기화
     */
    resetForm: function () {

        $('#shipPolicyNo').val('');
        $('#shipPolicyNoInfo').text('');

        //배송정책
        $('#shipPolicyNm, #shipMethod, #shipFee, #freeCondition, #diffQty, #extraJejuFee, #extraIslandFee').val('');

        //배송추가정보
        $('#claimShipFee, #releaseZipcode, #releaseAddr1, #releaseAddr2, #returnAddr1, #returnAddr2, #returnZipcode').val('');

        //radio
        $('input:radio[name="shipType"]:input[value="BUNDLE"]').trigger('click');
        $('input:radio[name="shipKind"]:input[value="FREE"]').trigger('click');
        $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click'); //배송비결제방식
        $('input:radio[name="dispYn"]:input[value="Y"]').trigger('click');
        $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');

        $("input:checkbox[id='defaultYn']").prop("checked", false);
        $("input:checkbox[id='samePartnerAddr']").prop("checked", false);
        $("input:checkbox[id='sameReleaseAddr']").prop("checked", false);
        $("input:checkbox[id='holidayExceptYn']").prop("checked", false);

        $('#useYn').val("Y");           //사용여부
        $('#safeNumberUseYn').val("Y")  //안심번호 사용여부
        $('#releaseDay').val('')        //출고기한
        $('#releaseTime').val('')       //출고시간

        //타입 별 등록 항목 초기화
        shipPolicy.changeShipKindDS("FREE");

        //배송가능지역
        $('#shipArea').val("ALL")
        shipPolicy.changeShipArea();

        //출고기한
        $('#releaseDay').val('');
        $('#releaseTime').val('');

        shipPolicy.getReleaseDay(0);

        $('#extraJejuFee, #extraIslandFee').prop("disabled", false);

        $('#releaseGibunAddrArea').hide();
        $('#returnGibunAddrArea').hide();

        //배송방법
        shipPolicy.changeShipMethod();
        $('.shipMethod').attr('disabled', false);

    },
    /**
     * 점포 조회
     * @param callBack
     * @returns {boolean}
     */
    getStorePop : function(callBack) {

        var partnerType= $('input[name="schPartnerType"]:checked').val();

        if ($.jUtil.isEmpty(partnerType)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        if ( partnerType == "SELLER"){
            alert("Seller(DS)는 점포조회가 불가합니다.");
            return false;
        }

        windowPopupOpen("/common/popup/storePop?callback="
            + callBack + "&storeType="
            + partnerType
            , "partnerStatus", "1100", "620", "yes", "no");

    },
    /**
     * 배송정책관리 > 검색영역 초기화
     */
    resetSearchForm: function () {

        shipPolicy.initSearchDate("-700d");

        $('#schPartnerStatus, #schPartnerGrade, #schOperatorType').val('');
        $('#schPartnerPeriodType').val("REGDT");
        $('#schKeyword, #schStoreId, #schStoreNm').val('')
        $('#schType').val("partnerNm");

        $('#partnerId').val('');
        $('#partnerInfo').text('');

        $('input:radio[name="schPartnerType"]:input[value="SELLER"]').trigger('click');

    },
    /**
     * 판매자 그리드 row 선택
     */
    sellerGridRowSelect: function(selectRowId, isFormReset, partnerId, storeId, partnerNm) {

        if(isFormReset) {
            shipPolicy.resetForm();
        }

        var storeNm = '';
        if (selectRowId != null) {
            var rowDataJson = sellerListGrid.dataProvider.getJsonRow(selectRowId);

            partnerId = rowDataJson.partnerId;
            partnerNm = rowDataJson.partnerNm;

            storeId = rowDataJson.storeId;
            storeNm = rowDataJson.storeNm
        }

        $('#partnerId').val(partnerId);
        $('#partnerInfo').text( partnerId +" | " + partnerNm);

        $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');       //수량별차등
        $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click'); //선결제여부

        if ($.jUtil.isEmpty(storeId)) {
            //DS
            $('#storeId').val('0');
            $('#storeInfo').text('DS');
        }else {
            //TD
            $('#storeId').val(storeId);
            $('#storeInfo').text(storeId + " | " + storeNm);

            //기본값 셋팅
            $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
            $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');

            //수량별차등,착불 불가
            $('#prepaymentYnN').prop("disabled", true);
            $('#diffYnY').prop("disabled", true);
        }

        CommonAjax.basic({
            url: '/partner/getShipPolicyList.json?',
            data: { partnerId: partnerId, storeId:storeId },
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                shipPolicyListGrid.setData(res);
                $('#shipPolicyTotalCount, #shipPolicyTotalCountPop').html($.jUtil.comma(shipPolicyListGrid.gridView.getItemCount()));
                shipPolicy.changeShipMethod();
            }
        });

    },
    /**
     * 배송정책 그리드 row 선택
     * @param selectRowId
     */
    shipPolicyGridRowSelect: function (selectRowId) {

        shipPolicy.resetForm();

        var shipPolicyRowDataJson = shipPolicyListGrid.dataProvider.getJsonRow(selectRowId);

        $('#shipPolicyNoInfo').text(shipPolicyRowDataJson.shipPolicyNo); //배송정책번호
        $('#shipPolicyNo').val(shipPolicyRowDataJson.shipPolicyNo); //배송정책번호

        $('#shipPolicyNm').val(shipPolicyRowDataJson.shipPolicyNm); //배송정보명

        $('.shipMethod').hide().val('');
        $('.shipMethod').attr('disabled', true);

        if ( $('#storeId').val() == '' || $('#storeId').val() == "0" ) {
            //DS
            $('#shipMethodDS').val(shipPolicyRowDataJson.shipMethod); //배송방법
            $('#shipMethodDS').show();
            $('input:radio[name="prepaymentYn"]:input[value="'+shipPolicyRowDataJson.prepaymentYn+'"]').trigger('click'); //선결제여부
        } else {
            //TD
            $('#shipMethodTD').val(shipPolicyRowDataJson.shipMethod); //배송방법
            $('#shipMethodTD').show();
            $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click'); //선결제여부
        }
        $('#shipMethod').val(shipPolicyRowDataJson.shipMethod);

        $('input:radio[name="shipType"]:input[value="'+shipPolicyRowDataJson.shipType+'"]').trigger('click'); //배송유형
        $('input:radio[name="shipKind"]:input[value="'+shipPolicyRowDataJson.shipKind+'"]').trigger('click'); //배송비종류
        $('#shipFee').val(shipPolicyRowDataJson.shipFee); //배송비
        $('#freeCondition').val(shipPolicyRowDataJson.freeCondition); //무료조건비

        $('input:radio[name="diffYn"]:input[value="'+shipPolicyRowDataJson.diffYn+'"]').trigger('click'); //차등여부
        $('#diffQty').val(shipPolicyRowDataJson.diffQty > 0 ? shipPolicyRowDataJson.diffQty : ''); //수량별차등



        $('#shipArea').val(shipPolicyRowDataJson.shipArea); //배송가능지역

        $('#extraJejuFee').val(shipPolicyRowDataJson.extraJejuFee);
        $('#extraIslandFee').val(shipPolicyRowDataJson.extraIslandFee);

        $('input:radio[name="dispYn"]:input[value="'+shipPolicyRowDataJson.dispYn+'"]').trigger('click');

        $('#safeNumberUseYn').val(shipPolicyRowDataJson.safeNumberUseYn);
        $('#useYn').val(shipPolicyRowDataJson.useYn);

        //배송비추가정보
        $('#claimShipFee').val(shipPolicyRowDataJson.claimShipFee);
        $('#releaseZipcode').val(shipPolicyRowDataJson.releaseZipcode);
        $('#releaseAddr1').val(shipPolicyRowDataJson.releaseAddr1);
        $('#releaseAddr2').val(shipPolicyRowDataJson.releaseAddr2);
        $('#returnZipcode').val(shipPolicyRowDataJson.returnZipcode);
        $('#returnAddr1').val(shipPolicyRowDataJson.returnAddr1);
        $('#returnAddr2').val(shipPolicyRowDataJson.returnAddr2);

        $('#releaseDay').val(shipPolicyRowDataJson.releaseDay);
        $('#releaseTime').val(shipPolicyRowDataJson.releaseTime);
        shipPolicy.getReleaseDay(shipPolicyRowDataJson.releaseDay);

        $("input:checkbox[id='holidayExceptYn']").prop("checked", shipPolicyRowDataJson.holidayExceptYn == "Y" ? true : false);
        $("input:checkbox[id='defaultYn']").prop("checked", shipPolicyRowDataJson.defaultYn == "Y" ? true : false);

        //타입 별 등록 항목 초기화
        shipPolicy.changeShipArea();
    },
    /**
     * 배송방법 선택
     */
    changeShipMethod : function() {

        $('.shipMethod').hide().val('');

        var id = $("#storeId").val();

        if($.jUtil.isEmpty(id) || "0" == id )  {
            $('#shipMethodDS').show();
        } else {
            $('#shipMethodTD').show();
        }

    },
    /**
     * 출고기한 날짜 선택
     * @param val
     */
    getReleaseDay : function(val) {

        if (val != 1) {
            //오늘발송 외 기타
            $("#releaseDetailInfo").hide();
            $('#releaseTextInfo').show();
            $('#shipReleaseTimeSpanId').css('display', 'none');
            $('#releaseTime').val('');

        }else{
            //오늘발송
            $('#releaseDetailInfo').show();
            $('#releaseTextInfo').hide();
            $('#shipReleaseTimeSpanId').css('display', 'display');
        }

    },
    /**
     * 판매자 조회
     * @returns {boolean}
     */
    searchSeller: function () {

        if(!shipPolicy.valid.search()){
            return false;
        }

        //partner 정보 초기화
        var form = $('#shipPolicySearchForm').serializeObject();
        CommonAjax.basic({
            url: '/partner/getPartnerList.json',
            data: JSON.stringify(form),
            method: "post",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                shipPolicy.resetForm();
                sellerListGrid.setData(res);

                $('#sellerTotalCount').html($.jUtil.comma(sellerListGrid.gridView.getItemCount()));
                $('#partnerInfo').text('');
                shipPolicyListGrid.dataProvider.clearRows(); //배송정책 조회창 초기화
            }
        });

    },
    /**
     * 배송정책 등록/수정
     */
    setShipPolicy: function () {

        //valid
        if ( $('#storeId').val() == '' || $('#storeId').val() == "0" ) {
            if(!shipPolicy.valid.setSeller()){
                return false;
            }
        }else{
            if(!shipPolicy.valid.setHome()){
                return false;
            }
        }

        var form =  $('#shipPolicyForm').serializeObject(true);

        CommonAjax.basic({
            url: "/partner/setShipPolicy.json",
            method: "POST",
            contentType: 'application/json',
            data: JSON.stringify(form),
            callbackFunc: function (res) {
                alert(res.returnMsg);

                $('#shipPolicyNoInfo').text(res.returnKey);
                $('#shipPolicyNo').val(res.returnKey);

                $('.shipMethod').attr('disabled', true);

                if (popYn == 'Y') {
                    shipPolicy.sellerGridRowSelect(null, false, partnerSellerInfo.partnerId, 0, partnerSellerInfo.partnerNm);
                    if (partnerSellerInfo.partnerId == $('#partnerId', opener.document).val() && typeof opener.item.ship.getSellerShipList == 'function') {
                        opener.item.ship.getSellerShipList(partnerSellerInfo.partnerId, $('#shipPolicyNo', opener.document).val());
                    }
                } else {
                    shipPolicy.sellerGridRowSelect(sellerListGrid.gridView.getCurrent().itemIndex, false);
                }
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 배송비종류 선택
     * @param shipKind
     */
    changeShipKindDS : function(shipKind) {
        switch (shipKind) {
            case "FREE" :
                $('#shipFee').prop('readonly', true);
                $('#freeCondition').prop('readonly', true);
                $('#shipFee , #freeCondition').val('');

                //기본값 셋팅
                $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
                $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');

                //수량별차등,착불 불가
                $('#prepaymentYnN').prop("disabled", true);
                $('#diffYnY').prop("disabled", true);
                break;

            case "FIXED" :

                $('#shipFee').prop('readonly', false);
                $('#freeCondition').prop('readonly', true);
                $('#prepaymentYnN').prop("disabled", false);
                $('#freeCondition').val('');

                var type = $('input[name="shipType"]:checked').val();
                if(type == "ITEM") {
                    $('#diffYnY').prop("disabled", false);
                }else{
                    //수량별차등설정 불가
                    $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
                    $('#diffYnY').prop("disabled", true);
                }
                break;

            case "COND" :
                $('#shipFee').prop('readonly', false);
                $('#freeCondition').prop('readonly', false);

                //기본값 셋팅
                $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
                $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');

                //수량 별 차등. 착불 불가
                $('#diffYnY').prop("disabled", true);
                $('#prepaymentYnN').prop("disabled", true);

                break;
        }
    },
    /**
     * 배송비유형 (무료/유료/선택적항목)
     * 1) 배송비
     * 2) 무료배송 조건
     */
    changeShipKindTD : function(shipKind) {

        switch (shipKind) {
            case "FREE" :
                $('#shipFee').prop('readonly', true);
                $('#freeCondition').prop('readonly', true);
                $('#shipFee , #freeCondition').val('');
                break;

            case "FIXED" :
                $('#shipFee').prop('readonly', false);
                $('#freeCondition').prop('readonly', true);
                $('#freeCondition').val('');

                break;

            case "COND" :
                $('#shipFee').prop('readonly', false);
                $('#freeCondition').prop('readonly', false);
                break;
        }
    },
    /**
     * 배송유형 변경
     * @param shipType
     */
    changeShipTypeDS : function (shipType) {

        switch (shipType) {
            case "ITEM" :
                var shipKind = $('input[name="shipKind"]:checked').val();
                if(shipKind == "FIXED") {
                    $('#diffYnY').prop("disabled", false);
                }
                break;

            case "BUNDLE" :
                $('input:radio[name="diffYn"]:input[value="N"]').trigger('click');
                $('#diffYnY').prop("disabled", true);
                break;
        }
    },
    /**
     * 수량별 차등 여부
     * 1) 수량별 차등 갯수
     */
    changeDiffYn :function(diffYn, isExtraDisable) {
        switch(diffYn) {
            case "Y" :
                $('#diffQty').prop('readonly' , false);
                if(isExtraDisable) {
                    //DS
                    $('input:radio[name="prepaymentYn"]:input[value="Y"]').trigger('click');
                    $('#prepaymentYnN').prop("disabled", true);
                    $('#extraJejuFee, #extraIslandFee').prop("disabled", true);
                }
                break;

            case "N" :
                $('#diffQty').prop('readonly' , true);
                $('#diffQty').val('');

                if(isExtraDisable) {
                    var shipKind = $('input[name="shipKind"]:checked').val();
                    if(shipKind == 'FIXED') {
                        //유료일때만 착불 활성화
                        $('#prepaymentYnN').prop("disabled", false);
                    }
                    $('#extraJejuFee, #extraIslandFee').prop("disabled", false);
                }
                break;
        }
    },
    /**
     * 배송가능지역(전국/도서산간지역제외)
     * 1) 제주추가 배송비
     * 2) 도서산간 추가배송비
     */
    changeShipArea : function() {

        var shipArea = $('#shipArea').val();

        switch(shipArea) {
            case "EXCLUDE" :
                $('#extraJejuFee').prop('readonly', true);
                $('#extraIslandFee').prop('readonly', true);
                $('#extraJejuFee , #extraIslandFee').val('');
                break;

            case "ALL" :
                $('#extraJejuFee').prop('readonly', false);
                $('#extraIslandFee').prop('readonly', false);
                break;

            default:
                $('#extraJejuFee').prop('readonly', true);
                $('#extraIslandFee').prop('readonly', true);
                $('#extraJejuFee , #extraIslandFee').val('');
                break;
        }
    },
    /**
     * 사업자주소와 동일적용
     * @param obj
     */
    setSamePartnerAddr : function ( obj ) {

        if($.jUtil.isEmpty($('#partnerId').val())) {
            return $.jUtil.alert('판매업체를 먼저 선택해주세요.');
        }

        if ($(obj).prop('checked')) {
            CommonAjax.basic({
                url: '/partner/getPartnerAddr.json?',
                data: { partnerId: $('#partnerId').val()},
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    $('#releaseZipcode').val(res.zipcode);
                    $('#releaseAddr1').val(res.addr1);
                    $('#releaseAddr2').val(res.addr2);
                }
            });
        }else {
            $('#releaseZipcode').val('');
            $('#releaseAddr1').val('');
            $('#releaseAddr2').val('');
        }
    },
    /**
     * 출하지주소와 동일적용
     * @param obj
     */
    setSameReleseAddr : function ( obj) {

        if ($(obj).prop('checked')) {
            $('#returnZipcode').val($('#releaseZipcode').val());
            $('#returnAddr1').val($('#releaseAddr1').val());
            $('#returnAddr2').val($('#releaseAddr2').val());
        }else{
            $('#returnZipcode').val('');
            $('#returnAddr1').val('');
            $('#returnAddr2').val('');
        }
    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setRelieaseZipcode : function (res) {
        $("input:checkbox[id='samePartnerAddr']").prop("checked", false);
        $("#releaseZipcode").val(res.zipCode);
        $("#releaseAddr1").val(res.roadAddr);
        $("#releaseAddr2").val('');
    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setReturnZipcode : function (res) {
        $("input:checkbox[id='sameReleaseAddr']").prop("checked", false);
        $("#returnZipcode").val(res.zipCode);
        $("#returnAddr1").val(res.roadAddr);
        $("#returnAddr2").val('');
    },
};

/**
 * 유효성 체크
 * @type {{set: shipPolicy.valid.set}}₩
 */
shipPolicy.valid = {

    search : function() {
        // 날짜 체크
        var startDt = $("#schStartDate");
        var endDt = $("#schEndDate");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            shipPolicy.initSearchDate('-700d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        //검색어
        var searchKeyword = $('#schKeyword').val();
        var searchType = $('#schType').val();

        if(searchKeyword != '') {

            switch (searchType) {
                case "partnerNo" :
                    if ( searchKeyword.length < 10 || searchKeyword.length > 10) {
                        alert('사업자등록번호 검색 시 사업자등록번호 전체를 입력해 주세요  ');
                        $('#schKeyword').focus();
                        return false;
                    }

                    if (!$.jUtil.isAllowInput(searchKeyword, ['NUM'])) {
                        alert('사업자등록번호 검색 시 - 를 제외한 숫자로만 입력해주세요');
                        $('#schKeyword').focus();
                        return false;
                    }
                    break;

                default:
                    if (searchKeyword.length < 2) {
                        alert("검색 키워드는 2자 이상 입력해주세요.");
                        $('#schKeyword').focus();
                        return false;
                    }
            }
        }
        return true;
    },
    setSeller : function() {

        if(!shipPolicy.valid.common()) {
            return false;
        }

        var shipKind = $('input[name="shipKind"]:checked').val();

        //배송유형(고정일경우)
        if("FIXED" == shipKind || "COND" == shipKind) {
            if($.jUtil.isEmpty($('#shipFee').val()) || $('#shipFee').val() == "0" ) {
                alert("배송비를 입력해주세요 (금액은 1 ~ 999,999까지 입력 가능합니다.)")
                $('#shipFee').focus();
                return false;
            } else {
                if (!$.jUtil.isAllowInput($("#shipFee").val(), ['NUM'])) {
                    alert("배송비는 숫자만 입력해주세요.");
                    return false;
                }
            }

        }

        //반품배송비
        if ($.jUtil.isEmpty($("#claimShipFee").val())) {
            alert("반품/교환배송비를 입력해주세요. (금액은 0 ~ 999,999까지 입력 가능합니다.)");
            return false;
        } else {
            if (!$.jUtil.isAllowInput($("#claimShipFee").val(), ['NUM'])) {
                alert("반품/교환배송비 숫자만 입력해주세요.");
                return false;
            }
        }

        //배송가능지역
        let diffYn = $('input[name="diffYn"]:checked').val();
        if ("ALL" == $('#shipArea').val() && diffYn == 'N') {
            if (!$.jUtil.isAllowInput($("#extraIslandFee").val(), ['NUM'])
                || !$.jUtil.isAllowInput($("#extraJejuFee").val(), ['NUM'])) {
                alert("도서산간 배송비는 숫자만 입력해주세요.");
                return false;
            }
        }

        if ($('input[name="diffYn"]:checked').val() == 'Y' && !$.jUtil.isAllowInput($("#diffQty").val(), ['NUM'])) {
            return $.jUtil.alert('수량별 차등수량은 숫자만 입력해주세요.', 'diffQty');
        }

        return true;
    },
    setHome : function() {

        if(!shipPolicy.valid.common()) {
            return false;
        }

        var shipKind = $('input[name="shipKind"]:checked').val() ;

        //배송유형(고정일경우)
        if ("FIXED" == shipKind || "COND" == shipKind) {
            if ($.jUtil.isEmpty($('#shipFee').val()) || $('#shipFee').val() == "0") {
                alert("배송비를 입력해주세요 (금액은 1000 ~ 4,000까지 입력 가능합니다.)")
                $('#shipFee').focus();
                return false;
            } else {
                if (!$.jUtil.isAllowInput($("#shipFee").val(), ['NUM'])) {
                    alert("배송비는 숫자만 입력해주세요.");
                    return false;
                }
            }
        }

        //반품배송비
        if ($.jUtil.isEmpty($("#claimShipFee").val())) {
            alert("반품/교환배송비를 입력해주세요. (금액은 0 ~ 4,000까지 입력 가능합니다.)");
            return false;
        } else {
            if (!$.jUtil.isAllowInput($("#claimShipFee").val(), ['NUM'])) {
                alert("반품/교환배송비 숫자만 입력해주세요.");
                return false;
            }
        }

        //배송가능지역
        if ("ALL" == $('#shipArea').val()) {
            if ($.jUtil.isEmpty($('#extraIslandFee').val())
                || $.jUtil.isEmpty($('#extraJejuFee').val())) {
                alert("도서산간 배송비를 입력해주세요 (금액은 0 ~ 4,000 까지 입력 가능합니다.)");
                $('#extraJejuFee').focus();
                return false;
            } else {
                if (!$.jUtil.isAllowInput($("#extraIslandFee").val(), ['NUM'])
                    || !$.jUtil.isAllowInput($("#extraJejuFee").val(), ['NUM'])) {
                    alert("도서산간 배송비는 숫자만 입력해주세요.");
                    return false;
                }
            }
        }

        if ($('input[name="diffYn"]:checked').val() == 'Y' && !$.jUtil.isAllowInput($("#diffQty").val(), ['NUM'])) {
            return $.jUtil.alert('수량별 차등수량은 숫자만 입력해주세요.', 'diffQty');
        }

        var arr = [$('#shipFee') ,$('#claimShipFee') ,$('#extraJejuFee'), $('#extraIslandFee')];

        if (! shipPolicy.valid.checkMultiple(1000, arr)) {
            return false;
        }

        return true;

    },
    /**
     * 점포 배송비 체크
     * @param multiple
     * @param arr
     * @returns {boolean}
     */
    checkMultiple : function (multiple, arr) {

        var maxFee = 4000; //4000원 이하로만 등록 가능
        var msg = "점포의 배송비 정책은 " + multiple + "원 단위로 최대 " + maxFee +"원 까지 입력 가능합니다.";

        for(var i =0; i < arr.length; i++) {
            var value ="";
            if(typeof arr[i] == 'object') {
                value = $(arr[i]).val();
                if (value % multiple != 0) {
                    alert(msg);
                    $(arr[i]).focus();
                    return false;
                }else {
                    if( value > maxFee) {
                        alert(msg);
                        $(arr[i]).focus();
                        return false;
                    }
                }
            }
        }
        return true;
    },

    common : function() {

        var shipPolicyNm = $('#shipPolicyNm').val();

        //배송정책명
        if ($.jUtil.isEmpty(shipPolicyNm)) {
            alert("배송정책명을 입력해주세요.");
            $('#shipPolicyNm').focus();
            return false;
        }

        if ($.jUtil.isEmpty($('#shipMethod').val())) {
            alert("배송방법을 선택해주세요.");
            $('.shipMethod').focus();
            return false;
        }


        //출하기한
        if ($.jUtil.isEmpty($('#releaseDay').val())) {
            alert("출고기한을 확인해주세요.");
            $('#releaseDay').focus();
            return false;
        }else{
            if($('#releaseDay').val() == "1") {
                if ($.jUtil.isEmpty($('#releaseTime').val())) {
                    alert("출고기한을 확인해주세요 ")
                    return false;
                }
            }
        }

        var shipKind = $('input[name="shipKind"]:checked').val();

        //배송유형 (조건부일경우)
        if ("COND" == shipKind) {
            if ($.jUtil.isEmpty($('#freeCondition').val()) || $('#freeCondition').val() == "0") {
                alert("무료배송 조건을 입력해주세요 (금액은 1 ~ 9,999,999까지 입력 가능합니다)")
                $('#freeCondition').focus();
                return false;
            } else{
                if (!$.jUtil.isAllowInput($("#freeCondition").val(), ['NUM'])) {
                    alert("무료배송 조건은 숫자만 입력해주세요.");
                    return false;
                }
            }
        }

        //배송정보
        if ($.jUtil.isEmpty($('#releaseZipcode').val())|| $.jUtil.isEmpty($('#releaseAddr1').val())){
            alert("출고지 정보를 입력해주세요.")
            return false;
        }

        if ($.jUtil.isEmpty($('#returnZipcode').val()) || $.jUtil.isEmpty($('#returnAddr1').val())){
            alert("회수지 정보를 입력해주세요.")
            return false;
        }

        return true;
    }
};

/**
 * Callback 영역
 * @type {{schOriginStore: store.callBack.schOriginStore, schFcStore: store.callBack.schFcStore}}
 */
shipPolicy.callBack = {
    schStore: function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    }
};


