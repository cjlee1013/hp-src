/**
 * 제휴업체관리 > 제휴채널관리
 */
$(document).ready(function() {
    affliChannelListGrid.init();
    affliChannel.init();
    CommonAjaxBlockUI.global();

});

// affliChannel 그리드
var affliChannelListGrid = {

  gridView : new RealGridJS.GridView("affliChannelListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),

  init : function() {

      affliChannelListGrid.initGrid();
      affliChannelListGrid.initDataProvider();
      affliChannelListGrid.event();

  },
  initGrid : function() {

      affliChannelListGrid.gridView.setDataSource(affliChannelListGrid.dataProvider);
      affliChannelListGrid.gridView.setStyles(affliChannelListGridBaseInfo.realgrid.styles);
      affliChannelListGrid.gridView.setDisplayOptions(affliChannelListGridBaseInfo.realgrid.displayOptions);
      affliChannelListGrid.gridView.setColumns(affliChannelListGridBaseInfo.realgrid.columns);
      affliChannelListGrid.gridView.setOptions(affliChannelListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {

      affliChannelListGrid.dataProvider.setFields(affliChannelListGridBaseInfo.dataProvider.fields);
      affliChannelListGrid.dataProvider.setOptions(affliChannelListGridBaseInfo.dataProvider.options);
  },
  event : function() {
      // 그리드 선택
      affliChannelListGrid.gridView.onDataCellClicked = function(gridView, index) {
          affliChannel.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
      affliChannelListGrid.dataProvider.clearRows();
      affliChannelListGrid.dataProvider.setRows(dataList);
  },
  delData : function(dataList){
    affliChannelListGrid.dataProvider.removeRows(dataList,false);
  },
  excelDownload: function() {
    if(affliChannelListGrid.gridView.getItemCount() == 0) {
        alert("검색 결과가 없습니다.");
        return false;
    }

    var _date = new Date();
    var fileName = "affliChannelList"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    affliChannelListGrid.gridView.exportGrid( {
        type: "excel",
        target: "local",
        fileName: fileName+".xlsx",
        showProgress: true,
        progressMessage: " 엑셀 데이터 추줄 중입니다."
    });

}

};

// affliChannel관리
var affliChannel = {
    isValidationByPartnerId : false,
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
        this.initSearchDate('-364d');
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        affliChannel.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        affliChannel.setDate.setEndDate(0);
        affliChannel.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * affliChannel 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#affliChannelSetForm').ajaxForm({
            url: '/',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                affliChannel.searchReset();
                affliChannel.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(affliChannel.search); //affliChannel 검색
        $('#searchResetBtn').bindClick(affliChannel.searchReset); //affliChannel 검색폼 초기화
        $('#setBtn').bindClick(affliChannel.set); //affliChannel 등록,수정
        $('#resetBtn').bindClick(affliChannel.reset); //affliChannel 입력폼 초기화
        $("#excelDownloadBtn").bindClick(affliChannelListGrid.excelDownload);

        $("#cookieHistBtn").on("click", function () {
            affliChannel.openPopup('cookie')
        });

        $("#commissionHistBtn").on("click", function () {
            affliChannel.openPopup('commissionRate')
        });

        $('#urlParameter').calcTextLength('keyup', '#urlParameterCnt');
        $('#channelNm').calcTextLength('keyup', '#channelNmCnt');


        $(document).on("click", "button[id=addSalesCd]", function(){

            var addSalesCd = $('#salesCd').val();
            if(!$.jUtil.isEmpty(addSalesCd)){
                affliChannel.addSalesCd(addSalesCd);
            } else{
                alert("매출코드를 입력해 주세요");
                return false;
            }

        });
        $(document).on("click", "button[id^=salesCdDelete_]", function(){
            affliChannel.delSalesCd(this);
        });

    },

    /**
     * affliChannel 검색
     */
    search : function() {
        if(!affliChannel .valid.search()){
            return false;
        
        }
        CommonAjax.basic({url:'/partner/getAffiliateChannelList.json?' + $('#affiliateChannelSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            affliChannel.reset();
            affliChannelListGrid.setData(res);
            $('#affliChannelTotalCount').html($.jUtil.comma(affliChannelListGrid.gridView.getItemCount()));
        }});

    },
    /**
     * 검색폼 초기화
     */
    searchReset : function() {
        affliChannel.initSearchDate('-364d');
        $('#searchUseYn, #searchSiteType, #searchKeyword').val('');
        $('#searchType').val('CHANNELNM');
    },
    /**
     * 변경이력팝업
     */
    openPopup : function(type) {
        var channelId = $("#channelId").val();
        if($.jUtil.isEmpty(channelId)) {
            return false;
        }
        var popupUrl = '';
        if(type === 'cookie') {
            popupUrl = "/partner/popup/affiliateChannelCookieTimeHistoryPop?channelId="+channelId;
        } else {
            popupUrl = "/partner/popup/affiliateChannelCommissionRateHistoryPop?channelId="+channelId;
        }

        $.jUtil.openNewPopup(popupUrl, 1120, 540);
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        affliChannel.reset();
        var rowDataJson = affliChannelListGrid.dataProvider.getJsonRow(selectRowId);

        $('#channelIdTxt').html(rowDataJson.channelId);
        $('#channelId'). val(rowDataJson.channelId);
        $('#channelNm').val(rowDataJson.channelNm);
        $('#channelNmCnt').html(rowDataJson.channelNm.length);
        $('#cookieTime').val(rowDataJson.cookieTime);
        $('#urlParameter').val(rowDataJson.urlParameter);
        $('#urlParameterCnt').html(rowDataJson.urlParameter.length);

        $('input[name="siteType"][value="' + rowDataJson.siteType + '"]').trigger('click'); //사이트 구분
        $('input[name="siteType"]').prop("disabled",true);
        $('#commissionRate').val(rowDataJson.commissionRate);
        $('#partnerId').val(rowDataJson.partnerId);
        $('#partnerId').prop("disabled",true);
        $('#partnerNm').html(rowDataJson.partnerNm);
        affliChannel.isValidationByPartnerId = true;
        $('#useYn').val(rowDataJson.useYn);
        $('#cookieHistBtn, #commissionHistBtn').show();

        CommonAjax.basic({
            url:'/partner/getChannelGroupList.json?channelId=' + rowDataJson.channelId,
            data:null,
            method:"GET",
            successMsg:null,
            callbackFunc:function(res) {
                var resultCount = res.length;
                if (resultCount > 0) {
                    res.forEach(function (item) {
                        affliChannel.addSalesCd(item.salesCd);
                    });
                }
            }
        });
    },
    /**
     * 제휴사 매출 코드 추가
     * @param salesCd
     */
    addSalesCd : function(salesCd){

        if (!$.jUtil.isAllowInput(salesCd, ['ENG', 'NUM','NOT_SPACE']) || salesCd.length < 1 || salesCd.length > 50) {
            alert("1~50자의 영문, 숫자만 사용가능합니다.");
            $('#salesCd').val('');
            $('#salesCd').focus();
            return;
        }

        if(!$.jUtil.isEmpty($("#salesCdDList_" + salesCd).val())){
            alert("등록된 매출 코드 입니다.");
            $('#salesCd').val('');
            return false;
        }
        //(매출 코드 4개 단위로 줄바꿈)
        var html ="";
        var salesCdSize = $("#salesCdList > span").length;
        if(parseInt(salesCdSize) > 0 && parseInt(salesCdSize) % 4 == 0 ){
            html += "</br>";
        }
        html += "<span id='salesCd_" + salesCd + "' class='text-normal'>";
        html += "<input type='hidden'  value='" + salesCd + "' name ='salesCdlist' id ='salesCdDList_" + salesCd + "' />";
        html += "<span class='mg-r-10 mg-l-10 mg-t-10'>" + salesCd + "</span>";
        html += "<button type='button' class='ui button small delete-thumb' id='salesCdDelete_" + salesCd + "'>x</button>";
        html += "</span>";


        $("#salesCdList").append(html);


    },
    /**
     * 제휴사 매출 코드 삭제
     * @param obj
     */
    delSalesCd : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var salesCd = idArr[1];
        $("#salesCd_" + salesCd).remove();
        var cdListObj  =  $("#salesCdList > span");

        //재 배열화(매출 코드 4개 단위로 줄바꿈)
        $("#salesCdList").children().remove();
        cdListObj.each(function(index){

            if(index > 0 && index % 4 == 0){
                $("#salesCdList").append("</br>")
            }
            $("#salesCdList").append(cdListObj[index]);
        });
    },
    /**
     * affliChannel 등록/수정
     */
    set : function() {

        if(!affliChannel.valid.set()){
            return false;
        
        }
        var setData = {
            'channelId' : $('#channelId').val()
            , 'channelNm' : $('#channelNm').val()
            , 'partnerId' : $('#partnerId').val()
            , 'partnerNm' : $('#partnerNm').val()
            , 'siteType' : $("input:radio[name=siteType]:checked").val()
            , 'useYn' : $('#useYn').val()
            , 'cookieTime' : $('#cookieTime').val()
            , 'commissionRate' : $('#commissionRate').val()
            , 'urlParameter' : $('#urlParameter').val()
        };

        //매출코드 리스트
        var salesCdlist = new Array();
        $("input[name=salesCdlist]").each(function(){
            salesCdlist.push($(this).val());
        });

        setData.salesCdList = salesCdlist;

        CommonAjax.basic({url:'/partner/setAffliChannel.json',data: JSON.stringify(setData), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                affliChannel.search();
        }});

    },
    /**
     * affliChannel 입력/수정 폼 초기화
     */
    reset : function() {
        $('#channelId, #channelNm, #cookieTime, #urlParameter, #commissionRate, #partnerId, #useYn').val('');
        $('#partnerId').prop("disabled",false);
        $('#partnerNm, #channelIdTxt').html('');
        affliChannel.isValidationByPartnerId = false;
        $('input:radio[name="siteType"]:input[value="HOME"]').trigger('click');
        $('#useYn').val('Y');
        $('#salesCd').val('');
        $("#salesCdList").children().remove();
        $('#cookieHistBtn, #commissionHistBtn').hide();

        $('#urlParameterCnt').html('0');
        $('#channelNmCnt').html('0');
        $('input[name="siteType"]').prop("disabled",false);

    },
    /**
     * 제휴 업체 상호명 조회
     */
    getPartnerName : function(){

        var partnerId  = $('#partnerId').val();

        if(!$.jUtil.isEmpty(partnerId)){
            CommonAjax.basic({url:'/partner/getAffiliateNm.json?partnerId=' + partnerId, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                    if($.jUtil.isNotEmpty(res.returnKey)) {
                        $('#partnerNm').html(res.returnKey);
                        affliChannel.isValidationByPartnerId = true;
                    } else {
                        $('#partnerNm').html('');
                        affliChannel.isValidationByPartnerId = false;
                    }
                }});
        } else {
            alert("제휴업체 ID를 입력해 주세요;");
            return false;
        }
    }
};
/**
 * affliChannel 검색,입력,수정 validation check
 */
affliChannel.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-364d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
      return true;

    },
    set : function () {
        if($.jUtil.isEmpty($('#channelNm').val())) {
            alert("제휴채널명을 입력해 주세요.");
            $('#channelNm').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#partnerId').val())) {
            alert("제휴업체ID를 검색해 주세요.");
            $('#partnerId').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#cookieTime').val())) {
            alert("쿠키 유효기간을 입력해 주세요.");
            $('#cookieTime').focus();
            return false;
        }
        if(!$.jUtil.isAllowInput($("#cookieTime").val(), ['NUM'])) {
            alert("쿠키 유효기간은 숫자만 입력해주세요.");
            $("#cookieTime").val('').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#commissionRate').val())) {
            alert("수수료를 입력해 주세요.");
            $('#commissionRate').focus();
            return false;
        }
        if($.jUtil.isPercentInput($('#commissionRate').val()) == false) {
            alert("수수료는 100미만, 소수점 2자리까지 입력할 수 있습니다.");
            return false;
        }
        if($('#commissionRate').val() >= 100) {
            alert("수수료는 100미만, 소수점 2자리까지 입력할 수 있습니다.");
            return false;
        }
        if(!affliChannel.isValidationByPartnerId) {
            alert("제휴업체 유효성 검사를 진행해 주세요.");
            $('#partnerId').focus();
            return false;
        }
        return true;
    }
};