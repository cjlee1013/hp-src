/**
 * 판매업체 - 택배사 팝업
 */

var delivery = {
    init : function() {
        this.event();
        this.initAll();
        this.getPostCodeStatus();
    },
    event : function() {

        // 유효성 검사
        $(document).on("click", "button[id^=getCreditCdBtn_]", function() {
            delivery.getCreditCd($(this).data("idx"));
        });

        // 초기화
        $(document).on("click", "button[id^=initCreditCdBtn_]", function() {
            delivery.initCreditCd($(this).data("idx"));
        });

        // 적용 버튼
        $("#setDeliveryBtn").on("click", function() {
            delivery.setDelivery();
        });

        // 취소 버튼
        $("#cancelDeliveryBtn").on("click", function() {
            // delivery.drawDeliveryList();
        });
    },
    initAll : function () {
        $("#setForm").each(function () {
            this.reset();
        });

        delivery.drawDeliveryList();
    },
    // 택배사 정보 그리기
    drawDeliveryList : function () {
        if (deliveryList.length > 0) {
            var html = "";
            for (var idx in deliveryList) {
                var data = deliveryList[idx];

                html += "<tr id='tr_" + data.dlvCd + "' data-index='" + idx + "'>";
                html += "    <td class='text-center'>" + data.dlvNm + "</td>";
                html += "    <td>";
                html += "        <div class='ui form inline mg-l-60'>";
                html += "            <input type='text' id='creditCd_" + idx + "' class='ui input medium mg-r-5' style='width: 150px;' maxlength='12'>";
                html += "            <input type='hidden' id='dlvCd_" + idx + "' value='" + data.dlvCd + "'>";
                html += "            <input type='hidden' id='creditFlag_" + idx + "' value=false>";
                html += "            <button type='button' class='ui button medium font-malgun mg-r-5' id='getCreditCdBtn_" + idx + "' data-idx='" + idx + "'>인증</button>";
                html += "            <button type='button' class='ui button medium font-malgun' id='initCreditCdBtn_" + idx + "' data-idx='" + idx + "'>초기화</button>";
                html += "        </div>";
                html += "    </td>";
                html += "</tr>";
            }
            $("#deliveryTb > tbody:last").html(html);


            if (deliveryInfo != null && deliveryInfo.length > 0) {
                for (var idx in deliveryInfo) {
                    var info = deliveryInfo[idx];
                    var targetTrIndex = $('#tr_'+info.dlvCd).data("index");
                    $("#creditCd_" + targetTrIndex).val(info.creditCd).prop("readonly", true);
                    $("#creditFlag_" + targetTrIndex).val("true");
                }
            }
        }
    },
    // 유효성 검사
    getCreditCd : function (idx) {
        if ($("#creditCd_" + idx).val() == "") {
            alert("택배사 계약코드를 입력하세요.");
            return;
        }

        CommonAjax.basic({
            url:"/partner/popup/getCompanyCreditValidInfo.json"
            , data:{dlvCd:$("#dlvCd_" + idx).val(), creditCd:$("#creditCd_" + idx).val()}
            , method : "GET"
            , callbackFunc:function(res) {
                if (res[0].success != "Y"){
                    $("#creditFlag_" + idx).val("false");
                    alert("유효하지 않은 코드 입니다.");
                    $("#creditCd_" + idx).attr("readOnly", false);
                }
                else {
                    $("#creditFlag_" + idx).val("true");
                    alert("유효한 계약 코드 입니다.");
                    $("#creditCd_" + idx).attr("readOnly", true);
                }
            }
        });
    },
    // 초기화
    initCreditCd : function (idx) {
        $("#creditCd_" + idx).val("");
        $("#creditFlag_" + idx).val("false");
        $("#creditCd_" + idx).prop("readOnly", false);
    },
    // 적용
    setDelivery : function () {
        var creditCheck = true;
        var dataArray = new Array();

        if (deliveryList.length > 0) {
            for (var idx in deliveryList) {
                if (deliveryList[idx].dlvCd != '001') {
                    if ($("#creditCd_" + idx).val() != "" && $("#creditFlag_" + idx).val() == "false") {
                        creditCheck = false;
                        break;
                    }
                }

                var dataObj = new Object();

                if ($("#creditCd_" + idx).val() != "") {
                    dataObj.dlvCd = $("#dlvCd_" + idx).val();
                    dataObj.creditCd = $("#creditCd_" + idx).val();

                    dataArray.push(dataObj);
                }
            }

            if (!creditCheck) {
                alert("유효성을 확인해 주세요.");
                return;
            }

            if (!confirm("적용하시겠습니까?")) {
                return;
            }

            eval("opener." + callback)(dataArray);
            self.close();
        }
    },

    //우체국택배 이용신청 상태 조회
    getPostCodeStatus : function(){
        if (!partnerId) return;
        var targetTrIndex = $('#tr_D001').data("index");

        CommonAjax.basic({
            url : "/partner/pop/getReturnPostCodeRequest.json",
            data : { partnerId : partnerId },
            method : "POST",
            callbackFunc : function(res) {
                switch (res.requestStatus) {
                    case 'R1' :
                    case 'R2' :
                        $('#postStatusDiv').html('승인 처리 중입니다.').show();
                        $("#creditCd_" + targetTrIndex).val('');
                        break;
                    case 'R3' : //완료
                        $('#postStatusDiv').html('승인 되었습니다.<br>재신청 시 기존 신청 정보 삭제 후 신규 신청 됩니다.').show();
                        $("#creditCd_" + targetTrIndex).val(res.postStoreCd);
                        break;
                    case 'R8' : //실패
                        $('#postStatusDiv').html('승인에 실패하였습니다.<br>재신청 시 기존 신청 정보 삭제 후 신규 신청 됩니다.').show();
                        break;
                    default :
                        $('#postStatusDiv').hide();
                        break;
                }
            },
            errorCallbackFunc : function(res) { //요청 내역이 없는 경우
                $('#postStatusDiv').hide();
            }
         });
    },

};
