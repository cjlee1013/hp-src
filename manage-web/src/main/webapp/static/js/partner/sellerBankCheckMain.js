/**
 * 업체관리 > 판매업체관리 > 판매업체 정산계좌 검증
 */
$(document).ready(function() {
    sellerBankCheckListGrid.init();
    sellerBankCheck.init();
    CommonAjaxBlockUI.global();
});

var sellerBankCheckListGrid = {
    gridView : new RealGridJS.GridView("sellerBankCheckListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
      sellerBankCheckListGrid.initGrid();
      sellerBankCheckListGrid.initDataProvider();
      sellerBankCheckListGrid.event();
    },
    initGrid : function() {
      sellerBankCheckListGrid.gridView.setDataSource(sellerBankCheckListGrid.dataProvider);
      sellerBankCheckListGrid.gridView.setStyles(bankCheckListGridBaseInfo.realgrid.styles);
      sellerBankCheckListGrid.gridView.setDisplayOptions(bankCheckListGridBaseInfo.realgrid.displayOptions);
      sellerBankCheckListGrid.gridView.setColumns(bankCheckListGridBaseInfo.realgrid.columns);
      sellerBankCheckListGrid.gridView.setOptions(bankCheckListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
      sellerBankCheckListGrid.dataProvider.setFields(bankCheckListGridBaseInfo.dataProvider.fields);
      sellerBankCheckListGrid.dataProvider.setOptions(bankCheckListGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
      sellerBankCheckListGrid.dataProvider.clearRows();
      sellerBankCheckListGrid.dataProvider.setRows(dataList);
      sellerBankCheckListGrid.gridView.orderBy(['chgDt'],[RealGridJS.SortDirection.DESCENDING]);
    },
    excelDownload: function () {
        if (sellerBankCheckListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "정산계좌_검증" + _date.getFullYear() + (_date.getMonth() + 1)
                + _date.getDate();

        sellerBankCheckListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

var sellerBankCheck = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.initSearchDate('-30d');
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        sellerBankCheck.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        sellerBankCheck.setDate.setEndDate(0);
        sellerBankCheck.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#schBtn').bindClick(sellerBankCheck.search); //sellerBankCheck 검색
        $('#schResetBtn').bindClick(sellerBankCheck.searchFormReset); //sellerBankCheck 검색폼 초기화
        $("#excelDownloadBtn").click(function() {
            sellerBankCheckListGrid.excelDownload();
        });
    },
    /**
     * sellerBankCheck 검색
     */
    search : function() {
        if(!sellerBankCheck .valid.search()){
            return;
        }

        CommonAjax.basic({
            url : '/partner/getSellerBankCheckList.json?' + $('#sellerBankCheckSchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                sellerBankCheckListGrid.setData(res);
                $('#sellerBankCheckTotalCount').html($.jUtil.comma(sellerBankCheckListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        sellerBankCheck.initSearchDate('-30d');
        $('#schType').val('partnerId');
        $('#schKeyword').val('');
    }
};

/**
 * sellerBankCheck 검색,입력,수정 validation check
 */
sellerBankCheck.valid = {
    search : function () {
        const startDt = $("#schStartDt");
        const endDt = $("#schEndDt");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 2) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

      return true;
    }
};
