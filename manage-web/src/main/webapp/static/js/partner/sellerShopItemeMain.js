/**
 * 업체관리 > 샐로샵관리 > 셀러샵 전시관리
 */
$(document).ready(function() {

    sellerShopListGrid.init();
    sellerShop.init();
    sellerShopItemListGrid.init();
    sellerShopItem.init();

    CommonAjaxBlockUI.global();
});

// 셀러샵 그리드
var sellerShopListGrid = {

    gridView : new RealGridJS.GridView("sellerShopListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        sellerShopListGrid.initGrid();
        sellerShopListGrid.initDataProvider();
        sellerShopListGrid.event();

    },
    initGrid : function() {

        sellerShopListGrid.gridView.setDataSource(sellerShopListGrid.dataProvider);
        sellerShopListGrid.gridView.setStyles(sellerShopListGridBaseInfo.realgrid.styles);
        sellerShopListGrid.gridView.setDisplayOptions(sellerShopListGridBaseInfo.realgrid.displayOptions);
        sellerShopListGrid.gridView.setColumns(sellerShopListGridBaseInfo.realgrid.columns);
        sellerShopListGrid.gridView.setOptions(sellerShopListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        sellerShopListGrid.dataProvider.setFields(sellerShopListGridBaseInfo.dataProvider.fields);
        sellerShopListGrid.dataProvider.setOptions(sellerShopListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        sellerShopListGrid.gridView.onDataCellClicked = function(gridView, index) {
            sellerShop.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        sellerShopListGrid.dataProvider.clearRows();
        sellerShopListGrid.dataProvider.setRows(dataList);

    }

};
// 셀러샵 아이템 리스트 그리드
var sellerShopItemListGrid = {

    gridView : new RealGridJS.GridView("sellerShopItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        sellerShopItemListGrid.initGrid();
        sellerShopItemListGrid.initDataProvider();

    },
    initGrid : function() {

        sellerShopItemListGrid.gridView.setDataSource(sellerShopItemListGrid.dataProvider);
        sellerShopItemListGrid.gridView.setStyles(sellerShopItemListGridBaseInfo.realgrid.styles);
        sellerShopItemListGrid.gridView.setDisplayOptions(sellerShopItemListGridBaseInfo.realgrid.displayOptions);
        sellerShopItemListGrid.gridView.setColumns(sellerShopItemListGridBaseInfo.realgrid.columns);
        sellerShopItemListGrid.gridView.setOptions(sellerShopItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        sellerShopItemListGrid.dataProvider.setFields(sellerShopItemListGridBaseInfo.dataProvider.fields);
        sellerShopItemListGrid.dataProvider.setOptions(sellerShopItemListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        sellerShopItemListGrid.dataProvider.clearRows();
        sellerShopItemListGrid.dataProvider.setRows(dataList);

    },
    addDataList : function (dataList) {
        sellerShopItemListGrid.dataProvider.addRows(dataList);
    },

};

// 셀러샵관리
var sellerShop = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        sellerShop.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        sellerShop.setDate.setEndDate(0);
        sellerShop.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        //검색 역영
        $('#searchBtn').bindClick(sellerShop.search);   // 검색
        $('#searchResetBtn').bindClick(sellerShop.searchFormReset); // 검색폼 초기화
        $('#setSellerShopItmBtn').bindClick(sellerShop.setSellerShopItm); // 셀러샵 정보 저장
        $('#resetBtn').bindClick(sellerShop.resetForm); // 셀러샵 등록 정보 초기화

    },
    /**
     * 셀러샵 검색
     */
    search : function() {

        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            sellerShop.initSearchDate('-1d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        CommonAjax.basic({url:'/partner/getSellerShopList.json?' + $('#sellerShopItemSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                sellerShop.resetForm();
                sellerShopListGrid.setData(res);
                $('#sellerShopTotalCount').html($.jUtil.comma(sellerShopListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        sellerShop.initSearchDate('-1d');
        $('#searchType').val('SELLERID');  // 검색어 구분
        $('#searchKeyword').val('');    //검색어

    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        sellerShop.resetForm();

        var rowDataJson = sellerShopListGrid.dataProvider.getJsonRow(selectRowId);

        $('#sellerId').html( rowDataJson.partnerId)
        $('#sellerNm').html( rowDataJson.shopNm)
        $("input:radio[name='dispType']:input[value='" + rowDataJson.shopDispType + "']").prop("checked", true);

        CommonAjax.basic({url:'/partner/getSellerShopItemList.json?partnerId='+rowDataJson.partnerId
            , data: null
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(res) {
                //셀러샵 아이템 리스트
                sellerShopItemListGrid.setData(res);

            }});

    },

    /**
     * 셀러샵 전시 관리 아이템 등록/수정
     */
    setSellerShopItm : function() {


        var setItem = new Object();
        setItem.partnerId = $('#sellerId').html();

        if ($.jUtil.isEmpty(setItem.partnerId)) {
            alert("셀러를 선택해 주세요");
            return false;
        }

        sellerShopItem.resetItemPriority();

        setItem.itemList = sellerShopItemListGrid.dataProvider.getJsonRows();
        setItem.dispType = $("input:radio[name='dispType']:checked").val();


        CommonAjax.basic({url:'/partner/setSellerShopItem.json',data: JSON.stringify(setItem), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                sellerShop.search();
            }});


    },
    /**
     * 셀러샵 입력/수정 폼 초기화
     */
    resetForm : function() {
        sellerShopItemListGrid.dataProvider.clearRows();
        $('#sellerId').html('');
        $('#sellerNm').html('');


    }

};
// 셀러샵아이템
var sellerShopItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#addItemPopUp').bindClick(sellerShopItem.addItemPopUp);
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = sellerShopItemListGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }
        sellerShopItemListGrid.dataProvider.removeRows(check, false);

    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = sellerShopItemListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품 선택해 주세요.");
            return false;
        }

        realGridMoveRow(sellerShopItemListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬


    },

    /**
     * 상품 우선순위 재 정렬
     */
    resetItemPriority : function(){
        // Priority 정렬
        sellerShopItemListGrid.dataProvider.getJsonRows(0,-1).forEach(function (data, index) {

            var itemInfo = sellerShopItemListGrid.gridView.getValues(index);
            itemInfo.priority = parseInt(index) + 1
            sellerShopItemListGrid.gridView.setValues(index , itemInfo ,true);

        });
    },
    /**
     * 상품조회(용도옵션포함)팝업
     */
    addItemPopUp : function() {
        var sellerId = $('#sellerId').text();
        if($.jUtil.isEmpty(sellerId)) {
            alert('등록하실 셀러를 먼저 선택해 주세요.');
            return;
        }

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=sellerShopItem.setItemList&isMulti=Y&mallType=DS&storeType=DS&partnerId=' + sellerId, 1084, 650);
    },
    /**
     * 상품리스트 설정
     */
    setItemList : function(itemList) {

        var gridList = sellerShopItemListGrid.dataProvider.getJsonRows();


        itemList.forEach(function (item) {
            gridList.forEach(function (gridItem, idx) {
                if(gridItem.itemNo == item.itemNo ){
                    gridList.splice(idx, 1)
                }
            });
        });

        sellerShopItemListGrid.dataProvider.clearRows();
        sellerShopItemListGrid.setData(itemList);
        sellerShopItemListGrid.addDataList(gridList);
        sellerShopItem.resetItemPriority();

    }


};