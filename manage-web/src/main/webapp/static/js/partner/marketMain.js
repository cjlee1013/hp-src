/**
 * 마켓연동관리 > 마켓연동 업체관리
 */
$(document).ready(function() {
    marketListGrid.init();
    marketCommissionGrid.init();
    market.init();
    CommonAjaxBlockUI.global();
});

// 마켓연동업체관리 그리드
var marketListGrid = {
    gridView : new RealGridJS.GridView("marketListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        marketListGrid.initGrid();
        marketListGrid.initDataProvider();
        marketListGrid.event();
    },
    initGrid : function() {
        marketListGrid.gridView.setDataSource(marketListGrid.dataProvider);

        marketListGrid.gridView.setStyles(marketListGridBaseInfo.realgrid.styles);
        marketListGrid.gridView.setDisplayOptions(marketListGridBaseInfo.realgrid.displayOptions);
        marketListGrid.gridView.setColumns(marketListGridBaseInfo.realgrid.columns);
        marketListGrid.gridView.setOptions(marketListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        marketListGrid.dataProvider.setFields(marketListGridBaseInfo.dataProvider.fields);
        marketListGrid.dataProvider.setOptions(marketListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        marketListGrid.gridView.onDataCellClicked = function(gridView, index) {
            market.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        marketListGrid.dataProvider.clearRows();
        marketListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "마켓연동업체 관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        marketListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 상품상세 일괄공지 리스트 그리드
var marketCommissionGrid = {

    gridView : new RealGridJS.GridView("marketCommissionGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {

        marketCommissionGrid.initGrid();
        marketCommissionGrid.initDataProvider();
        marketCommissionGrid.event();

    },
    initGrid : function() {
        marketCommissionGrid.gridView.setDataSource(marketCommissionGrid.dataProvider);

        marketCommissionGrid.gridView.setStyles(marketCommissionGridBaseInfo.realgrid.styles);
        marketCommissionGrid.gridView.setDisplayOptions(marketCommissionGridBaseInfo.realgrid.displayOptions);
        marketCommissionGrid.gridView.setColumns(marketCommissionGridBaseInfo.realgrid.columns);
        marketCommissionGrid.gridView.setOptions(marketCommissionGridBaseInfo.realgrid.options);

    },
    initDataProvider : function() {
        marketCommissionGrid.dataProvider.setFields(marketCommissionGridBaseInfo.dataProvider.fields);
        marketCommissionGrid.dataProvider.setOptions(marketCommissionGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        marketCommissionGrid.gridView.onDataCellClicked = function(gridView, index) {
            market.commissionGridRowSelect(index.dataRow);
        };

    },
    setData : function(dataList) {
        marketCommissionGrid.dataProvider.clearRows();
        marketCommissionGrid.dataProvider.setRows(dataList);
    },
};

// 마켓연동업체관리
var market = {
    isCheckPartnerId    : false,   // 중복확인 여부
    /**
     * 초기화
     */
    init : function() {
        market.bindingEvent();
        market.searchFormReset();
        market.initApplyDate(-'1y');
    },

    initApplyDate : function (flag) {
        market.setDate = Calendar.datePickerRange('applyStartDt', 'applyEndDt');

        market.setDate.setEndDate(0);
        market.setDate.setStartDate(flag);

        $("#applyEndDt").datepicker("option", "minDate", $("#applyStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchResetBtn').bindClick(market.searchFormReset);
        $('#searchBtn').bindClick(market.search);
        $('#excelDownloadBtn').bindClick(market.excelDownload);

        $('#setSellerBtn').bindClick(market.setMarket);
        $('#resetBtn').bindClick(market.resetForm, true);

        $('#partnerStatusBtn').bindClick(market.getPartnerStatusPop); //회원상태
        $('#getPartnerNoBtn').bindClick(market.getPartnerNoPop);
        $('#getBizCateCd').bindClick(market.getBizCateCd);


        $("#schKeyword").notAllowInput('keyup', ['SPC_SCH']);
        $("#bankAccount").allowInput("keyup", ["NUM"], $(this).attr("id"));

        $('#resetCommissionFormBtn').bindClick(market.resetCommission);
        $('#saveCommissionBtn').bindClick(market.saveCommission);
        $('#addCommissionBtn').bindClick(market.addCommission);
        $('#delCommissionBtn').bindClick(market.delCommission);
        // 종목 추가
        $(document).on("click", "li[id^=categorySearchList_]", function() {
            market.addSchCategory(this);
        });

        // 종목 삭제
        $(document).on("click", "button[id^=categoryDelete_]", function(){
            market.delSchCategory(this);
        });

    },

    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        marketListGrid.excelDownload();
    },
    /**
     * 마켓연동업체 관리 검색
     */
    search : function() {

        var marketSearchForm = $('#marketSearchForm').serializeObject(true);
        CommonAjax.basic({
            url: '/partner/getMarketList.json',
            data: JSON.stringify(marketSearchForm),
            method: "POST",
            contentType: 'application/json',
            successMsg: null,
            callbackFunc: function (res) {
                market.resetForm();
                marketListGrid.setData(res);
                $('#marketTotalCount').html($.jUtil.comma(marketListGrid.gridView.getItemCount()));
            }
        });

    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schUseYn').val('');
    },

    /**
     * 마켓연동업체 관리 입력/수정 폼 초기화
     */
    resetForm : function(isClick) {

        $("#marketForm").each(function () {
            this.reset();
        });

        $('#partnerId').prop('readonly', false);

        if (isClick) {
            $('#checkPartnerId').show();
        }

        $('#gibunAddrArea').hide();
        marketCommissionGrid.dataProvider.clearRows();

        market.isCheckPartnerId = false;
        market.addCommissionBtnShow();

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        market.resetForm(false);

        var rowDataJson = marketListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: "/partner/getMarket.json?partnerId="+rowDataJson.partnerId,
            method: "GET",
            callbackFunc: function (marketDto) {
                market.isCheckBankAccnt = true;

                $('#partnerId').val(rowDataJson.partnerId.replace("coop", ""));
                $('#businessNm').val(marketDto.businessNm);
                $('#operatorType').val(marketDto.operatorType);

                $('#useYn').val(marketDto.useYn);
                $('#partnerStatus').val(marketDto.partnerStatus);

                $('#partnerNm').val(marketDto.partnerNm);
                $('#partnerOwner').val(marketDto.partnerOwner);
                $.jUtil.valSplit(marketDto.partnerNo, '-', 'partnerNo');

                $('#businessConditions').val(marketDto.businessConditions);
                $('#bizCateCd').val(marketDto.bizCateCd);
                $('#bizCateCdNm').val(marketDto.bizCateCdNm);
                $('#zipcode').val(marketDto.zipcode);

                $('#addr1').val(marketDto.addr1);
                $('#addr2').val(marketDto.addr2);

                $('#siteNm').val(marketDto.siteNm);
                $('#siteUrl').val(marketDto.siteUrl);

                $('#itemCertKey').val(marketDto.itemCertKey);
                $('#orderCertKey').val(marketDto.orderCertKey);

                $('#partnerId').prop('readonly', true);
                $('#checkPartnerId').hide();

                marketCommissionGrid.setData(marketDto.commissionList);
            }
        });
    },
    /**
     * 수수료 선택
     * @param selectRowId
     */
    commissionGridRowSelect : function(selectRowId) {

        var rowDataJson = marketCommissionGrid.dataProvider.getJsonRow(selectRowId);

        $('#applyStartDt').val(rowDataJson.applyStartDt);
        $('#applyEndDt').val(rowDataJson.applyEndDt);

        $('#commissionRate').val(rowDataJson.commissionRate);
        $('#commissionSeq').val(rowDataJson.commissionSeq);

        market.saveCommissionBtnShow();
    },
    /**
     * 추가버튼 노출
     */
    addCommissionBtnShow : function(){
        $('#addCommissionBtn').show();
        $('#saveCommissionBtn').hide();
    },
    /**
     * 수정버튼 노출
     */
    saveCommissionBtnShow : function() {
        $('#saveCommissionBtn').show();
        $('#addCommissionBtn').hide();
    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setZipcode : function (res) {
        $("#zipcode").val(res.zipCode);
        $("#addr1").val(res.roadAddr);
        $("#addr2").val('');
    },
    /**
     * 마켓연동업체 저장
     */
    setMarket : function() {

        if (market.valid.set()) {

            $('#partnerNo').val($.jUtil.valNotSplit('partnerNo', '-'));

            var marketForm = $('#marketForm').serializeObject();

            marketForm.isMod  = $('#partnerId').prop('readOnly');
            marketForm.partnerId = 'coop' +  $('#partnerId').val();

            var commissionList =  marketCommissionGrid.dataProvider.getJsonRows();
            marketForm.commissionList = commissionList;

            CommonAjax.basic({
                url: "/partner/setMarket.json",
                data: JSON.stringify(marketForm),
                contentType: "application/json",
                method: "POST",
                callbackFunc: function (res) {
                    alert(res.returnMsg);

                    let index = marketListGrid.gridView.searchItem({
                        fields: ['partnerId'],
                        values: [marketForm.partnerId]
                    });

                    if(index != -1) {
                       market.gridRowSelect(index);
                    }else {
                        market.search();
                        market.resetForm(true);
                    }
                }
            });
        }
    },

    /**
     * 판매자 회원상태 > 회원상태 팝업
     */
    getPartnerStatusPop : function() {
        if (!$.jUtil.isEmpty($("#partnerId").val())) {
            windowPopupOpen("/partner/popup/getPartnerStatusPop?partnerId="
                    + $("#partnerId").val() + "&callback="
                    + "market.callbackPartnerStatus" + "&partnerType="
                    + "COOP"
                    , "partnerStatus", "780", "700", "yes", "no");
        }
    },
    /**
     * 마켓연동 업체관리 > 사업자 등록 번호 조회
     */
    getPartnerNoPop : function() {
        windowPopupOpen("/partner/popup/getPartnerNoPop?partnerType=COOP&callback=" + "market.callbackPartnerNoChk"
            , "partnerNocheck", "480", "183", "yes", "no");
    },
    /**
     * 판매자 사업자정보 > 사업자 정보 입력 ( 콜백 )
     */
    callbackPartnerNoChk : function(data) {
        $('#partnerNm').val(data.partnerNm);
        $('#partnerOwner').val(data.partnerOwner);
        $('#partnerNo_1').val(data.partnerNo.substring(0, 3));
        $('#partnerNo_2').val(data.partnerNo.substring(3, 5));
        $('#partnerNo_3').val(data.partnerNo.substring(5));
    },

    /**
     * 종목 조회
     */
    getBizCateCd : function() {
        CommonAjax.basic({
            url: "/partner/getBizCateCdList.json",
            data: {
                codeNm: $("#bizCateCdNm").val()
            },
            method: "GET",
            callbackFunc: function (res) {
                var resultCount = res.length;

                if (resultCount == 0) {
                    $("#searchCategory").html("");
                    $("#searchCategory").append(
                        "<li class='ui-menu-item-wrapper'>검색결과가 없습니다.</li>");
                } else {
                    $("#searchCategory").html("");

                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];

                        $("#searchCategory").append(
                            "<li class='ui-menu-item-wrapper' id='categorySearchList_"
                            + data.mcCd + "'>" + data.mcNm + "</li>");
                    }
                }
                $("#searchCategory").show();
            }
        });
    },
    /**
     * 종목 추가
     * @param obj
     */
    addSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];
        var categoryCdNm = $(obj).html();

        market.addSchCategoryHtml(categoryCd, categoryCdNm);
        $("#searchCategory").hide();
    },
    /**
     * 종목 추가 html
     * @param categoryCd
     * @param categoryCdNm
     * @returns {boolean}
     */
    addSchCategoryHtml : function(categoryCd, categoryCdNm) {
        $('#bizCateCd').val(categoryCd);
        $('#bizCateCdNm').val(categoryCdNm);

        if ($("#bizCateCdDiv_" + categoryCd).length > 0) {
            $("#bizCateCdDiv_" + categoryCd).find(".categorySetDtoUseYn").val("Y");
            $("#bizCateCdDiv_" + categoryCd).show();
        } else {
            var html = "<span id='bizCateCdDiv_" + categoryCd + "' class='text-normal'>";
            html += "<span class='mg-r-10 mg-l-10 mg-t-10'>" + categoryCdNm + "</span>";
            html += "<button type='button' class='ui button small delete-thumb' id='categoryDelete_" + categoryCd + "'>x</button>";
            html += "</span>";

            $("#bizCateCdListId").html(html);
        }
    },
    /**
     * 종목 삭제
     * @param obj
     */
    delSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];

        $("#bizCateCdDiv_" + categoryCd).find(".categorySetDtoUseYn").val("N");
        $("#bizCateCdDiv_" + categoryCd).hide();
    },
    /**
     * 소득공제 제공여부
     */
    setCultureDeductionYn : function(cultureDeductionYn) {
        if (typeof cultureDeductionYn == 'object') {
            cultureDeductionYn = $(cultureDeductionYn).val();
        }

        if (cultureDeductionYn == 'Y') {
            $('#cultureDeductionNo').prop('readonly', false);
        } else {
            $('#cultureDeductionNo').val('');
            $('#cultureDeductionNo').prop('readonly', true);
        }
    },

    /**
     * 소수점 처리 function
     * @param evt
     */
    isNumberKey : function (evt) {
        // 숫자를 제외한 값을 입력하지 못하게 한다.
        var charCode = (evt.which) ? evt.which : event.keyCode;
        // 수수료율
        var _value = event.srcElement.value;

        if (event.keyCode < 48 || event.keyCode > 57) {
            if (event.keyCode !== 46) { //숫자와 . 만 입력가능하도록함
                return false;
            }
        }

        // 소수점(.)이 두번 이상 나오지 못하게
        var _pattern0 = /^\d*[.]\d*$/; // 현재 value 값에 소수점(.) 이 있으면 . 입력불가
        if (_pattern0.test(_value)) {
            if (charCode == 46) {
                return false;
            }
        }

        // 두자리 이하의 숫자만 입력가능
        var _pattern1 = /^\d{2}$/; // 현재 value 값이 2자리 숫자이면 . 만 입력가능
        // {숫자}의 값을 변경하면 자리수를 조정할 수 있다.
        if (_pattern1.test(_value)) {
            if (charCode != 46 && !_value.endsWith('.')) {
                alert("두자리 이하의 숫자만 입력가능합니다");
                return false;
            }
        }

        // 소수점 둘째자리까지만 입력가능
        var _pattern2 = /^\d*[.]\d{2}$/; // 현재 value 값이 소수점 둘째짜리 숫자이면 더이상 입력 불가
        // {숫자}의 값을 변경하면 자리수를 조정할 수 있다.
        if (_pattern2.test(_value)) {
            alert("소수점 둘째자리까지만 입력가능합니다.");
            return false;
        }
        return true;
    },
    /**
     *
     * @param evt
     * @returns {boolean}
     */
    delKorWord : function (evt) {
        var objTarget = evt.srcElement || evt.target;
        var _value = event.srcElement.value;
        if(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g.test(_value)) {
            objTarget.value = objTarget.value.replace(/[ㄱ-ㅎㅏ-ㅡ가-힣]/g,''); // g가 핵심: 빠르게 타이핑할때 여러 한글문자가 입력되어 버린다.
            objTarget.value = null;
            return false;
        }
    },
    /**
     * 수수료 Form 초기
     */
    resetCommission : function () {

        market.initApplyDate(-'1y');

        $('#applyStartDt').val('');
        $('#applyEndDt').val('');

        $('#commissionRate').val('');
        $('#commissionSeq').val('');

        market.addCommissionBtnShow();
    },
    /**
     * 수수료 수정/추가
     * @returns {*|boolean}
     */
    addCommission : function () {

        if(!market.valid.setCommission()) {
            return false;
        }

        let applyStartDt = $('#applyStartDt').val();
        let applyEndDt = $('#applyEndDt').val();
        let gridList = marketCommissionGrid.dataProvider.getJsonRows();

        for (var i in gridList) {

            if ( moment(gridList[i].applyStartDt).isBetween(applyStartDt, applyEndDt)
                ||  moment(gridList[i].applyEndDt).isBetween(applyStartDt, applyEndDt)
                || moment(applyStartDt).isBetween(gridList[i].applyStartDt, gridList[i].applyEndDt)
                || moment(applyEndDt).isBetween(gridList[i].applyStartDt, gridList[i].applyEndDt)){

                return $.jUtil.alert("적용기간을 중복으로 설정하실 수 없습니다. ", 'applyStartDt');
            }

            if (  moment(gridList[i].applyStartDt).isSame(applyStartDt)
                || moment(gridList[i].applyStartDt).isSame(applyEndDt)
                || moment(gridList[i].applyEndDt).isSame(applyStartDt)
                || moment(gridList[i].applyEndDt).isSame(applyEndDt))
                {
                return $.jUtil.alert("적용기간을 중복으로 설정하실 수 없습니다. ", 'applyStartDt');
            }

        }

        var value = {
            commissionRate: $('#commissionRate').val(),
            applyStartDt: $('#applyStartDt').val(),
            applyEndDt: $('#applyEndDt').val(),
            useYn: 'Y',
            changeYn: 'Y'
        };

        marketCommissionGrid.dataProvider.addRow(value);
        market.resetCommission();
    },
    /**
     * 수수료 수정/추가
     * @returns {*|boolean}
     */
    saveCommission : function () {

        if(!market.valid.setCommission()) {
            return false;
        }

        let applyStartDt = $('#applyStartDt').val();
        let applyEndDt = $('#applyEndDt').val();

        let gridList = marketCommissionGrid.dataProvider.getJsonRows();
        let currRow = marketCommissionGrid.gridView.getCurrent().itemIndex;

        for (var i in gridList) {

            if ( currRow != i && marketCommissionGrid.gridView.getItemCount() > 1 ) {

                if ( moment(gridList[i].applyStartDt).isBetween(applyStartDt, applyEndDt)
                    || moment(gridList[i].applyEndDt).isBetween(applyStartDt, applyEndDt)
                        || moment(applyStartDt).isBetween(gridList[i].applyStartDt, gridList[i].applyEndDt)
                        || moment(applyEndDt).isBetween(gridList[i].applyStartDt, gridList[i].applyEndDt)){

                    return $.jUtil.alert("적용기간을 중복으로 설정하실 수 없습니다. ", 'applyStartDt');
                }

                if (  moment(gridList[i].applyStartDt).isSame(applyStartDt)
                    || moment(gridList[i].applyStartDt).isSame(applyEndDt)
                    || moment(gridList[i].applyEndDt).isSame(applyStartDt)
                    || moment(gridList[i].applyEndDt).isSame(applyEndDt))
                {
                    return $.jUtil.alert("적용기간을 중복으로 설정하실 수 없습니다. ", 'applyStartDt');
                }


            }
        }

        let rowDataJson = marketCommissionGrid.dataProvider.getJsonRow(currRow);

        var value = {
            commissionSeq: rowDataJson.commissionSeq,
            commissionRate: $('#commissionRate').val(),
            applyStartDt: $('#applyStartDt').val(),
            applyEndDt: $('#applyEndDt').val(),
            useYn: 'Y',
            changeYn: 'Y'
        };

        marketCommissionGrid.gridView.setValues(currRow, value, true);
        market.resetCommission();
    },

    /**
     * 수수료 삭제
     */
    delCommission : function () {
        let curr = marketCommissionGrid.gridView.getCurrent();

        if( curr.itemIndex < 0) {
            alert("삭제하려는 데이터가 없습니다.");
            return false;
        }

        marketCommissionGrid.dataProvider.removeRow(curr.dataRow);

    }
};

market.valid = {

    setCommission : function () {

        let applyStartDt = $('#applyStartDt').val();
        let applyEndDt = $('#applyEndDt').val();

        if ($.jUtil.isEmpty($('#commissionRate').val())
            || $.jUtil.isEmpty(applyStartDt)
            || $.jUtil.isEmpty (applyEndDt)){
            return $.jUtil.alert("수수료율, 적용기간을 입력해주세요." );
        }

        if(moment(applyEndDt).diff(applyStartDt, "day", true) < 0) {
            alert("수수료기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }


        return true;
    },
    set : function() {
        var isValid = true;

        var checkObj = {
            partnerId: { //판매업체 ID
                types: ['ENG_LOWER', 'NUM'],
                min: 5,
                max: 12
            },
            businessNm: {   //마켓연동업체명
                types: ['KOR', 'ENG', 'NUM'],
                min: 2,
                max: 50
            },
            partnerOwner: { //대표자명
                types: ['KOR', 'ENG'],
                min: 2,
                max: 50
            },
            businessConditions: { //업태
                types: ['KOR', 'ENG'],
                min: 2,
                max: 50
            }
        };

        var partnerId = $('#partnerId').val();
        if(partnerId.includes('coop')) {
            alert("coop가 포함된 ID는 사용 할 수 없습니다.");
            isValid = false;
        }

        for (var id in checkObj) {
            var checkVal ;
            if ( id ==  'partnerId') { // 파트너 ID는 coop 포함으로 비교해야한다.
                checkVal = 'coop'+ $('#' + id).val();
            } else {
                checkVal = $('#' + id).val();
            }

            if(!$.jUtil.isEmpty(checkVal)) {
                if (!$.jUtil.isAllowInput(checkVal, checkObj[id].types)
                    || checkVal.length < checkObj[id].min || checkVal.length
                    > checkObj[id].max) {
                    alert(checkObj[id].min + "~" + checkObj[id].max + "자의 "
                        + $.jUtil.getAllowInputMessage(checkObj[id].types));
                    $('#' + id).focus();
                    isValid = false;
                    break;
                }
            }else{
                alert("필수값을 등록해주세요.");
                return false;
            }
        }

        if (!isValid) {
            return false;
        }

        var arr = [$('#partnerNm'), $('#partnerOwner'), $('#bizCateCdNm'), $('#zipcode'), $('#addr1') ];

        for(var i =0; i < arr.length; i++) {
            var value ="";
            if(typeof arr[i] == 'object') {
                value = $(arr[i]).val();
                if($.jUtil.isEmpty(value)) {
                    alert("사업자정보 필수값을 읿력해주세요");
                    return false;
                }
            }
        }

        if($.jUtil.isEmpty($('#siteNm').val())) {
            alert("사이트정보 필수 값을 입력해주세요");
            $('#partnerNm').focus();
            return false;
        }
        
        return true;
    },
    /**
     * 판매자 기본정보 > 아이디 중복확인
     */
    checkPartnerId : function() {

        if($('#partnerId').val().includes('coop')) {
            alert("coop가 포함된 ID는 사용 할 수 없습니다.");
            $("#partnerId").val('').focus();
            return;
        }

        var partnerId = 'coop'+ $('#partnerId').val();

        if ($.jUtil.isEmpty(partnerId)) {
            alert('사용할 아이디를 입력해주세요.');
            return;
        }

        if (!$.jUtil.isAllowInput(partnerId, ['ENG_LOWER', 'NUM']) || partnerId.length < 5 || partnerId.length > 12) {
            alert("5~12자의 영문 소문자, 숫자만 사용가능합니다.");
            $("#partnerId").focus();
            return;
        }

        market.isCheckPartnerId = false;

        CommonAjax.basic({
            url: "/partner/getMarketIdCheck.json",
            data: {partnerId: partnerId, partnerType: 'COOP'},
            method: "GET",
            callbackFunc: function (res) {
                if (res.returnCnt == 0) {
                    market.isCheckPartnerId = true;
                    alert("사용 가능한 아이디입니다.");
                }
            },
            errorCallbackFunc : function(resError) {
                alert(resError.responseJSON.returnMessage);
                $("#partnerId").val("").focus();
            }
        });
    },

};
