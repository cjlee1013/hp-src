/**
 * 판매업체 관리/제조사관리 > 판매업체 관리 관리
 */
$(document).ready(function() {
    sellerListGrid.init();
    seller.init();
    seller.file.init();
    CommonAjaxBlockUI.global();
    commonCategory.setCategorySelectBox(1, '', '', $('#categoryCd'));
});

// 판매업체관리 그리드
var sellerListGrid = {
    gridView : new RealGridJS.GridView("sellerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        sellerListGrid.initGrid();
        sellerListGrid.initDataProvider();
        sellerListGrid.event();
    },
    initGrid : function() {
        sellerListGrid.gridView.setDataSource(sellerListGrid.dataProvider);

        sellerListGrid.gridView.setStyles(sellerListGridBaseInfo.realgrid.styles);
        sellerListGrid.gridView.setDisplayOptions(sellerListGridBaseInfo.realgrid.displayOptions);
        sellerListGrid.gridView.setColumns(sellerListGridBaseInfo.realgrid.columns);
        sellerListGrid.gridView.setOptions(sellerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        sellerListGrid.dataProvider.setFields(sellerListGridBaseInfo.dataProvider.fields);
        sellerListGrid.dataProvider.setOptions(sellerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        sellerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            seller.getDetail(index.dataRow);
        };

        $('html').click(function(e) {
            if ($('#searchCategory').css('display') != 'none') {
                $('#searchCategory').hide();
            }
        });
    },
    setData : function(dataList) {
        sellerListGrid.dataProvider.clearRows();
        sellerListGrid.dataProvider.setRows(dataList);
        sellerListGrid.gridView.orderBy(['chgDt'],[RealGridJS.SortDirection.DESCENDING]);
    },
    excelDownload: function() {
        if(sellerListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "판매업체 관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        sellerListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// 업체관리
var seller = {
    isCheckPartnerId    : false,   // 중복확인 여부
    isCheckBankAccnt    : false,
    mngCnt              : 4,        // 담당자 입력개수
    deliveryInfo        : null,     // 반품택배사 정보
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.searchFormReset();
    },
    /**
     * 조회 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        seller.setDate = Calendar.datePickerRange('schRegStartDate', 'schRegEndDate');

        seller.setDate.setEndDate(0);
        seller.setDate.setStartDate(flag);

        $("#schRegEndDate").datepicker("option", "minDate", $("#schRegStartDate").val());
        $('#partnerOwner').allowInput("keyup", ["ENG", "KOR", "COMMA"]);
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $("input[name='sellerInfo.cultureDeductionYn']").bindChange(seller.setCultureDeductionYn);
        $("input[id^=sameAffiManager]").bindClick(seller.setSameAffiMng);
        $('#searchResetBtn').bindClick(seller.searchFormReset);
        $('#setSellerBtn').bindClick(seller.setSeller);
        $('#resetBtn').bindClick(seller.resetForm, true);
        $('#imageUploadBtn').bindClick(seller.setImage);
        $('#deleteImageBtn').bindClick(seller.unsetImage);
        $('#excelDownloadBtn').bindClick(seller.excelDownload);
        $('.uploadType').bindClick(seller.imagePreview);
        $('#searchBtn').bindClick(seller.search);
        $('#partnerStatusBtn').bindClick(seller.getPartnerStatusPop); //회원상태
        $('#getPartnerNoBtn').bindClick(seller.getPartnerNoPop);
        $('#getBizCateCd').bindClick(seller.getBizCateCd);
        $('#getPartnerPwPop').bindClick(seller.getPartnerPwPop);
        $('#returnDeliveryPopBtn').bindClick(seller.getReturnDeliveryPop);

        $("#schKeyword").notAllowInput('keyup', ['SPC_SCH']);
        $("#bankAccountNo").allowInput("keyup", ["NUM"]);

        // 종목 추가
        $(document).on("click", "li[id^=categorySearchList_]", function() {
            seller.addSchCategory(this);
        });

        // 종목 삭제
        $(document).on("click", "button[id^=categoryDelete_]", function(){
            seller.delSchCategory(this);
        });

        // 통신판매업 신고번호 불러오기
        $("#communityNotiNoBtn").on("click", function () {
            seller.getCommunityNotiNo($.jUtil.valNotSplit('partnerNo', ''));
        });

        $('#getAccAuthenticationBtn').on('click', function() {
            if (seller.isCheckBankAccnt) {
                seller.isCheckBankAccnt = false;
                $(this).text('계좌인증');
                $('#bankStatus').text('인증대기');
                $('#bankCd, #bankAccountNo').prop('disabled', false).val('');
                $('#depositor').val('');
            } else {
                seller.getBankAccntNoCertify();
            }
        });

        $('#bankCd, #bankAccountNo').on('change', function() {
            seller.isCheckBankAccnt = false;
        });

        $('#getSellerHistBtn').on('click', function() {
            if ($('#partnerId').prop('readonly')) {
                windowPopupOpen('/partner/popup/partnerSellerHistPop?partnerId=' + $('#partnerId').val(), "partnerSellerHistPop", 1600, 800);
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        var imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function() {
        sellerListGrid.excelDownload();
    },
    /**
     * 판매업체 관리 검색
     */
    search : function() {
        if (seller.valid.search()) {

            var sellerSearchForm = $('#sellerSearchForm').serialize();
            CommonAjax.basic({
                url: '/partner/getSellerList.json',
                data: sellerSearchForm,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    seller.resetForm();
                    sellerListGrid.setData(res);
                    $('#sellerTotalCount').html($.jUtil.comma(sellerListGrid.gridView.getItemCount()));
                }
            });
        }
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schKeyword').val('');

        // 조회 일자 초기화
        seller.initSearchDate('-1y');
    },
    /**
     * 이미지 업로드
     */
    setImage : function() {
        $('#sellerImageFile').click();
    },
    /**
     * 이미지 삭제 (초기화)
     */
    unsetImage : function() {
        $('#thumbUrl').prop('src', '');
        $('#imgUrl').val('');
        $('#thumbArea').hide();
        $('#uploadArea').show();
        $('#sellerImageFile').val('');
        $('#isImageUpdate').val('1');
    },
    /**
     * 판매업체 관리 입력/수정 폼 초기화
     */
    resetForm : function(isClick) {
        $("#sellerForm").each(function () {
            this.reset();
        });

        $('[id^="seq"]').val('');

        $('#cultureDeductionNo').prop('readonly', true);
        $('#itemNo').prop('readonly', true);
        $('#partnerId').prop('readonly', false);
        $('#partnerStatusBtn').prop('disabled', true);
        $('#deliveryPopPartnerId, #deliveryPopPartnerNm').val('');
        $('#ofVendorCd').text('');
        $('[id^=categoryDelete_]').trigger('click');

        if (isClick) {
            $('#checkPartnerId').show();
        }

        $('[id^=uploadPartnerFileSpan_]').each(function(){
            $(this).html('');
        });

        $('input:radio[name="sellerInfo.saleType"]').prop('disabled', true);

        seller.isCheckPartnerId = false;
        seller.isCheckBankAccnt = false;
        seller.deliveryInfo     = null;

        $('#certificationTable [id^=certificationSeq]').val('');
        $('#certificationTable [id^=certificationState]').val('I');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    getDetail : function(selectRowId) {
        seller.resetForm(false);

        var rowDataJson = sellerListGrid.dataProvider.getJsonRow(selectRowId);

        CommonAjax.basic({
            url: "/partner/getSeller.json?partnerId="+rowDataJson.partnerId,
            method: "GET",
            callbackFunc: function (sellerDto) {
                seller.isCheckBankAccnt = true;
                $('#partnerStatusBtn').prop('disabled', false);

                $('#partnerId').val(rowDataJson.partnerId);
                $('#deliveryPopPartnerId').val(rowDataJson.partnerId);
                $('#businessNm').val(sellerDto.businessNm);
                $('#operatorType').val(sellerDto.operatorType);
                $('#operatorTypeTxt').val($('#operatorType option[value="'+sellerDto.operatorType+'"]').text());
                $('#partnerStatus').val(sellerDto.partnerStatus);
                $('#partnerGrade').val(sellerDto.partnerGrade);
                // $('#approvalStatus').val(sellerDto.approvalStatus);
                $('#partnerNm').val(sellerDto.partnerNm);
                $('#partnerOwner').val(sellerDto.partnerOwner);
                $.jUtil.valSplit(sellerDto.partnerNo, '-', 'partnerNo');
                $('#communityNotiNo').val(sellerDto.communityNotiNo);
                $('#businessConditions').val(sellerDto.businessConditions);
                $('#bizCateCd').val(sellerDto.bizCateCd);
                $('#bizCateCdNm').val(sellerDto.bizCateCdNm);
                $('#zipcode').val(sellerDto.zipcode);
                $('#addr1').val(sellerDto.addr1);
                $('#addr2').val(sellerDto.addr2);
                $.jUtil.valSplit(sellerDto.phone1, '-', 'phone1');
                $.jUtil.valSplit(sellerDto.phone2, '-', 'phone2');
                $.jUtil.valSplit(sellerDto.infoCenter, '-', 'infoCenter');
                $('#email').val(sellerDto.email);
                $('#homepage').val(sellerDto.homepage);
                $('#companyInfo').val(sellerDto.companyInfo);
                $('#categoryCd').val(sellerDto.categoryCd);
                $('#cultureDeductionNo').val(sellerDto.cultureDeductionNo);
                $('input:radio[name="sellerInfo.cultureDeductionYn"][value="'+sellerDto.cultureDeductionYn+'"]').trigger('click');
                $('input:radio[name="sellerInfo.returnDeliveryYn"][value="'+sellerDto.returnDeliveryYn+'"]').trigger('click');
                $('#smsYnNm').html(sellerDto.smsYnNm);
                $('#emailYnNm').html(sellerDto.emailYnNm);
                $('input:radio[name="sellerInfo.saleType"][value="'+sellerDto.saleType+'"]').prop('disabled', false).trigger('click');
                $('#itemNo').val(sellerDto.itemNo);

                $('#ofVendorStatusNm').val(sellerDto.ofVendorStatusNm);
                $('#ofVendorCd').text(sellerDto.ofVendorCd);

                $('#bankCd').val(sellerDto.bankCd).prop("selected",true);
                $('#bankAccountNo').val(sellerDto.bankAccountNo);
                $('#depositor').val(sellerDto.depositor);
                $('#settlePaymentYn').val(sellerDto.settlePaymentYn);

                if (!$.jUtil.isEmpty(sellerDto.settleCycleType)) {
                    $('#settleCycleType').val(sellerDto.settleCycleType);
                }

                seller.addSchCategoryHtml(sellerDto.bizCateCd, sellerDto.bizCateCdNm);
                seller.getPartnerSellerDeliveryList(rowDataJson.partnerId);

                sellerDto.typesList.forEach(function (typesList){
                        var checkVal = typesList.kind + '_' + typesList.types;
                        $("input[name='typesList.types']").each(function() {

                            if ( $(this).val() == checkVal ) {
                                $(this).prop("checked",true);
                                return false;
                            }
                        })
                    }
                );

                sellerDto.managerList.forEach(function (item, index, array) {
                    var tagId =  $('.managerDiv[data-mng-cd="'+item.mngCd+'"]');

                    tagId.find('#seq'+index).val(item.seq);
                    tagId.find('#mngNm'+index).val(item.mngNm);
                    $.jUtil.valSplit(item.mngMobile, '-', ('mngMobile' + index));
                    if (_V.chkCellPhoneNum(item.mngPhone, true) || _V.chkTelNum(item.mngPhone, true)) {
                        $.jUtil.valSplit(item.mngPhone, '-', ('mngPhone' + index));
                    }
                    tagId.find('#mngEmail'+index).val(item.mngEmail);
                });

                //로그인 인증 담당자 정보
                sellerDto.certificationManagerList.forEach(function (item, index, array) {

                    var tarId = $('#certiManagerDiv' + index);

                    tarId.find('#certificationSeq' + index).val(item.certificationSeq);
                    tarId.find('#certificationMngNm' + index).val(item.certificationMngNm);
                    tarId.find('#certificationState' + index).val('U');

                    $.jUtil.valSplit(item.certificationMngPhone, '-', ('certificationMngPhone' + index));
                });


                //첨부서류
                for (let i in sellerDto.partnerFileList) {
                    seller.file.appendPartnerFile(sellerDto.partnerFileList[i]);
                }

                $('#partnerId').prop('readonly', true);
                $('#checkPartnerId').hide();
            }
        });
    },
    /**
     * 영업담당자와 동일 담당자 정보 셋팅
     * @param obj
     */
    setSameAffiMng : function(obj) {
        if ($(obj).prop('checked')) {
            if (!$.trim($('#mngNm0').val()) || !$.trim($('#mngEmail0').val()) ||
                    !$.trim($('#mngMobile0_1').val()) || !$.trim($('#mngMobile0_2').val()) || !$.trim($('#mngMobile0_3').val())) {
                alert('영업담당자를 입력해 주세요.');
                $(obj).prop('checked', false);
            } else {
                var index = $(obj).prop('id').substr($(obj).prop('id').length-1, 1);
                $('#mngNm'+index).val($('#mngNm0').val());
                $('#mngMobile'+index+'_1').val($('#mngMobile0_1').val());
                $('#mngMobile'+index+'_2').val($('#mngMobile0_2').val());
                $('#mngMobile'+index+'_3').val($('#mngMobile0_3').val());
                $('#mngPhone'+index+'_1').val($('#mngPhone0_1').val());
                $('#mngPhone'+index+'_2').val($('#mngPhone0_2').val());
                $('#mngPhone'+index+'_3').val($('#mngPhone0_3').val());
                $('#mngEmail'+index).val($('#mngEmail0').val());
            }
        }
    },
    /**
     * 우편번호 콜백
     * @param res
     */
    setZipcode : function (res) {
        $("#zipcode").val(res.zipCode);
        $("#addr1").val(res.roadAddr);
        $("#addr2").val("");

    },
    /**
     * 판매업체 저장
     */
    setSeller : function() {
        if (seller.valid.set()) {
            $('#partnerNo').val($.jUtil.valNotSplit('partnerNo', '-'));
            $('#phone1').val($.jUtil.valNotSplit('phone1', '-'));
            $('#phone2').val($.jUtil.valNotSplit('phone2', '-'));
            $('#infoCenter').val($.jUtil.valNotSplit('infoCenter', '-'));

            for(var i=0; i < seller.mngCnt; i++) {
                $('#mngMobile' + i).val($.jUtil.valNotSplit('mngMobile' + i, '-'));
                $('#mngPhone' + i).val($.jUtil.valNotSplit('mngPhone' + i, '-'));
            }

            for(var i=0; i < 5; i++) {
                $('#certificationMngPhone' + i).val($.jUtil.valNotSplit('certificationMngPhone' + i, '-'));
            }

            let sellerForm = $('#sellerForm').serializeObject();
            sellerForm.isMod = $('#partnerId').prop('readOnly'); // 수정여부

            // 반품택배 추가
            if ($("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val() == 'Y') {
                if (seller.deliveryInfo != null) {
                    sellerForm.deliveryList = [];
                    for (let i in seller.deliveryInfo) {
                        let data = seller.deliveryInfo[i];
                        sellerForm.deliveryList.push({dlvCd: data.dlvCd, creditCd: data.creditCd, useYn: "Y"});
                    }
                }
            }

            // 판매상품유형 등
            sellerForm.typesList = [];
            $("input[name='typesList.types']:checked").each(function() {
                let data = $(this).val().split("_");
                sellerForm.typesList.push({kind: data[0], types: data[1], useYn: 'Y'});
            });

            CommonAjax.basic({
                url: "/partner/setPartner.json",
                data: JSON.stringify(sellerForm),
                method: "POST",
                contentType: "application/json",
                callbackFunc: function (res) {
                    alert(res.returnMsg);
                }
            });
        }
    },
    /**
     * 판매자 회원상태 > 회원상태 팝업
     */
    getPartnerStatusPop : function() {
        if (!$.jUtil.isEmpty($("#partnerId").val()) && $("#partnerId").prop('readonly') == true) {
            windowPopupOpen("/partner/popup/getPartnerStatusPop?partnerId="
                    + $("#partnerId").val() + "&callback="
                    + "seller.callbackPartnerStatus" + "&partnerType="
                    + "SELLER"
                    , "partnerStatus", "780", "700", "yes", "no");
        }
    },
    /**
     * 판매자 승인상태 팝업창
     */
    getPartnerApprovalStatusPop : function() {
        if (!$.jUtil.isEmpty($("#partnerId").val())) {
            windowPopupOpen("/partner/popup/getPartnerApprovalStatusPop?partnerId="
                    + $("#partnerId").val() + "&callback="
                    + "seller.callbackPartnerApprovalStatus" + "&partnerType="
                    + "SELLER"
                    , "partnerStatus", "780", "340", "yes", "no");
        }
    },
    /**
     * 판매자 사업자정보 > 사업자 등록번호 조회
     */
    getPartnerNoPop : function() {
        if (!$('#partnerId').prop('readonly')) {
            windowPopupOpen("/partner/popup/getPartnerNoPop?partnerType=SELL" + "&callback=" + "seller.callbackPartnerNoChk"
                    , "partnerNocheck", "480", "183", "yes", "no");
        }
    },
    /**
     * 판매자 사업자정보 > 사업자 정보 입력 ( 콜백 )
     */
    callbackPartnerNoChk : function(data) {
        $('#partnerNm').val(data.partnerNm);
        $('#partnerOwner').val(data.partnerOwner);
        $('#partnerNo_1').val(data.partnerNo.substring(0, 3));
        $('#partnerNo_2').val(data.partnerNo.substring(3, 5));
        $('#partnerNo_3').val(data.partnerNo.substring(5));

        let checkPartnerNo = Number(data.partnerNo.substring(3, 5));

        if (80 < checkPartnerNo && checkPartnerNo < 89) {
            $('#operatorType').val('CB');
        } else {
            $('#operatorType').val('PB');
        }

        $('#operatorTypeTxt').val($('#operatorType option:selected').text());
    },
    /**
     * PW 관리 팝업창
     */
    getPartnerPwPop : function () {
        if (!$.jUtil.isEmpty($("#partnerId").val()) && $("#partnerId").prop('readonly') == true) {
            windowPopupOpen("/partner/popup/getPartnerPwPop?partnerId=" + $("#partnerId").val() + "&businessNm=" + $("#businessNm").val()
                    , "sendPartnerInfo", "460", "400", "yes", "no");
        } else {
            $.jUtil.alert('패스워드를 전송할 업체를 선택해주세요.');
        }
    },
    /**
     * 종목 조회
     */
    getBizCateCd : function() {
        CommonAjax.basic({
            url: "/partner/getBizCateCdList.json",
            data: {
                codeNm: $("#bizCateCdNm").val()
            },
            method: "GET",
            callbackFunc: function (res) {
                var resultCount = res.length;

                if (resultCount == 0) {
                    $("#searchCategory").html("");
                    $("#searchCategory").append(
                            "<li class='ui-menu-item-wrapper'>검색결과가 없습니다.</li>");
                } else {
                    $("#searchCategory").html("");

                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];

                        $("#searchCategory").append(
                                "<li class='ui-menu-item-wrapper' id='categorySearchList_"
                                + data.mcCd + "'>" + data.mcNm + "</li>");
                    }
                }
                $("#searchCategory").show();
            }
        });
    },
    /**
     * 종목 추가
     * @param obj
     */
    addSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];
        var categoryCdNm = $(obj).html();

        seller.addSchCategoryHtml(categoryCd, categoryCdNm);
        $("#searchCategory").hide();
    },
    /**
     * 종목 추가 html
     * @param categoryCd
     * @param categoryCdNm
     * @returns {boolean}
     */
    addSchCategoryHtml : function(categoryCd, categoryCdNm) {
        $('#bizCateCd').val(categoryCd);
        $('#bizCateCdNm').val(categoryCdNm);

        if ($("#bizCateCdDiv_" + categoryCd).length > 0) {
            $("#bizCateCdDiv_" + categoryCd).show();
        } else {
            var html = "<span id='bizCateCdDiv_" + categoryCd + "' class='text-normal'>";
            html += "<span class='mg-r-10 mg-l-10 mg-t-10'>" + categoryCdNm + "</span>";
            html += "<button type='button' class='ui button small delete-thumb' id='categoryDelete_" + categoryCd + "'>x</button>";
            html += "</span>";

            $("#bizCateCdListId").html(html);
        }
    },
    /**
     * 종목 삭제
     * @param obj
     */
    delSchCategory : function(obj) {
        var idArr = $(obj).attr("id").split("_");
        var categoryCd = idArr[1];

        $("#bizCateCdDiv_" + categoryCd).hide();
    },
    /**
     * 소득공제 제공여부
     */
    setCultureDeductionYn : function(cultureDeductionYn) {
        if (typeof cultureDeductionYn == 'object') {
            cultureDeductionYn = $(cultureDeductionYn).val();
        }

        if (cultureDeductionYn == 'Y') {
            $('#cultureDeductionNo').prop('readonly', false);
        } else {
            $('#cultureDeductionNo').val('').prop('readonly', true);
        }
    },
    /**
     * 반품택배사 계약정보 팝업창
     */
    getReturnDeliveryPop : function () {
        if ($("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val() == 'Y') {
            $("#callback").val("seller.callbackDelivery");
            $("#deliveryInfo").val(JSON.stringify(seller.deliveryInfo));
            windowPopupOpen("/partner/popup/getReturnDeliveryPop", "delivery_window", "630", "510", "yes", "no");
            $("form[name=deliveryPop]").submit();
        } else {
            alert('반품택배사 있음으로 설정해주세요.');
        }
    },
    /**
     * 반품택배사 계약정보 콜백
     * @param obj
     */
    callbackDelivery : function(obj) {
        if (obj != null) {
            seller.deliveryInfo = obj;
        }
    },
    /**
     * 반품택배사 조회
     * @param partnerId
     */
    getPartnerSellerDeliveryList : function(partnerId) {
        CommonAjax.basic({
            url: "/partner/getPartnerDeliveryList.json",
            data: {partnerId: partnerId},
            method: "GET",
            callbackFunc: function (res) {
                if (res.length > 0) {
                    seller.deliveryInfo = res;
                }
            }
        });
    },
    /**
     * 파트너관리 > 통신판매업신고번호 조회
     * @param partnerNo
     */
    getCommunityNotiNo: function (partnerNo) {
        if (partnerNo) {
            CommonAjax.basic({
                url: '/partner/getCommuniNotiNo.json',
                data: {partnerNo: partnerNo},
                method: 'GET',
                callbackFunc: function (res) {
                    if (res.returnCode === "1" && !$.jUtil.isEmpty(res.returnKey)) {
                        $("#communityNotiNo").prop('readonly', true).val(res.returnKey);
                    } else {
                        alert("해당 사업자등록번호로 인증된 통신판매업신고번호가 없습니다.");
                        $("#communityNotiNo").focus();
                    }
                }
            });

        }
    },
    /**
     * 계좌실명 인증 조회
     * @returns {*|boolean}
     */
    getBankAccntNoCertify : function() {
        if ($.jUtil.isEmpty($("#bankCd").val())) {
            return $.jUtil.alert('은행을 선택해주세요.', 'bankCd');
        }

        if ($.jUtil.isEmpty($("#bankAccountNo").val())) {
            return $.jUtil.alert('계좌번호를 입력해주세요.', 'bankAccountNo');
        }

        CommonAjax.basic({
            url: "/partner/getBankAccntNoCertify.json",
            data: {
                bankCd: $("#bankCd").val(),
                bankAccntNo: $("#bankAccountNo").val()
            },
            method: "POST",
            callbackFunc: function (res) {
                if ($.jUtil.isNotEmpty(res.depositor)) {
                    $('#depositor').val(res.depositor);
                    alert('인증되었습니다.');
                    $('#bankStatus').text('인증완료');
                    $('#getAccAuthenticationBtn').text('재설정');
                    $('#bankCd, #bankAccountNo').prop('disabled', true);
                    $('#depositor').val(res.depositor);
                } else {
                    $('#depositor').val('');
                    alert('인증 실패하였습니다.');
                }

                seller.isCheckBankAccnt = res;
            }
        });
    }
};

seller.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#schRegStartDate");
        var endDt = $("#schRegEndDate");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            seller.initSearchDate('-1y');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 검색어 체크
        var schKeyword = $('#schKeyword').val();

        if (schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            } else if (schKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        return true;
    },
    set : function() {
        var isValid = true;

        var checkObj = {
            partnerId: {
                types: ['ENG_LOWER', 'NUM'],
                min: 5,
                max: 12
            },
            businessNm: {   //판매업체명
                types: ['KOR', 'ENG', 'NUM', 'SPC_1'],
                min: 2,
                max: 50
            },
            partnerOwner: { //대표자명
                types: ['KOR', 'ENG', 'COMMA'],
                min: 2,
                max: 50
            },
            businessConditions: { //업태
                types: ['KOR', 'ENG'],
                min: 2,
                max: 50
            }
        };

        for (var id in checkObj) {
            var checkVal = $('#' + id).val();

            if (!$.jUtil.isAllowInput(checkVal, checkObj[id].types) || checkVal.length < checkObj[id].min || checkVal.length > checkObj[id].max) {
                alert(checkObj[id].min + "~" + checkObj[id].max + "자의 " + $.jUtil.getAllowInputMessage(checkObj[id].types));
                $('#' + id).focus();
                isValid = false;
                break;
            }
        }

        let partnerNm = $('#partnerNm').val().replace(/\s/gi, "").replace('(주)','').replace('주)','').replace('주식회사','').replace(/[^0-9a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣]/gi,'');
        let partnerOwner = $('#partnerOwner').val().replace(/\s/gi, "").replace('(주)','').replace('주)','').replace('주식회사','').replace(/[^0-9a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣]/gi,'');
        let depositor = $('#depositor').val().replace(/\s/gi, "").replace('(주)','').replace('주)','').replace('주식회사','').replace(/[^0-9a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣]/gi,'');

        for( var k = 0; k < 5; k++ ){

            if( !( $.jUtil.isEmpty($('#certificationMngNm' + k).val()) && $.jUtil.isEmpty($('#certificationMngPhone' + k + '_1').val())
                && $.jUtil.isEmpty($('#certificationMngPhone' + k + '_2').val()) && $.jUtil.isEmpty($('#certificationMngPhone' + k + '_3').val()) )  ){

                // 로그인 인증 담당자명 한글,영문
                var types = [ 'ENG' , 'KOR' ];
                if( !$.jUtil.isAllowInput($('#certificationMngNm' + k).val() , types )  ){
                    alert('로그인 인증담당자명 형식이 잘못되었습니다. 다시 확인해 주세요.');
                    isValid = false;
                    return false;
                }

                // 로그인 인증 담당자 핸드폰번호
                var phoneNumber = $('#certificationMngPhone' + k + '_1').val() + '-' + $('#certificationMngPhone' + k + '_2').val() + '-' + $('#certificationMngPhone' + k + '_3').val()
                if(!$.jUtil.phoneValidate( phoneNumber, true)) {
                    alert('로그인 인증담당자 휴대폰번호 형식이 잘못되었습니다. 다시 확인해 주세요.');
                    isValid = false;
                    return false;
                }
            }else{
                $('#certificationState' + k ).val('D');
            }
        };

        let checkDepositor = false;
        let checkConfirm = false;
        let checkConfirmMsg = '예금주와 상호명이 불일치합니다. 입력한 정보로 저장하시겠습니까?';

        if (depositor.includes(partnerNm)) {
            checkDepositor = true;
        }

        if ($('#operatorType').val() == 'CB') {
            if (!checkDepositor) {
                checkConfirm = true;
            }
        } else {
            if (checkDepositor || depositor.includes(partnerOwner)) {
            } else {
                checkConfirm = true;
                checkConfirmMsg = '예금주와 상호명이 불일치합니다. 예금주는 상호명과 일치하거나 상호명 또는 대표자명이 포함되어야 합니다. 입력한 정보대로 저장하시겠습니까?';
            }
        }

        if (checkConfirm && !confirm(checkConfirmMsg)) {
            isValid = false;
        }

        if (!isValid) {
            return false;
        } else {
            //반품택배사
            if (!$("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val()) {
                alert('반품택배사 정보를 입력해 주세요.');
                return false;
            } else {
                if ($("input:radio[name='sellerInfo.returnDeliveryYn']:checked").val() == 'Y') {
                    if (seller.deliveryInfo == null || seller.deliveryInfo.length < 1) {
                        alert('반품택배사 계약정보를 입력해 주세요.');
                        return false;
                    }
                }
            }

            if ($("input:radio[name='sellerInfo.cultureDeductionYn']:checked").val() == 'Y' && $.jUtil.isEmpty($('#cultureDeductionNo').val())) {
                return $.jUtil.alert('사업자 식별번호를 입력해주세요.', 'cultureDeductionNo');
            }

            if (!seller.isCheckBankAccnt) {
                return $.jUtil.alert('입금계좌의 계좌실명인증을 완료해주세요.', 'depositor');
            }

            return true;
        }
    },
    /**
     * 판매자 기본정보 > 아이디 중복확인
     */
    checkPartnerId : function() {
        var partnerId = $('#partnerId').val();

        if ($.jUtil.isEmpty(partnerId)) {
            alert('사용할 아이디를 입력해주세요.');
            return;
        }

        if (!$.jUtil.isAllowInput(partnerId, ['ENG_LOWER', 'NUM']) || $.jUtil.isNotAllowInput($('#partnerId').val(), ['SPACE']) || partnerId.length < 5 || partnerId.length > 12) {
            alert("5~12자의 영문 소문자, 숫자만 사용가능합니다.");
            $("#partnerId").focus();
            return;
        }

        if (partnerId.substr(0, 4).toLowerCase() == 'affi') {
            alert('affi로 시작하는 아이디는 판매업체 계정으로 사용할 수 없습니다.');
            $("#partnerId").focus();
            return;
        }

        seller.isCheckPartnerId = false;

        CommonAjax.basic({
            url: "/partner/getSellerIdCheck.json",
            data: {partnerId: partnerId, partnerType: 'SELL'},
            method: "GET",
            callbackFunc: function (res) {
                if (res.returnCnt == 0) {
                    seller.isCheckPartnerId = true;
                    alert("사용 가능한 아이디입니다.");
                }
            },
            errorCallbackFunc : function(resError) {
                alert(resError.responseJSON.returnMessage);
                $("#partnerId").val("").focus();
            }
        });
    }
};

seller.file = {
    init : function() {
        seller.file.event();
    },
    event : function() {
        //파일선택시
        $('input[name="partnerFile1"], input[name="partnerFile2"], input[name="partnerFile3"], input[name="partnerFile4"], input[name="partnerFile5"]').change(function() {
            seller.file.addFile($(this).data('file-id'), $(this).prop('name'));
        });

        //판일삭제
        $('[id^=uploadPartnerFileSpan_]').on('click', '.close', function() {
            if (confirm('등록된 파일을 삭제하시겠습니까?')) {
                $(this).closest('.partnerFile').remove();
            }
        });

        $('#partnerFileTable').on('click', '.partnerDownLoad', function() {
            $.jUtil.downloadFile($(this).data('url'), $(this).data('file-nm'), $(this).data('process-key'));
        });
    },
    clickFile : function(obj) {
        let limit = Number($('input[name="'+obj.data('field-name')+'"]').data('limit'));

        if ($('#uploadPartnerFileSpan_'+obj.data('file-type')).find('.close').length >= limit) {
            return $.jUtil.alert("파일 추가는 최대 "+limit+"개까지 가능합니다.");
        }

        $('input[name="'+obj.data('field-name')+'"]').click();
    },
    /**
     * 파일업로드
     */
    addFile : function(id, fieldName) {
        let file = $('input[name="'+fieldName+'"]');

        let params = {
            processKey : "PartnerJoinBinary",
            fieldName : fieldName,
            mode : "FILE"
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        seller.file.appendPartnerFile(f.fileStoreInfo, id);
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    appendPartnerFile : function(f, id) {
        if (id) { //등록
            let html = '<span class="ui span-button text mg-r-10 partnerFile">'
                    + '<input type="hidden" class="fileSeq" name="partnerFileList[].fileSeq" value=""/>'
                    + '<input type="hidden" class="fileType" name="partnerFileList[].fileType" value="'+$('#'+id).data('file-type')+'"/>'
                    + '<input type="hidden" class="fileUrl" name="partnerFileList[].fileUrl" value="'+f.fileId+'"/>'
                    + '<input type="hidden" class="fileNm" name="partnerFileList[].fileNm" value="'+f.fileName+'"/>'
                    + '<span class="partnerDownLoad" data-url="'+ f.fileId + '" '
                    + 'data-process-key="PartnerJoinBinary" '
                    + 'data-file-nm="' + f.fileName + '" '
                    + '" target="_blank" style="color:blue; text-decoration: underline; cursor: pointer;">'
                    + f.fileName
                    + '</span><span class="text close mg-l-5">[X]</span></span>';

            $('#'+id).append(html);
        } else { //조회
            let html = '<span class="ui span-button text mg-r-10 partnerFile">'
                    + '<input type="hidden" class="fileSeq" name="partnerFileList[].fileSeq" value="'+f.fileSeq+'"/>'
                    + '<input type="hidden" class="fileType" name="partnerFileList[].fileType" value="'+f.fileType+'"/>'
                    + '<input type="hidden" class="fileUrl" name="partnerFileList[].fileUrl" value="'+f.fileUrl+'"/>'
                    + '<input type="hidden" class="fileNm" name="partnerFileList[].fileNm" value="'+f.fileNm+'"/>'
                    + '<span class="partnerDownLoad" data-url="'+ f.fileUrl + '" '
                    + 'data-process-key="PartnerJoinBinary" '
                    + 'data-file-nm="' + f.fileNm + '" '
                    + '" target="_blank" style="color:blue; text-decoration: underline; cursor: pointer;">'
                    + f.fileNm
                    + '</span><span class="text close mg-l-5">[X]</span></span>';

            $('#uploadPartnerFileSpan_'+f.fileType).append(html);
        }
    }
};