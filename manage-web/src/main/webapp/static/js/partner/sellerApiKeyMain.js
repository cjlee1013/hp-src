/**
 * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
 */
$(document).ready(function() {
    sellerApiKeyListGrid.init();
    sellerApiKey.init();
    CommonAjaxBlockUI.global();
});

// sellerApiKey 그리드
var sellerApiKeyListGrid = {
    gridView : new RealGridJS.GridView("sellerApiKeyListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
      sellerApiKeyListGrid.initGrid();
      sellerApiKeyListGrid.initDataProvider();
      sellerApiKeyListGrid.event();
    },
    initGrid : function() {
      sellerApiKeyListGrid.gridView.setDataSource(sellerApiKeyListGrid.dataProvider);
      sellerApiKeyListGrid.gridView.setStyles(sellerApiKeyListGridBaseInfo.realgrid.styles);
      sellerApiKeyListGrid.gridView.setDisplayOptions(sellerApiKeyListGridBaseInfo.realgrid.displayOptions);
      sellerApiKeyListGrid.gridView.setColumns(sellerApiKeyListGridBaseInfo.realgrid.columns);
      sellerApiKeyListGrid.gridView.setOptions(sellerApiKeyListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
      sellerApiKeyListGrid.dataProvider.setFields(sellerApiKeyListGridBaseInfo.dataProvider.fields);
      sellerApiKeyListGrid.dataProvider.setOptions(sellerApiKeyListGridBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
      sellerApiKeyListGrid.dataProvider.clearRows();
      sellerApiKeyListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function () {
        if (sellerApiKeyListGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName = "셀러연동_APIKey_" + _date.getFullYear() + (_date.getMonth() + 1)
                + _date.getDate();

        sellerApiKeyListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};

// sellerApiKey 관리
var sellerApiKey = {
    /**
     * 초기화
     */
    init : function() {
        this.event();
        this.initSearchDate('-30d');
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function (flag) {
        sellerApiKey.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        sellerApiKey.setDate.setEndDate(0);
        sellerApiKey.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    event : function() {
        $('#schBtn').bindClick(sellerApiKey.search); //sellerApiKey 검색
        $('#schResetBtn').bindClick(sellerApiKey.searchFormReset); //sellerApiKey 검색폼 초기화
        $("#excelDownloadBtn").click(function() {
            sellerApiKeyListGrid.excelDownload();
        });
    },
    /**
     * sellerApiKey 검색
     */
    search : function() {
        if(!sellerApiKey .valid.search()){
            return;
        }

        CommonAjax.basic({
            url : '/partner/getSellerApiKeyList.json?' + $('#sellerApiKeySchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                sellerApiKeyListGrid.setData(res);
                $('#sellerApiKeyTotalCount').html($.jUtil.comma(sellerApiKeyListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        sellerApiKey.initSearchDate('-30d');
        $('#schUseYn, #schAgencyCd').val('');
    },
    /**
     * 판매업체 조회 팝업창
     */
    getPartnerPop : function(callBack) {
        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback="
            + callBack + "&partnerType="
            + "SELL"
            , "partnerStatus", "1080", "600", "yes", "no");
    },
    /**
     * 일괄 사용여부 수정
     * @param useYn
     * @returns {boolean}
     */
    setSellerApiKeyUseYn : function(useYn) {
        let plusapiSeq = '';
        let rowArr = sellerApiKeyListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('상품을 선택해주세요.');
            return false;
        }

        for (let i in rowArr) {
            let rowData = sellerApiKeyListGrid.dataProvider.getJsonRow(rowArr[i]);
            plusapiSeq += ',' + rowData.plusapiSeq;
        }

        CommonAjax.basic({
            url: "/partner/setSellerApiKeyUseYn.json",
            data: {
                plusapiSeqList: plusapiSeq, useYn: useYn
            },
            method: "POST",
            callbackFunc: function (res) {
                alert(rowArr.length + '건의 상품이 변경되었습니다.');
                $('#schBtn').trigger('click');
            }
        });
    }
};

/**
 * sellerApiKey 검색,입력,수정 validation check
 */
sellerApiKey.valid = {
    search : function () {
        const startDt = $("#schStartDt");
        const endDt = $("#schEndDt");

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-30d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

      return true;
    }
};

sellerApiKey.callBack = {
    schPartner : function(res) {
        $('#schPartnerId').val(res[0].partnerId);
        $('#schPartnerNm').val(res[0].partnerNm);
    }
};
