/** Main Script */
var shipCompanyManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        shipCompanyManageMain.getLinkCompanyCodeList();
        shipCompanyManageMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipCompanyManageMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipCompanyManageMain.reset, "shipCompanyManageSearchForm");

        // 연동업체설정 버튼
        $("#shipLinkCompanySetPop").bindClick(shipCompanyManageMain.openShipLinkCompanySetPop);

        // 하단 [저장] 버튼
        $("#saveBtn").bindClick(shipCompanyManageMain.save);
        // 하단 [초기화] 버튼
        $("#resetBtn").bindClick(shipCompanyManageMain.reset, "shipCompanyDetailForm");

        // 상세정보 연동업체 버튼
        $("#dlvLinkCompany").on('change', function() { shipCompanyManageMain.setDlvLinkCompany() });
        // 상세정보 택배사명 버튼
        $("#dlvCd").on('change', function() { shipCompanyManageMain.setDetailInfo() });
    },

    /**
     *  연동업체코드 리스트 조회
     */
    getLinkCompanyCodeList: function() {
        CommonAjax.basic({
            url         : '/ship/shipManage/getLinkCompanyCodeList.json?company=',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                linkCompanyCodeList = res;
            }
        });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!shipCompanyManageMain.valid()) {
            return;
        }

        var reqData = $("#shipCompanyManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/ship/shipManage/getShipCompanyManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipCompanyManageGrid.setData(res);
                $('#shipCompanyManageSearchCnt').html($.jUtil.comma(shipCompanyManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {

        var schKeywordTypeVal = $("#schKeywordType").val();
        var schKeywordlen = $("#schKeyword").val().length;
        if (!$.jUtil.isEmpty(schKeywordTypeVal) && schKeywordlen > 0 && schKeywordlen < 2) {
            alert("2글자 이상 검색해 주세요.");
            return false;
        }

        return true;
    },

    /**
     * 상세정보 택배사명 선택 시, 등록된 정보 셋팅
     */
    setDetailInfo : function(){
        var rows = shipCompanyManageGrid.dataProvider.getJsonRows();
        var dlvCd = $("#dlvCd").val();

        // 선택항목 초기화
        shipCompanyManageMain.reset("shipCompanyDetailForm");
        $("#dlvCd").val(dlvCd);

        // 기등록된 택배사가 정보가 있으면 셋팅
        for (var idx in rows) {
            if (dlvCd == rows[idx].dlvCd) {
                shipCompanyManageMain.gridRowSelect(idx);
            }
        }
    },

    /**
     * 상세정보 연동업체 셀렉트 박스 클릭 시, 연동업체코드 셋팅
     */
    setDlvLinkCompany : function(){
        var dlvLinkCompanyVal = $("#dlvLinkCompany").val();
        var appendHtml = "";

        switch (dlvLinkCompanyVal) {
            case "SWT" :
                appendHtml += '<select id="dlvLinkCompanyCd" name="dlvLinkCompanyCd" class="ui input medium mg-r-10" style="width: 230px">';
                appendHtml +=    '<option value="">코드</option>';
                for (var idx in linkCompanyCodeList) {
                    if (linkCompanyCodeList[idx].dlvLinkCompany == "SWT") {
                        var linkCompanyCd = linkCompanyCodeList[idx].linkCompanyCd;
                        var linkCompanyCdNm = linkCompanyCodeList[idx].linkCompanyCdNm;
                        
                        appendHtml += '<option value="' + linkCompanyCd + '">' + "[" + linkCompanyCd + "] " + linkCompanyCdNm + '</option>'
                    }
                }
                appendHtml += '</select>';
                break;
            default :
                appendHtml += '<input type="text" id="dlvLinkCompanyCd" name="dlvLinkCompanyCd" class="ui input" style="width: 230px" placeholder="코드" maxlength="15">';
                break;
        }

        $("#dlvLinkCompanyCd").remove();
        $("#dlvLinkCompany").parent().append(appendHtml);
    },

    /**
     * 검색영역/상세정보영역 폼 초기화
     * @param formId
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        // 상세정보 폼 reset 추가 작업
        if (formId === "shipCompanyDetailForm") {
            $("#dlvCd option:eq(0)").prop("selected", true);
            $("#dlvLinkCompany option:eq(0)").prop("selected", true);
            $("#dlvLinkCompanyCd option:eq(0)").prop("selected", true);
            $('input:radio[name=autoReturnYn]').prop("checked", false);
            $('input:radio[name=autoReturnDelayYn]').prop("checked", false);;
            $('input:radio[name=displayYn]').prop("checked", false);
            $("#csPhone").val("");
            $("#homePage").val("");
            $("#note").val("");

            shipCompanyManageMain.setDlvLinkCompany();
        }
    },

    /**
     * 연동업체설정 팝업창 open
     */
    openShipLinkCompanySetPop: function() {
        windowPopupOpen("/ship/popup/getShipLinkCompanySetPop?callback=", "shipLinkCompanySetPop", 640, 550, "yes", "yes");
    },

    /**
     * 데이터 저장
     */
    save: function() {
        if (!shipCompanyManageMain.saveValid()) {
            return;
        }

        if (!confirm("저장하시겠습니까?")) {
            return;
        }

        var data = new Object();

        data.dlvCd = $("#dlvCd").val();
        data.dlvLinkCompany = $("#dlvLinkCompany").val();
        data.dlvLinkCompanyCd = $("#dlvLinkCompanyCd").val();
        data.autoReturnYn = $("input[name='autoReturnYn']:checked").val();
        data.autoReturnDelayYn = $("input[name='autoReturnDelayYn']:checked").val();
        data.displayYn = $("input[name='displayYn']:checked").val();
        data.csPhone = $("#csPhone").val();
        data.homePage = $("#homePage").val();
        data.note = $("#note").val();

        CommonAjax.basic({
            url         : "/ship/shipManage/setShipCompanyInfo.json",
            data        : JSON.stringify(data),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function(res) {
                if (res.returnCode != "SUCCESS") {
                    alert(res.returnMessage);
                } else {
                    alert("저장되었습니다.");
                    shipCompanyManageMain.search();
                }
            }
        });
    },
    /**
     * 데이터 저장 전 유효성 체크
     */
    saveValid: function() {
        var dlvCdVal = $("#dlvCd").val();
        if ($.jUtil.isEmpty(dlvCdVal)) {
            alert("택배사명을 선택해 주세요.");
            return false;
        }

        var dlvLinkCompanyVal = $("#dlvLinkCompany").val();
        if ($.jUtil.isEmpty(dlvLinkCompanyVal)) {
            alert("연동업체를 선택해 주세요.");
            return false;
        }

        var dlvLinkCompanyCdVal = $("#dlvLinkCompanyCd").val();
        if ($.jUtil.isEmpty(dlvLinkCompanyCdVal)) {
            alert("연동업체코드를 선택해 주세요.");
            return false;
        }

        var autoReturnYnVal = $("input[name='autoReturnYn']:checked").val();
        if ($.jUtil.isEmpty(autoReturnYnVal)) {
            alert("자동반품접수 여부를 선택해 주세요.");
            return false;
        }

        var autoReturnDelayYnVal = $("input[name='autoReturnDelayYn']:checked").val();
        if ($.jUtil.isEmpty(autoReturnDelayYnVal)) {
            alert("자동반품지연 여부를 선택해 주세요.");
            return false;
        }

        var displayYnVal = $("input[name='displayYn']:checked").val();
        if ($.jUtil.isEmpty(displayYnVal)) {
            alert("노출여부를 선택해 주세요.");
            return false;
        }

        return true;
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = shipCompanyManageGrid.dataProvider.getJsonRow(selectRowId);

        $("#dlvCd").val(rowDataJson.dlvCd);
        $("#dlvLinkCompany").val(rowDataJson.dlvLinkCompany);
        $('input:radio[name=autoReturnYn]:input[value="' +rowDataJson.autoReturnYn+ '"]').prop("checked", true);
        $('input:radio[name=autoReturnDelayYn]:input[value="' +rowDataJson.autoReturnDelayYn+ '"]').prop("checked", true);
        $('input:radio[name=displayYn]:input[value="' +rowDataJson.displayYn+ '"]').prop("checked", true);
        $("#csPhone").val(rowDataJson.csPhone);
        $("#homePage").val(rowDataJson.homePage);
        $("#note").val(rowDataJson.note);

        shipCompanyManageMain.setDlvLinkCompany();
        $("#dlvLinkCompanyCd").val(rowDataJson.dlvLinkCompanyCd);
    }
};

/** Grid Script */
var shipCompanyManageGrid = {
    gridView: new RealGridJS.GridView("shipCompanyManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipCompanyManageGrid.initGrid();
        shipCompanyManageGrid.initDataProvider();
        shipCompanyManageGrid.event();
    },
    initGrid: function () {
        shipCompanyManageGrid.gridView.setDataSource(shipCompanyManageGrid.dataProvider);
        shipCompanyManageGrid.gridView.setStyles(shipCompanyManageGridBaseInfo.realgrid.styles);
        shipCompanyManageGrid.gridView.setDisplayOptions(shipCompanyManageGridBaseInfo.realgrid.displayOptions);
        shipCompanyManageGrid.gridView.setColumns(shipCompanyManageGridBaseInfo.realgrid.columns);
        shipCompanyManageGrid.gridView.setOptions(shipCompanyManageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipCompanyManageGrid.dataProvider.setFields(shipCompanyManageGridBaseInfo.dataProvider.fields);
        shipCompanyManageGrid.dataProvider.setOptions(shipCompanyManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipCompanyManageGrid.gridView.onDataCellClicked = function(gridView, index) {
            shipCompanyManageMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        shipCompanyManageGrid.dataProvider.clearRows();
        shipCompanyManageGrid.dataProvider.setRows(dataList);
    }
};
