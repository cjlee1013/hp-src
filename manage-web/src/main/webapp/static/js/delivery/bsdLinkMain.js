/** Main Script */
var bsdLinkMain = {
    /**
     * init 이벤트
     */
    init: function() {
        bsdLinkMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(bsdLinkMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(bsdLinkMain.reset, "bsdLinkSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(bsdLinkMain.excelDownload);

        // 전송 버튼
        $("#send").bindClick(bsdLinkMain.send);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#bsdLinkSearchForm").serialize();
        CommonAjax.basic({
            url         : '/ship/shipManage/getBsdLinkList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                bsdLinkGrid.setData(res);
                $('#bsdLinkSearchCnt').html($.jUtil.comma(bsdLinkGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 전송
     */
    send: function () {
        var checkRowIds = bsdLinkGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var bsdLinkNoList = new Array();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var checkRowId of checkRowIds) {
            var checkedRows = bsdLinkGrid.dataProvider.getJsonRow(checkRowId);
            console.log(checkedRows.linkResult);
            if (checkedRows.linkResult == 'Y') {
                alert("이미 전송된 건은 불가합니다.");
                return;
            }

            bsdLinkNoList.push(checkedRows.bsdLinkNo);
        }

        reqData.bsdLinkNoList = bsdLinkNoList;

        if (confirm("선택하신 주문건에 대해서 주문확인을 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/ship/shipManage/setBsdLinkTran.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                    } else {
                        alert("전송 했습니다.");
                    }

                    bsdLinkMain.search();
                }
            });
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (bsdLinkGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "BSD연동조회_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        bsdLinkGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        //  검색조건 (주문번호)
        //  선택 후 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 29) {
            alert("조회기간은 30일까지 설정 가능합니다.");
            shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-29d", "0");
            return false;
        }

        return true;
    }
}

/** Grid Script */
var bsdLinkGrid = {
    gridView: new RealGridJS.GridView("bsdLinkGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        bsdLinkGrid.initGrid();
        bsdLinkGrid.initDataProvider();
        bsdLinkGrid.event();
    },
    initGrid: function () {
        bsdLinkGrid.gridView.setDataSource(bsdLinkGrid.dataProvider);
        bsdLinkGrid.gridView.setStyles(bsdLinkGridBaseInfo.realgrid.styles);
        bsdLinkGrid.gridView.setDisplayOptions(bsdLinkGridBaseInfo.realgrid.displayOptions);
        bsdLinkGrid.gridView.setColumns(bsdLinkGridBaseInfo.realgrid.columns);
        bsdLinkGrid.gridView.setOptions(bsdLinkGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        bsdLinkGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "invoiceNo", showUrl: false});

        // 전체선택 false
        //shipManageGrid.gridView.setCheckBar({showAll: false});
    },
    initDataProvider: function () {
        bsdLinkGrid.dataProvider.setFields(bsdLinkGridBaseInfo.dataProvider.fields);
        bsdLinkGrid.dataProvider.setOptions(bsdLinkGridBaseInfo.dataProvider.options);
    },
    event: function() {
        bsdLinkGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var dlvCd = grid.getValue(index.itemIndex, "dlvCd");
            var invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");

            if (index.fieldName == "purchaseOrderNo") {
                orderUtil.setLinkTab("orderDetail", "주문정보 상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo );
            } else if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo, "shipHistoryPop", 649, 744, 0,0);
            }
        };
    },
    setData: function (dataList) {
        bsdLinkGrid.dataProvider.clearRows();
        bsdLinkGrid.dataProvider.setRows(dataList);
    }
};