const shippingUtil = {
    /**
     * 배송방법 설정
     */
    setChangeStoreType: function() {
        var shipMethodRef = $('#schStoreType').val();

        if (shipMethodRef != "DS" && shipMethodRef != "EXP") {
            shipMethodRef = "TD";
        }

        if (shipMethodRef == 'DS') {
            $('#schStoreId').prop("placeholder","판매업체ID");
            $('#schStoreNm').prop("placeholder","판매업체");
        } else {
            $('#schStoreId').prop("placeholder","점포ID");
            $('#schStoreNm').prop("placeholder","점포명");
        }

        $('#schShipMethod option').each(function () {
            var $this = $(this);
            var mallType = $this.attr("title");
            var shipMethod = $this.val();

            if (mallType != undefined) {
                if (shipMethodRef == "AURORA") {
                    if (shipMethod == "TD_DRCT") {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                } else if (shipMethodRef == "EXP") {
                    if (shipMethod == "TD_PICK" || shipMethod == "TD_QUICK") {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                } else {
                    if (mallType == shipMethodRef) {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                }
            }
        });

        $("#schShipMethod option:eq(0)").prop("selected", true);
    },

    /**
     * 배송방법 설정
     */
    setChangeStoreType2: function() {
        var shipMethodRef = $('#schStoreType').val();

        if (shipMethodRef != "DS") {
            shipMethodRef = "TD";
        }

        if (shipMethodRef == 'DS') {
            $('#schStoreId').prop("placeholder","판매업체ID");
            $('#schStoreNm').prop("placeholder","판매업체");
        } else {
            $('#schStoreId').prop("placeholder","점포ID");
            $('#schStoreNm').prop("placeholder","점포명");
        }
    },

    /**
     * DateCustom 이벤트
     */
    initSearchDateCustom : function(startId, endId, startDt, endDt) {
        var setDateVal = Calendar.datePickerRange(startId, endId);
        setDateVal.setEndDate(endDt);
        setDateVal.setStartDate(startDt);
        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },

    /**
     * Date 이벤트
     */
    initSearchDate : function(startId, startDt) {
        var setDateVal = Calendar.datePicker(startId);
        setDateVal.setDate(startDt);
    },

    /**
     * 말일 체크
     */
    isLastDay : function () {
        var now = new Date();
        var lastDate = new Date(now.getFullYear(),now.getMonth()+1,0);

        var nowD = now.getDate();
        var lastD = lastDate.getDate();

        return nowD == lastD;
    },

    /**
     * 오늘 날짜 가져오기 (yyyy-mm-dd)
     */
    getToday : function(){
        var date = new Date();
        var year = date.getFullYear();
        var month = ("0" + (1 + date.getMonth())).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);

        return year + "-" + month + "-" + day;
    }
};