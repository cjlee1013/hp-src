/** Main Script */
var shipMonitoringShipMain = {
    global : {
        tabId : ""
    },

    /**
     * init 이벤트
     */
    init: function() {
        shipMonitoringShipMain.setStoreOffice();
        shipMonitoringShipMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
        shippingUtil.setChangeStoreType();
        shipMonitoringShipMain.global.tabId = parent.Tabbar.getActiveTab();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(shipMonitoringShipMain.openStorePopup);
        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { shippingUtil.setChangeStoreType() });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipMonitoringShipMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipMonitoringShipMain.reset, "shipMonitoringShipSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(shipMonitoringShipMain.excelDownload);

        // 달력 - [오늘] 버튼
        // $("#setTodayBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","0", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-7d", "-1d")});
        // 달력 - [1개월] 버튼
        $("#setOneMonthBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-30d", "-1d")});
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
        }
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#shipMonitoringShipSearchForm").serialize();

        CommonAjax.basic({
            url         : '/ship/shipMonitoring/getShipMonitoringShipList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipMonitoringShipGrid.setData(res);
                $('#shipMonitoringShipSearchCnt').html($.jUtil.comma(shipMonitoringShipGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipMonitoringShipGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "배송모니터링_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipMonitoringShipGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.setChangeStoreType();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
        shipMonitoringShipMain.setStoreOffice();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        //  검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'bundleNo'
                || schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 점포 필수 선택
        // var schStoreIdVal = $("#schStoreId").val();
        // if ($.jUtil.isEmpty(schStoreIdVal)) {
        //     alert("점포/판매업체를 선택해 주세요.");
        //     return false;
        // }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);
        var todayFormat = moment(shippingUtil.getToday(), 'YYYY-MM-DD', true).add(-1,"day");

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if(todayFormat.diff(schEndDt, "day", true) < 0) {
            alert("전일자까지만 조회 가능합니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 29) {
            alert("조회기간은 30일까지 설정 가능합니다.");
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    }
}

/** Grid Script */
var shipMonitoringShipGrid = {
    gridView: new RealGridJS.GridView("shipMonitoringShipGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipMonitoringShipGrid.initGrid();
        shipMonitoringShipGrid.initDataProvider();
        shipMonitoringShipGrid.event();
    },
    initGrid: function () {
        shipMonitoringShipGrid.gridView.setDataSource(shipMonitoringShipGrid.dataProvider);
        shipMonitoringShipGrid.gridView.setStyles(shipMonitoringShipGridBaseInfo.realgrid.styles);
        shipMonitoringShipGrid.gridView.setDisplayOptions(shipMonitoringShipGridBaseInfo.realgrid.displayOptions);
        shipMonitoringShipGrid.gridView.setColumns(shipMonitoringShipGridBaseInfo.realgrid.columns);
        shipMonitoringShipGrid.gridView.setOptions(shipMonitoringShipGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        shipMonitoringShipGrid.gridView.setColumnProperty("invoiceNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "invoiceNo", showUrl: false});
        shipMonitoringShipGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});
        shipMonitoringShipGrid.gridView.setColumnProperty("userNoMask", "renderer", {type: "link", url: "unnecessary", requiredFields: "userNoMask", showUrl: false});
        shipMonitoringShipGrid.gridView.setColumnProperty("noRcvDeclrDt", "renderer", {type: "link", url: "unnecessary", requiredFields: "noRcvDeclrDt", showUrl: false});
    },
    initDataProvider: function () {
        shipMonitoringShipGrid.dataProvider.setFields(shipMonitoringShipGridBaseInfo.dataProvider.fields);
        shipMonitoringShipGrid.dataProvider.setOptions(shipMonitoringShipGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipMonitoringShipGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");
            var dlvCd = grid.getValue(index.itemIndex, "dlvCd");
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var userNo = grid.getValue(index.itemIndex, "userNo");
            var orderItemNo = grid.getValue(index.itemIndex, "orderItemNo");
            var bundleNo = grid.getValue(index.itemIndex, "bundleNo");
            var shipNo = grid.getValue(index.itemIndex, "shipNo");

            if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo, "shipHistoryPop", 649, 744, 0,0);
            } else if (index.fieldName == "purchaseOrderNo") {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
            } else if (index.fieldName == "userNoMask") {
                UIUtil.addTabWindowBesideParent(
                    "/user/userInfo/detail?userNo=" + userNo, "userDetailTab",
                    "회원정보 상세", 120, shipMonitoringShipMain.global.tabId);
            }  else if (index.fieldName == "noRcvDeclrDt" ) {
                var _url = '/order/popup/noReceiveShipPop?';
                var noRcvDataParam = {
                    'orderItemNo' : orderItemNo,
                    'bundleNo' : bundleNo,
                    'userNo' : userNo,
                    'purchaseOrderNo' : purchaseOrderNo,
                    'shipNo' : shipNo,
                    'noRcvPopType' : 'COMPLETE'
                };

                orderUtil.getWinPopup(_url.concat(jQuery.param(noRcvDataParam)), 900, 100, "noReceiveShipPop");
            }
        };
    },
    setData: function (dataList) {
        shipMonitoringShipGrid.dataProvider.clearRows();
        shipMonitoringShipGrid.dataProvider.setRows(dataList);
    }
};