/** 픽업 점포 관리 Main */
var pickupStoreManageMng = {
    imageUrlBase: 'https://image.homeplus.co.kr',
    /**
     * init 이벤트
     */
    init: function () {
        pickupStoreManageMng.bindEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!pickupStoreManageMng.validSearch()) {
                return false;
            }
            pickupStoreManageMng.search();
        });
        // 초기화 버튼
        $("#resetBtn").bindClick(pickupStoreManageMng.resetInfo);
    },
    /**
     * 마스터 조회조건 validation
     */
    validSearch: function () {
        // 점포유형 필수값 체크
        var schStoreType = $("#schStoreType").val();
        if ($.jUtil.isEmpty(schStoreType)) {
            alert("점포유형을 선택해 주세요.");
            return false;
        }
        return true;
    },
    /**
     * 픽업점포관리 마스터 조회
     */
    search: function () {
        $("#storeId").html('');
        $("#drawDiv").children().remove();
        var data = $("#pickupStoreManageSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getPickupStoreManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                pickupStoreManageGrid.setData(dataList);
                $("#pickupStoreManageSearchCnt").html($.jUtil.comma(pickupStoreManageGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 픽업점포 상세정보 조회
     */
    searchDetail: function (data) {
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getPickupStoreManageList.json?schStoreType='+data.storeType+'&schStoreId='+data.storeId,
            data        : null,
            method      : "GET",
            callbackFunc: function (dataList) {
                console.log(dataList);
                pickupStoreManageMng.drawDetail(dataList);
            }
        });
    },
    /**
     * 픽업점포 락커 리스트 조회 및 세팅
     */
    searchLockerList: function (storeId) {
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getPickupStoreLockerList.json?storeId='+storeId,
            data        : null,
            method      : "GET",
            callbackFunc: function (lockerList) {
                console.log(lockerList);
                if ($.jUtil.isNotEmpty(lockerList)) {
                    $("td[id^=head_]").attr('style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9; width: 100px');
                    $("td[id^=grid_]").attr('style', 'border-right: 1px solid #eee; text-align: center;').html(' ');

                    for (let locker of lockerList) {
                        var lockerHeadText = locker.useYn.toUpperCase() === 'Y' ? '(사용)' : '(사용안함)';
                        var lockerCnt = Number(locker.lockerNo.slice(-2));
                        $("td[id=head_"+locker.placeNo+"_"+lockerCnt+"]").html('락커 '+lockerCnt+' '+lockerHeadText);
                        $("td[id=grid_"+locker.placeNo+"_"+lockerCnt+"]").html(locker.lockerNo);
                    }
                }
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#pickupStoreManageSearchForm").resetForm();
    },
    /**
     * 그리드에서 클릭한 정보를 상세정보에 그려주기
     */
    drawDetail: function (dataList) {
        $("#storeId").html(dataList[0].storeId.lpad(4,"0")+" | "+dataList[0].storeNm);
        $("#drawDiv").children().remove(); // 그리기 전에 기존 그리드 삭제

        var tableCnt = 0;
        for (let store of dataList) {
            tableCnt += 1;
            var imgText = '';
            if ($.jUtil.isNotEmpty(store.imgUrl)) {
                var temp = store.imgUrl.split(",")[0]; // imgUrl 필드에 ",텍스트" 들어오는 경우가 있어 삭제처리
                var imgUrlParam = "'"+temp+"'";
                var urlArr = temp.split("/");
                var imageUrl = urlArr[urlArr.length-1]; // imgUrl 필드 마지막에 파일명만 가져옴
                imgText = '<a href="#" class="text text-blue text-underline" onclick="pickupStoreManageMng.previewImage('+imgUrlParam+')">'+imageUrl+'</a>';
            }

            $("#drawDiv").append($('<div class="com wrap-title sub">').attr('id', 'placeDetailDiv_'+tableCnt))
                .append($('<div class="ui wrap-table horizontal mg-t-10">').attr('id', 'tableHead_'+tableCnt));
            $("div[id=placeDetailDiv_"+tableCnt+"]").append($('<h3 class="subTitle"/>').text('픽업장소 '+tableCnt));
            $("div[id=tableHead_"+tableCnt+"]").append($('<table class="ui table">').attr('id', 'placeDetailTable_'+tableCnt, 'style', 'table-layout: fixed'));
            $("table[id=placeDetailTable_"+tableCnt+"]").append($('<colgroup>')).append($('<tbody>'));
            $("table[id=placeDetailTable_"+tableCnt+"] colgroup").append($('<col width="10%">')).append($('<col width="40%">'))
                .append($('<col width="10%">')).append($('<col width="40%">'));

            const tableRow1 = $('<tr>');
            tableRow1.append($('<th>').html('장소사용여부'));
            tableRow1.append($('<td>').html(store.placeUseYn));
            tableRow1.append($('<th>').html('이미지'));
            tableRow1.append($('<td>').html(imgText));
            const tableRow2 = $('<tr>');
            tableRow2.append($('<th>').html('픽업장소'));
            tableRow2.append($('<td>').html(store.placeNm));
            tableRow2.append($('<th>').html('전화번호'));
            tableRow2.append($('<td>').html(store.telNo));
            const tableRow3 = $('<tr>');
            tableRow3.append($('<th>').html('픽업장소상세'));
            tableRow3.append($('<td>').attr('colspan', 3).html(store.placeAddr));
            const tableRow4 = $('<tr>');
            tableRow4.append($('<th>').html('안내문구<br>(Web 노출)'));
            tableRow4.append($('<td>').attr('colspan', 3).html(store.placeExpln));
            const tableRow5 = $('<tr>');
            tableRow5.append($('<th>').html('운영시간'));
            tableRow5.append($('<td>').attr('colspan', 3).html(store.pickupTime));
            const tableRow6 = $('<tr>');
            tableRow6.append($('<th>').html('락커설정'));
            tableRow6.append($('<td>').attr('colspan', 3).html(store.lockerUseYn.toUpperCase() === 'Y' ? '락커 사용(Y)' : '락커 미사용(N)'));
            if (store.lockerUseYn.toUpperCase() === 'Y') {
                tableRow6.find("td").append($('<br>'));
                tableRow6.find("td").append($('<table class="ui wrap-table">').attr('id', 'lockerTable_'+tableCnt, 'style', 'table-layout: fixed'));
                tableRow6.find("td").append($('<br>'));
                tableRow6.find("td").append($('<div class="mg-t-10">')
                    .html('* 락커 운영 장소의 daily capa는 락커 운영 수와 동일<br>* 고객에게 락커번호 끝 2자리만 커뮤니케이션 함. 락커 문짝 표기도 동일.  e.g. 024101 -> 1, 024112 -> 12'));
                const lockerTableRow1 = $('<tr>');
                lockerTableRow1.append($('<td>').attr('id', 'head_'+store.placeNo+'_1', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 1'));
                lockerTableRow1.append($('<td>').attr('id', 'head_'+store.placeNo+'_2', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 2'));
                lockerTableRow1.append($('<td>').attr('id', 'head_'+store.placeNo+'_3', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 3'));
                lockerTableRow1.append($('<td>').attr('id', 'head_'+store.placeNo+'_4', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 4'));
                lockerTableRow1.append($('<td>').attr('id', 'head_'+store.placeNo+'_5', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 5'));
                const lockerTableRow2 = $('<tr>');
                lockerTableRow2.append($('<td>').attr('id', 'grid_'+store.placeNo+'_1'));
                lockerTableRow2.append($('<td>').attr('id', 'grid_'+store.placeNo+'_2'));
                lockerTableRow2.append($('<td>').attr('id', 'grid_'+store.placeNo+'_3'));
                lockerTableRow2.append($('<td>').attr('id', 'grid_'+store.placeNo+'_4'));
                lockerTableRow2.append($('<td>').attr('id', 'grid_'+store.placeNo+'_5'));
                const lockerTableRow3 = $('<tr>');
                lockerTableRow3.append($('<td>').attr('id', 'head_'+store.placeNo+'_6', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 6'));
                lockerTableRow3.append($('<td>').attr('id', 'head_'+store.placeNo+'_7', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 7'));
                lockerTableRow3.append($('<td>').attr('id', 'head_'+store.placeNo+'_8', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 8'));
                lockerTableRow3.append($('<td>').attr('id', 'head_'+store.placeNo+'_9', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 9'));
                lockerTableRow3.append($('<td>').attr('id', 'head_'+store.placeNo+'_10', 'style', 'border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;').html('락커 10'));
                const lockerTableRow4 = $('<tr>');
                lockerTableRow4.append($('<td>').attr('id', 'grid_'+store.placeNo+'_6'));
                lockerTableRow4.append($('<td>').attr('id', 'grid_'+store.placeNo+'_7'));
                lockerTableRow4.append($('<td>').attr('id', 'grid_'+store.placeNo+'_8'));
                lockerTableRow4.append($('<td>').attr('id', 'grid_'+store.placeNo+'_9'));
                lockerTableRow4.append($('<td>').attr('id', 'grid_'+store.placeNo+'_10'));
                tableRow6.find("table[id='lockerTable_"+tableCnt+"']").append(lockerTableRow1).append(lockerTableRow2).append(lockerTableRow3).append(lockerTableRow4);
            }
            $("table[id=placeDetailTable_"+tableCnt+"]").append(tableRow1).append(tableRow2).append(tableRow3)
                .append(tableRow4).append(tableRow5).append(tableRow6);
        }
        pickupStoreManageMng.searchLockerList(dataList[0].storeId);
    },
    /** 이미지 미리보기 팝업 */
    previewImage : function(imgUrlParam) {
        let imgUrl = pickupStoreManageMng.imageUrlBase + imgUrlParam;
        if (imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
};

/** Grid Script */
var pickupStoreManageGrid = {
    gridView: new RealGridJS.GridView("pickupStoreManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        pickupStoreManageGrid.initGrid();
        pickupStoreManageGrid.initDataProvider();
        pickupStoreManageGrid.event();
    },
    initGrid: function () {
        pickupStoreManageGrid.gridView.setDataSource(pickupStoreManageGrid.dataProvider);
        pickupStoreManageGrid.gridView.setStyles(pickupStoreManageGridBaseInfo.realgrid.styles);
        pickupStoreManageGrid.gridView.setDisplayOptions(pickupStoreManageGridBaseInfo.realgrid.displayOptions);
        pickupStoreManageGrid.gridView.setColumns(pickupStoreManageGridBaseInfo.realgrid.columns);
        pickupStoreManageGrid.gridView.setColumnProperty("storeNm", "mergeRule", {criteria:"value"});
        pickupStoreManageGrid.gridView.setOptions(pickupStoreManageGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        pickupStoreManageGrid.dataProvider.setFields(pickupStoreManageGridBaseInfo.dataProvider.fields);
        pickupStoreManageGrid.dataProvider.setOptions(pickupStoreManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        pickupStoreManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = pickupStoreManageGrid.dataProvider.getJsonRow(index.dataRow);
            pickupStoreManageMng.searchDetail(data);
        };
    },
    setData: function (dataList) {
        pickupStoreManageGrid.dataProvider.clearRows();
        pickupStoreManageGrid.dataProvider.setRows(dataList);
    }
};