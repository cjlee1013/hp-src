/** 선물세트 발송지 관리 Main */
var reserveShipPlaceManageMng = {
    /**
     * init 이벤트
     */
    init: function() {
        reserveShipPlaceManageMng.bindEvent();
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-7d", "0");
        Calendar.datePicker("shipDt");
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function() {
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!reserveShipPlaceManageMng.validSearch()) {
                return false;
            }
            reserveShipPlaceManageMng.search();
        });
        // 검색영역 초기화 버튼
        $("#searchResetBtn").bindClick(reserveShipPlaceManageMng.schReset);
        // 엑셀 다운로드 버튼
        $("#excelDownloadBtn").bindClick(reserveShipPlaceManageMng.excelDownload);
        // 엑셀 업로드 버튼
        $("#excelUploadBtn").bindClick(reserveShipPlaceManageMng.excelUpload);
        // 삭제 버튼
        $("#removeBtn").bindClick(reserveShipPlaceManageMng.remove);
        // 저장 버튼
        $("#saveBtn").bindClick(reserveShipPlaceManageMng.save);
        // 상세정보 초기화 버튼
        $("#resetBtn").bindClick(reserveShipPlaceManageMng.resetInfo);
    },
    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawDetail: function (data) {
        $('input:radio[name=shipPlaceStoreYn]:input[value="'+ data.shipPlaceStoreYn +'"]').prop("checked", true);
        $('input:radio[name=shipPlaceAnseongYn]:input[value="'+ data.shipPlaceAnseongYn +'"]').prop("checked", true);
        $('input:radio[name=shipPlaceHamanYn]:input[value="'+ data.shipPlaceHamanYn +'"]').prop("checked", true);
        $("#shipDt").val(data.shipDt);
    },
    /** 조회조건 validation */
    validSearch: function () {
        // 검색일자 empty & 1년 초과 검색제한
        if (!deliveryCore.dateValid($("#schFromDt"), $("#schEndDt"), ["isValid", "overOneYearRange"])) {
            return false;
        }
        return true;
    },
    /**
     * 선물세트 발송지 조회
     */
    search: function() {
        var data = $("#reserveShipPlaceSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/shipManage/getReserveShipPlaceMngList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(dataList) {
                console.log(dataList);
                reserveShipPlaceManageGrid.setData(dataList);
                $('#reserveShipPlaceSearchCnt').html($.jUtil.comma(reserveShipPlaceManageGrid.gridView.getItemCount()));
                //기본정보 초기화
                reserveShipPlaceManageMng.resetInfo();
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    schReset: function() {
        deliveryCore.initSearchDateCustom("schFromDt", "schEndDt", "-7d", "0");
    },
    /**
     * 상세정보 초기화
     */
    resetInfo: function() {
        $("#reserveShipPlaceInputForm input[type='text']").val("");
        $("#reserveShipPlaceInputForm input[type='radio']:input[value='Y']").prop("checked", true);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(reserveShipPlaceManageGrid.gridView.getItemCount() === 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "선물세트발송지관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        reserveShipPlaceManageGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 엑셀 업로드
     */
    excelUpload: function() {
        var popUrl = "/escrow/popup/reserveShipExcelUploadPop";
        var callBackUrl = "reserveShipPlaceManageMng.excelUploadCallback";
        var templateUrl = "/static/templates/reserveShipPlace_template.xlsx";
        $.jUtil.openNewPopup(popUrl+"?callback="+callBackUrl+"&templateUrl="+templateUrl, 600, 250);
    },
    /**
     * 엑셀 업로드 callback
     */
    excelUploadCallback: function(res) {
        alert("엑셀업로드 결과" + "\n등록: " + res.totalCnt + ", 성공: " + res.successCnt + ", 실패: " + res.errorCnt);
        reserveShipPlaceManageMng.search();
    },
    /**
     * 데이터 삭제
     */
    remove: function() {
        var checkedList = reserveShipPlaceManageGrid.gridView.getCheckedRows();
        if (checkedList.length === 0) {
            alert("삭제할 데이터를 체크하세요.");
            return false;
        }
        if(!confirm("삭제 하시겠습니까?")) {
            return false;
        }

        var targetList = new Array();
        for (var checkRowId of checkedList) {
            var data = new Object();
            var checkedRow = reserveShipPlaceManageGrid.dataProvider.getJsonRow(checkRowId);
            data.shipDt = checkedRow.shipDt;
            targetList.push(data);
        }
        CommonAjax.basic({
            url         : "/escrow/shipManage/deleteReserveShipPlaceList.json",
            data        : JSON.stringify(targetList),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("삭제되었습니다.");
                reserveShipPlaceManageMng.search();
            }
        });
    },
    /**
     * 데이터 저장 전 데이터 유효성 체크
     */
    valid: function() {
        // 발송일 선택여부 체크
        if ($.jUtil.isEmpty($("#shipDt").val())) {
            alert("발송일은 필수값입니다. 다시 확인해주세요.");
            return false;
        }
        return true;
    },
    /**
     * 선물세트 발송지 저장
     */
    save: function() {
        // Form 데이터 유효성 체크
        if (!reserveShipPlaceManageMng.valid()) {
            return false;
        }
        if(!confirm("저장 하시겠습니까?")) {
            return false;
        }
        CommonAjax.basic({
            url         : '/escrow/shipManage/saveReserveShipPlace.json',
            data        : _F.getFormDataByJson($("#reserveShipPlaceInputForm")),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                reserveShipPlaceManageMng.search();
            }
        });
    },
};

/** Grid Script */
var reserveShipPlaceManageGrid = {
    gridView: new RealGridJS.GridView("reserveShipPlaceManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        reserveShipPlaceManageGrid.initGrid();
        reserveShipPlaceManageGrid.initDataProvider();
        reserveShipPlaceManageGrid.event();
    },
    initGrid: function () {
        reserveShipPlaceManageGrid.gridView.setDataSource(reserveShipPlaceManageGrid.dataProvider);
        reserveShipPlaceManageGrid.gridView.setStyles(reserveShipPlaceManageGridBaseInfo.realgrid.styles);
        reserveShipPlaceManageGrid.gridView.setDisplayOptions(reserveShipPlaceManageGridBaseInfo.realgrid.displayOptions);
        reserveShipPlaceManageGrid.gridView.setColumns(reserveShipPlaceManageGridBaseInfo.realgrid.columns);
        reserveShipPlaceManageGrid.gridView.setOptions(reserveShipPlaceManageGridBaseInfo.realgrid.options);
        reserveShipPlaceManageGrid.gridView.setCheckBar({visible: true, exclusive: false, showAll: false});
    },
    initDataProvider: function () {
        reserveShipPlaceManageGrid.dataProvider.setFields(reserveShipPlaceManageGridBaseInfo.dataProvider.fields);
        reserveShipPlaceManageGrid.dataProvider.setOptions(reserveShipPlaceManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        reserveShipPlaceManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = reserveShipPlaceManageGrid.dataProvider.getJsonRow(index.dataRow);
            reserveShipPlaceManageMng.drawDetail(data);
        };
    },
    setData: function (dataList) {
        reserveShipPlaceManageGrid.dataProvider.clearRows();
        reserveShipPlaceManageGrid.dataProvider.setRows(dataList);
    }
};