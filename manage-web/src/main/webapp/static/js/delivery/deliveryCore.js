/**
 * ---------------------------------
 * date picker 및 주문개발파트에서 공통으로 사용 할 함수 모음
 * Created by 주문개발파트 on 2020.03
 * ---------------------------------
 * @type {{deliveryCore}, {deliveryCore_popup}, {deliveryCore.callBack}, {deliveryCore_category}}
 */
/* date picker 관련 함수 */
var deliveryCore = {
    /**
     * (상단 검색영역) 시작일 종료일 초기화
     * > 호출위치 : 작업할 페이지 스크립트 init 함수 안
     *
     * @param startDt 검색시작일 default 일자
     * @param endDt   검색종료일 default 일자
     *
     * @example deliveryCore.initSearchDate("0", "+30d");
     */
    initSearchDate : function(startDt, endDt) {
        deliveryCore.changeSearchDate(startDt, endDt);
    },
    /**
     * (상단 검색영역) 시작일 종료일 초기화
     * > 달력 ID 가 'schApply...Dt' 포멧이 아닌경우 선언
     * > 호출위치 : 작업할 페이지 스크립트 init 함수 안
     *
     * @param startId   검색시작일 ID
     * @param endId     검색종료일 ID
     * @param startDt   시작일자
     * @param endDt     종료일자
     *
     * @example deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "+7d");
     */
    initSearchDateCustom : function(startId, endId, startDt, endDt) {
        deliveryCore.changeSearchDateCustom(startId, endId, startDt, endDt);
    },

    /**
     * (상단 검색영역) 시작일 종료일 변경
     *
     * @param startDt 검색시작일
     * @param endDt   검색종료일
     * @example $("#setTodayBtn").on("click", () => {deliveryCore.changeSearchDate("0", "0")});
     */
    changeSearchDate : function(startDt, endDt) {
        var setDateVal = Calendar.datePickerRange('schApplyStartDt', 'schApplyEndDt');
        setDateVal.setStartDate(startDt);
        setDateVal.setEndDate(endDt);
        $("#schApplyEndDt").datepicker("option", "minDate", $("#schApplyStartDt").val());
    },
    /**
     * (상단 검색영역) 시작일 종료일 변경
     * > 달력 ID 가 'schApply...Dt' 포멧이 아닌경우 선언
     *
     * @param startId   검색시작일 ID
     * @param endId     검색종료일 ID
     * @param startDt   시작일자
     * @param endDt     종료일자
     *
     * @example $("#setTodayBtn").on("click", () => {deliveryCore.changeSearchDateCustom(""schFromDt", "schEndDt", 0", "0")});
     */
    changeSearchDateCustom : function(startId, endId, startDt, endDt) {
        var setDateVal = Calendar.datePickerRange(startId, endId);
        setDateVal.setStartDate(startDt);
        setDateVal.setEndDate(endDt);
        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },

    /**
     * (하단 기본정보영역) 적용시작일 적용종료일 초기화
     * > 호출위치 : 작업 할 페이지 스크립트 init 함수 안
     *
     * @param startDt  적용시작일 default 일자
     * @param endDt    적용종료일 default 일자
     *
     * @example deliveryCore.initApplyDate("0", "0");
     */
    initApplyDate : function(startDt, endDt) {
        var setDateVal = Calendar.datePickerRange('applyStartDt', 'applyEndDt');
        setDateVal.setStartDate(startDt);
        setDateVal.setEndDate(endDt);
        $("#applyEndDt").datepicker("option", "minDate", $("#applyStartDt").val());
    },
    /**
     * (하단 기본정보영역) 적용시작일 적용종료일 초기화
     * > 달력 ID 가 'apply...Dt' 포멧이 아닌경우 선언
     * > 호출위치 : 작업 할 페이지 스크립트 init 함수 안
     *
     * @param startId  적용시작일 ID
     * @param endId    적용종료일 ID
     * @param startDt  적용시작일 default 일자
     * @param endDt    적용종료일 default 일자
     *
     * @example deliveryCore.initApplyDateCustom("detailStartDt", "detailEndDt", "0", "0");
     */
    initApplyDateCustom : function(startId, endId, startDt, endDt) {
        var setDateVal = Calendar.datePickerRange(startId, endId);
        setDateVal.setStartDate(startDt);
        setDateVal.setEndDate(endDt);
        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },

    /**
     * 날짜 입력값에 대한 검증
     *
     * @param {object} _startObj    시작일
     * @param {object} _endObj      종료일
     * @param {array}  checkProps   검증항목
     *  - "isValid"
     *  - "overOneMonthRange"
     *  - "overOneYearRange"
     * @return {boolean} true:정상, false:비정상
     *
     * @example if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid"])) { return false; }
     */
    dateValid : function(_startObj, _endObj, checkProps) {
        var startDtVal = $("#"+_startObj.attr("id")).val();
        var endDtVal = $("#"+_endObj.attr("id")).val();
        var schStartDtVal = moment(startDtVal, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(endDtVal, 'YYYY-MM-DD', true);
        for (var checkProp of checkProps) {
            // [isValid] 날짜가 비어있거나 날짜형식(YYYY-MM-DD) 에 상이한지 check
            if (checkProp === "isValid") {
                if (!schStartDtVal.isValid() || !schEndDtVal.isValid()) {
                    alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                    return false;
                }
            }
            // [overOneMonthRange] 조회일자 범위가 1개월이 초과되는지 check
            if (checkProp === "overOneMonthRange") {
                if (schEndDtVal.diff(schStartDtVal, "month", true) > 1) {
                    alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                    _startObj.val(schEndDtVal.add(-1, "month").format("YYYY-MM-DD"));
                    return false;
                }
            }
            // [overOneYearRange] 조회일자 범위가 1년이 초과되는지 check
            if (checkProp === "overOneYearRange") {
                if (schEndDtVal.diff(schStartDtVal, "year", true) > 1) {
                    alert("조회기간은 최대 1년까지 설정가능합니다. 다시 확인해주세요.");
                    _startObj.val(schEndDtVal.add(-1, "year").format("YYYY-MM-DD"));
                    return false;
                }
            }
            // [overOneWeekRange] 조회일자 범위가 일주일 초과되는지 check
            if (checkProp === "overOneWeekRange") {
                if (schEndDtVal.diff(schStartDtVal, "day", true) > 6) {
                    alert("조회기간은 최대 일주일까지 설정가능합니다. 다시 확인해주세요.");
                    _startObj.val(schEndDtVal.add(-6, "day").format("YYYY-MM-DD"));
                    return false;
                }
            }
        }
        return true;
    }
};

/* 팝업창 관련 함수 */
var deliveryCore_popup = {
    /**
     * 팝업창 open
     *
     * @param {string} url
     * @param {null|string} target
     * @param {int} w       - width
     * @param {int} h       - height
     *
     * @example deliveryCore_popup.openPopup("/escrow/popup/closeDayImagePop", null, 1100, 1100);
     */
    openPopup : function(url, target, w, h) {
        if (target == null) {
            target = "_blank";
        }
        windowPopupOpen(url, target, w, h, "yes", "yes");
    },
    /**
     * 점포명 조회 팝업창 open
     *
     * @param callBack
     * @example <button...onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
     */
    openStorePop : function(callBack) {
        var storeTypeVal = $('#schStoreType').val();
        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return false;
        }

        deliveryCore_popup.openPopup("/common/popup/storePop?callback=" + callBack + "&storeType=" + storeTypeVal + "&storeRange=ALL" , "partnerStatus", 1100, 620);
    },
    /**
     * 점포선택 팝업창 open
     * > jsp 에 점포선택 팝업창을 호출하기 위한 hidden form 태그 생성해야함
     * >> itemShiftManageMain.jsp - $("#storeSelectPopForm") 참고
     *
     * @example <button...onclick="deliveryCore_popup.openStoreSelectPopup();">점포선택</button>
     */
    openStoreSelectPopup: function() {
        deliveryCore_popup.openPopup("", "storeSelectPop", 600, 450);
        $("#storeSelectPopForm").attr("method", "post")
                                .attr("action", "/common/popup/storeSelectPop")
                                .attr("target", "storeSelectPop")
                                .submit();
    },
    /**
     * 상품검색 팝업창 open
     * >> itemShiftManageMain.jsp - $("#storeSelectPopForm") 참고
     *
     * @param callback  콜백함수              - 필수
     * @param isMulti   Y:다건조회, N:단건조회  - 선택(df:Y)
     * @param mallType  ALL,TD,DS...        - 선택(df:ALL)
     * @param storeType HYPER,CLUB...       - 선택(df:HYPER)
     *
     * @example <button...onclick="deliveryCore_popup.bb('deliveryCore.callBack.openItemSearchPopup', 'N');">상품검색</button>
     */
    openItemSearchPopup: function(callback, isMulti, mallType, storeType) {
        var fullUrl = "/common/popup/itemPop";
        fullUrl += "?callback=" + callback;
        fullUrl += $.jUtil.isEmpty(isMulti) ? "&isMulti=Y" : "&isMulti=" + isMulti;
        fullUrl += $.jUtil.isEmpty(mallType) ? "" : "&mallType=" + mallType;
        fullUrl += $.jUtil.isEmpty(storeType) ? "&storeType=HYPER" : "&storeType=" + storeType;
        deliveryCore_popup.openPopup(fullUrl, "_blank", 1084, 650);
    },
    /**
     * 엑셀 일괄등록 팝업창 open
     *
     * @param templateUrl   엑셀양식이 존재하는 위치
     * @param callback      파일선택 후 호출할 callBack script
     *
     * @example deliveryCore_popup.openExcelInsertPop(templateUrl, 'itemShiftManageMain.aa');
     */
    openExcelInsertPop: function(templateUrl, callback) {
        var fullUrl = "/escrow/popup/excelInsertPop";
        fullUrl += "?templateUrl=" + templateUrl;
        fullUrl += "&callback=" + callback;
        deliveryCore_popup.openPopup(fullUrl, "_blank", 600, 250);
    },
    /**
     * 엑셀 일괄수정 팝업창 open
     *
     * @param templateUrl   엑셀양식이 존재하는 위치
     * @param callback      파일선택 후 호출할 callBack script
     *
     * @example deliveryCore_popup.openExcelUpdatePop(templateUrl, 'itemShiftManageMain.aa');
     */
    openExcelUpdatePop: function(templateUrl, callback) {
        var fullUrl = "/escrow/popup/excelUpdatePop";
        fullUrl += "?templateUrl=" + templateUrl;
        fullUrl += "&callback=" + callback;
        deliveryCore_popup.openPopup(fullUrl, "_blank", 600, 250);
    },
};
deliveryCore.callBack = {
    // 점포조회 팝업창 callBack
    setStoreInfo : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    // 점포검색 팝업창 callBack
    setStoreNoList : function(res) {
        console.log(res);
        // 공통 팝업에서 내려주는 storeType 이 없으면 (radio selected value 로 내려줌) 화면에 있는 storeType select box 선택값으로 세팅
        $("#applyStoreType").text($.jUtil.isEmpty(res.storeType) ? $("#storeType").val() : res.storeType);
        $("#applyStoreList").text(JSON.stringify(res.storeList));
        $("#applyStoreListCount").text(res.storeList.length);
    },
    // 상품검색 팝업창 callBack
    setItemInfo : function(resDataArr) {
        console.log(resDataArr);
        $('#applyItemNo').text(resDataArr[0].itemNo);
        $('#applyItemNm').text(resDataArr[0].itemNm);
    },
};

/* 카테고리 관련 함수 */
var deliveryCore_category = {
    /**
     * 대중소세 카테고리 노출 및 동작하도록 지원하는 함수
     * > 호출위치 : 해당 페이지 init 함수 안
     * > jsp 파일에 commonCategory.js 에 대해 CDN 선언 '필수'!
     *
     * @example deliveryCore_category.initCategory();
     */
    initCategory : function() {
        commonCategory.setCategorySelectBox(1, "", "", $("#schCateCd1"));
        commonCategory.setCategorySelectBox(2, "", "", $("#schCateCd2"),true);
        commonCategory.setCategorySelectBox(3, "", "", $("#schCateCd3"),true);
        commonCategory.setCategorySelectBox(4, "", "", $("#schCateCd4"),true);
    },
    /**
     * 대중소세 카테고리를 알맞게 선택해주는 함수
     * > 호출위치 : 그리드 클릭시 수행하는 함수 안
     * > jsp 파일에 commonCategory.js 에 대해 CDN 선언 '필수'!
     *
     * @param {object} data : json data
     * @example deliveryCore_category.changeCategory(data);
     */
    changeCategory : function(data) {
        commonCategory.setCategorySelectBox(1, '', data.lcateCd, $('#cateCd1'));
        commonCategory.setCategorySelectBox(2, data.lcateCd, data.mcateCd, $('#cateCd2'));
        commonCategory.setCategorySelectBox(3, data.mcateCd, data.scateCd, $('#cateCd3'));
        commonCategory.setCategorySelectBox(4, data.scateCd, data.dcateCd, $('#cateCd4'));
    }
};

/* ============ string prototype 재정의 함수 ============= */
/**
 * 좌측문자열채우기
 *
 * @param {int}     padLen 최대 채우고자 하는 길이
 * @param {string}  padStr 채우고자 하는 문자
 *
 * @example $("#schStoreId").val(schStoreIdVal.lpad(4, "0"));
 */
String.prototype.lpad = function(padLen, padStr) {
    var str = this;
    if (padStr.length > padLen) {
        console.log("오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다");
        return str + "";
    }
    while (str.length < padLen) {
        str = padStr + str;
    }
    str = str.length >= padLen ? str.substring(0, padLen) : str;
    return str;
};