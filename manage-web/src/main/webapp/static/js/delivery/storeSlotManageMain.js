var storeSlotManageMain = {

    /**
     * init 이벤트
     */
    init: function() {
        storeSlotManageMain.bindingEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "+1w");
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        // 상단 검색영역 검색
        $("#schBtn").bindClick(storeSlotManageMain.search);

        // 상단 검색영역 초기화
        $("#schResetBtn").bindClick(storeSlotManageMain.resetSch);

        // 오늘
        $("#setTodayBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "0") ;
        });
        // 1주일
        $("#setOneWeekBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "+1w") ;
        });
        // 1개월
        $("#setOneMonthBtn").on("click", () => {
            deliveryCore.changeSearchDateCustom("schStartDt", "schEndDt", "0", "+1m") ;
        });
        // 엑셀다운로드
        $("#excelDownloadBtn").bindClick(storeSlotManageMain.excelDownload);

        // 사용여부 변경
        $("#chgUseYnBtn").bindClick(storeSlotManageMain.saveUseYn);
    },

    /**
     * 검색영역 초기화
     */
    resetSch : function () {
        $("#storeSlotSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "+1w");
    },

    /**
     * slot 검색
     * @returns {boolean}
     */
    search: function() {
        if (!validCheck.search()) {
            return false;
        }

        var data = $("#storeSlotSearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getSlotManageList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                storeSlotGrid.setData(res);
                $('#storeSlotSearchCnt').html($.jUtil.comma(storeSlotGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 사용여부 변경
     */
    saveUseYn: function () {
        var checkRowIds = storeSlotGrid.gridView.getCheckedRows();
        var useYn = $("#saveUseYn").val();
        var storeId = $("#schStoreId").val();

        if(!validCheck.save(checkRowIds, useYn)) {
            return false;
        }
        //선택한 slot array
        var reqSlotArray = [];

        for (var checkRowId of checkRowIds) {
            //datetime format 형식으로 가져오기 위해 getJsonRow 대신 getOutputRow 를 사용함.
            var checkedRows = storeSlotGrid.dataProvider.getOutputRow({datetimeFormat: "yyyy-MM-dd"}, checkRowId);

            var slotObj = {};

            slotObj.slotId = checkedRows.slotId;
            slotObj.storeId = storeId;
            slotObj.shipDt = checkedRows.shipDt;
            slotObj.useYn = useYn;

            reqSlotArray.push(slotObj);
        }

        console.log(reqSlotArray);

        CommonAjax.basic({
            url         : '/escrow/storeDelivery/saveSlotManage.json',
            data        : JSON.stringify(reqSlotArray),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(data) {
                console.log(data);
                alert("저장하였습니다.");
                storeSlotManageMain.search();
            }
        });
    },

    /**
     * 데이터 엑셀 다운로드
     */
    excelDownload: function() {
        if(storeSlotGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "점포슬롯관리_" + $("#schStoreId").val() + "_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        storeSlotGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
};

//조회/검색/저장 검증
var validCheck = {
    search: function () {
        if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        }
        var schStoreId = $("#schStoreId").val();
        if ($.jUtil.isEmpty(schStoreId)) {
            alert("점포를 선택해 주세요.");
            return false;
        }
        return true;
    },
    save: function (checkRowIds, useYn) {
        if (checkRowIds.length === 0) {
            alert("선택된 행이 없습니다.");
            return false;
        }

        if($.jUtil.isEmpty(useYn)) {
            alert("사용여부를 선택해주세요.");
            return false;
        }

        return true;
    }
}

/** Grid Script */
var storeSlotGrid = {
    gridView: new RealGridJS.GridView("storeSlotGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        storeSlotGrid.initGrid();
        storeSlotGrid.initDataProvider();
    },
    initGrid: function () {
        storeSlotGrid.gridView.setDataSource(storeSlotGrid.dataProvider);
        storeSlotGrid.gridView.setStyles(storeSlotGridBaseInfo.realgrid.styles);
        storeSlotGrid.gridView.setDisplayOptions(storeSlotGridBaseInfo.realgrid.displayOptions);
        storeSlotGrid.gridView.setColumns(storeSlotGridBaseInfo.realgrid.columns);
        storeSlotGrid.gridView.setOptions(storeSlotGridBaseInfo.realgrid.options);

        setColumnLookupOption(storeSlotGrid.gridView, "useYn", $("#schUseYn"));
        setColumnLookupOptionForJson(storeSlotGrid.gridView, "shipWeekday", weekdayJson);
    },
    initDataProvider: function () {
        storeSlotGrid.dataProvider.setFields(storeSlotGridBaseInfo.dataProvider.fields);
        storeSlotGrid.dataProvider.setOptions(storeSlotGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        storeSlotGrid.dataProvider.clearRows();
        storeSlotGrid.dataProvider.setRows(dataList);
    }
};