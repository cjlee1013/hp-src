/** Main Script */
var shipMonitoringPickMain = {
    global : {
        tabId : ""
    },

    /**
     * init 이벤트
     */
    init: function() {
        shipMonitoringPickMain.setStoreOffice();
        shipMonitoringPickMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
        shippingUtil.setChangeStoreType2();
        shipMonitoringPickMain.global.tabId = parent.Tabbar.getActiveTab();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(shipMonitoringPickMain.openStorePopup);
        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { shippingUtil.setChangeStoreType2() });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipMonitoringPickMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipMonitoringPickMain.reset, "shipMonitoringPickSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(shipMonitoringPickMain.excelDownload);

        // 달력 - [어제] 버튼
        // $("#setTodayBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-1d", "-1d")});
        // 달력 - [1주일전] 버튼
        $("#setOneWeekBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-7d", "-1d")});
        // 달력 - [1개월] 버튼
        //$("#setOneMonthBtn").on("click", () => {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
        }
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#shipMonitoringPickSearchForm").serialize();

        CommonAjax.basic({
            url         : '/ship/shipMonitoring/getShipMonitoringPickList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipMonitoringPickGrid.setData(res);
                $('#shipMonitoringPickSearchCnt').html($.jUtil.comma(shipMonitoringPickGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipMonitoringPickGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "회수지연조회_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipMonitoringPickGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.setChangeStoreType2();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "-1d");
        shipMonitoringPickMain.setStoreOffice();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        //  검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'claimNo'
                || schKeywordTypeVal == 'claimBundleNo'
                || schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);
        var todayFormat = moment(shippingUtil.getToday(), 'YYYY-MM-DD', true).add(-1,"day");

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            return false;
        }

        if(todayFormat.diff(schEndDt, "day", true) < 0) {
            alert("전일자까지만 조회 가능합니다.");
            return false;
        }

        if (schEndDt.diff(schStartDt, "day", true) > 6) {
            alert("조회기간은 7일까지 설정 가능합니다.");
            return false;
        }


        var $schPickDelayDay = $('#schPickDelayDay');
        if (!/^$|^[0-9]$|^[1-9][0-9]*$/.test($schPickDelayDay.val()) || $schPickDelayDay.val() == '0') {
            alert('0보다 큰 숫자만 입력 가능합니다');
            $schPickDelayDay.focus();
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    }
}

/** Grid Script */
var shipMonitoringPickGrid = {
    gridView: new RealGridJS.GridView("shipMonitoringPickGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipMonitoringPickGrid.initGrid();
        shipMonitoringPickGrid.initDataProvider();
        shipMonitoringPickGrid.event();
    },
    initGrid: function () {
        shipMonitoringPickGrid.gridView.setDataSource(shipMonitoringPickGrid.dataProvider);
        shipMonitoringPickGrid.gridView.setStyles(shipMonitoringPickGridBaseInfo.realgrid.styles);
        shipMonitoringPickGrid.gridView.setDisplayOptions(shipMonitoringPickGridBaseInfo.realgrid.displayOptions);
        shipMonitoringPickGrid.gridView.setColumns(shipMonitoringPickGridBaseInfo.realgrid.columns);
        shipMonitoringPickGrid.gridView.setOptions(shipMonitoringPickGridBaseInfo.realgrid.options);

        // 그리드 링크 렌더링
        shipMonitoringPickGrid.gridView.setColumnProperty("pickInvoiceNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "pickInvoiceNo", showUrl: false});
        shipMonitoringPickGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});
        shipMonitoringPickGrid.gridView.setColumnProperty("userNoMask", "renderer", {type: "link", url: "unnecessary", requiredFields: "userNoMask", showUrl: false});
    },
    initDataProvider: function () {
        shipMonitoringPickGrid.dataProvider.setFields(shipMonitoringPickGridBaseInfo.dataProvider.fields);
        shipMonitoringPickGrid.dataProvider.setOptions(shipMonitoringPickGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipMonitoringPickGrid.gridView.onDataCellDblClicked = function (grid, index) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var claimBundleNo = grid.getValue(index.itemIndex, "claimBundleNo");
            var claimNo = grid.getValue(index.itemIndex, "claimNo");
            var claimType = grid.getValue(index.itemIndex, "claimType");

            const claimTabParam = {
                'purchaseOrderNo' : purchaseOrderNo,
                'claimNo' : claimNo,
                'claimBundleNo' : claimBundleNo,
                'claimType' : claimType
            }
            orderUtil.setLinkTab("claimDetailTab", "클레임정보상세","/claim/info/getClaimDetailInfoTab?" + jQuery.param(claimTabParam) );
        };

        shipMonitoringPickGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var invoiceNo = grid.getValue(index.itemIndex, "pickInvoiceNo");
            var dlvCd = grid.getValue(index.itemIndex, "pickDlvCd");
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
            var userNo = grid.getValue(index.itemIndex, "userNo");

            if (index.fieldName == "pickInvoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo, "shipHistoryPop", 649, 744, 0,0);
            } else if (index.fieldName == "purchaseOrderNo") {
                orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
            } else if (index.fieldName == "userNoMask") {
                UIUtil.addTabWindowBesideParent(
                    "/user/userInfo/detail?userNo=" + userNo, "userDetailTab",
                    "회원정보 상세", 120, shipMonitoringPickMain.global.tabId);
            }
        };
    },
    setData: function (dataList) {
        shipMonitoringPickGrid.dataProvider.clearRows();
        shipMonitoringPickGrid.dataProvider.setRows(dataList);
    }
};