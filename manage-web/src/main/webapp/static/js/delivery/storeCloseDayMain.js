/** Main Script */
var storeCloseDayMain = {
    /**
     * init 이벤트
     */
    init: function() {
        storeCloseDayMain.bindingEvent();
        deliveryCore.initSearchDate("0", "+7d");
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(storeCloseDayMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(storeCloseDayMain.reset);
        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", () => {deliveryCore.changeSearchDate("0", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", () => {deliveryCore.changeSearchDate("0", "+1w")});
        // 달력 - [1개월] 버튼
        $("#setOneMonthBtn").on("click", () => {deliveryCore.changeSearchDate("0", "+1m")});
        // [이미지변경] 버튼
        $("#chgImageBtn").bindClick(storeCloseDayMain.reset);
    },

    /**
     * 휴일 이미지 변경 팝업창 open
     */
    openChgImagePopup: function() {
        deliveryCore_popup.openPopup("/escrow/popup/closeDayImagePop", "closeDayImagePop", 1084, 650);
    },
    /**
     * 데이터 검색
     */
    search: function() {
        // Form 데이터 유효성 체크
        if (!storeCloseDayMain.valid()) {
            return;
        }

        var data = $("#storeCloseDaySearchForm").serialize();
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getStoreCloseDayList.json?' + data,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                storeCloseDayGrid.setData(res);
                $('#storeCloseDaySearchCnt').html($.jUtil.comma(storeCloseDayGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 데이터 검색 전 Form 데이터 유효성 체크
     */
    valid: function() {
        // date picker validation
        if (!deliveryCore.dateValid($("#schApplyStartDt"), $("#schApplyEndDt"), ["isValid"])) {
            return false;
        }
        return true;
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function() {
        $("#storeCloseDaySearchForm").resetForm();
        deliveryCore.initSearchDate("0", "+7d");
        $("#applyStoreCloseDayForm").attr("style", "display: none");
    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = storeCloseDayGrid.dataProvider.getJsonRow(selectRowId);
        $("#applyTitleNm").text(rowDataJson.titleNm);
        $("#applyStoreNm").text(rowDataJson.applyStoreNm);
        $("#applyStoreCloseDayForm").removeAttr("style", "display: none");
    }
};

/** Grid Script */
var storeCloseDayGrid = {
    gridView: new RealGridJS.GridView("storeCloseDayGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        storeCloseDayGrid.initGrid();
        storeCloseDayGrid.initDataProvider();
        storeCloseDayGrid.event();
    },
    initGrid: function () {
        storeCloseDayGrid.gridView.setDataSource(storeCloseDayGrid.dataProvider);
        storeCloseDayGrid.gridView.setStyles(storeCloseDayGridBaseInfo.realgrid.styles);
        storeCloseDayGrid.gridView.setDisplayOptions(storeCloseDayGridBaseInfo.realgrid.displayOptions);
        storeCloseDayGrid.gridView.setColumns(storeCloseDayGridBaseInfo.realgrid.columns);
        storeCloseDayGrid.gridView.setOptions(storeCloseDayGridBaseInfo.realgrid.options);

        setColumnLookupOption(storeCloseDayGrid.gridView, "closeDayType", $("#schCloseDayType"));
        setColumnLookupOption(storeCloseDayGrid.gridView, "useYn", $("#schUseYn"));
    },
    initDataProvider: function () {
        storeCloseDayGrid.dataProvider.setFields(storeCloseDayGridBaseInfo.dataProvider.fields);
        storeCloseDayGrid.dataProvider.setOptions(storeCloseDayGridBaseInfo.dataProvider.options);
    },
    event: function() {
        storeCloseDayGrid.gridView.onDataCellClicked = function(gridView, index) {
            storeCloseDayMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        storeCloseDayGrid.dataProvider.clearRows();
        storeCloseDayGrid.dataProvider.setRows(dataList);
    }
};