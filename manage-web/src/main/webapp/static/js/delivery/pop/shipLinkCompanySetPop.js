/** Main Script */
var shipLinkCompanySetPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipLinkCompanySetPop.bindingEvent();
        deliveryCore.initSearchDateCustom("stopStartDt", "stopEndDt", "0", "+1d");
        shipLinkCompanySetPop.checkStopDt();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [저장] 버튼
        $("#saveBtn").bindClick(shipLinkCompanySetPop.save);
        // [초기화] 버튼
        $("#resetBtn").bindClick(shipLinkCompanySetPop.reset);
        // 연동중지기간 설정 선택
        $('#chkStopDt').on('change', function() { shipLinkCompanySetPop.checkStopDt() });
        // 연동업체 셀렉트박스 선택
        $("#dlvLinkCompany").on('change', function() { shipLinkCompanySetPop.setDlvLinkCompanySelect($(this).val()) });
    },

    /**
     * 데이터 검색
     */
    search: function() {
        CommonAjax.basic({
            url         : '/ship/shipManage/getShipLinkCompanySetList.json?',
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipLinkCompanySetPopGrid.setData(res);
                $('#shipLinkCompanySetPopSearchCnt').html($.jUtil.comma(shipLinkCompanySetPopGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 저장
     */
    save: function() {
        if (!shipLinkCompanySetPop.saveValid()) {
            return;
        }

        if (!confirm("저장하시겠습니까?")) {
            return;
        }

        var data = new Object();

        data.dlvLinkCompany = $("#dlvLinkCompany").val();
        data.linkYn = $("input[name='linkYn']:checked").val();
        data.stopStartDt = $("#stopStartDt").val();
        data.stopEndDt = $("#stopEndDt").val();
        data.stopDtYn = $("#chkStopDt").is(":checked") ? "Y" : "N";

        CommonAjax.basic({
            url         : "/ship/shipManage/setShipLinkCompanySet.json",
            data        : JSON.stringify(data),
            method      : "POST",
            contentType : 'application/json',
            callbackFunc: function(res) {
                if (res.returnCode != "SUCCESS") {
                    alert(res.returnMessage);
                } else {
                    alert("저장되었습니다.");
                    shipLinkCompanySetPop.search();
                }
            }
        });
    },

    /**
     * 데이터 저장 전 유효성 체크
     */
    saveValid: function() {

        var dlvLinkCompanyVal = $("#dlvLinkCompany").val();

        if ($.jUtil.isEmpty(dlvLinkCompanyVal)) {
            alert("연동업체를 선택해 주세요.");
            return false;
        }

        var linkYnVal = $("input[name='linkYn']:checked").val();

        if ($.jUtil.isEmpty(linkYnVal)) {
            alert("연동여부를 선택해 주세요.");
            return false;
        }

        // 조회기간 (연동중지기간 설정시에만 체크)
        if($("#chkStopDt").is(":checked")) {
            var stopStartDt = moment($("#stopStartDt").val(), 'YYYY-MM-DD', true);
            var stopEndDt = moment($("#stopEndDt").val(), 'YYYY-MM-DD', true);

            if(!stopStartDt.isValid() || !stopEndDt.isValid()){
                alert("시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }

            if(stopEndDt.diff(stopStartDt, "day", true) < 0) {
                alert("시작일은 종료일보다 클 수 없습니다.");
                $("#stopStartDt").val(stopEndDt.format("YYYY-MM-DD"));
                return false;
            }
        }

        return true;
    },

    /**
     * 상세정보영역 폼 초기화
     */
    reset: function() {
        $("#dlvLinkCompany option:eq(0)").prop("selected", true);
        $('input:radio[name=linkYn]').prop("checked", false);
        $("#stopStartDt").val("");
        $("#stopEndDt").val("");
        $('#chkStopDt').prop('checked', false);
    },

    /**
     * 그리드 row 선택
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = shipLinkCompanySetPopGrid.dataProvider.getJsonRow(selectRowId);

        $("#dlvLinkCompany").val(rowDataJson.dlvLinkCompany);
        $('input:radio[name=linkYn]:input[value="' +rowDataJson.linkYn+ '"]').prop("checked", true);
        $("#stopStartDt").val(rowDataJson.stopStartDt);
        $("#stopEndDt").val(rowDataJson.stopEndDt);

        if(!$.jUtil.isEmpty(rowDataJson.stopStartDt) && !$.jUtil.isEmpty(rowDataJson.stopEndDt)) {
            $('#chkStopDt').prop('checked', true);
        } else {
            $('#chkStopDt').prop('checked', false);
        }

        shipLinkCompanySetPop.checkStopDt();
    },

    /**
     * 연동중지기간 설정
     */
    checkStopDt : function() {
        var chkVal = $("#chkStopDt").is(":checked");

        if (chkVal) {
            $("#stopStartDt").prop("disabled", false);
            $("#stopEndDt").prop("disabled", false);
        } else {
            $("#stopStartDt").val("");
            $("#stopEndDt").val("");
            $("#stopStartDt").prop("disabled", true);
            $("#stopEndDt").prop("disabled", true);
        }
    },

    /**
     * 연동업체 셀렉트 박스 선택
     */
    setDlvLinkCompanySelect : function(company) {
        var rows = shipLinkCompanySetPopGrid.dataProvider.getJsonRows();

        if ($.jUtil.isEmpty(company)) {
            shipLinkCompanySetPop.reset();
            return;
        }

        for (var idx in rows) {
            if (rows[idx].dlvLinkCompany == company) {
                shipLinkCompanySetPopGrid.gridView.setCurrent(idx);
                shipLinkCompanySetPop.gridRowSelect(idx);
                break;
            }
        }
    }

};

/** Grid Script */
var shipLinkCompanySetPopGrid = {
    gridView: new RealGridJS.GridView("shipLinkCompanySetPopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipLinkCompanySetPopGrid.initGrid();
        shipLinkCompanySetPopGrid.initDataProvider();
        shipLinkCompanySetPopGrid.event();
    },
    initGrid: function () {
        shipLinkCompanySetPopGrid.gridView.setDataSource(shipLinkCompanySetPopGrid.dataProvider);
        shipLinkCompanySetPopGrid.gridView.setStyles(shipLinkCompanySetPopGridBaseInfo.realgrid.styles);
        shipLinkCompanySetPopGrid.gridView.setDisplayOptions(shipLinkCompanySetPopGridBaseInfo.realgrid.displayOptions);
        shipLinkCompanySetPopGrid.gridView.setColumns(shipLinkCompanySetPopGridBaseInfo.realgrid.columns);
        shipLinkCompanySetPopGrid.gridView.setOptions(shipLinkCompanySetPopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipLinkCompanySetPopGrid.dataProvider.setFields(shipLinkCompanySetPopGridBaseInfo.dataProvider.fields);
        shipLinkCompanySetPopGrid.dataProvider.setOptions(shipLinkCompanySetPopGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipLinkCompanySetPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            shipLinkCompanySetPop.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        shipLinkCompanySetPopGrid.dataProvider.clearRows();
        shipLinkCompanySetPopGrid.dataProvider.setRows(dataList);
    }
};