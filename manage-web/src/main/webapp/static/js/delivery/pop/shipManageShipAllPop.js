/** Main Script */
var shipManageShipAllPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageShipAllPop.bindingEvent();
        Calendar.datePicker("scheduleShipDt");
        $('#scheduleShipDt').next().hide();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 배송방법 선택 시 필수값 입력 노출
        $("#shipMethod").bindChange(shipManageShipAllPop.changeInputForm);
        // [등록] 버튼
        $("#saveBtn").bindClick(shipManageShipAllPop.save);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var reqData = "bundleNo="+bundleNo;
        CommonAjax.basic({
            url         : '/ship/shipManage/getShipAllPopList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipManageShipAllPopGrid.setData(res);
                $('#shipManageSearchCnt').html($.jUtil.comma(shipManageShipAllPopGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 배송방법 선택 시 입력 FORM 노출
     */
    changeInputForm: function() {
        var selectVal = $('#shipMethod').val().split("_")[1];

        switch (selectVal) {
            case "DLV" :
                $('#dlvCd').show();
                $('#invoiceNo').show();
                $('#scheduleShipDt').hide().val("");
                $('#scheduleShipDt').next().hide();
                break;
            case "DRCT" :
                $('#dlvCd').hide().val("");
                $('#invoiceNo').hide().val("");
                $('#scheduleShipDt').show();
                $('#scheduleShipDt').next().show();
                break;
            default :
                $('#dlvCd').hide().val("");
                $('#invoiceNo').hide().val("");
                $('#scheduleShipDt').hide().val("");
                $('#scheduleShipDt').next().hide();
                break;
        }
    },

    /**
     * 저장
     */
    save: function() {
        var reqData = new Object();
        var shipMethod = $('#shipMethod').val();
        var shipMethodCode = $('#shipMethod').val().split("_")[1];
        var dlvCd = $('#dlvCd').val();
        var invoiceNo = $('#invoiceNo').val();
        var scheduleShipDt = $('#scheduleShipDt').val();

        if ($.jUtil.isEmpty(shipMethod)) {
            alert("배송방법을 선택해 주세요");
            return;
        }

        if (shipMethodCode == "DLV") {
            if ($.jUtil.isEmpty(dlvCd)) {
                alert("택배사를 선택해 주세요");
                return;
            }

            if ($.jUtil.isEmpty(invoiceNo)) {
                alert("송장번호를 입력해 주세요");
                return;
            }

            if (!/^[a-zA-Z0-9]*$/.test(invoiceNo)) {
                alert("송장번호 형식이 올바르지 않습니다");
                return;
            }
        }

        if (shipMethodCode == "DRCT") {
            if ($.jUtil.isEmpty(scheduleShipDt)) {
                alert("배송예정일을 입력해 주세요");
                return;
            }
        }

        reqData.shipMethod = shipMethod;
        reqData.dlvCd = dlvCd;
        reqData.invoiceNo = invoiceNo;
        reqData.scheduleShipDt = scheduleShipDt;
        reqData.bundleNo = bundleNo;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/ship/shipManage/setShipAll.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                        self.close();
                    } else {
                        alert("등록 완료되었습니다.");
                        eval("opener." + callBackScript + "(res);");
                        self.close();
                    }
                }
            });
        }
    }

};

/** Grid Script */
var shipManageShipAllPopGrid = {
    gridView: new RealGridJS.GridView("shipManageShipAllPopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipManageShipAllPopGrid.initGrid();
        shipManageShipAllPopGrid.initDataProvider();
        shipManageShipAllPopGrid.event();
    },
    initGrid: function () {
        shipManageShipAllPopGrid.gridView.setDataSource(shipManageShipAllPopGrid.dataProvider);
        shipManageShipAllPopGrid.gridView.setStyles(shipManageShipAllPopGridBaseInfo.realgrid.styles);
        shipManageShipAllPopGrid.gridView.setDisplayOptions(shipManageShipAllPopGridBaseInfo.realgrid.displayOptions);
        shipManageShipAllPopGrid.gridView.setColumns(shipManageShipAllPopGridBaseInfo.realgrid.columns);
        shipManageShipAllPopGrid.gridView.setOptions(shipManageShipAllPopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipManageShipAllPopGrid.dataProvider.setFields(shipManageShipAllPopGridBaseInfo.dataProvider.fields);
        shipManageShipAllPopGrid.dataProvider.setOptions(shipManageShipAllPopGridBaseInfo.dataProvider.options);
    },
    event: function() {

    },
    setData: function (dataList) {
        shipManageShipAllPopGrid.dataProvider.clearRows();
        shipManageShipAllPopGrid.dataProvider.setRows(dataList);
    }
};