/** Main Script */
var shipManageShipCompletePop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageShipCompletePop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [저장] 버튼
        $("#saveBtn").bindClick(shipManageShipCompletePop.save);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var parentGrid = window.opener.shipManageGrid;
        var checkRowIds = parentGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var bundleNoList = new Array();

        for (var idx in checkRowIds) {
            var checkedRows = parentGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            bundleNoList.push(checkedRows.bundleNo);
        }

        reqData.bundleNoList = $.unique(bundleNoList);

        CommonAjax.basic({
            url:'/ship/shipManage/getShipCompletePopList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                shipManageShipCompletePopGrid.setData(res);
            }
        });
    },

    /**
     * 저장
     */
    save: function() {
        var rows = shipManageShipCompletePopGrid.dataProvider.getJsonRows();
        var reqData = new Object();
        var bundleNoList = new Array();
        var reqCount = 0;

        for (var idx in rows) {
            bundleNoList.push(rows[idx].bundleNo);
        }

        reqData.bundleNoList = $.unique(bundleNoList);

        reqCount = bundleNoList.length;
        if (confirm('배송완료를 하시겠습니까?')) {
            CommonAjax.basic({
                url         : '/ship/shipManage/setShipComplete.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                        self.close();
                    } else {
                        shipManageShipCompletePop.resultTemplate(reqCount, res.data);
                        eval("opener." + callBackScript + "(res);");
                    }
                }
            });
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var innerHtml = [];
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        innerHtml.push('<div class="com wrap-title has-border">');
        innerHtml.push('    <h2 class="title font-malgun"></h2>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-15">');
        innerHtml.push('    <h3 style="font-size: 30px; text-align: center;">등록 '+ totalCnt +' 성공 '+ successCnt +' 실패 '+ failCnt +'</h3>');
        innerHtml.push('</div>');
        innerHtml.push('<div>');
        innerHtml.push('    <ul class="mg-t-15">');
        innerHtml.push('        <li>- 실패한 건은 배송중인 건이 아니거나 배송완료 가능한 상태가 아닌 주문입니다.</li>');
        innerHtml.push('    </ul>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="ui center-button-group">');
        innerHtml.push('    <span class="inner">');
        innerHtml.push('        <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>');
        innerHtml.push('    </span>');
        innerHtml.push('</div>');

        $('#divPop').html(innerHtml.join(''));
    }

};

/** Grid Script */
var shipManageShipCompletePopGrid = {
    gridView: new RealGridJS.GridView("shipManageShipCompletePopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipManageShipCompletePopGrid.initGrid();
        shipManageShipCompletePopGrid.initDataProvider();
        shipManageShipCompletePopGrid.customGrid();
        shipManageShipCompletePopGrid.event();
    },
    initGrid: function () {
        shipManageShipCompletePopGrid.gridView.setDataSource(shipManageShipCompletePopGrid.dataProvider);
        shipManageShipCompletePopGrid.gridView.setStyles(shipManageShipCompletePopGridBaseInfo.realgrid.styles);
        shipManageShipCompletePopGrid.gridView.setDisplayOptions(shipManageShipCompletePopGridBaseInfo.realgrid.displayOptions);
        shipManageShipCompletePopGrid.gridView.setColumns(shipManageShipCompletePopGridBaseInfo.realgrid.columns);
        shipManageShipCompletePopGrid.gridView.setOptions(shipManageShipCompletePopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipManageShipCompletePopGrid.dataProvider.setFields(shipManageShipCompletePopGridBaseInfo.dataProvider.fields);
        shipManageShipCompletePopGrid.dataProvider.setOptions(shipManageShipCompletePopGridBaseInfo.dataProvider.options);
    },
    customGrid: function () {
        shipManageShipCompletePopGrid.gridView.setColumnProperty("bundleNo", "mergeRule", {criteria:"value"});
    },
    event: function() {
    },
    setData: function (dataList) {
        shipManageShipCompletePopGrid.dataProvider.clearRows();
        shipManageShipCompletePopGrid.dataProvider.setRows(dataList);
    }
};