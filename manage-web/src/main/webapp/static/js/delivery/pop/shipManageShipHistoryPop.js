/** Main Script */
var shipManageShipHistoryPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageShipHistoryPop.search();
        shipManageShipHistoryPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

    },

    /**
     * 데이터 검색
     */
    search: function() {
        var dlvCd = global.dlvCd;
        var dlvNm = global.dlvNm;
        var invoiceNo = global.invoiceNo;

        var params = new Object();

        params.dlvCd = dlvCd;
        params.invoiceNo = invoiceNo;

        CommonAjax.basic({
            url:'/ship/shipManage/getShipHistory.json?' + jQuery.param(params),
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                var shipHistoryList = res;

                if (shipHistoryList.length > 0) {
                    $('#invoiceInfo').text('송장번호 : ' + shipHistoryList[0].dlvNm + ' ' + shipHistoryList[0].invoiceNo);

                    for (var idx in shipHistoryList) {
                        shipManageShipHistoryPop.historyTemplate(shipHistoryList[idx]);
                    }
                } else {
                    $('#invoiceInfo').text('송장번호 : ' + dlvNm+ ' ' + invoiceNo);
                    $('#ulList').html('<span">조회할 수 없습니다.</span>');
                    $('#ulList').css('text-align','center');
                }

                orderUtil.setWindowAutoResize();
            }
        });
    },

    /**
     * 배송 히스토리 템플릿
     */
    historyTemplate: function(data) {
        var innerHtml = [];

        // 택배위치
        if (!$.jUtil.isEmpty(data.shipWhere)) {
            data.shipWhere = '<span>(' +data.shipWhere+ ')</span>';
        }

        // 배송예정시간
        if (!$.jUtil.isEmpty(data.shipEstmateDt)) {
            data.shipEstmateDt = '<div class="info">' +
                '<p>배달예정시간 : ' + data.shipEstmateDt + '</p>' +
                '</div>';
        }

        // 배송기사
        if (!$.jUtil.isEmpty(data.shipMan)) {
            data.shipMan = '<div class="align-r">' +
                '<p>' + data.shipMan + '</p>' +
                '<p>' + $.jUtil.phoneFormatter(data.shipManPhone) + '</p>' +
                '</div>';
        }

        innerHtml.push('<li>');
        innerHtml.push('    <div>');
        innerHtml.push('        <p class="date">' + data.shipTranDt + '</p>');
        innerHtml.push('        <p class="state">' + data.shipDetail + data.shipWhere + '</p>');
        innerHtml.push(         data.shipEstmateDt);
        innerHtml.push('    </div>');
        innerHtml.push(     data.shipMan);
        innerHtml.push('</li>');

        $('#ulList').append(innerHtml.join(''));
    }
};