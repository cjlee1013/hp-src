/** Main Script */
var shipManageShipAllExcelPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageShipAllExcelPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [파일찾기] 버튼
        $("#searchUploadFile").bindClick(shipManageShipAllExcelPop.searchFile);
        // [저장] 버튼
        $("#saveBtn").bindClick(shipManageShipAllExcelPop.save);
        // [배송방법] 버튼
        $("input[name='shipMethod']").on("click", function() { shipManageShipAllExcelPop.setShipMethod($(this)) });
    },

    /**
     * [파일찾기] 버튼 클릭 (윈도우 파일 찾기 open)
     */
    searchFile: function() {
        $("#uploadFile").click();
    },

    /**
     * 선택한 파일 정보 불러오기
     */
    getFile: function(_obj) {
        var fileInfo = _obj[0].files[0];
        var fileName = fileInfo.name;
        $("#uploadFileNm").val(fileName);
    },

    /**
     * 배송방법 클릭
     */
    setShipMethod: function($this) {
        var method = $this.val();

        if (method == "DLV") {
            $("#dlvCd").prop('disabled',false);
        } else {
            $("#dlvCd option:eq(0)").prop("selected", true);
            $("#dlvCd").prop('disabled',true);
        }
    },

    /**
     * 저장
     */
    save: function() {
        var uploadFileVal = $("#uploadFile").val();
        var uploadFileNmVal = $("#uploadFileNm").val();
        var fileExt = uploadFileNmVal.split(".")[1];
        var dlvCd = $("#dlvCd").val();
        var shipMethod = $("input[name='shipMethod']:checked").val();

        if ($.jUtil.isEmpty(shipMethod)) {
            alert("배송방법을 입력해주세요.");
            return;
        }

        if ($.jUtil.isEmpty(dlvCd) && shipMethod == "DLV") {
            alert("택배사를 선택해주세요.");
            return;
        }

        if ($.jUtil.isEmpty(uploadFileVal) || $.jUtil.isEmpty(uploadFileNmVal)) {
            alert("파일을 등록해주세요.");
            return;
        }

        if (fileExt.indexOf("xlsx") < 0 && fileExt.indexOf("xls") < 0) {
            alert("지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            return false;
        }

        if (confirm("저장하시겠습니까?")) {
            $("#fileUploadForm").ajaxForm({
                url     : "/ship/shipManage/setShipAllExcel.json",
                method  : "POST",
                enctype : "multipart/form-data",
                success : function(res) {
                    var resData = res.data;

                    if (res.returnCode != "SUCCESS") {
                        alert(res.returnMessage);
                        self.close();
                    } else {
                        shipManageShipAllExcelPop.resultTemplate(resData.excelTotalCnt, resData.successCnt);
                        shipManageShipAllExcelPopGrid.init(shipMethod);
                        shipManageShipAllExcelPopGrid.setData(resData.failList);
                        eval("opener." + callBackScript + "(res);");
                    }
                },
                error : function(resError) {
                    alert('오류가 발생하였습니다.');
                    self.close();
                }
            }).submit();
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var innerHtml = [];
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        innerHtml.push('<div class="com wrap-title has-border">');
        innerHtml.push('    <h2 class="title font-malgun">일괄발송처리/수정</h2>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-15">');
        innerHtml.push('    <h3 style="font-size: 30px; text-align: center;">등록 '+ totalCnt +' 성공 '+ successCnt +' 실패 '+ failCnt +'</h3>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-10">');
        innerHtml.push('    <ul>');
        innerHtml.push('        <li>- 엑셀을 다운로드하여 실패 사유를 확인해주세요</li>');
        innerHtml.push('    </ul>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="ui center-button-group">');
        innerHtml.push('    <span class="inner">');
        innerHtml.push('        <button id="closeBtn" class="ui button xlarge font-malgun" onclick="shipManageShipAllExcelPop.excelDownload()">엑셀 다운</button>');
        innerHtml.push('    </span>');
        innerHtml.push('</div>');

        $('#divPop').html(innerHtml.join(''));
    }

    /**
     * 엑셀 다운로드
     */
    , excelDownload: function() {
        if (shipManageShipAllExcelPopGrid.gridView.getItemCount() == 0) {
            alert("실패건이 없습니다.");
            return;
        }

        var _date = new Date();
        var fileName =  "일괄발송처리실패목록_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipManageShipAllExcelPopGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    }

}

/** Grid Script */
var shipManageShipAllExcelPopGrid = {
    gridView: new RealGridJS.GridView("shipManageShipAllExcelPopGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function (shipMethod) {
        shipManageShipAllExcelPopGrid.initGrid();
        shipManageShipAllExcelPopGrid.initDataProvider();
        shipManageShipAllExcelPopGrid.event();

        // 택배배송, 직접배송 필요한 컬럼만 남긴다
        if (shipMethod == "DLV") {
            shipManageShipAllExcelPopGrid.gridView.removeColumn("scheduleShipDt");
        } else {
            shipManageShipAllExcelPopGrid.gridView.removeColumn("invoiceNo");
        }
    },
    initGrid: function () {
        shipManageShipAllExcelPopGrid.gridView.setDataSource(shipManageShipAllExcelPopGrid.dataProvider);
        shipManageShipAllExcelPopGrid.gridView.setStyles(shipManageShipAllExcelPopGridBaseInfo.realgrid.styles);
        shipManageShipAllExcelPopGrid.gridView.setDisplayOptions(shipManageShipAllExcelPopGridBaseInfo.realgrid.displayOptions);
        shipManageShipAllExcelPopGrid.gridView.setColumns(shipManageShipAllExcelPopGridBaseInfo.realgrid.columns);
        shipManageShipAllExcelPopGrid.gridView.setOptions(shipManageShipAllExcelPopGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        shipManageShipAllExcelPopGrid.dataProvider.setFields(shipManageShipAllExcelPopGridBaseInfo.dataProvider.fields);
        shipManageShipAllExcelPopGrid.dataProvider.setOptions(shipManageShipAllExcelPopGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipManageShipAllExcelPopGrid.gridView.onDataCellClicked = function(gridView, index) {
            //shipManageMain.gridRowSelect(index.dataRow);
        };
    },
    setData: function (dataList) {
        shipManageShipAllExcelPopGrid.dataProvider.clearRows();
        shipManageShipAllExcelPopGrid.dataProvider.setRows(dataList);
    }
}