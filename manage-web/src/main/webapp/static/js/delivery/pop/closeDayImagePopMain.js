/** Main Script */
var closeDayImagePopMain = {
    processKey: "SlotImage",
    imagePrefix: null,
    imageSuffix: null,
    /**
     * init 이벤트
     */
    init: function() {
        closeDayImagePopMain.imagePrefix = "";
        closeDayImagePopMain.imageSuffix = "";
        closeDayImagePopMain.drawForm("HYPER");
        closeDayImagePopMain.bindingEvent();
    },
    /**
     * JSON 으로 되어있는 값 알맞게 그려주기
     */
    drawForm: function(storeType) {
        closeDayImagePopMain.imagePrefix = storeType;
        $(".storeType").val(storeType);
        var siteImageList = storeType === "HYPER" ? hyperImageList : (storeType === "AURORA" ? auroraImageList : clubImageList);
        // reset
        $("td[data-tagType='dynamic_content']:not(.close_day_img) span").text("");
        $("td[data-tagType='dynamic_content'].close_day_img span[id*='_']").children().attr("src", "");
        $(".pcImgUrl").val("");
        $(".mobileImgUrl").val("");

        // draw
        for (var siteImage of siteImageList) {
            $("#closeDayType"+siteImage.closeDayType+" input.pcImgUrl").val(siteImage.pcImgUrl);
            $("#closeDayType"+siteImage.closeDayType+" input.mobileImgUrl").val(siteImage.mobileImgUrl);
            var closeDayTypeTagsId = siteImage.closeDayType + "_";
            $("td[data-tagType='dynamic_content'] span[id^='"+closeDayTypeTagsId+"']").each(function() {
                var tagId = $(this).attr("id");       //'F_pcImgUrl'
                var tagPropNm = tagId.split("_")[1];  //'pcImgUrl'
                var tagProp = siteImage[tagPropNm];   //'/ho/HYPER_F_PC'
                if ($(this).children().prop("tagName") === "IMG") {
                    if (!$.jUtil.isEmpty(tagProp)) {
                        $(this).children().attr("src", apiImgUrl+"/provide/view?processKey="+closeDayImagePopMain.processKey+"\&fileId="+tagProp);
                    }
                } else {
                    $(this).text(tagProp);
                }
            });
        }
    },
    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // 홈플러스(일반) 탭
        $("#storeType #HYPER").bindClick(closeDayImagePopMain.goTab, "HYPER");
        // 홈플러스(새벽) 탭
        $("#storeType #AURORA").bindClick(closeDayImagePopMain.goTab, "AURORA");
        // 더클럽 탭
        $("#storeType #CLUB").bindClick(closeDayImagePopMain.goTab, "CLUB");
        // 이미지 [등록] 버튼
        $("td[data-tagType='dynamic_content'] button").on("click", function () {
            closeDayImagePopMain.imageSuffix = $(this).closest("td").attr("id");
            closeDayImagePopMain.callImageFile();
        });
        // [저장] 버튼
        $("#saveBtn").bindClick(closeDayImagePopMain.saveAll);
        // 이미지 파일선택 및 업로드
        $('input[name="fileArr"]').change(function() {
            closeDayImagePopMain.uploadImage();
        });
    },

    /**
     * 탭으로 이동하는 것 처럼 보이게 style change & Form draw
     * @param tabId
     */
    goTab: function(tabId) {
        // 선택된 탭이 홈플러스(일반) 인 경우
        if (tabId === "HYPER") {
            $("#storeType #HYPER").addClass("tab-on");
            $("#storeType #AURORA").removeClass("tab-on");
            $("#storeType #CLUB").removeClass("tab-on");
            closeDayImagePopMain.drawForm("HYPER");
        // 선택된 탭이 홈플러스(새벽) 인 경우
        } else if (tabId === "AURORA") {
            $("#storeType #HYPER").removeClass("tab-on");
            $("#storeType #AURORA").addClass("tab-on");
            $("#storeType #CLUB").removeClass("tab-on");
            closeDayImagePopMain.drawForm("AURORA");
        // 선택된 탭이 더클럽 인 경우
        } else if (tabId === "CLUB") {
            $("#storeType #HYPER").removeClass("tab-on");
            $("#storeType #AURORA").removeClass("tab-on");
            $("#storeType #CLUB").addClass("tab-on");
            closeDayImagePopMain.drawForm("CLUB");
        }
    },

    /**
     * 등록할 이미지 파일 불러오기
     */
    callImageFile: function() {
        $('input[name=fileArr]').click();
    },
    /**
     * 이미지 업로드
     */
    uploadImage: function() {
        let file = $('#closeDayImgFile');
        // fileName 예시) "HYPER_C_PC", "CLUB_R_MOBILE"
        let fileName = closeDayImagePopMain.imagePrefix+"_"+closeDayImagePopMain.imageSuffix;
        let params = {
            processKey : closeDayImagePopMain.processKey,
            mode : "IMG",
            fileName : fileName
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];
                    if (f.hasOwnProperty("errors")) {
                        alert(f.error);
                    } else {
                        closeDayImagePopMain.setImage(closeDayImagePopMain.imageSuffix, {
                            'imgUrl'    : f.fileStoreInfo.fileId,
                            'src'       : apiImgUrl+"/provide/view?processKey="+closeDayImagePopMain.processKey+"\&fileId="+f.fileStoreInfo.fileId,
                        });
                        alert("이미지가 등록되었습니다.");
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 적용
     */
    setImage: function(imageType, params) {
        let closeDayType = imageType.split("_")[0]; // 휴일유형(F/R/S/C/E)
        let platformType = imageType.split("_")[1]; // platform(PC/MOBILE)

        if (platformType === "PC") {
            $("#closeDayType"+closeDayType+" input.pcImgUrl").val(params.imgUrl);
            $("#"+closeDayType+"_pcImgUrl").children().attr("src", params.src);
        } else {
            $("#closeDayType"+closeDayType+" input.mobileImgUrl").val(params.imgUrl);
            $("#"+closeDayType+"_mobileImgUrl").children().attr("src", params.src);
        }
    },
    /**
     * 휴일이미지 저장
     */
    saveAll: function() {
        // 입력값 validation
        if (!closeDayImagePopMain.valid()) {
            return false;
        }
        if (!confirm("저장 하시겠습니까?")) {
            return false;
        }
        var formData = $('#closeDayImagePopForm').serializeObject(true);
        // QAF2-1332, 임시휴무 유형에 기타휴무 이미지 추가
        var dataTypeT = new Object();
        dataTypeT.storeType = $('#closeDayTypeE input.storeType').val();
        dataTypeT.closeDayType = "T";
        dataTypeT.pcImgUrl = $('#closeDayTypeE input.pcImgUrl').val();
        dataTypeT.mobileImgUrl = $('#closeDayTypeE input.mobileImgUrl').val();
        formData.imageList.push(dataTypeT);

        var sendData = JSON.stringify(formData);
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/saveCloseDayImageList.json',
            data        : sendData,
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                console.log(res);
                alert("저장하였습니다.");
                // closeDayImagePopMain.drawForm(closeDayImagePopMain.imagePrefix);
                self.close();
            }
        });
    },
    /**
     * 데이터 저장 전 Form 데이터 유효성 체크
     */
    valid: function() {
        var validCnt = 0;
        $("[id^='closeDayType'] input[class$='pcImgUrl']").each(function() {
            if ($.jUtil.isEmpty($(this).val())) {
                validCnt += 1;
            }
        });
        // 등록되지 않은 PC 이미지가 있을 경우 저장 불가
        if (validCnt > 0) {
            alert("등록되지 않은 PC 이미지가 있습니다. 모든 PC 이미지가 등록되어야 저장 가능합니다.");
            return false;
        }
        return true;
    }
};