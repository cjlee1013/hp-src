/** Main Script */
var shipManageNotiDealyPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageNotiDealyPop.bindingEvent();
        Calendar.datePicker("delayShipDt");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        //기타 선택 시 textarea 노출
        $("#delayShipCd").bindChange(shipManageNotiDealyPop.selectDelayMsg);
        // [저장] 버튼
        $("#saveBtn").bindClick(shipManageNotiDealyPop.save);
    },

    /**
     * 셀렉트 박스 선택 이벤트
     * 기타 선택 시, 지연사유 입력 영역 SHOW
     */
    selectDelayMsg: function() {
        var selectVal = $('#delayShipCd').val();
        var $delayShipMsg = $("#delayShipMsg");

        if (selectVal == "ET") {
            $delayShipMsg.show();
        } else {
            $delayShipMsg.hide().val("");
        }
    },

    /**
     * 저장
     */
    save: function() {
        var reqData = new Object();
        var delayShipCd = $('#delayShipCd').val();
        var delayShipDt = $('#delayShipDt').val();
        var delayShipMsg = $('#delayShipMsg').val();

        if (delayShipDt == "") {
            alert("발송기한을 입력해주세요.");
            return;
        }

        if (delayShipCd == ""
                || (delayShipCd == "ET" && delayShipMsg == '')) {
            alert("사유를 입력해주세요.");
            return;
        }

        reqData.bundleNo = bundleNo;
        reqData.delayShipCd = delayShipCd;
        reqData.delayShipDt = delayShipDt;
        reqData.delayShipMsg = delayShipMsg;

        if (confirm("저장하시겠습니까?")) {
            CommonAjax.basic({
                url         : '/ship/shipManage/setNotiDelay.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "SUCCESS") {
                        alert("발송지연안내를 실패했습니다.");
                        self.close();
                    } else {
                        //shipManageNotiDealyPop.resultTemplate(reqCount, res.data);
                        eval("opener." + callBackScript + "(res);");
                        alert("발송지연안내를 처리했습니다.");
                        self.close();
                    }
                }
            });
        }
    },

    /**
     * 저장 후 팝업 템플릿
     */
    resultTemplate: function(totalCnt, successCnt) {
        var innerHtml = [];
        var failCnt = parseInt(totalCnt) - parseInt(successCnt);

        innerHtml.push('<div class="com wrap-title has-border">');
        innerHtml.push('    <h2 class="title font-malgun"></h2>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="mg-t-15">');
        innerHtml.push('    <h3 style="font-size: 30px; text-align: center;">등록 '+ totalCnt +' 성공 '+ successCnt +' 실패 '+ failCnt +'</h3>');
        innerHtml.push('</div>');
        innerHtml.push('<div>');
        innerHtml.push('    <ul class="mg-t-15">');
        innerHtml.push('        <li>- 실패한 건은 발송지연 안내를 한 경우, 발송기한 설정이 맞지 않는 경우 또는 배송중인 주문입니다.</li>');
        innerHtml.push('    </ul>');
        innerHtml.push('</div>');
        innerHtml.push('<div class="ui center-button-group">');
        innerHtml.push('    <span class="inner">');
        innerHtml.push('        <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>');
        innerHtml.push('    </span>');
        innerHtml.push('</div>');

        $('#divPop').html(innerHtml.join(''));
    }
};