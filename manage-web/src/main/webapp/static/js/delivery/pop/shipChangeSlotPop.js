const shipChangeSlotPop = {
    global : {
      checkData : null
      , dataProvider : null
    },

    init : function () {
        shipChangeSlotPop.global.checkData = opener.shipReserveManageGrid.gridView.getCheckedRows();
        shipChangeSlotPop.global.dataProvider = opener.shipReserveManageGrid.dataProvider;

        this.bindingEvent();
        this.pageInit();
    },

    pageInit : function () {
        shipChangeSlotPop.getChangeableInfo();
        $('#storeNm').text(shipChangeSlotPop.global.dataProvider.getJsonRow(shipChangeSlotPop.global.checkData[0]).storeNm);
    },

    bindingEvent : function () {
        $('#saveBtn').on('click', function () {
            const selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id');
            if($.jUtil.isEmpty(selectedInfo)) return ;
            shipChangeSlotPop.setShipChange(selectedInfo);
        });
    },

    getChangeableInfo : function () {
        const dataParam = {
            'storeId' : shipChangeSlotPop.global.dataProvider.getJsonRow(shipChangeSlotPop.global.checkData[0]).storeId
        };

        CommonAjax.basic(
            {
                url:'/ship/getShipSlotInfo.json?'.concat(jQuery.param(dataParam)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    $('#slotChangeArea').append(shipChangeSlotPop.writeToSlotTable(res));
                    shipChangeSlotPop.setRadioButton();
                    orderUtil.setWindowAutoResize();
                }
            });
    },

    setRadioButton : function (){
        const radioId = [
            ($.jUtil.isEmpty($('#placeNm').text()) ? 'dict' : 'pick'),
            $('#slotShipDt').text().split('-').join(''),
            $('#slotId').text().substr(0, 3),
            $('#slotId').text()
        ].join("_");
        $('#' + radioId).attr("checked", true);
    },

    setShipChange : function (_idInfo) {
      const idInfo = orderUtil.getSplitData(_idInfo);
      var dataParamList = new Array();

      for (var idx in shipChangeSlotPop.global.checkData) {
        var checkedRows = shipChangeSlotPop.global.dataProvider.getJsonRow(
            shipChangeSlotPop.global.checkData[idx]);

        const dataParam = {
          'bundleNo': checkedRows.bundleNo,
          'purchaseOrderNo': checkedRows.purchaseOrderNo,
          'userNo': checkedRows.userNo,
          'chgShipDt': orderUtil.toDateFormatting(idInfo[1]),
          'chgShiftId': idInfo[2],
          'chgSlotId': idInfo[3]
        }

        dataParamList.push(dataParam);
      }

      if (confirm("저장하시겠습니까?")) {
        CommonAjax.basic({
          url: '/ship/shipManage/setShipSlotInfo.json',
          data: JSON.stringify(dataParamList),
          method: "POST",
          contentType: "application/json; charset=utf-8",
          callbackFunc: function (res) {
            shipChangeSlotPop.resultTemplate(dataParamList.length,res);
          }
        });
      }
    },

    writeToSlotTable : function (_slotData) {
      var frontSlotHeader = shipChangeSlotPop.setDictionaryToArray(_slotData.frontSlotHeader);
      var frontSlotMap = shipChangeSlotPop.setDictionaryToArray(_slotData.frontSlotMap);
      var storePickupPlaceList = shipChangeSlotPop.setDictionaryToArray(_slotData.storePickupPlaceList);

      var table = $('<table class="ui table" style="width:1450px">');
      var tableColGroup = $('<colgroup>');
      var tbody = $('<tbody>');
      var tableRow = $('<tr>');

      if(frontSlotHeader.size <= 0){
        alert('배송지 SLOT 정보가 없습니다.');
        return false;
      }
      var dataChecker = 0;
      // table caption 추가
      table.append($('<caption>').html('배송SLOT 테이블'));
      // 배송시간 표시용 col을 추가한다.
      for(var i=0; i<16; i++ ){
          if(i==0) {
            tableColGroup.append($('<col>').attr('width', '100px'));
          } else {
            tableColGroup.append($('<col>').attr('width', '90px'));
          }
      }
      table.append(tableColGroup);

      // 헤더 설정
      $.each(frontSlotHeader, function (key, value) {
        if(dataChecker++ === 0) {
          tableRow.append($('<td>').html('배송시간대'));
        }
        tableRow.append($('<td>').html(new Date() === new Date(key) ? '오늘'.concat('(').concat(value).concat(')') : key.concat('(').concat(value).concat(')')));
      });

      tbody.append(tableRow);

      var index = 0;
      // 주문마감/가능 여부 표시
      $.each(frontSlotMap, function (key, value) {
        tableRow = $('<tr>');
        tableRow.append($('<th>').text(key));
        for(var j=0;j<value.length;j++){
          var valueText = [
            ($.jUtil.isEmpty(value[j].placeNo) ? '주문' : '픽업'),
            value[j].slotStatus === 'END' ? '마감' : value[j].slotStatus === 'CLOSE' ? '휴무' : value[j].slotStatus === 'READY' ? '준비중' : value[j].slotStatus === 'DAILY_END' ? '선택불가' : '가능'
          ];
          var td = $('<td>');
          var radioId = [
            ($.jUtil.isEmpty(value[j].placeNo) ? 'dict' : 'pick'),
            value[j].shipDt.split('-').join(''),
            value[j].shiftId,
            value[j].slotId
          ].join("_");
          if(valueText[1] === '마감' || valueText[1] === '준비중' || valueText[1] === '선택불가' ) {
            td.attr('class', 'finish').text(valueText[1] === '마감' ? valueText.join('') : valueText[1]);
            tableRow.append(td);
            continue;
          }
          if(valueText[1] === '휴무'){
            if(index === 0){
              td.attr('rowspan', Object.keys(frontSlotMap).length).attr('class', 'CLOSE').text(valueText[1]);
              tableRow.append(td);
            }
            continue;
          }
          td.append($('<input>').attr('type', 'radio').attr('name', 'changeSlotInfo').attr('id', radioId));
          td.append('&nbsp;');
          td.append($('<label>').attr('for', radioId).attr('class', 'lb-radio').text(valueText.join('')));
          tableRow.append(td);
        }
        tbody.append(tableRow);
        index++;
      });
      table.append(tbody);
      return table;
  },

  setDictionaryToArray : function (_dictionary) {
    if(!$.jUtil.isEmpty(_dictionary)){
      var returnMap = {};
      $.each(_dictionary, function (key, value) {
        returnMap[key] = value;
      });
      return returnMap;
    }
    return null;
  },

  /**
   * 저장 후 팝업 템플릿
   */
  resultTemplate: function(totalCnt, successCnt) {
    var innerHtml = [];
    var failCnt = parseInt(totalCnt) - parseInt(successCnt);

    innerHtml.push('<div class="com wrap-title has-border">');
    innerHtml.push('    <h2 class="title font-malgun"></h2>');
    innerHtml.push('</div>');
    innerHtml.push('<div class="mg-t-15">');
    innerHtml.push('    <h3 style="font-size: 30px; text-align: center;">등록 '+ totalCnt +' 성공 '+ successCnt +' 실패 '+ failCnt +'</h3>');
    innerHtml.push('</div>');
    innerHtml.push('<div class="ui center-button-group">');
    innerHtml.push('    <span class="inner">');
    innerHtml.push('        <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>');
    innerHtml.push('    </span>');
    innerHtml.push('</div>');

    $('#divPop').html(innerHtml.join(''));
  }

};