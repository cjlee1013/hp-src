/** Main Script */
var shipInfoPrintPop = {
    /**
     * init 이벤트
     */
    init: function() {
        shipInfoPrintPop.search();
        shipInfoPrintPop.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        $("#printBtn").bindClick(shipInfoPrintPop.printInfo);
    },

    /**
     * 출력
     */
    printInfo: function() {
        document.body.innerHTML = printArea.innerHTML;
        window.print();
        location.reload();
    },

    /**
     * 데이터 검색
     */
    search: function() {
        var params = new Object();

        params.bundleNo = bundleNo;

        CommonAjax.basic({
            url:'/ship/shipManage/getShipInfoPrintList.json?' + jQuery.param(params),
            data: null,
            contentType: 'application/json',
            method: "GET",
            callbackFunc: function(res) {
                var infoList = res;
                var htmlArr = new Array();

                for (var idx in infoList) {
                    if(idx == 0) {
                        htmlArr.push('<div class="com wrap-title"> ');
                    } else {
                        htmlArr.push('<div class="com wrap-title" style="page-break-before: always"> ');
                    }
                    htmlArr.push('  <h2 class="title font-malgun" style="float: none; text-align: center;">배달신청서</h2>');
                    htmlArr.push('</div>');
                    htmlArr.push('<div style="margin-bottom: 10px">');
                    htmlArr.push('    <table class="print tbl-data">');
                    htmlArr.push('    <colgroup>');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:10%">');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:12%">');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:10%">');
                    htmlArr.push('        <col style="width:9%">');
                    htmlArr.push('        <col style="width:12%">');
                    htmlArr.push('    </colgroup>');
                    htmlArr.push('    <tbody>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th >점포명</th>');
                    htmlArr.push('            <td id="storeNm" colspan="4">'+infoList[idx].storeNm+'</td>');
                    htmlArr.push('            <th>포스번호</th>');
                    htmlArr.push('            <td id="posNo">'+infoList[idx].posNo+'</td>');
                    htmlArr.push('            <th>영수증번호</th>');
                    htmlArr.push('            <td id="tradeNo" colspan="3">'+infoList[idx].tradeNo+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th>상품명</th>');
                    htmlArr.push('            <td id="itemNm1" colspan="4">'+infoList[idx].itemNm1+'</td>');
                    htmlArr.push('            <th>상품코드</th>');
                    htmlArr.push('            <td id="itemNo" colspan="4">'+infoList[idx].itemNo+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th>판매금액</th>');
                    htmlArr.push('            <td id="orderPrice" colspan="4">'+$.jUtil.comma(infoList[idx].orderPrice)+'</td>');
                    htmlArr.push('            <th>수량</th>');
                    htmlArr.push('            <td id="itemQty" colspan="4">'+infoList[idx].itemQty+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th>접수자</th>');
                    htmlArr.push('            <td colspan="4">이커머스</td>');
                    htmlArr.push('            <th>주문일시</th>');
                    htmlArr.push('            <td id="orderDt" colspan="4">'+infoList[idx].orderDt+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th rowspan="3">주문자</th>');
                    htmlArr.push('            <th>성명</th>');
                    htmlArr.push('            <td id="buyerNm" colspan="3">'+infoList[idx].buyerNm+'</td>');
                    htmlArr.push('            <th rowspan="3">수취인</th>');
                    htmlArr.push('            <th>성명</th>');
                    htmlArr.push('            <td id="receiverNm" colspan="3">'+infoList[idx].receiverNm+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th>전화번호</th>');
                    htmlArr.push('            <td></td>');
                    htmlArr.push('            <th>휴대전화</th>');
                    htmlArr.push('            <td id="buyerMobileNo">'+infoList[idx].buyerMobileNo+'</td>');
                    htmlArr.push('            <th>전화번호</th>');
                    htmlArr.push('            <td></td>');
                    htmlArr.push('            <th>휴대전화</th>');
                    htmlArr.push('            <td id="shipMobileNo">'+infoList[idx].shipMobileNo+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('        <tr>');
                    htmlArr.push('            <th>주소</th>');
                    htmlArr.push('            <td colspan="3"></td>');
                    htmlArr.push('            <th>주소</th>');
                    htmlArr.push('            <td id="addr" colspan="3">'+infoList[idx].addr+'</td>');
                    htmlArr.push('        </tr>');
                    htmlArr.push('    </tbody>');
                    htmlArr.push('    </table>');
                    htmlArr.push('</div>');
                }

                $('#printArea').append(htmlArr.join(''));
            }
        });
    }
};