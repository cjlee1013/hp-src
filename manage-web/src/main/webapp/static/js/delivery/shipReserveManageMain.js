/** Main Script */
var shipReserveManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        shipReserveManageMain.setStoreOffice();
        shipReserveManageMain.bindingEvent();
        shippingUtil.initSearchDate("schStartDt", "0");
        shippingUtil.setChangeStoreType();

    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(shipReserveManageMain.openStorePopup);
        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { shippingUtil.setChangeStoreType() });

        // 배송유형 선택
        $('input:checkbox[name=schShipType]').on('click', function() { shipReserveManageMain.setShipTypeSelect($(this)) });

        // 구매자 조회
        $("#schBuyer").on('click', function() { shipReserveManageMain.openMemberSearchPop() });
        // 구매자 비회원 선택
        $('input:radio[name=schNomemOrderYn]').on('change', function() { shipReserveManageMain.setBuyerSearch($(this)) });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipReserveManageMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipReserveManageMain.reset, "shipReserveManageSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(shipReserveManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "0")});
        // 달력 - [어제] 버튼
        $("#setOneWeekBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "-1d")});
        // 달력 - [1개월] 버튼
        //$("#setOneMonthBtn").on("click", function() {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 주문서출력
        $("#shipInfoPrint").bindClick(shipReserveManageMain.openShipInfoPrintPopup);
        // 배송시간 수정
        $("#shipChangeSlot").bindClick(shipReserveManageMain.openShipChangeSlotPopup);
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
            // $("#schShipDtType").val("reserveShipDt");
        }
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 회원 검색 팝업
     */
    openMemberSearchPop: function() {
        memberSearchPopup("callBack.searchMember");
    },

    /**
     * 구매자 회원/비회원 라디오 선택
     */
    setBuyerSearch: function($this) {
        var radioVal = $this.val();

        $('#schUserNo').val("");
        $('#schNomemOrderWord').val("");
        $("#schNomemOrderType option:eq(0)").prop("selected", true);

        if (radioVal == "Y") {
            $('#schNomemOrderWord').attr('readonly',false);
            $('#schNomemOrderType').show();
            $('#schBuyer').hide();
        } else {
            $('#schNomemOrderWord').attr('readonly',true);
            $('#schNomemOrderType').hide();
            $('#schBuyer').show();
        }
    },

    /**
     * 배송유형 설정
     */
    setShipTypeSelect: function($this) {
        $('input:checkbox[name=schShipType]').prop("checked", false);
        $this.prop("checked", true);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#shipReserveManageSearchForm").serialize();

        CommonAjax.basic({
            url         : '/ship/shipManage/getShipReserveManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipReserveManageGrid.setData(res);
                $('#shipReserveManageSearchCnt').html($.jUtil.comma(shipReserveManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 주문서출력 팝업창 open
     */
    openShipInfoPrintPopup: function() {
        var checkRowIds = shipReserveManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length > 1) {
            alert("1개의 배송건만 가능합니다.");
            return;
        }

        var checkedRow = shipReserveManageGrid.dataProvider.getJsonRow(checkRowIds[0]);
        var bundleNo = checkedRow.bundleNo;

        windowPopupOpen("/ship/popup/getShipInfoPrintPop?bundleNo="+bundleNo, "shipInfoPrintPop", 1060, 630, "yes", "yes");
    },

    /**
     * 배송시간 수정 팝업창 open
     */
    openShipChangeSlotPopup: function() {
        var checkRowIds = shipReserveManageGrid.gridView.getCheckedRows();
        var checkRowId0 = shipReserveManageGrid.dataProvider.getJsonRow(checkRowIds[0]);
        var shipType = checkRowId0.shipType;
        var storeId = checkRowId0.storeId;

        if (checkRowIds.length < 1) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var idx in checkRowIds) {
            var checkedRows = shipReserveManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            if(storeId != checkedRows.storeId
                || shipType != checkedRows.shipType
                || checkedRows.shipType == 'RESERVE_DRCT_DLV'
                || checkedRows.storeType != 'HYPER') {
                alert("배송시간 일괄수정은 주문의 아래 항목이 동일해야 합니다.\n선택한 주문을 다시 확인해주세요."
                    + "\n- 같은 점포의 주문인 경우 일괄수정 가능"
                    + "\n- 같은 배송유형(절임배추이거나 일반상품)인 경우 일괄수정 가능"
                    + "\n- 명절선물세트 주문은 수정 불가");
                return;
            }
        }

        windowPopupOpen("/ship/popup/shipChangeSlotPop", "shipChangeSlotPop", 800, 300, "yes", "yes");
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipReserveManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "예약배송관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipReserveManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.setChangeStoreType();
        shippingUtil.initSearchDate("schStartDt", "0");
        shipReserveManageMain.setStoreOffice();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        // 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'bundleNo'
                || schKeywordTypeVal == 'purchaseOrderNo')) {
            return true;
        }

        // 조회범위 1일로 제한
        $('#schEndDt').val($('#schStartDt').val());

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    },
    searchMember : function (res) {
        $('#schUserNo').val(res.userNo);
        $('#schNomemOrderWord').val(res.userNm);
    }
}

/** Grid Script */
var shipReserveManageGrid = {
    gridView: new RealGridJS.GridView("shipReserveManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipReserveManageGrid.initGrid();
        shipReserveManageGrid.initDataProvider();
        shipReserveManageGrid.event();
    },
    initGrid: function () {
        shipReserveManageGrid.gridView.setDataSource(shipReserveManageGrid.dataProvider);
        shipReserveManageGrid.gridView.setStyles(shipReserveManageGridBaseInfo.realgrid.styles);
        shipReserveManageGrid.gridView.setDisplayOptions(shipReserveManageGridBaseInfo.realgrid.displayOptions);
        shipReserveManageGrid.gridView.setColumns(shipReserveManageGridBaseInfo.realgrid.columns);
        shipReserveManageGrid.gridView.setOptions(shipReserveManageGridBaseInfo.realgrid.options);

        var sameDataMerge = ['purchaseOrderNo','bundleNo'];
        orderUtil.setRealGridMerge(shipReserveManageGrid, sameDataMerge, "mergeRule");

        // 그리드 링크 렌더링
        shipReserveManageGrid.gridView.setColumnProperty("invoiceNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "invoiceNo", showUrl: false});
        shipReserveManageGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        shipReserveManageGrid.dataProvider.setFields(shipReserveManageGridBaseInfo.dataProvider.fields);
        shipReserveManageGrid.dataProvider.setOptions(shipReserveManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipReserveManageGrid.gridView.onDataCellDblClicked = function (grid, index) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
        };

        shipReserveManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");
            var dlvCd = grid.getValue(index.itemIndex, "dlvCd");
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo, "shipHistoryPop", 649, 744, 0, 0);
            } else if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            }
        };
    },
    setData: function (dataList) {
        shipReserveManageGrid.dataProvider.clearRows();
        shipReserveManageGrid.dataProvider.setRows(dataList);

        var bundleCheckArray = [];

        shipReserveManageGrid.gridView.setCheckableExpression(
            orderUtil.setRealGridArraysCheckable(shipReserveManageGrid, bundleCheckArray, 'bundleNo'), true);
    }
};