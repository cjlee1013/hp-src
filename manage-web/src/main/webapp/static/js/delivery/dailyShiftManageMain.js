/** 특정일 배송관리 Main */
var dailyShiftMng = {
    /**
     * init 이벤트
     */
    init: function () {
        dailyShiftMng.bindEvent();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        dailyShiftMng.initDetailDate();
    },
    /**
     * 상세그리드 날짜 초기화
     */
    initDetailDate: function () {
        deliveryCore.initSearchDateCustom("orderStartDt", "orderEndDt", "0", "0");
        deliveryCore.initSearchDateCustom("shiftShipStartDt", "shiftShipEndDt", "0", "0");
        $("#orderStartDt, #orderEndDt, #shiftShipStartDt, #shiftShipEndDt").val("").prop("disabled", true);
    },
    /**
     * 이벤트 바인딩
     */
    bindEvent: function () {
        // 검색어 특수기호(%\) 입력 제한, 점포코드/상품번호 숫자만 입력으로 제한
        $("#schKeyword, #schItemKeyword").notAllowInput("keyup", ['SPC_SCH']).on("keyup", function () {
            var schType = $("#schType").val();
            var schKeyword = $("#schKeyword").val();
            var schItemType = $("#schItemType").val();
            var schItemKeyword = $("#schItemKeyword").val();
            if (schType === "ID" && !$.jUtil.isEmpty(schKeyword) && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
                alert("점포코드는 숫자만 입력가능합니다.");
                $("#schKeyword").val("").focus();
                return false;
            }
            if (schItemType === "NO" && !$.jUtil.isEmpty(schItemKeyword) && !$.jUtil.isAllowInput(schItemKeyword, ['NUM'])) {
                alert("상품번호는 숫자만 입력가능합니다.");
                $("#schItemKeyword").val("").focus();
                return false;
            }
        });
        // 배송기간 입력 validation
        $("#shiftShipStartDt, #shiftShipEndDt").on("change", function () {
            var orderStartDtVal = moment($("#orderStartDt").val(), 'YYYY-MM-DD', true);
            var shipDtVal = moment($(this).val(), 'YYYY-MM-DD', true);
            if (shipDtVal.diff(orderStartDtVal, "day", true) < 0) {
                alert("배송일은 주문일보다 과거인 날짜를 선택할 수 없습니다.");
                $(this).val("");
                return false;
            }
        });
        // 검색 버튼
        $("#searchBtn").on("click", function () {
            // 조회조건 validation
            if (!dailyShiftMng.validSearch()) {
                return false;
            }
            dailyShiftMng.search();
        });
        // 검색 초기화 버튼
        $("#searchResetBtn").bindClick(dailyShiftMng.resetInfo);
        // 엑셀다운 버튼
        $("#excelDownloadBtn").bindClick(dailyShiftMng.excelDownload);
        // 상품검색 버튼
        $("#itemSelectBtn").bindClick(dailyShiftMng.searchItemPop);
        // 삭제 버튼
        $("#removeBtn").bindClick(dailyShiftMng.remove);
        // 저장 버튼
        $("#saveBtn").bindClick(dailyShiftMng.save);
        // 초기화 버튼
        $("#resetBtn").bindClick(dailyShiftMng.resetDetail);
    },
    /**
     * 상품검색 팝업호출
     */
    searchItemPop: function() {
        deliveryCore_popup.openItemSearchPopup('deliveryCore.callBack.setItemInfo','N','TD', $("#schStoreType").val());
    },
    /**
     * 조회조건 validation
     */
    validSearch: function () {
        var schType = $("#schType").val();
        var schKeyword = $("#schKeyword").val();
        var schItemType = $("#schItemType").val();
        var schItemKeyword = $("#schItemKeyword").val();
        var schStartDt = $("#schStartDt").val();
        var schEndDt = $("#schEndDt").val();
        var schStartDtVal = moment(schStartDt, 'YYYY-MM-DD', true);
        var schEndDtVal = moment(schEndDt, 'YYYY-MM-DD', true);

        // 필수값, 검색일자 체크
        // ESCROW-1025, 상품번호 검색시 점포검색어 필수 제외, 그 외 점포검색어 필수
        if ((schItemType === "NAME" || $.jUtil.isEmpty(schItemKeyword)) && $.jUtil.isEmpty(schKeyword)) {
            alert("점포를 입력해주세요.");
            return false;
        } else if ($.jUtil.isEmpty(schStartDt) || $.jUtil.isEmpty(schEndDt)) {
            alert("일자를 입력해주세요.");
            return false;
        } else if (!deliveryCore.dateValid($("#schStartDt"), $("#schEndDt"), ["isValid", "overOneMonthRange"])) {
            return false;
        } else if (schEndDtVal.diff(schStartDtVal, "day", true) < 0) {
            alert("종료일이 시작일보다 앞설 수 없습니다.");
            $("#schStartDt").val(schEndDtVal.format("YYYY-MM-DD"));
            return false;
        }

        // 검색어 체크
        if (!$.jUtil.isEmpty(schKeyword) && schKeyword.length < 2) {
            alert("검색어는 2글자 이상 입력하세요.");
            return false;
        } else if (schType === "ID" && !$.jUtil.isEmpty(schKeyword) && !$.jUtil.isAllowInput(schKeyword, ['NUM'])) {
            alert("점포코드는 숫자만 입력가능합니다.");
            return false;
        } else if (!$.jUtil.isEmpty(schItemKeyword) && schItemKeyword.length < 2) {
            alert("상품검색은 2글자 이상 입력하세요.");
            return false;
        } else if (schItemType === "NO" && !$.jUtil.isEmpty(schItemKeyword) && !$.jUtil.isAllowInput(schItemKeyword, ['NUM'])) {
            alert("상품번호는 숫자만 입력가능합니다.");
            return false;
        }
        return true;
    },
    /**
     * 일자별 Shift 정보 조회
     */
    search: function () {
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getDailyShiftManageList.json',
            data        : _F.getFormDataByJson($("#dailyShiftManageSearchForm")),
            method      : "POST",
            contentType : 'application/json; charset=utf-8',
            callbackFunc: function (dataList) {
                console.log(dataList);
                dailyShiftManageGrid.setData(dataList);
                $("#dailyShiftManageSearchCnt").html($.jUtil.comma(dailyShiftManageGrid.gridView.getItemCount()));
                // 상세정보 초기화
                dailyShiftMng.resetDetail();
            }
        });
    },
    /**
     * 일자별 Shift 점포 정보 조회
     */
    searchShiftStore: function (dailyShiftMngSeq) {
        CommonAjax.basic({
            url         : '/escrow/storeDelivery/getDailyShiftStoreManageList.json?dailyShiftMngSeq=' + dailyShiftMngSeq,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                $("#storeList").val(JSON.stringify(res));
                $("#applyStoreList").text(JSON.stringify(res));
                $("#applyStoreListCount").text(res.length);
            }
        });
    },
    /**
     * 검색영역 초기화
     */
    resetInfo: function () {
        $("#dailyShiftManageSearchForm").resetForm();
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if(dailyShiftManageGrid.gridView.getItemCount() === 0) {
            alert("조회 결과가 없습니다.");
            return false;
        }
        var _date = new Date();
        var fileName =  "일자별Shift관리_" + _date.getFullYear() + ("0"+(_date.getMonth()+1)).slice(-2) + ("0"+_date.getDate()).slice(-2);
        dailyShiftManageGrid.gridView.exportGrid({
            type           : "excel",
            target         : "local",
            fileName       : fileName + ".xlsx",
            showProgress   : true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },
    /**
     * 저장정보 validation
     */
    validSave: function () {
        var applyItemNo = $("#applyItemNo").text();
        var applyStoreList = $("#applyStoreList").text();
        var applyStoreListCount = $("#applyStoreListCount").text();
        var orderStartDt = $("#orderStartDt").val();
        var orderEndDt = $("#orderEndDt").val();
        var shiftShipStartDt = $("#shiftShipStartDt").val();
        var shiftShipEndDt = $("#shiftShipEndDt").val();
        var orderStartDtVal = moment(orderStartDt, 'YYYY-MM-DD', true);
        var shiftShipStartDtVal = moment(shiftShipStartDt, 'YYYY-MM-DD', true);

        // 필수값,일자 체크
        if ($.jUtil.isEmpty(applyItemNo)) {
            alert("상품을 선택해주세요.");
            return false;
        } else if ($.jUtil.isEmpty(applyStoreList) || (!$.jUtil.isEmpty(applyStoreListCount) && applyStoreListCount < 1)) {
            alert("점포를 선택해주세요.");
            return false;
        } else if ($.jUtil.isEmpty(orderStartDt) || $.jUtil.isEmpty(orderEndDt)) {
            alert("주문기간을 입력해주세요.");
            return false;
        } else if ($.jUtil.isEmpty(shiftShipStartDt) || $.jUtil.isEmpty(shiftShipEndDt)) {
            alert("배송기간을 입력해주세요.");
            return false;
        } else if (shiftShipStartDtVal.diff(orderStartDtVal, "day", true) < 0) {
            alert("배송일은 주문일보다 과거인 날짜를 선택할 수 없습니다.");
            return false;
        }
        return true;
    },
    /**
     * 저장처리
     */
    save: function() {
        // 저장정보 validation
        if (!dailyShiftMng.validSave()) {
            return false;
        }

        // 중복체크 및 저장실행
        var data = new Object();
        var storeIdList = new Array();
        var dailyShiftMngSeq = $("#dailyShiftMngSeq").val();
        for (var storeData of JSON.parse($("#applyStoreList").text())) {
            var temp = Number(storeData.storeId);
            storeIdList.push(temp);
        }
        data.orderStartDt = $("#orderStartDt").val();
        data.orderEndDt = $("#orderEndDt").val();
        data.itemNo = $("#applyItemNo").text();
        data.storeIdList = storeIdList;
        if (!$.jUtil.isEmpty(dailyShiftMngSeq)) {
            data.dailyShiftMngSeq = dailyShiftMngSeq;
        }

        CommonAjax.basic({
            url: '/escrow/storeDelivery/checkDuplicateDailyShiftManage.json',
            data: JSON.stringify(data),
            method: "POST",
            contentType: 'application/json; charset=utf-8',
            callbackFunc: function (res) {
                if (res > 0) {
                    alert("선택한 기간내 중복된 데이터가 존재합니다.");
                    return false;
                } else {
                    // 저장실행
                    dailyShiftMng.saveDailyShift();
                }
            }
        });
    },
    /**
     * 일자별 Shift 정보 저장
     */
    saveDailyShift: function() {
        var data = new Object();
        var dailyShiftMngSeq = $("#dailyShiftMngSeq").val();

        // 업데이트인 경우, 기존 데이터 삭제 수행
        if (!$.jUtil.isEmpty(dailyShiftMngSeq)) {
            var dataList = new Array();
            data.dailyShiftMngSeq = dailyShiftMngSeq;
            dataList.push(data);
            dailyShiftMng.deleteDailyShift(dataList, "");
        }

        // 저장실행
        data.itemNo = $("#applyItemNo").text();
        data.orderStartDt = $("#orderStartDt").val();
        data.orderEndDt = $("#orderEndDt").val();
        data.shiftShipStartDt = $("#shiftShipStartDt").val();
        data.shiftShipEndDt = $("#shiftShipEndDt").val();
        CommonAjax.basic({
            url: '/escrow/storeDelivery/saveDailyShiftManage.json',
            data: JSON.stringify(data),
            method: "POST",
            contentType: 'application/json; charset=utf-8',
            callbackFunc: function (res) {
                dailyShiftMng.saveDailyShiftStore(res.dailyShiftMngSeq);
            }
        });
    },
    /**
     * 일자별 Shift 점포정보 저장
     */
    saveDailyShiftStore: function (dailyShiftMngSeq) {
        var formData = new Array();
        for (var storeData of JSON.parse($("#applyStoreList").text())) {
            var data = new Object();
            data.dailyShiftMngSeq = dailyShiftMngSeq;
            data.storeId = storeData.storeId;
            formData.push(data);
        }
        CommonAjax.basic({
            url: '/escrow/storeDelivery/saveDailyShiftStoreManageList.json',
            data: JSON.stringify(formData),
            method: "POST",
            contentType: 'application/json; charset=utf-8',
            callbackFunc: function (res) {
                if (res > 0) {
                    alert("저장하였습니다.");
                }
                dailyShiftMng.search();
            }
        });
    },
    /**
     * 삭제버튼 처리
     */
    remove: function () {
        // 메인 그리드 체크여부 확인
        var checkedList = dailyShiftManageGrid.gridView.getCheckedRows();
        if (checkedList.length === 0) {
            alert("삭제할 데이터를 체크하세요.");
            return false;
        }
        if(!confirm("삭제하시겠습니까?")) {
            return false;
        }
        // 삭제대상 세팅
        var targetList = new Array();
        for (var checkRowId of checkedList) {
            var data = new Object();
            var checkedRow = dailyShiftManageGrid.dataProvider.getJsonRow(checkRowId);
            data.dailyShiftMngSeq = checkedRow.dailyShiftMngSeq;
            targetList.push(data);
        }
        // 삭제처리
        dailyShiftMng.deleteDailyShift(targetList, "btn");
    },
    /**
     * 일자별 Shift 관리 삭제
     */
    deleteDailyShift: function (dataList, flag) {
        CommonAjax.basic({
            url: '/escrow/storeDelivery/deleteDailyShiftManage.json',
            data: JSON.stringify(dataList),
            method: "POST",
            contentType: 'application/json; charset=utf-8',
            callbackFunc: function (res) {
                if (flag === "btn") {
                    alert("삭제되었습니다.");
                    dailyShiftMng.search();
                }
            }
        });
    },
    /**
     * 상세 그리드 초기화
     */
    resetDetail: function () {
        $("#applyItemNo").text("");
        $("#applyItemNm").text("");
        $("#applyStoreList").text("");
        $("#applyStoreListCount").text("0");
        $("#dailyShiftMngSeq").val("");
        $("#storeList").val("");
        dailyShiftMng.initDetailDate();
    },
    /**
     * 그리드에서 클릭한 정보 하단 정보영역에 그려주기
     */
    drawDetail: function (data) {
        // key값 세팅
        $("#dailyShiftMngSeq").val(data.dailyShiftMngSeq);

        // 상품정보 세팅
        $("#applyItemNo").text(data.itemNo);
        $("#applyItemNm").text(data.itemNm);

        // 점포정보 세팅
        dailyShiftMng.searchShiftStore(data.dailyShiftMngSeq);

        // 주문기간, 배송기간 세팅
        $("#orderStartDt").val(moment(data.orderStartDt).format("YYYY-MM-DD"));
        $("#orderEndDt").val(moment(data.orderEndDt).format("YYYY-MM-DD"));
        $("#shiftShipStartDt").val(moment(data.shiftShipStartDt).format("YYYY-MM-DD"));
        $("#shiftShipEndDt").val(moment(data.shiftShipEndDt).format("YYYY-MM-DD"));
    },
    /**
     * 일자변경
     */
    setSearchDate: function (dateTypCd) {
        if (dateTypCd === "T") {
            deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
        } else if (dateTypCd === "W") {
            deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
        } else if (dateTypCd === "M") {
            deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1m", "0");
        }
    }
};

/** Grid Script */
var dailyShiftManageGrid = {
    gridView: new RealGridJS.GridView("dailyShiftManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        dailyShiftManageGrid.initGrid();
        dailyShiftManageGrid.initDataProvider();
        dailyShiftManageGrid.event();
    },
    initGrid: function () {
        dailyShiftManageGrid.gridView.setDataSource(dailyShiftManageGrid.dataProvider);
        dailyShiftManageGrid.gridView.setStyles(dailyShiftManageGridBaseInfo.realgrid.styles);
        dailyShiftManageGrid.gridView.setDisplayOptions(dailyShiftManageGridBaseInfo.realgrid.displayOptions);
        dailyShiftManageGrid.gridView.setColumns(dailyShiftManageGridBaseInfo.realgrid.columns);
        dailyShiftManageGrid.gridView.setOptions(dailyShiftManageGridBaseInfo.realgrid.options);
        dailyShiftManageGrid.gridView.setCheckBar({visible: true, exclusive: false, showAll: false});
        setColumnLookupOption(dailyShiftManageGrid.gridView, "storeType", $("#schStoreType"));
    },
    initDataProvider: function () {
        dailyShiftManageGrid.dataProvider.setFields(dailyShiftManageGridBaseInfo.dataProvider.fields);
        dailyShiftManageGrid.dataProvider.setOptions(dailyShiftManageGridBaseInfo.dataProvider.options);
    },
    event: function () {
        //그리드 클릭 이벤트
        dailyShiftManageGrid.gridView.onDataCellClicked = function (gridView, index) {
            var data = dailyShiftManageGrid.dataProvider.getJsonRow(index.dataRow);
            dailyShiftMng.drawDetail(data);
        };
    },
    setData: function (dataList) {
        dailyShiftManageGrid.dataProvider.clearRows();
        dailyShiftManageGrid.dataProvider.setRows(dataList);
    }
};