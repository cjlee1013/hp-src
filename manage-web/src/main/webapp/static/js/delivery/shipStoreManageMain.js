/** Main Script */
var shipStoreManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        shipStoreManageMain.setStoreOffice();
        shipStoreManageMain.bindingEvent();
        shippingUtil.initSearchDate("schStartDt", "0");
        shipStoreManageMain.setChangeStoreType();

    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(shipStoreManageMain.openStorePopup);
        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { shipStoreManageMain.setChangeStoreType() });

        // 구매자 조회
        $("#schBuyer").on('click', function() { shipStoreManageMain.openMemberSearchPop() });
        // 구매자 비회원 선택
        $('input:radio[name=schNomemOrderYn]').on('change', function() { shipStoreManageMain.setBuyerSearch($(this)) });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipStoreManageMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipStoreManageMain.reset, "shipStoreManageSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(shipStoreManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "0")});
        // 달력 - [1주일] 버튼
        $("#setOneWeekBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "-1d")});
        // 달력 - [1개월] 버튼
        //$("#setOneMonthBtn").on("click", function() {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
            $("#schShipDtType").val("reserveShipDt");
        }
    },

    /**
     * 배송방법 설정
     */
    setChangeStoreType: function() {
        var storeType = $('#schStoreType').val();

        $('#schShipMethod option').each(function () {
            var $this = $(this);
            var shipMethod = $this.val();

            if (!$.jUtil.isEmpty(shipMethod)) {
                if(storeType == 'HYPER') {
                    if(shipMethod == 'TD_DRCT'
                        || shipMethod == 'TD_PICK') {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                } else if (storeType == 'EXP') {
                    if(shipMethod == 'TD_QUICK') {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                }
            }
        });

        $("#schShipMethod option:eq(0)").prop("selected", true);
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 회원 검색 팝업
     */
    openMemberSearchPop: function() {
        memberSearchPopup("callBack.searchMember");
    },

    /**
     * 구매자 회원/비회원 라디오 선택
     */
    setBuyerSearch: function($this) {
        var radioVal = $this.val();

        $('#schUserNo').val("");
        $('#schNomemOrderWord').val("");
        $("#schNomemOrderType option:eq(0)").prop("selected", true);

        if (radioVal == "Y") {
            $('#schNomemOrderWord').attr('readonly',false);
            $('#schNomemOrderType').show();
            $('#schBuyer').hide();
        } else {
            $('#schNomemOrderWord').attr('readonly',true);
            $('#schNomemOrderType').hide();
            $('#schBuyer').show();
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#shipStoreManageSearchForm").serialize();

        CommonAjax.basic({
            url         : '/ship/shipManage/getShipStoreManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipStoreManageGrid.setData(res);
                $('#shipStoreManageSearchCnt').html($.jUtil.comma(shipStoreManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipStoreManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "점포배송관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipStoreManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 배송shift selectbox 설정
     */
    setShiftSelect: function(storeId) {
        var html = '<option value="">전체</option>';

        if ($.jUtil.isEmpty(storeId)) {
            $('#schShift').html(html);
        } else {
            var reqData = "storeId="+storeId+"&storeType="+$('#schStoreType').val();

            CommonAjax.basic({
                url         : '/ship/shipManage/getStoreShiftList.json?' + reqData,
                data        : null,
                method      : "GET",
                callbackFunc: function(res) {
                    for(var shift of res) {
                        html += '<option value="'+shift+'">'+shift+'</option>';
                    }

                    $('#schShift').html(html);
                }
            });
        }
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shipStoreManageMain.setChangeStoreType();
        shippingUtil.initSearchDate("schStartDt", "0");
        shipManageMain.setStoreOffice();
        shipManageMain.setShiftSelect();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        // 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'bundleNo'
                || schKeywordTypeVal == 'purchaseOrderNo'
                || schKeywordTypeVal == 'purchaseStoreInfoNo')) {
            return true;
        }

        // 조회범위 1일로 제한
        $('#schEndDt').val($('#schStartDt').val());

        // 점포 필수 선택
        var schStoreIdVal = $("#schStoreId").val();
        if ($.jUtil.isEmpty(schStoreIdVal)) {
            alert("점포를 선택해 주세요.");
            return false;
        }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        var $schSordNoMin = $('#schSordNoMin');
        var $schSordNoMax = $('#schSordNoMax');

        if(!$.jUtil.isEmpty($schSordNoMin.val()) || !$.jUtil.isEmpty($schSordNoMax.val())) {
            if (!/^$|^[0-9]$|^[1-9][0-9]*$/.test($schSordNoMin.val())
                || $schSordNoMin.val() == '0'
                || $.jUtil.isEmpty($schSordNoMin.val())) {
                alert('0보다 큰 숫자만 입력 가능합니다.');
                $schSordNoMin.focus();
                return false;
            }

            if (!/^$|^[0-9]$|^[1-9][0-9]*$/.test($schSordNoMax.val())
                || $schSordNoMax.val() == '0'
                || $.jUtil.isEmpty($schSordNoMax.val())) {
                alert('0보다 큰 숫자만 입력 가능합니다.');
                $schSordNoMax.focus();
                return false;
            }

            if(parseInt($schSordNoMin.val()) > parseInt($schSordNoMax.val())) {
                alert('단축번호 최소값이 최대값보다 큽니다.');
                return false;
            }
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);

        shipStoreManageMain.setShiftSelect(res[0].storeId);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);

        shipStoreManageMain.setShiftSelect();
    },
    searchMember : function (res) {
        $('#schUserNo').val(res.userNo);
        $('#schNomemOrderWord').val(res.userNm);
    }
}

/** Grid Script */
var shipStoreManageGrid = {
    gridView: new RealGridJS.GridView("shipStoreManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipStoreManageGrid.initGrid();
        shipStoreManageGrid.initDataProvider();
        shipStoreManageGrid.event();
    },
    initGrid: function () {
        shipStoreManageGrid.gridView.setDataSource(shipStoreManageGrid.dataProvider);
        shipStoreManageGrid.gridView.setStyles(shipStoreManageGridBaseInfo.realgrid.styles);
        shipStoreManageGrid.gridView.setDisplayOptions(shipStoreManageGridBaseInfo.realgrid.displayOptions);
        shipStoreManageGrid.gridView.setColumns(shipStoreManageGridBaseInfo.realgrid.columns);
        shipStoreManageGrid.gridView.setOptions(shipStoreManageGridBaseInfo.realgrid.options);

        var sameDataMerge = ['purchaseOrderNo','bundleNo'];
        orderUtil.setRealGridMerge(shipStoreManageGrid, sameDataMerge, "mergeRule");

        orderUtil.setCheckBarHeader([shipStoreManageGrid], "선택", null);
    },
    initDataProvider: function () {
        shipStoreManageGrid.dataProvider.setFields(shipStoreManageGridBaseInfo.dataProvider.fields);
        shipStoreManageGrid.dataProvider.setOptions(shipStoreManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipStoreManageGrid.gridView.onDataCellDblClicked = function (grid, index) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
        };
    },
    setData: function (dataList) {
        shipStoreManageGrid.dataProvider.clearRows();
        shipStoreManageGrid.dataProvider.setRows(dataList);
    }
};