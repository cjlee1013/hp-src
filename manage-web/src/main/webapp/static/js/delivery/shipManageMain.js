/** Main Script */
var shipManageMain = {
    /**
     * init 이벤트
     */
    init: function() {
        shipManageMain.setStoreOffice();
        shipManageMain.bindingEvent();
        shippingUtil.initSearchDate("schStartDt", "0");
        shippingUtil.setChangeStoreType();

    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포 조회 버튼
        $("#schStoreBtn").bindClick(shipManageMain.openStorePopup);
        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { shippingUtil.setChangeStoreType() });

        // 구매자 조회
        $("#schBuyer").on('click', function() { shipManageMain.openMemberSearchPop() });
        // 구매자 비회원 선택
        $('input:radio[name=schNomemOrderYn]').on('change', function() { shipManageMain.setBuyerSearch($(this)) });

        // 상단 [검색] 버튼
        $("#schBtn").bindClick(shipManageMain.search);
        // 상단 [초기화] 버튼
        $("#schResetBtn").bindClick(shipManageMain.reset, "shipManageSearchForm");
        // 상단 [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(shipManageMain.excelDownload);

        // 달력 - [오늘] 버튼
        $("#setTodayBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "0")});
        // 달력 - [어제] 버튼
        $("#setOneWeekBtn").on("click", function() {shippingUtil.initSearchDate("schStartDt", "-1d")});
        // 달력 - [1개월] 버튼
        //$("#setOneMonthBtn").on("click", function() {shippingUtil.initSearchDateCustom("schStartDt", "schEndDt","-1m", "0")});

        // 주문확인 버튼
        $("#confirmOrder").bindClick(shipManageMain.setConfirmOrder);
        // 주문서출력
        $("#shipInfoPrint").bindClick(shipManageMain.openShipInfoPrintPopup);
        // 발송지연안내 버튼
        $("#notiDelay").bindClick(shipManageMain.openNotiDelayPopup);
        // 일괄발송처리 버튼
        $("#shipAllExcel").bindClick(shipManageMain.openShipAllExcelPopup);
        // 선택발송처리 버튼
        $("#shipAll").bindClick(shipManageMain.openShipAllPopup);
        // 배송완료 버튼
        $("#shipComplete").bindClick(shipManageMain.openShipCompletePop);
    },

    /**
     * Store Office 권한으로 접속 시 고정
     */
    setStoreOffice: function() {
        if (!$.jUtil.isEmpty(so_storeId)) {
            $("#schStoreType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(so_storeId);
            $("#schStoreNm").val(so_storeNm);
            $("#schStoreType").val(so_storeType);
        }
    },

    /**
     * 점포 / 판매 업체 조회 팝업
     */
    openStorePopup: function() {
        var storeTypeVal = $('#schStoreType').val();

        if ($.jUtil.isEmpty(storeTypeVal)) {
            alert("점포유형을 선택해주세요.");
            return;
        }

        if (storeTypeVal == "DS"){
            windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
        }else{
            windowPopupOpen("/common/popup/storePop?callback=callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
        }
    },

    /**
     * 회원 검색 팝업
     */
    openMemberSearchPop: function() {
        memberSearchPopup("callBack.searchMember");
    },

    /**
     * 구매자 회원/비회원 라디오 선택
     */
    setBuyerSearch: function($this) {
        var radioVal = $this.val();

        $('#schUserNo').val("");
        $('#schNomemOrderWord').val("");
        $("#schNomemOrderType option:eq(0)").prop("selected", true);

        if (radioVal == "Y") {
            $('#schNomemOrderWord').attr('readonly',false);
            $('#schNomemOrderType').show();
            $('#schBuyer').hide();
        } else {
            $('#schNomemOrderWord').attr('readonly',true);
            $('#schNomemOrderType').hide();
            $('#schBuyer').show();
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#shipManageSearchForm").serialize();

        CommonAjax.basic({
            url         : '/ship/shipManage/getShipManageList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                shipManageGrid.setData(res);
                $('#shipManageSearchCnt').html($.jUtil.comma(shipManageGrid.gridView.getItemCount()));
            }
        });
    },

    /**
     * 주문확인
     */
    setConfirmOrder: function () {
        var checkRowIds = shipManageGrid.gridView.getCheckedRows();
        var reqData = new Object();
        var reqCnt = 0;
        var bundleNoList = new Array();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var idx in checkRowIds) {
            var checkedRows = shipManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            if (checkedRows.shipStatus != 'D1') {
                alert("신규주문 건만 진행할 수 있습니다.");
                return;
            }

            bundleNoList.push(checkedRows.bundleNo);
        }

        if (bundleNoList.length === 0) {
            alert("주문확인 가능한 건이 없습니다. 주문상태를 확인해 주세요.");
            return;
        }

        reqData.bundleNoList = bundleNoList;
        reqCnt = bundleNoList.length;

        if (confirm("선택하신 주문건에 대해서 주문확인을 진행하겠습니까?")) {
            CommonAjax.basic({
                url         : '/ship/shipManage/setConfirmOrder.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    var failCnt = parseInt(reqCnt) - parseInt(res.data);

                    if (res.returnCode != "SUCCESS" || failCnt > 0) {
                        alert("주문확인 처리 중 "+failCnt+"건 실패했습니다.");
                    } else {
                        alert("주문확인을 했습니다.");
                    }

                    shipManageMain.search();
                }
            });
        }
    },

    /**
     * 배송완료
     */
    openShipCompletePop: function () {
        var checkRowIds = shipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        for (var idx in checkRowIds) {
            var checkedRows = shipManageGrid.dataProvider.getJsonRow(checkRowIds[idx]);

            // samsung 말일 배송완료 금지
            if (checkedRows.partnerId == "samsung" && shippingUtil.isLastDay()) {
                alert("samgsung 상품은 매월 말일 배송완료 처리가 불가합니다.\n다음 달 1일 이후 완료처리 진행 부탁드립니다.");
                return;
            }

            if (checkedRows.shipStatus != 'D3') {
                alert("배송중인 건만 진행할 수 있습니다.");
                return;
            }
        }

        windowPopupOpen("/ship/popup/getShipCompletePop?callback=shipManageMain.search", "getShipCompletePop", 662, 467, "yes", "yes");
    },

    /**
     * 주문서출력 팝업창 open
     */
    openShipInfoPrintPopup: function() {
        var checkRowIds = shipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length > 1) {
            alert("1개의 배송건만 가능합니다.");
            return;
        }

        var checkedRow = shipManageGrid.dataProvider.getJsonRow(checkRowIds[0]);
        var bundleNo = checkedRow.bundleNo;

        windowPopupOpen("/ship/popup/getShipInfoPrintPop?bundleNo="+bundleNo, "shipInfoPrintPop", 1060, 630, "yes", "yes");
    },

    /**
     * 발송지연안내 팝업창 open
     */
    openNotiDelayPopup: function() {
        var checkRowIds = shipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (checkRowIds.length > 1) {
            alert("1개의 주문만 가능합니다.");
            return;
        }

        var checkedRow = shipManageGrid.dataProvider.getJsonRow(checkRowIds[0]);
        var bundleNo = checkedRow.bundleNo;

        if (checkedRow.delayShipDt != null) {
            alert("발송지연 안내는 1회만 가능합니다.");
            return;
        } else if (checkedRow.shipStatus != 'D1' && checkedRow.shipStatus != 'D2') {
            alert("신규주문/상품준비중 건만 진행할 수 있습니다.");
            return;
        }

        windowPopupOpen("/ship/popup/getNotiDelayPop?callback=shipManageMain.search&bundleNo="+bundleNo, "notiDelayPop", 530, 480, "yes", "yes");
    },

    /**
     * 일괄발송처리 팝업창 open
     */
    openShipAllExcelPopup: function() {
        // 택배배송 템플릿
        const templateUrlDlv = "/static/templates/shipAll_dlv_template.xlsx";
        // 직접배송 템플릿
        const templateUrlDrct = "/static/templates/shipAll_drct_template.xlsx";

        windowPopupOpen("/ship/popup/getShipAllExcelPop?callback=shipManageMain.search&templateUrlDlv="+templateUrlDlv+"&templateUrlDrct="+templateUrlDrct, "shipAllExcelPop", 666, 376, "yes", "yes");
    },

    /**
     * 선택발송처리 팝업창 open
     */
    openShipAllPopup: function() {
        var checkRowIds = shipManageGrid.gridView.getCheckedRows();

        if (checkRowIds.length === 0) {
            alert("주문을 선택해주세요.");
            return;
        }

        if (checkRowIds.length > 1 ) {
            alert("1개의 배송건만 선택해주세요.");
            return;
        }

        var checkedRows = shipManageGrid.dataProvider.getJsonRow(checkRowIds[0]);

        if (checkedRows.shipStatus != 'D2' && checkedRows.shipStatus != 'D3' && checkedRows.shipStatus != 'D4') {
            alert("상품준비중/배송중/배송완료 건만 진행할 수 있습니다.");
            return;
        }

        windowPopupOpen("/ship/popup/getShipAllPop?callback=shipManageMain.search&bundleNo="+checkedRows.bundleNo+"&mallType="+checkedRows.mallType, "shipAllPop", 645, 540, "yes", "yes");
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (shipManageGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "택배배송관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();
        shipManageGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();
        shippingUtil.setChangeStoreType();
        shippingUtil.initSearchDate("schStartDt", "0");
        shipManageMain.setStoreOffice();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {

        // 검색어 입력시, 다른 검색조건 무시
        var schKeywordVal = $("#schKeyword").val();
        var schKeywordTypeVal = $("#schKeywordType").val();

        if (!$.jUtil.isEmpty(schKeywordVal)
            && (schKeywordTypeVal == 'bundleNo'
                || schKeywordTypeVal == 'purchaseOrderNo'
                || schKeywordTypeVal == 'orderItemNo')) {
            return true;
        }

        // 조회범위 1일로 제한
        $('#schEndDt').val($('#schStartDt').val());

        // 점포 필수 선택
        // var schStoreIdVal = $("#schStoreId").val();
        // if ($.jUtil.isEmpty(schStoreIdVal)) {
        //     alert("점포/판매업체를 선택해 주세요.");
        //     return false;
        // }

        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#schStartDt");
        var $schEndDt = $("#schEndDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 정보를 입력해 주세요.");
            return false;
        }

        return true;
    }
}

/** 콜백 */
var callBack = {
    searchStore : function (res) {
        $('#schStoreId').val(res[0].storeId);
        $('#schStoreNm').val(res[0].storeNm);
    },
    searchPartner : function (res) {
        $('#schStoreId').val(res[0].partnerId);
        $('#schStoreNm').val(res[0].partnerNm);
    },
    searchMember : function (res) {
        $('#schUserNo').val(res.userNo);
        $('#schNomemOrderWord').val(res.userNm);
    }
}

/** Grid Script */
var shipManageGrid = {
    gridView: new RealGridJS.GridView("shipManageGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        shipManageGrid.initGrid();
        shipManageGrid.initDataProvider();
        shipManageGrid.event();
    },
    initGrid: function () {
        shipManageGrid.gridView.setDataSource(shipManageGrid.dataProvider);
        shipManageGrid.gridView.setStyles(shipManageGridBaseInfo.realgrid.styles);
        shipManageGrid.gridView.setDisplayOptions(shipManageGridBaseInfo.realgrid.displayOptions);
        shipManageGrid.gridView.setColumns(shipManageGridBaseInfo.realgrid.columns);
        shipManageGrid.gridView.setOptions(shipManageGridBaseInfo.realgrid.options);

        var sameDataMerge = ['purchaseOrderNo','bundleNo'];
        orderUtil.setRealGridMerge(shipManageGrid, sameDataMerge, "mergeRule");
        orderUtil.setCheckBarHeader([shipManageGrid], "선택", null);

        // 그리드 링크 렌더링
        shipManageGrid.gridView.setColumnProperty("invoiceNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "invoiceNo", showUrl: false});
        shipManageGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});
    },
    initDataProvider: function () {
        shipManageGrid.dataProvider.setFields(shipManageGridBaseInfo.dataProvider.fields);
        shipManageGrid.dataProvider.setOptions(shipManageGridBaseInfo.dataProvider.options);
    },
    event: function() {
        shipManageGrid.gridView.onDataCellDblClicked = function (grid, index) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            orderUtil.setLinkTab("orderDetail", "주문정보상세","/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo )
        };

        shipManageGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var invoiceNo = grid.getValue(index.itemIndex, "invoiceNo");
            var dlvCd = grid.getValue(index.itemIndex, "dlvCd");
            var itemNo = grid.getValue(index.itemIndex, "itemNo");
            var storeType = grid.getValue(index.itemIndex, "storeType");
            var siteType = grid.getValue(index.itemIndex, "siteType");

            if (index.fieldName == "invoiceNo" && !$.jUtil.isEmpty(invoiceNo)) {
                windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + dlvCd + "&invoiceNo=" + invoiceNo, "shipHistoryPop", 649, 744, 0, 0);
            } else if (index.fieldName == "itemNo") {
                var frontUrl = siteType == "CLUB" ? theclubFrontUrl : homeplusFrontUrl;
                window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
            }
        };
    },
    setData: function (dataList) {
        shipManageGrid.dataProvider.clearRows();
        shipManageGrid.dataProvider.setRows(dataList);
        var bundleCheckArray = [];

        for(let idx in dataList){
            if (dataList[idx].shipStatus == 'D5') {
                bundleCheckArray.push(dataList[idx].bundleNo);
            }
        }

        shipManageGrid.gridView.setCheckableExpression(
            orderUtil.setRealGridArraysCheckable(shipManageGrid, bundleCheckArray, 'bundleNo'), true);
    }
};