/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  claimReturnDetail.init();
});

// 클레임상세정보
var claimReturnDetail = {
    /**
     * 초기화
     */
    init : function() {
      claimCommonUtil.init();
      claimCommonUtil.registerPreRefund();
      claimReturnDetail.returnReasonInfo();
    },
    /**
     * 반품신청 사유, 환불금액 조회
     * **/
    returnReasonInfo : function (){
        CommonAjax.basic(
              {
                  url:'/claim/info/getClaimReasonInfo.json',
                  data: JSON.stringify({
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo,
                    "claimType" : claimType
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimCommonUtil.writeToSpan("returnDetailInfoTable", res.claimPickShipping);
                      /**
                       * 반품 상세정보 적용
                       * **/
                      claimCommonUtil.setClaimReasonType(res);
                      /**
                       * 미차감여부 적용
                       * **/
                      claimCommonUtil.setDeductInfo(res);
                      /**
                       * 반품배송비 text 적용
                       * **/
                      claimCommonUtil.setReturnShipAmt(res);
                      /**
                       * 첨부파일 적용
                       * **/
                      claimCommonUtil.setUploadFile(res);
                      /**
                       * 처리상태 버튼 노출
                       * **/
                      claimReturnDetail.setBtn(res);
                      /**
                       * 수거방법별 표기방법
                       * **/
                      claimCommonUtil.setPickShipText(res);
                      /**
                       * 환불수단 적용
                       * **/
                      claimCommonUtil.setRefundType(res);
                      /**
                       * 택배 동봉 여부에 따른 차감금액/환불에정금액 재계산
                       * **/
                      claimReturnDetail.checkClaimShipFeeEnclose(res);
                  }
              });
    },

    /**
     * 택배 동봉 여부에 따른 차감금액/환불에정금액 재계산
     * 반품에만 적용됨
     * **/
    checkClaimShipFeeEnclose : function(res){
        if(res.claimShipFeeEnclose == '택배 동봉'){
            var totShipPrice = parseInt(res.totAddShipPrice) + parseInt(res.totAddIslandShipPrice);
            $('#addShipPrice').text(0);
            $('#addIslandShipPrice').text(0);
            $('#discountAmt').text($.jUtil.comma(parseInt($('#discountAmt').text().replace(',','')) - parseInt(totShipPrice)));
        }

    },

    /**
     * 처리상태 버튼 노출
     * **/
    setBtn : function(res){
        var claimStatusCode = res.claimStatusCode;//클레임상태
        var pickStatus = res.claimPickShipping.pickStatusCode;//수거상태
        var marketType = $.jUtil.isNotEmpty(res.marketType);
        //수거지변경 : 반품신청(C1), 반품보류(C8) && 수거대기(NN), 수거중(P2), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P2' || pickStatus == 'P8') && !marketType){
            $('#chgPickAddrBtn').show();
        }
        //수거요청 : 반품신청(C1), 반품승인(C2), 반품보류(C8) && 수거예약(N0), 수거대기(NN), 수거요청(P0), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8')&& (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P0' || pickStatus == 'P8') && !marketType){
           $('#reqPick').show();
        }
        //수거완료 : 반품신청(C1), 반품승인(C2), 반품보류(C8) && 수거예약(N0), 수거대기(NN), 수거요청(P0), 요청완료(P1), 수거중(P2), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P0' || pickStatus == 'P1' || pickStatus == 'P2' || pickStatus == 'P8') && !marketType){
            $('#completePick').show();
        }
        //반품승인 : 반품신청(C1), 반품보류(C8) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거완료(P3), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatus == 'N0' || pickStatus == 'NN' || pickStatus == 'P1' || pickStatus == 'P2' || pickStatus == 'P3' || pickStatus == 'P8')){
            $('#reqApprove').show();
        }
        //반품보류 : 반품신청(C1) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거완료(P3), 수거실패(P8)
        if(claimStatusCode == 'C1' && (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P1' || pickStatus == 'P2' || pickStatus == 'P3' || pickStatus == 'P8')){
            $('#reqPending').show();
        }
        //반품철회 : 반품신청(C1), 반품보류(C8) && 수거대기(NN,N0), 수거요청완료(P1), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P0'  || pickStatus == 'P8')){
            $('#reqWithDraw').show();
        }
        //반품거부 : 반품신청(C1), 반품보류(C8)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
            $('#reqReject').show();
        }
        // 보류/거절 사유 표기
        if(claimStatusCode == 'C8'){//보류 사유
            $('#pendingReason').show();
        }else if(claimStatusCode == 'C9'){//거절 사유
            $('#rejectReason').show();
        }
    }
};
/**
 * 수거지 변경 [저장]시 호출되는 함수
 * **/
function setAddrCallBack(res){
    $('#pickReceiverNm').text(res.name);
    $('#pickMobileNo').text(res.phone);
    $('#pickZipcode').text('(' + res.zipCode + ')');
    $('#pickAddr').text(res.addr1);
    $('#pickAddrDetail').text(res.addr2);
}







