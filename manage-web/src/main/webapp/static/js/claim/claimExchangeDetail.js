/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  claimExchangeDetail.init();
});
// 클레임상세정보
var claimExchangeDetail = {
    /**
     * 초기화
     */
    init : function() {
      claimCommonUtil.init();
      claimExchangeDetail.exchangeReasonInfo();
    },
    /**
     * 교환신청 사유, 환불금액 조회
     * **/
    exchangeReasonInfo : function (){
        CommonAjax.basic(
              {
                  url:'/claim/info/getClaimReasonInfo.json',
                  data: JSON.stringify({
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo,
                    "claimType" : claimType
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      /**
                       * 반품 수거지 정보 적용
                       * **/
                      claimCommonUtil.writeToSpan("pickDetailInfoTable", res.claimPickShipping);
                      /**
                       * 반품 수거지 정보 적용
                       * **/
                      claimCommonUtil.writeToSpan("exchangeDetailInfoTable", res.claimExchShipping);
                      /**
                       * 교환 상세정보 적용
                       * **/
                      claimCommonUtil.setClaimReasonType(res);
                      /**
                       * 미차감여부 적용
                       * **/
                      claimCommonUtil.setDeductInfo(res);
                      /**
                       * 반품배송비 text 적용
                       * **/
                      claimCommonUtil.setReturnShipAmt(res);
                      /**
                       * 첨부파일 적용
                       * **/
                      claimCommonUtil.setUploadFile(res);
                      /**
                       * 처리상태 버튼 노출
                       * **/
                      claimExchangeDetail.setBtn(res);
                      /**
                       * 수거방법별 표기방법
                       * **/
                      claimCommonUtil.setPickShipText(res);
                      /**
                       * 배송방법별 표기방법
                       * **/
                      claimCommonUtil.setExchShipText(res);
                  }
              });
    },

    /**
     * 처리상태 버튼 노출
     * **/
    setBtn : function(res){
        var claimStatusCode = res.claimStatusCode;//클레임상태
        var pickStatus = res.claimPickShipping.pickStatusCode;//수거상태
        var exchStatus = $.jUtil.isEmpty(res.claimExchShipping) ? '' : res.claimExchShipping.exchStatusCode;//교환상태
        var marketType = res.marketType !== '-';
        //수거방법변경 : 교환신청(C1), 교환보류(C8) && 수거대기(NN), 수거중(P2), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatus == 'N0' ||pickStatus == 'NN' || pickStatus == 'P2' || pickStatus == 'P8') && !marketType){
            $('#chgPickShipTypeBtn').show();
        }
        //배송지방법변경 : 교환신청(C1), 교환보류(C8), 교환승인(C2)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8' || claimStatusCode == 'C2') && !marketType){
            $('#chgShipTypeBtn').show();
        }
        //수거요청 : 교환신청(C1), 교환승인(C2), 교환보류(C8) && 수거예약(N0), 수거대기(NN), 수거요청(P0), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatus == 'N0' || pickStatus == 'NN' || pickStatus == 'P0' || pickStatus == 'P8')){
           $('#reqPick').show();
        }
        //수거완료 : 교환신청(C1), 교환보류(C8) && 수거대기(NN), 수거요청(P0), 요청완료(P1), 수거중(P2), 수거실패(P8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C2' || claimStatusCode == 'C8') && (pickStatus == 'N0' || pickStatus == 'NN' || pickStatus == 'P0' || pickStatus == 'P1' || pickStatus == 'P2' || pickStatus == 'P8')){
            $('#completePick').show();
        }
        //교환배송 : 교환신청(C1), 교환보류(C8)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && !marketType){
            $('#reqApprove').show();
        }
        //교환보류 : 교환신청(C1) && 수거대기(NN), 요청완료(P1), 수거중(P2), 수거완료(P3), 수거실패(P8)
        if(claimStatusCode == 'C1' && (pickStatus == 'N0' || pickStatus == 'NN' || pickStatus == 'P0' || pickStatus == 'P1' || pickStatus == 'P2' || pickStatus == 'P3'|| pickStatus == 'P8')){
            $('#reqPending').show();
        }
        //교환철회 : 교환신청(C1), 교환보류(C8) && 수거대기(NN,N0), 자동접수요청(P0), 수거실패(P8) && 상품출고대기(D0)
        if((claimStatusCode == 'C1' || claimStatusCode == 'C8') && (pickStatus == 'N0' || pickStatus == 'NN' || pickStatus == 'P0' ||  pickStatus == 'P8') && exchStatus == 'D0'){
            $('#reqWithDraw').show();
        }
        //교환거부 : 교환신청(C1), 교환보류(C8)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
            $('#reqReject').show();
        }
        //교환완료 : 교환신청(C1) && 수거완료(P3)
        if(claimStatusCode == 'C1' || claimStatusCode == 'C8'){
            $('#reqComplete').show();
        }
        // 보류/거절 사유 표기
        if(claimStatusCode == 'C8'){//보류 사유
            $('#pendingReason').show();
        }else if(claimStatusCode == 'C9'){//거절 사유
            $('#rejectReason').show();
        }
    }
};

/**
 * 수거/배송지 변경 [저장]시 호출되는 함수
 * **/
function setAddrCallBack(res){
    //reqType 1:수거지 2:배송지
    var reqTypeName = res.reqType  == '1' ? 'pick' : 'receive';
    $('#'+ reqTypeName  + 'Nm').text(res.name);
    $('#'+ reqTypeName  + 'Mobile').text(res.phone);
    $('#'+ reqTypeName  + 'Zipcode').text('(' + res.zipCode + ')');
    $('#'+ reqTypeName  + 'Addr').text(res.addr1);
    $('#'+ reqTypeName  + 'AddrDetail').text(res.addr2);
}



