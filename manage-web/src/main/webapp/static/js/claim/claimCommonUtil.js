/**
 * 주문관리 > 클레임관리
 */

var claimCommonUtil = {

    /**
     * 초기화
     */
    init : function() {

      claimDetailInfoListGrid.init();
      claimProcessGrid.init();
      claimHistoryGrid.init();
      claimCommonUtil.bindingEvent();
      claimCommonUtil.claimDetailInfo();
      claimCommonUtil.claimProcessList();
      claimCommonUtil.claimHistoryList();
      claimCommonUtil.claimDetailInfoList();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
      $('#closeBtn').bindClick(function() { UIUtil.RemoveTabWindow('claimDetailTab')});

      $("#purchaseOrderNo").on("click", function () {
          var agent = navigator.userAgent.toLowerCase();
          if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
              window.open('/order/orderManageDetail?purchaseOrderNo=' + $('#purchaseOrderNo').text())
          }else{
              UIUtil.addTabWindow("/order/orderManageDetail?purchaseOrderNo=" + $('#purchaseOrderNo').text(),'orderDetail', '주문정보상세', '135px')
          }
      });

      $('#uploadFileName').on('click', function() {
          $.jUtil.downloadFile($('#uploadFileNameUrl').val(), '첨부파일1.'+$('#uploadFileNameType').val(), 'ClaimAddImage');
      });

      $('#uploadFileName2').on('click', function() {
          $.jUtil.downloadFile($('#uploadFileNameUrl2').val(), '첨부파일2.'+$('#uploadFileNameType2').val(), 'ClaimAddImage');
      });

      $('#uploadFileName3').on('click', function() {
          $.jUtil.downloadFile($('#uploadFileNameUrl3').val(), '첨부파일3.'+$('#uploadFileNameType3').val(), 'ClaimAddImage');
      });
    },

    /**
     * 클레임 정보 조회
     * **/
    claimDetailInfo : function(){
      CommonAjax.basic(
              {
                  url:'/claim/info/getClaimMainDetailInfo.json',
                  data: JSON.stringify({
                    "purchaseOrderNo" : purchaseOrderNo,
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimCommonUtil.writeToSpan("claimDetailinfoTable", res);
                      //전체취소인 경우 클레임번들번호를 노출시키지 않는다.
                      if(res.claimPartYn == 'N' && claimType == 'C'){
                          $('#claimBundleNo,#claimBundleNoText').hide()
                      }
                  }
              });
    },

    /**
     * 클레임 처리내역 리스트
     * **/
    claimProcessList : function(){
      CommonAjax.basic(
              {
                  url:'/claim/info/getClaimProcessList.json?claimNo=' + claimNo + '&claimType=' + claimType,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimProcessGrid.setData(res);
                  }
              });
    },

    /**
     * 클레임 히스토리 리스트 조회
     * **/
    claimHistoryList : function(){
      CommonAjax.basic(
              {
                  url:'/claim/info/getClaimHistoryList.json?claimNo=' + claimNo,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimHistoryGrid.setData(res);
                  }
              });
    },

    /**
     * 클레임 주문정보 리스트 조회
     * **/
    claimDetailInfoList : function(){
      CommonAjax.basic(
              {
                  url:'/claim/info/getClaimDetailInfoList.json',
                  data: JSON.stringify({
                    "purchaseOrderNo" : purchaseOrderNo,
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimDetailInfoListGrid.setData(res);
                  }
              });
    },

    /**
     * 등록된 환불예정금액 조회
     * **/
    registerPreRefund : function (){
        CommonAjax.basic(
              {
                  url:'/claim/info/getRegisterPreRefund.json?claimNo='+claimNo,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimCommonUtil.writeToSpan("refundMainTable", res);

                      //상품/행사 할인 금액 구하기 [상품할인금액 + 행사할인금액]
                      $('#itemPromoDiscountAmt').text($.jUtil.comma(res.itemDiscountAmt + res.promoDiscountAmt));

                      //대체주문금액 row 노출 여부, 대체주문금액이 추가됐을 경우만 노출
                      if(res.substitutionAmt > 0){
                          $('#discountAmt').text(res.substitutionAmt + res.discountAmt);
                          $('#refundAmt').text(res.refundAmt - res.substitutionAmt);
                          $('#pgAmt,#mhcAmt,#dgvAmt,#mileageAmt,#ocbAmt').text(0);
                          $('#substitutionAmtRow').show();
                      }

                      //추가도서산간배송비 row 노출 여부, 추가도서산간배송비가 추가됐을 경우만 노출
                      if(res.addIslandShipPrice > 0){
                          $('#addIslandShipPriceRow').show();
                      }

                      //임직원할인금액 row 노출 여부, 임직원할인금액이 0원 이상인 경우만 노출
                      if(res.empDiscountAmt > 0){
                          $('#empDiscountAmtRow').show();
                      }

                  }
              });
    },

    /**
     * 클레임 사유 적용
     * **/
    setClaimReasonType : function(res){
        //신청 사유/상세
        $('#claimReasonType').text(res.claimReasonType);
        $('#claimReasonDetail').text(res.claimReasonDetail);

        //보류 사유/상세
        $('#pendingReasonType').text(res.pendingReasonType);
        $('#pendingReasonDetail').text(res.pendingReasonDetail);

        //거부 사유/상세
        $('#rejectReasonType').text(res.rejectReasonType);
        $('#rejectReasonDetail').text(res.rejectReasonDetail);

        $('#whoReason').text(res.whoReason)

        if(res.whoReason == '구매자 책임'){
            $('#claimShipFeeEnclose').text(" | " + res.claimShipFeeEnclose);
        }
    },

    /**
     * 미차감 여부 적용
     * **/
    setDeductInfo : function(res){

        if(res.deductPromoYn == 'Y'){
            $('#deductPromoYn').text('행사유지');
            $('#deductPromoYn').show();
        }

        if(res.deductDiscountYn == 'Y'){
            $('#deductDiscountYn').show();
            if(res.deductPromoYn == 'Y'){
                $('#deductDiscountYn').text(' / 할인유지');
            }else{
                $('#deductDiscountYn').text('할인유지');
            }
        }

        if(res.deductShipYn == 'Y'){
            $('#deductShipYn').show();
            if(res.deductPromoYn == 'Y' || res.deductDiscountYn == 'Y'){
                $('#deductShipYn').text(' / 배송비 미차감');
            }else{
                $('#deductShipYn').text('배송비 미차감');
            }
        }
    },

    /**
     * 반품배송비 text 적용
     * 반송 배송비 or 추가 도서산간 배송비가 0원 이상일 경우 text 문구 추가
     * **/
    setReturnShipAmt : function(res){
        var returnShipAmtText = '';//반품배송비 text

        if(res.totAddShipPrice > 0 || res.totAddIslandShipPrice > 0 ){
            returnShipAmtText = '(';
            //추가배송비 적용
            if (res.totAddShipPrice > 0) {
                returnShipAmtText += '추가 배송비 ' + $.jUtil.comma(res.totAddShipPrice) + '원'
            }

            //추가 도서산간 배송비 적용
            if (res.totAddIslandShipPrice > 0) {
                (res.totAddShipPrice > 0 ? returnShipAmtText += ' + '
                    : returnShipAmtText += '');
                returnShipAmtText += '추가 반송도서산간 배송비 ' + $.jUtil.comma(res.totAddIslandShipPrice) + '원'
            }
            returnShipAmtText += ')';
        }
        $('#returnShipAmt').text($.jUtil.comma(parseInt(res.totAddShipPrice) + parseInt(res.totAddIslandShipPrice)) + '원');//반품배송비 적용
        $('#returnShipAmtText').text(returnShipAmtText);//반품배송비 text 적용

    },

    /**
     * 첨부파일 적용
     * uploadFileName 값이 있을경우
     * 버튼 활성화/unerline/link 적용
     * **/
    setUploadFile : function(res){
        if(!$.jUtil.isEmpty(res.uploadFileName)){
            $('#uploadFileName').css("text-decoration", "underline");
            $("#uploadFileNameUrl").val(res.uploadFileName);
            claimCommonUtil.getUploadContentType($('#uploadFileNameUrl').val(), '');
        }else{
            $('#uploadFileName').hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName2)){
            $('#uploadFileName2').css("text-decoration", "underline");
            $("#uploadFileNameUrl2").val(res.uploadFileName2);
            claimCommonUtil.getUploadContentType($('#uploadFileNameUrl2').val(), '2');

        }else{
            $("#uploadFileName2").hide();
        }

        if(!$.jUtil.isEmpty(res.uploadFileName3)){
            $('#uploadFileName3').css("text-decoration", "underline");
            $("#uploadFileNameUrl3").val(res.uploadFileName3);
            claimCommonUtil.getUploadContentType($('#uploadFileNameUrl3').val(), '3');

        }else{
            $("#uploadFileName3").hide();
        }

        if($.jUtil.isEmpty(res.uploadFileName) && $.jUtil.isEmpty(res.uploadFileName2) && $.jUtil.isEmpty(res.uploadFileName3)){
            $('#uploadRow').hide();
        }

    },

    /**
     * 첨부파일 확장자 조회
     * image/[확장자]
     * **/
    getUploadContentType : function(fileId, seq){
        var dataParam = {
            'fileId'      :   fileId
        };
        var contentType;

        //파라미터 세팅
        //취소요청
        CommonAjax.basic(
            {
                url: '/claim/info/getClaimImageInfo.json?'.concat(jQuery.param(dataParam)),
                method: "GET",
                contentType: 'application/json?',
                successMsg: null,
                callbackFunc: function (res) {
                    $("#uploadFileNameType" + seq).val(res.contentType.replace('image/',''));
                }
            });
    },

    /**
     * 환불수단 적용
     * **/
    setRefundType : function(res){
        var failFlag = false;
        var phonePayFlag = false;//휴대폰 결제 유무

        for(var i=0;i<res.refundTypeList.length;i++){
            $('#paymentType').append(res.refundTypeList[i].paymentType);

            //원결제 수단 여부 확인
            if(res.refundTypeList[i].originClaimPaymentNo == '0'){

                if(res.refundTypeList[i].paymentTypeCode == 'CARD'){
                    $('#paymentType').append(' / ' + res.refundTypeList[i].methodNm + '(원결제 수단)');

                }else{
                    $('#paymentType').append('(원결제 수단)');
                }
            }
            //환불수단 표시
            if(!$.jUtil.isEmpty(res.refundTypeList[i].refundType)){
                $('#paymentType').append(' | ' + res.refundTypeList[i].refundType + ' | ' + res.refundTypeList[i].refundStatusName + '<br>');
            }
            //환불실패여부 확인 P4:환불 실패
           if(res.refundTypeList[i].refundStatus == 'P4'){
                failFlag = true;
            };

            //휴대폰 결제여부 확인
            if(res.refundTypeList[i].paymentTypeCode != 'PHONE'){
                phonePayFlag = true;
            };

        }
        //환불실패된 경우 문구 노출
        if(failFlag){
            $('#refundFailReason').show();
            $('#refundFailReason').text('※환불처리가 실패하였습니다. 환불수단을 변경해주세요');
            $('#refundFailReason').append('<br>');
        }

    },

    /**
     * 수거방법 텍스트 설정
     * **/
    setPickShipText : function(res) {

        var pickShipText = '';

        var pickDlv = $.jUtil.isEmpty(res.claimPickShipping.pickDlv) ? '-' : res.claimPickShipping.pickDlv;
        var pickRegisterType = $.jUtil.isEmpty(res.claimPickShipping.pickRegisterType) ? '-' : res.claimPickShipping.pickRegisterType;
        var pickInvoiceNo = $.jUtil.isEmpty(res.claimPickShipping.pickInvoiceNo) ? '-' : res.claimPickShipping.pickInvoiceNo;
        var shipDt = $.jUtil.isEmpty(res.claimPickShipping.shipDt) ? '-' : res.claimPickShipping.shipDt;
        var pickMemo = $.jUtil.isEmpty(res.claimPickShipping.pickMemo) ? '-' : res.claimPickShipping.pickMemo;
        var storeNm = $.jUtil.isEmpty(res.claimPickShipping.storeNm) ? '-' : res.claimPickShipping.storeNm;

        if (res.claimPickShipping.shipType == 'DS') {
            switch (res.claimPickShipping.pickShipTypeCode) {
                //택배배송
                case 'C' :
                    pickShipText = "택배수거 | " + pickRegisterType + " | " + pickDlv + " | " + pickInvoiceNo;
                    $('#pickDlv').val(res.claimPickShipping.pickDlvCd);
                    $('#pickInvoiceNo').val(pickInvoiceNo);
                    $('#shipSearchBtn').show();
                    break;
                //직배송
                case 'D' :
                    pickShipText = "직접수거 | 수거예정일 : " + shipDt;
                    break;
                //직배송
                case 'P' :
                    pickShipText = "우편 | " + pickMemo;
                    break;
                //배송없음
                case 'N' :
                    pickShipText = '수거안함';
                    break;
            }
        } else if (res.claimPickShipping.shipType == 'PICK'){
            if(res.claimPickShipping.pickShipTypeCode == 'N'){
                pickShipText = '수거안함';
            }else{
                pickShipText = '점포방문 | ' + storeNm;
            }
            $('#pickAddrRow1,#pickAddrRow2').hide();
        }else{
            if('TD_DRCT'.indexOf(res.claimPickShipping.shipMethod) > -1) {
                if(res.claimPickShipping.pickShipTypeCode == 'N'){
                    pickShipText = '수거안함';
                }else if(res.claimPickShipping.pickShipTypeCode == 'T'){
                    pickShipText = '고객센터 직접 반품';
                }else{
                    var slotShipDateTime = $.jUtil.isEmpty(res.claimPickShipping.slotShipDateTime) ? '-' : res.claimPickShipping.slotShipDateTime
                    pickShipText = "자차수거 | 수거예정일 : " + shipDt.substring(0,10) + " " + slotShipDateTime;
                }
            }else if(res.claimPickShipping.shipMethod == 'TD_QUICK'){
                if(res.claimPickShipping.pickShipTypeCode == 'N'){
                    pickShipText = '수거안함';
                }else{
                    pickShipText = '퀵수거';
                }
            }else{
                switch (res.claimPickShipping.pickShipTypeCode) {
                //택배배송
                case 'C' :
                    pickShipText = "택배수거 | " + pickRegisterType +" | " + pickDlv +  " | " + pickInvoiceNo;
                    $('#pickDlv').val(res.claimPickShipping.pickDlvCd);
                    $('#pickInvoiceNo').val(pickInvoiceNo);
                    $('#shipSearchBtn').show();
                    break;
                //직배송
                case 'D' :
                    pickShipText = "직접수거 | 수거예정일 : " + shipDt;
                    break;
                case 'P' :
                    pickShipText = "우편 | " + pickMemo;
                    break;
                //수거안함
                case 'N' :
                    pickShipText = '수거안함';
                    break;
                }
            }
        }

        //수거 실패인 경우 수거방법 영역에 [수거실패] 표기
        //자동수거인 경우 [수거요청완료] 표기
        if(res.claimPickShipping.pickStatusCode == 'P8' || res.claimPickShipping.pickStatusCode == 'P0'){
            $('#pickStatusText').text(res.claimPickShipping.pickStatus);
        }

        //NN,N0 경우 [수거대기] 표기
        if(res.claimPickShipping.pickStatusCode == 'NN' || res.claimPickShipping.pickStatusCode == 'N0'){
            $('#pickStatusText').text('수거대기');
            $('#shipSearchBtn').hide();
        }

        $('#pickShipText').text(pickShipText);
    },

    /**
     * 배송방법 텍스트 설정
     * **/
    setExchShipText : function(res) {

        var exchShipText = '';

        var exchDlv = $.jUtil.isEmpty(res.claimExchShipping.exchDlv) ? '-' : res.claimExchShipping.exchDlv;
        var pickRegisterType = $.jUtil.isEmpty(res.claimPickShipping.pickRegisterType) ? '-' : res.claimPickShipping.pickRegisterType;
        var exchInvoiceNo = $.jUtil.isEmpty(res.claimExchShipping.exchInvoiceNo) ? '-' : res.claimExchShipping.exchInvoiceNo;
        var shipDt = $.jUtil.isEmpty(res.claimExchShipping.exchD0Dt) ? '-' : res.claimExchShipping.exchD0Dt;
        var exchMemo = $.jUtil.isEmpty(res.claimExchShipping.exchMemo) ? '-' : res.claimExchShipping.exchMemo;
        var storeNm = $.jUtil.isEmpty(res.claimPickShipping.storeNm) ? '-' : res.claimPickShipping.storeNm;

        if (res.claimPickShipping.shipType == 'PICK'){
            if(res.claimExchShipping.exchShipTypeCode == 'N'){
                exchShipText = '배송없음';
            }else{
                exchShipText = '점포방문 | ' + storeNm;
            }
            $('#pickAddrRow1,#pickAddrRow2,#exchAddrRow1,#exchAddrRow2').hide();
        }else{
            if('TD_DRCT'.indexOf(res.claimPickShipping.shipMethod) > -1) {
                if(res.claimExchShipping.exchShipTypeCode == 'N'){
                    exchShipText = '배송없음';
                }else{
                    exchShipText = "자차배송 | 배송예정일 : " + shipDt.substring(0,10);
                }
            }else if(res.claimPickShipping.shipMethod == 'TD_QUICK'){
                if(res.claimExchShipping.exchShipTypeCode == 'N'){
                    exchShipText = '배송없음';
                }else{
                    exchShipText = "퀵";
                }
            }else{
                switch (res.claimExchShipping.exchShipTypeCode) {
                //택배배송
                case 'C' :
                    exchShipText = "택배 | " +pickRegisterType + " | "  + exchDlv +" | "+ exchInvoiceNo;
                    $('#exchDlv').val(res.claimExchShipping.exchDlvCd);
                    $('#exchInvoiceNo').val(exchInvoiceNo);
                    $('#exchShipSearchBtn').show();
                    break;
                //직배송
                case 'D' :
                    exchShipText = "직접배송 | 배송예정일 : " + shipDt.substring(0,10);
                    break;
                case 'P' :
                    exchShipText = "우편배송 | " + exchMemo;
                    break;
                //수거안함
                case 'N' :
                    exchShipText = '배송없음';
                    break;
                }
            }
        }

        //배송 실패인 경우 수거방법 영역에 [배송실패] 표기
        if(res.claimExchShipping.exchStatusCode == 'D4'){
            $('#exchStatusText').text(res.claimExchShipping.exchStatus);
        }

        //NN,N0 경우 [수거대기] 표기
        if(res.claimExchShipping.exchStatusCode == 'D0'){
            $('#exchStatusText').text('상품출고대기');
            $('#exchShipSearchBtn').hide();
        }


        $('#exchShipText').text(exchShipText);
    },
    /**
     * 환불/교환 수거지 정보 변경
     * @param reqType 1:수거, 2:배송
     * **/
    setAddr: function (reqType) {
        if(reqType == 1){
            windowPopupOpen("/order/claim/popup/pop/deliveryInfoPop?reqType=" + reqType + "&name=" + $('#buyerNm').text() + "&phone=" + $('#buyerMobileNo').text()
                + "&zipCode" + $('#pickZipcode').text() + "&addr=" + $('#pickAddr').text() + "&addrDetail=" + $('#pickAddrDetail').text()
            , "deliverySearchPop", "610", "340", "yes", "no");
        }else if(reqType == 2){
            windowPopupOpen("/order/claim/popup/pop/deliveryInfoPop?reqType=" + reqType + "&name=" + $('#name').text() + "&phone=" + $('#phone').text()
                + "&zipCode" + $('#exchZipcode').text() + "&addr=" + $('#exchAddr').text() + "&addrDetail=" + $('#exchAddrDetail').text()
            , "deliverySearchPop", "610", "340", "yes", "no");
        }
    },

    /**
     * 환불수단 변경 팝업창 호출
     * **/
    chgRefundType : function(){
      windowPopupOpen('/claim/popup/getChangePaymentTypePop?claimNo=' + $('#claimNo').text()
        + "&claimBundleNo=" + $('#claimBundleNo').text() +"&buyerNm=" + $('#buyerNm').text(),
        'chgRefundTypePop', '800', '310', 'yes', 'no')
    },

    /**
     * 수거/배송 배송방법변경
     * @param reqType 1:수거, 2:배송
     * **/
    chgDeliveryType : function(reqType){
        windowPopupOpen('/claim/popup/getChangeDeliveryTypePop?claimBundleNo=' + $('#claimBundleNo').text()
        + "&claimNo=" + $('#claimNo').text() +"&claimType=" + claimType +"&reqType=" + reqType,
        'getChangeDeliveryTypePop', '1100', '550', 'yes', 'no')
    },


    /**
     * 승인/보류/철회/거부 팝업창 호출
     * @param claimType C:취소, R:반품, X:교환
     * @param processType Approve : 승인, Pending : 보류, Reject : 거부, Withdraw : 철회
     * **/
    claimPopOpen : function(claimType, processType){
        windowPopupOpen('/claim/popup/getClaimProcessReqPop?claimNo=' + $('#claimNo').text()
        + "&claimBundleNo=" + $('#claimBundleNo').text() +"&claimType="+claimType+"&processType="+processType+"&claimPickShippingNo=" + $('#claimPickShippingNo').text(),
        'claimDetailTab', '800', '310', 'yes', 'no')
    },

    /**
     * 수거 요청 팝업창 호출
     * @param claimType C:취소, R:반품, X:교환
     * @param processType Approve : 승인, Pending : 보류, Reject : 거부, Withdraw : 철회
     * **/
    pickReqPopOpen : function(pickType){
        windowPopupOpen('/claim/popup/getPickProcessReqPop?claimPickShippingNo=' + $('#claimPickShippingNo').text()
        + "&claimBundleNo=" + $('#claimBundleNo').text() + "&claimNo="+ $('#claimNo').text() + "&pickType="+pickType,
        'pickRequestPop', '1100', '600','yes', 'no')
    },

    /**
     * 수거 완료 팝업창 호출
     * @param claimType C:취소, R:반품, X:교환
     * @param processType Approve : 승인, Pending : 보류, Reject : 거부, Withdraw : 철회
     * **/
    pickPopOpen : function(pickType){
        windowPopupOpen('/claim/popup/getPickProcessReqPop?claimPickShippingNo=' + $('#claimPickShippingNo').text()
        + "&claimBundleNo=" + $('#claimBundleNo').text() + "&claimNo="+ $('#claimNo').text() + "&pickType="+pickType,
        'pickRequestPop', '800', '310','yes', 'no')
    },

    /**
     * 교환배송 팝업창 호출
     * **/
    exchShippingPop : function(){
        windowPopupOpen('/claim/popup/exchShippingPop?claimBundleNo=' + $('#claimBundleNo').text() + "&claimNo="+ $('#claimNo').text(),
        'exchShippingPop', '1100', '550', 'yes', 'no')
    },

    /**
     * 교환완료 팝업창 호출
     * **/
    exchCompletePop : function(){
        windowPopupOpen('/claim/popup/exchCompletePop?claimExchShippingNo=' + $('#claimExchShippingNo').text()
        + "&claimBundleNo=" + $('#claimBundleNo').text() + "&claimNo="+ $('#claimNo').text(),
        'exchCompletePop', '800', '310', 'yes', 'no')
    },

    /**
     * 차감 할인정보 팝업창 호출
     * **/
    discountInfoPopOpen : function(){
        windowPopupOpen('/claim/popup/discountInfoPop?claimNo=' + $('#claimNo').text(),
        'discountInfoPop', '1100', '450', 'yes', 'no')
    },

    /**
     * 차감 행사정보 팝업창 호출
     * **/
    promoInfoPopOpen : function(){
        windowPopupOpen('/claim/popup/promoInfoPop?claimNo=' + $('#claimNo').text(),
        'promoInfoPop', '1100', '400', 'yes', 'no')
    },

    /**
     * 취소 사은행사 팝업창 호출
     * **/
    claimGiftInfoPop : function(){
        windowPopupOpen('/claim/popup/claimGiftInfoPop?claimNo=' + $('#claimNo').text(),
        'claimGiftInfoPop', '1100', '400', 'yes', 'no')
    },

    /**
     * 판매자정보 팝업창 호출
     * **/
    sellerInfo : function(){
        windowPopupOpen('/claim/popup/getSellerInfoPop?purchaseOrderNo=' + purchaseOrderNo + "&claimNo="+ $('#claimNo').text(),
        'sellerInfoPop', '800', '450', 'yes', 'no')
    },
    /**
     * 배송조회 팝업
     * **/
    shipHistoryPop : function(){
        windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + $('#pickDlv').val() + '&invoiceNo=' + $('#pickInvoiceNo').val(), "claimShipHistoryPop", 649, 744, 0,0);

    },
    /**
     * 교환배송조회 팝업
     * **/
    exchShipHistoryPop : function(){
        windowPopupOpen("/ship/popup/getShipHistoryPop?dlvCd=" + $('#exchDlv').val() + '&invoiceNo=' + $('#exchInvoiceNo').val(), "claimExchShipHistoryPop", 649, 744, 0,0);

    },
    writeToSpan : function (_formId, _data) {
        var value = "";
        var spanId = $('#' + _formId).find('span');
        for(var idx = 0; idx < spanId.length; idx++){
            if (!$.jUtil.isEmpty(spanId[idx].id)) {
                value = _data[spanId[idx].id];
                if ($.jUtil.isEmpty(value)) {
                    value = '';
                }
                var checkId = spanId[idx].id.toLowerCase();
                if (checkId.indexOf("amt") > -1 || checkId.indexOf("price")
                    > -1) {
                    if (!isNaN(Number(value))) {
                        value = $.jUtil.comma(Number(value));
                    } else {
                        value = 0;
                    }
                }
                $(spanId[idx]).html(value);
            }
        }
    }

};
// 클레임상품 리스트 그리드
var claimDetailInfoListGrid = {
  gridView : new RealGridJS.GridView("claimDetailInfoListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimDetailInfoListGrid.initGrid();
    claimDetailInfoListGrid.initDataProvider();
    claimDetailInfoListGrid.event();
  },
  initGrid : function() {
    claimDetailInfoListGrid.gridView.setDataSource(claimDetailInfoListGrid.dataProvider);

    claimDetailInfoListGrid.gridView.setStyles(claimDetailInfoListGridBaseInfo.realgrid.styles);
    claimDetailInfoListGrid.gridView.setDisplayOptions(claimDetailInfoListGridBaseInfo.realgrid.displayOptions);
    claimDetailInfoListGrid.gridView.setColumns(claimDetailInfoListGridBaseInfo.realgrid.columns);
    claimDetailInfoListGrid.gridView.setOptions(claimDetailInfoListGridBaseInfo.realgrid.options);

    claimDetailInfoListGrid.gridView.setColumnProperty("claimStatus", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("pickStatus", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("exchStatus", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("refundStatus", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("orderItemNo", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("itemNo", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("claimBundleNo", "mergeRule", {criteria:"value"});
    claimDetailInfoListGrid.gridView.setColumnProperty("bundleNo", "mergeRule", {criteria:"value"});

    claimDetailInfoListGrid.gridView.setColumnProperty("itemNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "itemNo", showUrl: false});


    //클레임 취소일 경우 수거상태 컬럼 hide 시킨다.
    if(claimType == 'C'){
        claimDetailInfoListGrid.gridView.setColumnProperty("pickStatus", "visible", false);
        claimDetailInfoListGrid.gridView.setColumnProperty("exchStatus", "visible", false);
    }

    //클레임 취소일 경우 수거상태 컬럼 hide 시킨다.
    if(claimType == 'R'){
        claimDetailInfoListGrid.gridView.setColumnProperty("exchStatus", "visible", false);
    }

    //클레임 환불일 경우 [환불상태] 비노출
    // 클레임 환불일 경우 [환불상태] 배송상태 노출
    if(claimType == 'X'){
        claimDetailInfoListGrid.gridView.setColumnProperty("refundStatus", "visible", false);
    }


  },
  initDataProvider : function() {
    claimDetailInfoListGrid.dataProvider.setFields(claimDetailInfoListGridBaseInfo.dataProvider.fields);
    claimDetailInfoListGrid.dataProvider.setOptions(claimDetailInfoListGridBaseInfo.dataProvider.options);
  },
  event : function() {

    claimDetailInfoListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
          var itemNo = grid.getValue(index.itemIndex, "itemNo");
          var storeType = grid.getValue(index.itemIndex, "storeType");
          var siteType = grid.getValue(index.itemIndex, "siteType");

          if (index.fieldName == "itemNo") {

              if(siteType == 'CLUB'){
                  window.open(clubUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
              }else{
                  window.open(frontUrl + "/item?itemNo=" + itemNo + "&storeType=" + storeType);
              }

          }
    };
  },
  setData : function(dataList) {
    claimDetailInfoListGrid.dataProvider.clearRows();
    claimDetailInfoListGrid.dataProvider.setRows(dataList);
  },
  getDataList : function(){
      return claimDetailInfoListGrid.dataProvider.getJsonRows(0,-1);
  }
};

// 클레임 처리내역 리스트 그리드
var claimProcessGrid = {
  gridView : new RealGridJS.GridView("claimProcessGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimProcessGrid.initGrid();
    claimProcessGrid.initDataProvider();
    claimProcessGrid.event();
  },

  initGrid : function() {
    claimProcessGrid.gridView.setDataSource(claimProcessGrid.dataProvider);

    claimProcessGrid.gridView.setStyles(claimProcessGridBaseInfo.realgrid.styles);
    claimProcessGrid.gridView.setDisplayOptions(claimProcessGridBaseInfo.realgrid.displayOptions);
    claimProcessGrid.gridView.setColumns(claimProcessGridBaseInfo.realgrid.columns);
    claimProcessGrid.gridView.setOptions(claimProcessGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
    claimProcessGrid.dataProvider.setFields(claimProcessGridBaseInfo.dataProvider.fields);
    claimProcessGrid.dataProvider.setOptions(claimProcessGridBaseInfo.dataProvider.options);
  },
  event : function() {

  },
  setData : function(dataList) {
    claimProcessGrid.dataProvider.clearRows();
    claimProcessGrid.dataProvider.setRows(dataList);
    claimProcessGrid.gridView.orderBy(['processDt'],[RealGridJS.SortDirection.ASCENDING]);
  }
};

// 클레임 처리내역 리스트 그리드
var claimHistoryGrid = {
  gridView : new RealGridJS.GridView("claimHistoryGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),

  init : function() {
    claimHistoryGrid.initGrid();
    claimHistoryGrid.initDataProvider();
    claimHistoryGrid.event();
  },
  initGrid : function() {
    claimHistoryGrid.gridView.setDataSource(claimHistoryGrid.dataProvider);

    claimHistoryGrid.gridView.setStyles(claimHistoryGridBaseInfo.realgrid.styles);
    claimHistoryGrid.gridView.setDisplayOptions({
      showEmptyMessage: true,
      emptyMessage: "히스토리 내역이 없습니다."
    });
    claimHistoryGrid.gridView.setColumns(claimHistoryGridBaseInfo.realgrid.columns);
    claimHistoryGrid.gridView.setOptions(claimHistoryGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
    claimHistoryGrid.dataProvider.setFields(claimHistoryGridBaseInfo.dataProvider.fields);
    claimHistoryGrid.dataProvider.setOptions(claimHistoryGridBaseInfo.dataProvider.options);
  },
  event : function() {
    // 그리드 선택
    claimHistoryGrid.gridView.onDataCellClicked = function(gridView, index) {
      claimCancelDetail.gridRowSelect(index.dataRow);
    };
  },
  setData : function(dataList) {
    claimHistoryGrid.dataProvider.clearRows();
    claimHistoryGrid.dataProvider.setRows(dataList);
  }
};

function getData(){
    return claimDetailInfoListGrid.getDataList();
}