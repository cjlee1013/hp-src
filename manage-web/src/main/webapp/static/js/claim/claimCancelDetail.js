/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  claimCancelDetail.init();
});



// 클레임상세정보
var claimCancelDetail = {

    /**
     * 초기화
     */
    init : function() {
        claimCommonUtil.init();
        claimCancelDetail.cancelReasonInfo();
        claimCommonUtil.registerPreRefund();

    },

    /**
     * 취소신청 사유
     * **/
    cancelReasonInfo : function (){

        CommonAjax.basic(
              {
                  url:'/claim/info/getClaimReasonInfo.json',
                  data: JSON.stringify({
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo,
                    "claimType" : claimType
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      /**
                       * 취소상세 정보 적용
                       * **/
                      claimCommonUtil.writeToSpan("cancelDetailInfoTable", res);
                      /**
                       * 미차감여부 적용
                       * **/
                      claimCommonUtil.setDeductInfo(res);
                      /**
                       * 처리상태 버튼 노출
                       * **/
                      claimCancelDetail.setBtn(res);
                      /**
                       * 환불수단 적용
                       * **/
                      claimCommonUtil.setRefundType(res);
                  }
              });
    },

    /**
     * 처리상태 버튼 노출
     * **/
    setBtn : function(res){
        var claimStatusCode = res.claimStatusCode;
        var marketType = $.jUtil.isNotEmpty(res.marketType);
        //처리상태 버튼 노출
        // 신청(C1), 승인(C2) : 승인,보류,철회,거부 버튼 노출
        // 보류(C8) : 승인,철회,거부 버튼 노출
        // 거절(C9) : 거절 사유 정보 노출
        // 마켓연동일경우
        // 취소 : 취소승인
        // 반품 : 반품승인,반품보류,반품철회,반품거부
        // 교환 : 수거요청,수거완료,교환배송,교환완료,교환철회,교환거부
        if(marketType) {
            if(claimStatusCode == 'C1') {
                $('#reqApprove').show();
                $('#reqWithDraw').show();
            }
        } else {
            if(claimStatusCode == 'C1'){
                $('#reqApprove').show();
                $('#reqPending').show();
                $('#reqWithDraw').show();
                $('#reqReject').show();
            }else if(claimStatusCode == 'C8'){
                $('#reqApprove').show();
                $('#reqWithDraw').show();
                $('#reqReject').show();
                //취소 보류 사유
                $('#pendingReason').show();
            }else if(claimStatusCode == 'C9'){
                //취소 거절 사유
                $('#rejectReason').show();
            }
        }
    }
};



