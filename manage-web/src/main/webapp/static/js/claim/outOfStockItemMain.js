/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  outOfStockItem.init();
  deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "0");
  outOfStockItemListGrid.init();
  CommonAjaxBlockUI.global();
});

// 클레임관리
var outOfStockItem = {
  /**
   * 초기화
   */
  init : function() {
    this.bindingEvent();
    this.setStoreId();
  },
  initSearchDate : function (flag) {
    deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "0");
    outOfStockItem.callBack.searchStoreShift();
  },

  /**
   * binding 이벤트
   */
  bindingEvent: function() {
    //[판매업체] 선택
    $('#schStoreType').bindChange(outOfStockItem.chgStoreType);
    //[대체여부] 선택
    $('#schSubstitutionYn').bindChange(outOfStockItem.chgSubstitutionYn);
    // 점포 조회 버튼
    $("#schStoreBtn").bindClick(outOfStockItem.openStorePopup);
    // [엑셀다운로드] 버튼
    $("#excelDownloadBtn").bindClick(outOfStockItemListGrid.excelDownload);
    // [검색] 버튼
    $("#searchBtn").bindClick(outOfStockItem.search);
    // [초기화] 버튼
    $("#resetBtn").bindClick(outOfStockItem.reset);
    //[단축번호] 숫자만 입력
    $("#schStartSordNo,#schEndSordNo").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));
    //종료일자 변경 시
    $("#schEndDt").change(function() {outOfStockItem.callBack.searchStoreShift()});
  },

  /**
   * 클레임 검색
   */
  search : function() {

    let data = _F.getFormDataByJson($("#searchForm"));

    const schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
    const schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

    if(!schStartDt.isValid() || !schEndDt.isValid()){
        alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
        return false;
    }

    if($('#schSearch').val() != ''){
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("상세 검색 시, 조회기간은 최대 1개월까지 설정 가능합니다.");
            $("#schStartDt").val(
                schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
    }
    //구매자 정보로 검색시, 조회기간 3개월
     else if(!$.jUtil.isEmpty($('#schStoreId').val()) || !$.jUtil.isEmpty($('#schShiftId').val()) || !$.jUtil.isEmpty($('#schRefundStatus').val()) || !$.jUtil.isEmpty($('#schSubStatus').val())){
        if (schEndDt.diff(schStartDt, "week", true) > 1) {
          alert("조회기간은 최대 7일까지 설정 가능합니다.");
          $("#schStartDt").val(schEndDt.add(-1, "week").format("YYYY-MM-DD"));
          return false;
        }
    //판매자 정보로 검색시, 조회기간 1개월
    }else{
        if (schEndDt.diff(schStartDt, "day", true) > 1) {
            alert("검색조건이 입력되지 않아, 조회기간은 최대 1일까지 설정 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "day").format("YYYY-MM-DD"));
            return false;
        }
    }
    if(schEndDt.diff(schStartDt, "day", true) < 0) {
        alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
        $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
        return false;
    }

    if(!$.jUtil.isEmpty($('#schStartSordNo').val())){
        if($.jUtil.isEmpty($('#schEndSordNo').val())){
            alert("조회 종료 단축번호를 입력해주세요");
            return false;
        }
    }
    if(!$.jUtil.isEmpty($('#schEndSordNo').val())){
        if($.jUtil.isEmpty($('#schStartSordNo').val())){
            alert("조회 시작 단축번호를 입력해주세요");
            return false;
        }
    }

    if(!$.jUtil.isEmpty($('#schStartSordNo').val()) && !$.jUtil.isEmpty($('#schEndSordNo').val())){
        if(parseInt($('#schStartSordNo').val()) > parseInt($('#schEndSordNo').val())){
            alert("단축번호 조회 시작번호가 종료번호보다 작을 수 없습니다.");
            return false;
        }
    }

    CommonAjax.basic(
        {
          url:'/claim/outOfStock/getOutOfStockItemList.json',
          data: data,
          contentType: 'application/json',
          method: "POST",
          successMsg: null,
          callbackFunc:function(res) {
            outOfStockItemListGrid.setData(res);
            $('#outOfStockItemTotalCount').html($.jUtil.comma(outOfStockItemListGrid.gridView.getItemCount()));
          }
        });
  },

  /**
   * 검색폼 초기화
   */
  reset : function() {
      $('#schStoreType,#schSubstitutionYn,#schRefundStatus,#schSubStatus').val('ALL');
      $('#schDateType').val('DELIVERY');
      $('#schSearchType').val('PURCHASEORDERNO');
      deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "0");
      $('#schStoreId,#schStoreName,#schSearch').val('')
      $('#schShiftId').find("option").remove();
      $('#schShiftId').append("<option value='ALL'>전체</option>");
  },

  /**
   * 구매자 회원/비회원 선택
   * **/
  chgSubstitutionYn : function(){
    $('#schSubStatus').val('ALL');
    if($('#schSubstitutionYn').val() == 'Y'){
        $('#schSubStatus').hide();
    }else{
        $('#schSubStatus').show();
    }
  },

  /**
   * 판매업체 조회 팝업창
   */
  getPartnerPop : function(callBack) {
    windowPopupOpen("/common/partnerPop?partnerId="
        + '' + "&callback="
        + callBack + "&partnerType="
        + $('#storeType').val()
        , "partnerStatus", "1080", "600", "yes", "no");
  },
  /**
   * 배송유형 선택
   */
  checkShipType : function() {
    let schShipType = $("input[name='schShipType']:checked").val();

    if(schShipType == 'DS'){
      $('#shipTypeTD').attr('disabled', true);
      $('#shipTypeDS').attr('disabled', false);
    }else if(schShipType == 'TD'){
      $('#shipTypeTD').attr('disabled', false);
      $('#shipTypeDS').attr('disabled', true);
    }else{
      $('#shipTypeDS,#shipTypeTD').attr('disabled', false);
    }
  },

  /**
   * 그리드 row 선택
   * @param selectRowId
   */
  gridRowSelect : function(selectRowId) {
    var rowDataJson = outOfStockItemListGrid.dataProvider.getJsonRow(selectRowId);
    UIUtil.addTabWindow('/claim/info/getClaimDetailInfoTab?purchaseOrderNo='
        + rowDataJson.purchaseOrderNo + "&claimNo=" + rowDataJson.claimNo
        + "&claimBundleNo=" + rowDataJson.claimBundleNo + "&claimType="+ rowDataJson.claimTypeCode,
        'claimDetailTab', '클레임정보상세', '135px')
  },

  callbackMemberSearchPop : function(memberData) {
    $("#schUserSearch").val(memberData.userNo);
  },

  chgStoreType : function(){
       $('#schShiftId').find("option").remove();//select box 옵션 제거
       $('#schShiftId').append("<option value='ALL'>전체</option>");

      const storeType = $('#schStoreType').val();
      const schStoreId = $('#schStoreId');
      const schStoreNm = $('#schStoreName');
      schStoreId.val('');
      schStoreNm.val('');
      if(storeType === 'ALL'){
          $('#schStoreBtn').attr('disabled', true);
      }else{
          $('#schStoreBtn').attr('disabled', false);
      }

      if (storeType === 'DS') {
          schStoreId.prop("placeholder","판매자ID");
          schStoreNm.prop("placeholder","판매자명");
      } else {
          schStoreId.prop("placeholder","점포ID");
          schStoreNm.prop("placeholder","점포명");
      }
  },

  /**
   * 점포 / 판매 업체 조회 팝업
   */
  openStorePopup: function() {
      const storeTypeVal = $('#schStoreType').val();
      if ($.jUtil.isEmpty(storeTypeVal)) {
          alert("점포유형을 선택해주세요.");
          return;
      }
      if (storeTypeVal === "DS"){
          windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=outOfStockItem.callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
      }else{
          windowPopupOpen("/common/popup/storePop?callback=outOfStockItem.callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
      }
  },

  /**
   * Store 계정 접속 여부 확인
   */
  setStoreId: function() {
      if (!$.jUtil.isEmpty(storeId)) {
          $("#schStoreType").val(storeType);
          $("#schStoreId").val(storeId);
          $("#schStoreName").val(storeNm);
          $("#schStoreType").prop("disabled",true);
          $("#schStoreBtn").prop("disabled",true);
      }
  }
};

outOfStockItem.callBack = {
    searchPartner : function(res) {
      $('#schStoreId').val(res[0].partnerId);
      $('#schStoreName').val(res[0].partnerNm);
      this.searchStoreShift();
    },
    searchStore : function (res) {
      $('#schStoreId').val(res[0].storeId);
      $('#schStoreName').val(res[0].storeNm);
      this.searchStoreShift();
    },
    searchStoreShift(){

        if($.jUtil.isEmpty($('#schStoreId').val())){
            return false;
        }

        var weekDay = new Date($('#schEndDt').val()).getDay() + 1;

        var param = {
            'schStoreType' : $('#schStoreType').val(),
            'schStoreId' : $('#schStoreId').val(),
            'weekDay' : weekDay
        }
        $('#schShiftId').find("option").remove();
        $('#schShiftId').append("<option value='ALL'>전체</option>");

        CommonAjax.basic(
        {
          url:'/claim/outOfStock/getStoreShiftList.json',
          data: JSON.stringify(param),
          contentType: 'application/json',
          method: "POST",
          successMsg: null,
          callbackFunc:function(res) {
               $.each(res, function (key, value) {
                  $('#schShiftId').append("<option value=" + value + ">" + value + "</option>")
              });
          }
        });
    },

};

// 클레임관리 그리드
var outOfStockItemListGrid = {
  gridView : new RealGridJS.GridView("outOfStockItemListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    outOfStockItemListGrid.initGrid();
    outOfStockItemListGrid.initDataProvider();
    outOfStockItemListGrid.event();
  },
  initGrid : function() {
    outOfStockItemListGrid.gridView.setDataSource(outOfStockItemListGrid.dataProvider);

    outOfStockItemListGrid.gridView.setStyles(outOfStockItemListGridBaseInfo.realgrid.styles);
    outOfStockItemListGrid.gridView.setDisplayOptions(outOfStockItemListGridBaseInfo.realgrid.displayOptions);
    outOfStockItemListGrid.gridView.setColumns(outOfStockItemListGridBaseInfo.realgrid.columns);
    outOfStockItemListGrid.gridView.setOptions(outOfStockItemListGridBaseInfo.realgrid.options);
    outOfStockItemListGrid.gridView.setColumnProperty("purchaseOrderNo", "mergeRule", {criteria:"value"});
    outOfStockItemListGrid.gridView.setColumnProperty("subPurchaseOrderNo", "mergeRule", {criteria:"value"});

    outOfStockItemListGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});
    outOfStockItemListGrid.gridView.setColumnProperty("subPurchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "subPurchaseOrderNo", showUrl: false});
  },
  initDataProvider : function() {
    outOfStockItemListGrid.dataProvider.setFields(outOfStockItemListGridBaseInfo.dataProvider.fields);
    outOfStockItemListGrid.dataProvider.setOptions(outOfStockItemListGridBaseInfo.dataProvider.options);
  },
  event : function() {

      outOfStockItemListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
          var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
          var subPurchaseOrderNo = grid.getValue(index.itemIndex, "subPurchaseOrderNo");

          if (index.fieldName == 'purchaseOrderNo') {
              UIUtil.addTabWindow("/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo,'orderDetail', '주문정보상세', '135px');
          }else if(index.fieldName == 'subPurchaseOrderNo'){
              if(subPurchaseOrderNo != '-'){
                  UIUtil.addTabWindow("/order/orderManageDetail?purchaseOrderNo=" + subPurchaseOrderNo,'orderDetail', '주문정보상세', '135px');
              }
          }
    };

  },
  setData : function(dataList) {
    outOfStockItemListGrid.dataProvider.clearRows();
    outOfStockItemListGrid.dataProvider.setRows(dataList);
    outOfStockItemListGrid.gridView.orderBy(['shipDt','orderDt'],[RealGridJS.SortDirection.DESCENDING]);

  },

  excelDownload: function() {
    if(outOfStockItemListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    var _date = new Date();
    var fileName =  "결품/대체상품 조회_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    outOfStockItemListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};