/**
 * 주문관리 > 클레임관리 > 클레임정보상세 > 차감 할인정보 팝업
 */
$(document).ready(function() {
  sellerInfoPop.init();
});

// 클레임상세정보
var sellerInfoPop = {

    init : function(){

        sellerInfoPop.claimDetailInfoList();
    },

    /**
     * 클레임 주문정보 리스트 조회
     * **/
    claimDetailInfoList : function(){
      CommonAjax.basic(
              {
                  url:'/claim/info/getClaimDetailInfoList.json',
                  data: JSON.stringify({
                    "purchaseOrderNo" : purchaseOrderNo,
                    "claimNo" : claimNo
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {

                      let partnerIdList = [];

                      $.each(res,function(key,value){
                          if(partnerIdList.indexOf(value.partnerId) == -1 ){
                              partnerIdList.push(value.partnerId);
                          }
                      });
                      sellerInfoPop.getPartnerDetail(partnerIdList);
                  }
              });
    },


    getPartnerDetail : function(partnerIdList){

        $.each(partnerIdList,function(key,value){
            let partnerId = value;

            $('#partnerTable').clone().insertBefore('#partnerTable').attr("id", partnerId+"Table");

            CommonAjax.basic(
              {
                  url: "/partner/getSeller.json?partnerId="+partnerId,
                  method: "GET",
                  callbackFunc: function(res) {

                      let mngNm = '';
                      let mngMobile = '';
                      $.each(res.managerList,function(key,value){
                          if(value.mngCd == 'CS'){
                              mngNm = value.mngNm;
                              mngMobile = value.mngMobile;

                          }

                      });

                      $("#" + partnerId + "Table tbody tr").each(function () {
                          $(this).find("span[id='partnerNm']").text(res.partnerNm);
                          $(this).find("span[id='partnerId']").text(res.partnerId);
                          $(this).find("span[id='mngNm']").text(mngNm);
                          $(this).find("span[id='mngMobile']").text(mngMobile);
                          $(this).find("span[name='name']").text(res.name);
                          $(this).find("span[name='addr1']").text(res.addr1);
                          $(this).find("span[name='addr2']").text(res.addr2);
                      });
                  }

              });

        })

        $('#partnerTable').hide();


    }
};

