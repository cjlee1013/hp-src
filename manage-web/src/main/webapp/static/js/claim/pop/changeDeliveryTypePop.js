
/**
 * 주문관리 > 클레임관리 > 클레임 정보 상세 > 수거/배송 방법 변경
 */
$(document).ready(function() {
  chgDeliveryType.init();
  CommonAjaxBlockUI.global();
});

// 수거/배송 방법 변경
var chgDeliveryType = {

    init : function() {
        this.initSearchDate();
        this.bindingEvent();
        chgDeliveryType.chgDeliveryType();
        chgDeliveryType.searchClaimShipping();

      },

      initSearchDate : function () {
          chgDeliveryType.setDate = CalendarTime.datePicker('deliveryDt', '');
      },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [수거/배송방법] 선택
        $('#selectDeliveryType').bindChange(function() { chgDeliveryType.chgDeliveryType('')});

        //[저장]버튼 클릭
        $('#chgDeliveryBtn').bindClick(chgDeliveryType.saveDeliveryType);

        $("input:radio[name='selectDeliveryType']").bindChange(function() { chgDeliveryType.setDeliveryTypeTD('')});

        //연락처 - 숫자만 입력
        $("#mobileNo_1,#mobileNo_2,#mobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#invoice").allowInput("keyup", ["NUM","NOT_SPACE","ENG"], $(this).attr("id"));
    },

    searchClaimShipping : function(){

        CommonAjax.basic(
              {
                  url:'/claim/info/getClaimReasonInfo.json',
                  data: JSON.stringify({
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo,
                    "claimType" : claimType
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {

                      $('#bundleNo').val(res.bundleNo);
                      $('#purchaseOrderNo').val(res.purchaseOrderNo);
                      $('#storeNm').text(res.claimPickShipping.storeNm);

                      if(!$.jUtil.isEmpty(res.claimPickShipping.placeAddrExtnd )){
                          var startTime = res.claimPickShipping.startTime;
                          var endTime = res.claimPickShipping.endTime;
                          $('#placeNm').text("(방문위치 : "+ res.claimPickShipping.placeAddrExtnd + " / " + "이용시간 " + startTime.substring(0,2) + " : " + startTime.substring(2,4) + " ~ " + endTime.substring(0,2) + " : " + endTime.substring(2,4) +")");
                      }

                      shipType = res.claimPickShipping.shipType;
                      shipMethod = res.claimPickShipping.shipMethod;


                      switch (reqType) {
                          case '1' :
                              $('#claimPickShippingNo').val(res.claimPickShipping.claimPickShippingNo);
                              $('#name').val(res.claimPickShipping.pickReceiverNm);
                              $('#mobileNo').val(res.claimPickShipping.pickMobileNo);
                              $('#zipCode').val(res.claimPickShipping.pickZipcode);
                              $('#addr').val(res.claimPickShipping.pickAddr);
                              $('#addrDetail').val(res.claimPickShipping.pickAddrDetail);
                              $('#gibunAddr').val(res.claimPickShipping.pickBaseAddr);


                              $('#selectDlvCd').val(res.claimPickShipping.pickDlvCd);
                              $('#invoice').val(res.claimPickShipping.pickInvoiceNo);
                              $('#deliveryDt').val(res.claimPickShipping.shipDt);
                              $('#pickMemo').val(res.claimPickShipping.pickMemo);
                              $('#selectSlot').val(res.claimPickShipping.shiftId + '_' + res.claimPickShipping.slotId)

                              chgDeliveryType.setPickWay(shipType, res.claimPickShipping.pickShipTypeCode)
                              break;
                          case '2':
                              $('#claimExchShippingNo').val(res.claimExchShipping.claimExchShippingNo);
                              $('#name').val(res.claimExchShipping.exchReceiverNm);
                              $('#mobileNo').val(res.claimExchShipping.exchMobileNo);
                              $('#zipCode').val(res.claimExchShipping.exchZipcode);
                              $('#addr').val(res.claimExchShipping.exchAddr);
                              $('#addrDetail').val(res.claimExchShipping.exchAddrDetail);
                              $('#gibunAddr').val(res.claimExchShipping.exchBaseAddr);


                              $('#selectDlvCd').val(res.claimExchShipping.exchDlvCd);
                              $('#invoice').val(res.claimExchShipping.exchInvoiceNo);
                              $('#deliveryDt').val(res.claimExchShipping.exchD0Dt);
                              $('#pickMemo').val(res.claimExchShipping.exchMemo);

                              $('#noneRadio').hide();
                              chgDeliveryType.setPickWay(shipType, res.claimExchShipping.exchShipTypeCode)
                              break;
                      }
                      $.jUtil.valSplit($("input[name$='mobileNo']" ).val(), '-', 'mobileNo');
                  }
              })
    },

    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * shipType TD,AURORA:TD매장 DS:DS매장
     * **/
    setPickWay : function(shipType, pickShipType){
        switch (shipType) {
            case 'TD' : case 'AURORA' : case 'PICK' ://TD점포
                $('#pickWayTD').show();
                $('#pickWayDS').hide();
                $('#tdWayTitle').show();

                //배송방법 안내 문구 보이기
                if(reqType == '2'){
                    $('#exchInfoText').show();
                }
                //점포방문
                if(shipType == 'PICK'){
                    $('#pickRadioLabel,#pickPlace').show();

                    if(pickShipType == 'D'){
                        $("input:radio[id='pickRadio'][value='"+pickShipType+"']").prop('checked',true);
                    }else if(pickShipType == 'N'){
                        $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                    }
                }else{//자차,퀵 수거

                    if(shipMethod == 'TD_QUICK'){//쿽수거
                        $('#quickRadioLabel').show();
                        $('#slotDiv,#slotChangeArea').hide();

                        if(pickShipType == 'N'){
                            $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }else{
                            $("input:radio[id='quickRadio'][value='D']").prop('checked',true);
                        }
                    }else{//자차수거
                        $('#tdRadioLabel').show();

                        if(pickShipType == 'D'){
                           $("input:radio[id='tdRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }else if(pickShipType == 'N'){
                           $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }

                    }

                }
                chgDeliveryType.setDeliveryTypeTD();
                chgDeliveryType.getChangeableInfo();
                break;
            case 'DS' ://DS점포
                $('#pickWayDS').show();
                $('#pickWayTD').hide();

               chgDeliveryType.chgDeliveryType(pickShipType)
               break;
            default :
                if('TD_DRCT'.indexOf(shipMethod) > -1) {
                    $('#pickWayTD').show();
                    $('#pickWayDS').hide();
                    $('#tdWayTitle').show();
                    $('#tdRadioLabel').show();
                    $("input:radio[id='tdRadio'][value='D']").prop('checked',true);
                    chgDeliveryType.setDeliveryTypeTD();
                    chgDeliveryType.getChangeableInfo();
                }else{
                    $('#pickWayDS').show();
                    $('#pickWayTD').hide();
                    chgDeliveryType.chgDeliveryType(pickShipType)
                }
                break;
        }
    },


    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType 1:수거 2:배송
     * **/
    chgDeliveryType : function(pickShipType){

        if($.jUtil.isEmpty(pickShipType)){
            pickShipType = $('#selectDeliveryType').val();
        }

        switch (pickShipType) {
            case 'C' : //택배배송
                if(reqType ==1){
                    $('#text1').text('· 일반 접수 시, 수거 요청이 자동으로 이루어지지 않습니다. 판매자가 직접 수거요청 해야 합니다.').append('<br>');
                    $('#text2').text('· 자동접수 지원 택배사 : CJ대한통운, 한진택배, 로젠택배, 현대택배, GTX로지스, KG로지스, KGB택배');
                    $('#text3').text('');
                }else{
                    $('#text1').text('· 선택한 교환 건을 배송처리 합니다.').append('<br>');
                    $('#text2').text('· 등록된 배송정보는 고객 마이페이지에서 노출됩니다.');
                    $('#text3').text('');
                }

                $("#parcelDiv,#pickShippingRow").show();
                $("#pickDiv,#postDiv").hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'D' ://자차배송
                if(reqType ==1){
                    $('#text1').text('· 업체에서 상품을 수거할 날짜를 선택하세요.');
                    $('#text2').text('');
                    $('#text3').text('');
                }else{
                    $('#text1').text('· 선택한 교환 건을 배송처리 합니다.').append('<br>');
                    $('#text2').text('· 등록된 배송정보는 고객 마이페이지에서 노출됩니다.').append('<br>');
                    $('#text3').text('· 상품을 받을 날짜를 선택하세요');
                }

                $('#pickDiv,#pickShippingRow').show();
                $('#parcelDiv,#postDiv').hide();
                $("#pickDiv").find("img").show();
                break;
            case 'P' ://우편배송
                $('#text1').text('· 우편발송 선택 시 배송조회가 불가합니다.');
                $('#text2').text('');
                $('#text3').text('');

                $('#postDiv,#pickShippingRow').show();
                $('#parcelDiv,#pickDiv').hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'N' :
                if(reqType ==1){
                    $('#text1').text('· 수거방법을 "수거안함"으로 선택하면, "수거완료" 단계로 즉시 변경됩니다.');
                    $('#text2').text('');
                    $('#text3').text('');
                }else{
                    $('#text1').text('· 선택한 교환 건을 배송처리 합니다.').append('<br>');
                    $('#text2').text('· 등록된 배송정보는 고객 마이페이지에서 노출됩니다.').append('<br>');
                    $('#text3').text('· 교환배송 방법을 "배송없음"으로 선택하면, "교환완료" 단계로 즉시변경됩니다.');
                }

                $("#parcelDiv,#postDiv,#pickDiv,#pickShippingRow").hide();
                $("#pickDiv").find("img").hide();
                break;
        }
        $('#selectDeliveryType').val(pickShipType);
    },

    /**
     * 배송시간 변경 slot
     * **/
    getChangeableInfo : function () {
        var selectSlot = $('#selectSlot').val();

        var data = {
            'bundleNo' : $('#bundleNo').val(),
            'purchaseOrderNo' : $('#purchaseOrderNo').val()
        }
        CommonAjax.basic(
            {
                url:'/order/getShipSlotInfo.json?'.concat(jQuery.param(data)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if(res.returnCode == '0000' || res.returnCode == 'SUCCESS'){
                        $('#slotChangeArea').empty();
                        $('#slotChangeArea').append(orderUtil.writeToSlotTable(res.data));

                        if(!$.jUtil.isEmpty(selectSlot)){
                            $("input:radio[name='changeSlotInfo'][id$='"+selectSlot+"']").prop('checked',true);
                        }
                    }
                }
            });
    },

    setDeliveryTypeTD : function(selectDeliveryType){

        if($.jUtil.isEmpty(selectDeliveryType)){
            selectDeliveryType = $("input:radio[name='selectDeliveryType']:checked").val();
        }

        if (selectDeliveryType == 'D' && shipType != 'PICK') {

            if(shipMethod == 'TD_QUICK'){
                $('#pickShippingRow').show();
            }else{
                $('#slotDiv,#pickShippingRow').show();
            }
        } else {
            $('#slotDiv,#pickShippingRow').hide();
        }
    },
    validation : function(){

        //이름
        if ($.jUtil.isEmpty($('#name').val())) {
            alert('이름을 입력주세요');
            return false;
        }

        if($('#name').val().length < 2) {
            alert('이름을 2자 이상 입력해주세요.');
            return false;
        }

        //연락처
        if ($.jUtil.isEmpty($('#mobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($('#mobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#mobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#mobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }
        //수거지 우편번호
        if ($.jUtil.isEmpty($('#zipCode').val())) {
            alert('수거지 우편번호를 입력주세요');
            return false;
        }
        //수거지 주소
        if ($.jUtil.isEmpty($('#addr').val())) {
            alert('수거지 주소를 입력주세요');
            return false;
        }
        //수거지 상세주소
        if ($.jUtil.isEmpty($('#addrDetail').val())) {
            alert('수거지 상세주소를 입력주세요');
            return false;
        }
        var pickShipType;

        if(shipType == 'DS'){
            pickShipType = $('#selectDeliveryType').val();
            switch (pickShipType) {
            case 'C':
                if($.jUtil.isEmpty($('#selectDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($('#invoice').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' :
                if ($.jUtil.isEmpty($('#deliveryDt').val())) {
                    alert('수거 예정일을 입력해주세요');
                    return false;
                }
                break;
            }
        }else if(shipType === 'TD' || shipType === 'AURORA'){
            pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();

            switch (pickShipType) {
            case 'D' :
                if(shipMethod != 'TD_QUICK') {
                    var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보

                    if ($.jUtil.isEmpty(selectedInfo)) {
                        alert('수거 배송시간을 선택해주세요');
                        return false;
                    }
                    break;
                }
            }
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
                if ($.jUtil.isEmpty(selectedInfo)) {
                    alert('수거 배송시간을 선택해주세요');
                    return false;
                }
            }else{
                pickShipType = $('#selectDeliveryType').val();
                switch (pickShipType) {
                case 'C':
                    if($.jUtil.isEmpty($('#selectDlvCd').val())){
                        alert('택배사를 입력 해 주세요');
                        return false;
                    }else if($.jUtil.isEmpty($('#invoice').val())){
                        alert('송장번호를 입력 해 주세요');
                        return false;
                    };
                    break;
                case 'D' :
                    if ($.jUtil.isEmpty($('#deliveryDt').val())) {
                        alert('수거 예정일을 입력해주세요');
                        return false;
                    }
                    break;
                }
            }

        }
        return true;
    },

    /**
     * 수거/교환 방법변경 저장 버튼클릭
     * **/
    saveDeliveryType : function(){
        var reqTypeName = reqType == 1 ? '수거' : '배송';
        $('#mobileNo').val($.jUtil.valNotSplit('mobileNo', '-'));

        var param;

        //N 인 경우 유효성 검사를 하지 않는다. 배송방법 : 수거안함, 점포방무
        var validSkipVal;

        //수거 안함 선택 시 유효성 검사 skip하기 위해 배송 방법 조회
        if(shipType == 'PICK'){
            validSkipVal = 'N'
        }else if(shipType == 'DS'){
            validSkipVal = $('#selectDeliveryType').val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else if(shipMethod == 'TD_QUICK'){
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                validSkipVal = $('#selectDeliveryType').val();
            }
        }

        //수거 안함 선택 시 유효성 검사 skip
        if(validSkipVal != 'N'){
            if(!chgDeliveryType.validation()){
                return false;
            }
        }
        switch (reqType) {
            case '1' :
                param = chgDeliveryType.getPickShippingParam();
                break;
            case '2' :
                param = chgDeliveryType.getExchShippingParam();
                break;
            default :
                alert('클레임 타입 설정 오류, 진행 할 수 없습니다.')
                return false;
                break;
        }

        if (!confirm(reqTypeName + '정보를 변경하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url         : '/claim/popup/chgClaimShipping.json',
            data        : JSON.stringify(param),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    return false;
                } else {
                    alert(reqTypeName + '정보 변경 성공');
                    opener.location.reload();
                    self.close();
                }
            }
        });
    },

    getPickShippingParam : function(){
        var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
        var pickShipDt;
        var shiftId;
        var slotId;
        var pickShipType;

        if(shipType == 'PICK'){
            pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
        }else if(shipType == 'DS'){
            pickShipType = $('#selectDeliveryType').val();
            pickShipDt = $('#deliveryDt').val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();

                if(pickShipType == 'D'){
                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    pickShipDt = orderUtil.toDateFormatting(idInfo[1]);
                    slotId = idInfo[3];
                    shiftId = idInfo[2];
                }
            }else if(shipMethod == 'TD_QUICK'){
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                pickShipType = $('#selectDeliveryType').val();
                pickShipDt = $('#deliveryDt').val();
            }
        }

        var claimPickShipping = {
            "claimPickShippingNo" : $('#claimPickShippingNo').val(),
            "claimBundleNo" : claimBundleNo,
            "modifyType" : "ALL",
            "pickShipType" : pickShipType,
            "pickDlvCd": $('#selectDlvCd').val(),
            "pickInvoiceNo": $('#invoice').val().replace(' ',''),
            'shipDt' : pickShipDt,
            'slotId' : slotId,
            'shiftId' : shiftId,
            "pickReceiverNm" : $('#name').val(),
            "pickMobileNo" : $('#mobileNo').val(),
            "pickZipcode": $('#zipCode').val(),
            "pickRoadBaseAddr" : $('#addr').val(),
            "pickBaseAddr" : $('#gibunAddr').val(),
            "pickDetailAddr" : $('#addrDetail').val(),
            "pickMemo" : $('#pickMemo').val(),
            'pickStatus' : '',
            'shipType' : shipType
        };


        var param = {
            "claimBundleNo": claimBundleNo,
            "claimNo": claimNo,
            "claimShippingReqType": "P",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimPickShippingModifyDto": claimPickShipping
        };

        return param;
    },


    getExchShippingParam : function(){
        var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
        var pickShipDt;
        var shiftId;
        var slotId;
        var pickShipType;

        if(shipType == 'PICK'){
            pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
        }else if(shipType == 'DS'){
            pickShipType = $('#selectDeliveryType').val();
            pickShipDt = $('#deliveryDt').val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();

                if(pickShipType == 'D'){
                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    pickShipDt = orderUtil.toDateFormatting(idInfo[1]);
                    slotId = idInfo[3];
                    shiftId = idInfo[2];
                }
            }else if(shipMethod == 'TD_QUICK'){
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                pickShipType = $('#selectDeliveryType').val();
                pickShipDt = $('#deliveryDt').val();
            }
        }

        var claimExchShipping = {
            "claimExchShippingNo" : $('#claimExchShippingNo').val(),
            "claimBundleNo" : claimBundleNo,
            "modifyType" : "ALL",
            "exchShipType" : pickShipType,
            "exchDlvCd": $('#selectDlvCd').val(),
            "exchInvoiceNo": $('#invoice').val().replace(' ',''),
            'shipDt' : pickShipDt,
            'slotId' : slotId,
            'shiftId' : shiftId,
            "exchReceiverNm" : $('#name').val(),
            "exchMobileNo" : $('#mobileNo').val(),
            "exchZipcode": $('#zipCode').val(),
            "exchRoadBaseAddr" : $('#addr').val(),
            "exchRoadAddrDetail" : $('#addrDetail').val(),
            "exchBaseAddr" : $('#gibunAddr').val(),
            "exchDetailAddr" : $('#addrDetail').val(),
            "exchMemo" : $('#pickMemo').val(),
            'exchStatus' : '',
            'shipType' : shipType
        };

        var param = {
            "claimBundleNo": claimBundleNo,
            "claimNo": claimNo,
            "claimShippingReqType": "E",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimExchShippingModifyDto": claimExchShipping
        };

        return param;
    },

    setZipcode : function(res){
        $("#zipCode").val(res.zipCode);
        $("#addr").val(res.roadAddr);
        $('#gibunAddr').val(res.gibunAddr);
    },


}