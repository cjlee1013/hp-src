$(document).ready(function() {
    exchComplete.init();
    CommonAjaxBlockUI.global();
});

/**
 * 클레임 완료 요청
 * **/
var exchComplete = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { exchComplete.reqClaimComplete()});
    },

    /**
     * 클레임 완료 요청
     * > 취소/반품/교환 완료 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimComplete : function(){

        if(!confirm('교환완료 하시겠습니까?')){
            return false;
        };

        var param = exchComplete.getExchShippingParam();

        /* 1.교환 배송상태 변경 - 교환배송완료 요청 */
        CommonAjax.basic({
            url         : '/claim/popup/chgClaimShipping.json',
            data        : JSON.stringify(param),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    return false;
                } else {
                    /* 2.교환 상태 변경 - 교환완료 요청 */
                    CommonAjax.basic(
                    {
                        url:'/claim/popup/reqClaimProcess.json',
                        data: _F.getFormDataByJson($("#claimReqForm")),
                        contentType: 'application/json',
                        method: "POST",
                        successMsg: null,
                        callbackFunc: function(res) {
                            if(res.returnCode != '0000'){
                                alert('교환완료 실패 [' + res.returnMessage + ']');
                                return false;
                            }else{
                                alert('교환완료 성공');
                                opener.location.reload();
                                self.close();
                            }
                        }
                    });

                }
            }
        });
    },

    getExchShippingParam : function(){

        var claimExchShipping = {
            "claimExchShippingNo" : claimExchShippingNo,
            "claimBundleNo" : claimBundleNo,
            "modifyType" : "STATUS",
            "exchShipType" : "",
            "exchStatus" : "D3"
        };

        var param = {
            "claimBundleNo": claimBundleNo,
            "claimNo": claimNo,
            "claimShippingReqType": "E",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimExchShippingModifyDto": claimExchShipping
        };
        return param;
    }
}