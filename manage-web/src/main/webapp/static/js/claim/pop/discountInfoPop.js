/**
 * 주문관리 > 클레임관리 > 클레임정보상세 > 차감 할인정보 팝업
 */
$(document).ready(function() {
  discount.init();
  discountInfoListGrid.init();
});

// 클레임상세정보
var discount = {

    init : function(){
        discount.getDiscountList()
    },

    getDiscountList : function(){
        CommonAjax.basic(
              {
                  url:'/claim/popup/getDiscountList.json?claimNo='+claimNo,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      discountInfoListGrid.setData(res);

                      //할인 금액 별 text
                      var discountText = '';
                      //총 할인금액
                      var totalDiscountAmt = 0;

                      var itemCouponAmt = 0;//상품쿠폰 금액 총액
                      var itemDiscountAmt = 0;//상품즉시할인 금액 총액
                      var affiDiscountAmt = 0;//제휴즉시할인 금액 총액
                      var shipCouponAmt = 0;//배송비쿠폰 금액 총액
                      var cartCouponAmt = 0;//장바구니쿠폰 금액 총액
                      var cardDiscountAmt= 0;//카드사즉시할인 금액 총액
                      var empDiscoutAmt = 0;//임직원 금액 총액
                      var etcDiscountAmt= 0;//카드사즉시할인 금액 총액
                      var addCouponAmt= 0;//중복쿠폰 금액 총액

                      //총 할인금액 구하기
                      for(var i=0;i<res.length;i++){
                          totalDiscountAmt += parseInt(res[i].discountAmt);
                      }

                      //할인 별 총금액 구하기
                      for(var i=0;i<res.length;i++){
                          switch (res[i].additionTypeDetail) {
                              case '상품쿠폰' :
                                  itemCouponAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '상품즉시할인' :
                                  itemDiscountAmt += parseInt(res[i].discountAmt);
                                  break
                              case '제휴즉시할인' :
                                  affiDiscountAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '배송비쿠폰' :
                                  shipCouponAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '장바구니쿠폰' :
                                  cartCouponAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '카드사즉시할인' :
                                  cardDiscountAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '임직원 할인' :
                                  empDiscoutAmt += parseInt(res[i].discountAmt);
                                  break;
                              case '중복쿠폰' :
                                  addCouponAmt += parseInt(res[i].discountAmt);
                                  break;
                              default :
                                  etcDiscountAmt += parseInt(res[i].discountAmt);
                                  break;
                          }
                      }


                      //상담 노출을 위한 list 선언
                      var discountList = new Array();

                      //상단 노출 list에 push
                      if(res.length > 0){
                          $('#leftBrc,#rightBrc').show();
                          discountList.push({totDetail : itemCouponAmt > 0 ? '상품쿠폰 : ' + $.jUtil.comma(itemCouponAmt) + '원 ': ''});
                          discountList.push({totDetail : itemDiscountAmt > 0 ? '상품즉시할인 : ' + $.jUtil.comma(itemDiscountAmt) + '원 ': ''});
                          discountList.push({totDetail : affiDiscountAmt > 0 ? '제휴즉시할인 : ' + $.jUtil.comma(affiDiscountAmt) + '원 ': ''});
                          discountList.push({totDetail : shipCouponAmt > 0 ? '배송비쿠폰 : ' + $.jUtil.comma(shipCouponAmt) + '원 ': ''});
                          discountList.push({totDetail : cartCouponAmt > 0 ? '장바구니쿠폰 : ' + $.jUtil.comma(cartCouponAmt) + '원 ': ''});
                          discountList.push({totDetail : cardDiscountAmt > 0 ? '카드사즉시할인 : ' + $.jUtil.comma(cardDiscountAmt) + '원 ': ''});
                          discountList.push({totDetail : empDiscoutAmt > 0 ? '임직원 할인 : ' + $.jUtil.comma(empDiscoutAmt) + '원 ': ''});
                          discountList.push({totDetail : addCouponAmt > 0 ? '중복쿠폰 : ' + $.jUtil.comma(addCouponAmt) + '원 ': ''});
                          discountList.push({totDetail : etcDiscountAmt > 0 ? '기타 : ' + $.jUtil.comma(etcDiscountAmt) + '원 ': ''});
                      }

                      //할인 정보 별 종류/금액 노출
                      for(var i=0;i < discountList.length;i++){
                          //종류 값이 있을 경우 노출
                          if(!$.jUtil.isEmpty(discountList[i].totDetail)) {
                              //첫번째 데이터 여부 확인 : '+' 추가 여부
                              if ($.jUtil.isEmpty(discountText)) {
                                  discountText += discountList[i].totDetail;
                              } else {
                                  discountText += ' + ' + discountList[i].totDetail;
                              }
                          }
                      }
                      $('#totalDiscountAmt').text($.jUtil.comma(totalDiscountAmt) + '원');
                      $('#discountText').text(discountText);
                  }
              });
    }
}

// 클레임 처리내역 리스트 그리드
var discountInfoListGrid = {
  gridView : new RealGridJS.GridView("discountInfoListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    discountInfoListGrid.initGrid();
    discountInfoListGrid.initDataProvider();
  },

  initGrid : function() {
    discountInfoListGrid.gridView.setDataSource(discountInfoListGrid.dataProvider);

    discountInfoListGrid.gridView.setStyles(discountInfoListGridBaseInfo.realgrid.styles);
    discountInfoListGrid.gridView.setDisplayOptions(discountInfoListGridBaseInfo.realgrid.displayOptions);
    discountInfoListGrid.gridView.setColumns(discountInfoListGridBaseInfo.realgrid.columns);
    discountInfoListGrid.gridView.setOptions(discountInfoListGridBaseInfo.realgrid.options);

    discountInfoListGrid.gridView.setDisplayOptions({
      showEmptyMessage: true,
      emptyMessage: "차감 할인정보가 없습니다."
    });
  },
  initDataProvider : function() {
    discountInfoListGrid.dataProvider.setFields(discountInfoListGridBaseInfo.dataProvider.fields);
    discountInfoListGrid.dataProvider.setOptions(discountInfoListGridBaseInfo.dataProvider.options);
  },
  setData : function(dataList) {
    discountInfoListGrid.dataProvider.clearRows();
    discountInfoListGrid.dataProvider.setRows(dataList);
  }
};

