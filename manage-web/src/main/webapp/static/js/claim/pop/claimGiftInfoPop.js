/**
 * 주문관리 > 클레임관리 > 클레임정보상세 > 차감 할인정보 팝업
 */
$(document).ready(function() {
  claimGiftInfo.init();
  claimGiftInfoListGrid.init();
});

// 클레임상세정보
var claimGiftInfo = {

    init : function(){
        claimGiftInfo.getDiscountList()
    },

    getDiscountList : function(){
        CommonAjax.basic(
              {
                  url:'/claim/popup/claimGiftInfoList.json?claimNo='+claimNo,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      claimGiftInfoListGrid.setData(res);
                  }
              });
    }
}

// 클레임 처리내역 리스트 그리드
var claimGiftInfoListGrid = {
  gridView : new RealGridJS.GridView("claimGiftInfoListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimGiftInfoListGrid.initGrid();
    claimGiftInfoListGrid.initDataProvider();
  },

  initGrid : function() {
    claimGiftInfoListGrid.gridView.setDataSource(claimGiftInfoListGrid.dataProvider);

    claimGiftInfoListGrid.gridView.setStyles(claimGiftItemGridBaseInfo.realgrid.styles);
    claimGiftInfoListGrid.gridView.setDisplayOptions(claimGiftItemGridBaseInfo.realgrid.displayOptions);
    claimGiftInfoListGrid.gridView.setColumns(claimGiftItemGridBaseInfo.realgrid.columns);
    claimGiftInfoListGrid.gridView.setOptions(claimGiftItemGridBaseInfo.realgrid.options);

    claimGiftInfoListGrid.gridView.setDisplayOptions({
      showEmptyMessage: true,
      emptyMessage: "취소사은행사가 없습니다."
    });
  },
  initDataProvider : function() {
    claimGiftInfoListGrid.dataProvider.setFields(claimGiftItemGridBaseInfo.dataProvider.fields);
    claimGiftInfoListGrid.dataProvider.setOptions(claimGiftItemGridBaseInfo.dataProvider.options);
  },
  setData : function(dataList) {
    claimGiftInfoListGrid.dataProvider.clearRows();
    claimGiftInfoListGrid.dataProvider.setRows(dataList);
  }
};

