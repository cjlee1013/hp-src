$(document).ready(function() {
    pickRequest.init();
    CommonAjaxBlockUI.global();
});

/**
 * 수거요청
 * **/
var pickRequest = {
    /**
     * 초기화
     */
    init : function() {
      this.initSearchDate();
      this.bindingEvent();
      this.searchClaimShipping();
    },

    initSearchDate : function () {
          pickRequest.setDate = CalendarTime.datePicker('shipDt', '');
      },

    bindingEvent : function(){
        $('#reqBtn').bindClick(function() { pickRequest.reqPickRequest()});

        // [수거/배송방법] 선택
        $('#selectDeliveryType').bindChange(function() { pickRequest.chgDeliveryType('')});

        $("input:radio[name='selectDeliveryType']").bindChange(function() { pickRequest.setDeliveryTypeTD('') });

        //송장번호 - 숫자만 입력
        $("#pickInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE","ENG"], $(this).attr("id"));
    },


    searchClaimShipping : function(){
    CommonAjax.basic(
          {
              url:'/claim/info/getClaimReasonInfo.json',
              data: JSON.stringify({
                "claimNo" : claimNo,
                "claimBundleNo" : claimBundleNo,
                "claimType" : "R"
              }),
              contentType: 'application/json',
              method: "POST",
              successMsg: null,
              callbackFunc: function(res) {

                  $('#bundleNo').val(res.bundleNo);
                  $('#purchaseOrderNo').val(res.purchaseOrderNo);
                  $('#userRequestYn').val(res.userRequestYn);

                  if(!$.jUtil.isEmpty(res.claimPickShipping.placeAddrExtnd )){
                      var startTime = res.claimPickShipping.startTime;
                      var endTime = res.claimPickShipping.endTime;
                      $('#placeNm').text("(방문위치 : "+ res.claimPickShipping.placeAddrExtnd + " / " + "이용시간 " + startTime.substring(0,2) + " : " + startTime.substring(2,4) + " ~ " + endTime.substring(0,2) + " : " + endTime.substring(2,4) +")");
                  }

                  $.each(res.claimPickShipping,function(key, value){
                      $('#'+key).val(value);
                  });

                  $('#selectSlot').val($('#shiftId').val() + '_' + $('#slotId').val())
                  pickRequest.setPickWay(res.claimPickShipping.shipType, res.claimPickShipping.pickShipTypeCode)

                  $.jUtil.valSplit($("input[name$='pickMobileNo']" ).val(), '-', 'pickMobileNo');
              }
          })
    },

    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * shipType TD,AURORA:TD매장 DS:DS매장
     * **/
    setPickWay : function(shipType, pickShipType){
        switch (shipType) {
            case 'TD' : case 'AURORA' : case 'PICK' ://TD점포
                $('#pickWayTD').show();
                $('#pickWayDS').hide();
                $('#tdWayTitle').show();

                //점포방문
                if(shipType == 'PICK'){
                    $('#pickRadioLabel').show();
                    $('#tdRadioLabel').hide();
                    if(pickShipType == 'D'){
                        $("input:radio[id='pickRadio'][value='"+pickShipType+"']").prop('checked',true);
                    }else if(pickShipType == 'N'){
                        $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                    }
                }else{//자차배송
                    if($('#shipMethod').val() === 'TD_QUICK'){//쿽수거
                        $('#quickRadioLabel').show();
                        $('#slotDiv,#slotChangeArea').hide();

                        if(pickShipType == 'D'){
                            $("input:radio[id='quickRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }else if(pickShipType == 'N'){
                            $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }

                    }else{//자차수거
                        $('#tdRadioLabel,#directRadioLabel').show();

                        if(pickShipType == 'D'){
                            $("input:radio[id='tdRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }else if(pickShipType == 'N'){
                            $("input:radio[id='noneRadio'][value='"+pickShipType+"']").prop('checked',true);
                        }

                    }
                }
                pickRequest.setDeliveryTypeTD();
                pickRequest.getChangeableInfo();
                break;
            case 'DS' ://DS점포
                $('#pickWayDS').show();
                $('#pickWayTD').hide();

               pickRequest.chgDeliveryType(pickShipType)
               break;
            default :
                if('TD_DRCT'.indexOf($('#shipMethod').val()) > -1) {
                    $('#pickWayTD').show();
                    $('#pickWayDS').hide();
                    $('#tdWayTitle').show();

                    $('#pickRadioLabel,#pickPlace').hide();
                    $('#tdRadioLabel,#directRadioLabel').show();
                    $("input:radio[id='tdRadio'][value='D']").prop('checked',true);
                    pickRequest.setDeliveryTypeTD();
                    pickRequest.getChangeableInfo();
                }else{
                    $('#pickWayDS').show();
                    $('#pickWayTD').hide();
                    pickRequest.chgDeliveryType(pickShipType)
                }
                break;
        }
    },
    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType 1:수거 2:배송
     * **/
    chgDeliveryType : function(pickShipType){

        if($.jUtil.isEmpty(pickShipType)){
            pickShipType = $('#selectDeliveryType').val();
        }

        switch (pickShipType) {
            case 'C' : //택배배송
                $('#text1').text('· 일반 접수 시, 수거 요청이 자동으로 이루어지지 않습니다. 판매자가 직접 수거요청 해야 합니다.').append('<br>');
                $('#text2').text('· 자동접수 지원 택배사 : CJ대한통운, 한진택배, 로젠택배, 현대택배, GTX로지스, KG로지스, KGB택배');
                $('#text3').text('');


                $("#parcelDiv,#pickShippingRow").show();
                $("#pickDiv,#postDiv").hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'D' ://자차배송

                $('#text1').text('· 업체에서 상품을 수거할 날짜를 선택하세요.');
                $('#text2').text('');
                $('#text3').text('');

                $('#pickDiv,#pickShippingRow').show();
                $('#parcelDiv,#postDiv').hide();
                $("#pickDiv").find("img").show();
                break;
            case 'P' ://우편배송
                $('#text1').text('· 우편발송 선택 시 배송조회가 불가합니다.');
                $('#text2').text('');
                $('#text3').text('');

                $('#postDiv,#pickShippingRow').show();
                $('#parcelDiv,#pickDiv').hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'N' :

                $('#text1').text('· 수거방법을 "수거안함"으로 선택하면, "수거완료" 단계로 즉시 변경됩니다.');
                $('#text2').text('');
                $('#text3').text('');

                $("#parcelDiv,#postDiv,#pickDiv,#pickShippingRow").hide();
                $("#pickDiv").find("img").hide();
                break;
        }
        $('#selectDeliveryType').val(pickShipType);
    },

    /**
     * 배송시간 변경 slot
     * **/
    getChangeableInfo : function () {
        var selectSlot = $('#selectSlot').val();

        var data = {
            'bundleNo' : $('#bundleNo').val(),
            'purchaseOrderNo' : $('#purchaseOrderNo').val()
        }
        CommonAjax.basic(
            {
                url:'/order/getShipSlotInfo.json?'.concat(jQuery.param(data)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if(res.returnCode == '0000' || res.returnCode == 'SUCCESS'){
                        $('#slotChangeArea').empty();
                        $('#slotChangeArea').append(orderUtil.writeToSlotTable(res.data))
                        console.log(selectSlot)

                        $("input:radio[name='changeSlotInfo'][id$='"+selectSlot+"']").prop('checked',true);
                        orderUtil.setWindowAutoResize();
                    }
                }
            });
    },

    setDeliveryTypeTD : function(selectDeliveryType){

        if($.jUtil.isEmpty(selectDeliveryType)){
            selectDeliveryType = $("input:radio[name='selectDeliveryType']:checked").val();
        }

        if (selectDeliveryType == 'D' && $('#shipType').val() != 'PICK') {
            if($('#shipMethod').val() == 'TD_QUICK') {
                $('#pickShippingRow').show();
            }else{
                $('#slotDiv,#pickShippingRow').show();
            }
        } else {
            $('#slotDiv,#pickShippingRow').hide();
        }
    },

    validation : function(){

        //이름
        if ($.jUtil.isEmpty($('#pickReceiverNm').val())) {
            alert('이름을 입력주세요');
            return false;
        }

        if($('#pickReceiverNm').val().length < 2) {
            alert('이름을 2자 이상 입력해주세요.');
            return false;
        }

        //연락처
        if ($.jUtil.isEmpty($('#pickMobileNo').val())) {
            alert('연락처를 입력주세요');
            return false;
        }

        if($('#pickMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#pickMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#pickMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }
        //수거지 우편번호
        if ($.jUtil.isEmpty($('#pickZipcode').val())) {
            alert('수거지 우편번호를 입력주세요');
            return false;
        }
        //수거지 주소
        if ($.jUtil.isEmpty($('#pickAddr').val())) {
            alert('수거지 주소를 입력주세요');
            return false;
        }
        //수거지 상세주소
        if ($.jUtil.isEmpty($('#pickAddrDetail').val())) {
            alert('수거지 상세주소를 입력주세요');
            return false;
        }
        var pickShipType;

        if($('#shipType').val() == 'DS'){
            pickShipType = $('#selectDeliveryType').val();
            switch (pickShipType) {
            case 'C':
                if($.jUtil.isEmpty($('#pickDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($('#pickInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' :
                if ($.jUtil.isEmpty($('#shipDt').val())) {
                    alert('수거 예정일을 입력해주세요');
                    return false;
                }
                break;
            }
        }else if($('#shipType').val() === 'TD' || $('#shipType').val() === 'AURORA'){
            pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();

            switch (pickShipType) {
            case 'D' :
                if($('#shipMethod').val() != 'TD_QUICK') {//쿽수거
                    var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보

                    if ($.jUtil.isEmpty(selectedInfo)) {
                        alert('수거 배송시간을 선택해주세요');
                        return false;
                    }
                }
                break;
            }
        }else{
            if('TD_DRCT'.indexOf($('#shipMethod').val()) > -1) {
                var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
                if ($.jUtil.isEmpty(selectedInfo)) {
                    alert('수거 배송시간을 선택해주세요');
                    return false;
                }
            }else{
                pickShipType = $('#selectDeliveryType').val();
                switch (pickShipType) {
                case 'C':
                    if($.jUtil.isEmpty($('#pickDlvCd').val())){
                        alert('택배사를 입력 해 주세요');
                        return false;
                    }else if($.jUtil.isEmpty($('#pickInvoiceNo').val())){
                        alert('송장번호를 입력 해 주세요');
                        return false;
                    };
                    break;
                case 'D' :
                    if ($.jUtil.isEmpty($('#shipDt').val())) {
                        alert('수거 예정일을 입력해주세요');
                        return false;
                    }
                    break;
                }
            }

        }
        return true;
    },

    /**
     * 수거요청
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqPickRequest: function(){
        //N 인 경우 유효성 검사를 하지 않는다. 배송방법 : 수거안함, 점포방무
        var validSkipVal;

        //수거 안함 선택 시 유효성 검사 skip하기 위해 배송 방법 조회
        if($('#shipType').val() == 'PICK'){
            validSkipVal = 'N'
            $('#pickShipType').val('D');
        }else if($('#shipType').val() == 'DS'){
            validSkipVal = $('#selectDeliveryType').val();
            $('#pickShipType').val($('#selectDeliveryType').val());
        }else{
            if('TD_DRCT'.indexOf($('#shipMethod').val()) > -1) {
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else if($('#shipMethod').val() == 'TD_QUICK'){//쿽수거
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                validSkipVal = $('#selectDeliveryType').val();
            }
            $('#pickShipType').val(validSkipVal);
        }

        //수거 안함 선택 시 유효성 검사 skip
        if(validSkipVal != 'N'){
            if(!pickRequest.validation()){
                return false;
            }
        }
        var param = pickRequest.getPickShippingParam();

        if($('#userRequestYn').val() == 'Y'){
            alert('고객이 직접 수거 요청한 주문입니다.\n고객과 연락하여 수거시간 재확인 후 변경하여주세요.')
        }

        if(!confirm('수거요청 하시겠습니까?')){
            return false;
        };


        CommonAjax.basic(
        {
            url : '/claim/popup/chgClaimShipping.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if('0000,SUCCESS'.indexOf(res.returnCode) < -1) {
                    alert('수거요청 실패 [' + res.returnMessage + ']');
                }else{
                    alert('수거요청 성공');
                    opener.location.reload();
                    window.close();
                }

            }
        });
    },
    getPickShippingParam : function(){
        var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
        var pickShipType;

        if($('#shipType').val() == 'PICK'){
            pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
        }else if($('#shipType').val() == 'DS'){
            pickShipType = $('#selectDeliveryType').val();
        }else{
            if('TD_DRCT'.indexOf($('#shipMethod').val()) > -1) {
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();

                if(pickShipType == 'D'){
                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    $('#shipDt').val(orderUtil.toDateFormatting(idInfo[1]));
                    $('#slotId').val(idInfo[3]);
                    $('#shiftId').val(idInfo[2]);
                }
            }else if($('#shipMethod').val() == 'TD_QUICK'){
                pickShipType = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                pickShipType = $('#selectDeliveryType').val();
            }
        }
        $('#pickShipType').val(pickShipType);

        var claimPickShipping = $('#pickReqForm').serializeObject(true);

        var param = {
            "claimBundleNo": claimBundleNo,
            "claimNo": claimNo,
            "claimShippingReqType": "P",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimPickShippingModifyDto": claimPickShipping
        };

        return param;
    },

    setZipcode : function(res){
        $("#pickZipcode").val(res.zipCode);
        $("#pickAddr").val(res.roadAddr);
        $('#pickBaseAddr').val(res.gibunAddr);
    }
}


