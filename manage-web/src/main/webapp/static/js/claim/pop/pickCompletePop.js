$(document).ready(function() {
    pickComplete.init();
    CommonAjaxBlockUI.global();
});

/**
 * 수거요청
 * **/
var pickComplete = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#reqBtn').bindClick(function() { pickComplete.reqPickComplete()});
    },

    /**
     * 수거요청
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqPickComplete: function(){

        if(!confirm('수거완료 하시겠습니까?')){
            return false;
        };

        CommonAjax.basic(
        {
            url:'/claim/popup/reqChgPickStatus.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if('0000,SUCCESS'.indexOf(res.returnCode) < -1) {
                    alert('수거완료 상태 변경 실패 [' + res.returnMessage + ']');
                }else{
                    alert('수거완료 성공');
                    opener.location.reload();
                    window.close();
                }
            }
        });
    }
}