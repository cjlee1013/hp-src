$(document).ready(function() {
    claimDelivery.init();
});

/**
 * 클레임 상품배송 요청
 * **/
var claimDelivery = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { claimDelivery.reqClaimDelivery()});
    },

    /**
     * 클레임 상품배송 요청
     * > 취소/반품/교환 상품배송 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimDelivery: function(){

        if(!confirm(claimTypeName + '상품 배송을 하시겠습니까?')){
            return false;
        };

        CommonAjax.basic(
        {
            url:'/claim/popup/reqClaimProcess.json',
            data: _F.getFormDataByJson($("#claimReqForm")),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert(claimTypeName + '상품 배송 실패 [' + res.returnMessage + ']');
                    return false;
                }
                alert(claimTypeName + '상품 배송 성공');
                opener.location.reload();
                window.close();
            }
        });
    }
}