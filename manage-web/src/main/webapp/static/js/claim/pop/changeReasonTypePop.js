
/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  chgReasonType.init();
});

// 수거/배송 방법 변경
const chgReasonType = {

    init : function(){
        chgReasonType.claimList()
    },


    /**
     * binding 이벤트
     */
    bindingEvent: function() {

      // [클레임 구분] 선택
      $('#reqBtn').bindClick(chgReasonType.submit());
    },

    /**
     * 클레임 주문정보 리스트 조회
     * 부모창 realgrid 데이터 나타내기
     * **/
    claimList : function(){
        const data = parent.opener.getData()
        let compareItemNo;

        for(let itemInfo of data){

            if(compareItemNo != itemInfo.itemNo){
                const tableRow = $('<tr>');
                tableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text(itemInfo.itemNo));
                tableRow.append($('<td>').attr('style', 'font-weight: bold; font-size: 14px; overflow:hidden;white-space:nowrap;text-overflow:ellipsis;').text(itemInfo.itemName));
                $('#itemInfoTable > tbody:last').append(tableRow);
            }
            const optTableRow = $('<tr>');
            optTableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text(itemInfo.itemNo));
            optTableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 14px').text('\t' + itemInfo.optItemNm == null ? '' : itemInfo.optItemNm));
            optTableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; text-align: center; font-size: 14px').text($.jUtil.comma(itemInfo.claimPrice) + '원'));
            optTableRow.append($('<td>').attr('style', 'border-right: 1px solid #eee; font-size: 14px; text-align:center').text('\t' + itemInfo.claimQty));
            optTableRow.append($('<td hidden>').html(itemInfo.orderOptNo));
            optTableRow.append($('<td hidden>').html(itemInfo.orderType));
            optTableRow.append($('<td hidden>').html(itemInfo.orderItemNo));
            optTableRow.append($('<td>').html(itemInfo.claimQty));
            $('#itemInfoTable > tbody:last').append(optTableRow);

            compareItemNo = itemInfo.itemNo;
        }
    },

    /**
     * 사유변경 요청
     * **/
    submit : function(){
        //사유 상세 사유 확인
        if ($('#claimReasonType').val() == '' || $('#claimReasonType').val() == null) {
            alert('사유를 선택해주세요');
            return false;
        }

        if($('#claimReasonDetail').val() == '' || $('#claimReasonDetail').val() == null){
           alert('사유를 입력해주세요');
           return false;
        }

        if(!confirm('사유를 변경하시겠습니까?')){
            return false;
        };

        CommonAjax.basic(
            {
                 url:'/claim/info/changeClaimReason.json',
                 data: {
                     "claimReqNo" : claimReqNo,
                     "claimReasonType" : $('#claimReasonType').val(),
                     "claimReasonDetail" : $('#claimReasonDetail').val()
                 },
                 contentType: 'application/json',
                 method: "POST",
                 successMsg: null,
                 callbackFunc:function(res) {
                     if(res.returnCode != '0000'){
                         alert('사유변경 실패 [' + res.returnMessage + ']');
                         return false;
                     }
                     alert('사유변경 성공');
                     opener.location.reload();
                     window.close();

                 }
            });
    }
}