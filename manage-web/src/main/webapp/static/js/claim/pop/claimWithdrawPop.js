$(document).ready(function() {
    claimWithdraw.init();
    CommonAjaxBlockUI.global();
});

/**
 * 클레임 철회 요청
 * **/
var claimWithdraw = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { claimWithdraw.reqClaimWithdraw()});
    },

    /**
     * 클레임 철회 요청
     * > 취소/반품/교환 철회 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimWithdraw: function(){

        if(!confirm(claimTypeName + '철회 하시겠습니까?')){
            return false;
        };

        CommonAjax.basic(
        {
            url:'/claim/popup/reqClaimProcess.json',
            data: _F.getFormDataByJson($("#claimReqForm")),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert(claimTypeName + '철회 실패 [' + res.returnMessage + ']');
                }
                alert(claimTypeName + '철회 성공');
                opener.location.reload();
                window.close();
            }
        });
    }
}