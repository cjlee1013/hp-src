
/**
 * 주문관리 > 클레임관리 > 클레임 정보 상세 > 수거/배송 방법 변경
 */
$(document).ready(function() {
  exchShipping.init();
  CommonAjaxBlockUI.global();
});

// 수거/배송 방법 변경
var exchShipping = {

    init : function() {
        this.initSearchDate();
        this.bindingEvent();
        exchShipping.exchShipping();
        exchShipping.searchClaimShipping();

      },

      initSearchDate : function () {
          exchShipping.setDate = CalendarTime.datePicker('exchD0Dt', '');
      },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [수거/배송방법] 선택
        $('#exchShipType').bindChange(function() { exchShipping.exchShipping('')});

        //[저장]버튼 클릭
        $('#chgDeliveryBtn').bindClick(exchShipping.saveDeliveryType);

        //연락처 - 숫자만 입력
        $("#exchMobileNo_1,#exchMobileNo_2,#exchMobileNo_3").allowInput("keyup", ["NUM","NOT_SPACE"], $(this).attr("id"));

        //송장번호 - 숫자만 입력
        $("#exchInvoiceNo").allowInput("keyup", ["NUM","NOT_SPACE","ENG"], $(this).attr("id"));
    },

    searchClaimShipping : function(){

        CommonAjax.basic(
              {
                  url:'/claim/info/getClaimReasonInfo.json',
                  data: JSON.stringify({
                    "claimNo" : claimNo,
                    "claimBundleNo" : claimBundleNo,
                    "claimType" : 'X'
                  }),
                  contentType: 'application/json',
                  method: "POST",
                  successMsg: null,
                  callbackFunc: function(res) {
                      shipType = res.claimPickShipping.shipType;
                      shipMethod = res.claimPickShipping.shipMethod;
                      $.each(res.claimExchShipping, function(key, value){
                          $('#'+key).val(value);
                      });

                      if(!$.jUtil.isEmpty(res.claimPickShipping.placeAddrExtnd )){
                          var startTime = res.claimPickShipping.startTime;
                          var endTime = res.claimPickShipping.endTime;
                          $('#placeNm').text("(방문위치 : "+ res.claimPickShipping.placeAddrExtnd + " / " + "이용시간 " + startTime.substring(0,2) + " : " + startTime.substring(2,4) + " ~ " + endTime.substring(0,2) + " : " + endTime.substring(2,4) +")");
                      }

                      $('#bundleNo').val(res.bundleNo);
                      $('#purchaseOrderNo').val(res.purchaseOrderNo);
                      $('#storeNm').text(res.claimPickShipping.storeNm);

                      $.jUtil.valSplit($("input[name$='exchMobileNo']" ).val(), '-', 'exchMobileNo');
                      exchShipping.setPickWay(res.claimPickShipping.shipType, $.jUtil.isEmpty(res.claimExchShipping.exchShipTypeCode) ? res.claimPickShipping.pickShipTypeCode :  res.claimExchShipping.exchShipTypeCode);
                  }
              })
    },

    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * shipType TD,AURORA:TD매장 DS:DS매장
     * **/
    setPickWay : function(shipType, exchShipType){
        switch (shipType) {
            case 'TD' : case 'AURORA' : case 'PICK' ://TD점포
                $('#pickWayTD').show();
                $('#pickWayDS').hide();
                $('#tdWayTitle').show();

                //점포방문
                if(shipType == 'PICK'){
                    $('#pickRadioLabel,#pickPlace').show();
                    $('#tdRadioLabel,#exchShippingRow').hide();

                    $("input:radio[id='pickRadio']").prop('checked',true);
                    $('#slotDiv').hide();

                }else{//자차배송
                    if(shipMethod == 'TD_QUICK') {
                        $('#quickRadioLabel').show();
                        $('#slotDiv,#slotChangeArea').hide();

                        $("input:radio[id='quickRadio']").prop('checked',true);
                    }else{
                        $('#tdRadioLabel').show();
                        $("input:radio[id='tdRadio']").prop('checked',true);
                        $('#slotDiv').show();
                        exchShipping.getChangeableInfo();
                    }
                }
                break;
            case 'DS' ://DS점포
                $('#pickWayDS').show();
                $('#pickWayTD').hide();

               exchShipping.exchShipping(exchShipType)
               break;

            default :
                if('TD_DRCT'.indexOf(shipMethod) > -1) {
                    $('#pickWayTD').show();
                    $('#pickWayDS').hide();
                    $('#tdWayTitle').show();

                    $('#pickRadioLabel,#pickPlace').hide();
                    $('#tdRadioLabel').show();
                    $("input:radio[id='tdRadio']").prop('checked',true);
                    $('#slotDiv').show();
                    exchShipping.getChangeableInfo();
                }else{
                    $('#pickWayDS').show();
                    $('#pickWayTD').hide();
                    exchShipping.exchShipping(exchShipType)
                }
                break;
        }
    },


    /**
     * [수거/배송방법] 선택 값에 따른 문구 설정
     * reqType 1:수거 2:배송
     * **/
    exchShipping : function(exchShipType){

        if($.jUtil.isEmpty(exchShipType)){
            exchShipType = $('#exchShipType').val();
        }

        $('#text1').text('· 선택한 교환 건을 배송처리 합니다.').append('<br>');
        $('#text2').text('· 등록된 배송정보는 고객 마이페이지에서 노출됩니다.').append('<br>');

        switch (exchShipType) {
            case 'C' ://택배배송
                $('#text3').text('');

                $("#parcelDiv").show();
                $("#pickDiv,#postDiv").hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'D' ://자차배송
                $('#text3').text('· 상품을 받을 날짜를 선택하세요.');

                $('#pickDiv').show();
                $('#parcelDiv,#postDiv').hide();
                $("#pickDiv").find("img").show();
                break;
            case 'P' ://우편배송
                $('#text3').text('');

                $('#postDiv').show();
                $('#parcelDiv,#pickDiv').hide();
                $("#pickDiv").find("img").hide();
                break;
            case 'N' :
                $('#text3').text('· 교환배송 방법을 "배송없음"으로 선택하면, "교환완료" 단계로 즉시변경됩니다.');


                $("#parcelDiv,#postDiv,#pickDiv").hide();
                $("#pickDiv").find("img").hide();
                break;
        }
        $('#exchShipType').val(exchShipType);
    },

    /**
     * 배송시간 변경 slot
     * **/
    getChangeableInfo : function () {
        var data = {
            'bundleNo' : $('#bundleNo').val(),
            'purchaseOrderNo' : $('#purchaseOrderNo').val()
        }
        CommonAjax.basic(
            {
                url:'/order/getShipSlotInfo.json?'.concat(jQuery.param(data)),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function(res) {
                    if(res.returnCode == '0000' || res.returnCode == 'SUCCESS'){
                        $('#slotChangeArea').empty();
                        $('#slotChangeArea').append(orderUtil.writeToSlotTable(res.data))
                        orderUtil.setWindowAutoResize();
                    }
                }
            });
    },

    validation : function(){

        //배송지 이름
        if($.jUtil.isEmpty($('#exchReceiverNm').val())){
            alert('교환배송지 이름을 입력주세요')
            return false
        }
        //배송지 연락처
        if($.jUtil.isEmpty($('#exchMobileNo').val())){
            alert('교환배송지 연락처를 입력주세요')
            return false;
        }
        if($('#exchMobileNo_1').val().length < 2) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#exchMobileNo_2').val().length < 3) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }

        if($('#exchMobileNo_3').val().length < 4) {
            alert('연락처를 다시 한번 확인해 주세요');
            return false;
        }
        //배송지 우편번호
        if($.jUtil.isEmpty($('#exchZipcode').val())){
            alert('교환배송지 우편번호를 입력주세요')
            return false;
        }
        //배송지 주소
        if($.jUtil.isEmpty($('#exchAddr').val())){
            alert('교환배송지 주소를 입력주세요')
            return false;
        }
        //배송지 상세주소
        if($.jUtil.isEmpty($('#exchAddrDetail').val())){
            alert('교환배송지 상세주소를 입력주세요')
            return false;
        }

        var pickShipType;

        if(shipType == 'DS'){
            pickShipType = $('#exchShipType').val();
            switch (pickShipType) {
            case 'C':
                if($.jUtil.isEmpty($('#exchDlvCd').val())){
                    alert('택배사를 입력 해 주세요');
                    return false;
                }else if($.jUtil.isEmpty($('#exchInvoiceNo').val())){
                    alert('송장번호를 입력 해 주세요');
                    return false;
                };
                break;
            case 'D' :
                if ($.jUtil.isEmpty($('#exchD0Dt').val())) {
                    alert('배송예정일을 입력해주세요');
                    return false;
                }
                break;
            case 'P' :
                if ($.jUtil.isEmpty($('#exchMemo').val())) {
                    alert('메모를 입력해 주세요');
                    return false;
                }
                break;
            }
        }else if(shipType === 'TD' || shipType === 'AURORA'){
            if(shipMethod != 'TD_QUICK') {//쿽수거
                var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
                if ($.jUtil.isEmpty(selectedInfo)) {
                    alert('배송시간을 선택해주세요');
                    return false;
                }
            }
        }
        return true;
    },

    /**
     * 수거/교환 방법변경 저장 버튼클릭
     * **/
    saveDeliveryType : function(){

        $('#exchMobileNo').val($.jUtil.valNotSplit('exchMobileNo', '-'));


        //N 인 경우 유효성 검사를 하지 않는다. 배송방법 : 수거안함, 점포방문
        var validSkipVal;

        //수거 안함 선택 시 유효성 검사 skip하기 위해 배송 방법 조회
        if(shipType == 'PICK'){
            validSkipVal = 'N'
        }else if(shipType == 'DS'){
            validSkipVal = $('#exchShipType').val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else if(shipMethod == 'TD_QUICK'){
                validSkipVal = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                validSkipVal = $('#exchShipType').val();
            }
        }

        //수거 안함 선택 시 유효성 검사 skip
        if(validSkipVal != 'N'){
            if(!exchShipping.validation()){
                return false;
            }
        }

        var param = exchShipping.getExchShippingParam();

        if (!confirm('교환 상품을 배송처리 하시겠습니까?')) {
            return false;
        }

        CommonAjax.basic({
            url         : '/claim/popup/chgClaimShipping.json',
            data        : JSON.stringify(param),
            method      : "POST",
            contentType : "application/json; charset=utf-8",
            callbackFunc: function(res) {
                if (res.returnCode != "0000") {
                    alert(res.returnMessage);
                    return false;
                } else {
                    alert('교환배송 처리 성공');
                    opener.location.reload();
                    self.close();
                }
            }
        });
    },

    getExchShippingParam : function(){
        var selectedInfo = $('input[name="changeSlotInfo"]:checked').attr('id'); //slot select 정보
        var pickShipDt;
        var shiftId;
        var slotId;
        var exchShipType;

        if(shipType == 'PICK'){
            exchShipType = $("input:radio[name='selectDeliveryType']:checked").val();
        }else{
            if('TD_DRCT'.indexOf(shipMethod) > -1) {
                exchShipType = $("input:radio[name='selectDeliveryType']:checked").val();

                if(exchShipType == 'D'){
                    var idInfo = orderUtil.getSplitData(selectedInfo);
                    pickShipDt = orderUtil.toDateFormatting(idInfo[1]);
                    slotId = idInfo[3];
                    shiftId = idInfo[2];
                }
            }else if(shipMethod == 'TD_QUICK'){
                exchShipType = $("input:radio[name='selectDeliveryType']:checked").val();
            }else{
                exchShipType = $('#exchShipType').val();
                pickShipDt = $('#exchD0Dt').val();
            }
        }

        var exchStatus = exchShipType == 'N' ? 'D3' : 'D1'

        var claimExchShipping = {
            "claimExchShippingNo" : $('#claimExchShippingNo').val(),
            "claimBundleNo" : claimBundleNo,
            "modifyType" : "ALL",
            "exchShipType" : exchShipType,
            "exchDlvCd": $('#exchDlvCd').val(),
            "exchInvoiceNo": $('#exchInvoiceNo').val().replace(' ',''),
            'shipDt' : pickShipDt,
            'slotId' : slotId,
            'shiftId' : shiftId,
            'exchD0Dt' : $('#exchD0Dt').val(),
            "exchMemo" : $('#exchMemo').val(),
            "exchReceiverNm" : $('#exchReceiverNm').val(),
            "exchMobileNo" : $('#exchMobileNo').val(),
            "exchZipcode": $('#exchZipcode').val(),
            "exchRoadBaseAddr" : $('#exchAddr').val(),
            "exchRoadAddrDetail" : $('#exchAddrDetail').val(),
            "exchBaseAddr" : $('#exchBaseAddr').val(),
            "exchDetailAddr" : $('#exchAddrDetail').val(),
            'exchStatus' : exchStatus,
            'shipType' : shipType
        };

        var param = {
            "claimBundleNo": claimBundleNo,
            "claimNo": claimNo,
            "claimShippingReqType": "E",//반품/교환 요청타입, P:반품, E:교환, null:반품/교환
            "claimExchShippingModifyDto": claimExchShipping
        };

        return param;
    },

    setZipcode : function(res){
        $("#exchZipcode").val(res.zipCode);
        $("#exchAddr").val(res.roadAddr);
        $('#exchBaseAddr').val(res.gibunAddr);
    },


}