$(document).ready(function() {
    refundComplete.init();
});

/**
 * 수거요청
 * **/
var refundComplete = {
    /**
     * 초기화
     */
    init : function() {
        reqRefundCompleteList = window.opener.reqRefundCompleteList;
        this.bindingEvent();
    },

    bindingEvent : function(){
        $('#refundCompleteBtn').bindClick(function() { refundComplete.refundComplete()});
    },

    /**
     * 환불완료요청
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    refundComplete: function(){
        var param = {
            "claimPaymentNoList" : reqRefundCompleteList
        }
        if(!confirm('환불완료 하시겠습니까?')){
            return false;
        };


        CommonAjax.basic(
        {
            url:'/claim/refundFail/reqRefundComplete.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert('환불 완료 실패 [' + res.returnMessage + ']');
                    return false;
                }
                $('#resultTag').show();
                $('#requestTag').hide();

                $.each(res.data, function(key, value){
                    $('#'+key).text(value);
                });
                opener.location.reload();
            }
        });
    }
}