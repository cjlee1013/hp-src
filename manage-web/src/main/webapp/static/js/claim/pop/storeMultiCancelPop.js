$(document).ready(function() {
    storeMultiCancelPop.init();
    CommonAjaxBlockUI.global();

});

/**
 * 수거요청
 * **/
var storeMultiCancelPop = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },

    bindingEvent : function(){
        $('#reqMultiCancelBtn').bindClick(function(){ storeMultiCancelPop.reqMultiCancel()});
    },

    /**
     * 일괄취소 요청
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqMultiCancel: function(){

        if(!confirm('일괄취소 하시겠습니까?')){
            return false;
        };

        var param = {
            "claimReasonType" : $('#claimReasonType').val(),
            "claimReasonDetail" : $('#claimReasonDetail').val(),
            "itemList" : window.opener.reqStoreMultiCancelList
        };

        CommonAjax.basic(
        {
            url:'/claim/storeMultiCancel/reqStoreMultiCancel.json',
            data: JSON.stringify(param),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert('일괄취소 실패 [' + res.returnMessage + ']');
                    return false;
                }
                $('#resultTag').show();
                $('#requestTag').hide();

                $.each(res.data, function(key, value){
                    $('#'+key).text(value);
                });
                $('#totalCnt').text(window.opener.reqStoreMultiCancelList.length);

                eval("opener." + callBackScript + "(res);");
            }
        });
    }
}