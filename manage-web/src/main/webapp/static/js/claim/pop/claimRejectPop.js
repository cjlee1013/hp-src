$(document).ready(function() {
    claimReject.init();
    CommonAjaxBlockUI.global();
});

/**
 * 클레임 거부 요청
 * **/
var claimReject = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { claimReject.reqClaimReject()});
    },

    /**
     * 클레임 거부 요청
     * > 취소/반품/교환 거부 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimReject : function(){

        if($.jUtil.isEmpty($('#reasonType').val())){
            alert(claimTypeName + '거부 사유를 선택해주세요.');
            return false;
        }

        var detailReasonRequired = '';

        $.each(claimReasonTypeList, function(key, value){
            if(value.mcCd == $('#reasonType').val()){
                detailReasonRequired = value.detailReasonRequired;
            }
        });

        if($.jUtil.isEmpty($('#reasonTypeDetail').val())){
            if(detailReasonRequired === 'Y'){
                alert('거부상세사유를 입력하세요.');
                return false;
            } else {
                $('#reasonTypeDetail').val($('#reasonType option:selected').text());
            }
        }

        if(!confirm(claimTypeName + '거부 하시겠습니까?')){
            return false;
        }

        CommonAjax.basic(
        {
            url:'/claim/popup/reqClaimProcess.json',
            data: _F.getFormDataByJson($("#claimReqForm")),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert(claimTypeName + '거부 실패 [' + res.returnMessage + ']');
                    return false;
                }
                alert(claimTypeName + '거부 성공');
                opener.location.reload();
                window.close();
            }
        });
    }
}