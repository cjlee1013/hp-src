/**
 * 주문관리 > 클레임관리 > 클레임정보상세 > 차감 할인정보 팝업
 */
$(document).ready(function() {
  promo.init();
  promoInfoListGrid.init();
});

// 클레임상세정보
var promo = {

    init : function(){
        promo.getPromoList()
    },

    getPromoList : function(){
        CommonAjax.basic(
              {
                  url:'/claim/popup/getPromoInfoList.json?claimNo='+claimNo,
                  data: null,
                  contentType: 'application/json',
                  method: "GET",
                  successMsg: null,
                  callbackFunc: function(res) {
                      promoInfoListGrid.setData(res);
                  }
              });
    }
}

// 클레임 처리내역 리스트 그리드
var promoInfoListGrid = {
  gridView : new RealGridJS.GridView("promoInfoListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    promoInfoListGrid.initGrid();
    promoInfoListGrid.initDataProvider();
  },

  initGrid : function() {
    promoInfoListGrid.gridView.setDataSource(promoInfoListGrid.dataProvider);

    promoInfoListGrid.gridView.setStyles(promoInfoListGridBaseInfo.realgrid.styles);
    promoInfoListGrid.gridView.setDisplayOptions(promoInfoListGridBaseInfo.realgrid.displayOptions);
    promoInfoListGrid.gridView.setColumns(promoInfoListGridBaseInfo.realgrid.columns);
    promoInfoListGrid.gridView.setOptions(promoInfoListGridBaseInfo.realgrid.options);

    promoInfoListGrid.gridView.setDisplayOptions({
      showEmptyMessage: true,
      emptyMessage: "차감 행사정보가 없습니다."
    });
  },
  initDataProvider : function() {
    promoInfoListGrid.dataProvider.setFields(promoInfoListGridBaseInfo.dataProvider.fields);
    promoInfoListGrid.dataProvider.setOptions(promoInfoListGridBaseInfo.dataProvider.options);
  },
  setData : function(dataList) {
    promoInfoListGrid.dataProvider.clearRows();
    promoInfoListGrid.dataProvider.setRows(dataList);
  }
};

