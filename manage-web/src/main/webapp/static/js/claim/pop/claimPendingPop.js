$(document).ready(function() {
    claimPending.init();
    CommonAjaxBlockUI.global();
});

/**
 * 클레임 보류 요청
 * **/
var claimPending = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { claimPending.reqClaimPending()});
    },

    /**
     * 클레임 보류 요청
     * > 취소/반품/교환 보류 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimPending: function(){


        if($.jUtil.isEmpty($('#reasonType').val())){
            alert('보류사유를 선택하세요.')
            return false;
        }

        var detailReasonRequired = '';

        $.each(claimReasonTypeList, function(key, value){
            if(value.mcCd == $('#reasonType').val()){
                detailReasonRequired = value.detailReasonRequired;
            }
        });

        if(detailReasonRequired === 'Y'){
            if($.jUtil.isEmpty($('#reasonTypeDetail').val())){
                alert('보류상세사유를 입력하세요.')
                return false;
            }
        }


        if(!confirm(claimTypeName + '보류를 하시겠습니까?')){
            return false;
        };

        CommonAjax.basic(
        {
            url:'/claim/popup/reqClaimProcess.json',
            data: _F.getFormDataByJson($("#claimReqForm")),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert(claimTypeName + '보류 실패 [' + res.returnMessage + ']');
                    return false;
                }
                alert(claimTypeName + '보류 성공');
                opener.location.reload();
                window.close();
            }
        });
    }
}