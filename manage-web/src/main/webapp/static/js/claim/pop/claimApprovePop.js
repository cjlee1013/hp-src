$(document).ready(function() {
    claimApprove.init();
    CommonAjaxBlockUI.global();
});

/**
 * 클레임 승인 요청
 * **/
var claimApprove = {
    /**
     * 초기화
     */
    init : function() {
      this.bindingEvent();
    },

    bindingEvent : function(){
        $('#claimReqBtn').bindClick(function() { claimApprove.reqClaimApprove()});
    },

    /**
     * 클레임 승인 요청
     * > 취소/반품/교환 승인 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimApprove : function(){

        if(!confirm(claimTypeName + '승인 하시겠습니까?')){
            return false;
        };

        if(claimTypeName == '반품'){
            var param = {
                "claimBundleNo": $('#claimBundleNo').val(),
                "claimNo": $('#claimNo').val(),
                "pickStatus" : "P3"
            };
            /* 반품 수거상태 변경 - 수거완료 요청 */
            CommonAjax.basic({
                url         : '/claim/popup/reqChgPickStatus.json',
                data        : JSON.stringify(param),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function(res) {
                    if (res.returnCode != "0000") {
                        alert('반품 수거 상태 변경 실패 [' + res.returnMessage + ']');
                        return false;
                    } else {
                        claimApprove.reqClaimApproveAjax();
                    }
                }
            });
        }else{
            claimApprove.reqClaimApproveAjax();
        }
    },

    /**
     * 클레임 승인 요청 Ajax
     * > 취소/반품/교환 승인 요청 함수
     * @param ClaimProcessReqSetDto
     * @return 응답코드 0000 : 성공, 그 외 : 실패
     * **/
    reqClaimApproveAjax : function(){
        CommonAjax.basic(
        {
            url:'/claim/popup/reqClaimProcess.json',
            data: _F.getFormDataByJson($("#claimReqForm")),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                if(res.returnCode != '0000'){
                    alert(claimTypeName + '승인 실패 [' + res.returnMessage + ']');
                    return false;
                }
                alert(claimTypeName + '승인 성공');
                opener.location.reload();
                window.close();
            }
        });
    }
}