/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  refundFail.init();
  refundFailListGrid.init();
  CommonAjaxBlockUI.global();
});

// 클레임관리
var refundFail = {
    /**
     * 초기화
     */
    init : function() {

      this.bindingEvent();
      this.initSearchDate("-3d");

    },
    initSearchDate : function (flag) {
      deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

      // [검색] 버튼
      $("#searchBtn").bindClick(refundFail.search);
      // [초기화] 버튼
      $("#resetBtn").bindClick(refundFail.reset);
      // [초기화] 버튼
      $("#excelDownloadBtn").bindClick(refundFailListGrid.excelDownload);

      // [환불완료] 버튼
      $("#reqRefundComplete").bindClick(refundFail.reqRefundComplete);

      //[구매자 회원/비회원] 선택
      $("input[name='schUserYn']").bindChange(refundFail.chgUserYn);

      $("input:checkbox").change(function () {
          refundFail.checkBoxChecked($(this), 'ALL');
      });
    },

    /**
     * 환불실패대상 검색
     */
    search : function() {
        const data = $("form[name=refundFailSearchForm]").serialize();

        const schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        const schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if (!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }

        //구매자 정보로 검색시, 조회기간 3개월
        if (!$.jUtil.isEmpty($('#schUserSearch').val()) && $("input:radio[name='schUserYn']:checked").val() == 'Y') {
            if (schEndDt.diff(schStartDt, "month", true) > 1) {
                alert("조회기간은 최대 1개월까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }
        } else if (!$.jUtil.isEmpty($('#schClaimSearch').val())) {
            if (schEndDt.diff(schStartDt, "month", true) > 1) {
                alert("조회기간은 최대 1개월까지 설정 가능합니다.");
                $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }
        //나머지는 조회기간 1주일
        } else {
            if (schEndDt.diff(schStartDt, "week", true) > 1) {
                alert("검색조건이 입력되지 않아, 조회기간은 최대 7일까지 설정 가능합니다.");
                $("#schStartDt").val(
                    schEndDt.add(-1, "week").format("YYYY-MM-DD"));
                return false;
            }
        }
        if (schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }

        CommonAjax.basic(
            {
                url: '/claim/refundFail/getRefundFailList.json?' + data,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    refundFailListGrid.setData(res);
                    $('#refundFailTotalCount').html($.jUtil.comma(refundFailListGrid.gridView.getItemCount()));
                }
            });
    },

    /**
     * 환불완료 요청 팝업 호출
     * **/
    reqRefundComplete : function(){

        var checkRowIds = refundFailListGrid.gridView.getCheckedRows();
        if (checkRowIds.length === 0) {
            alert("환불실패건을 선택해주세요.");
            return;
        }
        reqRefundCompleteList.splice(0, reqRefundCompleteList.length);

        for (var checkRowId of checkRowIds) {
            var checkedRows = refundFailListGrid.dataProvider.getJsonRow(checkRowId);
            reqRefundCompleteList.push(
                {
                    claimPaymentNo: checkedRows.claimPaymentNo,
                    claimNo: checkedRows.claimNo

                }
            );
        }
        windowPopupOpen("/claim/refundFail/popup/getRefundCompletePop", "refundCompletePop", '800', '310', 'yes', 'no');
    },


    /**
     * 검색폼 초기화
     */
    reset : function() {
        $('#refundDateType').val('REQUEST');
        deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-3d", "0");
        $('#schClaimSearchType').val('PURCHASEORDERNO');
        $('#schClaimSearch,#schUserSearch').val('')
        $('#schBuyerInfo').val('BUYERNM');
        $('#schBuyerInfo').hide();
        $("input[name='schUserYn'][value='Y']").prop('checked',true);
        $('#schUserYn').val('Y')
        $("#schUserSearch").attr("readonly", true);
        refundFail.checkBoxChecked($("input:checkbox"), 'ALL');
        $("input:checkbox[name='parentMethodCd'][value='ALL']").prop('checked',true);
    },

    /**
     * 구매자 회원/비회원 선택
     * **/
    chgUserYn : function(schUserYn){
      if (typeof schUserYn == 'object') {
          schUserYn = $(schUserYn).val();
      }

      $("#schUserSearch").val('');
      if(schUserYn == 'Y'){
          $('#schBuyerInfo').hide();//비회원 selectBox 숨기기
          $('#userSchButton').show();//회원조회 버튼 보이기
          $("#schUserSearch").attr("readonly", true);//회원조회 input readonly로 변경
      }else{
          $('#schBuyerInfo').show();//비회원 selectBox 보이기
          $('#userSchButton').hide();//회원조회 버튼 숨기기
          $("#schUserSearch").attr("readonly", false);//회원조회 input readonly로 헤재
      }

    },
    checkBoxChecked : function (_selectedInfo, _allValue){
        const unCheckedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']');
        const checkedBox = $('input:checkbox[name=' + _selectedInfo.attr('name') + ']:checked');

        if(_selectedInfo.val() === _allValue){
            unCheckedBox.each(function (_idx, _value) {
                if(_value.value !== _allValue){
                    this.checked = _selectedInfo.is(':checked');
                }
            });
        } else {
            const checkedValue = checkedBox.map(function () {
                return this.value;
            }).get();
            if(checkedBox.length === (unCheckedBox.length - 1)){
                let isAllCheck = false;
                if(checkedValue.indexOf(_allValue) === -1){
                    isAllCheck = true;
                }
                unCheckedBox.each(function () {
                    if(this.value === _allValue){
                        this.checked = isAllCheck;
                    }
                });
            }
        }
    }

};

// 클레임관리 그리드
var refundFailListGrid = {
  gridView : new RealGridJS.GridView("refundFailListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    refundFailListGrid.initGrid();
    refundFailListGrid.initDataProvider();
    refundFailListGrid.event();
  },
  initGrid : function() {
    refundFailListGrid.gridView.setDataSource(refundFailListGrid.dataProvider);

    refundFailListGrid.gridView.setStyles(refundFailListGridBaseInfo.realgrid.styles);
    refundFailListGrid.gridView.setDisplayOptions(refundFailListGridBaseInfo.realgrid.displayOptions);
    refundFailListGrid.gridView.setColumns(refundFailListGridBaseInfo.realgrid.columns);
    refundFailListGrid.gridView.setOptions(refundFailListGridBaseInfo.realgrid.options);


    refundFailListGrid.gridView.setColumnProperty("claimNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "claimNo", showUrl: false});
    refundFailListGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});

    //refundFailListGrid.gridView.setColumnProperty("refundFailNo", "mergeRule", {criteria:"value"});

  },
  initDataProvider : function() {
    refundFailListGrid.dataProvider.setFields(refundFailListGridBaseInfo.dataProvider.fields);
    refundFailListGrid.dataProvider.setOptions(refundFailListGridBaseInfo.dataProvider.options);
  },
  event : function() {
    // 그리드 선택
    refundFailListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
        var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");
        var claimNo = grid.getValue(index.itemIndex, "claimNo");
        var claimBundleNo = grid.getValue(index.itemIndex, "claimBundleNo");
        var claimType = grid.getValue(index.itemIndex, "claimType");

        if (index.fieldName == "claimNo") {
            UIUtil.addTabWindow('/claim/info/getClaimDetailInfoTab?purchaseOrderNo='+ purchaseOrderNo + "&claimNo=" + claimNo
                + "&claimBundleNo=" + claimBundleNo + "&claimType="+ claimType,'claimDetailTab', '클레임정보상세', '135px')
        }

        if (index.fieldName == "purchaseOrderNo") {
            UIUtil.addTabWindow("/order/orderManageDetail?purchaseOrderNo=" + purchaseOrderNo, "orderDetail","주문정보상세",'135px');
        }
    };
  },
  setData : function(dataList) {
    refundFailListGrid.dataProvider.clearRows();
    refundFailListGrid.dataProvider.setRows(dataList);
  },
  /*  setColumnProperty : function (){
     refundFailListGrid.gridView.set("orderNo","render",{
       type : "link",
       url : "/order/refundFail/getClaimOrderInfo",
       requiredFields : "orderNo",
       showUrl : false
     })
    },*/

  excelDownload: function() {
    if(refundFailListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    var _date = new Date();
    var fileName =  "환불실패대상 조회_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    refundFailListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};