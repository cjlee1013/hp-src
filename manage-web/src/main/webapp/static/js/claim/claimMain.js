/**
 * 주문관리 > 클레임관리
 */
$(document).ready(function() {
  claim.init();
  deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
  claimListGrid.init();
  CommonAjaxBlockUI.global();
});

// 클레임관리
var claim = {
  /**
   * 초기화
   */
  init : function() {
    this.bindingEvent();
    this.chgClaimType();
    this.setStoreId();
  },

  initSearchDate : function (flag) {
    deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "0");
  },

  /**
   * binding 이벤트
   */
  bindingEvent: function() {

    // [클레임 구분] 선택
    $('#schClaimType').bindChange(claim.chgClaimType);
    //[처리상태] 선택
    $('#schClaimStatus').bindChange(claim.chgClaimStatus);
    //[수거배송상태] 선택
    $('#schPickupStatus').bindChange(claim.chgPickupStatus);
    //[판매업체] 선택
    $('#storeType').bindChange(claim.chgStoreType);
    // [검색] 버튼
    $("#searchBtn").bindClick(claim.search);
    // [초기화] 버튼
    $("#resetBtn").bindClick(claim.reset);
    // [초기화] 버튼
    $("#excelDownloadBtn").bindClick(claimListGrid.excelDownload);
    //[구매자 회원/비회원] 선택
    $("input[name='schUserYn']").bindChange(claim.chgUserYn);

    $("input[name='schReserveShipMethod']").bindClick(claim.checkShipType);

    //[판매업체] 선택
    $('#storeType').bindChange(claim.chgStoreType);

    // 점포 조회 버튼
    $("#schPartnerBtn").bindClick(claim.openStorePopup);

  },

  /**
   * 클레임 검색
   */
  search : function() {

    var data = _F.getFormDataByJson($("#claimSearchForm"));

    var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
    var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

    if(!schStartDt.isValid() || !schEndDt.isValid()){
        alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
        return false;
    }

    //구매자 정보로 검색시, 조회기간 3개월
    if(!$.jUtil.isEmpty($('#schUserSearch').val()) && $("input:radio[name='schUserYn']:checked").val() == 'Y'){
        if (schEndDt.diff(schStartDt, "month", true) > 3) {
          alert("구매자로 검색시, 조회기간은 최대 3개월 범위까지만 가능합니다.");
          $("#schStartDt").val(schEndDt.add(-3, "month").format("YYYY-MM-DD"));
          return false;
        }
    //판매자 정보로 검색시, 조회기간 1개월
    }else if($('#schPartnerId').val() != '' && $('#schPartnerName').val() != ''){
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
          alert("판매자로 검색시, 조회기간은 최대 1개월 범위까지만 가능합니다.");
          $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
          return false;
        }
    //나머지는 조회기간 1주일
    }
    //판매자 정보로 검색시, 조회기간 1개월
    else if($('#claimSearchType').val() == 'ITEMNO' && $('#schClaimSearch').val() != ''){
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
          alert("상품번호로 검색시, 조회기간은 최대 1개월 범위까지만 가능합니다.");
          $("#schStartDt").val(schEndDt.add(-1, "month").format("YYYY-MM-DD"));
          return false;
        }
    //나머지는 조회기간 1주일
    }else{
        if (schEndDt.diff(schStartDt, "week", true) > 1) {
            alert("조회기간은 최대 1주일 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "week").format("YYYY-MM-DD"));
            return false;
        }
    }
    if(schEndDt.diff(schStartDt, "day", true) < 0) {
        alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
        $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
        return false;
    }
    CommonAjax.basic(
        {
          url:'/claim/info/getClaimList.json',
          data: data,
          contentType: 'application/json',
          method: "POST",
          successMsg: null,
          callbackFunc:function(res) {
            claimListGrid.setData(res);
            $('#claimTotalCount').html($.jUtil.comma(claimListGrid.gridView.getItemCount()));
          }
        });
  },

  /**
   * 검색폼 초기화
   */
  reset : function() {
    $('#schClaimStatus,#schPickupStatus,#partnerCd,#storeType,#schClaimChannel').val('ALL');
    $('#schClaimType').val('C');
    $('#claimSearchType').val('CLAIMBUNDLENO');
    $('#claimDateType').val('REQUEST');
    deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "0", "0");
    $('#schUserSearch,#schPartnerId,#schPartnerName,#schClaimSearch').val('')
    $('#schUserYn').val('Y')
    $("input[name='schUserYn'][value='Y']").prop('checked',true);
    $('#schPickupStatus').attr('disabled', true);
    $('#schBuyerInfo').val('BUYERNM');
    $('#schBuyerInfo').hide();
    $("#schUserSearch").attr("readonly", true);
  },

  /**
   * 클레임 타입 선택
   * **/
  chgClaimType : function(){
    $('#schClaimStatus').val('ALL');
    $('#schClaimStatus').attr('disabled', false);
    $('#schPickupStatus').val('ALL');
    $('#schExchStatus').val('ALL');

    if($('#schClaimType').val() == 'C'){
        $('#schPickupStatus').attr('disabled', true);
    }else{
        $('#schPickupStatus').attr('disabled', false);
    }

    if($('#schClaimType').val() != 'X'){
        $('#schExchStatus').attr('disabled', true);
    }else{
        $('#schExchStatus').attr('disabled', false);
    }

    $('#schClaimStatus').children('option:not(:first)').remove();

    var claimName = $('#schClaimType').val() == 'C' ? '취소' : $('#schClaimType').val() == 'R' ? '반품' : '교환';

    //클레임 상태 select box 옵션 추가
    switch ($('#schClaimType').val()) {
      case "C" : case "R" :
        $.each(claimStatisList, function(key, value){
          $('#schClaimStatus').append("<option value="+value.mcCd+">"+claimName+""+value.mcNm+"</option>")
        });
        break;
      case "X" :
        $.each(claimStatisList, function(key, value){
          if(value.ref1 != 'df'){
            $('#schClaimStatus').append("<option value="+value.mcCd+">"+claimName+""+value.mcNm+"</option>")
          }
        });
        break;
    }


  },
  /**
   * 처리상태선택
   * **/
  chgClaimStatus : function(){
    if($('#schClaimType').val() != 'C'){
        $('#schPickupStatus').val('ALL');
        $('#schPickupStatus').attr('disabled', false);
    }
  },

  /**
   * 수거배송상태선택
   * **/
  chgPickupStatus : function(){
    if($('#schClaimType').val() == 'X'){
        $('#schExchStatus').val('ALL');
        $('#schExchStatus').attr('disabled', false);
    }
  },

  /**
   * 구매자 회원/비회원 선택
   * **/
  chgUserYn : function(schUserYn){
    if (typeof schUserYn == 'object') {
        schUserYn = $(schUserYn).val();
    }

    $("#schUserSearch").val('');
    if(schUserYn == 'Y'){
        $('#schBuyerInfo').hide();//비회원 selectBox 숨기기
        $('#userSchButton').show();//회원조회 버튼 보이기
        $("#schUserSearch").attr("readonly", true);//회원조회 input readonly로 변경
    }else{
        $('#schBuyerInfo').show();//비회원 selectBox 보이기
        $('#userSchButton').hide();//회원조회 버튼 숨기기
        $("#schUserSearch").attr("readonly", false);//회원조회 input readonly로 헤재
    }

  },
  chgStoreType : function(){
    if($('#storeType').val() == 'ALL'){
        $('#schPartnerBtn').attr('disabled', true);
    }else{
       $('#schPartnerBtn').attr('disabled', false);
    }

  },
  /**
   * 판매업체 조회 팝업창
   */
  getPartnerPop : function(callBack) {
    windowPopupOpen("/common/partnerPop?partnerId="
        + '' + "&callback="
        + callBack + "&partnerType="
        + $('#storeType').val()
        , "partnerStatus", "1080", "600", "yes", "no");
  },
  /**
   * 배송유형 선택
   */
  checkShipType : function() {
    var schReserveShipMethod = $("input[name='schReserveShipMethod']:checked").val();

    if(schReserveShipMethod == 'RESERVE_TD_DLV'){
      $('#shipTypeTD').attr('disabled', true);
      $('#shipTypeDS').attr('disabled', false);
      $('#shipTypeItem').attr('disabled', true);
    }else if(schReserveShipMethod == 'RESERVE_TD_DRCT'){
      $('#shipTypeTD').attr('disabled', false);
      $('#shipTypeDS').attr('disabled', true);
      $('#shipTypeItem').attr('disabled', true);
    }else if(schReserveShipMethod == 'RESERVE_DRCT_NOR'){
      $('#shipTypeTD').attr('disabled', true);
      $('#shipTypeDS').attr('disabled', true);
      $('#shipTypeItem').attr('disabled', false);
    }else{
      $('#shipTypeDS,#shipTypeTD,#shipTypeItem').attr('disabled', false);
    }

  },

  /**
   * 그리드 row 선택
   * @param selectRowId
   */
  gridRowSelect : function(selectRowId) {
    var rowDataJson = claimListGrid.dataProvider.getJsonRow(selectRowId);
    UIUtil.addTabWindow('/claim/info/getClaimDetailInfoTab?purchaseOrderNo='
        + rowDataJson.purchaseOrderNo + "&claimNo=" + rowDataJson.claimNo
        + "&claimBundleNo=" + rowDataJson.claimBundleNo + "&claimType="+ rowDataJson.claimTypeCode,
        'claimDetailTab', '클레임정보상세', '135px')
  },

  callbackMemberSearchPop : function(memberData) {
    $("#schUserSearch").val(memberData.userNo);
  },

  chgStoreType : function(){
      var storeType = $('#storeType').val();
      var schStoreId = $('#schPartnerId');
      var schStoreNm = $('#schPartnerName');
      schStoreId.val('');
      schStoreNm.val('');
      if(storeType === 'ALL'){
          $('#schPartnerBtn').attr('disabled', true);
      }else{
          $('#schPartnerBtn').attr('disabled', false);
      }

      if (storeType === 'DS') {
          schStoreId.prop("placeholder","판매자ID");
          schStoreNm.prop("placeholder","판매자명");
      } else {
          schStoreId.prop("placeholder","점포ID");
          schStoreNm.prop("placeholder","점포명");
      }
  },

  /**
   * 점포 / 판매 업체 조회 팝업
   */
  openStorePopup: function() {
      var storeTypeVal = $('#storeType').val();

      if ($.jUtil.isEmpty(storeTypeVal)) {
          alert("점포유형을 선택해주세요.");
          return;
      }

      if (storeTypeVal === "DS"){
          windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=claim.callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
      }else{
          windowPopupOpen("/common/popup/storePop?callback=claim.callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
      }
  },

  /**
   * Store 계정 접속 여부 확인
   */
  setStoreId: function() {
      if (!$.jUtil.isEmpty(storeId)) {
          $("#storeType").val(soStoreType);
          $("#schPartnerId").val(storeId);
          $("#schPartnerName").val(storeNm);
          $("#schClaimType").val('R');
          $("#storeType").prop("disabled",true);
          $("#schPartnerBtn").prop("disabled",true);
          claim.chgClaimType();
      }
  },
};

claim.callBack = {
  searchPartner : function(res) {
    $('#schPartnerId').val(res[0].partnerId);
    $('#schPartnerName').val(res[0].partnerNm);
  },
  searchStore : function (res) {
    $('#schPartnerId').val(res[0].storeId);
    $('#schPartnerName').val(res[0].storeNm);
  }

};

// 클레임관리 그리드
var claimListGrid = {
  gridView : new RealGridJS.GridView("claimListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    claimListGrid.initGrid();
    claimListGrid.initDataProvider();
    claimListGrid.event();
  },
  initGrid : function() {
    claimListGrid.gridView.setDataSource(claimListGrid.dataProvider);

    claimListGrid.gridView.setStyles(claimListGridBaseInfo.realgrid.styles);
    claimListGrid.gridView.setDisplayOptions(claimListGridBaseInfo.realgrid.displayOptions);
    claimListGrid.gridView.setColumns(claimListGridBaseInfo.realgrid.columns);
    claimListGrid.gridView.setOptions(claimListGridBaseInfo.realgrid.options);

    claimListGrid.gridView.setColumnProperty("claimNo", "mergeRule", {criteria:"value"});
    claimListGrid.gridView.setColumnProperty("claimBundleNo", "mergeRule", {criteria:"value"});
    claimListGrid.gridView.setColumnProperty("purchaseOrderNo", "mergeRule", {criteria:"value"});
    claimListGrid.gridView.setColumnProperty("bundleNo", "mergeRule", {criteria:"value"});


  },
  initDataProvider : function() {
    claimListGrid.dataProvider.setFields(claimListGridBaseInfo.dataProvider.fields);
    claimListGrid.dataProvider.setOptions(claimListGridBaseInfo.dataProvider.options);
  },
  event : function() {
    // 그리드 선택
    claimListGrid.gridView.onDataCellDblClicked = function(gridView, index) {
      claim.gridRowSelect(index.dataRow);
    };
  },
  setData : function(dataList) {
    claimListGrid.dataProvider.clearRows();
    claimListGrid.dataProvider.setRows(dataList);
    claimListGrid.gridView.orderBy(['claimNo'],[RealGridJS.SortDirection.DESCENDING]);
  },
  /*  setColumnProperty : function (){
     claimListGrid.gridView.set("orderNo","render",{
       type : "link",
       url : "/order/claim/getClaimOrderInfo",
       requiredFields : "orderNo",
       showUrl : false
     })
    },*/

  excelDownload: function() {
    if(claimListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    var _date = new Date();
    var fileName =  "클레임관리_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    claimListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};