/**
 * 주문관리 > 클레임관리 > 점포 일괄주문취소
 */
$(document).ready(function() {
  storeMultiCancel.init();
  deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
  storeMultiCancelListGrid.init();
  CommonAjaxBlockUI.global();
});

// 점포 일괄주문취소
var storeMultiCancel = {

  /**
   * 초기화
   */
  init : function() {
    this.bindingEvent();
    this.setStoreId();
  },
  initSearchDate : function (flag) {
    deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", flag, "0");
  },

  /**
   * binding 이벤트
   */
  bindingEvent: function() {

    //[판매업체] 선택
    $('#schStoreType').bindChange(storeMultiCancel.chgStoreType);

    // 점포 조회 버튼
    $("#schStoreBtn").bindClick(storeMultiCancel.openStorePopup);

    // [일괄취소] 버튼
    $("#reqMultiCancel").bindClick(storeMultiCancel.reqMultiCancel);

    // [엑셀다운로드] 버튼
    $("#excelDownloadBtn").bindClick(storeMultiCancelListGrid.excelDownload);

    // [검색] 버튼
    $("#searchBtn").bindClick(storeMultiCancel.search);
    // [초기화] 버튼
    $("#resetBtn").bindClick(storeMultiCancel.reset);
  },

  /**
   * 점포 일괄주문취소 검색
   */
  search : function() {

    var data = _F.getFormDataByJson($("#searchForm"));

    var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
    var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

    if(!schStartDt.isValid() || !schEndDt.isValid()){
        alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
        return false;
    }


    if($('#schSearch').val() != ''){
        if (schEndDt.diff(schStartDt, "month", true) > 1) {
            alert("상세 검색 시, 조회기간은 최대 1개월까지 설정 가능합니다.");
            $("#schStartDt").val(
                schEndDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
    }
    //구매자 정보로 검색시, 조회기간 3개월
     else if(!$.jUtil.isEmpty($('#schStoreId').val()) || !$.jUtil.isEmpty($('#schShiftId').val()) || !$.jUtil.isEmpty($('#schRefundStatus').val()) || !$.jUtil.isEmpty($('#schSubStatus').val())){
        if (schEndDt.diff(schStartDt, "week", true) > 1) {
          alert("조회기간은 최대 7일까지 설정 가능합니다.");
          $("#schStartDt").val(schEndDt.add(-1, "week").format("YYYY-MM-DD"));
          return false;
        }
    //판매자 정보로 검색시, 조회기간 1개월
    }else{
        if (schEndDt.diff(schStartDt, "day", true) > 1) {
            alert("검색조건이 입력되지 않아, 조회기간은 최대 1일까지 설정 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-1, "day").format("YYYY-MM-DD"));
            return false;
        }
    }
    if(schEndDt.diff(schStartDt, "day", true) < 0) {
        alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
        $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
        return false;
    }
    CommonAjax.basic(
        {
          url:'/claim/storeMultiCancel/getStoreMultiCancelList.json',
          data: data,
          contentType: 'application/json',
          method: "POST",
          successMsg: null,
          callbackFunc:function(res) {
            storeMultiCancelListGrid.setData(res);
            $('#storeMultiCancelTotalCount').html($.jUtil.comma(storeMultiCancelListGrid.gridView.getItemCount()));
          }
        });
  },

  /**
   * 검색폼 초기화
   */
  reset : function() {
    $('#schShiftId,#schSubstitutionYn,#schRefundStatus,#schSubStatus').val('ALL');
    $('#schStoreType').val('HYPER');
    $('#schDateType').val('DELIVERY');
    $('#schSearchType').val('PURCHASEORDERNO');
    deliveryCore.initSearchDateCustom("schStartDt", "schEndDt", "-1w", "0");
    $('#schStoreId,#schStoreName,#schSearch').val('')
  },

  /**
   * 일괄취소 요청 팝업 호출
   * **/
  reqMultiCancel : function(){

      var checkRowIds = storeMultiCancelListGrid.gridView.getCheckedRows();
      if (checkRowIds.length === 0) {
          alert("선택된 주문건이 없습니다.");
          return;
      }
      reqStoreMultiCancelList.splice(0, reqStoreMultiCancelList.length);

      for (var checkRowId of checkRowIds) {
          var checkedRows = storeMultiCancelListGrid.dataProvider.getJsonRow(checkRowId);
          reqStoreMultiCancelList.push(
              {
                  purchaseOrderNo : checkedRows.purchaseOrderNo,
                  bundleNo : checkedRows.bundleNo
              }
          );
      }
      windowPopupOpen("/claim/storeMultiCancel/popup/getStoreMultiCancelPop?callback=storeMultiCancel.search", "storeMultiCancelPop", '800', '380', 'yes', 'no');
  },


  chgStoreType : function(){
       $('#schShiftId').find("option").remove();//select box 옵션 제거
       $('#schShiftId').append("<option value='ALL'>전체</option>");

      const storeType = $('#schStoreType').val();
      const schStoreId = $('#schStoreId');
      const schStoreNm = $('#schStoreName');
      schStoreId.val('');
      schStoreNm.val('');
      if(storeType === 'ALL'){
          $('#schStoreBtn').attr('disabled', true);
      }else{
          $('#schStoreBtn').attr('disabled', false);
      }

      if (storeType === 'DS') {
          schStoreId.prop("placeholder","판매자ID");
          schStoreNm.prop("placeholder","판매자명");
      } else {
          schStoreId.prop("placeholder","점포ID");
          schStoreNm.prop("placeholder","점포명");
      }
  },

  /**
   * 점포 / 판매 업체 조회 팝업
   */
  openStorePopup: function() {
      const storeTypeVal = $('#schStoreType').val();

      if ($.jUtil.isEmpty(storeTypeVal)) {
          alert("점포유형을 선택해주세요.");
          return;
      }

      if (storeTypeVal === "DS"){
          windowPopupOpen("/common/popup/partnerPop?partnerId=&callback=storeMultiCancel.callBack.searchPartner&partnerType=SELL" , "partnerPop", 1100, 620, "yes", "yes");
      }else{
          windowPopupOpen("/common/popup/storePop?callback=storeMultiCancel.callBack.searchStore&storeType=" + storeTypeVal , "storePop", 1100, 620, "yes", "yes");
      }
  },

  /**
   * Store 계정 접속 여부 확인
   */
  setStoreId: function() {
      if (!$.jUtil.isEmpty(storeId)) {
          $("#schStoreType").val(storeType);
          $("#schStoreId").val(storeId);
          $("#schStoreName").val(storeNm);
          $("#schStoreType").prop("disabled",true);
          $("#schStoreBtn").prop("disabled",true);
      }
  }
};

storeMultiCancel.callBack = {
    searchPartner : function(res) {
      $('#schStoreId').val(res[0].partnerId);
      $('#schStoreName').val(res[0].partnerNm);
      this.searchStoreShift();
    },
    searchStore : function (res) {
      $('#schStoreId').val(res[0].storeId);
      $('#schStoreName').val(res[0].storeNm);
      this.searchStoreShift();
    },
    searchStoreShift(){
        const param = {
            'schStoreType' : $('#schStoreType').val(),
            'schStoreId' : $('#schStoreId').val(),
        }

        CommonAjax.basic(
        {
          url:'/claim/outOfStock/getStoreShiftList.json',
          data: JSON.stringify(param),
          contentType: 'application/json',
          method: "POST",
          successMsg: null,
          callbackFunc:function(res) {
               $.each(res, function (key, value) {
                  $('#schShiftId').append("<option value=" + value + ">" + value + "</option>")
              });
          }
        });
    },
    /**
     * Store 계정 접속 여부 확인
     */
    setStoreId: function() {
        if (!$.jUtil.isEmpty(storeId)) {
            $("#storeType").prop("disabled",true);
            $("#schStoreBtn").prop("disabled",true);
            $("#schStoreId").val(storeId);
            $("#schStoreName").val(storeNm);
        }
    }
};

// 일괄취소 그리드
var storeMultiCancelListGrid = {
  gridView : new RealGridJS.GridView("storeMultiCancelListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
    storeMultiCancelListGrid.initGrid();
    storeMultiCancelListGrid.initDataProvider();
    storeMultiCancelListGrid.event();
  },
  initGrid : function() {
    storeMultiCancelListGrid.gridView.setDataSource(storeMultiCancelListGrid.dataProvider);

    storeMultiCancelListGrid.gridView.setStyles(storeMultiCancelListGridBaseInfo.realgrid.styles);
    storeMultiCancelListGrid.gridView.setDisplayOptions(storeMultiCancelListGridBaseInfo.realgrid.displayOptions);
    storeMultiCancelListGrid.gridView.setColumns(storeMultiCancelListGridBaseInfo.realgrid.columns);
    storeMultiCancelListGrid.gridView.setOptions(storeMultiCancelListGridBaseInfo.realgrid.options);
    storeMultiCancelListGrid.gridView.setColumnProperty("purchaseOrderNo", "renderer", {type: "link", url: "unnecessary", requiredFields: "purchaseOrderNo", showUrl: false});

  },
  initDataProvider : function() {
    storeMultiCancelListGrid.dataProvider.setFields(storeMultiCancelListGridBaseInfo.dataProvider.fields);
    storeMultiCancelListGrid.dataProvider.setOptions(storeMultiCancelListGridBaseInfo.dataProvider.options);
  },
  event : function() {

      storeMultiCancelListGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var purchaseOrderNo = grid.getValue(index.itemIndex, "purchaseOrderNo");

            if (index.fieldName == "purchaseOrderNo") {
                UIUtil.addTabWindow('/order/orderManageDetail?purchaseOrderNo=' + purchaseOrderNo,'orderDetail', '주문정보상세', '150px')
            }
      };
  },
  setData : function(dataList) {
    storeMultiCancelListGrid.dataProvider.clearRows();
    storeMultiCancelListGrid.dataProvider.setRows(dataList);
  },

  excelDownload: function() {
    if(storeMultiCancelListGrid.gridView.getItemCount() == 0) {
      alert("검색 결과가 없습니다.");
      return false;
    }

    var _date = new Date();
    var fileName =  "점포 일괄주문취소_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

    storeMultiCancelListGrid.gridView.exportGrid({
      type: "excel",
      target: "local",
      fileName: fileName + ".xlsx",
      showProgress: true,
      progressMessage: "엑셀 Export중입니다."
    });
  }
};