/**
 * 사이트관리 > 스티커관리 
 */
$(document).ready(function() {
    stickerApplyListGrid.init();
    stickerApplyItemListGrid.init();

    stickerApply.init();
    CommonAjaxBlockUI.global();
    stickerApply.getSticker();
});

// 스티커 리스트 그리드
var stickerApplyListGrid = {

    gridView : new RealGridJS.GridView("stickerApplyListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
       stickerApplyListGrid.initGrid();
       stickerApplyListGrid.initDataProvider();
       stickerApplyListGrid.event();
    },
    initGrid : function() {
       stickerApplyListGrid.gridView.setDataSource(stickerApplyListGrid.dataProvider);

       stickerApplyListGrid.gridView.setStyles(stickerApplyListGridBaseInfo.realgrid.styles);
       stickerApplyListGrid.gridView.setDisplayOptions(stickerApplyListGridBaseInfo.realgrid.displayOptions);
       stickerApplyListGrid.gridView.setColumns(stickerApplyListGridBaseInfo.realgrid.columns);
       stickerApplyListGrid.gridView.setOptions(stickerApplyListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
       stickerApplyListGrid.dataProvider.setFields(stickerApplyListGridBaseInfo.dataProvider.fields);
       stickerApplyListGrid.dataProvider.setOptions(stickerApplyListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
       stickerApplyListGrid.dataProvider.clearRows();
       stickerApplyListGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
       stickerApplyListGrid.gridView.onDataCellClicked = function(gridView, index) {
            stickerApply.gridRowSelect(index.dataRow);
        };
    },
};

// 스티커 리스트 그리드
var stickerApplyItemListGrid ={

    gridView : new RealGridJS.GridView("stickerApplyItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {
        stickerApplyItemListGrid.initGrid();
        stickerApplyItemListGrid.initDataProvider();
        stickerApplyItemListGrid.event();
    },
    initGrid : function() {
        stickerApplyItemListGrid.gridView.setDataSource(stickerApplyItemListGrid.dataProvider);

        stickerApplyItemListGrid.gridView.setStyles(stickerApplyItemListGridBaseInfo.realgrid.styles);
        stickerApplyItemListGrid.gridView.setDisplayOptions(stickerApplyItemListGridBaseInfo.realgrid.displayOptions);
        stickerApplyItemListGrid.gridView.setColumns(stickerApplyItemListGridBaseInfo.realgrid.columns);
        stickerApplyItemListGrid.gridView.setOptions(stickerApplyItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        stickerApplyItemListGrid.dataProvider.setFields(stickerApplyItemListGridBaseInfo.dataProvider.fields);
        stickerApplyItemListGrid.dataProvider.setOptions(stickerApplyItemListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        stickerApplyItemListGrid.dataProvider.clearRows();
        stickerApplyItemListGrid.dataProvider.setRows(dataList);
    },
    event : function() {

    },
    delRow : function (dataList) {
        stickerApplyItemListGrid.dataProvider.removeRows(dataList,false);
    },
    excelDownload: function() {
        if(stickerApplyItemListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        let _date = new Date();
        let fileName = "스티커대상상품관리_"+_date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        stickerApplyItemListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};


//스티커 대상상품 관리
var stickerApply = {

    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
        this.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
    },

    initSearchDate : function (flag) {
        stickerApply.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        stickerApply.setDate.setEndDate(0);
        stickerApply.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        stickerApply.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);
        stickerApply.setDateTime.setEndDateByTimeStamp(flag);
        stickerApply.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#schBtn').bindClick(stickerApply.search);
        $('#schResetBtn').bindClick(stickerApply.searchFormReset);

        $('#setStickerApplyBtn').bindClick(stickerApply.setStickerApply);
        $('#resetBtn').bindClick(stickerApply.resetForm);

        $('#stickerApplyNm').calcTextLength('keyup', '#textCountKr_keyword');
        $('#addItemBtn').bindClick(stickerApply.addItemsPopUp);
        $('#deleteItemBtn').bindClick(stickerApply.deleteItem);
        $('#schGridItemBtn').bindClick(stickerApply.schGridItemNo);

        $('#excelDownloadBtn').bindClick(stickerApply.excelDownload);
        $("#excelUploadBtn").bindClick(stickerApply.excelUpload);
    },
    /**
     * 스티커 리스트 검색
     * @returns {boolean}
     */
    search : function() {

        if(!stickerApply.valid.search()){
            return false;
        }

        CommonAjax.basic({
            url: '/manage/sticker/getStickerApplyList.json?' + $('#stickerApplySearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                stickerApplyListGrid.setData(res);
                $('#stickerApplyTotalCount').html($.jUtil.comma(stickerApplyListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = stickerApplyItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                stickerApplyItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                stickerApplyItemListGrid.gridView.onDataCellClicked();
            }else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        stickerApply.initSearchDate('-30d');

        $('#schDateType').val("REGDT");
        $('#schType').val("itemNm");
        $('#schUseYn').val('');
        $('#schKeyword').val('');

        $("#stickerApplyNm").setTextLength('#textCountKr_keyword');

    },
    /**
     * Item Grid 삭제
     */
    deleteItem : function () {

        let rowArr = stickerApplyItemListGrid.gridView.getCheckedRows(false);

        if (rowArr.length == 0) {
            alert('삭제하려는 상품을 선택해주세요.');
            return false;
        }

        for (let i in rowArr) {
            stickerApplyItemListGrid.dataProvider.removeRow(rowArr[i] - parseInt(i));
        }
        stickerApply.calcStickerApplyItemTotalCnt();
    },
    /**
     * 엑셀 업로드
     */
    excelUpload: function() {
        var popUrl = "/manage/popup/uploadStickerApplyPop";
        var callBackUrl = "stickerApply.applyPopupCallback";

        $.jUtil.openNewPopup(popUrl+"?callBackScript="+callBackUrl, 650, 370);
    },

    /**
     * 공통팝업 콜백 - 선택한 데이터 적용 대상 그리드에 추가
     * @param resDataArr
     */
    applyPopupCallback : function(resDataArr) {

        var resultCount = resDataArr.length;

        if( resultCount > 0 ) {
            var itemCount = stickerApplyItemListGrid.gridView.getItemCount();
            for (var i=0; i<resultCount; i++) {
                var data  = {
                    itemNo : resDataArr[i].itemNo,
                    itemNm : resDataArr[i].itemNm,
                    useYn : 'Y'
                };

                var isExist = stickerApplyItemListGrid.gridView.searchItem({fields: ["itemNo"], values: [resDataArr[i].itemNo], wrap: true});
                if (isExist < 1) {
                    stickerApplyItemListGrid.dataProvider.addRow(data);
                    itemCount++;
                }
            }
        }
        stickerApply.calcStickerApplyItemTotalCnt();

    },

    /**
     * 상품조회팝업
     */
    addItemsPopUp : function(){
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=stickerApply.applyPopupCallback&isMulti=Y' , 1084, 650);
    },
    /**
     * 엑셀 다운로드
     */
    excelDownload : function () {
        stickerApplyItemListGrid.excelDownload();
    },
    /**
     * 스티커 등록/수정
     */
    setStickerApply : function() {

        if(!stickerApply.valid.set()){
            return false;
        }

        var form = $('#stickerApplySetForm').serializeObject();

        //상품추가
        form.itemList = stickerApplyItemListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({
            url:'/manage/sticker/setStickerApply.json' ,
            data: JSON.stringify(form) ,
            method:"POST" ,
            successMsg:null ,
            contentType:"application/json",
            callbackFunc:function(res) {
                alert(res.returnMsg);
                $('#stickerApplyNo').val(res.returnKey);
                $('#stickerApplyNoTxt').text(res.returnKey);

                stickerApply.search();
            }});
    },
    /**
     * 스티커 등록/수정 폼 초기화
     */
    resetForm : function() {

        $('#stickerApplyNo').val('');
        $('#stickerApplyNoTxt').text('');
        $('#stickerApplyNm').val('');
        $('#stickerNo').val('');

        $("#stickerApplyNm").setTextLength('#textCountKr_keyword');
        $('#schGridItemNo').val('');

        $(".chkStickerApplyStoreType").prop("checked", false);

        stickerApply.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");

        $('input:radio[name="useYn"]:input[value="Y"]').trigger('click');

        stickerApplyItemListGrid.dataProvider.clearRows();
        stickerApply.calcStickerApplyItemTotalCnt();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        stickerApply.resetForm();

        var rowDataJson = stickerApplyListGrid.dataProvider.getJsonRow(selectRowId);

        $('#stickerApplyNo').val(rowDataJson.stickerApplyNo);
        $('#stickerApplyNoTxt').text(rowDataJson.stickerApplyNo);
        $('#stickerApplyNm').val(rowDataJson.stickerApplyNm);

        if ($('#stickerNo option[value='+rowDataJson.stickerNo+']').length > 0 ) { // 존재하면
            $('#stickerNo').val(rowDataJson.stickerNo);
        } else {
            $('#stickerNo').val('');
        }

        $('#dispStartDt').val(moment(rowDataJson.dispStartDt).format("YYYY-MM-DD HH:mm:ss"));
        $('#dispEndDt').val(moment(rowDataJson.dispEndDt).format("YYYY-MM-DD HH:mm:ss"));

        $("input:checkbox[id='hyperDispYn']").prop("checked", rowDataJson.hyperDispYn == "Y" ? true : false);
        $("input:checkbox[id='expDispYn']").prop("checked", rowDataJson.expDispYn == "Y" ? true : false);

        $('input:radio[name="useYn"]:input[value="'+rowDataJson.useYn+'"]').trigger('click');

        $("#stickerApplyNm").setTextLength('#textCountKr_keyword');

        CommonAjax.basic({
            url:'/manage/sticker/getStickerApplyItemList.json?stickerApplyNo=' + rowDataJson.stickerApplyNo,
            data:null ,
            method:"GET" ,
            successMsg:null ,
            callbackFunc:function(res) {
                stickerApplyItemListGrid.setData(res);
                stickerApply.calcStickerApplyItemTotalCnt();

            }});
    },
    /**
     * 스티커 관리 데이터 조회
     */
    getSticker : function () {
        CommonAjax.basic({
            url:'/manage/sticker/getStickerForSelectBox.json',
            data: null ,
            method:"GET" ,
            successMsg:null ,
            callbackFunc:function(resData) {
                $.each(resData, function (idx, val) {
                    $('#stickerNo').append('<option value="' + val.stickerNo + '" '+'>' + val.stickerNm + '</option>');
                });
                $('#stickerNo').css('-webkit-padding-end','30px');
            }});
    },
    /**
     * 스티커 적용 상품 total 수량
     */
    calcStickerApplyItemTotalCnt : function () {
        $('#stickerApplyItemTotalCount').html(
            $.jUtil.comma(stickerApplyItemListGrid.gridView.getItemCount()));
    }

};

/**
 * 스티커관리 유효성 체크
 * @type {{search: stickerApply.valid.search, set: stickerApply.valid.set}}
 */
stickerApply.valid = {

    search : function () {
        // 날짜 체크
        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");

        if (!$.jUtil.isEmpty(startDt.val()) || (!$.jUtil.isEmpty(endDt.val()))) {
            if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(
                endDt.val(), 'YYYY-MM-DD', true).isValid()) {
                alert("조회기간 날짜 형식이 맞지 않습니다");
                $("#schStartDt").focus();
                return false;
            }
            if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
                alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
                startDt.val(
                    moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
                return false;
            }

            if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
                alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
                startDt.val(endDt.val());
                return false;
            }
        }

        let schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
            if(schKeyword.length < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#schKeyword').focus();
                return false;
            }
        }
        return true;

    },
    set : function(){

        if ($.jUtil.isEmpty($('#stickerApplyNm').val()))  {
            alert("관리명을 입력해주세요.")
            $('#stickerApplyNm').focus();
            return false;
        }

        if($("input:checkbox[class=chkStickerApplyStoreType]:checked").length == 0) {
            alert("잠포유형을 선택해주세요.");
            return false;
        }

        if ($.jUtil.isEmpty($('#stickerNo').val()))  {
            alert("스티커명을 선택해주세요.")
            $('#stickerNo').focus();
            return false;
        }

        return true;
    }

};