$(document).ready(function () {
    dspMainThemeItemMng.init();
    dspMainThemeListGrid.init();
    dspMainThemeItemListGrid.init();
    CommonAjaxBlockUI.targetId('searchBtn');
});

// dspMainTheme 그리드
var dspMainThemeListGrid = {
    gridView : new RealGridJS.GridView("dspMainThemeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspMainThemeListGrid.initGrid();
        dspMainThemeListGrid.initDataProvider();
        dspMainThemeListGrid.event();
    },
    initGrid : function() {
        dspMainThemeListGrid.gridView.setDataSource(dspMainThemeListGrid.dataProvider);
        dspMainThemeListGrid.gridView.setStyles(dspMainThemeGridBaseInfo.realgrid.styles);
        dspMainThemeListGrid.gridView.setDisplayOptions(dspMainThemeGridBaseInfo.realgrid.displayOptions);
        dspMainThemeListGrid.gridView.setColumns(dspMainThemeGridBaseInfo.realgrid.columns);
        dspMainThemeListGrid.gridView.setOptions(dspMainThemeGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspMainThemeListGrid.dataProvider.setFields(dspMainThemeGridBaseInfo.dataProvider.fields);
        dspMainThemeListGrid.dataProvider.setOptions(dspMainThemeGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspMainThemeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspMainTheme.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspMainThemeListGrid.dataProvider.clearRows();
        dspMainThemeListGrid.dataProvider.setRows(dataList);
    }
};

var dspMainTheme = {
    gridRowSelect: function (selectRowId) {
        var rowDataJson = dspMainThemeListGrid.dataProvider.getJsonRow(selectRowId);
        var siteType = $('input[name="siteType"]:checked').val();

        $('#themeId').val(rowDataJson.themeId);

        dspMainThemeItemMng.search(rowDataJson.themeId);
    }
};

var dspMainThemeItemListGrid = {
    gridView: new RealGridJS.GridView("dspMainThemeItemListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        dspMainThemeItemListGrid.initGrid();
        dspMainThemeItemListGrid.initDataProvider();
        dspMainThemeItemListGrid.event();
    },
    initGrid: function () {
        dspMainThemeItemListGrid.gridView.setDataSource(dspMainThemeItemListGrid.dataProvider);

        dspMainThemeItemListGrid.gridView.setStyles(dspMainThemeItemGridBaseInfo.realgrid.styles);
        dspMainThemeItemListGrid.gridView.setDisplayOptions(dspMainThemeItemGridBaseInfo.realgrid.displayOptions);
        dspMainThemeItemListGrid.gridView.setColumns(dspMainThemeItemGridBaseInfo.realgrid.columns);
        dspMainThemeItemListGrid.gridView.setOptions(dspMainThemeItemGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        dspMainThemeItemListGrid.dataProvider.setFields(dspMainThemeItemGridBaseInfo.dataProvider.fields);
        dspMainThemeItemListGrid.dataProvider.setOptions(dspMainThemeItemGridBaseInfo.dataProvider.options);
    },
    event: function () {
        // 그리드 선택
        dspMainThemeItemListGrid.gridView.onDataCellClicked = function (gridView, index) {
            if($.jUtil.isNotEmpty(index)) {
                dspMainThemeItemMng.getGrid(index.dataRow);
            }
        };
    },
    setData: function (dataList) {
        dspMainThemeItemListGrid.dataProvider.clearRows();
        dspMainThemeItemListGrid.dataProvider.setRows(dataList);
    }
};

var dspMainThemeItemMng = {
    year: null,             // 출력 달의 년(YYYY)
    month: null,            // 출력 달의 월(M/MM)
    firstDayOfWeek: null,   // 출력 달의 1일자 요일
    today: null,            // 오늘 날짜 (YYYY-MM-DD)
    selectTdId: null,       // 선택한 td id
    dispDt: null,           // 전시날짜
    isUpdate: false,
    dspMainThemeItemList : {},    // 아이템 리스트

    init: function () {
        this.bindingEvent();
        this.initSetDate();     // today date setting
        this.getCalendarData(); // display calendar data
    },
    /**
     * 오늘 년,월,일 조회 후 세팅
     */
    initSetDate: function() {
        var date = $.jUtil.getCurrentDate();
        var tmpDate = date.split("-");

        dspMainThemeItemMng.year = tmpDate[0];
        dspMainThemeItemMng.month = tmpDate[1];
        dspMainThemeItemMng.today = date;
    },
    bindingEvent: function () {
        $('#searchBtn').bindClick(dspMainThemeItemMng.search);
        $('#searchResetBtn').bindClick(dspMainThemeItemMng.searchFormReset);
        $('#setDspMainThemeItem').bindClick(dspMainThemeItemMng.setDspMainThemeItem);
        $('#resetDspMainThemeItem').bindClick(dspMainThemeItemMng.resetForm);
        // $('#linkType').bindChange(dspMainThemeItemMng.chgLinkInfo);
        $('#dispKind').bindChange(dspMainThemeItemMng.chgDispKind);
        $('#addItemPopUp').bindClick(dspMainThemeItemMng.addItemPopUp);
        $('#addSellerItemPopUp').bindClick(dspMainThemeItemMng.addSellerItemPopUp);
        $('#sortPriority').bindClick(dspMainThemeItemMng.sortDspMainThemeItem);
        $("input:radio[name=themeItemType]").bindChange(dspMainThemeItemMng.changeThemeItemType);
        $('#linkType').bindChange(dspMainThemeItemMng.changeLinkType);
        $('#schGridItemBtn').bindClick(dspMainThemeItemMng.schGridItemNo);

        $(document).on('change','input[name="fileArr"]',function(){
            dspMainThemeItemMng.img.addImg();
        });

        //이미지 등록 버튼
        $(document).on('click','.addImgBtn',function(){
            dspMainThemeItemMng.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();
        });

        //이미지 삭제 버튼
        $(document).on('click','.deleteImgBtn',function(){
            if(dspMainThemeItemMng.isUpdate) {
                alert('등록된 배너는 수정이 불가능 합니다.');
                return;
            }
            dspMainThemeItemMng.img.setImg($(this).next(), null, true);
        });

        // 사이트 구분 변경시
        $('input[name="siteType"]').change(function () {
            dspMainThemeItemMng.resetCalendar();
            dspMainThemeItemMng.setDispStoreType($(this));
            $('#themeId').val('');
            dspMainThemeListGrid.dataProvider.clearRows();
            dspMainThemeItemListGrid.dataProvider.clearRows();
            dspMainThemeItemList = new Array(); // 상품 리스트
            $("#"+dspMainThemeItemMng.dispDt).removeAttr("style");
            dspMainThemeItemMng.dispDt = null;
            dspMainThemeItemMng.resetForm();
        });

        // 스토어 구분 변경시
        $('#storeType').on('change', function(){
            dspMainThemeItemMng.setItemStoreType($(this));
        });

        // click event binding ( calandar )
        $("#prevMonth").bindClick(dspMainThemeItemMng.clickEventBinding, "prevMonth");    // [이전 달] 버튼
        $("#nextMonth").bindClick(dspMainThemeItemMng.clickEventBinding, "nextMonth");    // [다음 달] 버튼
        $("#prevYear").bindClick(dspMainThemeItemMng.clickEventBinding, "prevYear");      // [이전 년도] 버튼
        $("#nextYear").bindClick(dspMainThemeItemMng.clickEventBinding, "nextYear");      // [다음 년도] 버튼
        $("#saveBtn").bindClick(dspMainThemeItemMng.resetBusinessDay);                           // [초기화] 버튼
    },
    downloadTemplate: function() {
        var fileUrl = "";
        var applyScope = $("#applyScope").val();
        switch (applyScope) {
            case "ITEM":
                fileUrl = "/static/templates/itemCoupon_template.xlsx";
                break;
            case "SELL":
                fileUrl = "/static/templates/sellerCoupon_template.xlsx";
                break;
        }

        if(fileUrl != "") {
            location.href = fileUrl;
        } else {
            alert("양식 파일이 없습니다.");
            return false;
        }
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = dspMainThemeItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                dspMainThemeItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
                dspMainThemeItemListGrid.gridView.onDataCellClicked();
            }else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var storeType = $('#storeType').val();
        var siteType  = $('input[name="siteType"]:checked').val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(storeType === 'TD') {
            if(siteType == 'HOME') {
                $('#storeType').append('<option value="DS">셀러상품</option>');
                $dispStore.prepend($('#storeTypeHome').html());
            } else {
                $('#storeType option[value="DS"]').remove();
                $dispStore.prepend($('#storeTypeClub').html());
            }
        } else {
            $dispStore.prepend($('#storeTypeNone').html());
        }
        $dispStore.find('select').attr('name' ,'itemStoreType');
        $dispStore.show();
    },

    /**
     * 노출 상품 구분 영역 세팅
     */
    setItemStoreType : function(obj) {
        var storeType = $('#storeType').val();
        var siteType  = $('input[name="siteType"]:checked').val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(storeType === 'TD') {
            if(siteType == 'HOME') {
                // $('#storeType').append('<option value="DS">셀러상품</option>');
                $dispStore.prepend($('#storeTypeHome').html());
            } else {
                $('#storeType option[value="DS"]').remove();
                $dispStore.prepend($('#storeTypeClub').html());
            }
        } else {
            $dispStore.prepend($('#storeTypeNone').html());
        }
        $dispStore.find('select').attr('name' ,'itemStoreType');
        $dispStore.show();
    },
    /**
     * click event binding
     */
    clickEventBinding: function(gubun) {
        switch (gubun) {
            case "prevMonth" :  // [이전 달] 버튼
                if(dspMainThemeItemMng.year === 2000 && dspMainThemeItemMng.month === 1){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return false;
                }
                if(dspMainThemeItemMng.month === 1) { // [이전 달] 1월일 경우 전 년도 12월로 바꿔준다.
                    dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year - 1, 12);
                } else {
                    dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year, dspMainThemeItemMng.month - 1);
                }
                break;
            case "nextMonth" :  // [다음 달] 버튼
                if(dspMainThemeItemMng.month === 12) { // [다름 달] 12월일 경우 다음 년도 1월로 바꿔준다.
                    dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year + 1, 1);
                } else {
                    dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year, dspMainThemeItemMng.month + 1);
                }
                break;
            case "prevYear"  :  // [이전 년도] 버튼
                if(dspMainThemeItemMng.year === 2000){
                    alert('2000년 1월 1일까지만 조회가 가능합니다.');
                    return;
                }
                dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year - 1, dspMainThemeItemMng.month);
                break;
            case "nextYear"  :  // [다음 년도] 버튼
                dspMainThemeItemMng.getCalendarData(dspMainThemeItemMng.year + 1, dspMainThemeItemMng.month);
                break;
        }
    },
    /**
     * 캘린더 초기화
     */
    resetCalendar: function() {
        $("#mainThemeItemCalendar > tbody").empty();
    },
    /**
     * 화면 출력용 캘린더 데이터 조회
     */
    getCalendarData: function (year, month) {
        // 전달 파라미터가 비어있으면 오늘 날짜 기준으로 조회
        if($.jUtil.isEmpty(year) && $.jUtil.isEmpty(month)) {
            year = dspMainThemeItemMng.year;
            month = dspMainThemeItemMng.month;
        }

        CommonAjax.basic({
            url         : "/escrow/businessDay/getCalendarDate.json?schYear=" + year + "&schMonth="+ month,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                console.log(res);
                if (res.length == 0) {
                    alert("정상적으로 조회되지 않았습니다.\n잠시 후에 다시 시도해 주시길 바랍니다.");
                    return;
                }
                // 캘린더 초기화
                dspMainThemeItemMng.resetCalendar();

                // 전역 date 변수 setting
                dspMainThemeItemMng.year = res.year;
                dspMainThemeItemMng.month = res.month;
                dspMainThemeItemMng.today = res.today;
                dspMainThemeItemMng.firstDayOfWeek = res.firstDayOfWeek;

                // 캘린더 상단 년도,월 출력
                $("#curYear").html(res.year);
                $("#curMonth").html(res.month);

                // 캘린더 draw
                dspMainThemeItemMng.drawCalendar(res.dayList);

                // 기존 선택된 영업일 정보 음영처리
                if(!$.jUtil.isEmpty(dspMainThemeItemMng.selectTdId)) {
                    $("#" + dspMainThemeItemMng.selectTdId).css('background-color', '#828282');
                }

                // 일자별 상품 수 그리기.
                dspMainThemeItemMng.getMainThemeItemCntList();
            }
        })
    },
    /**
     * 캘린더 출력을 위한 태그 create
     */
    drawCalendar: function(data) {
        var tags = '';
        var weekend = 1;
        var dateCnt = 1;

        // '1일' 그리기 전, 전 달 여백 채울 태그 create
        var firstBlankTags = '';
        var firstBlankDateCnt = 0;
        for (firstBlankDateCnt; firstBlankDateCnt<dspMainThemeItemMng.firstDayOfWeek-1; firstBlankDateCnt++) {
            firstBlankTags += "<td></td>";
        }

        for (var i in data) {
            // N주차 데이터 그리기... start
            if (dateCnt % 7 === 1) {
                tags += "<tr name=" + weekend + "week>";
                // 1주차 일 때, 전 달 여백 채울 태그 draw
                if (weekend === 1) {
                    dateCnt = dspMainThemeItemMng.firstDayOfWeek;
                    tags += firstBlankTags;
                }
            }
            // 일자 draw
            tags += "<td id=" + data[i].date + " onclick='dspMainThemeItemMng.displayMainThemeItemDay($(this));'>";
            // 평일은 검정색, 주말은 빨간색으로 draw
            if(data[i].dayOfWeek === 6 || data[i].dayOfWeek === 7) {
                tags += "<div class='dateArea text-red'>";
            } else {
                tags += "<div class='dateArea'>";
            }
            tags += data[i].day;

            // N주차 데이터 그리기... 종료
            if (dateCnt % 7 === 0) {
                tags += "</tr>";
                weekend++;
            }
            dateCnt++;
        }
        // 마지막 일자 그린 후, 캘린더 여백 채울 태그 create
        var lastMarginCnt = (weekend*7) - (firstBlankDateCnt+data.length);
        for (var lastBlankDateCnt=0; lastBlankDateCnt<lastMarginCnt; lastBlankDateCnt++) {
            tags += "<td></td>";
        }

        // 최종 draw
        $("#mainThemeItemCalendar > tbody").append(tags);
    },
    /**
     * 캘린더에서 클릭 시, 상세 정보 하단에 노출
     * @param selectDay
     */
    displayMainThemeItemDay: function ($selectDate) {
        // 하단 영업일 정보 영역 초기화
        $("#" + dspMainThemeItemMng.selectTdId).removeAttr("style");

        // 선택된 td의 id 저장
        dspMainThemeItemMng.selectTdId = $selectDate.attr("id");
        $selectDate.css('background-color', '#828282');

        // 선택한 날짜 선택
        dspMainThemeItemMng.dispDt = $selectDate.attr("id");

        // 홈테마 아이템 리스트 초기화
        dspMainThemeItemList = new Array();

        // 홈테마 리스트 채우기.
        dspMainThemeItemMng.searchMainTheme();
    },
    /**
     * 일자별 상품수 조회
     */
    getMainThemeItemCntList: function() {
        var dispDate = dspMainThemeItemMng.year + '-' + dspMainThemeItemMng.month;
        var siteType = $('input[name="siteType"]:checked').val();

        if(dspMainThemeItemMng.month < 10) {
            dispDate = dspMainThemeItemMng.year + '-' + '0' + dspMainThemeItemMng.month;
        }

        CommonAjax.basic({
            url         : "/manage/dspMain/getDspMainItemCnt.json?dispDate=" + dispDate + "&siteType=" + siteType,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                if (res.length == 0) {
                    return;
                }
                dspMainThemeItemMng.drawDspMainThemeItemCntList(res);
            }
        });
    },
    /**
     * 일자별 상품수 리스트 draw
     */
    drawDspMainThemeItemCntList: function(data) {
        for(var i in data) {
            dspMainThemeItemMng.drawDspMainThemeItemCnt(data[i]);
        }
    },
    /**
     * 영업일 정보 draw
     */
    drawDspMainThemeItemCnt: function(data) {
        var dataDispDt = data.dispDt;
        if ($.jUtil.isEmpty(dataDispDt)) {
            return;
        }

        $('<div>', {
            text: data.itemCnt,
            css: {
                backgroundColor: '#f54040',
                padding: '3px 5px',
                marginBottom: '3px',
                color: '#fff'
            }
        }).appendTo($("#"+dataDispDt));
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeThemeItemType : function() {
        var themeItemType = $("input:radio[name=themeItemType]:checked").val();

        switch (themeItemType) {
            case "ITEM" :
                $('#itemTr').show();
                $('#itemNo, #itemNm').val('');
                $('#linkTr').hide();
                $('.bannerDiv, #setDspMainThemeItem').hide();
                break;
            case "BANNER" :
                $('#itemTr').hide();
                $('#linkInfoDiv').html($('#linkCommon').html());
                $('#linkTr').show();
                $('.bannerDiv, #setDspMainThemeItem').show();
                break;
        }
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function() {
        var linkType = $("#linkType").val();

        switch (linkType) {
            case "EXH" :
            case "EVENT" :
                $("#linkInfoDiv").html($('#linkCommon').html());
                break;
            case "URL" :
                $("#linkInfoDiv").html($('#linkUrl').html());
                break;
            default :
                $("#linkInfoDiv").html($('#linkNone').html());
                break;
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function() {
        var linkType  = $('#linkType').val();
        switch (linkType) {
            case "EXH"  :
                dspMainThemeItemMng.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                dspMainThemeItemMng.addPromoPopUp('EVENT');
                break;
        }
    },
    /**
     * 포로모션 조회팝업
     */
    addPromoPopUp : function(promoType){
        var url = '/common/popup/promoPop?callback=dspMainThemeItemMng.addPromoPopUpCallBack&isMulti=N&siteType=' + $("#siteType").val()+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백
     */
    addPromoPopUpCallBack : function(resDataArr) {
        $('#linkInfoDiv').find('.linkInfo').val(resDataArr[0].promoNo);
        $('#linkInfoDiv').find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    search: function (themeId) {
        CommonAjax.basic({
            url: '/manage/dspMain/getDspMainThemeItemList.json',
            data: { themeId : themeId, dispDt : dspMainThemeItemMng.dispDt },
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                dspMainThemeItemListGrid.setData(res);
                dspMainThemeItemList = new Array();
                res.forEach(function (data, index) {
                    dspMainThemeItemList[index] = {
                        themeId : data.themeId
                        , dispDt : data.dispDt
                        , itemNo : data.itemNo
                        , itemNm : data.itemNm
                        , dispYn : data.dispYn
                        , dispYnTxt : data.dispYnTxt
                        , priority : data.priority
                    }
                });

                $('#mainThemeItemTotalCount').html(dspMainThemeItemListGrid.gridView.getItemCount());
                dspMainThemeItemMng.resetForm();
            }
        });
    },
    searchMainTheme: function () {
        var siteType = $('input[name="siteType"]:checked').val();

        CommonAjax.basic({
            url: '/manage/dspMain/getDspMainTheme.json',
            data: { dispDt : dspMainThemeItemMng.dispDt, siteType : siteType },
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                $('#themeId').val('');
                dspMainThemeListGrid.setData(res);
                $('#mainThemeItemTotalCount').html(0);
                dspMainThemeItemMng.resetForm();
                dspMainThemeItemListGrid.dataProvider.clearRows();
            }
        });
    },
    resetForm: function () {
        dspMainThemeItemMng.isUpdate = false;
        $("input:radio[name='themeItemType']").prop('disabled', false);
        $("input:radio[name='themeItemType']:radio[value='ITEM']").trigger('click');

        var siteType = $('input[name="siteType"]:checked').val();

        $('#itemNo, #itemNm, #itemSeq').val('');
        $('#dispYn').val('Y');
        $('select[name="storeType"]').attr('disabled', false);
        $('select[name="itemStoreType"]').attr('disabled', false);
        $('#storeType').val('TD').trigger('change');
        if(siteType === 'HOME') {
            $('#itemStoreType').val('HYPER');
        } else {
            $('#itemStoreType').val('CLUB');
        }
        $('#storeType').val('TD');
        dspMainThemeItemMng.getCalendarData();

        $('#linkTr').find('select').val('EXH').trigger('change');
        dspMainThemeItemMng.img.init();
    },
    getGrid: function (selectRowId) {
        var rowDataJson = dspMainThemeItemListGrid.dataProvider.getJsonRow(selectRowId);
        var themeItemType = rowDataJson.themeItemType;

        // $("input:radio[name='themeItemType']").prop('disabled', false);

        $('#itemSeq').val(rowDataJson.itemSeq);

        dspMainThemeItemMng.isUpdate = true;

        switch (themeItemType) {
            case 'ITEM' :
                $("input:radio[name='themeItemType']:radio[value='ITEM']").prop('checked', true).trigger('change');
                $('#storeType').val(rowDataJson.mallType).trigger('change');
                break;
            case 'BANNER' :
                $("input:radio[name='themeItemType']:radio[value='BANNER']").prop('checked', true).trigger('change');

                $('.imgDisplayView').find('.imgUrl').val()
                $('.imgDisplayView').find('.imgWidth').val()
                $('.imgDisplayView').find('.imgHeight').val()

                $('#linkType').val(rowDataJson.linkType).trigger('change');;
                $('#linkInfoDiv').find('.linkInfo').val(rowDataJson.linkInfo);
                $('#linkInfoDiv').find('.linkInfoNm').val(rowDataJson.dispNm);
                $('#dispYn').val(rowDataJson.dispYn);

                dspMainThemeItemMng.img.setImg($('.bannerDiv').find('.itemImg'), {
                    'imgUrl'   : rowDataJson.imgUrl,
                    'imgWidth' : rowDataJson.imgWidth,
                    'imgHeight': rowDataJson.imgHeight,
                    'src'      : hmpImgUrl + "/provide/view?processKey=HomeThemeMobileBanner&fileId=" + rowDataJson.imgUrl,
                });

                break;
        }

        $("input:radio[name='themeItemType']").prop('disabled', true);
    },
    setDspMainThemeItem : function() {
        if(!dspMainThemeItemMng.setValid()) {
            return false;
        }

        var themeItemType = $("input:radio[name=themeItemType]:checked").val();

        dspMainThemeItemList = new Array();

        switch (themeItemType) {
            case 'ITEM' :
                dspMainThemeItemList[0] = {
                    itemSeq : $('#itemSeq').val()
                    , dispDt : dspMainThemeItemMng.dispDt
                    , themeItemType : themeItemType
                    , themeId : $('#themeId').val()
                    , itemNo : $('#itemNo').val()
                    , imgUrl : ''
                    , imgWidth : 0
                    , imgHeight : 0
                    , linkType : ''
                    , linkInfo : ''
                    , dispYn : 'Y'
                    , itemStoreType : $('select[name="itemStoreType"]').val()
                };
                break;
            case 'BANNER' :
                dspMainThemeItemList[0] = {
                    itemSeq : $('#itemSeq').val()
                    , dispDt : dspMainThemeItemMng.dispDt
                    , themeItemType : themeItemType
                    , themeId : $('#themeId').val()
                    , itemNo : ''
                    , imgUrl : $('.imgDisplayView').find('.imgUrl').val()
                    , imgWidth : $('.imgDisplayView').find('.imgWidth').val()
                    , imgHeight : $('.imgDisplayView').find('.imgHeight').val()
                    , linkType : $('#linkType').val()
                    , linkInfo : $('#linkInfoDiv').find('.linkInfo').val()
                    , dispYn : 'Y'
                    , itemStoreType : $('select[name="itemStoreType"]').val()
                };
                break;
        }

        CommonAjax.basic({
            url : '/manage/dspMain/setDspMainThemeItem.json',
            data : JSON.stringify(dspMainThemeItemList),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainThemeItemMng.search($('#themeId').val());
            }
        });
    },
    /**
     * 전시여부 변경
     */
    chgDispYn : function (action) {
        var checkedRows = dspMainThemeItemListGrid.gridView.getCheckedRows(false);
        var setUseYn, setUseYnNm;

        if (checkedRows.length == 0) {
            alert("전시여부 변경할 상품을 선택해 주세요.");
            return false;
        }

        switch (action) {
            case 'Y' :
                setDispYn = 'Y';
                setDispYnNm = '전시';
                break;
            case 'N' :
                setDispYn = 'N';
                setDispYnNm = '전시안함';
                break;
        }

        // RealGrid 값 변경.
        var setGridValue = { dispYn : setDispYn, dispYnTxt : setDispYnNm };
        checkedRows.forEach(function(data) {
            dspMainThemeItemListGrid.gridView.setValues(data, setGridValue, true);
        });

        // BackData 전시여부 값 갱신.
        dspMainThemeItemList.forEach(function (data, index) {
            var itemNo = dspMainThemeItemListGrid.gridView.getValue(index, 0);
            var itemSeq = dspMainThemeItemListGrid.gridView.getValue(index, 5);
            var dispYn = dspMainThemeItemListGrid.gridView.getValue(index, 7);
            var itemStoreType = dspMainThemeItemListGrid.gridView.getValue(index, 8);

            dspMainThemeItemList[index] = {
                itemSeq : itemSeq
                , themeId : $('#themeId').val()
                , dispDt : dspMainThemeItemMng.dispDt
                , itemNo : itemNo
                , dispYn : dispYn
                , itemStoreType : itemStoreType
                , priority : parseInt(index) + 1
            }
        });

        CommonAjax.basic({
            url : '/manage/dspMain/setDspMainItemThemeDisplay.json',
            data : JSON.stringify(dspMainThemeItemList),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainThemeItemMng.resetForm();
                dspMainThemeItemMng.search($('#themeId').val());
            }
        });
    },
    sortDspMainThemeItem : function() {
        if($.jUtil.isEmpty(dspMainThemeItemMng.dispDt)) {
            alert('변경할 상품이 없습니다.');
            return false;
        }

        CommonAjax.basic({
            url : '/manage/dspMain/setDspMainThemeItem.json',
            data : JSON.stringify(dspMainThemeItemList),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainThemeItemMng.resetForm();
                dspMainThemeItemMng.search($('#themeId').val());
            }
        });
    },
    /**
     * dspMainThemeItemListGrid 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = dspMainThemeItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(dspMainThemeItemListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬
        dspMainThemeItemList.forEach(function (data, index) {
            var itemNo = dspMainThemeItemListGrid.gridView.getValue(index, 0);
            var itemSeq = dspMainThemeItemListGrid.gridView.getValue(index, 5);
            var itemStoreType = dspMainThemeItemListGrid.gridView.getValue(index, 8);

            dspMainThemeItemList[index] = {
                itemSeq : itemSeq
                , themeId : $('#themeId').val()
                , dispDt : dspMainThemeItemMng.dispDt
                , itemNo : itemNo
                , itemStoreType : itemStoreType
                , priority : parseInt(index) + 1
            }
        });
    },
    /**
     * 링크 유형별 노출변경
     */
    chgLinkInfo : function() {
        // 입력 폼 Reset
        $('#setLinkInfo').val('');

        // Type에 따른 노출 입력폼 변경
        switch ($('#linkType option:selected').val()) {
            case 'ITEM' :
                $('#selectTextBox').hide();
                break;
            default :
                $('#selectTextBox').show();
                break;
        }
    },
    /**
     * 배너 노출 구분
     */
    chgDispKind : function() {
        // 입력 폼 Reset
        $("input:radio[name='bannerType']:radio[value='A']").prop('checked', true);
        $("input:radio[name='titleUseYn']:radio[value='Y']").prop('checked', true);
        $('#mainTitleNm, #subTitleNm').val('');

        // Type에 따른 노출 입력폼 변경
        switch ($('#dispKind option:selected').val()) {
            case 'PC' :
                $('#bannerTypePc, #bannerTitlePc').show();
                $('#inputTagNm').html('배너 (PC) <span class="text-red">*</span>');
                break;
            default :
                $('#bannerTypePc, #bannerTitlePc').hide();
                $('#inputTagNm').html('배너 (MOBILE) <span class="text-red">*</span>');
                break;
        }
    },
    /**
     * 상품조회팝업 ( 점포 )
     */
    addItemPopUp : function() {
        if(dspMainThemeItemMng.isUpdate) {
            alert('이미 등록된 상품은 상품 수정이 불가능 합니다.');
            return;
        }

        var storeType = $('#storeType').val();
        var siteType = $('input[name="siteType"]:checked').val();

        switch (storeType) {
            case 'TD' :
                if(siteType === 'HOME') {
                    $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspMainThemeItemMng.setItemInfo&isMulti=Y&mallType=TD&storeType=HYPER', 1084, 650);
                } else {
                    $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspMainThemeItemMng.setItemInfo&isMulti=Y&mallType=TD&storeType=CLUB', 1084, 650);
                }
                break;
            case 'DS' :
                $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspMainThemeItemMng.setItemInfo&isMulti=Y&mallType=DS&storeType=DS', 1084, 650);
                break;
        }
    },
    /**
     * 상품 업로드 팝업 ( 엑셀 )
     */
    openItemReadPopup : function () {
        var target = "mainThemeItemPop";
        var targetDisp = dspMainThemeItemMng.dispDt;
        var callback = "dspMainThemeItemMng.itemExcelPopCallback";
        var url = "/manage/popup/uploadMainThemeItemPop?callback=" + callback + "&targetDisp=" + targetDisp;

        if($.jUtil.isEmpty(targetDisp)) {
            alert('등록일자가 존재하지 않습니다.');
            return;
        }

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function () {
        dspMainThemeItemMng.search(dspMainThemeListGrid.dataProvider.getJsonRow(0).themeId);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        dspMainThemeItemList = new Array();
        resData.forEach(function (data, index) {
            dspMainThemeItemList[index] = {
                themeId : $('#themeId').val()
                , dispDt : dspMainThemeItemMng.dispDt
                , itemNo : data.itemNo
                , itemStoreType : $('select[name="itemStoreType"]').val()
                , themeItemType : 'ITEM'
                , dispYn : 'Y'
                , imgUrl : ''
            }
        });

        CommonAjax.basic({
            url : '/manage/dspMain/setDspMainThemeItemByMulti.json',
            data : JSON.stringify(dspMainThemeItemList),
            method : "POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainThemeItemMng.resetForm();
                dspMainThemeItemMng.search($('#themeId').val());
            }
        });
    },
    setValid: function () {
        var themeItemType = $("input:radio[name=themeItemType]:checked").val();

        if($.jUtil.isEmpty(dspMainThemeItemMng.dispDt)) {
            alert("전시날짜를 선택해주세요.");
            return false;
        } if($.jUtil.isEmpty($('#themeId').val())) {
            alert("등록하실 테마를 먼저 선택해 주세요.");
            return false;
        }

        switch (themeItemType) {
            case 'ITEM' :
                if($.jUtil.isEmpty($('#itemNo').val())) {
                    alert("상품을 등록해주세요.");
                    return false;
                }
                break;
            case 'BANNER' :
                if($.jUtil.isEmpty($('#linkInfoDiv').find('.linkInfo').val()) && $('#linkType').val() != 'NONE' ) {
                    alert("링크 정보를 등록해 주세요.");
                    return false;
                }
                if($.jUtil.isEmpty($('.imgDisplayView').find('.imgUrl').val())) {
                    alert("배너를 등록해 주세요.");
                    return false;
                }
                break;
        }
        return true;
    }
};

dspMainThemeItemMng.img = {
    imgDiv : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        dspMainThemeItemMng.img.resetImg();
        dspMainThemeItemMng.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){
            dspMainThemeItemMng.img.setImg($(this))
        });
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');
        let params = {
            processKey : 'HomeThemeMobileBanner',
            mode : 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        dspMainThemeItemMng.img.setImg(dspMainThemeItemMng.img.imgDiv, {
                            'imgNm'     : f.fileStoreInfo.fileName,
                            'imgUrl'    : f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'       : hmpImgUrl + "/provide/view?processKey=HomeThemeMobileBanner&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        dspMainThemeItemMng.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        dspMainThemeItemMng.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    }
};