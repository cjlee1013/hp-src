/**
 * 업체관리 > 셀러샵관리 > 셀러샵 등록/수정
 */
$(document).ready(function() {
    dspLeafletCategoryListGrid.init();
    dspLeafletCategory.init();
    CommonAjaxBlockUI.global();
});

// dspLeafletCategory 그리드
var dspLeafletCategoryListGrid = {
  gridView : new RealGridJS.GridView("dspLeafletCategoryListGrid"),
  dataProvider : new RealGridJS.LocalDataProvider(),
  init : function() {
      dspLeafletCategoryListGrid.initGrid();
      dspLeafletCategoryListGrid.initDataProvider();
      dspLeafletCategoryListGrid.event();
  },
  initGrid : function() {
      dspLeafletCategoryListGrid.gridView.setDataSource(dspLeafletCategoryListGrid.dataProvider);
      dspLeafletCategoryListGrid.gridView.setStyles(dspLeafletCategoryListGridBaseInfo.realgrid.styles);
      dspLeafletCategoryListGrid.gridView.setDisplayOptions(dspLeafletCategoryListGridBaseInfo.realgrid.displayOptions);
      dspLeafletCategoryListGrid.gridView.setColumns(dspLeafletCategoryListGridBaseInfo.realgrid.columns);
      dspLeafletCategoryListGrid.gridView.setOptions(dspLeafletCategoryListGridBaseInfo.realgrid.options);
  },
  initDataProvider : function() {
      dspLeafletCategoryListGrid.dataProvider.setFields(dspLeafletCategoryListGridBaseInfo.dataProvider.fields);
      dspLeafletCategoryListGrid.dataProvider.setOptions(dspLeafletCategoryListGridBaseInfo.dataProvider.options);
  },
  event : function() {
      // 그리드 선택
      dspLeafletCategoryListGrid.gridView.onDataCellClicked = function(gridView, index) {
          dspLeafletCategory.gridRowSelect(index.dataRow);
      };
  },
  setData : function(dataList) {
      dspLeafletCategoryListGrid.dataProvider.clearRows();
      dspLeafletCategoryListGrid.dataProvider.setRows(dataList);
  }
};

// dspLeafletCategory 관리
var dspLeafletCategory = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspLeafletCategory.search); //dspLeafletCategory 검색
        $('#searchResetBtn').bindClick(dspLeafletCategory.searchFormReset); //dspLeafletCategory 검색폼 초기화
        $('#setBtn').bindClick(dspLeafletCategory.set); //dspLeafletCategory 등록,수정
        $('#resetBtn').bindClick(dspLeafletCategory.resetForm); //dspLeafletCategory 입력폼 초기화

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            dspLeafletCategory.img.addImg();
        });

        // 카테고리 이미지 (PC) 삭제
        $('#pcDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                dspLeafletCategory.img.deleteImg('pc');
            }
        });

        // 카테고리 이미지 (MOBILE) 삭제
        $('#mobileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                dspLeafletCategory.img.deleteImg('mobile');
            }
        });
    },
    /**
     * dspLeafletCategory 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspLeafletCategory/getDspLeafletCategoryList.json?' + $('#dspLeafletCategorySearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspLeafletCategory.resetForm();
                dspLeafletCategoryListGrid.setData(res);
                $('#dspLeafletCategoryTotalCount').html($.jUtil.comma(dspLeafletCategoryListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schKeyword, #searchUseYn').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspLeafletCategoryListGrid.dataProvider.getJsonRow(selectRowId);

        $('#cateNo').val(rowDataJson.cateNo);
        $('#cateNm').val(rowDataJson.cateNm);
        $('#priority').val(rowDataJson.priority);

        dspLeafletCategory.img.setImg('pc', {
            'imgUrl': rowDataJson.pcImg,
            'src' : hmpImgUrl + "/provide/view?processKey=LeafletCategoryPc&fileId=" + rowDataJson.pcImg,
            'changeYn' : 'Y'
        });

        dspLeafletCategory.img.setImg('mobile', {
            'imgUrl': rowDataJson.mobileImg,
            'src' : hmpImgUrl + "/provide/view?processKey=LeafletCategoryMobile&fileId=" + rowDataJson.mobileImg,
            'changeYn' : 'Y'
        });

        $('#useYn').val(rowDataJson.useYn);
    },
    /**
     * dspLeafletCategory 등록/수정
     */
    set : function() {
        if(!dspLeafletCategory.valid.set()) {
            return false;
        }

        CommonAjax.basic({
            url:'/manage/dspLeafletCategory/setDspLeafletCategory.json',
            data : JSON.stringify($('#dspLeafletCategorySetForm').serializeObject())
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                dspLeafletCategory.resetForm();
                dspLeafletCategory.searchFormReset();
                dspLeafletCategory.search();
            }
        });
    },
    /**
     * dspLeafletCategory 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#cateNo, #cateNm, #priority, #useYn').val('');
        $('#useYn').val('Y');
        dspLeafletCategory.img.init();
    }
};

/**
 * dspLeafletCategory 검색,입력,수정 validation check
 */
dspLeafletCategory.valid = {
    set : function () {
        if($.jUtil.isEmpty($('#cateNm').val())) {
            $('#cateNm').focus();
            alert('카테고리명을 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#priority').val())) {
            $('#priority').focus();
            alert('우선순위를 입력해 주세요.');
            return false;
        }
        if($('#priority').val() === "0" && $('#cateNo').val() !== "0") {
            $('#priority').focus();
            alert('우선순위는 0이 될 수 없습니다.');
            return false;
        }
        if($.jUtil.isEmpty($('#pcImg').val())) {
            alert('카테고리 이미지를 등록해 주세요. ( PC )');
            return false;
        }
        if($.jUtil.isEmpty($('#mobileImg').val())) {
            alert('카테고리 이미지를 등록해 주세요. ( MOBILE )');
            return false;
        }
        if($('#cateNo').val() === "0") {
            $('#priority').val("0");
            $('#useYn').val("Y");
        }
        return true;
    }
};

/**
 * 셀러샵관리 이미지
 */
dspLeafletCategory.img = {
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        dspLeafletCategory.img.setImg('pc', null);
        dspLeafletCategory.img.setImg('mobile', null);
        commonEditor.inertImgurl = null;
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'pc':
                if (params) {
                    $('#categoryPcImgUrlTag').prop('src', params.src);
                    $('#pcImg').val(params.imgUrl);
                    $('#pcImgWidth').val(params.width);
                    $('#pcImgHeight').val(params.height);
                    $('#pcImgChangeYn').val(params.changeYn);

                    $('.categoryPcDisplayView').show();
                    $('.categoryPcDisplayView').siblings('.pcDisplayReg').hide();
                } else {
                    $('#categoryPcImgUrlTag').prop('src', '');
                    $('#pcImg').val('');
                    $('#pcImgWidth').val('');
                    $('#pcImgHeight').val('');
                    $('#pcImgChangeYn').val('N');

                    $('.categoryPcDisplayView').hide();
                    $('.categoryPcDisplayView').siblings('.pcDisplayReg').show();
                }
                break;
            case 'mobile':
                if (params) {
                    $('#categoryMobileImgUrlTag').prop('src', params.src);
                    $('#mobileImg').val(params.imgUrl);
                    $('#mobileImgWidth').val(params.width);
                    $('#mobileImgHeight').val(params.height);
                    $('#mobileImgChangeYn').val(params.changeYn);

                    $('.categoryMobileDisplayView').show();
                    $('.categoryMobileDisplayView').siblings('.mobileDisplayReg').hide();
                } else {
                    $('#categoryMobileImgUrlTag').prop('src', '');
                    $('#mobileImg').val('');
                    $('#mobileImgWidth').val('');
                    $('#mobileImgHeight').val('');
                    $('#mobileImgChangeYn').val('N');

                    $('.categoryMobileDisplayView').hide();
                    $('.categoryMobileDisplayView').siblings('.mobileDisplayReg').show();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#imageFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#imageFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#imageFile');
        let ext = $('#imageFile').get(0).files[0].name.split('.');
        let params = {
            processKey : 'LeafletCategoryPc',
            mode : 'IMG'
        };

        if(dspLeafletCategory.img.setImgType === 'mobile') {
            params.processKey = 'LeafletCategoryMobile';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        dspLeafletCategory.img.setImg(dspLeafletCategory.img.setImgType, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        dspLeafletCategory.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        dspLeafletCategory.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};