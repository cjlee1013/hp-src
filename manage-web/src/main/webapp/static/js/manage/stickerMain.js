/**
 * 사이트관리 > 스티커관리 
 */
$(document).ready(function() {
    stickerListGrid.init();
    sticker.init();
    CommonAjaxBlockUI.global();

});

// 스티커 리스트 그리드
var stickerListGrid ={

    gridView : new RealGridJS.GridView("stickerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        stickerListGrid.initGrid();
        stickerListGrid.initDataProvider();
        stickerListGrid.event();
    },
    initGrid : function() {

        stickerListGrid.gridView.setDataSource(stickerListGrid.dataProvider);

        stickerListGrid.gridView.setStyles(stickerListGridBaseInfo.realgrid.styles);
        stickerListGrid.gridView.setDisplayOptions(stickerListGridBaseInfo.realgrid.displayOptions);
        stickerListGrid.gridView.setColumns(stickerListGridBaseInfo.realgrid.columns);
        stickerListGrid.gridView.setOptions(stickerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        stickerListGrid.dataProvider.setFields(stickerListGridBaseInfo.dataProvider.fields);
        stickerListGrid.dataProvider.setOptions(stickerListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        stickerListGrid.dataProvider.clearRows();
        stickerListGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        stickerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            sticker.gridRowSelect(index.dataRow);
        };
    },
};

//스티커관리
var sticker = {
    init : function() {

        sticker.bindingEvent();
        sticker.initSearchDate('-30d');
        sticker.img.init();
    },

    initSearchDate : function (flag) {
        sticker.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        sticker.setDate.setEndDate(0);
        sticker.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#schBtn').bindClick(sticker.search);
        $('#schResetBtn').bindClick(sticker.searchFormReset);

        $('#setStickerBtn').bindClick(sticker.setSticker);
        $('#resetBtn').bindClick(sticker.resetForm);

        $('#stickerNm').calcTextLength('keyup', '#textCountKr_keyword');

        $('input[name="fileArr"]').change(function() {
            sticker.img.addImg();
        });

        $('.uploadType').bindClick(sticker.img.imagePreview);

    },
    /**
     * 스티커 리스트 검색
     * @returns {boolean}
     */
    search : function() {

        if(!sticker.valid.search()){
            return false;
        }

        CommonAjax.basic({
            url: '/manage/sticker/getStickerList.json?' + $('#stickerSearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                stickerListGrid.setData(res);
                $('#stickerTotalCount').html($.jUtil.comma(stickerListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        sticker.initSearchDate("-1m");

        $('#schType').val("stickerNm");
        $('#schUseYn').val('');
        $('#schKeyword').val('');
    },
    /**
     * 스티커 등록/수정
     */
    setSticker : function() {

        if(!sticker.valid.set()){
            return false;
        }

        var form = $('#stickerSetForm').serializeObject(true);

        CommonAjax.basic({
            url:'/manage/sticker/setSticker.json' ,
            data: JSON.stringify(form) ,
            method:"POST" ,
            successMsg:null ,
            contentType:"application/json",
            callbackFunc:function(res) {
                alert(res.returnMsg);
                sticker.resetForm();
                sticker.search();
            }});
    },
    /**
     * 스티커 등록/수정 폼 초기화
     */
    resetForm : function() {

        $('#stickerNo').val('');
        $('#stickerNm').val('');
        $('input:radio[name="useYn"][value="Y"]').trigger('click');

        $("#stickerNm").setTextLength('#textCountKr_keyword');

        sticker.img.resetImg();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        sticker.resetForm();

        var rowDataJson = stickerListGrid.dataProvider.getJsonRow(selectRowId);

        $('#stickerNo').val(rowDataJson.stickerNo);
        $('#stickerNm').val(rowDataJson.stickerNm);
        $('input:radio[name="useYn"][value="'+rowDataJson.useYn+'"]').trigger('click');

        $("#stickerNm").setTextLength('#textCountKr_keyword');

        CommonAjax.basic({
            url:'/manage/sticker/getStickerImgList.json?stickerNo=' + rowDataJson.stickerNo ,
            data:null ,
            method:"GET" ,
            successMsg:null ,
            callbackFunc:function(res) {
                let stickerImgList = res;
                for (let i in stickerImgList) {
                    sticker.img.setImg($('#'+stickerImgList[i].device), {
                        'imgNo' :  stickerImgList[i].imgNo,
                        'imgUrl': stickerImgList[i].imgUrl,
                        'imgWidth' : stickerImgList[i].imgWidth,
                        'imgHeight' : stickerImgList[i].imgHeight,
                        'useYn' : stickerImgList[i].useYn,
                        'src'   : stickerImgList[i].imgUrl,
                        'device': stickerImgList[i].device
                    });
                }
            }});
    }
};

/**
 * 스티커관리 유효성 체크
 * @type {{search: sticker.valid.search, set: sticker.valid.set}}
 */
sticker.valid = {

    search : function () {

        // 날짜 체크
        let startDt = $("#schStartDate");
        let endDt = $("#schEndDate");

        if (!$.jUtil.isEmpty(startDt.val()) || (!$.jUtil.isEmpty(endDt.val()))) {
            if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(
                endDt.val(), 'YYYY-MM-DD', true).isValid()) {
                alert("조회기간 날짜 형식이 맞지 않습니다");
                $("#schstartDate").focus();
                return false;
            }
            if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
                alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
                startDt.val(
                    moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
                return false;
            }

            if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
                alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
                startDt.val(endDt.val());
                return false;
            }
        }

        let schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
            if(schKeyword.length < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#schKeyword').focus();
                return false;
            }
        }
        return true;

    },
    set : function(){

        if ($.jUtil.isEmpty($('#stickerNm').val()))  {
            alert("스티커명을 입력해주세요.")
            $('#stickerNm').focus();
            return false;
        }

        let form = $('#stickerSetForm').serializeObject(true);
        //이미지 등록 여부 조회
        for(let i in form.stickerImgList) {
            let f = form.stickerImgList[i];

            if(f.device == 'PC'
                && (f.imgUrl == '' || f.imgUrl == 'undefined')){
                alert("이미지를 등록해 주세요.");
                return false;
            }
        }

        return true;
    }

};
/**
 * 스티커 관리 이미지 등록 영역
 * @type {{init: sticker.img.init, imgDiv: null, setImg: sticker.img.setImg, deleteImg: sticker.img.deleteImg, addImg: sticker.img.addImg, resetImg: sticker.img.resetImg, clickFile: sticker.img.clickFile}}
 */
sticker.img = {
    imgDiv : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        sticker.img.resetImg();
        sticker.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ sticker.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNo').val(params.imgNo);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgHeight').val(params.imgHeight);

            if(params.device == 'PC') {
                obj.find('.imgUrlTag').prop('src',
                    hmpImgUrl + "/provide/view?processKey="
                    + sticker.img.getProcessKey(obj) + "&fileId=" + params.src);

                obj.parents('.imgDisplayView').show();
                obj.parents('.imgDisplayView').siblings(
                    '.imgDisplayReg').hide();
            }
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNo').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgUrlTag').prop('src', '');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#stickerImgFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#stickerImgFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#stickerImgFile');
        let params = {
            processKey : 'StickerPCImg',
            mode : 'IMG'
        };

        params.processKey =  sticker.img.getProcessKey(sticker.img.imgDiv);

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        sticker.img.setImg(sticker.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth' : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'   : f.fileStoreInfo.fileId,
                            'device': 'PC'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(device) {
        if (confirm('삭제하시겠습니까?')) {
            sticker.img.setImg($('#' + device), null);
        }
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(device) {
        sticker.img.imgDiv = $('#'+device);
        $('input[name=fileArr]').click();
    },

    /**
     * ProcessKey
     * @param obj
     * @returns {*}
     */
    getProcessKey : function (obj) {
        var processKey;

        if (typeof obj != 'undefined') {
            processKey = obj.data('processkey');
        }
        return processKey;
    }
};