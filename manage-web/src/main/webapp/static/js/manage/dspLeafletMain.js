/**
 * 사이트관리 > 전문관관리 > 마트전단 전시 관리
 */
$(document).ready(function() {
    dspLeafletListGrid.init();
    dspLeafletItemCategoryListGrid.init();
    dspLeafletItemListGrid.init();

    dspLeaflet.init();
    CommonAjaxBlockUI.global();

    dspLeafletItemCategory.init();
    dspLeafletItem.init();
});

// dspLeaflet 그리드
var dspLeafletListGrid = {
    gridView : new RealGridJS.GridView("dspLeafletListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspLeafletListGrid.initGrid();
        dspLeafletListGrid.initDataProvider();
        dspLeafletListGrid.event();
    },
    initGrid : function() {
        dspLeafletListGrid.gridView.setDataSource(dspLeafletListGrid.dataProvider);
        dspLeafletListGrid.gridView.setStyles(dspLeafletGridBaseInfo.realgrid.styles);
        dspLeafletListGrid.gridView.setDisplayOptions(dspLeafletGridBaseInfo.realgrid.displayOptions);
        dspLeafletListGrid.gridView.setColumns(dspLeafletGridBaseInfo.realgrid.columns);
        dspLeafletListGrid.gridView.setOptions(dspLeafletGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspLeafletListGrid.dataProvider.setFields(dspLeafletGridBaseInfo.dataProvider.fields);
        dspLeafletListGrid.dataProvider.setOptions(dspLeafletGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspLeafletListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspLeaflet.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspLeafletListGrid.dataProvider.clearRows();
        dspLeafletListGrid.dataProvider.setRows(dataList);
    }
};

// dspLeafletItemCategoryListGrid 그리드
var dspLeafletItemCategoryListGrid = {
    gridView : new RealGridJS.GridView("dspLeafletItemCategoryListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspLeafletItemCategoryListGrid.initGrid();
        dspLeafletItemCategoryListGrid.initDataProvider();
        dspLeafletItemCategoryListGrid.event();
    },
    initGrid : function() {
        dspLeafletItemCategoryListGrid.gridView.setDataSource(dspLeafletItemCategoryListGrid.dataProvider);
        dspLeafletItemCategoryListGrid.gridView.setStyles(dspLeafletItemCategoryListGridBaseInfo.realgrid.styles);
        dspLeafletItemCategoryListGrid.gridView.setDisplayOptions(dspLeafletItemCategoryListGridBaseInfo.realgrid.displayOptions);
        dspLeafletItemCategoryListGrid.gridView.setColumns(dspLeafletItemCategoryListGridBaseInfo.realgrid.columns);
        dspLeafletItemCategoryListGrid.gridView.setOptions(dspLeafletItemCategoryListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspLeafletItemCategoryListGrid.dataProvider.setFields(dspLeafletItemCategoryListGridBaseInfo.dataProvider.fields);
        dspLeafletItemCategoryListGrid.dataProvider.setOptions(dspLeafletItemCategoryListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspLeafletItemCategoryListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspLeafletItemCategory.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspLeafletItemCategoryListGrid.dataProvider.clearRows();
        dspLeafletItemCategoryListGrid.dataProvider.setRows(dataList);
    }
};

// dspLeafletItemListGrid 그리드
var dspLeafletItemListGrid = {
    gridView : new RealGridJS.GridView("dspLeafletItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspLeafletItemListGrid.initGrid();
        dspLeafletItemListGrid.initDataProvider();
        dspLeafletItemListGrid.event();
    },
    initGrid : function() {
        dspLeafletItemListGrid.gridView.setDataSource(dspLeafletItemListGrid.dataProvider);
        dspLeafletItemListGrid.gridView.setStyles(dspLeafletItemListGridBaseInfo.realgrid.styles);
        dspLeafletItemListGrid.gridView.setDisplayOptions(dspLeafletItemListGridBaseInfo.realgrid.displayOptions);
        dspLeafletItemListGrid.gridView.setColumns(dspLeafletItemListGridBaseInfo.realgrid.columns);
        dspLeafletItemListGrid.gridView.setOptions(dspLeafletItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspLeafletItemListGrid.dataProvider.setFields(dspLeafletItemListGridBaseInfo.dataProvider.fields);
        dspLeafletItemListGrid.dataProvider.setOptions(dspLeafletItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspLeafletItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspLeafletItem.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspLeafletItemListGrid.dataProvider.clearRows();
        dspLeafletItemListGrid.dataProvider.setRows(dataList);
    },
    addData : function (dataList) {
        dspLeafletItemListGrid.dataProvider.addRows(dataList);
    }
};

// dspLeaflet 관리
var dspLeaflet = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-1d');
        this.initSetDate('-30d');
    },
    initSearchDate : function (flag) {
        dspLeaflet.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspLeaflet.setDate.setEndDate(0);
        dspLeaflet.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSetDate : function (flag) {
        dspLeaflet.setDate = Calendar.datePickerRange('dispStartDt', 'dispEndDt');
        dspLeaflet.setDate.setEndDate(0);
        dspLeaflet.setDate.setStartDate(flag);
        $("#dispEndDt").datepicker("option", "minDate", $("#dispStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setDspLeafletItemBtn').bindClick(dspLeaflet.set); // 등록,수정
        $('#resetDspLeafletItemBtn').bindClick(dspLeaflet.resetForm); // 입력폼 초기화
        $('#searchBtn').bindClick(dspLeaflet.search); // 검색
        $('#addItemPopUp').bindClick(dspLeafletItem.addItemPopUp); // 상품 검색
        $('#addItemPopUpByCheck').bindClick(dspLeafletItem.addItemPopUpByCheck); // 상품 검색
        $('#martLeafletNm').calcTextLength('keyup', '#martLeafletNmCnt');

        // 파일 선택시
        $('input[name="fileArr"]').change(function() {
            dspLeaflet.img.addImg();
        });

        // 카테고리 이미지 (PC) 삭제
        $('#pcDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                dspLeaflet.img.deleteImg('pc');
            }
        });

        // 카테고리 이미지 (MOBILE) 삭제
        $('#mobileDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                dspLeaflet.img.deleteImg('mobile');
            }
        });
    },
    /**
     * dspLeaflet 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspLeaflet/getDspLeafletList.json'
            , data : $('#dspLeafletSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspLeaflet.resetForm();
                dspLeafletItemCategory.resetForm();
                dspLeafletItemListGrid.dataProvider.clearRows();
                dspLeafletListGrid.setData(res);
                $('#dspLeafletTotalCount').html($.jUtil.comma(dspLeafletListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#schKeyword').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspLeafletListGrid.dataProvider.getJsonRow(selectRowId);

        $('#leafletNo').val(rowDataJson.leafletNo);
        $('#martLeafletNm').val(rowDataJson.martLeafletNm);
        $('#martLeafletNmCnt').html(rowDataJson.martLeafletNm.length);
        $('#siteType').val(rowDataJson.siteType);
        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);
        $('#itemTotalCount').html('0');
        dspLeafletItem.itemList = new Array();
        dspLeafletItemListGrid.dataProvider.clearRows();
        dspLeafletItemCategory.search();
    },
    /**
     * dspLeaflet 등록/수정
     */
    set : function() {
        if(!dspLeaflet.valid.set()) {
            return false;
        }

        let dspLeafletSetForm = $('#dspLeafletSetForm').serializeObject(true);
        dspLeafletSetForm.itemCategoryList = dspLeafletItemCategory.itemCategoryList;
        dspLeafletSetForm.itemList = dspLeafletItem.itemList;

        CommonAjax.basic({
            url:'/manage/dspLeaflet/setDspLeaflet.json',
            data : JSON.stringify(dspLeafletSetForm)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                dspLeaflet.resetForm();
                dspLeaflet.searchFormReset();
                dspLeaflet.search();
            }
        });
    },
    /**
     * dspLeaflet 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#leafletNo, #martLeafletNm').val('');
        $('#siteType').val('HOME');
        dspLeaflet.initSetDate('-30d');
        $('#martLeafletNmCnt, #itemTotalCount').html('0');

        dspLeafletItemCategory.setOnData();
        dspLeafletItemCategory.resetForm();

        dspLeafletItemListGrid.dataProvider.clearRows();
    }
};

/**
 * dspLeaflet 검색,입력,수정 validation check
 */
dspLeaflet.valid = {
    set : function () {
        if($.jUtil.isEmpty($('#siteType').val())) {
            $('#siteType').focus();
            alert('사이트를 선택해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#martLeafletNm').val())) {
            $('#martLeafletNm').focus();
            alert('마트전단 명을 입력해 주세요.');
            return false;
        }

        var leafletNo = $('#leafletNo').val();
        var isChecked = true;
        var itemCnt = 0;
        if($.jUtil.isEmpty(leafletNo)) { // 신규 등록
            dspLeafletItem.itemList.forEach(function (item, index) {
                if(!$.jUtil.isEmpty(item.itemList)) {
                    item.itemList.forEach(function (data, index) {
                        if(data.cateNo === 0) {
                            isChecked = false;
                            if(data.useYn === 'Y') {
                                itemCnt++;
                            }
                        }
                    });
                }
            });

            if(isChecked) {
                alert('대표상품이 등록되지 않아 저장이 불가능합니다.');
                return false;
            }

            if(itemCnt < 7) {
                alert('대표상품은 최소 7개 이상 등록 되어야 합니다.');
                return false;
            }

        } else { // 수정
            isChecked = false;
            dspLeafletItem.itemList.forEach(function (item, index) {
                if(!$.jUtil.isEmpty(item.itemList)) {
                    item.itemList.forEach(function (data, index) {
                        if(data.cateNo === 0) {
                            isChecked = true;
                            if(data.useYn === 'Y') {
                                itemCnt++;
                            }
                        }
                    });
                }
            });

            if(isChecked && itemCnt < 7) {
                alert('대표상품은 최소 7개 이상 등록 되어야 합니다.');
                return false;
            }
        }

        return true;
    }
};

// dspLeafletItemCategory 관리
var dspLeafletItemCategory = {
    itemCategoryList : {},
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.setOnData();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#schGridItemBtn').bindClick(dspLeafletItemCategory.schGridItemNo);
        $('#setDspLeafletItemCategory').bindClick(dspLeafletItemCategory.setGridRowSelect);
        $('#resetDspLeafletItemCategory').bindClick(dspLeafletItemCategory.resetForm); //dspLeaflet 검색폼 초기화
    },
    /**
     * dspLeafletItemCategory Init 데이터.
     */
    setOnData : function() {
        CommonAjax.basic({
            url : '/manage/dspLeaflet/getDspLeafletItemCategoryList.json'
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspLeafletItemCategoryListGrid.setData(res);
                dspLeafletItemCategory.itemCategoryList = new Array();
                dspLeafletItem.itemList = new Array();
                res.forEach(function (data, index) {
                    dspLeafletItemCategory.itemCategoryList[index] = {
                        getDb : false
                        , leafletNo : data.leafletNo
                        , cateNo : data.cateNo
                        , useYn : data.useYn
                        , priority : parseInt(index) + 1
                    }
                });
            }
        });
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = dspLeafletItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                dspLeafletItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
            } else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * dspLeafletItemCategory 검색 ( leafletNo )
     */
    search : function() {
        const leafletNo = $('#leafletNo').val();
        CommonAjax.basic({
            url : '/manage/dspLeaflet/getDspLeafletItemCategoryList.json?leafletNo=' + leafletNo
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspLeafletItemCategoryListGrid.setData(res);
                dspLeafletItemCategory.itemCategoryList = new Array();
                res.forEach(function (data, index) {
                    dspLeafletItemCategory.itemCategoryList[index] = {
                        getDb : false
                        , leafletNo : data.leafletNo
                        , cateNo : data.cateNo
                        , useYn : data.useYn
                        , priority : parseInt(index) + 1
                    }
                });
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspLeafletItemCategoryListGrid.dataProvider.getJsonRow(selectRowId);

        $('#dispCateNm').val(rowDataJson.cateNm);
        $('#setDspLeafletItemCategoryUseYn').val(rowDataJson.useYn);

        // 아이템 리스트 가져오기.
        const leafletNo = $('#leafletNo').val();
        const curRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
        const cateNo = dspLeafletItemCategoryListGrid.gridView.getValue(curRow, 0);

        if(!dspLeafletItemCategory.itemCategoryList[selectRowId].getDb === true) {
            dspLeafletItemCategory.itemCategoryList[selectRowId].getDb = true;
            dspLeafletItem.search(leafletNo, cateNo);
        } else {
            const arrItem = dspLeafletItem.itemList[selectRowId].itemList;
            dspLeafletItemListGrid.setData(arrItem);
        }

        $('#itemTotalCount').html(dspLeafletItemListGrid.gridView.getItemCount());
    },
    /**
     * 그리드 row 값 셋팅
     */
    setGridRowSelect : function() {

        if($.jUtil.isEmpty($('#dispCateNm').val())) {
            alert("변경 하실 카테고리를 먼저 선택해 주세요.");
            return;
        }

        let setUseYnTxt = '사용';
        const curRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
        const setUseYn = $('#setDspLeafletItemCategoryUseYn').val();

        if(setUseYn === 'N') {
            setUseYnTxt = '사용안함';
        }

        const values = { useYn : setUseYn, useYnTxt : setUseYnTxt };

        dspLeafletItemCategoryListGrid.gridView.setValues(curRow, values, true);

        // Back Data 변경.
        dspLeafletItemCategory.itemCategoryList[curRow] = {
            getDb : dspLeafletItemCategory.itemCategoryList[curRow].getDb
            , leafletNo : dspLeafletItemCategory.itemCategoryList[curRow].leafletNo
            , cateNo : dspLeafletItemCategory.itemCategoryList[curRow].cateNo
            , useYn : setUseYn
            , priority : dspLeafletItemCategory.itemCategoryList[curRow].priority
        }

        alert("변경 되었습니다.");
    },
    /**
     * dspLeafletItemCategory 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#dispCateNm').val('');
        $('#setDspLeafletItemCategoryUseYn').val('Y');
    },
    /**
     * dspLeafletItemCategory 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {

        var checkedRows = dspLeafletItemCategoryListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(dspLeafletItemCategoryListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬
        dspLeafletItemCategory.itemCategoryList.forEach(function (data, index) {
            const cateNo = dspLeafletItemCategoryListGrid.gridView.getValue(index, 0);
            const useYn = dspLeafletItemCategoryListGrid.gridView.getValue(index, 2);

            dspLeafletItemCategory.itemCategoryList[index] = {
                //Todo : getDb 값으로 문제가 될 경우가 존재할 수도 있으므로... 나중에 테스트좀 더 해보자. ( 필요하면 Controller > Grid 변수 추가 필요. )
                getDb : dspLeafletItemCategory.itemCategoryList[index].getDb
                , leafletNo : dspLeafletItemCategory.itemCategoryList[index].leafletNo
                , cateNo : cateNo
                , useYn : useYn
                , priority : parseInt(index) + 1
            }
        });
    }
};

// dspLeafletItem 관리
var dspLeafletItem = {
    itemList : {},
    /**
     * 초기화
     */
    init : function() {
        dspLeafletItem.itemList = new Array();
    },
    /**
     * dspLeafletItemCategory 검색 ( leafletNo )
     */
    search : function(leafletNo, cateNo) {
        CommonAjax.basic({
            url : '/manage/dspLeaflet/getDspLeafletItemList.json?leafletNo=' + leafletNo + '&cateNo=' + cateNo
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                dspLeafletItemListGrid.setData(res);

                const selectRowId = dspLeafletItemCategoryListGrid.gridView.getCurrent();
                let arrList = new Array();
                res.forEach(function (data, index) {
                    let itemList;
                    itemList = {
                        leafletNo : leafletNo
                        , cateNo : cateNo
                        , itemNo : data.itemNo
                        , itemNm1 : data.itemNm1
                        , useYn : data.useYn
                        , useYnTxt : data.useYnTxt
                        , itemTypeNm : data.itemTypeNm
                        , chgNm      : data.chgNm
                        , priority : parseInt(index) + 1
                    }
                    arrList.push(itemList);
                });
                dspLeafletItem.itemList[selectRowId.dataRow] = {
                    itemList : arrList
                };

                $('#itemTotalCount').html(dspLeafletItemListGrid.gridView.getItemCount());
            }
        });
    },
    /**
     * 사용여부 변경
     */
    chgUseYn : function (action) {
        var checkedRows = dspLeafletItemListGrid.gridView.getCheckedRows(false);
        var setUseYn, setUseYnNm;

        if (checkedRows.length == 0) {
            alert("사용여부 변경할 상품을 선택해 주세요.");
            return false;
        }

        switch (action) {
            case 'Y' :
                setUseYn = 'Y';
                setUseYnNm = '사용';
                break;
            case 'N' :
                setUseYn = 'N';
                setUseYnNm = '사용안함';
                break;
        }

        // RealGrid 값 변경.
        var setGridValue = { useYn : setUseYn, useYnTxt : setUseYnNm };
        checkedRows.forEach(function(data) {
            dspLeafletItemListGrid.gridView.setValues(data, setGridValue, true);
        });

        // BackData 사용여부 값 갱신.
        const curCategoryRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
        const curRow = dspLeafletItemListGrid.gridView.getCurrent().itemIndex;

        // Priority 정렬
        dspLeafletItem.itemList[curCategoryRow].itemList.forEach(function (data, index) {
            var itemNo = dspLeafletItemListGrid.gridView.getValue(index, 0);
            var itemNm1 = dspLeafletItemListGrid.gridView.getValue(index, 1);
            var itemTypeNm = dspLeafletItemListGrid.gridView.getValue(index, 2);
            var useYn = dspLeafletItemListGrid.gridView.getValue(index, 3);
            var useYnTxt = dspLeafletItemListGrid.gridView.getValue(index, 4);
            var chgNm = dspLeafletItemListGrid.gridView.getValue(index, 5);

            dspLeafletItem.itemList[curCategoryRow].itemList[index] = {
                itemNo : itemNo
                , itemNm1 : itemNm1
                , itemTypeNm : itemTypeNm
                , useYn : useYn
                , useYnTxt : useYnTxt
                , chgNm : chgNm
                , priority : parseInt(index) + 1
                , cateNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].cateNo
                , leafletNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].leafletNo
            }
        });
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspLeafletItemListGrid.dataProvider.getJsonRow(selectRowId);

        $('#dispItemNo').val(rowDataJson.itemNo);
        $('#setDspLeafletItemUseYn').val(rowDataJson.useYn);
    },
    /**
     * 그리드 row 선택
     */
    setGridRowSelect : function() {
        if($.jUtil.isEmpty($('#dispItemNo').val())) {
            alert("변경 하실 상품을 먼저 선택해 주세요.");
            return;
        }

        let setUseYnTxt = '사용';
        const curRow = dspLeafletItemListGrid.gridView.getCurrent().itemIndex;
        const setUseYn = $('#setDspLeafletItemUseYn').val();

        if(setUseYn === 'N') {
            setUseYnTxt = '사용안함';
        }

        // RealGrid 값 변경.
        const values = { useYn : setUseYn, useYnTxt : setUseYnTxt };
        dspLeafletItemListGrid.gridView.setValues(curRow, values, true);

        // Back Data 변경.
        const curCategoryRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
        dspLeafletItem.itemList[curCategoryRow].itemList[curRow] = {
            itemNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].itemNo
            , itemNm1 : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].itemNm1
            , itemTypeNm : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].itemTypeNm
            , useYn : setUseYn
            , useYnTxt : setUseYnTxt
            , chgNm : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].chgNm
            , priority : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].priority
            , cateNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].cateNo
            , leafletNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].leafletNo
        }

        alert("변경 되었습니다.");
    },
    /**
     * 상품 업로드 팝업 ( 엑셀 )
     */
    openItemReadPopup : function () {
        if(!dspLeafletItemCategory.itemCategoryList[dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex].getDb === true) {
            alert("추가하실 카테고리 정보를 먼저 선택해 주세요.");
            return false;
        }
        var target = "leafletItemPop";
        var callback = "dspLeafletItem.itemExcelPopCallback";
        var url = "/manage/popup/uploadLeafletItemPop?callback=" + callback;

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function (res) {
        dspLeafletItem.setItemInfoByExcel(res);
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = dspLeafletItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(dspLeafletItemListGrid, $(obj).attr("key"), checkedRows);

        const curCategoryRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
        const curRow = dspLeafletItemListGrid.gridView.getCurrent().itemIndex;

        // Priority 정렬
        dspLeafletItem.itemList[curCategoryRow].itemList.forEach(function (data, index) {
            var itemNo = dspLeafletItemListGrid.gridView.getValue(index, 0);
            var itemNm1 = dspLeafletItemListGrid.gridView.getValue(index, 1);
            var itemTypeNm = dspLeafletItemListGrid.gridView.getValue(index, 2);
            var useYn = dspLeafletItemListGrid.gridView.getValue(index, 3);
            var useYnTxt = dspLeafletItemListGrid.gridView.getValue(index, 4);
            var chgNm = dspLeafletItemListGrid.gridView.getValue(index, 5);

            dspLeafletItem.itemList[curCategoryRow].itemList[index] = {
                itemNo : itemNo
                , itemNm1 : itemNm1
                , itemTypeNm : itemTypeNm
                , useYn : useYn
                , useYnTxt : useYnTxt
                , chgNm : chgNm
                , priority : parseInt(index) + 1
                , cateNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].cateNo
                , leafletNo : dspLeafletItem.itemList[curCategoryRow].itemList[curRow].leafletNo
            }
        });
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function() {
        if(!dspLeafletItemCategory.itemCategoryList[dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex].getDb === true) {
            alert("추가하실 카테고리 정보를 먼저 선택해 주세요.");
            return false;
        }
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspLeafletItem.setItemInfo&isMulti=Y&mallType=TD&storeType=HYPER', 1084, 650);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        const selectRowId = dspLeafletItemCategoryListGrid.gridView.getCurrent();
        let arrList = dspLeafletItem.itemList[selectRowId.dataRow].itemList;
        const lastIdx = arrList.length;

        // 상품이 없어 최초 등록일 경우
        if(lastIdx === 0) {
            // 현재 셀렉트 된 cateNo를 가져온다.
            const curRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
            const cateNo = dspLeafletItemCategoryListGrid.gridView.getValue(curRow, 0);
            let leafletNo = $('#leafletNo').val();
            if($.jUtil.isEmpty($('#leafletNo').val())) {
                leafletNo = 0;
            }

            arrList = new Array();
            resData.forEach(function (data, index) {
                let itemList;
                let options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                let dataRow = dspLeafletItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    leafletNo : leafletNo
                    , cateNo : cateNo
                    , itemNo : data.itemNo
                    , itemNm1 : data.itemNm
                    , useYn : "Y"
                    , useYnTxt : "저장대기"
                    , itemTypeNm : "저장 후 확인 가능"
                    , priority : parseInt(index)
                }
                if(dataRow === -1) {
                    dspLeafletItem.setItemGrid(data);
                    arrList.push(itemList);
                }
            });
        } else {
            resData.forEach(function (data, index) {
                let itemList;
                let options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                let dataRow = dspLeafletItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    leafletNo : arrList[0].leafletNo
                    , cateNo : arrList[0].cateNo
                    , itemNo : data.itemNo
                    , itemNm1 : data.itemNm
                    , useYn : "Y"
                    , useYnTxt : "저장대기"
                    , itemTypeNm : "저장 후 확인 가능"
                    , priority : parseInt(lastIdx + index)
                }

                if(dataRow === -1) {
                    dspLeafletItem.setItemGrid(data);
                    arrList.push(itemList);
                }
            });
        }

        dspLeafletItem.itemList[selectRowId.dataRow] = {
            itemList : arrList
        };
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfoByExcel : function(resData) {
        const selectRowId = dspLeafletItemCategoryListGrid.gridView.getCurrent();
        let arrList = dspLeafletItem.itemList[selectRowId.dataRow].itemList;
        const lastIdx = arrList.length;

        // 상품이 없어 최초 등록일 경우
        if(lastIdx === 0) {
            // 현재 셀렉트 된 cateNo를 가져온다.
            const curRow = dspLeafletItemCategoryListGrid.gridView.getCurrent().itemIndex;
            const cateNo = dspLeafletItemCategoryListGrid.gridView.getValue(curRow, 0);
            let leafletNo = $('#leafletNo').val();
            if($.jUtil.isEmpty($('#leafletNo').val())) {
                leafletNo = 0;
            }

            arrList = new Array();
            resData.forEach(function (data, index) {
                let itemList;
                let options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                let dataRow = dspLeafletItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    leafletNo : leafletNo
                    , cateNo : cateNo
                    , itemNo : data.itemNo
                    , itemNm1 : data.itemNm1
                    , useYn : data.useYn
                    , useYnTxt : data.useYnTxt
                    , itemTypeNm : data.itemTypeNm
                    , priority : parseInt(index)
                }
                if(dataRow === -1) {
                    dspLeafletItem.setItemGridByExcel(data);
                    arrList.push(itemList);
                }
            });
        } else {
            resData.forEach(function (data, index) {
                let itemList;
                let options = {
                    startIndex: 0,
                    fields: ['itemNo'],
                    values: [data.itemNo]
                };
                let dataRow = dspLeafletItemListGrid.dataProvider.searchDataRow(options);

                itemList = {
                    leafletNo : arrList[0].leafletNo
                    , cateNo : arrList[0].cateNo
                    , itemNo : data.itemNo
                    , itemNm1 : data.itemNm1
                    , useYn : data.useYn
                    , useYnTxt : data.useYnTxt
                    , itemTypeNm : data.itemTypeNm
                    , priority : parseInt(lastIdx + index)
                }

                if(dataRow === -1) {
                    dspLeafletItem.setItemGridByExcel(data);
                    arrList.push(itemList);
                }
            });
        }

        dspLeafletItem.itemList[selectRowId.dataRow] = {
            itemList : arrList
        };
    },
    /**
     * 상품을 Grid 에 추가해준다.
     * Item 상태는 공통 팝업에서 제공해 주지 않으므로
     * 저장 후 다시 조회 시에 노출 가능하나... 필요시 공통 팝업에서 제공 요청.
     *
     * 현재 버전은 아래와 같이 저장 후 확인 가능으로 노출해준다.
     */
    setItemGrid : function(data) {
        const itemData = {
            itemNo : data.itemNo
            , itemNm1 : data.itemNm
            , itemTypeNm : "저장 후 확인 가능"
            , useYn : "Y"
            , useYnTxt : "저장대기"
        }
        dspLeafletItemListGrid.dataProvider.addRow(itemData);
    },
    /**
     * 상품을 Grid 에 추가해준다.
     * Item 상태는 공통 팝업에서 제공해 주지 않으므로
     * 저장 후 다시 조회 시에 노출 가능하나... 필요시 공통 팝업에서 제공 요청.
     *
     * 현재 버전은 아래와 같이 저장 후 확인 가능으로 노출해준다.
     */
    setItemGridByExcel : function(data) {
        const itemData = {
            itemNo : data.itemNo
            , itemNm1 : data.itemNm1
            , itemTypeNm : data.itemTypeNm
            , useYn : data.useYn
            , useYnTxt : data.useYnTxt
        }
        dspLeafletItemListGrid.dataProvider.addRow(itemData);
    }
};