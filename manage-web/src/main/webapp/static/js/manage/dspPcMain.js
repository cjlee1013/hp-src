/**
 * 사이트관리 > 전시관리 > PC 메인관리
 */
$(document).ready(function() {
    dspPcMainListGrid.init();
    dspPcMain.init();
    dspMainCommon.init();
    CommonAjaxBlockUI.global();

});
// dspPcMain 그리드
var dspPcMainListGrid = {

    gridView : new RealGridJS.GridView("dspPcMainListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        dspPcMainListGrid.initGrid();
        dspPcMainListGrid.initDataProvider();
        dspPcMainListGrid.event();

    },
    initGrid : function() {

        dspPcMainListGrid.gridView.setDataSource(dspPcMainListGrid.dataProvider);
        dspPcMainListGrid.gridView.setStyles(dspPcMainListGridBaseInfo.realgrid.styles);
        dspPcMainListGrid.gridView.setDisplayOptions(dspPcMainListGridBaseInfo.realgrid.displayOptions);
        dspPcMainListGrid.gridView.setColumns(dspPcMainListGridBaseInfo.realgrid.columns);
        dspPcMainListGrid.gridView.setOptions(dspPcMainListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        dspPcMainListGrid.dataProvider.setFields(dspPcMainListGridBaseInfo.dataProvider.fields);
        dspPcMainListGrid.dataProvider.setOptions(dspPcMainListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspPcMainListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspPcMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspPcMainListGrid.dataProvider.clearRows();
        dspPcMainListGrid.dataProvider.setRows(dataList);

    },

};

//배너 검색, 초기화
//배너 등록
//전체 초기화
//그리드 선택
var dspPcMain = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');

    },
    initSearchDate : function (flag) {
        dspPcMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspPcMain.setDate.setEndDate(0);
        dspPcMain.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },

    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspPcMain.search);
        $('#searchResetBtn').bindClick(dspPcMain.searchFormReset);
        $('#setPcMainBtn').bindClick(dspPcMain.setDspPcMain);
        $('#resetBtn').bindClick(dspPcMain.resetForm);

    },
    /**
     * DSP PC 메인 검색
     */
    search : function() {

        CommonAjax.basic({url:'/manage/dspMain/getPcMainList.json?' + $('#dspMainSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
               dspPcMain.resetForm();
               dspPcMainListGrid.setData(res);
                $('#dspPcMainTotalCount').html($.jUtil.comma(dspPcMainListGrid.gridView.getItemCount()));
            }});
    },
    checkUrl : function(url){
        var expUrl = /(http(s)?:\/\/)([a-z0-9\w])/gi;
        return expUrl.test(url);
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $('#searchPeriodType').val('REGDT');
        dspPcMain.initSearchDate('-7d');
        $('#searchSiteType').val('');
        $('#searchDispYn').val('');
        $('#searchLoc1Depth').val('');
        $('#searchType').val('BANNER');
        $('#searchKeyword').val('');

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = dspPcMainListGrid.dataProvider.getJsonRow(selectRowId);

        $('#siteType').val(rowDataJson.siteType).trigger('change').prop('disabled' , true);
        $('#loc1Depth').val(rowDataJson.loc1depth).trigger('change');
        $('#mainNo').val(rowDataJson.mainNo);
        /*$('#siteType').val(rowDataJson.siteType).prop('disabled' , true);*/

        $('#dispYn').val(rowDataJson.dispYn);
        $('#priority').val(rowDataJson.priority);
        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);
        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNmCnt').html(rowDataJson.bannerNm.length);



        CommonAjax.basic({url:'/manage/dspMain/getPcMainDetail.json?mainNo=' + rowDataJson.mainNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {

            switch (rowDataJson.loc1depth) {

                    case "TOP"  :
                        dspMainTopBannerDetail.setBannerDetail(res);
                        break;
                    case "LOGO" :
                        dspMainLogoBannerDetail.setBannerDetail(res);
                        break;
                    case "GNB" :
                        dspMainGnbBannerDetail.setBannerDetail(res);
                        break;
                    case "GCATE":
                        dspMainGcateBannerDetail.setBannerDetail(res);
                        break;
                    case "BIG":
                        dspMainBigBannerDetail.setBannerDetail(res);
                        break;
                    case "QUICK":
                        dspMainQuickBannerDetail.setBannerDetail(res);
                        break;
                    case "CENTER":
                        dspMainCenterBannerDetail.setBannerDetail(res);
                        break;
                    case "LWING":
                        dspMainLwingBannerDetail.setBannerDetail(res);
                        break;
                }


        }});


    },

    /**
     * DSP PC 메인 등록/수정
     */
    setDspPcMain : function() {

        //배너 기본 정보 역영 파라미터 유효성 검사
        if(!dspMainCommon.setCommonBannerValid()){
            return false;
        }

        var loc1Depth = $('#loc1Depth').val();

        var bannerInfo  = new Object();
        var bannerCommon = $("#dspPcMainSetForm").serializeObject();

        switch (loc1Depth) {

            case "TOP"  :
                if(!dspMainTopBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainTopBannerDetail.getBannerDetail();
                break;
            case "LOGO" :
                if(!dspMainLogoBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainLogoBannerDetail.getBannerDetail();
                break;
            case "GNB" :
                if(!dspMainGnbBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainGnbBannerDetail.getBannerDetail();
                break;
            case "GCATE":
                if(!dspMainGcateBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainGcateBannerDetail.getBannerDetail();
                break;
            case "BIG":
                if(!dspMainBigBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainBigBannerDetail.getBannerDetail();
                break;
            case "QUICK":
                if(!dspMainQuickBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainQuickBannerDetail.getBannerDetail();
                break;
            case "CENTER":
                if(!dspMainCenterBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainCenterBannerDetail.getBannerDetail();
                break;
            case "LWING":
                if(!dspMainLwingBannerDetail.setBannerValid()){
                    return false;
                }
                bannerDetail = dspMainLwingBannerDetail.getBannerDetail();
                break;
        }

        bannerInfo= $.extend({}, bannerCommon, bannerDetail);
        CommonAjax.basic({url:'/manage/dspMain/setPcMain.json',data: JSON.stringify(bannerInfo), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                dspPcMain.search();
            }});
    },
    /**
     * DSP PC 메인 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#siteType').prop('disabled' , false);
        $('#siteType').val('HOME').trigger('change');
    }

};

//배너기본 정보 영역
//링크 정보 입력 영역 (링크 정보 초기화는 각 배너 타입 js파일에 구현)
//이마지 등록 삭제
var dspMainCommon ={

    linkInfoDiv : null,
    storeList : null,
    $dispStoreCnt : null,
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSetDate('0');
        dspMainCommon.linkInfoDiv = null;
        dspMainCommon.storeList = null;
        dspMainCommon.$dispStoreCnt = null;
        dspMainCommon.img.init();
    },
    initSetDate : function (flag) {
        dspPcMain.setDate = CalendarTime.datePickerRange('dispStartDt', 'dispEndDt');
        dspPcMain.setDate.setEndDateByTimeStamp(0);
        dspPcMain.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#dispStartDt").val());

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $("#siteType").bindChange(dspMainCommon.siteType);
        $("#loc1Depth").bindChange(dspMainCommon.changeLoc);
        $('#bannerNm').calcTextLength('keyup', '#bannerNmCnt');

        /*****************************이미지 이벤트 등록*******************************************/

        $(document).on('change','input[name="fileArr"]',function(){

            dspMainCommon.img.addImg();

        });

        //이미지 등록 버튼
        $(document).on('click','.addImgBtn',function(){

            dspMainCommon.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();

        });

        //이미지 삭제 버튼
        $(document).on('click','.deleteImgBtn',function(){

            dspMainCommon.img.setImg($(this).next(), null, true);

        });


        /*****************************노출 점포 관려 이벤트 등록*******************************************/


        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('click','input:radio[name="dispStoreType"]',function(){

            dspMainCommon.setDispStoreType($(this));
        });
        //노출 점포 변경 셀렉트 박스
        $(document).on('change','select[name="dispStoreType"]',function(){

            dspMainCommon.changeDispStoreType($(this));
        });
        //점포 검색 팝업
        $(document).on('click','.storeInfoBtn',function(){

            dspMainCommon.setStorePop($(this));

        });
        /*노출 점포 관려 이벤트 종료*/


        /******************************링크 정보 관려 이벤트 등록******************************************/


        //링크 정보  -> ITEM_TD -> 점포 타입 변경
        $(document).on('change','.storeType',function(){

            dspMainCommon.changeLinkStoreType($(this));

        });
        //택배점 설정 체크박스
        $(document).on('click','.dlvStoreYn',function(){
            dspMainCommon.setDlvStoreYn($(this));

        });

        //링크타입 변경
        $(document).on('change','.linkeType',function(){

            dspMainCommon.changeLinkType($(this).val() , $(this).next('div') );

        });
        /*링크 정보 관려 이벤트 등록 종료*/

    },
    //점포 상품 점포 유형 변경
    changeLinkStoreType : function(obj){

        $(obj).parent().find('.linkInfo').val('');
        $(obj).parent().find('.linkInfoNm').val('');
        $(obj).parent().find('.dlvStoreYn').prop("checked", false).val('N');
        if($(obj).val() == 'HYPER' ||  $(obj).val() == 'CLUB'){

            $(obj).parent().find('.dlvStoreList').prop("disabled", false).val('');

        } else{
            $(obj).parent().find('.dlvStoreList').prop("disabled", true).val('');

        }

    },
    setStorePop : function(obj) {
        dspMainCommon.$dispStoreCnt =  $(obj).parent().find('#dispStoreCnt');
        if ($.jUtil.isEmpty(dspMainCommon.storeList)) {
            dspMainCommon.openStoreInfoPopup(obj);
        } else {
            dspMainCommon.openStoreInfoPopup(obj,dspMainCommon.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        var resCount = res.storeList.length;
        dspMainCommon.storeList = res.storeList;
        dspMainCommon.$dispStoreCnt.html(resCount);
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(obj,data) {
        var storeType = $(obj).parent().find('select[name="dispStoreType"]').val();

        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if ($("#chgYn").val() == 'Y') {
            $('.storePop input[name="setYn"]').val('N');
        } else {
            $('.storePop input[name="setYn"]').val('Y');
        }

        var url = "/common/popup/storeSelectPop";
        var target = "storeSelectPop";
        windowPopupOpen(url, target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    //노출 점포 영역 세팅
    setDispStoreType : function(obj){

        var siteType  = $('#siteType').val();
        var dispStoreType = $(obj).val();
        $dispStore = $(obj).parent().parent().find('#dispStore');

        $dispStore.find('select').remove();

        if(dispStoreType == 'ALL'){
            $dispStore.hide();
            dspMainCommon.storeList = null;
        }else{

            if(siteType == 'HOME'){
                $dispStore.prepend($('#storeTypeHome').html());
            }else{
                $dispStore.prepend($('#storeTypeClub').html());
            }
            $dispStore.find('select').attr('name' ,'dispStoreType');
            $dispStore.show();
        }
        dspMainCommon.changeDispStoreType(obj);


    },
    changeDispStoreType : function(obj){

        $(obj).parent().parent().find('#dispStoreCnt').html('0');
        dspMainCommon.storeList = null;

    },

    //택배점 설정 검사
    setDlvStoreYn : function(obj){
        if($(obj).is(":checked")){

            var storeTyep = $(obj).parent().parent().find('.storeType').val();

            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB' ){

                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }

            if ($.jUtil.isEmpty(linkInfo)) {

                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled" ,false);
        } else{
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled" ,true);

        }
    },
    //사이트 구분 변경
    siteType : function(){

        $("#loc1Depth").val('').trigger('change');
        $("#dispYn").val('Y');
        $("#bannerNm").val('');
        $("#mainNo").val('');
        dspMainCommon.initSetDate('0');

    },
    //전체 배너 템플릿 초기화
    resetAllLinkTypeTemplate : function(){
        dspMainTopBannerDetail.resetForm();
        dspMainLogoBannerDetail.resetForm();
        dspMainGnbBannerDetail.resetForm();
        dspMainGcateBannerDetail.resetForm();
        dspMainBigBannerDetail.resetForm();
        dspMainQuickBannerDetail.resetForm();
        dspMainCenterBannerDetail.resetForm();
        dspMainLwingBannerDetail.resetForm();

    },
    //전시 위처 변경 이벤트
    changeLoc : function(){

        var loc1Depth = $("#loc1Depth").val();
        $('.bannerTemplate').hide();
        $('#priority').val('').prop("disabled", false);
        dspMainCommon.resetAllLinkTypeTemplate();
        dspMainCommon.getLoc2Depth();

        switch (loc1Depth) {
            case 'BIG' :
                $("#bannerTemplateBig").show();
                dspMainBigBannerDetail.changeLinkType();
                break;
            case 'TOP' :
                $("#bannerTemplateTop").show();
                dspMainTopBannerDetail.changeLinkType();
                $('#priority').prop("disabled", true).val('');
                break;
            case 'LOGO' :
                $("#bannerTemplateLogo").show();
                dspMainLogoBannerDetail.changeLinkType();
                // $('#priority').prop("disabled", true).val('');
                break;
            case 'GNB' :
                $("#bannerTemplateGnb").show();
                dspMainGnbBannerDetail.changeLinkType();
                $('#priority').prop("disabled", true).val('');
                break;
            case 'GCATE' :
                dspMainCommon.getLoc2Depth('GCATE');
                $("#bannerTemplateGcate").show();
                dspMainGcateBannerDetail.changeLinkType();
                $('#priority').prop("disabled", true).val('');
                break;
            case 'QUICK' :
                $("#bannerTemplateQuick").show();
                dspMainQuickBannerDetail.changeLinkType();
                break;
            case 'CENTER' :
                $("#bannerTemplateCenter").show();
                dspMainCenterBannerDetail.changeLinkType();
                break;
            case 'LWING' :
                $("#bannerTemplateLwing").show();
                dspMainLwingBannerDetail.changeLinkType();
                break;
        }


    },
    //대대 카테고리 리스트 조회
    getLoc2Depth : function(type){
            if(type == 'GCATE') {
                CommonAjax.basic({
                    url: '/item/category/getGroupList.json?schType=GCATENM&schUseYn=Y',
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc: function (res) {

                        var resultCount = res.length;
                        var option = "";
                        for (var idx = 0; resultCount > idx; idx++) {
                            var data = res[idx];
                            option += "<option value=" + data.gcateCd + ">"
                                + data.gcateNm + "</option>";

                        }
                        $("#loc2Depth").prop('disabled', false);
                        $("#loc2Depth").html(option);

                    }
                });
            } else{

                $("#loc2Depth").prop('disabled', true);
                $("#loc2Depth").html("<option value=\"\">-</option>");

            }


    },
    //기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
    popLink : function(obj){

        linkInfoDiv = $(obj).parent();
        var linkeType  = $(obj).parent().prev('select').val();
        var storeType = $(obj).parent().find('.storeType').val();

        switch (linkeType) {
            case "EXH"  :
                dspMainCommon.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                dspMainCommon.addPromoPopUp('EVENT');
                break;
            case "ITEM_DS" :
                dspMainCommon.addItemPopUp('DS');
                break;
            case "ITEM_TD":
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                dspMainCommon.addItemPopUp('TD' , storeType);
                break;
        }

    },
    /**
     * 상품조회팝업 콜백 함ㅅ후
     */
    addItemPopUpCallBack : function(resDataArr) {

        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType , storeType){

        var url = '/common/popup/itemPop?callback=dspMainCommon.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;

            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;

        }

        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백 함ㅅ후
     */
    addPromoPopUpCallBack : function(resDataArr) {

        linkInfoDiv.find('.linkInfo').val(resDataArr[0].promoNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 포로모션조회팝업
     */
    addPromoPopUp :  function(promoType){

        var url = '/common/popup/promoPop?callback=dspMainCommon.addPromoPopUpCallBack&isMulti=N&siteType=' + $("#siteType").val()+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);

    },
    //링크 상세 정보 입력
    setLinkInfo : function (data,linkInfo){

        $(linkInfo).find('.linkInfo').val(data.linkInfo);
        $(linkInfo).find('.linkInfoNm').val(data.linkInfoNm);

        if(data.linkType == 'ITEM_TD') {
            var storeType = data.linkOptions.substr(0,1);    //  H:HYPER, C:CLUB, E:EXPRESS
            var dlvStoreId = data.linkOptions.substr(2);

            switch (storeType) {
                case 'H' :
                    $(linkInfo).find('.storeType').val('HYPER');
                    break;
                case 'E' :
                    $(linkInfo).find('.storeType').val('EXP');
                    break;
                default:
                    $(linkInfo).find('.storeType').val('HYPER');
                    break;
            }

            // N : 일반상품, D : 택배점
            if(data.linkOptions.substr(1,1) == 'D') {
                $(linkInfo).find('.dlvStoreYn').prop("checked", true).val('Y');
                $(linkInfo).find('.dlvStoreList').prop("disabled", false).val(dlvStoreId);
            }
        }
    },
    //링크 타입에 따른 입력폼 변경
    changeLinkType : function(linkType, obj){

        var linkInfo ='';

        //링크없음
        var none = "";

        switch (linkType) {
            case "EXH"  :
            case "EVENT" :
            case "ITEM_DS" :
            case "ITEM_TD":
                linkInfo  = $("#linkCommon").html();
                break;
            case "URL":
                linkInfo  = $("#linkUrl").html();
                break;
            default :
                linkInfo =  none;
        }


        if($('#siteType').val() == 'HOME' && linkType == 'ITEM_TD'){

            linkInfo = $("#storeTypeHome").html() + linkInfo +  $("#dlvStoreTypeHome").html()

        }
        if($('#siteType').val() == 'CLUB' && linkType == 'ITEM_TD'){

            linkInfo = $("#storeTypeClub").html() + linkInfo +  $("#dlvStoreTypeClub").html()

        }
        if($('#loc1Depth').val() == 'TOP'){
            linkInfo += '<button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);">초기화</button>';
        }
        obj.html(linkInfo);
    },
    //배너 기본 정보 파라미터 유효성 검사
    setCommonBannerValid : function(){


        if($.jUtil.isEmpty($('#loc1Depth').val())){

            alert("전시 위치를 선택해 주세요");
            return false;

        }
        if($.jUtil.isEmpty($('#priority').val()) && $('#loc1Depth').val() != 'TOP' && $('#loc1Depth').val() != 'LOGO'&& $('#loc1Depth').val() != 'GNB'&& $('#loc1Depth').val() != 'GCATE'){

            alert("전시순서를 입력해 주세요");
            return false;

        }
        if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM']) && $('#loc1Depth').val() != 'TOP' && $('#loc1Depth').val() != 'LOGO' && $('#loc1Depth').val() != 'GNB'&& $('#loc1Depth').val() != 'GCATE') {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        if($.jUtil.isEmpty($('#bannerNm').val())){

            alert("배너명을 입력해 주세요");
            return false;

        }

        return true;


    },
    //배너 링크 기본 정보 유효성 검사
    setCommonLinkValid : function (data) {


        var loc1Depth = $('#loc1Depth').val();


        if( loc1Depth == 'BIG' && !$.jUtil.isEmpty(data.textYn ) && data.textYn =='Y' && $.jUtil.isEmpty( data.title)){

            alert("타이틀을 입력해 주세요." );
            return false;
        }
        if(loc1Depth == 'BIG' && !$.jUtil.isEmpty(data.textYn ) && data.textYn =='Y' && $.jUtil.isEmpty( data.subTitle)){

            alert("서브 타이틀을 입력해 주세요." );
            return false;

        }
        if($.jUtil.isEmpty(data.imgUrl) && loc1Depth !='TOP'){

            alert("이미지를 등록해 주세요." );
            return false;

        }
        if($.jUtil.isEmpty(data.linkInfo) && data.linkType != 'NONE'){

            alert("링크정보를 등록해 주세요." );
            return false;
        }
        if(data.linkType =='URL' && !dspPcMain.checkUrl(data.linkInfo)){

            alert("http:// 또는 https://를 포함하여 url을 입력해 주세요." );
            return false;

        }
        if(data.dlvStoreYn == 'Y' && $.jUtil.isEmpty(data.dlvStoreId) ){
            alert("택배점을 선택해 주세요");
            return false;

        }
        return true;

    }
};

dspMainCommon.img = {
    imgDiv : null,
    bigBannerProcessMap : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        dspMainCommon.img.resetImg();
        dspMainCommon.img.setImgProcessKey();
        dspMainCommon.img.imgDiv = null;

    },
    setImgProcessKey : function(){
        /**
            PcMainBigBanner1	1200*450
            PcMainBigBanner2	590*450
            PcMainBigBanner3	790*450
            PcMainBigBanner4	390*450
            PcMainBigBanner5	390*220
            PcMainBigBanner6	290*450
         */
        dspMainCommon.img.bigBannerProcessMap =
            [
                ["PcMainBigBanner1"],                   //1단
                ["PcMainBigBanner2","PcMainBigBanner2"],//2단 기본
                ["PcMainBigBanner3","PcMainBigBanner4"],//2단 좌측강조
                ["PcMainBigBanner4","PcMainBigBanner4","PcMainBigBanner4"], //3단 기본
                ["PcMainBigBanner3","PcMainBigBanner5","PcMainBigBanner5"], //3단좌측강조
                ["PcMainBigBanner6","PcMainBigBanner2","PcMainBigBanner6"], //3단 중앙 강조
                ["PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner4","PcMainBigBanner5","PcMainBigBanner5"], //5단 중앙 강조
                ["PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5"] //6단

            ];

    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ dspMainCommon.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var file = $('#itemFile');

        var loca1Depth = $('#loc1Depth').val();
        var processKey ='';
        switch (loca1Depth) {
            case "BIG" :

                var division = dspMainCommon.img.imgDiv.find('.division').val();
                processKey = dspMainCommon.img.getProcessKey(division);
                break;
            case "LWING" :
                var division = dspMainCommon.img.imgDiv.find('.division').val();
                processKey = dspMainCommon.img.getProcessKey(division);
                break;
            default:
                processKey = dspMainCommon.img.getProcessKey();
                break;

        }
        var params = {
            processKey : processKey,
            mode : 'IMG'
        };


        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        dspMainCommon.img.setImg(dspMainCommon.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                          /*  'src'   : hmpImgUrl + f.fileStoreInfo.fileId+"?"+Math.floor(Math.random()*1000)*/
                            'src'    : hmpImgUrl + "/provide/view?processKey=" +params.processKey+"&fileId=" + f.fileStoreInfo.fileId

                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        dspMainCommon.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        dspMainCommon.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    /**
     * 이미지 프로세스키
     * @param obj
     */
    getProcessKey : function (diviSion) {

        var processKey = '';
        var loca1Depth = $('#loc1Depth').val();

        switch (loca1Depth) {
            case "TOP":
                processKey ='PcMainTopBanner';
                break;
            case "LOGO":
                processKey ='PcMainLogoBanner';
                break;
            case "GNB":
                processKey ='PcMainGnbBanner';
                break;
            case "GCATE":
                processKey ='PcMainGcateBanner';
                break;
            case "BIG":
                var template = $("#template").val();
                var templateDetail = $("#templateDetail").val();
                switch (template) {

                    case "1":
                        processKey = dspMainCommon.img.bigBannerProcessMap[0][0];
                        break;
                    case "2":
                        if(templateDetail == 'N'){
                            processKey = dspMainCommon.img.bigBannerProcessMap[1][diviSion];
                        } else{
                            processKey = dspMainCommon.img.bigBannerProcessMap[2][diviSion];
                        }
                        break;
                    case "3":
                        if(templateDetail == 'N'){
                            processKey = dspMainCommon.img.bigBannerProcessMap[3][diviSion];
                        } else if(templateDetail == 'LP'){
                            processKey = dspMainCommon.img.bigBannerProcessMap[4][diviSion];
                        } else{
                            processKey = dspMainCommon.img.bigBannerProcessMap[5][diviSion];
                        }
                        break;
                    case "5":
                        processKey = dspMainCommon.img.bigBannerProcessMap[6][diviSion];
                        break;
                    case "6":
                        processKey = dspMainCommon.img.bigBannerProcessMap[7][diviSion];
                        break;
                }

                break;
            case "QUICK":
                processKey ='PcMainQuickBanner';
                break;
            case "CENTER":
                processKey ='PcMainCenterBanner';
                break;
            case "LWING":
                processKey ='PcMainLwingBanner1';
                break;
            default:

        }
        return processKey;
    }
};