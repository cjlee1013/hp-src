/**
 * 사이트관리 > 전시관리 > MOBILE 메인관리
 */
$(document).ready(function() {
    dspMobileMainListGrid.init();
    dspMobileMain.init();
    dspMainCommon.init();
    CommonAjaxBlockUI.global();

});

// dspMobileMain 그리드
var dspMobileMainListGrid = {
    gridView : new RealGridJS.GridView("dspMobileMainListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspMobileMainListGrid.initGrid();
        dspMobileMainListGrid.initDataProvider();
        dspMobileMainListGrid.event();
    },
    initGrid : function() {
        dspMobileMainListGrid.gridView.setDataSource(dspMobileMainListGrid.dataProvider);
        dspMobileMainListGrid.gridView.setStyles(dspMobileMainListGridBaseInfo.realgrid.styles);
        dspMobileMainListGrid.gridView.setDisplayOptions(dspMobileMainListGridBaseInfo.realgrid.displayOptions);
        dspMobileMainListGrid.gridView.setColumns(dspMobileMainListGridBaseInfo.realgrid.columns);
        dspMobileMainListGrid.gridView.setOptions(dspMobileMainListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspMobileMainListGrid.dataProvider.setFields(dspMobileMainListGridBaseInfo.dataProvider.fields);
        dspMobileMainListGrid.dataProvider.setOptions(dspMobileMainListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspMobileMainListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspMobileMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspMobileMainListGrid.dataProvider.clearRows();
        dspMobileMainListGrid.dataProvider.setRows(dataList);
    }
};

var dspMobileMain = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');
    },
    initSearchDate : function (flag) {
        dspMobileMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspMobileMain.setDate.setEndDate(0);
        dspMobileMain.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspMobileMain.search);
        $('#searchResetBtn').bindClick(dspMobileMain.searchFormReset);
        $('#setMobileMainBtn').bindClick(dspMobileMain.setDspMobileMain);
        $('#resetBtn').bindClick(dspMobileMain.resetForm);
    },
    checkUrl : function(url){
        var expUrl = /(http(s)?:\/\/)([a-z0-9\w])/gi;
        return expUrl.test(url);
    },
    /**
     * DSP MOBILE 메인 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspMain/getMobileMainList.json?' + $('#dspMainSearchForm').serialize()
            , data : null
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                dspMobileMain.resetForm();
                dspMobileMainListGrid.setData(res);
                $('#dspMobileMainTotalCount').html($.jUtil.comma(dspMobileMainListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('REGDT');
        dspMobileMain.initSearchDate('-7d');
        $('#searchSiteType, #searchDispYn, #searchLoc1Depth, #searchKeyword').val('');
        $('#searchType').val('BANNER');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = dspMobileMainListGrid.dataProvider.getJsonRow(selectRowId);

        $('#siteType').val(rowDataJson.siteType).trigger('change');
        $('#loc1Depth').val(rowDataJson.loc1depth).trigger('change');
        $('#siteType').val(rowDataJson.siteType).prop('disabled' , true);
        $('#mainNo').val(rowDataJson.mainNo);
        $('#dispYn').val(rowDataJson.dispYn);

        if(rowDataJson.loc1depth == "SPLASH") {
            $('#splash').val(rowDataJson.splash).trigger('change');
            $("#splashDiv").show();
            $("#priorityDiv").hide();
        } else {
            $('#priority').val(rowDataJson.priority);
            $("#splashDiv").hide();
            $("#priorityDiv").show();
        }

        if(rowDataJson.loc1depth === 'NOTI' || rowDataJson.loc1depth === 'EXP' || rowDataJson.loc1depth === 'HYPER' ) {
            $('#displayTimeTr').show();
        } else {
            $('#displayTimeTr').hide();
        }
        $('#dispTimeYn').val(rowDataJson.dispTimeYn).change();
        $('#dispStartTime').val(rowDataJson.dispStartTime);
        $('#dispEndTime').val(rowDataJson.dispEndTime);

        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);
        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNmCnt').html(rowDataJson.bannerNm.length);

        CommonAjax.basic({
            url : '/manage/dspMain/getMobileMainDetail.json?mainNo=' + rowDataJson.mainNo
            , data : null
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                switch (rowDataJson.loc1depth) {
                        case "LOGO" :
                            dspMainLogoBannerDetail.setBannerDetail(res);
                            break;
                        case "BIG":
                            dspMainBigBannerDetail.setBannerDetail(res);
                            break;
                        case "QUICK":
                            dspMainQuickBannerDetail.setBannerDetail(res);
                            break;
                        case "NOTI":
                        case "QML":
                        case "HTH":
                        case "LBB":
                            dspMainNotiBannerDetail.setBannerDetail(res);
                            break;
                        case "SPLASH":
                            $('#splash').val(rowDataJson.splash);
                            dspMainSplashBannerDetail.setBannerDetail(res);
                            break;
                        case "MKT":
                        case "CMKT":
                            dspMainMktBannerDetail.setBannerDetail(res);
                            break;
                        case "HYPER":
                            dspMainDeliveryBannerDetail.setBannerDetail(res);
                            break;
                        case "EXP":
                            dspMainDeliveryBannerDetail.setBannerDetail(res);
                            break;
                }
            }
        });
    },
    /**
     * DSP MOBILE 메인 등록/수정
     */
    setDspMobileMain : function() {
        //배너 기본 정보 역영 파라미터 유효성 검사
        if(!dspMainCommon.setCommonBannerValid()){
            return false;
        }

        var loc1Depth = $('#loc1Depth').val();
        var bannerCommon = $("#dspMobileMainSetForm").serializeObject();

        switch (loc1Depth) {
            case "LOGO" :
                if(!dspMainLogoBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainLogoBannerDetail.getBannerDetail();
                break;
            case "BIG":
                if(!dspMainBigBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainBigBannerDetail.getBannerDetail();
                break;
            case "QUICK":
                if(!dspMainQuickBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainQuickBannerDetail.getBannerDetail();
                break;
            case "NOTI":
            case "QML":
            case "HTH":
            case "LBB":
                if(!dspMainNotiBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainNotiBannerDetail.getBannerDetail();
                break;
            case "SPLASH":
                if(!dspMainSplashBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainSplashBannerDetail.getBannerDetail();
                break;
            case "MKT":
            case "CMKT":
                if(!dspMainMktBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainMktBannerDetail.getBannerDetail();
                break;
            case "HYPER" :
                if(!dspMainDeliveryBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainDeliveryBannerDetail.getBannerDetail();
                break;
            case "EXP" :
                if(!dspMainDeliveryBannerDetail.setBannerValid()) {
                    return false;
                }
                bannerDetail = dspMainDeliveryBannerDetail.getBannerDetail();
                break;

        }

        var bannerInfo  = new Object();
        bannerInfo = $.extend({}, bannerCommon, bannerDetail);

        CommonAjax.basic({
            url : '/manage/dspMain/setMobileMain.json'
            , data : JSON.stringify(bannerInfo)
            , method : "POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMobileMain.search();
            }
        });
    },
    /**
     * DSP MOBILE 메인 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#siteType').prop('disabled', false);
        $('#siteType').val('HOME').trigger('change');
    }
};

/**
 * 배너기본 정보 영역
 * 링크 정보 입력 영역 (링크 정보 초기화는 각 배너 타입 js파일에 구현)
 * 이미지 등록 삭제
 */
var dspMainCommon ={
    linkInfoDiv : null,
    storeList : null,
    $dispStoreCnt : null,
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSetDate('0');

        dspMainCommon.linkInfoDiv = null;
        dspMainCommon.storeList = null;
        dspMainCommon.$dispStoreCnt = null;
    },
    initSetDate : function (flag) {
        dspMobileMain.setDate = CalendarTime.datePickerRange('dispStartDt', 'dispEndDt');
        dspMobileMain.setDate.setEndDateByTimeStamp(0);
        dspMobileMain.setDate.setStartDate(flag);
        $('#searchEndDt').datepicker("option", "minDate", $('#dispStartDt').val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $("#siteType").bindChange(dspMainCommon.siteType);
        $("#loc1Depth").bindChange(dspMainCommon.changeLoc);
        $('#bannerNm').calcTextLength('keyup', '#bannerNmCnt');

        $('#dispTimeYn').change(function() {
            $('#displayTimeSpan').hide();
            $('#dispStartTime, #dispEndTime').val('');
            if($('#dispTimeYn').val() === 'Y') {
                $('#displayTimeSpan').show();
            }
        });

        // 스플래시 유형 구분 변경에 따란 전시 기간 필드 노출 비 노출 처리
        $(document).on('change', 'select[name="splash"]', function() {
            if($(this).val() == 'SEASON') {
                $("#dispPeriod").hide();
            } else {
                $("#dispPeriod").show();
            }
            dspMainCommon.changeDispStoreType($(this));
        });

        /***************************** 이미지 이벤트 등록 *****************************/
        $(document).on('change', 'input[name="fileArr"]', function() {
            dspMainCommon.img.addImg();
        });

        // 이미지 등록 버튼
        $(document).on('click', '.addImgBtn', function() {
            dspMainCommon.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();
        });

        // 이미지 삭제 버튼
        $(document).on('click', '.deleteImgBtn', function() {
            dspMainCommon.img.setImg($(this).next(), null, true);
        });
        /***************************** 이미지 이벤트 종료 *****************************/

        /***************************** 노출 점포 관려 이벤트 등록 *****************************/
        // 노출 점포 선택 라디오 버튼 ( 전체노출 : ALL ,노출 점포 :  PART )
        $(document).on('click', 'input:radio[name="dispStoreType"]', function() {
            dspMainCommon.setDispStoreType($(this));
        });
        // 노출 점포 변경 셀렉트 박스
        $(document).on('change', 'select[name="dispStoreType"]', function() {
            dspMainCommon.changeDispStoreType($(this));
        });
        // 점포 검색 팝업
        $(document).on('click', '.storeInfoBtn', function() {
            dspMainCommon.setStorePop($(this));
        });
        /***************************** 노출 점포 관려 이벤트 종료 *****************************/

        /***************************** 링크 정보 관려 이벤트 등록 *****************************/
        // 링크 정보 -> ITEM_TD -> 점포 타입 변경
        $(document).on('change', '.storeType', function() {
            dspMainCommon.changeLinkStoreType($(this));
        });
        // 택배점 설정 체크박스
        $(document).on('click', '.dlvStoreYn', function() {
            dspMainCommon.setDlvStoreYn($(this));
        });
        //링크타입 변경
        $(document).on('change', '.linkeType', function() {
            dspMainCommon.changeLinkType($(this).val(), $(this).next('div'));
        });
        /***************************** 링크 정보 관려 이벤트 종료 *****************************/
    },
    /**
     * 점포 상품 점포 유형 변경
     */
    changeLinkStoreType : function(obj) {
        $(obj).parent().find('.linkInfo').val('');
        $(obj).parent().find('.linkInfoNm').val('');
        $(obj).parent().find('.dlvStoreYn').prop("checked", false).val('N');
        if($(obj).val() == 'HYPER' || $(obj).val() == 'CLUB') {
            $(obj).parent().find('.dlvStoreList').prop("disabled", false).val('');
        } else {
            $(obj).parent().find('.dlvStoreList').prop("disabled", true).val('');
        }
    },
    setStorePop : function(obj) {
        dspMainCommon.$dispStoreCnt = $(obj).parent().find('#dispStoreCnt');
        if ($.jUtil.isEmpty(dspMainCommon.storeList)) {
            dspMainCommon.openStoreInfoPopup(obj);
        } else {
            dspMainCommon.openStoreInfoPopup(obj,dspMainCommon.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback : function(res) {
        var resCount = res.storeList.length;
        dspMainCommon.storeList = res.storeList;
        dspMainCommon.$dispStoreCnt.html(resCount);
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(obj, data) {
        var storeType = $(obj).parent().find('select[name="dispStoreType"]').val();
        /*$('select[name="dispStoreType"]').val();*/
        $('.storePop input[name="storeTypePop"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if ($("#chgYn").val() == 'Y') {
            $('.storePop input[name="setYn"]').val('N');
        } else {
            $('.storePop input[name="setYn"]').val('Y');
        }

        var url = "/common/popup/storeSelectPop";
        var target = "storeSelectPop";
        windowPopupOpen(url, target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;
        frm.submit();
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var siteType  = $('#siteType').val();
        var dispStoreType = $(obj).val();
        var loc1Depth = $('#loc1Depth').val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(dispStoreType == 'ALL') {
            $dispStore.hide();
            dspMainCommon.storeList = null;
        } else {
            if(siteType == 'HOME') {
                $dispStore.prepend($('#storeTypeHome').html());
                if(loc1Depth == 'HYPER' || loc1Depth == 'EXP' ){
                    $dispStore.find('select').val(loc1Depth);
                    $dispStore.find('select').attr('disabled' , true);
                }
            } else {
                $dispStore.prepend($('#storeTypeClub').html());
            }

            $dispStore.find('select').attr('name', 'dispStoreType');
            $dispStore.show();
        }

        dspMainCommon.changeDispStoreType(obj);
    },
    changeDispStoreType : function(obj) {
        $(obj).parent().parent().find('#dispStoreCnt').html('0');
        dspMainCommon.storeList = null;
        /*$('#dispStoreCnt').html('0');*/
    },
    /**
     * 택배점 설정 검사
     */
    setDlvStoreYn : function(obj) {
        if($(obj).is(":checked")) {
            var storeTyep = $(obj).parent().parent().find('.storeType').val();
            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB') {
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');

                return false;
            }
            if($.jUtil.isEmpty(linkInfo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');

                return false;
            }

            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled", false);
        } else {
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled", true);
        }
    },
    /**
     * 사이트 구분 변경
     */
    siteType : function() {
        $("#loc1Depth").val('').trigger('change');
        $("#dispYn").val('Y');
        $("#bannerNm, #mainNo").val('');
        dspMainCommon.initSetDate('0');
    },
    /**
     * 전체 배너 템플릿 초기화
     */
    resetAllLinkTypeTemplate : function() {
        dspMainLogoBannerDetail.resetForm();
        dspMainBigBannerDetail.resetForm();
        dspMainQuickBannerDetail.resetForm();
        dspMainNotiBannerDetail.resetForm();
        dspMainSplashBannerDetail.resetForm();
        dspMainMktBannerDetail.resetForm();
        dspMainDeliveryBannerDetail.resetForm();
    },
    /**
     * 전시 위처 변경 이벤트
     */
    changeLoc : function() {
        var loc1Depth = $("#loc1Depth").val();

        $('.bannerTemplate').hide();
        $("#splashDiv").hide();
        $("#splash").val("");
        $("#priorityDiv").show();
        $("#divType").html('전시순서');
        $("#dispPeriod").show();
        $('#priority').val('').prop("disabled", false);
        $('#siteType').prop('disabled' , false);
        /* $('#siteType').val('HOME');*/

        $('#displayTimeTr').hide();
        $('#dispTimeYn').val('N').change();
        $('#dispStartTime, #dispEndTime').val('');

        dspMainCommon.resetAllLinkTypeTemplate();

        switch (loc1Depth) {
            case 'BIG' :
                $("#bannerTemplateBig").show();
                dspMainBigBannerDetail.changeLinkType();
                break;
            case 'LOGO' :
                $("#bannerTemplateLogo").show();
                // $('#priority').val('').prop("disabled", true);
                dspMainLogoBannerDetail.changeLinkType();
                break;
            case 'NOTI' :
                $('#displayTimeTr').show();
                $("#bannerTemplateNoti").show();
                $("#noticeBannerTxt").html("• 공지배너");
                dspMainNotiBannerDetail.changeLinkType();
                break;
            case 'QML' :
                $("#bannerTemplateNoti").show();
                $("#noticeBannerTxt").html("• 퀵메뉴 하단배너");
                dspMainNotiBannerDetail.changeLinkType();
                break;
            case 'HTH' :
                $("#bannerTemplateNoti").show();
                $("#noticeBannerTxt").html("• 홈테마 상단배너");
                dspMainNotiBannerDetail.changeLinkType();
                break;
            case 'LBB' :
                $("#bannerTemplateNoti").show();
                $("#noticeBannerTxt").html("• 사자고 하단배너");
                dspMainNotiBannerDetail.changeLinkType();
                break;
            case 'QUICK' :
                $("#bannerTemplateQuick").show();
                dspMainQuickBannerDetail.changeLinkType();
                break;
            case 'SPLASH' :
                $("#bannerTemplateSplash").show();
                $("#splashDiv").show();
                $("#priorityDiv").hide();
                $("#divType").html('유형구분');
                /*dspMainSplashBannerDetail.changeLinkType();*/
                break;
            case 'MKT' :
                $("#bannerTemplateMkt").show();
                dspMainMktBannerDetail.changeLinkType();
                $("#mktImg").html("760*600");
                $("#mktTitle").html("• 마케팅 배너");
                break;
            case 'CMKT' :
                $("#bannerTemplateMkt").show();
                dspMainMktBannerDetail.changeLinkType();
                $('#siteType').val('CLUB').prop('disabled' , true);
                $("#mktImg").html("670*320");
                $("#mktTitle").html("• 클럽 마케팅 배너");
                break;
            case 'HYPER' :
                $('#displayTimeTr').show();
                $('#bannerTemplateDelivery').show();
                $('#deliveryImg').html("392*136");
                $('#deliveryBannerTxt').html("• HYPER 배송배너");
                dspMainDeliveryBannerDetail.changeLinkType();
                break;
            case 'EXP' :
                $('#displayTimeTr').show();
                $('#bannerTemplateDelivery').show();
                $('#deliveryImg').html("392*136");
                $('#deliveryBannerTxt').html("• EXPRESS 배송배너");
                dspMainDeliveryBannerDetail.changeLinkType();
                break;
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function(obj) {
        linkInfoDiv = $(obj).parent();

        var linkeType  = $(obj).parent().prev('select').val();
        var storeType = $(obj).parent().find('.storeType').val();
        var gnbDeviceType = $(obj).parent().find('.gnbDeviceType').val();
        var siteType = $('#siteType').val();

        switch (linkeType) {
            case "EXH"  :
                dspMainCommon.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                dspMainCommon.addPromoPopUp('EVENT');
                break;
            case "ITEM_DS" :
                dspMainCommon.addItemPopUp('DS');
                break;
            case "ITEM_TD":
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                dspMainCommon.addItemPopUp('TD', storeType);
                break;
            case "GNB":
                dspMainCommon.addGnbPopUp(gnbDeviceType, siteType);
                break;
        }
    },
    /**
     * GNB 조회 콜백 함수
     */
    addGnbPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].gnbNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].dispNm);
    },
    /**
     * GNB 조회 팝업
     */
    addGnbPopUp : function(gnbDeviceType, siteType) {
        $.jUtil.openNewPopup('/common/popup/gnbPop?callback=dspMainCommon.addGnbPopUpCallBack&isMulti=N&deviceType=' + gnbDeviceType + '&siteType=' + siteType, 1084, 650);
    },
    /**
     * 상품조회팝업 콜백 함수
     */
    addItemPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType, storeType) {
        var url = '/common/popup/itemPop?callback=dspMainCommon.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;
            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;
        }

        $.jUtil.openNewPopup(url, 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백 함ㅅ후
     */
    addPromoPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].promoNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 포로모션조회팝업
     */
    addPromoPopUp : function(promoType) {
        var url = '/common/popup/promoPop?callback=dspMainCommon.addPromoPopUpCallBack&isMulti=N&siteType=' + $("#siteType").val()+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 링크 상세 정보 입력
     */
    setLinkInfo : function (data,linkInfo) {
        $(linkInfo).find('.linkInfo').val(data.linkInfo);
        $(linkInfo).find('.linkInfoNm').val(data.linkInfoNm);

        if(data.linkType == 'ITEM_TD') {
            var storeType = data.linkOptions.substr(0,1);    //  H:HYPER, C:CLUB, E:EXPRESS
            var dlvStoreId = data.linkOptions.substr(2);

            switch (storeType) {
                case 'H' :
                    $(linkInfo).find('.storeType').val('HYPER');
                    break;
                case 'E' :
                    $(linkInfo).find('.storeType').val('EXP');
                    break;
                default:
                    $(linkInfo).find('.storeType').val('HYPER');
                    break;
            }

            // N : 일반상품, D : 택배점
            if(data.linkOptions.substr(1,1) == 'D') {
                $(linkInfo).find('.dlvStoreYn').prop("checked", true).val('Y');
                $(linkInfo).find('.dlvStoreList').prop("disabled", false).val(dlvStoreId);
            }
        }

        if(data.linkType == 'GNB') {
            $(linkInfo).find('.gnbDeviceType').val(data.gnbDeviceType);
        }
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function(linkType, obj) {
        var loc1Depth = $('#loc1Depth').val();
        var linkInfo ='';
        var none = "";

        switch (linkType) {
            case "EXH"  :
            case "EVENT" :
            case "ITEM_DS" :
            case "ITEM_TD":
                linkInfo  = $("#linkCommon").html();
                break;
            case "GNB":
                linkInfo  = $("#linkGnbDevice").html() + $("#linkCommon").html();
                break;
            case "URL":
                linkInfo  = $("#linkUrl").html();
                break;
            default :
                linkInfo =  none;
        }

        if($('#siteType').val() == 'HOME' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeHome").html() + linkInfo +  (( loc1Depth == 'HYPER' || loc1Depth == 'EXP' ) ? '' : $("#dlvStoreTypeHome").html()) ;
        }
        if($('#siteType').val() == 'CLUB' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeClub").html() + linkInfo +  $("#dlvStoreTypeClub").html()
        }
        if($('#loc1Depth').val() == 'TOP') {
            linkInfo += '<button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);">초기화</button>';
        }

        obj.html(linkInfo);

        if( loc1Depth == 'HYPER' || loc1Depth == 'EXP' ){
            $('#deliveryBannerLinkInfo').find('select').val(loc1Depth);
            $('#deliveryBannerLinkInfo').find('select').attr('disabled', true);
        }

    },
    /**
     * 배너 링크 기본 정보 유효성 검사
     */
    setCommonLinkValid : function (data) {
        var loc1Depth = $('#loc1Depth').val();
        var siteType = $('#siteType').val();

        if(siteType =='CLUB' && loc1Depth == 'BIG' && $.jUtil.isEmpty(data.title)) {
            alert("타이틀을 입력해 주세요." );
            return false;
        }
        if(siteType =='CLUB' && loc1Depth == 'BIG' && $.jUtil.isEmpty( data.subTitle)) {
            alert("서브 타이틀1을 입력해 주세요." );
            return false;
        }
        if(siteType =='CLUB' && loc1Depth == 'BIG' && $.jUtil.isEmpty( data.subTitle2)) {
            alert("서브 타이틀2을 입력해 주세요." );
            return false;
        }
        if($.jUtil.isEmpty(data.imgUrl) && loc1Depth !='TOP' && loc1Depth !='LWING') {
            alert("이미지를 등록해 주세요." );
            return false;
        }
        if($.jUtil.isEmpty(data.linkInfo) && data.linkType != 'NONE'&& data.linkType != 'HOME') {
            alert("링크정보를 등록해 주세요." );
            return false;
        }
        if(data.linkType =='URL' && !dspMobileMain.checkUrl(data.linkInfo)) {
            alert("http:// 또는 https://를 포함하여 url을 입력해 주세요." );
            return false;
        }
        if(data.dlvStoreYn == 'Y' && $.jUtil.isEmpty(data.dlvStoreId)) {
            alert("택배점을 선택해 주세요");
            return false;
        }

        return true;
    },
    /**
     * 배너 기본 정보 파라미터 유효성 검사
     */
    setCommonBannerValid : function() {
        if($.jUtil.isEmpty($('#loc1Depth').val())) {
            alert("전시 위치를 선택해 주세요");
            return false;
        }
        if($.jUtil.isEmpty($('#priority').val()) && $('#loc1Depth').val() != 'SPLASH') {
            alert("전시순서를 입력해 주세요");
            return false;
        }
        if(!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])&& $('#loc1Depth').val() != 'SPLASH') {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        // 배송시간 배너일 경우 전시구간 확인
        if($('#loc1Depth').val() === 'NOTI') {
            var dispTimeYn = $("#dispTimeYn").val();
            var dispStartTime = $("#dispStartTime").val();
            var dispEndTime = $("#dispEndTime").val();

            if (dispTimeYn == 'Y') {
                if (!moment(dispStartTime, 'HH:mm', true).isValid()) {
                    alert("전시시간대(시작) 형식이 맞지 않습니다. 시:분 형태로 입력 가능합니다.");
                    $("#dispStartTime").val('').focus();
                    return false;
                } else if (!moment(dispEndTime, 'HH:mm', true).isValid()) {
                    alert("전시시간대(종료) 형식이 맞지 않습니다. 시:분 형태로 입력 가능합니다.");
                    $("#dispEndTime").val('').focus();
                    return false;
                }
                // HH의 00 -> 24로 출력 됨.
                if((moment(dispStartTime, 'HH:mm').format('k')) > 23 && dispStartTime.split(':')[0] != '00') {
                    alert("시작 시간의 범위는 0~23 사이로 입력해 주세요.");
                    $("#dispStartTime").val('').focus();
                    return false;
                }
                if((moment(dispEndTime, 'HH:mm').format('k')) > 23 && dispEndTime.split(':')[0] != '00') {
                    alert("종료 시간의 범위는 0~23 사이로 입력해 주세요.");
                    $("#dispEndTime").val('').focus();
                    return false;
                }

                if (dispStartTime > dispEndTime) {
                    alert("전시구간 시작시간은 종료시간보다 빠를 수 없습니다.");
                    $("#dispStartTime").val('').focus();
                    return false;
                }
            }
        }
        if($.jUtil.isEmpty($('#bannerNm').val())) {
            alert("배너명을 입력해 주세요");
            return false;
        }

        return true;
    }
};

dspMainCommon.img = {
    imgDiv : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        dspMainCommon.img.resetImg();
        dspMainCommon.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ dspMainCommon.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var file = $('#itemFile');
        var params = {
            processKey : dspMainCommon.img.getProcessKey(),
            mode : 'IMG'
        };


        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        dspMainCommon.img.setImg(dspMainCommon.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,

                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        dspMainCommon.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        dspMainCommon.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    /**
     * 이미지 프로세스키
     * @param obj
     */
    getProcessKey : function () {

        var processKey = '';
        var loca1Depth = $('#loc1Depth').val();
        var siteType  = $('#siteType').val();

        switch (loca1Depth) {
            case "LOGO":
                processKey ='MobileMainLogBanner';
                break;
            case "BIG":
                if(siteType == 'HOME'){
                    processKey ='MobileMainBigBanner2';
                }else{
                    processKey ='MobileMainBigBanner';
                }
                break;
            case "QUICK":
                processKey ='MobileMainQuickBanner';
                break;
            case "NOTI":
            case 'QML' :
            case 'HTH' :
            case 'LBB' :
                processKey ='MobileMainNotiBanner';
                break;
            case "SPLASH":
                processKey ='MobileMainSplashBanner';
                break;
            case "MKT":
                processKey ='MobileMainMktBanner';
                break;
            case "CMKT":
                processKey ='MobileMainClubMktBanner';
                break;
            case "HYPER":
                processKey ='MobileMainDeliveryBanner';
                break;
            case "EXP":
                processKey ='MobileMainDeliveryBanner';
                break;

        }
        return processKey;
    }
};