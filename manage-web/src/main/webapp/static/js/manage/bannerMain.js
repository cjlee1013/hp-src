/**
 * 사이트관리 > 전시관리 > 배너관리
 */
$(document).ready(function() {
    bannerListGrid.init();
    banner.init();
    bannerCommon.init();
    CommonAjaxBlockUI.global();
});

// banner 그리드
var bannerListGrid = {
    gridView : new RealGridJS.GridView("bannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        bannerListGrid.initGrid();
        bannerListGrid.initDataProvider();
        bannerListGrid.event();
    },
    initGrid : function() {
        bannerListGrid.gridView.setDataSource(bannerListGrid.dataProvider);
        bannerListGrid.gridView.setStyles(bannerListGridBaseInfo.realgrid.styles);
        bannerListGrid.gridView.setDisplayOptions(bannerListGridBaseInfo.realgrid.displayOptions);
        bannerListGrid.gridView.setColumns(bannerListGridBaseInfo.realgrid.columns);
        bannerListGrid.gridView.setOptions(bannerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        bannerListGrid.dataProvider.setFields(bannerListGridBaseInfo.dataProvider.fields);
        bannerListGrid.dataProvider.setOptions(bannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        bannerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            banner.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        bannerListGrid.dataProvider.clearRows();
        bannerListGrid.dataProvider.setRows(dataList);
    },
};

//배너 검색, 초기화
//배너 등록
//전체 초기화
//그리드 선택
var banner = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');
    },
    initSearchDate : function (flag) {
        banner.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        banner.setDate.setEndDate(0);
        banner.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(banner.search);
        $('#searchResetBtn').bindClick(banner.searchFormReset);
        $('#setPcMainBtn').bindClick(banner.setBanner);
        $('#resetBtn').bindClick(bannerCommon.resetBannerForm);
        $('#bannerNm').calcTextLength('keyup', '#bannerNmCnt');

        $('#template').bindChange(bannerCommon.changeTemplate);
        $('#searchLoc1Depth').bindChange(banner.setCategoryData, 'search');
        $('#loc1Depth').bindChange(banner.setCategoryData, 'set');

        $('#device').bindChange(banner.switchTextBox);

        $('.textYn').change(function() {
            bannerCommon.setText(this);
        });

        $('#dispTimeYn').change(function() {
            $('#displayTimeSpan').hide();
            $('#dispStartTime, #dispEndTime').val('');
            if($('#dispTimeYn').val() === 'Y') {
                $('#displayTimeSpan').show();
            }
        });

        $(".linkeType").each(function() {
            $(this).find("option[value='GNB']").hide();
        });
    },
    /**
     * 전시위치 > 카테고리 정보
     */
    switchTextBox : function() {
        var device = $('#device').val();
        if(device === 'MOBILE') {
            $('.textYn').each(function() {
                $(this).val('N').attr('disabled', true);
                $(this).parent().parent().find('.bigBannerTitle').val('').prop('readOnly', true);
                $(this).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly', true);
            });
            $(".linkeType").each(function() {
                $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
                $(this).find("option[value='GNB']").show();
            });
        } else {
            $('.textYn').each(function() {
                $(this).val('N').attr('disabled', false).prop('checked', false);
                $(this).parent().parent().find('.bigBannerTitle').val('').prop('readOnly', true);
                $(this).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly', true);
            });
            $(".linkeType").each(function() {
                $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
                $(this).find("option[value='GNB']").hide();
            });
        }
        bannerCommon.img.init();
        banner.changeImgSizeDescription($('#loc1Depth').val());
    },
    /**
     * 전시위치 > 카테고리 정보
     */
    setCategoryData : function(type) {
        if(type === 'search') {
            var loc1DepthValue = $('#searchLoc1Depth').val();
            switch (loc1DepthValue) {
                case 'LBB' :
                    commonCategory.setCategorySelectBox(1, '', '', $('#searchLoc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#searchLoc3Depth'));
                    break;
                case 'MBB' :
                    commonCategory.setCategorySelectBox(1, '', '', $('#searchLoc2Depth'));
                    commonCategory.setCategorySelectBox(2, '', '', $('#searchLoc3Depth'));
                    break;
                default :
                    commonCategory.setCategorySelectBox(0, '', '', $('#searchLoc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#searchLoc3Depth'));
                    break;
            }
        } else {
            var loc1DepthValue = $('#loc1Depth').val();
            $('#priority').val('');
            switch (loc1DepthValue) {
                case 'LBB' :
                    $('#device').val('PC').trigger('change');
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", false);
                    commonCategory.setCategorySelectBox(1, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                case 'MBB' :
                    $('#device').val('PC').trigger('change');
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", false);
                    commonCategory.setCategorySelectBox(1, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(2, '', '', $('#loc3Depth'));
                    break;
                case 'CMB':
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", true);
                    $('#device').val('MOBILE').trigger('change');
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                case 'CSB':
                case 'BKB':
                case 'ORB':
                case 'OCB':
                case 'DTB':
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    $('#device').val('PC').trigger('change').attr("disabled", false);
                    $('#priority, #loc2Depth, #loc3Depth').attr("disabled", true);
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                case 'SBB':
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    $('#device').val('PC').trigger('change');
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", true);
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                case 'AEB':
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    $('#device').val('MOBILE').trigger('change').attr('disabled', true);
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", true);
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                case 'UGB':
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    $('#device').val('PC').trigger('change').attr("disabled", false);
                    $('#priority').attr("disabled", false);
                    $('#loc2Depth, #loc3Depth').attr("disabled", true);
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
                default :
                    $('#device').val('PC').trigger('change');
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                    $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", false);
                    commonCategory.setCategorySelectBox(1, '', '', $('#loc2Depth'));
                    commonCategory.setCategorySelectBox(0, '', '', $('#loc3Depth'));
                    break;
            }
            banner.changeImgSizeDescription(loc1DepthValue);
        }
    },
    changeImgSizeDescription : function (loc1DepthValue) {
        var device = $('#device').val();
        var template = $('#template').val();

        $("[class^='bigBannerDiv']").find('#bannerDivImgType').html('JPG / JPEG / PNG');

        switch (loc1DepthValue) {
            case 'LBB' :
            case 'MBB' :
                if(device === 'PC') {
                    switch (template) {
                        case '1' :
                            $("[class^='bigBannerDiv']").find('#bannerDivImgSize').html('940*340');
                            break;
                        case '2' :
                            $("[class^='bigBannerDiv']").find('#bannerDivImgSize').html('460*340');
                            break;
                        case '3' :
                            $("[class^='bigBannerDiv']").find('#bannerDivImgSize').html('300*340');
                            break
                    }
                } else {
                    $("[class^='bigBannerDiv']").find('#bannerDivImgSize').html('750*460');
                }
                break;
            case 'CMB' :
                $('#bannerDivImgSize').html('670*140');
                break;
            case 'CSB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('940*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('750*140');
                    break;
                }
            case 'DTB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('540*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('670*140');
                    break;
                }
                break;
            case 'BKB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('1200*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('750*140');
                    break;
                }
                break;
            case 'ORB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('640*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('670*320');
                    break;
                }
                break;
            case 'OCB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('1200*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('750*140');
                    break;
                }
                break;
            case 'AEB' :
                $('#bannerDivImgSize').html('600*600');
                break;
            case 'SBB' :
                $('#bannerDivImgSize').html('200*200');
                $("[class^='bigBannerDiv']").find('#bannerDivImgType').html('PNG');
                break;
            case 'UGB' :
                if(device === 'PC') {
                    $('#bannerDivImgSize').html('1200*130');
                    break;
                } else {
                    $('#bannerDivImgSize').html('750*140');
                    break;
                }
                break;
            default :
                $('#bannerDivImgSize').html('940*340');
                break;
        }
    },
    changeCategoryCustomSelectBox : function(type, depth, prefix, afterPrefix) {
        if(type === 'search') {
            var loc1DepthValue = $('#searchLoc1Depth').val();
            switch (loc1DepthValue) {
                case 'LBB' :
                    break;
                case 'MBB' :
                    commonCategory.changeCategoryCustomSelectBox(depth, prefix, afterPrefix);
                    break;
                default :
                    break;
            }
        } else {
            var loc1DepthValue = $('#loc1Depth').val();
            switch (loc1DepthValue) {
                case 'LBB' :
                    break;
                case 'MBB' :
                    commonCategory.changeCategoryCustomSelectBox(depth, prefix, afterPrefix);
                    break;
                default :
                    break;
            }
        }
    },
    /**
     * 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/banner/getBannerList.json'
            , data : $('#bannerSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                bannerListGrid.setData(res);
                $('#bannerTotalCount').html($.jUtil.comma(bannerListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('regDt');
        banner.initSearchDate('-7d');
        $('#searchSiteType, #searchDispYn, #searchDevice, #searchLoc1Depth').val('');
        commonCategory.setCategorySelectBox(0, '', '', $('#searchLoc2Depth'));
        commonCategory.setCategorySelectBox(0, '', '', $('#searchLoc3Depth'));
        $('#searchType').val('bannerNm');
        $('#searchKeyword').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = bannerListGrid.dataProvider.getJsonRow(selectRowId);
        var loc1Depth = rowDataJson.loc1Depth;

        $('#bannerNo').val(rowDataJson.bannerNo);
        $('#loc1Depth').val(loc1Depth).trigger('change');
        $('#siteType').val(rowDataJson.siteType);
        $('#dispYn').val(rowDataJson.dispYn);
        $('#priority').val(rowDataJson.priority);
        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);

        if(loc1Depth === 'DTB') {
            $('#displayTimeTr').show();
        } else {
            $('#displayTimeTr').hide();
        }
        $('#dispTimeYn').val(rowDataJson.dispTimeYn).change();
        $('#dispStartTime').val(rowDataJson.dispStartTime);
        $('#dispEndTime').val(rowDataJson.dispEndTime);

        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNmCnt').html(rowDataJson.bannerNm.length);
        $('#device').val(rowDataJson.device);
        banner.switchTextBox();

        switch (loc1Depth) {
            case "LBB" :
                commonCategory.setCategorySelectBox(1, '', rowDataJson.loc2Depth, $('#loc2Depth'));
                break;
            case "MBB" :
                commonCategory.setCategorySelectBox(1, '', rowDataJson.loc2Depth, $('#loc2Depth'));
                commonCategory.setCategorySelectBox(2, rowDataJson.loc2Depth, rowDataJson.loc3Depth, $('#loc3Depth'));
                break;
        }

        CommonAjax.basic({url:'/manage/banner/getBannerLinkList.json?bannerNo=' + rowDataJson.bannerNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                bannerCommon.setBannerDetail(res, rowDataJson.template);
            }});

        switch (loc1Depth) {
            case 'LBB' :
            case 'MBB' :
                $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                break;
            case 'CMB':
            case 'CSB':
            case 'BKB':
            case 'ORB':
            case 'OCB':
            case 'DTB':
            case 'SBB':
            case 'AEB':
            case 'UGB':
                break;
            default :
                $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                break;
        }
    },

    /**
     * DSP PC 메인 등록/수정
     */
    setBanner : function() {
        if(!bannerCommon.setBannerValid()){
            return false;
        }

        var bannerDetail = $("#bannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < parseInt($("#template").val()) ; i++){
            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD') {
                //링크 옵션 설정1
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }
            }
            linkList.push(linkInfo);
            linkInfo = new Object();
        }
        bannerDetail.linkList = linkList;


        CommonAjax.basic({
            url:'/manage/banner/setBanner.json',
            data : JSON.stringify(bannerDetail)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                bannerCommon.resetForm();
                bannerCommon.resetBannerForm();
                // banner.searchFormReset();
                banner.search();
            }
        });
    },
    /**
     *  입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#siteType').prop('disabled', false);
        $('#siteType').val('HOME').trigger('change');
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        banner.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        banner.setDateTime.setEndDateByTimeStamp(flag);
        banner.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
};

// 배너기본 정보 영역
// 링크 정보 입력 영역 (링크 정보 초기화는 각 배너 타입 js파일에 구현)
// 이마지 등록 삭제
var bannerCommon = {

    linkInfoDiv : null,
    storeList : null,
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        banner.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        bannerCommon.linkInfoDiv = null;
        bannerCommon.storeList = null;
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        //$("#siteType").bindChange(bannerCommon.siteType);
        $("#loc1Depth").bindChange(bannerCommon.changeLoc);

        /*****************************노출 점포 관려 이벤트 등록*******************************************/
        $(document).on('change','input[name="fileArr"]',function(){
            bannerCommon.img.addImg();
        });

        //이미지 등록 버튼
        $(document).on('click','.addImgBtn',function(){
            bannerCommon.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();
        });

        //이미지 삭제 버튼
        $(document).on('click','.deleteImgBtn',function(){
            bannerCommon.img.setImg($(this).next(), null, true);
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('click','input:radio[name="dispStoreType"]',function(){
            bannerCommon.setDispStoreType($(this));
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('change','select[name="dispStoreType"]',function(){
            bannerCommon.changeDispStoreType($(this));
        });

        //점포 검색 팝업
        $(document).on('click','.storeInfoBtn',function(){
            bannerCommon.setStorePop($(this));
        });
        /*****************************노출 점포 관려 이벤트 종료*******************************************/

        /******************************링크 정보 관려 이벤트 등록******************************************/
        //링크 정보  -> ITEM_TD -> 점포 타입 변경
        $(document).on('change','.storeType',function(){
            bannerCommon.changeLinkStoreType($(this));
        });

        //택배점 설정 체크박스
        $(document).on('click','.dlvStoreYn',function(){
            bannerCommon.setDlvStoreYn($(this));
        });

        //링크타입 변경
        $(document).on('change','.linkeType',function(){
            bannerCommon.changeLinkType($(this).val() , $(this).next('div') );
        });
        /******************************링크 정보 관려 이벤트 종료******************************************/
    },
    /**
     * 점포 상품 점포 유형 변경
     */
    changeLinkStoreType : function(obj) {
        $(obj).parent().find('.linkInfo').val('');
        $(obj).parent().find('.linkInfoNm').val('');
        $(obj).parent().find('.dlvStoreYn').prop("checked", false).val('N');

        if($(obj).val() == 'HYPER' ||  $(obj).val() == 'CLUB'){
            $(obj).parent().find('.dlvStoreList').prop("disabled", false).val('');
        } else{
            $(obj).parent().find('.dlvStoreList').prop("disabled", true).val('');
        }
    },
    setStorePop : function(obj) {
        if ($.jUtil.isEmpty(bannerCommon.storeList)) {
            bannerCommon.openStoreInfoPopup();
        } else {
            bannerCommon.openStoreInfoPopup(bannerCommon.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        var resCount = res.storeList.length;

        bannerCommon.storeList = res.storeList;
        $('#dispStoreCnt').html(resCount);
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(data) {
        var storeType = $('select[name="dispStoreType"]').val();

        $('.storePop input[name="storeType"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if ($("#chgYn").val() == 'Y') {
            $('.storePop input[name="setYn"]').val('N');
        } else {
            $('.storePop input[name="setYn"]').val('Y');
        }

        var target = "storeSelectPop";
        windowPopupOpen('', target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var siteType  = $('#siteType').val();
        var dispStoreType = $(obj).val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(dispStoreType == 'ALL') {
            $dispStore.hide();
            bannerCommon.storeList = null;
        } else {
            if(siteType == 'HOME') {
                $dispStore.prepend($('#storeTypeHome').html());
            } else {
                $dispStore.prepend($('#storeTypeClub').html());
            }
            $dispStore.find('select').attr('name' ,'dispStoreType');
            $dispStore.show();
        }
        bannerCommon.changeDispStoreType();
    },
    changeDispStoreType : function() {
        bannerCommon.storeList = null;
        $('#dispStoreCnt').html('0');
    },
    /**
     * 택배점 설정 검사
     */
    setDlvStoreYn : function(obj) {
        if($(obj).is(":checked")) {
            var storeTyep = $(obj).parent().parent().find('.storeType').val();
            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB') {
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            if ($.jUtil.isEmpty(linkInfo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled" ,false);
        } else {
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled" ,true);
        }
    },
    /**
     * 전체 배너 템플릿 초기화
     */
    resetAllLinkTypeTemplate : function() {
        bannerCommon.resetForm();
    },
    /**
     * 전시 위치 변경 이벤트
     */
    changeLoc : function() {
        var loc1Depth = $("#loc1Depth").val();

        $('.bannerTemplate, #displayTimeTr').hide();
        bannerCommon.resetAllLinkTypeTemplate();

        $('#dispTimeYn').val('N').change();
        $('#dispStartTime, #dispEndTime').val('');

        switch (loc1Depth) {
            case 'BIG' :
                $("#bannerTemplateBig").show();
                bannerCommon.changeBannerLinkType();
                break;
            case 'DTB' :
                $("#displayTimeTr").show();
                break;
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function(obj) {
        linkInfoDiv = $(obj).parent();

        var linkeType  = $(obj).parent().prev('select').val();
        var storeType = $(obj).parent().find('.storeType').val();
        var gnbDeviceType = $(obj).parent().find('.gnbDeviceType').val();
        var siteType = $('#siteType').val();

        switch (linkeType) {
            case "EXH"  :
                bannerCommon.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                bannerCommon.addPromoPopUp('EVENT');
                break;
            case "ITEM_DS" :
                bannerCommon.addItemPopUp('DS');
                break;
            case "ITEM_TD" :
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                bannerCommon.addItemPopUp('TD' , storeType);
                break;
            case "GNB":
                bannerCommon.addGnbPopUp(gnbDeviceType , siteType);
                break;
        }
    },
    /**
     * GNB 조회 콜백 함수
     */
    addGnbPopUpCallBack : function(resDataArr){
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].gnbNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].bannerNm);
    },
    /**
     * GNB 조회 팝업
     */
    addGnbPopUp : function(gnbDeviceType , siteType){
        $.jUtil.openNewPopup('/common/popup/gnbPop?callback=bannerCommon.addGnbPopUpCallBack&isMulti=N&deviceType=' +gnbDeviceType + '&siteType='+ siteType  , 1084, 650);
    },
    /**
     * 상품조회팝업 콜백 함ㅅ후
     */
    addItemPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType , storeType){
        var url = '/common/popup/itemPop?callback=bannerCommon.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;
            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;
        }
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백 함ㅅ후
     */
    addPromoPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].promoNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 포로모션 조회팝업
     */
    addPromoPopUp :  function(promoType){
        var url = '/common/popup/promoPop?callback=bannerCommon.addPromoPopUpCallBack&isMulti=N&siteType=' + $("#siteType").val()+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 링크 상세 정보 입력
     */
    setLinkInfo : function (data, obj) {

        $(obj).find('.linkInfo').val(data.linkInfo);
        $(obj).find('.linkInfoNm').val(data.linkInfoNm);

        if(data.linkType == 'ITEM_TD') {
            var storeType = data.linkOptions.substr(0,1);    //  H:HYPER, C:CLUB, E:EXPRESS
            var dlvStoreId = data.linkOptions.substr(2);

            switch (storeType) {
                case 'H' :
                    $(obj).find('.storeType').val('HYPER');
                    break;
                case 'E' :
                    $(obj).find('.storeType').val('EXP');
                    break;
                default:
                    $(obj).find('.storeType').val('HYPER');
                    break;
            }

            // N : 일반상품, D : 택배점
            if(data.linkOptions.substr(1,1) == 'D') {
                $(obj).find('.dlvStoreYn').prop("checked", true).val('Y');
                $(obj).find('.dlvStoreList').prop("disabled", false).val(dlvStoreId);
            }
        }
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function(linkType, obj) {
        var linkInfo ='';
        var none = "";

        switch (linkType) {
            case "EXH"  :
            case "EVENT" :
            case "ITEM_DS" :
            case "ITEM_TD":
                linkInfo  = $("#linkCommon").html();
                break;
            case "URL":
                linkInfo  = $("#linkUrl").html();
                break;
            case "GNB":
                linkInfo  = $("#linkGnbDevice").html() + $("#linkCommon").html();
                break;
            default :
                linkInfo =  none;
        }

        if($('#siteType').val() == 'HOME' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeHome").html() + linkInfo +  $("#dlvStoreTypeHome").html()
        }
        if($('#siteType').val() == 'CLUB' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeClub").html() + linkInfo +  $("#dlvStoreTypeClub").html()
        }
        if($('#loc1Depth').val() == 'TOP') {
            linkInfo += '<button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="bannerCommon.resetLinkInfo(this);">초기화</button>';
        }
        obj.html(linkInfo);
    },
    /**
     * 파라미터 유효성 검사
     */
    setBannerValid : function() {
        // 기본 폼 검사
        if($.jUtil.isEmpty($('#loc1Depth').val())) {
            alert("전시 위치를 선택해 주세요");
            return false;
        } else {
            switch ($('#loc1Depth').val()) {
                case 'LBB' :
                    if($.jUtil.isEmpty($('#loc2Depth').val())) {
                        alert("대분류를 선택해 주세요.");
                        return false;
                    }
                    break;
                case 'MBB' :
                    if($.jUtil.isEmpty($('#loc2Depth').val())){
                        alert("대분류를 선택해 주세요.");
                        return false;
                    }
                    if($.jUtil.isEmpty($('#loc3Depth').val())){
                        alert("중분류를 선택해 주세요.");
                        return false;
                    }
                    break;
            }
        }
        // 전시위치
        if($('#loc1Depth').val() === 'LBB' || $('#loc1Depth').val() === 'MBB') {
            if ($.jUtil.isEmpty($('#priority').val())) {
                alert("우선 순위를 입력해 주세요");
                return false;
            }
            if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
                alert("숫자만 입력 가능 합니다.");
                $("#priority").val('');
                $("#priority").focus();
                return;
            }
        }
        // 배송시간 배너일 경우 전시구간 확인
        if($('#loc1Depth').val() === 'DTB') {
            var dispTimeYn = $("#dispTimeYn").val();
            var dispStartTime = $("#dispStartTime").val();
            var dispEndTime = $("#dispEndTime").val();

            if (dispTimeYn == 'Y') {
                if (!moment(dispStartTime, 'HH:mm', true).isValid()) {
                    alert("전시시간대(시작) 형식이 맞지 않습니다. 시:분 형태로 입력 가능합니다.");
                    $("#dispStartTime").val('').focus();
                    return false;
                } else if (!moment(dispEndTime, 'HH:mm', true).isValid()) {
                    alert("전시시간대(종료) 형식이 맞지 않습니다. 시:분 형태로 입력 가능합니다.");
                    $("#dispEndTime").val('').focus();
                    return false;
                }
                // HH의 00 -> 24로 출력 됨.
                if((moment(dispStartTime, 'HH:mm').format('k')) > 23 && dispStartTime.split(':')[0] != '00') {
                    alert("시작 시간의 범위는 0~23 사이로 입력해 주세요.");
                    $("#dispStartTime").val('').focus();
                    return false;
                }
                if((moment(dispEndTime, 'HH:mm').format('k')) > 23 && dispEndTime.split(':')[0] != '00') {
                    alert("종료 시간의 범위는 0~23 사이로 입력해 주세요.");
                    $("#dispEndTime").val('').focus();
                    return false;
                }
                if (dispStartTime > dispEndTime) {
                    alert("전시구간 시작시간은 종료시간보다 빠를 수 없습니다.");
                    $("#dispStartTime").val('').focus();
                    return false;
                }
            }
        }

        if($.jUtil.isEmpty($('#bannerNm').val())) {
            alert("배너명을 입력해 주세요.");
            return false;
        }
        if($('#bannerNm').val().length > 20) {
            alert("배너명은 20자를 넘을 수 없습니다.");
            $('#bannerNm').val('').focus();
            $('#bannerNmCnt').html('0');
            return false;
        }

        // 베너 폼 검사
        var bannerDetail = $("#bannerSetForm").serializeObject(true);
        for(var i=0; i < parseInt($("#template").val()); i++) {
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].title)) {
                alert("타이틀을 입력해 주세요." );
                return false;
            }
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].subTitle)) {
                alert("서브 타이틀을 입력해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].imgUrl)) {
                alert("이미지를 등록해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].linkInfo) && bannerDetail.linkList[i].linkType != 'NONE') {
                alert("링크정보를 등록해 주세요." );
                return false;
            }
            if(bannerDetail.linkList[i].dlvStoreYn == 'Y' && $.jUtil.isEmpty(bannerDetail.linkList[i].dlvStoreId) && bannerDetail.linkList[i]) {
                alert("택배점을 선택해 주세요");
                return false;
            }
        }

        return true;
    },
    /**
     * 입력 폼 초기화 ( 등록 또는 검색 후 )
     */
    resetBannerForm : function() {
        $('#loc1Depth, #bannerNo, #bannerNm, #priority').val('');
        banner.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        commonCategory.setCategorySelectBox(1, '', '', $('#loc2Depth'));
        commonCategory.setCategorySelectBox(2, '', '', $('#loc3Depth'));

        $("#template").val('1').trigger('change');
        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
        $('#siteType').val('HOME');
        $('#dispYn').val('Y');
        $('#device').val('PC');
        $('#priority, #loc2Depth, #loc3Depth, #device').attr("disabled", false);
        $('#dispTimeYn').val('N').change();
        $('#dispStartTime, #dispEndTime').val('');
        $('#bannerTr, #displayTimeTr').hide();
        $('#bannerNmCnt').html('0');
    },
    /**
     * 배너 입력 폼 초기화 ( 배너 정보 변경 시 )
     */
    resetForm : function() {
        var loc1DepthValue = $('#loc1Depth').val();
        if(loc1DepthValue === 'LBB' || loc1DepthValue === 'MBB') {
            commonCategory.setCategorySelectBox(1, '', '', $('#loc2Depth'));
            commonCategory.setCategorySelectBox(2, '', '', $('#loc3Depth'));
        }

        $("#template").val('1').trigger('change');
        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
    },
    /**
     * 배너 상세 정보 입력
     */
    setBannerDetail : function(data, template) {
        $("#template").val(template).trigger('change');

        if (!$.jUtil.isEmpty(data)) {
            for(var i =0 ; i < parseInt(template) ; i++) {
                var link  = data[i];
                var templateNum = i+1;

                if(link.textYn == 'Y') {
                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', true);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,false).val(link.title);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,false).val(link.subTitle);
                } else {
                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', false);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,true).val('');
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,true).val('');
                }

                bannerCommon.img.setImg($('.bigBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'   : hmpImgUrl + "/provide/view?processKey=" + bannerCommon.img.getProcessKey() + "&fileId=" + link.imgUrl,
                });

                $("#bigBannerLinktype" + templateNum ).val(link.linkType).trigger('change');
                bannerCommon.setLinkInfo(link ,  $("#bigBannerLinkInfo" + templateNum));
            }
        }
    },
    /**
     * 배너 단 설정
     */
    changeTemplate : function(){
        bannerCommon.resetLinkTypeAll();
        var template = $("#template").val();

        //배너 입력 영역 설정
        switch (template) {
            case "3" :
                $('.bigBannerDiv3').css("display", "");
            case "2" :
                $('.bigBannerDiv2').css("display", "");
            case "1" :
                $('.bigBannerDiv1').css("display", "");
                break;
        };

        banner.changeImgSizeDescription($('#loc1Depth').val());
    },
    /**
     * 링크 타입 전체 초기화
     */
    resetLinkTypeAll : function() {
        $("[class^='bigBannerDiv']").css("display", "none");
        $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('');
        $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='bigBannerDiv']").find('.deleteImgBtn').trigger('click');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').prop('readonly' ,true).val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').prop('readonly' ,true).val('');

        $('.bigBannerDiv1').css("display", "");
    },
    /**
     * 링크 타입 초기화 (단 번호별 초기화)
     */
    resetLinkInfo : function(linkTemplateNumber) {
        $('.bigBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerTitle').val('').prop('readOnly' , true);
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSubTitle').val('').prop('readOnly' , true);
        $('.bigBannerDiv'+linkTemplateNumber).find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.bigBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerTitle').prop('readonly' ,true).val('');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSubTitle').prop('readonly' ,true).val('');
    },
    /**
     * 사이트 구분에 따른 링크 타입 변경
     */
    changeBannerLinkType : function() {
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){
            $('select[id^="bigBannerLinktype"]').html($("#linkTypeHome").html());
        } else{
            $('select[id^="bigBannerLinktype"]').html($("#linkTypeClub").html());
        }

        $('select[id^="bigBannerLinktype"]').trigger('change');
    },
    /**
     * 타이틀 입력란 설정
     */
    setText : function(obj){
        if($(obj).prop("checked")) {
            $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , false);
            $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , false);
            $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", true).val('Y');
        } else {
            $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false).val('N');
        }
    }
};

bannerCommon.img = {
    imgDiv : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        bannerCommon.img.resetImg();
        bannerCommon.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ bannerCommon.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');
        let params = {
            processKey : bannerCommon.img.getProcessKey(),
            mode : 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        bannerCommon.img.setImg(bannerCommon.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth'  : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        bannerCommon.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        bannerCommon.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    getProcessKey : function() {
        var processKey = '';
        var loc1DepthValue = $('#loc1Depth').val();
        var device = $('#device').val();
        var template = $('#template').val();

        switch (loc1DepthValue) {
            case 'LBB' :
            case 'MBB' :
                if(device === 'PC') {
                    switch (template) {
                        case '1' :
                            processKey = 'Template1Banner';
                            break;
                        case '2' :
                            processKey = 'Template2Banner';
                            break;
                        case '3' :
                            processKey = 'Template3Banner';
                            break
                    }
                } else {
                    processKey = 'MobileBigBanner';
                }
                break;
            case 'CMB' :
                processKey = 'CategoryMainBanner';
                break;
            case 'CSB' :
                if(device === 'PC') {
                    processKey = 'CustomerCenterPcBanner';
                    break;
                } else {
                    processKey = 'CustomerCenterMobileBanner';
                    break;
                }
            case 'UGB' :
                if(device === 'PC') {
                    processKey = 'MemberGradePcBanner';
                    break;
                } else {
                    processKey = 'CustomerCenterMobileBanner';
                    break;
                }
            case 'DTB' :
                if(device === 'PC') {
                    processKey = 'DeliveryTimeBannerPc';
                    break;
                } else {
                    processKey = 'DeliveryTimeBannerMobile';
                    break;
                }
                break;
            case 'BKB' :
                if(device === 'PC') {
                    processKey = 'BasketPcBanner';
                    break;
                } else {
                    processKey = 'BasketMobileBanner';
                    break;
                }
                break;
            case 'ORB' :
                if(device === 'PC') {
                    processKey = 'OrderPcBanner';
                    break;
                } else {
                    processKey = 'OrderMobileBanner';
                    break;
                }
                break;
            case 'OCB' :
                if(device === 'PC') {
                    processKey = 'OrderCompletePcBanner';
                    break;
                } else {
                    processKey = 'OrderCompleteMobileBanner';
                    break;
                }
                break;
            case 'AEB' :
                processKey = 'AndroidExitBanner';
                break;
            case 'SBB' :
                processKey = 'PcFloatingBanner';
                break;
            default :
                processKey = 'Template1Banner';
                break;
        }

        return processKey;
    }
};