/**
 * 업체관리 > 샐로샵관리 > 셀러샵 전시관리
 */
$(document).ready(function() {

    recomSellerShopListGrid.init();
    recomSellerShop.init();
    recomSellerShopItemListGrid.init();
    recomSellerShopItem.init();
    $('#manual').hide();
    CommonAjaxBlockUI.global();
});

// 셀러샵 그리드
var recomSellerShopListGrid = {

    gridView : new RealGridJS.GridView("recomSellerShopListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        recomSellerShopListGrid.initGrid();
        recomSellerShopListGrid.initDataProvider();
        recomSellerShopListGrid.event();

    },
    initGrid : function() {

        recomSellerShopListGrid.gridView.setDataSource(recomSellerShopListGrid.dataProvider);
        recomSellerShopListGrid.gridView.setStyles(recomSellerShopListGridBaseInfo.realgrid.styles);
        recomSellerShopListGrid.gridView.setDisplayOptions(recomSellerShopListGridBaseInfo.realgrid.displayOptions);
        recomSellerShopListGrid.gridView.setColumns(recomSellerShopListGridBaseInfo.realgrid.columns);
        recomSellerShopListGrid.gridView.setOptions(recomSellerShopListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        recomSellerShopListGrid.dataProvider.setFields(recomSellerShopListGridBaseInfo.dataProvider.fields);
        recomSellerShopListGrid.dataProvider.setOptions(recomSellerShopListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        recomSellerShopListGrid.gridView.onDataCellClicked = function(gridView, index) {
            recomSellerShop.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        recomSellerShopListGrid.dataProvider.clearRows();
        recomSellerShopListGrid.dataProvider.setRows(dataList);

    }

};
// 셀러샵 아이템 리스트 그리드
var recomSellerShopItemListGrid = {

    gridView : new RealGridJS.GridView("recomSellerShopItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        recomSellerShopItemListGrid.initGrid();
        recomSellerShopItemListGrid.initDataProvider();
        recomSellerShopItemListGrid.event();

    },
    initGrid : function() {

        recomSellerShopItemListGrid.gridView.setDataSource(recomSellerShopItemListGrid.dataProvider);
        recomSellerShopItemListGrid.gridView.setStyles(recomSellerShopItemListGridBaseInfo.realgrid.styles);
        recomSellerShopItemListGrid.gridView.setDisplayOptions(recomSellerShopItemListGridBaseInfo.realgrid.displayOptions);
        recomSellerShopItemListGrid.gridView.setColumns(recomSellerShopItemListGridBaseInfo.realgrid.columns);
        recomSellerShopItemListGrid.gridView.setOptions(recomSellerShopItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        recomSellerShopItemListGrid.dataProvider.setFields(recomSellerShopItemListGridBaseInfo.dataProvider.fields);
        recomSellerShopItemListGrid.dataProvider.setOptions(recomSellerShopItemListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        recomSellerShopItemListGrid.dataProvider.clearRows();
        recomSellerShopItemListGrid.dataProvider.setRows(dataList);

    },
    addDataList : function (dataList) {
        recomSellerShopItemListGrid.dataProvider.addRows(dataList);
    },
    event : function () {
        recomSellerShopItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            recomSellerShopItem.gridRowSelect(index.dataRow);
        };
    }

};

// 셀러샵관리
var recomSellerShop = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        recomSellerShop.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        recomSellerShop.setDate.setEndDate(0);
        recomSellerShop.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        //검색 역영
        $('#searchBtn').bindClick(recomSellerShop.search);   // 검색
        $('#searchResetBtn').bindClick(recomSellerShop.searchFormReset); // 검색폼 초기화
        $('#setSellerShopItmBtn').bindClick(recomSellerShop.setSellerShopItm); // 셀러샵 정보 저장
        $('#resetBtn').bindClick(recomSellerShop.resetForm); // 셀러샵 등록 정보 초기화
        $("input:radio[name='recommendType']").bindChange(recomSellerShop.changeRecommendType);

    },
    /**
     * 상품추천 방식 변경 이벤트
     */
    changeRecommendType : function(){

        var recommendType = $("input:radio[name='recommendType']:checked").val();
        recomSellerShopItemListGrid.dataProvider.clearRows();
        $('#itemNo').html('');
        $('#itemUseYn').val('Y');
        $('#itemPriority').val('');

        if(recommendType == 'AU'){
            $('#manual').hide();

        } else{
            $('#manual').show();
        }

    },
    /**
     * 셀러샵 검색
     */
    search : function() {

        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            recomSellerShop.initSearchDate('-1d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        CommonAjax.basic({url:'/manage/getRecomSellerShopList.json?' + $('#recomSellerShopSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                recomSellerShop.resetForm();
                recomSellerShopListGrid.setData(res);
                $('#recomSellerShopTotalCount').html($.jUtil.comma(recomSellerShopListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        recomSellerShop.initSearchDate('-1d');
        $('#searchType').val('SHOPNM');  // 검색어 구분
        $('#searchKeyword').val('');    //검색어

    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        recomSellerShop.resetForm();

        var rowDataJson = recomSellerShopListGrid.dataProvider.getJsonRow(selectRowId);

        $('#partnerId').val( rowDataJson.partnerId)
        $('#shopNm').html( rowDataJson.shopNm);
        $('#shopInfo').html( rowDataJson.shopInfo);


        $("input:radio[name='recommendUseYn']:input[value='" + rowDataJson.recommendUseYn + "']").prop("checked", true);
        $("input:radio[name='recommendType']:input[value='" + rowDataJson.recommendType + "']").prop("checked", true).trigger('change');

        if(rowDataJson.recommendType == 'MA') {
            CommonAjax.basic({
                url: '/manage/getRecomSellerShopItemList.json?partnerId='
                    + rowDataJson.partnerId
                , data: null
                , method: "GET"
                , successMsg: null
                , callbackFunc: function (res) {
                    //셀러샵 아이템 리스트
                    recomSellerShopItemListGrid.setData(res);

                }
            });
        }

    },

    /**
     * 셀러샵 전시 관리 아이템 등록/수정
     */
    setSellerShopItm : function() {


        var setItem = new Object();

        var partnerId = $('#partnerId').val();
        var recommendUseYn = $("input:radio[name='recommendUseYn']:checked").val();
        var recommendType = $("input:radio[name='recommendType']:checked").val();



        if ($.jUtil.isEmpty(partnerId)) {
            alert("저장할 셀러샵을 선택해 주세요.");
            return false;
        }

        var itemListCnt = 0;

        var gridList = recomSellerShopItemListGrid.dataProvider.getJsonRows();

        //기존에 등록 되어 있는 상품은 제외 하고 등록
        if(gridList.length > 0) {
            gridList.forEach(function (gridItem) {
                if(gridItem.useYn == 'Y'){
                    itemListCnt++;
                }
            });
        }


        if(recommendType == 'MA' && itemListCnt == 0){
            alert("추천할 상품을 등록해 주세요.");
            return false;
        }
        if(itemListCnt > 4 && recommendType == 'MA'){
            alert("추천 상품 노출은 최대 4개까지 설정 가능합니다.");
            return false;
        }
        if(itemListCnt < 4 && recommendType == 'MA'){
            alert("추천 상품 등록이 필요합니다.");
            return false;
        }
        if(recommendType == 'MA'){

            setItem.itemList = recomSellerShopItemListGrid.dataProvider.getJsonRows();
        }

        setItem.partnerId = partnerId;
        setItem.recommendUseYn = recommendUseYn;
        setItem.recommendType = recommendType;


        CommonAjax.basic({url:'/manage/setRecomSellerShopItem.json',data: JSON.stringify(setItem), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                recomSellerShop.search();
            }});


    },
    getPartnerPop : function() {
        var url = '/common/popup/sellerShopPop?callback=recomSellerShop.schPartner&isMulti=N';
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 판매업체 조회 팝업창 Callback
     */
    schPartner : function(res) {

        recomSellerShop.resetForm();
        $('#partnerId').val( res[0].partnerId)
        $('#shopNm').html( res[0].shopNm);
        $('#shopInfo').html( res[0].shopInfo);


    },
    /**
     * 셀러샵 입력/수정 폼 초기화
     */
    resetForm : function() {
        recomSellerShopItemListGrid.dataProvider.clearRows();
        $('#sellerItemSetForm').resetForm();
        $('#shopNm').html('');
        $('#shopInfo').html('');
        $("input:radio[name='recommendType']").trigger('change');

    }

};
// 셀러샵아이템
var recomSellerShopItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#addItemPopUp').bindClick(recomSellerShopItem.addItemPopUp);
        $('#setItem').bindClick(recomSellerShopItem.setItem);

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        var rowDataJson = recomSellerShopItemListGrid.dataProvider.getJsonRow(selectRowId);
        $('#itemUseYn').val(rowDataJson.useYn);
        $('#itemNo').html(rowDataJson.itemNo);
        $('#itemPriority').val(rowDataJson.priority);


    },
    /**
     * 단일 상품 정보 변경
     */
    setItem :function(){

        var itemNo = $('#itemNo').html();
        var itemUseYn  = $('#itemUseYn').val();
        var itemPriority =$('#itemPriority').val();

        if ($.jUtil.isEmpty(itemNo)) {

            alert('저장할 상품을 선택해 주세요!');
            return false;
        }

        var curRow = recomSellerShopItemListGrid.gridView.getCurrent().itemIndex;
        var rowDataJson = recomSellerShopItemListGrid.dataProvider.getJsonRow(curRow);

        rowDataJson.useYn = itemUseYn;
        rowDataJson.priority = itemPriority;
        if(itemUseYn == 'Y'){
            rowDataJson.useYnTxt = '사용';
        } else{
            rowDataJson.useYnTxt = '사용안함';
        }
        recomSellerShopItemListGrid.gridView.setValues(curRow, rowDataJson, true);
;
        },
    /**
     * 상품조회(용도옵션포함)팝업
     */
    addItemPopUp : function() {
        var partnerId = $('#partnerId').val();
        if($.jUtil.isEmpty(partnerId)) {
            alert('등록 하실 판매업체를 먼저 선택해 주세요.');
            return;
        }
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=recomSellerShopItem.setItemList&isMulti=Y&mallType=DS&storeType=DS&partnerId=' + partnerId, 1084, 650);
    },

    /**
     * 상품 우선순위 재 정렬
     */
    resetItemPriority : function(){
        // Priority 정렬
        recomSellerShopItemListGrid.dataProvider.getJsonRows(0,-1).forEach(function (data, index) {

            var itemInfo = recomSellerShopItemListGrid.gridView.getValues(index);
            itemInfo.priority = parseInt(index) + 1
            recomSellerShopItemListGrid.gridView.setValues(index , itemInfo ,true);

        });
    },
    /**
     * 상품리스트 설정
     */
    setItemList : function(itemList) {

        var gridList = recomSellerShopItemListGrid.dataProvider.getJsonRows();


        itemList.forEach(function (item) {
            item.useYn ='Y';
            item.useYnTxt ='사용';
            gridList.forEach(function (gridItem, idx) {
                if(gridItem.itemNo == item.itemNo){
                    gridList.splice(idx, 1);
                }
            });
        });

        recomSellerShopItemListGrid.dataProvider.clearRows();
        recomSellerShopItemListGrid.setData(itemList);
        recomSellerShopItemListGrid.addDataList(gridList);
        recomSellerShopItem.resetItemPriority();


    }


};