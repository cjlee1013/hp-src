/**
 * 사이트관리 > FAQ 관리
 */
$(document).ready(function() {
    faqListGrid.init();
    faq.init();
    CommonAjaxBlockUI.global();
    commonEditor.initEditor('#faqBodyEdit', 300, 'ItemDetail');
    commonEditor.hmpImgUrl = hmpImgUrl;

});
var commonEditor;
// faq 그리드
var faqListGrid = {
    gridView : new RealGridJS.GridView("faqListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        faqListGrid.initGrid();
        faqListGrid.initDataProvider();
        faqListGrid.event();


    },
    initGrid : function() {
        faqListGrid.gridView.setDataSource(faqListGrid.dataProvider);

        faqListGrid.gridView.setStyles(faqListGridBaseInfo.realgrid.styles);
        faqListGrid.gridView.setDisplayOptions(faqListGridBaseInfo.realgrid.displayOptions);
        faqListGrid.gridView.setDisplayOptions({
            rowHoverMask: {
                visible: true,
                hoverMask: "row",
                styles: {
                    background:"#30c0c0c0"
                }
            }
        });

        faqListGrid.gridView.setColumns(faqListGridBaseInfo.realgrid.columns);
        var dispArea = $('#searchDispArea').val();
        if(dispArea =='PARTNER'){ // 파트너 FAQ일 경우 노출 사이트 컬럼 숨김 처리
            var col = faqListGrid.gridView.columnByName("dispTxt");
            if(col){
                var visible = !faqListGrid.gridView.getColumnProperty(col, "visible");
                faqListGrid.gridView.setColumnProperty(col, "visible", visible);
            }
        }
        faqListGrid.gridView.setOptions(faqListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        faqListGrid.dataProvider.setFields(faqListGridBaseInfo.dataProvider.fields);
        faqListGrid.dataProvider.setOptions(faqListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        faqListGrid.gridView.onDataCellClicked = function(gridView, index) {
            faq.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        faqListGrid.dataProvider.clearRows();
        faqListGrid.dataProvider.setRows(dataList);
    },

};

// faq관리
var faq = {
    /**
     * 초기화
     */
    init : function() {
        this.initAjaxForm();
        this.bindingEvent();
    },
    /**
     * FAQ 등록/수정 Ajax 폼
     */
    initAjaxForm : function() {
        $('#faqSetForm').ajaxForm({
            url: '/manage/faq/setFaq.json',
            type: 'post',
            success: function(resData) {
                alert(resData.returnMsg);
                faq.searchFormReset();
                faq.search();
            },
            error: function(e) {
                if(e.responseJSON.errors[0].detail != null) {
                    alert(e.responseJSON.errors[0].detail);
                } else {
                    alert("오류가 발생하였습니다! 잠시 후에 다시 시도해주세요.");
                }
            }
        });
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {


        $('#searchBtn').bindClick(faq.search);
        $('#searchResetBtn').bindClick(faq.searchFormReset);

        $('#setFaqBtn').bindClick(faq.setFaq);
        $('#resetBtn').bindClick(faq.resetForm);
        $('#faqSubject').calcTextLength('keyup', '#textCountFaqSubject');
        $('#faqSetForm').on('propertychange change keyup paste input', '#fr-image-by-url-layer-text-1', function(e){

            commonEditor.inertImgurl = $('#fr-image-by-url-layer-text-1').val();
        });
        $("#faqSearchForm :checkbox").change(function(){
            if($(this).prop("checked"))  {
                 $(this).val('Y');
            }else {
                $(this).val('N');
            }
        });
        $("#faqSetForm :checkbox").change(function(){
            if($(this).prop("checked"))  {
                $(this).val('Y');
            }else {
                $(this).val('N');
            }
        });

    },

    /**
     * FAQ 검색
     */
    search : function() {

        var searchKeyword = $('#searchKeyword').val();
        var searchKeywordLength = $('#searchKeyword').val().length;

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if(searchKeywordLength < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#searchKeyword').focus();
                return false;
            }

        }

        CommonAjax.basic({url:'/manage/faq/getFaqList.json?' + $('#faqSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            faq.resetForm();
            faqListGrid.setData(res);
            $('#faqTotalCount').html($.jUtil.comma(faqListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchClassCd').val('');
        $('#searchUseYn').val('');
        $('#searchType').val('QUESTION');
        $('#searchKeyword').val('');
        $("#faqSearchForm :checkbox").prop( "checked", true );
        $("#faqSearchForm :checkbox").val( 'Y');
    },

    /**
     * FAQ 등록/수정
     */
    setFaq : function() {

        //유효성 체크가 필요한 오브젝트 배열 생성
        var objectArray = [$('#classCd') ,$('#priority') ,$('#faqSubject')];
        $('#faqBody').val( commonEditor.getHtmlData('#faqBodyEdit'));
        objectArray.push($('#faqBody'))

        if($.jUtil.isEmptyObjectArray(objectArray)){
            alert("필수 입력 항목을 선택/입력 해주세요." );
            return false;
        }

        if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        if ( $('#priority').val()> 999) {
            alert("최대 999까지 입력 가능합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }

        if( $('#searchDispArea').val() =='FRONT'){
            if( $( '#dispHomeYn' ).val()=='N'   && $( '#dispClubYn' ).val()=='N' ){
                alert("노출사이트는 반드시 1개는 선택해야 합니다.");
                return false;
            }

        }

        $('#faqSetForm').submit();
    },
    /**
     * FAQ 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#faqNo, #classCd, #useYn, #priority, #faqSubject').val('');
        $('#useYn').val('Y');
        $('#textCountFaqSubject').html('0');
        $("#faqSetForm :checkbox").prop( "checked", true );
        $("#faqSetForm :checkbox").val('Y');
        commonEditor.setHtml('#faqBodyEdit' ,'');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = faqListGrid.dataProvider.getJsonRow(selectRowId);

        $('#faqNo').val(rowDataJson.faqNo);
        $('#classCd').val(rowDataJson.classCd);
        $('#useYn').val(rowDataJson.useYn);
        $('#priority').val(rowDataJson.priority);
        $('#faqSubject').val(rowDataJson.faqSubject);
        $('#textCountFaqSubject').html(rowDataJson.faqSubject.length);

        commonEditor.setHtml('#faqBodyEdit' ,rowDataJson.faqBody);

        if( $('#searchDispArea').val() =='FRONT'){

            if(rowDataJson.dispHomeYn == 'Y'){
                $( '#dispHomeYn' ).prop( "checked", true );
                $( '#dispHomeYn' ).val('Y');
            }else{
                $( '#dispHomeYn' ).prop( "checked", false );
                $( '#dispHomeYn' ).val('N');
            }
            if(rowDataJson.dispClubYn =='Y'){
                $( '#dispClubYn' ).prop( "checked", true );
                $( '#dispClubYn' ).val('Y');
            }else{
                $( '#dispClubYn' ).prop( "checked", false );
                $( '#dispClubYn' ).val('N');
            }

        }



    }
};
