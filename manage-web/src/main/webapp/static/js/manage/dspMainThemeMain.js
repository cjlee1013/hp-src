/**
 * 사이트관리 > 전문관관리 > 마트전단 전시 관리
 */
$(document).ready(function() {
    dspMainThemeListGrid.init();
    dspMainTheme.init();
    CommonAjaxBlockUI.global();
});

// dspMainTheme 그리드
var dspMainThemeListGrid = {
    gridView : new RealGridJS.GridView("dspMainThemeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspMainThemeListGrid.initGrid();
        dspMainThemeListGrid.initDataProvider();
        dspMainThemeListGrid.event();
    },
    initGrid : function() {
        dspMainThemeListGrid.gridView.setDataSource(dspMainThemeListGrid.dataProvider);
        dspMainThemeListGrid.gridView.setStyles(dspMainThemeGridBaseInfo.realgrid.styles);
        dspMainThemeListGrid.gridView.setDisplayOptions(dspMainThemeGridBaseInfo.realgrid.displayOptions);
        dspMainThemeListGrid.gridView.setColumns(dspMainThemeGridBaseInfo.realgrid.columns);
        dspMainThemeListGrid.gridView.setOptions(dspMainThemeGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspMainThemeListGrid.dataProvider.setFields(dspMainThemeGridBaseInfo.dataProvider.fields);
        dspMainThemeListGrid.dataProvider.setOptions(dspMainThemeGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspMainThemeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspMainTheme.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspMainThemeListGrid.dataProvider.clearRows();
        dspMainThemeListGrid.dataProvider.setRows(dataList);
    }
};

// dspMainTheme 관리
var dspMainTheme = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
        this.initSetDate('-30d');
    },
    initSearchDate : function (flag) {
        dspMainTheme.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspMainTheme.setDate.setEndDate(0);
        dspMainTheme.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSetDate : function (flag) {
        dspMainTheme.setDate = Calendar.datePickerRange('dispStartDt', 'dispEndDt');
        dspMainTheme.setDate.setEndDate(0);
        dspMainTheme.setDate.setStartDate(flag);
        $("#dispEndDt").datepicker("option", "minDate", $("#dispStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setBtn').bindClick(dspMainTheme.set); // 등록,수정
        $('#resetBtn').bindClick(dspMainTheme.resetForm); // 입력폼 초기화
        $('#searchBtn').bindClick(dspMainTheme.search); // 검색
        $('#searchResetBtn').bindClick(dspMainTheme.searchFormReset); // 검색
    },
    /**
     * dspMainTheme 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspMain/getDspMainThemeList.json'
            , data : $('#dspMainThemeSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspMainTheme.resetForm();
                dspMainThemeListGrid.setData(res);
                $('#dspMainThemeTotalCount').html($.jUtil.comma(dspMainThemeListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('REGDT');
        dspMainTheme.initSearchDate('-30d');
        $('#searchSiteType').val('');
        $('#searchUseYn').val('');
        $('#searchLoc1Depth').val('');
        $('#searchType').val('themeNm');
        $('#searchKeyword').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspMainThemeListGrid.dataProvider.getJsonRow(selectRowId);

        $('#themeId').val(rowDataJson.themeId);
        $('#siteType').val(rowDataJson.siteType).attr('disabled', true);
        $('#themeNm').val(rowDataJson.themeNm);
        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);
        $('#priority').val(rowDataJson.priority);
        $('#useYn').val(rowDataJson.useYn);
    },
    /**
     * dspMainTheme 등록/수정
     */
    set : function() {
        if(!dspMainTheme.valid.set()) {
            return false;
        }

        CommonAjax.basic({
            url:'/manage/dspMain/setDspMainTheme.json',
            data : JSON.stringify($('#dspMainThemeSetForm').serializeObject())
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainTheme.resetForm();
                dspMainTheme.searchFormReset();
                dspMainTheme.search();
            }
        });
    },
    /**
     * dspMainTheme 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#themeId, #priority, #themeNm').val('');
        $('#siteType').val('HOME').attr('disabled', false);
        $('#useYn').val('Y');
        dspMainTheme.initSetDate('-30d');
    }
};

/**
 * dspMainTheme 검색,입력,수정 validation check
 */
dspMainTheme.valid = {
    set : function () {
        if($.jUtil.isEmpty($('#siteType').val())) {
            $('#siteType').focus();
            alert('사이트를 선택해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#themeNm').val())) {
            $('#themeNm').focus();
            alert('테마명을 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#priority').val())) {
            $('#priority').focus();
            alert('우선순위를 입력해 주세요.');
            return false;
        }
        if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        if ( $('#priority').val()> 999) {
            alert("최대 999까지 입력 가능합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        return true;
    }
};