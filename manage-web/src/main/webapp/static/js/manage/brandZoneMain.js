/**
 * 사이트관리 > 브랜드좀
 */
$(document).ready(function() {

    brandZoneListGrid.init();
    brandZoneItemListGrid.init();
    brandZone.init();
    CommonAjaxBlockUI.global();

});

// 브랜드존 리스트 그리드
var brandZoneListGrid ={

    gridView : new RealGridJS.GridView("brandZoneListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        brandZoneListGrid.initGrid();
        brandZoneListGrid.initDataProvider();
        brandZoneListGrid.event();
    },
    initGrid : function() {

        brandZoneListGrid.gridView.setDataSource(brandZoneListGrid.dataProvider);

        brandZoneListGrid.gridView.setStyles(brandZoneListGridBaseInfo.realgrid.styles);
        brandZoneListGrid.gridView.setDisplayOptions(brandZoneListGridBaseInfo.realgrid.displayOptions);
        brandZoneListGrid.gridView.setColumns(brandZoneListGridBaseInfo.realgrid.columns);
        brandZoneListGrid.gridView.setOptions(brandZoneListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        brandZoneListGrid.dataProvider.setFields(brandZoneListGridBaseInfo.dataProvider.fields);
        brandZoneListGrid.dataProvider.setOptions(brandZoneListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        brandZoneListGrid.dataProvider.clearRows();
        brandZoneListGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        brandZoneListGrid.gridView.onDataCellClicked = function(gridView, index) {
            brandZone.gridRowSelect(index.dataRow);
        };
    },
};

// 브랜드존 > 상품 리스트 그리드
var brandZoneItemListGrid ={

    gridView : new RealGridJS.GridView("brandZoneItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        brandZoneItemListGrid.initGrid();
        brandZoneItemListGrid.initDataProvider();
        brandZoneItemListGrid.event();
    },
    initGrid : function() {

        brandZoneItemListGrid.gridView.setDataSource(brandZoneItemListGrid.dataProvider);

        brandZoneItemListGrid.gridView.setStyles(brandZoneItemListGridBaseInfo.realgrid.styles);
        brandZoneItemListGrid.gridView.setDisplayOptions(brandZoneItemListGridBaseInfo.realgrid.displayOptions);
        brandZoneItemListGrid.gridView.setColumns(brandZoneItemListGridBaseInfo.realgrid.columns);
        brandZoneItemListGrid.gridView.setOptions(brandZoneItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        brandZoneItemListGrid.dataProvider.setFields(brandZoneItemListGridBaseInfo.dataProvider.fields);
        brandZoneItemListGrid.dataProvider.setOptions(brandZoneItemListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        brandZoneItemListGrid.dataProvider.clearRows();
        brandZoneItemListGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        brandZoneItemListGrid.gridView.onDataCellClicked = function(gridView, index) {
            brandZone.gridRowSelectItem(index.dataRow);
        };
    },
};


//스티커관리
var brandZone = {
    init : function() {
        brandZone.bindingEvent();
        brandZone.initSearchDate('-30d');
        brandZone.img.init();
    },

    initSearchDate : function (flag) {
        brandZone.setDate = Calendar.datePickerRange('schStartDate', 'schEndDate');

        brandZone.setDate.setEndDate(0);
        brandZone.setDate.setStartDate(flag);

        $("#schEndDate").datepicker("option", "minDate", $("#schStartDate").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#schBtn').bindClick(brandZone.search);
        $('#schResetBtn').bindClick(brandZone.searchFormReset);

        $('#setBrandZoneBtn').bindClick(brandZone.setBrandZone);
        $('#resetBtn').bindClick(brandZone.resetForm);

        $('#addBrandZoneItem').bindClick(brandZone.addBrandZoneItem);
        $('#updateBrandZoneItem').bindClick(brandZone.updateBrandZoneItem);
        $('#resetBrandZoneItem').bindClick(brandZone.resetBrandZoneItem);

        $('input[name="fileArr"]').change(function() {
            brandZone.img.addImg();
        });

        $('.uploadType').bindClick(brandZone.img.imagePreview);
    },
    /**
     * 스티커 리스트 검색
     * @returns {boolean}
     */
    search : function() {

        if(!brandZone.valid.search()){
            return false;
        }

        CommonAjax.basic({
            url: '/manage/brandZone/getBrandZoneList.json?' + $('#brandZoneSearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                brandZoneListGrid.setData(res);
                $('#brandZoneTotalCount').html($.jUtil.comma(brandZoneListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        brandZone.initSearchDate("-30d");
        $('#schType').val("brandNm");
        $('#schKeyword').val('');
    },
    /**
     * 스티커 등록/수정
     */
    setBrandZone : function() {

        if(!brandZone.valid.set()){
            return false;
        }

        var form = $('#brandZoneSetForm').serializeObject(true);
        form.brandZoneItemList = brandZoneItemListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({
            url:'/manage/brandZone/setBrandZone.json' ,
            data: JSON.stringify(form) ,
            method:"POST" ,
            successMsg:null ,
            contentType:"application/json",
            callbackFunc:function(res) {
                alert(res.returnMsg);
                brandZone.search();
            }});
    },
    /**
     * 브랜드 팝업
     * @param callback
     * @returns {boolean}
     */
    addBrandPop : function() {
        $.jUtil.openNewPopup('/common/popup/brandPop?callback=brandZone.callBack.schBrand', 1100, 620);
    },
    /**
     * 상품조회 팝업
     */
    addItemPopUp : function() {
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=brandZone.callBack.schItem&isMulti=N&&limitSize=0', 1100, 620);
    },
    /**
     * 스티커 등록/수정 폼 초기화
     */
    resetForm : function() {

        $('#brandzoneNo').val('');
        $('#siteType').val('HOME');
        $('#brandNo').val('');
        $('#brandNm').text('');
        $('#priority').val('');
        $('#useYn').val('Y');

        brandZone.img.resetImg();
        brandZone.resetBrandZoneItem();
        brandZoneItemListGrid.dataProvider.clearRows();

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        brandZone.resetForm();

        var rowDataJson = brandZoneListGrid.dataProvider.getJsonRow(selectRowId);

        $('#brandzoneNo').val(rowDataJson.brandzoneNo);
        $('#siteType').val(rowDataJson.siteType);
        $('#brandNo').val(rowDataJson.brandNo);
        $('#brandNm').text(rowDataJson.brandNm);
        $('#priority').val(rowDataJson.priority);
        $('#useYn').val(rowDataJson.useYn);

        brandZone.img.setImg($('#PC'), {
            'imgUrl': rowDataJson.pcImgUrl,
            'imgWidth' : rowDataJson.pcImgWidth,
            'imgHeight' : rowDataJson.pcImgHeight,
            'src'   : hmpImgUrl + "/provide/view?processKey=" + $('#PC').data('processkey') + "&fileId=" + rowDataJson.pcImgUrl,
        });

        brandZone.img.setImg($('#MOBILE'), {
            'imgUrl': rowDataJson.mobileImgUrl,
            'imgWidth' : rowDataJson.mobileImgWidth,
            'imgHeight' : rowDataJson.mobileImgHeight,
            'src'   : hmpImgUrl + "/provide/view?processKey=" + $('#MOBILE').data('processkey') + "&fileId=" + rowDataJson.mobileImgUrl,
        });

        CommonAjax.basic({
            url: '/manage/brandZone/getBrandZoneItemList.json?brandzoneNo='
                + rowDataJson.brandzoneNo,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                brandZoneItemListGrid.setData(res);
            }
        });
    },
    /**
     * 아이템 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelectItem : function(selectRowId) {

        var rowDataJson = brandZoneItemListGrid.dataProvider.getJsonRow(selectRowId);

        $('#itemNo').text(rowDataJson.itemNo);
        $('#itemNm').text(rowDataJson.itemNm);
        $('#optNo').text(rowDataJson.optNo);
        $('#opt1Val').text(rowDataJson.opt1Val);
        $('#itemPriority').val(rowDataJson.priority);
        $('#itemUseYn').val(rowDataJson.useYn);
    },
     /**
     * 조회 상품 저장
     */
    addBrandZoneItem : function () {

        if(!brandZone.valid.add()){
            return false;
        }

        var itemList = brandZoneItemListGrid.dataProvider.getJsonRows();
        var isCheckItem = true;

        itemList.forEach(function (griditem) {
        if (griditem.itemNo == $('#itemNo').text()) {
         isCheckItem = false;
         return false;
        }
        });

        if(isCheckItem == false) {
            alert("동일 상품이 등록되어 있습니다.");
            return false;
        }

        var value = {
            itemNo          : $('#itemNo').text(),
            itemNm          : $('#itemNm').text(),
            optNo           : $('#optNo').text(),
            opt1Val         : $('#opt1Val').text(),
            mallTypeNm      : $('#mallTypeNm').val(),
            priority        : $('#itemPriority').val(),
            useYn           : $('#itemUseYn').val(),
            useYnNm         : ($('#itemUseYn').val() == "Y" ? "사용" : "사용안함"),
        };

        brandZoneItemListGrid.dataProvider.addRow(value);
        brandZone.resetBrandZoneItem();
    },
    /**
     * 상품 수정 그리드 반영
     * @returns {boolean}
     */
    updateBrandZoneItem : function () {

        if(!brandZone.valid.add()){
            return false;
        }

        var row = brandZoneItemListGrid.gridView.getCurrent().itemIndex;
        var rowDataJson = brandZoneItemListGrid.dataProvider.getJsonRow(row);
        if( row < 0){
            alert ("수정 하려는 데이터가 없습니다.");
            return false;
        }

        var value = {
            itemNo          : rowDataJson.itemNo,
            itemNm          : rowDataJson.itemNm,
            optNo           : rowDataJson.optNo,
            opt1Val         : rowDataJson.opt1Val,
            priority        : $('#itemPriority').val(),
            useYn           : $('#itemUseYn').val(),
            useYnNm         : ($('#itemUseYn').val() == "Y" ? "사용" : "사용안함"),
        };

        brandZoneItemListGrid.gridView.setValues(row, value, true);
    },
    /**
     * 상품등록영역 초기화
     */
    resetBrandZoneItem : function () {
        $('#itemNo').text('');
        $('#itemNm').text('');

        $('#optNo').text('');
        $('#opt1Val').text('')

        $('#itemPriority').val('')
        $('#itemUse').val('Y');
    },
};

/**
 * brandzone validation
 * @type {{search: brandZone.valid.search, set: brandZone.valid.set}}
 */
brandZone.valid = {

    search : function () {

        // 날짜 체크
        var  startDt = $("#schStartDate");
        var  endDt = $("#schEndDate");

        if (!$.jUtil.isEmpty(startDt.val()) || (!$.jUtil.isEmpty(endDt.val()))) {
            if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(
                endDt.val(), 'YYYY-MM-DD', true).isValid()) {
                alert("조회기간 날짜 형식이 맞지 않습니다");
                $("#schstartDate").focus();
                return false;
            }
            if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
                alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
                startDt.val(
                    moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
                return false;
            }

            if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
                alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
                startDt.val(endDt.val());
                return false;
            }
        }

        var  schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
        }
        return true;

    },
    set : function(){

        if ($.jUtil.isEmpty($('#brandNo').val()))  {
            alert("브랜드번호를 입력해주세요.")
            $('#brandNo').focus();
            return false;
        }
        if ($.jUtil.isEmpty($('#priority').val()))  {
            alert("우선순위를 입력해주세요.")
            $('#priority').focus();
            return false;

        } else{
            if(!$.jUtil.isAllowInput( $('#priority').val(), ['NUM']) || $('#priority').val() == 0) {
                alert("우선순위는 숫자만 입력 가능합니다. (1~9999)");
                $("#priority").focus();
                return false;
            }
        }

        var  form = $('#brandZoneSetForm').serializeObject(true);

        if(form.pcImgUrl == '' || form.pcImgUrl == 'undefined') {
            alert("PC 로고 이미지를 등록해 주세요.");
            return false;
        }

        if(form.mobileImgUrl == '' || form.mobileImgUrl == 'undefined') {
            alert("모바일 로고 이미지를 등록해 주세요.");
            return false;
        }

        if(brandZoneItemListGrid.gridView.getItemCount() < 5){
            alert("브랜드관 노출상품은 최소 5개 이상 등록되어야 합니다.")
            return false;
        }

        return true;
    },
    add : function () {

        if($.jUtil.isEmpty($('#itemNo').text()) || $.jUtil.isEmpty($('#itemNm').text()) ) {
            return $.jUtil.alert("상품조회 후 추가 가능합니다.");
        }

        if(!$.jUtil.isAllowInput( $('#itemPriority').val(), ['NUM']) || $('#itemPriority').val() == 0) {
            return $.jUtil.alert("우선순위는 숫자만 입력 가능합니다. (1~9999) " , 'itemPriority');
        }

        return true;
    },
};

/**
 * brandzone img
 * @type {{init: brandZone.img.init, imgDiv: null, setImg: brandZone.img.setImg, deleteImg: brandZone.img.deleteImg, addImg: brandZone.img.addImg, resetImg: brandZone.img.resetImg, clickFile: brandZone.img.clickFile}}
 */
brandZone.img = {
    imgDiv : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        brandZone.img.resetImg();
        brandZone.img.imgDiv = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ brandZone.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgWidth').val(params.imgWidth);
            obj.find('.imgHeight').val(params.imgHeight);
            obj.find('.imgUrlTag').prop('src', params.src);

            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgWidth').val('');
            obj.find('.imgHeight').val('');
            obj.find('.imgUrlTag').prop('src', '');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#brandZoneImgFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#brandZoneImgFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var  file = $('#brandZoneImgFile');
        var  params = {
            processKey : 'BrandZonePCImg',
            mode : 'IMG'
        };

        if( brandZone.img.imgDiv.data('type') == "PC" ) {
            params.processKey = 'BrandZonePCImg';
        } else {
            params.processKey = 'BrandZoneMobileImg';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var  errorMsg = "";
                for (var  i in resData.fileArr) {
                    var  f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        brandZone.img.setImg(brandZone.img.imgDiv, {
                            'imgUrl': f.fileStoreInfo.fileId,
                            'imgWidth' : f.fileStoreInfo.width,
                            'imgHeight' : f.fileStoreInfo.height,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(device) {
        if (confirm('삭제하시겠습니까?')) {
            brandZone.img.setImg($('#' + device), null);
        }
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(device) {
        brandZone.img.imgDiv = $('#'+device);
        $('input[name=fileArr]').click();
    }
};

/**
 * brandzone callback
 * @type {{schBrand: brandZone.callBack.schBrand, schFcStore: brandZone.callBack.schFcStore}}
 */
brandZone.callBack = {

    schBrand: function (res) {
        $('#brandNo').val(res[0].brandNo);
        $('#brandNm').text(res[0].brandNm);

    },
    schItem : function (res) {
        $('#itemNo').text(res[0].itemNo);
        $('#itemNm').text(res[0].itemNm);
    }
};
