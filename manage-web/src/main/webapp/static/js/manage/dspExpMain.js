/**
 * 사이트관리 > 전시관리 > EXPRESS 전시관리
 */
$(document).ready(function() {
    expMainListGrid.init();
    expMainItemListGrid.init();
    expMain.init();
    expMainItem.init();
    CommonAjaxBlockUI.global();
});

var expMainListGrid = {
    gridView : new RealGridJS.GridView("expMainListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        expMainListGrid.initGrid();
        expMainListGrid.initDataProvider();
        expMainListGrid.event();
    },
    initGrid : function() {
        expMainListGrid.gridView.setDataSource(expMainListGrid.dataProvider);
        expMainListGrid.gridView.setStyles(expMainListGridBaseInfo.realgrid.styles);
        expMainListGrid.gridView.setDisplayOptions(expMainListGridBaseInfo.realgrid.displayOptions);
        expMainListGrid.gridView.setColumns(expMainListGridBaseInfo.realgrid.columns);
        expMainListGrid.gridView.setOptions(expMainListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        expMainListGrid.dataProvider.setFields(expMainListGridBaseInfo.dataProvider.fields);
        expMainListGrid.dataProvider.setOptions(expMainListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        expMainListGrid.gridView.onDataCellClicked = function(gridView, index) {
            expMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        expMainListGrid.dataProvider.clearRows();
        expMainListGrid.dataProvider.setRows(dataList);
    },
};

var expMainItemListGrid = {
    gridView : new RealGridJS.GridView("expMainItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        expMainItemListGrid.initGrid();
        expMainItemListGrid.initDataProvider();
        expMainItemListGrid.event();
    },
    initGrid : function() {
        expMainItemListGrid.gridView.setDataSource(expMainItemListGrid.dataProvider);
        expMainItemListGrid.gridView.setStyles(expMainItemListGridBaseInfo.realgrid.styles);
        expMainItemListGrid.gridView.setDisplayOptions(expMainItemListGridBaseInfo.realgrid.displayOptions);
        expMainItemListGrid.gridView.setColumns(expMainItemListGridBaseInfo.realgrid.columns);
        expMainItemListGrid.gridView.setOptions(expMainItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        expMainItemListGrid.dataProvider.setFields(expMainItemListGridBaseInfo.dataProvider.fields);
        expMainItemListGrid.dataProvider.setOptions(expMainItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
        expMainItemListGrid.dataProvider.clearRows();
        expMainItemListGrid.dataProvider.setRows(dataList);
    },
};

var expMain = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d'); //default :30일
        this.initDispDate();
    },
    /**
     * 검색 일자 초기화
     * @param flag
     */
    initSearchDate : function (flag) {
        expMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        expMain.setDate.setEndDate(0);
        expMain.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 노출일 초기화
     * @param flag
     */
    initDispDate : function () {
        expMain.initDateTime('7d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(expMain.search);
        $('#searchResetBtn').bindClick(expMain.searchFormReset);
        $('#resetBtn').bindClick(expMain.inputFormReset);
        $('#setBtn').bindClick(expMain.setExpMain);
        $('#schGridItemBtn').bindClick(expMain.schGridItemNo);

        // 이미지 (PC) 삭제
        $('#imgDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expMain.img.deleteImg('pc');
            }
        });

        // 이미지 (M) 삭제
        $('#mImgDelBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                expMain.img.deleteImg('mobile');
            }
        });

        $('#manageNm').calcTextLength('keyup', '#manageNmCnt');
        $('#themeNm').calcTextLength('keyup', '#themeNmCnt');
        $('#themeSubNm').calcTextLength('keyup', '#themeSubNmCnt');
        $('#tabNm').calcTextLength('keyup', '#tabNmCnt');

        $("input:radio[name=dispType]").bindChange(expMain.switchTemplate);

        $('input[name="fileArr"]').change(function() {
            expMain.img.addImg();
        });

        $("#priority").on({
            keydown: function (e) {
                if (e.which === 32) return false;
            },
            keyup: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            },
            change: function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
    },
    switchTemplate : function() {
        var dispType = $("input:radio[name=dispType]:checked").val();

        $('#themeMainTr').hide();
        $('#themeSubTr').hide();
        $('#tabTr').hide();
        $('#imgTr').hide();
        expMain.img.init();

        switch (dispType) {
            case 'THEME' :
                $('#themeMainTr').show();
                $('#themeSubTr').show();
                $('#imgTr').show();
                break;
            case 'REC' :
                $('#tabTr').show();
                break;
        }

        // Call Reset Form
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = expMainItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                expMainItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
            }else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        expMain.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        expMain.setDateTime.setEndDateByTimeStamp(flag);
        expMain.setDateTime.setStartDate(0);

        // $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 검색
     */
    search : function() {
        var searchKeyword = $('#searchKeyword').val();
        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('조회를 위해 최소 2글자 이상 입력해 주세요.');
                return false;
            }
        }

        CommonAjax.basic({
            url : '/manage/expMain/getExpMainList.json'
            , data : $('#expMainSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                expMainListGrid.setData(res);
                $('#expMainTotalCount').html($.jUtil.comma(expMainListGrid.gridView.getItemCount()));
                expMain.inputFormReset();
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('REGDT');
        expMain.initSearchDate('-30d');
        $('#searchUseYn, #searchLoc').val('');
        $('#searchType').val('dispId');
        $('#searchKeyword').val('');
    },
    /**
     * 입력폼 초기화
     */
    inputFormReset : function() {
        $('#dispId').val('').text('');
        $("input:radio[name='dispType']:radio[value='THEME']").prop('checked', true).trigger('change');
        $("input:radio[name='dispType']").attr('disabled', false);
        expMain.switchTemplate();
        expMain.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        $('#manageNm, #themeNm, #themeSubNm, #tabNm, #priority').val('');
        $('#manageNmCnt, #themeNmCnt, #themeSubNmCnt, #tabNmCnt, #itemTotalCount').html(0);
        $('#useYn').val('Y');

        expMainItem.itemList = new Array();
        expMainItemListGrid.dataProvider.clearRows();
        expMain.img.init();
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        expMain.inputFormReset();

        var rowDataJson = expMainListGrid.dataProvider.getJsonRow(selectRowId);
        var dispType = rowDataJson.dispType;

        $('#dispId').val(rowDataJson.dispId).text(rowDataJson.dispId);
        $("input:radio[name='dispType']:radio[value='" + dispType + "']").prop('checked', true).trigger('change');
        $("input:radio[name='dispType']").attr('disabled', true);
        $('#manageNm').val(rowDataJson.manageNm);
        $('#manageNmCnt').html(rowDataJson.manageNm.length);

        if(dispType === 'THEME') {
            $('#themeNm').val(rowDataJson.themeNm);
            $('#themeNmCnt').html(rowDataJson.themeNm.length);
            $('#themeSubNm').val(rowDataJson.themeSubNm);
            $('#themeSubNmCnt').html(rowDataJson.themeSubNm.length);
        } else {
            $('#tabNm').val(rowDataJson.tabNm);
            $('#tabNmCnt').html(rowDataJson.tabNm.length);
        }

        $('#priority').val(rowDataJson.priority);
        $('#useYn').val(rowDataJson.useYn);

        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);

        expMain.switchTemplate();

        if($.jUtil.isNotEmpty(rowDataJson.imgUrl)) {
            expMain.img.setImg('pc', {
                'imgUrl': rowDataJson.imgUrl,
                'src': hmpImgUrl
                    + "/provide/view?processKey=ExpThemeMainBanner&fileId="
                    + rowDataJson.imgUrl
            });
        }

        if($.jUtil.isNotEmpty(rowDataJson.mimgUrl)) {
            expMain.img.setImg('mobile', {
                'imgUrl': rowDataJson.mimgUrl,
                'src': hmpImgUrl
                    + "/provide/view?processKey=ExpMobileThemeMainBanner&fileId="
                    + rowDataJson.mimgUrl
            });
        }

        CommonAjax.basic({
            url: '/manage/expMain/getExpMainItemList.json?dispId='
                + rowDataJson.dispId,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                expMainItemListGrid.setData(res);
                $('#itemTotalCount').html(res.length);

                expMainItem.itemList = new Array();
                expMainItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
                    var itemNo = expMainItemListGrid.gridView.getValue(idx, 0);
                    var dispYn = expMainItemListGrid.gridView.getValue(idx, 10);

                    expMainItem.itemList[idx] = {
                        itemNo : itemNo
                        , dispYn : dispYn
                        , priority : parseInt(idx) + 1
                    };
                });
            }
        });
    },
    /**
     * 등록/수정
     */
    setExpMain : function() {
        if(!expMain.setValid()){
            return false;
        }

        var setForm = $("#expMainSetForm").serializeObject();

        if(!$.jUtil.isEmpty(expMainItem.itemList)) {
            setForm.itemList = expMainItem.itemList;
        }

        CommonAjax.basic({
            url:'/manage/expMain/setExpMain.json',
            data : JSON.stringify(setForm)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                expMain.inputFormReset();
                expMain.search();
            }
        });
    },
    setValid : function() {
        var dispType = $("input:radio[name=dispType]:checked").val();

        switch (dispType) {
            case 'THEME' :
                if ($.jUtil.isEmpty($('#themeNm').val())) {
                    alert('테마명(메인)을 입력해 주세요.');
                    $('#themeNm').focus();
                    return false;
                }
                if ($.jUtil.isEmpty($('#themeSubNm').val())) {
                    alert('테마명(서브)을 입력해 주세요.');
                    $('#themeSubNm').focus();
                    return false;
                }
                /*
                if ($.jUtil.isEmpty($('#imgUrl').val())) {
                    alert('이미지(PC)를 등록해 주세요.');
                    return false;
                }
                if ($.jUtil.isEmpty($('#mimgUrl').val())) {
                    alert('이미지(M)를 등록해 주세요.');
                    return false;
                }
                */
                break;
            case 'REC' :
                if ($.jUtil.isEmpty($('#tabNm').val())) {
                    alert('텝 노출명을 입력해 주세요.');
                    $('#tabNm').focus();
                    return false;
                }
                break;
        }

        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("전시기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("전시기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("전시기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if ($.jUtil.isEmpty($('#manageNm').val())) {
            alert('관리명을 입력해 주세요.');
            $('#manageNm').focus();
            return false;
        }
        if ($('#useYn').val() == 'Y') {
            if ($('#priority').val() == '') {
                alert('우선순위를 입력해주세요.');
                $('#priority').focus();
                return false;
            }

            var pattern_number = /[0-9]+$/;
            if (!pattern_number.test($('#priority').val())) {
                alert('숫자만 입력 가능하며, 최대 9999까지만 입력하실 수 있습니다.');
                $('#priority').focus();
                return false;
            }
        }
        return true;
    }
};

expMain.img = {
    imgDiv : null,
    setImgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        expMain.img.resetImg();
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        expMain.img.setImg('pc', null);
        expMain.img.setImg('mobile', null);
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(imgType, params) {
        switch (imgType) {
            case 'pc':
                if (params) {
                    $('#imgUrlTag').prop('src', params.src);
                    $('#imgUrl').val(params.imgUrl);
                    $('.displayView').show();
                    $('.displayReg').hide();
                } else {
                    $('#imgUrlTag').prop('src', '');
                    $('#imgUrl').val('');
                    $('.displayView').hide();
                    $('.displayReg').show();
                }
                break;
            case 'mobile':
                if (params) {
                    $('#mimgUrlTag').prop('src', params.src);
                    $('#mimgUrl').val(params.imgUrl);
                    $('.mDisplayView').show();
                    $('.mDisplayReg').hide();
                } else {
                    $('#mimgUrlTag').prop('src', '');
                    $('#mimgUrl').val('');
                    $('.mDisplayView').hide();
                    $('.mDisplayReg').show();
                }
                break;
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');
        let params = {
            processKey : 'ExpThemeMainBanner',
            mode : 'IMG'
        };

        if(expMain.img.setImgType === 'mobile') {
            params.processKey = 'ExpMobileThemeMainBanner';
        }

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        expMain.img.setImg(expMain.img.setImgType, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        expMain.img.setImg(imgType, null);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(type) {
        expMain.img.setImgType = type;
        $('input[name=fileArr]').click();
    }
};

var expMainItem = {
    itemList : {},      // 아이템 리스트
    init: function () {
        this.bindingEvent();
        expMainItem.itemList = new Array(); // 상품 리스트
    },
    bindingEvent: function () {
        $('#addItemPopUp').bindClick(expMainItem.addItemPopUp);
    },
    /**
     * 전시여부 변경
     */
    chgDispYn : function (action) {
        var checkedRows = expMainItemListGrid.gridView.getCheckedRows(false);
        var setDispYn, setDispYnNm;

        if (checkedRows.length == 0) {
            alert("전시여부 변경할 상품을 선택해 주세요.");
            return false;
        }

        switch (action) {
            case 'Y' :
                setDispYn = 'Y';
                setDispYnNm = '전시';
                break;
            case 'N' :
                setDispYn = 'N';
                setDispYnNm = '전시안함';
                break;
        }

        // RealGrid 값 변경.
        var setGridValue = { dispYn : setDispYn, dispYnTxt : setDispYnNm };
        checkedRows.forEach(function(data) {
            expMainItemListGrid.gridView.setValues(data, setGridValue, true);
        })

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        expMainItem.itemList = new Array();
        expMainItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            var itemNo = expMainItemListGrid.gridView.getValue(idx, 0);
            var dispYn = expMainItemListGrid.gridView.getValue(idx, 10);

            expMainItem.itemList[idx] = {
                itemNo : itemNo
                , dispYn : dispYn
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 대상 상품 엑셀 다운로드
     */
    excelDownload : function () {
        if(expMainItemListGrid.gridView.getItemCount() == 0) {
            alert("대상 상품 데이터가 없습니다.");
            return false;
        }

        var _date = new Date();
        var fileName =  "익스프레스 전시관리 대상 상품 리스트_" + _date.getFullYear() + (_date.getMonth() + 1) + _date.getDate();

        expMainItemListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export중입니다."
        });
    },
    /**
     * 상품 업로드 팝업 ( 엑셀 )
     */
    openItemReadPopup : function () {
        var target = "expMainItem";
        var callback = "expMainItem.itemExcelPopCallback";
        var url = "/manage/popup/uploadExpMainItemPop?callback=" + callback;

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function (data) {
        expMainItem.setItemInfoByExcel(data);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfoByExcel : function(resData) {
        var addObject = new Array();

        resData.forEach(function (data, index) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = expMainItemListGrid.dataProvider.searchDataRow(options);

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , itemNm : data.itemNm
                , dispYnTxt: data.dispYnTxt
                , regNm : ''
                , regDt : ''
                , dispYn : data.dispYn
                , priority : index
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        expMainItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        expMainItem.itemList = new Array();
        expMainItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            var itemNo = expMainItemListGrid.gridView.getValue(idx, 0);
            var dispYn = expMainItemListGrid.gridView.getValue(idx, 10);

            expMainItem.itemList[idx] = {
                itemNo : itemNo
                , dispYn : dispYn
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = expMainItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(expMainItemListGrid, $(obj).attr("key"), checkedRows);

        // 우선순위 변동에 따른 정렬
        expMainItem.itemList = new Array();
        expMainItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            var itemNo = expMainItemListGrid.gridView.getValue(idx, 0);
            var dispYn = expMainItemListGrid.gridView.getValue(idx, 10);

            expMainItem.itemList[idx] = {
                itemNo : itemNo
                , dispYn : dispYn
                , priority : parseInt(idx) + 1
            };
        });
    },
    /**
     * 상품조회팝업 ( 점포 )
     */
    addItemPopUp : function() {
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=expMainItem.setItemInfo&isMulti=Y&mallType=TD&storeType=EXP', 1084, 650);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {

        var addObject = new Array();

        resData.forEach(function (data, index) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = expMainItemListGrid.dataProvider.searchDataRow(options);

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , itemNm : data.itemNm
                , dispYnTxt: '전시'
                , regNm : ''
                , regDt : ''
                , dispYn : 'Y'
                , priority : index
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        expMainItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        // 전체 데이터를 Array Object에 순서대로 구성해 준다. ( 저장 또는 수정시 사용 )
        expMainItem.itemList = new Array();
        expMainItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            var itemNo = expMainItemListGrid.gridView.getValue(idx, 0);
            var dispYn = expMainItemListGrid.gridView.getValue(idx, 10);

            expMainItem.itemList[idx] = {
                itemNo : itemNo
                , dispYn : dispYn
                , priority : parseInt(idx) + 1
            };
        });
    }
};