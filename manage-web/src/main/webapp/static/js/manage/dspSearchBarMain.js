/**
 *  사이트관리 > 전시관리 > 검색창관리
 **/

$(document).ready(function() {
    dspSearchBarListGrid.init();
    dspSearchBarMain.init();
    CommonAjaxBlockUI.global();
});

// dspSearchBarMain 그리드
var dspSearchBarListGrid = {
    gridView: new RealGridJS.GridView("dspSearchBarListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init : function() {
        dspSearchBarListGrid.initGrid();
        dspSearchBarListGrid.initDataProvider();
        dspSearchBarListGrid.event();
        $('#selectItemPopup').hide();
    },
    initGrid : function() {
        dspSearchBarListGrid.gridView.setDataSource(dspSearchBarListGrid.dataProvider);
        dspSearchBarListGrid.gridView.setStyles(dspSearchBarListGridBaseInfo.realgrid.styles);
        dspSearchBarListGrid.gridView.setDisplayOptions(dspSearchBarListGridBaseInfo.realgrid.displayOptions);
        dspSearchBarListGrid.gridView.setColumns(dspSearchBarListGridBaseInfo.realgrid.columns);
        dspSearchBarListGrid.gridView.setOptions(dspSearchBarListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspSearchBarListGrid.dataProvider.setFields(dspSearchBarListGridBaseInfo.dataProvider.fields);
        dspSearchBarListGrid.dataProvider.setOptions(dspSearchBarListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspSearchBarListGrid.gridView.onDataCellClicked = function (gridView, index) {
            dspSearchBarMain.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspSearchBarListGrid.dataProvider.clearRows();
        dspSearchBarListGrid.dataProvider.setRows(dataList);
    }
};

// dspSearchBar 관리
var dspSearchBarMain = {
    linkInfoDiv : null,
    storeList : null,
    /**
     * 초기화
     */
    init : function () {
        this.bindingEvent();
        this.initSearchDate('-7d');
        this.initSetDate('1d');
        dspSearchBarMain.linkInfoDiv = null;
        dspSearchBarMain.storeList = null;
    },
    /**
     * 조회 날짜 설정
     */
    initSearchDate : function(flag) {
        dspSearchBarMain.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspSearchBarMain.setDate.setEndDate(0);
        dspSearchBarMain.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 입력폼 날짜 설정
     */
    initSetDate : function (flag) {
        dspSearchBarMain.setDate = CalendarTime.datePickerRange('dispStartDt', 'dispEndDt', {timeFormat:'HH:00:00'}, true, {timeFormat:'HH:59:59'}, true);
        dspSearchBarMain.setDate.setEndDateByTimeStamp('7d');
        dspSearchBarMain.setDate.setStartDate(flag);
        // $("#dispEndDt").datepicker("option", "minDate", $("#dispStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function () {
        $('#searchBtn').bindClick(dspSearchBarMain.search); //dspSearchBarMain 검색
        $('#searchResetBtn').bindClick(dspSearchBarMain.searchReset); //dspSearchBarMain 검색폼 초기화
        $('#setBtn').bindClick(dspSearchBarMain.set); //dspSearchBarMain 등록,수정
        $('#resetBtn').bindClick(dspSearchBarMain.reset); //dspSearchBarMain 입력폼 초기화
        $('#addItemPopUp').bindClick(dspSearchBarMain.addItemPopUp); // 상품 Popup 호출
        $('#locCd').bindChange(dspSearchBarMain.changeLocation); // 상품 Popup 호출
        $("input:radio[name='siteType']").bindChange(dspSearchBarMain.changeSiteType);

        $('#keyword').calcTextLength('keyup', '#keywordCnt');

        $('input[name="pcUseYn"]').change(function() {
            if($(this).prop("checked")) {
                $(this).val('Y');
            } else {
                $(this).val('N');
            }
        });
        $('input[name="mobileUseYn"]').change(function() {
            if($(this).prop("checked")) {
                $(this).val('Y');
            } else {
                $(this).val('N');
            }
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('click','input:radio[name="dispStoreType"]',function(){
            dspSearchBarMain.setDispStoreType($(this));
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('change','select[name="dispStoreType"]',function(){
            dspSearchBarMain.changeDispStoreType($(this));
        });

        //점포 검색 팝업
        $(document).on('click','.storeInfoBtn',function(){
            dspSearchBarMain.setStorePop($(this));
        });

        /******************************링크 정보 관려 이벤트 등록******************************************/
        //링크 정보  -> ITEM_TD -> 점포 타입 변경
        $(document).on('change','.storeType',function(){
            dspSearchBarMain.changeLinkStoreType($(this));
        });

        //택배점 설정 체크박스
        $(document).on('click','.dlvStoreYn',function(){
            dspSearchBarMain.setDlvStoreYn($(this));
        });

        //링크타입 변경
        $(document).on('change','.linkeType',function(){
            dspSearchBarMain.chgLinkInfo($(this).val() , $(this).next('div') );
        });
        /******************************링크 정보 관려 이벤트 종료******************************************/
    },
    changeSiteType : function(){
        $('#locCd').val('KEYWORD').trigger('change');
    },
    /**
     * 노출 위치 변경
     */
    changeLocation : function (){

        var location = $('#locCd').val();
        if(location =='KEYWORD'){
            $('#linkDiv').show();
        } else{
            $('#linkDiv').hide();
        }
        $("#bannerLinkType1").val('KEYWORD').trigger('change');
    },
    /**
     * dspSearchBarMain 검색
     */
    search : function () {
        if (!dspSearchBarMain.valid.search()) {
            return false;
        }
        CommonAjax.basic({
            url : '/manage/dspSearchBar/getDspSearchBarList.json?' + $('#dspSearchBarSearchForm').serialize(),
            data : null,
            method : "GET",
            successMsg : null,
            callbackFunc: function (res) {
                dspSearchBarMain.reset();
                dspSearchBarListGrid.setData(res);
                $('#dspSearchBarTotalCount').html($.jUtil.comma(dspSearchBarListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 검색폼 초기화
     */
    searchReset : function () {
        dspSearchBarMain.initSearchDate('-7d');
        $('#searchPeriodType').val('regDt');
        $('#searchUseYn, #searchBar, #searchDevice, #searchSiteType, #searchLocCd, #searchKeyword').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function (selectRowId) {
        var rowDataJson = dspSearchBarListGrid.dataProvider.getJsonRow(selectRowId);

        $('#keywordNo').val(rowDataJson.keywordNo);
        $('#useYn').val(rowDataJson.useYn);
        $('#locCd').val(rowDataJson.locCd).trigger('change');

        Calendar.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        Calendar.setCalDate('dispEndDt', rowDataJson.dispEndDt);

        $('#keyword').val(rowDataJson.keyword);
        $('#keywordCnt').html(rowDataJson.keyword.length);
        $('#setWeightOrPriority').val(rowDataJson.weightOrPriority);

        $("input:radio[name='siteType']:radio[value='" + rowDataJson.siteType
            + "']").prop('checked', true);
        $("input:radio[name='siteType']").attr("disabled", true);

        if (rowDataJson.pcUseYn == 'Y') {
            $('input[name="pcUseYn"]').prop('checked', true).val('Y');
        } else {
            $('input[name="pcUseYn"]').prop('checked', false).val('N');
        }

        if (rowDataJson.mobileUseYn == 'Y') {
            $('input[name="mobileUseYn"]').prop('checked', true).val('Y');
        } else {
            $('input[name="mobileUseYn"]').prop('checked', false).val('N');
        }
        if (rowDataJson.locCd == 'KEYWORD'){
            var link = {
                'linkType': rowDataJson.linkType
                , 'linkInfo': rowDataJson.linkInfo
                , 'linkOptions': rowDataJson.linkOptions
                , 'linkInfoNm': rowDataJson.linkInfoNm
            };

            $("#bannerLinkType1").val(link.linkType).trigger('change');
            dspSearchBarMain.setLinkInfo(link, $("#bannerLinkInfo1"));
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function(obj) {
        linkInfoDiv = $(obj).parent();

        var linkeType  = $(obj).parent().prev('select').val();
        var storeType = $(obj).parent().find('.storeType').val();
        var gnbDeviceType = $(obj).parent().find('.gnbDeviceType').val();
        // var siteType = $('#siteType').val();
        var siteType = $(":input:radio[name=siteType]:checked").val()

        switch (linkeType) {
            case "EXH"  :
                dspSearchBarMain.addPromoPopUp('EXH', siteType);
                break;
            case "EVENT" :
                dspSearchBarMain.addPromoPopUp('EVENT', siteType);
                break;
            case "ITEM_DS" :
                dspSearchBarMain.addItemPopUp('DS');
                break;
            case "ITEM_TD" :
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                dspSearchBarMain.addItemPopUp('TD' , storeType);
                break;
            case "GNB":
                dspSearchBarMain.addGnbPopUp(gnbDeviceType , siteType);
                break;
        }
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType , storeType){
        var url = '/common/popup/itemPop?callback=dspSearchBarMain.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;
            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;
        }
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 상품조회팝업 콜백 함ㅅ후
     */
    addItemPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    /**
     * 포로모션조회팝업 콜백 함ㅅ후
     */
    addPromoPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].promoNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 포로모션 조회팝업
     */
    addPromoPopUp :  function(promoType, siteType){
        var url = '/common/popup/promoPop?callback=dspSearchBarMain.addPromoPopUpCallBack&isMulti=N&siteType=' + siteType+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 링크 상세 정보 입력
     */
    setLinkInfo : function (data, obj) {
        $(obj).find('.linkInfo').val(data.linkInfo);
        $(obj).find('.linkInfoNm').val(data.linkInfoNm);

        if(data.linkType == 'ITEM_TD') {
            var storeType = data.linkOptions.substr(0,1);    //  H:HYPER, C:CLUB, E:EXPRESS
            var dlvStoreId  = data.linkOptions.substr(2);

            /*link.linkOptions.substr(0,1) == 'H' ? 'HYPER' : 'CLUB'*/
            switch (data.linkOptions.substr(0,1)) {
                case 'H' :
                    $(obj).find('.storeType').val('HYPER');
                    break;
                case 'E' :
                    $(obj).find('.storeType').val('EXP');
                    break;
                default:
                    $(obj).find('.storeType').val('HYPER');
                    break;
            }

            // N : 일반상품, D : 택배점
            if(data.linkOptions.substr(1,1) == 'D') {
                $(obj).find('.dlvStoreYn').prop("checked", true).val('Y');
                $(obj).find('.dlvStoreList').prop("disabled", false).val(dlvStoreId);
            }
        }
    },
    /**
     * 링크 유형별 노출변경
     */
    chgLinkInfo : function(linkType, obj) {
        var linkInfo ='';
        var none = "";

        switch (linkType) {
            case "EXH"  :
            case "EVENT" :
            case "ITEM_DS" :
            case "ITEM_TD":
                linkInfo  = $("#linkCommon").html();
                break;
            case "URL":
                linkInfo  = $("#linkUrl").html();
                break;
            case "GNB":
                linkInfo  = $("#linkGnbDevice").html() + $("#linkCommon").html();
                break;
            case "KEYWORD":
                linkInfo  = $("#linkKeyword").html();
                break;
            default :
                linkInfo =  none;
        }

        if(linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeHome").html() + linkInfo +  $("#dlvStoreTypeHome").html()
        }
        if($('#loc1Depth').val() == 'TOP') {
            linkInfo += '<button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspSearchBarMain.resetLinkInfo(this);">초기화</button>';
        }
        obj.html(linkInfo);
    },
    /**
     * 택배점 설정 검사
     */
    setDlvStoreYn : function(obj) {
        if($(obj).is(":checked")) {
            var storeTyep = $(obj).parent().parent().find('.storeType').val();
            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB') {
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            if ($.jUtil.isEmpty(linkInfo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled" ,false);
        } else {
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled" ,true);
        }
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        $('#itemNo').val(resData[0].itemNo);
        $('#itemNm').val(resData[0].itemNm);
    },
    /**
     * 점포 상품 점포 유형 변경
     */
    changeLinkStoreType : function(obj) {
        $(obj).parent().find('.linkInfo').val('');
        $(obj).parent().find('.linkInfoNm').val('');
        $(obj).parent().find('.dlvStoreYn').prop("checked", false).val('N');

        if($(obj).val() == 'HYPER' ||  $(obj).val() == 'CLUB'){
            $(obj).parent().find('.dlvStoreList').prop("disabled", false).val('');
        } else{
            $(obj).parent().find('.dlvStoreList').prop("disabled", true).val('');
        }
    },
    setStorePop : function(obj) {
        if ($.jUtil.isEmpty(dspSearchBarMain.storeList)) {
            dspSearchBarMain.openStoreInfoPopup();
        } else {
            dspSearchBarMain.openStoreInfoPopup(dspSearchBarMain.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        var resCount = res.storeList.length;

        dspSearchBarMain.storeList = res.storeList;
        $('#dispStoreCnt').html(resCount);
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(data) {
        var storeType = $('select[name="dispStoreType"]').val();

        $('.storePop input[name="storeType"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if ($("#chgYn").val() == 'Y') {
            $('.storePop input[name="setYn"]').val('N');
        } else {
            $('.storePop input[name="setYn"]').val('Y');
        }

        var target = "storeSelectPop";
        windowPopupOpen('', target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var siteType  = $('#siteType').val();
        var dispStoreType = $(obj).val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(dispStoreType == 'ALL') {
            $dispStore.hide();
            dspSearchBarMain.storeList = null;
        } else {
            if(siteType == 'HOME') {
                $dispStore.prepend($('#storeTypeHome').html());
            } else {
                $dispStore.prepend($('#storeTypeClub').html());
            }
            $dispStore.find('select').attr('name' ,'dispStoreType');
            $dispStore.show();
        }
        dspSearchBarMain.changeDispStoreType();
    },
    changeDispStoreType : function() {
        dspSearchBarMain.storeList = null;
        $('#dispStoreCnt').html('0');
    },
    /**
     * 택배점 설정 검사
     */
    setDlvStoreYn : function(obj) {
        if($(obj).is(":checked")) {
            var storeTyep = $(obj).parent().parent().find('.storeType').val();
            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB') {
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            if ($.jUtil.isEmpty(linkInfo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled" ,false);
        } else {
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled" ,true);
        }
    },
    /**
     * 전체 배너 템플릿 초기화
     */
    resetAllLinkTypeTemplate : function() {
        dspSearchBarMain.resetForm();
    },
    /**
     * dspSearchBarMain 등록/수정
     */
    set : function () {
        if (!dspSearchBarMain.valid.set()) {
            return false;
        }

        const setWeightOrPriority = $('#setWeightOrPriority').val();
        switch ($('#locCd option:selected').val()) {
            case 'TAG' :
                $('#priority').val(setWeightOrPriority);
                break
            case 'KEYWORD' :
                $('#weight').val(setWeightOrPriority);
                break;
        }

        var setDspSearchBar = $('#dspSearchBarSetForm').serializeObject();
        if(setDspSearchBar.locCd == 'KEYWORD') {
            var linkInfo = setDspSearchBar.linkList[0];
            if(linkInfo.linkType == 'ITEM_TD') {
                //링크 옵션 설정1
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    if($.jUtil.isEmpty(linkInfo.dlvStoreId)) {
                        alert('택배점 선택을 진행해 주세요.');
                        return false;
                    }
                    linkInfo.linkOptions = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;
                } else {
                    linkInfo.linkOptions = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }
            }

            setDspSearchBar.linkType = linkInfo.linkType;
            setDspSearchBar.linkInfo = linkInfo.linkInfo;
            setDspSearchBar.linkOptions = linkInfo.linkOptions;
        } else {
            setDspSearchBar.linkType = 'NONE';
        }


        CommonAjax.basic({ url:'/manage/dspSearchBar/setDspSearchBar.json',
            data : JSON.stringify(setDspSearchBar),
            method:"POST",
            successMsg : null,
            contentType : "application/json",
            callbackFunc : function(res) {
                alert(res.returnMsg);
                dspSearchBarMain.reset();
                dspSearchBarMain.search();
        }});
    },
    /**
     * dspSearchBarMain 입력/수정 폼 초기화
     */
    reset : function () {
        dspSearchBarMain.initSetDate('1d');
        $('#keywordNo, #linkInfo, #keyword, #itemNo, #itemNm, #setLinkInfo, #weight, #setWeightOrPriority, #priority').val('');
        $('#keywordCnt').html(0);
        $('#locCd').val('KEYWORD').trigger('change');
        $('#useYn').val('Y');
        $('#linkType').val('KEYWORD');
        $('#selectTextBox').show();
        $('#selectItemPopup').hide();

        $("input:radio[name='siteType']:radio[value='HOME']").prop('checked', true);
        $("input:radio[name='siteType']").attr("disabled", false);
        $('input[name="pcUseYn"]').prop('checked', false).val('N');
        $('input[name="mobileUseYn"]').prop('checked', false).val('N');
        $("#bannerLinkType1").val('KEYWORD').trigger('change');
    }
};
/**
 * dspSearchBarMain 검색,입력,수정 validation check
 */
dspSearchBarMain.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();
        if (!moment(startDt.val(), 'YYYY-MM-DD', true).isValid() || !moment(
            endDt.val(), 'YYYY-MM-DD', true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            restriction.initSearchDate('-7d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(
                moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
        }
        return true;
    },
    set: function () {
        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if($.jUtil.isEmpty($('#useYn').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요. ( 사용여부 )");
            $('#useYn').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#setWeightOrPriority').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요. ( 가중치/우선순위 )");
            $('#setWeightOrPriority').focus();
            return false;
        }
        if($('#setWeightOrPriority').val() < 1 || $('#setWeightOrPriority').val() > 9) {
            alert("가중치/우선순위는 1~9의 숫자만 입력해 주세요.");
            $('#setWeightOrPriority').val('').focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 설정 가능합니다. \n전시 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD HH:mm:ss"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(moment(endDt.val()).add(-1, "day").format("YYYY-MM-DD 00:00:00"));
            return false;
        }
        if($.jUtil.isEmpty($('#keyword').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요. ( 키워드 )");
            $('#keyword').focus();
            return false;
        }
        if($('input[name="pcUseYn"]').is(":checked") == false && $('input[name="mobileUseYn"]').is(":checked") == false) {
            alert("필수 입력 항목을 선택/입력 해주세요. ( 디바이스 )");
            return false;
        }

        var bannerDetail = $("#dspSearchBarSetForm").serializeObject(true);
        if($.jUtil.isEmpty(bannerDetail.linkList[0].linkInfo) && bannerDetail.linkList[0].linkType != 'NONE' && bannerDetail.locCd == 'KEYWORD') {
            alert("링크정보를 등록해 주세요." );
            return false;
        }

        return true;
    }
};