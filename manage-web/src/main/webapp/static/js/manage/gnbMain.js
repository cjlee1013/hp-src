/**
 * 사이트관리 > GNB 관
 */
$(document).ready(function() {
    gnbListGrid.init();
    gnb.init();
    CommonAjaxBlockUI.global();
});

// 공지사항 그리드
var gnbListGrid = {

    gridView : new RealGridJS.GridView("gnbListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        gnbListGrid.initGrid();
        gnbListGrid.initDataProvider();
        gnbListGrid.event();

    },
    initGrid : function() {

        gnbListGrid.gridView.setDataSource(gnbListGrid.dataProvider);
        gnbListGrid.gridView.setStyles(gnbListGridBaseInfo.realgrid.styles);
        gnbListGrid.gridView.setDisplayOptions(gnbListGridBaseInfo.realgrid.displayOptions);
        gnbListGrid.gridView.setColumns(gnbListGridBaseInfo.realgrid.columns);
        gnbListGrid.gridView.setOptions(gnbListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        gnbListGrid.dataProvider.setFields(gnbListGridBaseInfo.dataProvider.fields);
        gnbListGrid.dataProvider.setOptions(gnbListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        gnbListGrid.gridView.onDataCellClicked = function(gridView, index) {
            gnb.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        gnbListGrid.dataProvider.clearRows();
        gnbListGrid.dataProvider.setRows(dataList);

    },

};

// GNB 관리
var gnb = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-10d');
        this.initSetDate('-10d');
    },
    initSearchDate : function (flag) {
        gnb.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        gnb.setDate.setEndDate(0);
        gnb.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSetDate : function (flag) {

        gnb.setDate = Calendar.datePickerRange('dispStartDt', 'dispEndDt');
        gnb.setDate.setEndDate(0);
        gnb.setDate.setStartDate(flag);
        $("#dispEndDt").datepicker("option", "minDate", $("#dispStartDt").val());

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(gnb.search);
        $('#searchResetBtn').bindClick(gnb.searchFormReset);
        $('#resetBtn').bindClick(gnb.resetForm);
        $('#linkType').bindChange(gnb.changeLinkType);
        $('#setGnbBtn').bindClick(gnb.setGnb);
        $('#regImageBtn').bindClick(gnb.setImg);


    },
    /**
     * GNB 검색
     */
    search : function() {

        var searchKeyword = $('#searchKeyword').val();
        var searchKeywordLength = $('#searchKeyword').val().length;

        if(!gnb.validDate($("#searchStartDt") ,$("#searchEndDt"),'-10d' ,gnb.initSearchDate)){
            return false;
        }

        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if(searchKeywordLength < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#searchKeyword').focus();
                return false;
            }

        }
        CommonAjax.basic({url:'/manage/gnb/getGnbList.json?' + $('#gnbSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                gnb.resetForm();
                gnbListGrid.setData(res);
                $('#gnbTotalCount').html($.jUtil.comma(gnbListGrid.gridView.getItemCount()));
            }});

    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        gnb.initSearchDate('-10d');
        $('#searchSite').val('');
        $('#searchDevice').val('PC');
        $('#searchUseYn').val('');
        $('#searchType').val('BANNER');
        $('#searchKeyword').val('');

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = gnbListGrid.dataProvider.getJsonRow(selectRowId);

        $('#gnbNo').val(rowDataJson.gnbNo);
        $('input[name="siteType"][value="' + rowDataJson.siteType + '"]').trigger('click'); //사이트 구분
        $('#bannerNm').val(rowDataJson.bannerNm);   //배너명
        $('input[name="device"][value="' + rowDataJson.device + '"]').trigger('click'); //디바이스
        $('#useYn').val(rowDataJson.useYn); //사용여부
        $('#priority').val(rowDataJson.priority); //전시순서
        $('#dispStartDt').val(rowDataJson.dispStartDt); //전시시작
        $('#dispEndDt').val(rowDataJson.dispEndDt);  //전시종료
        if(rowDataJson.iconKind == 'NEW'){   // new icon
            $('#iconKind').prop( "checked", true );
            $('#iconKind').val(rowDataJson.iconKind);
        }else{
            $('#iconKind').prop( "checked", false );
        }

        $('#linkType').val(rowDataJson.linkType).trigger('change');
        $('#linkInfo').val(rowDataJson.linkInfo);
        $('#dispNm').val(rowDataJson.dispNm);


    },
    /**
     * 링크 유형에 따라 입력 박스 변경
     */
    changeLinkType : function(){

        $('#linkInfo').prop('readonly',false);
        $('#linkInfo').width('200px');

        $('#linkInfo').val('');
        $('#linkCommon').hide();

        var linkeType  = $("#linkType").val();

        switch (linkeType) {
            case "EXH"  :
            case "EVENT" :
                $('#linkCommon').show();
                $('#linkInfo').prop('readonly',true);
                break;
            case "NONE" :
                $('#linkInfo').prop('readonly',true);
                break;
            case "URL" :
                $('#linkInfo').width('600px');
                break;
        }
    },
    //기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
    popLink : function(){

        var linkeType  = $("#linkType").val();

        switch (linkeType) {
            case "EXH"  :
            case "EVENT" :
                var url = '/common/popup/promoPop?callback=gnb.addPromoPopUpCallBack&isMulti=N&siteType=' + $('input[name="siteType"]:checked').val()+ "&promoType=" + linkeType;
                $.jUtil.openNewPopup(url , 1084, 650);
                break;
        }

    },
    /**
     * 포로모션조회팝업 콜백 함수
     */
    addPromoPopUpCallBack : function(resDataArr) {

        $('#linkInfo').val(resDataArr[0].promoNo);
        $('#linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 전시기간  validation check
     */
    validDate : function(startDt , endDt ,baseInterval , callBackFunction){

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("전시기간 날짜 형식이 맞지 않습니다");
            callBackFunction(baseInterval);
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        return true;
    },
    checkUrl : function(url){
        var expUrl = /(http(s)?:\/\/)([a-z0-9\w])/gi;
        return expUrl.test(url);
    },
    /**
     * GNB 등록/수정
     */
    setGnb : function() {

        var setGnb = $('#gnbSetForm').serializeObject();

        var objectArray = [$('#priority') ,$('#dispNm')];

        //필수 입력값
        if($.jUtil.isEmptyObjectArray(objectArray)){
            alert("필수 입력 항목을 선택/입력 해주세요." );
            return false;
        }
        //전시순서
        if (!$.jUtil.isAllowInput( setGnb.priority, ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        //우선순위는 1~99 범위 내에서 선택
        if(setGnb.priority < 1 && setGnb.priority > 99 ){
            alert("전시 순서는 1~99안에서 입력해 주세요");
            $("#priority").val('');
            $("#priority").focus();
            return false;
        }

        if($.jUtil.isEmpty(setGnb.linkInfo) && setGnb.linkType != 'NONE'){

            alert("링크정보를 등록해 주세요." );
            return false;
        }
        /*if(setGnb.linkType =='URL' && !gnb.checkUrl(setGnb.linkInfo)){

            alert("http:// 또는 https://를 포함하여 url을 입력해 주세요." );
            return false;

        }*/


        //전시기간
        if(!gnb.validDate($('#dispStartDt') ,$('#dispEndDt') ,'-10d' ,gnb.initSetDate)){
            return false;
        }

        CommonAjax.basic({url:'/manage/gnb/setGnb.json',data: JSON.stringify(setGnb), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                alert(res.returnMsg);
                gnb.search();
            }});

    },
    /**
     * GNB 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#gnbNo').val('');
        $('input:radio[name="siteType"][value="HOME"]').trigger('click');
        $('input:radio[name="device"][value="PC"]').trigger('click');
        $('#bannerNm').val('');
        $('#userYn').val('Y');
        $('#priority').val('');
        gnb.initSetDate('-10d');
        $('#linkType').val('NONE').trigger('change');
        $('#linkInfo').val('');
        $("#iconKind").prop( "checked", false );
        $('#dispNm').val('');
    }

};

