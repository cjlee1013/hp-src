/**
 * 사이트관리 > 전시관리 > 메인 테마 관리
 */
$(document).ready(function() {
    dspMainThemePeriodListGrid.init();
    dspMainThemePeriodItemListGrid.init();
    dspMainThemePeriod.init();
    dspMainThemePeriodItem.init();
    CommonAjaxBlockUI.global();
});

var dspMainThemePeriodListGrid = {
    gridView : new RealGridJS.GridView("dspMainThemePeriodListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspMainThemePeriodListGrid.initGrid();
        dspMainThemePeriodListGrid.initDataProvider();
        dspMainThemePeriodListGrid.event();
    },
    initGrid : function() {
        dspMainThemePeriodListGrid.gridView.setDataSource(dspMainThemePeriodListGrid.dataProvider);
        dspMainThemePeriodListGrid.gridView.setStyles(dspMainThemePeriodGridBaseInfo.realgrid.styles);
        dspMainThemePeriodListGrid.gridView.setDisplayOptions(dspMainThemePeriodGridBaseInfo.realgrid.displayOptions);
        dspMainThemePeriodListGrid.gridView.setColumns(dspMainThemePeriodGridBaseInfo.realgrid.columns);
        dspMainThemePeriodListGrid.gridView.setOptions(dspMainThemePeriodGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspMainThemePeriodListGrid.dataProvider.setFields(dspMainThemePeriodGridBaseInfo.dataProvider.fields);
        dspMainThemePeriodListGrid.dataProvider.setOptions(dspMainThemePeriodGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspMainThemePeriodListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspMainThemePeriod.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspMainThemePeriodListGrid.dataProvider.clearRows();
        dspMainThemePeriodListGrid.dataProvider.setRows(dataList);
    }
};

var dspMainThemePeriodItemListGrid = {
    gridView: new RealGridJS.GridView("dspMainThemePeriodItemListGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        dspMainThemePeriodItemListGrid.initGrid();
        dspMainThemePeriodItemListGrid.initDataProvider();
    },
    initGrid: function () {
        dspMainThemePeriodItemListGrid.gridView.setDataSource(dspMainThemePeriodItemListGrid.dataProvider);

        dspMainThemePeriodItemListGrid.gridView.setStyles(dspMainThemePeriodItemGridBaseInfo.realgrid.styles);
        dspMainThemePeriodItemListGrid.gridView.setDisplayOptions(dspMainThemePeriodItemGridBaseInfo.realgrid.displayOptions);
        dspMainThemePeriodItemListGrid.gridView.setColumns(dspMainThemePeriodItemGridBaseInfo.realgrid.columns);
        dspMainThemePeriodItemListGrid.gridView.setOptions(dspMainThemePeriodItemGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        dspMainThemePeriodItemListGrid.dataProvider.setFields(dspMainThemePeriodItemGridBaseInfo.dataProvider.fields);
        dspMainThemePeriodItemListGrid.dataProvider.setOptions(dspMainThemePeriodItemGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        dspMainThemePeriodItemListGrid.dataProvider.clearRows();
        dspMainThemePeriodItemListGrid.dataProvider.setRows(dataList);
    }
};

var dspMainThemePeriod = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');
        this.initSetDate('0');
    },
    initSearchDate : function (flag) {
        dspMainThemePeriod.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspMainThemePeriod.setDate.setEndDate(0);
        dspMainThemePeriod.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSetDate : function (flag) {
        dspMainThemePeriod.setDate = CalendarTime.datePickerRange('dispStartDt', 'dispEndDt', {timeFormat:"HH:00:00"}, true, {timeFormat:"HH:59:59"}, true);
        dspMainThemePeriod.setDate.setEndDateByTimeStamp(0);
        dspMainThemePeriod.setDate.setStartDate(flag);
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#setBtn').bindClick(dspMainThemePeriod.set); // 등록,수정
        $('#resetBtn').bindClick(dspMainThemePeriod.resetForm); // 입력폼 초기화
        $('#searchBtn').bindClick(dspMainThemePeriod.search); // 검색
        $('#searchResetBtn').bindClick(dspMainThemePeriod.searchFormReset); // 검색

        $('#themeMngNm').calcTextLength('keyup', '#themeMngNmCnt');
        $('#themeNm').calcTextLength('keyup', '#themeNmCnt');

        $('#locType').bindChange(dspMainThemePeriod.changeLinkType);
    },
    /**
     * dspMainTheme 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspMainThemePeriod/getDspMainThemePeriodList.json'
            , data : $('#dspMainThemeSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspMainThemePeriod.resetForm();
                dspMainThemePeriodListGrid.setData(res);
                $('#dspMainThemePeriodTotalCount').html($.jUtil.comma(dspMainThemePeriodListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('REGDT');
        dspMainThemePeriod.initSearchDate('-7d');
        $('#searchLocType').val('LION');
        $('#searchUseYn').val('');
        $('#searchType').val('themeNm');
        $('#searchKeyword').val('');
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function() {
        var locType = $("#locType").val();

        $('#linkTr').find('.linkInfo, .linkInfoNm').val('');

        if(locType === 'FIX') {
            $('#linkTr').show();
        } else {
            $('#linkTr').hide();
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function() {
        var linkType  = $('#linkType').val();
        switch (linkType) {
            case "EXH"  :
                dspMainThemePeriod.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                dspMainThemePeriod.addPromoPopUp('EVENT');
                break;
        }
    },
    /**
     * 포로모션 조회팝업
     */
    addPromoPopUp : function(promoType) {
        var url = '/common/popup/promoPop?callback=dspMainThemePeriod.addPromoPopUpCallBack&isMulti=N&siteType=' + $("#siteType").val()+ "&promoType=" + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백 함수
     */
    addPromoPopUpCallBack : function(resDataArr) {
        $('#linkTr').find('.linkInfo').val(resDataArr[0].promoNo);
        $('#linkTr').find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        const rowDataJson = dspMainThemePeriodListGrid.dataProvider.getJsonRow(selectRowId);

        $('#themeId').val(rowDataJson.themeId);
        $('#locType').val(rowDataJson.locType).attr('disabled', true).trigger('change');

        if(rowDataJson.locType === 'FIX') {
            $('#linkTr').find('.linkInfo').val(rowDataJson.linkInfo);
            $('#linkTr').find('.linkInfoNm').val(rowDataJson.linkInfoNm);
        }

        $('#themeNm').val(rowDataJson.themeNm);
        $('#themeNmCnt').html(rowDataJson.themeNm.length);
        $('#themeMngNm').val(rowDataJson.themeMngNm);
        $('#themeMngNmCnt').html(rowDataJson.themeMngNm.length);
        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);
        $('#priority').val(rowDataJson.priority);
        $('#useYn').val(rowDataJson.useYn);

        CommonAjax.basic({
            url:'/manage/dspMainThemePeriod/getDspMainThemePeriodItemList.json',
            data : { 'themeId' : rowDataJson.themeId }
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspMainThemePeriodItemListGrid.setData(res);
                $('#dspMainThemePeriodItemTotalCount').html(dspMainThemePeriodItemListGrid.gridView.getItemCount());
            }
        });
    },
    /**
     * dspMainTheme 등록/수정
     */
    set : function() {
        if(!dspMainThemePeriod.valid.set()) {
            return false;
        }

        var setDspMainThemePeriod = $('#dspMainThemePeriodSetForm').serializeObject();

        // 고정행사의 경우 기획전만 등록 가능.
        if(setDspMainThemePeriod.locType === 'FIX') {
            setDspMainThemePeriod.linkType = 'EXH';
        } else {
            setDspMainThemePeriod.linkType = 'NONE';
            setDspMainThemePeriod.linkInfo = null;
        }

        // 그리드의 화면대로 우선순위 세팅.
        dspMainThemePeriodItemListGrid.dataProvider.getRows(0, -1).forEach(function(data, idx) {
            var setGridValue = { priority : parseInt(idx) + 1 };
            dspMainThemePeriodItemListGrid.gridView.setValues(idx, setGridValue, true);
        });

        setDspMainThemePeriod.listItem = dspMainThemePeriodItemListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({
            url:'/manage/dspMainThemePeriod/setDspMainThemePeriod.json',
            data : JSON.stringify(setDspMainThemePeriod)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                dspMainThemePeriod.resetForm();
                dspMainThemePeriod.search();
            }
        });
    },
    /**
     * dspMainTheme 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#themeId, #priority, #themeNm, #themeMngNm').val('');
        $('#locType').val('LION').attr('disabled', false).trigger('change');
        $('#linkTr').find('.linkInfo, .linkInfoNm').val('');
        $('#useYn').val('Y');
        $('#themeMngNmCnt, #themeNmCnt, #dspMainThemePeriodItemTotalCount').html('0');

        dspMainThemePeriod.initSetDate('0');
        dspMainThemePeriodItemListGrid.dataProvider.clearRows();
    }
};

/**
 * dspMainTheme 검색,입력,수정 validation check
 */
dspMainThemePeriod.valid = {
    set : function () {
        if($.jUtil.isEmpty($('#themeMngNm').val())) {
            $('#themeMngNm').focus();
            alert('관리 테마명을 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#themeNm').val())) {
            $('#themeNm').focus();
            alert('전시 테마명을 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#priority').val())) {
            $('#priority').focus();
            alert('우선순위를 입력해 주세요.');
            return false;
        }
        if(!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        if($('#priority').val()> 999) {
            alert("최대 999까지 입력 가능합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }
        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("전시기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("전시기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD HH:mm:ss"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("전시기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(moment(endDt.val()).add(-1, "day").format("YYYY-MM-DD 00:00:00"));
            return false;
        }
        if($('#locType').val() === 'FIX' && $.jUtil.isEmpty($('#linkTr').find('.linkInfo').val())) {
            alert("기획전은 필수 항목 입니다.");
            return;
        }
        return true;
    }
};

var dspMainThemePeriodItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspMainThemePeriodItem.search);
        $('#addItemPopUp').bindClick(dspMainThemePeriodItem.addItemPopUp);
        $('#schGridItemBtn').bindClick(dspMainThemePeriodItem.schGridItemNo);
    },
    /**
     * dspMainTheme 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/dspMainThemePeriod/getDspMainThemePeriodList.json'
            , data : $('#dspMainThemeSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                dspMainThemePeriodListGrid.setData(res);
                $('#dspMainThemeTotalCount').html($.jUtil.comma(dspMainThemePeriodListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = dspMainThemePeriodItemListGrid.gridView.getCheckedRows(false);
        if (checkedRows.length == 0) {
            alert("순서 변경할 상품을 선택해 주세요.");
            return false;
        }

        realGridMoveRow(dspMainThemePeriodItemListGrid, $(obj).attr("key"), checkedRows);
    },
    /**
     * 그리드 상품 조회
     */
    schGridItemNo : function () {

        let itemNo = $('#schGridItemNo').val();

        if(!$.jUtil.isAllowInput( itemNo, ['NUM']) || itemNo.length < 9) {
            return $.jUtil.alert("상품번호를 정확히 입력해주세요.", 'schGridItemNo');
        }

        if (!$.jUtil.isEmpty(itemNo)) {
            var rowIndex = dspMainThemePeriodItemListGrid.dataProvider.searchDataRow({fields : ["itemNo"], values : [itemNo]});

            if (rowIndex > -1) {
                dspMainThemePeriodItemListGrid.gridView.setCurrent({dataRow : rowIndex, fieldIndex : 1});
            } else {
                return $.jUtil.alert("검색된 상품이 없습니다.", 'schGridItemNo');
            }
        }
    },
    /**
     * 전시여부 변경
     */
    chgDispYn : function (action) {
        var checkedRows = dspMainThemePeriodItemListGrid.gridView.getCheckedRows(false);
        var setDispYn, setDispYnNm;

        if (checkedRows.length == 0) {
            alert("전시여부 변경할 상품을 선택해 주세요.");
            return false;
        }

        switch (action) {
            case 'Y' :
                setDispYn = 'Y';
                setDispYnNm = '전시';
                break;
            case 'N' :
                setDispYn = 'N';
                setDispYnNm = '전시안함';
                break;
        }

        // RealGrid 값 변경.
        var setGridValue = { dispYn : setDispYn, dispYnTxt : setDispYnNm };
        checkedRows.forEach(function(data) {
            dspMainThemePeriodItemListGrid.gridView.setValues(data, setGridValue, true);
        });
    },
    /**
     * 상품 업로드 팝업 ( 엑셀 )
     */
    openItemReadPopup : function () {
        var target = "dspMainThemePeriodItem";
        var callback = "dspMainThemePeriodItem.itemExcelPopCallback";
        var url = "/manage/popup/uploadMainThemePeriodItemPop?callback=" + callback;

        windowPopupOpen(url, target, 600, 350);
    },
    /**
     * 상품 일괄등록 팝업 콜백
     */
    itemExcelPopCallback : function (data) {
        dspMainThemePeriodItem.setItemInfoByExcel(data);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfoByExcel : function(resData) {
        var addObject = new Array();

        resData.forEach(function (data) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = dspMainThemePeriodItemListGrid.dataProvider.searchDataRow(options);

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , itemNm : data.itemNm
                , dispYnTxt: data.dispYnTxt
                , regNm : ''
                , regDt : ''
                , dispYn : data.dispYn
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        dspMainThemePeriodItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        $('#dspMainThemePeriodItemTotalCount').html(dspMainThemePeriodItemListGrid.gridView.getItemCount());
    },
    /**
     * 상품조회팝업 ( 점포 )
     */
    addItemPopUp : function() {
        switch ($('#storeType').val()) {
            case 'TD' :
                $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspMainThemePeriodItem.setItemInfo&isMulti=Y&mallType=TD&storeType=' + $("#itemStoreType option:selected").val(), 1084, 650);
                break;
            case 'DS' :
                $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspMainThemePeriodItem.setItemInfo&isMulti=Y&mallType=DS&storeType=DS', 1084, 650);
                break;
        }
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {

        var addObject = new Array();
        var mallType = 'TD'
        var mallTypeNm = '점포상품';
        var selectedItemStoreType = $("#itemStoreType option:selected").val();
        if($.jUtil.isEmpty(selectedItemStoreType)) {
            mallType = 'DS';
            mallTypeNm = '셀러상품'
        }

        resData.forEach(function (data, index) {
            // 중복체크를 진행 한다.
            var options = {
                startIndex: 0,
                fields: ['itemNo'],
                values: [data.itemNo]
            };
            var dataRow = dspMainThemePeriodItemListGrid.dataProvider.searchDataRow(options);

            // 추가할 아이템 Object 구성.
            var setObject = {
                itemNo : data.itemNo
                , itemNm : data.itemNm
                , dispYnTxt: '전시'
                , regNm : ''
                , regDt : ''
                , dispYn : 'Y'
                , priority : index
            };

            // -1 이 리턴 될 경우 중복 없음.
            if(dataRow === -1) {
                addObject.push(setObject);
            }
        });

        // 중복 제거된 Object 들을 Grid에 첫번째 행부터 추가 해준다.
        dspMainThemePeriodItemListGrid.dataProvider.insertRows(0, addObject, 0, -1, false);

        $('#dspMainThemePeriodItemTotalCount').html(dspMainThemePeriodItemListGrid.gridView.getItemCount());
    }
};