/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > 좌측 윙배너
 */
$(document).ready(function() {

    dspMainLwingBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainLwingBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="lwingBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="lwingBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="lwingBannerLinktype"]').trigger('change');
    },

    //링크 타입 초기화 (단 번호별 초기화)
    resetLinkInfo : function(linkTemplateNumber){


        $('.lwingBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $('.lwingBannerDiv'+linkTemplateNumber).find('.lwingBannerTitle').val('').prop('readOnly' , true);;
        $('.lwingBannerDiv'+linkTemplateNumber).find('.lwingBannerSubTitle').val('').prop('readOnly' , true);;
        $('.lwingBannerDiv'+linkTemplateNumber).find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.lwingBannerDiv'+linkTemplateNumber).find('.lwingBannerTitle').prop('readonly' ,true).val('');
        $('.lwingBannerDiv'+linkTemplateNumber).find('.lwingBannerSubTitle').prop('readonly' ,true).val('');
        $('.lwingBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');


    },

    //링크 타입 전체 초기화
    resetLinkTypeAll : function(){

        $("[class^='lwingBannerDiv']").css("display", "none");
        $("[class^='lwingBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='lwingBannerDiv']").find('.lwingBannerTitle').val('');
        $("[class^='lwingBannerDiv']").find('.lwingBannerSubTitle').val('');
        $("[class^='lwingBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='lwingBannerDiv']").find('.lwingBannerTitle').prop('readonly' ,true).val('');
        $("[class^='lwingBannerDiv']").find('.lwingBannerSubTitle').prop('readonly' ,true).val('');
        $("[class^='lwingBannerDiv']").find('.deleteImgBtn').trigger('click');

        $('.lwingBannerDiv1').css("display", "");

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainLwingBannerDetail.resetLinkTypeAll();
        $("#lwingBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    setBannerValid : function(){

        var bannerDetail = $("#lwingBannerSetForm").serializeObject(true);

        var dispStoreType = $("#lwingBannerSetForm").find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }

        for(var i =0 ; i < 1 ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }

        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){

        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART'

        $("#lwingBannerSetForm").find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');

        if(dispStoreType == 'PART'){

            $('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $("#lwingBannerSetForm").find('#dispStoreCnt').html(data.storeList.length);

        }

        if (!$.jUtil.isEmpty(data.linkList)) {

            for(var i =0 ; i < 1 ; i++){

                var link  = data.linkList[i];
                var templateNum = i+1;

                dspMainCommon.img.setImg($('.lwingBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

                });

                $("#lwingBannerLinktype" + templateNum ).val(link.linkType).trigger('change');

                dspMainCommon.setLinkInfo(link ,  $("#lwingBannerLinkInfo" + templateNum));

            }

        }

    },
    getBannerDetail : function(){
        var bannerDetail = $("#lwingBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];

            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Object();

        }
        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }

        return bannerDetail;

    }
};