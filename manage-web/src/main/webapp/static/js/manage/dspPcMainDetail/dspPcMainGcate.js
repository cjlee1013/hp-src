/**
 * 사이트관리 > 전시관리 > PC메인관리 > 대대분류배너
 */
$(document).ready(function() {

    dspMainGcateBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainGcateBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //링크 타입 초기화
    resetLinkInfo : function(rinkNo){

        $("[class^='gcateBannerDiv" + rinkNo+ "']").find('select').val('EXH').trigger('change');
        $("[class^='gcateBannerDiv" + rinkNo+ "']").find('.deleteImgBtn').trigger('click');

    },
    resetLinkTypeAll : function(){

        $("[class^='gcateBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='gcateBannerDiv']").find('.deleteImgBtn').trigger('click');

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainGcateBannerDetail.resetLinkTypeAll();

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="gcateBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="gcateBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="gcateBannerLinktype"]').trigger('change');

    },
    setBannerValid : function(){

        var bannerDetail = $("#gcateBannerSetForm").serializeObject();


        for(var i =0 ; i < bannerDetail.linkList.length ; i++){


            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){



        if (!$.jUtil.isEmpty(data.linkList)) {

            for(var i = 0 ; i < data.linkList.length ; i++){


                var link  = data.linkList[i];
                var divisionNum = i+1;

                //이미지
                dspMainCommon.img.setImg($('.gcateBannerDiv' + divisionNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'   : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl,

                });


                $("#gcateBannerLinktype" + divisionNum).val(link.linkType).trigger('change')
                dspMainCommon.setLinkInfo(link ,  $("#gcateBannerLinkInfo" + divisionNum));

            }

        }

    },

    getBannerDetail : function(){
        var bannerDetail = $("#gcateBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < bannerDetail.linkList.length ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Object();
        }
        bannerDetail.linkList  = linkList;
        return bannerDetail;

    }
};