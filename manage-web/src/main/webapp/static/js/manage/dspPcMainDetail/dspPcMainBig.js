/**
 * 사이트관리 > 전시관리 > 모바일 > 메인빅배너
 */
$(document).ready(function() {

    dspMainBigBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainBigBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $(document).on('change','#template',function(){
            dspMainBigBannerDetail.changeTemplate();
            dspMainBigBannerDetail.changeImgSize();
            dspMainBigBannerDetail.changeTextLenght();

        });

        $(document).on('change','#templateDetail',function(){
            dspMainBigBannerDetail.changeImgSize();
            dspMainBigBannerDetail.changeTextLenght();
        });


        $(document).on('keyup','.bigBannerTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });
        $(document).on('keyup','.bigBannerSubTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });

        $(document).on('change','.textYn',function(){
            dspMainBigBannerDetail.setText($(this));
        });

    },
    //타이틀 입력란 설정
    setText : function(obj){

        if($(obj).prop("checked"))  {
            $(obj).val('Y');
            $(obj).parent().parent().find('.bigBannerTitle').prop('readOnly' ,false);
            $(obj).parent().parent().find('.bigBannerSubTitle').prop('readOnly' ,false);


           /* $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , false);
            $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , false);
            $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", true).val('Y');*/

        }else {
            $(obj).val('N');
            $(obj).parent().parent().find('.bigBannerTitle').val('').prop('readOnly' ,true).trigger('keyup');
            $(obj).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly' ,true).trigger('keyup');

            /*$("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false).val('N');*/
        }

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="bigBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="bigBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="bigBannerLinktype"]').trigger('change');
    },
    /**
     * 배너 단 설정
     */
    changeTemplate : function(){


        dspMainBigBannerDetail.resetLinkTypeAll();


        var template = $("#template").val();
        var templateDetail = $("#templateDetail").val();

        //배너 입력 영역 설정
        switch (template) {

            case "6"  :
                $('.bigBannerDiv6').css("display", "");
            case "5" :
                $('.bigBannerDiv5').css("display", "");
                $('.bigBannerDiv4').css("display", "");
            case "3" :
                $('.bigBannerDiv3').css("display", "");
            case "2" :
                $('.bigBannerDiv2').css("display", "");
            case "1" :
                $('.bigBannerDiv1').css("display", "");
                break;

        };
        //단 선택 상세 정보 셀렉트 박스 제어
        $("#templateDetail").html('');
        switch (template) {

            case "6"  :
            case "1" :
                $("#templateDetail").prop('disabled' , true);
                break;
            case "5" :
            case "3" :
            case "2" :
                $("#templateDetail").append( $("#templateDetail" + template).html());
                $("#templateDetail").prop('disabled' , false);

                break;
        };


    },
    /**
     * 배너 단 설정에 따른 이미지 최적화 가이드 및 텍스트 사이즈 변경
     */
    changeImgSize : function(){

        var template = $("#template").val();
        var templateDetail = $("#templateDetail").val();


        switch (template) {

            case "1"  :
                $("#bigBannerDiv1ImgSize").html('1200*450');
                break;
            case "2" :
                switch (templateDetail) {
                    case "N" :
                        $("#bigBannerDiv1ImgSize").html('590*450');
                        $("#bigBannerDiv2ImgSize").html('590*450');
                        break;
                    case "LP" :
                        $("#bigBannerDiv1ImgSize").html('790*450');
                        $("#bigBannerDiv2ImgSize").html('390*450');
                        break;
                }
                break;
            case "3" :
                switch (templateDetail) {
                    case "N" :
                        $("#bigBannerDiv1ImgSize").html('390*450');
                        $("#bigBannerDiv2ImgSize").html('390*450');
                        $("#bigBannerDiv3ImgSize").html('390*450');
                        break;
                    case "LP" :
                        $("#bigBannerDiv1ImgSize").html('790*450');
                        $("#bigBannerDiv2ImgSize").html('390*220');
                        $("#bigBannerDiv3ImgSize").html('390*220');
                        break;
                    case "CP" :
                        $("#bigBannerDiv1ImgSize").html('290*450');
                        $("#bigBannerDiv2ImgSize").html('590*450');
                        $("#bigBannerDiv3ImgSize").html('290*450');
                        break;
                }
                break;
            case "5" :
                $("#bigBannerDiv1ImgSize").html('390*220');
                $("#bigBannerDiv2ImgSize").html('390*220');
                $("#bigBannerDiv3ImgSize").html('390*450');
                $("#bigBannerDiv4ImgSize").html('390*220');
                $("#bigBannerDiv5ImgSize").html('390*220');
                break;
            case "6" :
                $("#bigBannerDiv1ImgSize").html('390*220');
                $("#bigBannerDiv2ImgSize").html('390*220');
                $("#bigBannerDiv3ImgSize").html('390*220');
                $("#bigBannerDiv4ImgSize").html('390*220');
                $("#bigBannerDiv5ImgSize").html('390*220');
                $("#bigBannerDiv6ImgSize").html('390*220');
                break;
        };


    },
    /**
     * 배너 단 설정에 따른 이미지 최적화 가이드 및 텍스트 사이즈 변경
     */
    changeTextLenght : function(){

        var template = $("#template").val();
        var templateDetail = $("#templateDetail").val();



        switch (template) {

            case "1"  :
                $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv1TitleBaseCnt").html('20');
                $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                $("#bigDiv1SubBaseCnt").html('28');
                break;
            case "2" :
                switch (templateDetail) {
                    case "N" :
                        $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv1TitleBaseCnt").html('20');
                        $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv1SubBaseCnt").html('28');

                        $(".bigDiv2TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv2TitleBaseCnt").html('20');
                        $(".bigDiv2SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv2SubBaseCnt").html('28');
                        break;
                    case "LP" :
                        $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv1TitleBaseCnt").html('20');
                        $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv1SubBaseCnt").html('28');

                        $(".bigDiv2TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv2TitleBaseCnt").html('15');
                        $(".bigDiv2SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv2SubBaseCnt").html('20');
                        break;
                }
                break;
            case "3" :
                switch (templateDetail) {
                    case "N" :
                        $(".bigDiv1TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv1TitleBaseCnt").html('15');
                        $(".bigDiv1SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv1SubBaseCnt").html('20');

                        $(".bigDiv2TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv2TitleBaseCnt").html('15');
                        $(".bigDiv2SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv2SubBaseCnt").html('20');

                        $(".bigDiv3TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv3TitleBaseCnt").html('15');
                        $(".bigDiv3SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv3SubBaseCnt").html('20');
                        break;
                    case "LP" :
                        $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv1TitleBaseCnt").html('20');
                        $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv1SubBaseCnt").html('28');

                        $(".bigDiv2TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv2TitleBaseCnt").html('20');
                        $(".bigDiv2SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv2SubBaseCnt").html('28');

                        $(".bigDiv3TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv3TitleBaseCnt").html('20');
                        $(".bigDiv3SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv3SubBaseCnt").html('28');
                        break;
                    case "CP" :
                        $(".bigDiv1TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv1TitleBaseCnt").html('15');
                        $(".bigDiv1SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv1SubBaseCnt").html('20');

                        $(".bigDiv2TitleBaseCnt").prop('maxlength','20');
                        $("#bigDiv2TitleBaseCnt").html('20');
                        $(".bigDiv2SubBaseCnt").prop('maxlength','28');
                        $("#bigDiv2SubBaseCnt").html('28');

                        $(".bigDiv3TitleBaseCnt").prop('maxlength','15');
                        $("#bigDiv3TitleBaseCnt").html('15');
                        $(".bigDiv3SubBaseCnt").prop('maxlength','20');
                        $("#bigDiv3SubBaseCnt").html('20');
                        break;
                }
                break;
            case "5" :
                $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv1TitleBaseCnt").html('20');
                $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                $("#bigDiv1SubBaseCnt").html('28');

                $(".bigDiv2TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv2TitleBaseCnt").html('20');
                $(".bigDiv2SubBaseCnt").prop('maxlength','28');
                $("#bigDiv2SubBaseCnt").html('28');

                $(".bigDiv3TitleBaseCnt").prop('maxlength','15');
                $("#bigDiv3TitleBaseCnt").html('15');
                $(".bigDiv3SubBaseCnt").prop('maxlength','20');
                $("#bigDiv3SubBaseCnt").html('20');

                $(".bigDiv4TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv4TitleBaseCnt").html('20');
                $(".bigDiv4SubBaseCnt").prop('maxlength','28');
                $("#bigDiv4SubBaseCnt").html('28');

                $(".bigDiv5TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv5TitleBaseCnt").html('20');
                $(".bigDiv5SubBaseCnt").prop('maxlength','28');
                $("#bigDiv5SubBaseCnt").html('28');
                break;
            case "6" :
                $(".bigDiv1TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv1TitleBaseCnt").html('20');
                $(".bigDiv1SubBaseCnt").prop('maxlength','28');
                $("#bigDiv1SubBaseCnt").html('28');

                $(".bigDiv2TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv2TitleBaseCnt").html('20');
                $(".bigDiv2SubBaseCnt").prop('maxlength','28');
                $("#bigDiv2SubBaseCnt").html('28');

                $(".bigDiv3TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv3TitleBaseCnt").html('20');
                $(".bigDiv3SubBaseCnt").prop('maxlength','28');
                $("#bigDiv3SubBaseCnt").html('28');

                $(".bigDiv4TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv4TitleBaseCnt").html('20');
                $(".bigDiv4SubBaseCnt").prop('maxlength','28');
                $("#bigDiv4SubBaseCnt").html('28');

                $(".bigDiv5TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv5TitleBaseCnt").html('20');
                $(".bigDiv5SubBaseCnt").prop('maxlength','28');
                $("#bigDiv5SubBaseCnt").html('28');

                $(".bigDiv6TitleBaseCnt").prop('maxlength','20');
                $("#bigDiv6TitleBaseCnt").html('20');
                $(".bigDiv6SubBaseCnt").prop('maxlength','28');
                $("#bigDiv6SubBaseCnt").html('28');
                break;
        };


    },
    //링크 타입 초기화 (단 번호별 초기화)
    resetLinkInfo : function(linkTemplateNumber){


        $('.bigBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , true).trigger('keyup');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , true).trigger('keyup');
        $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.bigBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');


    },

    //링크 타입 전체 초기화
    resetLinkTypeAll : function(){

        $("[class^='bigBannerDiv']").css("display", "none");
        $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , true).trigger('keyup');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , true).trigger('keyup');
        $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='bigBannerDiv']").find('.deleteImgBtn').trigger('click');
        $('.bigBannerDiv1').css("display", "");

    },
    //배너 입력 폼 초기화
    resetForm : function(){

        $("#template").val('1').trigger('change');
        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    setBannerValid : function(){

        var bannerDetail = $("#bigBannerSetForm").serializeObject(true);

        var dispStoreType = $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }

        for(var i =0 ; i < parseInt($("#template").val()) ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }

        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){

        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART'

        /*$('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');*/

        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');



        if(dispStoreType == 'PART'){
            $("#bigBannerSetForm").find('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $("#bigBannerSetForm").find('#dispStoreCnt').html(data.storeList.length);
        }


        $("#template").val(data.template).trigger('change');

        if(!$.jUtil.isEmpty(data.templateDetail)){

            $("#templateDetail").val(data.templateDetail).trigger('change');

        }

        if (!$.jUtil.isEmpty(data.linkList)) {


            for(var i =0 ; i < parseInt(data.template) ; i++){

                var link  = data.linkList[i];
                var templateNum = i+1;

                if(link.textYn == 'Y'){

                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', true);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,false).val(link.title);
                    $('#bigDiv' + templateNum +'TitleCnt').html(link.title.length);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,false).val(link.subTitle);
                    $('#bigDiv' + templateNum +'SubCnt').html(link.subTitle.length);


                } else {

                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', false);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,true).val('');
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,true).val('');

                }

                dspMainCommon.img.setImg($('.bigBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey(templateNum-1) + "&fileId=" + link.imgUrl

                });

                $("#bigBannerLinktype" + templateNum ).val(link.linkType).trigger('change');

                dspMainCommon.setLinkInfo(link ,  $("#bigBannerLinkInfo" + templateNum));



            }

        }

    },
    getBannerDetail : function(){
        var bannerDetail = $("#bigBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < parseInt($("#template").val()) ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Object();

        }
        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }

        return bannerDetail;

    }
};