/**
 * 사이트관리 > 전시관리 > PC메인관리 > 최상단배너
 */
$(document).ready(function() {

    dspMainTopBannerDetail.init();
    CommonAjaxBlockUI.global();

});
var dspMainTopBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $(document).on('change','input[name="division"]',function(){

            dspMainTopBannerDetail.changeDivision(this);
        });
    },
    /**
     * 링크 분할 설정 변경
     */
    changeDivision : function(obj){

        dspMainTopBannerDetail.resetLinkTypeAll();

        switch (obj.value) {

            case "4"  :
                $("#topBannerLinktype4").prop("disabled", false);
                $("#topBannerLinkInfo4").find('.addLinkInfoBtn').prop("disabled", false);

            case "3" :
                $("#topBannerLinktype3").prop("disabled", false);
                $("#topBannerLinkInfo3").find('.addLinkInfoBtn').prop("disabled", false);

            case "2" :
                $("#topBannerLinktype2").prop("disabled", false);
                $("#topBannerLinkInfo2").find('.addLinkInfoBtn').prop("disabled", false);
                break;

        }

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="topBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="topBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="topBannerLinktype"]').trigger('change');

    },
    //개별 링크 타입  초기화
    resetLinkInfo : function(obj){
        $(obj).parent().prev('select').val('EXH').trigger('change');
    },
    //링크 타입 전체 초기화
    resetLinkTypeAll : function(){

        $('select[id^="topBannerLinktype"]').val('EXH').trigger('change');

        //링크 타입 disable
        $("#topBannerLinktype2").prop("disabled", true);
        $("#topBannerLinktype3").prop("disabled", true);
        $("#topBannerLinktype4").prop("disabled", true);

        //검색 팝업 disable
        $("#topBannerLinkInfo2").find('.addLinkInfoBtn').prop("disabled", true);
        $("#topBannerLinkInfo3").find('.addLinkInfoBtn').prop("disabled", true);
        $("#topBannerLinkInfo4").find('.addLinkInfoBtn').prop("disabled", true);

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainTopBannerDetail.resetLinkTypeAll();
        $("#topBannerSetForm").find('.deleteImgBtn').trigger('click');
        $('input:radio[name=division]:input[value="1"]').prop('checked', true).trigger('change');
        $('input:radio[name=closeType]:input[value=N]').prop('checked', true);
        $("#bgColor").val('');


    },
    setBannerValid : function(){

        var bannerDetail = $("#topBannerSetForm").serializeObject();


        if($.jUtil.isEmpty($('#imgUrl').val())){
            alert("이미지를 등록해 주세요." );
            return false;

        }

        if($.jUtil.isEmpty($('#bgColor').val())){
            alert("배경색상을 등록해 주세요." );
            return false;

        }


        var division = $("input:radio[name=division]:checked").val();

        for(var i =0 ; i < division ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }



        return true;


    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){


        $("#bgColor").val(data.bgColor); //배경색상
        $('input:radio[name=closeType]:input[value=' + data.closeType + ']').prop('checked', true);

        if (!$.jUtil.isEmpty(data.linkList)) {

            //이미지
            dspMainCommon.img.setImg($('#topBannerSetForm').find('.itemImg'), {
                'imgUrl': data.linkList[0].imgUrl,
                'imgHeight': data.linkList[0].imgHeight,
                'imgWidth': data.linkList[0].imgWidth,
                'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + data.linkList[0].imgUrl

            });

            var division = parseInt(data.division);

            $('input:radio[name=division]:input[value=' + division + ']').prop('checked', true).trigger('change');

            for(var i = 0 ; i < division ; i++){


                var link  = data.linkList[i];
                var divisionNum = i+1;
                $("#topBannerLinktype" + divisionNum).val(link.linkType).trigger('change')
                dspMainCommon.setLinkInfo(link ,  $("#topBannerLinkInfo" + divisionNum));

            }

        }

    },
    getBannerDetail : function(){


        var bannerDetail = $("#topBannerSetForm").serializeObject();
        var division = $("input:radio[name=division]:checked").val();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < division ; i++){

            linkInfo = bannerDetail.linkList[i];
            linkInfo.imgUrl = bannerDetail.imgUrl;

            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {

                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;


                } else {

                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }

            linkList.push(linkInfo);
            linkInfo = new Object();
        }
        bannerDetail.linkList  = linkList;
        return bannerDetail;

    }
};