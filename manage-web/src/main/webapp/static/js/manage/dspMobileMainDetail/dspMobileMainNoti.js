/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > 공지배너
 */
$(document).ready(function() {

    dspMainNotiBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainNotiBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="notiBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="notiBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="notiBannerLinktype"]').trigger('change');
    },

    //링크 타입 초기화 (단 번호별 초기화)
    resetLinkInfo : function(linkTemplateNumber){


        $('.notiBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $('.notiBannerDiv'+linkTemplateNumber).find('.notiBannerTitle').val('').prop('readOnly' , true);;
        $('.notiBannerDiv'+linkTemplateNumber).find('.notiBannerSubTitle').val('').prop('readOnly' , true);;
        $('.notiBannerDiv'+linkTemplateNumber).find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.notiBannerDiv'+linkTemplateNumber).find('.notiBannerTitle').prop('readonly' ,true).val('');
        $('.notiBannerDiv'+linkTemplateNumber).find('.notiBannerSubTitle').prop('readonly' ,true).val('');
        $('.notiBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');


    },

    //링크 타입 전체 초기화
    resetLinkTypeAll : function(){

        $("[class^='notiBannerDiv']").css("display", "none");
        $("[class^='notiBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='notiBannerDiv']").find('.notiBannerTitle').val('');
        $("[class^='notiBannerDiv']").find('.notiBannerSubTitle').val('');
        $("[class^='notiBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='notiBannerDiv']").find('.notiBannerTitle').prop('readonly' ,true).val('');
        $("[class^='notiBannerDiv']").find('.notiBannerSubTitle').prop('readonly' ,true).val('');
        $("[class^='notiBannerDiv']").find('.deleteImgBtn').trigger('click');

        $('.notiBannerDiv1').css("display", "");

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainNotiBannerDetail.resetLinkTypeAll();
        $("#notiBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    setBannerValid : function(){

        var bannerDetail = $("#notiBannerSetForm").serializeObject(true);

        var dispStoreType =  $("#notiBannerSetForm").find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }

        for(var i =0 ; i < 1 ; i++){


            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){

        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART'

        $("#notiBannerSetForm").find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');

        if(dispStoreType == 'PART'){
            $('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $("#notiBannerSetForm").find('#dispStoreCnt').html(data.storeList.length);
        }




        if (!$.jUtil.isEmpty(data.linkList)) {


            for(var i =0 ; i < 1 ; i++){

                var link  = data.linkList[i];
                var templateNum = i+1;

                if(link.textYn == 'Y'){

                    $('.notiBannerDiv' + templateNum).find('.textYn').prop('checked', true);
                    $('.notiBannerDiv' + templateNum).find('.notiBannerTitle').prop('readonly' ,false).val(link.title);
                    $('.notiBannerDiv' + templateNum).find('.notiBannerSubTitle').prop('readonly' ,false).val(link.subTitle);

                } else {

                    $('.notiBannerDiv' + templateNum).find('.textYn').prop('checked', false);
                    $('.notiBannerDiv' + templateNum).find('.notiBannerTitle').prop('readonly' ,true).val('');
                    $('.notiBannerDiv' + templateNum).find('.notiBannerSubTitle').prop('readonly' ,true).val('');

                }

                dspMainCommon.img.setImg($('.notiBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

                });


                $("#notiBannerLinktype" + templateNum ).val(link.linkType).trigger('change');

                dspMainCommon.setLinkInfo(link ,  $("#notiBannerLinkInfo" + templateNum));



            }

        }

    },
    getBannerDetail : function(){
        var bannerDetail = $("#notiBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Object();

        }
        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }

        return bannerDetail;

    }
};