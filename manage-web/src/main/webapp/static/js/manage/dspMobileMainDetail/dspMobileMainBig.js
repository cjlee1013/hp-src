/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > 메인빅배너
 */
$(document).ready(function() {

    dspMainBigBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainBigBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $(document).on('keyup','.bigBannerTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });
        $(document).on('keyup','.bigBannerSubTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });
        $(document).on('keyup','.bigBannerSub2Title',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });
    },

    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="bigBannerLinktype"]').html($("#linkTypeHome").html());
            $("#bigBannerClub").hide();
            $("#mobileBigImgSize").html('750*460');

        } else{
            $('select[id^="bigBannerLinktype"]').html($("#linkTypeClub").html());
            $("#bigBannerClub").show();
            $("#mobileBigImgSize").html('440*440');
        }
        $('select[id^="bigBannerLinktype"]').trigger('change');
    },

    //링크 타입 초기화 (단 번호별 초기화)
    resetLinkInfo : function(linkTemplateNumber){

        $('.bigBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerTitle').val('');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSubTitle').val('');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSub2Title').val('');
        $('.bigBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');
        $('.bigBannerDiv'+linkTemplateNumber).find('.cnt').html('0');

    },

    //링크 타입 전체 초기화
    resetLinkTypeAll : function(){

        $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSub2Title').val('');
        $("[class^='bigBannerDiv']").find('.deleteImgBtn').trigger('click');
        $("[class^='bigBannerDiv']").find('.cnt').html('0');

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainBigBannerDetail.resetLinkTypeAll();
        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    setBannerValid : function(){

        var bannerDetail = $("#bigBannerSetForm").serializeObject(true);

        var dispStoreType = $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }
        for(var i =0 ; i < 1 ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }

        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){

        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART';
        var siteTyep = $('#siteType').val();

        $("#bigBannerSetForm").find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');


        if(dispStoreType == 'PART'){
            $("#bigBannerSetForm").find('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $("#bigBannerSetForm").find('#dispStoreCnt').html(data.storeList.length);
        }


        if (!$.jUtil.isEmpty(data.linkList)) {


            for(var i =0 ; i < 1 ; i++){

                var link  = data.linkList[i];
                var templateNum = i+1;

                if(siteTyep == 'CLUB'){
                    if($.jUtil.isNotEmpty(link.title)){
                        $('.bigBannerTitle').val(link.title);
                        $('.bigDivTitleCnt').html(link.title.length);
                    }
                    if($.jUtil.isNotEmpty(link.subTitle)){
                        $('.bigBannerSubTitle').val(link.subTitle);
                        $('.bigDivSubCnt').html(link.subTitle.length);
                    }
                    if($.jUtil.isNotEmpty(link.subTitle2)){
                        $('.bigBannerSub2Title').val(link.subTitle2);
                        $('.bigDivSub2Cnt').html(link.subTitle2.length);
                    }
                }
                dspMainCommon.img.setImg($('.bigBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl


                });

                $("#bigBannerLinktype" + templateNum ).val(link.linkType).trigger('change');

                dspMainCommon.setLinkInfo(link ,  $("#bigBannerLinkInfo" + templateNum));
            }

        }

    },
    getBannerDetail : function(){
        var bannerDetail = $("#bigBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Object();

        }
        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }

        return bannerDetail;

    }
};