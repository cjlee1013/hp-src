/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > 스플래시
 */
$(document).ready(function() {

    dspMainSplashBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainSplashBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //링크 타입 초기화
    resetLinkInfo : function(){

        $("[class^='splashBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='splashBannerDiv']").find('.deleteImgBtn').trigger('click');

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainSplashBannerDetail.resetLinkInfo();

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="splashBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="splashBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="splashBannerLinktype"]').trigger('change');

    },
    setBannerValid : function(){

        var bannerDetail = $("#splashBannerSetForm").serializeObject();


        for(var i =0 ; i < 1 ; i++){


            if($.jUtil.isEmpty(bannerDetail.linkList[i].imgUrl)){

                alert("이미지를 등록해 주세요." );
                return false;

            }
        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){


        if (!$.jUtil.isEmpty(data.linkList)) {

            for(var i = 0 ; i < data.linkList.length ; i++){


                var link  = data.linkList[i];
                var divisionNum = i+1;

                //이미지
                dspMainCommon.img.setImg($('.splashBannerDiv' + divisionNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

                });



                $("#splashBannerLinktype" + divisionNum).val(link.linkType).trigger('change')
                dspMainCommon.setLinkInfo(link ,  $("#splashBannerLinkInfo" + divisionNum));

            }

        }

    },

    getBannerDetail : function(){
        var bannerDetail = $("#splashBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];
    /*        if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }*/
            linkList.push(linkInfo);
            linkInfo = new Array();
        }
        bannerDetail.linkList  = linkList;
        return bannerDetail;

    }
};