/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > mkt배너
 */
$(document).ready(function() {

    dspMainMktBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainMktBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //링크 타입 초기화
    resetLinkInfo : function(){

        $("[class^='mktBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='mktBannerDiv']").find('.deleteImgBtn').trigger('click');

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainMktBannerDetail.resetLinkInfo();
        $("#mktBannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="mktBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="mktBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="mktBannerLinktype"]').trigger('change');

    },
    setBannerValid : function(){

        var bannerDetail = $("#mktBannerSetForm").serializeObject();

        var dispStoreType = $("#mktBannerSetForm").find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }


        for(var i =0 ; i < 1 ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }
        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){


        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART'

        $("#mktBannerSetForm").find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');


        if(dispStoreType == 'PART'){
            $("#mktBannerSetForm").find('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $("#mktBannerSetForm").find('#dispStoreCnt').html(data.storeList.length);
        }


        if (!$.jUtil.isEmpty(data.linkList)) {

            for(var i = 0 ; i < data.linkList.length ; i++){


                var link  = data.linkList[i];
                var divisionNum = i+1;

                //이미지
                dspMainCommon.img.setImg($('.mktBannerDiv' + divisionNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

                });


                $("#mktBannerLinktype" + divisionNum).val(link.linkType).trigger('change')
                dspMainCommon.setLinkInfo(link ,  $("#mktBannerLinkInfo" + divisionNum));

            }

        }

    },

    getBannerDetail : function(){
        var bannerDetail = $("#mktBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Array();
        }
        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }

        return bannerDetail;

    }
};