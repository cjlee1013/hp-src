/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > 배송배너(HYPER,EXP)
 */
$(document).ready(function() {
    CommonAjaxBlockUI.global();
});

var dspMainDeliveryBannerDetail = {

    changeLinkType : function(){
        $('#deliveryBannerLinktype').html($('#linkTypeHyper').html());
        $('#deliveryBannerLinktype').trigger('change');
    },
    resetInfo : function(){
        $('.deliveryBannerDiv').find('select').val('EXH').trigger('change');
        $('.deliveryBannerDiv').find('.deleteImgBtn').trigger('click');
    },
    resetInfoAll : function(){
        $('.deliveryBannerDiv').find('select').val('EXH').trigger('change');
        $('.deliveryBannerDiv').find('.deleteImgBtn').trigger('click');
        $('.deliveryBannerDiv').css("display", "");
    },
    //전시위치 변경시
    resetForm : function(){
        dspMainDeliveryBannerDetail.resetInfoAll();
        $('#deliveryBannerSetForm').find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');

    },
    setBannerValid : function(){

        var bannerDetail = $('#deliveryBannerSetForm').serializeObject(true);

        var dispStoreType =  $('#deliveryBannerSetForm').find('input:radio[name=dispStoreType]:checked').val();

        if(dispStoreType == 'PART' && $.jUtil.isEmpty(dspMainCommon.storeList)){
            alert("노출 점포를 선택해 주세요");
            return false;
        }

        if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[0])){
            return false;
        }

        return true;
    },
    //row 클릭시 정보 셋팅
    setBannerDetail : function(data){

        var dispStoreType  =  data.dispStoreType == 'ALL' ? 'ALL' : 'PART'

        $('#deliveryBannerSetForm').find('input:radio[name=dispStoreType]:input[value=' +dispStoreType + ']').prop('checked', true).trigger('click');

        if(dispStoreType == 'PART'){
            $('select[name="dispStoreType"]').val(data.dispStoreType);
            dspMainCommon.storeList = data.storeList;
            $('#deliveryBannerSetForm').find('#dispStoreCnt').html(data.storeList.length);
        }

        if (!$.jUtil.isEmpty(data.linkList)) {

            var link  = data.linkList[0];

            dspMainCommon.img.setImg($('.deliveryBannerDiv').find('.itemImg'), {
                'imgUrl': link.imgUrl,
                'imgHeight': link.imgHeight,
                'imgWidth': link.imgWidth,
                'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

            });

            $('#deliveryBannerLinktype').val(link.linkType).trigger('change');

            dspMainCommon.setLinkInfo(link ,  $('#deliveryBannerLinkInfo'));
        }

    },
    getBannerDetail : function(){
        var bannerDetail = $('#deliveryBannerSetForm').serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        linkInfo = bannerDetail.linkList[0];
        if(linkInfo.linkType == 'ITEM_TD'){
            //링크 옵션 설정
            if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

            } else {
                linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
            }

        }
        linkList.push(linkInfo);

        bannerDetail.linkList  = linkList;
        if(!$.jUtil.isEmpty(dspMainCommon.storeList)){

            bannerDetail.storeList = dspMainCommon.storeList;
        }
        return bannerDetail;

    }
};