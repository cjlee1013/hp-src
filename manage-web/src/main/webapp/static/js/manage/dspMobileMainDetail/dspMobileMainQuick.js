/**
 * 사이트관리 > 전시관리 > 모바일메인관리 > quick배너
 */
$(document).ready(function() {

    dspMainQuickBannerDetail.init();
    CommonAjaxBlockUI.global();

});

var dspMainQuickBannerDetail = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

    },

    //링크 타입 초기화
    resetLinkInfo : function(){

        $("[class^='quickBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='quickBannerDiv']").find('.deleteImgBtn').trigger('click');

    },
    //배너 입력 폼 초기화
    resetForm : function(){
        dspMainQuickBannerDetail.resetLinkInfo();

    },
    //사이트 구분에 따른 링크 타입 변경
    changeLinkType : function(){
        var siteType = $('#siteType').val();

        if(siteType == 'HOME'){

            $('select[id^="quickBannerLinktype"]').html($("#linkTypeHome").html());

        } else{
            $('select[id^="quickBannerLinktype"]').html($("#linkTypeClub").html());
        }
        $('select[id^="quickBannerLinktype"]').trigger('change');

    },
    setBannerValid : function(){

        var bannerDetail = $("#quickBannerSetForm").serializeObject();


        for(var i =0 ; i < 1 ; i++){

            if(!dspMainCommon.setCommonLinkValid(bannerDetail.linkList[i])){
                return false;
            }

        }


        return true;

    },
    //배너 상세 정보 입력
    setBannerDetail : function(data){


        if (!$.jUtil.isEmpty(data.linkList)) {

            for(var i = 0 ; i < data.linkList.length ; i++){


                var link  = data.linkList[i];
                var divisionNum = i+1;

                //이미지
                dspMainCommon.img.setImg($('.quickBannerDiv' + divisionNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'imgHeight': link.imgHeight,
                    'imgWidth': link.imgWidth,
                    'src'    : hmpImgUrl + "/provide/view?processKey=" + dspMainCommon.img.getProcessKey() + "&fileId=" + link.imgUrl

                });


                $("#quickBannerLinktype" + divisionNum).val(link.linkType).trigger('change')
                dspMainCommon.setLinkInfo(link ,  $("#quickBannerLinkInfo" + divisionNum));

            }

        }

    },

    getBannerDetail : function(){
        var bannerDetail = $("#quickBannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();

        for(var i =0 ; i < 1 ; i++){

            linkInfo = bannerDetail.linkList[i];
            if(linkInfo.linkType == 'ITEM_TD'){
                //링크 옵션 설정
                if (!$.jUtil.isEmpty(linkInfo.dlvStoreYn) && linkInfo.dlvStoreYn == 'Y' ) {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'D' + linkInfo.dlvStoreId;

                } else {
                    linkInfo.linkOptions  = linkInfo.storeType.substring( 0, 1 ) + 'N';
                }

            }
            linkList.push(linkInfo);
            linkInfo = new Array();
        }
        bannerDetail.linkList  = linkList;
        return bannerDetail;

    }
};