/**
 * 사이트관리 > 공지사항 관리
 */
$(document).ready(function() {
    noticeListGrid.init();
    noticePartnerListGrid.init();
    notice.init();
    noticePartner.init();
    CommonAjaxBlockUI.global();
    commonEditor.initEditor('#noticeDescEdit', 300, 'ItemDetail');
    commonEditor.hmpImgUrl = hmpImgUrl;

});
var commonEditor;


// 공지사항 파트나 리스트 그리드
var noticePartnerListGrid ={

    gridView : new RealGridJS.GridView("noticePartnerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        noticePartnerListGrid.initGrid();
        noticePartnerListGrid.initDataProvider();
        $('#partnerList').hide();
    },
    initGrid : function() {

        noticePartnerListGrid.gridView.setDataSource(noticePartnerListGrid.dataProvider);
        noticePartnerListGrid.gridView.setStyles(noticePartnerListGridBaseInfo.realgrid.styles);
        noticePartnerListGrid.gridView.setDisplayOptions(noticePartnerListGridBaseInfo.realgrid.displayOptions);
        noticePartnerListGrid.gridView.setColumns(noticePartnerListGridBaseInfo.realgrid.columns);
        noticePartnerListGrid.gridView.setOptions(noticePartnerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        noticePartnerListGrid.dataProvider.setFields(noticePartnerListGridBaseInfo.dataProvider.fields);
        noticePartnerListGrid.dataProvider.setOptions(noticePartnerListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        noticePartnerListGrid.dataProvider.clearRows();
        noticePartnerListGrid.dataProvider.setRows(dataList);
    },
    addData : function (dataList) {
        noticePartnerListGrid.dataProvider.addRows(dataList);
    },
    delData : function(dataList){
        noticePartnerListGrid.dataProvider.removeRows(dataList,false);
    },
    addDataList : function (dataList) {
        noticePartnerListGrid.dataProvider.addRows(dataList);
    },


};
// 공지사항 그리드
var noticeListGrid = {

    gridView : new RealGridJS.GridView("noticeListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        noticeListGrid.initGrid();
        noticeListGrid.initDataProvider();
        noticeListGrid.event();

    },
    initGrid : function() {

        noticeListGrid.gridView.setDataSource(noticeListGrid.dataProvider);
        noticeListGrid.gridView.setStyles(noticeListGridBaseInfo.realgrid.styles);
        noticeListGrid.gridView.setDisplayOptions(noticeListGridBaseInfo.realgrid.displayOptions);
        noticeListGrid.gridView.setColumns(noticeListGridBaseInfo.realgrid.columns);
        noticeListGrid.gridView.setOptions(noticeListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        noticeListGrid.dataProvider.setFields(noticeListGridBaseInfo.dataProvider.fields);
        noticeListGrid.dataProvider.setOptions(noticeListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        noticeListGrid.gridView.onDataCellClicked = function(gridView, index) {
            notice.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        noticeListGrid.dataProvider.clearRows();
        noticeListGrid.dataProvider.setRows(dataList);

    },

};
//개별공지사항 관리
var noticePartner ={
    init : function() {
        this.bindingEvent();

    },
    bindingEvent : function() {
        $('#deletePartner').bindClick(noticePartner.deleteCheckedPartner);
        $('#addPartner').bindClick(noticePartner.addPartnerPopUp);
    },
    /**
     * 개별 공지 사항 파트너 리스트 검색
     */
    searchPartner : function(noticeNo) {

        CommonAjax.basic({url:'/manage/notice/getNoticePartnerList.json?', data:{noticeNo: noticeNo}, method:"GET", successMsg:null, callbackFunc:function(res) {
                noticePartnerListGrid.setData(res);
            }});
    },
    /**
     * 개별 공지 사항 파트너 등록(중복된 데이터가 있으면 기 등록되어 있던 데이터 삭제)
     */
    addPartner : function(partnerList){

        var gridList = noticePartnerListGrid.dataProvider.getJsonRows();
        var delList = new Array();

        partnerList.forEach(function (item) {
            gridList.forEach(function (partner,idx) {

                if(item.partnerId == partner.partnerId){
                    gridList.splice(idx, 1)
                }

            });
        });
        // 기 등록 파트너  + 신규 파트너 리스트 - 중복 데이터
        var partnerLength = gridList.length + partnerList.length;

        if(partnerLength < 1){
            alert("개별공지는 1명 이상의 판매자 등록 후 이용 가능합니다.");
            return false;
        }


        noticePartnerListGrid.dataProvider.clearRows();
        noticePartnerListGrid.setData(partnerList);
        noticePartnerListGrid.addDataList(gridList);
/*


        if(delList.length >0){
            noticePartner.deletePartner(delList);
        }
        noticePartnerListGrid.addData(partnerList);*/
        return true;
    },

    /**
     * 개별 공지 사항 파트너 등록 팝업 호출
     */
    addPartnerPopUp : function(){

        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback=noticePartner.addPartner"
            + "&partnerType="
            + "SELL"
            + "&isMulti=Y"
            , "partnerPop", "1080", "600", "yes", "no");

    },
    /**
     * 개별 공지 사항 선택된 파트너 삭제
     */
    deleteCheckedPartner : function(){

        var checkedVal = noticePartnerListGrid.gridView.getCheckedRows(true);

        if (checkedVal.length == 0) {
            alert("선택한 항목이 없습니다.");
            return;
        }
        noticePartner.deletePartner(checkedVal);

    },
    deletePartner : function (delPartnerList) {
        noticePartnerListGrid.delData(delPartnerList);
    }
};
// 공지사항관리
var notice = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');
    },
    initSearchDate : function (flag) {
        notice.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        notice.setDate.setEndDate(0);
        notice.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(notice.search);
        $('#setNoticeBtn').bindClick(notice.setnotice);
        $('#searchResetBtn').bindClick(notice.searchFormReset);
        $('#resetBtn').bindClick(notice.resetForm);
        $('#noticeTitle').calcTextLength('keyup', '#textCountNoticeTitle');
        $("#searchDispArea").bindChange(notice.setSearchDispArea);
        $("#dispArea").bindChange(notice.setDispArea);
        /*$("#addFile").bindClick(notice.setFile);*/

        $("#noticeKind").change(function(){

            if( $(this).val() =="PRIVATE" ){
                $('#partnerList').show();
            }else{
                $('#partnerList').hide();
            }
        });

        $('#noticeSetForm').on('propertychange change keyup paste input', '#fr-image-by-url-layer-text-1', function(e){

            commonEditor.inertImgurl = $('#fr-image-by-url-layer-text-1').val();
        });

        $("#noticeSetForm :checkbox").change(function(){
            if($(this).prop("checked"))  {
                $(this).val('Y');
            }else {
                $(this).val('N');
            }
        });
        //파일선택시
        $('input[name="fileArr"]').change(function() {
            notice.file.addFile();
        });
        //파일삭제 이벤트
        $('[id^=fileList]').on('click', '.close', function() {
            $(this).closest('.file').remove();
        });
    },


    /**
     * 등록/수정입력란의 노출영역에 따라 공지 구분 값 및 노출사이트  동적 설정
     */
    setDispArea : function(){

        var dispArea = $("#dispArea").val();
        var objectSelect = new Array();       //셀렉트박스 -> disabled 여부, 공통코드명 , defaultName  , defaultCode , selectedCode
        var objectCheckBox = new Array();     //체크박스 -> disabled 여부, checked 여부 , value
        switch (dispArea) {
            case "FRONT":
                objectSelect =[false,'notice_class_front','선택','',''];
                objectCheckBox =[ false, true , 'Y' ];
                break;
            case "ADMIN":
                objectSelect =[false,'notice_class_admin','선택','',''];
                objectCheckBox =[ true, false , 'N' ];
                break;
            case "PARTNER":
                objectSelect =[false,'notice_class_partner','선택','',''];
                objectCheckBox =[ true, false , 'N' ];
                break;
            default :
                objectSelect =[true,'','선택','',''];
                objectCheckBox =[ true, false , 'N' ];
        }
        notice.setObjSelectOption($('#noticeKind'),objectSelect[0],objectSelect[1],objectSelect[2],objectSelect[3],objectSelect[4]);
        notice.setObjCheckBoxOption($("#noticeSetForm :checkbox") , objectCheckBox[0], objectCheckBox[1], objectCheckBox[2]);

    },
    /**
     * 검색 영역의 노출영역에 따라 공지 구분 값 및 노출사이트  동적 설정
     */
    setSearchDispArea : function(){

        var dispArea = $("#searchDispArea").val();
        var objSelectClass = new Array();   // 공지사항 구분
        var objSelectDisp  = new Array();      //공지사항 노출 사이트
        switch (dispArea) {
            //disabled 여부, 공통코드명 , defaultName  , defaultCode , selectedCode
            case "FRONT":
                objSelectClass=[false,'notice_class_front','전체','',''];
                objSelectDisp=[false,'notice_disp_site','전체','',''];
                break;
            case "ADMIN":
                objSelectClass=[false,'notice_class_admin','전체','',''];
                objSelectDisp=[true,'','전체','',''];
                break;
            case "PARTNER":
                objSelectClass=[false,'notice_class_partner','전체','','' ];
                objSelectDisp=[true,'','전체','','' ];
                break;
            default :
                objSelectClass=[true,'','전체','',''];
                objSelectDisp=[true,'','전체','',''];
        }
        //id , disabled , 공통코드명 , defaultName  , defaultCode , selectedCode
        notice.setObjSelectOption($('#searchClassCd'),objSelectClass[0],objSelectClass[1],objSelectClass[2],objSelectClass[3],objSelectClass[4]);
        notice.setObjSelectOption($('#searchDispSite'),objSelectDisp[0],objSelectDisp[1],objSelectDisp[2],objSelectDisp[3],objSelectDisp[4]);

    },
    /**
     * 공지사항 검색 check box 값 설정
     */
    setObjCheckBoxOption : function(obj , disabled , checked , val){

        obj.prop( "disabled", disabled );
        obj.prop( "checked", checked );
        obj.val(val);
    },
    /**
     * 공지사항 검색 select box 값 설정
     */
    setObjSelectOption : function(obj, disabled , code , defaultName , defaultCode,selectedCode){

        obj.empty();

        obj.prop( "disabled", disabled );

        if(defaultName !='' && defaultName !=  null ){
            obj.append( "<option value="+ defaultCode +">" + defaultName + "</option>");
        }
        if(code != null && code != ''){

            CommonAjax.basic({
                url: "/common/code/getCodeList.json",
                data: {
                    typeCode: code
                },
                method: "GET",
                callbackFunc: function (res) {
                    var resultCount = res.length;
                    var option ="";
                    for (var idx = 0; resultCount > idx; idx++) {
                        var data = res[idx];
                        if(selectedCode ==  data.mcCd){
                            option += "<option value="+ data.mcCd +" selected >" + data.mcNm + "</option>";
                        }else{
                            option += "<option value="+ data.mcCd +">" + data.mcNm + "</option>";
                        }

                    }

                    obj.append(option);

                }
            });
        }
    },
    /**
     * 공지사항 검색
     */
    search : function() {

        var searchKeyword = $('#searchKeyword').val();
        var searchKeywordLength = $('#searchKeyword').val().length;
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            notice.initSearchDate('-7d');
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 6) {
            alert("6개월 이내만 검색 가능합니다.");
            startDt.val(moment(endDt.val()).add(-6, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if(searchKeywordLength < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#searchKeyword').focus();
                return false;
            }

        }

        CommonAjax.basic({url:'/manage/notice/getNoticeList.json?' + $('#noticeSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                notice.resetForm();
                noticeListGrid.setData(res);
                $('#noticeTotalCount').html($.jUtil.comma(noticeListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 첨부파일리스트조회
     */
    getNoticeFileList : function(noticeNo) {
        CommonAjax.basic({url:'/manage/notice/getNoticeFileList.json?noticeNo=' + noticeNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                for (let i in res) {
                    if (res[i].useYn == 'Y') {
                        notice.file.appendFile(res[i]);
                    }
                    notice.file.appendFileSeq(res[i]);
                }
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        $('#searchPeriodType').val('REGDT');
        notice.initSearchDate('-7d');
        $("#searchDispArea").val('');
        notice.setObjSelectOption($('#searchClassCd'),true,'','전체','');
        notice.setObjSelectOption($('#searchDispSite'),true,'','전체','');
        $('#searchUseYn').val('');
        $('#searchTopYn').val('');
        $('#searchType').val('TITLE');
        $('#searchKeyword').val('');

    },

    /**
     * 공지사항 등록/수정
     */
    setnotice : function() {

        //유효성 체크가 필요한 오브젝트 배열 생성
        var objectArray = [$('#dispArea') ,$('#noticeKind') ,$('#useYn') , $('#topYn') ,$('#noticeTitle')];

        //에디터 영역 값 설정
        $('#noticeDesc').val( commonEditor.getHtmlData('#noticeDescEdit'));
        objectArray.push( $('#noticeDesc'));

       if($.jUtil.isEmptyObjectArray(objectArray)){
            alert("필수 입력 항목을 선택/입력 해주세요." );
            return false;
       }

       //노출 영역이 프론트일 경우 노출 사이트 1개 선택 필요
       if( $('#dispArea').val() =='FRONT'){
            if( $( '#dispHomeYn' ).val()=='N'   && $( '#dispClubYn' ).val()=='N' ){
                alert("노출사이트는 반드시 1개는 선택해야 합니다.");
                return false;
            }

        }

        var setNotie = $('#noticeSetForm').serializeObject(false);

        if(setNotie.dispArea=='PARTNER' && setNotie.noticeKind == 'PRIVATE'){
            var partnerList = noticePartnerListGrid.dataProvider.getJsonRows();
            if(partnerList.length < 1){
                alert("개별공지는 1명 이상의 판매자 등록 후 이용 가능합니다.");
                return false;
            }
            setNotie.partnerList = partnerList;
        }

        CommonAjax.basic({
            url : '/manage/notice/setNotice.json'
            , data : JSON.stringify(setNotie)
            , method : "POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                notice.resetForm();
                notice.setNoticeData(setNotie);
            }
        });
    },
    /**
     * 대체불가상품 등록/수정 후 데이터 갱신
     */
    setNoticeData : function(notice) {
        var options = {
            fields: ['noticeNo'],
            values: [notice.noticeNo]
        };

        var itemIndex = noticeListGrid.gridView.searchItem(options);

        // 신규 등록 시 오류를 방지하기 위함. ( 그리드에 찾는 데이터가 없을 경우 -1 )
        if(itemIndex != -1) {
            var dispAreaTxt = '';
            switch (notice.dispArea) {
                case 'FRONT' :
                    dispAreaTxt = 'front';
                    break;
                case 'ADMIN' :
                    dispAreaTxt = 'Admin';
                    break;
                case 'PARTNER' :
                    dispAreaTxt = '파트너';
                    break;
            }

            var noticeKindTxt = '';
            switch (notice.noticeKind) {
                case 'GENERAL' :
                    noticeKindTxt = '일반';
                    break;
                case 'MANUAL' :
                    noticeKindTxt = '매뉴얼';
                    break;
                case 'PRIVATE' :
                    noticeKindTxt = '개별공지';
                    break;
                case 'WINNER' :
                    noticeKindTxt = '당첨자발표';
                    break;
                case 'EVENT' :
                    noticeKindTxt = '이벤트';
                    break;
            }

            var topYnTxt = 'N';
            if(notice.topYn === 'Y') {
                topYnTxt = 'Y';
            }

            var useYnTxt = '사용안함';
            if(notice.useYn === 'Y') {
                useYnTxt = '사용';
            }

            var dispTxt = '';
            if(notice.dispHomeYn === 'Y' && notice.dispClubYn === 'Y') {
                dispTxt = '홈플러스/더클럽';
            } else if(notice.dispClubYn === 'Y') {
                dispTxt = '더클럽';
            } else if(notice.dispHomeYn === 'Y') {
                dispTxt = '홈플러스';
            }

            // RealGrid 값 변경.
            var setGridValue = {
                useYn : notice.useYn
                , useYnTxt : useYnTxt
                , dispArea : notice.dispArea
                , dispAreaTxt : dispAreaTxt
                , noticeKind : notice.noticeKind
                , noticeKindTxt : noticeKindTxt
                , noticeTitle : notice.noticeTitle
                , topYn : notice.topYn
                , topYnTxt : topYnTxt
                , dispTxt : dispTxt
                , dispHomeYn : notice.dispHomeYn
                , dispClubYn : notice.dispClubYn
            };

            noticeListGrid.gridView.setValues(itemIndex, setGridValue, true);
        }
    },
    /**
     * 공지사항 입력/수정 폼 초기화
     */
    resetForm : function() {
        $('#noticeNo, #noticeTitle, #noticeDesc, #dispArea').val('');
        $('#useYn').val('Y');
        $('#topYn').val('N');
        notice.setObjSelectOption($('#noticeKind'),true,'','선택','','');
        notice.setObjCheckBoxOption($("#noticeSetForm :checkbox") , true, false, 'N');
        $('#textCountNoticeTitle').html('0');
        commonEditor.setHtml('#noticeDescEdit' ,'');
        $('#dispArea').prop( "disabled", false );
        $('#partnerList').hide();
        noticePartnerListGrid.dataProvider.clearRows();
        $("#fileList").html('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        notice.resetForm();

        var rowDataJson = noticeListGrid.dataProvider.getJsonRow(selectRowId);

        //노티스별 파일 리스트
        notice.getNoticeFileList(rowDataJson.noticeNo);

        CommonAjax.basic({
            url : '/manage/notice/getNotice.json?noticeNo=' + rowDataJson.noticeNo
            , data : null
            , method : "GET"
            , successMsg : null
            , callbackFunc : function(res) {
                $('#noticeNo').val(res.noticeNo);
                $('#dispArea').val(res.dispArea);

                noticePartnerListGrid.dataProvider.clearRows();

                if(res.dispArea=='FRONT') {
                    $('#dispArea').prop( "disabled", false );
                    $('#partnerList').hide();

                    notice.setObjSelectOption($('#noticeKind'), false, 'notice_class_front', '선택', '', res.noticeKind);

                    if(res.dispHomeYn == 'Y') {
                        notice.setObjCheckBoxOption($("#dispHomeYn"), false, true, 'Y');
                    } else {
                        notice.setObjCheckBoxOption($("#dispHomeYn"), false, false, 'N');
                    }
                    if(res.dispClubYn =='Y') {
                        notice.setObjCheckBoxOption($("#dispClubYn"), false, true, 'Y');
                    } else {
                        notice.setObjCheckBoxOption($("#dispClubYn"), false, false, 'N');
                    }
                } else if(res.dispArea == 'ADMIN') {
                    $('#dispArea').prop( "disabled", false );
                    $('#partnerList').hide();
                    notice.setObjSelectOption($('#noticeKind'), false, 'notice_class_admin', '선택', '', res.noticeKind);
                    notice.setObjCheckBoxOption($("#noticeSetForm :checkbox") , true, false, 'N');
                } else if(res.dispArea =='PARTNER') {

                    if(res.noticeKind == 'PRIVATE') {
                        $('#dispArea').val(res.dispArea)
                        $('#dispArea').prop( "disabled", true );
                        notice.setObjSelectOption($('#noticeKind'), true, '', res.noticeKindTxt, res.noticeKind, '');
                        $('#partnerList').show();
                        noticePartner.searchPartner(res.noticeNo);
                    } else {
                        $('#dispArea').prop( "disabled", false );
                        $('#partnerList').hide();
                        notice.setObjSelectOption($('#noticeKind'), false, 'notice_class_partner', '선택', '', res.noticeKind);
                    }

                    notice.setObjCheckBoxOption($("#noticeSetForm :checkbox"), true, false, 'N');
                }

                $('#useYn').val(res.useYn);
                $('#topYn').val(res.topYn);

                $('#noticeTitle').val(res.noticeTitle);

                commonEditor.setHtml('#noticeDescEdit' ,res.noticeDesc);

                notice.setNoticeData(res);
            }});
    }
};
/**
 * 파트너공지 팝업 이미지
 */
notice.file = {
    /**
     * 파일 업로드 클릭 이벤트
     * @param obj
     */
    clickFile : function(obj) {
        var fileCnt = $("#fileList > li").length;
        if(fileCnt<2){
            $('input[name=fileArr]').click();
        } else{
            alert("파일은 최대 2개까지 첨부 가능합니다.");
            return ;
        }
    },
    /**
     * 파일엄로드
     */
    addFile : function() {
        if (notice.file.valid()) {
            var file = $('#itemFile');
            var params = {
                processKey : "ItemFile",
                mode : "FILE"
            };

            CommonAjax.upload({
                file: file,
                data: params,
                callbackFunc: function (resData) {
                    var errorMsg = "";
                    for (var i in resData.fileArr) {

                        var f = resData.fileArr[i];

                        if (f.hasOwnProperty("error")) {
                            errorMsg += f.error;
                        } else {

                            notice.file.appendFile(f.fileStoreInfo);
                        }
                    }
                    if (!$.jUtil.isEmpty(errorMsg)) {
                        alert(errorMsg);
                    }
                }
            });
        }
    },
    /**
     * 파일 업로드 유효성 검사
     * @returns {boolean}
     */
    valid : function() {


        return true;
    },
    appendFile : function(f){

        var html =  '<li class ="file"><span class="ui span-button text mg-r-10">'
            + '<input type="hidden" class="fileUrl" name="fileList[].fileId" value="'+f.fileId+'"/>'
            + '<input type="hidden" class="fileNm" name="fileList[].fileName" value="'+f.fileName+'"/>'
            + '<a href="'
            + hmpImgUrl + "/provide/view?processKey=ItemFile&fileId=" + f.fileId
            + '" target="_blank" style="color:#ff685b; text-decoration: underline; ">'
            + f.fileName
            + '</a><span class="text close mg-l-5">[X]</span></span></li>';

        $("#fileList").append(html);



    },
    appendFileSeq : function(f){

        var html =  '<input type="hidden" name="fileSeq[].fileSeq" value="'+f.fileSeq+'"/>'
        $("#fileList").append(html);



    }
};