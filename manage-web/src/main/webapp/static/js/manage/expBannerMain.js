/**
 * 사이트관리 > 전시관리 > 배너관리
 */
$(document).ready(function() {
    expBannerListGrid.init();
    expBanner.init();
    expBannerCommon.init();
    CommonAjaxBlockUI.global();
});

// banner 그리드
var expBannerListGrid = {
    gridView : new RealGridJS.GridView("expBannerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        expBannerListGrid.initGrid();
        expBannerListGrid.initDataProvider();
        expBannerListGrid.event();
    },
    initGrid : function() {
        expBannerListGrid.gridView.setDataSource(expBannerListGrid.dataProvider);
        expBannerListGrid.gridView.setStyles(expBannerListGridBaseInfo.realgrid.styles);
        expBannerListGrid.gridView.setDisplayOptions(expBannerListGridBaseInfo.realgrid.displayOptions);
        expBannerListGrid.gridView.setColumns(expBannerListGridBaseInfo.realgrid.columns);
        expBannerListGrid.gridView.setOptions(expBannerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        expBannerListGrid.dataProvider.setFields(expBannerListGridBaseInfo.dataProvider.fields);
        expBannerListGrid.dataProvider.setOptions(expBannerListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        expBannerListGrid.gridView.onDataCellClicked = function(gridView, index) {
            expBanner.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        expBannerListGrid.dataProvider.clearRows();
        expBannerListGrid.dataProvider.setRows(dataList);
    },
};

//배너 검색, 초기화
//배너 등록
//전체 초기화
//그리드 선택
var expBanner = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
    },
    initSearchDate : function (flag) {
        expBanner.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        expBanner.setDate.setEndDate(0);
        expBanner.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(expBanner.search);
        $('#searchResetBtn').bindClick(expBanner.searchFormReset);
        $('#setPcMainBtn').bindClick(expBanner.setBanner);
        $('#resetBtn').bindClick(expBannerCommon.resetBannerForm);
        $('#bannerNm').calcTextLength('keyup', '#bannerNmCnt');

        $('#template').bindChange(expBannerCommon.changeTemplate);
        $('#loc').bindChange(expBanner.setCategoryData, 'set');

        $('#device').bindChange(expBanner.switchTextBox);

        $('.textYn').change(function() {
            expBannerCommon.setText(this);
        });

        $(".linkeType").each(function() {
            $(this).find("option[value='GNB']").hide();
        });
    },
    /**
     * 전시위치 > 카테고리 정보
     */
    switchTextBox : function() {
        var loc = $('#loc').val();
        if(!$.jUtil.isEmpty(loc)) {
            var device = $('#device').val();
            $("#template").val('1').trigger('change');
            $('#bannerTr').hide();

            $("#bannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
            $('#dispStoreCnt').html('0');

            expBannerCommon.storeList = null;

            if (device === 'MOBILE') {
                $('.textYn').each(function () {
                    $(this).val('N').attr('disabled', true);
                    $(this).parent().parent().find('.bigBannerTitle').val('').prop('readOnly', true);
                    $(this).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly', true);
                });
                $(".linkeType").each(function () {
                    $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
                });
            } else {
                if(loc === 'UBB') {
                    $('#bannerTr').show();
                }
                $('.textYn').each(function () {
                    $(this).val('N').attr('disabled', false).prop('checked', false);
                    $(this).parent().parent().find('.bigBannerTitle').val('').prop('readOnly', true);
                    $(this).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly', true);
                });
                $(".linkeType").each(function () {
                    $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
                });
            }

            expBanner.changeImgSizeDescription();
        }
    },
    /**
     * 전시위치 > 카테고리 정보
     */
    setCategoryData : function(type) {
        if(type === 'search') {

        } else {
            var locValue = $('#loc').val();
            $('#priority').val('');
            $('#displayStore').hide();

            switch (locValue) {
                case 'UBB' :
                    $('#device').val('PC').trigger('change');
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').show();
                    break;
                case 'MLB' :
                    $('#device').val('PC').trigger('change');
                    $('#displayStore').show();
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    break;
                case 'LLB':
                    $('#device').val('PC').trigger('change');
                    $('#bannerTr, #bannerTdTxt1, #bannerTdTxt2').hide();
                    break;
                default :
                    expBannerCommon.resetBannerForm();
                    break;
            }

            expBanner.changeImgSizeDescription();
        }
    },
    changeImgSizeDescription : function (locValue) {
        var device = $('#device').val();
        var loc = $('#loc').val();
        var template = $('#template').val();
        var templateDetail = $('#templateDetail').val();

        switch (device) {
            case 'PC' :
                if(loc === 'UBB') {
                    switch (template) {
                        case "1"  :
                            $("#bannerDiv1ImgSize").html('1200*450');
                            break;
                        case "2" :
                            switch (templateDetail) {
                                case "N" :
                                    $("#bannerDiv1ImgSize").html('590*450');
                                    $("#bannerDiv2ImgSize").html('590*450');
                                    break;
                                case "LP" :
                                    $("#bannerDiv1ImgSize").html('790*450');
                                    $("#bannerDiv2ImgSize").html('390*450');
                                    break;
                            }
                            break;
                        case "3" :
                            switch (templateDetail) {
                                case "N" :
                                    $("#bannerDiv1ImgSize").html('390*450');
                                    $("#bannerDiv2ImgSize").html('390*450');
                                    $("#bannerDiv3ImgSize").html('390*450');
                                    break;
                                case "LP" :
                                    $("#bannerDiv1ImgSize").html('790*450');
                                    $("#bannerDiv2ImgSize").html('390*220');
                                    $("#bannerDiv3ImgSize").html('390*220');
                                    break;
                                case "CP" :
                                    $("#bannerDiv1ImgSize").html('590*450');
                                    $("#bannerDiv2ImgSize").html('290*450');
                                    $("#bannerDiv3ImgSize").html('290*450');
                                    break;
                            }
                            break;
                        case "5" :
                            $("#bannerDiv1ImgSize").html('390*220');
                            $("#bannerDiv2ImgSize").html('390*220');
                            $("#bannerDiv3ImgSize").html('390*450');
                            $("#bannerDiv4ImgSize").html('390*220');
                            $("#bannerDiv5ImgSize").html('390*220');
                            break;
                        case "6" :
                            $("#bannerDiv1ImgSize").html('390*220');
                            $("#bannerDiv2ImgSize").html('390*220');
                            $("#bannerDiv3ImgSize").html('390*220');
                            $("#bannerDiv4ImgSize").html('390*220');
                            $("#bannerDiv5ImgSize").html('390*220');
                            $("#bannerDiv6ImgSize").html('390*220');
                            break;
                    }
                } else {
                    $("#bannerDiv1ImgSize").html('1200*130');
                    break;
                }
                break;
            case 'MOBILE' :
                if(loc === 'UBB') {
                    $("#bannerDiv1ImgSize").html('670*400');
                } else {
                    $("#bannerDiv1ImgSize").html('750*180');
                }
                break;
            default :
                $("#bannerDiv1ImgSize").html('750*460');
                break;
        }
    },
    /**
     * 검색
     */
    search : function() {
        CommonAjax.basic({
            url : '/manage/expBanner/getExpBannerList.json'
            , data : $('#expBannerSearchForm').serialize()
            , method : "GET"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                expBannerListGrid.setData(res);
                $('#expBannerTotalCount').html($.jUtil.comma(expBannerListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        $('#searchPeriodType').val('regDt');
        expBanner.initSearchDate('-30d');
        $('#searchSiteType, #searchDispYn, #searchDevice, #searchLoc').val('');
        $('#searchType').val('bannerNm');
        $('#searchKeyword').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = expBannerListGrid.dataProvider.getJsonRow(selectRowId);
        var loc = rowDataJson.loc;

        $('#bannerNo').val(rowDataJson.bannerNo).text(rowDataJson.bannerNo);
        $('#loc').val(loc).trigger('change');
        $('#dispYn').val(rowDataJson.dispYn);
        $('#priority').val(rowDataJson.priority);
        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);
        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNmCnt').html(rowDataJson.bannerNm.length);
        $('#device').val(rowDataJson.device);
        expBanner.switchTextBox();

        CommonAjax.basic({url:'/manage/expBanner/getExpBannerLinkList.json?bannerNo=' + rowDataJson.bannerNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                expBannerCommon.setBannerDetail(res, rowDataJson.template, rowDataJson.templateDetail);
            }});

        if(rowDataJson.loc === 'MLB') {
            CommonAjax.basic({
                url: '/manage/expBanner/getExpBannerStoreList.json?bannerNo='
                    + rowDataJson.bannerNo,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    if(!$.jUtil.isEmpty(res)) {
                        $("#bannerSetForm").find('input:radio[name=dispStoreType]:input[value=PART]').prop('checked', true).trigger('click');
                        expBannerCommon.storeList = res;
                        $("#bannerSetForm").find('#dispStoreCnt').html(res.length);
                    } else {
                        $("#bannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
                    }
                }
            });
        }
    },

    /**
     * DSP PC 메인 등록/수정
     */
    setBanner : function() {
        if(!expBannerCommon.setBannerValid()){
            return false;
        }

        var bannerDetail = $("#bannerSetForm").serializeObject();
        var linkInfo = new Object();
        var linkList = new Array();
        var storeList = new Array();

        for(var i =0 ; i < parseInt($("#template").val()) ; i++){
            linkInfo = bannerDetail.linkList[i];

            // 링크 옵션이 점포 상품일 경우 store_type "EXP" / store_kind "N"
            if(linkInfo.linkType == 'ITEM_TD') {
                linkInfo.linkOptions  = 'EN';
            }

            if ($('input:checkbox').eq(i).is(":checked") == true) {
                linkInfo.textYn = "Y";
            } else {
                linkInfo.textYn = "N";
            }

            linkList.push(linkInfo);
            linkInfo = new Object();
        }
        bannerDetail.linkList = linkList;

        if(!$.jUtil.isEmpty(expBannerCommon.storeList)) {
            storeList = expBannerCommon.storeList;
        }
        bannerDetail.storeList = storeList;

        CommonAjax.basic({
            url:'/manage/expBanner/setExpBanner.json',
            data : JSON.stringify(bannerDetail)
            , method:"POST"
            , successMsg : null
            , contentType : "application/json"
            , callbackFunc : function(res) {
                alert(res.returnMsg);
                expBannerCommon.resetForm();
                expBannerCommon.resetBannerForm();
                // expBanner.searchFormReset();
                expBanner.search();
            }
        });
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        expBanner.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        expBanner.setDateTime.setEndDateByTimeStamp(flag);
        expBanner.setDateTime.setStartDate(0);

        // $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
};

// 배너기본 정보 영역
// 링크 정보 입력 영역 (링크 정보 초기화는 각 배너 타입 js파일에 구현)
// 이마지 등록 삭제
var expBannerCommon = {

    linkInfoDiv : null,
    storeList : null,
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        expBannerCommon.img.init();
        expBanner.initDateTime('7d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        expBannerCommon.linkInfoDiv = null;
        expBannerCommon.storeList = null;
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        /*****************************노출 점포 관려 이벤트 등록*******************************************/
        $(document).on('change','input[name="fileArr"]',function(){
            expBannerCommon.img.addImg();
        });

        //이미지 등록 버튼
        $(document).on('click','.addImgBtn',function(){
            expBannerCommon.img.imgDiv = $(this).parent().prev().children('div');
            $('input[name=fileArr]').click();
        });

        //이미지 삭제 버튼
        $(document).on('click','.deleteImgBtn',function(){
            expBannerCommon.img.setImg($(this).next(), null, true);
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('click','input:radio[name="dispStoreType"]',function(){
            expBannerCommon.setDispStoreType($(this));
        });

        //노출 점포 선택 라디오 버튼 (전체노출 : ALL , 노출 점포 :  PART)
        $(document).on('change','select[name="dispStoreType"]',function(){
            expBannerCommon.changeDispStoreType($(this));
        });

        $(document).on('change','#templateDetail',function(){
            expBanner.changeImgSizeDescription();
        });

        $(document).on('keyup','.bigBannerTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });
        $(document).on('keyup','.bigBannerSubTitle',function(){
            $(this).parent().find('.cnt').html($(this).val().length);
        });

        //점포 검색 팝업
        $(document).on('click','.storeInfoBtn',function(){
            expBannerCommon.setStorePop($(this));
        });
        /*****************************노출 점포 관려 이벤트 종료*******************************************/

        /******************************링크 정보 관려 이벤트 등록******************************************/
        //링크 정보  -> ITEM_TD -> 점포 타입 변경
        $(document).on('change','.storeType',function(){
            expBannerCommon.changeLinkStoreType($(this));
        });

        //택배점 설정 체크박스
        $(document).on('click','.dlvStoreYn',function(){
            expBannerCommon.setDlvStoreYn($(this));
        });

        //링크타입 변경
        $(document).on('change','.linkeType',function(){
            expBannerCommon.changeLinkType($(this).val() , $(this).next('div') );
        });
        /******************************링크 정보 관려 이벤트 종료******************************************/
    },
    /**
     * 점포 상품 점포 유형 변경
     */
    changeLinkStoreType : function(obj) {
        $(obj).parent().find('.linkInfo').val('');
        $(obj).parent().find('.linkInfoNm').val('');
        $(obj).parent().find('.dlvStoreYn').prop("checked", false).val('N');

        if($(obj).val() == 'HYPER' ||  $(obj).val() == 'CLUB'){
            $(obj).parent().find('.dlvStoreList').prop("disabled", false).val('');
        } else{
            $(obj).parent().find('.dlvStoreList').prop("disabled", true).val('');
        }
    },
    setStorePop : function(obj) {
        if ($.jUtil.isEmpty(expBannerCommon.storeList)) {
            expBannerCommon.openStoreInfoPopup();
        } else {
            expBannerCommon.openStoreInfoPopup(expBannerCommon.storeList);
        }
    },
    /**
     * 점포 팝업 call back
     */
    storePopupCallback: function(res) {
        expBannerCommon.storeList = res.storeList;
        $('#dispStoreCnt').html(res.storeList.length);
    },
    /**
     * 점포 유형에 따른 점포 팝업
     */
    openStoreInfoPopup : function(data) {
        var storeType = $('select[name="dispStoreType"]').val();

        $('.storePop input[name="storeType"]').val(storeType);
        $('.storePop input[name="storeList"]').val(JSON.stringify(data));

        if ($("#chgYn").val() == 'Y') {
            $('.storePop input[name="setYn"]').val('N');
        } else {
            $('.storePop input[name="setYn"]').val('Y');
        }

        var url = "/common/popup/storeSelectPop";
        var target = "storeSelectPop";
        windowPopupOpen(url, target, 600, 450);

        var frm = document.getElementById('storeSelectPopForm');
        frm.target = target;

        frm.submit();
    },
    /**
     * 노출 점포 영역 세팅
     */
    setDispStoreType : function(obj) {
        var siteType  = $('#siteType').val();
        var dispStoreType = $(obj).val();

        $dispStore = $(obj).parent().parent().find('#dispStore');
        $dispStore.find('select').remove();

        if(dispStoreType == 'ALL') {
            $dispStore.hide();
            expBannerCommon.storeList = null;
        } else {
            if(siteType == 'HOME') {
                $dispStore.prepend($('#storeTypeHome').html());
            } else {
                $dispStore.prepend($('#storeTypeClub').html());
            }
            $dispStore.find('select').attr('name' ,'dispStoreType');
            $dispStore.show();
        }
        expBannerCommon.changeDispStoreType();
    },
    changeDispStoreType : function() {
        expBannerCommon.storeList = null;
        $('#dispStoreCnt').html('0');
    },
    /**
     * 택배점 설정 검사
     */
    setDlvStoreYn : function(obj) {
        if($(obj).is(":checked")) {
            var storeTyep = $(obj).parent().parent().find('.storeType').val();
            var linkInfo = $(obj).parent().parent().find('.linkInfo').val();

            if(storeTyep != 'HYPER' && storeTyep != 'CLUB') {
                alert("새벽배송 또는 익스프레스는 택배점 설정이 불가 합니다.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            if ($.jUtil.isEmpty(linkInfo)) {
                alert("대상 상품 번호를 먼저 설정해주세요.");
                $(obj).prop("checked", false);
                $(obj).val('N');
                return false;
            }
            $(obj).val('Y');
            $(obj).parent().find('select').prop("disabled" ,false);
        } else {
            $(obj).val('N');
            $(obj).parent().find('select').val('').prop("disabled" ,true);
        }
    },
    /**
     * 기획전, 이벤트, 셀러상품, 매장상품 조회 팝업 분기
     */
    popLink : function(obj) {
        linkInfoDiv = $(obj).parent();

        var linkeType  = $(obj).parent().prev('select').val();
        var gnbDeviceType = $(obj).parent().find('.gnbDeviceType').val();
        var siteType = $('#siteType').val();

        switch (linkeType) {
            case "EXH"  :
                expBannerCommon.addPromoPopUp('EXH');
                break;
            case "EVENT" :
                expBannerCommon.addPromoPopUp('EVENT');
                break;
            case "ITEM_DS" :
                expBannerCommon.addItemPopUp('DS');
                break;
            case "ITEM_TD" :
                //하이퍼인지 익스프레스인지 확인 하는 로직 필요
                expBannerCommon.addItemPopUp('TD' , 'EXP');
                break;
            case "GNB":
                expBannerCommon.addGnbPopUp(gnbDeviceType , siteType);
                break;
        }
    },
    /**
     * GNB 조회 콜백 함수
     */
    addGnbPopUpCallBack : function(resDataArr){
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].gnbNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].bannerNm);
    },
    /**
     * GNB 조회 팝업
     */
    addGnbPopUp : function(gnbDeviceType , siteType){
        $.jUtil.openNewPopup('/common/popup/gnbPop?callback=expBannerCommon.addGnbPopUpCallBack&isMulti=N&deviceType=' +gnbDeviceType + '&siteType='+ siteType  , 1084, 650);
    },
    /**
     * 상품조회팝업 콜백 함ㅅ후
     */
    addItemPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].itemNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].itemNm);
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function(itemType, storeType){
        var url = '/common/popup/itemPop?callback=expBannerCommon.addItemPopUpCallBack&isMulti=N&';

        switch (itemType) {
            case "TD"  :
                url += 'mallType=TD&storeType=' + storeType;
                break;
            case "DS" :
                url += 'mallType=DS&storeType=DS';
                break;
        }
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 포로모션조회팝업 콜백 함ㅅ후
     */
    addPromoPopUpCallBack : function(resDataArr) {
        linkInfoDiv.find('.linkInfo').val(resDataArr[0].promoNo);
        linkInfoDiv.find('.linkInfoNm').val(resDataArr[0].promoNm);
    },
    /**
     * 포로모션 조회팝업
     */
    addPromoPopUp :  function(promoType){
        var url = '/common/popup/promoPop?callback=expBannerCommon.addPromoPopUpCallBack&isMulti=N&siteType=HOME&promoType=' + promoType;
        $.jUtil.openNewPopup(url , 1084, 650);
    },
    /**
     * 링크 상세 정보 입력
     */
    setLinkInfo : function (data, obj) {
        $(obj).find('.linkInfo').val(data.linkInfo);
        $(obj).find('.linkInfoNm').val(data.linkInfoNm);
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function(linkType, obj) {
        var linkInfo ='';
        var none = "";

        switch (linkType) {
            case "EXH"  :
            case "EVENT" :
            case "ITEM_DS" :
            case "ITEM_TD":
                linkInfo  = $("#linkCommon").html();
                break;
            case "URL":
                linkInfo  = $("#linkUrl").html();
                break;
            case "GNB":
                linkInfo  = $("#linkGnbDevice").html() + $("#linkCommon").html();
                break;
            default :
                linkInfo =  none;
        }

        if($('#siteType').val() == 'HOME' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeHome").html() + linkInfo +  $("#dlvStoreTypeHome").html()
        }
        if($('#siteType').val() == 'CLUB' && linkType == 'ITEM_TD') {
            linkInfo = $("#storeTypeClub").html() + linkInfo +  $("#dlvStoreTypeClub").html()
        }
        if($('#loc').val() == 'TOP') {
            linkInfo += '<button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="expBannerCommon.resetLinkInfo(this);">초기화</button>';
        }
        obj.html(linkInfo);
    },
    /**
     * 파라미터 유효성 검사
     */
    setBannerValid : function() {
        // 기본 폼 검사
        if($.jUtil.isEmpty($('#loc').val())) {
            alert("전시 위치를 선택해 주세요");
            return false;
        } else {
            switch ($('#loc').val()) {
                case 'LBB' :
                case 'MBB' :
                    break;
            }
        }
        if($.jUtil.isEmpty($('#priority').val())) {
            $('#priority').focus();
            alert('우선순위를 입력해 주세요.');
            return false;
        }
        if($.jUtil.isEmpty($('#bannerNm').val())) {
            alert("배너명을 입력해 주세요.");
            return false;
        }
        if($('#bannerNm').val().length > 20) {
            alert("배너명은 20자를 넘을 수 없습니다.");
            $('#bannerNm').val('').focus();
            $('#bannerNmCnt').html('0');
            return false;
        }

        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("전시기간을 입력해주세요.")
            $('#dispStartDt').focus();
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("전시기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("전시기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        // 베너 폼 검사
        var bannerDetail = $("#bannerSetForm").serializeObject(true);
        for(var i=0; i < parseInt($("#template").val()); i++) {
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].title)) {
                alert("타이틀을 입력해 주세요." );
                return false;
            }
            if(!$.jUtil.isEmpty(bannerDetail.linkList[i].textYn) && bannerDetail.linkList[i].textYn =='Y' && $.jUtil.isEmpty( bannerDetail.linkList[i].subTitle)) {
                alert("서브 타이틀을 입력해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].imgUrl)) {
                alert("이미지를 등록해 주세요." );
                return false;
            }
            if($.jUtil.isEmpty(bannerDetail.linkList[i].linkInfo) && bannerDetail.linkList[i].linkType != 'NONE') {
                alert("링크정보를 등록해 주세요." );
                return false;
            }
        }

        return true;
    },
    /**
     * 입력 폼 초기화 ( 등록 또는 검색 후 )
     */
    resetBannerForm : function() {
        $('#loc, #bannerNo, #bannerNm, #priority').val('');
        expBanner.initDateTime('7d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");

        $("#template").val('1').trigger('change');
        $("#bannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
        $('#siteType').val('HOME');
        $('#dispYn').val('Y');
        $('#device').val('PC').trigger('change');
        $('#priority, #device').attr("disabled", false);
        $('#bannerTr, .bigBannerDiv1').hide();
        $('#bannerNmCnt, #dispStoreCnt').html('0');

        expBannerCommon.storeList = null;
    },
    /**
     * 배너 입력 폼 초기화 ( 배너 정보 변경 시 )
     */
    resetForm : function() {
        $("#template").val('1').trigger('change');
        $("#bannerSetForm").find('input:radio[name=dispStoreType]:input[value=ALL]').prop('checked', true).trigger('click');
        $('#dispStoreCnt').html('0');

        expBannerCommon.storeList = null;
    },
    /**
     * 배너 상세 정보 입력
     */
    setBannerDetail : function(data, template, templateDetail) {
        $("#template").val(template).trigger('change');
        $("#templateDetail").val(templateDetail).trigger('change');

        if (!$.jUtil.isEmpty(data)) {
            for(var i =0 ; i < parseInt(template) ; i++) {
                var link  = data[i];
                var templateNum = i+1;

                if(link.textYn == 'Y') {
                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', true);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,false).val(link.title);
                    $('#bannerTitleCnt' + templateNum).html(link.title.length);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,false).val(link.subTitle);
                    $('#bannerSubTitleCnt' + templateNum).html(link.subTitle.length);
                } else {
                    $('.bigBannerDiv' + templateNum).find('.textYn').prop('checked', false);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerTitle').prop('readonly' ,true).val('');
                    $('#bannerTitleCnt' + templateNum).html(0);
                    $('.bigBannerDiv' + templateNum).find('.bigBannerSubTitle').prop('readonly' ,true).val('');
                    $('#bannerSubTitleCnt' + templateNum).html(0);
                }

                expBannerCommon.img.setImg($('.bigBannerDiv' + templateNum).find('.itemImg'), {
                    'imgUrl': link.imgUrl,
                    'src'   : hmpImgUrl + "/provide/view?processKey=BenefitPCTopBanner" + "&fileId=" + link.imgUrl,
                });

                $("#bigBannerLinktype" + templateNum ).val(link.linkType).trigger('change');
                expBannerCommon.setLinkInfo(link ,  $("#bigBannerLinkInfo" + templateNum));
            }
        }
    },
    /**
     * 배너 단 설정
     */
    changeTemplate : function(){
        expBannerCommon.resetLinkTypeAll();
        var template = $("#template").val();

        //배너 입력 영역 설정
        switch (template) {
            case "6" :
                $('.bigBannerDiv6').css("display", "");
            case "5" :
                $('.bigBannerDiv5').css("display", "");
                $('.bigBannerDiv4').css("display", "");
            case "3" :
                $('.bigBannerDiv3').css("display", "");
            case "2" :
                $('.bigBannerDiv2').css("display", "");
            case "1" :
                $('.bigBannerDiv1').css("display", "");
                break;
        };

        //단 선택 상세 정보 셀렉트 박스 제어
        $("#templateDetail").html('');
        switch (template) {
            case "6"  :
            case "1" :
                $("#templateDetail").prop('disabled' , true);
                break;
            case "5" :
            case "3" :
            case "2" :
                $("#templateDetail").append( $("#templateDetail" + template).html());
                $("#templateDetail").prop('disabled' , false);

                break;
        };

        expBanner.changeImgSizeDescription();
    },
    /**
     * 링크 타입 전체 초기화
     */
    resetLinkTypeAll : function() {
        $("[class^='bigBannerDiv']").css("display", "none");
        $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('');
        $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='bigBannerDiv']").find('.deleteImgBtn').trigger('click');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').prop('readonly' ,true).val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').prop('readonly' ,true).val('');

        $('.bigBannerDiv1').css("display", "");
    },
    /**
     * 링크 타입 전체 초기화
     */
    resetLinkType : function() {
        $("[class^='bigBannerDiv']").css("display", "none");
        $("[class^='bigBannerDiv']").find('select').val('EXH').trigger('change');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('');
        $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false);
        $("[class^='bigBannerDiv']").find('.deleteImgBtn').trigger('click');
        $("[class^='bigBannerDiv']").find('.bigBannerTitle').prop('readonly' ,true).val('');
        $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').prop('readonly' ,true).val('');
    },
    /**
     * 링크 타입 초기화 (단 번호별 초기화)
     */
    resetLinkInfo : function(linkTemplateNumber) {
        $('.bigBannerDiv'+linkTemplateNumber).find('select').val('EXH').trigger('change');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerTitle').val('').prop('readOnly' , true);;
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSubTitle').val('').prop('readOnly' , true);;
        $('.bigBannerDiv'+linkTemplateNumber).find('input[name="linkList[].textYn"]').prop("checked", false);
        $('.bigBannerDiv'+linkTemplateNumber).find('.deleteImgBtn').trigger('click');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerTitle').prop('readonly' ,true).val('');
        $('.bigBannerDiv'+linkTemplateNumber).find('.bigBannerSubTitle').prop('readonly' ,true).val('');
    },
    /**
     * 타이틀 입력란 설정
     */
    setText : function(obj){


        if($(obj).prop("checked"))  {
            $(obj).val('Y');
            $(obj).parent().parent().find('.bigBannerTitle').prop('readOnly' ,false);
            $(obj).parent().parent().find('.bigBannerSubTitle').prop('readOnly' ,false);


            /* $("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , false);
             $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , false);
             $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", true).val('Y');*/

        }else {
            $(obj).val('N');
            $(obj).parent().parent().find('.bigBannerTitle').val('').prop('readOnly' ,true).trigger('keyup');
            $(obj).parent().parent().find('.bigBannerSubTitle').val('').prop('readOnly' ,true).trigger('keyup');

            /*$("[class^='bigBannerDiv']").find('.bigBannerTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('.bigBannerSubTitle').val('').prop('readOnly' , true);
            $("[class^='bigBannerDiv']").find('input[name="linkList[].textYn"]').prop("checked", false).val('N');*/
        }
    }
};

expBannerCommon.img = {
    imgDiv : null,
    bigBannerProcessMap : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        expBannerCommon.img.resetImg();
        expBannerCommon.img.setImgProcessKey();
        expBannerCommon.img.imgDiv = null;
    },
    setImgProcessKey : function() {
        /**
         PcMainBigBanner1	1200*450
         PcMainBigBanner2	590*450
         PcMainBigBanner3	790*450
         PcMainBigBanner4	390*450
         PcMainBigBanner5	390*220
         PcMainBigBanner6	290*450
         */
        expBannerCommon.img.bigBannerProcessMap =
            [
                ["PcMainBigBanner1"],                   //1단
                ["PcMainBigBanner2","PcMainBigBanner2"],//2단 기본
                ["PcMainBigBanner3","PcMainBigBanner4"],//2단 좌측강조
                ["PcMainBigBanner4","PcMainBigBanner4","PcMainBigBanner4"], //3단 기본
                ["PcMainBigBanner3","PcMainBigBanner5","PcMainBigBanner5"], //3단좌측강조
                ["PcMainBigBanner2","PcMainBigBanner6","PcMainBigBanner6"], //3단 중앙 강조
                ["PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner4","PcMainBigBanner5","PcMainBigBanner5"], //5단 중앙 강조
                ["PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5","PcMainBigBanner5"] //6단

            ];
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ expBannerCommon.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#itemFile');
        var division = expBannerCommon.img.imgDiv.find('.division').val();
        let params = {
            processKey : expBannerCommon.img.getProcessKey(division),
            mode : 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        expBannerCommon.img.setImg(expBannerCommon.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        expBannerCommon.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        expBannerCommon.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    getProcessKey : function(division) {
        var processKey = '';
        var loc = $('#loc').val();
        var device = $('#device').val();
        var template = $('#template').val();
        var templateDetail = $('#templateDetail').val();

        switch (device) {
            case 'PC' :
                if(loc === 'UBB') {
                    switch (template) {
                        case "1":
                            processKey = expBannerCommon.img.bigBannerProcessMap[0][0];
                            break;
                        case "2":
                            if(templateDetail == 'N') {
                                processKey = expBannerCommon.img.bigBannerProcessMap[1][division];
                            } else {
                                processKey = expBannerCommon.img.bigBannerProcessMap[2][division];
                            }
                            break;
                        case "3":
                            if(templateDetail == 'N') {
                                processKey = expBannerCommon.img.bigBannerProcessMap[3][division];
                            } else if(templateDetail == 'LP') {
                                processKey = expBannerCommon.img.bigBannerProcessMap[4][division];
                            } else {
                                processKey = expBannerCommon.img.bigBannerProcessMap[5][division];
                            }
                            break;
                        case "5":
                            processKey = expBannerCommon.img.bigBannerProcessMap[6][division];
                            break;
                        case "6":
                            processKey = expBannerCommon.img.bigBannerProcessMap[7][division];
                            break;
                        default :
                            processKey = 'BenefitPCTopBanner';
                            break;
                    }
                } else {
                    processKey = 'ExpPcBannerType1';
                    break;
                }
                break;
            case 'MOBILE' :
                if(loc === 'UBB') {
                    processKey = 'ExpMobileBannerType1';
                } else {
                    processKey = 'ExpMobileBannerType2';
                }
                break;
            default :
                processKey = 'BenefitPCTopBanner';
                break;
        }

        return processKey;
    }
};