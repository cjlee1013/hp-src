/**
 * 사이트 관리 > 전시관리 > 메인테마(기간) 상품 일괄등록 엑셀읽기 js
 */
$(document).ready(function () {
    mainThemePeriodItemExcelPop.init();
    CommonAjaxBlockUI.global();
});

var mainThemePeriodItemExcelPop = {
    /**
     * init 이벤트
     */
    init: function() {
        mainThemePeriodItemExcelPop.bindingEvent();
    }
    /**
     * binding 이벤트
     */
    , bindingEvent: function() {
        $("#downloadTemplate").bindClick(mainThemePeriodItemExcelPop.downloadTemplate);// 양식 다운로드 버튼
        $("#schUploadFile").bindClick(mainThemePeriodItemExcelPop.searchFile);         // 파일찾기 버튼
        $("#selectBtn").bindClick(mainThemePeriodItemExcelPop.selectFile);             // 등록 버튼
    }
    /**
     * 업로드양식 다운로드
     */
    , downloadTemplate: function() {
        var fileUrl = "/static/templates/mainThemePeriodItem_template.xlsx";

        if(fileUrl != "") {
            location.href = fileUrl;
        } else {
            alert("양식 파일이 없습니다.");
            return false;
        }
    }
    /**
     * [파일찾기] 버튼 클릭 (윈도우 파일 찾기 open)
     */
    , searchFile: function() {
        $("#uploadFile").click();
    }
    /**
     * 선택한 파일 정보 불러오기
     */
    , getFile: function(_obj) {
        var fileInfo = _obj[0].files[0];
        var fileName = fileInfo.name;
        $("#uploadFileNm").val(fileName);
    }
    /**
     * 파일 읽기, callBack 함수 호출
     */
    , selectFile: function() {
        if (!mainThemePeriodItemExcelPop.valid()) {
            return false;
        }

        var url = "/manage/dspMainThemePeriod/dspMainThemePeriodItemExcel.json";

        // 파일읽기 호출
        $("#fileUploadForm").ajaxForm({
            url     : url,
            method  : "POST",
            enctype : "multipart/form-data",
            timeout : 30000,
            success : function(res) {
                // df message alert
                if ($.jUtil.isEmpty(callBackScript)) {
                    alert("완료되었습니다.");
                    // call back 호출
                    opener.dspMainThemePeriodItem.itemExcelPopCallback(res);
                } else {
                    eval('opener.' + callBackScript + '(res);');
                }
                // close
                self.close();
            },
            error   : function(resError) {
                // error message alert
                if (resError.responseJSON != null) {
                    if (resError.responseJSON.returnMessage != null) {
                        alert(resError.responseJSON.returnMessage);
                    } else {
                        alert(CommonErrorMsg.uploadFailMsg);
                    }
                } else if (resError.status !== 200) {
                    alert(CommonErrorMsg.dfErrorMsg);
                }
            },
        });
        $("#fileUploadForm").submit();
    }
    /**
     * 파일 읽기 전, 유효성 검증
     * @return {boolean}
     */
    , valid: function() {
        var uploadFileVal = $("#uploadFile").val();
        var uploadFileNmVal = $("#uploadFileNm").val();

        if ($.jUtil.isEmpty(uploadFileVal) || $.jUtil.isEmpty(uploadFileNmVal)) {
            alert("파일 선택 후 진행해주세요.");
            return false;
        }
        return true;
    }
};