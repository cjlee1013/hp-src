/**
 * 사이트관리 > 팝업 관리 > 파트너공지 팝업 관리
 */
$(document).ready(function() {
    dspPopupListGrid.init();
    dspPopupPartnerListGrid.init();
    dspPopup.init();
    CommonAjaxBlockUI.global();
});

// 파트너공지 팝업 리스트 그리드
var dspPopupListGrid = {
    gridView : new RealGridJS.GridView("dspPopupListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspPopupListGrid.initGrid();
        dspPopupListGrid.initDataProvider();
        dspPopupListGrid.event();
    },
    initGrid : function() {
        dspPopupListGrid.gridView.setDataSource(dspPopupListGrid.dataProvider);
        dspPopupListGrid.gridView.setStyles(dspPopupListGridBaseInfo.realgrid.styles);
        dspPopupListGrid.gridView.setDisplayOptions(dspPopupListGridBaseInfo.realgrid.displayOptions);
        dspPopupListGrid.gridView.setColumns(dspPopupListGridBaseInfo.realgrid.columns);
        dspPopupListGrid.gridView.setOptions(dspPopupListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspPopupListGrid.dataProvider.setFields(dspPopupListGridBaseInfo.dataProvider.fields);
        dspPopupListGrid.dataProvider.setOptions(dspPopupListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspPopupListGrid.gridView.onDataCellClicked = function(gridView, index) {
            dspPopup.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspPopupListGrid.dataProvider.clearRows();
        dspPopupListGrid.dataProvider.setRows(dataList);
    }
};

// 팝업 노출 파트너 리스트 그리드
var dspPopupPartnerListGrid = {
    gridView : new RealGridJS.GridView("dspPopupPartnerListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspPopupPartnerListGrid.initGrid();
        dspPopupPartnerListGrid.initDataProvider();
        $('#partnerList').hide();
    },
    initGrid : function() {
        dspPopupPartnerListGrid.gridView.setDataSource(dspPopupPartnerListGrid.dataProvider);
        dspPopupPartnerListGrid.gridView.setStyles(dspPopupPartnerListGridBaseInfo.realgrid.styles);
        dspPopupPartnerListGrid.gridView.setDisplayOptions(dspPopupPartnerListGridBaseInfo.realgrid.displayOptions);
        dspPopupPartnerListGrid.gridView.setColumns(dspPopupPartnerListGridBaseInfo.realgrid.columns);
        dspPopupPartnerListGrid.gridView.setOptions(dspPopupPartnerListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspPopupPartnerListGrid.dataProvider.setFields(dspPopupPartnerListGridBaseInfo.dataProvider.fields);
        dspPopupPartnerListGrid.dataProvider.setOptions(dspPopupPartnerListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dspPopupPartnerListGrid.dataProvider.clearRows();
        dspPopupPartnerListGrid.dataProvider.setRows(dataList);
    },
    addData : function (dataList) {
        dspPopupPartnerListGrid.dataProvider.addRows(dataList);
    }
};

// 파트너공지 팝업 관리
var dspPopup = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-30d');
        this.initDateTime('7d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
    },
    initSearchDate : function (flag) {
        dspPopup.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        dspPopup.setDate.setEndDate(0);
        dspPopup.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspPopup.search);
        $('#resetBtn').bindClick(dspPopup.resetForm);
        $('#searchResetBtn').bindClick(dspPopup.searchFormReset);
        $('#setDspPopupBtn').bindClick(dspPopup.setDspPopup);
        $('#excelDownloadBtn').bindClick(dspPopup.excelDownload);
        $('#itemNo').focusout(dspPopup.getItemName);
        // $('#linkType').bindChange(dspPopup.chgLinkInfo);
        $('input[name="dispTarget"]').bindChange(dspPopup.chgDisplayTarget);
        $('#addItemPopUp').bindClick(dspPopup.addItemPopUp);
        $('#addPartner').bindClick(dspPopup.addPartnerPopUp);
        $('#deletePartner').bindClick(dspPopup.deleteCheckedPartner);
        $('#popupNm').calcTextLength('keyup', '#popupNmCnt');

        //링크타입 변경
        $(document).on('change', '#linkType',function() {
            dspPopup.changeLinkType($(this).val(), $(this).next('div'));
        });

        // 파일선택시
        $('input[name="fileArr"]').change(function() {
            dspPopup.img.addImg();
        });

        // 등록 > 이미지 삭제
        $('.deleteImgBtn').on('click', function() {
            if (confirm('삭제하시겠습니까?')) {
                dspPopup.img.deleteImg($(this).siblings('.uploadType'));
            }
        });
    },
    /**
     * 일자시간 초기화
     * @param flag
     * @param startId
     * @param endId
     */
    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
        dspPopup.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        dspPopup.setDateTime.setEndDateByTimeStamp(flag);
        dspPopup.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 링크 타입에 따른 입력폼 변경
     */
    changeLinkType : function(linkType, obj) {
        var linkInfo = '';
        var none = '';

        switch (linkType) {
            case "NOTICE":
            case "FAQ":
                linkInfo = $("#linkNotice").html();
                break;
            case "URL":
                linkInfo = $("#linkUrl").html();
                break;
            default :
                linkInfo =  none;
        }

        obj.html(linkInfo);
    },
    /**
     * 링크 유형별 노출변경
     */
    chgLinkInfo : function() {
        $('#setLinkInfo').val('');
        switch ($('#linkType option:selected').val()) {
            case 'MENU' :
                $('#setLinkInfo').val('Tab_itemSetMain').show();
                break;
            default :
                $('#setLinkInfo').hide();
                break;
        }
    },
    /**
     * 상품조회팝업
     */
    addItemPopUp : function() {
        $.jUtil.openNewPopup('/common/popup/itemPop?callback=dspPopup.setItemInfo&isMulti=N&mallType=TD&storeType=HYPER', 1084, 650);
    },
    /**
     * 상품조회 후 값 입력
     */
    setItemInfo : function(resData) {
        $('#itemNo').val(resData[0].itemNo);
        $('#itemNm').val(resData[0].itemNm);
    },
    /**
     * 노출 대상 노출변경
     */
    chgDisplayTarget : function() {
        switch ($('input:radio[name="dispTarget"]:checked').val()) {
            case 'ALL' :
                $('#partnerList').hide();
                $('#partnerTotalCount').html($.jUtil.comma(0));
                dspPopupPartnerListGrid.dataProvider.clearRows();
                break;
            case 'PART' :
                $('#partnerList').show();
                break;
         }
    },
    /**
     * 파트너 등록 팝업 호출
     */
    addPartnerPopUp : function(){

        windowPopupOpen("/common/popup/partnerPop?partnerId="
            + '' + "&callback=dspPopup.addPartner"
            + "&partnerType="
            + "SELL"
            + "&isMulti=Y"
            , "partnerPop", "1080", "600", "yes", "no");

    },
    /**
     * 파트너공지 팝업 등록/수정
     */
    setDspPopup : function() {
        if(!dspPopup.valid.set()) {
            return false;
        }

        switch ($('#linkType option:selected').val()) {
            case 'ITEM' :
                $('#linkInfo').val($('#itemNo').val());
                break;
            default :
                $('#linkInfo').val($('#setLinkInfo').val());
                break;
        }

        let setDspPopup = $('#dspPopupSetForm').serializeObject();
        setDspPopup.partnerList = dspPopupPartnerListGrid.dataProvider.getJsonRows();

        CommonAjax.basic({ url:'/manage/dspPopup/setDspPopup.json', data : JSON.stringify(setDspPopup), method:"POST", successMsg : null, contentType : "application/json", callbackFunc : function(res) {
                alert(res.returnMsg);
                dspPopup.resetForm();
                // dspPopup.searchFormReset();
                dspPopup.search();
        }});
    },
    /**
     * 파트너 리스트 추가 ( 팝업 노출 판매자 그리드 )
     */
    addPartner : function(partnerList) {
        let partnerGrid = dspPopupPartnerListGrid.dataProvider.getJsonRows();
        if(partnerGrid.length > 0) {
            partnerGrid.forEach(function (item, index) {
                partnerList.forEach(function (partner, idx) {
                    if(item.partnerId === partner.partnerId) {
                        partnerList.splice(idx, 1);
                    }
                });
            });
        }
        $('#partnerTotalCount').html($.jUtil.comma(partnerGrid.length + partnerList.length));
        dspPopupPartnerListGrid.addData(partnerList);
    },
    /**
     * 파트너 리스트 삭제 ( 팝업 노출 판매자 그리드 )
     */
    deleteCheckedPartner : function() {
        let checkedVal = dspPopupPartnerListGrid.gridView.getCheckedRows(true) + "";
        if(checkedVal != null && checkedVal != '') {
            dspPopupPartnerListGrid.dataProvider.removeRows(checkedVal.split(","),true);
        } else {
            alert("선택한 항목이 없습니다.");
        }
    },
    /**
     * 파트너공지 팝업 관리검색
     */
    search : function() {
        if(!dspPopup.valid.search()){
            return false;
        }
        CommonAjax.basic({url:'/manage/dspPopup/getDspPopupList.json?' + $('#dspPopupSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
            dspPopup.resetForm();
            dspPopupListGrid.setData(res);
            $('#dspPopupTotalCount').html($.jUtil.comma(dspPopupListGrid.gridView.getItemCount()));
        }});
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = dspPopupListGrid.dataProvider.getJsonRow(selectRowId);

        $('#popupNo').val(rowDataJson.popupNo);
        $('#useYn').val(rowDataJson.useYn);

        CalendarTime.setCalDate('dispStartDt', rowDataJson.dispStartDt);
        CalendarTime.setCalDate('dispEndDt', rowDataJson.dispEndDt);

        $('#popupNm').val(rowDataJson.popupNm);
        $('#popupNmCnt').html(rowDataJson.popupNm.length);

        $('input[name="closeBtnType"][value="' + rowDataJson.closeBtnType + '"]').prop('checked', true);
        $('input[name="closeBtnPeriod"][value="' + rowDataJson.closeBtnPeriod + '"]').prop('checked', true);

        dspPopup.img.setImg($(this), {
            'imgNo' : rowDataJson.imgNo,
            'imgNm' : rowDataJson.imgNm,
            'imgUrl': rowDataJson.imgUrl,
            'width' : rowDataJson.imgWidth,
            'height': rowDataJson.imgHeight,
            'src'   : hmpImgUrl + "/provide/view?processKey=PartnerPopupImage&fileId=" + rowDataJson.imgUrl,
            'changeYn' : 'N'
        });

        var linkType = rowDataJson.linkType;
        $('#linkType').val(linkType).trigger('change');
        switch ($('#linkType option:selected').val()) {
            case 'NOTICE' :
            case 'FAQ' :
            case 'URL' :
                $('#selectTextBox').find('#linkInfo').val(rowDataJson.linkInfo);
                break;
            default :
                $('#selectTextBox').find('#linkInfo').val('');
                break;
        }

        $('input[name="dispTarget"][value="' + rowDataJson.dispTarget + '"]').prop('checked', true);
        let dispTarget = rowDataJson.dispTarget;
        switch (dispTarget) {
            case 'ALL' :
                $('#partnerList').hide();
                break;
            case 'PART' :
                dspPopupPartnerListGrid.dataProvider.clearRows();
                $('#partnerList').show();
                // 리스트 넣기;
                CommonAjax.basic({url:'/manage/dspPopup/getDspPopupPartnerList.json?popupNo=' + rowDataJson.popupNo, data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                        dspPopupPartnerListGrid.setData(res);
                        $('#partnerTotalCount').html($.jUtil.comma(dspPopupPartnerListGrid.gridView.getItemCount()));
                }});
                break;
        }
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {
        dspPopup.initSearchDate('-30d');
        $('#searchUseYn, #searchKeyword').val('');
        $('#searchPeriodType').val('DISPLAY');
        $('#searchType').val('popupNm');
    },
    /**
     * 입력/수정 폼 초기화
     */
    resetForm : function() {
        dspPopup.initDateTime('7d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        $('#popupNo, #linkInfo, #popupNm, #itemNo, #itemNm, #setLinkInfo').val('');
        $('#useYn').val('Y');
        $('#linkType').val('MENU').trigger('change');
        $('input[name="closeBtnPeriod"], input[name="dispTarget"]').prop('checked', false);
        dspPopup.chgDisplayTarget();
        $('input[name="closeBtnPeriod"][value="0"]').prop('checked', true);
        $('input[name="dispTarget"][value="ALL"]').prop('checked', true);
        $('#partnerList').hide();
        $('#partnerTotalCount').html($.jUtil.comma(0));
        $('#popupNmCnt').html('0');
        dspPopupPartnerListGrid.dataProvider.clearRows();
        $('#setLinkInfo').val('Tab_itemSetMain').show();
        dspPopup.img.init();
    }
};

dspPopup.valid = {
    search : function () {
        // 날짜 체크
        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();

        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            seller.initSearchDate('-90d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "month", true) > 3) {
            alert("최대 3개월 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2) {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }
        return true;
    },
    set : function () {
        // 날짜 체크
        var startDt = $("#dispStartDt");
        var endDt = $("#dispEndDt");

        if($.jUtil.isEmpty($('#useYn').val())) {
            alert("사용여부를 선택해 주세요.");
            $('#useYn').focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 1) {
            alert("최대 1년 범위까지만 설정 가능합니다. \n전시 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD HH:mm:ss"));
            return false;
        }
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }
        if($.jUtil.isEmpty($('#popupNm').val())) {
            alert("팝업명을 입력해 주세요.");
            $('#popupNm').focus();
            return false;
        }
        if($.jUtil.isEmpty($('input:radio[name="closeBtnPeriod"]:checked').val())) {
            alert("숨김 기능 버튼을 선택해 주세요.");
            return false;
        }
        if($.jUtil.isEmpty($('#imgUrl').val())) {
            alert("이미지를 등록해 주세요.");
            return false;
        }

        switch($('#linkType option:selected').val()) {
            case 'URL' :
                if($.jUtil.isEmpty($('#selectTextBox').find('#linkInfo').val())) {
                    alert("Url 링크를 입력해 주세요.");
                    $('#selectTextBox').find('#linkInfo').focus();
                    return false;
                }
                break;
        }

        if($.jUtil.isEmpty($('input:radio[name="dispTarget"]:checked').val())) {
            alert("노출 대상을 선택해 주세요.");
            return false;
        }

        return true;
    }
};

/**
 * 파트너공지 팝업 이미지
 */
dspPopup.img = {
    /**
     * 이미지 초기화
     */
    init : function() {
        dspPopup.img.setImg($(this));
        commonEditor.inertImgurl = null;
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params, isDelete) {
        if (params) {
            $('.imgUrl').val(params.imgUrl);
            if (!$('.imgNo').val()) {
                $('.imgNo').val(params.imgNo);
            }
            $('.imgNm').val(params.imgNm);
            $('.width').val(params.width);
            $('.height').val(params.height);
            $('.changeYn').val(params.changeYn);
            $('.imgUrlTag').prop('src', params.src);

            $('.imgDisplayView').show();
            $('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            $('.imgUrl').val('');
            if (!isDelete) {
                $('.imgNo').val('');
            }
            $('.imgNm').val('');
            $('.width').val('');
            $('.height').val('');
            $('.changeYn').val('N');
            $('.imgUrlTag').prop('src', '');

            $('.imgDisplayView').hide();
            $('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#imageFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#imageFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#imageFile');
        let ext = $('#imageFile').get(0).files[0].name.split('.');
        let params = {
            processKey : 'PartnerPopupImage',
            mode : 'IMG'
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        dspPopup.img.setImg(dspPopup.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'width' : f.fileStoreInfo.width,
                            'height': f.fileStoreInfo.height,
                            'src'   : hmpImgUrl + "/provide/view?processKey=" + params.processKey + "&fileId=" + f.fileStoreInfo.fileId,
                            'ext'   : ext[1],
                            'changeYn' : 'Y'
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        let imgUrl = $(obj).find('img').prop('src');
        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(obj) {
        dspPopup.img.setImg(obj, null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function() {
        $('input[name=fileArr]').click();
    }
};