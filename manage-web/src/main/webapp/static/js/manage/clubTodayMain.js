/**
 * 상품관리 > 점포상품관리 > 클럽투데이 관리
 */
$(document).ready(function() {

    clubTodayListGrid.init();
    clubToday.init();
    clubTodayItemListGrid.init();
    clubTodayItem.init();

    CommonAjaxBlockUI.global();
    commonEditor.initEditor('#contentsDescEdit', 300, 'FrontBanner');
});
var commonEditor;

var clubTodayItemListGrid = {

    gridView : new RealGridJS.GridView("clubTodayItemListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        clubTodayItemListGrid.initGrid();
        clubTodayItemListGrid.initDataProvider();
        clubTodayItemListGrid.event();

    },
    initGrid : function() {

        clubTodayItemListGrid.gridView.setDataSource(clubTodayItemListGrid.dataProvider);
        clubTodayItemListGrid.gridView.setStyles(clubTodayItemListGridBaseInfo.realgrid.styles);
        clubTodayItemListGrid.gridView.setDisplayOptions(clubTodayItemListGridBaseInfo.realgrid.displayOptions);
        clubTodayItemListGrid.gridView.setColumns(clubTodayItemListGridBaseInfo.realgrid.columns);
        clubTodayItemListGrid.gridView.setOptions(clubTodayItemListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        clubTodayItemListGrid.dataProvider.setFields(clubTodayItemListGridBaseInfo.dataProvider.fields);
        clubTodayItemListGrid.dataProvider.setOptions(clubTodayItemListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        clubTodayItemListGrid.gridView.onDataCellClicked = function(gridView, index) {

        };
    },
    setData : function(dataList) {
        clubTodayItemListGrid.dataProvider.clearRows();
        clubTodayItemListGrid.dataProvider.setRows(dataList);

    },
    addDataList : function (dataList) {
        clubTodayItemListGrid.dataProvider.addRows(dataList);
    },
    addData : function (data) {
        clubTodayItemListGrid.dataProvider.addRow(data);
    },
    delDataList : function(dataList){
        clubTodayItemListGrid.dataProvider.removeRows(dataList,false);
    },
    delData : function(data){
        clubTodayItemListGrid.dataProvider.removeRow(data);
    }
};
var clubTodayItem = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#addItemPopUp').bindClick(clubTodayItem.addItemPopUp);
    },
    /**
     * 선택한 row 삭제
     */
    removeCheckData : function() {
        var check = clubTodayItemListGrid.gridView.getCheckedRows(true);

        if (check.length == 0) {
            alert("삭제하실 대상을 선택해 주세요.");
            return;
        }
        clubTodayItemListGrid.dataProvider.removeRows(check, false);
        clubTodayItem.resetItemPriority();
    },
    /**
     * 그리드 row 이동
     * @param obj
     * @returns {boolean}
     */
    moveRow : function(obj) {
        var checkedRows = clubTodayItemListGrid.gridView.getCheckedRows(false);

        if (checkedRows.length == 0) {
            alert("순서변경할 상품 선택해 주세요.");
            return false;
        }

        realGridMoveRow(clubTodayItemListGrid, $(obj).attr("key"), checkedRows);

        // Priority 정렬
        clubTodayItem.resetItemPriority();

    },

    /**
     * 상품 우선순위 재 정렬
     */
    resetItemPriority : function(){
        // Priority 정렬
        clubTodayItemListGrid.dataProvider.getJsonRows(0,-1).forEach(function (data, index) {

            var itemInfo = clubTodayItemListGrid.gridView.getValues(index);
            itemInfo.priority = parseInt(index) + 1
            clubTodayItemListGrid.gridView.setValues(index , itemInfo ,true);

        });
    },
    /**
     * 상품조회 팝업
     */
    addItemPopUp : function() {

        $.jUtil.openNewPopup('/common/popup/itemPop?callback=clubTodayItem.setItemList&isMulti=Y&mallType=TD&&storeType=CLUB', 1084, 650);
    },
    /**
     * 상품리스트 설정
     */
    setItemList : function(itemList) {

        var gridList = clubTodayItemListGrid.dataProvider.getJsonRows();

        itemList.forEach(function (item) {
            gridList.forEach(function (gridItem, idx) {
                if(gridItem.itemNo == item.itemNo ){
                    gridList.splice(idx, 1)
                }
            });
        });
        if( (gridList.length + itemList.length ) > 20){
            alert("최대 20개까지 등록할 수 있습니다.");
            return false;
        }

        clubTodayItemListGrid.dataProvider.clearRows();
        clubTodayItemListGrid.setData(itemList);
        clubTodayItemListGrid.addDataList(gridList);
        clubTodayItem.resetItemPriority();

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        clubTodayItem.resetForm();

        var rowDataJson = clubTodayItemListGrid.dataProvider.getJsonRow(selectRowId);

    }

}

// 클럽투데이 그리드
var clubTodayListGrid = {

    gridView : new RealGridJS.GridView("clubTodayListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),

    init : function() {

        clubTodayListGrid.initGrid();
        clubTodayListGrid.initDataProvider();
        clubTodayListGrid.event();

    },
    initGrid : function() {

        clubTodayListGrid.gridView.setDataSource(clubTodayListGrid.dataProvider);
        clubTodayListGrid.gridView.setStyles(clubTodayListGridBaseInfo.realgrid.styles);
        clubTodayListGrid.gridView.setDisplayOptions(clubTodayListGridBaseInfo.realgrid.displayOptions);
        clubTodayListGrid.gridView.setColumns(clubTodayListGridBaseInfo.realgrid.columns);
        clubTodayListGrid.gridView.setOptions(clubTodayListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {

        clubTodayListGrid.dataProvider.setFields(clubTodayListGridBaseInfo.dataProvider.fields);
        clubTodayListGrid.dataProvider.setOptions(clubTodayListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        clubTodayListGrid.gridView.onDataCellClicked = function(gridView, index) {
            clubToday.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        clubTodayListGrid.dataProvider.clearRows();
        clubTodayListGrid.dataProvider.setRows(dataList);

    }

};

// 클럽투데이관리
var clubToday = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
        this.initSearchDate('-7d');
        this.initSetDate('-7d');
    },
    initSearchDate : function (flag) {
        clubToday.setDate = Calendar.datePickerRange('searchStartDt', 'searchEndDt');
        clubToday.setDate.setEndDate(0);
        clubToday.setDate.setStartDate(flag);
        $("#searchEndDt").datepicker("option", "minDate", $("#searchStartDt").val());
    },
    initSetDate : function (flag) {

        clubToday.setDate = Calendar.datePickerRange('dispStartDt', 'dispEndDt');
        clubToday.setDate.setEndDate(0);
        clubToday.setDate.setStartDate(flag);
        $("#dispEndDt").datepicker("option", "minDate", $("#dispStartDt").val());

    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        //검색 역영
        $('#searchBtn').bindClick(clubToday.search);   // 검색
        $('#searchResetBtn').bindClick(clubToday.searchFormReset); // 검색폼 초기화

        $('#setClubTodayBtn').bindClick(clubToday.setClubToday); // 클럽투데이 정보 저장
        $('#resetBtn').bindClick(clubToday.resetForm); // 클럽투데이 정보 저장
        $('input[name="fileArr"]').change(function() {
            clubToday.img.addImg();
        });
        $('#todayNm').calcTextLength('keyup', '#todayNmCnt');
        $('#todayTitle').calcTextLength('keyup', '#todayTitleCnt');

    },
    /**
     * 클럽투데이 검색
     */
    search : function() {

        var startDt = $("#searchStartDt");
        var endDt = $("#searchEndDt");
        var searchKeyword = $("#searchKeyword").val();
        var searchType = $("#searchType").val();


        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            clubToday.initSearchDate('-7d');
            return false;
        }

        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("최대 2년 범위까지만 조회 가능합니다. \n조회 기간을 다시 설정해주세요.");
            startDt.val(moment(endDt.val()).add(-2, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        if (searchKeyword != "") {
            if ($.jUtil.isNotAllowInput(searchKeyword, ['SPC_SCH'])) {
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            } else if (searchKeyword.length < 2 && searchType !=='TODAYNO')  {
                alert('검색 키워드는 2자 이상 입력해주세요.');
                return false;
            }
        }

        CommonAjax.basic({url:'/manage/clubToday/getClubTodayList.json?' + $('#clubTodaySearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                clubToday.resetForm();
                clubTodayListGrid.setData(res);
                $('#clubTodayTotalCount').html($.jUtil.comma(clubTodayListGrid.gridView.getItemCount()));
            }});
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        clubToday.initSearchDate('-7d');
        $('#searchType').val('TODAYNM');  // 검색어 구분
        $('#searchPeriodType').val('REGDT');  // 검색어 구분
        $('#searchKeyword').val('');    //검색어
        $('#searchDispYn').val('');    //노출여부

    },

    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {

        clubToday.resetForm();

        var rowDataJson = clubTodayListGrid.dataProvider.getJsonRow(selectRowId);

        $('#todayNm').val(rowDataJson.todayNm);
        $('#todayNmCnt').html(rowDataJson.todayNm.length);
        $('#dispStartDt').val(rowDataJson.dispStartDt);
        $('#dispEndDt').val(rowDataJson.dispEndDt);
        $('#priority').val(rowDataJson.priority);
        $('#dispYn').val(rowDataJson.dispYn);
        $('#todayTitle').val(rowDataJson.todayTitle);
        $('#todayTitleCnt').html(rowDataJson.todayTitle.length);
        $('#todayNo').val(rowDataJson.todayNo);

        //대표 이미지(pc>상세)
        clubToday.img.setImg($('#PCBANNER'), {
            'imgUrl': rowDataJson.pcImgUrl,
            'src'    : hmpImgUrl + "/provide/view?processKey=ClubTodayPcBanner&fileId=" + rowDataJson.pcImgUrl

        });

        //대표 이미지(공통)
        clubToday.img.setImg($('#MOBILEBANNER'), {
            'imgUrl': rowDataJson.mobileImgUrl,
            'src'    : hmpImgUrl + "/provide/view?processKey=ClubTodayMobileBanner&fileId=" + rowDataJson.mobileImgUrl

        });

        CommonAjax.basic({url:'/manage/clubToday/getClubTodayItemList.json?todayNo='+rowDataJson.todayNo
            , data: null
            , method:"GET"
            , successMsg:null
            , callbackFunc:function(res) {
                clubTodayItemListGrid.setData(res);
            }});

    },
    /**
     * 클럽투데이 등록/수정 유효성 검사
     */
    setClubTodayValid : function(){

        if($.jUtil.isEmpty($('#todayNm').val())){
            alert("클럽투데이 명을 입력해 주세요" );
            $("#todayNm").focus();
            return false;

        }
        if($.jUtil.isEmpty($('#priority').val())){
            alert("전시순서를 명을 입력해 주세요" );
            $("#priority").focus();
            return false;

        }
        if (!$.jUtil.isAllowInput( $('#priority').val(), ['NUM'])) {
            alert("숫자만 입력 가능 합니다.");
            $("#priority").val('');
            $("#priority").focus();
            return;
        }

        if($.jUtil.isEmpty($('#todayTitle').val())){
            alert("노출 타이틀을 명을 입력해 주세요" );
            $("#todayTitle").focus();
            return false;

        }
        if($.jUtil.isEmpty($('#pcImgUrl').val())){
            alert("배너(PC) 이미지를 등록해 주세요." );
            return false;

        }
        if($.jUtil.isEmpty($('#mobileImgUrl').val())){
            alert("배너(모바일) 이미지를 등록해 주세요." );
            return false;

        }

        var itemList = clubTodayItemListGrid.dataProvider.getJsonRows();
        if(itemList.length < 10){

            alert("최소 10개의 상품을 등록해 주세요." );
            return false;
        }

        return true;
    },
    /**
     * 클럽투데이 등록/수정
     */
    setClubToday : function() {

        if(clubToday.setClubTodayValid()){

            var setClubToday = $('#clubTodaySetForm').serializeObject(true);
            setClubToday.itemList = clubTodayItemListGrid.dataProvider.getJsonRows();

            CommonAjax.basic({url:'/manage/clubToday/setClubToday.json',data: JSON.stringify(setClubToday), method:"POST", successMsg:null, contentType:"application/json",callbackFunc:function(res) {
                    alert(res.returnMsg);
                    clubToday.search();
                }});
        }

    },
    /**
     * 클럽투데이 입력/수정 폼 초기화
     */
    resetForm : function() {

        $('#todayNm').val('');
        $('#todayNmCnt').html('0');
        clubToday.initSetDate('-7d');
        $('#priority').val('');
        $('#dispYn').val('');
        $('#todayTitle').val('');
        $('#todayTitleCnt').html('0');
        $('#todayNo').val('');
        $('#dispYn').val('Y');

        clubToday.img.resetImg();
        clubTodayItemListGrid.dataProvider.clearRows();
    }

};
clubToday.img = {
    imgDiv : null,
    imgType : null,
    /**
     * 이미지 초기화
     */
    init : function() {
        clubToday.img.resetImg();
        clubToday.img.imgDiv = null;
        clubToday.img.imgType = null;
    },
    /**
     * 이미지 초기화
     * @param obj
     * @param params
     */
    resetImg : function(){
        $('.uploadType').each(function(){ clubToday.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {
        if (params) {
            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', params.src);
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {
            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {



        if ($('#itemFile').get(0).files[0].size > 2097152) {
            return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
        } else if ($('#itemFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        var file = $('#itemFile');
        var params;

        if(clubToday.img.imgType == 'PCBANNER'){
            params = {
                processKey : 'ClubTodayPcBanner',
                mode : 'IMG'
            };

        } else{
            params = {
                processKey : 'ClubTodayMobileBanner',
                mode : 'IMG'
            };
        }


        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                var errorMsg = "";
                for (var i in resData.fileArr) {
                    var f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        errorMsg += f.error;
                    } else {
                        clubToday.img.setImg(clubToday.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'    : hmpImgUrl + "/provide/view?processKey=" +params.processKey+"&fileId=" + f.fileStoreInfo.fileId

                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        clubToday.img.setImg($('#'+imgType), null, true);
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        clubToday.img.imgDiv = $('#'+imgType);
        clubToday.img.imgType = imgType;
        $('input[name=fileArr]').click();
    }
};