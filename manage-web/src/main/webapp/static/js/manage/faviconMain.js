/**
 * 사이트관리 > 파비콘 관리
 */
$(document).ready(function() {
    faviconListGrid.init();
    favicon.init();
    CommonAjaxBlockUI.global();

});

// 파비콘 리스트 그리드
var faviconListGrid ={

    gridView : new RealGridJS.GridView("faviconListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        faviconListGrid.initGrid();
        faviconListGrid.initDataProvider();
        faviconListGrid.event();
    },
    initGrid : function() {

        faviconListGrid.gridView.setDataSource(faviconListGrid.dataProvider);
        faviconListGrid.gridView.setStyles(faviconListGridBaseInfo.realgrid.styles);
        faviconListGrid.gridView.setDisplayOptions(faviconListGridBaseInfo.realgrid.displayOptions);
        faviconListGrid.gridView.setColumns(faviconListGridBaseInfo.realgrid.columns);
        faviconListGrid.gridView.setOptions(faviconListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        faviconListGrid.dataProvider.setFields(faviconListGridBaseInfo.dataProvider.fields);
        faviconListGrid.dataProvider.setOptions(faviconListGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        faviconListGrid.dataProvider.clearRows();
        faviconListGrid.dataProvider.setRows(dataList);
    },
    event : function() {
        // 그리드 선택
        faviconListGrid.gridView.onDataCellClicked = function(gridView, index) {
            favicon.gridRowSelect(index.dataRow);
        };
    },
};

//파비톤 관
var favicon = {

    init : function() {

        $('.iosImgArea').css("display", "none");
        $('.aosImgArea').css("display", "none");

        this.bindingEvent();

        favicon.initCalendarDate("searchStartDt", "searchEndDt", false, "10d");
        favicon.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");

        favicon.img.init();
    },
    /**
     * 날짜 셋팅
     * @param startId
     * @param endId
     * @param isTimeStamp
     * @param flag
     */
    initCalendarDate : function (startId, endId, isTimeStamp, flag) {

        var startObj= $("#"+startId);
        var endObj  = $("#"+endId);

        if ( isTimeStamp == true) {
            favicon.setDate = CalendarTime.datePickerRange(startId, endId,{timeFormat: 'HH:mm:ss'});
            favicon.setDate.setStartDate(0);
            favicon.setDate.setEndDateByTimeStamp(flag);

        } else {
            favicon.setDate = Calendar.datePickerRange(startId, endId);
            favicon.setDate.setStartDate(0);
            favicon.setDate.setEndDate(flag);
        }

        endObj.datepicker("option", "minDate", startObj.val());
    },

    initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {

        favicon.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

        favicon.setDateTime.setEndDateByTimeStamp(flag);
        favicon.setDateTime.setStartDate(0);

        $("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {

        $('#searchBtn').bindClick(favicon.search);
        $('#searchResetBtn').bindClick(favicon.searchFormReset);

        $('#setFaviconBtn').bindClick(favicon.setFavicon);
        $('#resetBtn').bindClick(favicon.resetForm);
        $('#device').bindChange(favicon.changeDevice);
        $('#bannerNm').calcTextLength('keyup', '#textCountKr_keyword');

        //이미지
        favicon.img.init();

        //파일선택시
        $('input[name="fileArr"]').change(function() {
            favicon.img.addImg();
        });

        $('.uploadType').bindClick(favicon.img.imagePreview);

    },
    /**
     * 파비콘 리스트 검색
     * @returns {boolean}
     */
    search : function() {

        if(!favicon.valid.search()){
            return false;
        }
        CommonAjax.basic({
            url: '/manage/favicon/getFaviconList.json?' + $('#faviconSearchForm').serialize(),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                faviconListGrid.setData(res);
                $('#faviconTotalCount').html($.jUtil.comma(faviconListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchFormReset : function() {

        favicon.initCalendarDate("searchStartDt", "searchEndDt", false, "10d");

        $('#searchSiteType').val("HOME");
        $('#searchDevice').val("PC");
        $('#searchUseYn').val('');
        $('#schType').val("bannerNm");
        $('#schKeyword').val('');
    },
    /**
     * 공지사항 등록/수정
     */
    setFavicon : function() {

        if(!favicon.valid.set()){
            return false;
        }

        let setFavicon = $('#faviconSetForm').serializeObject(true);
        
        CommonAjax.basic({
            url:'/manage/favicon/setFavicon.json',
            data: JSON.stringify(setFavicon),
            method:"POST",
            successMsg:null,
            contentType:"application/json"
            ,callbackFunc:function(res) {
                alert(res.returnMsg);
                favicon.resetForm();
                favicon.search();
            }});
    },
    /**
     * 공지사항 입력/수정 폼 초기화
     */
    resetForm : function() {

        $('input:radio[name="siteType"]:input[value="HOME"]').trigger('click');

        $('#bannerNm').val('');
        $('#device').val("PC");
        $('#useYn').val('Y');
        $('#faviconNo').val('');

        favicon.initDateTime('10d', 'dispStartDt', 'dispEndDt', "HH:00:00", "HH:59:59");
        favicon.changeDevice();
        $('#textCountKr_keyword').text(0);

        favicon.img.resetImg();
    },
    /**
     * 디바이스 변경
     */
    changeDevice : function () {
        
        let device = $('#device').val();

        switch(device) {

            case "PC" :
                $('.pcImgArea').css("display", "");
                $('.iosImgArea').css("display", "none");
                $('.aosImgArea').css("display", "none");
                break;

            case "IOS" :
                $('.pcImgArea').css("display", "none");
                $('.iosImgArea').css("display", "");
                $('.aosImgArea').css("display", "none");
                break;

            case "ANDROID" :
                $('.pcImgArea').css("display", "none");
                $('.iosImgArea').css("display", "none");
                $('.aosImgArea').css("display", "");
                break;
        }

    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        
        favicon.resetForm();
        
        let rowDataJson = faviconListGrid.dataProvider.getJsonRow(selectRowId);

        $('input:radio[name="siteType"]:input[value="'+rowDataJson.siteType+'"]').trigger('click');
        $('#bannerNm').val(rowDataJson.bannerNm);
        $('#bannerNm').setTextLength('#textCountKr_keyword');

        $('#device').val(rowDataJson.device);

        $('#dispStartDt').val(moment(rowDataJson.dispStartDt).format("YYYY-MM-DD HH:mm:ss"));
        $('#dispEndDt').val(moment(rowDataJson.dispEndDt).format("YYYY-MM-DD HH:mm:ss"));

        $('#faviconNo').val(rowDataJson.faviconNo);


        favicon.changeDevice();

        CommonAjax.basic({
            url:'/manage/favicon/getFaviconImgList.json?faviconNo=' + rowDataJson.faviconNo ,
            data:null ,
            method:"GET" ,
            successMsg:null ,
            callbackFunc:function(res) {
                let faviconImgList = res;
                for (let i in faviconImgList) {
                    favicon.img.setImg($('#'+faviconImgList[i].imgType), {
                        'imgNo' : faviconImgList[i].imgNo,
                        'imgNm' : faviconImgList[i].imgNm,
                        'imgUrl': faviconImgList[i].imgUrl,
                        'useYn' : faviconImgList[i].useYn,
                        'src'   : faviconImgList[i].imgUrl
                    });
                }
            }});
    }
};

/**
 * validation
 * @type {{search: eventNotice.valid.search, set: eventNotice.valid.set}
 */
favicon.valid = {

    search : function () {

        // 날짜 체크
        let startDt = $("#searchStartDt");
        let endDt = $("#searchEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val()))) {
            alert("검색기간을 입력해주세요.")
            $("#schStartDt").focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#schStartDt").focus();
            return false;
        }
        if (moment(endDt.val()).diff(startDt.val(), "year", true) > 3) {
            alert("검색 기간은 최대 1년 범위까지만 가능합니다.");
            startDt.val(moment(endDt.val()).add(-1, "year").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("검색기간의 시작일은 종료일보다 클 수 없습니다.");
            startDt.val(endDt.val());
            return false;
        }

        let schKeyword = $('#schKeyword').val();
        if(schKeyword != "") {
            if ($.jUtil.isNotAllowInput(schKeyword, ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#schKeyword').focus();
                return false;
            }
            if(schKeyword.length < 2){
                alert("검색어는 2자리 이상 입력해 주세요");
                $('#schKeyword').focus();
                return false;
            }
        }
        return true;

    },
    set : function(){

        if( $.jUtil.isEmpty($('#device').val()) || $.jUtil.isEmpty($('#bannerNm').val())) {
            alert("필수 입력 항목을 선택/입력해주세요.");
            return false;
        }

        // 날짜 체크
        let startDt = $("#dispStartDt");
        let endDt = $("#dispEndDt");

        if ($.jUtil.isEmpty(startDt.val()) || ($.jUtil.isEmpty(endDt.val())))  {
            alert("전시기간을 확인해주세요.")
            $('#dispStartDt').focus();
            return false;
        }
        if(!moment(startDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid() || !moment(endDt.val(),'YYYY-MM-DD HH:mm:ss',true).isValid()) {
            alert("날짜 형식이 맞지 않습니다");
            $('#dispStartDt').focus();
            return false;
        }
        
        if(moment(endDt.val()).diff(startDt.val(), "day", true) < 0) {
            alert("종료일시가 시작일시 보다 빠릅니다. ");
            startDt.val(endDt.val());
            return false;
        }

        if(moment(startDt.val()).diff(endDt.val(), "day", true) > 0) {
            alert("시작일시가 종료 보다 늦습니다. ");
            startDt.val(endDt.val());
            return false;
        }

        let form = $('#faviconSetForm').serializeObject(true);

        //이미지 등록 여부 조회
        for(let i in form.faviconImg) {
            let f = form.faviconImg[i];

            if (f.device == $('#device').val() && f.required == 'Y') {
                if (f.imgUrl == '' || f.imgUrl == 'undefined') {
                    alert("필수 이미지를 등록해 주세요.");
                    return false;
                }
            }
        }
        return true;
    }

};

/**
 * 파비콘 관리 이미지 등록 영역
 * @type {{init: favicon.img.init, imgDiv: null, setImg: favicon.img.setImg, deleteImg: favicon.img.deleteImg, addImg: favicon.img.addImg, resetImg: favicon.img.resetImg, imagePreview: favicon.img.imagePreview, clickFile: favicon.img.clickFile}}
 */
favicon.img = {
    imgDiv : null,

    /**
     * 이미지 초기화
     */
    init : function() {
        favicon.img.resetImg();
        favicon.img.imgDiv = null;
    },
    resetImg : function(){
        $('.uploadType').each(function(){ favicon.img.setImg($(this))});
    },
    /**
     * 이미지 정보 설정
     * @param obj
     * @param params
     */
    setImg : function(obj, params) {

        if (params) {

            obj.find('.imgUrl').val(params.imgUrl);
            obj.find('.imgNm').val(params.imgNm);
            obj.find('.imgUrlTag').prop('src', hmpImgUrl + "/provide/view?processKey=" + favicon.img.getProcessKey(obj)+ "&fileId=" + params.src);
            if (!obj.find('.imgNo').val()) {
                obj.find('.imgNo').val(params.imgNo);
            }
            obj.parents('.imgDisplayView').show();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').hide();
        } else {

            obj.find('.imgUrl').val('');
            obj.find('.imgNm').val('');
            obj.find('.imgUrlTag').prop('src', '');
            obj.find('.imgNo').val('');

            obj.parents('.imgDisplayView').hide();
            obj.parents('.imgDisplayView').siblings('.imgDisplayReg').show();
        }
    },
    /**
     * 이미지 업로드
     * @param obj
     */
    addImg : function() {

        if ($('#device').val() == 'PC') {
            if ($('#faviconImgFile').get(0).files[0].size > 1048576) {
                return $.jUtil.alert("용량을 초과하였습니다.\n1MB 이하로 업로드해주세요.");
            }
        }else {
            if ($('#faviconImgFile').get(0).files[0].size > 2097152) {
                return $.jUtil.alert("용량을 초과하였습니다.\n2MB 이하로 업로드해주세요.");
            }
        }
        if ($('#faviconImgFile').get(0).files[0].name.length > 45) {
            return $.jUtil.alert("파일명은 45자(확장자포함) 이내로 입력 가능합니다.");
        }

        let file = $('#faviconImgFile');
        let params = {
            processKey : favicon.img.getProcessKey(favicon.img.imgDiv),
            mode : "IMG"
        };

        CommonAjax.upload({
            file: file,
            data: params,
            callbackFunc: function (resData) {
                let errorMsg = "";
                for (let i in resData.fileArr) {
                    let f = resData.fileArr[i];

                    if (f.hasOwnProperty("error")) {
                        alert(f.error);
                    } else {
                        favicon.img.setImg(favicon.img.imgDiv, {
                            'imgNm' : f.fileStoreInfo.fileName,
                            'imgUrl': f.fileStoreInfo.fileId,
                            'src'   : f.fileStoreInfo.fileId,
                        });
                    }
                }
                if (!$.jUtil.isEmpty(errorMsg)) {
                    alert(errorMsg);
                }
            }
        });
    },
    /**
     * 이미지 미리보기 팝업
     * @param thisParam
     */
    imagePreview : function(obj) {
        let imgUrl = $(obj).find('img').prop('src');

        if(imgUrl) {
            $.jUtil.imgPreviewPopup(imgUrl);
        }
    },
    /**
     * 이미지 삭제 (초기화)
     */
    deleteImg : function(imgType) {
        if (confirm('삭제하시겠습니까?')) {
            favicon.img.setImg($('#' + imgType), null,  true);
        }
    },
    /**
     * 이미지 클릭처리
     * @param obj
     */
    clickFile : function(imgType) {
        favicon.img.imgDiv = $('#'+imgType);
        $('input[name=fileArr]').click();
    },
    /**
     * 파비콘 ProcessKey
     * @param obj
     */
    getProcessKey : function (obj) {

        let prefix = "Favicon";
        let imgType = obj.find('.imgType').val().replace("_", "");
        return prefix + imgType;
    }
};