/**
 * menu depth info
 */

$(document).ready(function() {
    dspCacheManageListGrid.init();
    dspCacheManage.init();
    CommonAjaxBlockUI.global();
});

// dspCacheManage 그리드
var dspCacheManageListGrid = {
    gridView : new RealGridJS.GridView("dspCacheManageListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dspCacheManageListGrid.initGrid();
        dspCacheManageListGrid.initDataProvider();
        dspCacheManageListGrid.event();
    },
    initGrid : function() {
        dspCacheManageListGrid.gridView.setDataSource(dspCacheManageListGrid.dataProvider);
        dspCacheManageListGrid.gridView.setStyles(dspCacheManageListGridBaseInfo.realgrid.styles);
        dspCacheManageListGrid.gridView.setDisplayOptions(dspCacheManageListGridBaseInfo.realgrid.displayOptions);
        dspCacheManageListGrid.gridView.setColumns(dspCacheManageListGridBaseInfo.realgrid.columns);
        dspCacheManageListGrid.gridView.setOptions(dspCacheManageListGridBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        dspCacheManageListGrid.dataProvider.setFields(dspCacheManageListGridBaseInfo.dataProvider.fields);
        dspCacheManageListGrid.dataProvider.setOptions(dspCacheManageListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dspCacheManageListGrid.gridView.onDataCellClicked = function(gridView, index) {
          dspCacheManage.gridRowSelect(index.dataRow);
        };
    },
    setData : function(dataList) {
        dspCacheManageListGrid.dataProvider.clearRows();
        dspCacheManageListGrid.dataProvider.setRows(dataList);
    }
};

// dspCacheManage 관리
var dspCacheManage = {
    /**
     * 초기화
     */
    init : function() {
        this.bindingEvent();
    },
    /**
     * 이벤트 바인딩
     */
    bindingEvent : function() {
        $('#searchBtn').bindClick(dspCacheManage.search); //dspCacheManage 검색
        $('#searchResetBtn').bindClick(dspCacheManage.searchReset); //dspCacheManage 검색폼 초기화
        $('#setBtn').bindClick(dspCacheManage.set); //dspCacheManage 등록,수정
        $('#resetBtn').bindClick(dspCacheManage.reset); //dspCacheManage 입력폼 초기화
        $('#btnForceUpdate').bindClick(dspCacheManage.forceUpdate); // Cache 강제 적용 ( 임시 )
    },
    /**
     * dspCacheManage 검색
     */
    search : function() {
        CommonAjax.basic({
            url : "/manage/dspCacheManage/getDspCacheManageList.json?" + $('#dspCacheManageSearchForm').serialize(),
            data : null,
            method : "GET",
            callbackFunc : function (res) {
                dspCacheManage.reset();
                dspCacheManageListGrid.setData(res);
                $('#dspCacheManageTotalCount').html($.jUtil.comma(dspCacheManageListGrid.gridView.getItemCount()));
            }
        });
    },
    /**
     * 검색폼 초기화
     */
    searchReset : function() {
        $('#searchCacheNm').val('');
        $('#searchUseYn').val('');
    },
    /**
     * 그리드 row 선택
     * @param selectRowId
     */
    gridRowSelect : function(selectRowId) {
        var rowDataJson = dspCacheManageListGrid.dataProvider.getJsonRow(selectRowId);
        $('#cacheNm').val(rowDataJson.cacheNm);
        $('#cacheUrl').val(rowDataJson.cacheUrl);
        $('#useYn').val(rowDataJson.useYn);
        $('#manageSeq').val(rowDataJson.manageSeq);
    },
    /**
     * dspCacheManage 등록/수정
     */
    set : function() {
        if(!dspCacheManage .valid.set()){
            return false;
        }


        const setData = $('#dspCacheManageSetForm').serializeObject();
        CommonAjax.basic({
            url : "/manage/dspCacheManage/setDspCacheManage.json",
            data : JSON.stringify(setData),
            method : "POST",
            successMsg:null,
            contentType:"application/json",
            callbackFunc : function (res) {
                alert(res.returnMsg);
                dspCacheManage.search();
            }
        });
    },
    /**
     * dspCacheManage 입력/수정 폼 초기화
     */
    reset : function() {
        $('#manageSeq, #cacheNm, #priority, #useYn, #cacheUrl').val('');
    },
    /**
     * 캐쉬 강제 적용
     */
    forceUpdate : function() {
        const triggerUrl = $('#cacheUrl').val();

        if($.jUtil.isEmpty(triggerUrl)) {
            alert('강제 적용할 캐쉬 목록을 먼저 선택해 주세요.');
            return false;
        }

        if(confirm("해당 Url 호출을 통해 캐쉬 강제 업데이트를 진행 하겠습니까? \nURL : [" + triggerUrl + "]")) {
            CommonAjax.basic({
                url : "/manage/dspCacheManage/setForceCacheUpdate.json?triggerUrl=" + triggerUrl,
                data : null,
                method : "POST",
                successMsg:null,
                contentType:"application/json",
                callbackFunc : function (res) {
                    dspCacheManage.updateCacheInfo(res.returnCnt);
                }
            });

        }
    },
    /**
     * 캐쉬 정보 갱신
     */
    updateCacheInfo : function(cacheCnt) {
        $('#cacheCount').val(cacheCnt);

        const setData = $('#dspCacheManageSetForm').serializeObject();
        CommonAjax.basic({
            url : "/manage/dspCacheManage/setDspCacheInformation.json",
            data : JSON.stringify(setData),
            method : "POST",
            successMsg:null,
            contentType:"application/json",
            callbackFunc : function (res) {
                alert(res.returnMsg);
                dspCacheManage.reset();
                dspCacheManage.search();
            }
        });
    }
};
    /**
     * dspCacheManage 검색,입력,수정 validation check
     */
dspCacheManage.valid = {
    set : function () {

        if($.jUtil.isEmpty($('#cacheNm').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#cacheNm').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#cacheUrl').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#cacheUrl').focus();
            return false;
        }
        if($.jUtil.isEmpty($('#useYn').val())) {
            alert("필수 입력 항목을 선택/입력 해주세요.");
            $('#useYn').focus();
            return false;
        }
        return true;
    }
};