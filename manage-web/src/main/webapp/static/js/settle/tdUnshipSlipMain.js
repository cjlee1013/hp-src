/** Main Script */
var tdUnshipSlipMain = {
    /**
     * 전역변수
     */
    global: {
        currentDate : ""
        , currntStoreType : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        tdUnshipSlipMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // [검색] 버튼
        $("#schBtn").bindClick(tdUnshipSlipMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(tdUnshipSlipMain.reset, "tdUnshipSlipSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(tdUnshipSlipMain.excelDownload);
        // [생성] 버튼
        $("#createSlip").bindClick(tdUnshipSlipMain.createSlip);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#tdUnshipSlipSearchForm").serialize();
        CommonAjax.basic({
            url         : '/settle/unshipSlip/getTdUnshipSlipList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                if(res.length > 0) {
                    $('#createSlip').prop("disabled",true);
                } else {
                    $('#createSlip').prop("disabled",false);
                }

                tdUnshipSlipMain.global.currentDate = $('#schYear').val() + $('#schMonth').val();
                tdUnshipSlipMain.global.currntStoreType = $('#schStoreType').val();

                tdUnshipSlipGrid.setData(res);
                $('#tdUnshipSlipSearchCnt').html($.jUtil.comma(tdUnshipSlipGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = [ "amt20800", "amt30376", "amt24290", "amt30200", "amt35000", "amt14160"];
                tdUnshipSlipMain.setRealGridHeaderSum(tdUnshipSlipGrid.gridView, stringCol);
            }
        });
    },

    /**
     * 전표 생성
     */
    createSlip: function() {
        var reqData = $("#tdUnshipSlipSearchForm").serializeObject();

        if (confirm("전표를 생성하시겠습니까?\n취소가 불가능하며 30분 이상 소요됩니다.")) {
            CommonAjax.basic({
                url         : '/settle/unshipSlip/setTdUnshipSlip.json',
                data        : JSON.stringify(reqData),
                method      : "POST",
                contentType : "application/json; charset=utf-8",
                callbackFunc: function() {
                    alert("전표가 생성이 시작됐습니다.\n대략 30분 후 상세데이터를 확인해 주세요.");
                    //tdUnshipSlipMain.search();
                }
            });
        }
    },

    /**
     * 전표 생성 가능 확인
     */
    isCheckSlip: function() {
        var reqData = $("#tdUnshipSlipSearchForm").serialize();
        var isCheck = false;

        CommonAjax.basic({
            url         : '/settle/unshipSlip/getTdUnshipSlipCnt.json?' + reqData,
            data        : null,
            method      : "GET",
            async       : false,
            callbackFunc: function(res) {
                if (res > 0) {
                    isCheck = true;
                } else {
                    isCheck = false;
                }
            }
        });

        return isCheck;
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (tdUnshipSlipGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  "TD미배송전표조회_" + tdUnshipSlipMain.global.currentDate + "_" + tdUnshipSlipMain.global.currntStoreType;
        tdUnshipSlipGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        var year = moment().format("YYYY");
        var month = moment().format("MM");

        $('#schMonth').val(month);
        $('#schYear').val(year);
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        return true;
    }
}

/** Grid Script */
var tdUnshipSlipGrid = {
    gridView: new RealGridJS.GridView("tdUnshipSlipGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        tdUnshipSlipGrid.initGrid();
        tdUnshipSlipGrid.initDataProvider();
    },
    initGrid: function () {
        tdUnshipSlipGrid.gridView.setDataSource(tdUnshipSlipGrid.dataProvider);
        tdUnshipSlipGrid.gridView.setStyles(tdUnshipSlipGridBaseInfo.realgrid.styles);
        tdUnshipSlipGrid.gridView.setDisplayOptions(tdUnshipSlipGridBaseInfo.realgrid.displayOptions);
        tdUnshipSlipGrid.gridView.setColumns(tdUnshipSlipGridBaseInfo.realgrid.columns);
        tdUnshipSlipGrid.gridView.setOptions(tdUnshipSlipGridBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = ["basicDt", "storeType","costCenterName","costCenter","company", "category"];
        SettleCommon.setRealGridSumHeader(tdUnshipSlipGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        tdUnshipSlipGrid.dataProvider.setFields(tdUnshipSlipGridBaseInfo.dataProvider.fields);
        tdUnshipSlipGrid.dataProvider.setOptions(tdUnshipSlipGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        tdUnshipSlipGrid.dataProvider.clearRows();
        tdUnshipSlipGrid.dataProvider.setRows(dataList);
    }
};