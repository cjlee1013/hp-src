/***
 * 부가세 신고내역 그리드 설정
 */
var monthlySalesVATListGrid = {
    gridView : new RealGridJS.GridView("monthlySalesVATListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        monthlySalesVATListGrid.initGrid();
        monthlySalesVATListGrid.initDataProvider();
        monthlySalesVATListGrid.event();
    },
    initGrid : function () {
        monthlySalesVATListGrid.gridView.setDataSource(monthlySalesVATListGrid.dataProvider);
        monthlySalesVATListGrid.gridView.setStyles(monthlySalesVATListBaseInfo.realgrid.styles);
        monthlySalesVATListGrid.gridView.setDisplayOptions(monthlySalesVATListBaseInfo.realgrid.displayOptions);
        monthlySalesVATListGrid.gridView.setOptions(monthlySalesVATListBaseInfo.realgrid.options);
        monthlySalesVATListGrid.gridView.setColumns(monthlySalesVATListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "partnerId", "vendorCd", "partnerNm", "businessNm", "partnerNo" ] ];
        SettleCommon.setRealGridSumHeader(monthlySalesVATListGrid.gridView, mergeCells);

        //컬럼 그룹핑 너비 조정
        monthlySalesVATListGrid.gridView.setColumnProperty("과세", "displayWidth", 600);
        monthlySalesVATListGrid.gridView.setColumnProperty("면세", "displayWidth", 600);
    },
    initDataProvider : function() {
        monthlySalesVATListGrid.dataProvider.setFields(monthlySalesVATListBaseInfo.dataProvider.fields);
        monthlySalesVATListGrid.dataProvider.setOptions(monthlySalesVATListBaseInfo.dataProvider.options);
    },
    event : function() {
        //이미지 버튼 클릭시 Action
        monthlySalesVATListGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            var partnerId = monthlySalesVATListGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).orgPartnerId;
            var basicDt = monthlySalesVATListGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).basicDt;
            var startDt = basicDt + '-01';
            var endDt = moment(startDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
            startDt = moment(startDt, 'YYYY-MM-DD', true).format("YYYYMMDD");

            var url = "/settle/monthlySalesVAT/getMonthlySalesVATOrder.json?partnerId="+ partnerId +"&startDt="+ startDt +"&endDt="+ endDt;
            CommonAjax.basic({
                url: url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    monthlySalesVATDetailGrid.setData(res);

                    CommonAjax.basic({
                        url:'/settle/monthlySalesVAT/getMonthlyAdjustList.json?startDt='+ startDt +'&endDt='+ endDt +'&partnerId='+ partnerId,
                        data: null,
                        contentType: 'application/json',
                        method: "GET",
                        successMsg: null,
                        callbackFunc:function(res) {
                            adjustListGrid.setData(res);
                            monthlySalesVATDetailGrid.excelDownload(partnerId, basicDt);
                        }
                    });

                }
            });
        };
    },
    setData : function(dataList) {
        monthlySalesVATListGrid.dataProvider.clearRows();
        monthlySalesVATListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(monthlySalesVATListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridColumnVisible(monthlySalesVATListGrid.gridView, [SettleCommon.getRealGridColumnName(monthlySalesVATListGrid.gridView, "주문상세내역")], false);

        var fileName = monthlySalesVAT.setExcelFileName("부가세신고내역");
        monthlySalesVATListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            allColumns : false,
            //showProgress: true,
            applyDynamicStyles : true,
            pagingAllItems : true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

        SettleCommon.setRealGridColumnVisible(monthlySalesVATListGrid.gridView, [SettleCommon.getRealGridColumnName(monthlySalesVATListGrid.gridView, "주문상세내역")], true);

    }
};
/*
부가세 신고내역 상세 그리드
*/

var monthlySalesVATDetailGrid = {
    gridView : new RealGridJS.GridView("monthlySalesVATDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        monthlySalesVATDetailGrid.initGrid();
        monthlySalesVATDetailGrid.initDataProvider();
    },
    initGrid : function () {
        monthlySalesVATDetailGrid.gridView.setDataSource(monthlySalesVATDetailGrid.dataProvider);
        monthlySalesVATDetailGrid.gridView.setStyles(monthlySalesVATDetailBaseInfo.realgrid.styles);
        monthlySalesVATDetailGrid.gridView.setDisplayOptions(monthlySalesVATDetailBaseInfo.realgrid.displayOptions);
        monthlySalesVATDetailGrid.gridView.setOptions(monthlySalesVATDetailBaseInfo.realgrid.options);
        monthlySalesVATDetailGrid.gridView.setColumns(monthlySalesVATDetailBaseInfo.realgrid.columns);

        //컬럼 그룹핑 너비 조정
        monthlySalesVATDetailGrid.gridView.setColumnProperty("결제 금액", "displayWidth", 500);
        monthlySalesVATDetailGrid.gridView.setColumnProperty("카드할인", "displayWidth", 160);
        monthlySalesVATDetailGrid.gridView.setColumnProperty("상품할인", "displayWidth", 220);
    },
    initDataProvider : function() {
        monthlySalesVATDetailGrid.dataProvider.setFields(monthlySalesVATDetailBaseInfo.dataProvider.fields);
        monthlySalesVATDetailGrid.dataProvider.setOptions(monthlySalesVATDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        monthlySalesVATDetailGrid.dataProvider.clearRows();
        monthlySalesVATDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function(partnerId, basicDt) {
        if(monthlySalesVATDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        var fileName = "부가세신고내역_상세내역_"+partnerId+"_"+ basicDt.replace(/-/gi, "");
        RealGridJS.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: "엑셀 데이터 추줄 중입니다.",
            exportGrids:[
                { grid: monthlySalesVATDetailGrid.gridView, sheetName: "주문상세내역" },
                { grid: adjustListGrid.gridView, sheetName: "조정상세내역" }
            ]
        });

    }
};
/* 정산조정 상세 그리드 */
var adjustListGrid = {
    gridView : new RealGridJS.GridView("adjustListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        adjustListGrid.initGrid();
        adjustListGrid.initDataProvider();
    },
    initGrid : function() {
        adjustListGrid.gridView.setDataSource(adjustListGrid.dataProvider);

        adjustListGrid.gridView.setStyles(adjustListBaseInfo.realgrid.styles);
        adjustListGrid.gridView.setDisplayOptions(adjustListBaseInfo.realgrid.displayOptions);
        adjustListGrid.gridView.setColumns(adjustListBaseInfo.realgrid.columns);
        adjustListGrid.gridView.setOptions(adjustListBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        adjustListGrid.dataProvider.setFields(adjustListBaseInfo.dataProvider.fields);
        adjustListGrid.dataProvider.setOptions(adjustListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        adjustListGrid.dataProvider.clearRows();
        adjustListGrid.dataProvider.setRows(dataList);
    },
};
/***
 * 부가세 신고내역
 */
var monthlySalesVAT = {
    init : function () {
        $('#searchField').val('');
        this.initSearchYM();
        // 입력조건 생성
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,PARTNER_NM,PARTNER_NO,VENDOR_CD");
    },
    initSearchYM : function () {
        var today = new Date();
        var year = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("YYYY");
        var month = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("MM");

        $('#startYear, #endYear').val(year);
        SettleCommon.setMonth(year,'startMonth');
        SettleCommon.setMonth(year,'endMonth');

        $('#startMonth, #endMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(monthlySalesVAT.search);
        $('#searchResetBtn').bindClick(monthlySalesVAT.resetSearchForm);
        $('#excelDownloadBtn').bindClick(monthlySalesVATListGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });
    },
    valid : function () {
        var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        startDt = moment(startDt, 'YYYY-MM-DD', true);
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            var year = $("#startYear").val();
            var month = $("#startMonth").val();
            $("#startYear").val($("#endYear").val());
            $("#startMonth").val($("#endMonth").val());
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
            var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
            $("#endYear").val(year);
            SettleCommon.setMonth(year,'endMonth');
            $("#endMonth").val(month);
            return false;
        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색조건을 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'PARTNER_ID' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'VENDOR_CD' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('업체코드는 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    search : function () {
        if (!monthlySalesVAT.valid()) return false;
        CommonAjaxBlockUI.startBlockUI();

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var buttonInfo = {
            buttonCnt : 1,
            buttonName : "excelDown",
            buttonImg : [ {name : "orderDetail", img : "/static/images/settle/btn_excel.png", width:70} ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["orderDetail"],
            value : [""],
            isValueNotOption : true
        };

        var searchForm = $("form[name=searchForm]").serialize();

        var startDt = $("#startYear").val() + $("#startMonth").val()+ '01';
        var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
        endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "PARTNER_ID") {
            searchForm += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            searchForm += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NO") {
            searchForm += "&partnerNo="+ $('#searchField').val();
        } else if ($('#searchType').val() == "VENDOR_CD") {
            searchForm += "&vendorCd="+ $('#searchField').val();
        }

        CommonAjax.basic(
            {
                url:'/settle/monthlySalesVAT/getMonthlySalesVATList.json?'+ encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    monthlySalesVATListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(monthlySalesVATListGrid.gridView.getItemCount()));

                    //주문상세 엑셀다운로드
                    var targetColumn = SettleCommon.getRealGridColumnName(monthlySalesVATListGrid.gridView, "상세내역");
                    $.each(res, function(_rowId, _data){
                       monthlySalesVATListGrid.dataProvider.setValue(_rowId, targetColumn, "orderDetail");
                    });

                    //건수가 0건이 아닌 경우에만 수행
                    if(monthlySalesVATListGrid.gridView.getItemCount() > 0) {
                        //버튼생성
                        SettleCommon.setRealGridCellButton(monthlySalesVATListGrid.gridView, buttonInfo, targetColumn);
                    }

                    //그룹헤더컬럼 합계
                    var stringCol = ["taxSalesAmt", "taxPgCardAmt", "incomeDeductAmt", "expenditureAmt", "taxPgEtcAmt", "taxHomeDiscountAmt",
                        "freeTaxSalesAmt", "freeTaxPgCardAmt", "freeTaxIncomeDeductAmt", "freeTaxExpenditureAmt", "freeTaxPgEtcAmt", "freeTaxHomeDiscountAmt"];
                    SettleCommon.setRealGridHeaderSum(monthlySalesVATListGrid.gridView, stringCol);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });

    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val();
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            monthlySalesVAT.init();
        });
    }
};