/** Main Script */
var tdUnshipOrderMain = {
    /**
     * 전역변수
     */
    global: {
        currentDate : ""
        , currntStoreType : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        tdUnshipOrderMain.bindingEvent();
        tdUnshipOrderMain.setChangeStoreType();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 점포유형 SelctBox Change
        $("#schStoreType").on('change', function() { tdUnshipOrderMain.setChangeStoreType() });

        // [검색] 버튼
        $("#schBtn").bindClick(tdUnshipOrderMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(tdUnshipOrderMain.reset, "tdUnshipOrderSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(tdUnshipOrderMain.excelDownload);

        // 추가정보 체크박스 버튼
        $("#schShipInfo").bindClick(tdUnshipOrderMain.setShipInfo);

        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd2'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd3'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd4'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd5'), true);
    },

    /**
     * 점포유형 select box 선택
     */
    setChangeStoreType: function() {
        var storeType = $('#schStoreType').val();
        var reqData = "storeType=" + storeType + "&storeRange=REAL";

        CommonAjax.basic({
            url         : '/settle/common/getStoreList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                var html = '<option value="">전체</option>';

                for(let idx in res) {
                    html += '<option value="'+res[idx].originStoreId+'">'+res[idx].storeNm+'</option>';
                }

                $('#schStoreId').html(html);
            }
        });
    },

    /**
     * 추가정보 체크박스 선택
     */
    setShipInfo: function() {
        if($('#schShipInfo').prop("checked")) {
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("bundleNo"), "visible", true);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("shipMethodNm"), "visible", true);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("shippingDt"), "visible", true);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("currentShipStatus"), "visible", true);
        } else {
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("bundleNo"), "visible", false);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("shipMethodNm"), "visible", false);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("shippingDt"), "visible", false);
            tdUnshipOrderGrid.gridView.setColumnProperty(tdUnshipOrderGrid.gridView.columnByName("currentShipStatus"), "visible", false);
        }
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#tdUnshipOrderSearchForm").serialize();
        CommonAjax.basic({
            url         : '/settle/unshipOrder/getTdUnshipOrderList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                tdUnshipOrderMain.global.currentDate = $('#schYear').val() + $('#schMonth').val();
                tdUnshipOrderMain.global.currntStoreType = $('#schStoreType').val();

                tdUnshipOrderGrid.setData(res);
                $('#tdUnshipOrderSearchCnt').html($.jUtil.comma(tdUnshipOrderGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = [ "completeQty", "completeAmt", "empDiscountAmt", "couponChargeSumAmt", "couponSellerChargeAmt","couponCardChargeAmt",
                    "couponHomeChargeAmt", "cardCouponSumAmt", "cardCouponCardAmt", "cardCouponHomeAmt", "salesAmt", "supplyAmt", "vatAmt", "originPrice"];
                tdUnshipOrderMain.setRealGridHeaderSum(tdUnshipOrderGrid.gridView, stringCol);
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (tdUnshipOrderGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridSumHeader(tdUnshipOrderGrid.gridView, []);
        var fileName =  "TD미배송주문조회_" + tdUnshipOrderMain.global.currentDate + "_" + tdUnshipOrderMain.global.currntStoreType;
        tdUnshipOrderGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다.",
            done: function() {
                //헤더 merge
                var mergeCells = [ ["storeType", "mallType","companyCd","companyNm","originStoreId", "storeNm", "division", "deptNo", "categoryNm", "itemNo", "itemNm"]
                                  ,["purchaseOrderNo","orderItemNo"]
                                  ,["paymentBasicDt","shipStatus","shipDt","bundleNo","shipMethodNm","shippingDt"]];
                SettleCommon.setRealGridSumHeader(tdUnshipOrderGrid.gridView, mergeCells);
            }
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        var year = moment().format("YYYY");
        var month = moment().format("MM");

        $('#schMonth').val(month);
        $('#schYear').val(year);

        tdUnshipOrderMain.setChangeStoreType();

        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
        $('#formMappingCateCd1 option:first').prop('selected', true).change();
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        return true;
    }
}

/** Grid Script */
var tdUnshipOrderGrid = {
    gridView: new RealGridJS.GridView("tdUnshipOrderGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        tdUnshipOrderGrid.initGrid();
        tdUnshipOrderGrid.initDataProvider();
    },
    initGrid: function () {
        tdUnshipOrderGrid.gridView.setDataSource(tdUnshipOrderGrid.dataProvider);
        tdUnshipOrderGrid.gridView.setStyles(tdUnshipOrderGridBaseInfo.realgrid.styles);
        tdUnshipOrderGrid.gridView.setDisplayOptions(tdUnshipOrderGridBaseInfo.realgrid.displayOptions);
        tdUnshipOrderGrid.gridView.setColumns(tdUnshipOrderGridBaseInfo.realgrid.columns);
        tdUnshipOrderGrid.gridView.setOptions(tdUnshipOrderGridBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = [ ["storeType", "mallType","companyCd","companyNm","originStoreId", "storeNm", "division", "deptNo", "categoryNm", "itemNo", "itemNm"]
                          ,["purchaseOrderNo","orderItemNo"]
                          ,["paymentBasicDt","shipStatus","shipDt","bundleNo","shipMethodNm","shippingDt"]];
        SettleCommon.setRealGridSumHeader(tdUnshipOrderGrid.gridView, mergeCells);
    },
    initDataProvider: function () {
        tdUnshipOrderGrid.dataProvider.setFields(tdUnshipOrderGridBaseInfo.dataProvider.fields);
        tdUnshipOrderGrid.dataProvider.setOptions(tdUnshipOrderGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        tdUnshipOrderGrid.dataProvider.clearRows();
        tdUnshipOrderGrid.dataProvider.setRows(dataList);
    }
};