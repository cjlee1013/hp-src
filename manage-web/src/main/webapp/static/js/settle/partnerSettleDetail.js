/***
 * 판매자 정산정보 상세 그리드 설정
 */
var partnerSettleGrid = {
    gridView : new RealGridJS.GridView("partnerSettleGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSettleGrid.initGrid();
        partnerSettleGrid.initDataProvider();
        partnerSettleGrid.event();
    },
    initGrid : function () {
        partnerSettleGrid.gridView.setDataSource(partnerSettleGrid.dataProvider);
        partnerSettleGrid.gridView.setStyles(partnerSettleGridBaseInfo.realgrid.styles);
        partnerSettleGrid.gridView.setDisplayOptions(partnerSettleGridBaseInfo.realgrid.displayOptions);
        partnerSettleGrid.gridView.setColumns(partnerSettleGridBaseInfo.realgrid.columns);
        partnerSettleGrid.gridView.setOptions(partnerSettleGridBaseInfo.realgrid.options);

        //헤더 merge 컬럼 나열
        var mergeCells = [ ["settlePayState", "settleState", "settleSrl", "settlePreDt", "payCompleteDt", "payMethodNm", "settleCycleType", "salesPeriod", "partnerId", "vendorCd", "partnerNm"],
            ["chgDt", "chgId", "bankInfo"] ];

        //그리드 상단 합계
        SettleCommon.setRealGridSumHeader(partnerSettleGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        partnerSettleGrid.dataProvider.setFields(partnerSettleGridBaseInfo.dataProvider.fields);
        partnerSettleGrid.dataProvider.setOptions(partnerSettleGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        partnerSettleGrid.gridView.onDataCellClicked = function(gridView, index) {
            partnerSettleGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        partnerSettleGrid.dataProvider.clearRows();
        partnerSettleGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        var rowDataJson = partnerSettleGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/partnerSettleDetail/getPartnerSettleItem.json?settleSrl='+ rowDataJson.settleSrl;
        CommonAjax.basic({
            url: _url,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                partnerSettleDetailGrid.setData(res);
                $('#partnerSettleDetailCnt').html($.jUtil.comma(partnerSettleDetailGrid.gridView.getItemCount()));

                //합계
                var stringCol = ["couponChargeAmt", "couponSellerChargeAmt", "couponHomeChargeAmt", "couponCardChargeAmt", "cardCouponAmt", "cardCouponHomeAmt", "cardCouponCardAmt"];
                SettleCommon.setRealGridHeaderSum(partnerSettleDetailGrid.gridView, stringCol);
            }
        });
    },
    excelDownload: function() {
        if(partnerSettleGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = partnerSettleForm.setExcelFileName("판매자 정산정보");

        partnerSettleGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 판매자 정산정보 상세 그리드
 */
var partnerSettleDetailGrid = {
    gridView : new RealGridJS.GridView("partnerSettleDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSettleDetailGrid.initGrid();
        partnerSettleDetailGrid.initDataProvider();
    },
    initGrid : function () {
        partnerSettleDetailGrid.gridView.setDataSource(partnerSettleDetailGrid.dataProvider);
        partnerSettleDetailGrid.gridView.setStyles(partnerSettleDetailBaseInfo.realgrid.styles);
        partnerSettleDetailGrid.gridView.setDisplayOptions(partnerSettleDetailBaseInfo.realgrid.displayOptions);
        partnerSettleDetailGrid.gridView.setColumns(partnerSettleDetailBaseInfo.realgrid.columns);
        partnerSettleDetailGrid.gridView.setOptions(partnerSettleDetailBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = [ ["basicDt", "partnerId", "vendorCd", "partnerNm", "orderType", "itemNo", "itemNm", "taxYn" ] ];

        //그리드 상단 합계
        SettleCommon.setRealGridSumHeader(partnerSettleDetailGrid.gridView, mergeCells);

        //컬럼 그룹핑 너비 조정
        partnerSettleDetailGrid.gridView.setColumnProperty("상품할인", "displayWidth", 240);
        partnerSettleDetailGrid.gridView.setColumnProperty("카드할인", "displayWidth", 180);

    },
    initDataProvider : function() {
        partnerSettleGrid.dataProvider.setFields(partnerSettleGridBaseInfo.dataProvider.fields);
        partnerSettleGrid.dataProvider.setOptions(partnerSettleGridBaseInfo.dataProvider.options);
    },
    initDataProvider : function() {
        partnerSettleDetailGrid.dataProvider.setFields(partnerSettleDetailBaseInfo.dataProvider.fields);
        partnerSettleDetailGrid.dataProvider.setOptions(partnerSettleDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        partnerSettleDetailGrid.dataProvider.clearRows();
        partnerSettleDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerSettleDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = partnerSettleForm.setExcelFileName("판매자정산정보_상품별정산상세내역");

        partnerSettleDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 판매자 정산정보 상세 상세
 */
var partnerSettleForm = {
    init : function () {
        this.initSearchDate('-1d');

        CommonAjax.basic({
            url:'/settle/partnerSettleDetail/getPartnerSettleList.json?settleSrl='+ $('#settleSrl').val(),
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                partnerSettleGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerSettleGrid.gridView.getItemCount()));

                if (partnerSettleGrid.gridView.getItemCount() > 0) {
                    $('#partnerNm').text(partnerSettleGrid.dataProvider.getJsonRow(0).partnerNm);
                    $('#partnerOwner').text(partnerSettleGrid.dataProvider.getJsonRow(0).partnerOwner);
                    $('#operatorType').text(partnerSettleGrid.dataProvider.getJsonRow(0).operatorType);
                    $('#partnerNo').text(partnerSettleGrid.dataProvider.getJsonRow(0).partnerNo);
                    $('#bankAccountNo').text(partnerSettleGrid.dataProvider.getJsonRow(0).bankAccountNo);
                }

                //합계
                var stringCol = [];
                SettleCommon.setRealGridHeaderSum(partnerSettleGrid.gridView, stringCol);
            }
        });

        var _url = '/settle/partnerSettleDetail/getPartnerSettleItem.json?settleSrl='+ $('#settleSrl').val();
        CommonAjax.basic({
            url: _url,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                partnerSettleDetailGrid.setData(res);
                $('#partnerSettleDetailCnt').html($.jUtil.comma(partnerSettleDetailGrid.gridView.getItemCount()));

                //합계
                var stringCol = ["couponChargeAmt", "couponSellerChargeAmt", "couponHomeChargeAmt", "couponCardChargeAmt", "cardCouponAmt", "cardCouponHomeAmt", "cardCouponCardAmt"];
                SettleCommon.setRealGridHeaderSum(partnerSettleDetailGrid.gridView, stringCol);
            }
        });
    },
    initSearchDate : function (flag) {
        partnerSettleForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        partnerSettleForm.setDate.setEndDate(0);
        partnerSettleForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(partnerSettleForm.search);
        $('#searchResetBtn').bindClick(partnerSettleForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(partnerSettleGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(partnerSettleDetailGrid.excelDownload);
    },
    valid : function () {
        var schStartDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(schEndDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(schEndDt.format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    search : function () {
        if(!partnerSettleForm.valid()) return false;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = "&settleCycleType="+$("input:radio[name='settleCycleType']:checked").val()+"&partnerId="+ $('#partnerId').text();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        CommonAjax.basic({
            url:'/settle/partnerSettleDetail/getPartnerSettleList.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                partnerSettleGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerSettleGrid.gridView.getItemCount()));

                //합계
                var stringCol = [];
                SettleCommon.setRealGridHeaderSum(partnerSettleGrid.gridView, stringCol);

                if (partnerSettleGrid.gridView.getItemCount() > 0) {
                    var _settleSrl = partnerSettleGrid.dataProvider.getJsonRow(0).settleSrl;
                    var _url = '/settle/partnerSettleDetail/getPartnerSettleItem.json?settleSrl='+ _settleSrl;
                    CommonAjax.basic({
                        url: _url,
                        data: null,
                        contentType: 'application/json',
                        method: "GET",
                        successMsg: null,
                        callbackFunc:function(res) {
                            partnerSettleDetailGrid.setData(res);
                            $('#partnerSettleDetailCnt').html($.jUtil.comma(partnerSettleDetailGrid.gridView.getItemCount()));

                            //합계
                            var stringCol = ["couponChargeAmt", "couponSellerChargeAmt", "couponHomeChargeAmt", "couponCardChargeAmt", "cardCouponAmt", "cardCouponHomeAmt", "cardCouponCardAmt"];
                            SettleCommon.setRealGridHeaderSum(partnerSettleDetailGrid.gridView, stringCol);
                        }
                    });
                } else {
                    partnerSettleDetailGrid.dataProvider.clearRows();
                }
                CommonAjaxBlockUI.stopBlockUI();
            }
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            partnerSettleForm.init();
        });
    }
};