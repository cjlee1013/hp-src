var balanceSheetListGrid = {
    gridView : new RealGridJS.GridView("balanceSheetListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        balanceSheetListGrid.initGrid();
        balanceSheetListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        balanceSheetListGrid.gridView.setDataSource(balanceSheetListGrid.dataProvider);
        balanceSheetListGrid.gridView.setStyles(balanceSheetListBaseInfo.realgrid.styles);
        balanceSheetListGrid.gridView.setDisplayOptions(balanceSheetListBaseInfo.realgrid.displayOptions);
        balanceSheetListGrid.gridView.setColumns(balanceSheetListBaseInfo.realgrid.columns);
        balanceSheetListGrid.gridView.setOptions(balanceSheetListBaseInfo.realgrid.options);
        balanceSheetListGrid.gridView.onDataCellDblClicked = balanceSheetListGrid.selectRow;
        var mergeCells = [["basicDt", "slipType", "slipName", "partnerNm"],[ "status", "chgDt", "chgId"]];
        SettleCommon.setRealGridSumHeader(balanceSheetListGrid.gridView, mergeCells);
    },
    selectRow : function(gridView, data) {
        var row = balanceSheetListGrid.dataProvider.getJsonRow(data.dataRow);
        var basicDt = row.basicDt.replace(/-/gi, "");
        var params = 'startDt='+basicDt+'&endDt='+basicDt+'&slipType='+row.slipType;
        balanceSheetDetailGrid.fileName ="전표조회_"+row.slipType+"_"+row.basicDt;
        CommonAjax.basic(
        {
            url: '/settle/balanceSheet/getDetail.json?'+params,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                balanceSheetDetailGrid.setData(res);
                //그룹헤더컬럼 합계
                var stringCol = [ "drAmt", "crAmt"];
                balanceSheetListGrid.setRealGridHeaderSum(balanceSheetDetailGrid.gridView, stringCol);
            }
        });
    },
    initDataProvider : function() {
        balanceSheetListGrid.dataProvider.setFields(balanceSheetListBaseInfo.dataProvider.fields);
        balanceSheetListGrid.dataProvider.setOptions(balanceSheetListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        balanceSheetListGrid.dataProvider.clearRows();
        balanceSheetListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(balanceSheetListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        balanceSheetListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: balanceSheetListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    fileName : "",
    startDt : "",
    endDt : "",
    slipType : ""
};
var balanceSheetDetailGrid = {
    gridView : new RealGridJS.GridView("balanceSheetDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        balanceSheetDetailGrid.initGrid();
        balanceSheetDetailGrid.initDataProvider();
    },
    initGrid : function () {
        balanceSheetDetailGrid.gridView.setDataSource(balanceSheetDetailGrid.dataProvider);
        balanceSheetDetailGrid.gridView.setStyles(balanceSheetDetailBaseInfo.realgrid.styles);
        balanceSheetDetailGrid.gridView.setDisplayOptions(balanceSheetDetailBaseInfo.realgrid.displayOptions);
        balanceSheetDetailGrid.gridView.setColumns(balanceSheetDetailBaseInfo.realgrid.columns);
        balanceSheetDetailGrid.gridView.setOptions(balanceSheetDetailBaseInfo.realgrid.options);
        var mergeCells = [["basicDt", "slipName", "partnerNm", "costCenter", "costCenterName", "category", "accountCode", "accountName", "description"]];
        SettleCommon.setRealGridSumHeader(balanceSheetDetailGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        balanceSheetDetailGrid.dataProvider.setFields(balanceSheetDetailBaseInfo.dataProvider.fields);
        balanceSheetDetailGrid.dataProvider.setOptions(balanceSheetDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        balanceSheetDetailGrid.dataProvider.clearRows();
        balanceSheetDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(balanceSheetDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        balanceSheetDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: balanceSheetDetailGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};
var balanceSheetDetailAllGrid = {
    gridView : new RealGridJS.GridView("balanceSheetDetailAllGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        balanceSheetDetailAllGrid.initGrid();
        balanceSheetDetailAllGrid.initDataProvider();
    },
    initGrid : function () {
        balanceSheetDetailAllGrid.gridView.setDataSource(balanceSheetDetailAllGrid.dataProvider);
        balanceSheetDetailAllGrid.gridView.setStyles(balanceSheetDetailAllBaseInfo.realgrid.styles);
        balanceSheetDetailAllGrid.gridView.setDisplayOptions(balanceSheetDetailAllBaseInfo.realgrid.displayOptions);
        balanceSheetDetailAllGrid.gridView.setColumns(balanceSheetDetailAllBaseInfo.realgrid.columns);
        balanceSheetDetailAllGrid.gridView.setOptions(balanceSheetDetailAllBaseInfo.realgrid.options);
        var mergeCells = [["basicDt", "slipName", "partnerNm", "costCenter", "costCenterName", "category", "accountCode", "accountName", "description"]];
        SettleCommon.setRealGridSumHeader(balanceSheetDetailAllGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        balanceSheetDetailAllGrid.dataProvider.setFields(balanceSheetDetailAllBaseInfo.dataProvider.fields);
        balanceSheetDetailAllGrid.dataProvider.setOptions(balanceSheetDetailAllBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        balanceSheetDetailAllGrid.dataProvider.clearRows();
        balanceSheetDetailAllGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        var params = 'startDt='+balanceSheetListGrid.startDt;
        params += '&endDt='+balanceSheetListGrid.endDt;
        if (balanceSheetListGrid.slipType != "") {
            params += '&slipType='+balanceSheetListGrid.slipType;
        }
        CommonAjax.basic(
            {
                url: '/settle/balanceSheet/getDetail.json?'+params,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    balanceSheetDetailAllGrid.setData(res);
                    var stringCol = [ "drAmt", "crAmt"];
                    balanceSheetListGrid.setRealGridHeaderSum(balanceSheetDetailAllGrid.gridView, stringCol);
                    balanceSheetDetailAllGrid.gridView.exportGrid( {
                        type: "excel",
                        target: "local",
                        fileName: balanceSheetDetailAllGrid.fileName+".xlsx",
                        showProgress: false,
                        progressMessage: " 엑셀 데이터 추줄 중입니다."
                    });
                }
            });

    },
    fileName : ""
};
/***
 * 매출조회 파트너 상세
 */
var balanceSheetListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
        CommonAjax.basic(
{
            url:'/settle/balanceSheet/getSlipType.json',
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                for (var item of res) {
                    $('#slipType').append(new Option(item.typeName,item.slipType));
                }
            }
        });
    },
    initSearchDate : function (flag) {
        balanceSheetListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        balanceSheetListForm.setDate.setEndDate('-1d');
        balanceSheetListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(balanceSheetListForm.search);
        $('#clearBtn').bindClick(balanceSheetListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(balanceSheetListGrid.excelDownload);
        $('#excelDownloadDetailBtn').bindClick(balanceSheetDetailGrid.excelDownload);
        $('#excelDownloadDetailAllBtn').bindClick(balanceSheetDetailAllGrid.excelDownload);
    },
    search : function () {
        var startDt;
        var endDt;
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).endOf('month');

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }

        }
        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var searchForm = "startDt="+startDt+"&endDt="+endDt;
        balanceSheetListGrid.startDt = startDt;
        balanceSheetListGrid.endDt = endDt;
        var slipType = $("#slipType").val();
        if (slipType != "") {
            searchForm = searchForm + "&slipType=" + slipType;
            balanceSheetListGrid.slipType = slipType;
            balanceSheetListGrid.fileName = "전표조회_"+$("#slipType option:selected").text()+"_";
            balanceSheetDetailAllGrid.fileName = "전표조회_상세일괄다운_"+$("#slipType option:selected").text()+"_";
        } else {
            balanceSheetListGrid.fileName = "전표조회_";
            balanceSheetDetailAllGrid.fileName = "전표조회_상세일괄다운_";
        }
        balanceSheetListGrid.fileName += startDt+"_"+endDt;
        balanceSheetDetailAllGrid.fileName += startDt+"_"+endDt;
        CommonAjax.basic(
        {
            url:'/settle/balanceSheet/getList.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                balanceSheetListGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(balanceSheetListGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = [ "drAmt", "crAmt"];
                balanceSheetListGrid.setRealGridHeaderSum(balanceSheetListGrid.gridView, stringCol);
                if(balanceSheetListGrid.gridView.getItemCount() > 0) {
                    //체크박스 선택조건 설정.
                    balanceSheetListGrid.gridView.setCheckableExpression("value['status'] = '대기'",true);
                }
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            balanceSheetListForm.init();
        });
    },
    setInterface : function () {
        if(confirm("전표를 확정하시겠습니까? 취소가 불가능합니다.")) {
            var checkedCnt; var successCnt = 0; var failCnt = 0;
            var basicDt,slipType,params;
            checkedCnt = balanceSheetListGrid.gridView.getCheckedRows().length;
            $.each(balanceSheetListGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
                basicDt = balanceSheetListGrid.dataProvider.getJsonRow(_dataRowId).basicDt.replace(/-/gi, "");
                slipType = balanceSheetListGrid.dataProvider.getJsonRow(_dataRowId).slipType;
                params = "?basicDt="+basicDt+"&slipType="+ slipType;
                CommonAjax.basic({
                    url: "/settle/balanceSheet/setInterface.json"+params,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (resData) {
                        if (resData.resultCd = '0000') {
                            successCnt++;
                        } else {
                            failCnt++;
                        }
                        if (checkedCnt == (successCnt + failCnt)) {
                            alert("전표가 확정되었습니다.");
                            balanceSheetListForm.search();
                        }
                    }
                })
            });
        }
    }
};