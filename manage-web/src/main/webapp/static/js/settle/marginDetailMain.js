/** Main Script */
var marginDetailMain = {
    /**
     * 전역변수
     */
    global: {
        currentStartDate : ""
        , currentEndDate : ""
        , mallType : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        marginDetailMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {
        // [검색] 버튼
        $("#schBtn").bindClick(marginDetailMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(marginDetailMain.reset, "marginDetailSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(marginDetailMain.excelDownload);

        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd2'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd3'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd4'), true);
        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd5'), true);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#marginDetailSearchForm").serializeObject();

        CommonAjax.basic({
            url:'/settle/margin/getMarginDetailList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                marginDetailMain.global.currentStartDate = $('#schStartDt').val().replace(/-/gi, "");
                marginDetailMain.global.currentEndDate = $('#schEndDt').val().replace(/-/gi, "");
                marginDetailMain.global.mallType = $('#mallType').val();

                marginDetailGrid.setData(res);
                $('#marginDetailSearchCnt').html($.jUtil.comma(marginDetailGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = ["completeQty","discountAmt","completeInAmt","completeExAmt","orgInAmt","orgExAmt","margin","marginRate"];

                marginDetailMain.setRealGridHeaderSum(marginDetailGrid.gridView, stringCol, res);
            }
        });
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (marginDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  marginDetailMain.global.mallType +  " 마진상세조회_" + marginDetailMain.global.currentStartDate + "_" + marginDetailMain.global.currentEndDate;
        marginDetailGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "0");

        SettleCommon.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
        $('#formMappingCateCd1 option:first').prop('selected', true).change();
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns, data) {
        var resLen = data.length;

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
        _gridView.setColumnProperty("marginRate", "header", {summary: {styles: {textAlignment: "far", "numberFormat": "#,##0"}, text: ""}});

        if(resLen > 0) {
            for (var i = 0; i < _groupColumns.length; i++) {
                _gridView.setColumnProperty(_groupColumns[i], "header", {
                    summary: {
                        styles: {textAlignment: "far", "numberFormat": "#,##0"},
                        expression: "sum"
                    }
                });
            }

            var marginSum = 0;
            var completeExAmtSum = 0;
            var marginRateSum = 0;

            for (var i = 0; i < resLen; i++) {
                completeExAmtSum += Number(data[i].completeExAmt.replace(/,/g, ""));
                marginSum += Number(data[i].margin.replace(/,/g, ""));
            }

            if (marginSum != 0 && completeExAmtSum != 0) {
                marginRateSum = (marginSum / completeExAmtSum * 100).toFixed(2);
                if (isNaN(marginRateSum)) {
                    marginRateSum = 0;
                }
            }

            _gridView.setColumnProperty("marginRate", "header", {summary: {styles: {textAlignment: "far", "numberFormat": "#,##0"}, text: marginRateSum + "%"}});
        }
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        return true;
    }
}

/** Grid Script */
var marginDetailGrid = {
    gridView: new RealGridJS.GridView("marginDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marginDetailGrid.initGrid();
        marginDetailGrid.initDataProvider();
    },
    initGrid: function () {
        marginDetailGrid.gridView.setDataSource(marginDetailGrid.dataProvider);
        marginDetailGrid.gridView.setStyles(marginDetailGridBaseInfo.realgrid.styles);
        marginDetailGrid.gridView.setDisplayOptions(marginDetailGridBaseInfo.realgrid.displayOptions);
        marginDetailGrid.gridView.setColumns(marginDetailGridBaseInfo.realgrid.columns);
        marginDetailGrid.gridView.setOptions(marginDetailGridBaseInfo.realgrid.options);

        var stringCol = ["basicDt","taxYn","deptNo","classNo","subClassNo","partnerId","vendorCd","partnerName","itemNo","itemName","itemSaleAmt"];
        SettleCommon.setRealGridSumHeader(marginDetailGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        marginDetailGrid.dataProvider.setFields(marginDetailGridBaseInfo.dataProvider.fields);
        marginDetailGrid.dataProvider.setOptions(marginDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        marginDetailGrid.dataProvider.clearRows();
        marginDetailGrid.dataProvider.setRows(dataList);
    }
};