var paymentInfoListGrid = {
    gridView : new RealGridJS.GridView("paymentInfoListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        paymentInfoListGrid.initGrid();
        paymentInfoListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        paymentInfoListGrid.gridView.setDataSource(paymentInfoListGrid.dataProvider);
        paymentInfoListGrid.gridView.setStyles(paymentInfoListBaseInfo.realgrid.styles);
        paymentInfoListGrid.gridView.setDisplayOptions(paymentInfoListBaseInfo.realgrid.displayOptions);
        paymentInfoListGrid.gridView.setColumns(paymentInfoListBaseInfo.realgrid.columns);
        paymentInfoListGrid.gridView.setOptions(paymentInfoListBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [
            ["basicDt", "settleBasicDt" , "originStoreId", "originStoreNm", "mallType", "gubun", "purchaseOrderNo", "bundleNo", "claimNo", "partnerId", "ofVendorCd", "partnerName", "orderType", "orderItemNo", "itemNo", "itemNm", "taxYn", "commissionRate"]
            ];
        SettleCommon.setRealGridSumHeader(paymentInfoListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        paymentInfoListGrid.dataProvider.setFields(paymentInfoListBaseInfo.dataProvider.fields);
        paymentInfoListGrid.dataProvider.setOptions(paymentInfoListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        paymentInfoListGrid.dataProvider.clearRows();
        paymentInfoListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(paymentInfoListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridSumHeader(paymentInfoListGrid.gridView, []);
        paymentInfoListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: paymentInfoListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            done: function () {
                var mergeCells = [
                    ["basicDt" , "settleBasicDt" , "originStoreId", "originStoreNm", "mallType", "gubun", "purchaseOrderNo", "bundleNo", "claimNo", "partnerId", "ofVendorCd", "partnerName", "orderType", "orderItemNo", "itemNo", "itemNm", "taxYn", "commissionRate"]
                    ];
                SettleCommon.setRealGridSumHeader(paymentInfoListGrid.gridView, mergeCells);
            }
        });

    },
    startDt : "",
    endDt : "",
    storeType : "",
    searchType : "",
    searchValue : "",
    gubun : "",
    fileName : ""
};
/***
 * 매출조회 파트너 상세
 */
var paymentInfoListForm = {
    init : function () {
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        paymentInfoListForm.setDate = Calendar.datePickerRange('startDt', 'endDt');
        paymentInfoListForm.setDate.setEndDate('-1d');
        paymentInfoListForm.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(paymentInfoListForm.search);
        $('#clearBtn').bindClick(paymentInfoListForm.resetSearchForm);
        $('#excelDownloadSumBtn').bindClick(paymentInfoSumGrid.excelDownload);
        $('#excelDownloadBtn').bindClick(paymentInfoListGrid.excelDownload);
        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchValue').attr('readonly', true).val('');
            } else {
                $('#searchValue').removeAttr('readonly');
                $('#searchValue').focus();
            }
        });
    },
    search : function () {
        var startDt = moment($("#startDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#endDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#startDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#startDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var mallType = $("#mallType").val();
        if (mallType != "") {
            searchForm = searchForm + "&mallType=" + mallType;
        }
        var storeType;
        if (mallType == "DS" || mallType == "DC") {
            storeType = "DS";
        } else {
            storeType = $("#storeType").val();
        }
        if (storeType != "") {
            searchForm = searchForm + "&storeType=" + storeType;
        }
        var searchType = $("#searchType").val();
        var searchValue = $("#searchValue").val();
        if (searchType != "") {
            searchForm = searchForm + "&" + searchType + "=" + searchValue;
        }
        var gubun = $("#gubun").val();
        paymentInfoSumGrid.fileName = "일마감조회_";
        if (gubun == "paymentInfo") {
            var url = "getPaymentInfoSum";
            paymentInfoSumGrid.fileName += "결제일_";
        } else {
            var url = "getSettleInfoSum";
            paymentInfoSumGrid.fileName += "매출일_";
        }
        paymentInfoSumGrid.fileName += "점포별합계_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
            {
                url:'/settle/management/'+url+'.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    paymentInfoSumGrid.setData(res);
                    //그룹헤더컬럼 합계
                    var stringCol = [ "completeAmt" , "settleAmt" ,"saleAgencyFee", "shipAmt"
                        , "claimReturnShipAmt", "couponSellerChargeAmt" , "couponHomeChargeAmt", "couponCardChargeAmt"
                        ,"cardCouponHomeAmt" ,"cardCouponCardAmt","shipDiscountAmt","empDiscountAmt","promoDiscountAmt"
                        ,"cartCouponAmt","ekpiAmt" ,"orderPrice","pgAmt","dgvAmt","mhcAmt","ocbAmt","mileageAmt" ];
                    paymentInfoListForm.setRealGridHeaderSum(paymentInfoSumGrid.gridView, stringCol);

                    paymentInfoListGrid.startDt = startDt;
                    paymentInfoListGrid.endDt = endDt;
                    paymentInfoListGrid.storeType = storeType;
                    paymentInfoListGrid.searchType = searchType;
                    paymentInfoListGrid.searchValue = searchValue;
                    paymentInfoListGrid.gubun = gubun;
                }
            });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            paymentInfoListForm.init();
        });
    }
};
var paymentInfoSumGrid = {
    gridView : new RealGridJS.GridView("paymentInfoSumGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        paymentInfoSumGrid.initGrid();
        paymentInfoSumGrid.initDataProvider();
    },
    initGrid : function () {
        paymentInfoSumGrid.gridView.setDataSource(paymentInfoSumGrid.dataProvider);
        paymentInfoSumGrid.gridView.setStyles(paymentInfoSumBaseInfo.realgrid.styles);
        paymentInfoSumGrid.gridView.setDisplayOptions(paymentInfoSumBaseInfo.realgrid.displayOptions);
        paymentInfoSumGrid.gridView.setColumns(paymentInfoSumBaseInfo.realgrid.columns);
        paymentInfoSumGrid.gridView.setOptions(paymentInfoSumBaseInfo.realgrid.options);
        paymentInfoSumGrid.gridView.onDataCellDblClicked = paymentInfoSumGrid.selectRow;
        paymentInfoSumGrid.mergeHeader();
    },
    initDataProvider : function() {
        paymentInfoSumGrid.dataProvider.setFields(paymentInfoSumBaseInfo.dataProvider.fields);
        paymentInfoSumGrid.dataProvider.setOptions(paymentInfoSumBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        paymentInfoSumGrid.dataProvider.clearRows();
        paymentInfoSumGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(paymentInfoSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridSumHeader(paymentInfoSumGrid.gridView, []);
        paymentInfoSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: paymentInfoSumGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다.",
            done: paymentInfoSumGrid.mergeHeader
        });

    },
    selectRow : function(gridView, data) {
        var row = paymentInfoSumGrid.dataProvider.getJsonRow(data.dataRow);

        var startDt = paymentInfoListGrid.startDt;
        var endDt = paymentInfoListGrid.endDt;

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var mallType = row.mallType;
        if (mallType != "") {
            searchForm = searchForm + "&mallType=" + mallType;
        }
        var storeType = paymentInfoListGrid.storeType;
        if (storeType != "") {
            searchForm = searchForm + "&storeType=" + storeType;
        }
        var searchType = paymentInfoListGrid.searchType;
        if (searchType != "") {
            searchForm = searchForm + "&" + searchType + "=" + paymentInfoListGrid.searchValue;
        }

        var originStoreId = row.originStoreId;
        if (originStoreId != "전체") {
            searchForm = searchForm + "&originStoreId=" + originStoreId;
        }

        var gubun = paymentInfoListGrid.gubun;
        paymentInfoListGrid.fileName = "일마감조회_";
        if (gubun == "paymentInfo") {
            var url = "getPaymentInfoList";
            paymentInfoListGrid.fileName += "결제일_";
        } else {
            var url = "getSettleInfoList";
            paymentInfoListGrid.fileName += "매출일_";
        }
        if (mallType == 'TD') {
            paymentInfoListGrid.fileName += row.originStoreNm + "_";
        } else {
            paymentInfoListGrid.fileName += row.mallType + "_";
        }
        paymentInfoListGrid.fileName += startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");

        CommonAjax.basic(
        {
            url:'/settle/management/'+url+'.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                paymentInfoListGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(paymentInfoListGrid.gridView.getItemCount()));
                //그룹헤더컬럼 합계
                var stringCol = [ "completeQty","completeAmt","completeExAmt", "settleAmt","settleExAmt"
                    ,"saleAgencyFee", "shipAmt", "claimReturnShipAmt", "couponSellerChargeAmt"
                    , "couponHomeChargeAmt", "couponCardChargeAmt","cardCouponHomeAmt"
                    ,"cardCouponCardAmt","shipDiscountAmt","empDiscountAmt","promoDiscountAmt","cartCouponAmt"
                    ,"ekpiAmt" ,"orderPrice","pgAmt","dgvAmt","mhcAmt","ocbAmt","mileageAmt" ];
                paymentInfoListForm.setRealGridHeaderSum(paymentInfoListGrid.gridView, stringCol);
            }
        });
    },
    mergeHeader : function() {
        var mergeCells = [
            [ "mallType", "originStoreId", "originStoreNm" ]
        ];
        SettleCommon.setRealGridSumHeader(paymentInfoSumGrid.gridView, mergeCells);
    },
    fileName : ""
};