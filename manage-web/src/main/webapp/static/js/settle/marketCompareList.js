/***
 * 마켓정산대사 조회
 */
var marketCompareForm = {
    init : function () {
        this.initSearchDate('-1d');
        CommonAjaxBlockUI.global();
    },
    initSearchDate : function (flag) {
        marketCompareForm.setDate = Calendar.datePickerRange('startDt', 'endDt');
        marketCompareForm.setDate.setEndDate('-1d');
        marketCompareForm.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(marketCompareForm.search);
        $('#clearBtn').bindClick(marketCompareForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(marketCompareListGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#startDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#endDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#startDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#startDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var dateType = $("#dateType").val();
        if (dateType != "") {
            searchForm = searchForm + "&dateType=" + dateType;
        }
        var siteType = $("#siteType").val();
        if (siteType != "") {
            searchForm = searchForm + "&siteType=" + siteType;
        }
        marketCompareSumGrid.fileName = "마켓정산대사_합계내역_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        marketCompareDailyGrid.fileName = "마켓정산대사_일별상세내역_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
            {
                url:'/settle/market/getCompareSum.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    marketCompareDailyGrid.setData(res);
                    marketCompareDailyGrid.dateType = dateType;
                    var summary = new Array();
                    for (var i=0;i < res.length;i++) {
                        if (summary[res[i].siteType] == undefined) {
                            summary[res[i].siteType] = res[i];
                            summary[res[i].siteType].homeShipOrderPrice = parseInt(res[i].homeShipOrderPrice);
                            summary[res[i].siteType].homeShipOrderRefPrice = parseInt(res[i].homeShipOrderRefPrice);
                            summary[res[i].siteType].homeSumAmt = parseInt(res[i].homeSumAmt);
                            summary[res[i].siteType].marketShipOrderPrice = parseInt(res[i].marketShipOrderPrice);
                            summary[res[i].siteType].marketShipOrderRefPrice = parseInt(res[i].marketShipOrderRefPrice);
                            summary[res[i].siteType].marketSumAmt = parseInt(res[i].marketSumAmt);
                            summary[res[i].siteType].diffShipOrderPrice = parseInt(res[i].diffShipOrderPrice);
                            summary[res[i].siteType].diffShipOrderRefPrice = parseInt(res[i].diffShipOrderRefPrice);
                            summary[res[i].siteType].diffSumAmt = parseInt(res[i].diffSumAmt);
                        } else {
                            summary[res[i].siteType].homeShipOrderPrice += parseInt(res[i].homeShipOrderPrice);
                            summary[res[i].siteType].homeShipOrderRefPrice += parseInt(res[i].homeShipOrderRefPrice);
                            summary[res[i].siteType].homeSumAmt += parseInt(res[i].homeSumAmt);
                            summary[res[i].siteType].marketShipOrderPrice += parseInt(res[i].marketShipOrderPrice);
                            summary[res[i].siteType].marketShipOrderRefPrice += parseInt(res[i].marketShipOrderRefPrice);
                            summary[res[i].siteType].marketSumAmt += parseInt(res[i].marketSumAmt);
                            summary[res[i].siteType].diffShipOrderPrice += parseInt(res[i].diffShipOrderPrice);
                            summary[res[i].siteType].diffShipOrderRefPrice += parseInt(res[i].diffShipOrderRefPrice);
                            summary[res[i].siteType].diffSumAmt += parseInt(res[i].diffSumAmt);
                        }
                    }
                    marketCompareSumGrid.setData(Object.values(summary));
                    //그룹헤더컬럼 합계
                    var stringCol = [ "homeShipOrderPrice" , "homeShipOrderRefPrice" ,"homeSumAmt", "marketShipOrderPrice"
                        , "marketShipOrderRefPrice", "marketSumAmt" , "diffShipOrderPrice", "diffShipOrderRefPrice", "diffSumAmt"];
                    marketCompareForm.setRealGridHeaderSum(marketCompareSumGrid.gridView, stringCol);
                    marketCompareForm.setRealGridHeaderSum(marketCompareDailyGrid.gridView, stringCol);
                }
            });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            marketCompareForm.init();
        });
    }
};
var marketCompareSumGrid = {
    gridView : new RealGridJS.GridView("marketCompareSumGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        marketCompareSumGrid.initGrid();
        marketCompareSumGrid.initDataProvider();
    },
    initGrid : function () {
        marketCompareSumGrid.gridView.setDataSource(marketCompareSumGrid.dataProvider);
        marketCompareSumGrid.gridView.setStyles(marketCompareSumBaseInfo.realgrid.styles);
        marketCompareSumGrid.gridView.setDisplayOptions(marketCompareSumBaseInfo.realgrid.displayOptions);
        marketCompareSumGrid.gridView.setColumns(marketCompareSumBaseInfo.realgrid.columns);
        marketCompareSumGrid.gridView.setOptions(marketCompareSumBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [ ["siteType"] ];
        SettleCommon.setRealGridSumHeader(marketCompareSumGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        marketCompareSumGrid.dataProvider.setFields(marketCompareSumBaseInfo.dataProvider.fields);
        marketCompareSumGrid.dataProvider.setOptions(marketCompareSumBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        marketCompareSumGrid.dataProvider.clearRows();
        marketCompareSumGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketCompareSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        marketCompareSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: marketCompareSumGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};
var marketCompareDailyGrid = {
    gridView : new RealGridJS.GridView("marketCompareDailyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        marketCompareDailyGrid.initGrid();
        marketCompareDailyGrid.initDataProvider();
    },
    initGrid : function () {
        marketCompareDailyGrid.gridView.setDataSource(marketCompareDailyGrid.dataProvider);
        marketCompareDailyGrid.gridView.setStyles(marketCompareDailyBaseInfo.realgrid.styles);
        marketCompareDailyGrid.gridView.setDisplayOptions(marketCompareDailyBaseInfo.realgrid.displayOptions);
        marketCompareDailyGrid.gridView.setColumns(marketCompareDailyBaseInfo.realgrid.columns);
        marketCompareDailyGrid.gridView.setOptions(marketCompareDailyBaseInfo.realgrid.options);
        marketCompareDailyGrid.gridView.onDataCellDblClicked = marketCompareDailyGrid.selectRow;
        //헤더 merge
        var mergeCells = [ ["basicDt", "siteType"] ];
        SettleCommon.setRealGridSumHeader(marketCompareDailyGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        marketCompareDailyGrid.dataProvider.setFields(marketCompareDailyBaseInfo.dataProvider.fields);
        marketCompareDailyGrid.dataProvider.setOptions(marketCompareDailyBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        marketCompareDailyGrid.dataProvider.clearRows();
        marketCompareDailyGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketCompareDailyGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        marketCompareDailyGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: marketCompareDailyGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    selectRow : function(gridView, data) {
        var row = marketCompareDailyGrid.dataProvider.getJsonRow(data.dataRow);

        var basicDt = row.basicDt.replace(/-/gi, "");
        var searchForm = "startDt="+basicDt+"&endDt="+basicDt;
        var dateType = marketCompareDailyGrid.dateType;
        if (dateType != "") {
            searchForm = searchForm + "&dateType=" + dateType;
        }
        var siteType = row.siteType;
        if (siteType != "") {
            searchForm = searchForm + "&siteType=" + siteType;
        }
        marketCompareListGrid.fileName = "마켓정산대사_차이내역_" + basicDt;

        CommonAjax.basic(
            {
                url:'/settle/market/getCompareList.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    marketCompareListGrid.setData(res);
                    //그룹헤더컬럼 합계
                    var stringCol = [ "homeAmt", "marketAmt", "diffAmt"];
                    marketCompareForm.setRealGridHeaderSum(marketCompareListGrid.gridView, stringCol);
                }
            });
    },
    dateType : "",
    fileName : ""
};
var marketCompareListGrid = {
    gridView : new RealGridJS.GridView("marketCompareListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        marketCompareListGrid.initGrid();
        marketCompareListGrid.initDataProvider();
    },
    initGrid : function () {
        marketCompareListGrid.gridView.setDataSource(marketCompareListGrid.dataProvider);
        marketCompareListGrid.gridView.setStyles(marketCompareListBaseInfo.realgrid.styles);
        marketCompareListGrid.gridView.setDisplayOptions(marketCompareListBaseInfo.realgrid.displayOptions);
        marketCompareListGrid.gridView.setColumns(marketCompareListBaseInfo.realgrid.columns);
        marketCompareListGrid.gridView.setOptions(marketCompareListBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [
            ["basicDt", "payScheduleDt" , "siteType", "purchaseOrderNo"
                , "marketOrderNo" , "itemNo", "gubun", "diffType"]
        ];
        SettleCommon.setRealGridSumHeader(marketCompareListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        marketCompareListGrid.dataProvider.setFields(marketCompareListBaseInfo.dataProvider.fields);
        marketCompareListGrid.dataProvider.setOptions(marketCompareListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        marketCompareListGrid.dataProvider.clearRows();
        marketCompareListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketCompareListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        marketCompareListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: marketCompareListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};