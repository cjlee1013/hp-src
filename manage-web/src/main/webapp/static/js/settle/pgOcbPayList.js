/***
 * OCB 점별 실적조회
 */
var ocbStoreGrid = {
    gridView : new RealGridJS.GridView("ocbStoreGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        ocbStoreGrid.initGrid();
        ocbStoreGrid.initDataProvider();
        ocbStoreGrid.event();
    },
    initGrid : function () {
        ocbStoreGrid.gridView.setDataSource(ocbStoreGrid.dataProvider);
        ocbStoreGrid.gridView.setStyles(ocbStoreBaseInfo.realgrid.styles);
        ocbStoreGrid.gridView.setDisplayOptions(ocbStoreBaseInfo.realgrid.displayOptions);
        ocbStoreGrid.gridView.setOptions(ocbStoreBaseInfo.realgrid.options);
        ocbStoreGrid.gridView.setColumns(ocbStoreBaseInfo.realgrid.columns);

        //합계
        var mergeCells = [ ["storeType", "originStoreId", "storeNm" ] ];
        SettleCommon.setRealGridSumHeader(ocbStoreGrid.gridView, mergeCells);

        //컬럼 그룹핑 너비 조정
        ocbStoreGrid.gridView.setColumnProperty("사용수수료", "displayWidth", 240);

    },
    initDataProvider : function() {
        ocbStoreGrid.dataProvider.setFields(ocbStoreBaseInfo.dataProvider.fields);
        ocbStoreGrid.dataProvider.setOptions(ocbStoreBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        ocbStoreGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            ocbStoreGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        ocbStoreGrid.dataProvider.clearRows();
        ocbStoreGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        var rowDataJson = ocbStoreGrid.dataProvider.getJsonRow(selectRowId);
        var param = 'originStoreId='+ rowDataJson.originStoreId + pgOcbPayForm.setSearchDate();
        ocbDailyGrid.search(param);
        ocbTidGrid.search(param);
    },
    excelDownload: function() {
        if(ocbStoreGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = pgOcbPayForm.setExcelFileName("OCB실적조회_점별합계");
        ocbStoreGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * OCB 일별 실적조회
 */
var ocbDailyGrid = {
    gridView : new RealGridJS.GridView("ocbDailyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        ocbDailyGrid.initGrid();
        ocbDailyGrid.initDataProvider();
        ocbDailyGrid.event();
    },
    initGrid : function () {
        ocbDailyGrid.gridView.setDataSource(ocbDailyGrid.dataProvider);
        ocbDailyGrid.gridView.setStyles(ocbDailyBaseInfo.realgrid.styles);
        ocbDailyGrid.gridView.setDisplayOptions(ocbDailyBaseInfo.realgrid.displayOptions);
        ocbDailyGrid.gridView.setOptions(ocbDailyBaseInfo.realgrid.options);
        ocbDailyGrid.gridView.setColumns(ocbDailyBaseInfo.realgrid.columns);

        var mergeCells = [ ["basicDt", "storeType", "originStoreId", "storeNm"] ];
        SettleCommon.setRealGridSumHeader(ocbDailyGrid.gridView, mergeCells);

        //컬럼 그룹핑 너비 조정
        ocbDailyGrid.gridView.setColumnProperty("사용수수료", "displayWidth", 240);
    },
    initDataProvider : function() {
        ocbDailyGrid.dataProvider.setFields(ocbDailyBaseInfo.dataProvider.fields);
        ocbDailyGrid.dataProvider.setOptions(ocbDailyBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        ocbDailyGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            ocbDailyGrid.selectRow(index.dataRow);
        };
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = ocbDailyGrid.dataProvider.getJsonRow(selectRowId);
        var endDt = moment(rowDataJson.basicDt, 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        var param = 'originStoreId='+ rowDataJson.originStoreId +"&startDt="+ rowDataJson.basicDt.replace(/-/gi, "") +"&endDt="+ endDt;
        ocbTidGrid.search(param);
        CommonAjaxBlockUI.stopBlockUI();
    },
    search : function (param) {
        var _url = '/settle/ocb/getDailyPgOcbPay.json?' + param;

        CommonAjax.basic({
            url: _url,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                ocbDailyGrid.setData(res);

                //그룹헤더컬럼 합계
                var stringCol = ["fee", "feeVat", "totFee"];
                SettleCommon.setRealGridHeaderSum(ocbDailyGrid.gridView, stringCol);
            }
        });
    },
    setData : function(dataList) {
        ocbDailyGrid.dataProvider.clearRows();
        ocbDailyGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(ocbDailyGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = pgOcbPayForm.setExcelFileName("OCB실적조회_일별상세");
        ocbDailyGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};

/***
 * OCB 건별 실적조회
 */
var ocbTidGrid = {
    gridView : new RealGridJS.GridView("ocbTidGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        ocbTidGrid.initGrid();
        ocbTidGrid.initDataProvider();
    },
    initGrid : function () {
        ocbTidGrid.gridView.setDataSource(ocbTidGrid.dataProvider);
        ocbTidGrid.gridView.setStyles(ocbTidBaseInfo.realgrid.styles);
        ocbTidGrid.gridView.setDisplayOptions(ocbTidBaseInfo.realgrid.displayOptions);
        ocbTidGrid.gridView.setOptions(ocbTidBaseInfo.realgrid.options);
        ocbTidGrid.gridView.setColumns(ocbTidBaseInfo.realgrid.columns);

        var mergeCells = [ ["basicDt", "ocbStoreNo", "paymentNo", "purchaseOrderNo", "approvalNo", "orderType", "tradeType", "approvalType"] ];
        SettleCommon.setRealGridSumHeader(ocbTidGrid.gridView, mergeCells);

    },
    initDataProvider : function() {
        ocbTidGrid.dataProvider.setFields(ocbTidBaseInfo.dataProvider.fields);
        ocbTidGrid.dataProvider.setOptions(ocbTidBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        ocbTidGrid.dataProvider.clearRows();
        ocbTidGrid.dataProvider.setRows(dataList);
    },
    search : function (param) {
        var _url = '/settle/ocb/getTidPgOcbPay.json?'+ param;
        CommonAjax.basic({
            url: _url,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                ocbTidGrid.setData(res);
                SettleCommon.setRealGridHeaderSum(ocbTidGrid.gridView, []);
            }
        });
    },
    excelDownload: function() {
        if(ocbTidGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = pgOcbPayForm.setExcelFileName("OCB실적조회_건별상세");
        ocbTidGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * OCB실적조회 폼
 */
var pgOcbPayForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
    },
    initSearchDate : function (flag) {
        pgOcbPayForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        pgOcbPayForm.setDate.setEndDate(0);
        pgOcbPayForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth').val(month);
        $('#startYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(pgOcbPayForm.search);
        $('#searchResetBtn').bindClick(pgOcbPayForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(ocbStoreGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(ocbDailyGrid.excelDownload);
        $('#ocbDownloadBtn').bindClick(ocbTidGrid.excelDownload);

    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 31) {
                alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }

        }

        return true;
    },
    setSearchDate : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#startYear").val() +'-'+ $("#startMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        return '&startDt=' + startDt + '&endDt=' + endDt;
    },
    search : function () {
        if(!pgOcbPayForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = $("form[name=searchForm]").serialize();
        searchForm += pgOcbPayForm.setSearchDate();

        CommonAjax.basic({
            url: '/settle/ocb/getStorePgOcbPay.json?' + searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                ocbStoreGrid.setData(res);

                //그룹헤더컬럼 합계
                var stringCol = ["fee", "feeVat", "totFee"];
                SettleCommon.setRealGridHeaderSum(ocbStoreGrid.gridView, stringCol);

                if(ocbStoreGrid.gridView.getItemCount() > 0) {
                    ocbDailyGrid.search(searchForm);
                    ocbTidGrid.search(searchForm);
                } else {
                    ocbDailyGrid.dataProvider.clearRows();
                    ocbTidGrid.dataProvider.clearRows();
                }
                CommonAjaxBlockUI.stopBlockUI();
            }
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val();
        }
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            pgOcbPayForm.init();
            ocbStoreGrid.init();
            ocbDailyGrid.init();
            ocbTidGrid.init();
        });
    }
};