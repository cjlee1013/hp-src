/** Main Script */
var dcOnlineReceivePayMain = {
    /**
     * 전역변수
     */
    global: {
        currentStartDate : ""
        , currentEndDate : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        dcOnlineReceivePayMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 검색조건 select box
        $("#schKeywordType").on('change', function() { dcOnlineReceivePayMain.setChangeKeywordType() });

        // [검색] 버튼
        $("#schBtn").bindClick(dcOnlineReceivePayMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(dcOnlineReceivePayMain.reset, "dcOnlineReceivePaySearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(dcOnlineReceivePayMain.excelDownload);
        // [주문상세 엑셀다운] 버튼
        $("#schExcelDownloadDetailBtn").bindClick(dcOnlineReceivePayMain.excelDownloadDetail);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#dcOnlineReceivePaySearchForm").serializeObject();

        reqData.schStartDt = $('#schStartYear').val() + $('#schStartMonth').val() + "01";
        reqData.schEndDt = $('#schEndYear').val() + $('#schEndMonth').val() + "01";

        CommonAjax.basic({
            url:'/settle/dcOnlineReceivePay/getDcOnlineReceivePayList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                dcOnlineReceivePayMain.global.currentStartDate = $('#schStartYear').val() + $('#schStartMonth').val();
                dcOnlineReceivePayMain.global.currentEndDate = $('#schEndYear').val() + $('#schEndMonth').val();
                dcOnlineReceivePayGrid.setData(res);
                $('#dcOnlineReceivePaySearchCnt').html($.jUtil.comma(dcOnlineReceivePayGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = [ "basicQty", "basicAmt", "purchaseQty", "purchaseAmt", "tranQty",
                    "tranAmt", "lossQty", "lossAmt", "unknownLossQty", "unknownLossAmt", "refundQty", "refundAmt",
                    "salesQty","salesAmt","termEndQty","termEndAmt","orgPrice","orderQty","orderAmt","tranAvgAmt",
                    "discountAmt","sellAmt","realSaleAmt","supplyAmt","margin"];

                dcOnlineReceivePayMain.setRealGridHeaderSum(dcOnlineReceivePayGrid.gridView, stringCol);

                dcOnlineReceivePayMain.searchDetail();
            }
        });
    },

    /**
     * 주문상세 데이터 검색
     */
    searchDetail: function() {
        var reqData = $("#dcOnlineReceivePaySearchForm").serializeObject();

        reqData.schStartDt = $('#schStartYear').val() + $('#schStartMonth').val() + "01";
        reqData.schEndDt = $('#schEndYear').val() + $('#schEndMonth').val() + "01";

        CommonAjax.basic({
            url:'/settle/dcOnlineReceivePay/getDcOnlineOrderDetailList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                dcOnlineOrderDetailGrid.setData(res);
            }
        });
    },

    /**
     * 검색 조건 select box 선택 이벤트
     */
    setChangeKeywordType: function() {
        var $schKeywordType = $('#schKeywordType');
        var $schKeyword = $('#schKeyword');

        if($.jUtil.isEmpty($schKeywordType.val())) {
            $schKeyword.val("");
            $schKeyword.prop("disabled",true);
            $schKeyword.prop("placeholder","");
        } else {
            $schKeyword.prop("disabled",false);
            $schKeyword.prop("placeholder","검색어를 입력하세요");
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (dcOnlineReceivePayGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  "DC온라인수불 조회_" + dcOnlineReceivePayMain.global.currentStartDate + "_" + dcOnlineReceivePayMain.global.currentEndDate;
        dcOnlineReceivePayGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 주문상세 엑셀 다운로드
     */
    excelDownloadDetail: function() {
        if (dcOnlineOrderDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var startDt = $('#schStartYear').val() + $('#schStartMonth').val();
        var endDt = $('#schEndYear').val() + $('#schEndMonth').val();

        var fileName =  "DC온라인수불 조회_주문상세_" + startDt + "_" + endDt;
        dcOnlineOrderDetailGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        dcOnlineReceivePayMain.setChangeKeywordType();

        var year = moment().format("YYYY");
        var month = moment().format("MM");

        $('#schStartMonth, #schEndMonth').val(month);
        $('#schStartYear, #schEndYear').val(year);
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var schKeywordTypeVal = $("#schKeywordType").val();
        var schKeywordVal = $("#schKeyword").val();

        if(!$.jUtil.isEmpty(schKeywordTypeVal) && $.jUtil.isEmpty(schKeywordVal)) {
            alert('검색조건을 입력해주세요.');
            $("#schKeyword").focus();
            return false;
        }

        return true;
    }
}

/** Grid Script */
var dcOnlineReceivePayGrid = {
    gridView: new RealGridJS.GridView("dcOnlineReceivePayGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        dcOnlineReceivePayGrid.initGrid();
        dcOnlineReceivePayGrid.initDataProvider();
    },
    initGrid: function () {
        dcOnlineReceivePayGrid.gridView.setDataSource(dcOnlineReceivePayGrid.dataProvider);
        dcOnlineReceivePayGrid.gridView.setStyles(dcOnlineReceivePayGridBaseInfo.realgrid.styles);
        dcOnlineReceivePayGrid.gridView.setDisplayOptions(dcOnlineReceivePayGridBaseInfo.realgrid.displayOptions);
        dcOnlineReceivePayGrid.gridView.setColumns(dcOnlineReceivePayGridBaseInfo.realgrid.columns);
        dcOnlineReceivePayGrid.gridView.setOptions(dcOnlineReceivePayGridBaseInfo.realgrid.options);

        var stringCol = [["itemNo", "rmsItemNo", "itemName", "taxYn", "deptNo", "storeId", "deptStoreCd"],["vendorCd"]];

        SettleCommon.setRealGridSumHeader(dcOnlineReceivePayGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        dcOnlineReceivePayGrid.dataProvider.setFields(dcOnlineReceivePayGridBaseInfo.dataProvider.fields);
        dcOnlineReceivePayGrid.dataProvider.setOptions(dcOnlineReceivePayGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        dcOnlineReceivePayGrid.dataProvider.clearRows();
        dcOnlineReceivePayGrid.dataProvider.setRows(dataList);
    }
};

var dcOnlineOrderDetailGrid = {
    gridView: new RealGridJS.GridView("dcOnlineOrderDetailGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        dcOnlineOrderDetailGrid.initGrid();
        dcOnlineOrderDetailGrid.initDataProvider();
    },
    initGrid: function () {
        dcOnlineOrderDetailGrid.gridView.setDataSource(dcOnlineOrderDetailGrid.dataProvider);
        dcOnlineOrderDetailGrid.gridView.setStyles(dcOnlineOrderDetailGridBaseInfo.realgrid.styles);
        dcOnlineOrderDetailGrid.gridView.setDisplayOptions(dcOnlineOrderDetailGridBaseInfo.realgrid.displayOptions);
        dcOnlineOrderDetailGrid.gridView.setColumns(dcOnlineOrderDetailGridBaseInfo.realgrid.columns);
        dcOnlineOrderDetailGrid.gridView.setOptions(dcOnlineOrderDetailGridBaseInfo.realgrid.options);
    },
    initDataProvider: function () {
        dcOnlineOrderDetailGrid.dataProvider.setFields(dcOnlineOrderDetailGridBaseInfo.dataProvider.fields);
        dcOnlineOrderDetailGrid.dataProvider.setOptions(dcOnlineOrderDetailGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        dcOnlineOrderDetailGrid.dataProvider.clearRows();
        dcOnlineOrderDetailGrid.dataProvider.setRows(dataList);
    }
};