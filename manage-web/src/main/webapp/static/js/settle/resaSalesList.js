var resaSalesListGrid = {
    gridView : new RealGridJS.GridView("resaSalesListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        resaSalesListGrid.initGrid();
        resaSalesListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        resaSalesListGrid.gridView.setDataSource(resaSalesListGrid.dataProvider);
        resaSalesListGrid.gridView.setStyles(resaSalesListBaseInfo.realgrid.styles);
        resaSalesListGrid.gridView.setDisplayOptions(resaSalesListBaseInfo.realgrid.displayOptions);
        resaSalesListGrid.gridView.setColumns(resaSalesListBaseInfo.realgrid.columns);
        resaSalesListGrid.gridView.setOptions(resaSalesListBaseInfo.realgrid.options);
        resaSalesListGrid.gridView.onDataCellDblClicked = resaSalesListGrid.selectRow;
        var mergeCells = [["basicDt", "storeId", "storeNm", "costCenter"]];
        SettleCommon.setRealGridSumHeader(resaSalesListGrid.gridView, mergeCells);

        resaSalesListGrid.gridView.setColumnProperty("ReSA", "displayWidth", 360);
        resaSalesListGrid.gridView.setColumnProperty("정산", "displayWidth", 420);
        resaSalesListGrid.gridView.setColumnProperty("차이내역", "displayWidth", 360);
    },
    initDataProvider : function() {
        resaSalesListGrid.dataProvider.setFields(resaSalesListBaseInfo.dataProvider.fields);
        resaSalesListGrid.dataProvider.setOptions(resaSalesListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        resaSalesListGrid.dataProvider.clearRows();
        resaSalesListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(resaSalesListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        resaSalesListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: resaSalesListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    fileName : ""
};
/***
 * 매출조회 파트너 상세
 */
var resaSalesListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
    },
    initSearchDate : function (flag) {
        resaSalesListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        resaSalesListForm.setDate.setEndDate('-1d');
        resaSalesListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(resaSalesListForm.search);
        $('#clearBtn').bindClick(resaSalesListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(resaSalesListGrid.excelDownload);
        $('#salesUpload').bindClick(resaSalesListForm.openFile);
        $('#salesSubmit').bindClick(resaSalesListForm.saveFile);
        $('#salesReset').bindClick(resaSalesListForm.resetSalesForm);
        $('#uploadFile').bindChange(resaSalesListForm.changeFile);
    },
    search : function () {
        var startDt;
        var endDt;
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).endOf('month');

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }

        }
        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var dateType = $("input:radio[name='dateType']:checked").val();
        var searchForm = "startDt="+startDt+"&endDt="+endDt+"&dateType="+dateType;
        var diffOnly = $("#diffOnly").is(":checked");
        if (diffOnly) {
            searchForm = searchForm + "&diffOnly=Y";
        }
        resaSalesListGrid.fileName = "SALES_자표조회_검색결과_" + startDt+"_"+endDt;
        CommonAjax.basic(
        {
            url:'/settle/resaSales/getList.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                resaSalesListGrid.setData(res);

                //그룹헤더컬럼 합계
                var stringCol = [ "resaSales", "resa15191", "resa20720", "resa64800", "resa72905", "resa20744"
                    ,"settleSales", "settle15191", "settle20720","settle20720DsCart", "settle64800", "settle72905", "settle20744"
                    ,"diffSales", "diff15191", "diff20720", "diff64800", "diff72905", "diff20744"];
                resaSalesListGrid.setRealGridHeaderSum(resaSalesListGrid.gridView, stringCol);
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
        });
        resaSalesListForm.init();
    },
    resetSalesForm : function() {
        $("#salesUploadForm").each(function () {
            this.reset();
        });
        $('#uploadFileNm').val('');
        $('#uploadFile').val('');
    },
    // 파일 찾기 버튼 선택
    openFile : function() {
        $('#uploadFile').trigger('click');
    },
    changeFile : function() {
        var fileInfo = $('#uploadFile')[0].files[0];
        if (!fileInfo) {
            alert('업로드 대상 파일을 확인해주세요.');
            return false;
        }

        var fileExt = fileInfo.name;
        if (fileExt.indexOf(".csv") < 0) {
            alert("지정된 엑셀 양식(.csv)으로 업로드해주세요.");
            $('#uploadFile').val('');
            return false;
        }
        $("#uploadFileNm").val($('#uploadFile')[0].files[0].name);
        return true;
    },
    saveFile : function() {
        if (!resaSalesListForm.changeFile()) return false;

        if (!confirm("엑셀 업로드 하시겠습니까?")) return false;
        CommonAjaxBlockUI.startBlockUI();
        var reader = new FileReader();
        reader.onload = function () {
            var contents = reader.result;
            var rows = contents.split(",\n");
            if (rows.length < 2 || "BUSINESS_DATE,STORE,TYPE,COST_CENTER,AMT" != rows[0].toUpperCase()) {
                alert("업로드 불가한 파일입니다.\n데이터를 다시 확인하세요.");
                CommonAjaxBlockUI.stopBlockUI();
                return false;
            }
            var basicDt = rows[1].split(",")[0];
            CommonAjax.basic(
                {
                    url:'/settle/resaSales/getExist.json?basicDt='+basicDt,
                    data: null,
                    contentType: 'application/json',
                    method: "GET",
                    callbackFunc:function(res) {
                        if (res > 0) {
                            if (!confirm("업로드 시도 일자에 등록된 자료가 존재합니다. 현재 버전으로 업데이트 하시겠습니까?")) return false;
                        }
                        resaSalesListForm.upload(rows);
                    }
                });

        };
        reader.readAsText($('#uploadFile')[0].files[0]);
    },
    upload : function (rows) {
        var header = rows[0].toLowerCase().split(",");
        var data = [];
        for (var i=1;i<rows.length;i++) {
            if (rows[i].indexOf(",") > 0) {
                var row = rows[i].split(",");
                var obj = {};
                for (var j = 0; j < row.length; j++) {
                    obj[header[j]] = row[j];
                }
                data.push(obj);
            }
        }
        var jsonString = JSON.stringify(data);
        CommonAjax.basic(
            {
                url:'/settle/resaSales/setUpload.json',
                data: jsonString,
                contentType: 'application/json',
                method: "POST",
                callbackFunc:function(res) {
                    CommonAjaxBlockUI.stopBlockUI();
                    if (res > 0) {
                        alert("자료 업로드에 성공하였습니다");
                        resaSalesListForm.resetSalesForm();
                        resaSalesListForm.search();
                    } else {
                        alert("업로드 불가한 파일입니다.\n데이터를 다시 확인하세요.");
                    }
                }
            });
    }
};