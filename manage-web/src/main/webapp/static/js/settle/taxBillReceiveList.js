/***
 * 세금계산서 수취내역 그리드 설정
 */
var taxBillInvoiceListGrid = {
    gridView : new RealGridJS.GridView("taxBillInvoiceListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        taxBillInvoiceListGrid.initGrid();
        taxBillInvoiceListGrid.initDataProvider();
        taxBillInvoiceListGrid.event();
    },
    initGrid : function () {
        taxBillInvoiceListGrid.gridView.setDataSource(taxBillInvoiceListGrid.dataProvider);
        taxBillInvoiceListGrid.gridView.setStyles(taxBillInvoiceListBaseInfo.realgrid.styles);
        taxBillInvoiceListGrid.gridView.setDisplayOptions(taxBillInvoiceListBaseInfo.realgrid.displayOptions);
        taxBillInvoiceListGrid.gridView.setOptions(taxBillInvoiceListBaseInfo.realgrid.options);
        taxBillInvoiceListGrid.gridView.setColumns(taxBillInvoiceListBaseInfo.realgrid.columns);

        taxBillInvoiceListGrid.gridView.setOptions({ display: { fitStyle: "evenFill" } });
        taxBillInvoiceListGrid.initMergeSumHeader();

        //컬럼 그룹핑 너비 조정
        taxBillInvoiceListGrid.gridView.setColumnProperty("결제기준", "displayWidth", 240);
        taxBillInvoiceListGrid.gridView.setColumnProperty("입금기준", "displayWidth", 240);
    },
    initMergeSumHeader : function() {
        //헤더 merge
        var mergeCells = [ ["confirmYn", "taxReceiveNo", "issueDt", "supplier", "vendorCd", "taxBillReceiveType", "partnerNm", "partnerNo", "partnerOwner"]
            , ["regDt", "regId", "chgDt", "chgId"] ];
        SettleCommon.setRealGridSumHeader(taxBillInvoiceListGrid.gridView, mergeCells);

        taxBillInvoiceListGrid.gridView.setColumnProperty("confirmYn", "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    initDataProvider : function() {
        taxBillInvoiceListGrid.dataProvider.setFields(taxBillInvoiceListBaseInfo.dataProvider.fields);
        taxBillInvoiceListGrid.dataProvider.setOptions(taxBillInvoiceListBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        taxBillInvoiceListGrid.gridView.onDataCellClicked = function(gridView, index) {
            taxBillInvoiceListGrid.selectRow(index.dataRow);
        };
    },
    selectRow: function (selectRowId) {
        var rowDataJson = taxBillInvoiceListGrid.dataProvider.getJsonRow(selectRowId);

        $('#taxReceiveNo').text(rowDataJson.taxReceiveNo);
        $("#taxBillReceiveType option:selected").text(rowDataJson.taxBillReceiveType);
        $("#supplier").val(rowDataJson.supplier).prop('selected', true);
        $('#supplierInfo').trigger('change');
        $('#partnerNm').text(rowDataJson.partnerNm);
        $('#partnerOwner').text(rowDataJson.partnerOwner);
        $('#partnerNo').text(rowDataJson.partnerNo);
        $('#issueDt').val(rowDataJson.issueDt);
        $('#sumAmt').text($.jUtil.comma(rowDataJson.sumAmt));
        $('#supplyAmt').val(rowDataJson.supplyAmt);
        $('#vatAmt').val(rowDataJson.vatAmt);
        $('#partnerId').val(rowDataJson.partnerId);

        if (rowDataJson.confirmYn == 'Y') {
            $('#regSubmitBtn, #supplyAmt, #vatAmt, #issueDt, #supplier, #taxBillReceiveType').attr('disabled', true);
        } else {
            $('#regSubmitBtn, #supplyAmt, #vatAmt, #issueDt, #supplier, #taxBillReceiveType').attr('disabled', false);
        }
    },
    setData : function(dataList) {
        taxBillInvoiceListGrid.dataProvider.clearRows();
        taxBillInvoiceListGrid.dataProvider.setRows(dataList);
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", { summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, expression: "sum"} });
        }
        _gridView.setColumnProperty("diffAmt", "header", { summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, expression: "sum"} });

        taxBillInvoiceListGrid.initMergeSumHeader();
    },
    excelDownload: function() {
        if(taxBillInvoiceListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }
        SettleCommon.setRealGridSumHeader(taxBillInvoiceListGrid.gridView, []);

        var fileName = taxBillInvoice.setExcelFileName("수취내역관리");
        taxBillInvoiceListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
        taxBillInvoiceListGrid.initMergeSumHeader();
    }
};
/***
 * 세금계산서
 */
var taxBillInvoice = {
    init : function () {
        $('#searchField').val('');
        this.initSearchDate('0d');
        this.initSupplier();
        $('#regSubmitBtn, #supplyAmt, #vatAmt, #issueDt, #supplier, #taxBillReceiveType').attr('disabled', false);
        $('#taxReceiveNo, #partnerNm, #partnerOwner, #partnerNo, #sumAmt').text('');
        $('#partnerId').val('');
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        taxBillInvoice.searchDate = Calendar.datePickerRange("schStartDt", "schEndDt");
        taxBillInvoice.searchDate.setEndDate("0");
        taxBillInvoice.searchDate.setStartDate(flag);
    },
    initSupplier : function () {
        CommonAjaxBlockUI.startBlockUI();
        var $supplier = $("select[name='supplier']");
        $supplier.empty();
        var $supplierInfo = $("select[name='supplierInfo']");
        $supplierInfo.empty();

        var selectBoxStr = '<option selected value="">전체</option>';
        var selectBoxReg = '<option selected value="">전체</option>';
        CommonAjax.basic({
            url: "/settle/taxBillInvoice/getTaxBillSupplierList.json?taxBillReceiveType="+ $('#taxBillReceiveType').val(),
            method : "GET",
            callbackFunc: function (res) {
                $.each(res, function (key, resList) { //수취정보
                    selectBoxStr += '<option value="'+ resList.supplier +'" >'+ resList.supplier + '</option>';
                    selectBoxReg += '<option value="'+ resList.supplier +'" >'+ resList.partnerNm +'|'+ resList.partnerNo +'|'+
                         resList.partnerId +'|'+ resList.partnerOwner +'</option>';
                });
                $('#supplier').html(selectBoxStr);
                $('#supplierInfo').html(selectBoxReg);

                CommonAjaxBlockUI.stopBlockUI();
            }, error: function(xhr) {
                alert('잠시 뒤에 시도해주세요.');
                return;
            }
        });
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(taxBillInvoice.search);
        $('#searchResetBtn').bindClick(taxBillInvoice.resetSearchForm);
        $('#excelDownloadBtn').bindClick(taxBillInvoiceListGrid.excelDownload);
        $('#regSubmitBtn').bindClick(taxBillInvoice.insert);
        $('#regResetBtn').bindClick(taxBillInvoice.resetRegForm);
        $('#confirmBtn').bindClick(taxBillInvoice.confirm);
        $('#deleteBtn').bindClick(taxBillInvoice.delete);

        $("#issueDt").bind('input', function () {
            var value = $(this).val();
            value = value.replace(/[^0-9/-]/g, '');
            if (value.split('-').length < 3 && (value.length == 4 || value.length == 7)){
                value += '-';
            } else if (value.length >= 10) {
                $('#supplyAmt').focus();
            }
            $('#issueDt').val(value);
        });

        $('#vatAmt').bind('input', function () {
            var value = $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                if (value == "") {
                    $('#sumAmt').text(0);
                }
                $(this).focus();
                return;
            } else {
                var supplyAmt = $('#supplyAmt').val()==""? 0 : Number($('#supplyAmt').val());
                $('#sumAmt').text($.jUtil.comma(Number(value) + supplyAmt));
            }
        });

        $('#supplyAmt').bind('input', function () {
            var value = $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                if (value == "") {
                    $('#sumAmt').text(0);
                }
                $(this).focus();
                return;
            } else {
                var vatAmt = $('#vatAmt').val()==""? 0 : Number($('#vatAmt').val());
                $('#sumAmt').text($.jUtil.comma(Number(value) + vatAmt));
            }
        });

        $('#supplier').on('change', function() {
            var supplier = $(this).val();
            $("#supplierInfo").val(supplier);
            if ($('#supplier').val() != "") {
                var partnerInfo = $("#supplierInfo option:selected").text().split('|');
                $('#partnerNm').text(partnerInfo[0]);
                $('#partnerNo').text(partnerInfo[1]);
                $('#partnerId').val(partnerInfo[2]);
                $('#partnerOwner').text(partnerInfo[3]);
                $('#issueDt').focus();
            } else {
                $('#partnerNm, #partnerOwner, #partnerNo').text('');
                $('#partnerId').val('');
            }
        });
    },
    valid : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        return true;
    },
    confirm : function () {
        var checkData = "";
        $.each(taxBillInvoiceListGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
            var taxReceiveNo = taxBillInvoiceListGrid.dataProvider.getJsonRow(_dataRowId).taxReceiveNo;
            checkData += (taxReceiveNo + ",");
        });
        checkData = (checkData.slice(0, -1)).split(',');

        if(checkData == "" || checkData == ",") {
            alert("확정할 수취ID를 선택해주세요.");
            return false;
        }

        if(confirm("선택하신 건을 확정하시겠습니까? 확정 후에는 수정이 불가능합니다.")) {
            CommonAjax.basic({
                url: "/settle/taxBillInvoice/setTaxBillReceiveConfirm.json?taxReceiveNo=" + encodeURI(checkData),
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    if (res.returnCode == '0') {
                        alert("확정 처리되었습니다.\n전표조회를 확인해주세요.");
                        taxBillInvoice.search();
                    } else {
                        alert(res.returnMessage);
                    }
                }
            });
        }
    },
    delete : function () {
        var checkData = "";
        $.each(taxBillInvoiceListGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
            var taxReceiveNo = taxBillInvoiceListGrid.dataProvider.getJsonRow(_dataRowId).taxReceiveNo;
            checkData += (taxReceiveNo + ",");
        });
        checkData = (checkData.slice(0, -1)).split(',');

        if(checkData == "" || checkData == ",") {
            alert("삭제할 수취ID를 선택해주세요.");
            return false;
        }

        var loopCnt = checkData.length;
        if(confirm("선택하신 건을 목록에서 삭제하시겠습니까?")) {
            for (var i = 0; i < loopCnt; i++) {
                var taxReceiveNo = checkData[i];
                CommonAjax.basic({
                    url: "/settle/taxBillInvoice/deleteTaxBillReceive.json?taxReceiveNo="+taxReceiveNo,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (res) {
                    }
                });
            }
            alert('삭제되었습니다.');
            taxBillInvoice.search();
        }
    },
    search : function () {
        if(!taxBillInvoice.valid()) return false;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val();
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYY-MM-DD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;
        searchForm += '&taxBillReceiveType='+ $('#taxBillReceiveType').val();

        CommonAjax.basic(
            {
                url:'/settle/taxBillInvoice/getTaxBillReceiveList.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    taxBillInvoiceListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(taxBillInvoiceListGrid.gridView.getItemCount()));

                    //그룹헤더컬럼 합계
                    var stringCol = [ "sumAmt", "supplyAmt", "vatAmt", "commissionSumAmt", "commissionSupplyAmt", "commissionVatAmt"];
                    taxBillInvoiceListGrid.setRealGridHeaderSum(taxBillInvoiceListGrid.gridView, stringCol);

                    //건수가 0건이 아닌 경우에만 수행
                    if(taxBillInvoiceListGrid.gridView.getItemCount() > 0) {
                        //체크박스 선택조건 설정.
                        taxBillInvoiceListGrid.gridView.setCheckableExpression("( (values['confirmYn'] = 'N') )", true);
                    }

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            taxBillInvoice.init();
        });
        taxBillInvoice.resetRegForm();
    },
    resetRegForm : function () {
        $("form[name=registerForm]").each(function () {
            this.reset();
        });
        $('#taxReceiveNo, #partnerNm, #partnerOwner, #partnerNo, #sumAmt').text('');
        $('#regSubmitBtn, #supplyAmt, #vatAmt, #issueDt, #supplier, #taxBillReceiveType').attr('disabled', false);
        $('#partnerId').val('');
    },
    insert : function () {
        var supplier = $("#supplier").val();
        if (supplier == "") {
            alert("공급처를 선택하세요.");
            $("#supplier").focus();
            return false;
        }

        var supplyAmt = $("#supplyAmt").val();
        if (supplyAmt == "" || !$.isNumeric(supplyAmt) || isNaN(supplyAmt)) {
            alert("공급가액 입력값을 확인해주세요.");
            $("#supplyAmt").focus();
            return false;
        }

        var vatAmt = $("#vatAmt").val();
        if (vatAmt == "" || !$.isNumeric(vatAmt) || isNaN(vatAmt)) {
            alert("부가세 입력값을 확인해주세요.");
            $("#vatAmt").focus();
            return false;
        }
        $("#sumAmt").text($.jUtil.comma(Number(vatAmt) + Number(supplyAmt)));

        var issueDt = moment($("#issueDt").val(), 'YYYY-MM-DD', true);
        if(!issueDt.isValid()) {
            alert("발행일을 확인해주세요.");
            $("#issueDt").focus();
            return false;
        }

        var searchForm = $("form[name=registerForm]").serialize();
        searchForm = searchForm.replace('supplier', '');
        searchForm += "&taxReceiveNo="+ $("#taxReceiveNo").text();
        searchForm += "&partnerNm="+ $("#partnerNm").text();
        searchForm += "&partnerOwner="+ $("#partnerOwner").text();
        searchForm += "&partnerNo="+ $("#partnerNo").text();
        searchForm += "&supplyAmt="+ $("#supplyAmt").val();
        searchForm += "&vatAmt="+ $("#vatAmt").val();
        searchForm += "&supplier="+ $("#supplier option:selected").val();
        searchForm += "&sumAmt="+ $("#sumAmt").text().replace(/,/gi, "");

        var _url = ($("#taxReceiveNo").text()!='')? "/settle/taxBillInvoice/updTaxBillReceive.json?" : "/settle/taxBillInvoice/setTaxBillReceiveList.json?";
        var _msg = ($("#taxReceiveNo").text()!='')? "수정" : "등록";

        if (!confirm("수취내역 "+_msg+"하시겠습니까?")) return false;
        CommonAjax.basic({
            url: _url + searchForm,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                alert(res.returnMessage);
                if (res.returnCode == '0') {
                    taxBillInvoice.resetRegForm();
                    taxBillInvoice.search();
                }
            }
        });
    }
};