var marketSettleListGrid = {
    gridView : new RealGridJS.GridView("marketSettleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        marketSettleListGrid.initGrid();
        marketSettleListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        marketSettleListGrid.gridView.setDataSource(marketSettleListGrid.dataProvider);
        marketSettleListGrid.gridView.setStyles(marketSettleListBaseInfo.realgrid.styles);
        marketSettleListGrid.gridView.setDisplayOptions(marketSettleListBaseInfo.realgrid.displayOptions);
        marketSettleListGrid.gridView.setColumns(marketSettleListBaseInfo.realgrid.columns);
        marketSettleListGrid.gridView.setOptions(marketSettleListBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [
            ["paymentCompleteDt", "basicDt" , "payScheduleDt", "siteType", "marketOrderNo"
                , "marketOrderItemNo", "orderType", "gubun", "itemNo", "commissionRate"]
        ];
        SettleCommon.setRealGridSumHeader(marketSettleListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        marketSettleListGrid.dataProvider.setFields(marketSettleListBaseInfo.dataProvider.fields);
        marketSettleListGrid.dataProvider.setOptions(marketSettleListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        marketSettleListGrid.dataProvider.clearRows();
        marketSettleListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(marketSettleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        marketSettleListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: marketSettleListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};
/***
 * 마켓주문조회
 */
var marketSettleListForm = {
    init : function () {
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        marketSettleListForm.setDate = Calendar.datePickerRange('startDt', 'endDt');
        marketSettleListForm.setDate.setEndDate('-1d');
        marketSettleListForm.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(marketSettleListForm.search);
        $('#clearBtn').bindClick(marketSettleListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(marketSettleListGrid.excelDownload);
        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchValue').attr('readonly', true).val('');
            } else {
                $('#searchValue').removeAttr('readonly');
                $('#searchValue').focus();
            }
        });
    },
    search : function () {
        var startDt = moment($("#startDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#endDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#startDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#startDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var dateType = $("#dateType").val();
        if (dateType != "") {
            searchForm = searchForm + "&dateType=" + dateType;
        }
        var siteType = $("#siteType").val();
        if (siteType != "") {
            searchForm = searchForm + "&siteType=" + siteType;
        }
        var searchType = $("#searchType").val();
        var searchValue = $("#searchValue").val();
        if (searchType != "") {
            searchForm = searchForm + "&" + searchType + "=" + searchValue;
        }
        marketSettleListGrid.fileName = "마켓주문조회_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
            {
                url:'/settle/market/getSettleList.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    marketSettleListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(res.length));
                    //그룹헤더컬럼 합계
                    var stringCol = [ "settleAmt" , "completeAmt", "orderPrice" ,"marketDiscountHomeAmt", "marketDiscountMarketAmt"
                        , "commissionSumAmt", "saleAgencyFee" ,"commissionAmt", "etcAmt", "deductAmt"];
                    marketSettleListForm.setRealGridHeaderSum(marketSettleListGrid.gridView, stringCol);
                }
            });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            marketSettleListForm.init();
        });
    }
};