/**
 * 정산관리 그리드
 */
var partnerSettleGrid = {
    gridView : new RealGridJS.GridView("partnerSettleMngGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerSettleGrid.initGrid();
        partnerSettleGrid.initDataProvider();
        partnerSettleGrid.event();
    },
    initGrid : function() {
        partnerSettleGrid.gridView.setDataSource(partnerSettleGrid.dataProvider);

        partnerSettleGrid.gridView.setStyles(partnerSettleMngBaseInfo.realgrid.styles);
        partnerSettleGrid.gridView.setDisplayOptions(partnerSettleMngBaseInfo.realgrid.displayOptions);
        partnerSettleGrid.gridView.setColumns(partnerSettleMngBaseInfo.realgrid.columns);
        partnerSettleGrid.gridView.setOptions(partnerSettleMngBaseInfo.realgrid.options);

        var mergeCells = [ ["actionType", "settleState", "settleSrl", "settlePreDt", "payMethodNm", "settleCycleType", "salesPeriod", "partnerId", "vendorCd", "partnerNm", "businessNm"],
            ["chgDt", "chgId"] ];
        SettleCommon.setRealGridSumHeader(partnerSettleGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        partnerSettleGrid.dataProvider.setFields(partnerSettleMngBaseInfo.dataProvider.fields);
        partnerSettleGrid.dataProvider.setOptions(partnerSettleMngBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        partnerSettleGrid.gridView.onLinkableCellClicked = function (grid, index, url) {
            var partnerId = ("?partnerId=" + partnerSettleGrid.dataProvider.getJsonRow(index.dataRow).partnerId);
            var partnerNm = ("&partnerNm=" + partnerSettleGrid.dataProvider.getJsonRow(index.dataRow).partnerNm);
            var settleSrl = ("&settleSrl=" + partnerSettleGrid.dataProvider.getJsonRow(index.dataRow).settleSrl);

            url += encodeURI(partnerId + partnerNm + settleSrl);
            //탭생성
            SettleCommon.setLinkTab("partnerDetail", "판매자 정산 정보", url, 200);
        }

        //이미지 버튼 클릭시 Action
        partnerSettleGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            //처리에 필요한 정보를 가지고 온다.
            var settleNo = partnerSettleGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settleNo;
            var settleSrl = partnerSettleGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settleSrl;
            var settlePreDt = partnerSettleGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settlePreDt;

            //파라미터의 수가 0보다 클경우에만 Action
            if(settleNo.length > 0){
                switch (name) {
                    case 'settleComplete' : //정산완료
                        partnerSettle.settleComplete(settleSrl, false);
                        break;
                    case 'settlePostpone' : // 정산보류
                        partnerSettle.settlePostpone(settleSrl, false, settlePreDt);
                        break;
                    case 'postponeCancel' : //보류취소
                        partnerSettle.postponeCancel(settleSrl, false);
                        break;
                }
            }
        };
    },
    setData : function(dataList) {
        partnerSettleGrid.dataProvider.clearRows();
        partnerSettleGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerSettleGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var mergeCells = [ ["settleState", "settleSrl", "settlePreDt", "payMethodNm", "settleCycleType", "salesPeriod", "partnerId", "vendorCd", "partnerNm", "businessNm"],
            ["chgDt", "chgId"] ];
        SettleCommon.setRealGridColumnVisible(partnerSettleGrid.gridView, [SettleCommon.getRealGridColumnName(partnerSettleGrid.gridView, "처리")], false);
        partnerSettleGrid.setRealGridSumHeader(partnerSettleGrid.gridView, mergeCells);

        var fileName = partnerSettle.setExcelFileName("DS정산관리");
        partnerSettleGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            allColumns : false,
            //showProgress: true,
            applyDynamicStyles : true,
            pagingAllItems : true,
            progressMessage: "엑셀 Export중입니다."
        });

        mergeCells = [ ["actionType", "settleState", "settleSrl", "settlePreDt", "payMethodNm", "settleCycleType", "salesPeriod", "partnerId", "vendorCd", "partnerNm", "businessNm"],
            ["chgDt", "chgId"] ];
        SettleCommon.setRealGridColumnVisible(partnerSettleGrid.gridView, [SettleCommon.getRealGridColumnName(partnerSettleGrid.gridView, "처리")], true);
        SettleCommon.setRealGridHeaderSum(partnerSettleGrid.gridView, []);
        SettleCommon.setRealGridSumHeader(partnerSettleGrid.gridView, mergeCells);


    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[1], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
};
// 정산관리
var partnerSettle = {
    init : function() {
        this.initSearchDate('-1d');
        this.initSearchYM();
        $('#searchField').val('');
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,PARTNER_NM,SETTLE_SRL");
    },
    initSearchDate : function (flag) {
        partnerSettle.setDate = Calendar.datePickerRange('schStartDt','schEndDt');
        partnerSettle.setDate.setEndDate(0);
        partnerSettle.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(partnerSettle.search);
        $('#searchResetBtn').bindClick(partnerSettle.searchFormReset);
        $('#excelDownloadBtn').bindClick(partnerSettleGrid.excelDownload);

        $(".button").on("click", function () {
            var id = $(this).attr('id');
            switch (id) {
                case 'settleCompleteBtn':
                    partnerSettle.setPartnerSettleState(2, true);
                    break;
                case 'settlePostponeBtn':
                    partnerSettle.setPartnerSettleState(4, true);
                    break;
                case 'postponeCancelBtn':
                    partnerSettle.setPartnerSettleState(1, true);
                    break;
            }
        });

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });

    },
    valid : function() {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                SettleCommon.setMonth(year,'endMonth');
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                SettleCommon.setMonth(year,'endMonth');
                $("#endMonth").val(month);
                return false;
            }

        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색조건을 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'PARTNER_ID' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        } else if($("#searchType").val() == 'SETTLE_SRL' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('지급SRL은 숫자만 입력 가능합니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    search : function() {
        if (!partnerSettle.valid()) return false;
            CommonAjaxBlockUI.startBlockUI();

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var buttonInfo = {
            buttonCnt : 2,

            buttonName : "buttons", //다중으로 버튼을 생성할경우 buttonName + (1~buttonCnt)
            buttonImg : [
                {name : "settleComplete", img : "/static/images/settle/btn_settleComplete.png", width:70},
                {name : "settlePostpone", img : "/static/images/settle/btn_settlePostpone.png", width:70},
                {name : "postponeCancel", img : "/static/images/settle/btn_postponeCancel.png", width:70}
            ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["settleComplete,settlePostpone","postponeCancel"],
            value : ["settleStandBy", "postponeCancel"],
            isValueNotOption : false
        };

        var searchForm = $("form[name=searchForm]").serialize();

        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "PARTNER_ID") {
            searchForm += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            searchForm += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "SETTLE_SRL") {
            searchForm += "&settleSrl="+ $('#searchField').val();
        }

        CommonAjax.basic({
            url: '/settle/partnerSettle/getPartnerSettleList.json?'+ encodeURI(searchForm),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                partnerSettleGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerSettleGrid.gridView.getItemCount()));

                //파트너ID에 링크생성
                var detailUrl = "/settle/partnerSettleDetail/partnerSettleDetail";
                SettleCommon.setRealGridColumnLink(partnerSettleGrid.gridView, "settleSrl", detailUrl);
                SettleCommon.setRealGridColumnLink(partnerSettleGrid.gridView, "partnerId", detailUrl);
                SettleCommon.setRealGridColumnLink(partnerSettleGrid.gridView, "partnerNm", detailUrl);

                var targetColumn = SettleCommon.getRealGridColumnName(partnerSettleGrid.gridView, "처리");

                $.each(res, function(_rowId, _data){
                    if(_data.settleState.indexOf("정산대기") > -1) {
                        partnerSettleGrid.dataProvider.setValue(_rowId, targetColumn, "settleStandBy");
                    } else if(_data.settleState.indexOf("정산보류(수동)") > -1){
                        partnerSettleGrid.dataProvider.setValue(_rowId, targetColumn, "postponeCancel");
                    }
                });

                //건수가 0건이 아닌 경우에만 수행
                if(partnerSettleGrid.gridView.getItemCount() > 0) {
                    //체크박스 선택조건 설정.
                    partnerSettleGrid.gridView.setCheckableExpression("( (values['settleState'] = '정산대기') or (values['settleState'] = '정산보류(수동)') )", true);
                    //버튼생성
                    SettleCommon.setRealGridCellButton(partnerSettleGrid.gridView, buttonInfo, targetColumn);
                }

                //그룹헤더컬럼 합계
                SettleCommon.setRealGridHeaderSum(partnerSettleGrid.gridView, []);
                CommonAjaxBlockUI.stopBlockUI();
            }
        });

    },
    searchFormReset : function() {
        $("form[name=searchForm]").each(function() {
            this.reset();
            partnerSettle.init();
        });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val() +'_'+$("#endYear").val() + $("#endMonth").val();
        }
        return excelFileName;
    },
    setPartnerSettleState : function (settleProcessType, isMulti) {
        var data = ""; var isNotEqualState; var isOutOfState; var isTimeout; var lastState = "";
        var minPreDt = new Date().toLocaleDateString();
        if (isMulti) {
            $.each(partnerSettleGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
                var settleSrl = partnerSettleGrid.dataProvider.getJsonRow(_dataRowId).settleSrl;
                var settleState = partnerSettleGrid.dataProvider.getJsonRow(_dataRowId).settleState;
                var settlePreDt = partnerSettleGrid.dataProvider.getJsonRow(_dataRowId).settlePreDt;

                var settleStateInfo = new Map();
                settleStateInfo.set(1, "정산보류(수동)");
                settleStateInfo.set(2, "정산대기");
                settleStateInfo.set(4, "정산대기");

                if (lastState == "") {
                    lastState = settleState;
                } else if (lastState != settleState) {
                    isNotEqualState = true;
                    return false;
                }
                if (settlePreDt < minPreDt) {
                    minPreDt = settlePreDt;
                }
                //액션 수행
                if(settleState.indexOf(settleStateInfo.get(settleProcessType)) > -1){
                    if (settleProcessType == 4) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        if (minPreDt < today) {
                            isTimeout = true;
                            return false;
                        }
                    }
                    data += (settleSrl + ",");
                } else {
                    isOutOfState = true;
                    return false;
                }
            });
            $('#CheckedData').val(data.slice(0, -1));
        } else {
            $('#CheckedData').val(data);
        }

        if (isNotEqualState) {
            alert("선택하신 내역들의 지급상태가 모두 동일해야 합니다.");
            return false;
        }
        if (isTimeout) {
            alert("요청하신 상태로 변경이 불가능합니다. 지급에정일을 확인하세요");
            return false;
        }
        if (isOutOfState) {
            alert("요청하신 상태로 변경이 불가능합니다. 상태를 재확인하세요");
            return false;
        }
        if (data == "," || data == "") {
            alert("처리할 대상이 없습니다. 확인 후 진행해 주세요.");
            return false;
        }

        switch (settleProcessType) {
            case 1 : partnerSettle.postponeCancel($('#CheckedData').val(), isMulti); break; //정산보류 해제
            case 2 : partnerSettle.settleComplete($('#CheckedData').val(), isMulti); break; //정산완료
            case 4 : partnerSettle.settlePostpone($('#CheckedData').val(), isMulti, ""); break; //정산보류
            //case 11 : partnerSettle.setSettleCancel($('#CheckedData').val()); break; //정산취소
        }
    },
    settleComplete : function (data, isMulti) {
        var checkedData = ""; var loopCnt = 0; var successCnt = 0; var failCnt = 0; var failSettleSrl = "";
        if ( $("#CheckedData").val() != null &&  $("#CheckedData").val() != "" && $("#CheckedData").val().length > 0 && isMulti) {
            checkedData = $("#CheckedData").val().split(','); loopCnt = checkedData.length;
        } else {
            checkedData = data; loopCnt = 1;
        }
        if(confirm("정산완료 처리하시겠습니까? (총 " +loopCnt+ "건)")) {
            for (var i = 0; i < loopCnt; i++) {
                var settleSrl = loopCnt==1? data: checkedData[i];

                CommonAjax.basic({
                    url: "/settle/partnerSettle/setSettleComplete.json?settleSrl="+settleSrl,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (resData) {
                        if (resData.returnCode == '0') {
                            successCnt++;
                        } else {
                            failCnt++;
                            failSettleSrl += settleSrl + ',';
                        }
                        if (i == loopCnt && loopCnt == (successCnt + failCnt)) {
                            if (failCnt > 0) {
                                alert("이미 변경 완료된 건이 있어 요청한 처리가 불가합니다. 재검색하여 처리 가능한 건 확인 후 진행해 주세요. (성공 " + successCnt
                                    + "건, 실패 " + failCnt + "건, 실패지급SRL: " + failSettleSrl + ")");
                            } else {
                                alert("처리 완료되었습니다. (총 선택 " + loopCnt + "건, 성공 " + successCnt + "건, 실패 " + failCnt + "건)");
                                partnerSettle.search();
                            }
                        }  //result message
                    }
                })
            }
        }

    },
    settlePostpone : function (data, isMulti, settlePreDt) {
        var checkedData = ""; var loopCnt = 0; var successCnt = 0; var failCnt = 0; var failSettleSrl = "";
        if ( $("#CheckedData").val() != null &&  $("#CheckedData").val() != "" && $("#CheckedData").val().length > 0 && isMulti) {
            checkedData = $("#CheckedData").val().split(','); loopCnt = checkedData.length;
        } else {
            var today = moment(new Date()).format("YYYY-MM-DD");
            if (settlePreDt < today) {
                alert("요청하신 상태로 변경이 불가능합니다. 지급에정일을 확인하세요");
                return false;
            }
            checkedData = data; loopCnt = 1;
        }
        if(confirm("정산보류 처리하시겠습니까? (총 " +loopCnt+ "건)")) {
            for (var i = 0; i < loopCnt; i++) {
                var settleSrl = loopCnt==1? data: checkedData[i];

                CommonAjax.basic({
                    url: "/settle/partnerSettle/setSettlePostpone.json?settleSrl="+settleSrl,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (resData) {
                        if (resData.returnCode == '0') {
                            successCnt++;
                        } else {
                            failCnt++;
                            failSettleSrl += settleSrl + ',';
                        }
                        if (i == loopCnt && loopCnt == (successCnt + failCnt)) {
                            if (failCnt > 0) {
                                alert("이미 변경 완료된 건이 있어 요청한 처리가 불가합니다. 재검색하여 처리 가능한 건 확인 후 진행해 주세요. (성공 " + successCnt
                                    + "건, 실패 " + failCnt + "건, 실패지급SRL: " + failSettleSrl + ")");
                            } else {
                                alert("처리 완료되었습니다. (총 선택 " + loopCnt + "건, 성공 " + successCnt + "건, 실패 " + failCnt + "건)");
                                partnerSettle.search();
                            }
                        }  //result message
                    }
                })
            }
        }
    },
    postponeCancel : function (data, isMulti) {
        var checkedData = ""; var loopCnt = 0; var successCnt = 0; var failCnt = 0; var failSettleSrl = "";
        if ( $("#CheckedData").val() != null &&  $("#CheckedData").val() != "" && $("#CheckedData").val().length > 0 && isMulti) {
            checkedData = $("#CheckedData").val().split(','); loopCnt = checkedData.length;
        } else {
            checkedData = data; loopCnt = 1;
        }
        if(confirm("정산보류해제 처리하시겠습니까? (총 " +loopCnt+ "건)")) {
            for (var i = 0; i < loopCnt; i++) {
                var settleSrl = loopCnt==1? data: checkedData[i];

                CommonAjax.basic({
                    url: "/settle/partnerSettle/setPostponeCancel.json?settleSrl="+settleSrl,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (resData) {
                        if (resData.returnCode == '0') {
                            successCnt++;
                        } else {
                            failCnt++;
                            failSettleSrl += settleSrl + ',';
                        }
                        if (i == loopCnt && loopCnt == (successCnt + failCnt)) {
                            if (failCnt > 0) {
                                alert("이미 변경 완료된 건이 있어 요청한 처리가 불가합니다. 재검색하여 처리 가능한 건 확인 후 진행해 주세요. (성공 " + successCnt
                                    + "건, 실패 " + failCnt + "건, 실패지급SRL: " + failSettleSrl + ")");
                            } else {
                                alert("처리 완료되었습니다. (총 선택 " + loopCnt + "건, 성공 " + successCnt + "건, 실패 " + failCnt + "건)");
                                partnerSettle.search();
                            }
                        }  //result message
                    }
                })
            }
        }
    },
};
