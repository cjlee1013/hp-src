/*
* 계정과목별 데이터 조회
*/
var accountSubjectListGrid = {
    gridView : new RealGridJS.GridView("accountSubjectListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        accountSubjectListGrid.initGrid();
        accountSubjectListGrid.initDataProvider();
    },
    initGrid : function() {
        accountSubjectListGrid.gridView.setDataSource(accountSubjectListGrid.dataProvider);
        accountSubjectListGrid.gridView.setStyles(accountSubjectListBaseInfo.realgrid.styles);
        accountSubjectListGrid.gridView.setDisplayOptions(accountSubjectListBaseInfo.realgrid.displayOptions);
        accountSubjectListGrid.gridView.setColumns(accountSubjectListBaseInfo.realgrid.columns);
        accountSubjectListGrid.gridView.setOptions(accountSubjectListBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        accountSubjectListGrid.dataProvider.setFields(accountSubjectListBaseInfo.dataProvider.fields);
        accountSubjectListGrid.dataProvider.setOptions(accountSubjectListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        accountSubjectListGrid.dataProvider.clearRows();
        accountSubjectListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(accountSubjectListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = accountSubjectListForm.setExcelFileName("전표조회");
        accountSubjectListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            //showProgress: true,
            applyDynamicStyles : true,
            pagingAllItems : true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};
/*
* 계정과목별 데이터 조회
*/
var accountSubjectListForm = {
    searchDate : null,
    init : function() {
        accountSubjectListForm.event();
        accountSubjectListForm.initSearchForm();
    },
    event : function() {
        $('#searchBtn').bindClick(accountSubjectListForm.search);
        $('#clearBtn').bindClick(accountSubjectListForm.initSearchForm);
        $('#excelDownload').bindClick(accountSubjectListGrid.excelDownload);
    },
    initSearchForm : function() {
        $("#adjustSearchForm").each(function () {
            this.reset();
        });

        //조회 일자 초기화
        accountSubjectListForm.initSearchDate("0");
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        accountSubjectListForm.searchDate = Calendar.datePickerRange("startDt", "endDt");
        accountSubjectListForm.searchDate.setEndDate("0");
        accountSubjectListForm.searchDate.setStartDate(flag);
    },
    // 내용 검증
    valid : function () {
        var startDt = $("#startDt").val();
        var endDt = $("#endDt").val();

        if(!moment(startDt,'YYYY-MM-DD',true).isValid() || !moment(endDt,'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        if (moment(endDt).diff(startDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            $("#startDt").val(moment(endDt).add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt).diff(startDt, "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#startDt").val(endDt);
            return false;
        }
        return true;
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        excelFileName = fileName +'_'+ $("#startDt").val().replace(/-/gi, "")+'_'+$("#endDt").val().replace(/-/gi, "");

        return excelFileName;
    },
    search : function () {
        if(!accountSubjectListForm.valid()) return false;
        CommonAjaxBlockUI.startBlockUI();
        var searchFormData = 'startDt='+$("#startDt").val().replace(/-/gi, "")+'&endDt='+$("#endDt").val().replace(/-/gi, "");

        CommonAjax.basic({
            url: '/settle/accounting/getAccountSubjectList.json?'+ searchFormData,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                accountSubjectListGrid.setData(res);
                CommonAjaxBlockUI.stopBlockUI();
            }
        });

    },
};
