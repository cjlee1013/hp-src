/**
 * 지급관리 그리드
 */
var partnerPaymentGrid = {
    gridView : new RealGridJS.GridView("partnerPaymentMngGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        partnerPaymentGrid.initGrid();
        partnerPaymentGrid.initDataProvider();
        partnerPaymentGrid.event();
        CommonAjaxBlockUI.global();
    },
    initGrid : function() {
        partnerPaymentGrid.gridView.setDataSource(partnerPaymentGrid.dataProvider);
        partnerPaymentGrid.gridView.setStyles(partnerPaymentMngBaseInfo.realgrid.styles);
        partnerPaymentGrid.gridView.setDisplayOptions(partnerPaymentMngBaseInfo.realgrid.displayOptions);
        partnerPaymentGrid.gridView.setOptions(partnerPaymentMngBaseInfo.realgrid.options);
        partnerPaymentGrid.gridView.setColumns(partnerPaymentMngBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [
            ["actionType", "settleSrl" , "settlePayState", "payMethod", "partnerId", "ofVendorCd", "partnerName"
            , "partnerNm", "settlePreDt", "setSettlePreDt", "settleDt", "settleCycleType" ]
            ,[ "partnerNo" , "bankInfo" , "bankNm" , "bankAccountNo" , "bankAccountHolder" , "payPartnerId"
                , "payCompany" , "chgDt" , "chgId" ] ];
        SettleCommon.setRealGridSumHeader(partnerPaymentGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        partnerPaymentGrid.dataProvider.setFields(partnerPaymentMngBaseInfo.dataProvider.fields);
        partnerPaymentGrid.dataProvider.setOptions(partnerPaymentMngBaseInfo.dataProvider.options);
    },
    event : function() {
        //이미지 버튼 클릭시 Action
        partnerPaymentGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            //처리에 필요한 정보를 가지고 온다.
            var partnerId = partnerPaymentGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).partnerId;
            var settleSrl = partnerPaymentGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settleSrl;
            var settlePayState = partnerPaymentGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settlePayState;
            var settlePreDt = partnerPaymentGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).settlePreDt;
            var payMethod = partnerPaymentGrid.dataProvider.getJsonRow(grid.getCurrent().dataRow).payMethod;

            //파라미터의 수가 0보다 클경우에만 Action
            if(partnerId.length > 0){
                switch (name) {
                    case 'payComplete' : //지급완료
                        if ('지급대행' == payMethod) {
                            alert ('지급대행을 통해 지급하는 경우 수동 지급처리가 불가합니다.');
                            return;
                        }
                        partnerPayment.setPayState(5,false, settleSrl,settlePayState,settlePreDt);
                        break;
                    case 'payPostpone' : /// 보류
                        partnerPayment.setPayState(3,false, settleSrl,settlePayState,settlePreDt);
                        break;
                    case 'payCancel' : /// 중지
                        partnerPayment.setPayState(4,false, settleSrl,settlePayState,settlePreDt);
                        break;
                    case 'payRequest' : /// 요청
                        partnerPayment.setPayState(2,false, settleSrl,settlePayState,settlePreDt);
                        break;
                    case 'payPostponeCancel' : //보류해제
                        partnerPayment.setPayState(1,false, settleSrl,settlePayState,settlePreDt);
                        break;
                    case 'edit' :
                        if (!['지급대기','지급보류','지급실패'].includes(settlePayState)) {
                            alert("지급예정일을 변경할 수 없습니다");
                            return;
                        }
                        partnerPayment.setSettlePreDt(partnerPaymentGrid.dataProvider, grid.getCurrent().dataRow);
                        break;
                    case 'payHomeAccount' :
                        if (!['지급대기','지급보류','지급실패'].includes(settlePayState)) {
                            alert("계좌를 변경할 수 없습니다");
                            return;
                        }
                        partnerPayment.setAccount(settleSrl,'homeplus');
                        break;
                    case 'paySellerAccount' :
                        if (!['지급대기','지급보류','지급실패'].includes(settlePayState)) {
                            alert("계좌를 변경할 수 없습니다");
                            return;
                        }
                        partnerPayment.setAccount(settleSrl,partnerId);
                        break;
                }
            }
        };
    },
    setData : function(dataList) {
        partnerPaymentGrid.dataProvider.clearRows();
        partnerPaymentGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(partnerPaymentGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "처리")], false);
        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "지급일변경")], false);
        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "계좌변경")], false);
        var fileName = partnerPayment.setExcelFileName("DS지급관리");
        partnerPaymentGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            allColumns : false,
            //showProgress: true,
            applyDynamicStyles : true,
            pagingAllItems : true,
            progressMessage: "엑셀 Export중입니다."
        });
        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "처리")], true);
        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "지급일변경")], true);
        SettleCommon.setRealGridColumnVisible(partnerPaymentGrid.gridView, [SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "계좌변경")], true);
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};
// 지급관리
var partnerPayment = {
    init: function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
        $('#searchField').val('');
    },
    initSearchDate: function (flag) {
        partnerPayment.setDate = Calendar.datePickerRange('schStartDt','schEndDt');
        partnerPayment.setDate.setEndDate('-1d');
        partnerPayment.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSearchYM : function () {
        var year = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schEndDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },
    bindingEvent: function () {
        $('#searchBtn').bindClick(partnerPayment.search);
        $('#searchResetBtn').bindClick(partnerPayment.searchFormReset);
        $('#excelDownloadBtn').bindClick(partnerPaymentGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });
    },
    setSettlePreDt : function (_gridDataProvider, _rowId) {
        var settleSrl = ("&settleSrl=" + _gridDataProvider.getJsonRow(_rowId).settleSrl);
        var partnerId = ("&partnerId=" + _gridDataProvider.getJsonRow(_rowId).partnerId);
        var settlePreDt = ("&settlePreDt=" + _gridDataProvider.getJsonRow(_rowId).settlePreDt);
        var partnerNm = ("&partnerNm=" + _gridDataProvider.getJsonRow(_rowId).partnerName);

        var _url = encodeURI("/settle/partnerPayment/popup/setSettlePreDt?" + settleSrl + partnerId + settlePreDt + partnerNm);
        windowPopupOpen(_url, "_blank", 610, 390);
    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            var endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                var year = $("#startYear").val();
                var month = $("#startMonth").val();
                $("#startYear").val($("#endYear").val());
                $("#startMonth").val($("#endMonth").val());
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 93) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $("#endYear").val(year);
                $("#endMonth").val(month);
                return false;
            }

        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색키워드를 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'partnerId' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        } else if($("#searchType").val() == 'partnerNo' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('사업자번호는 숫자만 입력 가능합니다.');
            $("#searchField").focus();
            return false;
        } else if($("#searchType").val() == 'bankAccountNo' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('계좌번호는 숫자만 입력 가능합니다.');
            $("#searchField").focus();
            return false;
        } else if($("#searchType").val() == 'settleSrl' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('지급SRL은 숫자만 입력 가능합니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    search: function () {
        if (!partnerPayment.valid()) return false;

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var editButtonInfo = {
            buttonCnt : 1, //case 3
            buttonName : "editBtn",
            buttonImg : [{name : "edit", img : "/static/images/settle/btn_edit.png", width:40}],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["edit"],
            value : [true],
            isValueNotOption : false
        };

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var stateButtonInfo = {
            buttonCnt : 4,
            buttonName : "stateBtn", //다중으로 버튼을 생성할경우 buttonName + (1~buttonCnt)
            buttonImg : [
                {name : "payComplete", img : "/static/images/settle/btn_payComplete.png", width:70},
                {name : "payPostpone", img : "/static/images/settle/btn_payPostpone.png", width:70},
                {name : "payRequest", img : "/static/images/settle/btn_payRequest.png", width:70},
                {name : "payCancel", img : "/static/images/settle/btn_payCancel.png", width:70},
                {name : "payPostponeCancel", img : "/static/images/settle/btn_payPostponeCancel.png", width:70}
            ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["payComplete,payPostpone","payCancel","payPostponeCancel","payRequest"],
            value : ["payStandBy","payRequest","payPostpone","payFail"],
            isValueNotOption : false
        };

        var accountButtonInfo = {
            buttonCnt : 2,
            buttonName : "payAccountBtn", //다중으로 버튼을 생성할경우 buttonName + (1~buttonCnt)
            buttonImg : [
                {name : "payHomeAccount", img : "/static/images/settle/btn_payHomeAccount.png", width:70},
                {name : "paySellerAccount", img : "/static/images/settle/btn_paySellerAccount.png", width:70},
            ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["payHomeAccount","paySellerAccount"],
            value : ["seller","homeplus"],
            isValueNotOption : false
        };

        var startDt;
        var endDt;
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = $("#schStartDt").val().split('-').join('');
            endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            endDt = $("#endYear").val() +'-'+ $("#endMonth").val() + '-01';
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm = '&startDt=' + startDt + '&endDt=' + endDt;
        if ($('#searchType').val() != "") {
            searchForm += "&" + $('#searchType').val() + "=" + $('#searchField').val();
        }
        if ($('#settlePayState').val() != "") {
            searchForm += "&settlePayState="+$('#settlePayState').val();
        }
        if ($('#payMethod').val() != "") {
            searchForm += "&payMethod="+$('#payMethod').val();
        }

        CommonAjax.basic({
            url: '/settle/partnerPayment/getPartnerPaymentList.json?'+ searchForm,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                partnerPaymentGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(partnerPaymentGrid.gridView.getItemCount()));

                var targetColumn = SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "처리");
                var setSettlePreDtColumn = SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "지급일변경");
                var payPartnerColumn = SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "계좌변경");
                var bankInfoColumn = SettleCommon.getRealGridColumnName(partnerPaymentGrid.gridView, "계좌정보");

                var isAccountChangeable = false, isPreDtChangeable;
                $.each(res, function(_rowId, _data){
                    if(_data.settlePayState.indexOf("지급대기") > -1) {
                        partnerPaymentGrid.dataProvider.setValue(_rowId, targetColumn, "payStandBy");
                        isAccountChangeable = true;
                        isPreDtChangeable = true;
                    } else if(_data.settlePayState.indexOf("지급요청") > -1){
                        partnerPaymentGrid.dataProvider.setValue(_rowId, targetColumn, "payRequest");
                        isAccountChangeable = false;
                        isPreDtChangeable = false;
                    } else if(_data.settlePayState.indexOf("지급보류") > -1){
                        partnerPaymentGrid.dataProvider.setValue(_rowId, targetColumn, "payPostpone");
                        isAccountChangeable = true;
                        isPreDtChangeable = true;
                    } else if(_data.settlePayState.indexOf("지급실패") > -1){
                        partnerPaymentGrid.dataProvider.setValue(_rowId, targetColumn, "payFail");
                        isAccountChangeable = false;
                        isPreDtChangeable = true;
                    }
                    if (isAccountChangeable) {
                        if (_data.payPartnerId.indexOf("homeplus") > -1) {
                            partnerPaymentGrid.dataProvider.setValue(_rowId,
                                payPartnerColumn, "homeplus");
                        } else {
                            partnerPaymentGrid.dataProvider.setValue(_rowId,
                                payPartnerColumn, "seller");
                        }
                    } else {
                        partnerPaymentGrid.dataProvider.setValue(_rowId,
                            payPartnerColumn, "");
                    }
                    if (isPreDtChangeable) {
                        partnerPaymentGrid.dataProvider.setValue(_rowId,
                            setSettlePreDtColumn, true);
                    }
                    partnerPaymentGrid.dataProvider.setValue(_rowId, bankInfoColumn, _data.bankNm+" / "+_data.bankAccountNo+" / "+_data.bankAccountHolder);
                });

                //건수가 0건이 아닌 경우에만 수행
                if(partnerPaymentGrid.gridView.getItemCount() > 0) {
                    //체크박스 선택조건 설정.
                    partnerPaymentGrid.gridView.setCheckableExpression("(value['settlePayState'] = '지급대기') or (value['settlePayState'] = '지급요청') or (value['settlePayState'] = '지급보류') or (value['settlePayState'] = '지급실패')", true);
                    //버튼생성
                    SettleCommon.setRealGridCellButton(partnerPaymentGrid.gridView, stateButtonInfo, targetColumn);
                    SettleCommon.setRealGridCellButton(partnerPaymentGrid.gridView, editButtonInfo, setSettlePreDtColumn);
                    SettleCommon.setRealGridCellButton(partnerPaymentGrid.gridView, accountButtonInfo, payPartnerColumn);
                }

                //그룹헤더컬럼 합계
                var stringCol = [ "settleAmt" ];
                partnerPaymentGrid.setRealGridHeaderSum(partnerPaymentGrid.gridView, stringCol);
            }
        });
    },
    setPayState : function (state, isMulti, settleSrl, settlePayState, settlePreDt) {
        var checkedData; var loopCnt; var successCnt = 0; var failCnt = 0; var failSettleSrl = "";
        var isNotEqualState;
        var isOutOfState;
        var isTimeout;
        var lastState = "";
        var minPreDt = new Date().toLocaleDateString();
        if (isMulti) {
            var rows = "";
            $.each(partnerPaymentGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
                settleSrl = partnerPaymentGrid.dataProvider.getJsonRow(_dataRowId).settleSrl;
                settlePayState = partnerPaymentGrid.dataProvider.getJsonRow(_dataRowId).settlePayState;
                settlePreDt = partnerPaymentGrid.dataProvider.getJsonRow(_dataRowId).settlePreDt;
                if (lastState == "") {
                    lastState = settlePayState;
                } else if (lastState != settlePayState) {
                    isNotEqualState = true;
                    return false;
                }
                if (settlePreDt < minPreDt) {
                    minPreDt = settlePreDt;
                }
                rows += (settleSrl + ",");
            });
            if (rows != "") {
                checkedData = rows.slice(0, -1).split(',');
                loopCnt = checkedData.length;
            } else {
                loopCnt = 0;
            }
        } else {
            checkedData = [settleSrl]; loopCnt = 1;
            lastState = settlePayState;
            minPreDt = settlePreDt;
        }
        if (loopCnt == 0) {
            alert("선택된 항목이 없습니다");
            return;
        }

        var today = moment(new Date()).format("YYYY-MM-DD");
        var now = moment(new Date()).format("HH:mm:ss");
        switch (state) {
            case 1:
                if (lastState != settlePayStateList[3]) {
                    isOutOfState = true;
                } else if (minPreDt < today) {
                    isTimeout = true;
                }
                break;
            case 2:
                if (lastState != settlePayStateList[4]) {
                    isOutOfState = true;
                } else if (minPreDt < today) {
                    isTimeout = true;
                }
                break;
            case 3:
                if (lastState != settlePayStateList[1]) {
                    isOutOfState = true;
                }
                break;
            case 4:
                if (lastState != settlePayStateList[2]) {
                    isOutOfState = true;
                } else if ('12:30:00' < now || minPreDt < today) {
                    isTimeout = true;
                }
                break;
            case 5:
                if (lastState != settlePayStateList[1]) {
                    isOutOfState = true;
                }
                break;
        }
        if (isNotEqualState) {
            alert("선택하신 내역들의 지급상태가 모두 동일해야 합니다.");
            return;
        }
        if (isOutOfState) {
            alert("요청하신 상태로 변경이 불가능합니다. 상태를 재확인하세요");
            return;
        }
        if (isTimeout) {
            alert("요청하신 상태로 변경이 불가능합니다. 지급에정일을 확인하세요");
            return;
        }
        var payStateNm = settlePayStateList[state];
        if(confirm("선택하신 건의 지급상태를 " + payStateNm + "로 변경하시겠습니까?")) {
            for (var i = 0; i < loopCnt; i++) {
                var data = checkedData[i];

                CommonAjax.basic({
                    url: "/settle/partnerPayment/setPartnerPaymentState.json?settleSrl="+data+"&settlePayState="+ state,
                    data: null,
                    method: "GET",
                    successMsg: null,
                    callbackFunc : function (resData) {
                        if (resData > 0) {
                            successCnt++;
                        } else {
                            failCnt++;
                            failSettleSrl += data + ',';
                        }
                        if (i == loopCnt && loopCnt == (successCnt + failCnt)) {
                            if (failCnt > 0) {
                                alert("일부 건의 지급상태 변경을 실패하였습니다 확인 후 재시도 바랍니다 (성공 " + successCnt
                                    + "건, 실패 " + failCnt + "건, 실패지급SRL: " + failSettleSrl + ")");
                            } else {
                                alert("선택하신 건의 지급상태가 "+ payStateNm +"로 변경되었습니다 \n(" + loopCnt + "건, 성공 " + successCnt + "건, 실패 " + failCnt + "건)");
                            }
                            partnerPayment.search();
                        }
                    }
                })
            }
        }

    },
    setAccount : function (settleSrl,partnerId) {
        if(confirm("선택하신 건의 지급계좌정보를 변경하시겠습니까?")) {
            CommonAjax.basic({
                url: "/settle/partnerPayment/setPartnerPaymentAccount.json?settleSrl="+settleSrl+"&partnerId="+ partnerId,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc : function (resData) {
                    if (resData > 0) {
                        alert("선택하신 건의 지급계좌정보가 변경되었습니다.");
                    } else {
                        alert("계좌변경이 실패하였습니다");
                    }
                    partnerPayment.search();
                }
            })
        }
    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return excelFileName;
    },
    searchFormReset: function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            partnerPayment.init();
        });
    },
}
