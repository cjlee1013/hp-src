var advanceListGrid = {
    gridView : new RealGridJS.GridView("advanceListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        advanceListGrid.initGrid();
        advanceListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        advanceListGrid.gridView.setDataSource(advanceListGrid.dataProvider);
        advanceListGrid.gridView.setStyles(advanceListBaseInfo.realgrid.styles);
        advanceListGrid.gridView.setDisplayOptions(advanceListBaseInfo.realgrid.displayOptions);
        advanceListGrid.gridView.setColumns(advanceListBaseInfo.realgrid.columns);
        advanceListGrid.gridView.setOptions(advanceListBaseInfo.realgrid.options);

        var mergeCells = [["basicDt", "paymentBasicDt", "payScheduleDt", "settleDt", "mallType", "purchaseOrderNo", "bundleNo", "itemNo", "gubun"]];
        SettleCommon.setRealGridSumHeader(advanceListGrid.gridView, mergeCells);
        advanceListGrid.gridView.setColumnProperty("선수금", "displayWidth", 700);
        advanceListGrid.gridView.setColumnProperty("선수수익", "displayWidth", 300);
    },
    initDataProvider : function() {
        advanceListGrid.dataProvider.setFields(advanceListBaseInfo.dataProvider.fields);
        advanceListGrid.dataProvider.setOptions(advanceListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        advanceListGrid.dataProvider.clearRows();
        advanceListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(advanceListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        advanceListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: advanceListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    fileName : ""
};
/***
 * 선수금조회 상세
 */
var advanceListForm = {
    init : function () {
        this.initSearchYM();
    },
    initSearchYM : function () {
        var date = new Date()
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        if (month < 10) {
            month = '0' + month;
        }

        $('#startMonth').val(month);
        $('#startYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(advanceListForm.search);
        $('#clearBtn').bindClick(advanceListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(advanceListGrid.excelDownload);
    },
    search : function () {
        var startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
        var endDt = moment(startDt, 'YYYY-MM-DD', true).endOf('month');
        startDt = moment(startDt, 'YYYY-MM-DD', true);

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var zeroType = $("#zeroType").val();
        var url;
        if (zeroType == "includeZero") {
            url = 'getList.json';
            searchForm = searchForm + "&includeZero=Y";
        } else if (zeroType == "OnlyZero") {
            url = 'getZeroList.json';
        } else {
            url = 'getList.json';
        }

        advanceListGrid.fileName = "선수금조회_";
        advanceListGrid.fileName += startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
        {
            url:'/settle/advance/'+ url + '?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                advanceListGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(advanceListGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                var stringCol = [ "completeAmt", "homeAmt", "sellerAmt", "profitAmt"
                    , "pgBalance","orderPrice", "pgAmt", "etcAmt", "receivedLast"
                    , "monthOrder", "monthPg","monthEtc","monthPgPay","decisionAmt"
                    ,"receivedLeft","dsSales","taxInvoice","revenueLeft"];
                advanceListGrid.setRealGridHeaderSum(advanceListGrid.gridView, stringCol);
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            advanceListForm.init();
        });
    }
};