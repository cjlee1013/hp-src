/**
 * 정산조정 그리드
 */
var adjustListGrid = {
    gridView : new RealGridJS.GridView("adjustListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        adjustListGrid.initGrid();
        adjustListGrid.initDataProvider();
        adjustListGrid.event();
    },
    initGrid : function() {
        adjustListGrid.gridView.setDataSource(adjustListGrid.dataProvider);

        adjustListGrid.gridView.setStyles(adjustListBaseInfo.realgrid.styles);
        adjustListGrid.gridView.setDisplayOptions(adjustListBaseInfo.realgrid.displayOptions);
        adjustListGrid.gridView.setColumns(adjustListBaseInfo.realgrid.columns);
        adjustListGrid.gridView.setOptions(adjustListBaseInfo.realgrid.options);

        //헤더 merge
        var mergeCells = [ ["adjustId", "partnerId", "vendorCd", "partnerNm", "itemNo", "itemNm", "adjustType", "gubun"]
            , ["comment", "separateYn", "regDt", "regId"] ];
        adjustListGrid.setRealGridSumHeader(adjustListGrid.gridView, mergeCells);
    },
    setRealGridSumHeader : function (_gridView, _mergeCells) {
        //합계를 사용하도록 그리드 설정
        _gridView.setOptions({ summaryMode : "statistical"});

        //상단 합계 헤더 스타일
        _gridView.setHeader({ summary: { visible: true, styles: { background: "#11ff0000", textAlignment: "far" }, mergeCells: _mergeCells  } });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계 ", styles: { textAlignment: "far" } } });
    },
    initDataProvider : function() {
        adjustListGrid.dataProvider.setFields(adjustListBaseInfo.dataProvider.fields);
        adjustListGrid.dataProvider.setOptions(adjustListBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData : function(dataList) {
        adjustListGrid.dataProvider.clearRows();
        adjustListGrid.dataProvider.setRows(dataList);

    },
    excelDownload: function() {
        if(adjustListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = adjustListForm.setExcelFileName("정산조정관리");
        adjustListGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            //showProgress: true,
            applyDynamicStyles : true,
            pagingAllItems : true,
            progressMessage: "엑셀 Export중입니다."
        });
    }
};
/**
 * 정산조정 엑셀 업로드 결과
 */
var adjustUploadGrid = {
    gridView : new RealGridJS.GridView("adjustUploadGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        adjustUploadGrid.initGrid();
        adjustUploadGrid.initDataProvider();
    },
    initGrid : function() {
        adjustUploadGrid.gridView.setDataSource(adjustUploadGrid.dataProvider);

        adjustUploadGrid.gridView.setStyles(adjustUploadBaseInfo.realgrid.styles);
        adjustUploadGrid.gridView.setDisplayOptions(adjustUploadBaseInfo.realgrid.displayOptions);
        adjustUploadGrid.gridView.setColumns(adjustUploadBaseInfo.realgrid.columns);
        adjustUploadGrid.gridView.setOptions(adjustUploadBaseInfo.realgrid.options);

        adjustUploadGrid.gridView.setDisplayOptions({
            heightMeasurer: "fixed",
            rowResizable: true,
            rowHeight: 30
        });
    },
    initDataProvider : function() {
        adjustUploadGrid.dataProvider.setFields(adjustUploadBaseInfo.dataProvider.fields);
        adjustUploadGrid.dataProvider.setOptions(adjustUploadBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        adjustUploadGrid.dataProvider.clearRows();
        adjustUploadGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(adjustUploadGrid.gridView.getItemCount() == 0) {
            return false;
        }

        var fileName = "정산조정_업로드결과";
        adjustUploadGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    }
};
/**
 * 정산조정 관리 폼
 */
var adjustListForm = {
    searchDate : null,
    eventFlag: true,
	init : function() {
        adjustListForm.event();
        adjustListForm.initAdjustListForm();

        $('#uploadFileNm').val('');
        $('#uploadFile').val('');
        $('#regAmt').attr('readonly', true).val('');
        $('#regFee').attr('readonly', true).val('');
        $('#regGubun').attr('readonly', true).attr('disabled', true).val('');
        $('#regItemNm').text('');
        $('#regPartnerId').text('');
        $('#regVendorCd').text('');
        $('#regPartnerNm').text('');
        $('#regTypeDesc').text('유형을 선택하세요.');
	},
    event : function() {
        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchValue').attr('readonly', true).val('');
                $('#searchValue').text("");
            } else {
                $('#searchValue').removeAttr('readonly');
                $('#searchValue').focus();
            }
        });

        $('#adjustType').bindChange(adjustListForm.setAdjustType);
        $('#regGubun').bindChange(adjustListForm.setAdjustTypeDesc);
        $('#excelUpload').bindClick(adjustListForm.openFile);
        $('#uploadFile').bindChange(adjustListForm.changeFile);
        $('#searchBtn').bindClick(adjustListForm.search);
        $('#clearBtn').bindClick(adjustListForm.initAdjustListForm);
        $('#excelDownload').bindClick(adjustListGrid.excelDownload);
        $('#excelSample').bindClick(adjustListForm.excelSample);
        $('#btnItemSearch').bindClick(adjustListForm.checkItem);
        $('#excelReset').bindClick(adjustListForm.resetExcelForm);
        $('#regClear').bindClick(adjustListForm.resetRegForm);
        $('#regSubmit').bindClick(adjustListForm.insertAdjust);
        $('#deleteBtn').bindClick(adjustListForm.deleteAdjust);
        $('#excelSubmit').bindClick(adjustListForm.upload);

        $('#regComment').bind('input', function () {
            if ($(this).val().length > 100) {
                alert("100자가 초과되었습니다. 확인 후 다시 진행해 주세요.");
                $(this).val($(this).val().substring(0, 100));
                $(this).focus();
                return;
            }
        });

        $('#regAmt').bind('input', function () {
            var value	= $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                $(this).focus();
                return;
            }
        });

        $('#regItemNo').bind('input', function () {
            var value	= $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                $(this).focus();
                return;
            }
        });

        $('#regFee').bind('input', function () {
            var value	= $(this).val();
            if (!$.isNumeric(value)) {
                alert("숫자만 입력 가능합니다.");
                $(this).val($(this).val().slice(0, -1));
                $(this).focus();
                return;
            }
        });

        if($("#separateYn").is(":checked")) {
            $("#separateYn").val("Y");
        } else {
            $("#separateYn").val("N");
        }
    },
    initAdjustListForm : function() {
        $("#adjustSearchForm").each(function () {
            this.reset();
        });
        adjustListGrid.init();
        //조회 일자 초기화
        adjustListForm.initSearchDate("0");
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,VENDOR_CD,PARTNER_NM,ITEM_NO,ITEM_NM");
    },
    resetRegForm : function() {
        $("#adjustRegForm").each(function () {
            this.reset();
        });

        $('#regAmt').attr('readonly', true).val('');
        $('#regFee').attr('readonly', true).val('');
        $('#regGubun').attr('readonly', true).attr('disabled', true).val('');
        $('#regItemNm').text('');
        $('#regPartnerId').text('');
        $('#regVendorCd').text('');
        $('#regPartnerNm').text('');
        $('#regTypeDesc').text('유형을 선택하세요.');
    },
    //조회 일자 초기화
    initSearchDate : function (flag) {
        adjustListForm.searchDate = Calendar.datePickerRange("startDt_D", "endDt_D");
        adjustListForm.searchDate.setEndDate("0");
        adjustListForm.searchDate.setStartDate(flag);
    },
    setAdjustType : function () {
        var adjustType = $('#adjustType').val();
        switch (adjustType) {
            case "1":
                $('#regFee').attr('readonly', true).val('');
                $('#regAmt').removeAttr('readonly');
                $('#regGubun').attr('readonly', true).attr('disabled', true).val('1');
                $('#regTypeDesc').text('자사부담 업체 보상');
                $("#regAmt").focus();
                break;
            case "2":
                $('#regFee').attr('readonly', true).val('');
                $('#regAmt').removeAttr('readonly');
                $('#regGubun').attr('readonly', true).attr('disabled', true).val('2');
                $("#regAmt").focus();
                $('#regTypeDesc').text('고객 보상금에 대한 업체 청구');
                break;
            case "3":
                $('#regAmt').attr('readonly', true).val('');
                $('#regFee').removeAttr('readonly');
                $('#regGubun').attr('readonly', true).attr('disabled', true).val('1');
                $("#regFee").focus();
                $('#regTypeDesc').text('광고 진행으로 인한 광고비 부과');
                break;
            case "4":
                $('#regAmt').attr('readonly', true).val('');
                $('#regFee').removeAttr('readonly');
                $('#regGubun').removeAttr('disabled').removeAttr('readonly').val('1');
                $("#regFee").focus();
                $('#regTypeDesc').text('판매수수료 미달 집계로 판매수수료 부과');
                break;
            case "":
                $('#regAmt').attr('readonly', true).val('');
                $('#regFee').attr('readonly', true).val('');
                $('#regGubun').attr('readonly', true).attr('disabled', true).val('');
                $('#regTypeDesc').text('유형을 선택하세요.');
                break;
        }
    },
    setAdjustTypeDesc : function () {
        var regGubun = $('#regGubun').val();
        if ($('#adjustType').val() == "4") {
            switch (regGubun) {
                case "1":
                    $('#regTypeDesc').text('판매수수료 미달 집계로 판매수수료 부과');
                    break;
                case "2":
                    $('#regTypeDesc').text('판매수수료 초과 집계로 판매수수료 환급');
                    break;
                case "":
                    $('#regTypeDesc').text('유형을 선택하세요.');
                    break;
            }
        }
    },
    // 내용 검증
    validateCheck : function () {
        var startDt = $("#startDt_D").val();
        var endDt = $("#endDt_D").val();

        if(!moment(startDt,'YYYY-MM-DD',true).isValid() || !moment(endDt,'YYYY-MM-DD',true).isValid()) {
            alert("조회기간 날짜 형식이 맞지 않습니다");
            $("#setOneWeekBtn").trigger("click");
            return false;
        }

        // 1년 단위
        if (moment(endDt).diff(startDt, "month", true) > 3) {
            alert("3개월 이내만 검색 가능합니다.");
            $("#startDt_D").val(moment(endDt).add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        if(moment(endDt).diff(startDt, "day", true) < 0) {
            alert("종료일은 시작일보다 빠를 수 없습니다.");
            $("#startDt_D").val(endDt);
            return false;
        }

        if($("#searchType").val()=="ITEM_NO" && !$.jUtil.isAllowInput($('#searchValue').val(), ['NUM'])){
            alert("상품번호는 숫자만 입력 가능합니다");
            $("#searchValue").focus();
            return false;
        } if($("#searchType").val()=="VENDOR_CD" && !$.jUtil.isAllowInput($('#searchValue').val(), ['NUM'])){
            alert("업체코드는 숫자만 입력 가능합니다");
            $("#searchValue").focus();
            return false;
        } else if($("#searchType").val()=="PARTNER_ID" ){
            var partnerIdPattern = /^[A-Za-z0-9_]*$/;
            if(!partnerIdPattern.test($('#searchValue').val())){
                alert('판매업체ID는 영문, 숫자, 언더바만 가능합니다');
                $("#searchValue").focus();
                return false;
            }
        }
         return true;
    },
    excelSample : function() {
        var fileName = "정산조정_업로드양식";

        SettleCommon.setRealGridColumnVisible(adjustUploadGrid.gridView, [SettleCommon.getRealGridColumnName(adjustUploadGrid.gridView, "업로드결과")], false);

        var _dataProvider = adjustUploadGrid.dataProvider.getRows(0, -1);
        adjustUploadGrid.dataProvider.clearRows();

        var values = ["", "숫자로 입력 해주세요", "숫자로 입력 해주세요","유형코드로 입력 해주세요.\n1 = 업체 보상비\n2 = 업체 패널티\n3 = 광고수수료\n4 = 판매수수료 (부과)\n5 = 판매수수료 (환급)",
            "", "숫자로만 입력해주세요","숫자로만 입력해주세요","100자까지 입력 가능 합니다.","Y : 정산조건 건만 별도 집계\nN : 상품매출 정산에 합산 집계", "", ""];
        adjustUploadGrid.dataProvider.insertRow(0, values);

        adjustUploadGrid.gridView.setDisplayOptions({
            heightMeasurer: "fixed",
            rowResizable: true,
            rowHeight: 120
        });

        adjustUploadGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            allColumns : false,
            applyFitStyle : true,
            progressMessage: "엑셀 Export중입니다."
        });
        adjustUploadGrid.dataProvider.setRows(_dataProvider);
        SettleCommon.setRealGridColumnVisible(adjustUploadGrid.gridView, [SettleCommon.getRealGridColumnName(adjustUploadGrid.gridView, "업로드결과")], true);
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        excelFileName = fileName +'_'+ $("#startDt_D").val().replace(/-/gi, "")+'_'+$("#endDt_D").val().replace(/-/gi, "");

        return excelFileName;
    },
    search : function () {
        if(!adjustListForm.validateCheck()) return ;
        CommonAjaxBlockUI.startBlockUI();
        var searchFormData = adjustListForm.getFormData();

        CommonAjax.basic({
            url: '/settle/settleAdjust/getAdjustList.json?'+ encodeURI(searchFormData),
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                adjustListGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(adjustListGrid.gridView.getItemCount()));
                SettleCommon.setRealGridHeaderSum(adjustListGrid.gridView, []);

                //건수가 0건이 아닌 경우에만 수행
                if(adjustListGrid.gridView.getItemCount() > 0) {
                    //체크박스 선택조건 설정.
                    adjustListGrid.gridView.setCheckableExpression("( (values['useYn'] = 'Y') )", true);
                }
                CommonAjaxBlockUI.stopBlockUI();

            }
        });
    },
    checkItem : function () {
        var itemNo = $("#regItemNo").val();
        if ((itemNo == ""  || !$.isNumeric(itemNo))){
            alert("상품번호 입력값을 확인해주세요.");
            $("#regItemNo").focus();
            return false;
        }

        CommonAjaxBlockUI.startBlockUI();
        CommonAjax.basic({
            url: '/settle/settleAdjust/getAdjustItem.json?itemNo='+ itemNo,
            data: null,
            method: "GET",
            successMsg: null,
            callbackFunc: function (res) {
                if (res.data == null || res.data == undefined) {
                    alert(res.returnMessage);
                    $("#regItemNo").focus();
                } else {
                    $('#regItemNm').text(res.data.itemNm);
                    $('#regPartnerId').text(res.data.partnerId);
                    $('#regPartnerNm').text(res.data.partnerNm);
                    $('#regVendorCd').text(res.data.vendorCd);
                }
                CommonAjaxBlockUI.stopBlockUI();
            }
        });
    },
    getFormData : function() {
        var searchFormData = $("form[name=adjustSearchForm]").serialize();

        if ($('#searchType option:selected').text() == '상품번호') {
            searchFormData += '&itemNo=' + $("#searchValue").val();
        } else if ($('#searchType option:selected').text() == '판매업체ID') {
            searchFormData += '&partnerId=' + $("#searchValue").val();
        } else if ($('#searchType option:selected').text() == '상품명') {
            searchFormData += '&itemNm=' + $("#searchValue").val();
        } else if ($('#searchType option:selected').text() == '판매업체명') {
            searchFormData += '&partnerNm=' + $("#searchValue").val();
        } else if ($('#searchType option:selected').text() == '업체코드') {
            searchFormData += '&vendorCd=' + $("#searchValue").val();
        }

        searchFormData += '&startDt=' + $("#startDt_D").val();
        searchFormData += '&endDt=' + $("#endDt_D").val();
        if ($("#schAdjustType").val() != "") {
            searchFormData += '&adjustType=' + $("#schAdjustType").val();
        }

        return searchFormData;
    },
    resetExcelForm : function() {
        $("#excelUploadForm").each(function () {
            this.reset();
        });
        $('#uploadFileNm').val('');
        $('#uploadFile').val('');
    },
    // 파일 찾기 버튼 선택
    openFile : function() {
        $('#uploadFile').trigger('click');
    },
    changeFile : function() {
        var fileInfo = $('#uploadFile')[0].files[0];
        if (!fileInfo) {
            alert('업로드 대상 파일을 확인해주세요.');
            return false;
        }

        var fileExt = fileInfo.name;
        if (fileExt.indexOf(".xlsx") < 0 && fileExt.indexOf(".xls") < 0) {
            alert("상품 일괄등록은 지정된 엑셀 양식(.xlsx .xls)으로 업로드해주세요.");
            $('#uploadFile').val('');
            return false;
        }
        $("#uploadFileNm").val($('#uploadFile')[0].files[0].name);
        return true;
    },
    upload : function() {
        if (!adjustListForm.changeFile()) return false;

        if (!confirm("엑셀 업로드 하시겠습니까?\n업로드 결과가 다운로드 됩니다.")) return false;
        CommonAjaxBlockUI.startBlockUI();
        $('form[name="excelUploadForm"]').ajaxForm({
            url: '/settle/settleAdjust/uploadExcel.json',
            enctype: 'multipart/form-data',
            contentType: false,
            method: "POST",
            successMsg: null,
            success: function(res) {
                alert('처리되었습니다.');
                adjustUploadGrid.initGrid();
                adjustUploadGrid.setData(res);
                adjustUploadGrid.excelDownload();
                CommonAjaxBlockUI.stopBlockUI();
                adjustListForm.resetExcelForm();
                adjustListForm.search();
            },
            error: function() {
                alert('오류가 발생하였습니다.');
                CommonAjaxBlockUI.stopBlockUI();
            }
        }).submit();
    },
    //임의조정 입력
    insertAdjust:function () {
        var itemNo = $("#regItemNo").val();
        if ((itemNo == ""  || !$.isNumeric(itemNo))){
            alert("상품번호 입력값을 확인해주세요.");
            return false;
        }

        var adjustType = $("#adjustType").val();
        if (adjustType == "") {
            alert("유형 입력값을 확인해주세요.");
            return false;
        }
        var adjustAmt = $("#regAmt").val();
        if (($('#adjustType').val() != '3' && $('#adjustType').val() != '4') && (adjustAmt == "" || !$.isNumeric(adjustAmt))) {
            alert("정산조정 금액 입력값을 확인해주세요.");
            $('#regAmt').focus();
            return false;
        }
        var adjustFee = $("#regFee").val(); //계산서 대상만
        if (($('#adjustType').val() == '3' || $('#adjustType').val() == '4') && (adjustFee == "" || !$.isNumeric(adjustFee))) {
            alert("정산조정 수수료 입력값을 확인해주세요.");
            $('#regFee').focus();
            return false;
        }
        if ($('#adjustType').val() == '4') { //판매 수수료
            if ($('#regGubun').val() == "") {
                alert("정산조정 가산/차감 여부를 선택해주세요.");
                $('#regGubun').focus();
                return false;
            }

            if ($('#regGubun').val() == "2") {
                adjustType = "5"; //차감으로 변경
            }
        }
        var separateYn = $("#separateYn").is(":checked")? "Y" : "N";
        var comment = $("#regComment").val();
        var searchFormData = "itemNo="+ itemNo + "&adjustType="+ adjustType +"&adjustAmt="+ adjustAmt
                             +"&adjustFee="+ adjustFee +"&comment="+ comment +"&separateYn="+ separateYn;

        if (!confirm("정산조정을 등록하시겠습니까?")) return false;
        CommonAjaxBlockUI.startBlockUI();
        if(adjustListForm.eventFlag) {
            adjustListForm.eventFlag = false;
            CommonAjax.basic({
                url: "/settle/settleAdjust/insertAdjust.json?" + searchFormData,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    if (res.returnCode == 0) {
                        alert('등록되었습니다.');
                        adjustListForm.resetRegForm();
                        adjustListForm.search();
                    } else {
                        alert(res.returnMessage);
                    }
                    adjustListForm.eventFlag = true;
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        } else {
            alert('처리 중입니다.');
        }
    },
    //임의조정 입력
    deleteAdjust: function () {
        var checkData = "";
        $.each(adjustListGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
            var adjustId = adjustListGrid.dataProvider.getJsonRow(_dataRowId).adjustId;
            checkData += (adjustId + ",");
        });
        checkData = (checkData.slice(0, -1)).split(',');

        if(checkData == "" || checkData == ",") {
            alert("삭제할 조정ID를 선택해주세요.");
            return false;
        }

        var loopCnt = checkData.length;
        if (!confirm("선택하신 건을 목록에서 삭제하시겠습니까?")) return false;
        CommonAjaxBlockUI.startBlockUI();
        for (var i = 0; i < loopCnt; i++) {
            var adjustId = checkData[i];

            CommonAjax.basic({
                url: "/settle/settleAdjust/deleteAdjust.json?adjustId="+ adjustId,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
        }
        alert("삭제되었습니다.");
        adjustListForm.search();
    },
};
