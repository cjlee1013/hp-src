/***
 * AP전표조회 그리드 설정
 */
var apInvoiceListGrid = {
    gridView : new RealGridJS.GridView("apInvoiceListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        apInvoiceListGrid.initGrid();
        apInvoiceListGrid.initDataProvider();
        apInvoiceListGrid.event();
    },
    initGrid : function () {
        apInvoiceListGrid.gridView.setDataSource(apInvoiceListGrid.dataProvider);
        apInvoiceListGrid.gridView.setStyles(apInvoiceListGridBaseInfo.realgrid.styles);
        apInvoiceListGrid.gridView.setDisplayOptions(apInvoiceListGridBaseInfo.realgrid.displayOptions);
        apInvoiceListGrid.gridView.setOptions(apInvoiceListGridBaseInfo.realgrid.options);
        apInvoiceListGrid.gridView.setColumns(apInvoiceListGridBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "slipName", "invoiceTypeLookupCode", "invoiceNum", "partnerId", "vendorCd", "partnerNm", "businessNm"],
                        ["invoiceDate", "creationDate", "ofInvoiceDate", "termsName", "description", "statusNm", "chgDt", "chgId"] ];
        SettleCommon.setRealGridSumHeader(apInvoiceListGrid.gridView, mergeCells);

        apInvoiceListGrid.gridView.setOptions({ display: { fitStyle: "evenFill" } });
    },
    initDataProvider : function() {
        apInvoiceListGrid.dataProvider.setFields(apInvoiceListGridBaseInfo.dataProvider.fields);
        apInvoiceListGrid.dataProvider.setOptions(apInvoiceListGridBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        apInvoiceListGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            apInvoiceListGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        apInvoiceListGrid.dataProvider.clearRows();
        apInvoiceListGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = apInvoiceListGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/accounting/getAPBalanceSheet.json?startDt='+ rowDataJson.basicDt.replace(/-/gi, "")
            +"&invoiceNum="+ rowDataJson.invoiceNum +"&rfInvoiceId="+ rowDataJson.rfInvoiceId;

        CommonAjax.basic(
            {
                url: _url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    apBalanceSheetGrid.setData(res);
                    apBalanceSheetGrid.gridView.setColumnProperty("invoiceAmount", "header", { summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, expression: "sum"} });

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    excelDownload: function() {
        if(apInvoiceListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = apInvoiceListForm.setExcelFileName("AP전표조회");
        apInvoiceListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * AP전표조회 상세 그리드
 */
var apBalanceSheetGrid = {
    gridView : new RealGridJS.GridView("apBalanceSheetGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        apBalanceSheetGrid.initGrid();
        apBalanceSheetGrid.initDataProvider();
    },
    initGrid : function () {
        apBalanceSheetGrid.gridView.setDataSource(apBalanceSheetGrid.dataProvider);
        apBalanceSheetGrid.gridView.setStyles(apBalanceSheetGridBaseInfo.realgrid.styles);
        apBalanceSheetGrid.gridView.setDisplayOptions(apBalanceSheetGridBaseInfo.realgrid.displayOptions);
        apBalanceSheetGrid.gridView.setOptions(apBalanceSheetGridBaseInfo.realgrid.options);
        apBalanceSheetGrid.gridView.setColumns(apBalanceSheetGridBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "slipName", "invoiceNum", "lineNumber", "lineTypeLookupCode", "taxCode"],
                        ["description", "globalAttribute15", "costCenterName", "globalAttribute14", "accountName"] ];
        SettleCommon.setRealGridSumHeader(apBalanceSheetGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        apBalanceSheetGrid.dataProvider.setFields(apBalanceSheetGridBaseInfo.dataProvider.fields);
        apBalanceSheetGrid.dataProvider.setOptions(apBalanceSheetGridBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        apBalanceSheetGrid.dataProvider.clearRows();
        apBalanceSheetGrid.dataProvider.setRows(dataList);
    },
    ExcelDownloadAll: function() {
        CommonAjaxBlockUI.startBlockUI();
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        var _url = '/settle/accounting/getAPBalanceSheet.json?startDt='+ $("#schStartDt").val().replace(/-/gi, "")
            +"&endDt="+ endDt +"&slipType="+ $("#slipType option:selected").val();

        CommonAjax.basic({
            url: _url,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                apBalanceSheetGrid.setData(res);
                apBalanceSheetGrid.gridView.setColumnProperty("invoiceAmount", "header", { summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, expression: "sum"} });

                var fileName = 'AP전표조회_'+ $("#slipType option:selected").text() +'_전체상세내역_'
                    + $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
                apBalanceSheetGrid.gridView.exportGrid( {
                    type: "excel",
                    target: "local",
                    fileName: fileName+".xlsx",
                    showProgress: true,
                    progressMessage: " 엑셀 데이터 추줄 중입니다."
                });
            }
        });
        CommonAjaxBlockUI.stopBlockUI();
    },
    excelDownload: function() {
        if(apBalanceSheetGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = 'AP전표조회_'+ $("#slipType option:selected").text() +'_상세내역_'
            + $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        apBalanceSheetGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
};
/***
 * AP전표조회 폼
 */
var apInvoiceListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSlipType();
    },
    initSearchDate : function (flag) {
        apInvoiceListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        apInvoiceListForm.setDate.setEndDate(0);
        apInvoiceListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    initSlipType : function () {
        CommonAjaxBlockUI.startBlockUI();
        var $slipType = $("select[name='slipType']");
        $slipType.empty();

        var selectBoxStr = '';
        CommonAjax.basic({
            url: "/settle/accounting/getAPSlipTypeList.json",
            method : "GET",
            callbackFunc: function (res) {
                $.each(res, function (key, resList) {
                    selectBoxStr += '<option value="'+ resList.slipType +'" >'+ resList.typeName +'</option>';
                });
                $('#slipType').html(selectBoxStr);

            }, error: function(xhr) {
                alert('잠시 뒤에 시도해주세요.');
                return;
            }
        });
        CommonAjaxBlockUI.stopBlockUI();
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(apInvoiceListForm.search);
        $('#searchResetBtn').bindClick(apInvoiceListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(apInvoiceListGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(apBalanceSheetGrid.excelDownload);
        $('#excelDownloadAllBtn').bindClick(apBalanceSheetGrid.ExcelDownloadAll);
        $('#confirmBtn').bindClick(apInvoiceListForm.confirm);
    },
    valid : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }
        return true;
    },
    search : function () {
        if(!apInvoiceListForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = $("form[name=searchForm]").serialize();
        var startDt = $("#schStartDt").val().split('-').join('');
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        CommonAjax.basic(
            {
                url: '/settle/accounting/getAPInvoiceList.json?' + searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    apInvoiceListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(apInvoiceListGrid.gridView.getItemCount()));

                    apInvoiceListGrid.gridView.setColumnProperty("invoiceAmount", "header", { summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, expression: "sum"} });

                    //건수가 0건이 아닌 경우에만 수행
                    if(apInvoiceListGrid.gridView.getItemCount() > 0) {
                        //체크박스 선택조건 설정.
                        apInvoiceListGrid.gridView.setCheckableExpression("( (values['status'] = 'N') )", true);
                    }
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    confirm : function () {
        var mileageReqDetailList = [];
        var returnData;
        $.each(apInvoiceListGrid.gridView.getCheckedRows(true), function (_idx, _dataRowId) {
            var invoiceNum = apInvoiceListGrid.dataProvider.getJsonRow(_dataRowId).invoiceNum;
            var rfInvoiceId = apInvoiceListGrid.dataProvider.getJsonRow(_dataRowId).rfInvoiceId;
            var mngEmail = apInvoiceListGrid.dataProvider.getJsonRow(_dataRowId).mngEmail;

            returnData = {
                "invoiceNum": invoiceNum,
                "rfInvoiceId": rfInvoiceId,
                "mngEmail": mngEmail,
                "regId": ""
            };

            mileageReqDetailList.push(returnData);
        });
        console.log('data='+ JSON.stringify(mileageReqDetailList));


        if(mileageReqDetailList.length == 0) {
            alert("확정할 전표를 선택해주세요.");
            return false;
        }
        var returnMessage = '';
        if(confirm("선택하신 건을 확정하시겠습니까? 확정 후에는 수정이 불가능합니다.")) {
            CommonAjax.basic({
                url: "/settle/accounting/setAPInvoiceConfirm.json",
                contentType: 'application/json',
                data: JSON.stringify(mileageReqDetailList),
                method: "POST",
                successMsg: null,
                callbackFunc: function (res) {
                    returnMessage = res.returnMessage;
                    if (res.returnCode == 0) {
                        alert(returnMessage);
                        apInvoiceListForm.search();
                    }
                }
            });
        }
    },
    setExcelFileName : function (fileName) {
        fileName = fileName +'_'+ $("#slipType option:selected").text() +'_'
            + $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        return fileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            apInvoiceListForm.init();
            apInvoiceListGrid.init();
            apBalanceSheetGrid.init();
        });
    }
};