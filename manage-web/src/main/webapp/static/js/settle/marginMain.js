/** Main Script */
var marginMain = {
    /**
     * 전역변수
     */
    global: {
        currentStartDate : ""
        , currentEndDate : ""
        , mallType : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        marginMain.bindingEvent();
        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "0");
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // [검색] 버튼
        $("#schBtn").bindClick(marginMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(marginMain.reset, "marginSearchForm");
        // [엑셀다운 팀별] 버튼
        $("#excelDownloadTeamBtn").bindClick(marginMain.excelDownloadTeam);
        // [엑셀다운 파트별] 버튼
        $("#excelDownloadPartBtn").bindClick(marginMain.excelDownloadPart);
        // [엑셀다운 유형별] 버튼
        $("#excelDownloadHeadBtn").bindClick(marginMain.excelDownloadHead);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#marginSearchForm").serializeObject();

        CommonAjax.basic({
            url:'/settle/margin/getMarginList.json',
            data: JSON.stringify(reqData),
            contentType: 'application/json',
            method: "POST",
            successMsg: null,
            callbackFunc: function(res) {
                marginMain.global.currentStartDate = $('#schStartDt').val().replace(/-/gi, "");
                marginMain.global.currentEndDate = $('#schEndDt').val().replace(/-/gi, "");
                marginMain.global.mallType = $('#mallType').val();

                marginTeamGrid.setData(res.teamList);
                marginPartGrid.setData(res.partList);
                marginHeadGrid.setData(res.headList);

                //그룹헤더컬럼 합계
                var stringCol = ["completeInAmt","completeExAmt","marginAmt"];

                marginMain.setRealGridHeaderSum(marginTeamGrid.gridView, stringCol, res.teamList);
                marginMain.setRealGridHeaderSum(marginPartGrid.gridView, stringCol, res.partList);
                marginMain.setRealGridHeaderSum(marginHeadGrid.gridView, stringCol, res.headList);
            }
        });
    },

    /**
     * 팀별 엑셀 다운로드
     */
    excelDownloadTeam: function() {
        if (marginTeamGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  marginMain.global.mallType +  " 마진조회_팀별합계_" + marginMain.global.currentStartDate + "_" + marginMain.global.currentEndDate;
        marginTeamGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 파트별 엑셀 다운로드
     */
    excelDownloadPart: function() {
        if (marginPartGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  marginMain.global.mallType + " 마진조회_파트별합계_" + marginMain.global.currentStartDate + "_" + marginMain.global.currentEndDate;
        marginPartGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 유형별 엑셀 다운로드
     */
    excelDownloadHead: function() {
        if (marginPartGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  marginMain.global.mallType + " 마진조회_유형별합계_" + marginMain.global.currentStartDate + "_" + marginMain.global.currentEndDate;
        marginHeadGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        shippingUtil.initSearchDateCustom("schStartDt", "schEndDt", "-1d", "0");
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns, data) {
        var resLen = data.length;
        var completeInAmtSum = 0;
        var completeExAmtSum = 0;
        var marginAmtSum = 0;
        var completeInAmtRateSum = 0;
        var completeExAmtRateSum = 0;
        var marginAmtRateSum = 0;

        for (var i=0; i<resLen; i++) {
            if(data[i].name.indexOf(" - ") < 0) {
                completeInAmtRateSum += parseFloat(data[i].completeInAmtRate.replace("%",""));
                completeExAmtRateSum += parseFloat(data[i].completeExAmtRate.replace("%",""));
                completeInAmtSum += Number(data[i].completeInAmt.replace(/,/g, ""));
                completeExAmtSum += Number(data[i].completeExAmt.replace(/,/g, ""));
                marginAmtSum += Number(data[i].marginAmt.replace(/,/g, ""));
            }
        }

        if (marginAmtSum != 0 && completeExAmtSum != 0) {
            marginAmtRateSum = (marginAmtSum / completeExAmtSum * 100).toFixed(2);
            if (isNaN(marginAmtRateSum)) {
                marginAmtRateSum = 0;
            }
        }

        completeInAmtSum = $.jUtil.comma(completeInAmtSum.toString());
        completeExAmtSum = $.jUtil.comma(completeExAmtSum.toString());
        marginAmtSum = $.jUtil.comma(marginAmtSum.toString());

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });

        _gridView.setColumnProperty("completeInAmt", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:completeInAmtSum} });

        _gridView.setColumnProperty("completeInAmtRate", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:Math.round(completeInAmtRateSum)+"%" } });

        _gridView.setColumnProperty("completeExAmt", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:completeExAmtSum} });

        _gridView.setColumnProperty("completeExAmtRate", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:Math.round(completeExAmtRateSum)+"%" } });

        _gridView.setColumnProperty("marginAmt", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:marginAmtSum} });

        _gridView.setColumnProperty("marginAmtRate", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:marginAmtRateSum+"%" } });
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        return true;
    }
}

/** Grid Script */
var marginTeamGrid = {
    gridView: new RealGridJS.GridView("marginTeamGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marginTeamGrid.initGrid();
        marginTeamGrid.initDataProvider();
    },
    initGrid: function () {
        marginTeamGrid.gridView.setDataSource(marginTeamGrid.dataProvider);
        marginTeamGrid.gridView.setStyles(marginTeamGridBaseInfo.realgrid.styles);
        marginTeamGrid.gridView.setDisplayOptions(marginTeamGridBaseInfo.realgrid.displayOptions);
        marginTeamGrid.gridView.setColumns(marginTeamGridBaseInfo.realgrid.columns);
        marginTeamGrid.gridView.setOptions(marginTeamGridBaseInfo.realgrid.options);

        var stringCol = ["name"];
         SettleCommon.setRealGridSumHeader(marginTeamGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        marginTeamGrid.dataProvider.setFields(marginTeamGridBaseInfo.dataProvider.fields);
        marginTeamGrid.dataProvider.setOptions(marginTeamGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        marginTeamGrid.dataProvider.clearRows();
        marginTeamGrid.dataProvider.setRows(dataList);
    }
};

var marginPartGrid = {
    gridView: new RealGridJS.GridView("marginPartGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marginPartGrid.initGrid();
        marginPartGrid.initDataProvider();
    },
    initGrid: function () {
        marginPartGrid.gridView.setDataSource(marginPartGrid.dataProvider);
        marginPartGrid.gridView.setStyles(marginPartGridBaseInfo.realgrid.styles);
        marginPartGrid.gridView.setDisplayOptions(marginPartGridBaseInfo.realgrid.displayOptions);
        marginPartGrid.gridView.setColumns(marginPartGridBaseInfo.realgrid.columns);
        marginPartGrid.gridView.setOptions(marginPartGridBaseInfo.realgrid.options);

        var stringCol = ["name"];
         SettleCommon.setRealGridSumHeader(marginPartGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        marginPartGrid.dataProvider.setFields(marginPartGridBaseInfo.dataProvider.fields);
        marginPartGrid.dataProvider.setOptions(marginPartGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        marginPartGrid.dataProvider.clearRows();
        marginPartGrid.dataProvider.setRows(dataList);
    }
};

var marginHeadGrid = {
    gridView: new RealGridJS.GridView("marginHeadGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        marginHeadGrid.initGrid();
        marginHeadGrid.initDataProvider();
    },
    initGrid: function () {
        marginHeadGrid.gridView.setDataSource(marginHeadGrid.dataProvider);
        marginHeadGrid.gridView.setStyles(marginHeadGridBaseInfo.realgrid.styles);
        marginHeadGrid.gridView.setDisplayOptions(marginHeadGridBaseInfo.realgrid.displayOptions);
        marginHeadGrid.gridView.setColumns(marginHeadGridBaseInfo.realgrid.columns);
        marginHeadGrid.gridView.setOptions(marginHeadGridBaseInfo.realgrid.options);

        var stringCol = ["name"];
        SettleCommon.setRealGridSumHeader(marginHeadGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        marginHeadGrid.dataProvider.setFields(marginHeadGridBaseInfo.dataProvider.fields);
        marginHeadGrid.dataProvider.setOptions(marginHeadGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        marginHeadGrid.dataProvider.clearRows();
        marginHeadGrid.dataProvider.setRows(dataList);
    }
};