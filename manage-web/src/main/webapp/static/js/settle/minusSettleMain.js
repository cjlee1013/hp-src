/** Main Script */
var minusSettleMain = {
    /**
     * 전역변수
     */
    global: {
        startDt : ""
        , endDt : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        minusSettleMain.bindingEvent();
        minusSettleMain.initSearchDateCustom("0");
        minusSettleMain.initSearchYM();
    },

    /**
     * init Date 이벤트
     */
    initSearchDateCustom : function(flag) {
        var setDateVal = Calendar.datePickerRange("startDt", "endDt");
        setDateVal.setStartDate(flag);
        setDateVal.setEndDate(0);
    },

    /**
     * init Date 이벤트
     */
    initSearchYM : function() {
        var year = moment().format("YYYY");
        var month = moment().format("MM");

        $('#startMonth, #endMonth').val(month);
        $('#startYear, #endYear').val(year);
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 검색조건 select box
        $("#schKeywordType").on('change', function() { minusSettleMain.setChangeKeywordType() });

        // [검색] 버튼
        $("#schBtn").bindClick(minusSettleMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(minusSettleMain.reset, "minusSettleSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(minusSettleMain.excelDownload);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        //주의! 버튼 생성시 동일이름으로 버튼 생성시에 주의 요망.
        //만들때마다 초기화되므로 위에서 생성한 버튼아이디값이 초기화 될 수 있음.
        var buttonInfo = {
            buttonCnt : 1,
            buttonName : "settleFlagComplete",
            buttonImg : [ {name : "settleFlagComplete", img : "/static/images/settle/btn_settleFlagComplete.png", width:70} ],
            buttonGap : 0,
            buttonMargin : 0,
            buttonCombine : ["settleFlagComplete"],
            value : [""],
            isValueNotOption : true
        };

        var reqData = $("#minusSettleSearchForm").serialize();

        var paramStartDt = "";
        var paramEndDt = "";
        var dateType = $("input:radio[name='dateType']:checked").val();
        var inpStartDt = $("#startDt").val();
        var inpEndDt = $("#endDt").val();
        var inpStartYear = $("#startYear").val();
        var inpEndYear = $("#endYear").val();
        var inpStartMonth = $("#startMonth").val();
        var inpEndMonth = $("#endMonth").val();

        // 일별, 월별 날짜 설정
        if (dateType =='D') {
            paramStartDt = inpStartDt.replace(/-/gi, "");
            paramEndDt = moment(inpEndDt, 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
        } else {
            paramStartDt = inpStartYear + inpStartMonth + '01';
            paramEndDt = moment(inpEndYear +'-'+ inpEndMonth + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }

        reqData += '&schStartDt=' + paramStartDt + '&schEndDt=' + paramEndDt;

        CommonAjax.basic({
            url         : '/settle/minusSettle/getMinusSettleList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                minusSettleGrid.setData(res);
                $('#minusSettleSearchCnt').html($.jUtil.comma(minusSettleGrid.gridView.getItemCount()));

                minusSettleMain.global.startDt = dateType == 'D' ? paramStartDt : paramStartDt;
                minusSettleMain.global.endDt = dateType == 'D' ?
                    moment(inpEndDt, 'YYYY-MM-DD', true).format("YYYYMMDD") : moment(paramEndDt, 'YYYYMMDD', true).add(-1, "day").format("YYYYMMDD");

                //버튼생성
                var targetColumn = SettleCommon.getRealGridColumnName(minusSettleGrid.gridView, "수기마감");
                $.each(res, function(_rowId, _data){
                    minusSettleGrid.dataProvider.setValue(_rowId, targetColumn, "settleFlagComplete");
                });

                if(minusSettleGrid.gridView.getItemCount() > 0) {
                    SettleCommon.setRealGridCellButton(minusSettleGrid.gridView, buttonInfo, targetColumn);
                }

                //그룹헤더컬럼 합계
                SettleCommon.setRealGridHeaderSum(minusSettleGrid.gridView, []);
            }
        });
    },

    /**
     * 검색 조건 select box 선택 이벤트
     */
    setChangeKeywordType: function() {
        var $schKeywordType = $('#schKeywordType');
        var $schKeyword = $('#schKeyword');

        if($.jUtil.isEmpty($schKeywordType.val())) {
            $schKeyword.val("");
            $schKeyword.prop("disabled",true);
            $schKeyword.prop("placeholder","");
        } else {
            $schKeyword.prop("disabled",false);
            $schKeyword.prop("placeholder","검색어를 입력하세요");
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (minusSettleGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  "DS역정산관리_" + minusSettleMain.global.startDt + "_" + minusSettleMain.global.endDt;
        minusSettleGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        minusSettleMain.setChangeKeywordType();
        minusSettleMain.initSearchDateCustom("0");
        minusSettleMain.initSearchYM();
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var dateType = $("input:radio[name='dateType']:checked").val();

        if (dateType =='D') {
            // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
            var $schStartDt = $("#startDt");
            var $schEndDt = $("#endDt");
            var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
            var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

            if(!schStartDt.isValid() || !schEndDt.isValid()) {
                alert("검색기간을 입력해주세요.");
                return false;
            }

            if(schEndDt.diff(schStartDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                minusSettleMain.initSearchDateCustom("0");
                return false;
            }

            if (schEndDt.diff(schStartDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월까지 설정 가능합니다.");
                $schStartDt.val(schEndDt.add(-3, "month").format("YYYY-MM-DD"));
                return false;
            }

        } else { //M
            var $startYear = $("#startYear");
            var $startMonth = $("#startMonth");
            var $endYear = $("#endYear");
            var $endMonth = $("#endMonth");

            var startDt = $startYear.val() + '-' + $startMonth.val() + '-01';
            var endDt = $endYear.val() +'-'+ $endMonth.val() + '-01';

            startDt = moment(startDt, 'YYYY-MM-DD', true);
            endDt = moment(endDt, 'YYYY-MM-DD', true).add(1, "day");

            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $startYear.val($endYear.val());
                $startMonth.val($endMonth.val());
                $endYear.val($startYear.val());
                $endMonth.val($startMonth.val());
                return false;
            }

            if (endDt.diff(startDt, "month", true) > 3) {
                alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
                var year = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("YYYY");
                var month = moment(startDt, 'YYYY-MM-DD', true).add(2, "month").format("MM");
                $endYear.val(year);
                $endMonth.val(month);
                return false;
            }

        }

        var schKeywordTypeVal = $("#schKeywordType").val();
        var schKeywordVal = $("#schKeyword").val();

        if(!$.jUtil.isEmpty(schKeywordTypeVal) && $.jUtil.isEmpty(schKeywordVal)) {
            alert('검색조건을 입력해주세요.');
            $("#schKeyword").focus();
            return false;
        }

        return true;
    }
}

/** Grid Script */
var minusSettleGrid = {
    gridView: new RealGridJS.GridView("minusSettleGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        minusSettleGrid.initGrid();
        minusSettleGrid.initDataProvider();
        minusSettleGrid.event();
    },
    initGrid: function () {
        minusSettleGrid.gridView.setDataSource(minusSettleGrid.dataProvider);
        minusSettleGrid.gridView.setStyles(minusSettleGridBaseInfo.realgrid.styles);
        minusSettleGrid.gridView.setDisplayOptions(minusSettleGridBaseInfo.realgrid.displayOptions);
        minusSettleGrid.gridView.setColumns(minusSettleGridBaseInfo.realgrid.columns);
        minusSettleGrid.gridView.setOptions(minusSettleGridBaseInfo.realgrid.options);

        var stringCol = [["actionType", "settleState", "settleSrl", "settlePreDt", "settlePeriodCd", "settleCycleType",
            "settleStartEndDt", "partnerId", "vendorCd", "partnerNm"],[ "chgDt", "chgId"]];

        SettleCommon.setRealGridSumHeader(minusSettleGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        minusSettleGrid.dataProvider.setFields(minusSettleGridBaseInfo.dataProvider.fields);
        minusSettleGrid.dataProvider.setOptions(minusSettleGridBaseInfo.dataProvider.options);
    },
    event : function() {
        //이미지 버튼 클릭시 Action
        minusSettleGrid.gridView.onImageButtonClicked = function (grid, itemIndex, column, buttonIndex, name) {
            var settleSrl = grid.getValue(itemIndex, "settleSrl");

            var reqData = new Object();

            reqData.settleSrl = settleSrl;

            if (confirm("선택하신 건을 마감처리 하시겠습니까?\n다음 지급SRL의 보류금액이 집계되지 않습니다.")) {
                CommonAjax.basic({
                    url         : '/settle/minusSettle/setMinusSettleComplete.json',
                    data        : JSON.stringify(reqData),
                    method      : "POST",
                    contentType : "application/json; charset=utf-8",
                    callbackFunc: function(res) {
                        if (res.returnCode != "SUCCESS") {
                            alert(res.returnMessage);
                        } else {
                            alert("선택하신 건이 수기마감 되었습니다.");
                        }

                        minusSettleMain.search();
                    }
                });
            }
        };
    },
    setData: function (dataList) {
        minusSettleGrid.dataProvider.clearRows();
        minusSettleGrid.dataProvider.setRows(dataList);
    }
};