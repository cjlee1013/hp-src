
/***
 * 판매현황 점별 합계 그리드 설정
 */
var dailyPaymentSumGrid = {
    gridView : new RealGridJS.GridView("dailyPaymentSumGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyPaymentSumGrid.initGrid();
        dailyPaymentSumGrid.initDataProvider();
        dailyPaymentSumGrid.event();
    },
    initGrid : function () {
        dailyPaymentSumGrid.gridView.setDataSource(dailyPaymentSumGrid.dataProvider);
        dailyPaymentSumGrid.gridView.setStyles(dailyPaymentSumBaseInfo.realgrid.styles);
        dailyPaymentSumGrid.gridView.setDisplayOptions(dailyPaymentSumBaseInfo.realgrid.displayOptions);
        dailyPaymentSumGrid.gridView.setOptions(dailyPaymentSumBaseInfo.realgrid.options);
        dailyPaymentSumGrid.gridView.setColumns(dailyPaymentSumBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ [ "mallType", "originStoreId", "storeNm" ] ];
        SettleCommon.setRealGridSumHeader(dailyPaymentSumGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        dailyPaymentSumGrid.dataProvider.setFields(dailyPaymentSumBaseInfo.dataProvider.fields);
        dailyPaymentSumGrid.dataProvider.setOptions(dailyPaymentSumBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dailyPaymentSumGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            dailyPaymentSumGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        dailyPaymentSumGrid.dataProvider.clearRows();
        dailyPaymentSumGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = dailyPaymentSumGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/dailyPayment/getDailyPaymentList.json?dateType='+ $("input:radio[name='dateType']:checked").val();
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#startYear").val() +'-'+ $("#startMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        _url += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "PARTNER_ID") {
            _url += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            _url += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "VENDOR_CD") {
            _url += "&vendorCd="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NO") {
            _url += "&itemNo="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NM") {
            _url += "&itemNm=" + $('#searchField').val();
        }

        if ( rowDataJson.originStoreId != '전체') {
            _url += "&originStoreId="+ Number(rowDataJson.originStoreId) +"&storeType="+ rowDataJson.storeType;
        }
        _url += "&siteType="+ rowDataJson.siteType;
        if (rowDataJson.mallType.length == 2) {
            _url += "&mallType="+ rowDataJson.mallType;
        } else {
            _url += "&mallType=TD";
        }

        CommonAjax.basic(
            {
                url: _url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailyPaymentListGrid.setData(res);
                    dailyPaymentInfoGrid.setRealGridHeaderSum(dailyPaymentListGrid.gridView, [], 40);

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    excelDownload: function() {
        if(dailyPaymentSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailyPaymentListForm.setExcelFileName("판매현황_점별합계");
        dailyPaymentSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * 판매현황 그리드 설정
 */
var dailyPaymentListGrid = {
    gridView : new RealGridJS.GridView("dailyPaymentListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyPaymentListGrid.initGrid();
        dailyPaymentListGrid.initDataProvider();
        dailyPaymentListGrid.event();
    },
    initGrid : function () {
        dailyPaymentListGrid.gridView.setDataSource(dailyPaymentListGrid.dataProvider);
        dailyPaymentListGrid.gridView.setStyles(dailyPaymentListBaseInfo.realgrid.styles);
        dailyPaymentListGrid.gridView.setDisplayOptions(dailyPaymentListBaseInfo.realgrid.displayOptions);
        dailyPaymentListGrid.gridView.setOptions(dailyPaymentListBaseInfo.realgrid.options);
        dailyPaymentListGrid.gridView.setColumns(dailyPaymentListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "originStoreId", "storeNm", "mallType", "partnerId", "vendorCd", "partnerNm", "businessNm", "orderType", "itemType", "itemNo", "itemNm", "taxYn", "feeRate", "orgPrice", "itemSaleAmt" ] ];
        SettleCommon.setRealGridSumHeader(dailyPaymentListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        dailyPaymentListGrid.dataProvider.setFields(dailyPaymentListBaseInfo.dataProvider.fields);
        dailyPaymentListGrid.dataProvider.setOptions(dailyPaymentListBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dailyPaymentListGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            dailyPaymentListGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        dailyPaymentListGrid.dataProvider.clearRows();
        dailyPaymentListGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = dailyPaymentListGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/dailyPayment/getDailyPaymentInfo.json?partnerId='+ rowDataJson.partnerId +'&itemNo='+ rowDataJson.itemNo;

        if(rowDataJson.orgPrice != 0) {
            _url += "&orgPrice="+ rowDataJson.orgPrice;
        }
        if(rowDataJson.itemSaleAmt != 0) {
            _url += "&itemSaleAmt="+ rowDataJson.itemSaleAmt;
        }
        if ( rowDataJson.mallType != 'DS' && rowDataJson.mallType != 'DC') {
            _url += "&originStoreId="+ Number(rowDataJson.originStoreId) +"&storeType="+ rowDataJson.storeType;
        }
        var basicDt = rowDataJson.basicDt;
        if (basicDt.length < 8) {
            basicDt = basicDt +'-01';
            var endDt = moment(basicDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
            _url += "&endDt="+ endDt;
        }
        if ( rowDataJson.mallType.length == 2) {
            _url += "&mallType="+ rowDataJson.mallType
        } else {
            _url += "&mallType=TD";
        }
        _url += "&siteType="+ rowDataJson.siteType +"&startDt="+ basicDt.replace(/-/gi, "");

        CommonAjax.basic(
            {
                url: _url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailyPaymentInfoGrid.setData(res);
                    dailyPaymentInfoGrid.setRealGridHeaderSum(dailyPaymentInfoGrid.gridView, [], 40);

                    dailyPaymentInfoGrid.gridView.setColumnProperty(dailyPaymentInfoGrid.gridView.columnByField("userNo"),
                        "header", { summary: { text: " ", styles: { textAlignment: "center" } } });
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    excelDownload: function() {
        if(dailyPaymentListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailyPaymentListForm.setExcelFileName("판매현황_상품별합계");
        dailyPaymentListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true
        });
    },
};
/***
 * 판매현황 상세 그리드
 */
var dailyPaymentInfoGrid = {
    gridView : new RealGridJS.GridView("dailyPaymentInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailyPaymentInfoGrid.initGrid();
        dailyPaymentInfoGrid.initDataProvider();
    },
    initGrid : function () {
        dailyPaymentInfoGrid.gridView.setDataSource(dailyPaymentInfoGrid.dataProvider);
        dailyPaymentInfoGrid.gridView.setStyles(dailyPaymentDetailBaseInfo.realgrid.styles);
        dailyPaymentInfoGrid.gridView.setDisplayOptions(dailyPaymentDetailBaseInfo.realgrid.displayOptions);
        dailyPaymentInfoGrid.gridView.setOptions(dailyPaymentDetailBaseInfo.realgrid.options);
        dailyPaymentInfoGrid.gridView.setColumns(dailyPaymentDetailBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "originStoreId", "storeNm", "mallType", "orderType", "purchaseOrderNo", "claimNo"
            , "bundleNo", "gubun", "orderItemNo", "itemNo", "itemNm", "taxYn", "feeRate", "userNm" ] ];
        SettleCommon.setRealGridSumHeader(dailyPaymentInfoGrid.gridView, mergeCells);

    },
    initDataProvider : function() {
        dailyPaymentInfoGrid.dataProvider.setFields(dailyPaymentDetailBaseInfo.dataProvider.fields);
        dailyPaymentInfoGrid.dataProvider.setOptions(dailyPaymentDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dailyPaymentInfoGrid.dataProvider.clearRows();
        dailyPaymentInfoGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailyPaymentInfoGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailyPaymentListForm.setExcelFileName("판매현황_주문상세내역");
        dailyPaymentInfoGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns, _index) {
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "data" && idx > 0 && idx < _index){
                _gridView.setColumnProperty(_gridView.getColumns()[idx].name, "header", {
                    summary: {
                        styles: { textAlignment: "far", "numberFormat": "#,##0" },
                        expression: "sum"} });
            }
        });

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
        _gridView.setColumnProperty(_gridView.getColumns()[_index], "header", { summary: { text: "", styles: { textAlignment: "center" } } });
    },
};
/***
 * 판매현황 폼
 */
var dailyPaymentListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
        $('#searchField').val('');
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,PARTNER_NM,VENDOR_CD,ITEM_NO,ITEM_NM");
        $('#storeType').val('HYPER');
        $('#mallType').val('TD');
        $('#siteType').attr('disabled', true);
    },
    initSearchDate : function (flag) {
        dailyPaymentListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailyPaymentListForm.setDate.setEndDate(0);
        dailyPaymentListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth').val(month);
        $('#startYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailyPaymentListForm.search);
        $('#searchResetBtn').bindClick(dailyPaymentListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailyPaymentListGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(dailyPaymentInfoGrid.excelDownload);
        $('#sumExcelDownloadBtn').bindClick(dailyPaymentSumGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });

        $('#mallType').on('change', function() {
            if($(this).val() != "TD") {
                $('#storeType').val('');
            }

            if($(this).val() == "MARKET") {
                $('#siteType').removeAttr('disabled');
            } else {
                $('#siteType').attr('disabled', true).val('');
            }
        });
    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 32) {
                alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }

        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색키워드를 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'PARTNER_ID' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'VENDOR_CD' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('업체코드는 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'ITEM_NO' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('상품명은 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    setSearchData : function () {
        var searchForm = "&"+ $("form[name=searchForm]").serialize();
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#startYear").val() +'-'+ $("#startMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        searchForm = searchForm.replace("&siteType=","&=");
        if ($('#mallType').val() == "MARKET") {
            searchForm = searchForm.replace("&mallType=","&=");
            if ($('#siteType').val() != "") {
                searchForm += "&siteType="+ $('#siteType').val();
            } else {
                searchForm += "&mallType=TD&siteType="+ $('#mallType').val();
            }
        } else if ($('#mallType').val() == "HOME") {
            searchForm = searchForm.replace("&mallType=","&=")+ "&siteType=HOME&mallType=";
        } else if ($('#mallType').val() != "") {
            searchForm += "&siteType=HOME";
        }

        if ($('#searchType').val() == "PARTNER_ID") {
            searchForm += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            searchForm += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "VENDOR_CD") {
            searchForm += "&vendorCd="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NO") {
            searchForm += "&itemNo="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NM") {
            searchForm += "&itemNm="+ $('#searchField').val();
        }
        return searchForm;
    },
    search : function () {
        if(!dailyPaymentListForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        var searchForm = dailyPaymentListForm.setSearchData();

        CommonAjax.basic(
            {
                url: '/settle/dailyPayment/getDailyPaymentSum.json?' + encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailyPaymentSumGrid.setData(res);
                    SettleCommon.setRealGridHeaderSum(dailyPaymentSumGrid.gridView, []);

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val();
        }
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            dailyPaymentListForm.init();
        });
    }
};