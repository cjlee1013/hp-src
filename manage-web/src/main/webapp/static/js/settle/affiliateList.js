var affiliateSalesGrid = {
    gridView : new RealGridJS.GridView("affiliateSalesGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        affiliateSalesGrid.initGrid();
        affiliateSalesGrid.initDataProvider();
    },
    initGrid : function () {
        affiliateSalesGrid.gridView.setDataSource(affiliateSalesGrid.dataProvider);
        affiliateSalesGrid.gridView.setStyles(affiliateSalesBaseInfo.realgrid.styles);
        affiliateSalesGrid.gridView.setDisplayOptions(affiliateSalesBaseInfo.realgrid.displayOptions);
        affiliateSalesGrid.gridView.setColumns(affiliateSalesBaseInfo.realgrid.columns);
        affiliateSalesGrid.gridView.setOptions(affiliateSalesBaseInfo.realgrid.options);
        affiliateSalesGrid.gridView.onDataCellClicked = affiliateSalesGrid.selectRow;
        var mergeCells = [["basicDt", "affiliateCd", "channelNm", "affiliateNm", "siteType", "siteName"]];
        SettleCommon.setRealGridSumHeader(affiliateSalesGrid.gridView, mergeCells);
    },
    selectRow : function(gridView, data) {
        var row = affiliateSalesGrid.dataProvider.getJsonRow(data.dataRow);
        var basicDt = row.basicDt.replace(/-/gi, "");
        var params = 'startDt='+basicDt+'&endDt='+basicDt+'&affiliateCd='+row.affiliateCd+'&siteType='+row.siteType;
        if (affiliateSalesGrid.gubun != "") {
            params += '&gubun='+affiliateSalesGrid.gubun;
        }
        affiliateDetailGrid.fileName = "제휴채널실적_"+basicDt+"_"+row.affiliateCd+"_"+row.siteName;
        CommonAjax.basic(
        {
            url: '/settle/affiliate/getDetail.json?'+params,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                affiliateDetailGrid.setData(res);
                var channelNm = (row.channelNm==null?"":row.channelNm);
                var affiliateNm = (row.affiliateNm==null?"":row.affiliateNm);
                $('#descriptionDetail').html("제휴채널명: " + channelNm + " | 제휴업체명: " + affiliateNm + " | 주문건 수: " + row.orderCnt + " | 취소건 수: " + row.cancelCnt);
                //그룹헤더컬럼 합계
                var stringCol = [ "orderPrice", "shipPrice", "vatPrice", "supplyAmt", "commissionAmt"];
                affiliateSalesForm.setRealGridHeaderSum(affiliateDetailGrid.gridView, stringCol);
            }
        });
    },
    initDataProvider : function() {
        affiliateSalesGrid.dataProvider.setFields(affiliateSalesBaseInfo.dataProvider.fields);
        affiliateSalesGrid.dataProvider.setOptions(affiliateSalesBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        affiliateSalesGrid.dataProvider.clearRows();
        affiliateSalesGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(affiliateSalesGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        affiliateSalesGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: affiliateSalesGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : "",
    startDt : "",
    endDt : "",
    searchType : "",
    searchValue : "",
    siteType : "",
    gubun : ""
};
var affiliateDetailGrid = {
    gridView : new RealGridJS.GridView("affiliateDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        affiliateDetailGrid.initGrid();
        affiliateDetailGrid.initDataProvider();
    },
    initGrid : function () {
        affiliateDetailGrid.gridView.setDataSource(affiliateDetailGrid.dataProvider);
        affiliateDetailGrid.gridView.setStyles(affiliateDetailBaseInfo.realgrid.styles);
        affiliateDetailGrid.gridView.setDisplayOptions(affiliateDetailBaseInfo.realgrid.displayOptions);
        affiliateDetailGrid.gridView.setColumns(affiliateDetailBaseInfo.realgrid.columns);
        affiliateDetailGrid.gridView.setOptions(affiliateDetailBaseInfo.realgrid.options);
        affiliateDetailGrid.gridView.setColumnFilters("platform",affiliateDetailGrid.filters)
        var mergeCells = [["basicDt", "affiliateCd", "channelNm", "affiliateNm", "siteType", "siteName"]];
        SettleCommon.setRealGridSumHeader(affiliateDetailGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        affiliateDetailGrid.dataProvider.setFields(affiliateDetailBaseInfo.dataProvider.fields);
        affiliateDetailGrid.dataProvider.setOptions(affiliateDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        affiliateDetailGrid.dataProvider.clearRows();
        affiliateDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(affiliateDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        affiliateDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: affiliateDetailGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : "",
    filters : [{
        name: "PC",
        criteria: "value = 'PC'"
    }, {
        name: "모바일",
        criteria: "(value = 'MWEB') or (value = 'APP')"
    }]
};
var affiliateDetailAllGrid = {
    gridView : new RealGridJS.GridView("affiliateDetailAllGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        affiliateDetailAllGrid.initGrid();
        affiliateDetailAllGrid.initDataProvider();
    },
    initGrid : function () {
        affiliateDetailAllGrid.gridView.setDataSource(affiliateDetailAllGrid.dataProvider);
        affiliateDetailAllGrid.gridView.setStyles(affiliateDetailAllBaseInfo.realgrid.styles);
        affiliateDetailAllGrid.gridView.setDisplayOptions(affiliateDetailAllBaseInfo.realgrid.displayOptions);
        affiliateDetailAllGrid.gridView.setColumns(affiliateDetailAllBaseInfo.realgrid.columns);
        affiliateDetailAllGrid.gridView.setOptions(affiliateDetailAllBaseInfo.realgrid.options);
    },
    initDataProvider : function() {
        affiliateDetailAllGrid.dataProvider.setFields(affiliateDetailAllBaseInfo.dataProvider.fields);
        affiliateDetailAllGrid.dataProvider.setOptions(affiliateDetailAllBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        affiliateDetailAllGrid.dataProvider.clearRows();
        affiliateDetailAllGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        var params = 'startDt='+affiliateSalesGrid.startDt+'&endDt='+affiliateSalesGrid.endDt;
        if (affiliateSalesGrid.searchValue != "") {
            params += '&'+affiliateSalesGrid.searchType+'='+affiliateSalesGrid.searchValue;
        }
        if (affiliateSalesGrid.siteType != "") {
            params += '&siteType='+affiliateSalesGrid.siteType;
        }
        if (affiliateSalesGrid.gubun != "") {
            params += '&gubun='+affiliateSalesGrid.gubun;
        }
        CommonAjax.basic(
{
            url: '/settle/affiliate/getDetail.json?'+params,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                affiliateDetailAllGrid.setData(res);
                affiliateDetailAllGrid.gridView.exportGrid( {
                    type: "excel",
                    target: "local",
                    fileName: affiliateDetailAllGrid.fileName+".xlsx",
                    showProgress: false,
                    progressMessage: " 엑셀 데이터 추줄 중입니다."
                });
            }
        });

    },
    fileName : ""
};
/***
 * 제휴채널실적 상세
 */
var affiliateSalesForm = {
    init : function () {
        this.initSearchDate('-6d',0);
    },
    initSearchDate : function (startDt,endDt) {
        affiliateSalesForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        affiliateSalesForm.setDate.setEndDate(endDt);
        affiliateSalesForm.setDate.setStartDate(startDt);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(affiliateSalesForm.search);
        $('#searchResetBtn').bindClick(affiliateSalesForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(affiliateSalesGrid.excelDownload);
        $('#excelDownloadDetailBtn').bindClick(affiliateDetailGrid.excelDownload);
        $('#excelDownloadDetailAllBtn').bindClick(affiliateDetailAllGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 93) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        startDt = startDt.format("YYYYMMDD");
        endDt = endDt.format("YYYYMMDD");
        var searchForm = "startDt="+startDt+"&endDt="+endDt;
        var siteType = $("#siteType").val();
        if (siteType != "") {
            searchForm = searchForm + "&siteType=" + siteType;
        }
        var gubun = $("#gubun").val();
        if (gubun != "") {
            searchForm = searchForm + "&gubun=" + gubun;
        }
        var searchType = $("#searchType").val();
        var searchValue = $("#searchValue").val();
        if (searchValue != "") {
            if (searchValue.length == 1) {
                alert ("검색어는 2자 이상 입력해주세요.");
                return;
            }
            searchForm = searchForm + "&" + searchType + "=" + searchValue;
        }
        CommonAjax.basic(
        {
            url:'/settle/affiliate/getSales.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                affiliateSalesGrid.setData(res);
                $('#searchCnt').html($.jUtil.comma(affiliateSalesGrid.gridView.getItemCount()));
                affiliateSalesGrid.startDt = startDt;
                affiliateSalesGrid.endDt = endDt;
                affiliateSalesGrid.siteType = siteType;
                affiliateSalesGrid.gubun = gubun;
                affiliateSalesGrid.searchType = searchType;
                affiliateSalesGrid.searchValue = searchValue;
                affiliateSalesGrid.fileName ="제휴채널실적_"+startDt+"_"+endDt;
                affiliateDetailAllGrid.fileName = "제휴채널실적_상세내역_"+startDt+"_"+endDt;
                //그룹헤더컬럼 합계
                var stringCol = [ "orderCnt","cancelCnt","orderPrice", "shipPrice", "vatPrice", "supplyAmt", "commissionAmt"];
                affiliateSalesForm.setRealGridHeaderSum(affiliateSalesGrid.gridView, stringCol);
            }
        });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            affiliateSalesForm.init();
        });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    }
};