var etcPaymentForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchMonth();
        this.bindingEvent();
        CommonAjaxBlockUI.global();
    },
    initSearchDate : function (flag) {
        etcPaymentForm.setDate = Calendar.datePickerRange('startDt', 'endDt');
        etcPaymentForm.setDate.setEndDate('-1d');
        etcPaymentForm.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    initSearchMonth : function () {
        var year = moment($('#endDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#endDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startYear').val(year);
        etcPaymentForm.setMonth(year,'startMonth');
        $('#startMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(etcPaymentForm.search);
        $('#clearBtn').bindClick(etcPaymentForm.resetSearchForm);
        $('#excelDownloadStoreBtn').bindClick(etcPaymentStoreGrid.excelDownload);
        $('#excelDownloadDailyBtn').bindClick(etcPaymentDailyGrid.excelDownload);
        $('#excelDownloadListBtn').bindClick(etcPaymentListGrid.excelDownload);
    },
    search : function () {
        var startDt;
        var endDt;
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            startDt = moment($("#startDt").val(), 'YYYY-MM-DD', true);
            endDt = moment($("#endDt").val(), 'YYYY-MM-DD', true);

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#startDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 31) {
                alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                $("#startDt").val(endDt.add(-31, "day").format("YYYY-MM-DD"));
                return false;
            }
        } else { //M
            startDt = $("#startYear").val() + '-' + $("#startMonth").val() + '-01';
            endDt = moment(startDt, 'YYYY-MM-DD', true).endOf('month');
            startDt = moment(startDt, 'YYYY-MM-DD', true);
        }

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var storeType = $("#storeType").val();
        if (storeType != "") {
            searchForm = searchForm + "&storeType=" + storeType;
        }
        var fileName = getKind + "실적조회_";
        etcPaymentStoreGrid.fileName = fileName + "점별합계_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        etcPaymentDailyGrid.fileName = fileName + "일별상세_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        etcPaymentListGrid.fileName = fileName + "건별상세_" + startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
            {
                url:'/settle/management/get'+getKind+'PaymentStore.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    etcPaymentStoreGrid.setData(res);
                    var stringCol = [ "completeAmt","orderPrice","dgvAmt","collectAmt","mhcUsePoint","mhcAprPoint","mhcSavePoint" ];
                    etcPaymentForm.setRealGridHeaderSum(etcPaymentStoreGrid.gridView, stringCol);
                }
            });
        CommonAjax.basic(
            {
                url:'/settle/management/get'+getKind+'PaymentDaily.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    etcPaymentDailyGrid.setData(res);
                    var stringCol = [ "completeAmt","orderPrice","dgvAmt","collectAmt","mhcUsePoint","mhcAprPoint","mhcSavePoint" ];
                    etcPaymentForm.setRealGridHeaderSum(etcPaymentDailyGrid.gridView, stringCol);
                }
            });
        CommonAjax.basic(
            {
                url:'/settle/management/get'+getKind+'PaymentList.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    etcPaymentListGrid.setData(res);
                    var stringCol = [ "completeAmt","orderPrice","dgvAmt","collectAmt","mhcUsePoint","mhcAprPoint"];
                    etcPaymentForm.setRealGridHeaderSum(etcPaymentListGrid.gridView, stringCol);
                }
            });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            etcPaymentForm.init();
        });
    },
    setMonth : function(year,_id) {
        var nowDate = new Date();
        var maxMonth;
        if (year == nowDate.getFullYear()) {
            maxMonth = nowDate.getMonth();
        } else {
            maxMonth = 11;
        }
        $('#'+_id).empty();
        for (var i=1;i<=maxMonth+1;i++) {
            if (i<10) {
                $('#'+_id).append("<option value='0" +i+ "'>0" + i + " 월</option>");
            } else  {
                $('#'+_id).append("<option value='" +i+ "'>" + i + " 월</option>");
            }
        }
    }
};
var etcPaymentStoreGrid = {
    gridView : new RealGridJS.GridView("etcPaymentStoreGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        etcPaymentStoreGrid.initGrid();
        etcPaymentStoreGrid.initDataProvider();
    },
    initGrid : function () {
        etcPaymentStoreGrid.gridView.setDataSource(etcPaymentStoreGrid.dataProvider);
        etcPaymentStoreGrid.gridView.setStyles(etcPaymentStoreBaseInfo.realgrid.styles);
        etcPaymentStoreGrid.gridView.setDisplayOptions(etcPaymentStoreBaseInfo.realgrid.displayOptions);
        etcPaymentStoreGrid.gridView.setColumns(etcPaymentStoreBaseInfo.realgrid.columns);
        etcPaymentStoreGrid.gridView.setOptions(etcPaymentStoreBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [
            ["storeType" , "storeId" , "storeNm", "onlineCostCenter"]
        ];
        SettleCommon.setRealGridSumHeader(etcPaymentStoreGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        etcPaymentStoreGrid.dataProvider.setFields(etcPaymentStoreBaseInfo.dataProvider.fields);
        etcPaymentStoreGrid.dataProvider.setOptions(etcPaymentStoreBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        etcPaymentStoreGrid.dataProvider.clearRows();
        etcPaymentStoreGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(etcPaymentStoreGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        etcPaymentStoreGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: etcPaymentStoreGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};

var etcPaymentDailyGrid = {
    gridView : new RealGridJS.GridView("etcPaymentDailyGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        etcPaymentDailyGrid.initGrid();
        etcPaymentDailyGrid.initDataProvider();
    },
    initGrid : function () {
        etcPaymentDailyGrid.gridView.setDataSource(etcPaymentDailyGrid.dataProvider);
        etcPaymentDailyGrid.gridView.setStyles(etcPaymentDailyBaseInfo.realgrid.styles);
        etcPaymentDailyGrid.gridView.setDisplayOptions(etcPaymentDailyBaseInfo.realgrid.displayOptions);
        etcPaymentDailyGrid.gridView.setColumns(etcPaymentDailyBaseInfo.realgrid.columns);
        etcPaymentDailyGrid.gridView.setOptions(etcPaymentDailyBaseInfo.realgrid.options);
        etcPaymentDailyGrid.gridView.onDataCellClicked = etcPaymentDailyGrid.selectRow;
        var mergeCells = [["basicDt","storeType" , "storeId" , "storeNm", "onlineCostCenter"]];
        SettleCommon.setRealGridSumHeader(etcPaymentDailyGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        etcPaymentDailyGrid.dataProvider.setFields(etcPaymentDailyBaseInfo.dataProvider.fields);
        etcPaymentDailyGrid.dataProvider.setOptions(etcPaymentDailyBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        etcPaymentDailyGrid.dataProvider.clearRows();
        etcPaymentDailyGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(etcPaymentDailyGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        etcPaymentDailyGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: etcPaymentDailyGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};

var etcPaymentListGrid = {
    gridView : new RealGridJS.GridView("etcPaymentListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        etcPaymentListGrid.initGrid();
        etcPaymentListGrid.initDataProvider();
    },
    initGrid : function () {
        etcPaymentListGrid.gridView.setDataSource(etcPaymentListGrid.dataProvider);
        etcPaymentListGrid.gridView.setStyles(etcPaymentListBaseInfo.realgrid.styles);
        etcPaymentListGrid.gridView.setDisplayOptions(etcPaymentListBaseInfo.realgrid.displayOptions);
        etcPaymentListGrid.gridView.setColumns(etcPaymentListBaseInfo.realgrid.columns);
        etcPaymentListGrid.gridView.setOptions(etcPaymentListBaseInfo.realgrid.options);
        etcPaymentListGrid.gridView.onDataCellClicked = etcPaymentListGrid.selectRow;
        var mergeCells = [["basicDt","posNo","paymentNo", "purchaseOrderNo", "claimNo", "gubun",
        "branchCode","chnlCd" , "receiptNo","storeId", "appNo","mhcAprNumber","shipFshYn"]];
        SettleCommon.setRealGridSumHeader(etcPaymentListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        etcPaymentListGrid.dataProvider.setFields(etcPaymentListBaseInfo.dataProvider.fields);
        etcPaymentListGrid.dataProvider.setOptions(etcPaymentListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        etcPaymentListGrid.dataProvider.clearRows();
        etcPaymentListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(etcPaymentListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        etcPaymentListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: etcPaymentListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};