/** Main Script */
var unshipOrderMain = {
    /**
     * 전역변수
     */
    global: {
        currentDate : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        unshipOrderMain.bindingEvent();
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 검색조건 select box
        $("#schKeywordType").on('change', function() { unshipOrderMain.setChangeKeywordType() });

        // [검색] 버튼
        $("#schBtn").bindClick(unshipOrderMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(unshipOrderMain.reset, "unshipOrderSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(unshipOrderMain.excelDownload);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#unshipOrderSearchForm").serialize();
        CommonAjax.basic({
            url         : '/settle/unshipOrder/getUnshipOrderList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                unshipOrderMain.global.currentDate = $('#schYear').val() + $('#schMonth').val();
                unshipOrderGrid.setData(res);
                $('#unshipOrderSearchCnt').html($.jUtil.comma(unshipOrderGrid.gridView.getItemCount()));

                //그룹헤더컬럼 합계
                SettleCommon.setRealGridHeaderSum(unshipOrderGrid.gridView, []);
            }
        });
    },

    /**
     * 검색 조건 select box 선택 이벤트
     */
    setChangeKeywordType: function() {
        var $schKeywordType = $('#schKeywordType');
        var $schKeyword = $('#schKeyword');

        if($.jUtil.isEmpty($schKeywordType.val())) {
            $schKeyword.val("");
            $schKeyword.prop("disabled",true);
            $schKeyword.prop("placeholder","");
        } else {
            $schKeyword.prop("disabled",false);
            $schKeyword.prop("placeholder","검색어를 입력하세요");
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (unshipOrderGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  "미배송주문조회_" + unshipOrderMain.global.currentDate;
        unshipOrderGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        unshipOrderMain.setChangeKeywordType();

        var year = moment().format("YYYY");
        var month = moment().format("MM");

        $('#schMonth').val(month);
        $('#schYear').val(year);
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        var schKeywordTypeVal = $("#schKeywordType").val();
        var schKeywordVal = $("#schKeyword").val();

        if(!$.jUtil.isEmpty(schKeywordTypeVal) && $.jUtil.isEmpty(schKeywordVal)) {
            alert('검색조건을 입력해주세요.');
            $("#schKeyword").focus();
            return false;
        }

        return true;
    }
}

/** Grid Script */
var unshipOrderGrid = {
    gridView: new RealGridJS.GridView("unshipOrderGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        unshipOrderGrid.initGrid();
        unshipOrderGrid.initDataProvider();
    },
    initGrid: function () {
        unshipOrderGrid.gridView.setDataSource(unshipOrderGrid.dataProvider);
        unshipOrderGrid.gridView.setStyles(unshipOrderGridBaseInfo.realgrid.styles);
        unshipOrderGrid.gridView.setDisplayOptions(unshipOrderGridBaseInfo.realgrid.displayOptions);
        unshipOrderGrid.gridView.setColumns(unshipOrderGridBaseInfo.realgrid.columns);
        unshipOrderGrid.gridView.setOptions(unshipOrderGridBaseInfo.realgrid.options);

        var stringCol = ["basicDt", "carryDownType", "paymentBasicDt", "completeDt", "storeType", "originStoreId",
            "storeNm", "mallType", "gubun", "purchaseOrderNo", "bundleNo", "claimNo",
            "partnerId","vendorCd","supplier","partnerName", "orderType", "orderItemNo",
            "division", "deptNo", "itemNo", "itemNm", "taxYn", "commissionRate", "orgPrice",
            "itemSaleAmt","shipStatus","nonRcvType"];

        SettleCommon.setRealGridSumHeader(unshipOrderGrid.gridView, stringCol);
    },
    initDataProvider: function () {
        unshipOrderGrid.dataProvider.setFields(unshipOrderGridBaseInfo.dataProvider.fields);
        unshipOrderGrid.dataProvider.setOptions(unshipOrderGridBaseInfo.dataProvider.options);
    },
    setData: function (dataList) {
        unshipOrderGrid.dataProvider.clearRows();
        unshipOrderGrid.dataProvider.setRows(dataList);
    }
};