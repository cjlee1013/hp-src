var pgPaymentInfoListGrid = {
    gridView : new RealGridJS.GridView("pgPaymentInfoListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgPaymentInfoListGrid.initGrid();
        pgPaymentInfoListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        pgPaymentInfoListGrid.gridView.setDataSource(pgPaymentInfoListGrid.dataProvider);
        pgPaymentInfoListGrid.gridView.setStyles(pgPaymentInfoListBaseInfo.realgrid.styles);
        pgPaymentInfoListGrid.gridView.setDisplayOptions(pgPaymentInfoListBaseInfo.realgrid.displayOptions);
        pgPaymentInfoListGrid.gridView.setColumns(pgPaymentInfoListBaseInfo.realgrid.columns);
        pgPaymentInfoListGrid.gridView.setOptions(pgPaymentInfoListBaseInfo.realgrid.options);
        //헤더 merge
        var mergeCells = [ ["basicDt", "pgKind", "originStoreId", "originStoreNm", "purchaseOrderNo","pgOid", "mallType", "gubun"]];
        SettleCommon.setRealGridSumHeader(pgPaymentInfoListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        pgPaymentInfoListGrid.dataProvider.setFields(pgPaymentInfoListBaseInfo.dataProvider.fields);
        pgPaymentInfoListGrid.dataProvider.setOptions(pgPaymentInfoListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgPaymentInfoListGrid.dataProvider.clearRows();
        pgPaymentInfoListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgPaymentInfoListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgPaymentInfoListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgPaymentInfoListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    fileName : ""
};
/***
 * 매출조회 파트너 상세
 */
var pgPaymentInfoListForm = {
    init : function () {
        this.initSearchDate('-1d');
    },
    initSearchDate : function (flag) {
        pgPaymentInfoListForm.setDate = Calendar.datePickerRange('startDt', 'endDt');
        pgPaymentInfoListForm.setDate.setEndDate('-1d');
        pgPaymentInfoListForm.setDate.setStartDate(flag);
        $("#endDt").datepicker("option", "minDate", $("#startDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(pgPaymentInfoListForm.search);
        $('#clearBtn').bindClick(pgPaymentInfoListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(pgPaymentInfoListGrid.excelDownload);
        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchValue').attr('readonly', true).val('');
            } else {
                $('#searchValue').removeAttr('readonly');
                $('#searchValue').focus();
            }
        });
    },
    search : function () {
        var startDt = moment($("#startDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#endDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if (endDt.diff(startDt, "month", true) > 1) {
            alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
            $("#startDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#startDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }

        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");
        var mallType = $("#mallType").val();
        if (mallType != "") {
            searchForm = searchForm + "&mallType=" + mallType;
        }
        var storeType = $("#storeType").val();
        if (storeType != "") {
            searchForm = searchForm + "&storeType=" + storeType;
        }
        var pgKind = $("#pgKind").val();
        if (pgKind != "") {
            searchForm = searchForm + "&pgKind=" + pgKind;
        }
        var searchType = $("#searchType").val();
        if (searchType != "") {
            searchForm = searchForm + "&" + searchType + "=" + $("#searchValue").val();
        }
        var gubun = $("#gubun").val();
        searchForm = searchForm + "&gubun=" + gubun;
        pgPaymentInfoListGrid.fileName = "PG수수료_일마감조회_";
        if (gubun == "basicDt") {
            pgPaymentInfoListGrid.fileName += "결제일_";
        } else {
            pgPaymentInfoListGrid.fileName += "입금일_";
        }
        pgPaymentInfoListGrid.fileName += startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
            {
                url:'/settle/management/getPgPaymentInfoList.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    pgPaymentInfoListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(pgPaymentInfoListGrid.gridView.getItemCount()));
                    //그룹헤더컬럼 합계
                    var stringCol = [ "completeAmt", "shipAmt", "claimReturnShipAmt", "couponSellerChargeAmt", "couponHomeChargeAmt",
                        "couponCardChargeAmt", "cardCouponHomeAmt", "cardCouponCardAmt", "shipDiscountAmt", "empDiscountAmt", "promoDiscountAmt",
                        "cartCouponHomeAmt","ekpiAmt" , "orderPrice", "pgAmt", "dgvAmt", "mhcAmt", "ocbAmt",
                        "mileageAmt", "saleAgencyFee", "settleAmt", "commissionAmt", "depositAmt", "payCompleteAmt",
                        "depositBalance", "payScheduleAmt"];
                    pgPaymentInfoListGrid.setRealGridHeaderSum(pgPaymentInfoListGrid.gridView, stringCol);
                }
            });

    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            pgPaymentInfoListForm.init();
        });
    }
};