/***
 * 매출현황 그리드 설정
 */
var dailySettleSumGrid = {
    gridView : new RealGridJS.GridView("dailySettleSumGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleSumGrid.initGrid();
        dailySettleSumGrid.initDataProvider();
        dailySettleSumGrid.event();
    },
    initGrid : function () {
        dailySettleSumGrid.gridView.setDataSource(dailySettleSumGrid.dataProvider);
        dailySettleSumGrid.gridView.setStyles(dailySettleSumBaseInfo.realgrid.styles);
        dailySettleSumGrid.gridView.setDisplayOptions(dailySettleSumBaseInfo.realgrid.displayOptions);
        dailySettleSumGrid.gridView.setOptions(dailySettleSumBaseInfo.realgrid.options);
        dailySettleSumGrid.gridView.setColumns(dailySettleSumBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["mallType", "originStoreId", "storeNm"] ];

        //그리드 상단 합계
        SettleCommon.setRealGridSumHeader(dailySettleSumGrid.gridView, mergeCells);

    },
    initDataProvider : function() {
        dailySettleSumGrid.dataProvider.setFields(dailySettleSumBaseInfo.dataProvider.fields);
        dailySettleSumGrid.dataProvider.setOptions(dailySettleSumBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dailySettleSumGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            dailySettleSumGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        dailySettleSumGrid.dataProvider.clearRows();
        dailySettleSumGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = dailySettleSumGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/dailySettle/getDailySettleList.json?'+ dailySettleListForm.setSearchData();
        _url = _url.replace("&mallType=", "&=").replace("&storeType=","&=").replace("&siteType=","&=");
        if ( rowDataJson.originStoreId != '전체') {
            _url += "&originStoreId="+ Number(rowDataJson.originStoreId) +"&storeType="+ rowDataJson.storeType;
        }
        _url += "&siteType="+ rowDataJson.siteType;
        if (rowDataJson.mallType.length == 2) {
            _url += "&mallType="+ rowDataJson.mallType;
        } else {
            _url += "&mallType=TD";
        }

        var isAppend = false;
        var interval;
        if (rowDataJson.mallType == 'DC' || rowDataJson.mallType == 'DS') {
            if($("input:radio[name='dateType']:checked").val() =='M') {
                isAppend = true;
                _url = _url.replace("&endDt=", "&=") +"&endDt="+ $("#startYear").val() + $("#startMonth").val() + '15';
            }
            else if ($("input:radio[name='dateType']:checked").val() =='D') {
                var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
                var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

                if (endDt.diff(startDt, "day", true) > 15) {
                    isAppend = true;
                    interval = moment($("#schStartDt").val(), 'YYYY-MM-DD', true).add(14, "day").format("YYYYMMDD")
                    _url = _url.replace("&endDt=", "&=") +"&endDt="+ interval;
                }
            }
        }

        CommonAjax.basic(
            {
                url: _url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailySettleListGrid.setData(res);

                    if (isAppend) {
                        if ($("input:radio[name='dateType']:checked").val() =='M') {
                            _url = _url.replace("&startDt=", "&=") +"&startDt="+ $("#startYear").val() + $("#startMonth").val() + '15';
                            _url = _url.replace("&endDt=", "&=") +"&endDt="+ moment($("#startYear").val() +'-'+ $("#startMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
                        }
                        else if($("input:radio[name='dateType']:checked").val() =='D') {
                            _url = _url.replace("&startDt=", "&=") +"&startDt="+ interval;
                            _url = _url.replace("&endDt=", "&=") +"&endDt="+ moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");
                        }

                        CommonAjax.basic({
                            url: _url,
                            data: null,
                            contentType: 'application/json',
                            method: "GET",
                            successMsg: null,
                            callbackFunc:function(res) {
                                dailySettleListGrid.dataProvider.fillJsonData(JSON.stringify(res), {fillMode: 'append'});
                                dailySettleListGrid.gridView.setDataSource(dailySettleListGrid.dataProvider);
                            }
                        });
                    }

                    dailySettleInfoGrid.setRealGridHeaderSum(dailySettleListGrid.gridView, [], 41);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });


    },
    excelDownload: function() {
        if(dailySettleSumGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailySettleListForm.setExcelFileName("매출현황_점별합계");
        dailySettleSumGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 매출현황 그리드 설정
 */
var dailySettleListGrid = {
    gridView : new RealGridJS.GridView("dailySettleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleListGrid.initGrid();
        dailySettleListGrid.initDataProvider();
        dailySettleListGrid.event();
    },
    initGrid : function () {
        dailySettleListGrid.gridView.setDataSource(dailySettleListGrid.dataProvider);
        dailySettleListGrid.gridView.setStyles(dailySettleListBaseInfo.realgrid.styles);
        dailySettleListGrid.gridView.setDisplayOptions(dailySettleListBaseInfo.realgrid.displayOptions);
        dailySettleListGrid.gridView.setOptions(dailySettleListBaseInfo.realgrid.options);
        dailySettleListGrid.gridView.setColumns(dailySettleListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "originStoreId", "storeNm", "mallType", "partnerId", "vendorCd", "partnerNm", "businessNm", "orderType", "itemType", "itemNo", "itemNm", "taxYn", "orgPrice", "itemSaleAmt", "feeRate" ] ];

        //그리드 상단 합계
        SettleCommon.setRealGridSumHeader(dailySettleListGrid.gridView, mergeCells);

        //그리드 에디터화
        dailySettleListGrid.gridView.setEditOptions({
            editable      : false,
            appendable    : true,
            insertable    : false,
            updatable     : false,
            deletable     : false,
            softDeleting  : false,
            hideDeletedRows: true,
            readOnly      : false
        });

    },
    initDataProvider : function() {
        dailySettleListGrid.dataProvider.setFields(dailySettleListBaseInfo.dataProvider.fields);
        dailySettleListGrid.dataProvider.setOptions(dailySettleListBaseInfo.dataProvider.options);
    },
    event : function() {
        // 그리드 선택
        dailySettleListGrid.gridView.onDataCellDblClicked = function(gridView, index) {
            dailySettleListGrid.selectRow(index.dataRow);
        };
    },
    setData : function(dataList) {
        dailySettleListGrid.dataProvider.clearRows();
        dailySettleListGrid.dataProvider.setRows(dataList);
    },
    selectRow: function (selectRowId) {
        CommonAjaxBlockUI.startBlockUI();
        var rowDataJson = dailySettleListGrid.dataProvider.getJsonRow(selectRowId);
        var _url = '/settle/dailySettle/getDailySettleInfo.json?partnerId='+ rowDataJson.partnerId +"&itemNo="+ rowDataJson.itemNo;

        if(rowDataJson.orgPrice != 0) {
            _url += "&orgPrice="+ rowDataJson.orgPrice;
        }
        if(rowDataJson.itemSaleAmt != 0) {
            _url += "&itemSaleAmt="+ rowDataJson.itemSaleAmt;
        }
        if(rowDataJson.feeAmt != 0) {
            _url += "&feeAmt="+ rowDataJson.feeAmt;
        }
        var basicDt = rowDataJson.basicDt;
        if (basicDt.length < 8) {
            basicDt = basicDt +'-01';
            var endDt = moment(basicDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
            _url += "&endDt="+ endDt;
        }
        if ( rowDataJson.mallType != 'DS' && rowDataJson.mallType != 'DC') {
            _url += "&originStoreId="+ Number(rowDataJson.originStoreId) +"&storeType="+ rowDataJson.storeType;
        }
        if ( rowDataJson.mallType.length == 2) {
            _url += "&mallType="+ rowDataJson.mallType
        } else {
            _url += "&mallType=TD";
        }
        _url += "&siteType="+ rowDataJson.siteType +"&startDt="+ basicDt.replace(/-/gi, "");

        CommonAjax.basic(
            {
                url: _url,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    dailySettleInfoGrid.setData(res);

                    dailySettleInfoGrid.setRealGridHeaderSum(dailySettleInfoGrid.gridView, [], 40);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    excelDownload: function() {
        if(dailySettleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailySettleListForm.setExcelFileName("매출현황_상품별합계");
        dailySettleListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 매출관리 상세 그리드
 */
var dailySettleInfoGrid = {
    gridView : new RealGridJS.GridView("dailySettleInfoGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        dailySettleInfoGrid.initGrid();
        dailySettleInfoGrid.initDataProvider();
    },
    initGrid : function () {
        dailySettleInfoGrid.gridView.setDataSource(dailySettleInfoGrid.dataProvider);
        dailySettleInfoGrid.gridView.setStyles(dailySettleDetailBaseInfo.realgrid.styles);
        dailySettleInfoGrid.gridView.setDisplayOptions(dailySettleDetailBaseInfo.realgrid.displayOptions);
        dailySettleInfoGrid.gridView.setOptions(dailySettleDetailBaseInfo.realgrid.options);
        dailySettleInfoGrid.gridView.setColumns(dailySettleDetailBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["basicDt", "originStoreId", "storeNm", "mallType", "gubun", "purchaseOrderNo", "claimNo"
            , "bundleNo", "orderType", "orderItemNo", "itemNo", "itemNm", "taxYn", "feeRate" ] ];

        //그리드 상단 합계
        SettleCommon.setRealGridSumHeader(dailySettleInfoGrid.gridView,mergeCells);

    },
    initDataProvider : function() {
        dailySettleInfoGrid.dataProvider.setFields(dailySettleDetailBaseInfo.dataProvider.fields);
        dailySettleInfoGrid.dataProvider.setOptions(dailySettleDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        dailySettleInfoGrid.dataProvider.clearRows();
        dailySettleInfoGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(dailySettleInfoGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = dailySettleListForm.setExcelFileName("매출현황_주문상세내역");
        dailySettleInfoGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns, _index) {
        $.each(_gridView.getColumns(), function (idx, data) {
            if(data.type == "data" && idx > 0 && idx < _index){
                _gridView.setColumnProperty(_gridView.getColumns()[idx].name, "header", {
                    summary: {
                        styles: { textAlignment: "far", "numberFormat": "#,##0" },
                        expression: "sum"} });
            }
        });

        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
        _gridView.setColumnProperty(_gridView.getColumns()[_index], "header", { summary: { text: "", styles: { textAlignment: "center" } } });
    },
};
/***
 * 매출조회 파트너 상세
 */
var dailySettleListForm = {
    init : function () {
        this.initSearchDate('-1d');
        this.initSearchYM();
        $('#searchField').val('');
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,PARTNER_NM,VENDOR_CD,ITEM_NO,ITEM_NM");
        $('#storeType').val('HYPER');
        $('#mallType').val('TD');
        $('#siteType').attr('disabled', true).val('');
    },
    initSearchDate : function (flag) {
        dailySettleListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        dailySettleListForm.setDate.setEndDate(0);
        dailySettleListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());

        if (flag == '-1d') {
            $("#schEndDt").val($("#schStartDt").val());
        }
    },
    initSearchYM : function () {
        var year = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("YYYY");
        var month = moment($('#schStartDt').val(), 'YYYY-MM-DD', true).format("MM");

        $('#startMonth').val(month);
        $('#startYear').val(year);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(dailySettleListForm.search);
        $('#searchResetBtn').bindClick(dailySettleListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(dailySettleListGrid.excelDownload);
        $('#detailExcelDownloadBtn').bindClick(dailySettleInfoGrid.excelDownload);
        $('#sumExcelDownloadBtn').bindClick(dailySettleSumGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });

        $('#mallType').on('change', function() {
            if($(this).val() != "TD") {
                $('#storeType').val('');
            }

            if($(this).val() == "MARKET") {
                $('#siteType').removeAttr('disabled');
            } else {
                $('#siteType').attr('disabled', true).val('');
            }
        });
    },
    valid : function () {
        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day");

            if(!startDt.isValid() || !endDt.isValid()){
                alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
                return false;
            }
            if(endDt.diff(startDt, "day", true) < 0) {
                alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
                $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
                return false;
            }
            if (endDt.diff(startDt, "day", true) > 32) {
                alert("조회기간은 최대 1개월 범위까지만 가능합니다.");
                $("#schStartDt").val(endDt.add(-1, "month").format("YYYY-MM-DD"));
                return false;
            }

        }

        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색키워드를 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'PARTNER_ID' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'VENDOR_CD' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('업체코드는 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'ITEM_NO' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('상품명은 숫자 외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    setSearchData : function () {
        var searchForm = "&"+ $("form[name=searchForm]").serialize();

        if ($("input:radio[name='dateType']:checked").val() =='D') {
            var startDt = $("#schStartDt").val().split('-').join('');
            var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        } else {
            var startDt = $("#startYear").val() + $("#startMonth").val() + '01';
            var endDt = moment($("#startYear").val() +'-'+ $("#startMonth").val() + '-01', 'YYYY-MM-DD', true).add(1, "month").format("YYYYMMDD");
        }
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        searchForm = searchForm.replace("&siteType=","&=");
        if ($('#mallType').val() == "MARKET") {
            searchForm = searchForm.replace("&mallType=","&=");
            if ($('#siteType').val() != "") {
                searchForm += "&siteType="+ $('#siteType').val();
            } else {
                searchForm += "&mallType=TD&siteType="+ $('#mallType').val();
            }
        } else if ($('#mallType').val() == "HOME") {
            searchForm = searchForm.replace("&mallType=","&=")+ "&siteType=HOME&mallType=";
        } else if ($('#mallType').val() != "") {
            searchForm += "&siteType=HOME";
        }

        if ($('#searchType').val() == "PARTNER_ID") {
            searchForm += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            searchForm += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "VENDOR_CD") {
            searchForm += "&vendorCd="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NO") {
            searchForm += "&itemNo="+ $('#searchField').val();
        } else if ($('#searchType').val() == "ITEM_NM") {
            searchForm += "&itemNm="+ $('#searchField').val();
        }
        return searchForm;
    },
    search : function () {
        if(!dailySettleListForm.valid()) return;
        CommonAjaxBlockUI.startBlockUI();

        CommonAjax.basic(
            {
                url: '/settle/dailySettle/getDailySettleSum.json?' + encodeURI(dailySettleListForm.setSearchData()),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    dailySettleSumGrid.setData(res);
                    SettleCommon.setRealGridHeaderSum(dailySettleSumGrid.gridView, []);

                    CommonAjaxBlockUI.stopBlockUI();
                }
            });
    },
    setExcelFileName : function (fileName) {
        var excelFileName = "";
        if ($("input:radio[name='dateType']:checked").val() == "D") {
            excelFileName = fileName +'_'+ $("#schStartDt").val().replace(/-/gi, "")+'_'+$("#schEndDt").val().replace(/-/gi, "");
        } else {
            excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val();
        }
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function() {
            this.reset();
            dailySettleListForm.init();
            dailySettleListGrid.init();
            dailySettleInfoGrid.init();
            dailySettleSumGrid.init();
        });
    }
};