/***
 * 세금계산서 그리드 설정
 */
var taxBillInvoiceListGrid = {
    gridView : new RealGridJS.GridView("taxBillInvoiceListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        taxBillInvoiceListGrid.initGrid();
        taxBillInvoiceListGrid.initDataProvider();
        taxBillInvoiceListGrid.event();
    },
    initGrid : function () {
        taxBillInvoiceListGrid.gridView.setDataSource(taxBillInvoiceListGrid.dataProvider);
        taxBillInvoiceListGrid.gridView.setStyles(taxBillInvoiceListBaseInfo.realgrid.styles);
        taxBillInvoiceListGrid.gridView.setDisplayOptions(taxBillInvoiceListBaseInfo.realgrid.displayOptions);
        taxBillInvoiceListGrid.gridView.setOptions(taxBillInvoiceListBaseInfo.realgrid.options);
        taxBillInvoiceListGrid.gridView.setColumns(taxBillInvoiceListBaseInfo.realgrid.columns);

        //헤더 merge
        var mergeCells = [ ["taxKeyNo", "issueDt", "partnerId", "vendorCd", "partnerNm", "salesKind", "businessNm", "partnerNo", "partnerOwner", "mngId", "mngEmail"]
            , ["regDt", "regId"] ];
        SettleCommon.setRealGridSumHeader(taxBillInvoiceListGrid.gridView, mergeCells);
    },
    initDataProvider : function() {
        taxBillInvoiceListGrid.dataProvider.setFields(taxBillInvoiceListBaseInfo.dataProvider.fields);
        taxBillInvoiceListGrid.dataProvider.setOptions(taxBillInvoiceListBaseInfo.dataProvider.options);
    },
    event : function() {
    },
    setData : function(dataList) {
        taxBillInvoiceListGrid.dataProvider.clearRows();
        taxBillInvoiceListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(taxBillInvoiceListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName = taxBillInvoice.setExcelFileName("발행내역관리");
        taxBillInvoiceListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    }
};
/***
 * 세금계산서
 */
var taxBillInvoice = {
    init : function () {
        $('#searchField').val('');
        this.initSearchYM();
        // 입력조건 생성
        SettleCommon.getInputTypeSetter('searchType', "PARTNER_ID,VENDOR_CD,PARTNER_NM,PARTNER_NO,MNG_NM,MNG_ENAIL");
    },
    initSearchYM : function () {
        var today =  new Date();
        var year = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("YYYY");
        var month = moment(today, 'YYYY-MM-DD', true).add(-1, "month").format("MM");

        $('#startYear').val(year);
        SettleCommon.setMonth(year,'startMonth');
        $('#startMonth').val(month);
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(taxBillInvoice.search);
        $('#searchResetBtn').bindClick(taxBillInvoice.resetSearchForm);
        $('#excelDownloadBtn').bindClick(taxBillInvoiceListGrid.excelDownload);

        $('#searchType').on('change', function() {
            if($(this).val() == "") {
                $('#searchField').attr('readonly', true).val('');
            } else {
                $('#searchField').removeAttr('readonly');
                $('#searchField').focus();
            }
        });
    },
    valid : function () {
        if($("#searchType").val() != '' && $.jUtil.isEmpty($('#searchField').val())) {
            alert('검색조건을 입력해주세요.');
            $("#searchField").focus();
            return false;
        }
        if($("#searchType").val() == 'PARTNER_ID' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM', 'ENG'])){
            alert('판매업체ID는 숫자, 영문외의 문자는 입력할 수 없습니다.');
            $("#searchField").focus();
            return false;
        } else if($("#searchType").val() == 'PARTNER_NO' && !$.jUtil.isAllowInput($('#searchField').val(), ['NUM'])){
            alert('사업자번호는 숫자만 입력 가능합니다.');
            $("#searchField").focus();
            return false;
        }

        return true;
    },
    search : function () {
        CommonAjaxBlockUI.startBlockUI();
        var searchForm = $("form[name=searchForm]").serialize();

        var startDt = $("#startYear").val() +'-'+ $("#startMonth").val()+ '-01';
        var endDt = moment(startDt, 'YYYY-MM-DD', true).add(1, "month").format("YYYY-MM-DD");
        searchForm += '&startDt=' + startDt + '&endDt=' + endDt;

        if ($('#searchType').val() == "PARTNER_ID") {
            searchForm += "&partnerId="+$('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NM") {
            searchForm += "&partnerNm="+ $('#searchField').val();
        } else if ($('#searchType').val() == "PARTNER_NO") {
            searchForm += "&partnerNo="+ $('#searchField').val();
        } else if ($('#searchType').val() == "MNG_NM") {
            searchForm += '&mngNm=' + $("#searchField").val();
        } else if ($('#searchType').val() == "MNG_EMAIL") {
            searchForm += '&mngEmail=' + $("#searchField").val();
        } else if ($('#searchType').val() == "VENDOR_CD") {
            searchForm += '&vendorCd=' + $("#searchField").val();
        }

        CommonAjax.basic(
            {
                url:'/settle/taxBillInvoice/getTaxBillInvoiceList.json?'+ encodeURI(searchForm),
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    taxBillInvoiceListGrid.setData(res);
                    $('#searchCnt').html($.jUtil.comma(taxBillInvoiceListGrid.gridView.getItemCount()));

                    //그룹헤더컬럼 합계
                    SettleCommon.setRealGridHeaderSum(taxBillInvoiceListGrid.gridView, []);
                    CommonAjaxBlockUI.stopBlockUI();
                }
            });

    },
    setExcelFileName : function (fileName) {
        var excelFileName = fileName +'_'+ $("#startYear").val() + $("#startMonth").val();
        return excelFileName;
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            taxBillInvoice.init();
        });
    }
};