/** Main Script */
var partnerBalanceMain = {
    /**
     * 전역변수
     */
    global: {
        startDt : ""
        , endDt : ""
    },
    /**
     * init 이벤트
     */
    init: function() {
        partnerBalanceMain.bindingEvent();
        partnerBalanceMain.initSearchDateCustom("0");
    },

    /**
     * init Date 이벤트
     */
    initSearchDateCustom : function(flag) {
        var setDateVal = Calendar.datePickerRange("startDt", "endDt");
        setDateVal.setStartDate(flag);
        setDateVal.setEndDate(0);
    },

    /**
     * binding 이벤트
     */
    bindingEvent: function() {

        // 검색조건 select box
        $("#schKeywordType").on('change', function() { partnerBalanceMain.setChangeKeywordType() });

        // [검색] 버튼
        $("#schBtn").bindClick(partnerBalanceMain.search);
        // [초기화] 버튼
        $("#schResetBtn").bindClick(partnerBalanceMain.reset, "partnerBalanceSearchForm");
        // [엑셀다운] 버튼
        $("#schExcelDownloadBtn").bindClick(partnerBalanceMain.excelDownload);
    },

    /**
     * 데이터 검색
     */
    search: function() {
        if (!validCheck.search()) {
            return;
        }

        var reqData = $("#partnerBalanceSearchForm").serialize();

        var paramStartDt = "";
        var paramEndDt = "";
        var inpStartDt = $("#startDt").val();
        var inpEndDt = $("#endDt").val();

        // 일별 날짜 설정
        paramStartDt = inpStartDt.replace(/-/gi, "");
        paramEndDt = moment(inpEndDt, 'YYYY-MM-DD', true).add(1, "day").format("YYYYMMDD");

        reqData += '&schStartDt=' + paramStartDt + '&schEndDt=' + paramEndDt;

        CommonAjax.basic({
            url         : '/settle/partnerBalance/getPartnerBalanceList.json?' + reqData,
            data        : null,
            method      : "GET",
            callbackFunc: function(res) {
                partnerBalanceGrid.setData(res);
                $('#partnerBalanceSearchCnt').html($.jUtil.comma(partnerBalanceGrid.gridView.getItemCount()));

                partnerBalanceMain.global.startDt = paramStartDt;
                partnerBalanceMain.global.endDt = moment(inpEndDt, 'YYYY-MM-DD', true).format("YYYYMMDD");

                var stringCol = ["paymentSettleAmt", "settleSettleAmt", "finalSettleAmt", "minusSettleAmt"];

                // 그룹헤더컬럼 합계
                partnerBalanceMain.setRealGridHeaderSum(partnerBalanceGrid.gridView, stringCol, res);
            }
        });
    },

    /**
     * 검색 조건 select box 선택 이벤트
     */
    setChangeKeywordType: function() {
        var $schKeywordType = $('#schKeywordType');
        var $schKeyword = $('#schKeyword');

        if($.jUtil.isEmpty($schKeywordType.val())) {
            $schKeyword.val("");
            $schKeyword.prop("disabled",true);
            $schKeyword.prop("placeholder","");
        } else {
            $schKeyword.prop("disabled",false);
            $schKeyword.prop("placeholder","검색어를 입력하세요");
        }
    },

    /**
     * 엑셀 다운로드
     */
    excelDownload: function() {
        if (partnerBalanceGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        var fileName =  "파트너잔액조회_" + partnerBalanceMain.global.startDt + "_" + partnerBalanceMain.global.endDt;
        partnerBalanceGrid.gridView.exportGrid({
            type: "excel",
            target: "local",
            fileName: fileName + ".xlsx",
            showProgress: true,
            progressMessage: "엑셀 Export 중 입니다."
        });
    },

    /**
     * 검색영역 폼 초기화
     */
    reset: function(formId) {
        $("#"+formId).resetForm();

        partnerBalanceMain.setChangeKeywordType();
        partnerBalanceMain.initSearchDateCustom("0");
    },

    setRealGridHeaderSum : function (_gridView, _groupColumns, data) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }

        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });

        // 검색 데이터 중 가장 최근 날짜 기준으로 누적합계 SUM
        var basicDt = "";
        var resLen = data.length;
        var paymentSettleSum = "";
        var settleSettleSum = "";
        var finalSettleSum = "";
        var minusSettleSum = "";
        var advanceAmt = "";
        var balanceAmt = "";

        if (resLen > 0) {
            paymentSettleSum = 0;
            settleSettleSum = 0;
            finalSettleSum = 0;
            minusSettleSum = 0;
            advanceAmt = 0;
            balanceAmt = 0;

            for (var i=resLen-1; i>=0; i--) {
                if (i == resLen-1) {
                    basicDt = data[i].basicDt;
                }

                if(basicDt == data[i].basicDt) {
                    paymentSettleSum += Number(data[i].paymentSettleSum.replace(/,/g, ""));
                    settleSettleSum += Number(data[i].settleSettleSum.replace(/,/g, ""));
                    finalSettleSum += Number(data[i].finalSettleSum.replace(/,/g, ""));
                    minusSettleSum += Number(data[i].minusSettleSum.replace(/,/g, ""));
                } else {
                    break;
                }
            }

            // 미확정잔액, 미정산잔액 계산
            advanceAmt = paymentSettleSum - settleSettleSum;
            balanceAmt = settleSettleSum - finalSettleSum + minusSettleSum;

            // 콤마 적용
            paymentSettleSum = $.jUtil.comma(paymentSettleSum.toString());
            settleSettleSum = $.jUtil.comma(settleSettleSum.toString());
            finalSettleSum = $.jUtil.comma(finalSettleSum.toString());
            minusSettleSum = $.jUtil.comma(minusSettleSum.toString());
            advanceAmt = $.jUtil.comma(advanceAmt.toString());
            balanceAmt = $.jUtil.comma(balanceAmt.toString());
        }

        _gridView.setColumnProperty("paymentSettleSum", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:paymentSettleSum } });
        _gridView.setColumnProperty("settleSettleSum", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:settleSettleSum } });
        _gridView.setColumnProperty("finalSettleSum", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:finalSettleSum } });
        _gridView.setColumnProperty("minusSettleSum", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:minusSettleSum } });
        _gridView.setColumnProperty("advanceAmt", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:advanceAmt } });
        _gridView.setColumnProperty("balanceAmt", "header",{
            summary: { styles: { textAlignment: "far", "numberFormat": "#,##0" }, text:balanceAmt } });
    }
};

/** 검증 */
var validCheck = {
    /**
     * 검색조건 유효성 체크
     */
    search: function() {
        // 조회기간 (calendar.js 에서 공통 체크하는 영역에서 먼저 체크함)
        var $schStartDt = $("#startDt");
        var $schEndDt = $("#endDt");
        var schStartDt = moment($schStartDt.val(), 'YYYY-MM-DD', true);
        var schEndDt = moment($schEndDt.val(), 'YYYY-MM-DD', true);

        if(!schStartDt.isValid() || !schEndDt.isValid()) {
            alert("검색기간을 입력해 주세요.");
            return false;
        }

        if(schEndDt.diff(schStartDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            partnerBalanceMain.initSearchDateCustom("0");
            return false;
        }

        if (schEndDt.diff(schStartDt, "month", true) > 3) {
            alert("조회기간은 최대 3개월까지 설정 가능합니다.");
            $schStartDt.val(schEndDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }

        var schKeywordTypeVal = $("#schKeywordType").val();
        var schKeywordVal = $("#schKeyword").val();

        if(!$.jUtil.isEmpty(schKeywordTypeVal) && $.jUtil.isEmpty(schKeywordVal)) {
            alert('검색조건을 입력해주세요.');
            $("#schKeyword").focus();
            return false;
        }

        return true;
    }
}

/** Grid Script */
var partnerBalanceGrid = {
    gridView: new RealGridJS.GridView("partnerBalanceGrid"),
    dataProvider: new RealGridJS.LocalDataProvider(),
    init: function () {
        partnerBalanceGrid.initGrid();
        partnerBalanceGrid.initDataProvider();
        partnerBalanceGrid.event();
    },
    initGrid: function () {
        partnerBalanceGrid.gridView.setDataSource(partnerBalanceGrid.dataProvider);
        partnerBalanceGrid.gridView.setStyles(partnerBalanceGridBaseInfo.realgrid.styles);
        partnerBalanceGrid.gridView.setDisplayOptions(partnerBalanceGridBaseInfo.realgrid.displayOptions);
        partnerBalanceGrid.gridView.setColumns(partnerBalanceGridBaseInfo.realgrid.columns);
        partnerBalanceGrid.gridView.setOptions(partnerBalanceGridBaseInfo.realgrid.options);

        var stringCol = ["basicDt", "partnerId", "vendorCd", "parnterName"];

        SettleCommon.setRealGridSumHeader(partnerBalanceGrid.gridView, stringCol);

        //컬럼 그룹핑 너비 조정
        partnerBalanceGrid.gridView.setColumnProperty("발생 내역", "displayWidth", 450);
        partnerBalanceGrid.gridView.setColumnProperty("누적 합계", "displayWidth", 450);
    },
    initDataProvider: function () {
        partnerBalanceGrid.dataProvider.setFields(partnerBalanceGridBaseInfo.dataProvider.fields);
        partnerBalanceGrid.dataProvider.setOptions(partnerBalanceGridBaseInfo.dataProvider.options);
    },
    event : function() {

    },
    setData: function (dataList) {
        partnerBalanceGrid.dataProvider.clearRows();
        partnerBalanceGrid.dataProvider.setRows(dataList);
    }
};