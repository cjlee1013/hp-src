var pgSettleListGrid = {
    gridView : new RealGridJS.GridView("pgSettleListGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgSettleListGrid.initGrid();
        pgSettleListGrid.initDataProvider();
        CommonAjaxBlockUI.global();
    },
    initGrid : function () {
        pgSettleListGrid.gridView.setDataSource(pgSettleListGrid.dataProvider);
        pgSettleListGrid.gridView.setStyles(pgSettleListBaseInfo.realgrid.styles);
        pgSettleListGrid.gridView.setDisplayOptions(pgSettleListBaseInfo.realgrid.displayOptions);
        pgSettleListGrid.gridView.setColumns(pgSettleListBaseInfo.realgrid.columns);
        pgSettleListGrid.gridView.setOptions(pgSettleListBaseInfo.realgrid.options);
        var mergeCells = [ ["pgKind", "paymentTool"] ];
        SettleCommon.setRealGridSumHeader(pgSettleListGrid.gridView,mergeCells);
        pgSettleListGrid.gridView.setColumnProperty("결제기준", "displayWidth", 300);
        pgSettleListGrid.gridView.setColumnProperty("입금기준", "displayWidth", 300);
        pgSettleListGrid.gridView.setColumnProperty("지급대행", "displayWidth", 200);
    },
    initDataProvider : function() {
        pgSettleListGrid.dataProvider.setFields(pgSettleListBaseInfo.dataProvider.fields);
        pgSettleListGrid.dataProvider.setOptions(pgSettleListBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgSettleListGrid.dataProvider.clearRows();
        pgSettleListGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgSettleListGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgSettleListGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgSettleListGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};
var pgSettleDetailGrid = {
    gridView : new RealGridJS.GridView("pgSettleDetailGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgSettleDetailGrid.initGrid();
        pgSettleDetailGrid.initDataProvider();
    },
    initGrid : function () {
        pgSettleDetailGrid.gridView.setDataSource(pgSettleDetailGrid.dataProvider);
        pgSettleDetailGrid.gridView.setStyles(pgSettleDetailBaseInfo.realgrid.styles);
        pgSettleDetailGrid.gridView.setDisplayOptions(pgSettleDetailBaseInfo.realgrid.displayOptions);
        pgSettleDetailGrid.gridView.setColumns(pgSettleDetailBaseInfo.realgrid.columns);
        pgSettleDetailGrid.gridView.setOptions(pgSettleDetailBaseInfo.realgrid.options);
        var mergeCells = [ ["basicDt", "pgKind", "paymentTool"] ];
        SettleCommon.setRealGridSumHeader(pgSettleDetailGrid.gridView,mergeCells);
        pgSettleDetailGrid.gridView.setColumnProperty("결제기준", "displayWidth", 300);
        pgSettleDetailGrid.gridView.setColumnProperty("입금기준", "displayWidth", 300);
        pgSettleDetailGrid.gridView.setColumnProperty("지급대행", "displayWidth", 200);
    },
    initDataProvider : function() {
        pgSettleDetailGrid.dataProvider.setFields(pgSettleDetailBaseInfo.dataProvider.fields);
        pgSettleDetailGrid.dataProvider.setOptions(pgSettleDetailBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgSettleDetailGrid.dataProvider.clearRows();
        pgSettleDetailGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgSettleDetailGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgSettleDetailGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgSettleDetailGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};

var pgSettleHistoryGrid = {
    gridView : new RealGridJS.GridView("pgSettleHistoryGrid"),
    dataProvider : new RealGridJS.LocalDataProvider(),
    init : function() {
        pgSettleHistoryGrid.initGrid();
        pgSettleHistoryGrid.initDataProvider();
    },
    initGrid : function () {
        pgSettleHistoryGrid.gridView.setDataSource(pgSettleHistoryGrid.dataProvider);
        pgSettleHistoryGrid.gridView.setStyles(pgSettleHistoryBaseInfo.realgrid.styles);
        pgSettleHistoryGrid.gridView.setDisplayOptions(pgSettleHistoryBaseInfo.realgrid.displayOptions);
        pgSettleHistoryGrid.gridView.setColumns(pgSettleHistoryBaseInfo.realgrid.columns);
        pgSettleHistoryGrid.gridView.setOptions(pgSettleHistoryBaseInfo.realgrid.options);
        var mergeCells = [ ["basicDt","paymentDt", "pgKind", "mallType", "code", "description", "settleSrl", "gubun"] ];
        SettleCommon.setRealGridSumHeader(pgSettleHistoryGrid.gridView,mergeCells);
        pgSettleHistoryGrid.gridView.setColumnProperty("거래내역", "displayWidth", 300);
        pgSettleHistoryGrid.gridView.setColumnProperty("지급대행", "displayWidth", 300);
    },
    initDataProvider : function() {
        pgSettleHistoryGrid.dataProvider.setFields(pgSettleHistoryBaseInfo.dataProvider.fields);
        pgSettleHistoryGrid.dataProvider.setOptions(pgSettleHistoryBaseInfo.dataProvider.options);
    },
    setData : function(dataList) {
        pgSettleHistoryGrid.dataProvider.clearRows();
        pgSettleHistoryGrid.dataProvider.setRows(dataList);
    },
    excelDownload: function() {
        if(pgSettleHistoryGrid.gridView.getItemCount() == 0) {
            alert("검색 결과가 없습니다.");
            return false;
        }

        pgSettleHistoryGrid.gridView.exportGrid( {
            type: "excel",
            target: "local",
            fileName: pgSettleHistoryGrid.fileName+".xlsx",
            showProgress: true,
            progressMessage: " 엑셀 데이터 추줄 중입니다."
        });

    },
    fileName : ""
};
/***
 * PG정산관리
 */
var pgSettleListForm = {
    init : function () {
        this.initSearchDate('0d');
    },
    initSearchDate : function (flag) {
        pgSettleListForm.setDate = Calendar.datePickerRange('schStartDt', 'schEndDt');
        pgSettleListForm.setDate.setEndDate(0);
        pgSettleListForm.setDate.setStartDate(flag);
        $("#schEndDt").datepicker("option", "minDate", $("#schStartDt").val());
    },
    bindingEvent : function() {
        $('#searchBtn').bindClick(pgSettleListForm.search);
        $('#clearBtn').bindClick(pgSettleListForm.resetSearchForm);
        $('#excelDownloadBtn').bindClick(pgSettleListGrid.excelDownload);
        $('#excelDownloadDetailBtn').bindClick(pgSettleDetailGrid.excelDownload);
        $('#excelDownloadHistoryBtn').bindClick(pgSettleHistoryGrid.excelDownload);
    },
    search : function () {
        var startDt = moment($("#schStartDt").val(), 'YYYY-MM-DD', true);
        var endDt = moment($("#schEndDt").val(), 'YYYY-MM-DD', true);

        if(!startDt.isValid() || !endDt.isValid()){
            alert("조회기간 시작일, 종료일이 입력되어 있지 않거나 올바른 형식(YYYY-MM-DD)이 아닙니다.");
            return false;
        }
        if(endDt.diff(startDt, "day", true) < 0) {
            alert("조회기간의 시작일은 종료일보다 클 수 없습니다.");
            $("#schStartDt").val(endDt.format("YYYY-MM-DD"));
            return false;
        }
        if (endDt.diff(startDt, "day", true) > 93) {
            alert("조회기간은 최대 3개월 범위까지만 가능합니다.");
            $("#schStartDt").val(endDt.add(-3, "month").format("YYYY-MM-DD"));
            return false;
        }
        var searchForm = "startDt="+startDt.format("YYYYMMDD")+"&endDt="+endDt.format("YYYYMMDD");

        pgSettleListGrid.fileName = "PG정산관리_"+startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        pgSettleDetailGrid.fileName = "PG정산관리_일별_상세내역_"+startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        pgSettleHistoryGrid.fileName = "PG정산관리_히스토리_상세내역_"+startDt.format("YYYYMMDD")+"_"+endDt.format("YYYYMMDD");
        CommonAjax.basic(
        {
            url:'/settle/pgSettle/getPgSettleSum.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                pgSettleListGrid.setData(res);
                var stringCol = [ "basicPg", "basicCommission", "basicSettle", "basicSum"
                    , "payPg", "payCommission", "paySettle", "paySum", "depositBalance"
                    , "depositAmt", "settleAmt", "depositLeft", "receivableAmt"];
                pgSettleListForm.setRealGridHeaderSum(pgSettleListGrid.gridView, stringCol);
            }
        });
        CommonAjax.basic(
        {
            url:'/settle/pgSettle/getPgSettleDetail.json?'+ searchForm,
            data: null,
            contentType: 'application/json',
            method: "GET",
            successMsg: null,
            callbackFunc:function(res) {
                pgSettleDetailGrid.setData(res);
                var stringCol = [ "basicPg", "basicCommission", "basicSettle", "basicSum"
                    , "payPg", "payCommission", "paySettle", "paySum"
                    , "depositAmt", "settleAmt", "receivableAmt"];
                pgSettleListForm.setRealGridHeaderSum(pgSettleDetailGrid.gridView, stringCol);
                if (res.length >= 7) {
                    pgSettleDetailGrid.gridView.setColumnProperty("depositBalance", "header", { summary: { text: pgSettleListForm.numberWithCommas(res[0].depositBalance)} });
                    pgSettleDetailGrid.gridView.setColumnProperty("depositLeft", "header", { summary: { text: pgSettleListForm.numberWithCommas(res[res.length - 7].depositLeft)} });
                } else {
                    pgSettleDetailGrid.gridView.setColumnProperty("depositBalance", "header", { summary: { text: 0 } });
                    pgSettleDetailGrid.gridView.setColumnProperty("depositLeft", "header", { summary: { text: 0 } });
                }
            }
        });
        CommonAjax.basic(
            {
                url:'/settle/pgSettle/getPgReceivable.json?'+ searchForm,
                data: null,
                contentType: 'application/json',
                method: "GET",
                successMsg: null,
                callbackFunc:function(res) {
                    pgSettleHistoryGrid.setData(res);
                    var stringCol = [ "transAmt", "depositAmt"];
                    pgSettleListForm.setRealGridHeaderSum(pgSettleHistoryGrid.gridView, stringCol);
                    var maxTrans = new Array();
                    var maxDeposit = new Array();
                    var maxReceivable = new Array();
                    for (var i=0;i<res.length;i++){
                        maxTrans[res[i].pgKind] = res[i].transBalance;
                        maxDeposit[res[i].pgKind] = res[i].depositBalance;
                        maxReceivable[res[i].pgKind] = res[i].receivableAmt;
                    }
                    if (res.length > 0) {
                        pgSettleHistoryGrid.gridView.setColumnProperty("transBalance", "header", { summary: { text: pgSettleListForm.numberWithCommas(maxTrans['INICIS'] + maxTrans['TOSSPG'])} });
                        pgSettleHistoryGrid.gridView.setColumnProperty("depositBalance", "header", { summary: { text: pgSettleListForm.numberWithCommas(maxDeposit['INICIS'] + maxDeposit['TOSSPG'])} });
                        pgSettleHistoryGrid.gridView.setColumnProperty("receivableAmt", "header", { summary: { text: pgSettleListForm.numberWithCommas(maxReceivable['INICIS'] + maxReceivable['TOSSPG'])} });
                    } else {
                        pgSettleHistoryGrid.gridView.setColumnProperty("transBalance", "header", { summary: { text: 0 } });
                        pgSettleHistoryGrid.gridView.setColumnProperty("depositBalance", "header", { summary: { text: 0 } });
                        pgSettleHistoryGrid.gridView.setColumnProperty("receivableAmt", "header", { summary: { text: 0 } });
                    }
                }
            });
    },
    resetSearchForm : function () {
        $("form[name=searchForm]").each(function () {
            this.reset();
            pgSettleListForm.init();
        });
    },
    setRealGridHeaderSum : function (_gridView, _groupColumns) {
        for (var i = 0; i < _groupColumns.length; i++) {
            _gridView.setColumnProperty(_groupColumns[i], "header", {
                summary: {
                    styles: { textAlignment: "far", "numberFormat": "#,##0" },
                    expression: "sum"} });
        }
        //첫 컬럼에 합계 표현
        _gridView.setColumnProperty(_gridView.getColumns()[0], "header", { summary: { text: "합계", styles: { textAlignment: "center" } } });
    },
    numberWithCommas : function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
};