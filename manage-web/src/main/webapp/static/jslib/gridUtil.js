function GridResponseFill( _gridObj, _url, _isResultText, _fuc ) {

	// 데이터 삭제
	_gridObj.clearAll();
	//_gridObj.load(_url, "js"); // load 방법도 됨

	CommonAjax.basic({
		url : _url,
		method : 'get',
		callbackFunc : function(resData) {
			if ( resData == null ) {
				if (_isResultText) {
					alert('정상적으로 조회되지 않았습니다.');
				}
			} else {
                if ( resData.length <= 0 ) {
                    if (_isResultText) {
                        _gridObj.addRow(0,[]);
                        $(_gridObj.cells(0, 0).cell).attr('colspan', _gridObj.getColumnsNum()).html('검색 결과가 없습니다.').css("text-align","center").addClass('n_emptyList');
                    }
                } else {
                    _gridObj.clearAll();
                    _gridObj.parse(resData, 'js');
                }

                if (typeof _fuc == 'function') {
                    _fuc(resData);
                }
            }
		}
	});
}

function GridGetColNumByColName( _gridObj, _colName ) {

	var _gridColNum = _gridObj.getColumnsNum();

	for (var _col=0;_col<_gridColNum;_col++){
		if ( _colName == _gridObj.getColLabel(_col) ){
			return _col;
		}
	}

	return -1;// 못찾은 경우
}

function GridGetValue( _gridObj, _colName, _rowNum ) {

	var colNum = GridGetColNumByColName( _gridObj, _colName);
	var rowID= _gridObj.getRowId(_rowNum);

	return _gridObj.cells(rowID, colNum).getValue();
}

function GridGetValueByRowId( _gridObj, _colName, _rowID ) {

	var colNum = GridGetColNumByColName( _gridObj, _colName);
	return _gridObj.cells(_rowID, colNum).getValue();
}

function GridGetAllValueByRowId( _gridObj, _rowID ) {

    var columnCount = _gridObj.getColumnCount();
    var rowObj = {};
	for (var i=0; i < columnCount; i++) {
        rowObj[_gridObj.getColumnId(i)] = _gridObj.cells(_rowID, i).getValue();
	}
	return rowObj;
}

function GridSetValueByRowId( _gridObj, _colName, _rowID, _value ) {

	var colNum = GridGetColNumByColName( _gridObj, _colName);
	//var rowID= _gridObj.getRowId(_rowNum);

	return _gridObj.cells(_rowID, colNum).setValue(_value);
}

function GridCheckedByRowId( _gridObj, _colName, _rowId ) {
	var colNum = GridGetColNumByColName( _gridObj, _colName);
	_gridObj.cells(_rowId, colNum).setChecked(1);
}

//그리드 데이터 to 엑셀파일
function GridToExcel(_gridObj, _fileName, _sheetName, _isAll){
	var excelUrl = "/GridToExcel/convertGridToExcel?title="+ _sheetName +"&fileName="+ _fileName;
	var saveColumnsVisibility = [];
	var columnCount = _gridObj.getColumnCount();

	//hidden 컬럼까지 엑셀에 표시
	if (_isAll) {
		for (var i=0; i < columnCount; i++) {
			saveColumnsVisibility.push(_gridObj.isColumnHidden(i).toString());
			_gridObj.setColumnHidden(i, false);
		}
	}

	_gridObj.toExcel(excelUrl);

	if (_isAll) {
		_gridObj.setColumnsVisibility(saveColumnsVisibility.join(','));
	}
}

//그리드 셀의 값을 복사하기 위해 이벤트 추가
function GridonKeyPressed(code, ctrl, shift, command, _gridObj){
	//복사시 맥을 위해 metakey 추가
	if(code == 67 && (ctrl || command.metaKey)){
		if (!_gridObj._selectionArea) return alert("복사할 블럭을 선택하세요.");

		_gridObj.setCSVDelimiter("\t");
		_gridObj.copyBlockToClipboard()
	}
	return true;
}

//그리드 row 이동 _ctrl (firstTop: 맨위로, top: 위로, bottom: 아래로, lastBottom: 맨아래로), _rowIds (단일일 경우 id, 복수일 경우 ","컴마로 구분)
function GridMoveRow(_gridObj, _ctrl, _rowIds) {
	var rowsNum = _gridObj.getRowsNum() - 1;
	var rowArr = _rowIds.split(',');
	switch (_ctrl) {
		case "firstTop" : //맨위로 이동
			for (var i in rowArr.reverse()){
				_gridObj.moveRowTo(rowArr[i], -1,"move");
			}
			break;
		case "top" : //위로 이동
			for (var i in rowArr){
				var ind = _gridObj.getRowIndex(rowArr[i]);
				if (ind > 0 && _gridObj.cells(_gridObj.getRowId(ind-1), 0).getValue() == 0) {
					_gridObj.moveRowUp(rowArr[i]);
				}
			}
			break;
		case "bottom" : //아래로 이동
			for (var i in rowArr.reverse()){
				var ind = _gridObj.getRowIndex(rowArr[i]);
				if (ind < rowsNum && _gridObj.cells(_gridObj.getRowId(ind+1), 0).getValue() == 0) {
					_gridObj.moveRowDown(rowArr[i]);
				}
			}
			break;
		case "lastBottom" : //맨아래로 이동
			for (var i in rowArr){
				var rowId = _gridObj.getRowId(rowsNum);
				if (rowId != rowArr[i]) {
					_gridObj.moveRowTo(rowArr[i], rowId, "move");
				}
			}
			break;
        case "tenTop" :
            for (var i in rowArr){
                var ind = _gridObj.getRowId(_gridObj.getRowIndex(rowArr[i]) - 10);
                _gridObj.moveRowTo(rowArr[i], ind,"move");
            }
            break;
        case "tenBottom" :
            for (var i in rowArr){
                var ind = _gridObj.getRowId(_gridObj.getRowIndex(rowArr[i]) + 10);
                _gridObj.moveRowTo(rowArr[i], ind,"move");
            }
            break;

	}
}

//그리드 Rows 카운트 (검색결과없음 메세지 ROW를 제외하고 그리드 ROW를 카운트)
function GridRowsNum(_gridObj){
	var _rowsNum = _gridObj.getRowsNum();
	if (_rowsNum > 0 && _gridObj.doesRowExist(0)) {
		if ( $(_gridObj.cells(0, 0).cell).hasClass('n_emptyList') ) {
			_rowsNum --;
		}
	}
	return _rowsNum;
}

/**
 * 그리드 로우 합치기
 * @param 필수, 그리드 객체
 * @param 필수, 그룹으로 묶을 대상 colName
 * @param 선택, 첫 로우 색상(option)
 * @param 선택, 두번째 로우 색상(option)
 */
function GridSetRowGroupColor(_gridObj, _colName , _color , _secondColor) {

    if(_color == undefined)
        _color = "#ffffff";				//디폴드 흰색

    if(_secondColor == undefined)
        _secondColor ="#f9f9f9";		//디폴트 회색

    _gridObj.forEachRow(function(rowId){
        if(rowId == 1) {
            _gridObj.setRowColor(rowId, _color);
        }
        if(rowId >= 2){
            var preObj = _gridObj.getRowById(rowId-1);
            var tr = $(preObj)[0];
            var preColor = $(tr)[0].childNodes[0].bgColor;
            //console.log("rowId : " + rowId + " : " + (GridGetValueByRowId(_gridObj, _colName, rowId -1) + " :" + GridGetValueByRowId(_gridObj, _colName, rowId)));

            if(GridGetValueByRowId(_gridObj, _colName, rowId-1 ) == GridGetValueByRowId(_gridObj, _colName, rowId))
            {
                _gridObj.setRowColor(rowId ,preColor);
            }
            else {
                if(preColor != _color)
                    _gridObj.setRowColor(rowId, _color);
                else
                    _gridObj.setRowColor(rowId, _secondColor);
            }
        }
    });
}

/**
 * 그리드 로우 합치기
 * 컬럼(세로열)의 value와 keyColName의 value가 같으면 병합
 * @param 필수, 그리드 객체
 * @param 필수, rowspan의 대상 colName
 * @param 필수, 키로 체크할 colName
 */
function  GridSetGroupRowspan(_gridObj, _colName, _keyColName) {

    var rowspanIds = new Array();
    var colNum = GridGetColNumByColName(_gridObj , _colName);

    _gridObj.forEachRow(function(rowId) {
        if(rowId == 1 )
        {
            rowspanIds.push(rowId);
        }
        if(rowId >= 2 ) { //첫번째 열은 비교 대상이 없다.
            if( _keyColName != undefined) {
                if(GridGetValueByRowId(_gridObj, _colName, rowId - 1 ) == GridGetValueByRowId(_gridObj, _colName, rowId)
                    && GridGetValueByRowId(_gridObj, _keyColName, rowId - 1 )  == GridGetValueByRowId(_gridObj, _keyColName, rowId)
                ) {
                    rowspanIds.push(rowId);
                }
                else {
                    // console.log("rowId : " + rowId + "  START : " + rowspanIds[0] + " : , END : "+ rowspanIds.length );
                    //그리드 병합
                    _gridObj.setRowspan(rowspanIds[0], colNum, rowspanIds.length);

                    //그리드를 합친 이후 초기화. 현재 rowId부터 다시 시작.
                    rowspanIds = [];
                    rowspanIds.push(rowId);
                }
            }
            else {
                if(GridGetValueByRowId(_gridObj, _colName, rowId - 1 ) == GridGetValueByRowId(_gridObj, _colName, rowId)) {
                    rowspanIds.push(rowId);
                }
                else {
                    // console.log("rowId : " + rowId + "  START : " + rowspanIds[0] + " : , END : "+ rowspanIds.length );
                    //그리드 병합
                    _gridObj.setRowspan(rowspanIds[0], colNum, rowspanIds.length);

                    //그리드를 합친 이후 초기화. 현재 rowId부터 다시 시작.
                    rowspanIds = [];
                    rowspanIds.push(rowId);
                }
            }

            //마지막 로우는 다음 비교대상이 없기때문에 위의 else 구문을 탈수 없다
            if(rowId == _gridObj.getRowsNum()) {
                _gridObj.setRowspan(rowspanIds[0], colNum, rowspanIds.length);
            }
        }
    });
    rowspanIds = [];
}

/**
 * 그리드 로우 합치기(ColNum).
 * 컬럼(세로열)의 value와 keyColName의 value가 같으면 병합
 * 체크박스는 0번째 컬럼에 위치해야합니다.
 * @param 필수, 그리드 객체
 * @param 필수, 키로 체크할 colName
 * @param 선택, 병합하려는 colNum(Default = 0)
 * @param 선택, 체크박스 유무
 */
function  GridSetColnumRowspan(_gridObj, _keyColName, _colNum, _isCheckBox) {
    var rowspanIds = new Array();
    if(_colNum == undefined)
    {
        _colNum = 0;
    }
    if(_isCheckBox == undefined)
    {
        _isCheckBox = false;
    }

    _gridObj.forEachRow(function(rowId) {
        if(rowId == 1 )
        {
            rowspanIds.push(rowId);
        }
        if(rowId >= 2 ) { //첫번째 열은 비교 대상이 없다.
            if(_isCheckBox == true)
            {
                if($(_gridObj.cells(rowId -1, _colNum).cell).html() == $(_gridObj.cells(rowId, _colNum).cell).html()
                    && GridGetValueByRowId(_gridObj, _keyColName, rowId - 1 )  == GridGetValueByRowId(_gridObj, _keyColName, rowId)
                ) {
                    rowspanIds.push(rowId);
                }
                else {
                    _gridObj.setRowspan(rowspanIds[0], _colNum, rowspanIds.length);

                    rowspanIds = [];
                    rowspanIds.push(rowId);
                }
            }
            else {
                if(GridGetValueByRowId(_gridObj, _keyColName, rowId - 1 )  == GridGetValueByRowId(_gridObj, _keyColName, rowId)
                ) {
                    rowspanIds.push(rowId);
                }
                else {
                    _gridObj.setRowspan(rowspanIds[0], _colNum, rowspanIds.length);

                    rowspanIds = [];
                    rowspanIds.push(rowId);
                }
            }

            if(rowId == _gridObj.getRowsNum()) {
                _gridObj.setRowspan(rowspanIds[0], _colNum, rowspanIds.length);
            }
        }
    });
    rowspanIds = [];
}

/**
 * 병합된 체크박스 rowID 가져오기
 * 체크박스는 0번째 컬럼에 위치해야합니다.
 * dhtmlx.js getCheckedRows 함수 변형했습니다
 * @param 필수, 그리드 객체
 * @param 필수, 체크할 colNum
 */
function  GridGetMergedRowId(_gridObj, _colNum) {
    var checkedRowids = new Array();
    _gridObj.forEachRowA(function (rowId) {
        var cellObject = _gridObj.cells(rowId, _colNum);
        if (cellObject.changeState && cellObject.getValue() == 1) {
            checkedRowids.push(rowId);
        }
    }, true);

    return checkedRowids.join(",");
}

/**
 * 병합된 라디오 버튼 클릭시 그리드 컬럼 값 가져오기
 * 라디오버튼은 0번째 컬럼에 위치해야합니다.
 * @param 필수, 그리드 객체
 * @param 필수, 선택한 rowID
 * @param 필수, 선택한 그리드 이름
 * @param 필수, 콜백함수 이름
 */
function GridGetCheckedRowAllValueRadio(_gridObj, _rowID, _gridName, _callback) {

    _gridObj.forEachRow(function(id) {
        //라디오버튼 이벤트를 다시 넣어야함
        var attachEvent = $(_gridObj.cells(id, 0).cell).children("img").attr("onclick") + "";
        if(attachEvent.indexOf("GridGetCheckedRowAllValueRadio", 0) == -1)
        {
            attachEvent = attachEvent + " GridGetCheckedRowAllValueRadio("+_gridName+", "+id+", '"+_gridName+"' , "+ _callback+");";
            $(_gridObj.cells(id, 0).cell).children("img").attr("onclick", attachEvent);
        }
    });

    //배열은 항상 초기화
    var checkedRowAllValue = new Array();
    checkedRowAllValue = GridGetAllValueByRowId(_gridObj, _rowID);
    _callback(checkedRowAllValue);

}

/**
 * 병합된 로우 선택
 * _gridObj.enableMultiselect(true) 설정 필수
 * @param 필수, 그리드 객체
 * @param 필수, 선택한 rowID
 * @param 필수, 키로 체크할 colName
 */
function GridSetSelectGroupRow(_gridObj , _rowID, _colName) {
    var selectValue = GridGetValueByRowId(_gridObj, _colName , _rowID);
    var colNum = GridGetColNumByColName(_gridObj , _colName);

    //selectValue colNum으로 같은 값을 가진 rowId 구하기
    var searchResult = _gridObj.findCell(selectValue, colNum , false , true);
    //rowId로 선택하기.
    for(var i = 0; i < searchResult.length; i++) {
        _gridObj.selectRowById(searchResult[i][0] , true);
    }
}

/**
 * 사용자 정의 타입 셋팅 함수
 * @param String : 함수명
 * @param function : 셋팅 함수
 */
function GridSetCustomColumnTypes(_funcName, _setValueFunc) {
    window["eXcell_"+_funcName] = function (cell){
        if (cell){                  // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}   // read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            _setValueFunc(this, val);
        }
    }
}