//그리드 row 이동 _ctrl (firstTop: 맨위로, top: 위로, bottom: 아래로, lastBottom: 맨아래로)
function realGridMoveRow(_gridObj, _ctrl, rowArr) {
    const lastRowId = _gridObj.gridView.getItemCount() - 1;

    switch (_ctrl) {
        case "firstTop" : //맨위로 이동
            for (let i in rowArr.reverse()) {
                let dfNum = 0;

                _gridObj.dataProvider.moveRow(rowArr[i] + parseInt(i) + dfNum, dfNum);

                if (_gridObj.gridView.isCheckedRow(i)) {
                    dfNum++;
                }
            }

            _gridObj.gridView.checkRows(rowArr, false);

            for (let i in rowArr) {
                _gridObj.gridView.checkRow(i);
            }
            break;
        case "top" : //위로 이동
            for (let i in rowArr) {
                if(rowArr[i] > 0 && !_gridObj.gridView.isCheckedRow(rowArr[i]-1)) {
                    _gridObj.dataProvider.moveRow(rowArr[i], rowArr[i]-1);
                    _gridObj.gridView.checkRow(rowArr[i]-1);
                    _gridObj.gridView.checkRow(rowArr[i], false);
                }
            }
            break;
        case "bottom" : //아래로 이동
            for (let i in rowArr.reverse()) {
                if(rowArr[i] < lastRowId && !_gridObj.gridView.isCheckedRow(rowArr[i]+1)) {
                    _gridObj.dataProvider.moveRow(rowArr[i], rowArr[i]+1);
                    _gridObj.gridView.checkRow(rowArr[i]+1);
                    _gridObj.gridView.checkRow(rowArr[i], false);
                }
            }
            break;
        case "lastBottom" : //맨아래로 이동
            for (let i in rowArr) {
                let dfNum = 0;

                _gridObj.dataProvider.moveRow(rowArr[i] - parseInt(i) - dfNum, lastRowId);

                if (_gridObj.gridView.isCheckedRow(i)) {
                    dfNum++;
                }
            }

            _gridObj.gridView.checkRows(rowArr, false);

            let checkIdx = lastRowId;
            for (let i in rowArr) {
                _gridObj.gridView.checkRow(checkIdx--);
            }

            break;
    }
}

/**
 * RealGrid 컬럼에 Lookup Option 추가 - select box version
 * - Lookup 이란? 컬럼에 연결된 데이터 필드의 실제 값 대신 그 값과 연관된 다른 값을 셀에 표시 하는 Option.
 * - 코드값으로 저장된 데이터를 그리드에 노출 시 코드명으로 표시 할 수 있음.
 * - but, 해당 코드값을 가지고 있는 select box가 존재해야 함.
 *
 * @param _gridObj : {} 그리드 객체
 * @param _columnNm : string
 * @param _codeObj : select box
 */
function setColumnLookupOption(_gridObj, _columnNm, _codeObj) {
    let values = new Array();
    let labels = new Array();

    _codeObj.find("option").each(function () {
        values.push(this.value);
        labels.push(this.text);
    });

    if (values.length > 0 && labels.length > 0) {
        let columnLookup = _gridObj.columnByName(_columnNm);
        columnLookup.values = values;
        columnLookup.labels = labels;
        columnLookup.lookupDisplay = true;

        _gridObj.setColumn(columnLookup);
    }
}

/**
 * RealGrid 컬럼에 Lookup Option 추가 2 - jsonObject Data version
 * @param _gridObj
 * @param _columnNm
 * @param _jsonData
 */
function setColumnLookupOptionForJson(_gridObj, _columnNm, _jsonData) {
    let values = new Array();
    let labels = new Array();

    $.each(_jsonData, function(key, code) {
        values.push(code.mcCd);
        labels.push(code.mcNm);
    });

    if (values.length > 0 && labels.length > 0) {
        let columnLookup = _gridObj.columnByName(_columnNm);
        columnLookup.values = values;
        columnLookup.labels = labels;
        columnLookup.lookupDisplay = true;

        _gridObj.setColumn(columnLookup);
    }
}

function setRealGridCellButton (_gridView, _buttonInfo, _columnName) {
    //버튼을 언제나 보이도록 설정.
    _gridView.setColumnProperty(_columnName, "buttonVisibility", "always");
    //버튼을 사용가능하도록 컬럼 정보를 변경
    _gridView.setColumnProperty(_columnName, "button", "image");

    //버튼 renderer생성정보
    var buttonImage = {};
    //버튼에 대한 다이나믹스타일 정보
    var buttonDynamicStyleInfo = {};
    buttonDynamicStyleInfo.criteria = [];
    buttonDynamicStyleInfo.styles = [];

    //버튼 생성 정보가 버튼 조합 수 보다 적은 경우 오류
    if(_buttonInfo.buttonCnt != _buttonInfo.buttonCombine.length){
        alert("버튼생성이 잘못되었습니다. 관리자에게 문의하세요.");
        return false;
    }

    //버튼 생성 수 만큼 생성한다.
    for(var i = 0; i < _buttonInfo.buttonCnt; i++){
        //버튼ID
        buttonImage.id = (_buttonInfo.buttonName + (i+1));
        //버튼타입
        buttonImage.type = "imageButtons";
        //버튼 이미지 Gap
        buttonImage.imageGap = _buttonInfo.buttonGap;
        //버튼 정렬
        buttonImage.alignment = _buttonInfo.cellTextAlignment != null ? _buttonInfo.cellTextAlignment : "center";
        //버튼 마진정보
        buttonImage.margin = _buttonInfo.buttonMargin;
        //버튼 이미지 정보
        buttonImage.images = [];
        //버튼 조합만큼 이미지 정보를 생성.
        $.each(_buttonInfo.buttonCombine[i].split(","), function (combineIndex, combineData) {
            //버튼 이미지 정보와 조합정보를 비교하여 맞는 이미지번튼을 생성하도록 한다.
            $.each(_buttonInfo.buttonImg, function (imgIndex, imgData) {
                if(combineData == imgData.name){
                    var img = {};
                    img.name = imgData.name;
                    img.up = imgData.img;
                    img.hover = imgData.img.split(".").join("_hover.");
                    img.down = imgData.img.split(".").join("_active.");
                    img.width = imgData.width;
                    img.cursor = "pointer";
                    buttonImage.images.push(img);
                }
            });
        });
        //생성된 이미지버튼을 Renderer에 등록한다.
        _gridView.addCellRenderers(buttonImage);
        buttonDynamicStyleInfo.criteria.push("value" + (_buttonInfo.isValueNotOption ? "!=" : "=") + "'" + _buttonInfo.value[i] + "'");
        buttonDynamicStyleInfo.styles.push("renderer=" + buttonImage.id);
        //버튼 정보 초기화.
        buttonImage = {};
    }
    //버튼에 대한 다이나믹스타일 적용
    _gridView.setColumnProperty(_columnName, "dynamicStyles", [buttonDynamicStyleInfo]);
}