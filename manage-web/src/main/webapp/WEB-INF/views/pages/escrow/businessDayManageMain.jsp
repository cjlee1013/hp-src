<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel='stylesheet' type='text/css' href='/static/css/calandar.css'>
<style>
    .ui.table.calendar-mode div.businessDayType {
        position: absolute;top: 0; right: 0; font-size: 11px;
    }
</style>

<div class="escrow-main">
    <!-- 상단 타이틀 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">영업일 관리</h2>
    </div>

    <!-- 영업일 캘린더 -->
    <div>
        <div style="text-align: center;">
            <div class="ui form inline" style="display: inline-block;">
                <button id="prevYear" class="ui button small mint mg-r-5">이전 년도</button>
                <button id="prevMonth" class="ui button small mint mg-r-5">이전 달</button>
            </div>
            <span style="position: relative;top: 6px;margin: 2px 20px;font-size: 18px;font-weight: bold;">
          <span id="curYear"></span>년 <span id="curMonth"> </span>월
        </span>
            <div class="ui form inline" style="display: inline-block;">
                <button id="nextMonth" class="ui button small mint mg-r-5">다음 달</button>
                <button id="nextYear" class="ui button small mint mg-r-5">다음 년도</button>
            </div>
        </div>
        <div class="com wrap-title">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table calendar-mode" id="businessCalendar">
                    <caption>영업일 관리</caption>
                    <thead>
                    <tr>
                        <th class="text-center">월</th>
                        <th class="text-center">화</th>
                        <th class="text-center">수</th>
                        <th class="text-center">목</th>
                        <th class="text-center">금</th>
                        <th class="text-center"><span class="text-red">토</span></th>
                        <th class="text-center"><span class="text-red">일</span></th>
                    </tr>
                    </thead>
                    <tbody><!-- 캘린더 동적 생성영역... --></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- 하단 영업일 정보 -->
    <form id="businessDayManageForm" name="businessDayManageForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3 class="title">영업일 정보</h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>영업일 정보</caption>
                <tbody>
                <tr>
                    <th>영업일&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="businessDay" name="businessDay" class="ui input medium" readonly/>
                            <span style="font-size: 11px;vertical-align: middle;margin-left: 33px;color: #999;"> * 상단 캘린더에서 일자를 선택 해주세요.</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>일정유형</th>
                    <td>
                        <div class="ui form inline">
                            <label class="ui checkbox">
                                <input type="checkbox" id="holidayYn" name="holidayYn">
                                <span class="text mg-r-20">비영업일</span>
                            </label>
                            <label class="ui checkbox">
                                <input type="checkbox" id="nonDeliveryYn" name="nonDeliveryYn">
                                <span class="text mg-r-20">배송지연</span>
                            </label>
                            <label class="ui checkbox">
                                <input type="checkbox" id="nonSettleYn" name="nonSettleYn">
                                <span class="text mg-r-20">정산불가</span>
                            </label>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

<!-- 하단 버튼영역 (저장/초기화) -->
<div class="ui center-button-group mg-t-15 ">
    <span class="inner">
        <button id="saveBtn" style="width:60px;" class="ui button xlarge btn-danger">저장</button>
        <button id="resetBtn" style="width:60px;" class="ui button xlarge">초기화</button>
    </span>
</div>

<script>
    $(document).ready(function () {
        businessDayManageMain.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/escrow/businessDayManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>