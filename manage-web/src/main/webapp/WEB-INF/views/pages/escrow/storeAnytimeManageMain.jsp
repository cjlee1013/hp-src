<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="storeAnytimeManageSearchForm" name="storeAnytimeManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">점포 운영 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>점포 운영 관리</caption>
                    <tbody>
                    <tr>
                        <th>점포유형&nbsp;<span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                                    <option value="HYPER">HYPER</option>
                                </select>
                            </div>
                        </td>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10">
                                    <option value="NAME">점포명</option>
                                    <option value="ID">점포코드</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" value="">
                            </div>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="button" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 메인 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="storeAnytimeManageSearchCnt">0</span> 건</h3>
        </div>
        <div class="topline"></div>
        <div id="storeAnytimeManageGrid" style="width: 100%; height: 290px;"></div>
    </div>

    <!-- Shift 그리드 영역 -->
    <div class="mg-t-50"></div>
    <div>
        <select id="anytimeUseYnLookup" style="display: none;">
            <option value="Y">사용</option>
            <option value="N">사용안함</option>
        </select>
        <form id="storeAnytimeInputForm" name="storeAnytimeInputForm" onsubmit="return false;">
            <input type="hidden" id="storeId" name="storeId" value="">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>점포/Shift 애니타임 상세내역</caption>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <span class="text" id="storeNm"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>애니타임 사용여부</th>
                        <td>
                            <label class="ui radio small inline"><input type="radio" name="storeAnytimeUseYn" id="storeAnytimeUseY" value="Y"><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="storeAnytimeUseYn" id="storeAnytimeUseN" value="N"><span>사용안함</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="7">사용 Shift</th>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>일</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_110" name="shift_110" class="shiftUse" value="110"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_120" name="shift_120" class="shiftUse" value="120"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_130" name="shift_130" class="shiftUse" value="130"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_140" name="shift_140" class="shiftUse" value="140"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_150" name="shift_150" class="shiftUse" value="150"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_160" name="shift_160" class="shiftUse" value="160"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_170" name="shift_170" class="shiftUse" value="170"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_180" name="shift_180" class="shiftUse" value="180"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_190" name="shift_190" class="shiftUse" value="190"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>월</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_210" name="shift_210" class="shiftUse" value="210"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_220" name="shift_220" class="shiftUse" value="220"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_230" name="shift_230" class="shiftUse" value="230"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_240" name="shift_240" class="shiftUse" value="240"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_250" name="shift_250" class="shiftUse" value="250"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_260" name="shift_260" class="shiftUse" value="260"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_270" name="shift_270" class="shiftUse" value="270"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_280" name="shift_280" class="shiftUse" value="280"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_290" name="shift_290" class="shiftUse" value="290"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>화</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_310" name="shift_310" class="shiftUse" value="310"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_320" name="shift_320" class="shiftUse" value="320"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_330" name="shift_330" class="shiftUse" value="330"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_340" name="shift_340" class="shiftUse" value="340"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_350" name="shift_350" class="shiftUse" value="350"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_360" name="shift_360" class="shiftUse" value="360"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_370" name="shift_370" class="shiftUse" value="370"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_380" name="shift_380" class="shiftUse" value="380"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_390" name="shift_390" class="shiftUse" value="390"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>수</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_410" name="shift_410" class="shiftUse" value="410"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_420" name="shift_420" class="shiftUse" value="420"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_430" name="shift_430" class="shiftUse" value="430"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_440" name="shift_440" class="shiftUse" value="440"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_450" name="shift_450" class="shiftUse" value="450"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_460" name="shift_460" class="shiftUse" value="460"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_470" name="shift_470" class="shiftUse" value="470"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_480" name="shift_480" class="shiftUse" value="480"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_490" name="shift_490" class="shiftUse" value="490"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>목</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_510" name="shift_510" class="shiftUse" value="510"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_520" name="shift_520" class="shiftUse" value="520"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_530" name="shift_530" class="shiftUse" value="530"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_540" name="shift_540" class="shiftUse" value="540"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_550" name="shift_550" class="shiftUse" value="550"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_560" name="shift_560" class="shiftUse" value="560"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_570" name="shift_570" class="shiftUse" value="570"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_580" name="shift_580" class="shiftUse" value="580"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_590" name="shift_590" class="shiftUse" value="590"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>금</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_610" name="shift_610" class="shiftUse" value="610"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_620" name="shift_620" class="shiftUse" value="620"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_630" name="shift_630" class="shiftUse" value="630"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_640" name="shift_640" class="shiftUse" value="640"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_650" name="shift_650" class="shiftUse" value="650"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_660" name="shift_660" class="shiftUse" value="660"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_670" name="shift_670" class="shiftUse" value="670"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_680" name="shift_680" class="shiftUse" value="680"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_690" name="shift_690" class="shiftUse" value="690"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="mg-r-20" style="display: inline-block">
                                <span>토</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_710" name="shift_710" class="shiftUse" value="710"><span>Shift1</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_720" name="shift_720" class="shiftUse" value="720"><span>Shift2</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_730" name="shift_730" class="shiftUse" value="730"><span>Shift3</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_740" name="shift_740" class="shiftUse" value="740"><span>Shift4</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_750" name="shift_750" class="shiftUse" value="750"><span>Shift5</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_760" name="shift_760" class="shiftUse" value="760"><span>Shift6</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_770" name="shift_770" class="shiftUse" value="770"><span>Shift7</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_780" name="shift_780" class="shiftUse" value="780"><span>Shift8</span>
                            </label>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id="shift_790" name="shift_790" class="shiftUse" value="790"><span>Shift9</span>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge cyan" id="saveBtn">저장</button>
            <button class="ui button xlarge" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${storeAnytimeManageGridBaseInfo}
    $(document).ready(function() {
        storeAnytimeMng.init();
        storeAnytimeManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/escrow/storeAnytimeManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
