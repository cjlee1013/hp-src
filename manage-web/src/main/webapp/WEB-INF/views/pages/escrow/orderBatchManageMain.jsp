<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 마스터 검색영역 -->
    <div>
        <form id="orderBatchManageSearchForm" name="orderBatchManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">주문 배치 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>주문배치관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>배치명</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" >
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="float:left;">
                                <option value="">전체</option>
                                <option value="Y">사용중</option>
                                <option value="N">미사용</option>
                            </select>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 마스터 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="orderBatchManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="orderBatchManageGrid" style="height: 320px;"></div>
    </div>

    <!-- 마스터 상세영역 -->
    <form id="orderBatchManageInputForm" name="orderBatchManageInputForm" onsubmit="return false;">
    <div class="com wrap-title sub"></div>
    <div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>주문배치관리 상세정보</caption>
                <tbody>
                <tr>
                    <th>배치번호</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="batchNo" name="batchNo" class="ui input medium mg-r-5" disabled="true" >
                        </div>
                    </td>
                    <th>배치타입&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="batchType" name="batchType" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>배치명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="batchNm" name="batchNm" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                    <th>배치주기&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="batchTerm" name="batchTerm" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>설명</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="description" name="description" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                    </td>
                </tr>
                <tr>
                    <th>추가설정값1</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="metaField1" name="metaField1" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                    <th>추가설정값2</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="metaField2" name="metaField2" class="ui input medium mg-r-5" maxlength="50" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge cyan" id="saveBtn">저장</button>
            <button class="ui button xlarge" id="resetBtn">초기화</button>
            <button class="ui button xlarge gray-dark" id="deleteBtn">삭제</button>
        </span>
    </div>

    <!-- 히스토리 검색영역 -->
    <div id="historySearchDiv">
        <form id="orderBatchHistorySearchForm" name="orderBatchHistorySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">배치 히스토리 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>주문배치이력 검색</caption>
                    <tbody>
                    <tr>
                        <th>배치번호</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="historyBatchNo" name="historyBatchNo" class="ui input medium mg-r-5" disabled="true" >
                            </div>
                        </td>
                        <th>배치명</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="historyBatchNm" name="historyBatchNm" class="ui input medium mg-r-5" disabled="true" >
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="button" id="searchHistoryBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="조회시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="조회종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 히스토리 그리드 영역 -->
    <div id="historyGridDiv">
        <div class="com wrap-title sub" style="margin-top: 30px;"></div>
        <div class="topline"></div>
        <div id="orderBatchHistoryGrid" style="height: 320px;"></div>
    </div>
</div>

<script>
    const empId = '${empId}';
    ${orderBatchManageGridBaseInfo}
    ${orderBatchHistoryGridBaseInfo}
    $(document).ready(function () {
        orderBatchManageMng.init();
        orderBatchManageGrid.init();
        orderBatchHistoryGrid.init();
        orderBatchHistoryMng.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/escrow/orderBatchManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>