<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="cartCouponUseOrderExtractSearchForm" name="cartCouponUseOrderExtractSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">장바구니 쿠폰 사용 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>장바구니 쿠폰 사용 조회</caption>
                    <tbody>
                    <tr>
                        <th>주문일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>쿠폰번호</th>
                        <td>
                            <textarea id="schAddParamList" name="schAddParamList" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="쿠폰번호 복수 검색 시 쉼표(,)로 구분 &#13;&#10;(최대 10개 까지 가능)" ></textarea>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>주문번호</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schPurchaseOrderNo" name="schPurchaseOrderNo" class="ui input medium mg-r-5">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과</h3>
        </div>
        <div class="topline"></div>
        <div id="cartCouponUseOrderExtractGrid" style="width: 100%; height: 330px;"></div>
    </div>
</div>

<script>
    ${cartCouponUseOrderExtractGridBaseInfo}
    var excelDownloadAuth = '${excelDownloadAuth}';
    $(document).ready(function() {
        cartCouponUseOrderExtractMng.init();
        cartCouponUseOrderExtractGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/escrow/cartCouponUseOrderExtractMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
