<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="zipcodeManageSearchForm" name="zipcodeManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">우편번호 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>우편번호 관리</caption>
                    <tbody>
                    <tr>
                        <th>도서산간</th>
                        <td>
                            <select id="schIslandType" name="schIslandType" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="JEJU">제주권</option>
                                <option value="ISLAND">도서산간</option>
                                <option value="ALL">해당없음</option>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <!-- 검색키워드 타입 SelectBox -->
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" onchange="zipcodeManageMain.changeKeywordType();">
                                    <option value="ROADADDR">도로명</option>
                                    <option value="GIBUN">지번</option>
                                    <option value="ZIPCODE" selected>우편번호</option>
                                    <option value="BUILD_MNG_NO">건물 관리번호</option>
                                </select>

                                <!-- 검색키워드 '도로명'에 따른 Sub SelectBox -->
                                <select id="schSidoType" name="schSidoType" class="ui input medium mg-r-10" style="display:none;" onchange="zipcodeManageMain.getSigunguList();">
                                    <option value="">시/도 선택</option>
                                    <c:forEach var="sido" items="${zipcodeSidoList}">
                                        <option value="${sido}">${sido}</option>
                                    </c:forEach>
                                </select>
                                <select id="schSigunguType" name="schSigunguType" class="ui input medium mg-r-10" style="display:none;">
                                    <option value="">시군구 선택</option>
                                </select>

                                <div id="schInputBox" name="schInputBox" style="display: inline">
                                    <!-- 검색키워드 InputBox -->
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input medium" maxlength="100">
                                    <!-- 도로명주소 건물번호 InputBox -->
                                    <input type="text" id="schBuildNo" name="schBuildNo" class="ui input mg-l-10" style="display: none" maxlength="50" placeholder="건물번호">
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="zipcodeManageSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button type="button" id="chgJejuBtn" class="ui button medium sky-blue">제주권변경</button>
                <button type="button" id="chgIslandBtn" class="ui button medium mint">도서산간변경</button>
                <button type="button" id="chgNoneBtn" class="ui button medium btn-outline-info">해당없음변경</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="zipcodeManageGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${zipcodeManageGridBaseInfo}
    $(document).ready(function() {
        zipcodeManageMain.init();
        zipcodeManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/escrow/zipcodeManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>