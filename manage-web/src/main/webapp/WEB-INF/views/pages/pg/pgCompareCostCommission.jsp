<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">PG 수수료대사 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>PG 수수료대사 조회</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="dateType" name="dateType" style="width:150px;float:left;">
                                    <option value="basicDt">거래일</option>
                                    <option value="payScheduleDt">지급일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareCostCommissionForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareCostCommissionForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareCostCommissionForm.initSearchDate('-31d');">1개월전</button>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>PG사</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="pgKind" name="pgKind" style="width:150px;float:left;">
                                <option value="INICIS">INICIS</option>
                                <option value="TOSSPG">TOSSPG</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>차이구분</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="flagType" name="flagType" style="width:150px;float:left;">
                                <option value="amount">금액차이</option>
                                <option value="balance">잔액차이</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadBtn">엑셀다운로드</button>
            </div>
            <div class="mg-b-20 " id="pgCompareCostCommissionGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pgCompareCostCommissionList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${pgCompareCostCommissionBaseInfo}

    $(document).ready(function() {
        pgCompareCostCommissionForm.init();
        pgCompareCostCommissionGrid.init();
    });
</script>