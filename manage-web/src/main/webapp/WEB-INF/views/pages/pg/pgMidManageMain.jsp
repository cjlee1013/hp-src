<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="pgMidManageSearchForm" name="pgMidManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">PG상점ID 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>PG상점ID 검색</caption>
                    <tbody>
                    <tr>
                        <th>PG사</th>
                        <td>
                            <select id="schPgKind" name="schPgKind" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="pgKind" items="${pgKindList}">
                                    <option value="${pgKind.pgCd}">${pgKind.pgNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td/>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="pgMidManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="pgMidManageGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="pgMidManageForm" name="pgMidManageForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <tbody>
                    <tr>
                        <th>
                            PG&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="pgKind" name="pgKind" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="pgKind" items="${pgKindList}">
                                        <option value="${pgKind.pgCd}">${pgKind.pgNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                        </td>
                        <th>
                            노출순서&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <input type="text" id="dispSeq" name="dispSeq" class="ui input medium mg-r-5" maxlength="2">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            상점ID&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <input type="text" name="pgMid" id="pgMid" class="ui input medium" maxlength="15">
                        </td>
                        <th>사용목적</th>
                        <td>
                            <div class="ui form inline">
                                <select id="usePurpose" name="usePurpose" class="ui input medium mg-r-10">
                                <c:forEach var="pgMidUsePurpose" items="${pgMidUsePurposeList}">
                                    <option value="${pgMidUsePurpose.mcCd}">${pgMidUsePurpose.mcNm}</option>
                                </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>간편결제</th>
                        <td>
                            <div class="ui form inline">
                                <select id="easyPayTarget" name="easyPayTarget" class="ui input medium mg-r-10">
                                    <option value="">선택안함</option>
                                    <c:forEach var="easyPayTarget" items="${easyPayTargetList}">
                                        <option value="${easyPayTarget.methodCd}">${easyPayTarget.methodNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트 Key</th>
                        <td colspan="3">
                            <input type="text" name="siteKey" id="siteKey" class="ui input medium" maxlength="100">
                        </td>
                        <th>무이자사용여부</th>
                        <td colspan="2">
                            <label class="ui radio small inline"><input type="radio" name="freeInterestYn" id="freeInterestY" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="freeInterestYn" id="freeInterestN" value="N"><span>사용안함
                            </span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>비고</th>
                        <td colspan="5">
                            <input type="text" name="note" id="note" class="ui input medium" maxlength="100">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <colgroup>
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="addPgMidManageBtn">저장</button>
                    <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${pgMidManageGridBaseInfo}
    $(document).ready(function() {
        pgMidManageMain.init();
        pgMidManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pgMidManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
