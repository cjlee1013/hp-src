<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="pgCommissionSearchForm" name="pgCommissionSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">PG 수수료 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>PG 수수료 관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>적용기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>PG</th>
                        <td>
                            <select id="schPgKind" name="schPgKind" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="pgKind" items="${pgKindList}">
                                <option value="${pgKind.pgCd}">${pgKind.pgNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>결제수단</th>
                        <td>
                            <select id="schParentMethodCd" name="schParentMethodCd" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                                    <option value="${parentMethodCd.methodCd}">${parentMethodCd.methodNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>제목</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td colspan="3">
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="pgCommissionSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="pgCommissionManageGrid" style="height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="pgCommissionInputForm" name="pgCommissionInputForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">기본정보</h3>
    </div>
    <div class="topline"></div>
    <div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>PG 수수료 상세내역</caption>
                <tbody>
                <tr>
                    <th>번호</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="pgCommissionMngSeq" name="pgCommissionMngSeq" class="ui input medium mg-r-5" disabled="true">
                        </div>
                    </td>
                    <th>사용여부</th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                    </td>
                </tr>
                <tr>
                    <th>제목&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="text" id="commissionMngNm" name="commissionMngNm" class="ui input medium mg-r-5" maxlength="20" >
<%--                            <span class="text">( <span id="textCountCommissionMngNm">0</span> / 20자 )</span>--%>
                        </span>
                    </td>
                    <th>PG/결제수단</th>
                    <td>
                        <span class="ui form inline">
                            <select id="pgKind" name="pgKind" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="pgKind" items="${pgKindList}">
                                    <option value="${pgKind.pgCd}">${pgKind.pgNm}</option>
                                </c:forEach>
                            </select>
                            <select id="parentMethodCd" name="parentMethodCd" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                                    <option value="${parentMethodCd.methodCd}">${parentMethodCd.methodNm}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>기간&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5" placeholder="시작일">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5" placeholder="종료일">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                    <th>정산주기</th>
                    <td>
                        <div class="ui form inline">
                            <span class="ui form inline">
                                <select id="costPeriod" name="costPeriod" class="ui input medium mg-r-10">
                                    <option value="DAY">일정산</option>
                                    <option value="WEEK">주정산</option>
                                    <option value="EARLY_MONTH">익월초</option>
                                    <option value="END_MONTH">익월말</option>
                                    <option value="END_NEXT_MONTH">익익월말</option>
                                </select>
                                <input type="text" id="costDay" name="costDay" class="ui input medium mg-r-5" maxlength="2" disabled="true">
                                <span class="text">일</span>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>소수점처리</th>
                    <td>
                        <select id="decpntProcess" name="decpntProcess" class="ui input medium mg-r-10">
                            <option value="UP">올림</option>
                            <option value="DOWN">버림</option>
                            <option value="ROUND">반올림</option>
                        </select>
                    </td>
                    <th>VAT포함여부</th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="vatYn" id="vatY" value="Y" checked><span>포함</span></label>
                        <label class="ui radio small inline"><input type="radio" name="vatYn" id="vatN" value="N"><span>포함안함</span></label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </form>

    <!-- 상세영역 2 -->
    <div class="com wrap-title sub">
        <h3 class="title">결제수단별</h3>
    </div>
    <div class="topline"></div>
    <div>
        <form id="pgCommissionTargetForm" name="pgCommissionTargetForm" onsubmit="return false;">
            <div id="CARDDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                    <c:forEach var="cardPaymentMethod" varStatus="status" items="${cardPaymentMethodList}">
                        <c:if test="${status.index %2 == 0}">
                            <c:set var="left" value="${cardPaymentMethod}"/>
                            <c:set var="right" value="${cardPaymentMethodList[status.index+1]}"/>
                        <td rowspan="2">${left.methodNm}</td>
                        <td>
                            <span>신용</span>
                        </td>
                        <td>
                            <input type="text" id="${left.methodCd}CreditRate" name="${left.methodCd}List[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="${left.methodCd}PgCommissionTargetSeq" name="${left.methodCd}List[].pgCommissionTargetSeq" value=""/>
                        </td>
                        <td rowspan="2">${right.methodNm}</td>
                        <td>
                            <span>신용</span>
                        </td>
                        <td>
                            <input type="text" id="${right.methodCd}CreditRate" name="${right.methodCd}List[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="${right.methodCd}PgCommissionTargetSeq" name="${right.methodCd}List[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>체크</span>
                        </td>
                        <td>
                            <input type="text" id="${left.methodCd}DebitRate" name="${left.methodCd}List[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                        <td>
                            <span>체크</span>
                        </td>
                        <td>
                            <input type="text" id="${right.methodCd}DebitRate" name="${right.methodCd}List[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div id="KAKAOPAYDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td>카카오페이</td>
                        <td>
                            <span>카드</span>
                        </td>
                        <td>
                            <input type="text" id="KAKAOPAYCreditRate" name="KAKAOPAYList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="KAKAOPAYPgCommissionTargetSeq" name="KAKAOPAYList[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="KAKAOMONEYDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td>카카오머니</td>
                        <td>
                            <span>머니</span>
                        </td>
                        <td>
                            <input type="text" id="KAKAOMONEYDebitRate" name="KAKAOMONEYList[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="KAKAOMONEYPgCommissionTargetSeq" name="KAKAOMONEYList[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="NAVERPAYDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="2">네이버페이</td>
                        <td>
                            <span>카드</span>
                        </td>
                        <td>
                            <input type="text" id="NAVERPAYCreditRate" name="NAVERPAYList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="NAVERPAYPgCommissionTargetSeq" name="NAVERPAYList[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>머니</span>
                        </td>
                        <td>
                            <input type="text" id="NAVERPAYDebitRate" name="NAVERPAYList[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="SAMSUNGPAYDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="2">삼성페이</td>
                        <td>
                            <span>카드</span>
                        </td>
                        <td>
                            <input type="text" id="SAMSUNGPAYCreditRate" name="SAMSUNGPAYList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                            <input type="hidden" id="SAMSUNGPAYPgCommissionTargetSeq" name="SAMSUNGPAYList[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>머니</span>
                        </td>
                        <td>
                            <input type="text" id="SAMSUNGPAYDebitRate" name="SAMSUNGPAYList[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="RBANKDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="4">실시간계좌이체</td>
                        <td>
                            <span>최저수수료</span>
                        </td>
                        <td>
                            <input type="text" id="RBANKLowCommissionAmt" name="RBANKList[].rbankLowCommissionAmt" class="ui input medium mg-r-5" style="align:right; display: inline;" value="0">
                            <span>원</span>
                            <input type="hidden" id="RBANKPgCommissionTargetSeq" name="RBANKList[].pgCommissionTargetSeq" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span id="rbankCreditRateTitle">일반수수료</span>
                        </td>
                        <td>
                            <input type="text" id="RBANKCreditRate" name="RBANKList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    <tr id="rbankOverFstAmtRow">
                        <td>
                            <span>10만원초과</span>
                        </td>
                        <td>
                            <input type="text" id="RBANKOverFstAmt" name="RBANKList[].tossRbankFstCommissionAmt" class="ui input medium mg-r-5" style="align:right; display: inline;" value="0">
                            <span>원</span>
                        </td>
                    </tr>
                    <tr id="rbankOverNextAmtRow">
                        <td>
                            <span>100만원초과</span>
                        </td>
                        <td>
                            <input type="text" id="RBANKOverNextAmt" name="RBANKList[].tossRbankNextCommissionAmt" class="ui input medium mg-r-5" style="align:right; display: inline;" value="0">
                            <span>원</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="PHONEDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="2">휴대폰</td>
                        <td>
                            <span>환불수수료</span>
                        </td>
                        <td>
                            <input type="text" id="PHONECommonAmt" class="ui input medium mg-r-5" style="align:right; display: inline;" value="0">
                            <span>원</span>
                            <c:forEach var="phonePaymentMethod" varStatus="status" items="${phonePaymentMethodList}">
                                <input type="hidden" id="${phonePaymentMethod.methodCd}CreditRate" name="${phonePaymentMethod.methodCd}List[].creditCommissionRate" value="">
                                <input type="hidden" id="${phonePaymentMethod.methodCd}RefundAmt" name="${phonePaymentMethod.methodCd}List[].mobileRefundCommissionAmt" value="">
                                <input type="hidden" id="${phonePaymentMethod.methodCd}PgCommissionTargetSeq" name="${phonePaymentMethod.methodCd}List[].pgCommissionTargetSeq" value=""/>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>결제수수료</span>
                        </td>
                        <td>
                            <input type="text" id="PHONECommonRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="TOSSDiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="2">토스</td>
                        <td>
                            <span>카드</span>
                        </td>
                        <td>
                            <input type="text" id="TOSSCreditRate" name="TOSSList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <input type="hidden" id="TOSSPgCommissionTargetSeq" name="TOSSList[].pgCommissionTargetSeq" value=""/>
                            <span>%</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>머니</span>
                        </td>
                        <td>
                            <input type="text" id="TOSSDebitRate" name="TOSSList[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="PAYCODiv" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>상세수단</th>
                        <th colspan="2">수수료</th>
                    </tr>
                    <tr>
                        <td rowspan="2">페이코</td>
                        <td>
                            <span>카드</span>
                        </td>
                        <td>
                            <input type="text" id="PAYCOCreditRate" name="PAYCOList[].creditCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <input type="hidden" id="PAYCOPgCommissionTargetSeq" name="PAYCOList[].pgCommissionTargetSeq" value=""/>
                            <span>%</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>머니</span>
                        </td>
                        <td>
                            <input type="text" id="PAYCODebitRate" name="PAYCOList[].debitCommissionRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="6" value="0">
                            <span>%</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge cyan" id="savePgCommissionBtn">저장</button>
            <button class="ui button xlarge" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${pgCommissionManageGridBaseInfo}
    $(document).ready(function () {
        pgCommissionManageMng.init();
        pgCommissionManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pgCommissionManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>