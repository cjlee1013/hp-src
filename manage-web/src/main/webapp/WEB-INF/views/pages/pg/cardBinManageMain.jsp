<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="cardBinManageSearchForm" name="cardBinManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">카드BIN 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>카드BIN관리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="250px">
                        <col width="120px">
                        <col width="200px">
                        <col width="100px">
                        <col width="200px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카드BIN번호</th>
                        <td>
                            <div class="ui form inline">
                                <input id="schCardBinNo" name="schCardBinNo" type="text" class="ui input medium mg-r-5" maxlength="7" autocomplete="off">
                            </div>
                        </td>
                        <th>카드사</th>
                        <td>
                            <select id="schMethodCd" name="schMethodCd" class="ui input medium mg-r-10" style="width: 150px;">
                                <option value="">전체</option>
                                <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                    <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td/>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="width: 100px;">
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="cardBinManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="cardBinManageGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="cardBinManageForm" name="cardBinManageForm" onsubmit="return false;">
        <!-- cardBinNoSeq Key값 Hidden -->
        <input type="hidden" name="cardBinNoSeq" id="cardBinNoSeq" class="ui input medium" style="width: 100px;" readonly="true">
        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="250px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>
                            <div class="star-alignment">
                                <span class="text-center text-red">*</span> 카드사
                            </div>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="methodCd" name="methodCd" class="ui input medium mg-r-10" style="width: 150px;">
                                    <option value="">선택</option>
                                    <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                        <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <div class="star-alignment">
                                <span class="text-red">*</span>카드명
                            </div>
                        </th>
                        <td>
                            <input type="text" id="cardNm" name="cardNm" class="ui input medium mg-r-5" style="width: 300px;" maxlength="50">
                        </td>
                        <th>
                            <div class="star-alignment">
                                <span class="text-red">*</span>카드구분
                            </div>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="cardBinKind" name="cardBinKind" class="ui input medium mg-r-10" style="width: 150px;">
                                    <option value="">선택</option>
                                    <c:forEach var="cardBinKind" items="${commCardBinKindList}">
                                        <option value="${cardBinKind.mcCd}">${cardBinKind.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    <tr>
                        <th>카드BIN번호</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="cardBinNo" name="cardBinNo" class="ui input medium mg-r-5" style="width: 300px;" maxlength="7">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <colgroup>
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="addCardBinManageBtn">저장</button>
                    <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${cardBinManageGridBaseInfo}
    $(document).ready(function() {
        cardBinManageMain.init();
        cardBinManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/cardBinManageMain.js?v=${fileVersion}"></script>