<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="chargeDiscountBenefitManageSearchForm" name="chargeDiscountBenefitManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">청구할인 혜택 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>청구할인 혜택 관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>적용기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schApplyStartDt" name="schApplyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schApplyEndDt" name="schApplyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>사이트</th>
                        <td>
                            <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                            <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                            </c:forEach>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <th>청구할인명</th>
                        <td>
                            <span class="ui form inline">
                            <input id="schChargeDiscountNm" name="schChargeDiscountNm" type="text" class="ui input medium mg-r-5" maxlength="100">
                        </span>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="chargeDiscountBenefitManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="chargeDiscountBenefitManageGrid" style="height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="chargeDiscountBenefitManageForm" name="chargeDiscountBenefitManageForm" onsubmit="return false;">
        <input type="hidden" id="methodCd" name="methodCd" value="">
        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <tbody>
                    <tr>
                        <th>번호</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="chargeDiscountBenefitMngNo" id="chargeDiscountBenefitMngNo" class="ui input medium"
                                       maxlength="20" readonly>
                            </div>
                        </td>
                        <th>
                            사이트&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="siteType" name="siteType" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="siteType" items="${siteTypeList}">
                                        <option value="${siteType.code}">${siteType.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            청구할인명&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="chargeDiscountNm" id="chargeDiscountNm" class="ui input medium" maxlength="100">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            기간&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="applyStartDt" name="applyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="applyEndDt" name="applyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>
                            카드사&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="methodCdHOME" name="methodCdHOME" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="hmpPaymentMethod" items="${hmpPaymentMethodList}">
                                        <option value="${hmpPaymentMethod.methodCd}">${hmpPaymentMethod.methodNm}</option>
                                    </c:forEach>
                                </select>
<%--                                [ESCROW-343, CLUB 노출제외]--%>
<%--                                <select id="methodCdCLUB" name="methodCdCLUB" class="ui input medium mg-r-10" style="display: none">--%>
<%--                                    <option value="">선택</option>--%>
<%--                                    <c:forEach var="clubPaymentMethod" items="${clubPaymentMethodList}">--%>
<%--                                        <option value="${clubPaymentMethod.methodCd}">${clubPaymentMethod.methodNm}</option>--%>
<%--                                    </c:forEach>--%>
<%--                                </select>--%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            할인방법&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="discountType" name="discountType" class="ui input medium mg-r-10">
                                    <option value="PRICE">정액</option>
                                    <option value="RATE">정률</option>
                                </select>
                                <span id="discountTypeRate" style="display: none">
                                    <input type="text" name="discountRate" id="discountRate" class="ui input medium" maxlength="2">
                                    <span class="text">%&nbsp;&nbsp;</span>
                                    <span class="text">최대할인금액</span>
                                        <input type="text" name="discountMaxPrice" id="discountMaxPrice" class="ui input medium" maxlength="8">
                                    <span class="text">원</span>
                                </span>
                                <span id="discountTypePrice">
                                    <input type="text" name="discountPrice" id="discountPrice" class="ui input medium" maxlength="8">
                                    <span class="text">원</span>
                                </span>
                            </div>
                        </td>
                        <th>
                            최소구매 금액&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="purchaseMinPrice" id="purchaseMinPrice" class="ui input medium" maxlength="8">
                                <span class="text">원</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="addChargeDiscountBenefitManageBtn">저장</button>
                    <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${chargeDiscountBenefitManageGridBaseInfo}
    $(document).ready(function() {
        chargeDiscountBenefitManageMain.init();
        chargeDiscountBenefitManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/chargeDiscountBenefitManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>