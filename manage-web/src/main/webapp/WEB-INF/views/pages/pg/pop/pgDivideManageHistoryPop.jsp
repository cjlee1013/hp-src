<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">변경이력</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="pgDivideHistoryGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 변경이력 팝업 그리드 기본정보
    ${pgDivideManageHistoryPopBaseInfo};

    $(document).ready(function () {
        pgDivideHistoryGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 변경이력 팝업 그리드
    var pgDivideHistoryGrid = {
        realGrid: new RealGridJS.GridView("pgDivideHistoryGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            pgDivideHistoryGrid.initGrid();
            pgDivideHistoryGrid.initDataProvider();
            pgDivideHistoryMng.search();
        },
        initGrid: function () {
            pgDivideHistoryGrid.realGrid.setDataSource(pgDivideHistoryGrid.dataProvider);
            pgDivideHistoryGrid.realGrid.setStyles(pgDivideManageHistoryPopBaseInfo.realgrid.styles);
            pgDivideHistoryGrid.realGrid.setDisplayOptions(pgDivideManageHistoryPopBaseInfo.realgrid.displayOptions);
            pgDivideHistoryGrid.realGrid.setColumns(pgDivideManageHistoryPopBaseInfo.realgrid.columns);
            pgDivideHistoryGrid.realGrid.setOptions(pgDivideManageHistoryPopBaseInfo.realgrid.options);
            pgDivideHistoryGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            pgDivideHistoryGrid.dataProvider.setFields(pgDivideManageHistoryPopBaseInfo.dataProvider.fields);
            pgDivideHistoryGrid.dataProvider.setOptions(pgDivideManageHistoryPopBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            pgDivideHistoryGrid.dataProvider.clearRows();
            pgDivideHistoryGrid.dataProvider.setRows(dataList);
        }
    };

    // 변경이력 팝업 관리
    var pgDivideHistoryMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/pg/pgDivide/getPgDivideRateHistoryList.json?parentMethodCd=${parentMethodCd}&siteType=${siteType}"
                , method: "GET"
                , callbackFunc: function (res) {
                    console.log(res);
                    pgDivideHistoryGrid.setData(res);
                }
            });
        },
    };
</script>