<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div  class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="paymentPolicyManageSearchForm" name="paymentPolicyManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">결제정책 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>결제정책 관리 검색</caption>
                    <tbody>
                        <tr>
                            <th>기간</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schPeriod" name="schPeriod" class="ui input medium mg-r-10">
                                        <option value="applyDt">적용일</option>
                                        <option value="regDt">등록일</option>
                                        <option value="chgDt">수정일</option>
                                    </select>
                                    <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                    <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </div>
                            </td>
                            <th>사이트</th>
                            <td>
                                <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="siteType" items="${siteTypeList}">
                                    <option value="${siteType.code}">${siteType.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>사용여부</th>
                            <td>
                                <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">사용안함</option>
                                </select>
                            </td>
                            <td rowspan="2">
                                <div style="text-align: center;" class="ui form">
                                    <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                    <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>검색어</th>
                            <td colspan="5">
                                <div class="ui form inline">
                                    <select id="schType" name="schType" class="ui input medium mg-r-10" >
                                        <option value="policyNm">관리명</option>
                                        <option value="policyNo">관리번호</option>
                                    </select>
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" >
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="paymentPolicyManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="paymentPolicyGrid" style="height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="paymentPolicyInputForm" name="paymentPolicyInputForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">기본정보</h3>
    </div>
    <div class="topline"></div>
    <div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>결제정책 상세내역</caption>
                <tbody>
                <tr>
                    <th>관리번호</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="policyNo" name="policyNo" class="ui input medium mg-r-5"  disabled="true" >
                        </div>
                    </td>
                    <th>사이트&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <select id="siteType" name="siteType" class="ui input medium mg-r-10" >
                            <option value="">선택</option>
                            <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>관리명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="text" id="policyNm" name="policyNm" class="ui input medium mg-r-5" maxlength="50" >
                            <span class="text">( <span id="textCountPolicyNm">0</span> / 50자 )</span>
                        </span>
                    </td>
                    <th>적용대상&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <select id="applyTarget" name="applyTarget" class="ui input medium mg-r-10" >
                            <option value="ITEM">상품</option>
                            <option value="CATE">카테고리</option>
                            <option value="AFFI">제휴채널</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>적용기간&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <label class="ui checkbox mg-r-5" style="display: inline">
                                <input type="checkbox" id="unlimitedApplyEndDt">
                                <span class="text">상시</span>
                            </label>
                            <input type="text" id="applyStartDt" name="applyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="applyEndDt" name="applyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                    <th>우선순위&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="priority" name="priority" class="ui input medium mg-r-5"  maxlength="3">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                    </td>
                    <th>결제수단 안내&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <label class="ui radio small inline"><input type="radio" name="paymentMethodGuideYn" id="guideY" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="paymentMethodGuideYn" id="guideN" value="N"><span>사용안함</span></label>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- 상세영역 2 -->
    <div class="com wrap-title sub">
        <h3 class="title">결제수단 노출제어</h3>
    </div>
    <div class="topline"></div>
    <div>
        <div id="paymentMethodExposureInput" class="ui wrap-table horizontal mg-t-20">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody>
                <tr>
                    <th>노출 결제수단&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div id="exposMethodGrid" style="width: 90%; height: 350px;"></div>
                    </td>
                    <td valign="center">
                        <button class="ui button medium" id="methodAddBtn" style="display: block">← 추가</button>
                        <button class="ui button medium" id="methodRemoveBtn" style="display: block">삭제 →</button>
                    </td>
                    <td>
                        <div id="methodTreeGrid" style="width: 90%; height: 350px;"></div>
                    </td>
                </tr>
                <tr>
                    <th>포인트 사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio inline">
                            <input type="radio" name="pointUseYn" id="pointUseY" class="ui input medium" value="Y" checked><span>사용</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="pointUseYn" id="pointUseN" class="ui input medium" value="N"><span>사용안함</span>
                        </label>
                    </td>
                    <th>현금영수증 발급여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio inline">
                            <input type="radio" name="cashReceiptUseYn" id="cashReceiptUseY" class="ui input medium" value="Y" checked><span>발급</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="cashReceiptUseYn" id="cashReceiptUseN" class="ui input medium" value="N"><span>발급안함</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>카드할부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio inline">
                            <input type="radio" name="cardInstallmentUseYn" id="cardInstallmentUseY" class="ui input medium" value="Y" checked><span>사용</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="cardInstallmentUseYn" id="cardInstallmentUseN" class="ui input medium" value="N"><span>사용안함</span>
                        </label>
                    </td>
                    <th>무이자 사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio inline">
                            <input type="radio" name="freeInterestUseYn" id="freeInterestUseY" class="ui input medium" value="Y" checked><span>사용</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="freeInterestUseYn" id="freeInterestUseN" class="ui input medium" value="N"><span>사용안함</span>
                        </label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
        <input type="hidden" id="pgStoreSet" name="pgStoreSet" value="">
    </form>

    <div class="com wrap-title sub">
        <h3 class="title">PG 상점 설정</h3>
    </div>
    <div class="topline"></div>
    <div>
        <form id="pgMidManageForm" name="pgMidManageForm" onsubmit="return false;">
            <div id="pgMidInput" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>PG 상점 설정</th>
                        <td colspan="6">
                            <select id="pgStoreSetShow" name="pgStoreSetShow" class="ui input medium mg-r-10">
                                <option value="N">해당없음</option>
                                <option value="P">PG배분</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="${parentMethodCdList.size()}">PG 배분율</th>
                    <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                        <th>${parentMethodCd.methodNm}</th>
                        <th>INICIS</th>
                        <td colspan="2">
                            <select id="${parentMethodCd.methodCd}inicisMid" name="${parentMethodCd.methodCd}inicisMid" style="display: inline;" class="ui input medium mg-r-5">
                                <option value="">상점 ID 선택</option>
                                <c:forEach var="inicisCd" items="${inicisCdList}">
                                    <option value="${inicisCd.pgMid}">${inicisCd.pgMid}</option>
                                </c:forEach>
                            </select>
                            <input type="number" id="${parentMethodCd.methodCd}inicisRate" name="${parentMethodCd.methodCd}inicisRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="3" min="0" max="100" value="0">%
                        </td>
                        <th>TOSSPG</th>
                        <td>
                            <select id="${parentMethodCd.methodCd}tosspgMid" name="${parentMethodCd.methodCd}tosspgMid" style="display: inline;" class="ui input medium mg-r-5">
                                <option value="">상점 ID 선택</option>
                                <c:forEach var="tosspgCd" items="${tosspgCdList}">
                                    <option value="${tosspgCd.pgMid}">${tosspgCd.pgMid}</option>
                                </c:forEach>
                            </select>
                            <input type="number" id="${parentMethodCd.methodCd}tosspgRate" name="${parentMethodCd.methodCd}tosspgRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="3" min="0" max="100" value="0">%
                        </td>
                    </tr>
                    <input type="hidden" id="${parentMethodCd.methodCd}divideSeq" name="${parentMethodCd.methodCd}divideSeq" value="">
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner" style="float:left">
            <button class="ui button xlarge cyan" id="copyPolicyDetailBtn">복사</button>
        </span>
        <span class="inner">
            <button class="ui button xlarge cyan" id="savePolicyDetailBtn">저장</button>
            <button class="ui button xlarge" id="resetPolicyDetailBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${paymentPolicyGridBaseInfo}
    ${methodTreeGridBaseInfo}
    ${exposMethodGridBaseInfo}
    $(document).ready(function () {
        paymentPolicyMng.init();
        paymentPolicyGrid.init();
        methodTreeGrid.init();
        exposMethodGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/paymentPolicyManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>