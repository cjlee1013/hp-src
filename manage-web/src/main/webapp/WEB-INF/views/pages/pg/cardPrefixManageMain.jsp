<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="cardPrefixManageSearchForm" name="cardPrefixManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">카드 PREFIX 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>카드 PREFIX 검색</caption>
                    <tbody>
                    <tr>
                        <th>카드프리픽스</th>
                        <td>
                            <div class="ui form inline">
                                <input id="schCardPrefixNo" name="schCardPrefixNo" type="text" class="ui input medium mg-r-5" autocomplete="off">
                            </div>
                        </td>
                        <th>카드상세종류</th>
                        <td>
                            <select id="schCardDetailKind" name="schCardDetailKind" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="cardDetailKind" items="${commCardDetailKindList}">
                                    <option value="${cardDetailKind.mcCd}">${cardDetailKind.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>결제수단코드</th>
                        <td>
                            <select id="schMethodCd" name="schMethodCd" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                    <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>카드식별명</th>
                        <td>
                            <div class="ui form inline">
                                <input id="schCardIdenNm" name="schCardIdenNm" type="text" class="ui input medium mg-r-5" autocomplete="off">
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="cardPrefixManageSearchCnt">0</span>건</h3>
            <span class="pull-right mg-t-10">
                <table>
                    <tr>
                        <td>결제수단일괄변경 : &nbsp;</td>
                        <td>
                            <select id="bundleMethodCd" name="bundleMethodCd" class="ui input medium">
                                <option value="">선택</option>
                                <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                    <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <button type="button" class="ui button medium" onclick="cardPrefixManageMain.applyGridMethodCd();">입력</button>
                            <button type="button" class="ui button medium" onclick="cardPrefixManageMain.updateBundleMethodCd();">저장</button>
                        </td>
                    </tr>
                </table>
            </span>
        </div>
        <div class="topline"></div>
        <div id="cardPrefixManageGrid" style="height: 380px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="cardPrefixManageForm" name="cardPrefixManageForm" onsubmit="return false;">
        <!-- Key값 (cardPrefixNo, cardKind) Hidden -->
        <input type="hidden" name="cardPrefixNo" id="cardPrefixNo" class="ui input medium" readonly="true">
        <input type="hidden" name="cardKind" id="cardKind" class="ui input medium" readonly="true">

        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <tbody>
                    <tr>
                        <th>결제수단코드</th>
                        <td>
                            <div class="ui form inline">
                                <select id="methodCd" name="methodCd" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                        <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>카드상세종류</th>
                        <td>
                            <div class="ui form inline">
                                <select id="cardDetailKind" name="cardDetailKind" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="cardDetailKind" items="${commCardDetailKindList}">
                                        <option value="${cardDetailKind.mcCd}">${cardDetailKind.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="updateCardPrefixManageBtn">저장</button>
                    <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${cardPrefixManageGridBaseInfo}
    $(document).ready(function() {
        cardPrefixManageMain.init();
        cardPrefixManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/cardPrefixManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>