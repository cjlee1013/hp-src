<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="paymentPolicyItemManageSearchForm" name="paymentPolicyItemManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">결제정책 상품 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>결제정책 상품 관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>기간</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schPeriod" name="schPeriod" class="ui input medium mg-r-10">
                                    <option value="applyDt">적용일</option>
                                    <option value="regDt">등록일</option>
                                    <option value="chgDt">수정일</option>
                                </select>
                                <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>사이트</th>
                        <td>
                            <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="5">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                     data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10">
                                    <option value="itemNo">상품번호</option>
                                    <option value="policyNm">관리명</option>
                                    <option value="policyNo">관리번호</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 기본그리드 조회를 위한 영역 -->
    <div hidden="true">
        <select id="schApplyTarget" name="schApplyTarget" hidden="true">
            <option value="ITEM">상품</option>
            <option value="CATE">카테고리</option>
            <option value="AFFI">제휴채널</option>
        </select>
        <select id="schPgStoreSet" name="schPgStoreSet" hidden="true">
            <option value="N">해당없음</option>
            <option value="P">PG배분</option>
        </select>
        <select id="schPointUseYn" name="schPointUseYn" hidden="true">
            <option value="Y">사용</option>
            <option value="N">사용안함</option>
        </select>
        <select id="schCashReceiptUseYn" name="schCashReceiptUseYn" hidden="true">
            <option value="Y">발급</option>
            <option value="N">발급안함</option>
        </select>
        <select id="schCardInstallmentUseYn" name="schCardInstallmentUseYn" hidden="true">
            <option value="Y">사용</option>
            <option value="N">사용안함</option>
        </select>
        <select id="schFreeInterestUseYn" name="schFreeInterestUseYn" hidden="true">
            <option value="Y">사용</option>
            <option value="N">사용안함</option>
        </select>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="mngSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="paymentPolicyItemGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <!-- 상세영역 - 기본정보 -->
    <form id="paymentPolicyItemInputForm" name="paymentPolicyItemInputForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">기본정보</h3>
    </div>
    <div class="topline"></div>
    <div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody>
                <tr>
                    <th>관리번호</th>
                    <td>
                        <span class="text" id="policyNo"></span>
                    </td>
                    <th>사이트</th>
                    <td>
                        <span class="text" id="siteType"></span>
                    </td>
                </tr>
                <tr>
                    <th>관리명</th>
                    <td>
                        <span class="text" id="policyNm"></span>
                    </td>
                    <th>적용대상</th>
                    <td>
                        <span class="text" id="applyTarget"></span>
                    </td>
                </tr>
                <tr>
                    <th>적용기간</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="applyStartDt"></span>
                            <span class="text">&nbsp;~&nbsp;</span>
                            <span class="text" id="applyEndDt"></span>
                        </div>
                    </td>
                    <th>우선순위</th>
                    <td>
                        <span class="text" id="priority"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- 상세영역 2 -->
    <div class="pull-left" style="width: 50%; height: 500px;">
        <div class="com wrap-title sub mg-t-30">
            <h3 class="title">적용대상</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>적용대상</caption>
                <tbody>
                <tr>
                    <td>
                        검색결과 : <span id="applyTargetTotalCount">0</span>개
                        <span class="pull-right">
                            <button type="button" id="uploadExcelBtn" class="ui button medium" onclick="paymentPolicyItemMng.uploadExcel();">일괄등록</button>
                            <button type="button" class="ui button medium" onclick="paymentPolicyItemMng.addApplyTarget();">등록</button>
                            <button type="button" class="ui button medium" onclick="paymentPolicyItemMng.deleteApplyTarget();">삭제</button>
                            <button type="button" class="ui button medium" onclick="paymentPolicyItemMng.downloadExcel();">엑셀다운</button>
                        </span>
                        <div class="mg-t-20 mg-b-20" id="applyItemGrid" style="width: 100%; height: 300px;"></div>
                        <div class="mg-t-20 mg-b-20" id="applyCategoryGrid" style="width: 100%; height: 300px;"></div>
                        <div class="mg-t-20 mg-b-20" id="applyAffiliateGrid" style="width: 100%; height: 300px;"></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pull-right" style="width: 50%; height: 520px;">
        <div class="com wrap-title sub mg-t-30">
            <h3 class="title">결제수단 노출제어</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>결제수단 노출제어</caption>
                <colgroup>
                    <col width="30%">
                    <col width="70%">
                </colgroup>
                <tbody>
                <tr>
                    <th>노출 결제수단</th>
                    <td>
                        <div id="exposMethodGrid" style="width: 90%; height: 300px;"></div>
                    </td>
                </tr>
                <tr>
                    <th>포인트 사용여부</th>
                    <td>
                        <span class="text" id="pointUseYn"></span>
                    </td>
                </tr>
                <tr>
                    <th>현금영수증 발급여부</th>
                    <td>
                        <span class="text" id="cashReceiptUseYn"></span>
                    </td>
                </tr>
                <tr>
                    <th>카드할부</th>
                    <td>
                        <span class="text" id="cardInstallmentUseYn"></span>
                    </td>
                </tr>
                <tr>
                    <th>무이자 사용여부</th>
                    <td>
                        <span class="text" id="freeInterestUseYn"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </form>

    <div style="clear:both;"></div>
    <div class="com wrap-title sub">
        <h3 class="title">PG 상점 설정</h3>
    </div>
    <div class="topline"></div>
    <div>
        <form id="pgMidManageForm" name="pgMidManageForm" onsubmit="return false;">
            <div id="pgMidInput" class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>PG 상점 설정</th>
                        <td colspan="6">
                            <span class="text" id="pgStoreSet"></span>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="${parentMethodCdList.size()}">PG 배분율</th>
                    <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                        <th>${parentMethodCd.methodNm}</th>
                        <th>INICIS</th>
                        <td colspan="2">
                            <span class="text" id="${parentMethodCd.methodCd}inicisMid"></span>
                            <span class="text">&nbsp;/&nbsp;</span>
                            <span class="text" id="${parentMethodCd.methodCd}inicisRate"></span>
                        </td>
                        <th>TOSSPG</th>
                        <td>
                            <span class="text" id="${parentMethodCd.methodCd}tosspgMid"></span>
                            <span class="text">&nbsp;/&nbsp;</span>
                            <span class="text" id="${parentMethodCd.methodCd}tosspgRate"></span>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner" style="float:left">
            <button class="ui button xlarge cyan" id="policyItemHistoryBtn">히스토리</button>
        </span>
        <span class="inner">
            <button class="ui button xlarge cyan" id="savePolicyItemBtn">저장</button>
            <button class="ui button xlarge" id="resetPolicyItemBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${paymentPolicyItemGridBaseInfo}
    ${applyItemGridBaseInfo}
    ${applyCategoryGridBaseInfo}
    ${applyAffiliateGridBaseInfo}
    ${exposMethodGridBaseInfo}
    $(document).ready(function () {
        paymentPolicyItemMng.init();
        paymentPolicyItemGrid.init();
        applyItemGrid.init();
        applyCategoryGrid.init();
        applyAffiliateGrid.init();
        exposMethodGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/paymentPolicyItemManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>