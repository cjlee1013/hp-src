<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-title escrow-main">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">기본배분율 관리</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div style="text-align: right" class="ui form inline mg-t-15">
            <button class="ui button medium white" id="historyBtn">변경이력</button>
        </div>
        <!-- 기본배분율 조회결과 영역 -->
        <div class="com wrap-title sub">
            <table>
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="pgDivideManageGrid" style="width:100%; height:350px;"></div>
                    </td>
                </tr>
            </table>
        </div>
        <!-- 플랫폼 & 디바이스 & 결제수단 선택 영역 -->
        <div class="com wrap-title sub">
            <div id="groupInput" class="ui wrap-table horizontal mg-t-10">
                <form id="pgDivideManageSearchForm" name="pgDivideManageSearchForm" onsubmit="return false;">
                    <table class="ui table">
                        <tbody>
                        <tr>
                            <th>사이트</th>
                            <td>
                                <select class="ui input medium mg-r-5" id="siteType" name="siteType" style="float:left;" disabled="true">
                                    <option value="">- 선택 -</option>
                                    <option value="HOME">홈플러스</option>
<%--                                    [ESCROW-343, CLUB 노출제외]--%>
<%--                                    <option value="CLUB">더클럽</option>--%>
                                </select>
                            </td>
                            <th>디바이스</th>
                            <td>
                                <select class="ui input medium mg-r-5" id="platform" name="platform" style="float:left;" disabled="true">
                                    <option value="">- 선택 -</option>
                                    <option value="PC">PC</option>
                                    <option value="MOBILE">모바일</option>
                                </select>
                            </td>
                            <th>결제수단</th>
                            <td>
                                <select class="ui input medium mg-r-5" id="parentMethodCd" name="parentMethodCd" style="float:left;" disabled="true">
                                    <option value="">- 선택 -</option>
                                    <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                                        <option value=${parentMethodCd.methodCd}>${parentMethodCd.methodNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>INICIS</th>
                            <td colspan="3">
                                <select id="inicisMid" name="inicisMid" style="display: inline;" class="ui input medium mg-r-5">
                                    <option value="" >선택</option>
                                    <c:forEach var="inicisCd" items="${inicisCdList}">
                                        <option value="${inicisCd.pgMid}">${inicisCd.pgMid}</option>
                                    </c:forEach>
                                </select>
                                <input type="number" id="inicisRate" name="inicisRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="10" min="0" max="100" value="0">%
                            </td>
                            <th>TOSSPG</th>
                            <td>
                                <select id="tosspgMid" name="tosspgMid" style="display: inline;" class="ui input medium mg-r-5">
                                    <option value="" >선택</option>
                                    <c:forEach var="tosspgCd" items="${tosspgCdList}">
                                        <option value="${tosspgCd.pgMid}">${tosspgCd.pgMid}</option>
                                    </c:forEach>
                                </select>
                                <input type="number" id="tosspgRate" name="tosspgRate" class="ui input medium mg-r-5" style="align:right; display: inline;" maxlength="10" min="0" max="100" value="0">%
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    <input type="hidden" id="divideSeq" name="divideSeq" value="">
                </form>
            </div>
        </div>
        <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button xlarge cyan" id="saveDivideRateBtn">수정</button>
<%--                <button class="ui button xlarge" id="searchResetBtn">초기화</button>--%>
            </span>
        </div>
    </div>
</div>

<script>
    // 기본배분율 그리드 기본정보
    ${pgDivideManageGridBaseInfo};
    $(document).ready(function () {
        pgDivideManageGrid.init();
        pgDivideMng.init();
        pgDivideMng.search();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pgDivideManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>