<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div  class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="pgCompareApprovalSearchForm" name="pgCompareApprovalSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">PG 승인대사 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>PG 승인대사 검색</caption>
                    <tbody>
                        <tr>
                            <th>기간선택</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                    <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareApprovalMain.setSearchDate('D');">어제</button>
                                    <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareApprovalMain.setSearchDate('W');">1주일전</button>
                                    <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pgCompareApprovalMain.setSearchDate('M');">1개월전</button>
                                </div>
                            </td>
                            <th>PG 선택</th>
                            <td>
                                <select id="schPgKind" name="schPgKind" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="pgKind" items="${pgKindList}">
                                        <option value="${pgKind.pgCd}">${pgKind.pgNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="2">
                                <div style="text-align: center;" class="ui form">
                                    <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                    <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                    <button type="button" id="sumExcelDownBtn" class="ui button large white mg-t-5">엑셀다운</button><br/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>상세내역</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <select id="schTradeTypeCd" name="schTradeTypeCd" class="ui input medium mg-r-10" >
                                        <option value="">거래구분 전체</option>
                                        <option value="A">승인</option>
                                        <option value="C">취소</option>
                                    </select>
                                    <select id="schPaymentMethod" name="schPaymentMethod" class="ui input medium mg-r-10" >
                                        <option value="">결제수단 전체</option>
                                        <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                            <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                        </c:forEach>
                                    </select>
                                    <select id="schDiffReasonCd" name="schDiffReasonCd" class="ui input medium mg-r-10" >
                                        <option value="">차이구분 전체</option>
                                        <option value="R01">건없음(홈플러스)</option>
                                        <option value="R02">건없음(PG)</option>
                                        <option value="R03">금액상이</option>
                                        <option value="R04">결제수단상이</option>
                                        <option value="R05">중복(PG)</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">PG사별 승인 합계내역</h3>
        </div>
        <div id="pgCompareApprovalSumGrid" style="height: 150px;"></div>
    </div>

    <p>&nbsp;</p>

    <div style="margin-top: 30px; width: 100%;">
        <div class="com wrap-title sub" style="width: 50%; float: left;">
            <h3 class="title">승인 차이내역</h3>
        </div>
        <div style="width: 50%; float: right;">
            <button type="button" id="diffExcelDownBtn" class="ui button large white mg-t-5" style="float: right;">엑셀다운</button><br/>
        </div>
        <div id="pgCompareApprovalDiffGrid" style="height: 350px;"></div>
    </div>

</div>

<script>
    ${pgCompareApprovalSumGridBaseInfo}
    ${pgCompareApprovalDiffGridBaseInfo}
    $(document).ready(function () {
        pgCompareApprovalMain.init();
        pgCompareApprovalSumGrid.init();
        pgCompareApprovalDiffGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pgCompareApprovalMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>