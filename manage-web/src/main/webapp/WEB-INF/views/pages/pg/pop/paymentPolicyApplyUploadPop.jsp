<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>

<div class="com wrap-popup small">
    <form id="excelItemUploadForm" name="excelItemUploadForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun" id="popupTitle">
                상품 일괄등록
            </h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="130px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>업로드양식</th>
                    <td>
                        <button type="button" id="downloadTemplate" class="ui button medium btn-warning"><i class="fa fa-search"></i>다운로드</button><br/>
                    </td>
                </tr>
                <tr>
                    <th>일괄등록 파일선택</th>
                    <td>
                        <div style="display:none">
                            <input type="file" name="uploadFile" id="uploadFile">
                        </div>
                        <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" style="width: 190px;float: left" disblaed="true">
                        <button type="button" class="ui button medium mg-l-5" id="searchUploadFile">파일찾기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="mg-t-10">
            <ul class="sub-menu">
                <li>Excel 파일 작성시 유의사항</li>
                <li>* 한번 수행시 최대 1,000개로 제한</li>
                <li>* 저장 파일명은 : [*.xlsx] 로 함</li>
            </ul>
        </div>
        <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="uploadFileBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn">취소</button>
        </span>
        </div>
    </form>
</div>

<script>
    var callBackScript = "${callBackScript}";
    $(document).ready(function () {
        uploadPopMng.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/pg/pop/paymentPolicyApplyUploadPop.js?v=${fileVersion}"></script>