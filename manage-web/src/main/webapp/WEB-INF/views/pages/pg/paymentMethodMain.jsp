<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-title escorw-main">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">결제수단 관리</h2>
        <span class="inner" style="float:right">
            <button class="ui button xlarge dark-blue" id="excelDownloadAllBtn">엑셀다운</button>
        </span>
    </div>
    <form id="paymentMethodSearchForm" name="paymentMethodSearchForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <div id="groupLeft" class="ui wrap-table horizontal">
                <table>
                    <tr>
                        <!-- 결제수단 선택영역 -->
                        <td width="380px" valign="top" align="left">
                            <div id="groupReq" style="overflow: scroll; height: 350px;">
                            <br>
                            <table class="ui table">
                                <thead>
                                <tr>
                                    <th style="font-size: small;" align="center">사이트</th>
                                    <th style="font-size: small;" align="center">결제수단코드</th>
                                    <th style="font-size: small;" align="center">결제수단명</th>
                                    <th style="font-size: small;" align="center">선택</th>
                                </tr>
                                </thead>
                                <c:forEach var="parentMethodCd" items="${parentMethodCdList}" varStatus="status">
                                    <tbody>
                                    <tr align="center">
                                        <td align="center">
                                            ${parentMethodCd.siteNm}
                                        </td>
                                        <td align="center">
                                            ${parentMethodCd.methodCd}
                                        </td>
                                        <td align="center">
                                            ${parentMethodCd.methodNm}
                                        </td>
                                        <td>
                                            <span class="inner">
                                                <button class="ui button medium font-malgun"
                                                        onclick="paymentMethodMng.search('${parentMethodCd.methodCd}','${parentMethodCd.siteType}');">선택</button>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </c:forEach>
                            </table>
                            </div>
                        </td>
                        <td width="1%"></td>
                        <!-- 결제수단 검색 결과 -->
                        <td valign="top">
                            <div class="mg-t-20" id="paymentMethodGrid"
                                 style="overflow: scroll; height:350px;"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
    <!-- 결제수단 상세 검색 결과 -->
    <div class="com wrap-title sub">
        <div id="groupInput" class="ui wrap-table horizontal mg-t-10">
            <form id="inputForm" name="inputForm" enctype="multipart/form-data"
                  onsubmit="return false;" method="post">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>
                            사이트
                        </th>
                        <td>
                            <select class="ui input medium mg-r-10" id="siteType" name="siteType"
                                    style="float:left;">
                                <option value="">선택</option>
                                <option value="HOME">홈플러스</option>
<%--                                [ESCROW-343, CLUB 노출제외]--%>
<%--                                <option value="CLUB">더클럽</option>--%>
                            </select>
                        </td>
                        <th>
                            사용여부
                        </th>
                        <td colspan="2">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn"
                                    style="float:left;">
                                <option value="N">비사용</option>
                                <option value="Y">사용</option>
                            </select>
                            <select class="ui input medium mg-r-10" id="platform" name="platform">
                                <option value="">노출범위 선택</option>
                                <option value="ALL">전체</option>
                                <option value="PC">PC</option>
                                <option value="MOBILE">모바일</option>
                            </select>
                        </td>
                        <th>
                            노출순서&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="sortSeq" name="sortSeq" class="ui input medium mg-r-5" maxlength="4">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            상세결제수단 코드&nbsp;<span class="text-red">*</span>
                        </th>
                        <td colspan="2">
                            <input type="text" id="methodCd" name="methodCd"
                                   class="ui input medium mg-r-5" maxlength="10"
                                   title="key 값입니다 중복되지 않도록 입력해주세요.">
                        </td>
                        <th>
                            상세결제수단명&nbsp;<span class="text-red">*</span>
                        </th>
                        <td colspan="3">
                            <input type="text" id="methodNm" name="methodNm"
                                   class="ui input medium mg-r-5"
                                   maxlength="20">
                            <span> * 결제 페이지에서 노출됩니다. 정확히 입력해주세요.</span>
                        </td>
                    </tr>
                    <tr id="pgCodeArea1" name="pgCodeArea1">
                        <th>
                            INICIS 코드&nbsp;<span class="text-red">*</span>
                        </th>
                        <td colspan="2"><input type="text" id="inicisCd" name="inicisCd"
                                   class="ui input medium mg-r-5"
                                   maxlength="10"></td>
                        <th>
                            TOSSPG 코드&nbsp;<span class="text-red">*</span>
                        </th>
                        <td colspan="3"><input type="text" id="tosspgCd" name="tosspgCd"
                                   class="ui input medium mg-r-5"
                                   maxlength="10"></td>
                    </tr>
                    <tr id="imageArea" name="imageArea" style="display: none">
                        <th>
                            PC 노출이미지<span class="text-red">*</span>
                        </th>
                        <td colspan="2">
                            <div id="imgDiv">
                                <div class="text-center preview-image" style="height:132px;">
                                    <div id="thumbArea" style="display:none;">
                                        <button type="button" id="deleteImageBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                        <div id="listImgArea" class="uploadType" inputId="uploadListIds" style="padding-top: 50px;">
                                            <img id="thumbUrl" class="imgUrlTag" width="96" height="44" src="">
                                            <input type="hidden" class="imgUrl" name="imgUrl" value=""/>
                                            <input type="hidden" class="imgWidth" name="imgWidth" value=""/>
                                            <input type="hidden" class="imgHight" name="imgHight" value=""/>
                                            <input type="hidden" class="changeYn" name="changeYn" value="N"/>
                                        </div>
                                    </div>
                                    <div id="uploadArea" style="padding-top: 50px;" class="pd-t-40 text-size-sm text-gray-Lightest">
                                        사이즈 : 96 X 44<br>
                                        용량 : 2MB 이하<br><br>
                                        <button type="button" class="ui button medium" onclick="paymentMethodImg.saveImageFile('pc');">등록</button>
                                    </div>
                                </div>
<%--                            <div class="text-center preview-image imageGuideLayer"--%>
<%--                                 style="width:132px; height:132px;">--%>
<%--                                <div class="pd-t-40 text-size-sm text-gray-Lightest">--%>
<%--                                    사이즈 : 76 X 46<br>--%>
<%--                                    파일 : png, jpg, jpeg<br>--%>
<%--                                    용량 : 2MB 이하--%>
<%--                                </div>--%>
<%--                            </div>--%>
                            </div>
                        </td>

                        <th>
                            Mobile 노출이미지
                        </th>
                        <td colspan="3">
                            <div id="imgDivMobile">
                                <div class="text-center preview-image" style="height:132px;">
                                    <div id="thumbAreaMobile" style="display:none;">
                                        <button type="button" id="deleteImageBtnMobile" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                        <div id="listImgAreaMobile" class="uploadType" inputId="uploadListIds" style="padding-top: 50px;">
                                            <img id="thumbUrlMobile" class="imgUrlTag" width="86" height="38"  src="">
                                            <input type="hidden" class="imgUrlMobile" name="imgUrlMobile" value=""/>
                                            <input type="hidden" class="mobileChangeYn" name="mobileChangeYn" value="N"/>
                                        </div>
                                    </div>
                                    <div id="uploadAreaMobile" style="padding-top: 50px;" class="pd-t-40 text-size-sm text-gray-Lightest">
                                        사이즈 : 86 X 38<br>
                                        용량 : 2MB 이하<br><br>
                                        <button type="button" class="ui button medium" onclick="paymentMethodImg.saveImageFile('mobile');">등록</button>
                                    </div>
                                </div>
<%--                            <div class="text-center preview-image imageGuideLayer"--%>
<%--                                 style="width:132px; height:132px;">--%>
<%--                                <div class="pd-t-40 text-size-sm text-gray-Lightest">--%>
<%--                                    최적화 가이드<br><br>--%>
<%--                                    사이즈 : 90 X 60<br>--%>
<%--                                    파일 : png, jpg, jpeg<br>--%>
<%--                                    용량 : 2MB 이하--%>
<%--                                </div>--%>
<%--                            </div>--%>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="display:none;">
                    <input type="text" id="parentMethodCd" name="parentMethodCd">
                    <input type="text" id="pgCertifyKind" name="pgCertifyKind">
                    <input type="text" id="orgCode" name="orgCode" value="">
                </div>
            </form>
        </div>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="savePaymentMethodBtn">저장</button>
        </span>
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="searchResetBtn">초기화</button>
        </span>
    </div>
</div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="paymentMethodImgFile" style="display: none;"/>

<script>
    const apiImgUrl = '${apiImgUrl}';

    // 결제수단 그리드 기본정보
    ${paymentMethodGridBaseInfo};
    $(document).ready(function () {
        paymentMethodGrid.init();
        paymentMethodMng.init();
        // paymentImg.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/paymentMethodMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>