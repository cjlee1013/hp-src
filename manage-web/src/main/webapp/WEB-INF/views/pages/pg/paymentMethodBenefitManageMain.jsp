<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="paymentMethodBenefitManageSearchForm" name="paymentMethodBenefitManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">결제수단별 혜택관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>결제수단별 혜택관리 테이블</caption>
                    <tbody>
                    <tr>
                        <th>노출기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schApplyStartDt" name="schApplyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schApplyEndDt" name="schApplyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            </div>
                        </td>
                        <td/>
                        <th>제목</th>
                        <td>
                            <input type="text" id="schBenefitTitle" name="schBenefitTitle" class="ui input">
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트</th>
                        <td>
                            <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-5">
                                <option value="ALL">전체</option>
                            <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                            </c:forEach>
                            </select>
                        </td>
                        <td/>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="ALL">전체</option>
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="paymentMethodBenefitManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="paymentMethodBenefitManageGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="paymentMethodBenefitManageForm" name="paymentMethodBenefitManageForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <tbody>
                    <tr>
                        <th>
                            번호
                        </th>
                        <td>
                            <input type="text" id="paymentMethodBenefitManageSeq" name="paymentMethodBenefitManageSeq" class="ui input medium" readonly>
                        </td>
                        <th>
                            사이트&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <select id="siteType" name="siteType" class="ui input medium mg-r-10">
                                <option value="">선택</option>
                            <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                            </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            결제수단&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <select id="parentMethodCd" name="parentMethodCd" class="ui input medium mg-r-10">
                                <option value="">선택</option>
                            <c:forEach var="parentMethodCd" items="${parentMethodCdList}">
                                <option value="${parentMethodCd.methodCd}">${parentMethodCd.methodNm}</option>
                            </c:forEach>
                            </select>
                        </td>
                        <th>
                            사용여부
                        </th>
                        <td>
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                            <label class="ui radio small inline">
                                <input type="radio" name="useYn" id="use${codeDto.mcCd}" value="${codeDto.mcCd}" <c:if test="${codeDto.mcCd eq 'Y'}">checked</c:if>>
                                <span>${codeDto.mcNm}</span>
                            </label>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            제목&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <input type="text" id="benefitTitle" name="benefitTitle" class="ui input medium" maxlength="20">
                        </td>
                        <th>
                            노출기간&nbsp;<span class="text-red">*</span>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="applyStartDt" name="applyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="applyEndDt" name="applyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>혜택안내문구</th>
                        <td colspan="2">
                            <input type="text" id="benefitMessage1" name="benefitMessage1" class="ui input medium" maxlength="20"><br>
                            <input type="text" id="benefitMessage2" name="benefitMessage2" class="ui input medium" maxlength="20"><br>
                            <input type="text" id="benefitMessage3" name="benefitMessage3" class="ui input medium" maxlength="20">
                        </td>
                        <td style="vertical-align: bottom">* 결제 페이지에서 노출됩니다. 정확히 입력해주세요.<br>(최대 20글자 등록 가능)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <colgroup>
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="addPaymentMethodBenefitManageBtn">저장</button>
                    <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${paymentMethodBenefitManageGridBaseInfo}
    $(document).ready(function() {
        paymentMethodBenefitManageMain.init();
        paymentMethodBenefitManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/paymentMethodBenefitManage.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
<!-- 예외 input box style 적용 -->
<script>
    $(function () {
        console.log("??");
        $("#benefitMessage1").css("width", "500px");
        $("#benefitMessage2").css("width", "500px");
        $("#benefitMessage3").css("width", "500px");
    });
</script>
