<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="freeInterestSearchForm" name="freeInterestSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">무이자 혜택 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>무이자 혜택 관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>적용기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schApplyStartDt" name="schApplyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schApplyEndDt" name="schApplyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>사이트</th>
                        <td colspan="2">
                            <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="siteType" items="${siteTypeList}">
                                <option value="${siteType.code}">${siteType.name}</option>
                                </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th>제목</th>
                        <td>
                            <span class="ui form inline">
                            <input id="schFreeInterestNm" name="schFreeInterestNm" type="text" class="ui input medium mg-r-5" maxlength="20">
                        </span>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline mg-l-30">
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button>
                                <button type="button" id="schResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- Main 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="freeInterestSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="freeInterestGrid" style="height: 250px;"></div>
    </div>

    <!-- 기본영역 -->
    <form id="freeInterestForm" name="freeInterestForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">기본정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>기본정보</caption>
                    <tbody>
                    <tr>
                        <th>번호</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="freeInterestMngSeq" id="freeInterestMngSeq" class="ui input medium" maxlength="20" readonly>
                            </div>
                        </td>
                        <th>
                            사이트&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="siteType" name="siteType" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="siteType" items="${siteTypeList}">
                                        <option value="${siteType.code}">${siteType.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            제목&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="freeInterestNm" id="freeInterestNm" class="ui input medium" maxlength="20">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                                <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            적용기간&nbsp;<span class="text-red">*</span>
                        </th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="applyStartDt" name="applyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="applyEndDt" name="applyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="ui mg-t-30">
            <table class="ui">
                <tbody>
                <tr>
                    <td style="text-align: center;">
                        <button type="button" class="ui button xlarge cyan" id="saveBtn">저장</button>
                        <button type="button" class="ui button xlarge" id="resetBtn">초기화</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- Detail 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <div class="ui form inline"><h3 class="title">카드사 등록 정보</h3></div>
            <span style="float: right;"><h3 class="title">검색결과 : <span id="freeInterestApplyCnt">0</span>건</h3></span>
        </div>
        <div class="topline"></div>
        <div id="freeInterestApplyGrid" style="height: 250px;"></div>
    </div>

    <form id="freeInterestApplyForm" name="freeInterestApplyForm" onsubmit="return false;">
        <div class="com wrap-title">
            <input type="hidden" id="applySeq" name="applySeq">
            <input type="hidden" id="copyFlag" name="copyFlag" value="NONE">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">카드사 정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>카드사 정보</caption>
                    <tbody>
                    <tr>
                        <th>
                            카드사&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="methodCd" name="methodCd" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <c:forEach var="paymentMethod" items="${paymentMethodList}">
                                        <option value="${paymentMethod.methodCd}">${paymentMethod.methodNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>
                            결제금액&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="paymentAmt" id="paymentAmt" class="ui input medium" maxlength="5">
                                <span class="text">만원</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            무이자유형&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <select id="freeInterestType" name="freeInterestType" class="ui input medium mg-r-10">
                                    <option value="">선택</option>
                                    <option value="N">무이자</option>
                                    <option value="P">부분무이자</option>
                                </select>
                            </div>
                        </td>
                        <th>
                            할부개월&nbsp;<span class="text-red">*</span>
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" name="installmentTerm" id="installmentTerm" class="ui input medium" maxlength="20">
                                <span class="text">개월</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui mg-t-30">
        <table class="ui">
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="copyBtn">복사</button>
                    <button type="button" class="ui button xlarge cyan" id="saveAllBtn">등록</button>
                    <button type="button" class="ui button xlarge" id="resetDetailBtn">상세정보 초기화</button>
                    <button type="button" class="ui button xlarge cyan" id="applyDeleteBtn">삭제</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    ${freeInterestGridBaseInfo}
    ${freeInterestApplyGridBaseInfo}
    $(document).ready(function() {
        freeInterestMngMain.init();
        freeInterestGrid.init();
        freeInterestApplyGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/freeInterestMngMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>