<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-popup xlarge">
    <form id="historySearchForm" onsubmit="return false;">
        <input type="hidden" id="policyNo" name="policyNo" value="${policyNo}">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">히스토리 검색</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>히스토리 검색</caption>
                <colgroup>
                    <col width="120px">
                    <col width="*">
                    <col width="100px">
                </colgroup>
                <tbody>
                <tr>
                    <th>조회기간</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                    <td rowspan="2">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>${applyTarget}</th>
                    <td>
                        <div id="itemDiv" class="ui form inline">
                            <input type="text" id="schItemCd" name="schItemCd" class="ui input medium mg-r-5" style="width: 250px;">
                        </div>
                        <div id="cateDiv" class="ui form inline">
                            <select id="schCateCd1" name="schLcateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');"></select>
                            <select id="schCateCd2" name="schMcateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="중분류" onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');"></select>
                            <select id="schCateCd3" name="schScateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');"></select>
                            <select id="schCateCd4" name="schDcateCd" class="ui input medium mg-r-5" style="width:170px;" data-default="세분류"></select>
                        </div>
                        <div id="affiDiv" class="ui form inline">
                            <select id="schAffiliateType" name="schAffiliateType" class="ui input medium mg-r-5" style="width: 120px;">
                                <option value="channelId">채널ID</option>
                                <option value="channelNm">제휴채널명</option>
                            </select>
                            <input type="text" id="schAffiliateValue" name="schAffiliateValue" class="ui input medium mg-r-5" style="width: 150px;" minlength="2" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 결제정책 히스토리 팝업 그리드 -->
    <div class="topline"></div>
    <div id="policyHistoryPopGrid" style="width: 100%; height: 250px;"></div>
</div>

<script>
    var applyTarget = "${applyTarget}";

    // 결제정책 히스토리 팝업 그리드
    ${paymentPolicyHistoryPopBaseInfo}
    $(document).ready(function () {
        policyHistoryPopGrid.init();
        historyPopMng.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/pg/pop/paymentPolicyHistoryPop.js?v=${fileVersion}"></script>