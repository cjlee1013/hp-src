<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
          상품상세 홈플러스 공지
        </h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="homeplusBannerSchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  homeplusBanner.search();">
                <table class="ui table">
                    <caption>상품상세 홈플러스 공지 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="450px">
                        <col width="120px">
                        <col />
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDateType" style="width: 100px" name="schDateType" class="ui input medium mg-r-10" data-default="REGDT">
                                    <c:forEach var="codeDto" items="${bannerPeriodType}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>

                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:90px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:90px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispType" style="width: 120px" name="schDispType" class="ui input medium mg-r-10" data-default="ALL">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>노출여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispYn" style="width: 120px" name="schDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>등록자</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schRegNm" name="schRegNm" class="ui input medium mg-r-5" style="width: 120px;" >
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="schCateCd1" name="schCateCd1"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd2" name="schCateCd2"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd3" name="schCateCd3"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd4" name="schCateCd4"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="homeplusBannerTotalCount">0</span>건 </h3>
                <span class="text-red mg-l-10">※ 동일한 상품에 구분이 다른 공지가 중복 등록된 경우, 카테고리별 공지는 전체 공지보다 우선하여 적용됩니다.</span>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="homeplusBannerListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 상품상세 홈플러스 공지 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="homeplusBannerSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>상품상세 홈플러스 공지 등록/수정</caption>
                <colgroup>
                    <col width="140px">
                    <col width="*">
                    <col width="120px">
                    <col width="*">
                    <col>
                </colgroup>
                <tbody>
                <tr>
                    <th>제목<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="bannerNo" name="bannerNo" type="hidden">
                            <input id="bannerNm" name="bannerNm" type="text" style="width: 50%;" class="ui input medium mg-r-5" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>점포유형<span class="text-red"> *</span></th>
                    <td>
                        <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:150px;float:left;" >
                            <c:forEach var="codeDto" items="${bannerStoreType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>구분<span class="text-red"> *</span></th>
                    <div class="ui form inline ">
                        <td colspan="3">
                        <label class="ui radio medium inline"><input type="radio" name="dispType" class="ui input medium" value="ALL" checked><span>전체</span></label>
                        <label class="ui radio medium inline"><input type="radio" name="dispType" class="ui input medium" value="CATE"><span>카테고리별</span></label>
                        </td>
                    </div>
                </tr>
                <tr id="cateRow" style="display:none">
                    <th>카테고리<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="selectCateCd1" name="selectCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(1,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd2" name="selectCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(2,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd3" name="selectCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(3,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd4" name="selectCateCd4"
                                style="width:150px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr id="bannerDescRow">
                    <th>공지내용<span class="text-red"> *</span><br>(최대 1,000자까지 입력가능)</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <textarea name="bannerDesc" id="bannerDesc" class="ui input" style="width: 100%; height: 70px;" maxlength="1000"></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>노출여부<span class="text-red"> *</span></th>
                    <div class="ui form inline">
                        <td>
                            <label class="ui radio medium inline"><input type="radio" name="dispYn" class="ui input medium" value="Y" checked><span>노출</span></label>
                            <label class="ui radio medium inline"><input type="radio" name="dispYn" class="ui input medium" value="N"><span>비노출</span></label>
                        </td>
                    </div>
                    <th>노출기간<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="노출시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="노출종료일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge mg-r-5 white" id="resetFormBtn" >초기화</button>
            <button type="button" class="ui button xlarge mg-r-5 cyan" id="setBannerBtn">저장</button>
        </span>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/homeplusBannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>


<script>
    // 상품상세 일괄공지 리스트 리얼그리드 기본 정보
    ${homeplusBannerListGridBaseInfo}
</script>
