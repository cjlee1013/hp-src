<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">예약상품판매관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-5">
            <form id="searchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  store.search();">
                <table class="ui table">
                    <caption>검색</caption>
                    <colgroup>
                        <col width="120px;">
                        <col width="25%">
                        <col width="120px">
                        <col width="25%">
                        <col width="%">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="RSV" selected>예약판매기간</option>
                                    <option value="SEND">배송기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="calendarInit.initCalendarDate('schStartDt', 'schEndDt', 'SEARCH', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="calendarInit.initCalendarDate('schStartDt', 'schEndDt', 'SEARCH', '1w');">1주일후</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="calendarInit.initCalendarDate('schStartDt', 'schEndDt', 'SEARCH', '1m');">1개월후</button>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="searchBtn" class="ui button large mg-r-5 cyan"><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-5 white">초기화</button>
                                <button type="button" id="excelDownloadBtn"	class="ui button large mg-t-5 dark-blue" style="display: none;">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schStoreType" name="schStoreType" style="width:120px;float:left;" >
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${storeType}" varStatus="status">
                                    <c:if test="${code.mcCd ne 'DS' and code.mcCd ne 'AURORA' and code.mcCd ne 'EXP' }">
                                        <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <th>예약판매명</th>
                        <td>
                            <input id="schRsvSalesNm" name="schRsvSalesNm" type="text" class="ui input medium mg-r-5" style="width:300px"; maxlength="20" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <th>판매유형</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schItemType" name="schSaleType" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${saleType}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schUseYn" name="schUseYn" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${useYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="rsvSaleTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-10 mg-b-20" id="rsvSaleListGrid" style="width: 100%; height: 350px;"></div>
    </div>
    <div class="ui mg-t-5" style="padding-top: 10px; padding-bottom: 30px;">
        <form id="detailSetForm" name="itemSetForm" onsubmit="return false;">
            <input hidden id="rsvSaleSeq" name="rsvSaleSeq"/>
            <div class="com wrap-title sub">
                <h3 class="title">기본정보</h3>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>기본정보</caption>
                        <colgroup>
                            <col width="120px">
                            <col width="200">
                            <col width="120px">
                            <col width="200">
                            <col width="120px">
                            <col width="200">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>예약판매명<span class="text-center text-red"> *</span></th>
                            <td>
                                <input type="text" id="rsvSaleNm" name="rsvSaleNm" class="ui input medium mg-r-5" style="width: 300px;" isUpdate="true" maxlength="25">
                            </td>
                            <th>판매기간<span class="text-center text-red"> *</span></th>
                            <td colspan="3">
                                <div class="ui form inline dateControl">
                                    <input type="text" id="rsvSaleStartDt" name="rsvSaleStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" isUpdate="false">
                                    <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="rsvSaleEndDt" name="rsvSaleEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" isUpdate="false">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>점포유형<span class="text-center text-red"> *</span></th>
                            <td>
                                <select id="storeType" name="storeType" class="ui input medium" style="width: 120px;" isUpdate="false">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'DS' and codeDto.mcCd ne 'AURORA' and codeDto.mcCd ne 'EXP'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>판매유형<span class="text-center text-red"> *</span></th>
                            <td>
                                <select id="saleType" name="saleType" class="ui input medium" style="width: 120px;" isUpdate="false">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${saleType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>사용여부<span class="text-center text-red"> *</span></th>
                            <td>
                                <select id="useYn" name="useYn" class="ui input medium" style="width: 120px;" isUpdate="true">
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr id="trSaleWay">
                            <th>판매방법</th>
                            <td colspan="5">
                                <div class="ui form inline">
                                    <c:forEach var="codeDto" items="${saleWay}" varStatus="status">
                                        <c:set var="checked" value="" />
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="checked" value="checked" />
                                        </c:if>
                                        <label class="ui radio medium inline"><input type="radio" name="saleWay" value="${codeDto.mcCd}" ${checked} isUpdate="false"/><span>${codeDto.mcNm}</span></label>
                                    </c:forEach>
                                </div>
                            </td>
                        </tr>
                        <tr class="trHopeSendDays">
                            <th>희망발송일</th>
                            <td colspan="5">
                                <div class="ui form inline">
                                    <span class="text">주문일 기준</span><input type="text" id="hopeSendDays" name="hopeSendDays" class="ui input small" maxlength="3" style="width:40px; text-align: right;" isUpdate="false"><span class="text">일 이내</span>
                                </div>
                            </td>
                        </tr>
                        <tr id="trSendPeriod_haman">
                            <th rowspan="3">배송기간<span class="text-center text-red"> *</span></th>
                            <td colspan="5" id="tdSendPeriod_haman">
                                <div class="ui form inline dateControl" >
                                    <input hidden id="haman" name="haman"/>
                                    <span class="text" id="txtSendHaman">중앙(함안)</span>
                                    <input type="text" id="sendStartDt_haman" name="sendStartDt_haman" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" isUpdate="false">
                                    <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="sendEndDt_haman" name="sendEndDt_haman" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" isUpdate="false">
                                </div>
                            </td>
                        </tr>
                        <tr id="trSendPeriod_anseong">
                            <td colspan="5" id="tdSendPeriod_anseong">
                                <div class="ui form inline dateControl">
                                    <input hidden id="anseong" name="anseong"/>
                                    <span class="text" id="txtSendAnseong">중앙(안성)</span>
                                    <input type="text" id="sendStartDt_anseong" name="sendStartDt_anseong" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" isUpdate="false">
                                    <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="sendEndDt_anseong" name="sendEndDt_anseong" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" isUpdate="false">
                                </div>
                            </td>
                        </tr>
                        <tr id="trSendPeriod_store">
                            <th id="trSendPeriod" style="display: none;">배송기간<span class="text-center text-red"> *</span></th>
                            <td colspan="5">
                                <span class="ui form inline dateControl">
                                    <input hidden id="store" name="store"/>
                                    <span class="text" class="" id="txtStore">점포　　　</span>
                                    <input type="text" id="sendStartDt_store" name="sendStartDt_store" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" isUpdate="false">
                                    <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="sendEndDt_store" name="sendEndDt_store" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" isUpdate="false">
                                </span>

                                <!-- 요일 설정 -->
                                <label id="labelChkSetYn" class="ui checkbox mg-l-20 mg-r-20" style="display: inline-block; margin-top: 10px;">
                                    <input type="checkbox" id ="chkSetYn" name="chkSetYn" value="N" class="chkSetYn"><span>요일별 추가 설정</span>
                                </label>

                                <span id="chkLine">
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="monYn" name="monYn" value="N" class="chkDay"><span>월</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="tueYn" name="tueYn" value="N" class="chkDay"><span>화</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="wedYn" name="wedYn" value="N" class="chkDay"><span>수</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="thuYn" name="thuYn" value="N" class="chkDay"><span>목</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="friYn" name="friYn" value="N" class="chkDay"><span>금</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="satYn" name="satYn" value="N" class="chkDay"><span>토</span>
                                    </label>
                                    <label class="ui checkbox mg-r-20" style="display: inline-block; margin-top: 10px;">
                                        <input type="checkbox" id ="sunYn" name="sunYn" value="N" class="chkDay"><span>일</span>
                                    </label>
                                </span>
                            </td>
                        </tr>
                    </table>
            </div>
        </form>
        <div class="com wrap-title sub" style="margin-top: 20px;">
            <div class="ui form inline">
                <h3 class="title">적용상품정보 <span id="itemTotalCount" style="display: none" ></span></h3>
                <button type="button" class="ui button small mg-l-5 pull-right" id="deleteItemBtn" style="">삭제</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="uploadBtn" style="">엑셀업로드
                </button><span class="text ui small mg-l-10 mg-r-5 pull-right">|</span>
                <button type="button" class="ui button small mg-l-5 pull-right" id="addItemBtn" style="">추가</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="itemPopBtn" style="">상품검색</button>
                <input type="text" id="itemNm" name="itemNm" class="ui input medium mg-r-5 pull-right" placeholder="상품명" style="width:200px;" readonly>
                <input type="text" id="itemNo" name="itemNo" class="ui input medium mg-r-5 pull-right" placeholder="상품번호" style="width:120px;" readonly>
                <select id="sendPlace" name="sendPlace" class="ui input medium mg-r-5 pull-right" style="width: 100px;">
                    <option value="">발송지</option>
                    <c:forEach var="codeDto" items="${sendPlace}" varStatus="status">
                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="mg-b-5" id="itemListGrid" style="width: 100%; height: 350px;"></div>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/rsvSaleMain.js?${fileVersion}"></script>

<script>
    // 예약 정의 리스트 리얼그리드 기본 정보
    ${rsvSaleListGridBaseInfo}
    // 상품 리스트 리얼그리드 기본 정보
    ${itemListGridBaseInfo}

    var storeTypeJson = ${storeTypeJson};
    var saleTypeJson = ${saleTypeJson};
    var useYnJson = ${useYnJson};
    var sendPlaceJson = ${sendPlaceJson};
</script>

