<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리별 속성관리</h2>
    </div>
    <div class="com wrap-title sub" >

        <div class="ui wrap-table horizontal mg-t-10" style="margin-bottom: 50px;">
            <form id="categorySearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  categoryList.search();">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="500px">
                        <col width="100px">
                        <col width="150px">
                        <col />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>카테고리</th>
                            <td >
                                <select class="ui input medium mg-r-5" id="searchCateCd1" name="searchCateCd1"
                                        style="width:150px;float:left;" data-default="1depth"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5" id="searchCateCd2" name="searchCateCd2"
                                        style="width:150px;float:left;" data-default="2depth"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5" id="searchCateCd3" name="searchCateCd3"
                                        style="width:150px;float:left;" data-default="3depth">
                                </select>
                            </td>
                            <th>등록여부</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="searchResistYn" style="width: 100px" name="searchResistYn" class="ui input medium mg-r-10" >
                                        <option value="">전체</option>
                                        <c:forEach var="codeDto" items="${resistYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="ui form inline" style="float: right;">
                                    <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn" ><i class="fa fa-search"></i> 검색</button>
                                    <span style="margin-left:4px;"></span>
                                    <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>

        </div>
<%--        <div class="com wrap-title">--%>
<%--            <h2 class="title font-malgun" style="width: 49%; float: left;">카테고리정보</h2>--%>
<%--            <h2 class="title font-malgun" style="width: 49%; float: left;">속성분류 매칭 정보</h2>--%>
<%--        </div>--%>

        <div style="width: 48%; height: 600px; float: left;">
            <h3 class="title" style="float:none;">카테고리정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>검색결과 : <span id="categoryTotalCount">0</span>건</h3>
            </div>

            <div class="mg-t-20 mg-b-20" id="categoryListGrid" style="width: 100%; height: 600px;"></div>
        </div>


        <div style="width: 48%; height: 600px;float: right;">
            <h3 class="title" style="float:none;">속성분류 매칭 정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>속성분류 수 : <span id="attributeTotalCount">0</span>건</h3>
            </div>
            <div class="mg-t-20 mg-b-20" id="attributeListGrid" style="width: 100%; height: 350px;"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <form id="setAttributeForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  attributeList.setAttribute();">
                    <input type="hidden" id="scateCd" name="scateCd" value="">
                    <table class="ui table">
                        <colgroup>
                            <col width="170px">
                            <col width="180px">
                            <col width="150px">
                            <col />
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>분류번호</th>
                            <td  colspan="3">
                                <input type="text" id="attrNo" name="attrNo" class="ui input medium mg-r-5" style="width: 100px;" readonly/>

                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">관리분류명</th>
                            <td colspan="3">
                                <select class="ui input medium mg-r-5" id="selectAttrNo" name="selectAttrNo" style="width:300px;float:left;" data-default="선택" disabled>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">우선순위</th>
                            <td>
                                <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 100px;" maxlength="4"/>
                            </td>
                            <th>사용여부</th>
                            <td >
                                <div class="ui form inline">
                                    <select id="useYn" style="width: 100px" name="useYn" class="ui input medium mg-r-10" >
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>노출유형</th>
                            <td  colspan="3">
                                <input type="text" id="dispTypeTxt" name="dispTypeTxt" class="ui input medium mg-r-5" style="width: 100px;" readonly/>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="ui form inline" style="margin-top: 10px;float: right;">
                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setAttributeBtn" >저장</button>
                <span style="margin:4px;"></span>
                <button type="button" class="ui button large mg-r-5 white pull-left" id="resetAttributeBtn">초기화</button>
            </div>
        </div>



    </div>

</div>


<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/categoryAttributeMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>

<script>
    // 브랜드 리스트 리얼그리드 기본 정보
    ${categoryListGridBaseInfo}
    ${attributeListGridBaseInfo}
</script>