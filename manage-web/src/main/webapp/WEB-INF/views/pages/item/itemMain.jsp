<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<!-- script -->
<script type="text/javascript" src="/static/js/item/itemMain.js?${fileVersion}"></script>
<script>
    const mallType = '${mallType}';
</script>
<!-- 점포선택 폼 -->
<div style="display:none;">
    <form id="storeSelectPopForm" name="storeSelectPopForm" method="post">
        <input type="text" name="storeTypePop" value="HYPER"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" id="storeList" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="item.callBack.setStorePrNoList"/>
    </form>
</div>
<!-- 점포선택 폼 -->
<div style="display:none;" class="stopReasonPop" >
    <form id="stopReasonPop" name="stopReasonPopForm" method="post" action="/item/popup/stopReasonPop">
        <input type="text" name="itemStatus" value=""/>
        <input type="text" name="mallType" value=""/>
        <input type="text" name="itemNo" value=""/>
    </form>
</div>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <c:choose>
            <c:when test="${mallType eq 'DS'}">
                <h2 class="title font-malgun">${mallTypeNm} 등록/수정</h2>
                <c:set var="pagePath" value="/WEB-INF/views/pages/item/itemMainDS.jsp"/>
            </c:when>
            <c:when test="${menuType eq 'basic'}">
                <h2 class="title font-malgun">${mallTypeNm} 기본정보</h2>
                <c:set var="pagePath" value="/WEB-INF/views/pages/item/itemMainTDBasic.jsp"/>
            </c:when>
            <c:otherwise>
                <h2 class="title font-malgun">${mallTypeNm} 상세정보</h2>
                <c:set var="pagePath" value="/WEB-INF/views/pages/item/itemMainTDDetail.jsp"/>
            </c:otherwise>
        </c:choose>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1">
            <form id="itemSearchForm" onsubmit="return false;">
                <input type="hidden" name="mallType" value="${mallType}" />
                <c:choose>
                    <c:when test="${mallType eq 'DS'}">
                        <input type="hidden" name="schStoreType" value="DS" />
                        <input type="hidden" name="schStoreId" value="0" />
                    </c:when>
                    <c:when test="${menuType eq 'basic'}">
                        <input type="hidden" name="schStoreId" value="37" />
                    </c:when>
                </c:choose>
                <table class="ui table">
                    <caption>상품 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="45%">
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="chgDt">수정일</option>
                                    <c:if test="${mallType eq 'DS'}">
                                    <option value="saleDt">판매일</option>
                                    </c:if>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemMain.initSearchDate('-30d');">1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="itemMain.initSearchDate('-90d');">3개월</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5" >
                            <div style="text-align: center; " class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan " ><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5" >초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" >엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                    style="width:150px;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <c:if test="${menuType eq 'detail'}">
                        <tr>
                            <th>점포유형</th>
                            <td>
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <c:if test="${codeDto.ref3 eq 'REAL'}">
                                        <c:set var="checked" value="" />
                                        <c:if test="${codeDto.mcCd eq 'HYPER'}">
                                            <c:set var="checked" value="checked" />
                                        </c:if>
                                        <label class="ui radio inline"><input type="radio" name="schStoreType" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                                    </c:if>
                                </c:forEach>
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                        <c:if test="${menuType eq 'detail'}">
                            <th>점포</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="schStoreId" name="schStoreId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID" readonly>
                                    <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                                    <button type="button" class="ui button medium" onclick="store.getStorePop('item.callBack.schStore');" >조회</button>
                                </div>
                            </td>
                        </c:if>
                        <c:if test="${menuType ne 'detail'}">
                        <th>판매업체</th>
                        <td>
                            <div class="ui form inline">
                                <c:choose>
                                    <c:when test="${mallType eq 'DS'}">
                                        <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="판매업체ID" readonly>
                                        <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="판매업체명" readonly>
                                        <button type="button" class="ui button medium" onclick="itemMain.getPartnerPop('item.callBack.schPartner');" >조회</button>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-5" style="width: 100px;" value="homeplus" readonly>
                                        <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input medium mg-r-5" style="width: 150px;" value="홈플러스(주)" readonly>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </td>
                        </c:if>
                        <th>상품유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schItemType" style="width: 180px" name="schItemType" class="ui input medium">
                                    <option value="">전체</option>
                                    <c:choose>
                                        <c:when test="${mallType eq 'DS'}">
                                            <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                                                <c:if test="${codeDto.ref2 ne 'TD'}">
                                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                                                <c:if test="${codeDto.ref2 ne 'DS'}">
                                                    <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <c:if test="${mallType eq 'DS'}">
                        <tr>
                            <th rowspan="3">검색어</th>
                            <td rowspan="3">
                                <div class="ui form inline">
                                    <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 20%; vertical-align: top;">
                                        <option value="itemNo">상품번호</option>
                                        <option value="itemNm">상품명</option>
                                        <option value="regNm">등록자</option>
                                        <option value="chgNm">수정자</option>
                                        <option value="relationNo">상품연관번호</option>
                                    </select>
                                    <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수검색시 Enter또는 ,로 구분" ></textarea>
                                </div>
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                    <c:if test="${mallType ne 'DS'}">
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 20%; vertical-align: top;">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                    <option value="regNm">등록자</option>
                                    <option value="chgNm">수정자</option>
                                    <c:if test="${menuType eq 'basic'}">
                                        <option value="itemParent">parent ID</option>
                                    </c:if>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수검색시 Enter또는 ,로 구분"></textarea>
                            </div>
                        </td>
                    </c:if>
                        <th>상품상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schItemStatus" name="schItemStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:choose>
                                        <c:when test="${mallType eq 'DS'}">
                                            <c:forEach var="codeDto" items="${itemStatus}" varStatus="status">
                                                <c:if test="${codeDto.ref1 eq 'DS'}">
                                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="codeDto" items="${itemStatus}" varStatus="status">
                                                <c:if test="${codeDto.ref2 eq 'TDB'}">
                                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <c:if test="${mallType eq 'DS'}">
                    <tr>
                        <th>연관상품</th>
                        <td>
                            <select id="schRelationItem" name="schRelationItem" class="ui input medium mg-r-10" style="width: 180px">
                                <option value="">전체</option>
                                <option value="Y">연관상품</option>
                            </select>
                        </td>
                    </tr>
                    </c:if>
                    <c:if test="${menuType eq 'basic'}">
                    <tr>
                        <th>상품정보 오입력</th>
                        <td colspan="3">
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" id ="schRequiredEmptyAll" name="schRequiredEmpty" value="Y"><span>전체</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredCateEmpty" class="requiredEmpty" value="Y"><span>카테고리</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredMainImgEmpty" class="requiredEmpty" value="Y"><span>대표이미지</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredListImgEmpty" class="requiredEmpty" value="Y"><span>리스팅이미지</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredLabelImgEmpty" class="requiredEmpty" value="Y"><span>라벨이미지</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredOriginEmpty" class="requiredEmpty" value="Y"><span>원산지</span>
                            </label>
                            <label class="ui checkbox mg-r-20 mg-t-5 mg-b-5" style="display: inline-block">
                                <input type="checkbox" name="schRequiredItemNmEmpty" class="requiredEmpty" value="Y"><span>상품명 미확인</span>
                            </label>
                        </td>
                    </tr>
                    </c:if>
                    <c:if test="${menuType eq 'detail'}">
                        <tr>
                            <th></th><td></td>
                            <th>재고설정</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schStockType" name="schStockType" class="ui input medium" style="width: 180px">
                                        <option value="">전체</option>
                                        <c:forEach var="codeDto" items="${stockType}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </c:if>
                    </tbody>
                </table>
            </form>
        </div>

        <div>
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="itemTotalCount">0</span>건</h3>
                <c:if test="${menuType ne 'detail'}">
                <span id="changeItemStatus" style="margin-left: 40%; font-size: 14px;">• 판매상태 변경
                    <button id="itemStatusS" type="button" class="ui button medium" onclick="itemMain.getStopReason('S')">영구중지</button>
                    <button id="itemStatusP" type="button" class="ui button medium" onclick="itemMain.getStopReason('P')">일시중지</button>
                    <button id="itemStatusA" type="button" class="ui button medium" onclick="itemMain.getStopReason('A')">판매가능</button>
                </span>
                <span id="changeDispYn" class="pull-right" style="font-size: 14px;">• 노출여부 변경
                    <button type="button" class="ui button medium" onclick="itemMain.setItemDispYn('Y')">노출</button>
                    <button type="button" class="ui button medium" onclick="itemMain.setItemDispYn('N')">노출안함</button>
                </span>
                </c:if>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-b-20" id="itemListGrid" style="width: 100%; height: 323px;"></div>
    </div>
    <span id="itemTitleArea" style="display: none" >
        <div class="com wrap-title sub" style="background-color: gray;">
            <h3 id="itemTitle" class="title" style="color: white;padding-top: 5px;padding-left: 3px;">
                <span class="mg-l-5">상품 안내사항 &nbsp; | &nbsp; 상품번호 &nbsp; : &nbsp; </span><span id="itemNoView"></span>, &nbsp; 상품명 &nbsp; : &nbsp; <span id="itemNmView"></span>
            </h3>
        </div>
        <div class="box-notice box-blue">
            <table>
                <colgroup>
                    <col width="15%">
                    <col width="*">
                </colgroup>
                <tr>
                    <td>
                         <ul class="box-notice-content text-size-md" style="height: 100px;">
                            <li class="mg-r-10">
                                진행상태: <span class="itemStatusNm"></span><br>
                                수정일: <span id="itemChgDt"></span>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul class="box-notice-content text-size-md" style="height: 100px;">
                            <li id="statusDesc" class="mg-r-10">
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </span>
    <form id="itemSetForm" onsubmit="return false;">
        <jsp:include page="${pagePath}" />
    </form>

</div>
<script>
    // 상품 리스트 리얼그리드 기본 정보
    ${itemListGridBaseInfo}

    $(document).ready(function() {
        itemMain.init();
        CommonAjaxBlockUI.targetId('searchBtn');
        CommonAjaxBlockUI.targetId('setItemBtn');
        CommonAjaxBlockUI.targetId('setItemTempBtn');
        CommonAjaxBlockUI.targetId('itemStatusA');
    });
</script>