<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">점포상품 강제전시</h2>
    </div>
    <form id="itemDisplaySetForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal ">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="140px">
                    <col width="200px">
                    <col width="140px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>
                        <span class="text-red-star">상품번호</span>
                    </th>
                    <td>
                        <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" maxlength="9" style="width: 150px;" autocomplete="off">
                    </td>

                    <th>
                        <span class="text-red-star">점포타입</span>
                    </th>
                    <td>
                        <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:120px;float:left;">
                            <option value="">선택하세요.</option>
                            <option value="HYPER">HYPER</option>
                            <option value="EXP">EXPRESS</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>
                        <span class="text-red-star">점포코드</span>
                    </th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <input id="storeIds" name="storeIds" type="text" class="ui input medium mg-r-5" style="width: 550px;" autocomplete="off">
                        </span>
                        <div class="text-gray-light mg-t-5">점포코드와 콤마(,)만 입력해주세요.(예시 : 24,37)</div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn">저장</button>
                <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </form>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemForceDisplay.js?${fileVersion}"></script>

<script>

    $(document).ready(function() {
        item.init();
        CommonAjaxBlockUI.global();
    });

</script>



