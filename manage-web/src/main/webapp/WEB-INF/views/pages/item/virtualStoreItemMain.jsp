<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">택배점 취급상품관리</h2>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="storeSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  store.search();">
                <table class="ui table">
                    <caption>검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="350px">
                        <col width="%">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schStoreType" name="schStoreType" style="width:120px;float:left;" >
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${storeType}" varStatus="status">
                                    <c:if test="${code.mcCd eq 'HYPER' or code.mcCd eq 'CLUB'}">
                                        <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <th>점포종류</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schStoreKind" name="schStoreKind" style="width:120px;float:left;">
                                <c:forEach var="code" items="${storeKind}" varStatus="status">
                                    <c:if test="${code.mcCd eq 'DLV'}">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline pull-left">
                                <button type="submit" id="searchBtn" class="ui button large cyan"><i class="fa fa-search"></i>검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button>
                                <span style="margin-left:4px;"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schType" name="schType" style="width:120px;float:left;" data-default="storeNm">
                                <option value="STOREID" selected>점포코드</option>
                                <option value="STORENM">점포명</option>
                            </select>
                            <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5" style="width:120px"; maxlength="20" autocomplete="off">

                        </td>
                        <th>사용여부</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schUseYn" name="schUseYn" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${useYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="storeTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="storeListGrid" style="width: 100%; height: 350px;"></div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 취급 상품 수 : <span id="itemTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="itemListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="ui mg-t-15" style="padding-top: 10px; padding-bottom: 30px;">
        <form id="itemSetForm" name="itemSetForm" onsubmit="return false;">
            <div class="com wrap-title sub">
                <h3 class="title">택배점 취급상품 정보</h3>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>기본정보</caption>
                        <colgroup>
                            <col width="120px">
                            <col width="300px">
                            <col width="120px">
                            <col width="300px">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>택배점포</th>
                            <td>
                                <div class="ui form inline">
                                    <input hidden id="chgYn" name="chgYn"/>
                                    <input hidden id="storeType" name="storeType"/>
                                    <input hidden id="storeId" name="storeId"/>
                                    <span id="spanStoreId" name="spanStoreId" class="ui mg-r-5 text" style="width: 100px;"></span> <%--<span class="mg-r-5 text">|</span>--%>
                                    <span id="spanStoreNm" name="spanStoreNm" class="ui mg-r-5 text" style="width: 200px;"></span>
                                </div>
                            </td>
                            <th>기준점포</th>
                            <td>
                                <div class="ui form inline">
                                    <span id="originStoreId" name="originStoreId" class="ui medium mg-r-5 text" style="width: 100px;"></span> |
                                    <span id="originStoreNm" name="originStoreNm" class="ui medium mg-r-5 text" style="width: 200px;"></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>적용상품</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <input type="text" id="itemNo" name="itemNo" class="ui input medium mg-r-5" style="width: 120px;">
                                    <input type="text" id="itemNm" name="itemNm" class="ui input medium mg-r-5" style="width: 300px;" readonly="true">
                                    <button type="button" class="ui button medium dark-blue" id="itemPopBtn">조회</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>택배점 취급여부</th>
                            <td>
                                <div class="ui form inline">
                                    <label class="ui radio medium inline"><input type="radio" name="dealYn" value="Y" checked><span>취급</span></label>
                                    <label class="ui radio medium inline"><input type="radio" name="dealYn" value="N"><span>취급안함</span></label>
                                </div>
                            </td>
                            <th>가상점 단독판매여부</th>
                            <td>
                                <div class="ui form inline">
                                    <input hidden id="virtualStoreOnlyYn" name="virtualStoreOnlyYn"/>
                                    <span class="text" id="virtualStoreOnlyYnNm"></span>
                                    <span class="text mg-l-20">* 점포상품 상세정보 메뉴에서 설정해주세요.</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>택배점-택배배송정책</th>
                            <td colspan="3">
                                <select id="shipPolicyNo" name="shipPolicyNo" class="ui input medium" style="width: 180px;">
                                    <option value="" selected>선택해주세요</option>
                                    <c:forEach var="shipPolicy" items="${shipPolicyList}" varStatus="status">
                                        <option value="${shipPolicy.shipPolicyNo}">${shipPolicy.shipPolicyNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
            </div>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/virtualStoreItemMain.js?${fileVersion}"></script>

<script>
    // 점포 리스트 리얼그리드 기본 정보
    ${storeListGridBaseInfo}
    // 점포 리스트 리얼그리드 기본 정보
    ${itemListGridBaseInfo}

    var dealYnJson = ${dealYnJson};
    var useYnJson = ${useYnJson};
    var storeTypeJson = ${storeTypeJson};
    var virtualStoreOnlyYnJson = ${virtualStoreOnlyYnJson};
</script>

