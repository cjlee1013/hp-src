<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup large popup-request">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            <i class="fa fa-clone mg-r-5"></i> 판매자 검색
        </h3>
    </div>
    <!-- 검색옵션 -->
    <form id="partnerPopSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) partnerPopup.search();">
        <div class="pd-t-20">

            <div class="ui form inline" style="float:left">
                <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:150px;" data-default="판매자명">
                    <option value="partnerNm">판매자명</option>
                    <option value="partnerId">판매자ID</option>
                </select>
            </div>

            <div class="ui form inline" style="float:left">
                <input name="searchKeyword" id="searchKeyword" type="text" style="width: 430px;" class="ui input medium mg-r-5" autocomplete="off" maxlength="20">
                <button type="button" id="searchBtn" class="ui button medium btn-warning"><i class="fa fa-search"></i> 검색</button>
            </div>
            <!-- clear -->
            <div class="clearfix"></div>
        </div>
    </form>
    <!-- 그리드 영역 -->
    <div class="mg-t-20 mg-b-20" id="partnerSearchPopGrid" style="width: 100%; height: 350px;"></div>

    <!-- 하단 버튼 -->
    <div class="ui form inline mg-t-15 text-center">
        <button type="button" id="selectBtn" class="ui button xlarge btn-danger mg-r-10">선택</button>
        <button type="button" id="closeBtn" class="ui button xlarge">취소</button>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>

    // 파트너검색팝업 리얼그리드 기본 정보
    ${partnerSearchPopGridBaseInfo}

    $(document).ready(function() {
        partnerSearchPopGrid.init();
        partnerPopup.init();
        CommonAjaxBlockUI.global();
    });

    // 파트너검색팝업 그리드
    var partnerSearchPopGrid = {
        gridView : new RealGridJS.GridView("partnerSearchPopGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            partnerSearchPopGrid.initGrid();
            partnerSearchPopGrid.initDataProvider();
            partnerSearchPopGrid.event();
        },
        initGrid : function() {
            partnerSearchPopGrid.gridView.setDataSource(partnerSearchPopGrid.dataProvider);

            partnerSearchPopGrid.gridView.setStyles(partnerSearchPopGridBaseInfo.realgrid.styles);
            partnerSearchPopGrid.gridView.setDisplayOptions(partnerSearchPopGridBaseInfo.realgrid.displayOptions);
            partnerSearchPopGrid.gridView.setColumns(partnerSearchPopGridBaseInfo.realgrid.columns);
            partnerSearchPopGrid.gridView.setOptions(partnerSearchPopGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            partnerSearchPopGrid.dataProvider.setFields(partnerSearchPopGridBaseInfo.dataProvider.fields);
            partnerSearchPopGrid.dataProvider.setOptions(partnerSearchPopGridBaseInfo.dataProvider.options);
        },
        event : function() {
            // 그리드 더블클릭 선택
            partnerSearchPopGrid.gridView.onDataCellDblClicked = function(gridView, index) {
                partnerPopup.selectPartner();
            };
        },
        setData : function(dataList) {
            partnerSearchPopGrid.dataProvider.clearRows();
            partnerSearchPopGrid.dataProvider.setRows(dataList);
        }
    };

    // 파트너검색팝업
    var partnerPopup = {
        init : function() {
            this.bindingEvent();
        },
        bindingEvent : function() {
            $('#searchBtn').click(function() {
                partnerPopup.search();
            });

            $('#selectBtn').click(function() {
                partnerPopup.selectPartner();
            });

            $('#closeBtn').click(function() {
                partnerPopup.cancel();
            });

            $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
        },
        search : function() {
            if ($('#searchKeyword').val() && $.jUtil.isNotAllowInput($('#searchKeyword').val(), ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }
            if($('#searchKeyword').val().length < 2) {
                alert("검색어는 최소 2자리 이상 입력해 주세요.");
                $('#searchKeyword').focus();
                return false;
            }

            CommonAjax.basic({url:'/item/commission/getPartnerList.json?' + $('#partnerPopSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                    partnerSearchPopGrid.setData(res);
                }});
        },
        selectPartner : function() {
            var selectRowId = partnerSearchPopGrid.gridView.getCurrent();

            if (selectRowId.dataRow < 0) {
                alert('검색결과에서 판매자 정보를 선택하세요.');
                return false;
            }

            var rowDataJson = partnerSearchPopGrid.dataProvider.getJsonRow(selectRowId.dataRow);

            $('#partnerId', opener.document).val(rowDataJson.partnerId);
            $('#partnerNm', opener.document).val(rowDataJson.partnerNm);
            this.cancel();
        },
        cancel : function() {
            window.close();
        }
    };
</script>