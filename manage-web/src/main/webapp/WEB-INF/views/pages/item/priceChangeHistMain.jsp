<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매가변경관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="itemSearchForm" onsubmit="return false;">
                <input type="hidden" id="searchCateCd" name="searchCateCd">
                <table class="ui table">
                    <caption>판매가변경관리 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="500px">
                        <col width="100px">
                        <col width="250px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수검색 시 Enter 또는 , 로 구분"></textarea>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="storeType" class="ui input" value="HYPER" checked/><span>HYPER</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="storeType" class="ui input" value="EXP"/><span>EXPRESS</span>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="itemListTotalCount">0</span>건</h3>
        </div>

        <div class="topline"></div>

        <div id="itemListGrid" style="width: 100%; height: 320px;"></div>

    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 취급점포 수 : <span id="itemStoreListTotalCount">0</span>건</h3>
    </div>

    <div class="topline"></div>

    <div id="itemStoreListGrid" style="width: 100%; height: 320px;"></div>
    <br/>
    <div>
        <button class="ui button small font-malgun" id="updateSalePrice" style="float: right;">일괄적용</button>
        <input id="setSalePriceList" name="setSalePriceList" type="text" class="ui input medium mg-r-5" style="float:right; width: 100px;">
        <font style="float: right;">판매가 &nbsp;</font>
    </div>

    <br/>
    <br/>

    <div class="com wrap-title sub">
        <h3 class="title">• 판매가변경 정보</h3>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="priceChangeHistSetForm" onsubmit="return false;">
            <input type="hidden" id="setOriginPrice" name="setOriginPrice">
            <table class="ui table">
                <caption>판매가변경 정보</caption>
                <colgroup>
                    <col width="130px;">
                    <col width="*">
                    <col width="130px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>적용상품</th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="dispItemNo" name="dispItemNo" type="text" class="ui input medium mg-r-5" readonly style="width: 150px;">
                            <input id="dispItemNm" name="dispItemNm" type="text" class="ui input medium mg-r-5" readonly style="width: 250px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>기준점포</th>
                    <td colspan="2">
                        <div class="ui form inline">
                            <input id="dispStoreId" name="dispStoreId" type="text" class="ui input medium mg-r-5" readonly style="width: 70px;">
                            <input id="dispStoreNm" name="dispStoreNm" type="text" class="ui input medium mg-r-5" readonly style="width: 100px;">
                        </div>
                    </td>
                    <th>판매가&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="setSalePrice" id="setSalePrice" class="ui input medium" maxlength="8" style="width:100px">
                            <span class="text">원</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/priceChangeHistMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script>
    // 리얼그리드 기본 정보
    ${itemGridBaseInfo}
    ${itemStoreGridBaseInfo}
</script>