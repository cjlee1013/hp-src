<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">안전재고관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="safetyInventorySearchForm" onsubmit="return false;">
                <input type="hidden" id="searchCateCd" name="searchCateCd">
                <table class="ui table">
                    <caption>안전재고관리 검색</caption>
                    <colgroup>
                        <col width="80px">
                        <col width="560px">
                        <col width="100px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="4">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="searchCateCd1"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="searchCateCd2"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="searchCateCd3"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="searchCateCd4"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수검색 시 Enter 또는 , 로 구분"></textarea>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schUseYn" name="schUseYn" style="width: 100px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="safetyInventoryTotalCount">0</span>건</h3>
        </div>

        <div class="topline"></div>

        <div id="safetyInventoryListGrid" style="width: 100%; height: 320px;"></div>

    </div>

    <br/>

    <div class="com wrap-title sub">
        <h3 class="title">• 안전재고관리정보</h3>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="safetyInventorySetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>안전재고관리정보</caption>
                <colgroup>
                    <col width="160px;">
                    <col width="*">
                    <col width="130px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품번호&nbsp;<span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <div id="selectItemPopup" style="float: left;">
                                <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" style="width: 150px;">
                                <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" readonly style="width: 250px;">
                                <button class="ui button small gray-dark font-malgun" id="addItemPopUp">조회</button>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>HYPER 안전재고</th>
                    <td>
                        <span class="ui form inline">
                <c:forEach var="codeDto" items="${safetyInventoryYn}" varStatus="status">
                    <c:choose>
                        <c:when test="${codeDto.mcCd eq 'N'}">
                            <label class="ui radio inline"><input type="radio" name="hyperUseYn" value="${codeDto.mcCd}" checked/><span class="text">${codeDto.mcNm}</span></label>
                        </c:when>
                        <c:otherwise>
                            <label class="ui radio inline"><input type="radio" name="hyperUseYn" style="float: left;" value="${codeDto.mcCd}"/><span class="text">${codeDto.mcNm}</span></label>
                            <input type="text" id="hyperQty" name="hyperQty" class="ui input medium" style="width: 80px; text-align: right;" maxlength="4" readonly><span class="text mg-r-10"> 개</span>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>EXPRESS 안전재고</th>
                    <td>
                        <span class="ui form inline">
                <c:forEach var="codeDto" items="${safetyInventoryYn}" varStatus="status">
                    <c:choose>
                        <c:when test="${codeDto.mcCd eq 'N'}">
                            <label class="ui radio inline"><input type="radio" name="expressUseYn" value="${codeDto.mcCd}" checked/><span>${codeDto.mcNm}</span></label>
                        </c:when>
                        <c:otherwise>
                            <label class="ui radio inline"><input type="radio" name="expressUseYn" style="float: left;" value="${codeDto.mcCd}"/><span>${codeDto.mcNm}</span></label>
                            <input type="text" id="expressQty" name="expressQty" class="ui input medium" style="width: 80px; text-align: right;" maxlength="4" readonly><span class="text mg-r-10"> 개</span>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                        </span>
                    </td>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 100px;">
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/safetyStockMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonItem.js?${fileVersion}"></script>
<script>
    // 리얼그리드 기본 정보
    ${safetyInventoryGridBaseInfo}
</script>