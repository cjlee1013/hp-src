<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">새벽배송 배너 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="15%">
                        <col width="25%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <select id="searchPeriodType" name="searchPeriodType" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="regDt">등록일</option>
                                <option value="dispDt">전시일</option>
                            </select>
                        </td>
                        <td colspan="4">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                                <span class="text">&nbsp;&nbsp; &nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="auroraBannerMng.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="auroraBannerMng.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="auroraBannerMng.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="auroraBannerMng.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="4">
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 450px;" minlength="2" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="auroraBannerGridCnt">0</span>건</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20 " id="auroraBannerGrid" style="width: 100%; height: 350px;"></div>

    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 새벽배송 배너 등록/수정</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text">* 전시 일자가 중복될 경우 최근 등록된 새벽배송 배너가 노출됩니다. 전시 시작일시 및 종료일시를 정확히 설정해 주세요.</span>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="auroraBannerForm" onsubmit="return false;">
            <table class="ui table">
                <caption>새벽배송 배너 등록/수정</caption>
                <colgroup>
                    <col width="120px;">
                    <col width="*">
                    <col width="120px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>ID <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="bannerId" name="bannerId" type="text" class="ui input medium mg-r-5" style="width: 150px;" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>새벽배송명 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="bannerNm" name="bannerNm" type="text" class="ui input medium mg-r-5" style="width: 350px;" maxlength="100">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <span class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>구분 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <select id="dispKind" style="width:100px" name="dispKind" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 100px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <c:set var="selected" value=""/>
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected"/>
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="bannerTypePc">
                    <th>배너 설정 <span class="text-red">*</span></th>
                    <td style="width: 100px;">
                        <select id="template" style="width: 100px;float: left" name="template" class="ui input medium mg-r-10" >
                            <option value="1">1단</option>
                            <option value="2">2단</option>
                            <option value="3">3단</option>
                            <option value="5">5단</option>
                            <option value="6">6단</option>
                        </select>
                    </td>
                    <td colspan="2">
                        <select id="bannerType" style="width: 150px;float: left" name="bannerType" class="ui input medium mg-r-10" >
                    <c:forEach var="codeDto" items="${auroraBannerType}" varStatus="status">
                        <c:if test="${codeDto.ref1 eq '1'}">
                            <option value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></option>
                        </c:if>
                    </c:forEach>
                        </select>
                    </td>
                </tr>
                <%--배너 1단 입력 창 시작--%>
                <tr class ="bannerDiv1">
                    <input type ="hidden" name="linkList[].linkNo" value="1"/>
                    <th rowspan="3">이미지1 <span class="text-red">*</span></th>
                    <td colspan="4" id="bannerTdTxt1">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N" class="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv1" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv1" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3" id="bannerTdTxt2">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('1')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv1">
                    <td colspan="4">
                        <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType1" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo1">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);" >검색</button>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr class ="bannerDiv1">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="0">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc1">1200*450</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--배너 1단 입력 창 끝--%>
                <%--배너 2단 입력 창 시작--%>
                <tr class ="bannerDiv2" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="2"/>
                    <th rowspan="3">이미지2 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N" class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv2" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv2" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('2')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv2" style="display: none;">
                    <td colspan="4">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType2"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo2">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly>
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly>
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);" >검색</button>
                            </div>
                    </span>
                    </td>
                </tr>
                <tr class ="bannerDiv2" style="display: none;">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="1">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc2">590*450</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--배너 2단 입력 창 끝--%>
                <%--배너 3단 입력 창 시작--%>
                <tr class ="bannerDiv3" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="3"/>
                    <th rowspan="3">이미지3 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv3" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv3" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('3')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv3" style="display: none;">
                    <td colspan="4">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType3" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo3">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly>
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly>
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);">검색</button>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr class ="bannerDiv3" style="display: none;">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="2">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc3">390*450</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너 3단 입력 창 끝--%>
                <%-- 배너 4단 입력 창 시작--%>
                <tr class ="bannerDiv4" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="4"/>
                    <th rowspan="3">이미지4 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv4" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv4" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('4')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv4" style="display: none;">
                    <td colspan="4">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType4" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo4">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly>
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly>
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);">검색</button>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr class ="bannerDiv4" style="display: none;">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="3">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc4">390*220</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너 4단 입력 창 끝--%>
                <%-- 배너 5단 입력 창 시작--%>
                <tr class ="bannerDiv5" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="5"/>
                    <th rowspan="3">이미지5 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv5" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv5" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('5')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv5" style="display: none;">
                    <td colspan="4">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType5" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo5">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly>
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly>
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);">검색</button>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr class ="bannerDiv5" style="display: none;">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="4">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc5">390*220</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너 5단 입력 창 끝--%>
                <%-- 배너 6단 입력 창 시작--%>
                <tr class ="bannerDiv6" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="6"/>
                    <th rowspan="3">이미지6 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input id="bannerMainTitleDiv6" name="linkList[].title" type="text" class="ui input medium mg-r-5 bannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input id="bannerSubTitleDiv6" name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="auroraBannerMng.resetLinkInfo('6')">초기화</button>
                    </td>
                </tr>
                <tr class ="bannerDiv6" style="display: none;">
                    <td colspan="4">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bannerLinkType6" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkType">
                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                            <c:if test="${codeDto.ref1 eq 'TD'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="bannerLinkInfo6">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly>
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly>
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="auroraBannerMng.popLink(this);">검색</button>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr class ="bannerDiv6" style="display: none;">
                    <td colspan="4">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="5">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerImgSizeDesc6">350*200</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너 6단 입력 창 끝--%>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge dark-blue font-malgun" id="setAuroraBannerBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/auroraBannerMain.js?${fileVersion}"></script>

<script>
    var hmpImgUrl = '${hmpImgUrl}';
    var bannerTypeTemplate = ${auroraBannerTypeJson};

    ${auroraBannerGridBaseInfo}
</script>