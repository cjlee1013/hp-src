<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">EXPRESS 샵 카테고리 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                        <col width="*">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>샵 카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lCateCd"
                                    style="width:200px;float:left;" data-default="1depth">
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 180px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left"
                                        id="searchBtn"><i class="fa fa-search"></i> 검색
                                </button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left"
                                        id="searchResetBtn">초기화
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="expCategoryGridCnt">0</span>건</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20 " id="expCategoryGrid" style="width: 100%; height: 350px;"></div>

    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 샵 카테고리 등록/수정</h3>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="expCategoryForm" onsubmit="return false;">
            <input type="hidden" id="parentCateCd" name="parentCateCd">
        <table class="ui table">
            <caption>카테고리 등록/수정</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>카테고리 ID</th>
                <td colspan="4">
                    <div class="ui form inline">
                        <input id="cateCd" name="cateCd" type="text" class="ui input medium mg-r-5" style="width: 150px;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <th>카테고리명</th>
                <td colspan="4">
                    <div class="ui form inline">
                        <input id="cateNm" name="cateNm" type="text" class="ui input medium mg-r-5" style="width: 250px;" maxlength="20">
                        <span class="text">( <span style="color:red;" id="cateNmCnt">0</span> / 20자 )</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>사용여부</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width:150px;">
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>단계 (Depth)</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="depth" name="depth" style="width:100px;">
                            <option selected="" value="">선택</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </td>
                <th>부모카테고리</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-5" id="formCateCd1" name="lCateCd" style="width:200px;float:left;" data-default="1depth"></select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>우선순위</th>
                <td colspan="4">
                    <div class="ui form inline">
                        <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 100px;" maxlength="4">
                    </div>
                </td>
            </tr>
            <tr id="iconMobileTr" style="display: none;">
                <th>
                    <span class="text-red-star">아이콘 ON(M)</span>
                </th>
                <td>
                    <div id="onIconMobileImgDiv">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="onIconMobileDisplayView" style="display:none;">
                                    <button type="button" id="onIconMobileDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="onIconMobileImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="onIconMobileImgUrl" name="onIconMobileImgUrl" value=""/>
                                    <input type="hidden" id="onIconMobileImgWidth" name="onIconMobileImgWidth" value=""/>
                                    <input type="hidden" id="onIconMobileImgHeight" name="onIconMobileImgHeight" value=""/>
                                    <input type="hidden" id="onIconMobileImgChangeYn" name="onIconMobileImgChangeYn" value="N"/>
                                </div>
                                <div class="onIconMobileDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="onIconMobileImgRegBtn" class="ui button medium" onclick="expCategoryMng.img.clickFile('onIconMobile');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 96*96<br>
                                용량 : 2MB 이하<br>
                                파일 : PNG<br>
                            </div>
                        </div>
                    </div>
                </td>
                <th>
                    <span class="text-red-star">아이콘 OFF(M)</span>
                </th>
                <td>
                    <div id="offIconMobileImgDiv">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="offIconMobileDisplayView" style="display:none;">
                                    <button type="button" id="offIconMobileDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="offIconMobileImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="offIconMobileImgUrl" name="offIconMobileImgUrl" value=""/>
                                    <input type="hidden" id="offIconMobileImgWidth" name="offIconMobileImgWidth" value=""/>
                                    <input type="hidden" id="offIconMobileImgHeight" name="offIconMobileImgHeight" value=""/>
                                    <input type="hidden" id="offIconMobileImgChangeYn" name="offIconMobileImgChangeYn" value="N"/>
                                </div>
                                <div class="offIconMobileDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="offIconMobileImgRegBtn" class="ui button medium" onclick="expCategoryMng.img.clickFile('offIconMobile');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 96*96<br>
                                용량 : 2MB 이하<br>
                                파일 : PNG<br>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr id="iconPcTr" style="display: none;">
                <th>
                    <span class="text-red-star">아이콘 ON(PC)</span>
                </th>
                <td>
                    <div id="onIconPcImgDiv">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="onIconPcDisplayView" style="display:none;">
                                    <button type="button" id="onIconPcDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="onIconPcImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="onIconPcImgUrl" name="onIconPcImgUrl" value=""/>
                                    <input type="hidden" id="onIconPcImgWidth" name="onIconPcImgWidth" value=""/>
                                    <input type="hidden" id="onIconPcImgHeight" name="onIconPcImgHeight" value=""/>
                                    <input type="hidden" id="onIconPcImgChangeYn" name="onIconPcImgChangeYn" value="N"/>
                                </div>
                                <div class="onIconPcDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="onIconPcImgRegBtn" class="ui button medium" onclick="expCategoryMng.img.clickFile('onIconPc');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 100*100<br>
                                용량 : 2MB 이하<br>
                                파일 : PNG<br>
                            </div>
                        </div>
                    </div>
                </td>
                <th>
                    <span class="text-red-star">아이콘 OFF(PC)</span>
                </th>
                <td>
                    <div id="offIconPcImgDiv">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="offIconPcDisplayView" style="display:none;">
                                    <button type="button" id="offIconPcDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="offIconPcImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="offIconPcImgUrl" name="offIconPcImgUrl" value=""/>
                                    <input type="hidden" id="offIconPcImgWidth" name="offIconPcImgWidth" value=""/>
                                    <input type="hidden" id="offIconPcImgHeight" name="offIconPcImgHeight" value=""/>
                                    <input type="hidden" id="offIconPcImgChangeYn" name="offIconPcImgChangeYn" value="N"/>
                                </div>
                                <div class="offIconPcDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="offIconPcImgRegBtn" class="ui button medium" onclick="expCategoryMng.img.clickFile('offIconPc');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 100*100<br>
                                용량 : 2MB 이하<br>
                                파일 : PNG<br>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

    <div id="2depthDiv" style="display:none;">
        <div class="com wrap-title sub">
            <h3 class="title">• RMS 카테고리 매핑</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20" id="expCategoryMappingGrid" style="width: 100%; height: 350px;"></div>
        <button class="ui button small gray-light font-malgun" id="delBtn" style="float: left;" onclick="expRmsCategoryMappingMng.delRmsCategory();">삭제</button>
        <div style="float: right;">
            <td colspan="5">
                <select class="ui input medium mg-r-5" id="formMappingCateCd1" name="division"
                        style="width:150px;float:left;" data-default="1depth" onchange="expCategoryMng.changeCategoryMappingSelectBox(1,'formMappingCateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="formMappingCateCd2" name="groupNo"
                        style="width:150px;float:left;" data-default="2depth" onchange="expCategoryMng.changeCategoryMappingSelectBox(2,'formMappingCateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="formMappingCateCd3" name="dept"
                        style="width:150px;float:left;" data-default="3depth" onchange="expCategoryMng.changeCategoryMappingSelectBox(3,'formMappingCateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="formMappingCateCd4" name="classCd"
                        style="width:150px;float:left;" data-default="4depth" onchange="expCategoryMng.changeCategoryMappingSelectBox(4,'formMappingCateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="formMappingCateCd5" name="subclass"
                        style="width:150px;float:left;" data-default="5depth">
                </select>
            </td>&nbsp;&nbsp;
            <button class="ui button small gray-light font-malgun" id="addMappingCategory" onclick="expRmsCategoryMappingMng.addRmsCategory();">추가</button>
        </div>
    </div>

    <br/><br/>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge dark-blue font-malgun" id="setCategoryBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/expCategoryMain.js?${fileVersion}"></script>

<script>
    ${expCategoryGridBaseInfo}
    ${expRmsCategoryMappingGridBaseInfo}
    var hmpImgUrl = '${hmpImgUrl}';
    var hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>