<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">EXPRESS 샵 카테고리 상품 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>EXPRESS 상품 검색</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="10%">
                        <col width="30%">
                        <col width="5%">
                        <col width="20%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>샵 카테고리</th>
                        <td colspan="2">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lcateCd"
                                    style="width:200px;float:left;" data-default="1depth">
                            </select>
                        </td>
                        <th>매핑여부</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchStatusYn" name="searchStatusYn" style="width: 150px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${expStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:150px;float:left;">
                                <option value="itemNo">상품번호</option>
                                <option value="itemNm">상품명</option>
                            </select>
                        </td>
                        <td colspan="4">
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" style="float: left; width: 400px;" maxlength="20" autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="expCategoryItemCnt">0</span>건</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20" id="expCategoryItemGrid" style="width: 100%; height: 350px;"></div>

    </div>

    <div class="com wrap-title sub">
        <h3 class="title">상품 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="expCategoryItemForm" onsubmit="return false;">
            <input type="hidden" id="parentCateCd" name="parentCateCd">
            <table class="ui table">
                <caption>상품 등록/수정</caption>
                <colgroup>
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품 등록</th>
                    <td>
                        <div class="ui form inline">
                            <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" readonly style="width: 150px;">
                            <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" readonly style="width: 150px;">
                            <button class="ui button small gray-light font-malgun" id="addItemPopUp">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>카테고리</th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-5" id="formCateCd1" name="lcateCd"
                                    style="width:200px;float:left;" data-default="1depth"
                                    onchange="javascript:expCategoryItemMng.changeCategorySelectBox(1,'formCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="formCateCd2" name="mcateCd"
                                    style="width:200px;float:left;" data-default="2depth">
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge dark-blue font-malgun" id="setCategoryItemBtn">등록</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/expCategoryItemMain.js?${fileVersion}"></script>

<script>
    ${expCategoryItemGridBaseInfo}
</script>