<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">브랜드 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="brandSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  brandMain.search();">
                <table class="ui table">
                    <caption>브랜드 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="150px">
                        <col width="60%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="float:left;width:120px;" data-default="브랜드명" >
                                <option value="brandNm">브랜드명</option>
                                <option value="brandCode">브랜드코드</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="20" style="width: 400px;" autocomplete="off">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 180px" name="schUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색된 브랜드 : <span id="brandTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="brandListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 브랜드 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="brandSetForm" onsubmit="return false;">
            <table class="ui table" >
                <caption>브랜드 등록/수정</caption>
                <colgroup>
                    <col width="135px">
                    <col width="350px">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>브랜드 코드</th>
                    <td>
                        <div class="ui form inline">
                            <input id="brandNo" name="brandNo" type="text" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                        </div>
                    </td>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>브랜드명 (한글) <span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="brandNm" name="brandNm" type="text" class="ui input medium mg-r-5" style="width: 230px;" maxlength="15">
                            <span class="text">( <span style="color:red;" id="textCountKr">0</span> / 15자 )</span>ㄴ
                        </span>
                    </td>
                    <th>브랜드명 (영문)</th>
                    <td>
                        <span class="ui form inline">
                            <input id="brandNmEng" name="brandNmEng" type="text" class="ui input medium mg-r-5" style="width: 230px;" maxlength="25">
                            <span class="text">( <span style="color:red;" id="textCountEng">0</span> / 25자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>브랜드 소개</th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <textarea id="brandDesc" name="brandDesc" class="ui input mg-r-5" style="float:left;width: 700px; height: 122px;" maxlength="300"></textarea>
                            <span class="text">( <span style="color:red;" id="textCountDesc">0</span> / 300자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>홈페이지 url</th>
                    <td colspan="3">
                    <span class="ui form inline">
                        <input id="siteUrl" name="siteUrl" type="text" class="ui input medium mg-r-5" style="width: 700px;" maxlength="200">
                    </span>
                    </td>
                </tr>
                <tr>
                    <th>상품명 노출여부</th>
                    <td>
                        <span class="ui form inline">
                            <select class="ui input medium mg-r-10" id="itemDispYn" name="itemDispYn" style="width: 100px;">
                                <c:forEach var="codeDto" items="${brandItemDispYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <div id="prodDispDesc" class="text mg-t-5" style="display:none;">(*한글 브랜드명 기준으로 노출됩니다.)</div>
                        </span>
                    </td>
                    <th>전시명 선택</th>
                    <td>
                        <label class="ui radio inline">
                            <input type="radio" name="dispNm" class="ui input medium" value="KOR" checked ><span>한글</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="dispNm" class="ui input medium" value="ENG" "><span>영문</span>
                        </label>

                        <span class="ui form inline">
                            <input name="initial" id="initial" type="hidden" />
                            <select class="ui input medium mg-r-10" id="initialKor" name="initialKor" style="width: 80px;" >
                                <option value="-">-</option>
                                <c:forEach var="codeDto" items="${initialKor}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">´
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>

                            <select class="ui input medium mg-r-10" id="initialEng" name="initialEng" style="width: 80px;display:none">
                                <option value="-">-</option>
                                <c:forEach var="codeDto" items="${initialEng}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBrandBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<iframe style="display:none;" id="brandExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/brandMain.js?${fileVersion}"></script>

<script>
	// 브랜드 리스트 리얼그리드 기본 정보
    ${brandListGridBaseInfo}
</script>
