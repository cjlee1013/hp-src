<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<input type="hidden" name="itemNo" id="itemNo" />
<input type="hidden" name="mallType" id="mallType" value="${mallType}" />
<input type="hidden" name="storeType" id="storeType" />
<div class="com wrap-title sub">
    <h3 class="title">• 판매정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal ">
    <table class="ui table">
        <caption>판매정보</caption>
        <colgroup>
            <col width="140px">
            <col width="30%">
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>
                <span class="text-red-star">판매기간</span>
            </th>
            <td>
                <span class="ui form inline">
                    <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="salePeriodYn" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>
                    <span id="saleStartDtDivId">
                        <input type="hidden" id="originSaleStartDt"/>
                        <input type="text" name="saleStartDt" id="saleStartDt" class="ui input mg-r-5" style="width:140px;"/>
                    </span>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <span id="saleEndDtDivId">
                        <input type="hidden" id="originSaleEndDt"/>
                        <input type="text" name="saleEndDt" id="saleEndDt" class="ui input mg-r-5" style="width:140px;"/>
                    </span>
                </span>
            </td>
            <th>점포유형</th>
            <td colspan="3">
                <span id="storeTypeTxt"></span>
                <span class="ui form inline mg-l-10">
                    <button type="button" id="itemHist" class="ui button medium" >변경이력</button>
                </span>
            </td>
        </tr>
        <tr>
            <th>예약판매설정</th>
            <td colspan="5">
                <div>
                    <span>- 예약정보 : </span><span id="rsvInfoArea"></span>
                </div>
                <div>
                    <span>- 예약판매기간 : </span><span id="rsvDtArea"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>예약판매 부가설정</th>
            <td colspan="5" class="ui form inline">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="rsv.rsvPurchaseLimitYn" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>

                <span class="text">( 예약판매기간 동안의 구매자 1명이 1회에 최대 </span>
                <input type="text" name="rsv.rsvPurchaseLimitQty" id="rsvPurchaseLimitQty" class="ui input medium" style="width:50px;"/>
                <span class="text">개까지 구매가능 )</span>
            </td>
        </tr>
        <tr>
            <th>
                <span>온라인재고설정</span>
            </th>
            <td colspan="5">
                <span class="ui form inline">
                    <span class="text mg-r-10">재고관리유형 : <span id="stockTypeNm"></span></span>
                    <label class="ui checkbox"><input type="checkbox" name="onlineStockYn" value="Y"><span class="text mg-r-5">온라인재고설정</span></label>
                    <input type="text" name="onlineStockStartDt" id="onlineStockStartDt" class="ui input mg-r-5" style="width:140px;"/>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <input type="text" name="onlineStockEndDt" id="onlineStockEndDt" class="ui input mg-r-5" style="width:140px;"/>
                </span>
                <div class="text-gray-light mg-t-5">(온라인재고 설정기간 이외에는 ‘재고관리유형‘의 설정값으로 자동 적용됩니다)</div>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">판매단위수량</span></th>
            <td>
                <span class="ui form inline">
                    <span class="text"><input type="text" name="saleUnit" id="saleUnit" class="ui input " style="width:50px;text-align: center;" maxlength="3"/> 개</span>
                </span>
            </td>
            <th><span class="text-red-star">최소구매수량</span></th>
            <td colspan="3">
                <span class="ui form inline">
                    <span class="text"> 1회 주문 시 <input type="text" name="purchaseMinQty" id="purchaseMinQty" class="ui input " style="width:50px;text-align: center;" maxlength="3"/> 개부터 구매가능</span>
                </span>
            </td>
        </tr>
        <tr>
            <th>구매수량제한</th>
            <td colspan="5">
                <div class="ui form inline mg-t-10">
                    <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="purchaseLimitYn" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>
                    <span id="purchaseLimitArea">
                        <select class="ui input mg-r-5" name="purchaseLimitDuration" id="purchaseLimitDuration" data-default="1회별" style="width:100px;">
                            <c:set var="dfPurchaseLimitDuration" value="" />
                            <c:forEach var="codeDto" items="${limitDuration}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="dfPurchaseLimitDuration" value="${codeDto.mcCd}" />
                                </c:if>
                                <option value="${codeDto.mcCd}" data-reference1="${codeDto.ref1}" data-reference2="${codeDto.ref2}" data-reference3="${codeDto.ref3}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <span class="text">구매자 1명이 </span>
                        <span class="text purchaseLimitDuration purchaseLimitDurationP">1회에 </span>
                        <input type="text" name="purchaseLimitDay" id="purchaseLimitDay" class="ui input medium  purchaseLimitDuration purchaseLimitDurationO" value="1" style="width:50px;" maxlength="3"/>
                        <span class="text purchaseLimitDuration purchaseLimitDurationO">일 동안 </span>
                        <span class="text">최대 </span>
                        <input type="text" name="purchaseLimitQty" id="purchaseLimitQty" class="ui input " value="1" style="width:60px;" maxlength="3"/>
                        <span class="text">개까지 구매가능</span>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <th>상품용도 옵션</th>
            <td colspan="5" class="ui form inline">
                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="optSelUseYn" class="medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr id="optSelUseYnArea">
            <th>상품용도 옵션목록</th>
            <td colspan="5">
                옵션 수 : <span id="optTotalCount">0</span>개
                <div class="mg-t-20 mg-b-20" id="optListGrid" style="width: 100%; height: 350px;"></div>
                <span class="pull-right form ui inline">
                    <span class="text"><span id="optStoreTypeNm"></span> 전시여부</span>
                    <span>
                        <select class="ui input mg-l-5 mg-r-5" name="optDispYn" id="optDispYn" style="width:100px;">
                            <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                <option value="${codeDto.mcCd}" >${codeDto.ref2}</option>
                            </c:forEach>
                        </select>
                        <button type="button" class="ui button medium" onclick="item.opt.setDispYn();">일괄적용</button>
                    </span>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 점포정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table id="shipArea" class="ui table">
        <caption>점포정보</caption>
        <colgroup>
            <col width="140px">
            <col width="12.5%">
            <col width="120px">
            <col width="12.5%">
            <col width="130px">
            <col width="12.5%">
            <col width="90px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <td colspan="8">
                점포 수 : <span id="itemShipTotalCount">0</span>개
                <span class="pull-right ui form inline">
                    <button type="button" id="storeListGridCopy" class="ui button medium" >Text 복사</button>
                </span>
                <div class="mg-t-20 mg-b-20" id="storeListGrid" style="width: 100%; height: 324px;"></div>
                <span class="pull-right ui form inline">
                    <span class="text">온라인 재고수량</span>
                    <input type="text" name="setStockQty" id="setStockQty" class="ui input" style="width:100px;" maxlength="6" data-comma-count="1"/>
                    <span class="text">취급중지</span>
                    <span>
                        <select class="ui input mg-l-5 mg-r-5" name="setStopDealYn" id="setStopDealYn" style="width:100px;">
                            <option value="" >선택</option>
                            <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </span>
                    <button type="button" class="ui button medium" onclick="item.ship.setStockDealYn()">일괄적용</button>
                </span>
                <br><br><br>
                <span class="pull-right ui form inline">
                    <span class="text">중지기간</span>
                        <select class="ui input mg-r-5" name="setStopLimitYn" id="setStopLimitYn" style="width:100px;" disabled>
                        <c:forEach var="codeDto" items="${stopLimitYn}" varStatus="status">
                            <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                        </c:forEach>
                        </select>
                    <input type="text" name="setStopStartDt" id="setStopStartDt" class="ui input mg-r-5" style="width:90px;" disabled/>
                    <span class="text mg-r-5 mg-l-5">&nbsp;~&nbsp;</span>
                    <input type="text" name="setStopEndDt" id="setStopEndDt" class="ui input mg-r-5" style="width:90px;" disabled/>&nbsp;&nbsp;
                    <span class="text">중지사유</span>
                    <input type="text" name="setStopReason" id="setStopReason" class="ui input mg-r-5" style="width:300px;" maxlength="30" disabled/>
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <span class="text-gray-light">※ 점포별 정보는 오른쪽 하단의 '적용' 버튼을 클릭하여 점포리스트에 적용 후, 상품정보로 최종 저장해 주세요</span>
            </td>
        </tr>
        <tr>
            <th>점포ID</th>
            <td>
                <span id="storeId" class="text"></span>
            </td>
            <th>점포명</th>
            <td>
                <span id="storeNm" class="text" ></span>
            </td>
            <th>점포유형</th>
            <td>
                <span id="storeTypeNm" class="text" ></span>
            </td>
            <th>점포구분</th>
            <td>
                <span id="storeKindNm" class="text" ></span>
            </td>
        </tr>
        <tr>
            <th>재고수량</th>
            <td colspan="3">
                <span class="ui form inline">
                    <input type="text" name="stockQty" id="stockQty" class="ui input " style="width:100px;" maxlength="9" data-comma-count="1"/>
                    <span class="text mg-l-5 mg-r-10">개</span>
                    <label id="changeStockQtyLabel" class="ui checkbox mg-r-5 " style="display: none;">
                        <input type="checkbox" id="changeStockQtyCheck" name="changeStockQty" value="Y"><span class="text">수량변경</span>
                    </label>
                </span>
            </td>
            <th>판매가</th>
            <td colspan="3">
                <span id="salePrice" class="text" ></span>
            </td>
        </tr>
        <tr>
            <th>온라인 취급상태</th>
            <td colspan="3">
                <span id="pfrYnNm" class="text" ></span>
            </td>
            <th>가상점 단독판매</th>
            <td colspan="3">
                <span class="ui form inline">
                    <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="virtualStoreOnlyYn" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>
                </span>
            </td>
        </tr>
        <tr>
            <th>취급중지</th>
            <td colspan="7">
                <span class="ui form inline">
                    <c:forEach var="codeDto" items="${stopDealYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="stopDealYn" value="${codeDto.mcCd}" ${checked} ${codeDto.ref2}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>
                    <span class="text">- 중지사유 </span>
                    <input type="text" name="stopReason" id="stopReason" class="ui input" style="width:200px;" maxlength="30">
                </span>
            </td>
        </tr>
        <tr>
            <th>중지기간</th>
            <td colspan="7">
                <span class="ui form inline">
                    <select class="ui input mg-r-5" name="stopLimitYn" id="stopLimitYn" style="width:150px;">
                        <option value="" >선택</option>
                        <c:forEach var="codeDto" items="${stopLimitYn}" varStatus="status">
                            <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                    <input type="text" name="stopStartDt" id="stopStartDt" class="ui input mg-r-5" style="width:140px;"/>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <input type="text" name="stopEndDt" id="stopEndDt" class="ui input mg-r-5" style="width:140px;"/>
                </span>
            </td>
        </tr>
        <tr id="drctTr" style="display: none;">
            <th>자차</th>
            <td colspan="7">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="drctYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <select class="ui input mg-r-5 shipSelect" name="drctShipPolicyNo" id="drctShipPolicyNo" data-default="선택해주세요" style="width:200px;">
                        <option value="">선택해주세요</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr id="dlvTr" style="display: none;">
            <th>택배</th>
            <td colspan="7">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="dlvYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach><span class="text" ></span>
                <span class="ui form inline">
                    <select class="ui input mg-r-5 shipSelect" name="dlvShipPolicyNo" id="dlvShipPolicyNo" data-default="선택해주세요" style="width:200px;">
                        <option value="">선택해주세요</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr id="quickTr" style="display: none;">
            <th>퀵</th>
            <td colspan="7">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="quickYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <select class="ui input mg-r-5 shipSelect" name="quickShipPolicyNo" id="quickShipPolicyNo" data-default="선택해주세요" style="width:200px;">
                        <option value="">선택해주세요</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr id="pickTr" style="display: none;">
            <th>픽업</th>
            <td colspan="7">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="pickYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <select class="ui input mg-r-5 shipSelect" name="pickShipPolicyNo" id="pickShipPolicyNo" data-default="선택해주세요" style="width:200px;">
                        <option value="">선택해주세요</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr>
            <th>안심번호 사용여부</th>
            <td colspan="7">
                <span id="safeNumberUseYnNm" class="text-normal"></span>
                <span class="pull-right text-gray-light">('일반점'은 '선물세트' 예약판매기간 동안 '택배' 배송방법으로 자동 전환됩니다.)</span>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <span class="pull-right mg-t-10">
                    <button type="button" class="ui button medium gray-dark" onclick="item.ship.setShip();">적용</button>
                    <button type="button" class="ui button medium" onclick="if (confirm('등록된 정보를 모두 삭제하시겠습니까?')) {item.ship.getDetail();}">초기화</button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn" onclick="item.setItemTD();">저장</button>
        <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        <button type="button" class="ui button xlarge white font-malgun">미리보기</button>
    </span>
</div>

<script>

    const menuType = "${menuType}";
    const rsvDetailAreaHtml = $('#rsvDetailArea').clone();

    // 옵션 그리드
    ${optListGridBaseInfo}
    // 배송정보 그리드
    ${storeListGridBaseInfo}

    $(document).ready(function() {
        itemListGrid.init();
        optListGrid.init();
        storeListGrid.init();
        item.init();
    });
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/item/itemMainGrid.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainGridTDDetail.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainTDDetail.js?${fileVersion}"></script>