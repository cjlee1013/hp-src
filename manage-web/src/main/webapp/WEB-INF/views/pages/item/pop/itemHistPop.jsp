<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
	.popup-request {margin: 20px;}
	.tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
	.tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
	.tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
	.tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
	.wrap-table td {background-color: white; word-break:break-all;}
	.vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
	<div class="com popup-wrap-title">
		<h3 class="title pull-left">
			<i class="fa fa-clone mg-r-5"></i> 상품 변경이력
		</h3>
	</div>
	<div class="tabui-wrap pd-t-20">
		<div class="tabui medium col-02">
			<div id="itemHistGrid" style="width: 100%; height: 350px;"></div>
			<button id="moreHist" class="ui button large mg-t-10" style="width: 100%;" >더보기</button>

		</div>
	</div>
	<div id="histDetail" style="display: none;">
		<div  class="com wrap-title" style="width: 70%" >
			<ul class="tab_vicinity" style="float:left;">
				<li class="on" data-type="basic"><a href="#" class="tab_cnt">기본정보</a></li>
				<li data-type="sale"><a href="#" class="tab_cnt">판매정보</a></li>
				<li data-type="ship"><a href="#" class="tab_cnt">배송정보</a></li>
				<li data-type="optItem"><a href="#" class="tab_cnt">텍스트옵션 및 용도옵션정보</a></li>
				<li data-type="img"><a href="#" class="tab_cnt">이미지정보</a></li>
				<li data-type="etc"><a href="#" class="tab_cnt">부가정보</a></li>
				<li data-type="etcItem"><a href="#" class="tab_cnt">상품고시</a></li>
			</ul>
		</div>
		<div id="histDetailTable" class="ui wrap-table ">
			<table class="ui table">
				<colgroup><col width="100%"></colgroup>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>


<script>
	var RESDATA = {};

	${itemHistGridBaseInfo}

	var itemNo			= '${itemNo}';
	var storeType		= '${storeType}';

	var detailTable = {
		basic	: { // 기본정보
			'html'		: '', // Html 여부
			'mapKey'		: ['false'],
			'value'			: [
				{
					'상품상태'			: 'itemStatusNm',
					'상품명'				: 'itemNm',
					'상품유형'			: 'itemTypeNm',
					'카테고리 대분류'		: 'lcateNm',
					'카테고리 중분류'		: 'mcateNm',
					'카테고리 소분류'		: 'scateNm',
					'카테고리 세분류'		: 'dcateNm',
					'판매업체명(ID)'		: 'partnerId',
					'브랜드'				: 'brandNm',
					'제조사'				: 'makerNm',
					'노출여부'			: 'dispYnNm',
					'판매자 상품코드'			: 'sellerItemCd'
				}
			]
		},
		sale : { // 판매정보
			'html'		: '', // Html 여부
			'mapKey'		: ['false'],
			'value'			: [
				{
					'과세여부'						: 'taxYn',
					'정상가격'						: 'originPrice',
					'판매가격'						: 'salePrice',
					'매입가격'						: 'purchasePrice',
					'수수료율(VAT 포함)'				: 'commissionRate',
					'수수료원(VAT 포함)'				: 'commissionPrice',
					'할인여부'						: 'dcYn',
					'할인-가격'						: 'dcPrice',
					'할인-특정할인기간여부'				: 'dcPeriodYn',
					'할인-시작일시'					: 'dcStartDt',
					'할인-종료일시'					: 'dcEndDt',
					'할인-수수료율'					: 'dcCommissionRate',
					'할인-수수료액'					: 'dcCommissionPrice',
					'재고수량'						: 'stockQty',
					'판매기간-시작시간'					: 'saleStartDt',
					'판매기간-종료시간'					: 'saleEndDt',
					'예약판매사용여부'					: 'rsvYn',
					'예약판매시작일시'					: 'rsvStartDt',
					'예약판매종료일시'					: 'rsvEndDt',
					'예약상품출하시작일시'				: 'shipStartDt',
					'판매단위수량'						: 'saleUnit',
					'최소구매수량'						: 'purchaseMinQty',
					'구매수량제한'						: 'purchaseLimitDurationNm',
					'구매수량제한 - 일'				: 'purchaseLimitDay',
					'구매수량제한 - 개수'				: 'purchaseLimitQty',
					'장바구니제한'						: 'cartLimitYn'
				}
			]
		},
		ship : { // 배송정보
			'html'		: '', // Html 여부
			'mapKey'		: ['false'],
			'value'			: [
				{
					'배송정보 번호' 			: 'shipPolicyNo',
					'출고기한 (일)'			: 'releaseDay',
					'출고기한 (시간)' 			: 'releaseTime',
                    '출고기한 (주말, 공휴일 여부)'	: 'holidayExceptYn',
					'반품/교환 배송비 (편도)'		: 'claimShipFee',
                    '출고지-우편번호'			: 'releaseZipcode',
                    '출고지-기본주소(지번)'		: 'releaseAddr1',
                    '출고지-상세주소(지번)'		: 'releaseAddr2',
                    '회수지-우편번호'			: 'returnZipcode',
                    '회수지-기본주소(지번)'		: 'returnAddr1',
                    '회수지-상세주소(지번)'		: 'returnAddr2',
					'배송정보명'				: 'shipPolicyNm',
					'배송유형'				: 'shipKindNm',
					'배송방법'				: 'shipMethodNm',
					'배송비종류'				: 'shipTypeNm',
					'배송비'					: 'shipFee',
					'배송비 결제방식'			: 'prepaymentYnNm',
					'배송비 노출여부'			: 'shipDispYnNm'
				}
			]
		},
		img : { // 이미지정보
			'html'		: '', // Html 여부
			'colgroup'		: '<col width="150px"><col width="300px"><col width="150px">',
			'mapKey'		: ['imgListMap'],
			'value'			: [
				{
					'이미지유형'		: 'imgTypeNm',
					'이미지URL'		: 'imgUrl',
					'대표이미지'		: 'mainYn',
					'우선순위'		: 'priority'
				}
			]
		},
		optItem	: { // 옵션
			'html'		: '', // Html 여부
			'colgroup'		: '<col width="150px"><col width="150px"><col width="150px"><col width="*">',
			'mapKey'		: ['false', 'optListMap'],
			'value'			: [
					{
						'용도 옵션사용여부': 'optSelUseYnNm',
						'용도 옵션타이틀'	: 'opt1Title',
						'텍스트 옵션사용여부': 'optTxtUseYn',
						'텍스트 옵션단계'	: 'optTextDepth',
						'텍스트 1단계타이틀'	: 'opt1Text',
						'텍스트 2단계타이틀'	: 'opt2Text'
					},
					{
						'옵션번호'			: 'optNo',
						'1단계 옵션값'		: 'opt1Val',
						'노출여부'			: 'dispYnNm',
						'우선순위'			: 'priority'
					}
			]
		},
		etc : {
			'html'		: '', // Html 여부
			'colgroup'		: '<col width="300px"><col width="300px"><col width="*">',
			'mapKey'		: ['false', 'proofFileListMap', 'certListMap'],
			'value'			: [
					{
						// '안전인증|isCert'			: 'certListMap',
						'성인상품유형'				: 'adultTypeNm',
						'이미지 노출여부'			: 'imgDispYn',
						'선물하기사용여부'			: 'giftYn',
						'가격비교 사이트 등록여부'	: 'epYn',
						'검색 키워드'				: 'srchKeyword',
						'해외배송 대행여부'			: 'globalAgcyYn'
					},
					{
						'파일구분'				: 'fileTypeNm',
						'파일명'					: 'fileNm',
                        '파일URL'                : 'fileUrl'
					},
					{
						'인증여부'				: 'isCertNm',
						'인증그룹'				: 'certGroupNm',
						'면제유형'                : 'certExemptTypeNm',
						'인증유형'                : 'certTypeNm',
						'인증번호'                : 'certNo'
					}
			]
		},
		etcItem : {
			'html'		: '', // Html 여부
			'colgroup'					: '<col width="150px"><col width="400px"><col width="*">',
			'mapKey'					: ['false', 'noticeListMap'],
			'value'						: [
				{
					'상품군'				: 'gnoticeNm',
				},
				{
					'고시명'				: 'noticeNm',
					'정보고시항목명'		: 'noticeDesc',
					'정보고시항목명(TD용)'	: 'noticeHtml'
				}
			]
		}
	};


    // 팝업 html
    var itemHistPop = {
        popContsHtml	: '',
		lastSeq			: '',
		pageNo 			: 0,
        init : function() {
            this.event();
			itemHistPop.getHist();
        },
		initHtml : function() {
			for (var key in detailTable) {
				detailTable[key].html = '';
			}
		},
		setTab : function(_this) {
			$('.tab_vicinity li').removeClass('on');
			if (!_this) {
				$('.tab_vicinity li').eq(0).addClass("on");
			} else {
				_this.addClass("on");
				itemHistPop.setDetailTable(_this.data('type'));
			}
		},
		event : function() {
			$('.closeBtn').on('click', function() {
				itemHistPop.close();
			});

			$('.tab_vicinity li').on('click', function () {
				itemHistPop.setTab($(this));
			});

			$('#moreHist').on('click', function() {
				itemHistPop.getHist(true);
			});
		},
		getHist : function(isMore) {
			CommonAjax.basic({
				url : "/item/getItemHistGroup.json?itemNo="+itemNo+"&pageNo="+itemHistPop.pageNo+"&storeType="+storeType,
				method : 'get',
				callbackFunc : function(res) {
					if (res.length > 0) {
						if (isMore) {
							itemHistGrid.dataProvider.addRows(res);
						} else {
							itemHistGrid.setData(res);
						}

						if (res.length > 10) {
							itemHistPop.pageNo++;
							itemHistPop.deleteLastGrid();
						} else {
							$('#moreHist').hide();
							let rowDataJson = itemHistGrid.dataProvider.getJsonRow(itemHistGrid.gridView.getItemCount() - 1);
							itemHistPop.lastSeq	= rowDataJson.id;
						}
					}
				}
			});
		},
		deleteLastGrid : function() {
        	let lastIdx = itemHistGrid.gridView.getItemCount() - 1;
			let rowDataJson = itemHistGrid.dataProvider.getJsonRow(lastIdx);

			itemHistPop.lastSeq	= rowDataJson.id;
			itemHistGrid.dataProvider.removeRow(lastIdx);
		},
        setDetail : function(rowId) {

			let rowDataJson = itemHistGrid.dataProvider.getJsonRow(rowId);

			var lastRowId = itemHistGrid.gridView.getItemCount() - 1;
			var selectHistSeq = rowDataJson.id;
			var prevHistSeq = 0;

			if (rowId == lastRowId) {
				prevHistSeq = itemHistPop.lastSeq;
			} else {
				// 이전 인덱스 추출
				let rowDataJsonPrev = itemHistGrid.dataProvider.getJsonRow(rowId+1);
				prevHistSeq = rowDataJsonPrev.id;
			}

			CommonAjax.basic({
				url : '/item/getItemHistSelectDiff.json?selectHistSeq='+selectHistSeq+'&prevHistSeq='+prevHistSeq,
				method : 'get',
				callbackFunc : function(res) {
					RESDATA = res;
					itemHistPop.setDetailTable('basic', res);
					itemHistPop.setTab();
					$('#histDetail').fadeIn(500);
				}
			});
        },
		setDetailTable : function(type, data) {
			var retHtml = '';
			var vColgroup = '<col width="10%"><col width="90%">';
			var colgroup = detailTable[type].hasOwnProperty('colgroup') ? detailTable[type]['colgroup'] : '<col width="10%"><col width="90%">';

			if (!data) {
				data = RESDATA;
			} else {
				itemHistPop.initHtml();
			}

			if (detailTable[type]['html'] == '') {

				var headerHtml = [];
				var tbodyHtml = '';

				var lastHtml = '</tbody></table></td></tr>';

				// 헤더생성
				for (var i in detailTable[type]['value']) {
					headerHtml[i] = '<tr>';
					for (var key in detailTable[type]['value'][i]) {
						headerHtml[i] += '<th>' + key + '</th>';
					}
					headerHtml[i]+= '</tr>';
				}

				// 바디생성
				for (var i in detailTable[type]['mapKey']) {
					if (detailTable[type]['mapKey'][i]=='false') {
						// 세로 테이블
						retHtml += '<tr><td><table class="vertical-table"><tbody>' + vColgroup;
						for (var key in detailTable[type]['value'][i]) {
							// 배열
							if (typeof data[detailTable[type]['value'][i][key]] == 'object') {
								var thArr = key.split('|');

								retHtml += '<tr><th>' + thArr[0] + '</th><td>';

								for (var objKey in data[detailTable[type]['value'][i][key]]) {
									if (data[detailTable[type]['value'][i][key]][objKey][thArr[1]] == '-') {
										continue;
									} else {
										retHtml += data[detailTable[type]['value'][i][key]][objKey][thArr[1]] + ', ';
									}
								}

								retHtml += '</td></tr>';
							} else {
								// 스트링
								retHtml += '<tr><th>' + key + '</th><td>' + data[detailTable[type]['value'][i][key]] + '</td></tr>';
							}
						}
					} else {
						// 가로 테이블
						if (data[detailTable[type]['mapKey'][i]] == null || Object.keys(data[detailTable[type]['mapKey'][i]]).length == 0) {
							continue;
						}

						if (headerHtml[i].length)
							retHtml += ('<tr><td><table class="horizon-table"><tbody>' + colgroup + headerHtml[i]);
						for (var key in data[detailTable[type]['mapKey'][i]]) {
							tbodyHtml = '<tr>';
							for (var optKey in detailTable[type]['value'][i]) {
								tbodyHtml += '<td>' + data[detailTable[type]['mapKey'][i]][key][detailTable[type]['value'][i][optKey]] + '</td>';
							}
							tbodyHtml += '</tr>';
							retHtml += tbodyHtml;
						}
					}

					retHtml += lastHtml;
				}

				detailTable[type]['html'] = retHtml;
			} else {
				retHtml = detailTable[type]['html'];
			}

			$('#histDetailTable tbody').html(retHtml);
		},
		close : function() {
			window.close();
		}
    };

    $(document).ready(function() {
		CommonAjaxBlockUI.global();
		itemHistPop.init();
		itemHistGrid.init();
    });

	/**
	 * 상품관리 > 셀러상품관리/점포상품관리 > 변경이력 > 공통그리드 (TD/DS)
	 */
	let itemHistGrid = {
		gridView : new RealGridJS.GridView("itemHistGrid"),
		dataProvider : new RealGridJS.LocalDataProvider(),
		init : function() {
			itemHistGrid.initGrid();
			itemHistGrid.initDataProvider();
			itemHistGrid.event();
		},
		initGrid : function() {
			itemHistGrid.gridView.setDataSource(itemHistGrid.dataProvider);

			itemHistGrid.gridView.setStyles(itemHistGridBaseInfo.realgrid.styles);
			itemHistGrid.gridView.setDisplayOptions(itemHistGridBaseInfo.realgrid.displayOptions);
			itemHistGrid.gridView.setColumns(itemHistGridBaseInfo.realgrid.columns);
			itemHistGrid.gridView.setOptions(itemHistGridBaseInfo.realgrid.options);
		},
		initDataProvider : function() {
			itemHistGrid.dataProvider.setFields(itemHistGridBaseInfo.dataProvider.fields);
			itemHistGrid.dataProvider.setOptions(itemHistGridBaseInfo.dataProvider.options);
		},
		event : function() {
			// 그리드 선택
			itemHistGrid.gridView.onDataCellClicked = function(gridView, index) {
				$('#histDetail').hide();
				itemHistPop.setDetail(index.dataRow);
			};
		},
		setData : function(dataList) {
			itemHistGrid.dataProvider.clearRows();
			itemHistGrid.dataProvider.setRows(dataList);
		}
	};

</script>