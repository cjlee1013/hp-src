<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">점포상품 취급중지 일괄설정</h2>
    </div>

    <%-- 점포별 취급중지 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>점포상품 취급중지 일괄설정 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="72%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                <c:if test="${storeType.ref1 eq 'home'}">
                                    <c:set var="checked" value="" />
                                    <c:if test="${storeType.ref2 eq 'df'}">
                                        <c:set var="checked" value="checked" />
                                    </c:if>
                                    <label class="ui radio inline"><input type="radio" name="storeType" class="ui input" value="${storeType.mcCd}" ${checked}/><span>${storeType.mcNm}</span></label>
                                </c:if>
                            </c:forEach>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="submit" id="schBtn" class="ui button large cyan mg-r-5"><i class="fa fa-search"></i> 검색</button><BR>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5" >초기화</button><BR>
                                <button type="button" id="excelDownBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상품번호</th>
                        <td>
                            <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수검색 시 Enter 또는 , 로 구분"></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div>
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="stopDealTotalCnt">0</span>건</h3>
                <span class="pull-right" style="font-size: 14px;">
                    <button type="button" class="ui button medium" onclick="stopDealExcel.openExcelPopup();">일괄등록</button>
                </span>
            </div>
            <div class="topline"></div>
        </div>
        <div id="stopDealGrid" style="width: 100%; height: 470px;"></div>
    </div>
</div>

<script>
    // 그리드 정보
    ${stopDealGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/stopDealMain.js?${fileVersion}"></script>