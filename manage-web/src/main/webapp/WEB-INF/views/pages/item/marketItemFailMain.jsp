<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">외부연동 연동실패</h2>
        <button style="margin-top: -4px; margin-left: 9px;" id="btn_helpPop" type="button" class="ui button medium">도움말</button>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="marketItemFailSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>연동실패 검색</caption>
                    <colgroup>
                        <col width="15%">
                        <col width="45%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>전송일시</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemFail.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemFail.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemFail.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="marketItemFail.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" >엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>외부연동 사이트</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schPartner" name="schPartner" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="" selected>전체</option>
                                    <option value="naver">N마트</option>
                                    <option value="eleven">11번가</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>실패 사유</th>
                        <td>
                            <div class="ui form inline">
                                <select id="failReason" name="failReason" class="ui input medium mg-r-10" style="width: 250px">
                                    <option value="" selected>전체</option>
                                    <option value="1002">1002 : 마스터 상품정보 조회 실패</option>
                                    <option value="1003">1003 : 취급중지, 상품판매중지</option>
                                    <option value="1004">1004 : 카테고리 미맵핑상태</option>
                                    <option value="1005">1005 : 가상점 단독판매상품 판매중지처리</option>
                                    <option value="1006">1006 : 예약상품 판매중지처리</option>
                                    <option value="1007">1007 : 이미지 필수값 확인</option>
                                    <option value="1008">1008 : 성인상품 판매중지처리</option>
                                    <option value="1009">1009 : 외부연동 연동설정 중지 상품</option>
                                    <option value="2002">2002 : 지점별 가격 정보 조회 실패</option>
                                    <option value="1014">마켓사유로 외부연동 실패</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>


        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="marketItemFailTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="marketItemFailListGrid" style="width: 100%; height: 300px;"></div>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/marketItemFailMain.js?${fileVersion}"></script>

<script>
    //연동실패 Realgrid
    ${marketItemFailListGridBaseInfo}
</script>
