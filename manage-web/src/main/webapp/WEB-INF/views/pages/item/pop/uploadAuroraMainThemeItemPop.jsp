<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun" style="float: none">상품 일괄등록</h2>
    </div>

    <!-- 파일업로드 form -->
    <form id="fileUploadForm">
        <input type="hidden" id="fileType" name="fileType" value="${fileType}">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>파일등록</caption>
                <colgroup>
                    <col width="20%">
                    <col width="70%">
                    <col width="10%">
                </colgroup>
                <tbody>
                <tr id="trTemplate">
                    <th>업로드양식</th>
                    <td>
                        <button type="button" id="downloadTemplate" class="ui button medium btn-warning"><i class="fa fa-search"></i>다운로드</button><br/>
                    </td>
                </tr>
                <tr>
                    <th>파일등록</th>
                    <td>
                        <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="auroraMainThemeExcelPop.getFile($(this))" style="display: none">
                        <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly>
                    </td>
                    <td>
                        <button type="button" id="schUploadFile" class="ui button medium">파일찾기</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <div class="mg-t-10">
        <ul class="sub-menu">
            <li>Excel 파일 작성시 유의사항</li>
            <li><span style="color: #ee5555; ">* 일괄등록 1회 수행시 최대 상품 1,000개로 제한합니다.</span></li>
            <li>* 저장 파일명은 : [*.xlsx]</li>
        </ul>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="selectBtn" class="ui button xlarge btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript = '${callback}';
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/pop/auroraMainThemeUploadPop.js?v=${fileVersion}"></script>