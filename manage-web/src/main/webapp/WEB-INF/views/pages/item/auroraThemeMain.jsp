<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">새벽배송 테마상품 관리</h2>
    </div>
    <br>
    <div style="width: 49%; height: 350px; float: left;">
        <h3 class="title" style="float:none;">테마 정보</h3>
        <div class="topline"></div>
        <br/>
        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="auroraTheme.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="auroraTheme.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="auroraTheme.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="auroraTheme.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>
        <div class="mg-t-20 mg-b-20" id="auroraThemeListGrid" style="width: 100%; height: 350px;"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="170px;">
                    <col width="*">
                    <col width="135px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>테마 ID</th>
                    <td>
                        <input type="text" id="themeId" name="themeId" class="ui input medium mg-r-5" style="width: 150px;" readonly/>
                    </td>
                </tr>
                <tr>
                    <th>메인 테마명</th>
                    <td>
                        <input type="text" id="themeNm" name="themeNm" class="ui input medium mg-r-5" maxlength="15" style="width: 200px;" placeholder="15자 이내 등록"/>
                    </td>
                </tr>
                <tr>
                    <th>서브 테마명</th>
                    <td>
                        <input type="text" id="themeSubNm" name="themeSubNm" class="ui input medium mg-r-5" maxlength="20" style="width: 200px;" placeholder="20자 이내 등록"/>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <div class="ui form inline">
                            <select id="setAuroraThemeUseYn" name="setAuroraThemeUseYn" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="text-red-star">카테고리 이미지(PC)</span>
                    </th>
                    <td colspan="3">
                        <div id="pcImgDiv" style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="pcDisplayView" style="display:none;">
                                    <button type="button" id="pcDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="pcImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="pcImgUrl" name="pcImgUrl" value=""/>
                                    <input type="hidden" id="pcImgWidth" name="pcImgWidth" value=""/>
                                    <input type="hidden" id="pcImgHeight" name="pcImgHeight" value=""/>
                                    <input type="hidden" id="pcImgChangeYn" name="pcImgChangeYn" value="N"/>
                                </div>
                                <div class="pcDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="pcImgRegBtn" class="ui button medium" onclick="auroraTheme.img.clickFile('pc');">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 1200*220<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="text-red-star">카테고리 이미지(MOBILE)</span>
                    </th>
                    <td colspan="3">
                        <div id="mobileImgDiv" style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="mobileDisplayView" style="display:none;">
                                    <button type="button" id="mobileDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="mobileImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="mobileImgUrl" name="mobileImgUrl" value=""/>
                                    <input type="hidden" id="mobileImgWidth" name="mobileImgWidth" value=""/>
                                    <input type="hidden" id="mobileImgHeight" name="mobileImgHeight" value=""/>
                                    <input type="hidden" id="mobileImgChangeYn" name="mobileImgYn" value="N"/>
                                </div>
                                <div class="mobileDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="mobileImgRegBtn" class="ui button medium" onclick="auroraTheme.img.clickFile('mobile');">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 750*280<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="text">* 설정된 우선순위에 따라 테마가 순차적으로 노출됩니다.</h3>
        </div>

        <div class="ui form inline" style="margin-top: 20px;margin-bottom:20px;float: right;">
            <button type="button" class="ui button large mg-r-5 gray-dark pull-left" id="setAuroraTheme" >저장</button>
            <span style="margin:4px;"></span>
            <button type="button" class="ui button large mg-r-5 white pull-left" id="resetAuroraTheme">초기화</button>
        </div>
    </div>

    <div style="width: 50%; height: 350px; float: right;">
        <h3 class="title" style="float:none;">상품 등록/수정</h3>
        <div class="topline"></div>
        <br/>
        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="auroraThemeItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="auroraThemeItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="auroraThemeItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="auroraThemeItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>
        <div class="mg-t-20 mg-b-20" id="auroraThemeItemListGrid" style="width: 100%; height: 350px;"></div>

        <div id="selectItemPopup" style="float: right;">
            <button class="ui button small gray-light font-malgun" id="addItemPopUp">상품등록</button>
            <button class="ui button small gray-light font-malgun" id="addItemPopUpByExcel">일괄등록</button>
        </div>

        <div class="com wrap-title sub" style="width: 350px;">
            <h3 class="text">* 새벽배송 상품 등록/조회는 Hyper 및 새벽배송 상품 대상입니다.</h3>
            <h3 class="text">* 설정된 우선순위에 따라 상위에 노출됩니다.</h3>
        </div>

        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="170px">
                    <col width="200px">
                    <col width="170px">
                    <col width="200px">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품번호</th>
                    <td>
                        <input type="text" id="itemNo" name="itemNo" class="ui input medium mg-r-5" style="width: 200px;" readonly/>
                    </td>
                    <th>사용여부</th>
                    <td>
                        <div class="ui form inline">
                            <select id="setAuroraThemeItemUseYn" name="setAuroraThemeItemUseYn" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="ui form inline" style="margin-top: 20px;margin-bottom:20px;float: right;">
            <button type="button" class="ui button large mg-r-5 gray-dark pull-left" id="setAuroraThemeItem">등록</button>
            <span style="margin:4px;"></span>
            <button type="button" class="ui button large mg-r-5 white pull-left" id="resetAuroraThemeItem">초기화</button>
        </div>
    </div>

</div>

<div class="ui center-button-group mg-t-15" style="margin-top: 750px;">
    <span class="inner">
        <button class="ui button xlarge cyan font-malgun" id="setAuroraThemeItems">저장</button>
    </span>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/auroraThemeMain.js?${fileVersion}"></script>

<script>
    ${auroraThemeGridBaseInfo}
    ${auroraThemeItemGridBaseInfo}

    var hmpImgUrl = '${hmpImgUrl}';
</script>