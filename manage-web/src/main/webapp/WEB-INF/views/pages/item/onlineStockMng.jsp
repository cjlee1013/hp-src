<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">온라인재고 일괄설정</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1">
            <form id="itemSearchForm" onsubmit="return false;">
                <input type="hidden" name="mallType" value="${mallType}" />
                <table class="ui table">
                    <caption>온라인재고 일괄설정 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="schType" value="stockTypeOn"><span>재고관리유형 – 온라인재고</span>
                                </label>
                            </div>
                            <div class="ui form inline mg-t-20">
                                <label class="ui radio inline">
                                    <input type="radio" name="schType" value="itemNo" checked><span>상품번호</span>
                                </label>
                                <textarea id="schKeyword" name="schKeyword" class="ui input inline medium mg-r-5" style="width: 30%; height: 60px;" minlength="2" placeholder="상품번호 복수검색시 Enter또는 ,로 구분"></textarea>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td>
                            <div style="text-align: center; " class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan " ><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5" >초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div>
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="itemTotalCount">0</span>건</h3>
                <span class="ui form inline">
                    <button type="button" class="ui button medium mg-l-5 pull-right" onclick="onlineStockMng.setOnlineStockPop()">복수설정</button>
                    <button type="button" class="ui button medium mg-l-5 pull-right" onclick="onlineStockMng.getOnlineStockMngPop()">온라인재고 기준설정</button>
                </span>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-b-20" id="itemListGrid" style="width: 100%; height: 323px;"></div>
    </div>
    <form id="itemSetForm" onsubmit="return false;">
        <input type="hidden" name="itemNo" id="itemNo" />
        <input type="hidden" name="mallType" id="mallType" value="${mallType}" />
        <input type="hidden" name="storeType" id="storeType" />
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal ">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="140px">
                    <col width="30%">
                    <col width="120px">
                    <col width="20%">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>
                        <span class="text-red-star">상품번호</span>
                    </th>
                    <td>
                        <span id="itemNoView"></span>
                    </td>
                    <th>상품명</th>
                    <td colspan="3">
                        <span id="itemNmView"></span>
                        <span class="ui form inline mg-l-10">
                            <button type="button" id="itemHist" class="ui button medium" >변경이력</button>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span>온라인재고설정</span>
                    </th>
                    <td colspan="5">
                        <span class="ui form inline">
                            <span class="text mg-r-10">재고관리유형 : <span id="stockTypeNm"></span></span>
                            <label class="ui checkbox"><input type="checkbox" name="onlineStockYn" value="Y"><span class="text mg-r-5">온라인재고설정</span></label>
                            <input type="text" name="onlineStockStartDt" id="onlineStockStartDt" class="ui input mg-r-5" style="width:140px;"/>
                            <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                            <input type="text" name="onlineStockEndDt" id="onlineStockEndDt" class="ui input mg-r-5" style="width:140px;"/>
                        </span>
                        <div class="text-gray-light mg-t-5">(온라인재고 설정기간 이외에는 ‘재고관리유형‘의 설정값으로 자동 적용됩니다)</div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub mg-t-30">
            <span class="title">• 일괄설정방법 : </span>
            <span class="ui form inline">
                <span>
                    <select class="ui input mg-l-5 mg-r-5" name="setType" id="setType" style="width:150px;">
                        <option value="" >선택</option>
                        <option value="quantity" >수량입력</option>
                        <option value="rate" >비율입력</option>
                        <option value="auto" >자동설정</option>
                        <option value="mix" >복합설정</option>
                    </select>
                </span>
                <span>
                    <select class="ui input mg-l-5 mg-r-5" name="setKind" id="setKind" style="width:150px;display: none;">
                        <option value="" >선택</option>
                        <option value="ff" >FF</option>
                        <option value="gr" >GR</option>
                        <option value="gm" >GM</option>
                        <option value="cl" >CL</option>
                        <option value="electric" >Electric</option>
                    </select>
                </span>
                <span>
                    <select class="ui input mg-l-5 mg-r-5" name="setEvenNumber" id="setEvenNumber" style="width:150px;display: none;">
                        <option value="" >기본설정</option>
                        <option value="E" >짝수설정</option>
                    </select>
                </span>
                <span id="stockPrefix" class="text">수량입력(개) : </span>
                <span class="text">홈플러스</span>
                <input type="text" name="setStockQty" id="setStockQty" class="ui input" style="width:100px;" maxlength="6" data-comma-count="1"/>
                <span class="text">N마트</span>
                <input type="text" name="setRemainCntNaver" id="setRemainCntNaver" class="ui input" style="width:100px;" maxlength="6" data-comma-count="1"/>
                <span class="text">11번가</span>
                <input type="text" name="setRemainCntEleven" id="setRemainCntEleven" class="ui input" style="width:100px;" maxlength="6" data-comma-count="1"/>
                <button type="button" class="ui button medium mg-l-5" onclick="onlineStockMng.setStockGrid()">일괄적용</button>
            </span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table id="shipArea" class="ui table">
                <caption>점포정보</caption>
                <colgroup>
                    <col width="140px">
                    <col width="12.5%">
                    <col width="120px">
                    <col width="12.5%">
                    <col width="130px">
                    <col width="12.5%">
                    <col width="90px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <td colspan="8">
                        점포 수 : <span id="itemShipTotalCount">0</span>개
                        <span class="pull-right ui form inline">
                            <button type="button" id="storeListGridCopy" class="ui button medium" >Text 복사</button>
                        </span>
                        <div class="mg-t-20 mg-b-20" id="storeListGrid" style="width: 100%; height: 324px;"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <span class="text-gray-light">※ 점포별 정보는 오른쪽 하단의 '적용' 버튼을 클릭하여 점포리스트에 적용 후, 상품정보로 최종 저장해 주세요</span>
                    </td>
                </tr>
                <tr>
                    <th>점포ID</th>
                    <td>
                        <span id="storeId" class="text"></span>
                    </td>
                    <th>점포명</th>
                    <td>
                        <span id="storeNm" class="text" ></span>
                    </td>
                    <th>점포유형</th>
                    <td>
                        <span id="storeTypeNm" class="text" ></span>
                    </td>
                    <th>점포구분</th>
                    <td>
                        <span id="storeKindNm" class="text" ></span>
                    </td>
                </tr>
                <tr>
                    <th>홈플러스 온라인 재고수량</th>
                    <td colspan="7">
                        <span class="ui form inline">
                            <input type="text" name="stockQty" id="stockQty" class="ui input " style="width:100px;" maxlength="9" data-comma-count="1"/>
                            <span class="text mg-l-5 mg-r-10">개</span>
                            <label class="ui checkbox mg-r-5 changeStockQtyLabel" style="display: none;">
                                <input type="checkbox" id="changeStockQtyCheck" class="changeStockQtyCheck" name="changeStockQty" value="Y" data-param1="stockQty"><span class="text">수량변경</span>
                            </label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>N마트 온라인 재고수량</th>
                    <td colspan="7">
                        <span class="ui form inline">
                            <input type="text" name="remainCntNaver" id="remainCntNaver" class="ui input " style="width:100px;" maxlength="9"/>
                            <span class="text mg-l-5 mg-r-10">개</span>
                            <label class="ui checkbox mg-r-5 changeStockQtyLabel" style="display: none;">
                                <input type="checkbox" id="changeStockQtyCheckNaver" class="changeStockQtyCheck" name="changeStockQtyNaver" data-param1="remainCntNaver" value="Y"><span class="text">수량변경</span>
                            </label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>11번가 온라인 재고수량</th>
                    <td colspan="6">
                        <span class="ui form inline">
                            <input type="text" name="remainCntEleven" id="remainCntEleven" class="ui input " style="width:100px;" maxlength="9"/>
                            <span class="text mg-l-5 mg-r-10">개</span>
                            <label class="ui checkbox mg-r-5 changeStockQtyLabel" style="display: none;">
                                <input type="checkbox" id="changeStockQtyCheckEleven" class="changeStockQtyCheck" name="changeStockQtyEleven" data-param1="remainCntEleven" value="Y"><span class="text">수량변경</span>
                            </label>
                        </span>
                    </td>
                    <td>
                        <span class="pull-right mg-t-10">
                            <button type="button" class="ui button medium gray-dark" onclick="onlineStockMng.setStore();">적용</button>
                            <button type="button" class="ui button medium" onclick="if (confirm('등록된 정보를 모두 삭제하시겠습니까?')) {onlineStockMng.getStoreDetail();}">초기화</button>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn" onclick="onlineStockMng.setOnlineStock();">저장</button>
                <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </form>
</div>

<script>

    const menuType = '${menuType}';

    // 상품 리스트 리얼그리드 기본 정보
    ${itemListGridBaseInfo}
    // 옵션 그리드
    ${optListGridBaseInfo}
    // 배송정보 그리드
    ${storeListGridBaseInfo}

    $(document).ready(function() {
        itemListGrid.init();
        storeListGrid.init();
        onlineStockMng.init();
        CommonAjaxBlockUI.targetId('searchBtn');
        CommonAjaxBlockUI.targetId('setItemBtn');
    });

</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemOnlineStockMng.js?${fileVersion}"></script>