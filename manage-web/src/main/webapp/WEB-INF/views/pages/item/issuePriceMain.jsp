<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">이상매가 현황조회</h2>
    </div>

    <%-- 이상매가 현황 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>이상매가 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>당일 이상매가 현황</th>
                        <td>
                            <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                <c:if test="${storeType.ref1 eq 'home'}">
                                    <c:set var="checked" value="" />
                                    <c:if test="${storeType.ref2 eq 'df'}">
                                        <c:set var="checked" value="checked" />
                                    </c:if>
                                    <label class="ui radio inline"><input type="radio" name="schStoreType" class="ui input" value="${storeType.mcCd}" ${checked}/><span>${storeType.mcNm}</span></label>
                                </c:if>
                            </c:forEach>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="submit" id="schBtn" class="ui button large cyan mg-r-5"><i class="fa fa-search"></i> 검색</button>
                                <button type="button" id="excelDownBtn" class="ui button large">엑셀다운로드</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="issudPriceTotalCnt">0</span>건</h3>
        </div>
        <div id="issuePriceGrid" style="width: 100%; height: 470px;"></div>
    </div>
</div>

<script>
    // 그리드 정보
    ${issuePriceGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/issuePriceMain.js?${fileVersion}"></script>