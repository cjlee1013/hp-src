<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup large popup-request">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            <i class="fa fa-clone mg-r-5"></i> ${itemStatusNm} 사유입력
        </h3>
    </div>
    <!-- 검색옵션 -->
    <form id="stopReasonForm">
        <input type="hidden" id="itemNo" name="itemNo" value="${itemNo}" />
        <input type="hidden" name="itemStatus" value="${itemStatus}" />
        <input type="hidden" name="mallType" value="${mallType}" />
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>사유입력</caption>
                <colgroup>
                    <col width="20%">
                    <col width="80%">
                </colgroup>
                <tbody>
                <tr>
                    <th>사유입력</th>
                    <td>
                        <div class="ui form inline">
                            <textarea id="stopReason" name="stopReason" class="ui input mg-r-5" style="float:left;width: 450px; height: 122px;" maxlength="300"></textarea>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <!-- 하단 버튼 -->
    <div class="ui form inline mg-t-15 text-center">
        <button type="button" id="setBtn" class="ui button xlarge btn-danger mg-r-10">저장</button>
        <button type="button" id="closeBtn" class="ui button xlarge">취소</button>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>

    // 파트너검색팝업 리얼그리드 기본 정보
    ${partnerSearchPopGridBaseInfo}

    $(document).ready(function() {
        stopReason.init();
        CommonAjaxBlockUI.targetId('setBtn');
    });

    let stopReason = {
        init : function() {
            this.event();
        },
        event : function() {
            $('#setBtn').click(function() {
                stopReason.setStopReason();
            });

            $('#closeBtn').click(function() {
                stopReason.cancel();
            });
        },
        setStopReason : function() {
            if ($.jUtil.isEmpty($('#stopReason').val())) {
                return $.jUtil.alert('사유를 입력해주세요.', 'stopReason');
            }

            CommonAjax.basic({
                    url: "/item/setItemStatus.json",
                data: $('#stopReasonForm').serializeArray(),
                method: "post",
                callbackFunc: function (res) {
                    alert(res.returnMsg);
                    $('#searchBtn', opener.document).trigger('click');
                    if (!$.jUtil.isEmpty($('#itemNo', opener.document).val())) {
                        window.opener.item.getDetail($('#itemNo', opener.document).val());
                    }
                    stopReason.cancel();
                }
            });

        },
        cancel : function() {
            window.close();
        }
    };
</script>