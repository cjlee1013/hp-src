<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">대대분류 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="categoryGroupSearchForm"  onsubmit="return false;">
                <table class="ui table">
                    <caption>대대분류 검색</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="350px">
                        <col width="150px">
                        <col width="150px">
                        <col >
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 140px">
                                    <c:forEach var="codeDto" items="${schType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width:150px" >
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 120px" name="schUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${schUseYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn"><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                <span style="margin-left:4px;"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="noticeTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="categoryGroupListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 대대분류정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="categoryGroupSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>대대분류정보</caption>
                <colgroup>
                    <col width="160px">
                    <col width="300px">
                    <col width="160px">
                    <col width="300px">
                </colgroup>
                <tbody>
                <tr>
                    <th>대대분류ID</th>
                    <td colspan="3" >
                        <input id="gcateCd" name="gcateCd" type="text" class="ui input medium mg-r-5" style="width: 210px;" readonly="">
                    </td>
                </tr>
                <tr>
                    <th>대대분류명 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="gcateNm" name="gcateNm" class="ui input medium mg-r-5" maxlength="15" style="width: 210px;" >
                            <span class="text">
                                ( <span style="color:red;" id="gcateNmCnt" class="cnt">0</span>/15)
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <span class="ui form inline">
                            <select id="useYn" style="width: 150px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${schUseYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </td>
                    <th>우선순위 <span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="priority" name="priority" style="float:left;width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="40">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>카테고리 아이콘 ON <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="categoryGroup.img.deleteImg('CATEON');">삭제</button>
                                    <div id="CATEON" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="">
                                        <input type="hidden" class="imgNm" name="imgList[].imgNm" value="">
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="CATEON">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="categoryGroup.img.clickFile('CATEON');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                    <br><br><br>
                                    최적화 가이드<br>
                                    사이즈 : 60*60<br>
                                    용량 : 2MB 이하<br>
                                    확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                    <th>카테고리 아이콘 OFF <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="categoryGroup.img.deleteImg('CATEOFF');">삭제</button>
                                    <div id="CATEOFF" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="">
                                        <input type="hidden" class="imgNm" name="imgList[].imgNm" value="">
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="CATEOFF">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="categoryGroup.img.clickFile('CATEOFF');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 60*60<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>서브메인 카테고리 아이콘 <br>(PC) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="categoryGroup.img.deleteImg('SBP');">삭제</button>
                                    <div id="SBP" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="">
                                        <input type="hidden" class="imgNm" name="imgList[].imgNm" value="">
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="SBP">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="categoryGroup.img.clickFile('SBP');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 80*80<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                    <th>서브메인 카테고리 아이콘<br>(모바일) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="productImgDelBtn0" class="ui button small delete-thumb deleteImgBtn text-size-sm"onclick="categoryGroup.img.deleteImg('SBM');">삭제</button>
                                    <div id="SBM" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="">
                                        <input type="hidden" class="imgNm" name="imgList[].imgNm" value="">
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="SBM">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button"  class="ui button medium" onclick="categoryGroup.img.clickFile('SBM');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 120*120<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>


    <div class="com wrap-title sub">
        <h3 class="title">• 하위 대분류</h3>
    </div>
    <div id="categoryGroupLcateListGrid" style="width: 100%; height: 320px;"></div>
    <div class="ui center-button-group mg-t-15" style="float: right">
        <span class="inner" style="float: left">
            <select id="largeCategory" style="width: 150px;float: left" name="largeCategory" class="ui input medium mg-r-10" >
                 <option value="">카테고리</option>
                <c:forEach var="lcate" items="${lcategoryList}" varStatus="status">
                    <option value="${lcate.lcateCd}"
                            data-lcatenm='${lcate.lcateNm}'
                            data-useyntxt="${lcate.useYnTxt}"
                            data-dispyntxt ="${lcate.dispYnTxt}"
                            data-regnm ="${lcate.regNm}"
                            data-chgnm ="${lcate.chgNm}"
                            data-regdt ="${lcate.regDt}"
                            data-chgdt ="${lcate.chgDt}"
                    >${lcate.lcateNm}</option>
                </c:forEach>
            </select>

            <input type="text" id="lcateuseYnTxt" name="lcateuseYnTxt" class="ui input medium mg-r-5" style="width: 100px;float:left;" readonly>

            <input type="text" id="lCatedispYnTxt" name="lCatedispYnTxt" class="ui input medium mg-r-5" style="width: 100px;float: left" readonly>

            <select id="lCateuseYn" style="width: 100px;float: left" name="lCateuseYn" class="ui input medium mg-r-10" >
                <option value="addCate">추가</option>
                <option value="dellCate">삭제</option>
            </select>
            <button class="ui button medium cyan font-malgun" id="saveLcate">등록</button>
        </span>
    </div>
    <div class="ui center-button-group mg-t-15" style="margin-top: 90px">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            <button class="ui button xlarge dark-blue font-malgun" id="setStaticCategoryBtn">프론트 반영</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/categoryGroupMain.js?${fileVersion}"></script>

<script>
    // 판매업체 리스트 리얼그리드 기본 정보
    ${categoryGroupListGridBaseInfo}
    ${categoryGroupLcateListGridBaseInfo}
    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
