<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">외부연동 설정관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1">
            <form id="itemSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>외부연동 설정관리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="45%">
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="chgDt">수정일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="marketItemMain.initSearchDate('-30d');">1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="marketItemMain.initSearchDate('-90d');">3개월</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5" >
                            <div style="text-align: center; " class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan " ><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5" >초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" >엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                    style="width:150px;" data-default="세분류">
                            </select>
                        </td>
                        <th>상품상태</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schItemStatus" name="schItemStatus" style="width: 100px;">
                                    <option value="">전체</option>
                                    <option value="A">판매가능</option>
                                    <option value="T">임시저장</option>
                                    <option value="P">일시중지</option>
                                    <option value="S">영구중지</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 20%; vertical-align: top;">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                    <option value="regNm">등록자</option>
                                    <option value="chgNm">수정자</option>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" placeholder="상품번호 복수검색시 Enter또는 ,로 구분"></textarea>
                            </div>
                        </td>
                        <th>상품유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schItemType" style="width: 180px" name="schItemType" class="ui input medium">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                                        <c:if test="${codeDto.ref2 ne 'DS'}">
                                            <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>외부연동 사이트</th>
                        <td>
                            <select class="ui input mg-l-5 mg-r-5" name="schPartnerId" id="schPartnerId" style="width:120px;float:left;">
                                <option value="naver" >N마트</option>
                                <option value="eleven" >11번가</option>
                            </select>
                            <select class="ui input mg-l-5 mg-r-5" name="schSetYn" id="schSetYn" style="width:120px;">
                                <option value="" >전체</option>
                                <option value="Y" >등록</option>
                                <option value="N" >등록안함</option>
                            </select>
                        </td>
                        <th>중지기간설정</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schStopPeriodYn" name="schStopPeriodYn" style="width: 100px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div>
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="itemTotalCount">0</span>건</h3>
                <span class="pull-right" style="font-size: 14px;">• 외부연동 일괄전송
                    <button type="button" id="setMarketItemSend" class="ui button medium" onclick="marketItemMain.setMarketItemSend();">즉시전송</button>
                </span>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-b-20" id="marketItemSettingListGrid" style="width: 100%; height: 323px;"></div>
    </div>
    <form id="itemsSetForm" onsubmit="return false;">
    <div class="com wrap-title sub mg-t-30">
        <span class="ui form inline">
            <span class="pull-right" style="font-size: 14px;">• 일괄설정
                <select class="ui input mg-l-5 mg-r-5" name="providerId" id="setPartnerId" style="width:120px;">
                    <option value="" >선택</option>
                    <option value="naver" >N마트</option>
                    <option value="eleven" >11번가</option>
                </select>
                <select class="ui input mg-l-5 mg-r-5" name="useYn" id="setYn" style="width:120px;">
                    <option value="" >선택</option>
                    <option value="Y" >등록</option>
                    <option value="N" >등록안함</option>
                </select>
                <label class="ui checkbox">
                    <input type="checkbox" name="stopPeriodYn" id="setStopPeriodYn" value="Y"><span class="text mg-r-5">중지기간설정</span>
                </label>
                <input type="text" name="stopStartDt" id="setStopPeriodStartDt" class="ui input mg-r-5" style="width:140px;"/>
                <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                <input type="text" name="stopEndDt" id="setStopPeriodEndDt" class="ui input mg-r-5" style="width:140px;"/>
                <button type="button" id="setMarketItemsSettingBtn" class="ui button medium" >일괄적용</button>
            </span>
        </span>
    </div>
    </form>
    <form id="itemSetForm" onsubmit="return false;">
        <input type="hidden" name="itemNo" id="itemNo" />
        <div class="com wrap-title sub">
            <h3 class="title">• 외부연동 사이트 설정정보</h3>
        </div>
        <div class="topline"></div>

        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>외부연동 사이트 설정정보</caption>
                <colgroup>
                    <col width="120px">
                    <col width="39%">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품번호</th>
                    <td>
                        <div id="itemNoTxt"></div>
                    </td>
                    <th>상품명</th>
                    <td>
                        <div id="itemNmTxt"></div>
                    </td>
                </tr>
                <tr>
                    <th>N마트</th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${regYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="naverSetYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                        <span class="ui form inline stopPeriodArea">
                            <label class="ui checkbox mg-r-10">
                                <input type="checkbox" name="naverStopPeriodYn" id="naverStopPeriodYn" class="stopPeriodYn" value="Y"><span class="text">중지기간설정</span>
                            </label>
                            <span>
                                <input type="text" name="naverStopStartDt" id="naverStopStartDt" class="ui input mg-r-5" style="width:140px;"/>
                            </span>
                            <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                            <span>
                                <input type="text" name="naverStopEndDt" id="naverStopEndDt" class="ui input mg-r-5" style="width:140px;"/>
                            </span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>11번가</th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${regYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="elevenSetYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                        <span class="ui form inline stopPeriodArea">
                            <label class="ui checkbox mg-r-10">
                                <input type="checkbox" name="elevenStopPeriodYn" id="elevenStopPeriodYn" class="stopPeriodYn" value="Y"><span class="text">중지기간설정</span>
                            </label>
                            <span>
                                <input type="text" name="elevenStopStartDt" id="elevenStopStartDt" class="ui input mg-r-5" style="width:140px;"/>
                            </span>
                            <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                            <span>
                                <input type="text" name="elevenStopEndDt" id="elevenStopEndDt" class="ui input mg-r-5" style="width:140px;"/>
                            </span>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
             <span class="inner">
                 <button type="button" class="ui button xlarge cyan font-malgun" id="setMarketItemSettingBtn">저장</button>
                 <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
             </span>
        </div>
    </form>

</div>
<script>
    // 상품 리스트 리얼그리드 기본 정보
    ${MarketItemSettingListGridBaseInfo}

    $(document).ready(function() {
        marketItemMain.init();
        marketItemSettingListGrid.init();
        CommonAjaxBlockUI.targetId('setMarketItemsSettingBtn');
        CommonAjaxBlockUI.targetId('searchBtn');
    });

</script>
<script type="text/javascript" src="/static/js/item/marketItemSettingMain.js?${fileVersion}"></script>