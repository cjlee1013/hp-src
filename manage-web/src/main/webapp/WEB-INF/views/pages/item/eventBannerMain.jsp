<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
          상품상세 마케팅/이벤트
        </h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="eventBannerSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  eventBanner.search();">
                <table class="ui table">
                    <caption>상품상세 마케팅/이벤트 공지 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="450px">
                        <col width="120px">
                        <col />
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDateType" style="width: 100px" name="schDateType" class="ui input medium mg-r-10">
                                    <option value="REGDT" selected>등록일</option>
                                    <option value="DISPDT">노출일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispType" style="width: 100px" name="schDispType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="partnerNm">판매자명</option>
                                    <option value="partnerId">판매자ID</option>
                                    <option value="bannerNm">제목</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 200px;" minlength="2" >
                            </div>
                        </td>
                        <th>노출여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispYn" style="width: 100px" name="schDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="schCateCd1" name="schCateCd1"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd2" name="schCateCd2"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd3" name="schCateCd3"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd4" name="schCateCd4"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="eventBannerTotalCount">0</span>건  </h3>
                <span class="text-red">&nbsp;  ※ 동일한 상품에 구분이 다른 마케팅/이벤트가 중복 등록된 경우, 상품별>카테고리별>판매자별>전체상품 순으로 우선 적용됩니다.</span>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="eventBannerListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 상품상세 마케팅 공지 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="eventBannerSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>상품상세 마케팅 공지 등록/수정</caption>
                <colgroup>
                    <col width="120px">
                    <col width="*">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th><b>제목</b><span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="bannerNo" name="bannerNo" type="hidden" class="ui input medium mg-r-5" >
                            <input id="bannerNm" name="bannerNm" type="text" style="width: 50%;" class="ui input medium mg-r-5" maxlength="50">
                        </div>

                    </td>
                </tr>
                <tr>
                    <th>점포유형<span class="text-red"> *</span></th>
                    <div class="ui form inline">
                        <td>
                            <select id="storeType" name="storeType" class="ui input medium mg-r-5" style="width: 120px">
<                               <c:forEach var="codeDto" items="${bannerStoreType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </div>
                    <th>적용대상<span class="text-red"> *</span></th>
                    <div class="ui form inline">
                        <td>
                            <select id="dispType" name="dispType" class="ui input medium mg-r-5" style="width: 120px">
                                <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </div>

                </tr>
                <tr id="partnerRow" style="display: none;">
                    <th>판매자명(ID)<span class="text-red"> *</span></th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" name="partnerId" id="partnerId" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                                <input type="text" name="partnerNm" id="partnerNm" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                                <button type="button" class="ui button small gray-dark font-malgun" id="schPartnerPopBtn">조회</button>
                            </div>
                        </td>
                </tr>
                <tr id="cateRow" style="display:none">
                    <th>카테고리<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="selectCateCd1" name="selectCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(1,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd2" name="selectCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(2,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd3" name="selectCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(3,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd4" name="selectCateCd4"
                                style="width:150px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr id="itemRow">
                    <th>상품<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <li><button type="button" class="ui button medium gray-dark font-malgƒun" id="schItemPopBtn">상품 조회</button></li>
                        <div class="mg-t-5">
                            <ui id="items" ></ui>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>내용<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <div class="mg-t-20 mg-b-20" id="bannerDetailListGrid" style="width: 100%; height: 350px;"></div>
                        </div>
                        <span > * 추가/삭제</span>
                        <table class="ui table-inner mg-t-5" >
                            <caption>상품상세 마케팅/이벤트 공지 링크정보</caption>
                            <colgroup>
                                <col width="120px">
                                <col width="*">
                            </colgroup>
                            <form id="eventDetailSetForm">
                            <tbody>
                            <tr>
                            <th>이미지<span class="text-red"> *</span></th>
                                <td colspan="3">
                                    <div id="imgDiv">
                                        <div class="text-center preview-image" style="height:132px;">
                                            <div class="imgDisplayView" style="display:none;">
                                                <button type="button" id="imgDelBtn" onclick="eventBanner.img.deleteImg('imgBanner');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                                <div id="imgBanner" class="uploadType itemImg" inputId="uploadMNIds" data-processkey="EventItemBanner" style="cursor:hand;">

                                                    <img class="imgUrlTag" width="132" height="132" src="">
                                                    <input type="hidden" class="imgUrl" name="imgUrl" value=""/>
                                                    <input type="hidden" class="imgNm" name="imgNm" value=""/>
                                                    <input type="hidden" class="imgWidth" name="imgWidth" value=""/>
                                                    <input type="hidden" class="imgHeight" name="imgHeight" value=""/>
                                                    <input type="hidden" class="changeYn" name="changeYn" value="N"/>
                                                </div>
                                            </div>

                                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                                <button type="button" id="regImageBtn" class="ui button medium" onclick="eventBanner.img.clickFile('imgBanner');">등록</button>
                                            </div>

                                        </div>

                                    </div>
                                    <br>
                                    <div>(사이즈 : 840 * 1024 (최대) / 용량 : 2MB 이하 / 파일 : JPG, JPEG, PNG)</div>
                                </td>
                            </tr>
                            <tr>
                                <th>링크<span class="text-red"> *</span></th>
                                    <td>
                                        <div class="ui form inline">
                                            <select id="linkType" name="linkType" class="ui input medium mg-r-5" style="width: 120px">
                                                <c:forEach var="codeDto" items="${linkType}" varStatus="status">
                                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                                        <c:set var="selected" value="selected" />
                                                    </c:if>
                                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                                </c:forEach>
                                            </select>

                                            <span id="promoArea">
                                                <input id="promoNo" name="linkNo" type="text" class="ui input medium mg-r-5"  style="width: 120px;" readonly>
                                                <input id="promoNm" name="linkInfo" type="text" class="ui input medium mg-r-5"  style="width: 150px;" readonly>
                                                <button type="button" class="ui button medium gray-dark font-malgun" id="schPromoPopBtn" >검색</button>
                                            </span>

                                            <span id="urlArea" style="display: none">
                                                <input id="url" name="linkInfo" type="text" class="ui input medium mg-r-5" style="width: 400px;" value="https://" maxlength="100">
                                            </span>
                                        </div>
                                    </td>
                            </tr>

                            <tr>
                                <th>노출순서<span class="text-red"> *</span></th>
                                <td>
                                  <select id="priority" name="priority" class="ui input medium mg-r-10" style="width: 120px">
                                        <option value="" >선택</option>
                                        <c:forEach var="code" items="${priority}" varStatus="status">
                                            <c:if test="${code.mcCd eq 'ITEM_NOTICE'}">
                                                <c:forEach var="i" begin="1" end="${code.ref1}">
                                                     <option value="${i}">${i}</option>
                                                </c:forEach>
                                            </c:if>
                                         </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                            </form>
                        </table>
                        <div class="ui form inline" style="margin-top: 10px;float: right;">
                            <button type="submit" id="addDetailBtn" class="ui button medium mg-r-5 gray-dark pull-left">추가</button>
                            <button type="submit" id="saveDetailBtn" class="ui button medium mg-r-5 gray pull-left">저장</button>
                            <button type="submit" id="delDetailBtn" class="ui button medium mg-r-5 gray pull-left">삭제</button>
                            <button type="button" id="resetDetailForm" class="ui button medium mg-r-5 gray pull-left">초기화</button>
                        </div>
                </td>
                </tr>
                <tr>
                    <th>노출여부<span class="text-red"> *</span></th>
                    <div class="ui form inline">
                        <td>
                            <label class="ui radio medium inline"><input type="radio" name="dispYn" class="ui input medium" value="Y" checked><span>노출</span></label>
                            <label class="ui radio medium inline"><input type="radio" name="dispYn" class="ui input medium" value="N"><span>비노출</span></label>
                        </td>
                    </div>
                    <th>노출기간<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="노출시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="노출종료일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                   </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15" >
        <span class="inner">
            <button type="button" class="ui button xlarge mg-r-5 white" id="resetBannerBtn" >초기화</button>
            <button type="button" class="ui button xlarge mg-r-5 cyan" id="setBannerBtn" >저장</button>
        </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="bannerImgFile" style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/eventBannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>



<script>
    const hmpImgUrl = '${hmpImgUrl}';
    var dispTypeJson = ${dispTypeJson};

    // 상품상세 일괄공지 리스트 리얼그리드 기본 정보
    ${eventBannerListGridBaseInfo}
    ${bannerDetailListGridBaseInfo}
</script>

