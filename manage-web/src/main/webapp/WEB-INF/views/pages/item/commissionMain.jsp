<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매자별 기준수수료율</h2>
    </div>
    <div class="com wrap-title sub" >

        <div class="ui wrap-table horizontal mg-t-10">
            <form id="commissionSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  commission.search();">
                <input type="hidden" id="searchCateCd" name="searchCateCd">
                <table class="ui table">
                    <caption>판매자별 기준수수료율 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="700px">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:130px;float:left;" data-default="판매자명">
                                <option value="partnerNm">판매자명</option>
                                <option value="partnerId">판매자ID</option>
                            </select>
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="20" autocomplete="off" style="width:350px;float:left;">
                            </div>
                        </td>
                        <td rowspan="3" style="width: 300px;">
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn" ><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 dark-blue pull-left" id="excelDownloadBtn" style="display: none;">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="mcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="scateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="dcateCd"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchUseYn" name="searchUseYn" style="width: 80px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색된 결과 : <span id="commissionTotalCount">0</span>건</h3><p>&nbsp;&nbsp;※ 기준수수료율은 하위 분류 카테고리 기준으로 등록된 수수요율이 우선 적용됩니다.</p>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="commissionListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">판매자별 기준수수료율 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="commissionSetForm" onsubmit="return false;">
            <input type="hidden" id="commissionSeq" name="commissionSeq">
            <input type="hidden" id="cateKind" name="cateKind">
            <input type="hidden" id="cateCd" name="cateCd">
            <table class="ui table">
                <caption>금칙어 등록/수정</caption>

                <colgroup>
                    <col width="200px">
                    <col width="*">
                </colgroup>

                <tbody>
                <tr>
                    <th>판매업체&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <span class="ui form inline">
                                <input type="text" id="partnerId" name="partnerId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="판매업체ID" readonly>
                                <input type="text" id="partnerNm" name="partnerNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="판매업체명" readonly>
                                <button type="button" class="ui button medium" onclick="commission.getPartnerPop();" >조회</button>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>카테고리&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <select class="ui input medium mg-r-5" id="setCateCd1" name="lcateCd"
                                style="width:180px;float:left;" data-default="대분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(1,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd2" name="mcateCd"
                                style="width:180px;float:left;" data-default="중분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(2,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd3" name="scateCd"
                                style="width:180px;float:left;" data-default="소분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(3,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd4" name="dcateCd"
                                style="width:180px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>기준수수료율(VAT포함)&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input class="ui input medium mg-r-5" id="commissionRate" name="commissionRate"
                                   type="text" style="width: 60px;" maxlength="5"
                                   onkeypress="return commission.isNumberKey(event)"
                                   onkeyup="return commission.delKorWord(event)">
                        </span>
                        %
                    </td>
                </tr>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            <button class="ui button xlarge cyan font-malgun" id="setCommissionBtn">등록</button>
        </span>
    </div>
</div>

<iframe style="display:none;" id="commissionExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/commissionMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>

<script>
    // 브랜드 리스트 리얼그리드 기본 정보
    ${commissionListGridBaseInfo}
</script>