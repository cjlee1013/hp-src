<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">외부연동 현황관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="marketItemSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>외부연동 현황관리</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="30$">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>외부연동 사이트</th>
                        <td>
                            <label class="ui radio inline"><input type="radio" name="schPartner" class="ui input medium" value="naver" checked><span>N마트</span></label>
                            <label class="ui radio inline"><input type="radio" name="schPartner" class="ui input medium" value="eleven"><span>11번가</span></label>
                        </td>
                        <th>홈플러스 상품상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schItemStatus" name="schItemStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${itemStatus}" varStatus="status">
                                        <c:if test="${codeDto.ref2 eq 'TDB'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td rowspan="3" >
                            <div style="text-align: center; " class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan " ><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5" >초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" >엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="selectCateCd1" name="schLcateCd"
                                        style="width:150px;float:left;" data-default="대분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(1,'selectCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5" id="selectCateCd2" name="schMcateCd"
                                        style="width:150px;float:left;" data-default="중분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(2,'selectCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5" id="selectCateCd3" name="schScateCd"
                                        style="width:150px;float:left;" data-default="소분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(3,'selectCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5" id="selectCateCd4" name="schDcateCd"
                                        style="width:150px;float:left;" data-default="세분류">
                                </select>
                            </div>
                        </td>
                        <th>연동사이트 상품상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schMarketItemStatus" name="schMarketItemStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${marketItemStatus}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'Y'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 30%; vertical-align: top;">
                                    <option value="itemNo" selected>상품번호</option>
                                    <option value="itemNm">상품명</option>
                                    <option value="marketItemNo">연동사이트 상품번호</option>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 65%; height: 60px;" placeholder="상품번호 복수검색시 Enter또는 ,로 구분"></textarea>
                            </div>
                        </td>
                        <th>연동여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schSendYn" name="schSendYn" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${sendYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="marketItemTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="marketItemListGrid" style="width: 100%; height: 300px;"></div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 외부연동 사이트 상품정보</h3>
    </div>
    <div class="topline"></div>

    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <input type="hidden" id="partnerId" name="partnerId" value="">
            <caption>외부연동 사이트 상품정보</caption>
            <colgroup>
                <col width="15%">
                <col width="20%">
                <col width="15%">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>상품번호</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="itemNo"></span>
                        <button type="button" id="marketItemHist" class="ui button medium mg-l-10" style="display: none" >연동이력</button>
                    </div>
                </td>
                <th>상품명</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="itemNm"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>연동 사이트</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="marketNm"></span>
                    </div>
                </td>
                <th>연동사이트 상품번호</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="marketItemNo"></span>
                        <span class="text mg-l-10" id="sendMsg"></span>
                    </div>

                </td>
            </tr>
            <tr>
                <th>카테고리</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <div class="ui form inline">
                            <span class="text" id="cateNm"></span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>연동 사이트 카테고리</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <span class="text" id="marketCateNm"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>대표 판매가</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="salePrice"></span>
                    </div>
                </td>
                <th>대표 할인가</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="discountPrice"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>행사코드</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <span class="text" id="promoCd"></span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        점포 수 : <span id="marketItemStoreTotalCount">0</span>개
    </div>
    <div class="topline"></div>
    <div class="mg-b-10" id="marketItemStorePriceGrid" style="width: 100%; height: 300px;"></div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/marketItemMain.js?${fileVersion}"></script>

<script>
    //외부연동 현환관리 Realgrid
    ${marketItemListGridBaseInfo}
    ${marketItemStorePriceGridBaseInfo}
</script>
