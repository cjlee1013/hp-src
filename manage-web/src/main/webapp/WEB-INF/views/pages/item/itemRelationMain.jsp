<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">연관 상품관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="itemSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>연관 상품관리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="45%">
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="chgDt">수정일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemRelation.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemRelation.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemRelation.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="itemRelation.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>판매업체</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="판매업체ID" readonly>
                                <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="판매업체명" readonly>
                                <button type="button" class="ui button medium" onclick="itemRelation.getPartnerPop('itemRelation.callBack.schPartner');" >조회</button>
                            </div>
                        </td>
                        <th>노출여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schDispYn" name="schDispYn" style="width: 100px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 20%; vertical-align: top;">
                                    <option value="relationNo">연관번호</option>
                                    <option value="relationNm">연관명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <textarea id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" placeholder="연관번호 복수검색시 Enter또는 ,로 구분"></textarea>
                            </div>
                        </td>
                        <th>설정여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schUseYn" name="schUseYn" style="width: 100px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="itemRelationTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="itemRelationListGrid" style="width: 100%; height: 300px;"></div>
    </div>
    <form id="itemRelationSetForm" name="itemRelationSetForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 연관정보</h3>
        </div>
        <div class="topline"></div>

        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>연관정보</caption>
                <colgroup>
                    <col width="120px">
                    <col width="39%">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>연관번호</th>
                    <td>
                        <input type="hidden" id="relationNo" name="relationNo">
                        <input type="hidden" id="scateCd" name="scateCd">
                        <input type="hidden" id="partnerId" name="partnerId">
                        <div class="ui form inline">
                            <span class="text" id="relationNoText"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">연관명</span></th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <input id="relationNm" name="relationNm" type="text" class="ui input medium mg-r-5" style="width: 450px;" maxlength="100">
                            <span class="text">( <span style="color:red;" id="relationNmCount">0</span> / 100자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">노출여부</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="dispYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th><span class="text-red-star">설정여부</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                            <c:set var="disabled" value="" />
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 ne 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <c:if test="${codeDto.mcCd eq 'Y'}">
                                <c:set var="disabled" value="disabled" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input" value="${codeDto.mcCd}" ${disabled}  ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">PC 노출기준</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dispPcType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline" style="margin-top: 7px"><input type="radio" name="dispPcType" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th><span class="text-red-star">모바일 <br />노출기준</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dispMobileType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline" style="margin-top: 7px"><input type="radio" name="dispMobileType" class="ui input"  value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>인트로 이미지</th>
                    <td colspan="3">
                        <div class="text-center preview-image" style="width:132px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="introImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id='imgIntro' class="uploadType" data-type="INTRO" data-processkey="ItemRelation" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132px;" height="132px;" src="">
                                    <div class="ui form inline" >
                                        <input type="hidden" class="imgUrl" name="imgUrl" value="" id="introImgUrl"/>
                                        <input type="hidden" class="imgNm" name="imgNm" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="introImgRegBtn" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="introImg">등록</button>
                            </div>
                        </div>
                        <br>
                        <div class="mg-t-5">
                        ( 사이즈 : 840 x 세로제한없음 / 용량 : 3MB 이하 / 파일 : JPG, JPEG, PNG )
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 연관상품 상품목록 (<span id="itemRelationDetailTotalCount">0</span>/50개) </h3> <span class="text-red mg-l-10">리스트의 순서대로 프론트에 전시됩니다</span>
            <button type="button" class="ui button medium gray-dark font-malgun pull-right mg-r-10" id="schItemPopBtn">상품추가</button>
        </div>
        <div class="topline"></div>
        <div class="mg-b-10" id="itemRelationDetailListGrid" style="width: 100%; height: 300px;"></div>
        <div class="ui right-button-group" style="margin-bottom: 50px;margin-top: 0px;">
            <button type="button" class="ui button medium" onclick="itemRelation.deleteItemRelationSelect();">선택삭제</button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5 itemMoveCtrl"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5 itemMoveCtrl"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="top" class="ui button small mg-r-5 itemMoveCtrl"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5 itemMoveCtrl"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
        </div>
        <div class="ui center-button-group mg-t-15">
             <span class="inner">
                 <button type="button" class="ui button xlarge cyan font-malgun" id="setItemRelationBtn">저장</button>
                 <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
             </span>
        </div>
    </form>
</div>
<input type="file" name="introImg" data-file-id="imgIntro"  style="display: none;"/>

<script type="text/javascript" src="/static/js/item/itemRelationMain.js?${fileVersion}"></script>
<script>
    var item = {};
    const hmpImgUrl = '${hmpImgUrl}';
    // 연관 상품관리 그리드
    ${itemRelationListGridBaseInfo}
    ${itemRelationDetailListGridBaseInfo}
</script>
<script type="text/javascript" src="/static/js/item/itemMainCommon.js?${fileVersion}"></script>