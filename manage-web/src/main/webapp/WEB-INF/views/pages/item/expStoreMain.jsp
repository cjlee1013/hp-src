<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">EXPRESS 점포 주문시간 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="expStoreSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) expStore.search();">
                <table class="ui table">
                    <caption>점포관리검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="150px">
                        <col >
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:120px;float:left;" data-default="storeNm">
                                <option value="storeNm">점포명</option>
                                <option value="storeId">점포코드</option>
                                <option value="regNm">등록자</option>
                                <option value="chgNm">수정자</option>
                            </select>
                            <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" style="width:200px"; maxlength="20" autocomplete="off">

                        </td>
                        <th>주문가능여부</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchOrderAvailYn" name="searchOrderAvailYn" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${orderAvailYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="submit" id="searchBtn" class="ui button large mg-r-5 cyan pull-left"><i class="fa fa-search"></i>검색</button>
                                <button type="button" id="searchResetBtn" class="ui button large mg-r-5 white pull-left">초기화</button>&nbsp;
                                <button type="button" id="excelDownloadBtn"	class="ui button large mg-r-5 dark-blue pull-left">엑셀다운</button>
                                <button type="button" id="histExcelDownloadBtn"	class="ui button large mg-r-5 dark-blue pull-left">변경이력다운</button>
                                <span style="margin-left:4px;"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="expStoreTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="expStoreListGrid" style="width: 100%; height: 350px;"></div>
        <div id="expStoreHistTotalListPopGrid" style="display: none"></div>

    </div>

    <form id="expStoreSetForm" name="expStoreSetForm" onsubmit="return false;">
        <input type="hidden" id="useYn" name="useYn" value="">
        <div class="com wrap-title sub">
            <h3 class="title">• EXPRESS 점포 추가 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>EXPRESS 점포 추가 정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포코드</th>
                        <td>
                            <div class="ui form inline">
                                <input id="storeId" name="storeId" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="10" readonly>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포명</th>
                        <td>
                            <div class="ui form inline">
                                <input id="storeNm" name="storeNm" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="20" readonly>
                                <button type="button" id="expStoreHist" class="ui button medium mg-l-5" style="display: none" >변경이력</button>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <th>주문가능 여부<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="orderAvailYn" name="orderAvailYn" style="width: 150px;">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${orderAvailYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>

                                <select class="ui input medium mg-r-10" id="stopReasonCd" name="stopReasonCd" style="width: 150px;" disabled>
                                    <option value="" selected>사유선택</option>
                                    <c:forEach var="codeDto" items="${stopReasonCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input id="stopReasonTxt" name="stopReasonTxt" type="text" class="ui input medium mg-r-5" placeholder="직접입력" style="width: 150px; display: none" maxlength="12">
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <th>주문가능 시간<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10 timeArea" id="orderStartHour" name="orderStartHour" style="width: 80px;">
                                    <option value="">선택</option>
                                    <c:forEach var="i"  begin="1" end="23" step="1" varStatus="status">
                                        <option value="<c:if test="${i < 10}">0</c:if>${i}"><c:if test="${i < 10}">0</c:if>${i}</option>
                                    </c:forEach>
                                </select>

                                <select class="ui input medium mg-r-10 timeArea" id="orderStartMinute" name="orderStartMinute" style="width: 80px;">
                                    <option value="">선택</option>
                                    <option value="00">00</option>
                                    <c:forEach var="i"  begin="10" end="50" step="10" varStatus="status">
                                        <option value="${i}">${i}</option>
                                    </c:forEach>
                                </select>
                                <span class="text"> ~ </span>
                                <select class="ui input medium mg-r-10 timeArea" id="orderEndHour" name="orderEndHour" style="width: 80px;">
                                    <option value="">선택</option>
                                    <c:forEach var="i"  begin="1" end="23" step="1" varStatus="status">
                                        <option value="<c:if test="${i < 10}">0</c:if>${i}"><c:if test="${i < 10}">0</c:if>${i}</option>
                                    </c:forEach>
                                </select>

                                <select class="ui input medium mg-r-10 timeArea" id="orderEndMinute" name="orderEndMinute" style="width: 80px;">
                                    <option value="">선택</option>
                                    <option value="00">00</option>
                                    <c:forEach var="i"  begin="10" end="50" step="10" varStatus="status">
                                        <option value="${i}">${i}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>배달소요시간<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="deliveryTime" name="deliveryTime" type="text" class="ui input medium mg-r-5 timeArea" style="width: 150px;" maxlength="3" >
                                <span class="text"> 숫자입력만 가능합니다. 입력예시) 60</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
        </table>
        </div>
    </form>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setStoreBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/expStoreMain.js?${fileVersion}"></script>

<script>
    // 점포 리스트 리얼그리드 기본 정보
    ${expStoreListGridBaseInfo}

    //이력 다운용 리얼그리드
    ${expStoreHistTotalListPopGridBaseInfo}

</script>

