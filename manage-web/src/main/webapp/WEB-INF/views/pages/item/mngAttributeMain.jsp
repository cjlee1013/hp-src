<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">상품속성관리</h2>
    </div>
    <div class="com wrap-title sub" >

        <div class="ui wrap-table horizontal mg-t-10" style="margin-bottom: 50px;">
            <form id="mngAttributeSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  attributeGroupList.search();">
                <table class="ui table">
                    <caption>구분</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>구분</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchGattrType" name="searchGattrType" style="width:100px;float:left;" data-default="전체">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${gattrType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchUseYn" name="searchUseYn" style="width: 100px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" class="ui button large cyan" id="searchGroupBtn"><i class="fa fa-search"></i>검색</button>
                                <br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetGroupBtn">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:100px;float:left;">
                                <option value="gattrNo">분류번호</option>
                                <option value="chgNm">수정자</option>
                            </select>
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="20" autocomplete="off" style="width:250px;float:left;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="width: 48%; height: 600px; float: left;">
            <h3 class="title" style="float:none;">속성분류정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>속성분류 수 : <span id="attributeGroupTotalCount">0</span>건</h3>
            </div>
            <div class="mg-t-20 mg-b-20" id="attributeGroupListGrid" style="width: 100%; height: 350px;"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <form id="setAttributeGroupForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  attributeList.setAttribute();">
                    <table class="ui table">
                        <colgroup>
                            <col width="180px">
                            <col width="200px">
                            <col width="160px">
                            <col />
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>분류번호</th>
                            <td>
                                <input type="text" id="gattrNo" name="gattrNo" class="ui input medium mg-r-5" style="width: 100px;" readonly/>
                            </td>
                            <th class="text-red-star">구분</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="gattrType" name="gattrType" class="ui input medium mg-r-10" style="width: 100px">
                                        <option value="">선택</option>
                                        <c:forEach var="codeDto" items="${gattrType}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">노출분류명</th>
                            <td colspan="3">
                                <input type="text" id="gattrNm" name="gattrNm" class="ui input medium mg-r-5" style="width: 330px;" maxlength="12">
                            </td>
                        </tr>
                        <tr id="gattrMngNmTr">
                            <th class="text-red-star">관리분류명</th>
                            <td colspan="3">
                                <input type="text" id="gattrMngNm" name="gattrMngNm" class="ui input medium mg-r-5" style="width: 330px;" maxlength="12">
                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">선택방식</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="multiYn" name="multiYn" class="ui input medium mg-r-10" style="width: 100px">
                                        <option value="">선택</option>
                                        <c:forEach var="codeDto" items="${multiYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                            <th>사용여부</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="gattrUseYn" name="gattrUseYn" class="ui input medium mg-r-10" style="width: 100px">
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr id="gattrDispTypeTr">
                            <th class="text-red-star">노출유형</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <select id="gattrDispType" name="gattrDispType" class="ui input medium mg-r-10" style="width: 100px">
                                        <option value="">선택</option>
                                        <c:forEach var="codeDto" items="${gattrDispType}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="ui form inline" style="margin-top: 20px;margin-bottom:20px;float: right;">
                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setAttributeGroupBtn" >저장</button>
                <span style="margin:4px;"></span>
                <button type="button" class="ui button large mg-r-5 white pull-left" id="resetAttributeGroupBtn">초기화</button>
            </div>
        </div>

        <div style="width: 48%; height: 600px;float: right;">
            <h3 class="title" style="float:none;">속성정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>속성 수 : <span id="attributeTotalCount">0</span>건</h3>
            </div>
            <div class="mg-t-20 mg-b-20" id="attributeListGrid" style="width: 100%; height: 350px;"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <form id="setAttributeForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  attributeList.setAttribute();">
                    <input type="hidden" id="mGattrNo" name="mGattrNo">
                    <input type="hidden" id="dispType" name="dispType">
                    <table class="ui table">
                        <colgroup>
                            <col width="170px">
                            <col width="150px">
                            <col width="120px">
                            <col />
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>속성번호</th>
                            <td>
                                <input type="text" id="attrNo" name="attrNo" class="ui input medium mg-r-5" style="width: 100px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">속성명</th>
                            <td colspan="3">
                                <input type="text" id="attrNm" name="attrNm" class="ui input medium mg-r-5" style="width: 300px;">
                                <br>※ 검색태그의 속성명은 최대 6자, 상품속성의 속성명은 최대 12자까지 입력해주세요. 입력가능 글자수 초과 시 프론트 노출 확인이 필요합니다.
                            </td>
                        </tr>
                        <tr>
                            <th class="text-red-star">우선순위</th>
                            <td>
                                <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 80px;"/>
                            </td>
                            <th>사용여부</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="attrUseYn" name="attrUseYn" class="ui input medium mg-r-10" style="width: 100px">
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr id ="addImg" style="display: none">
                            <th class="text-red-star">이미지등록</th>
                            <td colspan="3">
                                <div style="position:relative; display:inline-block;">
                                    <div class="text-center preview-image" style="height:100px;min-width: 100px;;">
                                        <div class="imgDisplayView" style="display:none;">
                                            <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="attributeList.img.deleteImg();">삭제</button>
                                            <div id="uploadImg" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                                <img class="imgUrlTag" width="100" height="100" src="">
                                                <input type="hidden" class="imgUrl" id ="imgUrl" name="imgUrl" value="">
                                                <input type="hidden" class="imgNm" id ="imgNm" name="imgNm" value="">
                                                <input type="hidden" class="imgHeight" id ="imgHeight" name="imgHeight" value="">
                                                <input type="hidden" class="imgWidth" id ="imgWidth" name="imgWidth" value="">
                                            </div>
                                        </div>

                                        <div class="imgDisplayReg" style="padding-top: 40px;">
                                            <button type="button" class="ui button medium" onclick="attributeList.img.clickAddImgButton();">등록</button>
                                        </div>

                                    </div>
                                    <div style="width:150px; height:100px;display: inline-block;padding-left: 10px;">
                                        <br><br>
                                        최적화 가이드<br>
                                        사이즈 : 120*120<br>
                                        용량 : 2MB 이하<br>
                                        확장자 : JPG / JPEG / PNG<br>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr id ="addColor" style="display: none">
                            <th class="text-red-star">색상코드입력</th>
                            <td colspan="3">
                                <div class="ui form inline" style="float: left">
                                    <select id="colorType" name="colorType" class="ui input medium mg-r-10" style="width: 80px;">
                                        <c:forEach var="codeDto" items="${gattrColorType}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                    <input type="text" id="colorCode" name="colorCode" class="ui input medium mg-r-5" style="width: 200px;" placeholder="색상코드 입력(ex : #ffffff)"/>
                                    <div id ="singleColor" style ="background-color:#FFFFFF; width: 38px;height: 38px; border-radius: 50px;float: right " >

                                    </div>
                                    <div id ="mixColor"  class="mixedicon" style="display: none">

                                    </div>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="ui form inline" style="margin-top: 10px;float: right;">
                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setAttributeBtn" >저장</button>
                <span style="margin:4px;"></span>
                <button type="button" class="ui button large mg-r-5 white pull-left" id="resetAttributeBtn">초기화</button>
            </div>
        </div>

    </div>
</div>
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/mngAttributeMain.js?${fileVersion}"></script>

<script>
    ${attributeGroupListGridBaseInfo}
    ${attributeListGridBaseInfo}
    const hmpImgUrl = '${homeImgUrl}';
</script>