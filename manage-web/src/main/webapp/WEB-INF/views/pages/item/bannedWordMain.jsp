<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">금칙어 관리</h2>
    </div>
    <div class="com wrap-title sub" >

        <div class="ui wrap-table horizontal mg-t-10">
            <form id="bannedWordSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  bannedWord.search();">
                <input type="hidden" id="searchCateCd" name="searchCateCd">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="150px">
                        <col width="60%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>

                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:120px; float:left;">
                                <option value="keyword">키워드</option>
                                <option value="regNm">등록자</option>
                                <option value="chgNm">수정자</option>
                            </select>
                        </td>
                        <td colspan="2">
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" style="width:400px;" maxlength="20" autocomplete="off">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>금칙어 유형</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchBannedType" name="searchBannedType" style="width: 120px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${bannedType}" varStatus="status">
                                        <c:set var="selected" value="" />
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchUseYn" name="searchUseYn" style="width: 120px;">
                                <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:left; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색된 결과 : <span id="bannedWordTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="bannedWordListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">금칙어 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="bannedWordSetForm" onsubmit="return false;">
            <input type="hidden" id="bannedSeq" name="bannedSeq">
            <input type="hidden" id="cateKind" name="cateKind">
            <input type="hidden" id="cateCd" name="cateCd">
            <table class="ui table">
                <caption>금칙어 등록/수정</caption>
                <tbody>
                <tr>
                    <th>유형&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="bannedType" name="bannedType" style="width: 150px;">
                                    <option value="">선택</option>
                                <c:forEach var="codeDto" items="${bannedType}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>금칙어 키워드&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5" style="width: 450px;" maxlength="25">
                            <span class="text">( <span style="color:red;" id="keywordCount">0</span> / 25자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>설명</th>
                    <td>
                        <span class="ui form inline">
                            <textarea id="keywordDesc" name="keywordDesc" class="ui input mg-r-5" style="float:left;width: 450px; height: 122px;" maxlength="50"></textarea>
                            <span class="text">( <span style="color:red;" id="keywordDescCount">0</span> / 50자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            <button class="ui button xlarge cyan font-malgun" id="setBannedWordBtn">등록</button>
            <button class="ui button xlarge dark-blue font-malgun" id="editBannedWordBtn">수정</button>
        </span>
    </div>
</div>

<iframe style="display:none;" id="bannedWordExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/bannedWordMain.js?${fileVersion}"></script>

<script>
    // 브랜드 리스트 리얼그리드 기본 정보
    ${bannedWordListGridBaseInfo}
</script>