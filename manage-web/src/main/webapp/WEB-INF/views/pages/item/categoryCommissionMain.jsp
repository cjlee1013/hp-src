<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>

<!-- 기본 프레임 -->
<input id="cateCd" type="hidden">
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 기준수수료</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="60%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lcateCd" style="width:150px;float:left;" data-default="1depth" onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="mcateCd" style="width:150px;float:left;" data-default="2depth" onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="scateCd" style="width:150px;float:left;" data-default="3depth" onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                        </td>
                        <td>
                            <!-- 우측 검색 버튼 -->
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn"><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="commissionCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="commissionGrid" style="width:100%; height:350px;"></div>

        <div class="com wrap-title sub">
            <h3 class="title">카테고리 기준수수료율 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table mg-t-10">
            <table class="ui table">
                <caption>카테고리 기준수수료 등록/수정</caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <tbody>
                <tr>
                    <th >1depth</th>
                    <th>2depth</th>
                    <th>3depth</th>
                    <th>4depth</th>
                    <th>기준수수료율(VAT포함)</th>
                </tr>
                <tr>
                    <td>
                        <div class="ui form inline">
                            <input id="one" type="text" class="ui input medium mg-r-5" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input id="two" type="text" class="ui input medium mg-r-5" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input id="three" type="text" class="ui input medium mg-r-5" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input id="four" type="text" class="ui input medium mg-r-5" readonly>
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input id="comm" type="text" class="ui input medium mg-r-5" style="width:80px">
                            <span class="text">%</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
				<span class="inner">
                    <button type="button" class="ui button xlarge cyan font-malgun" id="setCategoryBtn">저장</button>
					<button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
				</span>
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript">
    ${commissionGridBaseInfo}
</script>
<script type="text/javascript" src="/static/js/item/categoryCommission.js?v=${fileVersion}"></script>
