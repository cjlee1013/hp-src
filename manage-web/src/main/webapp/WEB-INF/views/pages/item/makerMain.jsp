<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">제조사 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="makerSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  makerMain.search();">
                <input type="hidden" name="useYn" />
                <table class="ui table">
                    <caption>제조사 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="150px">
                        <col width="420px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>제조사</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:150px;float:left;" data-default="제조사명">
                                <option value="makerNm">제조사명</option>
                                <option value="makerCode">제조사코드</option>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="20" autocomplete="off">
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn" ><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 dark-blue pull-left" id="excelDownloadBtn">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색된 제조사 : <span id="makerTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="makerListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">제조사 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="makerSetForm" onsubmit="return false;">
        <table class="ui table">
            <caption>제조사 등록/수정</caption>
            <colgroup>
                <col width="120px">
                <col>
                <col width="120px">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th>제조사 코드</th>
                <td>
                    <div class="ui form inline">
                        <input id="makerNo" name="makerNo" type="text" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                    </div>
                </td>
                <th>사용여부 <span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                            <option value="Y">사용</option>
                            <option value="N">미사용</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>제조사명 <span class="text-red">*</span></th>
                <td colspan="3">
                    <span class="ui form inline">
                        <input id="makerNm" name="makerNm" type="text" class="ui input medium mg-r-5" style="width: 250px;" maxlength="15">
                        <span class="text">( <span style="color:red;" id="textCountKr">0</span> / 15자 )</span>
                    </span>
                </td>
            </tr>
            <tr>
                <th>홈페이지 URL</th>
                <td colspan="3">
                    <span class="ui form inline">
                        <input id="siteUrl" name="siteUrl" type="text" class="ui input medium mg-r-5" style="width: 650px;" maxlength="200">
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setMakerBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>
<iframe style="display:none;" id="makerExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/makerMain.js?${fileVersion}"></script>

<script>
	// 제조사 리스트 리얼그리드 기본 정보
    ${makerListGridBaseInfo}
</script>
