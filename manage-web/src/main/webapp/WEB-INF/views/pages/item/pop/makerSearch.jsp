<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup large popup-request">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            <i class="fa fa-clone mg-r-5"></i> 제조사 검색
        </h3>
    </div>
    <!-- 검색옵션 -->
    <form id="makerPopSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) makerPopup.search();">
        <input type="hidden" name="useYn" value="Y" />
        <div class="pd-t-20">

            <div class="ui form inline" style="float:left">
                <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:150px;" data-default="제조사명">
                    <option value="makerNm">제조사명</option>
                    <option value="makerCode">제조사코드</option>
                </select>
            </div>

            <div class="ui form inline" style="float:left">
                <input name="searchKeyword" id="searchKeyword" type="text" style="width: 430px;" class="ui input medium mg-r-5" autocomplete="off" maxlength="20">
                <button type="button" id="searchBtn" class="ui button medium btn-warning"><i class="fa fa-search"></i> 검색</button>
                <span style="margin-left:4px;"></span>
                <button type="button" id="searchResetBtn" name="initEventBtn" class="ui button medium">초기화</button>
            </div>
            <!-- clear -->
            <div class="clearfix"></div>
        </div>
    </form>
    <!-- 그리드 영역 -->
    <div class="mg-t-20 mg-b-20" id="makerSearchPopGrid" style="width: 100%; height: 350px;"></div>

    <!-- 하단 버튼 -->
    <div class="ui form inline mg-t-15 text-center">
        <button type="button" id="selectBtn" class="ui button xlarge btn-danger mg-r-10">선택</button>
        <button type="button" id="closeBtn" class="ui button xlarge">취소</button>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>

	// 제조사검색팝업 리얼그리드 기본 정보
    ${makerSearchPopGridBaseInfo}

    $(document).ready(function() {
        makerSearchPopGrid.init();
        makerPopup.init();
        CommonAjaxBlockUI.global();
    });

    // 제조사검색팝업 그리드
    var makerSearchPopGrid = {
        gridView : new RealGridJS.GridView("makerSearchPopGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            makerSearchPopGrid.initGrid();
            makerSearchPopGrid.initDataProvider();
            makerSearchPopGrid.event();
        },
        initGrid : function() {
            makerSearchPopGrid.gridView.setDataSource(makerSearchPopGrid.dataProvider);

            makerSearchPopGrid.gridView.setStyles(makerSearchPopGridBaseInfo.realgrid.styles);
            makerSearchPopGrid.gridView.setDisplayOptions(makerSearchPopGridBaseInfo.realgrid.displayOptions);
            makerSearchPopGrid.gridView.setColumns(makerSearchPopGridBaseInfo.realgrid.columns);
            makerSearchPopGrid.gridView.setOptions(makerSearchPopGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            makerSearchPopGrid.dataProvider.setFields(makerSearchPopGridBaseInfo.dataProvider.fields);
            makerSearchPopGrid.dataProvider.setOptions(makerSearchPopGridBaseInfo.dataProvider.options);
        },
        event : function() {
            // 그리드 더블클릭 선택
            makerSearchPopGrid.gridView.onDataCellDblClicked = function(gridView, index) {
                makerPopup.selectMaker();
            };
        },
        setData : function(dataList) {
            makerSearchPopGrid.dataProvider.clearRows();
            makerSearchPopGrid.dataProvider.setRows(dataList);
        }
    };

    // 제조사검색팝업
    var makerPopup = {
        init : function() {
            this.bindingEvent();
        },
        bindingEvent : function() {
            $('#searchBtn').click(function() {
                makerPopup.search();
            });

            $('#searchResetBtn').click(function() {
                makerPopup.searchFormReset();
            });

            $('#selectBtn').click(function() {
                makerPopup.selectMaker();
            });

            $('#closeBtn').click(function() {
                makerPopup.cancel();
            });

            $("#searchKeyword").notAllowInput('keyup', ['SPC_SCH']);
        },
        search : function() {
            if ($('#searchKeyword').val() && $.jUtil.isNotAllowInput($('#searchKeyword').val(), ['SPC_SCH'])){
                alert($.jUtil.getNotAllowInputMessage(["SPC_SCH"]));
                $('#searchKeyword').focus();
                return false;
            }

            CommonAjax.basic({url:'/item/maker/getMakerList.json?' + $('#makerPopSearchForm').serialize(), data:null, method:"GET", successMsg:null, callbackFunc:function(res) {
                makerSearchPopGrid.setData(res);
            }});
        },
        searchFormReset : function() {
            $('#searchKeyword').val('');
        },
        selectMaker : function() {
            var selectRowId = makerSearchPopGrid.gridView.getCurrent();

            if (selectRowId.dataRow < 0) {
                alert('검색결과에서 제조사 정보를 선택하세요.');
                return false;
            }

            var rowDataJson = makerSearchPopGrid.dataProvider.getJsonRow(selectRowId.dataRow);

            $('#makerNo', opener.document).val(rowDataJson.makerNo);
            $('#makerNm', opener.document).val(rowDataJson.makerNm);
            this.cancel();
        },
        cancel : function() {
            window.close();
        }
    };
</script>
