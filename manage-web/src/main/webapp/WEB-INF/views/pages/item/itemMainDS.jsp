<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<input type="hidden" name="mallType" id="mallType" value="${mallType}" />
<input type="hidden" name="storeType" id="storeType" value="DS" />
<div class="com wrap-title sub">
    <h3 class="title">• 기본정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>기본정보</caption>
        <colgroup>
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>상품번호</th>
            <td>
                <div class="ui form inline">
                    <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" style="width: 150px;" readonly>
                </div>
            </td>
            <th>상품상태</th>
            <td colspan="3">
                <span class="text-normal itemStatusNm">-</span>
                <button type="button" id="itemHist" class="ui button medium mg-l-10" data-store-type="DS" style="display: none" >변경이력</button>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">상품명</span></th>
            <td colspan="5">
                <span class="ui form inline">
                    <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" style="width: 650px;" maxlength="50">
                    <span class="text">( <span style="color:red;" id="itemNmCount">0</span> / 50자 )</span>
                </span>
            </td>
        </tr>
        <tr>
            <th>상품구분</th>
            <td>
                <span id="itemDivision" class="text-normal"></span>
            </td>
            <th>상품유형</th>
            <td>
                <div class="ui form inline">
                    <select id="itemType" style="width: 180px" name="itemType" class="ui input medium">
                        <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                            <c:set var="selected" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <c:if test="${codeDto.ref2 ne 'TD'}">
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
            </td>
            <th>판매자 상품코드</th>
            <td>
                    <span class="ui form inline">
                        <input id="sellerItemCd" name="sellerItemCd" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="25">
                    </span>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">카테고리</span></th>
            <td colspan="5">
                <select class="ui input medium mg-r-5" id="cateCd1" name="lcateCd"
                        style="width:150px;float:left;" data-default="대분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(1,'cateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="cateCd2" name="mcateCd"
                        style="width:150px;float:left;" data-default="중분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(2,'cateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="cateCd3" name="scateCd"
                        style="width:150px;float:left;" data-default="소분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(3,'cateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="cateCd4" name="dcateCd"
                        style="width:150px;" data-default="세분류">
                </select>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">판매업체</span></th>
            <td colspan="3">
                <div class="ui form inline mg-t-10">
                    <input type="text" name="businessNm" id="businessNm" style="margin-right: -1px; width:150px;" class="ui input readonly partnerInfo" placeholder="업체명을 입력하세요."/>
                    <ul id="searchPartnerLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 222px; display: none;"></ul>
                    <button type="button" class="partnerInfo" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getPartnerBtn">
                        <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                    </button>
                    <span id="partnerArea">
                        <input type="hidden" name="partnerId" id="partnerId"/>
                        <span id="spanPartnerNm" class="text text-blue"></span>
                        <button type="button" class="ui button small delete-thumb mg-l-5" onclick="item.sale.setPartnerYn('N')">x</button>
                    </span>
                </div>
            </td>
            <th>노출여부</th>
            <td>
                <div class="ui form inline">
                    <select class="ui input medium mg-r-10" id="dispYn" name="dispYn" style="width: 100px;">
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="selected" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub">
    <h3 class="title">• 판매정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal ">
    <table class="ui table">
        <caption>판매정보</caption>
        <colgroup>
            <col width="120px">
            <col width="250px">
            <col width="120px">
            <col width="150px">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th><span class="text-red-star">과세여부</span></th>
            <td colspan="5">
                <div id="taxYn">
                    <c:forEach var="codeDto" items="${taxYn}" varStatus="status">
                        <c:if test="${codeDto.ref2 eq 'DS'}">
                            <label class="ui radio inline"><input type="radio" name="taxYn" class="ui input medium" value="${codeDto.mcCd}" /><span>${codeDto.mcNm}</span></label>
                        </c:if>
                    </c:forEach>
                </div>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">정상가격</span></th>
            <td>
                <span class="ui form inline">
                    <input type="text" name="originPrice" id="originPrice" class="ui input " maxlength="9" style="width:90px;" data-comma-count="2" />
                    <span class="text mg-l-5">원</span>
                </span>
            </td>
            <th><span class="text-red-star">판매가격</span></th>
            <td>
                <span class="ui form inline">
                    <input type="hidden" id="originSalePrice"/>
                    <input type="text" name="salePrice" id="salePrice" class="ui input " maxlength="9" style="width:90px;" data-comma-count="2"/>
                    <span class="text mg-l-5">원</span>
                    <label class="ui checkbox mg-l-5">
                        <div class="hiddenPriceArea" id="salePriceDispYnArea" style="display: none;">
                            <input type="checkbox" name="cobuy.salePriceDispYn" style="display: none;" id="salePriceDispYn" value="Y"><span class="text text-guide">판매가격 노출</span>
                        </div>
                    </label>
                </span>
            </td>
            <th class="saleTypeNArea">
                <span class="text-red-star">수수료<br /> (VAT포함)</span>
            </th>
            <td class="saleTypeNArea">
                <span class="ui form inline" id="categoryCommissionDivId">
                    <select class="ui input mg-r-5" name="commissionType" id="commissionType" data-default="정률(%)" style="width:100px;" <c:if test="${roleCommissionYn eq 'N'}">disabled</c:if>>
                        <c:forEach var="codeDto" items="${commissionType}" varStatus="status">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                    <input type="text" name="commissionRate" id="commissionRate" class="ui input  mg-r-5" maxlength="9" style="width:80px;" <c:if test="${roleCommissionYn eq 'N'}">readonly</c:if>/>
                    <input type="text" name="commissionPrice" id="commissionPrice" class="ui input  mg-r-5" maxlength="9" style="width:80px; display: none;" <c:if test="${roleCommissionYn eq 'N'}">readonly</c:if>/>
                </span>
            </td>
            <th class="saleTypeDArea" style="display: none;">
                <span class="text-red-star">매입가</span>
            </th>
            <td class="saleTypeDArea" style="display: none;">
                <span class="ui form inline">
                    <input type="text" name="purchasePrice" id="purchasePrice" class="ui input  mg-r-5" maxlength="9" style="width:100px;"/>
                </span>
                <span class="text">원(VAT 제외)</span>
            </td>
        </tr>
        <tr>
            <th rowspan="2">할인가격<br />(셀러상품할인)</th>
            <td rowspan="2" colspan="3">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="dcYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline dcArea" style="display:none;">
                    <input type="text" name="dcPrice" id="dcPrice" class="ui input  mg-r-5" maxlength="9" style="width:140px;"/>
                    <span class="text mg-l-5">원 </span>
                    <span class="text mg-l-5">[할인률 : </span>
                    <span id="dcRate" class="text">0</span>
                    <span class="text">%]</span>
                </span>
                <div class="ui form inline mg-t-5 dcArea" style="display:none;">
                    <label class="ui checkbox mg-r-10">
                        <input type="checkbox" name="dcPeriodYn" id="dcPeriodYn" value="Y"><span class="text">특정기간만 할인</span>
                    </label>
                    <input type="text" name="dcStartDt" id="dcStartDt" class="ui input mg-r-5" style="width:140px;"/>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <input type="text" name="dcEndDt" id="dcEndDt" class="ui input mg-r-5" style="width:140px;"/>
                </div>
                <div class="ui form inline mg-t-5 dcArea" style="display:none;">
                    <span class="text">할인 시 수수료</span>
                    <select class="ui input mg-r-5" name="dcCommissionType" id="dcCommissionType" data-default="정률(%)" style="width:100px;" <c:if test="${roleCommissionYn eq 'N'}">disabled</c:if>>
                        <c:forEach var="codeDto" items="${commissionType}" varStatus="status">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                    <input type="text" name="dcCommissionRate" id="dcCommissionRate" class="ui input mg-r-5" maxlength="9" style="width:100px;" <c:if test="${roleCommissionYn eq 'N'}">readonly</c:if>/>
                    <input type="text" name="dcCommissionPrice" id="dcCommissionPrice" class="ui input mg-r-5" maxlength="9" style="width:100px;" <c:if test="${roleCommissionYn eq 'N'}">readonly</c:if>/>
                    <div class="mg-t-5 text-size-md text-gray-Lightest">•입력된 할인가격으로 판매됩니다. 할인 차액이 아닌 할인판매가격을 입력해주세요.</div>
                </div>
            </td>
            <th class="saleTypeNArea">기준수수료</th>
            <td class="saleTypeNArea"><span id="baseCommissionRate" class="text"></span> %</td>
            <th class="saleTypeDArea" style="display: none;"></th>
            <td class="saleTypeDArea" style="display: none;"></td>
        </tr>
        <tr>
            <th>원가</th>
            <td id="costPrice">
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">판매기간</span>
            </th>
            <td colspan="5">
                    <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="salePeriodYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>

                <span class="ui form inline">
                    <span id="saleStartDtDivId">
                        <input type="hidden" id="originSaleStartDt"/>
                        <input type="text" name="saleStartDt" id="saleStartDt" class="ui input mg-r-5" style="width:140px;"/>
                    </span>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <span id="saleEndDtDivId">
                        <input type="hidden" id="originSaleEndDt"/>
                        <input type="text" name="saleEndDt" id="saleEndDt" class="ui input mg-r-5" style="width:140px;"/>
                    </span>
                    <span class="text mg-l-10 text-guide hiddenPriceArea" id="hiddenPriceTxt" style="display: none;">(모객기간보다 길게 설정)</span>
                </span>
            </td>
        </tr>
        <tr>
            <th>예약판매설정</th>
            <td colspan="5">
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="rsvYn" class="ui input" value="${codeDto.mcCd}" ${checked} /><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <span class="text mg-r-10">예약판매기간</span>
                    <input type="hidden" name="saleRsvNo" id="saleRsvNo">
                    <input type="text" name="rsvStartDt" id="rsvStartDt" class="ui input mg-r-5" style="width:120px;"/>
                    <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                    <input type="text" name="rsvEndDt" id="rsvEndDt" class="ui input mg-r-5" style="width:120px;"/>
                    <span class="text mg-l-10">출하일</span>
                    <input type="text" name="shipStartDt" id="shipStartDt" class="ui input mg-r-5" style="width:120px;"/>
                </span>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">재고수량</span>
            </th>
            <td>
                <span class="ui form inline">
                    <input type="hidden" id="originstockQty"/>
                    <input type="hidden" id="stockQtyTemp"/>
                    <input type="text" name="stockQty" id="stockQty" class="ui input " style="width:100px;" maxlength="9" data-comma-count="1"/>
                    <span class="text mg-l-5 mg-r-10">개</span>
                    <label id="changeStockQtyLabel" class="ui checkbox mg-r-5 " style="display: none;">
                        <input type="checkbox" id="changeStockQtyCheck" name="chgStockQtyYn" value="Y"><span class="text">수량변경</span>
                    </label>
                    <%--<label class="ui checkbox mg-r-5"><input type="checkbox" id="setStockCall" name="setStockCall" value="Y"/><span class="text">재고연동</span></label>--%>
                </span>
            </td>
            <th><span class="text-red-star">판매단위수량</span></th>
            <td>
                <span class="ui form inline">
                    <span class="text"><input type="text" name="saleUnit" id="saleUnit" class="ui input " style="width:50px;text-align: center;" maxlength="3"/> 개</span>
                </span>
            </td>
            <th><span class="text-red-star">최소구매수량</span></th>
            <td>
                <span class="ui form inline">
                    <span class="text"> 1회 주문 시 <input type="text" name="purchaseMinQty" id="purchaseMinQty" class="ui input " style="width:50px;text-align: center;" maxlength="3"/> 개부터 구매가능</span>
                </span>
            </td>
        </tr>
        <tr>
            <th>구매수량제한</th>
            <td colspan="3">
                <div class="ui form inline mg-t-10">
                    <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="purchaseLimitYn" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                    </c:forEach>
                    <div id="purchaseLimitArea" class="mg-t-5">
                        <select class="ui input mg-r-5" name="purchaseLimitDuration" id="purchaseLimitDuration" data-default="1회별" style="width:100px;">
                            <c:set var="dfPurchaseLimitDuration" value="" />
                            <c:forEach var="codeDto" items="${limitDuration}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="dfPurchaseLimitDuration" value="${codeDto.mcCd}" />
                                </c:if>
                                <option value="${codeDto.mcCd}" data-reference1="${codeDto.ref1}" data-reference2="${codeDto.ref2}" data-reference3="${codeDto.ref3}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <span class="text">구매자 1명이 </span>
                        <span class="text purchaseLimitDuration purchaseLimitDurationP">1회에 </span>
                        <input type="text" name="purchaseLimitDay" id="purchaseLimitDay" class="ui input medium  purchaseLimitDuration purchaseLimitDurationO" value="1" style="width:50px;" maxlength="3"/>
                        <span class="text purchaseLimitDuration purchaseLimitDurationO">일 동안 </span>
                        <span class="text">최대 </span>
                        <input type="text" name="purchaseLimitQty" id="purchaseLimitQty" class="ui input " value="1" style="width:60px;" maxlength="3"/>
                        <span class="text">개까지 구매가능</span>
                    </div>
                </div>
            </td>
            <th>장바구니제한</th>
            <td>
                <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="cartLimitYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 옵션정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <!-- 옵션 타이틀 -->
    <input type="hidden" name="optTitleDepth" id="optDepth" value="0"/>
    <input type="hidden" name="opt1Title" id="opt1Title" value=""/>
    <input type="hidden" name="opt2Title" id="opt2Title" value=""/>

    <table class="ui table">
        <caption>옵션정보</caption>
        <colgroup>
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>텍스트형 옵션</th>
            <td>
                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="optTxtUseYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr class="setoptTxtFormAll" style="display: none;">
            <th>텍스트형 옵션목록</th>
            <td>
                <table class="ui table-inner">
                    <caption>텍스트형 옵션목록</caption>
                    <colgroup>
                        <col width="12.5%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>옵션단계</th>
                        <td>
                            <div class="setoptTxtFormAll">
                                <select class="ui input" name="optTxtDepth" id="optTxtDepth" data-default="단계선택" style="width:100px;">
                                    <c:forEach var="item"  begin="1" end="2" step="1" varStatus="status">
                                        <option value="${status.count}"  <c:if test="${status.count eq 1}"> selected </c:if> >${status.count}단계</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr id="optTxtDepthTr1" class="optTxtDepthTr">
                        <th>1단계 타이틀</th>
                        <td>
                                <span class="ui form inline">
                                    <input type="text" name="opt1Text" id="opt1Text" class="ui input optTxt mg-r-5" style="width:300px;" maxlength="25">
                                    <span class="text">(최대 25자)</span>
                                </span>
                        </td>
                    </tr>
                    <tr id="optTxtDepthTr2" class="optTxtDepthTr">
                        <th>2단계 타이틀</th>
                        <td>
                                <span class="ui form inline">
                                    <input type="text" name="opt2Text" id="opt2Text" class="ui input optTxt mg-r-5" style="width:300px;" maxlength="25"/>
                                    <span class="text">(최대 25자)</span>
                                </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 이미지정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>이미지정보</caption>
        <colgroup>
            <col width="115px">
            <col width="447px">
            <col width="115px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>
                <span class="text-red-star">상품이미지</span>
            </th>
            <td>
                <div id="imgMNDiv">
                    <c:forEach var="codeDto" items="${itemImgType}" varStatus="status">
                        <c:if test="${codeDto.mcCd eq 'MN'}">

                            <c:forEach var="idx" begin="0" end="${codeDto.ref1-1}" step="1">
                                <div style="position:relative; display:inline-block;">
                                    <div class="text-center preview-image" style="height:132px;">
                                        <div class="imgDisplayView" style="display:none;">
                                            <button type="button" id="productImgDelBtn${idx}" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                            <div id="${codeDto.mcCd}${idx}" class="uploadType itemImg" inputId="uploadMNIds" data-processkey="ItemMainImage" style="cursor:hand;">
                                                <img class="imgUrlTag" width="130" height="130" src="">
                                                <input type="hidden" class="mainYn" name="imgList[].mainYn" value="N"/>
                                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value=""/>
                                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                                <input type="hidden" class="imgType" name="imgList[].imgType" value="${codeDto.mcCd}"/>
                                            </div>
                                        </div>

                                        <div class="imgDisplayReg" style="padding-top: 50px;">
                                            <button type="button" id="productImgRegBtn${idx}" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="mainImg${idx}">등록</button>
                                        </div>
                                    </div>
                                    <div class="mg-t-5 imgMainYnRadioView" style="text-align: center;">
                                        <label class="ui radio">
                                            <input type="radio" name="mainYnRadio" value="${idx}"/><span>대표이미지</span>
                                        </label>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                    <div class="pd-t-20 text-size-md text-gray-Lightest">( 사이즈: 1200 x 1200 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG ) <button type="button" id="mainImgTest" class="ui button medium mg-t-5">프론트 대표이미지 확인용</button></div>
                </div>
            </td>
            <th>
                <span class="text-red-star">리스팅이미지</span>
            </th>
            <td>
                <div class="text-center preview-image" style="width:258px; height:132px;">
                    <div class="imgDisplayView" style="display:none;">
                        <button type="button" id="listImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                        <div id='imgLST' class="uploadType itemImg" data-type="LST" data-processkey="ItemListImage" style="cursor:hand;">
                            <img class="imgUrlTag" width="258px;" height="130px;" src="">
                            <div class="ui form inline" >
                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="" id="listImgUrl"/>
                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value="" id="listImgNm"/>
                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                <input type="hidden" class="imgType" name="imgList[].imgType" value="LST"/>
                            </div>
                        </div>
                    </div>
                    <div class="imgDisplayReg" style="padding-top: 50px;">
                        <button type="button" id="listImgRegBtn" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="listImg">등록</button>
                    </div>
                </div>
                <div class="pd-t-20 text-size-md text-gray-Lightest">( 사이즈: 900 X 470 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG ) <button type="button" id="listImgTest" class="ui button medium mg-t-5">프론트 리스트이미지 확인용</button> </div>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">상품상세설명</span>
            </th>
            <td colspan="3">
                <div id="detailHtml">
                    <!-- 에디터 영역 -->
                    <div id="editor1"></div>
                    <div class="mg-t-5">
                        <span class="pull-left">
                            <button type="button" class="ui button medium" onclick="if (confirm('등록된 상품상세설명을 모두 삭제하시겠습니까?')) {commonEditor.setHtml($('#editor1'), '');}">초기화</button>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30 shipArea">
    <h3 class="title">• 배송정보</h3>
</div>
<div class="topline shipArea"></div>
<div class="ui wrap-table horizontal shipArea">
    <table class="ui table">
        <caption>배송정보</caption>
        <colgroup>
            <col width="120px">
            <col width="40%">
            <col width="120px">
            <col width="40%">
        </colgroup>
        <tbody>
        <tr>
            <th rowspan="2">
                <span class="text-red-star">배송정책</span>
            </th>
            <td colspan="3">
                    <span class="ui form inline">
                        <select class="ui input mg-r-5" name="shipPolicyNo" id="shipPolicyNo" data-default="선택하세요" style="width:250px;">
                            <option value="">선택하세요</option>
                        </select>
                        <button type="button" class="ui button medium sky-blue mg-l-5" onclick="item.ship.getShipPolicyMainPop();">배송정보관리</button>
                    </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table class="ui table-inner">
                    <caption>배송상세정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="40%">
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>배송정보번호</th>
                        <td colspan="3"><span class="text deliveryDetail" id="detailShipPolicyNo"></span></td>
                    </tr>
                    <tr>
                        <th>배송정보명</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipPolicyNm"></span>
                        </td>
                        <th>배송주체</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipMngNm"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>배송유형</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipTypeNm"></span>
                        </td>
                        <th>배송방법</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipMethodNm"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>배송비종류</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipKindNm"></span>
                        </td>
                        <th>배송비</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipFee"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>배송비 결제방식</th>
                        <td>
                            <span class="text deliveryDetail" id="detailPrepaymentYn"></span>
                        </td>
                        <th>배송비 노출여부</th>
                        <td>
                            <span class="text deliveryDetail" id="detailShipFeeDispYn"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">반품교환 <br />배송비</span></th>
            <td colspan="3">
                <div class="ui form inline">
                    <input type="text" name="claimShipFee" id="claimShipFee" class="ui input medium shipBundleLayer" style="width:120px;" maxlength="6"><span class="text">원&nbsp;(편도)</span>
                </div>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">출고지</span></th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="releaseZipcode" id="releaseZipcode" class="ui input medium shipBundleLayer" readonly="readonly" style="width:60px;">
                    <button type="button" class="ui button small mg-l-5 mg-r-5 shipBundleLayer" id="releaseSchZipcode" onclick="zipCodePopup('item.ship.releaseSchZipcode');">우편번호
                    </button>
                    <input type="text" name="releaseAddr1" id="releaseAddr1" class="ui input medium mg-r-5 shipBundleLayer" readonly="readonly" style="width:250px;">
                    <input type="text" name="releaseAddr2" id="releaseAddr2" class="ui input medium shipBundleLayer mg-t-10" style="width:250px;" autocomplete="off">
                </div>
                <div style="width: 90%;" class="mg-t-10" >
                    <label class="ui checkbox mg-t-5 pull-right">
                        <input type="checkbox" id="copyPartnerAddr" class="shipBundleLayer">
                        <span class="text">사업자 주소 동일 적용</span>
                    </label>
                </div>
            </td>
            <th><span class="text-red-star">회수지</span></th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="returnZipcode" id="returnZipcode" class="ui input medium shipBundleLayer" readonly="readonly" style=" width:60px;">
                    <button type="button" class="ui button small mg-l-5 mg-r-5 shipBundleLayer" id="returnSchZipcode" onclick="zipCodePopup('item.ship.returnSchZipcode');">우편번호
                    </button>
                    <input type="text" name="returnAddr1" id="returnAddr1" class="ui input medium mg-r-5 shipBundleLayer" readonly="readonly" style="width:250px;">
                    <input type="text" name="returnAddr2" id="returnAddr2" class="ui input medium shipBundleLayer mg-t-10" style="width:250px;" autocomplete="off">
                </div>
                <div style="width: 90%" class="mg-t-10">
                    <label class="ui checkbox mg-l-5 pull-right">
                        <input type="checkbox" id="copyReleaseAddr" class="shipBundleLayer">
                        <span class="text">출고지 주소 동일 적용</span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">출고기한</span>
            </th>
            <td colspan="3">
                <span class="ui form inline">
                    <select class="ui input medium mg-r-5 shipBundleLayer" name="releaseDay" id="shipReleaseDay" data-default="선택" style="width:100px;">
                        <option value="">선택</option>
                        <option value="1">오늘발송</option>
                        <c:forEach var="item"  begin="2" end="15" step="1" varStatus="status">
                            <option value="${item}">${item}</option>
                        </c:forEach>
                        <option value="-1">미정</option>
                    </select>
                    <span id="releaseDetailInfo">
                        <span id="shipOneDay" class="text" style="display: none"> 당일 </span>
                        <span id="shipDays" class="text"> 일 이내 발송처리 </span>
                        <span id="shipReleaseTimeSpanId">
                            <select class="ui input medium mg-l-5 shipBundleLayer" name="releaseTime" id="shipReleaseTime" data-default="선택" style="width:70px;">
                                <option value="">선택</option>
                                <c:forEach var="item"  begin="1" end="24" step="1" varStatus="status">
                                    <option value="${status.count}">${status.count}</option>
                                </c:forEach>
                            </select>
                            <span class="text">시 결제 건까지 오늘발송처리 </span>
                        </span>
                        <select id='shipHolidayExceptYn' name='holidayExceptYn' class="ui input medium mg-l-5 shipBundleLayer" style="width: 170px">
                            <c:forEach var="codeDto" items="${holidayExcept}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </span>
                </span>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">안심번호 <br />서비스</span>
            </th>
            <td id="safetyNoDispYnNm" colspan="3">
                <c:forEach var="codeDto" items="${safeNumberUseYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="safeNumberUseYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 속성정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>속성정보</caption>
        <colgroup>
            <col width="120px">
            <col width="40%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>브랜드</th>
            <td>
                <div class="ui form inline mg-t-10">
                    <input type="text" name="brandNm" id="brandNm" style="margin-right: -1px; width:200px;" class="ui input" autocomplete="off" placeholder="브랜드명을 입력하세요." />
                    <ul id="searchBrandLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 222px; display: none;"></ul>
                    <button type="button" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getBrandBtn">
                        <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                    </button>
                    <span id="brandArea">
                        <input type="hidden" name="brandNo" id="brandNo" value="" />
                        <span id="spanBrandNm" class="text text-blue"></span>
                        <button type="button" id="deleteBrandBtn" class="ui button small delete-thumb mg-l-5" onclick="item.attr.setBrandYn('N')">x</button>
                    </span>
                </div>
            </td>
            <th>제조사</th>
            <td>
                <div class="ui form inline mg-t-10">
                    <input type="text" name="makerNm" id="makerNm" style="margin-right: -1px; width:200px;" class="ui input readonly makerInfo" placeholder="제조사명을 입력하세요."/>
                    <ul id="searchMakerLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 222px; display: none;"></ul>
                    <button type="button" class="makerInfo" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getMakerBtn">
                        <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                    </button>
                    <span id="makerArea">
                        <input type="hidden" name="makerNo" id="makerNo" value="" />
                        <span id="spanMakerNm" class="text text-blue"></span>
                        <button type="button" id="deleteMakerBtn" class="ui button small delete-thumb mg-l-5" onclick="item.attr.setMakerYn('N')">x</button>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <th>상품속성</th>
            <td colspan="3">
                <table class="ui table-inner mg-t-20" style="display: none;">
                    <caption>상품속성</caption>
                    <colgroup>
                        <col width="13%">
                        <col width="31.4%">
                        <col width="13%">
                        <col width="*">
                    </colgroup>
                    <tbody id="attrArea">
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>제조일자</th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="makeDt" id="makeDt" class="ui input mg-r-5" style="width:140px;"/>
                </div>
            </td>
            <th>유효일자</th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="expDt" id="expDt" class="ui input mg-r-5" style="width:140px;"/>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">상품고시정보</span>
            </th>
            <td colspan="3">
                <div class="ui form inline">
                    <select style="width: 340px" name="gnoticeNo" id="gnoticeNo" class="ui input medium">
                        <option value="0">상품군 선택</option>
                    </select>
                    <span class="pull-right">
                        <label class="ui checkbox mg-r-10">
                            <input type="checkbox" name="checkboxAllNoticeExp" id="checkboxAllNoticeExp" value="Y"><span class="text">전체 기본 문구 입력</span>
                        </label>
                    </span>
                </div>
                <div id="addNoticeData">
                    <table class="ui table-inner mg-t-20" style="">
                        <caption>상품속성</caption>
                        <colgroup>
                            <col width="13%">
                            <col width="31.4%">
                            <col width="13%">
                            <col width="*">
                        </colgroup>
                        <tbody id="addNoticeArea">

                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr id="isbnArea" style="display:none;">
            <th class="text-red-star">
                ISBN
            </th>
            <td colspan="3">
                <div class="ui form inline">
                    <input type="text" name="isbn" id="isbn" class="ui input mg-r-5" maxlength="13" style="width:340px;"/>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 부가정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>부가정보</caption>
        <colgroup>
            <col width="120px">
            <col width="50%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>성인상품유형</th>
            <td>
                <c:forEach var="codeDto" items="${adultType}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline" style="margin-right: 12px "><input type="radio" name="adultType" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <select class="ui input medium mg-r-5" id="imgDispYn" name="imgDispYn" style="width: 150px;">
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="selected" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <option value="${codeDto.mcCd}" ${selected}>이미지 ${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                </span>
            </td>
            <th>선물하기</th>
            <td>
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 ne 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="giftYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>가격비교 사이트</th>
            <td colspan="3">
                <c:forEach var="codeDto" items="${regYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 ne 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="epYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>검색키워드</th>
            <td colspan="3">
                <div class="ui form inline">
                    <input type="text" name="srchKeyword" id="srchKeyword" class="ui input" placeholder="예) 식품,라면,뽀글이,매운맛" maxlength="100" style="width:60%"/><br>
                    <span class="text">(키워드는 (,) 쉽표로 구분하여 입력해주세요. 최대 20개까지만 입력 가능합니다.)</span>
                </div>
            </td>
        </tr>
        <tr>
            <th>검색태그</th>
            <td colspan="3">
                <div id="schTagArea" class="ui wrap-table vertical mg-b-10">
                    <table class="ui table">
                        <caption>검색태그</caption>
                        <colgroup>
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="*">
                        </colgroup>
                        <tbody id="schTagTbodyArea">
                        </tbody>
                    </table>
                </div>
                <span class="text">선택한 태그 : </span><span id="selectedTags"></span>
            </td>
        </tr>
        <tr>
            <th class="text-red-star">해외배송</th>
            <td colspan="3">
                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 ne 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="globalAgcyYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr class="rentalArea" style="display: none;">
            <th class="text-red-star">월렌탈료</th>
            <td>
                <div class="ui form inline">
                    <input id="rentalFee" name="rentalFee" type="text" class="ui input medium" maxlength="9" style="width: 150px;" >
                </div>
            </td>
            <th class="text-red-star">등록비</th>
            <td>
                <div class="ui form inline">
                    <input id="rentalRegFee" name="rentalRegFee" type="text" class="ui input medium" maxlength="9" style="width: 150px;" >
                </div>
            </td>
        </tr>
        <tr class="rentalArea" style="display: none;">
            <th class="text-red-star">의무사용기간</th>
            <td>
                <div class="ui form inline">
                    <input id="rentalPeriod" name="rentalPeriod" type="text" class="ui input medium" maxlength="2" style="width: 150px;" ><span class="text">개월</span>
                </div>
            </td>
            <th>총렌탈료</th>
            <td>
                <span id="totalRentalFee" class="text"></span><span class="text">원</span>
            </td>
        </tr>
        <tr>
            <th class="text-red-star">안전인증</th>
            <td colspan="3">
                <table id="certTable" class="ui table-inner">
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <c:forEach var="codeDto" items="${itemCertGroup}" varStatus="status">
                        <tr id="certTr${codeDto.mcCd}">
                            <th>${codeDto.mcNm}</th>
                            <td>
                                <input type="hidden" name="certList[].itemCertSeq" class="itemCertSeq">
                                <input type="hidden" name="certList[].certGroup" class="certGroup" value="${codeDto.mcCd}">
                                <input type="hidden" name="certList[].isCert" class="isCert" value="N">
                                <c:set var="dfProdIsCert" value="" />
                                <c:forEach var="isCertDto" items="${itemIsCert}" varStatus="status2">
                                    <c:set var="checked" value="" />
                                    <c:choose>
                                        <c:when test="${codeDto.mcCd eq 'KD' and isCertDto.mcCd eq 'E'}">
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${isCertDto.ref1 eq 'df'}">
                                                <c:set var="dfProdIsCert" value="${isCertDto.mcCd}" />
                                                <c:set var="checked" value="checked" />
                                            </c:if>

                                            <label class="ui radio inline">
                                                <input type="radio" name="certList[${status.count}].isCert" id="cert_${codeDto.mcCd}_isCert_${isCertDto.mcCd}" class="ui input isCertRadio" data-cert-group="${codeDto.mcCd}" value="${isCertDto.mcCd}" ${checked} />
                                                <span>${isCertDto.mcNm}</span>
                                            </label>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>

                                <div class="ui form inline mg-t-10 certRow">
                                    <span class="addArea">
                                        <select class="ui input mg-r-5 certType" name="certList[].certType" data-cert-group="${codeDto.mcCd}" style="width:220px;">
                                            <option value="" >인증유형선택</option>
                                            <c:forEach var="typeCodeDto" items="${itemCertType}" varStatus="status">
                                                <c:set var="typeCodePrefix" value="${fn:substring(typeCodeDto.mcCd, 0, 2)}"/>
                                                <c:if test="${typeCodePrefix eq codeDto.mcCd}">
                                                    <option value="${typeCodeDto.mcCd}" data-ref1="${typeCodeDto.ref1}" data-ref2="${typeCodeDto.ref2}" data-ref3="${typeCodeDto.ref3}" >${typeCodeDto.mcNm}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                        <select class="energy ui input mg-r-5" style="display: none; width: 150px;">
                                            <option value="" >선택해주세요</option>
                                            <c:forEach var="etCer" items="${itemCertEtCer}" varStatus="status">
                                                <option value="${etCer.mcCd}" >${etCer.mcNm}</option>
                                            </c:forEach>
                                        </select>

                                        <input type="text" name="certList[].certNo" data-cert-group="${codeDto.mcCd}" class="ui input mg-r-5 certNo" style="width:300px;" maxlength="50" autocomplete="off"/>
                                        <span class="certNoSubArea"></span>
                                        <button type="button" class="ui button medium mg-r-5 certCheck" data-cert-group="${codeDto.mcCd}" onclick="item.ext.checkCert($(this))" >조회</button>
                                    </span>
                                    <c:if test="${codeDto.mcCd eq 'ER' or codeDto.mcCd eq 'ET'}">
                                        <button type="button" class="ui button medium dark-blue mg-r-5 certAdd">추가</button>
                                    </c:if>
                                </div>
                                <ul id="cert_${codeDto.mcCd}_detail" style="display: none;">
                                </ul>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>증빙서류</th>
            <td colspan="3">
                <table id="proofFileTable" class="ui table">
                    <colgroup>
                        <col width="50px">
                        <col width="130px">
                        <col width="350px">
                        <col width="30px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">구분</th>
                        <th class="text-center">설명</th>
                        <th class="text-center"></th>
                        <th class="text-center">서류 파일  PNG, JPG, JPEG, XLS, XLSX, PDF</th>
                    </tr>
                    <c:forEach var="codeDto" items="${proofFile}" varStatus="status">
                        <tr>
                            <td>${status.count}</td>
                            <td>${codeDto.mcNm}</td>
                            <td>${codeDto.ref1}</td>
                            <td>
                                <button type="button" class="ui button medium sky-blue proofUpload"
                                        data-file-type="${codeDto.mcCd}"
                                        data-field-name="proof${status.count}"
                                        onclick="item.file.clickFile($(this), 'PROOF');">추가
                                </button>
                            </td>
                            <td style="line-height: 30px;">
                                <span id="uploadProofFileSpan_${codeDto.mcCd}" data-file-type="${codeDto.mcCd}" class="ui form inline">
                                </span>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <button type="button" class="ui button medium mg-t-5" onclick="item.file.proofFileTotalDownload();">증빙서류 전체 다운로드</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div id="setStatusArea" class="ui right-button-group mg-t-15" style="display: none">
    <span class="text-size-lg" >• 판매상태 변경 : </span>
    <span class="inner">
        <button type="button" id="setStatusS" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('S', true)">영구중지</button>
        <button type="button" id="setStatusP" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('P', true)">일시중지</button>
        <button type="button" id="setStatusA" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('A', true)">판매가능</button>
    </span>
</div>
<div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" class="ui button xlarge white font-malgun" id="setItemTempBtn">임시저장</button>
        <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn">저장</button>
        <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        <button type="button" class="ui button xlarge white font-malgun">미리보기</button>
    </span>
</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="mainImg0" data-file-id="MN0"  style="display: none;"/>
<input type="file" name="mainImg1" data-file-id="MN1"  style="display: none;"/>
<input type="file" name="mainImg2" data-file-id="MN2"  style="display: none;"/>
<input type="file" name="listImg" data-file-id="imgLST"  style="display: none;"/>
<input type="file" name="proof1" data-file-id="uploadProofFileSpan_EXM"  style="display: none;"/>
<input type="file" name="proof2" data-file-id="uploadProofFileSpan_MAK"  style="display: none;"/>
<input type="file" name="proof3" data-file-id="uploadProofFileSpan_AWD"  style="display: none;"/>
<input type="file" name="proof4" data-file-id="uploadProofFileSpan_ORG"  style="display: none;"/>
<!-- 이미지정보 업로드 폼 -->
<script>

    // 옵션 그리드
    ${optListGridBaseInfo}
    // 추가구성 그리드
    ${optAddListGridBaseInfo}

    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';

    $(document).ready(function() {
        itemListGrid.init();
        item.init();
    });
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/item/itemMainGrid.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainDS.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>