<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
          상품상세 판매업체 공지
        </h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerBannerSchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  sellerBanner.search();">
                <table class="ui table">
                    <caption>상품상세 판매업체 공지</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="450px">
                        <col width="120px">
                        <col width="250px">
                        <col >
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDateType" style="width: 90px" name="schDateType" class="ui input medium mg-r-10" data-default="REGDT">
                                    <c:forEach var="codeDto" items="${bannerPeriodType}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>

                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:90px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:90px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispType" style="width: 100px" name="schDispType" class="ui input medium mg-r-10" data-default="ALL" >
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${dispType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="partnerNm">판매자명</option>
                                    <option value="partnerId">판매자ID</option>
                                    <option value="bannerNm">제목</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 200px;" minlength="2" >
                            </div>
                        </td>
                        <th>노출여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDispYn" style="width: 120px" name="schDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="schCateCd1" name="schCateCd1"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd2" name="schCateCd2"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd3" name="schCateCd3"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd4" name="schCateCd4"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="sellerBannerTotalCount">0</span>건 </h3>
                <span class="text-red">&nbsp; ※ 동일한 상품에 구분이 다른 공지가 중복 등록된 경우, 상품별>카테고리별>전체공지 순으로 우선 적용됩니다.</span>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="sellerBannerListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 상품상세 판매업체 공지 내용</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>상품상세 판매업체 공지 내용</caption>
                <colgroup>
                    <col width="150px">
                    <col width="150px">
                    <col width="150px">
                    <col width="150px">
                    <col width="150px">
                    <col width="150px">
                </colgroup>
                <tbody>
                <tr>
                    <th>제목</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span id="bannerNm" name="bannerNm" class="text" style="width: 50%;" ></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>구분</th>
                    <div class="ui form inline ">
                        <td colspan="3">
                            <span id="dispType" name="dispType" class="text" style="width: 50%;" ></span>
                        </td>
                    </div>
                </tr>
                <tr>
                    <th>판매자명(ID)</th>
                    <div class="ui form inline ">
                        <td colspan="3">
                            <span id="partnerId" name="partnerId" class="text" style="width: 50%;" ></span>
                        </td>
                    </div>
                </tr>
                <tr id="cateRow" style="display:none">
                    <th>카테고리</th>
                    <td colspan="3">
                        <span id="categoryNm" class="text" style="width: 50%;" ></span>
                    </td>
                </tr>
                <tr id="itemRow" style="display:none">
                    <th>상품</th>
                    <td colspan="3">
                        <span id="items" class="text" style="width: 50%;" ></span>
                    </td>
                </tr>
                <tr>
                    <th>공지내용</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span id="bannerDesc" name="bannerDesc" class="text" style="width: 50%;" ></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>노출여부</th>
                    <div class="ui form inline">
                        <td>
                            <span id="dispYn" name="dispYn" class="text" style="width: 50%;"></span>
                        </td>
                    </div>
                    <th>노출기간</th>
                    <td id="dispDateRow" style="display: none;">
                        <div class="ui form inline">
                            <span id="dispStartDt" name="dispStartDt" class="text" style="width: 50%;" ></span>
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <span id="dispEndDt" name="dispEndtDt" class="text" style="width: 50%;" ></span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/sellerBannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>

<script>
    // 상품상세 일괄공지 리스트 리얼그리드 기본 정보
    ${sellerBannerListGridBaseInfo}
</script>
