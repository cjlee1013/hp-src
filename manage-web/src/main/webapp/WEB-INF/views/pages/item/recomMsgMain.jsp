<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">상품 추천문구 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="recomMsgSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>상품 추천문구 관리</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="recomMsgMain.initCalendarDate('SEARCH', 'schStartDt', 'schEndDt', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomMsgMain.initCalendarDate('SEARCH', 'schStartDt', 'schEndDt', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomMsgMain.initCalendarDate('SEARCH', 'schStartDt', 'schEndDt', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomMsgMain.initCalendarDate('SEARCH', 'schStartDt', 'schEndDt', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 120px" name="schUseYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>점포유형</th>
                        <td>
                            <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 120px;">
                                <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                    <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" style="width: 120px" name="schType" class="ui input medium mg-r-10" >
                                    <option value="RECOMMNG" selected>관리명</option>
                                    <option value="RECOMMSG">추천문구</option>
                                    <option value="REGNM">등록자</option>
                                    <option value="ITEMNO">상품번호</option>
                                </select>
                                <input id="schValue" name="schValue" type="text" class="ui input medium mg-r-5" style="width:350px"; maxlength="20" autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="recomMsgTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="recomMsgGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 추천문구 등록/수정 </h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="recomMsgSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>추천문구 등록/수정 </caption>
                <colgroup>
                    <col width="12%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">관리명</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="recomMng" name="recomMng" class="ui input medium mg-r-5" maxlength="10" style="width:200px">
                            <span class="text"><span id="recomMngLen">0</span> / 10 자</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">추천문구</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="recomMsg" name="recomMsg" class="ui input medium mg-r-5" maxlength="14" style="width:200px">
                            <span class="text"><span id="recomMsgLen">0</span> / 14 자 (_ / ? ! ~ ( ) & % [ ] + ! * , . ☆ ★ ● ○ ▷ ◁ ♥ ♧ ※ ↑ ↓ → ← 만 사용가능)</span>
                        </div>
                    </td>
                </tr>
               <tr>
                   <th class="text-red-star">점포유형</th>
                   <td>
                       <select id="storeType" name="storeType" class="ui input medium" style="width: 100px;">
                           <c:forEach var="storeType" items="${storeType}" varStatus="status">
                               <option value="${storeType.mcCd}" ${storeType.ref2}>${storeType.mcNm}</option>
                           </c:forEach>
                       </select>
                   </td>
               </tr>
                <tr>
                    <th class="text-red-star">사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 100px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${useYn.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <option value="${useYn.mcCd}" ${checked}>${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">대상상품</th>
                    <td>
                        <span class="pull-left mg-b-10 ">* 등록 상품 : <span id="recomMsgItemCnt">0</span>건
                            <button type="button" class="ui button medium gray mg-l-5 mg-r-10" onclick="recomMsgItemGrid.removeCheckData()">선택삭제</button>
                        </span>

                        <span class="ui form inline mg-l-10">
                            <span class="text ">* 등록 상품 검색 : </span> <input type="text" id="itemGridSearchValue" name="itemGridSearchValue" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
                            <button type="submit" id="itemGridSearchBtn" class="ui button medium gray mg-l-10">검색</button>
                        </span>

                        </span>

                        <span class="pull-right mg-b-10">
                            <button type="button" class="ui button medium mg-r-5" onclick="recomMsgMain.popup.openPopup('ITEM');">상품등록</button>
                            <button type="button" class="ui button medium mg-r-5" onclick="recomMsgMain.popup.openPopup('EXCEL');">일괄등록</button>
                        </span>

                        <div class="mg-t-10 mg-b-20" id="recomMsgItemGrid" style="width: 100%; height: 350px;"></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">등록</button>
                <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
            </span>
        </div>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/recomMsgMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/recomMsgMain.grid.js?${fileVersion}"></script>
<script>

    // 리얼그리드 기본 정보
    ${recomMsgGridBaseInfo}
    ${recomItemGridBaseInfo}

</script>
