<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
	.popup-request {margin: 20px;}
	.tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
	.tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
	.tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
	.tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
	.wrap-table td {background-color: white; word-break:break-all;}
	.vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
	<div class="com popup-wrap-title">
		<h3 class="title pull-left">
			<i class="fa fa-clone mg-r-5"></i> 온라인재고 복수상품 일괄설정
		</h3>
	</div>
	<div class="tabui-wrap pd-t-20">
		<div class="com wrap-title sub">
			<h3 class="title pull-left">• 상품 수 : <span id="itemCount">0</span> 개</h3>
		</div>
	</div>
	<form id="itemSetForm" onsubmit="return false;">
	<div class="ui wrap-table horizontal mg-t-10">
		<table class="ui table">
			<caption>상품 검색</caption>
			<colgroup>
				<col width="20%">
				<col width="*">
			</colgroup>
			<tbody>
			<tr>
				<th>온라인재고기간</th>
				<td>
					<span class="ui form inline">
						<input type="text" name="onlineStockStartDt" id="onlineStockStartDt" class="ui input mg-r-5" style="width:140px;"/>
						<span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
						<input type="text" name="onlineStockEndDt" id="onlineStockEndDt" class="ui input mg-r-5" style="width:140px;"/>
					</span>
				</td>
			</tr>
			<tr>
				<th>일괄설정방법</th>
				<td>
					<span class="ui form inline">
						<span>
							<select class="ui input mg-r-5" name="setType" id="setType" style="width:150px;">
								<option value="" >선택</option>
								<option value="quantity" >수량입력</option>
								<option value="rate" >비율입력</option>
								<option value="auto" >자동설정</option>
								<option value="mix" >복합설정</option>
							</select>
						</span>
						<span>
							<select class="ui input mg-l-5 mg-r-5" name="setKind" id="setKind" style="width:150px;display: none;">
								<option value="" >선택</option>
								<option value="ff" >FF</option>
								<option value="gr" >GR</option>
								<option value="gm" >GM</option>
								<option value="cl" >CL</option>
								<option value="electric" >Electric</option>
							</select>
						</span>
						<span>
							<select class="ui input mg-l-5 mg-r-5" name="setEvenNumber" id="setEvenNumber" style="width:150px;display: none;">
								<option value="" >기본설정</option>
								<option value="E" >짝수설정</option>
							</select>
						</span>
					</span>
				</td>
			</tr>
			<tr>
				<th id="stockPrefix">수량입력(개)</th>
				<td>
					<span class="ui form inline">
						<span class="text">홈플러스</span>
						<input type="text" name="stockQty" id="setStockQty" class="ui input" style="width:100px;" maxlength="6" />
						<span class="text">N마트</span>
						<input type="text" name="stockQtyNaver" id="setRemainCntNaver" class="ui input" style="width:100px;" maxlength="6" />
						<span class="text">11번가</span>
						<input type="text" name="stockQtyEleven" id="setRemainCntEleven" class="ui input" style="width:100px;" maxlength="6" />
					</span>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	</form>
	<div class="ui center-button-group">
        <span class="inner">
			<button id="selectBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="onlineStockPop.close()">취소</button>
        </span>
	</div>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<script>

	const rowArr = opener.itemListGrid.gridView.getCheckedItems();
	let itemNoArr = [];
	for (let i in rowArr) {
		itemNoArr.push(opener.itemListGrid.gridView.getValue(rowArr[i], 'itemNo'));
	}

    // 팝업 html
    var onlineStockPop = {
        init : function() {
			$('#itemCount').text(rowArr.length);
			onlineStockPop.initDateTime('0d', 'onlineStockStartDt', 'onlineStockEndDt', "HH:00:00", "HH:59:59");
            this.event();
			$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').allowInput("keyup", ["NUM"]);
        },
		initDateTime : function (flag, startId, endId, _timeFormat, _endTimeFormat) {
			onlineStockPop.setDateTime = CalendarTime.datePickerRange(startId, endId, {timeFormat:_timeFormat}, true, {timeFormat:_endTimeFormat}, true);

			onlineStockPop.setDateTime.setEndDateByTimeStamp(flag);
			onlineStockPop.setDateTime.setStartDate(0);

			$("#"+endId).datepicker("option", "minDate", $("#"+startId).val());
		},
		initHtml : function() {
			for (var key in detailTable) {
				detailTable[key].html = '';
			}
		},
		event : function() {
			$("#selectBtn").bindClick(onlineStockPop.selectBtn);    // 등록 버튼

			$('.closeBtn').on('click', function() {
				onlineStockPop.close();
			});

			$('#setType').on('change', function(){
				if (!$.jUtil.isEmpty($(this).val())) {
					let stockPrefix = '비율입력(%)';
					switch ($(this).val()) {
						case 'quantity':
							stockPrefix = '수량입력(개)';
							$('#setKind, #setEvenNumber').hide();
							$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
							break;
						case 'rate':
							$('#setEvenNumber').show();
							$('#setKind').hide();
							$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
							break;
						case 'auto':
							$('#setKind, #setEvenNumber').show();
							$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', true);
							break;
						case 'mix':
							$('#setKind, #setEvenNumber').show();
							$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
							break;
					}

					$('#stockPrefix').text(stockPrefix);
				} else {
					$('#setKind, #setEvenNumber').hide();
					$('#stockPrefix').text('수량입력(개)');
					$('#setStockQty, #setRemainCntNaver, #setRemainCntEleven').val('').prop('disabled', false);
				}
			});
		},
		selectBtn : function() {
        	if (!confirm('저장하시겠습니까?')) {
				return false;
			}

			let setType = $('#setType').val();
			let setKind = $('#setKind').val();
			let setStockQty = Number($('#setStockQty').val());
			let setRemainCntNaver = Number($('#setRemainCntNaver').val());
			let setRemainCntEleven = Number($('#setRemainCntEleven').val());

			if ($.jUtil.isEmpty(setType)) {
				return $.jUtil.alert('일괄설정방법 입력해주세요.');
			}

			if (setType == 'auto' || setType == 'mix') {
				if ($.jUtil.isEmpty(setKind)) {
					return $.jUtil.alert('일괄설정방법 입력해주세요.');
				}
			}

			if (setType != 'auto' && ($.jUtil.isEmpty($('#setStockQty').val()) || $.jUtil.isEmpty($('#setRemainCntNaver').val()) || $.jUtil.isEmpty($('#setRemainCntEleven').val()))) {
				return $.jUtil.alert($('#stockPrefix').text() + ' 입력해주세요.');
			}


			if (setStockQty < setRemainCntNaver || setStockQty < setRemainCntEleven) {
				return $.jUtil.alert('제휴사 온라인재고기준은 홈플러스 온라인재고기준보다 크게 설정 할 수 없습니다.');
			}

			let itemForm        = $('#itemSetForm').serializeObject(true);
			itemForm['itemNoList'] = itemNoArr;

			CommonAjax.basic({
				url: '/item/setOnlineStockItem.json',
				data: JSON.stringify(itemForm),
				type: 'post',
				contentType: 'application/json',
				method:"POST",
				successMsg:null,
				callbackFunc:function(res) {
					alert(res.returnMsg);
					window.close();
				}
			});
		},
		close : function() {
        	if (confirm('취소하시겠습니까?')) {
				window.close();
			}
		}
    };

    $(document).ready(function() {
		CommonAjaxBlockUI.global();
		onlineStockPop.init();
    });

</script>