<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">점포 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="storeSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  store.search();">
                <table class="ui table">
                    <caption>점포관리검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="150px">
                        <col >
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchStoreType" name="searchStoreType" style="width:120px;float:left;" >
                                <option value="" selected>전체</option>
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>점포종류</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchStoreKind" name="searchStoreKind" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${storeKind}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="submit" id="searchBtn" class="ui button large mg-r-5 cyan pull-left"><i class="fa fa-search"></i>검색</button><
                                <button type="button" id="searchResetBtn" class="ui button large mg-r-5 white pull-left">초기화</button>&nbsp;
                                <button type="button" id="excelDownloadBtn"	class="ui button large mg-r-5 dark-blue pull-left">엑셀다운</button>
                                <span style="margin-left:4px;"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>구분</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchStoreAttr" name="searchStoreAttr" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${storeAttr}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchUseYn" name="searchUseYn" style="width:120px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="code" items="${useYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:120px;float:left;" data-default="storeNm">
                                <option value="storeNm">점포명</option>
                                <option value="storeId">점포코드</option>
                                <option value="onlineCostCenter">코스트센터</option>
                                <option value="regId">등록자</option>
                                <option value="chgId">수정자</option>
                            </select>
                            <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" style="width:200px"; maxlength="20" autocomplete="off">

                        </td>

                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="storeTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="storeListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <form id="storeSetForm" name="storeSetForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>기본정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="300px">
                        <col width="120px">
                        <col width="300px">
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포코드<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="storeId" name="storeId" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="10">
                                <input id="isNew" name="isNew" type="hidden" value="Y">
                            </div>
                        </td>
                        <th>점포명<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="storeNm" name="storeNm" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="20">
                            </div>
                        </td>
                        <th>영문점포명<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="storeNm2" name="storeNm2" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="10" >
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="storeType" name="storeType" style="width: 150px;">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>점포종류<span class="text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="storeKind" name="storeKind" style="width: 150px;">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${storeKind}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>코스트센터</th>
                        <td>
                            <span class="ui form inline">
                                <span class="text ">&nbsp;온라인</span><input id="onlineCostCenter" name="onlineCostCenter" type="text" class="ui input medium " style="width: 70px;" maxlength="5"><BR>
                                <span class="text">&nbsp;점포&nbsp;&nbsp;&nbsp;</span><input id="storeCostCenter" name="storeCostCenter" type="text" class="ui input medium mg-t-5" style="width: 70px;" maxlength="5">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>소재지역</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="region" name="region" style="width: 150px;">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${region}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>Fulfillment<BR>Center</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <div class="ui form inline">
                                    <input type="text" id="fcStoreId" name="fcStoreId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID">
                                    <input type="text" id="fcStoreNm" name="fcStoreNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                                    <button type="button" class="ui button medium" onclick="store.getStorePop('store.callBack.schFcStore');" >조회</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>픽업서비스</th>
                        <td>
                            <label class="ui radio medium inline"><input type="radio" name="pickupYn" value="Y" checked><span>제공</span></label>
                            <label class="ui radio medium inline"><input type="radio" name="pickupYn" value="N"><span>제공안함</span></label>
                        </td>

                        <th>기준점포</th>
                        <td colspan="3">
                            <div class="ui form inline" id="originStoreIdArea" style="display: none">
                                <input type="text" id="originStoreId" name="originStoreId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID" >
                                <input type="text" id="originStoreNm" name="originStoreNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                                <button type="button" class="ui button medium" onclick="store.getStorePop('store.callBack.schOriginStore');">조회</button>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>구분</th>
                       <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="storeAttr" name="storeAttr" style="width: 150px;">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${storeAttr}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>온라인여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="onlineYn" name="onlineYn" style="width: 150px;">
                                    <c:forEach var="codeDto" items="${onlineYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 상세정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="150px">
                    <col width="300px">
                    <col width="150px">
                    <col />
                </colgroup>
                <tbody>
                <tr>
                    <th>대표자명<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <input id="mgrNm" name="mgrNm" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="10">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>대표번호<span class="text-red"> *</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="phone" name="phone" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="15">
                        </span>
                    </td>
                    <th>FAX번호</th>
                    <td>
                        <span class="ui form inline">
                            <input id="fax" name="fax" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="15">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>휴무안내<span class="text-red"> *</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="closedInfo" name="closedInfo" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </span>
                    </td>
                    <th>운영시간</th>
                    <td>
                        <span class="ui form inline">
                            <input id="oprTime" name="oprTime" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>주소<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="zipcode" id="zipcode" class="ui input medium mg-r-5" style="width:150px;" readonly>
                            <button type="button" class="ui button small" onclick="zipCodePopup('store.setZipcode'); return false;">우편번호</button>
                            <br>
                            <input type="text" name="addr1" id="addr1" class="ui input medium mg-t-5 mg-r-5" style="width:250px;" readonly>
                            <input type="text" name="addr2" id="addr2" class="ui input medium mg-t-5" style="width:250px;">
                        </div>
                        <div style="height:20px;display: none" id="gibunAddrArea">
                            <span style="height:10px;display:block;"></span>
                            [지번] <span id="gibunAddr1Txt"></span>
                            <span id="gibunAddr2Txt"></span>
                        </div>
                    </td>
                </tr>
                </tbody>
        </table>
        </div>
    </form>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setStoreBtn">등록</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/storeMain.js?${fileVersion}"></script>

<script>
    // code
    var storeTypeJson = ${storeTypeJson};
    var storeKindJson = ${storeKindJson};
    var storeAttrJson = ${storeAttrJson};

    // 점포 리스트 리얼그리드 기본 정보
    ${storeListGridBaseInfo}
</script>

