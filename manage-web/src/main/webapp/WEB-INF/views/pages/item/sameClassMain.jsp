<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">동류그룹관리</h2>
    </div>
    <div class="com wrap-title sub" >

        <div class="ui wrap-table horizontal mg-t-10" style="margin-bottom: 50px;">
            <form id="sameClassGroupSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  sameClassGroupList.search();">
                <table class="ui table">
                    <caption>동류그룹 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="450px">
                        <col width="100px">
                        <col width="100px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td >
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px">
                                    <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" minlength="2" >
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 100px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline" style="float: right;">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchSameClassGroupBtn" ><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchSameClassGroupResetBtn">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="width: 55%; height: 600px; float: left;">
            <h3 class="title" style="float:none;">• 동류그룹정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>검색결과 : <span id="sameClassGroupTotalCount">0</span>건</h3>
            </div>

            <div class="mg-t-20 mg-b-20" id="sameClassGroupListGrid" style="width: 100%; height: 500px;"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <form id="setSameClassGroupForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  sameClassGroupList.setSameClassGroup();">
                    <table class="ui table" >
                        <colgroup>
                            <col width="150px">
                            <col width="400px">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th style="word-break: keep-all;">그룹번호</th>
                            <td >
                                <input type="text" id="sameClassNo" name="sameClassNo" class="ui input medium mg-r-5" style="width: 200px;" readonly/>

                            </td>
                        </tr>
                        <tr>
                            <th>그룹명 <span class="text-red">*</span></th>
                            <td >
                                <input type="text" id="sameClassNm" name="sameClassNm" class="ui input medium mg-r-5" style="width: 300px;" maxlength="50"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Hyper 제한수량</th>
                            <td class="inline">
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="hyperLimitYn" class="ui input medium" value="N" checked><span>제한안함</span>
                                </label>
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="hyperLimitYn" class="ui input medium" value="Y"><span>제한</span>
                                </label>
                                <span>
                                    <input type="text" id="hyperLimitQty" name="hyperLimitQty" class="ui input medium mg-l-5" style="width: 50px; display: inline;margin-right: 5px;"  maxlength="3" disabled><span>개</span>
                                </span>
                            </td>
                        </tr>
                        <tr>

                            <th>Club 제한수량</th>
                            <td class="inline">
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="clubLimitYn" class="ui input medium" value="N" checked><span>제한안함</span>
                                </label>
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="clubLimitYn" class="ui input medium" value="Y"><span>제한</span>
                                </label>
                                <span>
                                <input type="text" id="clubLimitQty" name="clubLimitQty" class="ui input medium mg-l-5" style="width: 50px; display: inline;margin-right: 5px;"  maxlength="3" disabled><span>개</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th>Express 제한수량</th>
                            <td class="inline">
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="expLimitYn" class="ui input medium" value="N" checked><span>제한안함</span>
                                </label>
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="expLimitYn" class="ui input medium" value="Y"><span>제한</span>
                                </label>
                                <span>
                                    <input type="text" id="expLimitQty" name="expLimitQty" class="ui input medium mg-l-5" style="width: 50px; display: inline;margin-right: 5px;" maxlength="3" disabled><span>개</span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th>사용여부</th>
                            <td >
                                <div class="ui form inline">
                                    <select id="sameClassGroupUseYn" style="width: 120px" name="useYn" class="ui input medium mg-r-10" >
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="ui form inline" style="margin-top: 10px;float: right;">
                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setSameClassGroupBtn" >저장</button>
                <span style="margin:4px;"></span>
                <button type="button" class="ui button large mg-r-5 white pull-left" id="resetSameClassGroupBtn">초기화</button>
            </div>
        </div>

        <div style="width: 43%; height: 600px;float: right;">
            <h3 class="title" style="float:none;">• 동류상품정보</h3>
            <div class="topline"></div>
            <div class="com wrap-title sub">
                <h3>검색결과 : <span id="sameClassTotalCount">0</span>건</h3>
            </div>
            <div class="mg-t-20 mg-b-20" id="sameClassListGrid" style="width: 100%; height: 500px;"></div>
            <div class="ui wrap-table horizontal mg-t-10">
                <form id="setSameClassForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  sameClassList.setSameClass();">
                    <input type="hidden" id="setSameClassNo" name="sameClassNo" value="">
                    <table class="ui table">
                        <colgroup>
                            <col width="80px">
                            <col width="350px">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>적용상품</th>
                            <td>
                                <div class="ui form inline">
                                    <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" style="width:100px">
                                    <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5"  style="width:180px" readonly>
                                    <button class="ui button medium sky-blue proofUpload" id="addItemPopUp">조회</button>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>사용여부</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="sameClassUseYn" style="width: 120px" name="useYn" class="ui input medium mg-r-10" >
                                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="ui form inline" style="margin-top: 10px;float: right;">
                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setSameClassBtn" >저장</button>
                <span style="margin:4px;"></span>
                <button type="button" class="ui button large mg-r-5 white pull-left" id="resetSameClassBtn">초기화</button>
            </div>
        </div>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/sameClassMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonItem.js?${fileVersion}"></script>>

<script>
    // 동류그룹,동류상품 리스트 리얼그리드 기본 정보
    ${sameClassGroupListGridBaseInfo}
    ${sameClassListGridBaseInfo}
</script>