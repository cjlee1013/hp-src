<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매가 변경이력</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="priceChangeHistGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 변경이력 팝업 그리드 기본정보
    ${priceChangeHistGridBaseInfo};

    $(document).ready(function () {
        priceChangeHistGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 변경이력 팝업 그리드
    var priceChangeHistGrid = {
        realGrid: new RealGridJS.GridView("priceChangeHistGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            priceChangeHistGrid.initGrid();
            priceChangeHistGrid.initDataProvider();
            priceChangeHistMng.search();
        },
        initGrid: function () {
            priceChangeHistGrid.realGrid.setDataSource(priceChangeHistGrid.dataProvider);
            priceChangeHistGrid.realGrid.setStyles(priceChangeHistGridBaseInfo.realgrid.styles);
            priceChangeHistGrid.realGrid.setDisplayOptions(priceChangeHistGridBaseInfo.realgrid.displayOptions);
            priceChangeHistGrid.realGrid.setColumns(priceChangeHistGridBaseInfo.realgrid.columns);
            priceChangeHistGrid.realGrid.setOptions(priceChangeHistGridBaseInfo.realgrid.options);
            priceChangeHistGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            priceChangeHistGrid.dataProvider.setFields(priceChangeHistGridBaseInfo.dataProvider.fields);
            priceChangeHistGrid.dataProvider.setOptions(priceChangeHistGridBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            priceChangeHistGrid.dataProvider.clearRows();
            priceChangeHistGrid.dataProvider.setRows(dataList);
        }
    };

    // 변경이력 팝업 관리
    var priceChangeHistMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/item/priceChangeHist/getPriceChangeHist.json?itemNo=${itemNo}&storeType=${storeType}"
                , method: "GET"
                , callbackFunc: function (res) {
                    console.log(res);
                    priceChangeHistGrid.setData(res);
                }
            });
        },
    };
</script>