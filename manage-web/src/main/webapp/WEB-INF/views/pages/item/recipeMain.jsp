<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">레시피 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="recipeSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>레시피 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="350px">
                        <col width="100px">
                        <col width="250px">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recipe.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recipe.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recipe.initSearchDate('-30d');">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 250px;" >
                        </td>
                        <th>노출여부</th>
                        <td>
                            <select id="searchDispYn" name="searchDispYn" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${dipsYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="recipeTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="recipeListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 레시피 정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="recipeSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>레시피정</caption>
                <colgroup>
                    <col width="200px">
                    <col width="250px">
                    <col width="100px">
                    <col width="150px">
                    <col width="100px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>레시피명 <span class="text-red">*</span></th>
                    <td colspan="4" >
                        <input id="title" name="title" type="text" class="ui input medium mg-r-5" style="width: 500px;">
                    </td>
                    <th>레시피 번호</th>
                    <td>
                        <input id="recipeNo" name="recipeNo" type="text" class="ui input medium mg-r-5" style="width: 200px;" readonly>
                    </td>
                </tr>
                <tr>
                    <th>분량 <span class="text-red">*</span></th>
                    <td>
                        <input id="cookingQuantity" name="cookingQuantity" type="text" class="ui input medium mg-r-5" style="width: 150px;">
                    </td>
                    <th>조리시간 <span class="text-red">*</span></th>
                    <td colspan="2">
                        <input id="cookingTime" name="cookingTime" type="text" class="ui input medium mg-r-5" style="width: 150px;" >
                    </td>
                    <th>난이도 <span class="text-red">*</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${grade}" varStatus="status">

                            <label class="ui radio inline" style="margin-right: 5px;"><input type="radio" name="grade" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>

                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>레시피상세이미지 <span class="text-red">*</span><br>(와이드형)</th>
                    <td colspan="2">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="recipe.img.deleteImg('titleImg');">삭제</button>
                                    <div id="titleImg" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl"id="titleImgUrl"  name="titleImgUrl" value="">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="recipe.img.clickFile('titleImg');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                사이즈 : 1200*660<br>
                                용량 : 4 MB 이하<br>
                                파일 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                    <th>상품상세이미지 <span class="text-red">*</span><br>(정방형)</th>
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="recipe.img.deleteImg('contentsImg');">삭제</button>
                                    <div id="contentsImg" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" id="contentsImgUrl" name="contentsImgUrl" value="">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                  <button type="button" class="ui button medium" onclick="recipe.img.clickFile('contentsImg');">등록</button>
                                </div>

                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                사이즈 : 630*630<br>
                                용량 : 4 MB 이하<br>
                                파일 : JPG, JPEG, PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>레시피 TIP <span class="text-red">*</span></th>
                    <td colspan="6">
                        <input type="text" id="cookingTip" name="cookingTip" class="ui input medium mg-r-5" style="width: 500px;" >
                    </td>
                </tr>
                <tr>
                    <th>요리방법 <span class="text-red">*</span></th>
                    <td colspan="6">
                        <span class="ui form inline">
                            <input type="hidden" id="contents" name="contents"/>
                            <div id="contentsDescEdit" name="contentsDescEdit"></div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>주재료</th>
                    <td colspan="6">
                        <input type="text" id="stuffListM" name="stuffList[].stuffDesc" class="ui input medium mg-r-5" style="width: 500px;" >
                        <input type="hidden" name="stuffList[].stuffType" value="M">
                    </td>
                </tr>
                <tr>
                    <th>부재료</th>
                    <td colspan="6">
                        <input type="text" id="stuffListS" name="stuffList[].stuffDesc" class="ui input medium mg-r-5" style="width: 500px;" >
                        <input type="hidden" name="stuffList[].stuffType" value="S">
                    </td>
                </tr>
                <tr>
                    <th>노출여부</th>
                    <td colspan="6">
                        <c:forEach var="codeDto" items="${dipsYn}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline " style="margin-right: 5px;"><input type="radio" name="dispYn" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline" style="margin-right: 5px;"><input type="radio" name="dispYn" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui horizontal mg-t-10"  >
        <div class="com wrap-title sub">
            <h3 class="title">• 상품정보</h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-1 0">

                <table class="ui table">
                    <tbody>
                        <tr>
                            <th style="width: 120px;">
                                상품등록 <span class="text-red">*</span>
                            </th>
                            <td>
                                <span class="ui left-button-group">
                                    <button type="button" key="top" class="ui button small mg-r-5" onclick="recipeItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="bottom" class="ui button small mg-r-5" onclick="recipeItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="recipeItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="recipeItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                                    <button type="button" class="ui button medium mg-r-5" onclick="recipeItem.removeCheckData();">선택삭제</button>
                                </span>
                                <span class="ui right-button-group">
                                      <button class="ui button medium gray-dark mg-r-5" id="addItemOptionPopUp">추가</button>
                                </span>
                                <div id="recipeItemListGrid" style="width: 100%; height: 150px;margin-top: 10px;"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>

    </div>


    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setRecipeBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/recipeMain.js?${fileVersion}"></script>

<script>
    ${recipeListGridBaseInfo};
    ${recipeItemListGridBaseInfo};
    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
