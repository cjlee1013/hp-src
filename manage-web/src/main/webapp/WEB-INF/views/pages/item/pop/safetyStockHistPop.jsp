<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">안전재고 수정이력</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="safetyStockHistGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 수정이력 팝업 그리드 기본정보
    ${safetyStockHistGridBaseInfo};

    $(document).ready(function () {
        safetyStockHistGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 수정이력 팝업 그리드
    var safetyStockHistGrid = {
        realGrid: new RealGridJS.GridView("safetyStockHistGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            safetyStockHistGrid.initGrid();
            safetyStockHistGrid.initDataProvider();
            priceChangeHistMng.search();
        },
        initGrid: function () {
            safetyStockHistGrid.realGrid.setDataSource(safetyStockHistGrid.dataProvider);
            safetyStockHistGrid.realGrid.setStyles(safetyStockHistGridBaseInfo.realgrid.styles);
            safetyStockHistGrid.realGrid.setDisplayOptions(safetyStockHistGridBaseInfo.realgrid.displayOptions);
            safetyStockHistGrid.realGrid.setColumns(safetyStockHistGridBaseInfo.realgrid.columns);
            safetyStockHistGrid.realGrid.setOptions(safetyStockHistGridBaseInfo.realgrid.options);
            safetyStockHistGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            safetyStockHistGrid.dataProvider.setFields(safetyStockHistGridBaseInfo.dataProvider.fields);
            safetyStockHistGrid.dataProvider.setOptions(safetyStockHistGridBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            safetyStockHistGrid.dataProvider.clearRows();
            safetyStockHistGrid.dataProvider.setRows(dataList);
        }
    };

    // 수정이력 팝업 관리
    var priceChangeHistMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/item/safetyStock/getSafetyStockHist.json?itemNo=${itemNo}"
                , method: "GET"
                , callbackFunc: function (res) {
                    safetyStockHistGrid.setData(res);
                }
            });
        },
    };
</script>