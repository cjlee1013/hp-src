<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    /* 캘린더모드 ui-kit.css 맨 아래에 추가할 것 */
    .ui.table.calendar-mode th,td {
        border-left: solid 1px #eee!important;
        border-right: solid 1px #eee!important;
        border-bottom: solid 1px #eee!important;
    }
    .ui.table.calendar-mode td:hover {
        background-color: #f9f9f9;
        cursor: pointer;
    }
    .ui.table.calendar-mode {
        height: 20px;
        position: relative;
    }
    .ui.table.calendar-mode {
        position: relative; top: 0; right: 0; font-size: 11px;
    }
</style>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">싸데이 상품 관리</h2>
    </div>

    <!-- 영업일 캘린더 -->
    <div style="width: 40%; float: left;">
        <div style="text-align: center;">
            <div class="ui form inline" style="display: inline-block;">
                <button id="prevYear" class="ui button small mint mg-r-5">이전 년도</button>
                <button id="prevMonth" class="ui button small mint mg-r-5">이전 달</button>
            </div>
            <span style="position: relative;top: 6px;margin: 2px 20px;font-size: 18px;font-weight: bold;">
              <span id="curYear"></span>년 <span id="curMonth"></span>월
            </span>
            <div class="ui form inline" style="display: inline-block;">
                <button id="nextMonth" class="ui button small mint mg-r-5">다음 달</button>
                <button id="nextYear" class="ui button small mint mg-r-5">다음 년도</button>
            </div>
        </div>

        <div class="com wrap-title">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table calendar-mode" id="onedayItemCalendar">
                    <caption>영업일 관리</caption>
                    <colgroup>
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="text-center">월</th>
                        <th class="text-center">화</th>
                        <th class="text-center">수</th>
                        <th class="text-center">목</th>
                        <th class="text-center">금</th>
                        <th class="text-center"><span class="text-red">토</span></th>
                        <th class="text-center"><span class="text-red">일</span></th>
                    </tr>
                    </thead>
                    <tbody><!-- 캘린더 동적 생성영역... --></tbody>
                </table>
            </div>
        </div>
    </div>

    <div style="width: 100%; float: left; margin-top: 15px;">

        <span class="text pull-left" style="margin-top: 5px;">* 검색결과 : <span id="onedayItemTotalCount">0</span>건</span>

        <span class="ui form inline mg-l-10 pull-right" style="margin-bottom: 5px;">
            <span class="text ">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
            <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
        </span>

        <div class="mg-t-20 mg-b-20" id="onedayItemListGrid" style="width: 100%; height: 350px;"></div>

        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="onedayItemMng.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="onedayItemMng.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="onedayItemMng.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="onedayItemMng.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="onedayItemMng.openItemReadPopup();">일괄등록</button>
            <button type="button" class="ui button medium" id="addItemPopUp">상품조회</button>

            <div id="dispStore" style="float: right;">
                <select style="width: 100px;" id="itemStoreType" name="itemStoreType" class="ui input medium mg-r-10 storeType">
                    <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
                        <c:if test="${codeDto.ref1 eq 'HOME' && codeDto.mcCd eq 'HYPER'}">
                            <option value="${codeDto.mcCd}">${codeDto.mcCd}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>

            <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 100px; float: right;">
                <option value="TD">점포상품</option>
                <option value="DS">셀러상품</option>
            </select>

            <li style="margin-left : 40px; margin-right: 5px; margin-top: 3px; float: right;">* 상품 전시영역</li>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="onedayItemMng.chgDispYn('N');">전시안함</button>
            <button type="button" class="ui button medium" onclick="onedayItemMng.chgDispYn('Y');">전시</button>
            <li style="margin-right: 5px; margin-top: 3px; float: right;">* 전시여부 변경</li>
        </span>
    </div>
</div>

<div class="ui center-button-group mg-t-15" style="margin-top: 50px;">
    <span class="inner">
        <button type="button" class="ui button xlarge mg-r-5 cyan" id="setOnedayItem">저장</button>
        <button type="button" class="ui button xlarge mg-r-5 white" id="resetOnedayItem">초기화</button>
    </span>
</div>

<div id="storeTypeHome" style="display: none">
    <select style="width: 100px;float: left;" id="itemStoreType" name="itemStoreType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME' && codeDto.mcCd eq 'HYPER'}">
                <option value="${codeDto.mcCd}">${codeDto.mcCd}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="storeTypeClub" style="display: none">
    <select style="width: 100px;float: left;" name="itemStoreType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="storeTypeNone" style="display: none">
    <select style="width: 100px;float: left;" id="itemStoreType" name="itemStoreType" class="ui input medium mg-r-10 storeType" readonly>
        <option value="DS">DS</option>
    </select>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/onedayItemMain.js?${fileVersion}"></script>

<script>
    ${onedayItemGridBaseInfo}
</script>