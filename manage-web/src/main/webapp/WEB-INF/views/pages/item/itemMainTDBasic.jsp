<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<input type="hidden" name="mallType" id="mallType" value="${mallType}" />
<input type="hidden" name="storeType" id="storeType" />
<div class="com wrap-title sub">
    <h3 class="title">• 기본정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>기본정보</caption>
        <colgroup>
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>상품번호</th>
            <td>
                <div class="ui form inline mg-b-10">
                    <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" style="width: 150px;" readonly>
                    <input type="hidden" id="itemStatus" name="itemStatus">
                </div>
                <span class="text">오프라인 Parent id : </span><span id="itemParent" class="text"></span>
            </td>
            <th>상품상태</th>
            <td>
                <span class="itemStatusNm" style="vertical-align: sub;">-</span>
                <span class="ui form inline mg-l-10">
                    <button type="button" id="itemHist" class="ui button medium" data-store-type="" >변경이력</button>
                </span>
            </td>
            <th>거래형태</th>
            <td>
                <span id="dealTypeNm" class="text"></span>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">상품명</span></th>
            <td colspan="5">
                <div class="ui form inline mg-b-10">
                    <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" style="width: 650px;" maxlength="50">
                    <span class="text">( <span style="color:red;" id="itemNmCount">0</span> / 50자 )</span>
                </div>
                <span class="text">오프라인 상품명 : </span><span id="lgcItemNm" class="text"></span>
            </td>
        </tr>
        <tr>
            <th>상품구분</th>
            <td>
                <span id="itemDivision" class="text-normal"></span>
            </td>
            <th>상품유형</th>
            <td colspan="3">
                <div class="ui form inline">
                    <select id="itemType" style="width: 180px" name="itemType" class="ui input medium">
                        <c:forEach var="codeDto" items="${itemType}" varStatus="status">
                            <c:if test="${codeDto.ref2 ne 'DS'}">
                                <c:set var="selected" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
                <div class="ui form inline" style="display: none">
                    <select id="shipAttr" name="shipAttr" class="ui input medium mg-r-10" style="width: 180px">
                        <option value="DRCT">직배전용상품</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">카테고리</span></th>
            <td colspan="5">
                <select class="ui input medium mg-r-5" id="cateCd1" name="lcateCd"
                        style="width:150px;float:left;" data-default="대분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(1,'cateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="cateCd2" name="mcateCd"
                        style="width:150px;float:left;" data-default="중분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(2,'cateCd');">
                </select>
                <select class="ui input medium mg-r-5" id="cateCd3" name="scateCd"
                        style="width:150px;float:left;" data-default="소분류"
                        onchange="javascript:commonCategory.changeCategorySelectBox(3,'cateCd');">
                </select>
                <select class="ui input medium mg-b-10" id="cateCd4" name="dcateCd"
                        style="width:150px;" data-default="세분류">
                </select>
                <span class="text">오프라인 카테고리 : </span><span id="lgcCate" class="text"></span>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">판매업체</span></th>
            <td colspan="3">
                    <span class="ui form inline">
                        <input id="partnerId" name="partnerId" type="text" class="ui input medium mg-r-5" style="width: 100px;" maxlength="20" readonly>
                        <input id="businessNm" name="businessNm" type="text" class="ui input medium" style="width: 150px;" maxlength="20" readonly>
                    </span>
            </td>
            <th>노출여부</th>
            <td>
                <div class="ui form inline">
                    <select class="ui input medium mg-r-10" id="dispYn" name="dispYn" style="width: 100px;">
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="selected" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <th>장바구니제한</th>
            <td colspan="5">
                <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="cartLimitYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>팩상품여부</th>
            <td id="packArea" colspan="5">
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub">
    <h3 class="title">• 판매정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal ">
    <table class="ui table">
        <caption>판매정보</caption>
        <colgroup>
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="20%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th><span class="text-red-star">과세여부</span></th>
            <td colspan="5">
                <c:forEach var="codeDto" items="${taxYn}" varStatus="status">
                    <c:if test="${codeDto.ref3 eq 'TD'}">
                        <label class="ui radio inline"><input type="radio" name="taxYn" class="ui input medium" value="${codeDto.mcCd}" /><span>${codeDto.mcNm}</span></label>
                    </c:if>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>대표판매정보</th>
            <td colspan="5">
                <span class="text-gray-light mg-l-10">(아래 정보는 금천점 기준정보로 점포별 상세정보는 상이할 수 있습니다.)
                    <button type="button" class="ui button medium pull-right gray-dark mg-b-10" onclick="parent.initialTab.addTab('menu_1_2_2', '점포상품 상세정보', '/item/itemMain/TD/detail');">점포별 설정</button>
                </span>
                <div class="mg-t-10 mg-b-20" id="saleDFListGrid" style="width: 100%; height: 150px;"></div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• (상품용도)옵션정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <!-- 옵션 타이틀 -->
    <input type="hidden" name="optTitleDepth" id="optDepth" value="1"/>
    <input type="hidden" name="opt1Title" id="opt1Title" value=""/>
    <input type="hidden" name="opt2Title" id="opt2Title" value=""/>

    <table class="ui table">
        <caption>용도옵션</caption>
        <colgroup>
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>용도옵션</th>
            <td id="optSelUseYn">
                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.mcCd eq 'N'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="optSelUseYn" class="ui input medium" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <label class="ui checkbox inline mg-r-5 ">
                    <span class="text">( </span>
                    <input type="checkbox" name="listSplitDispYn" id="listSplitDispYn" value="Y"><span class="text">리스트 분할 노출</span>
                    <span class="text">)</span>
                </label>
            </td>
        </tr>
        <tr id="optSelUseYnArea">
            <th>용도 옵션목록</th>
            <td>
                옵션 수 : <span id="optTotalCount">0</span>개
                <span class="ui form inline">
                    <input type="text" name="opt1TitleTxt" id="opt1TitleTxt" class="ui input mg-r-5 mg-l-20" style="width:200px;" maxlength="10" placeholder="옵션명을 설정해주세요.">
                    <button type="button" id="opt1TitleBtn" class="ui button medium gray-dark">옵션명설정</button>
                </span>
                <div class="mg-t-20 mg-b-20" id="optListGrid" style="width: 100%; height: 350px;"></div>
                <span class="ui left-button-group">
                    <button type="button" key="top" class="ui button small mg-r-5 optMoveCtrl"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                    <button type="button" key="bottom" class="ui button small mg-r-5 optMoveCtrl"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                    <button type="button" key="firstTop" class="ui button small mg-r-5 optMoveCtrl"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                    <button type="button" key="lastBottom" class="ui button small mg-r-5 optMoveCtrl"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                    <button type="button" class="ui button medium mg-r-5" onclick="item.opt.deleteOptSelect();">선택삭제</button>
                </span>
                <table class="ui table-inner mg-t-20">
                    <caption>옵션정보</caption>
                    <colgroup>
                        <col width="16%">
                        <col width="16%">
                        <col width="16%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>옵션번호</th>
                        <td id="optNo">

                        </td>
                        <th>옵션값</th>
                        <td>
                            <span class="ui form inline">
                                <input type="text" name="opt1Val" id="opt1Val" class="ui input mg-r-5" style="width:200px;" maxlength="20" >
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <span class="pull-left mg-t-10">
                    <button type="button" id="optAddBtn" class="ui button medium gray-dark" onclick="item.opt.setOpt();">추가</button>
                    <button type="button" class="ui button medium" onclick="item.opt.copyOpt();">복사</button>
                    <button type="button" class="ui button medium" onclick="item.opt.deleteOpt();">삭제</button>
                    <button type="button" class="ui button medium" onclick="item.opt.getDetail();">초기화</button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 이미지정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>이미지정보</caption>
        <colgroup>
            <col width="120px">
            <col width="40%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>
                <span class="text-red-star">상품이미지</span>
            </th>
            <td colspan="3">
                <div id="imgMNDiv">
                    <c:forEach var="codeDto" items="${itemImgType}" varStatus="status">
                        <c:if test="${codeDto.mcCd eq 'MN'}">

                            <c:forEach var="idx" begin="0" end="${codeDto.ref2-1}" step="1" varStatus="status">
                                <c:set var="mainYn" value="N" />
                                <c:if test="${idx eq '0'}">
                                    <c:set var="mainYn" value="Y" />
                                </c:if>

                                <div style="position:relative; display:inline-block;">
                                    <div class="text-center preview-image" style="height:132px;">
                                        <div class="imgDisplayView" style="display:none;">
                                            <button type="button" id="productImgDelBtn${status.count}" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                            <div id="${codeDto.mcCd}${status.count}" class="uploadType itemImg" inputId="uploadMNIds" data-processkey="ItemMainImage" style="cursor:hand;">
                                                <img class="imgUrlTag" width="132" height="132" src="">
                                                <input type="hidden" name="imgList[].mainYn" value="${mainYn}"/>
                                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value=""/>
                                                <input type="hidden" name="imgList[].priority" value="${status.count}"/>
                                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                                <input type="hidden" class="imgType" name="imgList[].imgType" value="${codeDto.mcCd}"/>
                                            </div>
                                        </div>

                                        <div class="imgDisplayReg" style="padding-top: 50px;">
                                            <button type="button" id="productImgRegBtn${status.count}" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="mainImg${status.count}" >등록</button>
                                        </div>
                                    </div>
                                    <div class="mg-t-5 imgMainYnRadioView" style="text-align: center;">
                                        <c:choose>
                                            <c:when test="${status.count eq 1}">
                                                <div class="text text-red-star">상품이미지_${status.count}</div>
                                                <div class="text">(대표이미지)</div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="text">상품이미지_${status.count}</div>
                                                <div class="text"><br></div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                    <div class="pd-t-20 text-size-md text-gray-Lightest">( 사이즈: 1200 x 1200 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG ) <button type="button" id="mainImgTDTest" class="ui button medium">프론트 대표이미지 확인용</button></div>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">리스팅이미지</span>
            </th>
            <td>
                <div id="imgLSTDiv" class="text-center preview-image" style="width:258px; height:132px;">
                    <div class="imgDisplayView" style="display:none;">
                        <button type="button" id="listImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                        <div id='imgLST' class="uploadType itemImg" data-type="LST" data-processkey="ItemListImage" style="cursor:hand;">
                            <img class="imgUrlTag" width="258px;" height="132px;" src="">
                            <div class="ui form inline" >
                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="" id="listImgUrl"/>
                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value="" id="listImgNm"/>
                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                <input type="hidden" class="imgType" name="imgList[].imgType" value="LST"/>
                            </div>
                        </div>
                    </div>
                    <div class="imgDisplayReg" style="padding-top: 50px;">
                        <button type="button" id="listImgRegBtn" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="listImg">등록</button>
                    </div>
                </div>
                <div class="pd-t-20 text-size-md text-gray-Lightest">( 사이즈: 900 X 470 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG )<br><br> <button type="button" id="listImgTest" class="ui button medium">프론트 리스트이미지 확인용</button></div>
            </td>
            <th>
                <span class="text-red-star">라벨이미지</span>
            </th>
            <td>
                <div id="imgLBLDiv" class="text-center preview-image" style="width:258px; height:132px;">
                    <div class="imgDisplayView" style="display:none;">
                        <button type="button" id="labelImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                        <div id='imgLabel' class="uploadType itemImg" data-type="LBL" data-processkey="ItemLabelImage" style="cursor:hand;">
                            <img class="imgUrlTag" width="258px;" height="132px;" src="">
                            <div class="ui form inline" >
                                <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value="" id="labelImgUrl"/>
                                <input type="hidden" class="imgNo" name="imgList[].imgNo" value=""/>
                                <input type="hidden" class="imgNm" name="imgList[].imgNm" value="" id="labelImgNm"/>
                                <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                <input type="hidden" class="imgType" name="imgList[].imgType" value="LBL"/>
                            </div>
                        </div>
                    </div>
                    <div class="imgDisplayReg" style="padding-top: 50px;">
                        <button type="button" id="labelImgRegBtn" class="ui button medium" onclick="item.img.clickFile($(this));" data-field-name="labelImg">등록</button>
                    </div>
                </div>
                <div class="pd-t-20 text-size-md text-gray-Lightest">( 사이즈: 840 X 세로제한없음 / 용량: 4MB 이하 / 파일 : JPG, JPEG, PNG )</div>
            </td>
        </tr>
        <tr>
            <th>
                <span>동영상</span>
            </th>
            <td colspan="3">
                <button type="button" class="ui button medium sky-blue proofUpload"
                        data-file-type="${codeDto.mcCd}"
                        data-field-name="videoFile"
                        onclick="item.file.clickFile($(this), 'VIDEO');">등록
                </button>
                <span id="uploadVideoFileSpan" data-file-type="VIDEO" class="ui form inline">
                </span>
                <div class="pd-t-20 text-size-md text-gray-Lightest">( 화면비율 : 1:1 / 용량 : 1MB / 파일명 : MP4 )</div>
            </td>
        </tr>
        <tr>
            <th>
                <span class="text-red-star">상품상세설명</span>
            </th>
            <td colspan="3">
                <div id="detailHtml">
                    <!-- 에디터 영역 -->
                    <div id="editor1"></div>
                    <div class="mg-t-5">
                            <span class="pull-left">
                                <button type="button" class="ui button medium" onclick="if (confirm('등록된 상품상세설명을 모두 삭제하시겠습니까?')) {commonEditor.setHtml($('#editor1'), '');}" >초기화</button>
                            </span>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 속성정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>속성정보</caption>
        <colgroup>
            <col width="120px">
            <col width="40%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>브랜드</th>
            <td>
                <div class="ui form inline mg-t-10">
                    <input type="text" name="brandNm" id="brandNm" style="margin-right: -1px; width:200px;" class="ui input" autocomplete="off" placeholder="브랜드명을 입력하세요." />
                    <ul id="searchBrandLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 222px; display: none;"></ul>
                    <button type="button" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getBrandBtn">
                        <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                    </button>
                    <span id="brandArea">
                            <input type="hidden" name="brandNo" id="brandNo" value="" />
                            <span id="spanBrandNm" class="text text-blue"></span>
                            <button type="button" id="deleteBrandBtn" class="ui button small delete-thumb mg-l-5" onclick="item.attr.setBrandYn('N')">x</button>
                        </span>
                </div>
            </td>
            <th>제조사</th>
            <td>
                <div class="ui form inline mg-t-10">
                    <input type="text" name="makerNm" id="makerNm" style="margin-right: -1px; width:200px;" class="ui input readonly makerInfo" placeholder="제조사명을 입력하세요."/>
                    <ul id="searchMakerLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 222px; display: none;"></ul>
                    <button type="button" class="makerInfo" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getMakerBtn">
                        <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                    </button>
                    <span id="makerArea">
                        <input type="hidden" name="makerNo" id="makerNo" value="" />
                        <span id="spanMakerNm" class="text text-blue"></span>
                        <button type="button" id="deleteMakerBtn" class="ui button small delete-thumb mg-l-5" onclick="item.attr.setMakerYn('N')">x</button>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <th>상품속성</th>
            <td colspan="3">
                <table class="ui table-inner" style="display: none;">
                    <caption>상품속성</caption>
                    <colgroup>
                        <col width="13%">
                        <col width="31.4%">
                        <col width="13%">
                        <col width="*">
                    </colgroup>
                    <tbody id="attrArea">
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th>노출 단위가격</th>
            <td colspan="3">
                <div class="ui form inline mg-t-5">
                    <label class="ui checkbox inline mg-r-5 ">
                        <input type="checkbox" name="unitDispYn" id="unitDispYn" value="Y"><span class="text">프론트 노출여부</span>
                    </label>
                    <input type="text" name="unitQty" id="unitQty" class="ui input mg-r-5" style="width:70px;" readonly/>
                    <input type="text" name="unitMeasure" id="unitMeasure" class="ui input mg-r-5" style="width:50px;" readonly/>
                    <span class="text">당 | 총구성값</span>
                    <input type="text" name="totalUnitQty" id="totalUnitQty" class="ui input mg-r-5" style="width:90px;" readonly/>
                </div>
            </td>
        </tr>
        <tr>
            <th><span class="text-red-star">원산지</span></th>
            <td colspan="3">
                <div class="ui form inline">
                    <input type="text" name="originTxt" id="originTxt" class="ui input" maxlength="50" style="width:550px;"/>
                </div>
            </td>
        </tr>
        <tr>
            <th>제조일자</th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="makeDt" id="makeDt" class="ui input mg-r-5" style="width:140px;"/>
                </div>
            </td>
            <th>유효일자</th>
            <td>
                <div class="ui form inline">
                    <input type="text" name="expDt" id="expDt" class="ui input mg-r-5" style="width:140px;"/>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                상품고시정보
            </th>
            <td colspan="3">
                <div id="noticeHtml">

                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="com wrap-title sub mg-t-30">
    <h3 class="title">• 부가정보</h3>
</div>
<div class="topline"></div>
<div class="ui wrap-table horizontal">
    <table class="ui table">
        <caption>부가정보</caption>
        <colgroup>
            <col width="120px">
            <col width="45%">
            <col width="120px">
            <col width="*">
        </colgroup>
        <tbody>
        <tr>
            <th>성인상품유형</th>
            <td>
                <c:forEach var="codeDto" items="${adultType}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 eq 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline" style="margin-right: 12px "><input type="radio" name="adultType" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
                <span class="ui form inline">
                    <select class="ui input medium mg-r-5" id="imgDispYn" name="imgDispYn" style="width: 150px;" disabled>
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="selected" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected" />
                            </c:if>
                            <option value="${codeDto.mcCd}" ${selected}>이미지 ${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                </span>
            </td>
            <th>선물하기</th>
            <td>
                <c:forEach var="codeDto" items="${setYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 ne 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="giftYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>가격비교 사이트</th>
            <td colspan="3">
                <c:forEach var="codeDto" items="${regYn}" varStatus="status">
                    <c:set var="checked" value="" />
                    <c:if test="${codeDto.ref1 ne 'df'}">
                        <c:set var="checked" value="checked" />
                    </c:if>
                    <label class="ui radio inline"><input type="radio" name="epYn" class="ui input mg-t-5" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th>외부연동 설정</th>
            <td colspan="3">
                <div>
                    <span class="text" style="width: 40px;display: inline-block;">N마트</span>
                    <span id="naverSetTxt" class="text"></span>
                    <span id="naverDtTxt" class="text"></span>
                </div>
                <div class="mg-t-5">
                    <span class="text" style="width: 40px;display: inline-block;">11번가</span>
                    <span id="elevenSetTxt" class="text"></span>
                    <span id="elevenDtTxt" class="text"></span>
                </div>
                <div class="pd-t-10 text-size-md">외부연동 설정은 외부연동 설정관리 메뉴에서 설정해주세요.</div>
            </td>
        </tr>
        <tr>
            <th>검색키워드</th>
            <td colspan="3">
                <div class="ui form inline">
                    <input type="text" name="srchKeyword" id="srchKeyword" class="ui input" placeholder="예) 식품,라면,뽀글이,매운맛" maxlength="100" style="width:60%"/>
                    <div class="text mg-t-5">(키워드는 (,) 쉽표로 구분하여 입력해주세요. 최대 20개까지만 입력 가능합니다.)</div>
                </div>
            </td>
        </tr>
        <tr>
            <th>검색태그</th>
            <td colspan="3">
                <div id="schTagArea" class="ui wrap-table vertical mg-b-10">
                    <table class="ui table">
                        <caption>검색태그</caption>
                        <colgroup>
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="12.5%">
                            <col width="*">
                        </colgroup>
                        <tbody id="schTagTbodyArea">
                        </tbody>
                    </table>
                </div>
                <span class="text">선택한 태그 : </span><span id="selectedTags"></span>
            </td>
        </tr>
        <tr>
            <th>안전인증</th>
            <td colspan="3">
                <div id="safetyArea" class="ui wrap-table mg-b-10">
                    <table class="ui table">
                        <caption>안전인증</caption>
                        <colgroup>
                            <col width="13%">
                            <col width="*">
                        </colgroup>
                        <tbody id="safetyTbodyArea">
                            <tr>
                                <th>어린이제품인증</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>제품안전인증</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>전파인증</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>기타인증</th>
                                <td>-</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div id="setStatusArea" class="ui right-button-group mg-t-15" style="display: none">
    <span class="text-size-lg" >• 판매상태 변경 : </span>
    <span class="inner">
        <button type="button" id="setStatusS" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('S', true)">영구중지</button>
        <button type="button" id="setStatusP" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('P', true)">일시중지</button>
        <button type="button" id="setStatusA" class="ui button medium white font-malgun" onclick="itemMain.getStopReason('A', true)">판매가능</button>
    </span>
</div>
<div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn" onclick="item.setItemTD();">저장</button>
        <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        <button type="button" class="ui button xlarge white font-malgun">미리보기</button>
    </span>
</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="mainImg1" data-file-id="MN1"  style="display: none;"/>
<input type="file" name="mainImg2" data-file-id="MN2"  style="display: none;"/>
<input type="file" name="mainImg3" data-file-id="MN3"  style="display: none;"/>
<input type="file" name="mainImg4" data-file-id="MN4"  style="display: none;"/>
<input type="file" name="mainImg5" data-file-id="MN5"  style="display: none;"/>
<input type="file" name="mainImg6" data-file-id="MN6"  style="display: none;"/>
<input type="file" name="listImg" data-file-id="imgLST"  style="display: none;"/>
<input type="file" name="labelImg" data-file-id="imgLabel"  style="display: none;"/>
<input type="file" name="proof1" data-file-id="uploadProofFileSpan_EXM"  style="display: none;"/>
<input type="file" name="proof2" data-file-id="uploadProofFileSpan_MAK"  style="display: none;"/>
<input type="file" name="proof3" data-file-id="uploadProofFileSpan_AWD"  style="display: none;"/>
<input type="file" name="proof4" data-file-id="uploadProofFileSpan_ORG"  style="display: none;"/>
<input type="file" name="videoFile" data-file-id="uploadVideoFileSpan"  style="display: none;"/>
<!-- 이미지정보 업로드 폼 -->
<script>

    const menuType = '${menuType}';
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';

    let itemCertType = ${itemCertTypeObj};
    let itemCertGroup = ${itemCertGroupObj};

    let storeIdPrView = '${storeIdDefault}';
    let storeId     = '${storeIdDefault}';

    // 옵션 그리드
    ${optListGridBaseInfo}
    // 대표판매정보 그리드
    ${saleDFListGridBaseInfo}

    $(document).ready(function() {
        itemListGrid.init();
        optListGrid.init();
        saleDFListGrid.init();
        item.init();
    });
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/item/itemMainGrid.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainGridTDBasic.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainTDBasic.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemMainCommon.js?${fileVersion}"></script>