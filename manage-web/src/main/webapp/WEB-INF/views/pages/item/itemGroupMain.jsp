<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">묶음형 상품관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="itemGroupSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>묶음형 상품관리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="150px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemGroup.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemGroup.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemGroup.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="itemGroup.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="groupNo" selected>묶음번호</option>
                                    <option value="groupItemNm">묶음상품명</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="regNm">등록자</option>
                                    <option value="chgNm">수정자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 200px;" minlength="2" >
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="width: 80px">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${useYn}" varStatus="status">
                                        <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="itemGroupTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="itemGroupListGrid" style="width: 100%; height: 300px;"></div>
    </div>
    <form id="itemGroupSetForm" name="itemGroupSetForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>

        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="160px">
                    <col width="150px">
                    <col width="150px">
                    <col width="200px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>묶음번호</th>
                    <td >
                        <input type="hidden" id="groupNo" name="groupNo">
                        <div class="ui form inline">
                            <span class="text" id="groupNoText"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>묶음상품명<span class="text-red"> *</span></th>
                    <td colspan="5">
                        <span class="ui form inline">
                            <input id="groupItemNm" name="groupItemNm" type="text" class="ui input medium mg-r-5" style="width: 450px;" maxlength="50">
                            <span class="text">( <span style="color:red;" id="itemNmCount">0</span> / 50자 )</span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>옵션단계</th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input" name="depth" id="depth" data-default="단계선택" style="width:80px;">
                                <c:forEach var="item"  begin="1" end="2" step="1" varStatus="status">
                                    <option value="${status.count}" <c:if test="${status.count eq 1}"> selected </c:if> >${status.count}단계</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>1단계 타이틀</th>
                    <td>
                        <span class="ui form inline">
                            <input type="text" name="opt1Title" id="opt1Title" class="ui input optTxt mg-r-5" style="width:280px;" maxlength="10">
                        </span>
                    </td>
                    <th>2단계 타이틀</th>
                    <td>
                        <span class="ui form inline">
                            <input type="text" name="opt2Title" id="opt2Title" class="ui input optTxt mg-r-5" style="width:280px;" maxlength="10" disabled/>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td colspan="5">
                        <select id="useYn" name="useYn" class="ui input medium mg-r-5" style="width: 150px" >
                            <c:forEach var="code" items="${useYn}" varStatus="status">
                                <c:set var="selected" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 묶음상품정보 </h3>
            <span class="text">
                <br><br>※ 상품용도옵션이 설정되지 않은 일반상품만 묶음 가능합니다.
                <br>하나의 상품이 여러 묶음에 속할 수 없습니다. 다른 묶음으로 이동하고자 할 경우 먼저 속한 묶음을 해제하시기 바랍니다.
            </span>
        </div>
        <div class="com wrap-title sub">
            상품수  : <span id="itemGroupDetailTotalCount">0</span>개
        </div>
        <div class="topline"></div>
        <div class="mg-b-10" id="itemGroupDetailListGrid" style="width: 100%; height: 300px;"></div>

        <span class="ui left-button-group">
            <button type="button" class="ui button medium mg-r-5 mg-b-10" onclick="itemGroup.deleteItemGroupSelect();">선택삭제</button>
        </span>

        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>묶음상품정보</caption>
                <colgroup>
                    <col width="160px">
                    <col width="300px">
                    <col width="160px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>적용상품<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="itemNo" id="itemNo" value="" class="ui input medium mg-r-5" style="width: 120px;" >
                            <input type="text" name="itemNm" id="itemNm" value="" class="ui input medium mg-r-5" style="width: 250px;" readonly>
                            <button type="button" class="ui button small gray-dark font-malgun" id="schItemPopBtn">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>1단계 옵션값<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="opt1Val" id="opt1Val" class="ui input medium mg-r-5" style="width:280px;">
                        </div>
                    </td>
                    <th>2단계 옵션값</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="opt2Val" id="opt2Val" class="ui input medium mg-r-5" style="width:280px;" disabled>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="ui form inline" style="margin-top: 10px;float: right;">
            <button type="button" id="addDetailBtn" class="ui button medium mg-r-5 gray-dark pull-left">추가</button>
            <button type="button" id="updateDetailBtn" class="ui button medium mg-r-5 gray-dark pull-left" style="display: none;" >저장</button>
            <button type="button" id="delDetailBtn" class="ui button medium mg-r-5 gray pull-left">삭제</button>
            <button type="button" id="resetDetailBtn" class="ui button medium mg-r-5 gray pull-left">초기화</button>
        </div>
        <div class="ui center-button-group mg-t-15">
             <span class="inner">
                 <button type="button" class="ui button xlarge cyan font-malgun" id="setItemGroupBtn">저장</button>
                 <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
             </span>
        </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/itemGroupMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonItem.js?${fileVersion}"></script>

<script>
    // 묶음형 상품관리 그리드
    ${itemGroupListGridBaseInfo}
    ${itemGroupDetailListGridBaseInfo}
</script>
