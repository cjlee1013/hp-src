<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="margin:10px;">
    <div style="font-size: 20px" >
        <h2 class="title font-malgun">마켓연동 실패사유별 도움말</h2>
    </div>
    - 실패사유에 코드가 포함된 경우(아래표 참고)는 당사 상품정보와 판매상태에 따른 이슈입니다. 상품 재연동을 위해 상품정보 확인이 필요합니다. <br />
    - 실패코드가 없는 경우는 연동하고자 하는 마켓 정책에 의해 실패된 경우입니다. 각 마켓쪽 정책 확인이 필요합니다.
    <div class="ui wrap-table mg-t-10">
        <table class="ui table">
            <caption>카테고리 기준수수료 등록/수정</caption>
            <colgroup>
                <col width="33%">
                <col width="33%">
                <col width="33%">
            </colgroup>
            <tbody>
            <tr>
                <th>실패사유</th>
                <th>실패사유 설명</th>
                <th>재연동 필요 시 확인사항</th>
            </tr>
            <tr>
                <td><div class="text-left">1002 : 마스터 상품정보 조회 실패</div></td>
                <td><div class="text-left">PFR 미통과, 온라인 취급안함 등 마켓연동 대상 상품이 아닌 경우</div></td>
                <td><div class="text-left">대표점의 PFR 통과 처리 및 온라인 취급상태 확인</div></td>
            </tr>
            <tr>
                <td><div class="text-left">1003 : 취급중지, 상품판매중지</div></td>
                <td><div class="text-left">판매중지 상품(취급중지, 상품상태/유형변경 등)</div></td>
                <td>
                    <div class="text-left">
                        마켓연동 가능상태 확인 <br />
                        - 상품유형 : 새상품인 경우 연동가능 <br />
                        - 상품상태 : 판매가능 상태인 경우 연동가능 <br />
                        - 대표점의 온라인취급여부=취급인 경우 연동가능
                    </div>
                </td>
            </tr>
            <tr>
                <td><div class="text-left">1004 : 카테고리 미맵핑상태</div></td>
                <td><div class="text-left">마켓연동 카테고리가 맵핑되지 않은 상태</div></td>
                <td>
                    <div class="text-left">
                        카테고리 맵핑 필요 <br />
                        (관련 메뉴 : 어드민>업체관리>마켓연동관리>마켓연동카테고리 매핑)
                    </div>
                </td>
            </tr>
            <tr>
                <td><div class="text-left">1005 : 가상점 단독판매상품 판매중지처리</div></td>
                <td><div class="text-left">가상점 단독판매 등록된 상품으로 마켓연동 판매중지 상품</div></td>
                <td><div class="text-left"></div></td>
            </tr>
            <tr>
                <td><div class="text-left">1006 : 예약상품 판매중지처리</div></td>
                <td><div class="text-left">예약상품 등록된 상품으로 마켓연동 판매중지 상품</div></td>
                <td><div class="text-left"></div></td>
            </tr>
            <tr>
                <td><div class="text-left">1007 : 이미지 필수값 확인</div></td>
                <td><div class="text-left">상품 이미지 누락으로 마켓연동 실패</div></td>
                <td><div class="text-left">상품이미지 등록 후 재연동</div></td>
            </tr>
            <tr>
                <td><div class="text-left">1008 : 성인상품 판매중지처리</div></td>
                <td><div class="text-left">성인상품 유형으로 등록된 상품으로 마켓연동 판매중지 상품</div></td>
                <td><div class="text-left"></div></td>
            </tr>
            <tr>
                <td><div class="text-left">1009 : 외부연동 연동설정 중지 상품</div></td>
                <td><div class="text-left">마켓연동 등록안함 상태</div></td>
                <td>
                    <div class="text-left">
                        연동 필요 시 마켓연동 등록으로 변경 <br />
                        (관련 메뉴 : 어드민>상품관리>마켓연동 상품관리>외부연동 설정관리)
                    </div>
                </td>
            </tr>
            <tr>
                <td><div class="text-left">2002 : 지점별 가격 정보 조회 실패</div></td>
                <td><div class="text-left">상품의 가격정보 부재</div></td>
                <td><div class="text-left"></div></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">확인</button>
        </span>
    </div>
</div>
