<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">새벽배송 상품 카테고리 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="60%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lCateCd"
                                    style="width:150px;float:left;" data-default="1depth">
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left"
                                        id="searchBtn"><i class="fa fa-search"></i> 검색
                                </button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left"
                                        id="searchResetBtn">초기화
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="categoryGridCnt">0</span>건</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20 " id="auroraCategoryGrid" style="width: 100%; height: 350px;"></div>

    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 카테고리 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="auroraCategoryForm" onsubmit="return false;">
            <input type="hidden" id="parentCateCd" name="parentCateCd">
        <table class="ui table">
            <caption>카테고리 등록/수정</caption>
            <colgroup>
                <col width="120px">
                <col width="23%">
                <col width="120px">
                <col width="23%">
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>카테고리 ID</th>
                <td>
                    <div class="ui form inline">
                        <input id="cateCd" name="cateCd" type="text" class="ui input medium mg-r-5" readonly>
                    </div>
                </td>
                <th>카테고리명</th>
                <td>
                    <div class="ui form inline">
                        <input id="cateNm" name="cateNm" type="text" class="ui input medium mg-r-5" maxlength="16">
                    </div>
                </td>
                <th>우선순위</th>
                <td>
                    <div class="ui form inline">
                        <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" maxlength="4">
                    </div>
                </td>
            </tr>
            <tr>
                <th>단계 (Depth)</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="depth" name="depth">
                            <option selected="" value="">선택</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </td>
                <th>부모카테고리</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-5" id="formCateCd1" name="lCateCd" style="width:150px;float:left;" data-default="1depth"></select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>노출범위</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="dispYn" name="dispYn">
                            <c:forEach var="codeDto" items="${categoryDispYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
                <th>사용여부</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="useYn" name="useYn">
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge dark-blue font-malgun" id="setCategoryBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/auroraCategoryMain.js?${fileVersion}"></script>

<script>
    ${auroraCategoryGridBaseInfo}
</script>