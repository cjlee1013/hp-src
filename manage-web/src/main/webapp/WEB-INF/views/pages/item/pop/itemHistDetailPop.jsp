<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
	.popup-request {margin: 20px;}
	.tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
	.tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
	.tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
	.tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
	.wrap-table td {background-color: white; word-break:break-all;}
	.vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
	<div class="com popup-wrap-title">
		<h3 class="title pull-left">
			<i class="fa fa-clone mg-r-5"></i> 상품 변경이력
		</h3>
	</div>
	<div class="tabui-wrap pd-t-20">
		<div class="tabui medium col-02">
			<div id="itemHistGrid" style="width: 100%; height: 350px;"></div>
			<button id="moreHist" class="ui button large mg-t-10" style="width: 100%;" >더보기</button>

		</div>
	</div>
	<div id="histDetail" style="display: none;">
		<div id="histDetailTable" class="ui wrap-table ">
			<table class="ui table">
				<colgroup><col width="100%"></colgroup>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>


<script>
	var RESDATA = {};

	${itemHistGridBaseInfo}

	var itemNo			= '${itemNo}';
	var storeType		= '${storeType}';

	var detailTable = {
		basic	: { // 기본정보
			'html'		: '', // Html 여부
			'mapKey'		: ['false'],
			'value'			: [
				{
					'판매기간-시작시간'			: 'saleStartDt',
					'판매기간-종료시간'			: 'saleEndDt',
					'예약판매부가설정'			: 'rsvPurchaseLimitYn',
					'온라인재고설정'			: 'onlineStockYn',
					'온라인재고설정기간-시작시간'	: 'onlineStockStartDt',
					'온라인재고설정기간-종료시간'	: 'onlineStockEndDt',
					'판매단위수량'				: 'saleUnit',
					'최소구매수량'				: 'purchaseMinQty',
					'구매수량제한여부'			: 'purchaseLimitYn',
					'구매수량제한-일'			: 'purchaseLimitDay',
					'구매수량제한-개수'			: 'purchaseLimitQty'
				}
			]
		}
	};


    // 팝업 html
    var itemHistPop = {
        popContsHtml	: '',
		lastSeq			: '',
		pageNo 			: 0,
        init : function() {
            this.event();
			itemHistPop.getHist();
        },
		initHtml : function() {
			for (var key in detailTable) {
				detailTable[key].html = '';
			}
		},
		setTab : function(_this) {
			$('.tab_vicinity li').removeClass('on');
			if (!_this) {
				$('.tab_vicinity li').eq(0).addClass("on");
			} else {
				_this.addClass("on");
				itemHistPop.setDetailTable(_this.data('type'));
			}
		},
		event : function() {
			$('.closeBtn').on('click', function() {
				itemHistPop.close();
			});

			$('.tab_vicinity li').on('click', function () {
				itemHistPop.setTab($(this));
			});

			$('#moreHist').on('click', function() {
				itemHistPop.getHist(true);
			});
		},
		getHist : function(isMore) {
			CommonAjax.basic({
				url : "/item/getItemHistDetailGroup.json?itemNo="+itemNo+"&pageNo="+itemHistPop.pageNo+"&storeType="+storeType,
				method : 'get',
				callbackFunc : function(res) {
					if (res.length > 0) {
						if (isMore) {
							itemHistGrid.dataProvider.addRows(res);
						} else {
							itemHistGrid.setData(res);
						}

						if (res.length > 10) {
							itemHistPop.pageNo++;
							itemHistPop.deleteLastGrid();
						} else {
							$('#moreHist').hide();
							let rowDataJson = itemHistGrid.dataProvider.getJsonRow(itemHistGrid.gridView.getItemCount() - 1);
							itemHistPop.lastSeq	= rowDataJson.histSeq;
						}
					}
				}
			});
		},
		deleteLastGrid : function() {
        	let lastIdx = itemHistGrid.gridView.getItemCount() - 1;
			let rowDataJson = itemHistGrid.dataProvider.getJsonRow(lastIdx);

			itemHistPop.lastSeq	= rowDataJson.histSeq;
			itemHistGrid.dataProvider.removeRow(lastIdx);
		},
        setDetail : function(rowId) {

			let rowDataJson = itemHistGrid.dataProvider.getJsonRow(rowId);

			var lastRowId = itemHistGrid.gridView.getItemCount() - 1;
			var prevHistSeq = 0;

			if (rowId == lastRowId) {
			} else {
				// 이전 인덱스 추출
				let rowDataJsonPrev = itemHistGrid.dataProvider.getJsonRow(rowId+1);
			}

			RESDATA = rowDataJson;
			itemHistPop.setDetailTable('basic', rowDataJson);
			$('#histDetail').fadeIn(500);
        },
		 setDetailTable : function(type, data) {
			var retHtml = '';
			var vColgroup = '<col width="10%"><col width="90%">';
			var colgroup = detailTable[type].hasOwnProperty('colgroup') ? detailTable[type]['colgroup'] : '<col width="10%"><col width="90%">';

			if (!data) {
				data = RESDATA;
			} else {
				itemHistPop.initHtml();
			}

			if (detailTable[type]['html'] == '') {

				var headerHtml = [];
				var tbodyHtml = '';

				var lastHtml = '</tbody></table></td></tr>';

				// 헤더생성
				for (var i in detailTable[type]['value']) {
					headerHtml[i] = '<tr>';
					for (var key in detailTable[type]['value'][i]) {
						headerHtml[i] += '<th>' + key + '</th>';
					}
					headerHtml[i]+= '</tr>';
				}

				// 바디생성
				for (var i in detailTable[type]['mapKey']) {
					if (detailTable[type]['mapKey'][i]=='false') {
						// 세로 테이블
						retHtml += '<tr><td><table class="vertical-table"><tbody>' + vColgroup;
						for (var key in detailTable[type]['value'][i]) {
							// 배열
							if (typeof data[detailTable[type]['value'][i][key]] == 'object') {
								var thArr = key.split('|');

								retHtml += '<tr><th>' + thArr[0] + '</th><td>';

								for (var objKey in data[detailTable[type]['value'][i][key]]) {
									if (data[detailTable[type]['value'][i][key]][objKey][thArr[1]] == '-') {
										continue;
									} else {
										retHtml += data[detailTable[type]['value'][i][key]][objKey][thArr[1]] + ', ';
									}
								}

								retHtml += '</td></tr>';
							} else {
								// 스트링
								retHtml += '<tr><th>' + key + '</th><td>' + data[detailTable[type]['value'][i][key]] + '</td></tr>';
							}
						}
					} else {
						// 가로 테이블
						if (data[detailTable[type]['mapKey'][i]] == null || Object.keys(data[detailTable[type]['mapKey'][i]]).length == 0) {
							continue;
						}

						if (headerHtml[i].length)
							retHtml += ('<tr><td><table class="horizon-table"><tbody>' + colgroup + headerHtml[i]);
						for (var key in data[detailTable[type]['mapKey'][i]]) {
							tbodyHtml = '<tr>';
							for (var optKey in detailTable[type]['value'][i]) {
								tbodyHtml += '<td>' + data[detailTable[type]['mapKey'][i]][key][detailTable[type]['value'][i][optKey]] + '</td>';
							}
							tbodyHtml += '</tr>';
							retHtml += tbodyHtml;
						}
					}

					retHtml += lastHtml;
				}

				detailTable[type]['html'] = retHtml;
			} else {
				retHtml = detailTable[type]['html'];
			}

			$('#histDetailTable tbody').html(retHtml);
		},
		close : function() {
			window.close();
		}
    };

    $(document).ready(function() {
		CommonAjaxBlockUI.global();
		itemHistPop.init();
		itemHistGrid.init();
    });

	/**
	 * 상품관리 > 셀러상품관리/점포상품관리 > 변경이력 > 공통그리드 (TD/DS)
	 */
	let itemHistGrid = {
		gridView : new RealGridJS.GridView("itemHistGrid"),
		dataProvider : new RealGridJS.LocalDataProvider(),
		init : function() {
			itemHistGrid.initGrid();
			itemHistGrid.initDataProvider();
			itemHistGrid.event();
		},
		initGrid : function() {
			itemHistGrid.gridView.setDataSource(itemHistGrid.dataProvider);

			itemHistGrid.gridView.setStyles(itemHistGridBaseInfo.realgrid.styles);
			itemHistGrid.gridView.setDisplayOptions(itemHistGridBaseInfo.realgrid.displayOptions);
			itemHistGrid.gridView.setColumns(itemHistGridBaseInfo.realgrid.columns);
			itemHistGrid.gridView.setOptions(itemHistGridBaseInfo.realgrid.options);
		},
		initDataProvider : function() {
			itemHistGrid.dataProvider.setFields(itemHistGridBaseInfo.dataProvider.fields);
			itemHistGrid.dataProvider.setOptions(itemHistGridBaseInfo.dataProvider.options);
		},
		event : function() {
			// 그리드 선택
			itemHistGrid.gridView.onDataCellClicked = function(gridView, index) {
				$('#histDetail').hide();
				itemHistPop.setDetail(index.dataRow);
			};
		},
		setData : function(dataList) {
			itemHistGrid.dataProvider.clearRows();
			itemHistGrid.dataProvider.setRows(dataList);
		}
	};

</script>