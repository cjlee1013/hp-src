<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">위해상품 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="harmfulItemSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>위해상품 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td >
                            <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                <c:forEach var="codeDto" items="${searchPeriodType}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td colspan="4">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="harmfulItem.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="harmfulItem.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="harmfulItem.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="harmfulItem.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>처리상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchItemStatus" style="width: 180px" name="searchItemStatus" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchItemStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchItemType" style="width: 180px" name="searchItemType" class="ui input medium" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchItemType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>검사기관</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchInstitute" style="width: 180px" name="searchInstitute" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchInstitute}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td colspan="4">
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 600px;" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="harmfulItemTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="harmfulItemListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 상세정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="harmfulItemSetForm" onsubmit="return false;">
            <input type="hidden" id="setNoDocument" name ="noDocument"/>
            <input type="hidden" id="setSeq" name ="seq"/>
            <input type="hidden" id="setCdInsptmachi" name ="cdInsptmachi"/>
        </form>
            <table class="ui table" id="harmfulItemDetail">
                <caption>위해상품상세</caption>
                <colgroup>
                    <col width="16%">
                    <col width="16%">
                    <col width="16%">
                    <col width="16%">
                    <col width="16%">
                    <col width="16%">
                </colgroup>
                <tbody>
                <tr>
                    <th>문서번호</th>
                    <td colspan="3" id="noDocument"  name ="itemDesc"></td>
                    <th>차수</th>
                    <td id="seq" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>제품명</th>
                    <td colspan="3" id="nmProd" name ="itemDesc"></td>
                    <th>바코드</th>
                    <td id="barcode" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>제품유형명</th>
                    <td colspan="3" id="nmProdtype" name ="itemDesc"></td>
                    <th>포장단위</th>
                    <td id="unitPack" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>제조일자</th>
                    <td id="dtMake" name ="itemDesc"></td>
                    <th>유통기한</th>
                    <td id="prdValid" name ="itemDesc"></td>
                    <th>원산지</th>
                    <td id="nmManufaccntr" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>제조업소</th>
                    <td id="nmManufacupso" name ="itemDesc"></td>
                    <th>제조업소 연락처</th>
                    <td id="telManufacupso" name ="itemDesc"></td>
                    <th>제조업소 주소</th>
                    <td id="addrManufac" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>판매업소</th>
                    <td id="nmSalerupso" name ="itemDesc"></td>
                    <th>판매업소 연락처</th>
                    <td id="telSalerupso" name ="itemDesc"></td>
                    <th>판매업소 주소</th>
                    <td id="addrSaler" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>유형</th>
                    <td id="nmRpttype" name ="itemDesc"></td>
                    <th>검사기관</th>
                    <td id="instituteTxt" name ="itemDesc"></td>
                    <th>검사구분</th>
                    <td id="nmInspttype" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>수거기관</th>
                    <td id="nmTakemachi" name ="itemDesc"></td>
                    <th>수거장소</th>
                    <td id="plcTake" name ="itemDesc"></td>
                    <th>수거일자</th>
                    <td id="dtTake" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>발신기관명</th>
                    <td id="nmRptrmachi" name ="itemDesc"></td>
                    <th>발신기관 담당자</th>
                    <td id="nmReporter" name ="itemDesc"></td>
                    <th>담당자 연락처</th>
                    <td id="telReporter" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>발신일자</th>
                    <td id="dtReport" name ="itemDesc"></td>
                    <th>수신일시</th>
                    <td id="recvDt" name ="itemDesc"></td>
                    <th>송신일시</th>
                    <td id="sendDt" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>처리상태</th>
                    <td id="statusResult" name ="itemDesc"></td>
                    <th>전송상태</th>
                    <td colspan="3" id="sendStatus" name ="itemDesc"></td>
                </tr>
                <tr>
                    <th>출처</th>
                    <td colspan="5">
                        대한상공회의소 유통물류진흥원 위해상품차단시스템  (<a href="http://upss.gs1kr.org" target="_blank">http://upss.gs1kr.org</a>)<br>
                        <button type="button" id="searchUpssPop" class="ui button large white mg-t-5">위해상품차단시스템 위해상품검사결과 보기</button>
                    </td>
                </tr>
                </tbody>
            </table>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 처리상태변경</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">

        <table class="ui table">
            <caption>위해상품상세</caption>
            <colgroup>
                <col width="16%">
                <col width="80%">
            </colgroup>
            <tbody>
            <tr>
                <th>처리상태</th>
                <td>
                    <c:forEach var="codeDto" items="${searchItemStatus}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline" style="margin-right: 10px">
                            <input type="radio" name="statusResult" class="ui input medium" value="${codeDto.mcCd}"  ${checked} ><span>${codeDto.mcNm}</span>
                        </label>
                    </c:forEach>
                </td>
            </tr>
            </tbody>
        </table>
    </div>



    <div class="ui horizontal mg-t-10"  >
        <div class="com wrap-title sub">
            <h3 class="title">• DS 위해상품 : <span id="">0</span>건</h3>
            <span class="inner">
                 <button type="button" id="deleteItem" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">삭제</button>
                <button type="button" id="addItem" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">추가</button>
             </span>

        </div>
        <div id="harmfulDSItemListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setHarmfulItemBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/item/harmfulItemMain.js?${fileVersion}"></script>

<script>
    // 위해상품 리스트 리얼그리드 기본 정보
    ${harmfulItemListGridBaseInfo}
    // DS 위해상품 리스트 리얼그리드 기본 정보
    ${harmfulDSItemListGridBaseInfo}
</script>
