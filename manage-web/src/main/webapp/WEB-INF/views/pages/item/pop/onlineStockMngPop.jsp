<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
	.popup-request {margin: 20px;}
	.tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
	.tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
	.tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
	.tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
	.wrap-table td {background-color: white; word-break:break-all;}
	.vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
	<div class="com popup-wrap-title">
		<h3 class="title pull-left">
			<i class="fa fa-clone mg-r-5"></i> 온라인재고 기준설정
		</h3>
	</div>
	<div class="tabui-wrap pd-t-20">
		<div class="com wrap-title sub">
			<h3 class="title pull-left">• 점포 수 : <span id="storeCount">0</span> 개</h3>
		</div>
		<div class="tabui medium col-02">
			<div id="onlineStockGrid" style="width: 100%; height: 600px;"></div>
		</div>
		<div class="com wrap-title sub">
			<h3 class="title pull-left">• 온라인재고 기준 업로드</h3>
		</div>

	</div>
	<div class="ui wrap-table horizontal mg-t-10">
		<table class="ui table">
			<caption>상품 검색</caption>
			<colgroup>
				<col width="10%">
				<col width="*">
			</colgroup>
			<tbody>
			<tr>
				<th>업로드양식</th>
				<td><button type="button" id="downloadTemplate" class="ui button medium btn-warning"><i class="fa fa-search"></i>다운로드</button></td>
			</tr>
			<tr>
				<th>파일등록</th>
				<td>
					<form id="fileUploadForm">
					<div class="ui form inline">
						<input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="onlineStockPop.getFile($(this))" style="display: none">
						<input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly="" style="width: 50%">
						<button type="button" id="schUploadFile" class="ui button medium mg-l-10">파일찾기</button>
					</div>
					</form>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="ui center-button-group">
        <span class="inner">
			<button id="selectBtn" class="ui button xlarge btn-danger font-malgun">업로드</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
        </span>
	</div>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<script>
	${onlineStockGridBaseInfo}

    // 팝업 html
    var onlineStockPop = {
        init : function() {
            this.event();
			onlineStockPop.getOnlineStockTemplateList();
        },
		initHtml : function() {
			for (var key in detailTable) {
				detailTable[key].html = '';
			}
		},
		event : function() {
			$("#downloadTemplate").bindClick(onlineStockPop.downloadTemplate);// 양식 다운로드 버튼
			$("#schUploadFile").bindClick(onlineStockPop.searchFile);         // 파일찾기 버튼
			$("#selectBtn").bindClick(onlineStockPop.selectFile);    // 등록 버튼

			$('.closeBtn').on('click', function() {
				onlineStockPop.close();
			});
		},
		getOnlineStockTemplateList : function() {
			CommonAjax.basic({
				url : "/item/getStockTemplateList.json",
				method : 'get',
				callbackFunc : function(res) {
					onlineStockGrid.setData(res);
					$('#storeCount').html($.jUtil.comma(onlineStockGrid.gridView.getItemCount()));
				}
			});
		},
		searchFile: function() {
			$("#uploadFile").click();
		},
		valid: function() {
			var uploadFileVal = $("#uploadFile").val();
			var uploadFileNmVal = $("#uploadFileNm").val();

			if (!confirm('신규 기준을 업로드하시겠습니까?')) {
				return false;
			}

			if ($.jUtil.isEmpty(uploadFileVal) || $.jUtil.isEmpty(uploadFileNmVal)) {
				alert("파일 선택 후 진행해주세요.");
				return false;
			}

			return true;
		},
		selectFile: function() {
			if (!onlineStockPop.valid()) {
				return false;
			}

			var url = "/item/setOnlineStockExcel.json";

			// 파일읽기 호출
			$("#fileUploadForm").ajaxForm({
				url     : url,
				method  : "POST",
				enctype : "multipart/form-data",
				timeout : 30000,
				success : function(res) {
					alert("완료되었습니다.");
					onlineStockPop.getOnlineStockTemplateList();
					// close
					// self.close();
				},
				error   : function(resError) {
					// error message alert
					if (resError.responseJSON != null) {
						if (resError.responseJSON.returnMessage != null) {
							alert(resError.responseJSON.returnMessage);
						} else {
							alert(CommonErrorMsg.uploadFailMsg);
						}
					} else if (resError.status !== 200) {
						alert(CommonErrorMsg.dfErrorMsg);
					}
				}
			});

			$("#fileUploadForm").submit();
		},
		/**
		 * 선택한 파일 정보 불러오기
		 */
		getFile: function(_obj) {
			var fileInfo= _obj[0].files[0];
			var fileNm  = fileInfo.name;
			$("#uploadFileNm").val(fileNm);
		},
		/**
		 * 업로드양식 다운로드
		 */
		downloadTemplate: function() {
			var fileUrl = "/static/templates/onlineStock_template.xlsx";

			if(fileUrl != "") {
				location.href = fileUrl;
			} else {
				alert("양식 파일이 없습니다.");
				return false;
			}
		},
		close : function() {
			window.close();
		}
    };

    $(document).ready(function() {
		CommonAjaxBlockUI.global();
		onlineStockPop.init();
		onlineStockGrid.init();
    });

	/**
	 * 상품관리 > 셀러상품관리/점포상품관리 > 변경이력 > 공통그리드 (TD/DS)
	 */
	let onlineStockGrid = {
		gridView : new RealGridJS.GridView("onlineStockGrid"),
		dataProvider : new RealGridJS.LocalDataProvider(),
		init : function() {
			onlineStockGrid.initGrid();
			onlineStockGrid.initDataProvider();
			onlineStockGrid.event();
		},
		initGrid : function() {
			onlineStockGrid.gridView.setDataSource(onlineStockGrid.dataProvider);

			onlineStockGrid.gridView.setStyles(onlineStockGridBaseInfo.realgrid.styles);
			onlineStockGrid.gridView.setDisplayOptions(onlineStockGridBaseInfo.realgrid.displayOptions);
			onlineStockGrid.gridView.setColumns(onlineStockGridBaseInfo.realgrid.columns);
			onlineStockGrid.gridView.setOptions(onlineStockGridBaseInfo.realgrid.options);
		},
		initDataProvider : function() {
			onlineStockGrid.dataProvider.setFields(onlineStockGridBaseInfo.dataProvider.fields);
			onlineStockGrid.dataProvider.setOptions(onlineStockGridBaseInfo.dataProvider.options);
		},
		event : function() {
			// 그리드 선택
			onlineStockGrid.gridView.onDataCellClicked = function(gridView, index) {
			};
		},
		setData : function(dataList) {
			onlineStockGrid.dataProvider.clearRows();
			onlineStockGrid.dataProvider.setRows(dataList);
		}
	};

</script>