<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">문자발송이력</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="recipientSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  recipientMain.search();">
                <table class="ui table">
                    <caption>알림톡 발송 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><span class="text-red-star">일자</span></th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="sendDt">발송일</option>
                                    <option value="regDt">등록일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recipientMain.initDate('0d');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recipientMain.initDate('-2d');">3일전</button>
                                <%--<button type="button" class="ui button small gray-dark font-malgun" onclick="recipientMain.initDate('-30d');">1개월전</button>--%>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn" onclick="recipientMain.search();"><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn" onclick="recipientMain.reset();">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>유형</th>
                        <td>
                            <select id="workType" name="workType" class="ui input medium mg-r-10" style="width: 50%" >
                                <option value="">전체</option>
                                <option value="ALIMTALK">알림톡</option>
                                <option value="SMS">문자</option>
                            </select>
                        </td>
                        <th>성공여부</th>
                        <td>
                            <select id="workStatus" name="workStatus" class="ui input medium mg-r-10" style="width: 50%" >
                                <option value="">전체</option>
                                <option value="COMPLETED">성공</option>
                                <option value="ERROR">실패</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">수신번호</span></th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schKeyword1" name="schKeyword1" class="ui input medium mg-r-5" placeholder="" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                                <input type="text" id="schKeyword2" name="schKeyword2" class="ui input medium mg-r-5" placeholder="" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;-&nbsp;&nbsp;</span>
                                <input type="text" id="schKeyword3" name="schKeyword3" class="ui input medium mg-r-5" placeholder="" style="width:100px;">
                            </div>
                            <input type="hidden" id="schKeyword" name="schKeyword">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <%--<div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="RecipientCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>--%>
        <div style="width:50%; height:500px; float:left;">
            <!-- 검색결과 그리드 -->
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="RecipientCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
            <div class="mg-t-20 mg-b-20" id="recipientListGrid" style="width: 100%; height: 600px;"></div>
        </div>
        <div style="width:45%; height:500px; float:right;">
            <div class="com wrap-title sub">
                <h3 class="title">상세 내용</h3>
            </div>
            <div class="topline"></div>
            <!-- 상세내용 테이블 -->
            <div class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <caption>상세 내용</caption>
                    <colgroup>
                        <col width="25%">
                        <col width="75%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>제목</th>
                        <td style="width: 550px;">
                            <div class="ui form inline">
                                <textarea class="ui input medium mg-r-5" id="subject" name="subject" style="width: 100%; height: 50px"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>내용</th>
                        <td style="width: 550px;">
                            <div class="ui form inline">
                                <textarea class="ui input medium mg-r-5" id="body" name="body" style="width: 100%; height: 450px"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr id="resendTr" style="display: none">
                        <th>재발송</th>
                        <td>
                            <div class="ui form inline text-center">
                                <button class="ui button xlarge cyan font-malgun" id="resendBtn" onclick="recipientMain.resend()">재발송</button>
                            </div>
                        </td>
                    </tr>
                    <input type="hidden" id="bodyArgument" name="bodyArgument">
                    <input type="hidden" id="getCurrentRow" name="getCurrentRow" value="-1">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/message/recipientMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>

<script>
    ${recipientGridBaseInfo}
</script>