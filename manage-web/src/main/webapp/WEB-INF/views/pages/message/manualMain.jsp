<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">메시지 수동발송 관리</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="manualSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  manualMain.search();">
                <table class="ui table">
                    <caption>메세지발송 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="endDt">수정일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="manualMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="manualMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="manualMain.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="manualMain.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <th>등록자</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="regId" name="regId" class="ui input medium mg-r-5" style="width: 250px;"/>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn"><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn">초기화</button><br/>
                                <%--<button type="button" class="ui button large dark-blue mg-t-5" id="excelDownloadBtn">엑셀다운</button><br/>--%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" />
                            </div>
                        </td>
                        <th>상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schWorkStatus" name="schWorkStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${messageWorkStatus}" varStatus="status">
                                        <option value="${codeDto.key}">${codeDto.value}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="manualTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="manualListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">등록/수정</h3>
        <h4 class="text" style="font-weight: normal;color: red">&nbsp;&nbsp;※ 광고성(마케팅 등) 목적으로 SMS를 발송하시면 안됩니다. (수신거부 기능이 없으므로 법적 문제 발생)</h4>
        <%--<button class="ui button small white font-malgun" style="float:right;" id="sampleBtn"><a href="/static/templates/uploadSendList_template.xlsx" download>엑셀양식 다운로드</a></button>--%>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="manualSetForm" onsubmit="return false;" enctype="multipart/form-data">
            <table class="ui table">
                <caption>등록/수정</caption>
                <colgroup>
                    <col width="15%">
                    <col width="30%">
                    <col width="15%">
                    <col width="30%">
                </colgroup>
                <tbody>
                    <input type="hidden" id="manualSendSeq" name="manualSendSeq" value="">
                    <input type="hidden" id="manualSendWorkSeq" name="manualSendWorkSeq" value="">
                    <input type="hidden" id="coupledWorkSeq" name="coupledWorkSeq" value="">
                    <tr>
                        <th>작업 종류</th>
                        <td>
                            <div class="ui form inline">
                                <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 30%" >
                                    <option value="HYPER">Hyper</option>
                                    <option value="CLUB">Club</option>
                                    <option value="EXPRESS">Express</option>
                                </select>
                                <select id="affiliationType" name="affiliationType" class="ui input medium mg-r-10" style="width: 30%" >
                                    <option value="HEAD">본사</option>
                                    <option value="STORE">점포</option>
                                </select>
                                <select id="workType" name="workType" class="ui input medium mg-r-10" style="width: 30%">
                                    <option value="ALIMTALK">알림톡</option>
                                    <option value="SMS">문자</option>
                                    <option value="MAIL">메일</option>
                                </select>
                            </div>
                        </td>
                        <th>발송 상태</th>
                        <td>
                            <div class="ui form inline">
                                <span class="text" id="workStatus" name="workStatus">발송 요청</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>작업 명</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input id="coupledWorkName" name="coupledWorkName" type="text" class="ui input medium mg-r-5" maxlength="40">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>설명</th>
                        <td colspan="3">
                        <span class="ui form inline">
                            <input id="description" name="description" type="text" class="ui input medium mg-r-5" maxlength="400">
                        </span>
                        </td>
                    </tr>
                    <tr>
                        <th>템플릿 선택</th>
                        <td>
                            <div class="ui form inline">
                                <select id="templateCode" name="templateCode" class="ui input medium mg-r-10" style="width: 100%">
                                </select>
                            </div>
                        </td>
                        <th>예약발송일시</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="workDt" name="workDt" class="ui input medium mg-r-5" placeholder="발송일" style="width:250px;">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="3">발송 컨텐츠</th>
                        <td colspan="3">
                            <input id="contentSubject" name="contentSubject" type="text" class="ui input medium mg-r-5" style="width: 100%" placeholder="제목">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input id="callback" name="callback" type="text" class="ui input medium mg-r-5" style="width: 100%" placeholder="발신자정보">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="3">
                            <textarea id="contentBodyT" name="contentBodyT" rows="6" class="ui input medium mg-r-5" style="width: 100%;" placeholder="내용"></textarea>
                            <div id="contentBodyH" name="contentBodyH" rows="6" style="width: 100%;"></div>
                            <input type="hidden" id="contentBody" name="contentBody">
                        </td>
                    </tr>
                </tbody>
                <tbody id="editBody">
                    <tr>
                    </tr>
                </tbody>
                <tbody id="setBody">
                    <tr>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" id="bodyArgument" name="bodyArgument">
        </form>
    </div>
    <div class="ui wrap-table horizontal mg-t-10" id="sendListDiv">
        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">수신자 목록(<span id="sendListCount">0</span>개)</h3>
                <button class="ui button medium white font-malgun mg-r-5" style="float:right;" id="sendRowDeleteBtn">삭제</button>
                <button class="ui button medium white font-malgun mg-r-5" style="float:right;" id="excelUploadBtn">엑셀일괄등록</button>
                <button class="ui button medium white font-malgun mg-r-5" style="float:right;" id="userSearchBtn">주문회원 검색</button>
            </div>
        </div>
        <div class="mg-t-20 mg-b-20" id="sendListGrid" style="width: 100%; height: 350px;"></div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setManualBtn">등록</button>
            <%--<button class="ui button xlarge cyan font-malgun" id="editManualBtn">수정</button>--%>
            <button class="ui button xlarge dark-blue font-malgun" id="cancelManualBtn" hidden>발송취소</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<style>
    /* ui-datepicker CSS 클래스에 추가로 z-index를 변경하여 Froala 에디터보다 위에 뜨도록 함. */
    .ui-datepicker {z-index: 100 !important;}
</style>

<iframe style="display:none;" id="manualExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/message/manualMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>

<script>
	// 수동 발송 리스트 리얼그리드 기본 정보
    ${manualWorkGridBaseInfo}
    // 발송 대상자 리스트 기본 정보
    ${sendListGridBaseInfo}
</script>
<script>
    let workStatusMap = ${workStatus};
    let empId = ${empId};
    let isHeadOffice = ${isHeadOffice};
</script>