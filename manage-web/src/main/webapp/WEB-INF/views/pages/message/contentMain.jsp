<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">템플릿 관리</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="contentSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  contentMain.search();">
                <table class="ui table">
                    <caption>컨텐츠 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="20%">
                        <col width="7%">
                        <col width="20%">
                        <col width="7%">
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5"  placeholder="템플릿코드 or 템플릿명 or 설명" style="width:500px">
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn"><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn">초기화</button><br/>
                                <button type="button" class="ui button large dark-blue mg-t-5" id="excelDownloadBtn">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 150px" >
                                    <option value="">전체</option>
                                    <option value="HYPER">HYPER</option>
                                    <option value="CLUB">CLUB</option>
                                    <option value="EXPRESS">EXPRESS</option>
                                </select>
                            </div>
                        </td>
                        <th>발송주체</th>
                        <td>
                            <div class="ui form inline">
                                <select id="affiliationType" name="affiliationType" class="ui input medium mg-r-10" style="width: 150px" >
                                    <option value="">전체</option>
                                    <option value="HEAD">본사</option>
                                    <option value="STORE">점포</option>
                                </select>
                            </div>
                        </td>
                        <th>발송채널</th>
                        <td>
                            <div class="ui form inline">
                                <select id="workType" name="workType" class="ui input medium mg-r-10" style="width: 150px" >
                                    <option value="">전체</option>
                                    <option value="ALIMTALK">알림톡</option>
                                    <option value="SMS">문자</option>
                                    <option value="MAIL">메일</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색된 템플릿 : <span id="contentTotalCount">0</span>개</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="contentListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">템플릿 상세 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="contentSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>템플릿 상세 내역</caption>
                <colgroup>
                    <col width="10%">
                    <col width="23%">
                    <col width="10%">
                    <col width="23%">
                    <col width="5%">
                    <col width="*">
                </colgroup>
                <tbody>
                <input type="hidden" id="contentSeq" name="contentSeq" value="0">
                <tr>
                    <th>템플릿명</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="name" name="name" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>설명</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="description" name="description" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th rowspan="2">제목</th>
                    <td rowspan="2">
                        <div class="ui form inline">
                            <textarea id="contentSubject" name="contentSubject" rows="2" class="ui input medium mg-r-5" style="width: 100%;"></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>점포유형</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="storeTypeF" name="storeType" class="ui input medium mg-r-10" style="width: 70%" >
                                <c:forEach var="storeType" items="${storeType}">
                                    <option value=${storeType.key}>${storeType.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>발주주체/채널</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="affiliationTypeF" name="affiliationType" class="ui input medium mg-r-10" style="width: 40%" >
                                <c:forEach var="affiliationType" items="${affiliationType}">
                                    <option value=${affiliationType.key}>${affiliationType.value}</option>
                                </c:forEach>
                            </select>
                            <select id="workTypeF" name="workType" class="ui input medium mg-r-10" style="width: 40%" >
                                <c:forEach var="workType" items="${workType}">
                                    <option value=${workType.key}>${workType.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>발신정보</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="callback" name="callback" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>템플릿코드</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="templateCode" name="templateCode" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th rowspan="6">내용</th>
                    <td rowspan="6" id="html">
                        <div id="contentBodyH" name="contentBodyH" rows="12" style="width: 100%;"></div>
                    </td>
                    <td rowspan="6" id="text">
                        <div class="ui form inline" id="textArea">
                            <textarea id="contentBodyT" name="contentBodyT" rows="12" class="ui input medium mg-r-5" style="width: 100%;"></textarea>--%>
                        </div>
                    </td>
                    <input type="hidden" id="contentBody" name="contentBody">
                </tr>
                <tr>
                    <th>기타정보1</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="etc1" name="etc1" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>기타정보2</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="etc2" name="etc2" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>기타정보3</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="etc3" name="etc3" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>기타정보4</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="etc4" name="etc4" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>기타정보5</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="etc5" name="etc5" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>발송차단여부</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="blockYn" name="blockYn" class="ui input medium mg-r-10" style="width: 100px" >
                                <option value="N">사용안함</option>
                                <option value="Y">사용</option>
                            </select>
                            <%--<input id="blockStartTime" name="blockStartTime" type="number" class="ui input medium mg-r-5" style="width: 100px; align:right; display: inline;" min=0 max=23 value="0"/>
                            <span id=blockRangeStart" name="blockRangeStart" class="text">시 부터</span>
                            <input id="blockEndTime" name="blockEndTime" type="number" class="ui input medium mg-r-5" style="width: 100px; align:right; display: inline;" min="0" max="23" value="0"/>
                            <span id=blockRangeEnd" name="blockRangeEnd" class="text">시 까지</span>--%>
                            <input id="blockStartTime" name="blockStartTime" type="text" class="ui input medium mg-r-5" style="width: 100px;"/>
                            <span id=blockRangeStart" name="blockRangeStart" class="text">시 부터</span>
                            <input id="blockEndTime" name="blockEndTime" type="text" class="ui input medium mg-r-5" style="width: 100px;"/>
                            <span id=blockRangeEnd" name="blockRangeEnd" class="text">시 까지</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>작성자</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="regId" name="regId" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>작성일시</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="regDt" name="regDt" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>수정자</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="chgId" name="chgId" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th>수정일시</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="chgDt" name="chgDt" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <input type="hidden" id="attachment" name="attachment">
                </tbody>
            </table>
        </form>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">첨부정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="mmsAttachForm" onsubmit="return false;" enctype="multipart/form-data">
            <table class="ui table">
                <caption>MMS 첨부 정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody>
                <tr>
                    <th>이미지 첨부</th>
                    <td colspan="1">
                        <input type="file" id="file" name="file" accept=".jpg, .jpeg">
                    </td>
                    <th>현재 등록 된 이미지</th>
                    <td colspan="1">
                        <label id="fileId"></label>
                        <button type="button" class="ui button small gray-dark font-malgun mg-l-10" id="deleteAttachmentBtn">삭제</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <form id="buttonForm" onsubmit="return false;">
            <table class="ui table">
                <caption>알림톡 버튼 정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                    <col width="20%">
                </colgroup>
                <tbody>
                <tr id="buttonSet1">
                    <th>버튼타입</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="buttonType1" name="buttonType" class="ui input medium mg-r-10" style="width: 100%" onchange="contentMain.changeButtonParamName(this.id)">
                                <c:forEach var="btn" items="${buttonInfo}">
                                    <option value=${btn.key}>${btn.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>버튼이름</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="buttonName1" name="buttonName" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam11"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param11" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam12"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param12" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr id="buttonSet2">
                    <th>버튼타입</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="buttonType2" name="buttonType" class="ui input medium mg-r-10" style="width: 100%" onchange="contentMain.changeButtonParamName(this.id)">
                                <c:forEach var="btn" items="${buttonInfo}">
                                    <option value=${btn.key}>${btn.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>버튼이름</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="buttonName2" name="buttonName" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam21"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param21" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam22"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param22" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr id="buttonSet3">
                    <th>버튼타입</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="buttonType3" name="buttonType" class="ui input medium mg-r-10" style="width: 100%" onchange="contentMain.changeButtonParamName(this.id)">
                                <c:forEach var="btn" items="${buttonInfo}">
                                    <option value=${btn.key}>${btn.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>버튼이름</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="buttonName3" name="buttonName" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam31"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param31" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam32"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param32" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr id="buttonSet4">
                    <th>버튼타입</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="buttonType4" name="buttonType" class="ui input medium mg-r-10" style="width: 100%" onchange="contentMain.changeButtonParamName(this.id)">
                                <c:forEach var="btn" items="${buttonInfo}">
                                    <option value=${btn.key}>${btn.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>버튼이름</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="buttonName4" name="buttonName" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam41"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param41" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam42"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param42" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr id="buttonSet5">
                    <th>버튼타입</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <select id="buttonType5" name="buttonType" class="ui input medium mg-r-10" style="width: 100%" onchange="contentMain.changeButtonParamName(this.id)">
                                <c:forEach var="btn" items="${buttonInfo}">
                                    <option value=${btn.key}>${btn.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>버튼이름</th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="buttonName5" name="buttonName" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam51"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param51" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                    <th id="buttonParam52"></th>
                    <td colspan="1">
                        <div class="ui form inline">
                            <input id="param52" name="param" type="text" class="ui input medium mg-r-5" style="width: 100%;">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setContentBtn">등록</button>
            <button class="ui button xlarge dark-blue font-malgun" id="editContentBtn">수정</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<iframe style="display:none;" id="contentExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/message/contentMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>

<script>
    // 알림톡 리스트 리얼그리드 기본 정보
    ${contentInfoGridBaseInfo}
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
