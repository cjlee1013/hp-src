<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">Push 이미지 관리</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="pushimageSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  pushimageMain.search();">
                <table class="ui table">
                    <caption>Push 이미지 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pushimageMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="pushimageMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun" onclick="pushimageMain.initSearchDate('-30d');">1개월전</button>
                            </div>
                        </td>
                        <th>이미지 이름</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="imageNameForSearch" name="imageName" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" />
                            </div>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn"><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색된 Push 이미지 작업 : <span id="pushimageTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="pushimageListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">Push 이미지 등록</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="pushimageSetForm" enctype="multipart/form-data" accept-charset="UTF-8" onsubmit="return false;">
            <table class="ui table">
                <caption>Push 이미지 등록</caption>
                <colgroup>
                    <col width="15%">
                    <col width="85%">
                </colgroup>
                <tbody>
                <tr>
                    <th>이미지 이름</th>
                    <td>
                        <input type="text" id="imageName" name="imageName" style="width: 50%">
                    </td>
                </tr>
                <tr>
                    <th>파일업로드</th>
                    <td>
                        <input type="file" id="imageFile" name="imageFile" accept=".gif, .jpeg, .jpg, .png" style="width: 100%">
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="registBtn">등록</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/message/pushimageMain.js?${fileVersion}"></script>

<script>
// Push 이미지 목록 리얼그리드 기본 정보
${pushimageGridBaseInfo}
</script>
