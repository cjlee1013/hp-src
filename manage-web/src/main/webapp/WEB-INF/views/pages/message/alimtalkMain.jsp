<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">알림톡 발송 조회</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="alimtalkSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  alimtalkMain.search();">
                <table class="ui table">
                    <caption>알림톡 발송 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="endDt">수정일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="alimtalkMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="alimtalkMain.initSearchDate('-2d');">3일전</button>
                                <%--<button type="button" class="ui button small gray-dark font-malgun" onclick="alimtalkMain.initSearchDate('-30d');">1개월전</button>--%>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn"><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn">초기화</button><br/>
                                <%--<button type="button" class="ui button large dark-blue mg-t-5" id="excelDownloadBtn">엑셀다운</button><br/>--%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" />
                            </div>
                        </td>
                        <th>상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schWorkStatus" name="schWorkStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${messageWorkStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색 결과 : <span id="alimtalkTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="alimtalkListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">알림톡 상세 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="alimtalkSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>알림톡 상세 정보</caption>
                <colgroup>
                    <col width="150px">
                    <col>
                    <col width="150px">
                    <col>
                    <col width="50%">
                </colgroup>
                <tbody>
                <tr>
                    <th>작업 이름</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span class="text" id="workName" name="workName"></span>
                        </div>
                    </td>
                    <td rowspan="6" style="vertical-align: top">
                        <div class="mg-t-5" style="float:right; width:100%">
                            <div class="com wrap-title sub">
                                <h3 class="title">
                                    발송 : <span id="setCount">0</span>건,
                                    성공 : <span id="sendCount">0</span>건,
                                    실패 : <span id="failCount">0</span>건
                                </h3>
                            </div>
                            <div class="topline"></div>
                        </div>
                        <div class="ui form inline">
                            <div class="mg-t-20 mg-b-20" id="sendListGrid" style="width: 100%; height: 382px;"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>작업 설명</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span class="text" id="description" name="description"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>제목</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span class="text" id="subject" name="subject"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>내용</th>
                    <td colspan="3">
                        <div class="ui form inline" style="overflow-y: auto;height: 150px">
                            <span class="text" id="bodyTemplate" name="bodyTemplate"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>등록일시</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="regDt" name="regDt"></span>
                        </div>
                    </td>
                    <th>등록자</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="regName" name="regName"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>수정일시</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="chgDt" name="chgDt"></span>
                        </div>
                    </td>
                    <th>수정자</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="chgName" name="chgName"></span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
        </span>
    </div>
</div>

<iframe style="display:none;" id="alimtalkExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/message/alimtalkMain.js?${fileVersion}"></script>

<script>
// 알림톡 리스트 리얼그리드 기본 정보
${alimtalkWorkGridBaseInfo}
// 발송자 리스트 리얼그리드 정보
${alimtalkSendListGridBaseInfo}
</script>
