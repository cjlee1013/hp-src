<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">덤 행사 관리</h2>
    </div>

    <%-- 덤 행사 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <input type="hidden" id="schPromoType" name="schPromoType" value="ADD"/>
                <table class="ui table">
                    <caption>덤 행사 검색</caption>
                    <colgroup>
                        <col width="12%">
                        <col width="27%">
                        <col width="12%">
                        <col width="27%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="PROMODT">행사일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="etcPromo.calendar.initCalendarDate(etcPromoSearch, 'schStartDt', 'schEndDt', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPromo.calendar.initCalendarDate(etcPromoSearch, 'schStartDt', 'schEndDt', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPromo.calendar.initCalendarDate(etcPromoSearch, 'schStartDt', 'schEndDt', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPromo.calendar.initCalendarDate(etcPromoSearch, 'schStartDt', 'schEndDt', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tr>
                        <th>유형</th>
                        <td>
                            <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                    <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 100px;">
                                    <option value="ETCMNGNM">행사명</option>
                                    <option value="REGNM">등록자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="etcPromoTotalCnt">0</span>건</h3>
        </div>
        <div id="etcPromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 덤 행사 상세 정보 영역 --%>
    <form id="etcPromoForm" name="etcPromoForm" onsubmit="return false;">
        <input type="hidden" id="promoType" name="promoType" value="ADD"/>
        <div class="com wrap-title sub">
            <h3 class="title">• 행사 상세</h3><span class="mg-l-10">* 행사 일자가 동일 상품에 중복 등록될 경우 최근 등록된 덤 행사 내용이 적용됩니다.</span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="11%">
                    <col width="89%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">유형</th>
                    <td>
                        <select id="storeType" name="storeType" class="ui input medium" style="width: 100px;">
                            <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                <option value="${storeType.mcCd}" data-sitetype="${storeType.ref2}">${storeType.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">덤 관리명</th>
                    <td class="ui form inline">
                        <input type="text" id="etcMngNm" name="etcMngNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 350px;text-align: left"><span class="text"><span id="etcMngNmLen"></span> / 20자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">덤 행사정보</th>
                    <td class="ui form inline">
                        <input type="text" id="etcInfo" name="etcInfo" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 350px;text-align: left">
                        <span class="text">
                            <span id="etcInfoLen"></span> / 20자 &nbsp;&nbsp;&nbsp;&nbsp;* 상품 상세 및 주문서에 기재되는 덤 행사 정보 입니다.
                        </span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">행사기간</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="promoStartDt" name="promoStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="promoEndDt" name="promoEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 100px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">노출 점포</th>
                    <td>
                        <c:forEach var="dispStoreType" items="${dispStoreType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${dispStoreType.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="dispStoreType" class="ui input" value="${dispStoreType.mcCd}" ${checked}/><span>${dispStoreType.mcNm}</span></label>
                        </c:forEach>

                        <button type="button" id="storePopBtn" class="ui button small gray-dark font-malgun mg-l-10">점포선택</button>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">적용상품정보</th>
                    <td>
                        <span class="ui form inlien">
                            상품 수 : <span id="itemCnt"></span>개
                            <span class="ui pull-right mg-b-5">
                                <button type="button" id="itemPopBtn" class="ui button small" onclick="etcPromo.popup.openItemPopup();">상품등록</button>
                                <button type="button" class="ui button small" onclick="etcPromo.popup.openItemReadPopup();">일괄등록</button>
                            </span>
                        </span>

                        <div class="mg-t-10 mg-b-5" id="etcPromoItemGrid" style="width: 100%; height: 350px;"></div>

                        <table id="itemArea" class="ui table-inner mg-t-10">
                            <caption>상품정보</caption>
                            <colgroup>
                                <col width="9%">
                                <col width="91%">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>상품 정보</th>
                                <td class="ui form inline">
                                    <input type="text" id="itemNo" name="itemNo" class="ui input medium mg-r-5" style="width: 100px;">
                                    <input type="text" id="itemNm" name="itemNm" class="ui input medium" style="width: 250px;">
                                </td>
                            </tr>
                            <tr>
                                <th class="text-red-star">전시여부</th>
                                <td>
                                    <div class="ui form inline">
                                        <select id="dispYn" name="dispYn" class="ui input medium" style="width: 100px;">
                                            <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                                <c:set var="selected" value="" />
                                                <c:if test="${dispYn.ref1 eq 'df'}">
                                                    <c:set var="selected" value="selected" />
                                                </c:if>
                                                <option value="${dispYn.mcCd}" ${selected}>${dispYn.mcNm}</option>
                                            </c:forEach>
                                        </select>

                                        <button type="button" id="setItemDispYn" class="ui button medium gray-dark mg-l-5" onclick="etcPromo.setItemDispYn();">적용</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
</div>

<!-- 점포선택 폼 -->
<div style="display:none;" class="storePop" >
    <form id="storePopForm" name="storePopForm" method="post" action="/common/popup/storeSelectPop">
        <input type="text" name="storeTypePop" value="HYPER"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" name="setYn" value="Y"/>
        <input type="text" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="etcPromo.popup.storePopCallback"/>
    </form>
</div>

<script>
    var dispStoreTypeJson= ${dispStoreTypeJson};

	// 덤 행사 조회 그리드 정보
    ${etcPromoGridBaseInfo}

	// 덤 행사 적용상품 그리드 정보
    ${etcPromoItemGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<%--<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>--%>
<script type="text/javascript" src="/static/js/promotion/etcPromoMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/etcPromoMain.grid.js?${fileVersion}"></script>
