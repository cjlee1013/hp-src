<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="">
    <!-- 검색영역 -->
    <div class="">
        <form id="couponSearchForm" name="couponSearchForm" onsubmit="return false;">
            <input type="hidden" id="schExceptTempStatusYn" name="schExceptTempStatusYn" value="Y">
            <input type="hidden" id="schIssueCouponOnlyYn" name="schIssueCouponOnlyYn" value="Y">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">쿠폰 사용 현황</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>쿠폰 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="500">
                        <col width="120px">
                        <col width="200px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id='schDateType' name='schDateType' class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="ISSUEDT" selected>발급기간</option>
                                    <option value="REGDT" selected>등록일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button medium mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button medium mg-r-5">1주일전</button>
                                <button type="button" id="setOneMonthBtn" class="ui button medium mg-r-5">1개월전</button>
                                <button type="button" id="setThreeMonthBtn" class="ui button medium">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="button" id="getCouponSearchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="initCouponSearchBtn" class="ui button large mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadCouponListBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id='schType' name='schType' class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="COUPONNM" selected>쿠폰명</option>
                                    <option value="COUPONNO">쿠폰번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" onKeypress="javascript:if (event.keyCode == 13) {couponSearch.getCouponSearch();}">
                            </div>
                        </td>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id='schStoreType' name='schStoreType' class="ui input medium mg-r-10" style="width: 120px">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:set var="selected" value="" />
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected"/>
                                        </c:if>
                                        <c:if test="${codeDto.ref3 ne 'VIRTUAL'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="couponSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="couponGrid" style="width: 100%; height: 195px" ></div>
    </div>
    <!-- 검색영역 -->
    <!-- 상세영역 -->
    <div class="mg-t-30">
        <form id="couponIssueHistSearchForm" name="couponIssueHistSearchForm" onsubmit="return false;">
            <input type="hidden" id="schPageType" name="schPageType" value="USE_HIST">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">쿠폰번호<span class="mg-l-10 mg-r-30" id="subCouponNo"></span><span class="text-mint-dark mg-l-10" id="couponStat"></span></h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="450px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>사용일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="histSchStartDt" name="histSchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="histSchEndDt" name="histSchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="button" id="getCouponIssueHistSearchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="initCouponIssueHistSearchBtn" class="ui button large mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadCouponHistListBtn" class="ui button large dark-blue  mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>회원검색</th>
                        <td>
                            <div class="ui form inline">
                                <input type="hidden" id="histSchValue" name="histSchValue">
                                <input type="text" id="memberId" name="memberId" class="ui input medium mg-r-5" style="width: 150px;" readonly="true">
                                <input type="text" id="memberNm" name="memberNm" class="ui input medium mg-r-5" style="width: 100px;" readonly="true">
                                <button type="button" class="ui button medium dark-blue" id="schMember">조회</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="couponIssueHistSearchCnt">0</span>건</h3>
            <span class="text mg-l-30" style="font-size: 14px" >※ 사용 및 취소(재발급)한 쿠폰까지 확인가능합니다.</span>
        </div>
        <div class="topline"></div>
        <div id="couponIssueHistGrid" style="width: 100%; height: 510px" ></div>
    </div>
    <!-- 상세영역 -->
</div>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/couponIssueHistory.js?v=${fileVersion}"></script>
<script>
    // 쿠폰 정보 Grid
    ${couponListGridBaseInfo}
    // 쿠폰 사용 정보 Grid
    ${couponUseHistoryGridBaseInfo}

    var storeTypeJson = ${storeTypeJson};
    var couponStatusJson = ${couponStatusJson};
    var couponPurposeJson = ${couponPurposeJson};
    var couponTypeJson = ${couponTypeJson};

</script>