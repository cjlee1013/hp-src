<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/jquery.validate.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/commonValidate.js?v=${fileVersion}"></script>
<div class="">
    <!-- 검색영역 -->
    <div class="">
        <form id="discountSearchForm" name="discountSearchForm" onsubmit="return false;">
            <input type="hidden" id="schPageType" name="schPageType" value="HIST">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">할인사용현황</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>할인 사용 조회 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="670px">
                        <col width="120px">
                        <col width="250px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-10" style="width: 120px; display: none;">
									<%--<option value="issueDt">적용기간</option>--%>
                                    <option value="regDt" selected>등록일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button medium mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button medium mg-r-5">1주일전</button>
                                <button type="button" id="setOneMonthBtn" class="ui button medium mg-r-5">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: right" class="ui form mg-l-30">
                                <button type="button" id="getDiscountSearchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="initDiscountSearchBtn" class="ui button large mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadDiscountListBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="discountNm">할인명</option>
                                    <option value="discountNo">할인번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 300px;" minlength="2" onKeypress="javascript:if (event.keyCode == 13) {discountSearch.getDiscountSearch();}">
                            </div>
                        </td>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id='schStoreType' name='schStoreType' class="ui input medium mg-r-10" style="width: 120px">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.ref3 ne 'VIRTUAL'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="discountSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="discountGrid" style="width: 100%; height: 178px;"></div>
    </div>
    <!-- 검색영역 -->

	<!-- 회원영역 -->
	<div class="mg-t-30">
        <form id="discountHistSearchForm" name="discountHistSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">할인번호<span class="mg-l-10 mg-r-30" id="subDiscountNo"></span></h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="450">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>주문일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="useSchStartDt" name="useSchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="useSchEndDt" name="useSchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: right" class="ui form mg-l-30">
                                <button type="button" id="getDiscountHistSearchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="initDiscountHistSearchBtn" class="ui button large mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadDiscountHistListBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>회원정보</th>
                        <td>
                            <div class="ui form inline">
                                <input type="hidden" id="userNo" name="userNo">
                                <input type="text" id="userId" name="userId" class="ui input medium mg-r-5" style="width: 150px;" readonly="true">
                                <input type="text" id="userNm" name="userNm" class="ui input medium mg-r-5" style="width: 100px;" readonly="true">
                                <button type="button" class="ui button medium dark-blue" id="schMember">조회</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="com wrap-title sub" style="margin-top: 30px;">
                <h3 class="title">검색결과 : <span id="discountUseSearchCnt">0</span>건</h3>
            </div>
            <div class="topline"></div>
            <div id="discountHistoryGrid" style="width: 100%; height: 510px;"></div>
        </form>
    </div>
</div>

<script>
    let discountKindJson = ${discountKindJson};
    let storeTypeJson = ${storeTypeJson};
    let useYnJson = ${useYnJson};

    // 할인사용조회 할인 리얼그리드 정보
    ${discountHistoryDiscountGridBaseInfo}
    // 할인사용조회 사용목록 리얼그리드 기본 정보
    ${discountHistoryGridBaseInfo}
</script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/discountHistory.js?v=${fileVersion}"></script>