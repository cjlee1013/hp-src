<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%--<!-- 검색영역 -->--%>
<form id="crmCouponSearchForm" name="crmCouponSearchForm" onsubmit="return false;">
    <input type="hidden" id="schExceptTempStatusYn" name="schExceptTempStatusYn" value="Y">
    <input type="hidden" id="schIssueCouponOnlyYn" name="schIssueCouponOnlyYn" value="Y">
    <input type="hidden" id="schDateType" name="schDateType" value="REGDT">
    <input type="hidden" id="schPurpose" name="schPurpose" value="CRM">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">CRM쿠폰 대량발급</h2>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>쿠폰 검색</caption>
            <colgroup>
                <col width="11%">
                <col width="39%">
                <col width="11%">
                <col width="20%">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>조회기간</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                        <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                        <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                        <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <button type="button" id="setTodayBtn" class="ui button medium mg-r-5">오늘</button>
                        <button type="button" id="setOneWeekBtn" class="ui button medium mg-r-5">1주일전</button>
                        <button type="button" id="setOneMonthBtn" class="ui button medium mg-r-5">1개월전</button>
                        <button type="button" id="setThreeMonthBtn" class="ui button medium">3개월전</button>
                    </div>
                </td>
                <!-- 검색 버튼 -->
                <td rowspan="2">
                    <div style="text-align: right;" class="ui form mg-l-30">
                        <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                        <button type="button" id="schResetBtn" class="ui button large mg-t-5">초기화</button><br/>
                        <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                    </div>
                </td>
            </tr>
            <tr>
                <th>검색어</th>
                <td>
                    <div class="ui form inline">
                        <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                            <option value="COUPONNM" selected>쿠폰명</option>
                            <option value="COUPONNO">쿠폰번호</option>
                        </select>
                        <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;" minlength="2">
                    </div>
                </td>
                <th>점포유형</th>
                <td>
                    <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 100px;">
                        <c:forEach var="storeType" items="${storeType}" varStatus="status">
                            <c:if test="${storeType.ref3 ne 'VIRTUAL'}">
                                <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</form>

<div class="com wrap-title sub" style="margin-top: 30px;">
    <h3 class="title">검색결과 : <span id="couponSearchCnt">0</span>건</h3>
</div>
<div class="topline"></div>
<div id="crmCouponGrid" style="width: 100%; height: 320px" ></div>
<!-- 검색영역 -->

<div class="mg-t-30"/>

<!-- 상세영역 -->
<form id="crmCouponForm" name="crmCouponForm" onsubmit="return false;">
    <input type="hidden" id="couponNo" name="couponNo" value="">
    <input type="hidden" id="couponFileNo" name="couponFileNo" value="">
    <input type="hidden" id="fileUrl" name="fileUrl" value="">
    <div class="com wrap-title sub">
        <span class="ui form inlien">
            <h3 class="title">발급회원 일괄등록</h3>
            <span class="ui pull-right mg-b-5">
                <button type="button" id="templateDownBtn" class="ui button small">엑셀 양식 다운로드</button>
            </span>
        </span>

    </div>
    <div class="topline"></div>
    <div id="uploadListGrid" style="width: 100%; height: 123px" ></div>

    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>발급회원 일괄등록</caption>
            <colgroup>
                <col width="11%">
                <col width="39%">
                <col width="11%">
                <col width="39%">
            </colgroup>

            <tbody>
            <tr>
                <th>쿠폰정보</th>
                <td>
                    <div class="ui form inline">
                        <span class="text" id="couponNoSpan"></span>
                    </div>
                </td>
                <th>파일등록</th>
                <td>
                    <div class="ui form inline">
                        <input type="file" id="uploadFile" name="fileArr" style="display: none;">
                        <button type="button" id="schFileBtn" class="ui button medium gray-dark mg-r-5">파일찾기</button>
                        <span id="uploadFileNm" class="text"></span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui mg-t-15"/>
    <div style="float: left;">
        <div>
            <strong style="color:red;">※ 쿠폰 대량발급 안내사항</strong><br/>
            - 등록 파일명: [*.xlsx]<br/>
            - 엑셀파일 문서보안 해제 후 업로드 필수<br/>
            - 업로드 이후 파일삭제 불가, 발급배치 전까지 취소가능
        </div>
    </div>
    <%-- 하단 버튼 영역 --%>
    <div class="ui mg-t-30">
        <table class="ui">
            <tbody>
            <tr>
                <td style="text-align: center;">
                    <button type="button" id="setBtn" class="ui button xlarge cyan mg-t-15 mg-r-5">업로드<button>
                    <button type="button" id="resetBtn" class="ui button xlarge white mg-t-15">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</form>
<!-- 상세영역 -->

<script>
    var couponTypeJson = ${couponTypeJson};
    var couponStatusJson = ${couponStatusJson};
    var dpartCdJson = ${dpartCdJson};

    // 쿠폰 정보 Grid
    ${crmCouponGridBaseInfo}

    // 업로드 그리드 정보
    ${uploadListGridBaseInfo}
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/crmCouponIssue.js?v=${fileVersion}"></script>
