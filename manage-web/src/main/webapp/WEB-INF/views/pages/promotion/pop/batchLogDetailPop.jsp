<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="batchDetailPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">배치 로그 상세</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="35%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>등록일</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="등록일" style="width:100px;">
                        </div>
                    </td>
                    <td>
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과 : <span id="batchLogDetailCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="batchLogDetailGrid" style="width: 100%; height: 400px;"></div>
</div>

<script>
    var batchStatusJson  = ${batchStatusJson};
    var batchNm  = "${batchNm}";

    // 그리드 정보
    ${batchLogDetailGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/pop/batchLogDetailPop.js?v=${fileVersion}"></script>