<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사은행사 관리</h2>
    </div>

    <%-- 사은행사 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="ersPromoSearchForm" name="ersPromoSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>사은행사 검색</caption>
                    <colgroup>
                        <col width="12%">
                        <col width="27%">
                        <col width="12%">
                        <col width="27%">
                        <col width="*">
                    </colgroup>
                    <tr>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT" selected>등록일</option>
                                    <option value="PROMODT">행사일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="initCalendarDate(ersPromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(ersPromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(ersPromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(ersPromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="8">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>구매기준유형</th>
                        <td>
                            <select id="schEventTargetType" name="schEventTargetType" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="eventTargetType" items="${eventTargetType}" varStatus="status">
                                    <option value="${eventTargetType.mcCd}">${eventTargetType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>지급기준유형</th>
                        <td>
                            <select id="schEventGiveType" name="schEventGiveType" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="eventGiveType" items="${eventGiveType}" varStatus="status">
                                    <option value="${eventGiveType.mcCd}">${eventGiveType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 100px;" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="ersPromoSearch.openStorePopup();" >조회</button>
                        </td>
                        <th>확정여부</th>
                        <td>
                            <select id="schConfirmYn" name="schConfirmYn" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="confirmYn" items="${confirmYn}" varStatus="status">
                                    <option value="${confirmYn.mcCd}">${confirmYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 180px;">
                                    <option value="EVENTNM">행사명</option>
                                    <option value="EVENTCD" selected>행사번호</option>
                                    <option value="ITEMNO">상품번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;">
                            </div>
                        </td>
                        <th>온라인 사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="ersPromoTotalCount">0</span>건</h3>
            <span class="ui form inlien">
                <span class="ui pull-right">
                    <button type="button" id="onlineNotUseBtn" class="ui button small" onclick="ersPromo.massOnlineUseBtn('N');">일괄 사용안함</button>
                </span>
            </span>
        </div>
        <div id="ersPromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 점포행사 상세 정보 영역 --%>
    <form id="ersPromoForm" name="ersPromoForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>기본정보</caption>
            <colgroup>
                <col width="8%">
                <col width="12%">
                <col width="8%">
                <col width="12%">
                <col width="8%">
                <col width="12%">
                <col width="8%">
                <col width="12%">
            </colgroup>
            <tbody>
                <tr>
                    <th>사은행사<br>번호</th>
                    <td>
                        <span class="text" id="eventCd"></span>
                    </td>
                    <th>사은행사명</th>
                    <td>
                        <span class="text" id="eventNm"></span>
                    </td>
                    <th>구매기준<br>유형</th>
                    <td>
                        <span class="text" id="eventTargetType"></span>
                    </td>
                    <th>지급기준<br>유형</th>
                    <td>
                        <span class="text" id="eventGiveType"></span>
                    </td>
                </tr>
                <tr>
                    <th>영수증<br>합산여부</th>
                    <td>
                        <span class="text" id="receiptSumYn"></span>
                    </td>
                    <th>테넌트<br>참여여부</th>
                    <td>
                        <span class="text" id="tenantYn"></span>
                    </td>
                    <th>연속식<br>적용여부</th>
                    <td>
                        <span class="text" id="multipleYn"></span>
                    </td>
                    <th>상품권금액<br>출력여부</th>
                    <td>
                        <span class="text" id="ticketAmtPrtYn"></span>
                    </td>
                </tr>
                <tr>
                    <th>고객정보<br>출력여부</th>
                    <td>
                        <span class="text" id="customerInfoPrtYn"></span>
                    </td>
                    <th>지급구분</th>
                    <td>
                        <span class="text" id="saleType"></span>
                    </td>
                    <th>주관부서명</th>
                    <td>
                        <span class="text" id="partName"></span>
                    </td>

                </tr>
                <tr>
                    <th>행사기간</th>
                    <td>
                        <span class="text" id="eventDate"></span>
                    </td>
                    <th>증정기간</th>
                    <td>
                        <span class="text" id="giveDate"></span>
                    </td>
                    <th>반환 기간</th>
                    <td>
                        <span class="text" id="returnDate"></span>
                    </td>
                </tr>
                <tr>
                    <th>확정여부</th>
                    <td>
                        <span class="text" id="confirmYn"></span>
                    </td>
                    <th>확정일시</th>
                    <td>
                        <span class="text" id="confirmDt"></span>
                    </td>
                    <th>확정사용자</th>
                    <td>
                        <span class="text" id="confirmUser"></span>
                    </td>
                </tr>
                <tr>
                    <th>취소구분</th>
                    <td>
                        <span class="text" id="cancelFlag"></span>
                    </td>
                    <th>취소일시</th>
                    <td>
                        <span class="text" id="cancelDt"></span>
                    </td>
                    <th>취소등록자</th>
                    <td>
                        <span class="text" id="cancelUser"></span>
                    </td>
                    <th>재확정여부</th>
                    <td>
                        <span class="text" id="reconfirmYn"></span>
                    </td>
                </tr>
                <tr>

                    <th>펀딩비율</th>
                    <td>
                        <span class="text" id="fundRate"></span>
                    </td>
                    <th>할인여부</th>
                    <td>
                        <span class="text" id="dcYn"></span>
                    </td>
                    <th>비자 rf<br>행사 구분</th>
                    <td>
                        <span class="text" id="visaRfEventFlag"></span>
                    </td>
                    <th>영수증문구</th>
                    <td>
                        <span class="text" id="receiptPrtTitle"></span>
                    </td>
                </tr>
                <tr>
                    <th>영수증출력<br>메세지1</th>
                    <td>
                        <span class="text" id="receiptMsg1"></span>
                    </td>
                    <th>영수증출력<br>메세지2</th>
                    <td>
                        <span class="text" id="receiptMsg2"></span>
                    </td>
                    <th>영수증출력<br>메세지3</th>
                    <td>
                        <span class="text" id="receiptMsg3"></span>
                    </td>
                    <th>영수증출력<br>메세지4</th>
                    <td>
                        <span class="text" id="receiptMsg4"></span>
                    </td>
                </tr>
                <tr>
                    <th>영수증출력<br>메세지5</th>
                    <td>
                        <span class="text" id="receiptMsg5"></span>
                    </td>
                    <th>영수증출력<br>메세지6</th>
                    <td>
                        <span class="text" id="receiptMsg6"></span>
                    </td>
                    <th>영수증출력<br>메세지7</th>
                    <td>
                        <span class="text" id="receiptMsg7"></span>
                    </td>
                    <th>영수증출력<br>메세지8</th>
                    <td>
                        <span class="text" id="receiptMsg8"></span>
                    </td>
                </tr>
                <tr>
                    <th>영수증출력<br>메세지9</th>
                    <td>
                        <span class="text" id="receiptMsg9"></span>
                    </td>
                    <th>영수증출력<br>메세지10</th>
                    <td>
                        <span class="text" id="receiptMsg10"></span>
                    </td>
                    <th>영수증출력<br>메세지11</th>
                    <td>
                        <span class="text" id="receiptMsg11"></span>
                    </td>
                    <th>영수증출력<br>메세지12</th>
                    <td>
                        <span class="text" id="receiptMsg12"></span>
                    </td>
                </tr>
                <tr>
                    <th>영수증출력<br>메세지13</th>
                    <td>
                        <span class="text" id="receiptMsg13"></span>
                    </td>
                    <th>영수증출력<br>메세지14</th>
                    <td>
                        <span class="text" id="receiptMsg14"></span>
                    </td>
                    <th>영수증출력<br>메세지15</th>
                    <td>
                        <span class="text" id="receiptMsg15"></span>
                    </td>
                    <th>온라인<br>사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 150px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">구매 가이드</th>
                    <td colspan="7" class="ui form inline">
                        <textarea id="purchaseGuide" name="purchaseGuide" class="ui input mg-r-5" style="float:left;width: 50%; height: 90px;"  minlength="10" maxlength="150"placeholder="10자 이상 필수등록입니다."></textarea>
                        <span class="text" style="vertical-align: bottom;"><span id="purchaseGuideLen"></span> / 150자</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title" style="float:none;">• 구간 행사 정보</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="intervalCount">0</span>건</h3>
        </div>
        <div id="ersPromoIntervalGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: left;">
        <h3 class="title" style="float:none;">• 적용 점포</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="storeCount">0</span>건</h3>
        </div>
        <div id="ersPromoStoreGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: right;">
        <h3 class="title" style="float:none;">• 적용 상품</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="itemCount">0</span>건</h3>
        </div>
        <div id="ersPromoItemGrid" style="width: 100%; height: 250px;"></div>
    </div>

<%--    <div class="com wrap-title sub" style="width: 49.8%; height: 300px; float: right;">--%>
<%--        <h3 class="title" style="float:none;">* 적용 카드</h3>--%>
<%--        <div class="topline"></div>--%>
<%--        <div class="com wrap-title sub">--%>
<%--            <h3>검색결과 : <span id="cardCount">0</span>건</h3>--%>
<%--        </div>--%>
<%--        <div id="ersPromoCardGrid" class="mg-b-20" style="width: 100%; height: 250px;"></div>--%>
<%--    </div>--%>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setEventBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/ersPromoMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/ersPromoMain.grid.js?${fileVersion}"></script>

<script>
    let eventTargetTypeJson = ${eventTargetTypeJson};
    let eventGiveTypeJson   = ${eventGiveTypeJson};
    let applyFlagJson       = ${applyFlagJson};
    let applyYnJson         = ${applyYnJson};
    let saleTypeJson        = ${saleTypeJson};
    let confirmYnJson       = ${confirmYnJson};
    let giftGiveTypeJson    = ${giftGiveTypeJson};
    let useYnJson           = ${useYnJson};

	// 사은행사 조회 그리드 정보
    ${ersPromoGridBaseInfo}

    // 사은행사 할인 구간 정보 그리드
    ${ersPromoIntervalGridBaseInfo}

    // 사은행사 적용점포 그리드
    ${ersPromoStoreGridBaseInfo}

    // 사은행사 적용 상품 그리드
    ${ersPromoItemGridBaseInfo}

    <%--// 사은행사 적용 카드 상품 그리드--%>
    <%--${ersPromoCardGridBaseInfo}--%>
</script>
