<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
    .promo-preview-image {
        position: relative;
        display: inline-block;
        min-width: 110px;
        height: 132px;
        padding: 3px;
        vertical-align: top;
        background-color: #eef3fb;
    }
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">전문관 전시 관리</h2>
    </div>

    <%-- 전문관 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>전문관 검색</caption>
                    <colgroup>
                        <col width="12%">
                        <col width="*">
                        <col width="12%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="specialZone.calendar.initCalendarDate(specialZoneSearch, 'schStartDt', 'schEndDt', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="specialZone.calendar.initCalendarDate(specialZoneSearch, 'schStartDt', 'schEndDt', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="specialZone.calendar.initCalendarDate(specialZoneSearch, 'schStartDt', 'schEndDt', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="specialZone.calendar.initCalendarDate(specialZoneSearch, 'schStartDt', 'schEndDt', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tr>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 100px;">
                                    <option value="BANNERNM">배너명</option>
                                    <option value="SPECIALNO">전문관번호</option>
                                    <option value="REGNM">등록자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="specialTotalCnt">0</span>건</h3>
        </div>
        <div id="specialZoneGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 전문관 상세 정보 영역 --%>
    <form id="specialZoneForm" name="specialZoneForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 배너 등록/수정</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">배너 명</th>
                    <td class="ui form inline">
                        <input type="text" id="bannerNm" name="bannerNm" class="ui input medium mg-r-5" minlength="2" maxlength="30" style="width: 250px;text-align: left"><span class="text"><span id="bannerNmLen"></span> / 30자</span>
                    </td>
                    <th>우선순위</th>
                    <td>
                        <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="3">
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">사용여부</th>
                    <td colspan="3">
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 120px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">기획전 링크</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="promoNo" name="promoNo" class="ui input medium" style="width:120px;" readonly>
                            <input type="text" id="promoNm" name="promoNm" class="ui input medium mg-l-5" style="width:350px;" readonly>
                            <button type="button" id="promoPopBtn" class="ui button small gray-dark font-malgun mg-l-10 promoPopBtn">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">배너<br>(PC)</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="pcImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="pcImg" class="uploadType promoImg" data-type="PC" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="530"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="270"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="pcImgRegBtn" class="ui button medium" onclick="specialZone.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 530 x 270<br>
                                파일 : JPG, JPEG, PNG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 45자 이내
                            </div>
                        </div>
                    </td>
                    <th class="text-red-star">배너<br>(모바일)</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="mobileImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="mobileImg" class="uploadType promoImg" data-type="MOBILE" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="750"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="360"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="mobileImgRegBtn" class="ui button medium" onclick="specialZone.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 750 x 360<br>
                                파일 : JPG, JPEG, PNG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 45자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" id="imgFile" name="fileArr" multiple style="display: none;"/>

<script>
    var hmpImgUrl   = "${hmpImgUrl}";

	// 전문관 조회 그리드 정보
    ${specialZoneGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/specialZone.js?${fileVersion}"></script>
