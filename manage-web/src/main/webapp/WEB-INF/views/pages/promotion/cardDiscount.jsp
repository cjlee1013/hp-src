<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="">
    <!-- 점포선택 폼 -->
    <div style="display:none;" class="storePop" >
        <form id="storeSelectPopForm" name="storeSelectPopForm" method="post" action="/common/popup/storeSelectPop">
            <input type="text" name="storeTypePop" value="HYPER"/>
            <input type="text" name="storeTypeRadioYn" value="N"/>
            <input type="text" name="setYn" value="N"/>
            <input type="text" name="storeList" value=""/>
            <input type="text" name="callBackScript" value="cardDiscount.storePopupCallback"/>
        </form>
    </div>
    <div style="display:none;" class="cardPop" >
        <form id="cardSelectPopForm" name="cardSelectPopForm" method="post" action="/common/popup/cardPop">
            <input type="text" name="multiYn" value="Y"/>
            <input type="text" name="setYn" value="N"/>
            <input type="text" name="siteType" value="HOME"/>
            <input type="text" name="paymentList" value=""/>
            <input type="text" name="paymentMethodType" value="CARD"/>
            <input type="text" name="groupCdNeedYn" value="Y"/>
            <input type="text" name="callBackScript" value="cardDiscount.cardPopupCallback"/>
        </form>
    </div>
    <!-- 검색영역 -->
    <div class="">
        <form id="cardDiscountSearchForm" name="cardDiscountSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">카드상품할인 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>할인 검색</caption>
                    <colgroup>
                        <col width="140px">
                        <col width="700px">
                        <col width="120px">
                        <col width="250px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="regDt" selected>등록일</option>
                                    <option value="issueDt">적용기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button medium mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button medium mg-r-5">1주일전</button>
                                <button type="button" id="setOneMonthBtn" class="ui button medium mg-r-5">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="button" id="getCardDiscountSearchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="initCardDiscountSearchBtn" class="ui button large mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadSearchListBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id='schStoreType' name='schStoreType' class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="">전체</option>
                                    <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                        <c:if test="${storeType.ref3 ne 'VIRTUAL'}">
                                            <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="">전체</option>
                                    <c:forEach var="useYnCd" items="${useYn}" varStatus="status">
                                        <option value="${useYnCd.mcCd}">${useYnCd.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id='schType' name='schType' class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="discountNo">할인번호</option>
                                    <option value="discountNm">할인명</option>
                                    <option value="itemNo">상품번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 580px;" minlength="2" onKeypress="javascript:if (event.keyCode == 13) {cardDiscountSearch.getCardDiscountSearch();}">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="cardDiscountSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="cardDiscountGrid" style="width: 100%; height: 323px;"></div>
        <div class="topline"></div>
    </div>
    <!-- 검색영역 -->

    <!-- 상세영역 -->
    <form id="cardDiscountForm" name="cardDiscountForm" onsubmit="return false;">
        <input type="hidden" id="chgYn" name="chgYn" value="">
        <input type="hidden" id="discountKind" name="discountKind" value="CARD">
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline"><h3 class="title">할인 정보</h3></div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>할인정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="300px">
                        <col width="120px">
                        <col width="300px">
                        <col width="120px">
                        <col width="200px">
                        <col width="*">
                    </colgroup>

                    <tbody>
                    <tr>
                        <th>할인번호</th>
                        <td>
                            <input type="text" name="discountNo" id="discountNo" class="ui input medium" style="width: 100px;" isUpdate="N" readonly="true">
                        </td>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id='storeType' name='storeType' class="ui input medium mg-r-10" style="width: 120px" isUpdate="N">
                                    <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                        <c:if test="${storeType.ref3 ne 'VIRTUAL'}">
                                            <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부<span class="text-center text-red"> *</span></th>
                        <td>
                            <div class="ui form inline">
                                <select id="useYn" name="useYn" class="ui input medium mg-r-10" style="width: 120px" isUpdate="Y">
                                    <c:forEach var="useYnCd" items="${useYn}" varStatus="status">
                                        <option value="${useYnCd.mcCd}">${useYnCd.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>관리할인명<span class="text-center text-red"> *</span></th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" name="discountNm" id="discountNm" class="ui input medium" style="width: 700px;" isUpdate="Y" maxlength="60">
                                <span class="text"><span style="color:red;" id="discountNmLen">0</span> / 60자</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시할인명<span class="text-center text-red"> *</span></th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" name="displayDiscountNm" id="displayDiscountNm" class="ui input medium" style="width: 700px;" isUpdate="Y" maxlength="60">
                                <span class="text"><span style="color:red;" id="displayDiscountNmLen">0</span> / 60자</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>적용기간<span class="text-center text-red"> *</span></th>
                        <td colspan="5">
                            <div class="ui form inline" id="issueDtDiv">
                                <input type="text" id="issueStartDt" name="issueStartDt" class="ui input medium mg-r-5" placeholder="적용기간 시작일" style="width:120px;" isUpdate="N">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="issueEndDt" name="issueEndDt" class="ui input medium mg-r-5" placeholder="적용기간 종료일" style="width:120px;" isUpdate="N">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- 상세영역 -->
    </form>
    <!-- 적용정보 -->
    <div class="com wrap-title">
        <div id="section2">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline">
                    <h3 id="applyInfoDiv" class="title">적용 정보</h3>
                </div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>적용 정보</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포설정</th>
                        <td>
                            <div class="ui form inline">
                                <input type="hidden" id="storeList" name="storeList">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-10" id="storeInfoBtn">점포선택</button><span class="text">(적용 점포 수 : <span id="storeCnt">0</span>개)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>할인카드</th>
                        <td>
                            <div class="ui form inline">
                                <input type="hidden" id="cardList" name="cardList">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-10" id="cardInfoBtn">카드선택</button><span class="text">(적용 카드 수 : <span id="cardCnt">0</span>개)</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- 적용정보 -->
    <!-- 적용 상품정보 -->
    <div class="com wrap-title">
        <div id="section1">
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline">
                    <h3 id="applyScopeNmDiv" class="title">적용 상품 정보</h3>
                </div>
                <div style="float: right; display: none;">
                    <span>※ 최대 등록 가능 상품 수 : 50,000개</span>
                </div>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>검색</caption>
                    <colgroup>
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="discountApplyGridSearchType" name="discountApplyGridSearchType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="applyCd">상품번호</option>
                                    <option value="applyNm">상품명</option>
                                </select>
                                <input type="text" id="discountApplyGridSearchValue" name="discountApplyGridSearchValue" class="ui input medium mg-r-5"
                                       style="width: 600px;" onkeypress="javascript:if (event.keyCode == 13) { cardDiscount.discountApplyGridSearch(); }">
                                <button type="button" id="discountApplyGridSearchBtn" class="ui button medium cyan" style="margin-right : 5px">검색</button>
                                <button type="button" id="discountApplyGridSearchInitBtn" class="ui button medium">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <div class="ui form inline">
                    <h3 class="title">적용 상품 목록</h3><span class="title mg-l-5" id="applyScopeGridCount"></span><span class="title" id="applyScopeCnt"></span>
                    <button type="button" class="ui button small mg-l-5 pull-right" id="setGroupCdBtn" style="">입력</button>
                    <input type="text" id="groupCd" name="groupCd" class="ui input medium mg--5 pull-right" style="width: 50px;" maxlength="2" isUpdate="N">
                    <span class="text ui small mg-l-10 mg-r-5 pull-right">| 카드그룹코드</span>
                    <button type="button" class="ui button small mg-l-5 pull-right" id="deleteApplyScope">삭제</button>
                    <button type="button" class="ui button small mg-l-5 pull-right" id="popupApplySearch" style="display: none">상품등록</button>
                    <button type="button" class="ui button small mg-l-5 pull-right" id="popupApplyAllSearch">일괄등록</button>
                    <button type="button" class="ui button small mg-l-5 pull-right" id="applyGridExcelDownloadBtn">엑셀다운</button>
                </div>
            </div>
            <div id="discountApplyGrid" style="width: 100%; height: 300px;"></div>
        </div>
    </div>
    <!-- 적용정보 -->

    <div class="ui mg-t-30">
        <table class="ui">
            <colgroup>
                <col width="300px">
                <col width="*">
                <col width="300px">
            </colgroup>
            <tbody>
            <tr>
                <td>
                    <button type="button" class="ui button xlarge dark-blue" id="copyCardDiscount" style="display: none;">복사</button>
                </td>
                <td style="text-align: center;">
                    <button type="button" class="ui button xlarge cyan" id="setCardDiscount">등록/수정</button>
                    <button type="button" class="ui button xlarge" id="initCardDiscountAll">초기화</button>
                </td>
                <td style="text-align: right"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    var storeTypeJson = ${storeTypeJson};
    var cardDiscountTypeJson = ${cardDiscountTypeJson};
    var cardDiscountShareYnJson = ${cardDiscountShareYnJson};
    var useYnJson = ${useYnJson};

    // 즉시할인 리얼그리드 기본 정보
    ${cardDiscountGridBaseInfo}
    // 즉시할인 적용대상 리얼그리드 기본 정보
    ${cardDiscountApplyGridBaseInfo}
</script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/cardDiscount.js?v=${fileVersion}"></script>