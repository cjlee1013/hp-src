<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
    .promo-preview-image {
        position: relative;
        display: inline-block;
        min-width: 110px;
        height: 132px;
        padding: 3px;
        vertical-align: top;
        background-color: #eef3fb;
    }
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">기획전 관리</h2>
    </div>

    <%-- 기획전 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="exhPromoSearchForm" name="exhPromoSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>기획전 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="initCalendarDate(exhPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '-14d', '0');">14일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(exhPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(exhPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '0', '1m');">1개월후</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="9">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan"><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <c:forEach var="siteType" items="${siteType}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${siteType.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input" value="${siteType.mcCd}" ${checked}/><span>${siteType.mcNm}</span></label>
                            </c:forEach>
                        </td>
                        <th>적용시스템</th>
                        <td>
                            <select id="schDevice" name="schDevice" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="promoDevice" items="${promoDevice}" varStatus="status">
                                    <option value="${promoDevice.mcCd}">${promoDevice.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                        <th>상품 점포유형</th>
                        <td>
                            <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                    <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>쿠폰 사용여부</th>
                        <td>
                            <select id="schCouponUseYn" name="schCouponUseYn" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                        <th>전시여부</th>
                        <td>
                            <select id="schDispYn" name="schDispYn" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                    <option value="${dispYn.mcCd}">${dispYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 120px;">
                                    <option value="PROMONO">기획전번호</option>
                                    <option value="MNGPROMONM">관리기획전명</option>
                                    <option value="DISPPROMONM">전시기획전명</option>
                                    <option value="REGNM">등록자</option>
                                    <option value="CHGNM">수정자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="exhTotalCnt">0</span>건</h3>
        </div>
        <div id="exhPromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 기획전 상세 정보 영역 --%>
    <form id="exhPromoForm" name="exhPromoForm" onsubmit="return false;">
        <input type="hidden" id="promoType" name="promoType" value="EXH"/>
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th>기획전 번호</th>
                    <td>
                        <span id="promoNo"></span>
                    </td>
                    <th class="text-red-star">관리기획전명</th>
                    <td class="ui form inline">
                        <input type="text" id="mngPromoNm" name="mngPromoNm" class="ui input medium mg-r-5" minlength="2" maxlength="30" style="width: 60%;text-align: left"><span class="text"><span id="mngPromoNmLen"></span> / 30자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사이트구분</th>
                    <td>
                        <div class="ui form inline">
                            <select id="siteType" name="siteType" class="ui input medium mg-r-10" style="width: 120px;">
                                <c:forEach var="siteType" items="${siteType}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${siteType.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${siteType.mcCd}" ${selected}>${siteType.mcNm}</option>
                                </c:forEach>
                            </select>

                            <label id="siteTypeLb" class="ui checkbox">
                                <input type="checkbox" id ="expYn" name="expYn" value="Y"><span class="text">EXPRESS</span>
                            </label>
                            <span id="expYnSpan" class="text">(EXPRESS)</span>
                        </div>
                    </td>
                    <th class="text-red-star">적용시스템</th>
                    <td>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="pcUseYn" name="pcUseYn" value="Y" checked><span>PC</span>
                        </label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="appUseYn" name="appUseYn" value="Y" checked><span>APP</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="mwebUseYn" name="mwebUseYn" value="Y" checked><span>M.Web</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>전시여부</th>
                    <td>
                        <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${dispYn.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="dispYn" class="ui input" value="${dispYn.mcCd}" ${checked}/><span>${dispYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th>사용여부</th>
                    <td>
                        <c:forEach var="useYn" items="${useYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${useYn.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input" value="${useYn.mcCd}" ${checked}/><span>${useYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시기획전명</th>
                    <td class="ui form inline">
                        <input type="text" id="dispPromoNm" name="dispPromoNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 60%;text-align: left"><span class="text"><span id="dispPromoNmLen"></span> / 20자</span>
                    </td>
                    <th>앱상단타이틀</th>
                    <td class="ui form inline">
                        <input type="text" id="appTopTitle" name="appTopTitle" class="ui input medium mg-r-5" maxlength="8" style="width: 40%;text-align: left"><span class="text"><span id="topTitleLen"></span> / 8자</span>
                    </td>
                </tr>
                <tr>
                    <th>기획전 노출영역</th>
                    <td>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="dispMainYn" name="dispMainYn" value="Y" checked><span>혜택존</span>
                        </label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="dispCategoryYn" name="dispCategoryYn" value="Y"><span>카테고리기획전</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="dispSearchYn" name="dispSearchYn" value="Y"><span>검색결과</span>
                        </label>
                    </td>
                    <th>혜택존 기획전<br>노출 우선순위</th>
                    <td>
                        <input type="text" id="mainPriority" name="mainPriority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="2">
                    </td>
                </tr>
                <tr>
                    <th>관련카테고리</th>
                    <td style="height:70px;">
                        <div class="ui form inline mg-b-5">
                            <button type="button" id="getCateBtn" class="ui button medium gray-dark font-malgun mg-r-10" onclick="exhPromo.popup.openPopup('CATE');">등록</button><span class="text" style="vertical-align: bottom">(등록수 : <span id="getCateCnt"></span> / 30개)</span>
                        </div>
                        <span id="selectedCates"></span>
                    </td>
                    <th>카테고리 기획전<br>노출 우선순위</th>
                    <td>
                        <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="1">
                    </td>
                </tr>
                <tr>
                    <th>검색키워드</th>
                    <td>
                        <div class="ui form inline">
                            <textarea id="searchKeyword" name="searchKeyword" class="ui input mg-r-5" style="float:left;width: 60%; height: 60px;" maxlength="255"></textarea>
                            <span class="text" style="vertical-align: bottom;"><span id="keywordLen"></span> / 255자</span>
                        </div>
                    </td>
                    <th>검색키워드<br>노출 우선순위</th>
                    <td>
                        <input type="text" id="searchPriority" name="searchPriority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="1">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 배너이미지</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>배너이미지</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">혜택존 PC</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="benefitPCImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="benefitPCImg" class="uploadType promoImg" data-type="PC_BENEFIT" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="380"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="190"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="BENEFIT"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="benefitPCImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 380 x 190<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                    <th class="text-red-star">혜택존<br>MWeb/App</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="benefitMOImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="benefitMOImg" class="uploadType promoImg" data-type="MO_BENEFIT" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="670"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="340"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="BENEFIT"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="benefitMOImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 670 x 340<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>카테고리<br>기획전<br>PC</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="catePCImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="catePCImg" class="uploadType promoImg" data-type="PC_CATE" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="460"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="230"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="CATE"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="catePCImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 460 x 230<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                    <th>카테고리<br>기획전<br>MWeb/App</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="cateMOImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="cateMOImg" class="uploadType promoImg" data-type="MO_CATE" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="670"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="140"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="CATE"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="cateMOImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 670 x 140<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색결과 PC</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="searchPCImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="searchPCImg" class="uploadType promoImg" data-type="PC_SEARCH" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="940"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="130"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="SEARCH"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="searchPCImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 940 x 130<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                    <th>검색결과<br>MWeb/App</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="searchMOImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="searchMOImg" class="uploadType promoImg" data-type="MO_SEARCH" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="670"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="140"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="SEARCH"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="searchMOImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 670 x 140<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 상단영역</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>상단영역</caption>
                <colgroup>
                    <col width="12%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>PC</th>
                    <td>
                        <div style="padding-bottom: 10px">
                            <c:forEach var="topType" items="${topType}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${topType.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline"><input type="radio" name="pcType" class="ui input" data-type="pc" value="${topType.mcCd}" ${checked}/><span>${topType.mcNm}</span></label>
                            </c:forEach>
                        </div><br>
                        <div id="pcImgDiv">
                            <div class="text-center preview-image" style="width:235px; height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="pcImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="topPCImg" class="uploadType promoImg" data-type="PC_TOP" style="cursor:hand;">
                                        <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                        <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value=""/>
                                        <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value=""/>
                                        <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                        <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="TOP"/>
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="pcTopImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                                </div>
                            </div>
                            <div class="text-center promo-preview-image imageGuideLayer">
                                <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                    최적화 가이드<br><br>
                                    사이즈 : 1200 x 제한없음<br>
                                    파일 : JPG<br>
                                    용량 : 2MB 이하<br>
                                    파일명 : 30자 이내
                                </div>
                            </div>
                            <div id="bgColorDiv" class="ui form inline" style="position:relative; display:inline-block; top:50px;">
                                <span class="text">배경색상:</span><input type="color" id="bgColor" name="bgColor" class="mg-l-5" value="#FF0000">
                            </div>
                        </div>
                        <div id="pcEditDiv" style="display:none;">
                            <!-- 에디터 영역 -->
                            <div id="pcEditor" name="pcEditor"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>MWeb/APP</th>
                    <td>
                        <div style="padding-bottom: 10px">
                            <c:forEach var="topType" items="${topType}" varStatus="status">
                                <c:if test="${topType.ref2 ne 'PC'}">
                                    <c:set var="checked" value="" />
                                    <c:if test="${topType.ref1 eq 'df'}">
                                        <c:set var="checked" value="checked" />
                                    </c:if>
                                    <label class="ui radio inline"><input type="radio" name="mobileType" class="ui input" data-type="mobile" value="${topType.mcCd}" ${checked}/><span>${topType.mcNm}</span></label>
                                </c:if>
                            </c:forEach>
                        </div><br>
                        <div id="mobileImgDiv">
                            <div class="text-center preview-image" style="width:235px; height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="mobileImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="topMOImg" class="uploadType promoImg" data-type="MO_TOP" style="cursor:hand;">
                                        <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                        <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                        <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value=""/>
                                        <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value=""/>
                                        <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                        <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                        <input type="hidden" class="imgType" name="imgList[].imgType" value="TOP"/>
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="mobileTopImgRegBtn" class="ui button medium" onclick="exhPromo.img.clickFile($(this));">등록</button>
                                </div>
                            </div>
                            <div class="text-center promo-preview-image imageGuideLayer">
                                <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                    최적화 가이드<br><br>
                                    사이즈 : 750 x 제한없음<br>
                                    파일 : JPG<br>
                                    용량 : 2MB 이하<br>
                                    파일명 : 30자 이내
                                </div>
                            </div>
                        </div>
                        <div id="mobileEditDiv" style="display:none;">
                            <!-- 에디터 영역 -->
                            <div id="mobileEditor" name="mobileEditor"></div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 쿠폰영역</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>쿠폰</caption>
                <colgroup>
                    <col width="12%">
                    <col width="88%">
                </colgroup>
                <tbody>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <c:forEach var="useYn" items="${useYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${useYn.mcCd eq 'N'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="couponUseYn" class="ui input" value="${useYn.mcCd}" ${checked}/><span>${useYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <%-- 2020.10.12 - open spec에서 제거. 나중을 위해 주석처리함 --%>
                <tr style="display: none;">
                    <th>템플릿</th>
                    <td>
                        <select id="couponTemplate" name="couponTemplate" class="ui input medium mg-r-5" style="width:100px;float:left;">
                            <option value="1">1단</option>
                            <option value="2">2단</option>
                        </select>
                    </td>
                </tr>
                <tr id="couponAreaTr">
                    <th>쿠폰내역</th>
                    <td>
                        <span class="ui form inlien">
                            쿠폰 수 : <span id="couponCnt"></span> / 10 개
                            <span class="ui pull-right mg-b-10">
                                <button type="button" id="couponPopBtn" class="ui button small" onclick="exhPromo.popup.openPopup('COUPON');">쿠폰등록</button>
                            </span>
                        </span>

                        <div class="mg-t-10 mg-b-15" id="couponGrid" style="width: 100%; height: 350px;"></div>

                        <span class="ui left-button-group">
                            <button type="button" key="top" class="ui button small mg-r-5" onclick="couponGrid.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="couponGrid.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="couponGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="couponGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                            <button type="button" class="ui button medium mg-r-5" onclick="couponGrid.removeCheckData();">선택삭제</button>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>쿠폰 주의사항</th>
                    <td>
                        <c:forEach var="useYn" items="${useYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${useYn.mcCd eq 'N'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="couponCautionYn" class="ui input" value="${useYn.mcCd}" ${checked}/><span>${useYn.mcNm}</span></label>
                        </c:forEach>
                        <div id="couponEditDiv" style="padding-top:10px;display:none;">
                            <!-- 에디터 영역 -->
                            <div id="couponEditor" name="couponEditor"></div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 상품영역</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>상품영역</caption>
                <colgroup>
                    <col width="10%">
                    <col width="90%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">상품분류</th>
                    <td>
                        분류 수 : <span id="themeCnt"></span> / 30 개
                        <div class="mg-t-10 mg-b-5" id="themeGrid" style="width: 100%; height: 350px;"></div>

                        <span class="ui left-button-group">
                            <button type="button" key="top" class="ui button small mg-r-5" onclick="themeGrid.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="themeGrid.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="themeGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="themeGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                            <button type="button" class="ui button small mg-r-5" onclick="exhPromo.theme.setThemeInit();themeGrid.removeCheckData();">선택삭제</button>
                        </span>

                        <table id="themeArea" class="ui table-inner mg-t-10">
                            <caption>상품분류</caption>
                            <colgroup>
                                <col width="10%">
                                <col width="40%">
                                <col width="12%">
                                <col width="38%">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>분류명</th>
                                <td class="ui form inline">
                                    <input type="hidden" id="themeNo" name="themeNo">
                                    <input type="hidden" id="tempThemeNo" name="tempThemeNo">
                                    <input type="text" id="themeNm" name="themeNm" class="ui input mg-r-5" maxlength="20" style="width:60%;"><span class="text"><span id="themeNmLen"></span> / 20자</span>
                                </td>
                                <th>전시여부</th>
                                <td>
                                    <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                        <c:set var="checked" value="" />
                                        <c:if test="${dispYn.ref1 eq 'df'}">
                                            <c:set var="checked" value="checked" />
                                        </c:if>
                                        <label class="ui radio inline"><input type="radio" name="themeDispYn" class="ui input" value="${dispYn.mcCd}" ${checked}/><span>${dispYn.mcNm}</span></label>
                                    </c:forEach>
                                </td>
                            </tr>
                            <tr>
                                <th>점포유형</th>
                                <td>
                                    <select id="storeType" name="storeType" class="ui input medium" style="width: 100px;">
                                        <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                            <option value="${storeType.mcCd}" ${storeType.ref2}>${storeType.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <th>정렬방식</th>
                                <td>
                                    <select id="sortKind" name="sortKind" class="ui input medium" style="width: 100px;">
                                        <c:forEach var="sortKind" items="${sortKind}" varStatus="status">
                                            <option value="${sortKind.mcCd}">${sortKind.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-red-star">전시기간</th>
                                <td colspan="3">
                                    <div class="ui form inline">
                                        <input type="text" id="themeDispStartDt" name="themeDispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                                        <span class="text">&nbsp;~&nbsp;</span>
                                        <input type="text" id="themeDispEndDt" name="themeDispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <th>PC 템플릿</th>
                                <td>
                                    <select id="pcTemplate" name="pcTemplate" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                        <option value="1">1단</option>
                                        <option value="4">4단</option>
                                    </select>
                                </td>
                                <th>모바일 템플릿</th>
                                <td>
                                    <select id="mobileTemplate" name="mobileTemplate" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                        <option value="1">1단</option>
                                        <option value="2">2단</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>상품내역</th>
                                <td colspan="3">
                                    <span class="ui form inline">
                                        <span class="ui pull-left mg-b-5">
                                            <span class="text mg-r-30">상품 수 : <span id="itemCnt"></span>개</span>
                                            <span class="text">등록상품 검색 : </span>
                                            <input type="text" id="itemGridSearchValue" name="itemGridSearchValue" class="ui input medium mg-r-5" maxlength="15" style="width: 150px;">
                                            <button type="button" id="itemGridSearchBtn" class="ui button small">검색</button>
                                        </span>
                                        <span class="ui pull-right mg-b-5">
                                            <button type="button" id="itemPopBtn" class="ui button small mg-r-5" onclick="exhPromo.popup.openPopup('ITEM');">상품등록</button>
                                            <button type="button" id="itemExcelUpBtn" class="ui button small mg-r-5" onclick="exhPromo.popup.openPopup('EXCEL');">일괄등록</button>
                                            <button type="button" id="itemExcelDownBtn" class="ui button small mg-r-5" onclick="">엑셀다운로드</button>
                                        </span>
                                    </span>

                                    <div class="mg-t-5 mg-b-10" id="itemGrid" style="width: 100%; height: 350px;"></div>

                                    <span class="ui left-button-group">
                                        <button type="button" key="top" class="ui button small mg-t-20 mg-r-5" onclick="itemGrid.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                                        <button type="button" key="bottom" class="ui button small mg-t-20 mg-r-5" onclick="itemGrid.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                                        <button type="button" key="firstTop" class="ui button small mg-t-20 mg-r-5" onclick="itemGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                                        <button type="button" key="lastBottom" class="ui button small mg-t-20 mg-r-5" onclick="itemGrid.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                                        <button type="button" class="ui button small mg-t-20 mg-r-5" onclick="itemGrid.removeCheckData();">선택삭제</button>
                                    </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <span class="pull-right mg-t-10">
                            <button type="button" id="setThemeBtn" class="ui button medium gray-dark" onclick="exhPromo.theme.setThemeBtn();">추가</button>
                            <button type="button" class="ui button medium" onclick="exhPromo.theme.setThemeInit('BTN');">초기화</button>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" id="imgFile" name="fileArr" multiple style="display: none;"/>

<script>
    var useYnJson       = ${useYnJson};
    var dispYnJson      = ${dispYnJson};
    var storeTypeJson   = ${storeTypeJson};
    var couponTypeJson  = ${couponTypeJson};
    var siteTypeJson    = ${siteTypeJson};
    var hmpImgUrl       = "${hmpImgUrl}";
    var hmpImgFrontUrl  = "${hmpImgFrontUrl}";

	// 기획전 조회 그리드 정보
    ${exhGridBaseInfo}

    // 쿠폰 그리드 정보
    ${exhCouponGridBaseInfo}

    // 상품 분류 그리드 정보
    ${exhThemeGridBaseInfo}

    // 상품 그리드 정보
    ${exhItemGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/exhPromoMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/exhPromoMain.grid.js?${fileVersion}"></script>
