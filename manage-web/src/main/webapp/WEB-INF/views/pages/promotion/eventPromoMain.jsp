<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
    .promo-preview-image {
        position: relative;
        display: inline-block;
        min-width: 110px;
        height: 132px;
        padding: 3px;
        vertical-align: top;
        background-color: #eef3fb;
    }
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">이벤트 관리</h2>
    </div>

    <%-- 이벤트 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="eventPromoSearchForm" name="eventPromoSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>이벤트 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="initCalendarDate(eventPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '-14d', '0');">14일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(eventPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="initCalendarDate(eventPromoSearch, 'schStartDt', 'schEndDt', SEARCH_AREA, '0', '1m');">1개월후</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="9">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <c:forEach var="siteType" items="${siteType}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${siteType.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input" value="${siteType.mcCd}" ${checked}/><span>${siteType.mcNm}</span></label>
                            </c:forEach>
                        </td>
                        <th>적용시스템</th>
                        <td>
                            <select id="schDevice" name="schDevice" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="promoDevice" items="${promoDevice}" varStatus="status">
                                    <option value="${promoDevice.mcCd}">${promoDevice.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                        <th>전시여부</th>
                        <td>
                            <select id="schDispYn" name="schDispYn" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                    <option value="${dispYn.mcCd}">${dispYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 120px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 120px;">
                                    <option value="PROMONO">이벤트번호</option>
                                    <option value="MNGPROMONM">관리이벤트명</option>
                                    <option value="DISPPROMONM">전시이벤트명</option>
                                    <option value="REGNM">등록자</option>
                                    <option value="CHGNM">수정자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="eventTotalCnt">0</span>건</h3>
        </div>
        <div id="eventPromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 이벤트 상세 정보 영역 --%>
    <form id="eventPromoForm" name="eventPromoForm" onsubmit="return false;">
        <input type="hidden" id="promoType" name="promoType" value="EVENT"/>
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th>이벤트 번호</th>
                    <td>
                        <span id="promoNo"></span>
                    </td>
                    <th class="text-red-star">관리이벤트명</th>
                    <td class="ui form inline">
                        <input type="text" id="mngPromoNm" name="mngPromoNm" class="ui input medium mg-r-5" minlength="2" maxlength="30" style="width: 60%;text-align: left"><span class="text"><span id="mngPromoNmLen"></span> / 30자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사이트구분</th>
                    <td>
                        <c:forEach var="siteType" items="${siteType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${siteType.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="siteType" class="ui input" value="${siteType.mcCd}" ${checked}/><span>${siteType.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th class="text-red-star">적용시스템</th>
                    <td>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="pcUseYn" name="pcUseYn" value="Y" checked><span>PC</span>
                        </label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="appUseYn" name="appUseYn" value="Y" checked><span>APP</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="mwebUseYn" name="mwebUseYn" value="Y" checked><span>M.Web</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>전시여부</th>
                    <td>
                        <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${dispYn.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="dispYn" class="ui input" value="${dispYn.mcCd}" ${checked}/><span>${dispYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th>사용여부</th>
                    <td>
                        <c:forEach var="useYn" items="${useYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${useYn.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input" value="${useYn.mcCd}" ${checked}/><span>${useYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시이벤트명</th>
                    <td class="ui form inline">
                        <input type="text" id="dispPromoNm" name="dispPromoNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 60%;text-align: left"><span class="text"><span id="dispPromoNmLen"></span> / 20자</span>
                    </td>
                    <th>앱상단타이틀</th>
                    <td class="ui form inline">
                        <input type="text" id="appTopTitle" name="appTopTitle" class="ui input medium mg-r-5" maxlength="8" style="width: 40%;text-align: left"><span class="text"><span id="topTitleLen"></span> / 8자</span>
                    </td>
                </tr>
                <tr>
                    <th>이벤트 노출영역</th>
                    <td colspan="3">
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="dispSearchYn" name="dispSearchYn" value="Y"><span>검색결과</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>검색키워드</th>
                    <td>
                        <div class="ui form inline">
                            <textarea id="searchKeyword" name="searchKeyword" class="ui input mg-r-5" style="float:left;width: 60%; height: 60px;" maxlength="255"></textarea>
                            <span class="text" style="vertical-align: bottom;"><span id="keywordLen"></span> / 255자</span>
                        </div>
                    </td>
                    <th>검색키워드<br>노출 우선순위</th>
                    <td>
                        <input type="text" id="searchPriority" name="searchPriority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="1">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 배너이미지</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>배너이미지</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">혜택존 PC</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="benefitPCImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="benefitPCImg" class="uploadType promoImg" data-type="PC_BENEFIT" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="380"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="190"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="BENEFIT"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="benefitPCImgRegBtn" class="ui button medium" onclick="eventPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 380 x 190<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                    <th class="text-red-star">혜택존<br>MWeb/App</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="benefitMOImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="benefitMOImg" class="uploadType promoImg" data-type="MO_BENEFIT" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="670"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="340"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="BENEFIT"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="benefitMOImgRegBtn" class="ui button medium" onclick="eventPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 670 x 340<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색결과 PC</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="searchPCImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="searchPCImg" class="uploadType promoImg" data-type="PC_SEARCH" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="940"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="130"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="PC"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="SEARCH"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="searchPCImgRegBtn" class="ui button medium" onclick="eventPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 940 x 130<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                    <th>검색결과<br>MWeb/App</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="searchMOImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="searchMOImg" class="uploadType promoImg" data-type="MO_SEARCH" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value="670"/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value="140"/>
                                    <input type="hidden" class="changeYn" name="imgList[].changeYn" value="N"/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value="MOBILE"/>
                                    <input type="hidden" class="imgType" name="imgList[].imgType" value="SEARCH"/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="searchMOImgRegBtn" class="ui button medium" onclick="eventPromo.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center promo-preview-image imageGuideLayer">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : 670 x 140<br>
                                파일 : JPG<br>
                                용량 : 2MB 이하<br>
                                파일명 : 30자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 이벤트내용</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>이벤트내용</caption>
                <colgroup>
                    <col width="12%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>PC</th>
                    <td>
                        <div id="pcEditDiv">
                            <!-- 에디터 영역 -->
                            <div id="pcEditor" name="pcEditor"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>MWeb/APP</th>
                    <td>
                        <div id="mobileEditDiv">
                            <!-- 에디터 영역 -->
                            <div id="mobileEditor" name="mobileEditor"></div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" id="imgFile" name="fileArr" multiple style="display: none;"/>

<script>
    var useYnJson       = ${useYnJson};
    var dispYnJson      = ${dispYnJson};
    var siteTypeJson    = ${siteTypeJson};
    var hmpImgUrl       = "${hmpImgUrl}";
    var hmpImgFrontUrl  = "${hmpImgFrontUrl}";

	// 이벤트 조회 그리드 정보
    ${eventGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/eventPromoMain.js?${fileVersion}"></script>
