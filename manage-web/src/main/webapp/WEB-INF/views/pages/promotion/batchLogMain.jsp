<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">배치 모니터링</h2>
        <h3 class="subTitle"> - IDE 배치 정보를 관리하는 메뉴입니다. 실제 배치 구동에 영향을 주지 않습니다.</h3>
    </div>

    <%-- 배치 로그 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>이벤트 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>배치명</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width:250px;">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="float:left;width: 120px;">
                                <option value="">전체</option>
                                <option value="Y">사용중</option>
                                <option value="N">미사용</option>
                            </select>
                        </td>
                        <td>
                            <div style="text-align: right;">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="ideBatchTotalCnt">0</span>건</h3>
        </div>
        <div id="ideBatchInfoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 점포행사 상세 정보 영역 --%>
    <form id="batchLogForm" name="batchLogForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody>
                <tr>
                    <th>일련번호</th>
                    <td>
                        <span class="text" id="seq"></span>
                    </td>
                    <th class="text-red-star">배치명</th>
                    <td class="ui form inline">
                        <input type="text" id="batchNm" name="batchNm" class="ui input medium mg-r-5" minlength="2" maxlength="100" style="width: 50%;text-align: left"><span class="text"><span id="batchNmLen"></span> / 100자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">배치관리명</th>
                    <td class="ui form inline">
                        <input type="text" id="batchMngNm" name="batchMngNm" class="ui input medium mg-r-5" minlength="2" maxlength="30" style="width: 20%;text-align: left"><span class="text"><span id="batchMngNmLen"></span> / 30자</span>
                    </td>
                    <th rowspan="3">배치설명</th>
                    <td class="ui form inline" rowspan="3">
                        <textarea id="batchInfo" name="batchInfo" class="ui input mg-r-5" style="float:left;width: 50%; height: 100px;" maxlength="200"></textarea>
                        <span class="text"><span id="batchInfoLen"></span> / 200자</span>
                    </td>
                </tr>
                <tr>
                    <th>배치주기</th>
                    <td class="ui form inline">
                        <input type="text" id="batchSchedule" name="batchSchedule" class="ui input medium mg-r-5" minlength="2" maxlength="30" style="width: 20%;text-align: left">
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 150px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub" style="width: 100%; float: left;">
            <h3 class="title" style="float:none;">• 배치 로그</h3>
            <div class="topline"></div>
            <span class="ui form inlien">
                 <h3 class="mg-t-10">검색결과 : <span id="batchLogCnt">0</span>건
                    <span class="ui pull-right mg-b-10">
                        <button type="button" id="couponPopBtn" class="ui button small" onclick="batchLog.popup.openPopup();">배치로그상세</button>
                    </span>

                 </h3>
            </span>

            <div id="batchLogGrid" style="width: 100%; height: 320px;"></div>
        </div>

        <%-- 하단 버튼 영역 --%>
        <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
            <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
        </span>
        </div>
    </form>
</div>

<script>
    var batchStatusJson = ${batchStatusJson};

    // IDE TF batch 그리드 정보
    ${ideBatchInfoGridBaseInfo}

    // 배치 로그 조회 그리드 정보
    ${batchLogGridBaseInfo}

    // 배치 로그 상세 조회 그리드 정보
    ${batchLogInfoGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/batchLogMain.js?${fileVersion}"></script>