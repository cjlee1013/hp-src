<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">이벤트 참여이력 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <form id="searchForm" name="searchForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>이벤트 참여이력 관리</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="20%">
                        <col width="5%">
                        <col width="20%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">이벤트</th>
                        <td class="ui form inline">
                            <select id="eventCode" name="eventCode" class="ui input medium mg-r-10" style="width: 180px">
                                <option value="">선택</option>
                                <c:forEach var="eventInfo" items="${eventSelectBoxInfo}" varStatus="status">
                                    <option value="${eventInfo.eventCode}">${eventInfo.eventName}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th scope="col">회원검색</th>
                        <td class="ui form inline">
                            <input type="text" id="userNm" name="userNm" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                            <input type="hidden" id="searchUserNo" name="searchUserNo">
                            <button type="button" class="ui button medium mg-r-5" id="userSchButton"
                                    onclick="memberSearchPopup('callbackMemberSearchPop', true); return false;">
                                조회
                            </button>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-10">
                                <button type="submit" class="ui button large cyan" id="searchBtn">검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="initBtn">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 회원메모관리 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="gridCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="participationHistoryGrid" style="background-color:white;width:100%;height:150px;"></div>
    </div>
</div>

<script type="text/javascript" src="/static/js/promotion/participationHistoryMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${participationHistoryGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        participationHistoryMain.init();
        participationHistoryGrid.init();
    });
</script>