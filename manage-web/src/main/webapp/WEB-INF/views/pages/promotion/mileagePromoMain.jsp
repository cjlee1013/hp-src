<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 행사 관리</h2>
    </div>

    <%-- 마일리지 행사 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 행사 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="25%">
                        <col width="11%">
                        <col width="25%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="PROMODT">행사일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="mileagePromo.view.initCalendarDate(mileagePromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileagePromo.view.initCalendarDate(mileagePromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileagePromo.view.initCalendarDate(mileagePromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileagePromo.view.initCalendarDate(mileagePromoSearch, 'schStartDt', 'schEndDt', 'SEARCH', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tr>
                        <th>주관부서</th>
                        <td>
                            <select id="schDepartCd" name="schDepartCd" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="department" items="${department}" varStatus="status">
                                    <option value="${department.mcCd}">${department.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 100px;">
                                    <option value="MILEAGENM">행사명</option>
                                    <option value="MILEAGENO">행사번호</option>
                                    <option value="REGNM">등록자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="mileagePromoTotalCnt">0</span>건</h3>
        </div>
        <div id="mileagePromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 마일리지 행사 상세 정보 영역 --%>
    <form id="mileagePromoForm" name="mileagePromoForm" onsubmit="return false;">
        <input type="hidden" id="promoType" name="promoType" value="MILEAGE"/>
        <input type="hidden" id="siteType" name="siteType" value="HOME"/>
        <div class="com wrap-title sub">
            <span class="ui form inline">
                <h3 class="title">• 행사 상세</h3>

                <span class="ui pull-right mg-b-5">
                    <button type="button" id="searchErsPromoBtn" class="ui button small mg-r-5" onclick="">자동설정</button>
                </span>
            </span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">행사번호</th>
                    <td colspan="3">
                        <span id="mileageNo"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">행사 명</th>
                    <td class="ui form inline" colspan="3">
                        <input type="text" id="mileageMngNm" name="mileageMngNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 350px;text-align: left"><span class="text"><span id="mileageMngNmLen"></span> / 20자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">행사기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="promoStartDt" id="promoStartDt" class="ui input mg-r-5" style="width:140px;"/>
                            <span class="text mg-r-5 mg-l-5">&nbsp;&nbsp;~ &nbsp;&nbsp;</span>
                            <input type="text" name="promoEndDt" id="promoEndDt" class="ui input mg-r-5" style="width:140px;"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">적립방법</th>
                    <td>
                        <div class="ui form inline">
                            <select id="earnType" name="earnType" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:forEach var="earnType" items="${earnType}" varStatus="status">
                                    <option value="${earnType.mcCd}">${earnType.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="earnPriceSpan" style="display:none;">
                                <input type="text" id="earnAmount" name="earnAmount" class="ui input medium" style="width: 100px;text-align: right;"><span class="text">원</span>
                            </span>
                            <span id="earnPercentSpan">
                                    <input type="text" id="earnPercent" name="earnPercent" class="ui input medium" style="width: 80px;text-align: right;" maxlength="4"><span class="text">%</span>
                                    <span class="text" style="padding-left: 20px;">최대적립금액</span>
                                    <input type="text" id="earnAmountMax" name="earnAmountMax" class="ui input medium mg-r-5" style="width: 150px;text-align: right;" maxlength="9"><span class="text">원</span>
                            </span>
                        </div>
                    </td>
                    <th class="text-red-star">주관부서</th>
                    <td>
                        <select id="departCd" name="departCd" class="ui input medium" style="width: 100px;">
                            <option value="">선택</option>
                            <c:forEach var="department" items="${department}" varStatus="status">
                                <option value="${department.mcCd}">${department.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">마일리지<br>점포유형</th>
                    <td>
                        <div class="ui form inline">
                            <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 100px;">
                                <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                    <option value="${storeType.mcCd}" data-ref3="${storeType.ref3}" >${storeType.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span class="text">( 연동코드 : <span id="mileageCode"></span> )</span>
                        </div>
                    </td>
                    <th class="text-red-star">최소구매금액</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="purchaseMin" name="purchaseMin" class="ui input medium mg-r-5" style="width: 150px;text-align: right;" maxlength="8"><span class="text">원</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">점포설정</th>
                    <td>
                        <c:forEach var="dispStoreType" items="${dispStoreType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${dispStoreType.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="dispStoreType" class="ui input" value="${dispStoreType.mcCd}" ${checked}/><span>${dispStoreType.mcNm}</span></label>
                        </c:forEach>

                        <button type="button" id="storePopBtn" class="ui button small gray-dark font-malgun mg-l-10 mg-r-10">점포선택</button><span class="text">(적용 점포 수 : <span id="storeCnt">0</span>개)</span>
                    </td>
                    <th class="text-red-star">마일리지<br>유효기간</th>
                    <td class="ui form inline">
                        <span class="text"> 등록일로부터 <input type="text" id="expireDayInp" name="expireDayInp" class="ui input medium mg-l-5 mg-r-5" style="width:70px;"> 일</span>
                    </td>
                </tr>
                <tr>
                    <th>결제수단</th>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" id="cardList" name="cardList">
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-10" id="cardInfoBtn">카드선택</button><span class="text">(적용 카드 수 : <span id="cardCnt">0</span>개)</span>
                        </div>
                    </td>
                    <th class="text-red-star">사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 100px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">마일리지<br>적립사유</th>
                    <td class="ui form inline" colspan="3">
                        <input type="text" id="displayMessage" name="displayMessage" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 350px;text-align: left"><span class="text"><span id="displayMessageLen"></span> / 20자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">구매가이드</th>
                    <td>
                        <div class="ui form inline">
                            <textarea id="purchaseGuide" name="purchaseGuide" class="ui input mg-r-5" style="float:left;width: 60%; height: 60px;" maxlength="150"></textarea>
                            <span class="text" style="vertical-align: bottom;"><span id="purchaseGuideLen"></span> / 150자</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub" style="margin-top: 20px;">
            <div class="ui form inline">
                <h3 class="title mg-r-20">• 적용 상품 정보 : <span id="itemCnt">0</span>건</h3>
                <button type="button" class="ui button small mg-l-5 pull-right" id="itemDelBtn">삭제</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="itemExcelUpBtn">일괄등록</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="itemPopBtn">상품등록</button>
            </div>
        </div>
        <div id="mileageItemGrid" style="width: 100%; height: 320px;"></div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
</div>

<!-- 점포선택 폼 -->
<div style="display:none;" class="storePop" >
    <form id="storePopForm" name="storePopForm" method="post" action="/common/popup/storeSelectPop">
        <input type="text" name="storeTypePop" value="HYPER"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" name="setYn" value="Y"/>
        <input type="text" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="mileagePromo.popup.storePopCallback"/>
    </form>
</div>

<div style="display:none;" class="cardPop" >
    <form id="cardSelectPopForm" name="cardSelectPopForm" method="post" action="/common/popup/cardPop">
        <input type="text" name="multiYn" value="Y"/>
        <input type="text" name="setYn" value="N"/>
        <input type="text" name="siteType" value="HOME"/>
        <input type="text" name="paymentList" value=""/>
        <input type="text" name="paymentMethodType" value="CARD"/>
        <input type="text" name="groupCdNeedYn" value="N"/>
        <input type="text" name="callBackScript" value="mileagePromo.popup.cardPopCallback"/>
    </form>
</div>

<div style="display:none;" class="ersPromoPop" >
    <form id="ersPromoPopForm" name="ersPromoPopForm" method="get" action="/common/popup/ersPromoPop">
        <input type="text" name="isMulti" value="N"/>
        <input type="text" name="callBackScript" value="mileagePromo.popup.ersPopCallback"/>
    </form>
</div>

<script>
    var storeTypeJson= ${storeTypeJson};

	// 마일리지 행사 조회 그리드 정보
    ${mileagePromoGridBaseInfo}

	// 마일리지 행사 적용상품 그리드 정보
    ${mileageItemGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/mileagePromoMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/mileagePromoMain.grid.js?${fileVersion}"></script>
