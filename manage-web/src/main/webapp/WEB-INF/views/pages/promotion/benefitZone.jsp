<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">혜택존 전시 관리</h2>
    </div>

    <%-- 혜택존 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>혜택존 검색</caption>
                    <colgroup>
                        <col width="12%">
                        <col width="*">
                        <col width="12%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="benefitZone.calendar.initCalendarDate(benefitZoneSearch, 'schStartDt', 'schEndDt', '0', '0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="benefitZone.calendar.initCalendarDate(benefitZoneSearch, 'schStartDt', 'schEndDt', '-1d', '0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="benefitZone.calendar.initCalendarDate(benefitZoneSearch, 'schStartDt', 'schEndDt', '-1m', '0');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="benefitZone.calendar.initCalendarDate(benefitZoneSearch, 'schStartDt', 'schEndDt', '-3m', '0');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="5">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tr>
                        <th>전시여부</th>
                        <td>
                            <select id="schDispYn" name="schDispYn" class="ui input medium" style="width: 100px;">
                                <option value="">전체</option>
                                <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                    <option value="${dispYn.mcCd}">${dispYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 100px;">
                                    <option value="BANNERNM">배너명</option>
                                    <option value="BENEFITNO">혜택존번호</option>
                                    <option value="REGNM">등록자</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 350px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="benefitTotalCnt">0</span>건</h3>
        </div>
        <div id="benefitZoneGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 혜택존 상세 정보 영역 --%>
    <form id="benefitZoneForm" name="benefitZoneForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 배너 등록/수정</h3>
        </div>
        <div class="topline"></div>
        <span class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text-red-star">사이트구분</th>
                    <td>
                        <select id="siteType" name="siteType" class="ui input medium" style="width: 120px;">
                            <c:forEach var="siteType" items="${siteType}" varStatus="status">
                                <option value="${siteType.mcCd}">${siteType.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>디바이스</th>
                    <td>
                        <select id="deviceType" name="deviceType" class="ui input medium" style="width: 120px;">
                            <c:forEach var="deviceType" items="${deviceType}" varStatus="status">
                                <option value="${deviceType.mcCd}">${deviceType.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시위치</th>
                    <td>
                        <select id="dispLoc" name="dispLoc" class="ui input medium" style="width: 120px;">
                            <c:forEach var="dispLoc" items="${dispLoc}" varStatus="status">
                                <option value="${dispLoc.mcCd}">${dispLoc.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>전시순서</th>
                    <td>
                        <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 50px;text-align: right;" maxlength="3">
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">배너 명</th>
                    <td colspan="3" class="ui form inline">
                        <input type="text" id="bannerNm" name="bannerNm" class="ui input medium mg-r-5" minlength="2" maxlength="20" style="width: 350px;text-align: left"><span class="text"><span id="bannerNmLen"></span> / 20자</span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">전시여부</th>
                    <td colspan="3">
                        <select id="dispYn" name="dispYn" class="ui input medium" style="width: 120px;">
                            <c:forEach var="dispYn" items="${dispYn}" varStatus="status">
                                <option value="${dispYn.mcCd}">${dispYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">링크</th>
                    <td colspan="3">
                        <span class="ui form">
                            <select id="linkType" name="linkType" class="ui input medium" style="width: 120px;float: left;">
                                <c:forEach var="linkType" items="${linkType}" varStatus="status">
                                    <option value="${linkType.mcCd}">${linkType.mcNm}</option>
                                </c:forEach>
                            </select>

                            <div id="linkDiv">
                                <select id="storeType" name="storeType" class="ui input medium mg-l-5" style="width: 100px;float: left;">
                                    <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                        <option value="${storeType.mcCd}" ${storeType.ref2}>${storeType.mcNm}</option>
                                    </c:forEach>
                                </select>

                                <input type="text" id="linkNo" name="linkNo" class="ui input medium mg-l-10" style="width:120px;float: left;" readonly>
                                <input type="text" id="linkNm" name="linkNm" class="ui input medium mg-l-5" style="width:350px;float: left;" readonly>
                                <button type="button" id="getLinkInfoBtn" class="ui button small gray-dark font-malgun mg-l-10 getLinkInfoBtn" style="float: left;">검색</button>
                                <input type="text" id="urlPath" name="urlPath" class="ui input medium mg-l-10" style="display: none;width:500px;float: left;" placeholder="http://" maxlength="100">

                                <label id="storeChecklb" class="ui checkbox mg-l-10" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
                                    <input type="checkbox" id="dlvStoreYn" name="dlvStoreYn" class="dlvStoreYn" value ="Y"><span>택배점 설정</span>
                                    <select id="dlvStoreId" name="dlvStoreId" class="ui input medium dlvStoreList"  style="width: 120px;float: right;margin-left: 10px;">
                                        <option value="">택배점</option>
                                        <c:forEach var="dlvStoreId" items="${dlvStoreList}" varStatus="status">
                                            <option value="${dlvStoreId.storeId}">${dlvStoreId.storeNm}</option>
                                        </c:forEach>
                                    </select>
                                </label>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">배너</th>
                    <td>
                        <div class="text-center preview-image" style="width:235px; height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" id="bannerImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                <div id="bannerImg" class="uploadType promoImg" style="cursor:hand;">
                                    <img class="imgUrlTag" width="235px;" height="132px;" src="">
                                    <input type="hidden" class="imgUrl" name="imgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="imgList[].imgWidth" value=""/>
                                    <input type="hidden" class="imgHeight" name="imgList[].imgHeight" value=""/>
                                    <input type="hidden" class="deviceType" name="imgList[].deviceType" value=""/>
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" id="pcImgRegBtn" class="ui button medium" onclick="benefitZone.img.clickFile($(this));">등록</button>
                            </div>
                        </div>
                        <div class="text-center preview-image imageGuideLayer" style="height:132px; height:132px;">
                            <div class="pd-t-20 text-size-sm text-gray-Lightest">
                                최적화 가이드<br><br>
                                사이즈 : <span id="imgSapn">1200 X 450</span><br>
                                파일 : JPG, JPEG, PNG<br>
                                용량 : <span id="imgSizeSapn">5</span>MB 이하<br>
                                파일명 : 45자 이내
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" id="setBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
            <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
        </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" id="imgFile" name="fileArr" multiple style="display: none;"/>

<script>
    var hmpImgUrl   = "${hmpImgUrl}";
    var linkTypeJson= ${linkTypeJson};
    var dispLocJson = ${dispLocJson};
    var storeTypeJson   = ${storeTypeJson};
    var dlvStoreListJson= ${dlvStoreListJson};

	// 혜택존 조회 그리드 정보
    ${benefitZoneGridBaseInfo}
</script>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefitZone.js?${fileVersion}"></script>
