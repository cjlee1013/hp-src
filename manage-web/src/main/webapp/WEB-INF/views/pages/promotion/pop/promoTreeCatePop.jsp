<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp" />
<%--<script type="text/javascript" src="/static/js/product/commonCategory.js?v=${fileVersion}"></script>--%>
<style>
    table {width: auto; !important; border-spacing:0;border-collapse:collapse;}
</style>

<div class="com wrap-popup medium popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 검색</h2>
    </div>

    <div id="${treeCategoryPopName}" class="mg-t-20" style="overflow: scroll; width:100%; height:500px; background-color:white;"></div>

    <div class="ui form inline mg-t-15 text-center">
        <button type="button" id="schBtn" class="ui button xlarge btn-danger mg-r-10">선택</button>
        <button type="button" id="cancelBtn" class="ui button xlarge">취소</button>
    </div>
</div>

<script>
    var callBackScript      = "${callBackScript}";
    var tsChecked           = ${tsChecked};
    var transDataParentNode = ${transDataParentNode};
    var transDataChildNode  = ${transDataChildNode};
    var isAllCheck          = ${isAllCheck};
    var stepFlag            = "${stepFlag}";

    // dHtmlx 그리드 선언
    ${treeCategoryPopMake}
    ${treeCategoryPopName}Load();

    var treeCatePopGrid = ${treeCategoryPopName};
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/treeCategoryPop.js?v=${fileVersion}"></script>