<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">쿠폰 관리</h2>
    </div>
    <!-- 점포선택 폼 -->
    <div style="display:none;" class="storePop" >
        <form id="storeSelectPopForm" name="storeSelectPopForm" method="post" action="/common/popup/storeSelectPop">
            <input type="text" name="storeTypePop" value="HYPER"/>
            <input type="text" name="storeTypeRadioYn" value="N"/>
            <input type="text" name="setYn" value="Y"/>
            <input type="text" name="storeList" value=""/>
            <input type="text" name="callBackScript" value="coupon.storePopupCallback"/>
        </form>
    </div>
    <div style="display:none;" class="barcodePop" >
        <form id="barcodeSelectPopForm" name="barcodeSelectPopForm" method="post" action="/common/popup/barcodePop">
            <input type="text" name="setYn" value="N"/>
            <input type="text" name="storeType" value="HYPER"/>
            <input type="text" name="getBarcode" value=""/>
            <input type="text" name="coyn" id="coyn" value="">
            <input type="text" name="callBackScript" value="coupon.barcodePopupCallback"/>
        </form>
    </div>
    <div style="display:none;" class="cardPop" >
        <form id="cardSelectPopForm" name="cardSelectPopForm" method="post" action="/common/popup/cardPop">
            <input type="text" name="multiYn" value="Y"/>
            <input type="text" name="setYn" value="N"/>
            <input type="text" name="siteType" value="HOME"/>
            <input type="text" name="paymentList" value=""/>
            <input type="text" name="groupCdNeedYn" value="N"/>
            <input type="text" name="paymentMethodType" value="CARD"/>
            <input type="text" name="callBackScript" value="coupon.cardPopupCallback"/>
        </form>
    </div>
    <%-- 쿠폰 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="couponSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>쿠폰 검색</caption>
                    <colgroup>
                        <col width="11%">
                        <col width="17%">
                        <col width="11%">
                        <col width="17%">
                        <col width="11%">
                        <col width="17%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT" selected>등록일</option>
                                    <option value="ISSUEDT">발급기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="calendarInit.searchPeriod('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="calendarInit.searchPeriod('-1w');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="calendarInit.searchPeriod('-1m');">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="8">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                        <th>점포유형</th>
                        <td>
                            <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                    <c:if test="${storeType.ref3 ne 'VIRTUAL'}">
                                        <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <th>쿠폰종류</th>
                        <td>
                            <select id="schCouponType" name="schCouponType" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="couponType" items="${couponType}" varStatus="status">
                                    <option value="${couponType.mcCd}">${couponType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>등록자</th>
                        <td>
                            <input type="text" id="schRegNm" name="schRegNm" class="ui input medium " style="width: 180px;" maxlength="15">
                        </td>
                    <tr>
                    </tr>
                    <th>쿠폰상태</th>
                        <td>
                            <select id="schCouponStatus" name="schCouponStatus" class="ui input medium" style="width: 180px;">
                                <option value="" selected>전체</option>
                                <option value="TEMP">임시저장</option>
                                <option value="READY">발급대기</option>
                                <option value="ISSUED">발급중</option>
                                <option value="STOP">발급중단</option>
                                <option value="COMP">발급종료</option>
                            </select>
                        </td>
                        <th>행사목적</th>
                        <td>
                            <select id="schPurpose" name="schPurpose" class="ui input medium" style="width: 180px;">
                                <option value="" selected>전체</option>
                                <c:forEach var="couponPurpose" items="${couponPurpose}" varStatus="status">
                                    <option value="${couponPurpose.mcCd}">${couponPurpose.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>분담여부</th>
                        <td>
                            <select id="schShareYn" name="schShareYn" class="ui input medium" style="width: 180px;">
                                <option value="" selected>전체</option>
                                <c:forEach var="shareYn" items="${useYn}" varStatus="status">
                                    <option value="${shareYn.mcCd}">${shareYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>등급쿠폰</th>
                        <td colspan="3">
                            <select id="schGradeCouponYn" name="schGradeCouponYn" class="ui input medium" style="width: 180px;">
                                <option value="" selected>전체</option>
                                <c:forEach var="gradeCouponYn" items="${useYn}" varStatus="status">
                                    <option value="${gradeCouponYn.mcCd}">${gradeCouponYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>귀속부서</th>
                        <td>
                            <select id="schDepartCd" name="schDepartCd" class="ui input medium" style="width: 180px;">
                                <option value="" selected>전체</option>
                                <c:forEach var="cmsUnit" items="${couponCmsUnit}" varStatus="status">
                                    <option value="${cmsUnit.mcCd}">${cmsUnit.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 180px;">
                                    <option value="COUPONNM">관리쿠폰명</option>
                                    <option value="COUPONNO" selected>쿠폰번호</option>
                                    <option value="ITEMNO">상품번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="couponTotalCount">0</span>건</h3>
        </div>
        <div id="couponListGrid" style="width: 100%; height: 323px;"></div>
    </div>

    <%-- 쿠폰 상세 정보 영역 --%>
    <form id="couponForm" name="couponForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">• 쿠폰 정보</h3> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="text" style=""> ※ 상품할인은 자동 적용되며 나머지 쿠폰은 다운로드를 통해 발급됩니다.</span>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>쿠폰 정보</caption>
            <colgroup>
                <col width="140px">
                <col width="*">
                <col width="140px">
                <col width="*">
                <col width="140px">
                <col width="*">
                <col width="140px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th>쿠폰번호</th>
                    <td>
                        <input type="text" name="couponNo" id="couponNo" class="ui input medium" style="width: 100px;" readonly="true" isUpdate="true">
                    </td>
                    <th>쿠폰종류</th>
                    <td>
                        <select id="couponType" name="couponType" class="ui input medium" style="width: 150px;">
                            <c:forEach var="couponType" items="${couponType}" varStatus="status">
                                <option value="${couponType.mcCd}">${couponType.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>쿠폰상태</th>
                    <td>
                        <input type="hidden" id="status" name="status">
                        <span class="text" id="statusNm"></span>
                    </td>
                    <th>점포유형</th>
                    <td>
                        <select id="storeType" name="storeType" class="ui input medium" style="width: 150px;">
                            <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                <c:if test="${storeType.ref3 ne 'VIRTUAL'}">
                                    <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>관리쿠폰명<span class="text-center text-red"> *</span></th>
                    <td colspan="7">
                        <div class="ui form inline">
                            <input type="text" id="manageCouponNm" name="manageCouponNm" class="ui input medium mg-r-10" style="width: 70%;" maxlength="35" isUpdate="true">
                            <span class="text"><span style="color:red;" id="manageCouponNmLen">0</span> / 35자</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시쿠폰명<span class="text-center text-red"> *</span></th>
                    <td colspan="7">
                        <div class="ui form inline">
                            <input type="text" id="displayCouponNm" name="displayCouponNm" class="ui input medium mg-r-10" style="width: 70%;" maxlength="35" isUpdate="true">
                            <span class="text"><span style="color:red;" id="displayCouponNmLen">0</span> / 35자</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>귀속부서<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <select id="departCd" name="departCd" class="ui input medium" style="width: 180px;">
                            <c:forEach var="cmsUnit" items="${couponCmsUnit}" varStatus="status">
                                <option value="${cmsUnit.mcCd}">${cmsUnit.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>쿠폰바코드<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline" id="barcodeInputLine" style="display: none;">
                            <input type="text" id="barcode" name="barcode" class="ui input medium mg-r-10" style="width: 120px;" maxlength="13" noShip noItem noImme noDupl>
                            <input type="hidden" id="barcodeCouponSeq" name="barcodeCouponSeq" class="ui input medium mg-r-10" style="width: 180px;" maxlength="10" noShip noDupl>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" id="barcodePopBtn">조회</button>
                            <button type="button" class="ui button small blue" id="pullCmsProduct" style="display: none;">상품불러오기</button>
                        </div>
                        <select id="cartBarcode" name="cartBarcode" class="ui input medium" style="width: 200px;">
                            <c:forEach var="cartBarcode" items="${couponCartBarcode}" varStatus="status">
                                <option value="${cartBarcode.mcCd}" data-dept="${cartBarcode.ref1}">${cartBarcode.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>등급쿠폰</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="gradeCouponYn" name="gradeCouponYn" class="ui input medium" style="width: 100px;">
                                <c:forEach var="gradeCouponYn" items="${useYn}" varStatus="status">
                                    <option value="${gradeCouponYn.mcCd}" <c:if test="${gradeCouponYn.ref2 eq 'df'}">selected</c:if>>${gradeCouponYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span css="inline" id="targetMonth"><input type="text" id="gradeCouponMonth" name="gradeCouponMonth" class="ui input medium mg-l-5 picker-input" readonly="readonly"></span>
                        </div>
                    </td>
                    <th>등급설정</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="userGradeSeq" name="userGradeSeq" class="ui input medium" style="width: 100px;">
                                <c:forEach var="userGradeSeq" items="${userGradeSeq}" varStatus="status">
                                    <c:if test="${userGradeSeq.mcCd ne '11110'}"> <%--gray제외--%>
                                        <option value="${userGradeSeq.mcCd}" <c:if test="${userGradeSeq.ref2 eq 'df'}">selected</c:if>>${userGradeSeq.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>적용시스템<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="applyPcYn" name="applyPcYn" class="chkApplySystem" value="Y" checked><span>PC</span>
                        </label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" id ="applyAppYn" name="applyAppYn" class="chkApplySystem" value="Y" checked><span>APP</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="applyMwebYn" name="applyMwebYn" class="chkApplySystem" value="Y" checked><span>M.Web</span>
                        </label>
                    </td>
                    <th>행사목적</th>
                    <td colspan="3">
                        <div class="ui form inline dateControl">
                            <select id="purpose" name="purpose" class="ui input medium mg-r-10" style="width: 180px;">
                                <c:forEach var="purpose" items="${couponPurpose}" varStatus="status">
                                    <option value="${purpose.mcCd}">${purpose.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="spanPurchaseStdDt" class="text" style="display: none;"> 구매기준일
                                <input type="text" id="purchaseStdDt" name="purchaseStdDt" class="ui input medium mg-l-5 mg-r-5" placeholder="구매기준일" maxlength="10" style="width:90px;" >
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>난수쿠폰</th>
                    <td colspan="7">
                        <div class="ui form inline">
                            <select id="randomNoIssueYn" name="randomNoIssueYn" class="ui input medium" style="width: 100px;">
                                <c:forEach var="randomNoIssueYn" items="${useYn}" varStatus="status">
                                    <option value="${randomNoIssueYn.mcCd}" <c:if test="${randomNoIssueYn.ref2 eq 'df'}">selected</c:if>>${randomNoIssueYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span class="randomInfo">
                                <select id="randomNoType" name="randomNoType" class="ui input medium mg-l-5" style="width: 100px;">
                                    <c:forEach var="couponRandomType" items="${couponRandomType}" varStatus="status">
                                        <option value="${couponRandomType.mcCd}" <c:if test="${couponRandomType.ref1 eq 'df'}">selected</c:if>>${couponRandomType.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <button type="button" class="ui button small gray-dark font-malgun mg-l-10" id="randomListDownloadBtn">엑셀다운</button>
                                <input type="text" id="randomNo" name="randomNo" class="ui input medium mg-l-5" style="width: 120px;" maxlength="12" noShip noItem noImme>
                                <span id="radomNotiText" class="text mg-l-15"> ※ 발급시작 후 난수가 생성됩니다. 생성완료시 다운로드 버튼이 활성화 됩니다.</span>
                                <input type="hidden" id="randomListDownAvailYn">
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>노출설정<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox"  id ="displayItYn" name="displayItYn" value="Y" checked isUpdate="true" noImme><span>상품상세</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"   id ="displayCzYn" name="displayCzYn" value="Y" checked isUpdate="true" noImme><span>쿠폰/이벤트</span>
                        </label>
                    </td>
                    <th>상품쿠폰중복</th>
                    <td colspan="3">
                        <select id="itemDuplYn" name="itemDuplYn" class="ui input medium" style="width: 100px;" >
                            <c:forEach var="itemDuplYn" items="${couponDuplYn}" varStatus="status">
                                <option value="${itemDuplYn.mcCd}">${itemDuplYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>할인방법<span class="text-center text-red"> *</span></th>
                    <td colspan="7" id="tdDiscountType">
                        <div class="ui form inline">
                            <select id="discountType" name="discountType" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:forEach var="discountType" items="${couponDiscountType}" varStatus="status">
                                    <option value="${discountType.mcCd}">${discountType.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="discountPriceSpan">
                                <input type="text" id="discountPrice" name="discountPrice" class="ui input medium" style="width: 100px;text-align: right;" maxlength="9" noShip noItem noImme><span class="text">원</span>
                            </span>
                            <span id="discountRateSpan" style="display:none;">
                                    <input type="text" id="discountRate" name="discountRate" class="ui input medium" style="width: 80px;text-align: right;" maxlength="3" noShip noItem noImme><span class="text">%</span>
                                    <span class="text" style="padding-left: 20px;">최대할인금액</span>
                                    <input type="text" id="discountMax" name="discountMax" class="ui input medium mg-r-5" style="width: 150px;text-align: right;" maxlength="9" noShip><span class="text">원</span>
                            </span>
                        </div>
                    </td>
                    <%--<th id="discountRateSpan2" style="display:none;">최대할인금액<span class="text-center text-red"> *</span></th>
                    <td id="discountRateSpan3" style="display:none;">
                        <div class="ui form inline">
                            <input type="text" id="discountMax" name="discountMax" class="ui input medium mg-r-5" style="width: 150px;text-align: right;"><span class="text">원</span>
                        </div>
                    </td>--%>
                </tr>
                <tr>
                    <th>분담설정</th>
                    <td colspan="7">
                        <div class="ui form inline">
                            <select id="shareYn" name="shareYn" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:forEach var="shareYn" items="${useYn}" varStatus="status">
                                <option value="${shareYn.mcCd}" <c:if test="${shareYn.ref1 eq 'df'}">selected</c:if>>${shareYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="shareSpan" style="margin-left: 10px;display: none">
                                <label class="ui checkbox mg-r-10" style="display: inline-block">
                                    <input type="checkbox" id ="shareHomeYn" name="shareHomeYn" value="Y"><span class="text">홈플러스:</span>
                                    <input type="text" id="shareHome" name="shareHome" class="ui input medium mg-l-5 mg-r-5" style="width: 50px;text-align: right;" maxlength="3"><span class="text">%</span>
                                </label>
                                <label class="ui checkbox mg-r-10" style="display: inline-block">
                                    <input type="checkbox" id ="shareSellerYn" name="shareSellerYn" value="Y"><span class="text">판매자:</span>
                                    <input type="text" id="shareSeller" name="shareSeller" class="ui input medium mg-l-5 mg-r-5" style="width: 50px;text-align: right;" maxlength="3"><span class="text">%</span>
                                </label>
                                <label class="ui checkbox mg-r-10" style="display: none;">
                                    <input type="checkbox" id ="shareCardYn" name="shareCardYn" value="Y"><span class="text">카드사:</span>
                                    <input type="text" id="shareCard" name="shareCard" class="ui input medium mg-l-5 mg-r-5" style="width: 50px;text-align: right;" maxlength="3"><span class="text">%</span>
                                </label>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>발급기간<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline dateControl">
                            <input type="text" id="issueStartDt" name="issueStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;">
                            <span class="text">&nbsp;~&nbsp;</span>
                            <input type="text" id="issueEndDt" name="issueEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;">
                        </div>
                    </td>
                    <th>최소구매금액<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="purchaseMin" name="purchaseMin" class="ui input medium mg-r-5" style="width: 150px;text-align: right;" maxlength="9" noItem noImme><span class="text">원</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>적용기간<span class="text-center text-red"> *</span></th>
                    <td colspan="5">
                        <div class="ui form inline dateControl">
                            <select id="validType" name="validType" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:forEach var="validType" items="${couponValidType}" varStatus="status">
                                    <option value="${validType.mcCd}">${validType.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="periodSpan">
                                <input type="text" id="validStartDt" name="validStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:120px;" noImme>
                                <span class="text">&nbsp;~&nbsp;</span>
                                <input type="text" id="validEndDt" name="validEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:120px;" noImme>
                            </span>
                            <span id="dateSpan" style="display: none">
                                <input type="text" id="validDay" name="validDay" class="ui input medium mg-r-5" style="width: 100px;text-align: right" maxlength="3"><span class="text">일</span>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>총 발급수량<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="issueLimitType" name="issueLimitType" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:forEach var="issueLimitType" items="${issueLimitType}" varStatus="status">
                                    <option value="${issueLimitType.mcCd}">${issueLimitType.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="issueMaxSpan" style="display: none;">
                                <input type="text" id="issueMaxCnt" name="issueMaxCnt" class="ui input medium mg-r-5" style="width: 80px;text-align: right" maxlength="7"><span class="text" id="issueMaxCntText">회</span>
                            </span>
                        </div>
                    </td>
                    <th>1인 발급수량<span class="text-center text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline" >
                            <input type="text" id="limitPerCnt" name="limitPerCnt" class="ui input medium mg-r-5" maxlength="2" style="width: 60px;text-align: right"  noImme><span class="text">회</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>1인 1일 발급수량</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="limitPerDayYn" name="limitPerDayYn" class="ui input medium mg-r-5" style="width: 100px;" noItem noImme noShip>
                                <c:forEach var="limitPerDayYn" items="${useYn}" varStatus="status">
                                    <option value="${limitPerDayYn.mcCd}" <c:if test="${limitPerDayYn.ref1 eq 'df'}">selected</c:if>>${limitPerDayYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="limitPerDaySpan" style="display: none;">
                                <input type="text" id="limitPerDayCnt" name="limitPerDayCnt" class="ui input medium mg-r-5" style="width: 80px;text-align: right" maxlength="2"><span class="text">회</span>
                            </span>
                        </div>
                    </td>
                    <th>배송Slot시간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="shipSlotYn" name="shipSlotYn" class="ui input medium mg-r-5" style="width: 100px;" >
                                <c:forEach var="shipSlotYn" items="${useYn}" varStatus="status">
                                    <option value="${shipSlotYn.mcCd}" <c:if test="${shipSlotYn.ref1 eq 'df'}">selected</c:if>>${shipSlotYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="shipSlotSpan" style="display: none;">
                                <input type="text" id="slotStartTime" name="slotStartTime" class="ui input medium mg-r-5" style="width: 60px;" maxlength="5"><span class="text">~</span>
                                <input type="text" id="slotEndTime" name="slotEndTime" class="ui input medium mg-l-5 mg-r-5" style="width: 60px;" maxlength="5"><span class="text">(시:분)</span>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>발급시간대</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="issueTimeYn" name="issueTimeYn" class="ui input medium mg-r-5" style="width: 100px;" >
                                <c:forEach var="issueTimeYn" items="${useYn}" varStatus="status">
                                    <option value="${issueTimeYn.mcCd}" <c:if test="${issueTimeYn.ref1 eq 'df'}">selected</c:if>>${issueTimeYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="issueTimeSpan" style="display: none;">
                                <input type="text" id="issueStartTime" name="issueStartTime" class="ui input medium mg-r-5" style="width: 60px;" maxlength="5"><span class="text">~</span>
                                <input type="text" id="issueEndTime" name="issueEndTime" class="ui input medium mg-l-5 mg-r-5" style="width: 60px;" maxlength="5"><span class="text">(시:분)</span>
                            </span>
                        </div>
                    </td>
                    <th>배송Slot적용요일</th>
                    <td colspan="3">
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validMon" name="validMon" value="Y" class="chkSlotDay" noItem noImme><span>월</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validTue" name="validTue" value="Y" class="chkSlotDay" noItem noImme><span>화</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validWed" name="validWed" value="Y" class="chkSlotDay" noItem noImme><span>수</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validThu" name="validThu" value="Y" class="chkSlotDay" noItem noImme><span>목</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validFri" name="validFri" value="Y" class="chkSlotDay" noItem noImme><span>금</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validSat" name="validSat" value="Y" class="chkSlotDay" noItem noImme><span>토</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id ="validSun" name="validSun" value="Y" class="chkSlotDay" noItem noImme><span>일</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>합주문 가능여부</th>
                    <td colspan="3">
                        <select id="combineOrderAvailYn" name="combineOrderAvailYn" class="ui input medium" style="width: 100px;">
                            <c:forEach var="availYn" items="${availYn}" varStatus="status">
                                <option value="${availYn.mcCd}">${availYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>픽업 가능여부</th>
                    <td colspan="3">
                        <select id="pickupAvailYn" name="pickupAvailYn" class="ui input medium" style="width: 100px;">
                            <c:forEach var="availYn" items="${availYn}" varStatus="status">
                                <option value="${availYn.mcCd}">${availYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 적용 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>적용 정보</caption>
            <colgroup>
                <col width="140px">
                <col width="*">
                <col width="140px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>점포설정<span class="text-center text-red"> *</span></th>
                <td>
                    <div class="ui form inline">
                        <input type="hidden" id="storeList" name="storeList">
                        <button type="button" class="ui button small gray-dark font-malgun mg-r-10" id="storeInfoBtn">점포선택</button><span class="text">(적용 점포 수 : <span id="storeCnt">0</span>개)</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>결제수단</th>
                <td id="tdPayment">
                    <div class="ui form inline" id="cardRows">
                        <select id="paymentType" name="paymentType" class="ui input medium" style="width: 120px;">
                            <option value=''>선택안함</option>
                            <c:forEach var="payment" items="${couponPaymentType}" varStatus="status">
                                <option value="${payment.mcCd}">${payment.mcNm}</option>
                            </c:forEach>
                        </select>
                        <select id="methodCd" name="methodCd" class="ui input medium" style="width: 390px; display: none;">
                            <option value=''>선택안함</option>
                            <c:forEach var="payment" items="${couponMethodList}" varStatus="status">
                                <option value="${payment.methodCd}">${payment.methodNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
                <th id="thDetailPayment">상세결제수단</th>
                <td id="tdDetailPayment">
                    <div class="ui form inline" id="detailPaymentRow">
                        <span class="ui form inline mg-l-10">
                            <input type="hidden" id="cardList" name="cardList">
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-10" id="cardInfoBtn">상세 결제수단 선택</button><span class="text" >(적용 결제수단 수 : <span id="cardCnt">0</span>개)</span>
                        </span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 적용 상품정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>적용 상품정보</caption>
            <colgroup>
                <col width="140px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>적용 상품 범위</th>
                <td id="tdApplyScope">
                    <select id="applyScope" name="applyScope" class="ui input medium" style="width: 100px;">
                        <option value=''>선택하세요</option>
                        <c:forEach var="applyScope" items="${applyScope}" varStatus="status">
                            <option value="${applyScope.mcCd}" <c:if test="${applyScope.ref1 eq 'df'}">selected</c:if>>${applyScope.mcNm}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    </form>
        <div class="ui wrap-table horizontal noNeedAllScope">
            <table class="ui table">
                <caption>적용대상 그리드 검색</caption>
                <colgroup>
                    <col width="140px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="applyGridSearchType" name="applyGridSearchType" class="ui input medium mg-r-10" style="width: 120px"></select>
                            <input type="text" id="applyGridSearchValue" name="applyGridSearchValue" class="ui input medium mg-r-5" style="width: 350px;">
                            <button type="button" id="applyGridSearchBtn" class="ui button medium cyan" style="margin-right : 5px">검색</button>
                            <button type="button" id="applyGridSearchInitBtn" class="ui button medium">초기화</button>
                            <span class="text" style="margin-left: 20px; display: none"> ※ 상품번호/카테고리번호/판매자ID로 다중 검색 시 검색어 사이에 구분자 콤마(“,”)를 입력해 주세요. (ex.100011110,100011111,100011121)</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub noNeedAllScope" style="margin-top: 20px;">
            <div class="ui form inline">
                <h3 class="title">• 적용 목록 </h3><span class="title mg-l-5" id="applySearchCount"></span><span class="title" id="applyCnt"></span>
                <button type="button" class="ui button small mg-l-5 pull-right" id="deleteApply" style="display: none;">적용대상 삭제</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="popupApplyAllSearch" style="display: none;">일괄등록</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="popupApplySearch" style="display: none;">등록</button>
                <%--<button type="button" class="ui button small mg-l-5 pull-right" id="pullCmsProduct" style="display: none;">상품불러오기</button>--%>
                <button type="button" class="ui button small mg-l-5 pull-right" id="applyExcelDownloadBtn" style="display: none;">엑셀다운</button>
            </div>
        </div>
        <div id="applyGrid" class="noNeedAllScope" style="width: 100%; height: 310px;"></div>
    <%--</div>--%>

    <div id="exceptGridArea">
        <div class="com wrap-title sub">
            <h3 class="title">• 제외 상품 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>제외 상품 그리드 검색</caption>
                <colgroup>
                    <col width="140px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="exceptGridSearchType" name="exceptGridSearchType" class="ui input medium mg-r-10" style="width: 120px">
                                <option value="itemNo">상품번호</option>
                                <option value="itemNm">상품명</option>
                            </select>
                            <input type="text" id="exceptGridSearchValue" name="exceptGridSearchValue" class="ui input medium mg-r-5" style="width: 350px;">
                            <button type="button" id="exceptGridSearchBtn" class="ui button medium cyan" style="margin-right : 5px">검색</button>
                            <button type="button" id="exceptGridSearchInitBtn" class="ui button medium">초기화</button>
                            <span class="text" style="margin-left: 20px; display: none;"> ※ 상품번호 다중 검색 시 검색어 사이에 구분자 콤마(“,”)를 입력해 주세요. (ex.100011110,100011111,100011121)</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub" style="margin-top: 20px;">
            <div class="ui form inline">
                <h3 class="title">• 제외 상품 목록</h3><span class="title mg-l-5" id="exceptSearchCount"></span><span class="title" id="exceptCnt"></span>
                <button type="button" class="ui button small mg-l-5 pull-right" id="deleteExcept" style="display: none;">제외대상 삭제</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="popupExceptApplyAllSearch" style="display: none;">일괄등록</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="popupExceptSearch" style="display: none;">상품등록</button>
                <button type="button" class="ui button small mg-l-5 pull-right" id="exceptExcelDownloadBtn" style="display: none;">엑셀다운</button>
            </div>
        </div>
        <div id="exceptGrid" style="width: 100%; height: 300px;"></div>
    </div>

    <%-- 하단 버튼 영역 --%>
    <div class="ui mg-t-30">
        <table class="ui">
            <colgroup>
                <col width="300px">
                <col width="*">
                <col width="300px">
            </colgroup>
            <tbody>
            <tr>
                <td>
                    <button type="button" class="ui button xlarge dark-blue" style="display: none;" id="delBtn">쿠폰삭</button>&nbsp;
                    <button type="button" class="ui button xlarge dark-blue" id="copyBtn">복사</button>
                </td>
                <td style="text-align: center;">
                    <button type="button" id="setCouponBtn" class="ui button xlarge cyan font-malgun">저장</button>
                    <button type="button" id="resetBtn" class="ui button xlarge white font-malgun">초기화</button>
                </td>
                <td style="text-align: right">
                    <button type="button" id="withdrawBtn" class="ui button xlarge sky-blue" style="display: none;">회수</button>
                    <button type="button" id="issueBtn" class="ui button xlarge sky-blue" style="display: none;">쿠폰발급</button>
                    <button type="button" id="stopBtn" class="ui button xlarge sky-blue" style="display: none;">발급중단</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<iframe style="display:none;" id="excelDownloadFrame"></iframe>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/benefit.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/couponMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/couponMain.grid.js?${fileVersion}"></script>
<!-- month picker -->
<script type="text/javascript" src="/static/js/core/jquery.ui.monthpicker.js"></script>


<style type="text/css">
    .picker-input {
        width: 70px !important;
        height: 25px !important;
        display: inherit !important;
        background-color: #ffffff !important;
        margin-right: 5px !important;
        /*border-color: #3e295f;
        border: 2px !important;*/
    }
</style>

<script>
    var useYnJson = ${useYnJson};
    var storeTypeJson = ${storeTypeJson};
    var couponTypeJson = ${couponTypeJson};
    var couponPurposeJson = ${couponPurposeJson};
    var couponDuplYnJson = ${couponDuplYnJson};
    var couponDiscountTypeJson = ${couponDiscountTypeJson};
    var couponValidTypeJson = ${couponValidTypeJson};
    var issueLimitTypeJson = ${issueLimitTypeJson};
    var couponApplyScopeJson = ${couponApplyScopeJson};
    var couponStatusJson = ${couponStatusJson};
    var shareYnJson = ${shareYnJson};
    var couponCmsUnitJson = ${couponCmsUnitJson};
    var couponCartBarcodeJson = ${couponCartBarcodeJson};

    var availYnJson = ${availYnJson};
    var couponPaymentTypeJson = ${couponPaymentTypeJson};
    var couponRandomTypeJson = ${couponRandomTypeJson};
    var couponMethodListJson = ${couponMethodListJson};
    var userGradeSeqJson = ${userGradeSeqJson};

    // 점포 리스트
    var createStoreList = null;
    var deleteStoreList = null;

    // 쿠폰 조회 그리드 정보
    ${couponListGridBaseInfo}

    // 적용대상 그리드 정보
    ${applyGridBaseInfo}

    // 제외 상품 그리드 정보
    ${exceptGridBaseInfo}
</script>
