<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">점포행사 관리</h2>
    </div>

    <%-- 점포행사 정보 조회 영역 --%>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="rmsPromoSearchForm" name="rmsPromoSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>점포행사 검색</caption>
                    <colgroup>
                        <col width="12%">
                        <col width="27%">
                        <col width="12%">
                        <col width="27%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REGDT" selected>등록일</option>
                                    <option value="PROMODT">행사일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="rmsPromoSearch.initCalendarDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="rmsPromoSearch.initCalendarDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="rmsPromoSearch.initCalendarDate('-1w');">1주일전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="8">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>행사종류</th>
                        <td>
                            <select id="schEventKind" name="schEventKind" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="eventKind" items="${eventKind}" varStatus="status">
                                    <option value="${eventKind.mcCd}">${eventKind.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>행사상태</th>
                        <td>
                            <select id="schEventState" name="schEventState" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="eventState" items="${eventState}" varStatus="status">
                                    <option value="${eventState.mcCd}">${eventState.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 100px;" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="rmsPromoSearch.openStorePopup();" >조회</button>
                        </td>
                        <th>온라인 사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 180px;">
                                <option value="">전체</option>
                                <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                    <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    <tr>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 180px;">
                                    <option value="NAME">행사명</option>
                                    <option value="DETAILID" selected>상세번호</option>
                                    <option value="PROMOID">행사번호</option>
                                    <option value="COMPID">구성번호</option>
                                    <option value="ITEMNO">상품번호</option>
                                </select>
                                <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="rmsPromoTotalCount">0</span>건</h3>
        </div>
        <div id="rmsPromoGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <%-- 점포행사 상세 정보 영역 --%>
    <form id="rmsPromoForm" name="rmsPromoForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>기본정보</caption>
            <colgroup>
                <col width="10%">
                <col width="23%">
                <col width="10%">
                <col width="23%">
                <col width="10%">
                <col width="23%">
            </colgroup>
            <tbody>
                <tr>
                    <th>행사번호</th>
                    <td>
                        <span class="text" id="promoDisplayId"></span>
                    </td>
                    <th>행사구성번호</th>
                    <td>
                        <span class="text" id="compDisplayId"></span>
                    </td>
                    <th>행사상세번호</th>
                    <td>
                        <span class="text" id="rpmPromoCompDetailId"></span>
                    </td>
                </tr>
                <tr>
                    <th>행사명</th>
                    <td>
                        <span class="text" id="name"></span>
                    </td>
                    <th>행사종류</th>
                    <td colspan="3">
                        <span class="text" id="eventKind"></span>
                    </td>
                </tr>
                <tr>
                    <th>최소구매</th>
                    <td>
                        <span class="text" id="purchaseMin"></span>
                    </td>
                    <th>할인금액</th>
                    <td>
                        <span class="text" id="discountInfo"></span>
                    </td>
                    <th>이중가격표기</th>
                    <td>
                        <span class="text" id="xoutYn"></span>
                    </td>
                </tr>
                <tr>
                    <th>행사기간</th>
                    <td>
                        <span class="text" id="eventDt"></span>
                    </td>
                    <th>행사상태</th>
                    <td>
                        <span class="text" id="state"></span>
                    </td>
                    <th>온라인<br>사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium" style="width: 150px;">
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: left;">
        <h3 class="title" style="float:none;">• 구간 할인 정보</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="intervalCount">0</span>건</h3>
        </div>
        <div id="rmsPromoIntervalGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: right;">
        <h3 class="title" style="float:none;">• 적용점포</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="storeCount">0</span>건</h3>
        </div>
        <div id="rmsPromoStoreGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: left;">
        <h3 class="title" style="float:none;">• 적용 상품</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="itemCount">0</span>건</h3>
        </div>
        <div id="rmsPromoItemGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <div class="com wrap-title sub" style="width: 49.8%; float: right;">
        <h3 class="title" style="float:none;">• 할인 상품</h3>
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3>검색결과 : <span id="discItemCount">0</span>건</h3>
        </div>
        <div id="rmsPromoDiscItemGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <%-- 하단 버튼 영역 --%>
    <div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button type="button" id="setEventBtn" class="ui button xlarge cyan font-malgun mg-t-15">저장</button>
        <button type="button" id="resetBtn" class="ui button xlarge white font-malgun mg-t-15">초기화</button>
    </span>
    </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/rmsPromoMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/promotion/rmsPromoMain.grid.js?${fileVersion}"></script>

<script>
    // 그리드 lookup을 위한 Json 데이터
    var eventKindJson       = ${eventKindJson};
    var eventStateJson      = ${eventStateJson};
    var changeTypeJson      = ${changeTypeJson};
    var storeTypeJson       = ${storeTypeJson};
    var xoutApplyFlagJson   = ${xoutApplyFlagJson};

	// 점포행사 조회 그리드 정보
    ${rmsPromoGridBaseInfo}

    // 점포행사 할인 구간 정보 그리드
    ${rmsPromoIntervalGridBaseInfo}

    // 점포행사 적용점포 그리드
    ${rmsPromoStoreGridBaseInfo}

    // 점포행사 적용 상품 그리드
    ${rmsPromoItemGridBaseInfo}

    // 점포행사 할인대상 상품 그리드
    ${rmsPromoDiscItemGridBaseInfo}
</script>
