<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">노출여부 이력조회</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="affiliateChannelCommissionRateHistoryGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
        </div>

        <!-- 하단 버튼영역 -->
        <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 변경이력 팝업 그리드 기본정보
    ${affiliateChannelCommissionRateHistoryPopBaseInfo};

    $(document).ready(function () {
        affiliateChannelCommissionRateHistoryGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 변경이력 팝업 그리드
    var affiliateChannelCommissionRateHistoryGrid = {
        realGrid: new RealGridJS.GridView("affiliateChannelCommissionRateHistoryGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            affiliateChannelCommissionRateHistoryGrid.initGrid();
            affiliateChannelCommissionRateHistoryGrid.initDataProvider();
            affiliateChannelCommissionRateHistoryMng.search();
        },
        initGrid: function () {
            affiliateChannelCommissionRateHistoryGrid.realGrid.setDataSource(affiliateChannelCommissionRateHistoryGrid.dataProvider);
            affiliateChannelCommissionRateHistoryGrid.realGrid.setStyles(affiliateChannelCommissionRateHistoryPopBaseInfo.realgrid.styles);
            affiliateChannelCommissionRateHistoryGrid.realGrid.setDisplayOptions(affiliateChannelCommissionRateHistoryPopBaseInfo.realgrid.displayOptions);
            affiliateChannelCommissionRateHistoryGrid.realGrid.setColumns(affiliateChannelCommissionRateHistoryPopBaseInfo.realgrid.columns);
            affiliateChannelCommissionRateHistoryGrid.realGrid.setOptions(affiliateChannelCommissionRateHistoryPopBaseInfo.realgrid.options);
            affiliateChannelCommissionRateHistoryGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            affiliateChannelCommissionRateHistoryGrid.dataProvider.setFields(affiliateChannelCommissionRateHistoryPopBaseInfo.dataProvider.fields);
            affiliateChannelCommissionRateHistoryGrid.dataProvider.setOptions(affiliateChannelCommissionRateHistoryPopBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            affiliateChannelCommissionRateHistoryGrid.dataProvider.clearRows();
            affiliateChannelCommissionRateHistoryGrid.dataProvider.setRows(dataList);
        }
    };

    // 변경이력 팝업 관리
    var affiliateChannelCommissionRateHistoryMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/partner/getAffiliateChannelHist.json?channelId=${channelId}"
                , method: "GET"
                , callbackFunc: function (res) {
                    console.log(res);
                    affiliateChannelCommissionRateHistoryGrid.setData(res);
                }
            });
        },
    };
</script>