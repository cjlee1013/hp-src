<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">제휴채널 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="affiliateChannelSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>제휴채널관리 검색</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                        <col width="*">
                        <col width="100px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affliChannel.initSearchDate('-3m');">최근 3개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affliChannel.initSearchDate('-6m');">최근 6개월</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan ">
                                    <i class="fa fa-search"></i>검색
                                </button>
                                <br/>
                                <button type="button" id="searchResetBtn"
                                        class="ui button large white mg-t-5">초기화
                                </button>
                                <br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 100px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dskSiteGugun}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn"  name="searchUseYn" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 150px">
                                    <c:forEach var="codeDto" items="${affliSearchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 450px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="affliChannelTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="affliChannelListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <input type="hidden" id="channelId" name="channelId" value="">
        <table class="ui table">
            <caption>기본정보</caption>
            <colgroup>
                <col width="150px">
                <col width="*">
                <col width="150px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>제휴채널ID</th>
                <td id="channelIdTxt"></td>
                <th>제휴채널명&nbsp;<span class="text-red">*</span></th>
                <td >
                    <div class="ui form inline">
                        <input id="channelNm" name="channelNm" type="text" class="ui input medium mg-r-5" style="width: 300px;" maxlength="30">
                        <span class="text">
                            ( <span style="color:red;" id="channelNmCnt" class="cnt">0</span>/30 자)
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>제휴업체ID&nbsp;<span class="text-red">*</span></th>
                <td >
                    <div class="ui form inline">
                        <input type="text" id="partnerId" name="partnerId" class="ui input medium mg-r-5"  style="width:100px;">
                        <span class="text">&nbsp;&nbsp;&nbsp;</span>
                        <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affliChannel.getPartnerName();">조회</button>
                    </div>
                </td>
                <th>제휴업체명</th>
                <td id="partnerNm" style="width:250px;"></td>
            </tr>
            <tr>
                <th>사이트 구분&nbsp;<span class="text-red">*</span></th>
                <td>
                    <c:forEach var="codeDto" items="${dskSiteGugun}" varStatus="status">
                        <c:choose>
                            <c:when test="${ status.index eq 0 }">
                                <label class="ui radio inline"><input type="radio" name="siteType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                            </c:when>
                            <c:otherwise>
                                <label class="ui radio inline"><input type="radio" name="siteType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </td>
                <th>사용여부</th>
                <td>
                    <div class="ui form inline">
                        <select id="useYn" style="width: 100px" name="useYn" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>쿠키 유효기간&nbsp;<span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <input id="cookieTime" name="cookieTime" type="text" class="ui input medium mg-r-5" style="width:100px; "maxlength="3">
                        <span class="text">&nbsp;시간&nbsp;</span>
                        <button class="ui button small gray-light font-malgun" id="cookieHistBtn" style="display: none;">변경이력</button>
                    </div>
                </td>
                <th>수수료&nbsp;<span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <input id="commissionRate" name="commissionRate" type="text" class="ui input medium mg-r-5" style="width:100px;">
                        <span class="text">&nbsp;%&nbsp;</span>
                        <button class="ui button small gray-light font-malgun" id="commissionHistBtn" style="display: none;">변경이력</button>
                    </div>
                </td>
            </tr>
            <tr>
                <th>URL 고정 파라미터</th>
                <td colspan="3">
                    <div class="ui form inline">
                        <input id="urlParameter" name="urlParameter" type="text" class="ui input medium mg-r-5" style="width: 650px;" maxlength="100">
                        <span class="text">
                            ( <span style="color:red;" id="urlParameterCnt" class="cnt">0</span>/100 자)
                        </span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/affliChannelMain.js?${fileVersion}"></script>

<script>
    // GNB 리스트 리얼그리드 기본 정보
    ${affliChannelListGridBaseInfo}
</script>