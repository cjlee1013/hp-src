<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마켓연동 업체관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="marketSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마켓연동업체 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 100px" name="schUseYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <!-- 검색버튼 -->
                        <td>
                            <div class="ui form inline" >
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn" ><i class="fa fa-search"></i> 검색</button>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                <button type="button" class="ui button large mg-r-5 dark-blue pull-left" id="excelDownloadBtn">엑셀다운</button>
                                <span style="margin-left:4px;"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="marketTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="marketListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <form id="marketForm" name="marketForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>판매업체ID<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <span class="text">coop</span><input type="text" name="partnerId" id="partnerId" class="ui input medium mg-r-5" style="width: 100px;" maxlength="12" >
                            <button type="button" id="checkPartnerId" class="ui button small gray-dark font-malgun mg-r-5" onclick="market.valid.checkPartnerId();">중복확인</button>
                        </div>
                    </td>
                    <th>연동업체명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="businessNm" id="businessNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사업자유형</th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="operatorTypeNm" id="operatorTypeNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="6" value="법인사업자" readonly>
                            <input type="hidden" name="operatorType" id="operatorType" class="ui input medium mg-r-5" style="width: 150px;" maxlength="6" value="CB" readonly>
                        </div>

                    </td>
                    <th>사용여부</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="useYn" name="useYn" class="ui input medium mg-r-5" style="width: 150px" >
                                <c:forEach var="code" items="${useYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 사업자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>사업자정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상호<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <!--TODO: readonly 임시 해제" -->
                            <input type="text" name="partnerNm" id="partnerNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="12" >
                        </div>
                    </td>
                    <th>대표자명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerOwner" id="partnerOwner" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사업자 등록 번호<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="hidden" id="partnerNo" name="partnerNo" value="">
                            <input type="text" name="partnerNo_1" id="partnerNo_1" class="ui input medium"  style="width: 45px;" maxlength="3" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_2" id="partnerNo_2" class="ui input medium"  style="width: 55px;" maxlength="2" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_3" id="partnerNo_3" class="ui input medium mg-r-5"  style="width: 70px;" maxlength="5" readonly="true">
                            <button type="button" class="ui button small gray-dark font-malgun" id="getPartnerNoBtn">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>업태<span class="text-red"> *</span></th>
                    <td>
                        <input type="text" name="businessConditions" id="businessConditions" class="ui input medium" style="width: 150px;" maxlength="50">
                    </td>
                    <th>종목<span class="text-red"> *</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="hidden" id="bizCateCd" name="bizCateCd" >
                            <input type="text" name="bizCateCdNm" id="bizCateCdNm" class="ui input medium mg-r-5" style="width: 250px;" >
                            <button type="button" class="ui button small gray-dark font-malgun" id="getBizCateCd">검색</button>
                            <ul id="searchCategory" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 10px; left: 0px; width: 150px; display: none;"></ul>
                        </span>
                        <span id="bizCateCdListId" style="margin-top: 5px">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>주소<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="zipcode" id="zipcode" class="ui input medium mg-r-5" style="width:150px;" readonly>
                            <button type="button" class="ui button small" onclick="zipCodePopup('market.setZipcode'); return false;">우편번호</button>
                            <br>
                            <input type="text" name="addr1" id="addr1" class="ui input medium mg-t-5 mg-r-5" style="width:250px;" readonly>
                            <input type="text" name="addr2" id="addr2" class="ui input medium mg-t-5" style="width:250px;">
                        </div>
                        <div style="height:20px;display: none" id="gibunAddrArea">
                            <span style="height:10px;display:block;"></span>
                            [지번] <span id="gibunAddr1Txt"></span>
                            <span id="gibunAddr2Txt"></span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 사이트정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>사이트정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트명<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="siteNm" id="siteNm" class="ui input medium" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                    <th>사이트URL</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="siteUrl" id="siteUrl" class="ui input medium" style="width: 250px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>수수료</th>
                    <td>
                        <span class="text-bold">* 마켓 수수료 등록/수정</span>
                        <table class="ui table-inner">
                            <caption>마켓수수료 등록/수정</caption>
                            <colgroup>
                                <col width="8%">
                                <col width="20%">
                                <col width="*">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>수수료율</th>
                                <td>
                                    <div class="ui form inline">
                                        <input type="hidden" id="commissionSeq" name="commissionSeq" value="">
                                        <input class="ui input medium mg-r-5" id="commissionRate" name="commissionRate"
                                           type="text" style="width: 150px;" maxlength="5"
                                           onkeypress="return market.isNumberKey(event)"
                                           onkeyup="return market.delKorWord(event)">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>적용기간</th>
                                <td>
                                    <div class="ui form inline">
                                        <input type="text" id="applyStartDt" name="applyStartDt" class="ui input medium mg-r-5" placeholder="적용시작일" style="width:90px;">
                                        <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                        <input type="text" id="applyEndDt" name="applyEndDt" class="ui input medium mg-r-5" placeholder="적용종료일" style="width:90px;">

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="ui form inline" style="margin-top: 10px;float: right;">
                            <button type="button" id="resetCommissionFormBtn" class="ui button medium mg-r-5 gray pull-left">초기화</button>
                            <button type="button" id="addCommissionBtn" class="ui button medium mg-r-5 gray pull-left">추가</button>
                            <button type="button" id="saveCommissionBtn" class="ui button medium mg-r-5 gray pull-left" style="display: none">저장</button>
                            <button type="button" id="delCommissionBtn" class="ui button medium mg-r-5 gray pull-left">삭제</button>
                        </div>

                    </td>
                    <td colspan="2">
                        <div class="ui form inline">
                            <div class="mg-t-20 mg-b-20" id="marketCommissionGrid" style="width: 100%; height: 200px;"></div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 연동정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>연동정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>API 인증키<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <span class="text">상품 API: </span><input type="text" name="itemCertKey" id="itemCertKey" class="ui input medium" style="width: 250px;" maxlength="200">
                            <span class="text mg-l-5">주문 API: </span><input type="text" name="orderCertKey" id="orderCertKey" class="ui input medium" style="width: 250px;" maxlength="200">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun" id="setSellerBtn">저장</button>
                <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/marketMain.js?${fileVersion}"></script>

<script>
    //  마켓연동 리스트 리얼그리드 기본 정보
    ${marketListGridBaseInfo}
    ${marketCommissionGridBaseInfo}
</script>
