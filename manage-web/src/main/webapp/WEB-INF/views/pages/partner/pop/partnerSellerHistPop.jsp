<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
	.popup-request {margin: 20px;}
	.tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
	.tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
	.tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
	.tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
	.wrap-table td {background-color: white; word-break:break-all;}
	.vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
	<div class="com popup-wrap-title">
		<h3 class="title pull-left">
			<i class="fa fa-clone mg-r-5"></i>변경이력
		</h3>
	</div>
	<div class="tabui-wrap pd-t-20">
		<div class="tabui medium col-02">
			<div id="partnerSellerHistGrid" style="width: 100%; height: 350px;"></div>
			<button id="moreHist" class="ui button large mg-t-10" style="width: 100%; display: none;" >더보기</button>

		</div>
	</div>
	<div id="histDetail" style="display: none;">
		<div  class="com wrap-title" style="width: 70%" >
			<ul class="tab_vicinity" style="float:left; display: none;">
				<li class="on" data-type="basic"><a href="#" class="tab_cnt">변경일시</a></li>
				<li data-type="sale"><a href="#" class="tab_cnt">판매업체명</a></li>
				<li data-type="ship"><a href="#" class="tab_cnt">대표자명</a></li>
				<li data-type="optItem"><a href="#" class="tab_cnt">통신판매업신고번호</a></li>
				<li data-type="img"><a href="#" class="tab_cnt">업테</a></li>
				<li data-type="etc"><a href="#" class="tab_cnt">종목</a></li>
				<li data-type="etcItem"><a href="#" class="tab_cnt">입금계좌</a></li>
			</ul>
		</div>
		<div id="histDetailTable" class="ui wrap-table ">
			<table class="ui table">
				<colgroup><col width="100%"></colgroup>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>


<script>
	let RESDATA = {};

	${partnerSellerHistGridBaseInfo}

	const partnerId			= '${partnerId}';

	let detailTable = {
		basic	: { // 기본정보
			'html'		: '', // Html 여부
			'mapKey'		: ['false'],
			'value'			: [
				{
					'판매업체명'			: 'businessNm',
					'대표자명'			: 'partnerOwner',
					'통신판매업 신고번호'	: 'communityNotiNo',
					'업태'				: 'businessConditions',
					'종목'				: 'bizCateNm',
					'은행계좌'			: 'bankAccountNo',
					'예금주'				: 'depositor',
					'지급여부'			: 'settlePaymentYnNm'
				}
			]
		}
	};


    // 팝업 html
    var partnerSellerHistPop = {
        popContsHtml	: '',
		lastSeq			: '',
		pageNo 			: 0,
        init : function() {
            this.event();
			partnerSellerHistPop.getHist();
        },
		initHtml : function() {
			for (var key in detailTable) {
				detailTable[key].html = '';
			}
		},
		setTab : function(_this) {
			$('.tab_vicinity li').removeClass('on');
			if (!_this) {
				$('.tab_vicinity li').eq(0).addClass("on");
			} else {
				_this.addClass("on");
				partnerSellerHistPop.setDetailTable(_this.data('type'));
			}
		},
		event : function() {
			$('.closeBtn').on('click', function() {
				partnerSellerHistPop.close();
			});

			$('.tab_vicinity li').on('click', function () {
				partnerSellerHistPop.setTab($(this));
			});

			$('#moreHist').on('click', function() {
				partnerSellerHistPop.getHist(true);
			});
		},
		getHist : function(isMore) {
			CommonAjax.basic({
				url : "/partner/getPartnerSellerHistGroup.json?partnerId="+partnerId+"&pageNo="+partnerSellerHistPop.pageNo,
				method : 'get',
				callbackFunc : function(res) {

					if (res.length > 0) {
						if (isMore) {
							partnerSellerHistGrid.dataProvider.addRows(res);
						} else {
							partnerSellerHistGrid.setData(res);
						}

						if (res.length > 10) {
							$('#moreHist').show();
							partnerSellerHistPop.pageNo++;
							partnerSellerHistPop.deleteLastGrid();
						} else {
							$('#moreHist').hide();
							let rowDataJson = partnerSellerHistGrid.dataProvider.getJsonRow(partnerSellerHistGrid.gridView.getItemCount() - 1);
							partnerSellerHistPop.lastSeq	= rowDataJson.histSeq;
						}
					}
				}
			});
		},
		deleteLastGrid : function() {
        	let lastIdx = partnerSellerHistGrid.gridView.getItemCount() - 1;
			let rowDataJson = partnerSellerHistGrid.dataProvider.getJsonRow(lastIdx);

			partnerSellerHistPop.lastSeq	= rowDataJson.histSeq;
			partnerSellerHistGrid.dataProvider.removeRow(lastIdx);
		},
        setDetail : function(rowId) {

			let rowDataJson = partnerSellerHistGrid.dataProvider.getJsonRow(rowId);

			var lastRowId = partnerSellerHistGrid.gridView.getItemCount() - 1;
			var selectHistSeq = rowDataJson.histSeq;
			var prevHistSeq = 0;

			if (rowId == lastRowId) {
				prevHistSeq = partnerSellerHistPop.lastSeq;
			} else {
				// 이전 인덱스 추출
				let rowDataJsonPrev = partnerSellerHistGrid.dataProvider.getJsonRow(rowId+1);
				prevHistSeq = rowDataJsonPrev.histSeq;
			}

			RESDATA = rowDataJson;
			partnerSellerHistPop.setDetailTable('basic', rowDataJson);
			$('#histDetail').fadeIn(500);

        },
		setDetailTable : function(type, data) {
			var retHtml = '';
			var vColgroup = '<col width="10%"><col width="90%">';
			var colgroup = detailTable[type].hasOwnProperty('colgroup') ? detailTable[type]['colgroup'] : '<col width="10%"><col width="90%">';

			if (!data) {
				data = RESDATA;
			} else {
				partnerSellerHistPop.initHtml();
			}

			if (detailTable[type]['html'] == '') {

				var headerHtml = [];
				var tbodyHtml = '';

				var lastHtml = '</tbody></table></td></tr>';

				// 헤더생성
				for (var i in detailTable[type]['value']) {
					headerHtml[i] = '<tr>';
					for (var key in detailTable[type]['value'][i]) {
						headerHtml[i] += '<th>' + key + '</th>';
					}
					headerHtml[i]+= '</tr>';
				}

				// 바디생성
				for (var i in detailTable[type]['mapKey']) {
					if (detailTable[type]['mapKey'][i]=='false') {
						// 세로 테이블
						retHtml += '<tr><td><table class="vertical-table"><tbody>' + vColgroup;
						for (var key in detailTable[type]['value'][i]) {
							// 배열
							if (typeof data[detailTable[type]['value'][i][key]] == 'object') {
								var thArr = key.split('|');

								retHtml += '<tr><th>' + thArr[0] + '</th><td>';

								for (var objKey in data[detailTable[type]['value'][i][key]]) {
									if (data[detailTable[type]['value'][i][key]][objKey][thArr[1]] == '-') {
										continue;
									} else {
										retHtml += data[detailTable[type]['value'][i][key]][objKey][thArr[1]] + ', ';
									}
								}

								retHtml += '</td></tr>';
							} else {
								// 스트링
								retHtml += '<tr><th>' + key + '</th><td>' + data[detailTable[type]['value'][i][key]] + '</td></tr>';
							}
						}
					} else {
						// 가로 테이블
						if (data[detailTable[type]['mapKey'][i]] == null || Object.keys(data[detailTable[type]['mapKey'][i]]).length == 0) {
							continue;
						}

						if (headerHtml[i].length)
							retHtml += ('<tr><td><table class="horizon-table"><tbody>' + colgroup + headerHtml[i]);
						for (var key in data[detailTable[type]['mapKey'][i]]) {
							tbodyHtml = '<tr>';
							for (var optKey in detailTable[type]['value'][i]) {
								tbodyHtml += '<td>' + data[detailTable[type]['mapKey'][i]][key][detailTable[type]['value'][i][optKey]] + '</td>';
							}
							tbodyHtml += '</tr>';
							retHtml += tbodyHtml;
						}
					}

					retHtml += lastHtml;
				}

				detailTable[type]['html'] = retHtml;
			} else {
				retHtml = detailTable[type]['html'];
			}

			$('#histDetailTable tbody').html(retHtml);
		},
		close : function() {
			window.close();
		}
    };

    $(document).ready(function() {
		CommonAjaxBlockUI.global();
		partnerSellerHistPop.init();
		partnerSellerHistGrid.init();
    });

	/**
	 * 상품관리 > 셀러상품관리/점포상품관리 > 변경이력 > 공통그리드 (TD/DS)
	 */
	let partnerSellerHistGrid = {
		gridView : new RealGridJS.GridView("partnerSellerHistGrid"),
		dataProvider : new RealGridJS.LocalDataProvider(),
		init : function() {
			partnerSellerHistGrid.initGrid();
			partnerSellerHistGrid.initDataProvider();
			partnerSellerHistGrid.event();
		},
		initGrid : function() {
			partnerSellerHistGrid.gridView.setDataSource(partnerSellerHistGrid.dataProvider);

			partnerSellerHistGrid.gridView.setStyles(partnerSellerHistGridBaseInfo.realgrid.styles);
			partnerSellerHistGrid.gridView.setDisplayOptions(partnerSellerHistGridBaseInfo.realgrid.displayOptions);
			partnerSellerHistGrid.gridView.setColumns(partnerSellerHistGridBaseInfo.realgrid.columns);
			partnerSellerHistGrid.gridView.setOptions(partnerSellerHistGridBaseInfo.realgrid.options);
		},
		initDataProvider : function() {
			partnerSellerHistGrid.dataProvider.setFields(partnerSellerHistGridBaseInfo.dataProvider.fields);
			partnerSellerHistGrid.dataProvider.setOptions(partnerSellerHistGridBaseInfo.dataProvider.options);
		},
		event : function() {
			// 그리드 선택
			partnerSellerHistGrid.gridView.onDataCellClicked = function(gridView, index) {
				$('#histDetail').hide();
				partnerSellerHistPop.setDetail(index.dataRow);
			};
		},
		setData : function(dataList) {
			partnerSellerHistGrid.dataProvider.clearRows();
			partnerSellerHistGrid.dataProvider.setRows(dataList);
		}
	};

</script>