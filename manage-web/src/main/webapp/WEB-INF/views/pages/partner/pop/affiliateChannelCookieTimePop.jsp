<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">쿠키 유효기간 변경이력</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="affiliateChannelCookieTimeHistoryGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
        </div>

        <!-- 하단 버튼영역 -->
        <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 변경이력 팝업 그리드 기본정보
    ${affiliateChannelCookieTimeHistoryPopBaseInfo};

    $(document).ready(function () {
        affiliateChannelCookieTimeHistoryGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 변경이력 팝업 그리드
    var affiliateChannelCookieTimeHistoryGrid = {
        realGrid: new RealGridJS.GridView("affiliateChannelCookieTimeHistoryGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            affiliateChannelCookieTimeHistoryGrid.initGrid();
            affiliateChannelCookieTimeHistoryGrid.initDataProvider();
            affiliateChannelCookieTimeHistoryMng.search();
        },
        initGrid: function () {
            affiliateChannelCookieTimeHistoryGrid.realGrid.setDataSource(affiliateChannelCookieTimeHistoryGrid.dataProvider);
            affiliateChannelCookieTimeHistoryGrid.realGrid.setStyles(affiliateChannelCookieTimeHistoryPopBaseInfo.realgrid.styles);
            affiliateChannelCookieTimeHistoryGrid.realGrid.setDisplayOptions(affiliateChannelCookieTimeHistoryPopBaseInfo.realgrid.displayOptions);
            affiliateChannelCookieTimeHistoryGrid.realGrid.setColumns(affiliateChannelCookieTimeHistoryPopBaseInfo.realgrid.columns);
            affiliateChannelCookieTimeHistoryGrid.realGrid.setOptions(affiliateChannelCookieTimeHistoryPopBaseInfo.realgrid.options);
            affiliateChannelCookieTimeHistoryGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            affiliateChannelCookieTimeHistoryGrid.dataProvider.setFields(affiliateChannelCookieTimeHistoryPopBaseInfo.dataProvider.fields);
            affiliateChannelCookieTimeHistoryGrid.dataProvider.setOptions(affiliateChannelCookieTimeHistoryPopBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            affiliateChannelCookieTimeHistoryGrid.dataProvider.clearRows();
            affiliateChannelCookieTimeHistoryGrid.dataProvider.setRows(dataList);
        }
    };

    // 변경이력 팝업 관리
    var affiliateChannelCookieTimeHistoryMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/partner/getAffiliateChannelHist.json?channelId=${channelId}"
                , method: "GET"
                , callbackFunc: function (res) {
                    console.log(res);
                    affiliateChannelCookieTimeHistoryGrid.setData(res);
                }
            });
        },
    };
</script>