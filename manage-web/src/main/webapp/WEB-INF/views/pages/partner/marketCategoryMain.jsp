<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마켓연동 카테고리매핑</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="marketCategorySearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  marketCategory.search();">
                <table class="ui table">
                    <caption> 마켓연동 카테고리매핑</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="62%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="schCateCd1" name="schCateCd1"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd2" name="schCateCd2"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd3" name="schCateCd3"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="schCateCd4" name="schCateCd4"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn" ><i class="fa fa-search"></i> 검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 dark-blue pull-left" id="excelDownloadBtn">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>등록여부</th>
                        <td>
                            <div class="ui form inline ">
                                <select id="schRegYn" name="schRegYn" class="ui input medium mg-r-5" style="width: 150px" >
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${regYn}" varStatus="status">
                                        <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="marketCategoryTotalCount">0</span>건  </h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="marketCategoryListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 카테고리 매핑정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="marketCategoryMapForm" onsubmit="return false;">
            <input type="hidden" id="isMod" name="isMod" value="N"/>
            <table class="ui table">
                <caption>카테고리 매핑정보 등록/수정</caption>
                <colgroup>
                    <col width="12%">
                    <col width="12%">
                    <col width="12%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>카테고리<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="selectCateCd1" name="selectCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(1,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd2" name="selectCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(2,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd3" name="selectCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:commonCategory.changeCategorySelectBox(3,'selectCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="selectCateCd4" name="selectCateCd4"
                                style="width:150px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <!--
                <tr>
                    <th>G마켓 연동여부</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="gmarketApplyYn" name="gmarketApplyYn" class="ui input medium mg-r-5 applyYn"
                                    style="width: 100px" data-cate="gmarketCateCd" >
                                <option value="">선택</option>
                                <c:forEach var="code" items="${applyYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                   <th>G마켓 카테고리</th>
                    <input type="hidden" name="gmarket" id="gmarket" value="${gmarket}" />
                    <input type="hidden" name="${gmarket}LeafCateCd" id="${gmarket}LeafCateCd" value=""/>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="gmarketCateCd1" name="gmarketCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(1,'gmarketCateCd', '${gmarket}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="gmarketCateCd2" name="gmarketCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(2,'gmarketCateCd', '${gmarket}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="gmarketCateCd3" name="gmarketCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(3,'gmarketCateCd', '${gmarket}');">
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>옥션 연동여부</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="auctionApplyYn" name="auctionApplyYn" class="ui input medium mg-r-5 applyYn"
                                    style="width: 100px" data-cate="auctionCateCd" >
                                <option value="">선택</option>
                                <c:forEach var="code" items="${applyYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>옥션 카테고리</th>
                    <input type="hidden" name="auction" id="auction" value="${auction}" />
                    <input type="hidden" name="${auction}LeafCateCd" id="${auction}LeafCateCd" value=""/>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="auctionCateCd1" name="auctionCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(1,'auctionCateCd', '${auction}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="auctionCateCd2" name="auctionCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(2,'auctionCateCd', '${auction}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="auctionCateCd3" name="auctionCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(3,'auctionCateCd', '${auction}');">
                        </select>
                        <select class="ui input medium mg-r-5 dcate" id="auctionCateCd4" name="auctionCateCd4"
                                style="width:150px;float:left;" data-default="세분류" data-id="${auction}" >
                        </select>
                    </td>
                </tr>-->
                <tr id="elevenArea">
                    <th>11번가 연동여부</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="elevenStApplyYn" name="elevenStapplyYn" class="ui input medium mg-r-5 applyYn"
                                    style="width: 100px" data-cate="elevenStCateCd">
                                <option value="">선택</option>
                                <c:forEach var="code" items="${applyYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>11번가 카테고리 </th>
                    <input type="hidden" name="elevenSt" id="elevenSt" value="${elevenSt}" />
                    <input type="hidden" name=${elevenSt}LeaCateCd" id="${elevenSt}LeafCateCd" value=""/>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="elevenStCateCd1" name="elevenStCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(1,'elevenStCateCd', '${elevenSt}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="elevenStCateCd2" name="elevenStCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(2,'elevenStCateCd', '${elevenSt}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="elevenStCateCd3" name="elevenStCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(3,'elevenStCateCd', '${elevenSt}');">
                        </select>
                        <select class="ui input medium mg-r-5 dcate" id="elevenStCateCd4" name="elevenStCateCd4"
                                style="width:150px;float:left;" data-default="세분류" data-id="${elevenSt}">
                        </select>
                    </td>
                </tr>
                <tr id="naverArea">
                    <th>N마트 연동여부</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="naverApplyYn" name="naverApplyYn" class="ui input medium mg-r-5 applyYn"
                                    style="width: 100px" data-cate="naverCateCd">
                                <option value="">선택</option>
                                <c:forEach var="code" items="${applyYn}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>N마트 카테고리 </th>
                    <input type="hidden" name=naver" id="naver" value="${naver}" />
                    <input type="hidden" name=${naver}LeaCateCd" id="${naver}LeafCateCd" value=""/>
                    <td colspan="3">
                        <select class="ui input medium mg-r-5" id="naverCateCd1" name="naverCateCd1"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(1,'naverCateCd', '${naver}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="naverCateCd2" name="naverCateCd2"
                                style="width:150px;float:left;" data-default="중분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(2,'naverCateCd', '${naver}');">
                        </select>
                        <select class="ui input medium mg-r-5" id="naverCateCd3" name="naverCateCd3"
                                style="width:150px;float:left;" data-default="소분류"
                                onchange="javascript:marketCategory.changeMarketCategorySelectBox(3,'naverCateCd', '${naver}');">
                        </select>
                        <select class="ui input medium mg-r-5 dcate" id="naverCateCd4" name="naverCateCd4"
                                style="width:150px;float:left;" data-default="세분류" data-id="${naver}">
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setMarketCategoryBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/marketCategoryMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?${fileVersion}"></script>

<script>
    // 마켓연동 카테고리 맵핑 리스트 리얼그리드 기본 정보
    ${marketCategoryListGridBaseInfo}
</script>
