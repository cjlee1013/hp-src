<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp" />
<!-- 기본 프레임 -->
<div class="com wrap-popup small popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">ID/PW 안내</h2>
    </div>
    <form id="sendForm" name="sendForm" method="POST" onsubmit="return false;">
    <div class="ui wrap-table">
    <table class="ui table">
        <caption>회원정보</caption>
        <colgroup>
            <col width="100px">
            <col width="120px">
            <col width="100px">
            <col width="*">
        </colgroup>
        <tbody>
            <tr>
                <th>ID</th>
                <td class="text-left">${partnerId}</td>
                <th>상호</th>
                <td id="partnerNm" class="text-left"></td>
            </tr>
            <tr>
                <th>계정상태</th>
                <td class="text-left">${loginStatusNm}</td>
                <th>사유</th>
                <td class="text-left">${loginRegReason}</td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="com wrap-title sub">
        <div class="ui form">
            <h3 class="text"> * 영업 담당자에게 발송합니다.</h3>
        </div>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>영업 담당자 정보</caption>
            <colgroup>
                <col width="15%">
                <col width="20%">
                <col width="65%">
            </colgroup>
            <tbody>
                <tr>
                    <th class="text-center">선택</th>
                    <th class="text-center">구분</th>
                    <th class="text-center">내용</th>
                </tr>
                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="checkEmail" id="checkEmail" value="EMAIL" class="text-center">
                    </td>
                    <td class="text-center">이메일</td>
                    <td id="emailAddr"><span class="text">${affiliateEmail}</span></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="checkMobile" id="checkMobile" value="MOBILE" class="text-center">
                    </td>
                    <td class="text-center">휴대폰</td>
                    <td id="mobile">${affiliateMobile}</td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button midium gray-dark font-malgun" id="sendPW">임시 패스워드 발급</button>
        </span>
    </div>

</div>

<script>

    $(document).ready(function(){
        $('#partnerNm').html($('#partnerNm', opener.document).val());
        $("#sendPW").click(function(){

            if (!$("input:checkbox[id='checkEmail']").is(":checked") && !$("input:checkbox[id='checkMobile']").is(":checked")) {
                alert("임시 패스워드를 발송할 대상을 선택하세요.");
                return;
           }

            if(confirm("임시 패스워드를 발급하시겠습니까?")) {

                CommonAjax.basic({
                    url: "/partner/popup/setPartnerTempPw.json",
                    data: {
                        partnerId   : getHttpParam("partnerId"),
                        businessNm  : '${businessNm}',
                        partnerType : getHttpParam("partnerType"),
                        emailCheck  : $("input:checkbox[id='checkEmail']").is(":checked") ? "Y" : "N",
                        mobileCheck : $("input:checkbox[id='checkMobile']").is(":checked") ? "Y" : "N",
                    },
                    method: "POST",
                    callbackFunc: function (ret) {
                        alert(ret.returnMsg);
                        self.close();
                    }
                });
            }
        });
    });

    function getHttpParam(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null) {
            return "";
        } else {
            return results[1];
        }
    }

</script>