<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">배송정책관리</h2>
    </div>
    <c:choose>
        <c:when test="${popYn eq 'Y'}">
            <div  class="com wrap-title sub" >
                <div class="com wrap-title sub">
                    <h3 class="title">• 배송정책 수 : <span id="shipPolicyTotalCountPop">0</span>건</h3>
                </div>
                <div class="topline"></div>
                <div id="shipPolicyListGridPop" style="width: 100%; height: 300px;"></div>
            </div>
        </c:when>
        <c:otherwise>
            <div  class="com wrap-title sub" >
                <div class="ui wrap-table horizontal mg-t-1 0">
                    <form id="shipPolicySearchForm" onsubmit="return false;">
                        <table class="ui table">
                            <caption>배송정책관리검색</caption>
                            <colgroup>
                                <col width="6%">
                                <col width="20%">
                                <col width="6%">
                                <col width="20%">
                                <col width="6%">
                                <col width="*">
                                <col width="10%">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>등록일</th>
                                <td colspan="5">
                                    <div class="ui form inline">
                                        <select id="schPartnerPeriodType" style="width: 100px" name="schPartnerPeriodType" class="ui input medium mg-r-10" data-default="REGDT">
                                            <c:forEach var="codeDto" items="${partnerPeriodType}" varStatus="status">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:forEach>
                                        </select>

                                        <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" style="width:100px;">
                                        <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                        <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" style="width:100px;">
                                        <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shipPolicy.initSearchDate('0');">오늘</button>
                                        <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shipPolicy.initSearchDate('-1d');">어제</button>
                                        <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shipPolicy.initSearchDate('-30d');">1개월전</button>
                                        <button type="button" class="ui button small gray-dark font-malgun " onclick="shipPolicy.initSearchDate('-90d');">3개월전</button>
                                    </div>
                                </td>
                                <!-- 검색 버튼 -->
                                <td rowspan="4">
                                    <div style="text-align: center;" class="ui form mg-t-15">
                                        <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                        <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>구분</th>
                                <td colspan="3">
                                    <label class="ui radio inline"><input type="radio" name="schPartnerType" class="ui input" value="SELLER" checked><span>Seller(DS)</span></label>
                                    <label class="ui radio inline"><input type="radio" name="schPartnerType" class="ui input" value="HYPER"><span>HYPER</span></label>
                                    <label class="ui radio inline"><input type="radio" name="schPartnerType" class="ui input" value="EXP"><span>Express</span></label>
                                </td>

                            </tr>
                            <tr>
                                <th>사업자유형</th>
                                <td>
                                    <div class="ui form inline">
                                        <select id="schOperatorType" style="width: 120px" name="schOperatorType" class="ui input medium mg-r-10">
                                            <option value="">전체</option>
                                            <c:forEach var="codeDto" items="${operatorType}" varStatus="status">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </td>
                                <th>점포</th>
                                <td>
                                    <div class="ui form inline">
                                        <input type="text" id="schStoreId" name="schStoreId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID" readonly>
                                        <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="점포명" readonly>
                                        <button type="button" class="ui button medium" onclick="shipPolicy.getStorePop('shipPolicy.callBack.schStore');" >조회</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>검색어</th>
                                <td >
                                    <div class="ui form inline">
                                        <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                                            <option value="partnerNm" selected="selected">판매업체명</option>
                                            <option value="partnerId">판매업체ID</option>
                                            <option value="partnerNo">사업자등록번호</option>
                                            <option value="partnerOwner">대표자</option>
                                        </select>
                                        <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 180px;" minlength="2" >
                                    </div>
                                </td>
                                <th>회원상태</th>
                                <td>
                                    <div class="ui form inline">
                                        <select id="schPartnerStatus" style="width: 100px" name="schPartnerStatus" class="ui input medium mg-r-10">
                                            <option value="">전체</option>
                                            <c:forEach var="codeDto" items="${partnerStatus}" varStatus="status">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="com wrap-title sub">
                    <h3 class="title">• 검색결과 : <span id="sellerTotalCount">0</span>건</h3>
                </div>
                <div class="topline"></div>
                <div id="sellerListGrid" style="width: 100%; height: 300px;"></div>

                <div class="com wrap-title sub">
                    <h3 class="title">• 배송정책 수 : <span id="shipPolicyTotalCount">0</span>건</h3>
                </div>
                <div class="topline"></div>
                <div id="shipPolicyListGrid" style="width: 100%; height: 300px;"></div>

            </div>
        </c:otherwise>
    </c:choose>
    <form id="shipPolicyForm" name="shipPolicyForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 배송상세정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>배송상세정보<</caption>
                <colgroup>
                    <col width="12%">
                    <col width="38%">
                    <col width="12%">
                    <col width="38%">
                </colgroup>
                <tbody>
                <tr>
                    <th>판매업체</th>
                    <td id="partnerRow">
                        <input type="hidden" id="partnerId" name="partnerId">
                        <div class="ui form inline">
                            <span class="text" id="partnerInfo"></span>
                        </div>
                    </td>
                    <th>점포</th>
                    <td id="storeRow">
                        <div class="ui form inline">
                            <input type="hidden" id="storeId" name="storeId" value="0">
                            <span class="text" id="storeInfo"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>배송정책번호</th>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" id="shipPolicyNo" name="shipPolicyNo">
                            <span class="text" id="shipPolicyNoInfo"></span>
                        </div>
                    </td>
                    <th><span class="text-red-star">배송정보명</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="shipPolicyNm" id="shipPolicyNm" class="ui input medium mg-r-5" style="width: 250px;" maxlength="25" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">배송방법</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="hidden" id="shipMethod" name="shipMethod" value="" />
                            <select id="shipMethodDS" name="shipMethodDS" class="ui input medium mg-r-5 shipMethod" style="width: 150px"  >
                                <option value="">선택</option>
                                <c:forEach var="code" items="${shipMethod}" varStatus="status">
                                    <c:if test="${code.ref1 eq 'DS'}">
                                    <option value="${code.mcCd}" >${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <select id="shipMethodTD" name="shipMethodTD" class="ui input medium mg-r-5 shipMethod" style="width: 150px;" >
                                <option value="">선택</option>
                                <c:forEach var="code" items="${shipMethod}" varStatus="status">
                                    <c:if test="${code.ref1 eq 'TD'}">
                                        <option value="${code.mcCd}" >${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>

                        </div>
                    </td>
                    <th><span class="text-red-star">배송유형</span></th>
                    <div class="ui form inline ">
                        <td>
                            <c:forEach var="codeDto" items="${shipType}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline">
                                    <input type="radio" id="shipType${codeDto.mcCd}" name="shipType" value="${codeDto.mcCd}" ${checked} class="ui input medium"><span class="text">${codeDto.mcNm}</span></label>
                            </c:forEach>
                        </td>
                    </div>
                </tr>
                <tr>
                    <th><span class="text-red-star">배송비종류</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${shipKind}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline">
                                <input type="radio" id="shipKind${codeDto.mcCd}" name="shipKind" class="ui input medium" value="${codeDto.mcCd}" ${checked}><span class="text">${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th><span class="text-red-star">배송비</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="shipFee" id="shipFee" class="ui input medium mg-r-5" style="width: 110px;text-align: right;" maxlength="6" readonly ><span class="text">원 I</span>
                            <input type="text" name="freeCondition" id="freeCondition" class="ui input medium" style="width: 110px;text-align: right;" maxlength="7" readonly><span class="text">원 이상 구매 시 무료</span>
                        </div>
                    </td>

                </tr>
                <tr>
                    <th><span class="text-red-star">수량별 차등</span></th>
                    <td>
                        <span class="ui form inline">
                            <label class="ui radio inline"><input type="radio" id="diffYnN" name="diffYn" value="N" checked><span class="text">설정안함</span></label>
                            <label class="ui radio inline"><input type="radio" id="diffYnY" name="diffYn" value="Y" disabled ><span class="text">설정함</span></label>
                        </span>
                        <BR><BR>
                        <span class="ui form inline">
                            <span class="text mg-r-10">구매수량</span> <input type="text" name="diffQty" id="diffQty" class="ui input medium" style="width: 90px;text-align: right;" maxlength="4" readonly ><span class="text mg-r-10"> 개 초과시 마다 배송비를 추가로 부과</span>
                        </span>
                    </td>
                    <th>배송비 결제방식 </th>
                    <td>
                        <c:forEach var="codeDto" items="${prepaymentYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" id="prepaymentYn${codeDto.mcCd}" name="prepaymentYn" value="${codeDto.mcCd}" ${checked}><span class="text">${codeDto.mcNm}</span></label>
                        </c:forEach>
                     </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">배송가능지역</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="shipArea" name="shipArea" class="ui input medium mg-r-5" style="width: 150px" >
                                <c:forEach var="code" items="${shipArea}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>도서산간 배송비</th>
                    <td>
                        <div class="ui form inline ">
                            <span class="text">제주</span>
                            <input type="text" name="extraJejuFee" id="extraJejuFee" class="ui input medium mg-r-5" style="width: 120px;text-align: right;" maxlength=6" ><span class="text">원 I 도서산간</span>
                            <input type="text" name="extraIslandFee" id="extraIslandFee" class="ui input medium mg-r-5" style="width: 120px; text-align: right;" maxlength="6"><span class="text">원 </span>
                        </div>
                        <BR>
                        <button type="button" class="ui button small gray-dark font-malgun" onclick="checkIslandPopup(); return false;" >제주/도서산간 지역 안내</button>
                    </td>
                </tr>
                <tr>
                    <th>배송비 노출여부</th>
                    <td>
                        <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" id="dispYn${codeDto.mcCd}" name="dispYn" value="${codeDto.mcCd}" ${checked}><span class="text">${codeDto.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th>사용여부</th>
                    <td>
                        <select id="useYn" name="useYn" class="ui input medium mg-r-5" style="width: 150px" >
                            <c:forEach var="code" items="${useYn}" varStatus="status">
                                <c:set var="selected" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>

                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 배송추가정보 </h3> <span class="text mg-l-5" >(이미 등록된 상품의 배송추가정보는 자동으로 변경되지 않습니다.) </span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>배송추가정보</caption>
                <colgroup>
                    <col width="13%">
                    <col width="37%">
                    <col width="13%">
                    <col width="37%">
                </colgroup>
                <tbody>
                <tr>
                    <th><span class="text-red-star">반품/교환배송비</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="claimShipFee" id="claimShipFee" class="ui input medium mg-r-5" style="width: 150px;text-align: right;" maxlength="6" > <span class="text">원</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><span class="text-red-star">출고지</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="releaseZipcode" id="releaseZipcode" class="ui input medium mg-r-5" style="width:150px;" readonly>
                            <button type="button" class="ui button small mg-r-5" onclick="zipCodePopup('shipPolicy.setRelieaseZipcode'); return false;">우편번호</button><br>
                            <input type="text" name="releaseAddr1" id="releaseAddr1" class="ui input medium mg-t-5 mg-r-5" style="width:225px;" readonly>
                            <input type="text" name="releaseAddr2" id="releaseAddr2" class="ui input medium mg-t-5" style="width:225px;" >
                        </div>
                        <label class="ui checkbox mg-r-5 mg-t-5 pull-right" >
                            <input type="checkbox" id="samePartnerAddr" name="samePartnerAddr" style="align:right" ><span class="text">사업자주소 동일적용</span>
                        </label>
                    </td>
                    <th><span class="text-red-star">회수지</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="returnZipcode" id="returnZipcode" class="ui input medium mg-r-5" style="width:150px;" readonly>
                            <button type="button" class="ui button small mg-r-5" onclick="zipCodePopup('shipPolicy.setReturnZipcode'); return false;">우편번호</button><br>
                            <input type="text" name="returnAddr1" id="returnAddr1" class="ui input medium mg-t-5 mg-r-5" style="width:225px;" readonly>
                            <input type="text" name="returnAddr2" id="returnAddr2" class="ui input medium mg-t-5" style="width:225px;" >
                        </div>

                        <label class="ui checkbox mg-r-5 mg-t-5 pull-right">
                            <input type="checkbox" id="sameReleaseAddr" name="sameReleaseAddr"><span class="text">출고지주소 동일 적용</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="text-red-star">출고기한</span>
                    </th>
                    <td colspan="4">
                    <span class="ui form inline">
                        <select class="ui input medium mg-r-5 shipBundleLayer" name="releaseDay" id="releaseDay" data-default="선택" style="width:150px;">
                            <option value="">선택</option>
                            <option value="1">오늘발송</option>
                            <c:forEach var="item"  begin="2" end="15" step="1" varStatus="status">
                                <option value="${item}">${item}</option>
                            </c:forEach>
                            <option value="-1">미정</option>
                        </select>
                        <span id="releaseTextInfo" class="text">일 이내 발송처리</span>

                        <span id="releaseDetailInfo" style="display:none">
                            <span class="text"> &nbsp;당일 </span>
                            <select class="ui input medium mg-l-5 shipBundleLayer" name="releaseTime" id="releaseTime" data-default="선택" style="width:70px;">
                                <option value="">선택</option>
                                <c:forEach var="item"  begin="1" end="24" step="1" varStatus="status">
                                    <option value="${status.count}">${status.count}</option>
                                </c:forEach>
                            </select>
                            <span class="text">시 결제 건까지 오늘발송처리 </span>
                        </span>
                        <span id="holidayExceptionYnInfo">
                            <span class="text">&nbsp;(&nbsp;</span><label class="ui checkbox">
                                <input type="checkbox" id="holidayExceptYn" name="holidayExceptYn" value="Y"><span class="text">주말/공휴일 제외)</span></label>
                        </span>
                    </span>
                    </td>
                </tr>
                <tr>
                    <th>안심번호 사용여부</th>
                    <td>
                        <select id="safeNumberUseYn" name="safeNumberUseYn" class="ui input medium mg-r-5" style="width: 150px" >
                            <c:forEach var="code" items="${safeNumberUseYn}" varStatus="status">
                                <c:set var="selected" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected" />
                                </c:if>
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <label class="ui checkbox mg-r-5 mg-t-10">
            <input type="checkbox" id="defaultYn" name="defaultYn" value="Y"><span class="text"> 기본 배송방법으로 설정 </span>
        </label>

        <div class="ui center-button-group mg-t-15">
             <span class="inner">
                 <button type="button" class="ui button xlarge cyan font-malgun" id="setShipPolicyBtn">저장</button>
                 <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
             </span>
        </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    const popYn = '${popYn}';
    const partnerSeller = '${partnerSeller}';

    // 판매업체 리스트, 배송정책 리얼그리드 기본 정보
    ${sellerListGridBaseInfo}
    ${shipPolicyListGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/partner/shipPolicyMain.js?${fileVersion}"></script>
