<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">셀러샵 등록/수정 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerShopSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>셀러샵 등록/수정 검색</caption>
                    <colgroup>
                        <col width="120px;">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일시</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp; &nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-90d');">3개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-6m');">6개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan"><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width:120px; float:left;" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${sellerShopUseYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width:120px; float:left;">
                                <option value="">전체</option>
                                <option value="shopNm">셀러샵이름</option>
                                <option value="shopUrl">셀러샵URL</option>
                                <option value="businessNm">업체명</option>
                                <option value="partnerNm">상호</option>
                                <option value="bizNo">사업자등록번호</option>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width:300px; float:left;" minlength="2">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="sellerShopTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="sellerShopListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 판매업체 정보&nbsp;</h3>
        <button id="btn_schPartner" type="button" class="ui button medium" onclick="sellerShop.getPartnerPop('sellerShop.schPartner');">업체조회</button>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>셀러샵 등록/수정 검색</caption>
                <colgroup>
                    <col width="100px">
                    <col width="*">
                    <col width="100px">
                    <col width="*">
                    <col width="100px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>판매업체ID</th>
                    <td>
                        <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-5" readonly="true" style="width:200px;">
                    </td>
                    <th>업체명</th>
                    <td>
                        <input type="text" id="schBusinessNm" name="schBusinessNm" class="ui input medium mg-r-5" readonly="true" style="width:200px;">
                    </td>
                    <th>상호</th>
                    <td>
                        <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input medium mg-r-5" readonly="true" style="width:200px;">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="sellerShopSetForm" onsubmit="return false;">
            <input type="hidden" id="partnerId" name="partnerId">
            <input type="hidden" id="shopLogoYn" name="shopLogoYn" value="N">
            <table class="ui table">
                <caption>셀러샵 등록/수정 등록/수정</caption>
                <colgroup>
                    <col width="180px">
                    <col width="350px">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>셀러샵 이름(닉네임) <span class="text-red">*</span></th>
                    <td colspan="3">
                        <input type="text" id="shopNm" name="shopNm" class="ui input medium mg-r-5" maxlength="10" style="width:200px; float: left;">&nbsp;
                        <button id="btn_validate_shop_nm" type="button" class="ui button medium" data-validation="true" onclick="sellerShop.validation('shopNm');" style="float: left;">중복확인</button>
                    </td>
                </tr>
                <tr>
                    <th>
                        셀러샵 프로필 이미지<span class="text-red">*</span><br>
                        <span style="color: red;">(해당 이미지는 추천 셀러샵 등 <br>다양한 영역에 활용 되므로 <br>정확한 사이즈로 등록해 주세요)</span>
                    </th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="profileDisplayView" style="display:none;">
                                    <button type="button" id="profileDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="profileImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="shopProfileImg" name="shopProfileImg" value=""/>
                                    <input type="hidden" id="profileChangeYn" name="profileChangeYn" value="N"/>
                                </div>
                                <div class="profileDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="profileImgRegBtn" class="ui button medium" onclick="sellerShop.img.clickFile('profile');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 1200*330<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / GIF<br>
                            </div>
                        </div>
                    </td>
                    <th>
                        <span class="text-red-star">셀러샵 로고</span>
                    </th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="logoDisplayView" style="display:none;">
                                    <button type="button" id="logoDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="logoImgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="shopLogoImg" name="shopLogoImg" value=""/>
                                    <input type="hidden" id="logoChangeYn" name="logoChangeYn" value="N"/>
                                </div>
                                <div class="logoDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="logoImgRegBtn" class="ui button medium" onclick="sellerShop.img.clickFile('logo');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 320*320<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>셀러샵 URL <span class="text-red">*</span></th>
                    <td colspan="3">
                        <input type="text" id="shopUrl" name="shopUrl" class="ui input medium mg-r-5" onkeyup="sellerShop.setHttpUrl(this.value);" maxlength="12" style="width:200px; float: left;">&nbsp;
                        <button id="btn_validate_shop_url" type="button" class="ui button medium" data-validation="true" onclick="sellerShop.validation('shopUrl');" style="float: left;">중복확인</button>
                        <div><br>
                        * PC : https://front.homeplus.co.kr/sellershop/<a id="pcHttpUrl"></a><br>
                        * MOBILE : https://mfront.homeplus.co.kr/sellershop/<a id="mobileHttpUrl"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>셀러샵 소개</th>
                    <td colspan="3">
                        <input type="text" id="shopInfo" name="shopInfo" class="ui input medium mg-r-5" maxlength="20" style="width:350px; float: left;">
                        &nbsp;&nbsp;
                        <span class="text" style="float: left;">
                             ( <span style="color:red;" id="shopInfoCnt" class="cnt">0</span>/20)
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>고객센터 정보</th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${sellerShopCsYn}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="csUseYn" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="csUseYn" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <br><br>
                        <input type="text" id="csInfo" name="csInfo" class="ui input medium mg-r-5" maxlength="100" style="width:500px;" placeholder="평일 9시부터 18시까지 ( 토/일 휴무 )">
                        <br>
                        <div>대표번호, 이메일은 판매업체 관리에서 등록한 정보로 노출됩니다.</div>
                    </td>
                </tr>
                <tr>
                    <th>셀러샵 사용여부</th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${sellerShopUseYn}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="useYn" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="useYn" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerShopMain.js?${fileVersion}"></script>
<script>
    // GNB 리스트 리얼그리드 기본 정보
    ${sellerShopListGridBaseInfo}

    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>