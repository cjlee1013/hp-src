<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<div class="com wrap-popup large popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원상태 변경</h2>
    </div>
    <table class="ui table">
        <caption>회원정보</caption>
        <colgroup>
            <col width="13%">
            <col width="20%">
            <col width="13%">
            <col width="20%">
            <col width="13%">
            <col width="20%">
        </colgroup>
        <tbody>
            <tr>
                <th>ID</th>
                <td>${partnerId}</td>
                <th>상호</th>
                <td>${partnerNm}</td>
                <th>상태</th>
                <td>${partnerStatusNm}</td>
            </tr>
        </tbody>
    </table>

    <div class="com wrap-title sub">
        <h3 class="title">상태변경</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>상태변경</caption>
            <colgroup>
                <col width="20%">
                <col width="80%">
            </colgroup>
            <tbody>
                <tr>
                    <th>회원상태</th>
                    <td>
                        <div class="ui form inline">
                            <select id="partnerStatus"  style="width: 180px" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${partnerStatusCodes}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.mcCd eq partnerStatus}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <c:if test="${(param.partnerType ne 'SELLER' and codeDto.ref2 ne 'seller_only') or (param.partnerType eq 'SELLER')}">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>변경사유</th>
                    <td>
                        <div class="ui form inline">
                            <input id="regReason" type="text" class="ui input medium mg-r-5" maxlength="40">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="ui center-button-group">
        <span class="inner">
            <button type="button" class="ui button midium btn-danger font-malgun" id="setBtn">저장</button>
            <button type="button" class="ui button midium font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">회원상태 이력</h3>
    </div>
    <div class="topline"></div>
    <div id="partnerStatusHistGrid" style="width: 100%; height: 320px;"></div>

</div>



<script>
    var partnerStatusHistGrid = {
        gridView : new RealGridJS.GridView("partnerStatusHistGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            partnerStatusHistGrid.initGrid();
            partnerStatusHistGrid.initDataProvider();
            partnerStatusHistGrid.event();
        },
        initGrid : function() {
            partnerStatusHistGrid.gridView.setDataSource(partnerStatusHistGrid.dataProvider);

            partnerStatusHistGrid.gridView.setStyles(partnerStatusHistGridBaseInfo.realgrid.styles);
            partnerStatusHistGrid.gridView.setDisplayOptions(partnerStatusHistGridBaseInfo.realgrid.displayOptions);
            partnerStatusHistGrid.gridView.setColumns(partnerStatusHistGridBaseInfo.realgrid.columns);
            partnerStatusHistGrid.gridView.setOptions(partnerStatusHistGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            partnerStatusHistGrid.dataProvider.setFields(partnerStatusHistGridBaseInfo.dataProvider.fields);
            partnerStatusHistGrid.dataProvider.setOptions(partnerStatusHistGridBaseInfo.dataProvider.options);
        },
        event : function() {
            // 그리드 선택
            partnerStatusHistGrid.gridView.onDataCellClicked = function(gridView, index) {
                seller.gridRowSelect(index.dataRow);
            };
        },
        setData : function(dataList) {
            if (dataList.length > 0) {
                partnerStatusHistGrid.dataProvider.clearRows();
                partnerStatusHistGrid.dataProvider.setRows(dataList);
            }
        }
    };

    ${partnerStatusHistGridBaseInfo}

    var roleStatusSetYn = '${roleStatusSetYn}';
    if (roleStatusSetYn == 'N') {
        alert("(회원상태변경관리) 권한이 없습니다.");
        window.open('about:blank', '_self');
    }

    $(document).ready(function(){
        partnerStatusHistGrid.init();
        partnerStatusHistGrid.setData(${partnerStatusHistList});

        $('#setBtn').click(function(){
            var partnerStatus = $("#partnerStatus").val();
            if ($.jUtil.isEmpty(partnerStatus)) {
                alert('회원상태를 선택해주세요.');
                return false;
            }

            if ($.jUtil.isEmpty($('#regReason').val())) {
                alert('변경사유를 입력해주세요.');
                return false;
            }

            var confirmMsg;
            switch (partnerStatus) {
                case "WITHDRAW" :
                    confirmMsg = "탈퇴 처리하시기전 등록된 상품/거래/클레임/정산대금이 있는지 우선 확인해 주세요. 탈퇴처리하시겠습니까?";
                    break;
                case "PENDING" :
                    confirmMsg = "승인대기 상태로 변경 시, 상품 등록 및 판매 활동을 할 수 없습니다. 승인대기 상태로 변경 하시겠습니까?";
                    break;
                case "PAUSE" :
                    confirmMsg = "일시정지 상태로 변경 시, 상품 등록 및 판매 활동을 할 수 없습니다. 일시정지 상태로 변경 하시겠습니까?";
                    break;
                default :
                    confirmMsg = "저장하시겠습니까?";
                    break;
            }

            if(confirm(confirmMsg)) {

                CommonAjax.basic({
                    url: '/partner/setStatus.json',
                    data: {
                        'partnerId': getHttpParam('partnerId'),
                        'partnerStatus': partnerStatus,
                        'regReason': $('#regReason').val()
                    },
                    method: 'post',
                    callbackFunc: function (ret) {
                        if (ret.returnCode == "0") {
                            alert(ret.returnMsg);
                            location.reload();
                        } else {
                            alert('정상적으로 처리되지 않았습니다. 관리자에게 문의주세요.');
                        }
                    }
                });
            }
        });

        $('#resetBtn').click(function(){
            $('#partnerStatus').val($("#partnerStatus option:eq(0)").val());
            $('#regReason').val('');
        });
    });


    function getHttpParam(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null) {
            return '';
        } else {
            return results[1];
        }
    }

</script>