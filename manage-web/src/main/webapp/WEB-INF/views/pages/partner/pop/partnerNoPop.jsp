<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp" />
<!-- 기본 프레임 -->
<div class="com wrap-popup small popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사업자 등록번호 조회</h2>
    </div>
    <div class="ui form inline" style="margin: 10px 0px 10px 0px;">
        <input type="text" id="partnerNo" name="partnerNo" class="ui input medium mg-r-5" style="width: 305px;height: 35px;" placeholder="사업자 등록번호 (‘-’없이 숫자만 입력)" maxlength="10">
        <button type="button" class="ui button right font-malgun" id="checkBtn">사업자번호 인증</button>
    </div>
    <span>
        사업자 등록번호 인증이 되지 않을 경우<br/>
        NICE평가정보 고객센터로 문의 바랍니다.(1600-1522)
    </span>
</div>

<script>
    $(document).ready(function(){
        $('#checkBtn').click(function(){
            var partnerNo = $('#partnerNo').val();
            if(partnerNo == '') {
                alert('사업자번호를 입력해주세요.');
                return false;
            }

            CommonAjax.basic({
                url:'/partner/checkPartnerNo.json',
                data: {
                    'partnerNo' : partnerNo.replace(/[^0-9]/g,""),
                    'partnerType' : $.jUtil.getHttpParam('partnerType'),
                },
                method:'get',
                callbackFunc:function(ret) {
                    var callback  = $.jUtil.getHttpParam('callback');
                    var returnObj = { partnerNo : partnerNo, partnerNm : ret.compName, partnerOwner : ret.repName };
                    if(callback) {
                        eval('opener.'+callback+'(returnObj);');
                    }
                    self.close();
                }
            });
        });
    });

</script>