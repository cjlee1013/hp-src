<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매업체 정산계좌 검증</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerBankCheckSchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>판매업체 정산계좌 검증</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>수정일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp; &nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerBankCheck.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerBankCheck.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerBankCheck.initSearchDate('-30d');">1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="sellerBankCheck.initSearchDate('-90d');">3개월</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="partnerId">판매업체ID</option>
                                    <option value="chgNm">수정자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="mg-r-30">
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="sellerBankCheckTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>
        <div id="sellerBankCheckListGrid" style="width: 100%; height: 900px;"></div>
    </div>
</div>


<!-- script -->
<script type="text/javascript" src="/static/js/partner/sellerBankCheckMain.js?${fileVersion}"></script>
<script>
    ${bankCheckListGridBaseInfo}

</script>