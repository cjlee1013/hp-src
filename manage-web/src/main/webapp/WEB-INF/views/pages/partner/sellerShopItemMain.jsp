<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">셀러샵 전시관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerShopItemSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>셀러샵 검색</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="30%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerShop.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 250px;" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="sellerShopTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="sellerShopListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 셀러샵 추천상품 등록/수정</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="sellerItemSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>상품정보</caption>
                <colgroup>
                    <col width="50px">
                    <col width="200px">
                </colgroup>
                <tbody>
                <tr>
                    <th>셀러ID <span class="text-red">*</span></th>
                    <td id ="sellerId">
                    </td>
                </tr>
                <tr >
                    <th>셀러명 <span class="text-red">*</span></th>
                    <td id ="sellerNm">
                    </td>
                </tr>
                <th>대표상품 노출 형식 <span class="text-red">*</span></th>
                <td id ="">
                    <c:forEach var="dispType" items="${dispType}" varStatus="status">
                        <c:set var="checked" value="" />
                        <c:if test="${dispType.ref1 eq 'df'}">
                            <c:set var="checked" value="checked" />
                        </c:if>
                        <label class="ui radio inline"><input type="radio" name="dispType" class="ui input" value="${dispType.mcCd}" ${checked}/><span>${dispType.mcNm}</span></label>
                    </c:forEach>
                </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <button class="ui button medium gray-dark mg-r-5 " id="addItemPopUp" style="float: right;margin-bottom: 10px;margin-top: 10px;">상품등록</button>


    <div id="sellerShopItemListGrid" style="width: 100%; height: 150px;margin-top: 10px;"></div>

    <div class="ui left-button-group" style="margin-top: 50px">
        <button type="button" key="top" class="ui button small mg-r-5" onclick="sellerShopItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
        <button type="button" key="bottom" class="ui button small mg-r-5" onclick="sellerShopItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
        <button type="button" class="ui button medium mg-r-5" onclick="sellerShopItem.removeCheckData();">선택삭제</button>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setSellerShopItmBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerShopItemeMain.js?${fileVersion}"></script>

<script>
    ${sellerShopListGridBaseInfo};
    ${sellerShopItemListGridBaseInfo};
</script>
