<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매업체 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>판매자 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="150px">
                        <col width="120px">
                        <col width="250px">
                        <col width="120px">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schRegStartDate" name="schRegStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schRegEndDate" name="schRegEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="seller.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="seller.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="seller.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="seller.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" style="display: none;">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사업자유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schOperatorType" style="width: 120px" name="schOperatorType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${operatorType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>회원등급</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schPartnerGrade" style="width: 100px" name="schPartnerGrade" class="ui input medium">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${partnerGrade}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>회원상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schPartnerStatus" style="width: 100px" name="schPartnerStatus" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${partnerStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 140px">
                                    <option value="businessNm">업체명</option>
                                    <option value="partnerId">판매자ID</option>
                                    <option value="partnerNo">사업자등록번호</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                            </div>
                        </td>
                        <th>OF 승인상태</th>
                        <td>
                            <select id="schOfVendorStatus" style="width: 100px" name="schOfVendorStatus" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${ofVendorStatus}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="sellerTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="sellerListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <form id="sellerForm" name="sellerForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
            <span class="pull-right">
                <button type="button" class="ui button small" id="getPartnerPwPop">PW전송</button>
                <button type="button" class="ui button small" id="getSellerHistBtn">변경이력</button>
            </span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="120px">
                    <col width="250px">
                    <col width="120px">
                    <col width="200px">
                    <col width="120px">
                    <col>
                </colgroup>
                <tbody>
                <tr>
                    <th>판매업체ID<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerId" id="partnerId" class="ui input medium mg-r-5" style="width: 150px;" maxlength="12" >
                            <button type="button" id="checkPartnerId" class="ui button small gray-dark font-malgun mg-r-5" onclick="seller.valid.checkPartnerId();">중복확인</button>
                        </div>
                    </td>
                    <th>판매업체명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="sellerInfo.businessNm" id="businessNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                    <th>OF 승인상태</th>
                    <td>
                        <span class="ui form inline mg-r-10">
                            <input type="text" name="sellerInfo.ofVendorStatusNm" id="ofVendorStatusNm" class="ui input medium mg-r-5" style="width: 70px;" readonly=true >
                        </span>
                        (vendor Code : <span id="ofVendorCd"></span>)
                    </td>
                </tr>
                <tr>
                    <th>사업자유형</th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="operatorTypeTxt" id="operatorTypeTxt"class="ui input medium mg-r-5" readonly>
                            <select id="operatorType" name="operatorType" class="ui input medium" style="width: 150px; display: none;">
                                <option value="">선택</option>
                                <c:forEach var="code" items="${operatorType}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>회원등급</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="partnerGrade" name="partnerGrade" class="ui input medium mg-r-5" style="width: 100px" disabled>
                                <c:forEach var="code" items="${partnerGrade}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                            <button type="button" class="ui button small gray-dark font-malgun" id="popupPartnerGradeBtn" disabled>변경</button>
                        </div>
                    </td>
                    <th>회원상태</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="partnerStatus" name="partnerStatus" class="ui input medium mg-r-5" style="width: 100px" disabled>
                                <c:forEach var="code" items="${partnerStatus}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                            <button type="button" class="ui button small gray-dark font-malgun" id="partnerStatusBtn" disabled>변경</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>판매유형</th>
                    <td colspan="5">
                        <span class="ui radio inline">
                            <label class="ui radio inline" style="margin-right: 10px;">
                                <input type="radio" name="sellerInfo.saleType" class="ui input" value="N" checked><span>일반</span>&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="ui radio inline" style="margin-right: 10px;">
                                <input type="radio" name="sellerInfo.saleType" class="ui input" value="D" disabled><span>직매입</span>
                            </label>
                        </span>RMS 상품번호 : <span class="ui form inline ">
                            <input type="text" name="sellerInfo.itemNo" id="itemNo" class="ui input medium mg-r-5" style="width: 130px;" maxlength="25" readonly>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>판매 상품유형</th>
                    <td colspan="5">
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" name="typesList.types" class="ui input" value="ITEM_T"><span>렌탈상품</span>
                        </label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block">
                            <input type="checkbox" name="typesList.types" class="ui input" value="ITEM_M"><span>휴대폰-가입상품</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox" name="typesList.types" class="ui input" value="ITEM_E"><span>e-ticket(상품)</span>
                        </label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 사업자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>사업자정보</caption>
                <colgroup>
                    <col width="150px">
                    <col width="300px">
                    <col width="160px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상호<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerNm" id="partnerNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="12" readonly>
                        </div>
                    </td>
                    <th>대표자명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerOwner" id="partnerOwner" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사업자 등록 번호<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" id="partnerNo" name="partnerNo" value="">
                            <input type="text" name="partnerNo_1" id="partnerNo_1" class="ui input medium"  style="width: 45px;" maxlength="3" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_2" id="partnerNo_2" class="ui input medium"  style="width: 55px;" maxlength="2" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_3" id="partnerNo_3" class="ui input medium mg-r-5"  style="width: 70px;" maxlength="5" readonly="true">
                            <button type="button" class="ui button small gray-dark font-malgun" id="getPartnerNoBtn">조회</button>
                            <button type="button" class="ui button small gray-dark font-malgun" style="display:none;" id="changePartnerNoBtn">변경</button>
                        </div>
                    </td>
                    <th>통신판매업신고번호<span class="text-red"> *</span></th>
                    <td>
                        <div id="communityNotiDiv" class="ui form inline ">
                            <input type="text" name="communityNotiNo" id="communityNotiNo" class="ui input medium mg-r-5" style="width: 200px;" maxlength="50">
                            <button type="button" id="communityNotiNoBtn" class="ui button medium mint" >자동입력</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>업태<span class="text-red"> *</span></th>
                    <td>
                        <input type="text" name="businessConditions" id="businessConditions" class="ui input medium" style="width: 150px;" maxlength="50">
                    </td>
                    <th>종목<span class="text-red"> *</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="hidden" id="bizCateCd" name="bizCateCd" >
                            <input type="text" name="bizCateCdNm" id="bizCateCdNm" class="ui input medium mg-r-5" style="width: 250px;" >
                            <button type="button" class="ui button small gray-dark font-malgun" id="getBizCateCd">검색</button>
                            <ul id="searchCategory" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 10px; left: 0px; width: 150px; display: none;"></ul>
                        </span>
                        <span id="bizCateCdListId" style="margin-top: 5px">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>주소<span class="text-red" id="spanPoint3"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="zipcode" id="zipcode" class="ui input medium mg-r-5" style="width:150px;" readonly="true">
                            <button type="button" class="ui button small" onclick="javascript:zipCodePopup('seller.setZipcode'); return false;">우편번호</button>
                            <br>
                            <input type="text" name="addr1" id="addr1" class="ui input medium mg-t-5 mg-r-5" style="width:225px;" readonly="true">
                            <input type="text" name="addr2" id="addr2" class="ui input medium mg-t-5" style="width:225px;">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 파트너정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>파트너정보</caption>
                <colgroup>
                    <col width="140px">
                    <col width="320px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>대표번호1<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="sellerInfo.phone1" id="phone1" value="">
                            <input type="text" name="phone1_1" id="phone1_1" class="ui input medium" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="phone1_2" id="phone1_2" class="ui input medium" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="phone1_3" id="phone1_3" class="ui input medium" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <th>대표번호2</th>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="sellerInfo.phone2" id="phone2" value="">
                            <input type="text" name="phone2_1" id="phone2_1" class="ui input medium" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="phone2_2" id="phone2_2" class="ui input medium" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="phone2_3" id="phone2_3" class="ui input medium" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>고객문의 연락처<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <span class="ui form inline mg-r-10">
                            <input type="hidden" name="sellerInfo.infoCenter" id="infoCenter" value="">
                            <input type="text" name="infoCenter_1" id="infoCenter_1" class="ui input medium" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="infoCenter_2" id="infoCenter_2" class="ui input medium" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="infoCenter_3" id="infoCenter_3" class="ui input medium" style="width: 55px;" maxlength="4">
                        </span>
                        <span class="text-red">
                            (배송, 환불 등 고객문의 대응의 목적으로 고객에게 제공됩니다. 고객응대가 가능한 정확한 정보를 입력해주시기 바랍니다.)
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>대표 이메일<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <span class="ui form inline mg-r-10">
                            <input type="text" name="sellerInfo.email" id="email" class="ui input medium" style="width: 200px" maxlength="30">
                        </span>
                        <span class="text-red" >
                            (홈플러스 상품상세, 주문내역에 노출됩니다. 고객응대가 가능한 정확한 정보를 입력해주시기 바랍니다.)
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>홈페이지</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="sellerInfo.homepage" id="homepage" class="ui input medium" style="width: 91%;" placeholder="http://" maxlength="100">
                        </div>
                    </td>
                    <th>회사소개 및<br>상품소개</th>
                    <td>
                        <textarea name="sellerInfo.companyInfo" id="companyInfo" class="ui input" style="width: 98%; height: 70px;" maxlength="500"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>대표 카테고리</th>
                    <td>
                        <div class="ui form inline">
                            <select name="sellerInfo.categoryCd" id="categoryCd" class="ui radio inline input" data-default="대표 카테고리" style="width:150px;"></select>
                        </div>
                    </td>
                    <th>도서/공연 소득공제 <br>&nbsp;&nbsp;제공여부</th>
                    <td>
                        <label class="ui radio inline" style="margin-right: 10px;"><input type="radio" name="sellerInfo.cultureDeductionYn" class="ui input medium" value="N" checked><span>제공안함</span></label>
                        <label class="ui radio inline" style="margin-right: 10px;"><input type="radio" name="sellerInfo.cultureDeductionYn" class="ui input medium" value="Y"><span>제공</span></label>
                        <span>
                            사업자 식별번호 :
                            <input type="text" id="cultureDeductionNo" name="sellerInfo.cultureDeductionNo" class="ui input medium mg-l-5" style="width: 120px; display: inline;" maxlength="30" readonly>
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>반품택배사</th>
                    <td>
                        <c:forEach var="codeDto" items="${returnDeliveryYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline" style="margin-right: 10px;"><input type="radio" name="sellerInfo.returnDeliveryYn" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                        </c:forEach>
                        <button type="button" id="returnDeliveryPopBtn" class="ui button medium blue sky mg-l-5">반품택배사 계약정보</button>
                    </td>
                    <th>마케팅 정보 수신</th>
                    <td>
                        SMS 수신 <span id="smsYnNm">동의함</span> | E-mail 수신 <span id="emailYnNm">동의함</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title mg-r-10">* 담당자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table">
            <table class="ui table">
                <caption>담당자정보</caption>
                <colgroup>
                    <col width="100px">
                    <col width="180px">
                    <col width="220px">
                    <col width="220px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>분류</th>
                    <th>이름<span class="text-red"> *</span></th>
                    <th>휴대폰번호<span class="text-red"> *</span></th>
                    <th>연락처</th>
                    <th>이메일<span class="text-red"> *</span></th>
                </tr>
                <tr id="managerDiv0" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="managerList[].seq" id="seq0">
                        <input type="hidden" name="managerList[].mngCd" id="mngCd0" value="AFFI">
                        영업 담당자<span class="text-red">* </span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="managerList[].mngNm" id="mngNm0" style="width: 100px;">
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngMobile" id="mngMobile0" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="mngMobile0_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="mngMobile0_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="mngMobile0_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngPhone" id="mngPhone0" value="">
                            <input type="text" class="ui input medium" name="mngPhone0_1" id="mngPhone0_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone0_2" id="mngPhone0_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone0_3" id="mngPhone0_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[].mngEmail" id="mngEmail0" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                <tr id="managerDiv1" class="managerDiv" data-mng-cd="SETT">
                    <td>
                        <input type="hidden" name="managerList[].seq" id="seq1">
                        <input type="hidden" name="managerList[].mngCd" id=mngCd1" value="SETT">
                        정산 담당자<span class="text-red"> *</span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input mg-r-5" name="managerList[].mngNm" id="mngNm1" style="width: 100px;"/>
                            <label class="ui checkbox mg-r-5">
                                <input type="checkbox" id="sameAffiManager1"><span class="text">영업담당자와 동일</span>
                            </label>
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngMobile" id="mngMobile1" value="">
                            <input type="text" class="ui input medium" name="mngMobile1_1" id="mngMobile1_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile1_2" id="mngMobile1_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile1_3" id="mngMobile1_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngPhone" id="mngPhone1" value="">
                            <input type="text" class="ui input medium" name="mngPhone1_1" id="mngPhone1_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone1_2" id="mngPhone1_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone1_3" id="mngPhone1_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[].mngEmail" id="mngEmail1" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                <tr id="managerDiv2" class="managerDiv" data-mng-cd="CS">
                    <td>
                        <input type="hidden" name="managerList[].seq" id="seq2">
                        <input type="hidden" name="managerList[].mngCd" id="mngCd2" value="CS">
                        CS 담당자<span class="text-red"> *</span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input mg-r-5" name="managerList[].mngNm" id="mngNm2" style="width: 100px;"/>
                            <label class="ui checkbox mg-r-5">
                                <input type="checkbox" id="sameAffiManager2"><span class="text">영업담당자와 동일</span>
                            </label>
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngMobile" id="mngMobile2" value="">
                            <input type="text" class="ui input medium" name="mngMobile2_1" id="mngMobile2_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile2_2" id="mngMobile2_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile2_3" id="mngMobile2_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngPhone" id="mngPhone2" value="">
                            <input type="text" class="ui input medium" name="mngPhone2_1" id="mngPhone2_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone2_2" id="mngPhone2_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone2_3" id="mngPhone2_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[].mngEmail" id="mngEmail2" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                <tr id="managerDiv3" class="managerDiv" data-mng-cd="LOGI">
                    <td>
                        <input type="hidden" name="managerList[].seq" id="seq3">
                        <input type="hidden" name="managerList[].mngCd" id="mngCd3" value="LOGI">
                        물류 담당자<span class="text-red"> *</span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input mg-r-5" name="managerList[].mngNm" id="mngNm3" style="width: 100px;"/>
                            <label class="ui checkbox mg-r-5">
                                <input type="checkbox" id="sameAffiManager3"><span class="text">영업담당자와 동일</span>
                            </label>
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngMobile" id="mngMobile3" value="">
                            <input type="text" class="ui input medium" name="mngMobile3_1" id="mngMobile3_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile3_2" id="mngMobile3_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile3_3" id="mngMobile3_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[].mngPhone" id="mngPhone3" value="">
                            <input type="text" class="ui input medium" name="mngPhone3_1" id="mngPhone3_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone3_2" id="mngPhone3_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone3_3" id="mngPhone3_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[].mngEmail" id="mngEmail3" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title mg-r-10">* 2단계 로그인 담당자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table">
            <table class="ui table" id="certificationTable">
                <caption>로그인 담당자정보</caption>
                <colgroup>
                    <col width="100px">
                    <col width="180px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>분류</th>
                    <th>이름</th>
                    <th>휴대폰번호</th>
                </tr>
                <tr id="certiManagerDiv0" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq0">
                        <input type="hidden" name="certificationManagerList[].certificationState" id="certificationState0" value="I" >
                        담당자
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="certificationManagerList[].certificationMngNm" id="certificationMngNm0" maxlength="10" style="width: 100px;">
                        </span>
                    </td>
                    <td class="text-left">
                        <div class="ui form inline">
                            <input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone0" value="">
                            <input type="text" class="ui input medium" name="certificationMngPhone0_1" id="certificationMngPhone0_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="certificationMngPhone0_2" id="certificationMngPhone0_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="certificationMngPhone0_3" id="certificationMngPhone0_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr id="certiManagerDiv1" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq1">
                        <input type="hidden" name="certificationManagerList[].certificationState" id="certificationState1" value="I" >
                        담당자
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="certificationManagerList[].certificationMngNm" id="certificationMngNm1" maxlength="10" style="width: 100px;">
                        </span>
                    </td>
                    <td class="text-left">
                        <div class="ui form inline">
                            <input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone1" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="certificationMngPhone1_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="certificationMngPhone1_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="certificationMngPhone1_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr id="certiManagerDiv2" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq2">
                        <input type="hidden" name="certificationManagerList[].certificationState" id="certificationState2" value="I" >
                        담당자
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="certificationManagerList[].certificationMngNm" id="certificationMngNm2" maxlength="10" style="width: 100px;">
                        </span>
                    </td>
                    <td class="text-left">
                        <div class="ui form inline">
                            <input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone2" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="certificationMngPhone2_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="certificationMngPhone2_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="certificationMngPhone2_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr id="certiManagerDiv3" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq3">
                        <input type="hidden" name="certificationManagerList[].certificationState" id="certificationState3" value="I" >
                        담당자
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="certificationManagerList[].certificationMngNm" id="certificationMngNm3" maxlength="10" style="width: 100px;">
                        </span>
                    </td>
                    <td class="text-left">
                        <div class="ui form inline">
                            <input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone3" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="certificationMngPhone3_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="certificationMngPhone3_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="certificationMngPhone3_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr id="certiManagerDiv4" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="certificationManagerList[].certificationSeq" id="certificationSeq4">
                        <input type="hidden" name="certificationManagerList[].certificationState" id="certificationState4" value="I" >
                        담당자
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="certificationManagerList[].certificationMngNm" id="certificationMngNm4" maxlength="10" style="width: 100px;">
                        </span>
                    </td>
                    <td class="text-left">
                        <div class="ui form inline">
                            <input type="hidden" name="certificationManagerList[].certificationMngPhone" id="certificationMngPhone4" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="certificationMngPhone4_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="certificationMngPhone4_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="certificationMngPhone4_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>



        <div class="com wrap-title sub">
            <h3 class="title mg-r-10">* 정산정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table horizontal">
                <caption>정산정보</caption>
                <colgroup>
                    <col width="150px">
                    <col width="450px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>입금계좌<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="bankCd" name="settleInfo.bankCd" class="ui input medium mg-r-5" style="width: 120px">
                                <option value="">선택</option>
                                <c:forEach var="code" items="${bankCode}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" name="settleInfo.bankAccountNo" id="bankAccountNo" class="ui input medium mg-r-10" style="width: 200px;" maxlength="70">
                            <button type="button" class="ui button small gray-dark font-malgun mg-l-10" id="getAccAuthenticationBtn">계좌인증</button>
                        </div>
                    </td>
                    <th>예금주<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" name="settleInfo.depositor" id="depositor" class="ui input medium trimText" style="width: 150px;" maxlength="70">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>정산주기<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="settleCycleType" name="settleInfo.settleCycleType" class="ui input medium mg-r-5" style="width: 120px" readonly="true">
                                <c:forEach var="code" items="${settleCycleType}" varStatus="status">
                                    <c:if test="${code.mcCd eq '1'}">
                                        <option value="${code.mcCd}">${code.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>지급여부</th>
                    <td>
                        <select id="settlePaymentYn" name="settleInfo.settlePaymentYn" class="ui input medium mg-r-5" style="width: 100px" ${SELLER_SETTLE_PAY}>
                            <c:forEach var="code" items="${settlePaymentYn}" varStatus="status">
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 첨부서류</h3>
        </div>
        <div class="topline"></div>
        <div id="partnerFileTable" class="ui wrap-table horizontal ">
            <table class="ui table">
                <caption>첨부서류</caption>
                <colgroup>
                    <col width="220px">
                    <col width="27px">
                    <col width="*">
                </colgroup>
                <tbody>
                <c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
                    <tr>
                        <th>${codeDto.mcNm}
                            <c:if test="${codeDto.ref3 eq 'red'}">
                                <span class="text-red"> *</span>
                            </c:if>
                        </th>
                        <td>
                            <button type="button" class="ui button medium sky-blue"
                                    data-file-type="${codeDto.mcCd}"
                                    data-field-name="partnerFile${status.count}"
                                    onclick="seller.file.clickFile($(this));">추가
                            </button>
                        </td>
                        <td style="line-height: 30px;">
                            <span id="uploadPartnerFileSpan_${codeDto.mcCd}" data-file-type="${codeDto.mcCd}" class="ui form inline">
                            </span>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun" id="setSellerBtn">저장</button>
                <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </form>
</div>
<div id="deliveryPop" style="display:none;">
    <form method="POST" action="/partner/popup/getReturnDeliveryPop" name="deliveryPop" target="delivery_window">
        <input type="hidden" name="callback" id="callback">
        <input type="hidden" name="deliveryInfo" id="deliveryInfo">
        <input type="hidden" name="partnerId" id="deliveryPopPartnerId">
        <input type="hidden" name="partnerNm" id="deliveryPopPartnerNm">
    </form>
</div>
<c:forEach var="codeDto" items="${partnerFileType}" varStatus="status">
    <input type="file" name="partnerFile${status.count}" data-file-id="uploadPartnerFileSpan_${codeDto.mcCd}" data-limit="${codeDto.ref1}"  style="display: none;"/>
</c:forEach>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerMain.js?${fileVersion}"></script>

<script>
	// 판매업체 리스트 리얼그리드 기본 정보
    ${sellerListGridBaseInfo}
</script>
