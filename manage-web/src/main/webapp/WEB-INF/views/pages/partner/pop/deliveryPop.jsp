<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp" />

<div class="large popup-request" style="padding:20px;">
	<div class="com wrap-title has-border">
		<h2 class="title font-malgun">반품택배사 계약 정보</h2>
	</div>
	<div class="mg-t-15 mg-b-20">
		<div class="mg-b-20" style="display: none;">
			<ul>
				<li>반품 택배사의 등록을 위해 계약한 택배사의 계약번호를 등록해주세요</li>
				<li>저장한 반품택배사가 없을 경우 직접수거로 진행됩니다.</li>
			</ul>
		</div>
		<span class="text">* 저장 시점에 바로 반영됩니다.</span>
		<form id="setForm" name="setForm" method="POST" onsubmit="return false;" novalidate="novalidate">
			<div class="ui wrap-table horizontal">
				<table class="ui table vertical" id="deliveryTb">
					<colgroup>
						<col width="120px">
						<col width="*">
					</colgroup>
					<tr>
						<th class="text-center">택배사</th>
						<th class="text-center">계약코드</th>
					</tr>
					<tbody>
					</tbody>
				</table>
			</div>
		</form>
		<span style="height:10px;display:block;"></span>
		<ul class="com-list-box sm">
			<li class="text-red">
				※ 택배사별 계약코드 복수 입력 가능(*원 출고 택배사 기준으로 반품 수거)
			</li>
		</ul>
	</div>
	<div class="ui center-button-group">
        <span class="inner">
			<button class="ui button large btn-danger" id="setDeliveryBtn">적용</button>
			<button class="ui button large" id="cancelDeliveryBtn" onclick="self.close()">취소</button>
		</span>
	</div>
</div>

<script>
	let deliveryList = ${deliveryList};
	let deliveryInfo = ${deliveryInfo};
	let callback = "${callback}";
	let partnerId = "${partnerId}";
	let partnerNm = "${partnerNm}";

	$(document).ready(function() {
		delivery.init();
	});
</script>
<script type="text/javascript" src="/static/js/partner/pop/deliveryPop.js?v=${fileVersion}"></script>