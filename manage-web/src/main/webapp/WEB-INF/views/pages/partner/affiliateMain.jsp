<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:11px;cursor: pointer;}
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">제휴업체 등록/수정</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="affiliateSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>판매자 검색</caption>
                    <colgroup>
                        <col width="130px">
                        <col width="350px">
                        <col width="120px">
                        <col width="150px">
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schRegStartDate" name="schRegStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schRegEndDate" name="schRegEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliate.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliate.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliate.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="affiliate.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" style="display: none;">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사업자유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schOperatorType" style="width: 120px" name="schOperatorType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${partnerOperatorType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>제휴유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schAffiliateType" style="width: 120px" name="schAffiliateType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${affiliateType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>회원상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schPartnerStatus" style="width: 100px" name="schPartnerStatus" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${partnerStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="affiliateNm">제휴업체명</option>
                                    <option value="partnerId">제휴업체ID</option>
                                    <option value="partnerNo">사업자등록번호</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="affiliateTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="affiliateListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <form id="affiliateForm" name="affiliateForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">• 기본정보</h3>
            <span class="pull-right">
                <button type="button" class="ui button small" id="getPartnerPwPop">PW전송</button>
            </span>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>제휴업체ID<span class="text-center text-red"> *</span></th>
                    <td colspan="2">
                        <div class="ui form inline ">
                            <span class="text">affi</span><input type="text" name="partnerId" id="partnerId" class="ui input medium mg-r-5" style="width: 150px;" maxlength="8">
                            <button type="button" id="checkPartnerId" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliate.valid.checkPartnerId();">중복확인</button>
                        </div>
                    </td>
                    <th>제휴업체명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="affiliateNm" id="affiliateNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사업자유형</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="operatorType" name="operatorType" class="ui input medium" style="width: 150px">
                                <option value="">선택</option>
                            <c:forEach var="code" items="${operatorType}" varStatus="status">
                                <option value="${code.mcCd}">${code.mcNm}</option>
                            </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>제휴유형<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <select id="affiliateType" name="affiliateType" class="ui input medium mg-r-5" style="width: 150px">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${affiliateType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>회원상태</th>
                    <td>
                        <div class="ui form inline ">
                            <select id="partnerStatus" name="partnerStatus" class="ui input medium mg-r-5" style="width: 150px" disabled>
                                <c:forEach var="code" items="${partnerStatus}" varStatus="status">
                                    <option value="${code.mcCd}">${code.mcNm}</option>
                                </c:forEach>
                            </select>
                            <button type="button" class="ui button small gray-dark font-malgun" id="partnerStatusBtn" >변경</button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 사업자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>사업자정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상호<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerNm" id="partnerNm" class="ui input medium mg-r-5" style="width: 150px;" maxlength="12" readonly>
                        </div>
                    </td>
                    <th>대표자명<span class="text-center text-red"> *</span></th>
                    <td>
                        <div class="ui form inline ">
                            <input type="text" name="partnerOwner" id="partnerOwner" class="ui input medium mg-r-5" style="width: 150px;" maxlength="50">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사업자 등록 번호<span class="text-red"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="hidden" id="partnerNo" name="partnerNo" value="">
                            <input type="text" name="partnerNo_1" id="partnerNo_1" class="ui input medium"  style="width: 45px;" maxlength="3" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_2" id="partnerNo_2" class="ui input medium"  style="width: 55px;" maxlength="2" readonly="true">
                            <span class="text">-</span>
                            <input type="text" name="partnerNo_3" id="partnerNo_3" class="ui input medium mg-r-5"  style="width: 70px;" maxlength="5" readonly="true">
                            <button type="button" class="ui button small gray-dark font-malgun" id="getPartnerNoBtn">조회</button>
                            <button type="button" class="ui button small gray-dark font-malgun" style="display:none;" id="changePartnerNoBtn">변경</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>업태<span class="text-red"> *</span></th>
                    <td>
                        <input type="text" name="businessConditions" id="businessConditions" class="ui input medium" style="width: 150px;" maxlength="50">
                    </td>
                    <th>종목<span class="text-red"> *</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="hidden" id="bizCateCd" name="bizCateCd" >
                            <input type="text" name="bizCateCdNm" id="bizCateCdNm" class="ui input medium mg-r-5" style="width: 250px;" >
                            <button type="button" class="ui button small gray-dark font-malgun" id="getBizCateCd">검색</button>
                            <ul id="searchCategory" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 10px; left: 0px; width: 150px; display: none;"></ul>
                        </span>
                        <span id="bizCateCdListId" style="margin-top: 5px">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>주소<span class="text-red" id="spanPoint3"> *</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" name="zipcode" id="zipcode" class="ui input medium mg-r-5" style="width:150px;" readonly="true">
                            <button type="button" class="ui button small" onclick="javascript:zipCodePopup('affiliate.setZipcode'); return false;">우편번호</button>
                            <br>
                            <input type="text" name="addr1" id="addr1" class="ui input medium mg-t-5 mg-r-5" style="width:225px;" readonly="true">
                            <input type="text" name="addr2" id="addr2" class="ui input medium mg-t-5" style="width:225px;">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title mg-r-10">* 담당자정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table">
            <table class="ui table">
                <caption>담당자정보</caption>
                <colgroup>
                    <col width="150px">
                    <col width="200px">
                    <col width="250px">
                    <col width="250px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>분류</th>
                    <th>이름<span class="text-red"> *</span></th>
                    <th>휴대폰번호<span class="text-red"> *</span></th>
                    <th>연락처</th>
                    <th>이메일<span class="text-red"> *</span></th>
                </tr>
                <tr id="managerDiv0" class="managerDiv" data-mng-cd="AFFI">
                    <td>
                        <input type="hidden" name="managerList[0].seq" id="seq0">
                        <input type="hidden" name="managerList[0].mngCd" id="mngCd0" value="AFFI">
                        제휴 담당자 <span class="text-red">* </span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input medium mg-r-10" name="managerList[0].mngNm" id="mngNm0" style="width: 100px;">
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[0].mngMobile" id="mngMobile0" value="">
                            <input type="text" class="ui input medium" name="mngMobile0_1" id="mngMobile0_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_2" id="mngMobile0_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile0_3" id="mngMobile0_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[0].mngPhone" id="mngPhone0" value="">
                            <input type="text" class="ui input medium" name="mngPhone0_1" id="mngPhone0_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone0_2" id="mngPhone0_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone0_3" id="mngPhone0_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[0].mngEmail" id="mngEmail0" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                <tr id="managerDiv1" class="managerDiv" data-mng-cd="SETT">
                    <td>
                        <input type="hidden" name="managerList[1].seq" id="seq1">
                        <input type="hidden" name="managerList[1].mngCd" id=mngCd1" value="SETT">
                        정산 담당자<span class="text-red"> *</span>
                    </td>
                    <td class="text-left">
                        <span class="ui form inline">
                            <input type="text" class="ui input mg-r-5" name="managerList[1].mngNm" id="mngNm1" style="width: 100px;"/>
                            <label class="ui checkbox mg-r-5">
                                <input type="checkbox" id="sameAffiManager1"><span class="text">제휴담당자와 동일</span>
                            </label>
                        </span>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[1].mngMobile" id="mngMobile1" value="">
                            <input type="text" class="ui input medium" name="mngMobile1_1" id="mngMobile1_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile1_2" id="mngMobile1_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngMobile1_3" id="mngMobile1_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="hidden" name="managerList[1].mngPhone" id="mngPhone1" value="">
                            <input type="text" class="ui input medium" name="mngPhone1_1" id="mngPhone1_1" style="width: 45px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone1_2" id="mngPhone1_2" style="width: 55px;" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" class="ui input medium" name="mngPhone1_3" id="mngPhone1_3" style="width: 55px;" maxlength="4">
                        </div>
                    </td>
                    <td>
                        <input type="text" class="ui input medium" name="managerList[1].mngEmail" id="mngEmail1" style="width: 300px;" maxlength="50">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun" id="setAffiliateBtn">저장</button>
                <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </form>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/affiliateMain.js?${fileVersion}"></script>

<script>
    // 판매업체 리스트 리얼그리드 기본 정보
    ${affiliateListGridBaseInfo}
</script>