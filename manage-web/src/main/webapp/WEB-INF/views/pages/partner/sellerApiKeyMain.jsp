<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">셀러연동 API Key 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="sellerApiKeySchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>셀러연동 API Key 관리</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="45%">
                        <col width="120px">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDt" name="schDt" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="regDt">발급일</option>
                                    <option value="expireDt">만료일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp; &nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerApiKey.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerApiKey.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sellerApiKey.initSearchDate('-30d');">1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="sellerApiKey.initSearchDate('-90d');">3개월</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>판매업체</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-5" style="width: 100px;" placeholder="판매업체ID" readonly>
                                <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input medium mg-r-5" style="width: 150px;" placeholder="판매업체명" readonly>
                                <button type="button" class="ui button medium" onclick="sellerApiKey.getPartnerPop('sellerApiKey.callBack.schPartner');" >조회</button>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width:120px; float:left;" name="schUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>호스팅업체</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schAgencyCd" style="width:120px; float:left;" name="schAgencyCd" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="mg-r-30">
            <div class="com wrap-title sub">
                <h3 class="title pull-left">• 검색결과 : <span id="sellerApiKeyTotalCount">0</span>건</h3>
                    <span id="changeUseYn" class="pull-right" style="font-size: 14px;">• 사용여부 변경
                    <button type="button" class="ui button medium" onclick="sellerApiKey.setSellerApiKeyUseYn('N')">사용안함</button>
                </span>
            </div>
            <div class="topline"></div>
        </div>
        <div id="sellerApiKeyListGrid" style="width: 100%; height: 320px;"></div>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/partner/sellerApiKeyMain.js?${fileVersion}"></script>
<script>
    ${sellerApiKeyListGridBaseInfo}

</script>