<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<div class="com wrap-popup large popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">승인상태 변경</h2>
    </div>
    <table class="ui table">
        <caption>회원정보</caption>
        <colgroup>
            <col width="13%">
            <col width="20%">
            <col width="13%">
            <col width="20%">
            <col width="13%">
            <col width="20%">
        </colgroup>
        <tbody>
            <tr>
                <th>ID</th>
                <td>${partnerId}</td>
                <th>상호</th>
                <td>${partnerNm}</td>
                <th>상태</th>
                <td>${partnerStatusNm}</td>
            </tr>
        </tbody>
    </table>

    <div class="com wrap-title sub">
        <h3 class="title">상태변경</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>상태변경</caption>
            <colgroup>
                <col width="20%">
                <col width="80%">
            </colgroup>
            <tbody>
                <tr>
                    <th>회원상태</th>
                    <td>
                        <div class="ui form inline">
                            <select id="partnerStatus"  style="width: 180px" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${approvalStatus}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.mcCd eq partnerStatus}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <c:if test="${codeDto.mcCd ne 'B'}">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>변경사유</th>
                    <td>
                        <div class="ui form inline">
                            <input id="chgReason" type="text" class="ui input medium mg-r-5" maxlength="40">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="ui center-button-group">
        <span class="inner">
            <button type="button" class="ui button midium btn-danger font-malgun" id="setBtn">저장</button>
            <button type="button" class="ui button midium font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>



<script>
    const beforePartnerStatus = '${partnerStatus}';

    $(document).ready(function(){
        $('#setBtn').click(function(){
            var partnerStatus = $("#partnerStatus").val();
            if (beforePartnerStatus == $("#partnerStatus").val()) {
                alert('승인상태를 선택해주세요.');
                return false;
            }

            if ($.jUtil.isEmpty($('#chgReason').val())) {
                alert('변경사유를 입력해주세요.');
                return false;
            }

            if(confirm("저장하시겠습니까?")) {

                CommonAjax.basic({
                    url: '/partner/setApprovalStatus.json',
                    data: {
                        'partnerId': $.jUtil.getHttpParam('partnerId'),
                        'approvalStatus': partnerStatus,
                        'chgReason': $('#chgReason').val()
                    },
                    method: 'post',
                    callbackFunc: function (ret) {
                        if (ret.returnCode == "0") {
                            alert(ret.returnMsg);
                            location.reload();
                        } else {
                            alert('정상적으로 처리되지 않았습니다. 관리자에게 문의주세요.');
                        }
                    }
                });
            }
        });

        $('#resetBtn').click(function(){
            $('#partnerStatus').val($("#partnerStatus option:eq(0)").val());
            $('#chgReason').val('');
        });
    });

</script>