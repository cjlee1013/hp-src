<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">프론트 구성요소 컨트롤</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="alimtalkSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  return;">
                <table class="ui table">
                    <caption>프론트 구성요소 컨트롤</caption>
                    <colgroup>
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>사용자 첫배송 실시간 조회</th>
                        <td>
                            <label><input type="radio" name="liveSlotOn" value="true" />On</label>
                            <label><input type="radio" name="liveSlotOn" value="false" />Off</label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/preferences/frontControlMain.js?${fileVersion}"></script>

<script>
</script>
