<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 통계</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 통계</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="10%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th rowspan="2">조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailyMileageSum.initSearchDate(-(new Date()).getDay()+1);">이번주</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailyMileageSum.initSearchDate('-1m');">1개월</button>
                            </div>
                        </td>
                        <th>마일리지 종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="mileageKind" name="mileageKind" style="width:150px;float:left;">
                                    <option value="" selected>전체</option>
                                    <c:forEach var="codeDto" items="${mileageKind}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:57px;">
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="text">~</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">마일리지 통계</h3>
            <div class="ui right-button-group mg-t-15" style="margin-top:-60px;">
                <button type="button" id="excelDownloadBtn" class="ui button medium dark-blue mg-t-5">엑셀다운</button>
            </div>
        </div>
        <div class="mg-b-20 " id="mileageListGrid" style="height: 400px; width:100%"></div>
        <div class="com wrap-title sub">
            <h3 class="title">타입별 통계</h3>
            <div class="ui right-button-group mg-t-15" style="margin-top:-60px;">
                <button type="button" id="detailExcelDownloadBtn" class="ui button medium dark-blue mg-t-5">엑셀다운</button>
            </div>
        </div>
        <div class="mg-b-20 " id="mileageDetailGrid" style="height: 400px; width:100%;"></div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/dailyMileageSum.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${mileageListBaseInfo}
    ${mileageDetailBaseInfo}

    $(document).ready(function() {
        dailyMileageSum.init();
        dailyMileageSum.bindingEvent();
        dailyMileageSumGrid.init();
        dailyMileageDetailGrid.init();
    });
</script>