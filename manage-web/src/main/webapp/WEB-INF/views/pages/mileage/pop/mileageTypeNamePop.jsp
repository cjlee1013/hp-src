<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<div class="com wrap-popup medium popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 유형 조회</h2>
    </div>
    <form name="searchForm" method="POST" onsubmit="return false;">
        <table class="ui table">
            <caption>마일리지 유형 조회</caption>
            <colgroup>
                <col width="120px">
                <col width="350px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th>유형명</th>
                    <td colspan="2">
                        <input type="text" id="mileageTypeName" name="mileageTypeName" class="ui input medium mg-r-5" style="width:300px;" maxlength="20" value="${mileageTypeName}">
                    </td>
                    <td>
                        <div style="text-align: center;" class="ui form mg-l-30">
                            <button class="ui button medium btn-danger font-malgun" id="searchBtn">검색</button>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <input type="hidden" id="mileageTypeNo" name="mileageTypeNo" value="${mileageTypeNo}">
        <div class="ui wrap-table horizontal mg-t-10">
            <div class="mg-b-20 " id="mileageListGrid" style="height: 200px; width:545px;"></div>
        </div>

        <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button large btn-danger font-malgun" id="selectBtn">선택</button>
                <button class="ui button large font-malgun" id="closeBtn" onclick="self.close()">취소</button>
            </span>
        </div>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageTypeSearchPop.js?v=${fileVersion}"></script>
<script>
    ${mileageListBaseInfo}

    $(document).ready(function() {
        mileageTypePop.init();
        mileageTypePop.bindingEvent();
        mileageTypePopGrid.init();
    });
</script>