<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">일괄 등록</h2>
    </div>

    <!-- 파일업로드 form -->
    <form id="fileUploadForm" name="fileUploadForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>파일등록</caption>
                <colgroup>
                    <col width="100px">
                    <col width="400px">
                    <col width="*">
                </colgroup>
                <tbody>
                    <tr>
                        <th>파일등록</th>
                        <td>
                            <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx, .xls" style="display: none">
                            <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly="true">
                        </td>
                        <td>
                            <button type="button" id="btnUploadFile" class="ui button medium">파일찾기</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- 엑셀양식 다운로드 -->
        <div class="mg-t-5">
            <a href="javascript:mileageDetailGrid.excelSample()" class="text-underline" style="color: #5a569a">일괄처리 엑셀양식 다운로드</a>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button large btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<div id="mileageDetailGrid" style="display:none;"></div>

<script>
    ${mileageDetailBaseInfo}
    var callBackScript = "${callBackScript}";

    $(document).ready(function() {
        mileageReqList.init();
        mileageReqList.bindingEvent();
        mileageDetailGrid.init();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageRequestExcelPop.js?v=${fileVersion}"></script>
