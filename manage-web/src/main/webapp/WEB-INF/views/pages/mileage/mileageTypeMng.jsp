<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 적립 타입 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 적립 타입 관리</caption>
                    <colgroup>
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>점포 유형</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:150px;float:left;">
                                    <option value="A" selected>전체</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>마일리지 종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="mileageKind" name="mileageKind" style="width:150px;float:left;">
                                    <option value="A" selected>전체</option>
                                    <c:forEach var="codeDto" items="${mileageKind}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용 여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="useYn" name="useYn" style="width:150px;float:left;">
                                    <option value="A" selected>전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:57px;">
                        <th>검색어</th>
                        <td colspan="5">
                            <select class="ui input medium mg-r-5" style="width:150px;float:left;" id="schDetailCode">
                                <option value="MILEAGE_TYPE_NO" selected>마일리지 번호</option>
                                <option value="MILEAGE_TYPE_NAME">마일리지 유형</option>
                                <option value="CODE">마일리지코드</option>
                            </select>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="schDetailDesc" name="schDetailDesc" class="ui input medium mg-r-5" style="width: 300px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="mileageListTotalCount">0</span>건</h3>
        </div>
        <div class="mg-b-20 " id="mileageListGrid" style="height: 400px; width:100%;"></div>

        <div id="groupRegister" class="ui wrap-table horizontal mg-t-10">
            <form id="registerForm" name="registerForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 적립 타입 등록</caption>
                    <colgroup>
                        <col width="15%">
                        <col width="30%">
                        <col width="15%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th colspan="4">
                            <div class="ui form mg-t-10">
                                <span class="text">기본 정보</span>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th>점포 유형</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schStoreType" name="storeType" style="width:150px;float:left;">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용 여부</th>
                        <td>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input small" value="Y" checked><span>사용</span></label>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input small" value="N"><span>미사용</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>마일리지 유형</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="mileageTypeName" name="mileageTypeName" class="ui input medium mg-r-5" style="width: 95%;" maxlength="20">
                            </div>
                        </td>
                        <th>마일리지 종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schMileageKind" name="mileageKind" style="width:150px;float:left;">
                                    <option value="" selected>전체</option>
                                    <c:forEach var="codeDto" items="${mileageKind}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>마일리지 코드</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="mileageCode" name="mileageCode" class="ui input medium mg-r-5" style="width: 70%;" maxlength="20">
                            </div>
                        </td>
                        <th>고객 노출 문구</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="displayMessage" name="displayMessage" class="ui input medium mg-r-5" style="width: 95%;" maxlength="20">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>마일리지 타입설명</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="mileageTypeExplain" name="mileageTypeExplain" class="ui input medium mg-r-5" style="width: 95%;" maxlength="50">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>마일리지 등록기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="regStartDt" name="regStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="regEndDt" name="regEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>마일리지 유효기간</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="expireType" name="expireType" style="width:120px;">
                                    <option value="TR" selected>등록일로부터</option>
                                    <option value="TT">기간일로부터</option>
                                </select>
                                <span id="useDtSpan">
                                    <input type="text" id="useStartDt" name="useStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="useEndDt" name="useEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                    <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </span>
                                <span id="expireDayInpSpan">
                                    <input type="text" id="expireDayInp" name="expireDayInp" class="ui input medium mg-r-5" style="width:100px;">
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align:center;">
                            <div class="ui form inline">
                                <button type="submit" id="saveBtn" class="ui button large cyan mg-t-5">저장</button>
                                <span class="text">&nbsp;&nbsp;</span>
                                <button type="button" id="resetRegBtn" class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="mileageTypeNo" name="mileageTypeNo" value="" />
                <input type="hidden" id="mileageCategory" name="mileageCategory" value="01" />
            </form>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageTypeMng.js?v=${fileVersion}"></script>
<script>
    ${mileageListBaseInfo}

    $(document).ready(function() {
        mileageTypeMng.init();
        mileageTypeMng.bindingEvent();
        mileageTypeMngGrid.init();
    });
</script>