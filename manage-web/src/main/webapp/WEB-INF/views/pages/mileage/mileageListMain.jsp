<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 내역 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 내역 조회</caption>
                    <colgroup>
                        <col width="110px">
                        <col width="400px">
                        <col width="110px">
                        <col width="220px">
                        <col width="110px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <span style="display:inline-block; float:left;">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                </span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageList.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageList.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageList.initSearchDate('0d');">오늘</button>
                            </div>
                        </td>
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="mileageCategory" name="mileageCategory" style="width:110px;float:left;">
                                    <option value="ALL" selected>전체</option>
                                    <c:forEach var="codeDto" items="${mileageCategory}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>회원번호</th>
                        <td colspan="3">
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="userNo" name="userNo" class="ui input medium mg-r-5" style="width:500px">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="mileageListTotalCount">0</span>건</h3>
            <span class="inner" style="float:right;margin-right: 10px;">
                <h3 class="title"> 사용 가능 마일리지 : <span id="mileageAmt">0</span>p / 소멸 예정 마일리지(2주이내) : <span id="expiredMileageAmt">0</span>p </h3>
            </span>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="mileageListGrid" style="height: 615px; width:100%;"></div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageListMain.js?v=${fileVersion}"></script>
<script>
    ${mileageListBaseInfo}

    $(document).ready(function() {
        mileageList.init();
        mileageList.bindingEvent();
        mileageListGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>