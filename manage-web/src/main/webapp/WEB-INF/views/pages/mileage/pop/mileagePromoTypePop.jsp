<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 마일리지 타입 조회
        </h2>
    </div>
    <div class="topline mg-t-10"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal">
        <form id="mileageTypePopForm" name="mileageTypePopForm">
            <table class="ui table" id="mileageTypePopTable">
                <caption>마일리지 타입 조회 - 마일리지 행사 관리</caption>
                <colgroup>
                    <col width="30%">
                    <col width="60%">
                    <col width="*">
                </colgroup>
                <tbody>
                    <tr>
                        <th>유형</th>
                        <td><span id="storeTypeNm"></span></td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form">
                                <button type="button" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5">
                                    <select id="schTypeCd" name="schTypeCd" class="ui input medium mg-r-5 mg-t-5" style="width:110px;float:left;">
                                        <option value="TYPE_NM" selected>마일리지유형</option>
                                        <option value="TYPE_NO">마일리지번호</option>
                                    </select>
                                    <input type="text" id="schTypeContents" name="schTypeContents" class="ui input medium mg-r-5 mg-t-5" style="width: 350px;">
                                </label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" id="storeType" name="storeType">
        </form>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="totalCnt">0</span>건</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <div class="mg-b-20" id="mileageTypeGrid" style="height: 200px; width: 100%"></div>
    </div>
    <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button large btn-danger font-malgun" id="selectBtn">선택</button>
                <button class="ui button large font-malgun" id="closeBtn" onclick="self.close()">취소</button>
            </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/pop/mileageTypePromoPop.js?v=${fileVersion}"></script>

<script>
    ${mileageTypeGridBaseInfo}
    var storeType = "${storeType}";
    var callBackScript = "${callBackScript}";
    var mileageTypeGrid = GridUtil.initializeGrid("mileageTypeGrid", mileageTypeGridBaseInfo);

    $(document).ready(function() {
        mileageTypePromoPop.init();
        CommonAjaxBlockUI.global();
    });
</script>