<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 난수번호 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 난수번호 관리</caption>
                    <colgroup>
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>난수발급기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageCouponManage.initSearchDate('-2m');">2개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageCouponManage.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageCouponManage.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:56px;">
                        <th>난수종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schCouponType" name="schCouponType" style="width:150px;float:left;">
                                    <option value="ALL" selected>전체</option>
                                    <option value="ONE">일회성</option>
                                    <option value="MULTI">다회성</option>
                                </select>
                            </div>
                        </td>
                        <th>실행여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schIssueYn" name="schIssueYn" style="width:150px;float:left;">
                                    <option value="ALL" selected>전체</option>
                                    <option value="Y">실행</option>
                                    <option value="N">실행안함</option>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <tr style="height:57px;">
                        <th>상세 검색</th>
                        <td colspan="5">
                            <select id="schDetailCode" name="schDetailCode" class="ui input medium mg-r-5" style="width:150px;float:left;">
                                <option value="MANAGE_NO" selected>번호</option>
                                <option value="COUPON_NM">제목</option>
                            </select>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="schDetailDesc" name="schDetailDesc" class="ui input medium mg-r-5" style="width: 300px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="searchCnt">0</span>건</h3>
        </div>
        <div class="mg-b-20 " id="mileageCouponGrid" style="height: 350px; width:100%;"></div>

        <div  class="com wrap-title sub mg-t-10" >
            <h3 class="title">* 기본 정보</h3>
        </div>
        <div id="groupRegister" class="ui wrap-table horizontal mg-t-10">
            <form id="registerForm" name="registerForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 난수번호 입력</caption>
                    <colgroup>
                        <col width="15%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>제목</th>
                        <td>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="couponNm" name="couponNm" class="ui input medium mg-r-5" style="width: 300px;">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="useYn" value="Y" checked><span>사용</span>
                                </label>
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="useYn" value="N"><span>사용안함</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>난수 발급기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="issueStartDt" name="issueStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="issueEndDt" name="issueEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>난수종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="couponType" name="couponType" style="width:150px;float:left;">
                                    <option value="ONE">일회성</option>
                                    <option value="MUL">다회성</option>
                                </select>
                                <button type="button" id="couponExcelDownBtn" class="ui button medium dark-blue" disabled>난수 엑셀다운</button>
                                <div id="couponNoArea" style="display: none;">
                                    <span class="text mg-l-5">일련번호</span>
                                    <input type="text" id="couponNo" name="couponNo" class="ui input medium mg-l-5" style="width: 150px;">
                                </div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>난수 발급수량</th>
                        <td>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="issueQty" name="issueQty" class="ui input medium mg-r-5" style="width: 300px;">
                            </div>
                        </td>
                        <th>지급 마일리지</th>
                        <td>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="mileageAmt" name="mileageAmt" class="ui input medium mg-r-5" style="width: 150px;">
                                <span class="text">P</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>1인 발급수량</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="personalIssueType" name="personalIssueType" style="width:80px;float:left;">
                                    <option value="D">일별</option>
                                    <option value="P">행사기간내</option>
                                </select>
                                <span class="text">횟수</span>
                                <input type="text" id="personalIssueQty" name="personalIssueQty" class="ui input medium mg-r-5" style="width: 130px;">
                            </div>
                        </td>
                        <th>마일리지 유형</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="mileageTypeNo" name="mileageTypeNo"  placeholder="id" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                                <input type="text" id="mileageTypeName" name="mileageTypeName" placeholder="유형명" class="ui input medium mg-r-5" style="width: 200px;" readonly>
                                <button type="submit" id="searchMileageTypeBtn" class="ui button medium cyan "><i class="fa fa-search"></i>조회</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>고객노출문구</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="displayMessage" name="displayMessage" class="ui input medium mg-r-5" style="width: 500px;" maxlength="20">
                            </div>
                        </td>
                    </tr>
                    <tr name="requestDetail">
                        <td colspan="4" style="text-align:center;">
                            <div class="ui form inline">
                                <button type="submit" id="saveBtn" class="ui button large cyan mg-t-5">저장</button>
                                <span class="text">&nbsp;&nbsp;</span>
                                <button type="button" id="resetRegBtn" class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="couponManageNo" name="couponManageNo" />
                <input type="hidden" id="issueYn" name="issueYn" />
            </form>
        </div>
        <div  class="com wrap-title sub" >
            <h3 class="title">* 난수번호 발급회원</h3>
        </div>
        <div class="topline"></div>
        <div class="com wrap-title horizontal mg-b-10" >
            <span class="pull-left mg-t-10">
                <button type="button" id="recoveryBtn" class="ui button medium white font-malgun mg-r-5">회수</button>
            </span>
        </div>
        <div class="mg-b-20 " id="mileageCouponUserGrid" style="height: 300px; width:100%;"></div>
    </div>
    <div class="mg-b-20 " id="mileageCouponDownGrid" style="height: 1px; width:1px;"></div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageCouponMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<script>
    ${mileageCouponGridBaseInfo}
    ${mileageCouponUserGridBaseInfo}
    ${mileageCouponDownGridBaseInfo}

    var mileageCouponGrid = GridUtil.initializeGrid("mileageCouponGrid", mileageCouponGridBaseInfo);
    var mileageCouponUserGrid = GridUtil.initializeGrid("mileageCouponUserGrid", mileageCouponUserGridBaseInfo);
    var mileageCouponDownGrid = GridUtil.initializeGrid("mileageCouponDownGrid", mileageCouponDownGridBaseInfo);

</script>