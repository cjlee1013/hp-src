<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마일리지 지급/회수 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 지급/회수 관리</caption>
                    <colgroup>
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="4%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-5" style="width:150px;float:left;">
                                    <option value="01" selected>실행예정일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="mileageReqList.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:56px;">
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schRequestType" name="requestType" style="width:150px;float:left;">
                                    <c:forEach var="code" items="${requestType}" varStatus="status">
                                        <option value="${code.code}">${code.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>마일리지 종류</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="mileageKind" name="mileageKind" style="width:150px;float:left;">
                                    <c:forEach var="codeDto" items="${mileageKind}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용 여부</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schUseYn" name="useYn" style="width:150px;float:left;">
                                    <option value="A" selected>전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:57px;">
                        <th>상세 검색</th>
                        <td colspan="5">
                            <select id="schDetailCode" name="schDetailCode" class="ui input medium mg-r-5" style="width:150px;float:left;">
                                <option value="MILEAGE_REQ_NO" selected>번호</option>
                                <option value="REQUEST_REASON">사유</option>
                                <option value="CODE">마일리지코드</option>
                            </select>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="schDetailDesc" name="schDetailDesc" class="ui input medium mg-r-5" style="width: 300px;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="mileageReqNo" name="mileageReqNo" value="">
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="mileageListTotalCount">0</span>건</h3>
        </div>
        <div class="mg-b-20 " id="mileageListGrid" style="height: 400px; width:100%;"></div>

        <div id="groupRegister" class="ui wrap-table horizontal mg-t-10">
            <form name="registerForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마일리지 지급/회수 입력</caption>
                    <colgroup>
                        <col width="15%">
                        <col width="30%">
                        <col width="10%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <td colspan="4" style="height:56px;"><b> * 기본 정보</b></td>
                    </tr>
                    <tr name="requestInfo">
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="requestTypeSelect" name="requestTypeSelect" style="width:150px;float:left;">
                                    <option value="01">지급</option>
                                    <option value="02">회수</option>
                                </select>
                            </div>
                        </td>
                        <th>실행 예정일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="processDt" name="processDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input type="hidden" id="directlyYn" name="directlyYn" value="N">
                            </div>
                        </td>
                    </tr>
                    <tr name="requestInfo">
                        <th>사유</th>
                        <td name="requestReasonTd">
                            <div class="ui form inline">
                                <input type="text" id="requestReason" name="requestReason" maxlength="30" class="ui input medium mg-r-5" style="width: 250px;">
                            </div>
                        </td>
                        <th name="requestTypeTd">마일리지 유형</th>
                        <td name="requestTypeTd">
                            <div class="ui form inline">
                                <input type="text" id="mileageTypeNo" name="mileageTypeNo" value="${mileageTypeNo}" placeholder="id" class="ui input medium mg-r-5" readonly="true" style="width: 50px;">
                                <input type="text" id="mileageTypeName" name="mileageTypeName" value="${mileageTypeName}" placeholder="유형명" class="ui input medium mg-r-5" style="width: 200px;">
                                <button type="submit" id="searchMileageTypeBtn" class="ui button medium cyan "><i class="fa fa-search"></i>조회</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr name="requestInfo">
                        <th>실행 여부</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="useYn" name="useYn" style="width:150px;float:left;">
                                    <option value="Y">실행</option>
                                    <option value="N">실행안함</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr name="requestInfo">
                        <th>상세 사유</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <textarea id="requestMessage" name="requestMessage" maxlength="200" class="ui input medium mg-r-5" style="width: 800px; height: 100px;" maxlength="200"></textarea>
                            </div>
                        </td>
                    </tr>
                    <td colspan="4" style="height:56px;"><b> * 대상 회원</b></td>
                    <tr name="requestDetail">
                        <th colspan="2">
                            <div class="ui form mg-t-10">
                                <button type="button" id="addSingle" class="ui button medium white font-malgun mg-r-5" onclick="memberSearchPopup('mileageReqList.addUserInfo', false);">대상자 추가</button>
                                <button type="button" id="addBulk" class="ui button medium white font-malgun mg-r-5">일괄 등록</button>
                                <button type="button" id="deleteAll" class="ui button medium white font-malgun mg-r-5">전체 삭제</button>
                            </div>
                        </th>
                        <th>&nbsp;</th>
                        <th> </th>
                    </tr>
                    <tr name="requestDetail">
                        <td colspan="4"><div class="mg-b-20" id="mileageDetailGrid" style="height: 300px; width:100%;" rows="5" cols="50"></div></td>
                    </tr>
                    <tr name="requestDetail">
                        <td colspan="4" style="text-align:center;">
                            <div class="ui form inline">
                                <button type="submit" id="saveBtn" class="ui button large cyan mg-t-5">저장</button>
                                <span class="text">&nbsp;&nbsp;</span>
                                <button type="button" id="resetRegBtn" class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="requestType" name="requestType" >
                <input type="hidden" id="completeDt" name="completeDt" value="${completeDt}">
                <input type="hidden" id="requestStatus" name="requestStatus">
            </form>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/mileage/mileageRequestList.js?v=${fileVersion}"></script>
<script>
    ${mileageListBaseInfo}
    ${mileageDetailBaseInfo}

    $(document).ready(function() {
        mileageReqList.init();
        mileageReqList.bindingEvent();
        mileageReqListGrid.init();
        mileageDetailGrid.init();
    });
</script>