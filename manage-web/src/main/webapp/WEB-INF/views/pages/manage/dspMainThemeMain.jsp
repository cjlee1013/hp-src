<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            홈테마 관리
        </h2>
    </div>
    <div class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspMainThemeSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) dspLeaflet.search();">
                <table class="ui table">
                    <caption>홈테마 관리 검색</caption>
                    <colgroup>
                        <col width="100px;">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <c:forEach var="codeDto" items="${dspMainPeriodType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainTheme.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainTheme.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainTheme.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="dspMainTheme.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 180px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspMainSiteType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>전시여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 180px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="themeNm">테마명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="dspMainThemeTotalCount">0</span>건</h3>
            </div>

            <div class="topline"></div>

        </div>

        <div class="mg-t-20 mg-b-20" id="dspMainThemeListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspMainThemeSetForm" onsubmit="return false;">
            <input type="hidden" id="themeId" name="themeId">

            <table class="ui table">
                <caption>마트전단 등록/수정</caption>
                <colgroup>
                    <col width="170px;">
                    <col width="*">
                    <col width="170px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트 구분&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="siteType" name="siteType" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${dskSiteGubun}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>우선순위&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input id="priority" name="priority" style="width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>테마명&nbsp;<span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input type="text" id="themeNm" name="themeNm" class="ui input medium mg-r-5" maxlength="15" style="width: 350px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간&nbsp;<span class="text-red"> *</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <select id="useYn" style="width: 180px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>

<div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
        <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
    </span>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMainThemeMain.js?${fileVersion}"></script>
<script>
    ${dspMainThemeGridBaseInfo}
</script>