<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">브랜드관 관리 </h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="brandZoneSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>브랜드관 관리</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="70%">
                        <col width="10%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="brandZone.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="brandZone.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="brandZone.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="brandZone.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" style="width: 120px" name="schType" class="ui input medium mg-r-10" >
                                    <option value="brandNm" selected>브랜드명</option>
                                    <option value="brandId" >브랜드ID</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5" style="width:120px"; maxlength="20" autocomplete="off">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="brandZoneTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="brandZoneListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 추천 브랜드관 기본정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="brandZoneSetForm" onsubmit="return false;">
            <input type="hidden" id="brandzoneNo" name="brandzoneNo" value="">
            <table class="ui table">
                <caption>브랜드관 기본정보 정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="30%">
                    <col width="10%">
                    <col width="30%">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트 구분 </th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="siteType" style="width: 150px" name="siteType" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>브랜드 코드 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="brandNo" name="brandNo" style="width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="10" readonly>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="brandZone.addBrandPop();">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>브랜드명</th>
                    <td colspan="3">
                        <span class="text" id="brandNm"></span>
                    </td>
                </tr>
                <tr>
                    <th>우선순위 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="priority" name="priority" style="width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="4">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="useYn" style="width: 150px" name="useYn" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>브랜드 로고<br>(PC) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="imgDelBtn0" onclick="brandZone.img.deleteImg('PC');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="PC" class="uploadType itemImg" inputId="uploadMNIds" data-type="PC" data-processkey="BrandZonePCImg" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="pcImgUrl" value=""/>
                                        <input type="hidden" class="imgWidth" name="pcImgWidth" value=""/>
                                        <input type="hidden" class="imgHeight" name="pcImgHeight" value=""/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id=imgRegBtn0" class="ui button medium" onclick="brandZone.img.clickFile('PC');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 66 * 66<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>

                    <th>브랜드 로고<br>(Mobile) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="imgDelBtn1" onclick="brandZone.img.deleteImg('MOBILE');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="MOBILE" class="uploadType itemImg" inputId="uploadMNIds" data-type="MOBILE" data-processkey="BrandZoneMobileImg" style="cursor:hand;">

                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="mobileImgUrl" value=""/>
                                        <input type="hidden" class="imgWidth" name="mobileImgWidth" value=""/>
                                        <input type="hidden" class="imgHeight" name="mobileImgHeight" value=""/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="imgRegBtn1" class="ui button medium" onclick="brandZone.img.clickFile('MOBILE');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 72 * 72<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>상품노출</th>
                    <td colspan="3">
                        <div style="text-align: right"> * 브랜드관 노출 상품은 최소 5개 이상 등록되어야 합니다.</div>
                        <div class="ui form inline">
                            <div class="mg-t-20 mg-b-20" id="brandZoneItemListGrid" style="width: 100%; height: 350px;"></div>

                            <span class="pull-right mg-b-10">
                            <button type="submit" id="schSellerItemPop" onclick="brandZone.addItemPopUp()" class="ui button medium gray-dark ">상품조회</button>
                        </span>
                        </div>

                        <table class="ui table-inner">
                            <input type="hidden" id="mallTypeNm" name="mallTypeNm">
                            <tbody>
                            <colgroup>
                                <col width="7%">
                                <col width="15%">
                                <col width="7%">
                                <col width="20%">
                                <col width="7%">
                                <col width="15%">
                                <col width="7%">
                                <col width="20%">
                            </colgroup>
                            <tr>
                                <th>상품번호</th>
                                    <td id="itemNo"></td>
                                <th>상품명</th>
                                    <td id="itemNm"></td>
                            </tr>
                            <tr>
                                <th>우선순위</th>
                                <td>
                                    <div class="ui form inline">
                                        <input id="itemPriority" name="itemPriority" type="text" class="ui input medium mg-r-5" style="width: 150px" maxlength="4">
                                    </div>
                                </td>
                                <th>사용여부</th>
                                <td colspan="6">
                                    <div class="ui form inline">
                                        <select id="itemUseYn" name="itemUseYn" class="ui input medium mg-r-5" style="width: 120px">
                                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="ui form inline" style="margin-top: 10px;float: right;">
                            <button type="button" id="resetBrandZoneItem" class="ui button medium mg-r-5 gray-dark pull-left">초기화</button>
                            <button type="submit" id="addBrandZoneItem" class="ui button medium mg-r-5 gray pull-left">추가</button>
                            <button type="submit" id="updateBrandZoneItem" class="ui button medium mg-r-5 gray pull-left">저장</button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBrandZoneBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
    </div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="brandZoneImgFile" style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/brandZoneMain.js?${fileVersion}"></script>

<script>
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
    //스티커 리스트 리얼그리드 기본 정보
    ${brandZoneListGridBaseInfo}
    ${brandZoneItemListGridBaseInfo}

</script>
