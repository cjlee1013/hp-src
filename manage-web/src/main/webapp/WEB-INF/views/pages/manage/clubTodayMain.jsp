<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">클럽투데이 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="clubTodaySearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>클럽투데이 검색</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="750px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <c:forEach var="codeDto" items="${dspClubPeriodType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="clubToday.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="clubToday.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="clubToday.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="clubToday.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시여부</th>
                        <td>
                            <select id="searchDispYn" name="searchDispYn" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${dspClubDispType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <c:forEach var="codeDto" items="${dspClubSchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 350px;" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="clubTodayTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="clubTodayListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 투데이 기본 정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="clubTodaySetForm" onsubmit="return false;">
            <input type="hidden" name="todayNo" id="todayNo" value="">
            <table class="ui table">
                <caption>투데이 기본 정보</caption>
                <colgroup>
                    <col width="130px">
                    <col width="300px">
                    <col width="130px">
                    <col width="300px">
                </colgroup>
                <tbody>
                <tr>
                    <th>클럽투데이 명 <span class="text-red">*</span></th>
                    <td colspan="3" >
                        <div class="ui form inline">
                            <input id="todayNm" name="todayNm" type="text" class="ui input medium mg-r-5" style="width: 300px;">
                            <span class="text">
                                         ( <span style="color:red;" id="todayNmCnt" class="cnt">0</span>/20)
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="3" >
                       <span class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>전시순서 <span class="text-red">*</span><br></th>
                    <td >
                        <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="6" >
                    </td>
                    <th>전시여부 <span class="text-red">*</span></th>
                    <td>
                        <select id="dispYn" style="width: 180px" name="dispYn" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dspClubDispType}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>노출 타이틀 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="todayTitle" name="todayTitle" class="ui input medium mg-r-5" style="width: 500px;" >
                            <span class="text">
                                 ( <span style="color:red;" id="todayTitleCnt" class="cnt">0</span>/20)
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>배너(PC) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="clubToday.img.deleteImg('PCBANNER');">삭제</button>
                                    <div id="PCBANNER" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" id ="pcImgUrl" name="pcImgUrl" value="">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="clubToday.img.clickFile('PCBANNER');">등록</button>
                                </div>

                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 520*360<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / GIF<br>
                            </div>
                        </div>
                    </td>
                    <th>배너(모바일) <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm"  onclick="clubToday.img.deleteImg('MOBILEBANNER');">삭제</button>
                                    <div id="MOBILEBANNER" class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" id="mobileImgUrl" name="mobileImgUrl" value="">
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="clubToday.img.clickFile('MOBILEBANNER');">등록</button>
                                </div>

                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 750*360<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / GIF<br>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui horizontal mg-t-10"  >
        <div class="com wrap-title sub">
            <h3 class="title">• 상품정보</h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-1 0">

                <table class="ui table">
                    <tbody>
                        <tr>
                            <th style="width: 150px;">
                                상품등록 <span class="text-red">*</span>
                            </th>
                            <td>
                                <span class="ui left-button-group">
                                    <button type="button" key="top" class="ui button small mg-r-5" onclick="clubTodayItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="bottom" class="ui button small mg-r-5" onclick="clubTodayItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="clubTodayItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
                                    <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="clubTodayItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
                                    <button type="button" class="ui button medium mg-r-5" onclick="clubTodayItem.removeCheckData();">선택삭제</button>
                                </span>
                                <span class="ui right-button-group">
                                      <button class="ui button medium gray-dark mg-r-5" id="addItemPopUp">추가</button>
                                </span>
                                <div id="clubTodayItemListGrid" style="width: 100%; height: 150px;margin-top: 10px;"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>

    </div>


    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setClubTodayBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/clubTodayMain.js?${fileVersion}"></script>

<script>
    ${clubTodayListGridBaseInfo};
    ${clubTodayItemListGridBaseInfo};
    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
