<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" id="popTitle">
         대상 상품 일괄 등록
        </h2>
        <ul class="mg-t-10">
            <li id="liTitle"></li>
        </ul>
    </div>

    <!-- 파일업로드 form -->
    <form id="fileUploadForm" name="fileUploadForm" method="POST">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <div class="mg-t-10">
                <span class="text">• 등록할 대상상품의 <b>상품번호를 등록하세요.</b> 엑셀 파일 업로드와 동시에 상품번호를 체크하여, 정상 입력건은 등록되고 오류 입력 건은 제외 처리하여 등록되지 않습니다. </span>
            </div>
            <div>
                <span class="text">• <b>1회 최대 1,000개까지 일괄 등록 가능하며,</b> 1,000개 초과 입력된 상품은 일괄 등록 대상에서 제외 처리하여 등록되지 않습니다. </span>
            </div>

            <table class="ui table">
                <caption>파일등록</caption>
                <colgroup>
                    <col width="20%">
                    <col width="70%">
                    <col width="10%">
                </colgroup>
                <tbody>
                    <tr id="trTemplate">
                        <th>업로드양식</th>
                        <td>
                            <button type="button" id="downloadTemplate" class="ui button medium btn-warning"><i class="fa fa-search"></i>다운로드</button><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>파일등록</th>
                        <td>

                            <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="uploadStickerApplyPop.getFile($(this))" style="display: none">
                            <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly>
                        </td>
                        <td>
                            <button type="button" id="schUploadFile" class="ui button medium">파일찾기</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="selectBtn" class="ui button xlarge btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript = "${callBackScript}";
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/pop/uploadStickerApplyPop.js?v=${fileVersion}"></script>
