<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">배너관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="bannerSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>배너관리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <option value="regDt">등록일</option>
                                    <option value="dispDt">전시일</option>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="banner.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="banner.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="banner.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="banner.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 180px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>디바이스</th>
                        <td colspan="1">
                            <div class="ui form inline">
                                <select id="searchDevice" style="width: 180px" name="searchDevice" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchLoc1Depth" style="width: 120px; float: left;" name="searchLoc1Depth" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${bannerLoc1dpeth}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select class="ui input medium mg-r-5" id="searchLoc2Depth" name="searchLoc2Depth"
                                        style="width:150px;float:left;" data-default="대분류"
                                        onchange="javascript:banner.changeCategoryCustomSelectBox('search', 1, 'searchLoc2Depth', 'searchLoc3Depth');">
                                </select>
                                <select class="ui input medium mg-r-5" id="searchLoc3Depth" name="searchLoc3Depth"
                                        style="width:150px;float:left;" data-default="중분류">
                                </select>
                            </div>
                        </td>
                        <th>전시여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDispYn" style="width: 180px" name="searchDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="bannerNm">배너명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="bannerTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="bannerListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 배너기본정보</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="bannerSetForm" onsubmit="return false;">
            <input type="hidden" id="bannerNo" name="bannerNo">
            <table class="ui table">
                <caption>베너 등록/수정</caption>
                <colgroup>
                    <col width="150px">
                    <col width="350px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트구분 <span class="text-red">*</span></th>
                    <td>
                        <select id="siteType" style="width: 180px" name="siteType" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>전시위치 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <select id="loc1Depth" style="width: 180px; float: left;" name="loc1Depth" class="ui input medium mg-r-10" >
                            <option value="">선택</option>
                            <c:forEach var="codeDto" items="${bannerLoc1dpeth}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <select class="ui input medium mg-r-5" id="loc2Depth" name="loc2Depth"
                                style="width:150px;float:left;" data-default="대분류"
                                onchange="javascript:banner.changeCategoryCustomSelectBox('set', 1, 'loc2Depth', 'loc3Depth');">
                        </select>
                        <select class="ui input medium mg-r-5" id="loc3Depth" name="loc3Depth"
                                style="width:150px;float:left;" data-default="중분류">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>디바이스&nbsp;<span class="text-red">*</span></th>
                    <td colspan="3">
                        <select id="device" name="device" style="width: 180px"  class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>전시여부 <span class="text-red">*</span></th>
                    <td>
                        <select id="dispYn" style="width: 180px" name="dispYn" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>전시순서 <span class="text-red">*</span></th>
                    <td>
                        <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="6" >
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="3">
                            <span class="ui form inline">
                                <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            </span>
                    </td>
                </tr>
                <tr id="displayTimeTr" style="display: none;">
                    <th>전시구간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="dispTimeYn" name="dispTimeYn" class="ui input medium mg-r-5" style="width: 100px;" >
                                <c:forEach var="dispTimeYn" items="${useYn}" varStatus="status">
                                    <option value="${dispTimeYn.mcCd}" <c:if test="${dispTimeYn.mcCd eq 'N'}">selected</c:if>>${dispTimeYn.mcNm}</option>
                                </c:forEach>
                            </select>
                            <span id="displayTimeSpan" style="display: none;">
                                <input type="text" id="dispStartTime" name="dispStartTime" class="ui input medium mg-r-5" style="width: 60px;" maxlength="5"><span class="text">~</span>
                                <input type="text" id="dispEndTime" name="dispEndTime" class="ui input medium mg-l-5 mg-r-5" style="width: 60px;" maxlength="5"><span class="text">(시:분)</span>
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>배너명 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="bannerNm" name="bannerNm" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="20" >
                            <span class="text">( <span style="color:red;" id="bannerNmCnt">0</span> / 20자 )</span>
                        </div>
                    </td>
                </tr>
                <tr id="bannerTr" style="display: none;">
                    <th>배너설정 <span class="text-red">*</span></th>
                    <td colspan="2">
                        <select id="template" style="width: 100px;float: left" name="template" class="ui input medium mg-r-10" >
                            <option value="1">1단</option>
                            <option value="2">2단</option>
                            <option value="3">3단</option>
                        </select>
                    </td>
                </tr>
                <%--빅배너 1단 입력 창 시작--%>
                <tr class ="bigBannerDiv1">
                    <input type ="hidden" name="linkList[].linkNo" value="1"/>
                    <th rowspan="3">이미지1 <span class="text-red">*</span></th>
                    <td colspan="3" id="bannerTdTxt1">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N" class="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly >
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly >
                        </div>
                    </td>
                    <td rowspan="3" id="bannerTdTxt2">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="bannerCommon.resetLinkInfo('1')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv1">
                    <td colspan="3">
                        <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype1"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo1">
                            <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                            <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                        <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="bannerCommon.popLink(this);" >검색</button>
                        </div>
                        <%--<input type ="hidden" name="linkList[].linkNo" value="1"/>--%>
                        </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv1">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                        <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDivImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDivImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--빅배너 1단 입력 창 끝--%>
                <%--빅배너 2단 입력 창 시작--%>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="2"/>
                    <th rowspan="3">이미지2 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly >
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly >
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="bannerCommon.resetLinkInfo('2')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype2"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo2">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="bannerCommon.popLink(this);" >검색</button>
                        </div>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                        <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDivImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDivImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--빅배너 2단 입력 창 끝--%>
                <%--빅배너 3단 입력 창 시작--%>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="3"/>
                    <th rowspan="3">이미지3 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 200px;float: left;margin-right: 20px;" maxlength="20" readonly >
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 200px;float: left" maxlength="20" readonly >
                        </div>
                    </td>
                    <td  rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="bannerCommon.resetLinkInfo('3')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype3"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo3">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="bannerCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                        <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDivImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDivImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너3단 입력 창 끝--%>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setPcMainBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>


<%--링크 관련 셀렉트 옵션 및 입력창 관리--%>
<div id ="linkCommon" style="display: none">
    <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
    <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
    <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="bannerCommon.popLink(this);">검색</button>
</div>

<div id ="linkUrl" style="display: none">
    <input name="linkList[].linkInfo" style="width: 600px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" value="http://">
</div>

<div id ="linkGnbDevice" style="display: none">
    <select style="width: 100px;float: left"  name="linkList[].gnbDeviceType" class="ui input medium mg-r-10 gnbDeviceType">
        <c:forEach var="codeDto" items="${dspMainGnbType}" varStatus="status">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeHome" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeClub" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id = "dlvStoreTypeClub" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'CLUB'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>
<div id = "dlvStoreTypeHome" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'HYPER'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>


<div id = "linkTypeHome" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 eq 'HOME'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>
<div id = "linkTypeClub" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref2 eq 'CLUB'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>

<%--링크 관련 셀렉트 옵션 및 입력창 관리 끝--%>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/bannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>

<script>
    ${bannerListGridBaseInfo}
    var hmpImgUrl = '${homeImgUrl}';
</script>