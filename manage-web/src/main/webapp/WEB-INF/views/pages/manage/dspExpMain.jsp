<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">EXPRESS 메인관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="expMainSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>EXPRESS 메인관리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <option value="REGDT">등록일</option>
                                    <option value="DISPDT">전시일</option>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expMain.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="expMain.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan"><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시구분</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchLoc" style="width: 120px; float: left;" name="searchLoc" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${expDisplayType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 180px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="dispId">전시번호</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                    <option value="manageNm">관리명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="expMainTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="expMainListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 전시구분 등록/수정</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="expMainSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>전시구분 등록/수정</caption>
                <colgroup>
                    <col width="120px;">
                    <col width="450px;">
                    <col width="120px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>전시번호</th>
                    <td colspan="4">
                        <input id="dispId" name="dispId" type="text" class="ui input medium mg-r-5" style="width: 150px;" readonly >
                    </td>
                </tr>
                <tr>
                    <th>전시구분</th>
                    <td colspan="4">
                        <c:forEach var="codeDto" items="${expDisplayType}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="dispType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="dispType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <span class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>관리명 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="manageNm" name="manageNm" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="15">
                            <span class="text">( <span style="color:red;" id="manageNmCnt">0</span> / 15자 )</span>
                        </div>
                    </td>
                </tr>
                <tr id="themeMainTr">
                    <th>테마명(메인) <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="themeNm" name="themeNm" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="15">
                            <span class="text">( <span style="color:red;" id="themeNmCnt">0</span> / 15자 )</span>
                        </div>
                    </td>
                </tr>
                <tr id="themeSubTr">
                    <th>테마명(서브) <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="themeSubNm" name="themeSubNm" type="text" class="ui input medium mg-r-5" style="width: 250px;" maxlength="20">
                            <span class="text">( <span style="color:red;" id="themeSubNmCnt">0</span> / 20자 )</span>
                        </div>
                    </td>
                </tr>
                <tr id="tabTr" style="display: none;">
                    <th>텝 노출명 <span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input id="tabNm" name="tabNm" type="text" class="ui input medium mg-r-5" style="width: 150px;" maxlength="6">
                            <span class="text">( <span style="color:red;" id="tabNmCnt">0</span> / 6자 )</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="useYn" name="useYn" class="ui input medium mg-r-5" style="width: 120px">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>우선순위&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 120px;">
                        </div>
                    </td>
                </tr>
                <tr id="imgTr">
                    <th>이미지 등록(PC)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="displayView" style="display:none;">
                                    <button type="button" id="imgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="imgUrl" name="imgUrl" value=""/>
                                </div>
                                <div class="displayReg" style="padding-top: 50px;">
                                    <button type="button" id="imgRegBtn" class="ui button medium" onclick="expMain.img.clickFile('pc');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 1200*600<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG<br>
                            </div>
                        </div>
                    </td>
                    <th>이미지 등록(M)</th>
                    <td colspan="2">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="mDisplayView" style="display:none;">
                                    <button type="button" id="mImgDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <img id="mimgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" id="mimgUrl" name="mimgUrl" value=""/>
                                </div>
                                <div class="mDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="mImgRegBtn" class="ui button medium" onclick="expMain.img.clickFile('mobile');">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 750*380<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG, JPEG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 대상상품 등록</h3>
    </div>

    <span class="text pull-left" style="margin-top: 5px;">* 상품건수 : <span id="itemTotalCount">0</span>건</span>

    <span class="ui form inline mg-l-10 pull-right" style="margin-bottom: 5px;">
        <span class="text ">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
        <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
    </span>

    <div class="mg-t-20 mg-b-20" id="expMainItemListGrid" style="width: 100%; height: 350px;"></div>

    <span class="ui left-button-group">
        <button type="button" key="top" class="ui button small mg-r-5" onclick="expMainItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
        <button type="button" key="bottom" class="ui button small mg-r-5" onclick="expMainItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
        <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="expMainItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
        <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="expMainItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
    </span>

    <span class="ui right-button-group" style="font-size: 13px;">
        <button type="button" class="ui button medium" onclick="expMainItem.excelDownload();">엑셀다운</button>
        <button type="button" class="ui button medium" onclick="expMainItem.openItemReadPopup();">일괄등록</button>
        <button type="button" class="ui button medium" id="addItemPopUp">상품조회</button>
        <li style="margin-left : 40px; margin-right: 5px; margin-top: 3px; float: right;">* 상품 전시영역</li>
    </span>

    <span class="ui right-button-group" style="font-size: 13px;">
        <button type="button" class="ui button medium" onclick="expMainItem.chgDispYn('N');">전시안함</button>
        <button type="button" class="ui button medium" onclick="expMainItem.chgDispYn('Y');">전시</button>
        <li style="margin-right: 5px; margin-top: 3px; float: right;">* 전시여부 변경</li>
    </span>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspExpMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>

<script>
    ${expMainListGridBaseInfo};
    ${expMainItemListGridBaseInfo};
    var hmpImgUrl = '${homeImgUrl}';
</script>