<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">PC 메인관리 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspMainSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>PC 메인관리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="15%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <c:forEach var="codeDto" items="${dspMainPeriodType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPcMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPcMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPcMain.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="dspPcMain.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 180px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspMainSiteType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>전시여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDispYn" style="width: 180px" name="searchDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${displayYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchLoc1Depth" style="width: 180px" name="searchLoc1Depth" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspMainLoc}" varStatus="status">
                                        <c:if test="${codeDto.ref2 eq 'P' }">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <c:forEach var="codeDto" items="${dspMainSchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="dspPcMainTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="dspPcMainListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 배너기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspPcMainSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>pc 메인 등록/수정</caption>
                <colgroup>
                    <col width="150px">
                    <col width="350px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                    <tr>
                        <th>번호</th>
                        <td>
                            <input id="mainNo" name="mainNo" type="text" class="ui input medium mg-r-5" style="width: 200px;" readonly >
                        </td>
                        <th>사이트구분 <span class="text-red">*</span></th>
                        <td>
                            <select id="siteType" style="width: 180px" name="siteType" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${dspMainSiteType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <select id="loc1Depth" style="width: 180px;float: left;" name="loc1Depth" class="ui input medium mg-r-10" >
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${dspMainLoc}" varStatus="status">
                                    <c:if test="${codeDto.ref2 eq 'P' }">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <select id="loc2Depth" style="width: 180px" name="loc2Depth" class="ui input medium mg-r-10" disabled>
                                <option value="">-</option>
                                <c:forEach var="codeDto" items="${dspMainLoc}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>전시여부 <span class="text-red">*</span></th>
                        <td>
                            <select id="dispYn" style="width: 180px" name="dispYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${displayYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>전시순서 <span class="text-red">*</span></th>
                        <td>
                            <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="6" >
                        </td>
                    </tr>

                    <tr>
                        <th>전시기간 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <span class="ui form inline">
                                <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            </span>
                        </td>
                    </tr>


                    <tr>
                        <th>배너명 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input id="bannerNm" name="bannerNm" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="20" >
                                <span class="text">
                                     ( <span style="color:red;" id="bannerNmCnt" class="cnt">0</span>/20)
                                </span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>


 <%--   <div id ="bannerDesc" ></div>--%>
    <%--/배너 상세 정보 역영/--%>
    <div id ="bannerTemplateTop" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainTopDetail.jsp"/>

    </div>
    <div id ="bannerTemplateLogo" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainLogoDetail.jsp"/>

    </div>

    <div id ="bannerTemplateGnb" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainGnbDetail.jsp"/>

    </div>
    <div id ="bannerTemplateGcate" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainGcateDetail.jsp"/>

    </div>
    <div id ="bannerTemplateBig" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainBigDetail.jsp"/>

    </div>
    <div id ="bannerTemplateQuick" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainQuickDetail.jsp"/>

    </div>
    <div id ="bannerTemplateCenter" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainCenterDetail.jsp"/>

    </div>

    <div id ="bannerTemplateLwing" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainLwingDetail.jsp"/>

    </div>
   <%-- <div id ="gnbBannerDiv" >
        <jsp:include page="/WEB-INF/views/pages/manage/dspPcMainDetail/dspPcMainGnbDetail.jsp">
            <jsp:param name="claimType" value="X"/>
            <jsp:param name="claimTypeName" value="교환"/>
        </jsp:include>
    </div>--%>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setPcMainBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


<%--링크 관련 셀렉트 옵션 및 입력창 관리--%>
<div id ="linkCommon" style="display: none">
    <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
    <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
    <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);">검색</button>
</div>

<div id ="linkUrl" style="display: none">
    <input name="linkList[].linkInfo" style="width: 600px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" value="http://"/>
</div>


<div id = "storeTypeHome" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeClub" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>



<div id = "dlvStoreTypeClub" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'CLUB'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>
<div id = "dlvStoreTypeHome" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'HYPER'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>


<div id = "linkTypeHome" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 ne 'MOBILE' }">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>
<div id = "linkTypeClub" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 ne 'HOME'  && codeDto.ref1 ne 'MOBILE'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>

<%--링크 관련 셀렉트 옵션 및 입력창 관리 끝--%>

<div style="display:none;" class="storePop" >
    <form id="storeSelectPopForm" name="storeSelectPopForm" method="post" action="/common/popup/storeSelectPop">
        <input type="text" name="storeTypePop" value="HYPER"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" name="setYn" value="Y"/>
        <input type="text" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="dspMainCommon.storePopupCallback"/>
    </form>
</div>



<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainTop.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainBig.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainGnb.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainQuick.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainLogo.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainGcate.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainCenter.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPcMainDetail/dspPcMainLwing.js?${fileVersion}"></script>

<script>
    // 판매업체 리스트 리얼그리드 기본 정보
    ${dspPcMainListGridBaseInfo}

    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
