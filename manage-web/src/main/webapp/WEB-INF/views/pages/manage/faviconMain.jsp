<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">파비콘관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="faviconSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>파비콘검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="350px">
                        <col width="120px">
                        <col width="100px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>전시기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 100px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>디바이스</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDevice" style="width: 100px" name="searchDevice" class="ui input medium">
                                    <c:forEach var="codeDto" items="${faviconDevice}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 100px" name="searchUseYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="bannerNm" selected>배너명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 200px;" minlength="2" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="faviconTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="faviconListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 파비콘 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="faviconSetForm" onsubmit="return false;">
            <input type="hidden" id="faviconNo" name="faviconNo">
            <table class="ui table">
                <caption>파비콘 정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="30%">
                    <col width="10%">
                    <col width="30%">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트구분</th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline" style="margin-right: 10px">
                                <input type="radio" name="siteType" class="ui input medium" value="${codeDto.mcCd}" ${checked} ><span>${codeDto.mcNm}</span>
                            </label>
                        </c:forEach>
                    </td>
                </tr>

                <tr>
                    <th>배너명 <span class="text-red">*</span></th>
                    <td colspan="5">
                        <div class="ui form inline">
                            <input id="bannerNm" name="bannerNm" style="width: 200px;" type="text" class="ui input medium mg-r-5" maxlength="20">
                            <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 20 )</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>디바이스</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="device" name="device" class="ui input medium mg-r-10" style="width: 180px">
                                <c:forEach var="codeDto" items="${faviconDevice}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="useYn" style="width: 180px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                <!--pc image -->
                <tr class="pcImgArea">
                    <th>이미지 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="imgPcDelBtn" onclick="favicon.img.deleteImg('PC');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="PC" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="PC"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="PC"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="favicon.img.clickFile('PC');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 16 * 16<br>
                                용량 : 2MB 이하<br>
                                확장자 : ICO<br>
                            </div>

                        </div>
                     </td>
                </tr>

                <!--ios image -->
                <tr class="iosImgArea">
                    <th>이미지<br>(아이폰 3G이하)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="iosDelBtn0" onclick="favicon.img.deleteImg('IP3G_LESS');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="IP3G_LESS" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="IP3G_LESS"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="N"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="IOS"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="favicon.img.clickFile('IP3G_LESS');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 57 * 57<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>

                            </div>
                        </div>

                    <th>이미지 <span class="text-red">*</span><br>(아이폰 레티나 이상)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="iosDelBtn1" onclick="favicon.img.deleteImg('IPR_MORE');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="IPR_MORE" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="IPR_MORE"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="IOS"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="ios2RegImageBtn" class="ui button medium" onclick="favicon.img.clickFile('IPR_MORE');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 120 * 120<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="iosImgArea">
                    <th>이미지<br>(아이패드 4이하)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="iosDelBtn2" onclick="favicon.img.deleteImg('IPAD4_LESS');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="IPAD4_LESS" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="IPAD4_LESS"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="N"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="IOS"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="ios3RegImageBtn" class="ui button medium" onclick="favicon.img.clickFile('IPAD4_LESS');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 76 * 76<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>

                    </td>
                    <th>이미지 <span class="text-red">*</span><br>(아이패드 레티나 이상)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="iosDelBtn3" onclick="favicon.img.deleteImg('IPADR_MORE');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="IPADR_MORE" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="IPADR_MORE"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="IOS"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="favicon.img.clickFile('IPADR_MORE');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 152 * 152<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>

                <!--aos image -->
                <tr class="aosImgArea">
                    <th>이미지<br>(HDPI)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="aosDelBtn0" onclick="favicon.img.deleteImg('HDPI');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id ="HDPI" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="HDPI"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="N"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="ANDROID"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="favicon.img.clickFile('HDPI');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 72 * 72<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                    <th>이미지 <span class="text-red">*</span><br>(XHDPI)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="aosDelBtn1" onclick="favicon.img.deleteImg('XHDPI');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="XHDPI" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="XHDPI"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="ANDROID"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="aos2RegImageBtn" class="ui button medium" onclick="favicon.img.clickFile('XHDPI');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 96 * 96<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                         </div>
                    </td>
                </tr>
                <tr class="aosImgArea">
                    <th>이미지 <span class="text-red">*</span><br>(XXHDPI)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="aosDelBtn2" onclick="favicon.img.deleteImg('XXHDPI');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="XXHDPI" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="XXHDPI"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="ANDROID"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium" onclick="favicon.img.clickFile('XXHDPI');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 144 * 144<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>

                    <th>이미지 <span class="text-red">*</span><br>(XXHDPI)</th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="aosDelBtn3" onclick="favicon.img.deleteImg('XXXHDPI');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="XXXHDPI" class="uploadType itemImg" inputId="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="faviconImg[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="faviconImg[].imgUrl" value=""/>
                                        <input type="hidden" class="imgNm" name="faviconImg[].imgNm" value=""/>
                                        <input type ="hidden" class="imgType" name="faviconImg[].imgType" value="XXXHDPI"/>
                                        <input type="hidden" class="useYn" name="faviconImg[].useYn" value="Y"/>
                                        <input type="hidden" class="required" name="faviconImg[].required" value="Y"/>
                                        <input type="hidden" class="device" name="faviconImg[].device" value="ANDROID"/>
                                    </div>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button"  class="ui button medium" onclick="favicon.img.clickFile('XXXHDPI');">등록</button>
                                </div>
                            </div>

                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                사이즈 : 196 * 196<br>
                                용량 : 2MB 이하<br>
                                확장자 : PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setFaviconBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="faviconImgFile" style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/faviconMain.js?${fileVersion}"></script>

<script>
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
    //파비콘 리스트 리얼그리드 기본 정보
    ${faviconListGridBaseInfo}
</script>
