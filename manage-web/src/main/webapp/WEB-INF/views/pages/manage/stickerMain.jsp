<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">스티커관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="stickerSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>스티커관리</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="300px">
                        <col width="120px">
                        <col width="150px">
                        <col />
                    </colgroup>
                    <tbody>
                    <tr>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sticker.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sticker.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="sticker.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="sticker.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" style="width: 100px" name="schType" class="ui input medium mg-r-10" >
                                    <option value="stickerNm" selected>스티커명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5" style="width:120px"; maxlength="20" autocomplete="off">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 100px" name="schUseYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="stickerTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="stickerListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 스티커 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="stickerSetForm" onsubmit="return false;">
            <input type="hidden" id="stickerNo" name="stickerNo" value="">
            <table class="ui table">
                <caption>스티커 정보</caption>
                <colgroup>
                    <col width="10%">
                    <col width="30%">
                    <col width="*">
                    <col >
                </colgroup>
                <tbody>
                <tr>
                    <th>스티커명 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input id="stickerNm" name="stickerNm" style="width: 200px;" type="text" class="ui input medium mg-r-5" maxlength="10">
                            <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 10자 )</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>이미지등록 <span class="text-red">*</span></th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" id="imgDelBtn0" onclick="sticker.img.deleteImg('PC');" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                    <div id="PC" class="uploadType itemImg" inputId="uploadMNIds" data-processkey="StickerPCImg" style="cursor:hand;">

                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgNo" name="stickerImgList[].imgNo" value=""/>
                                        <input type="hidden" class="imgUrl" name="stickerImgList[].imgUrl" value=""/>
                                        <input type="hidden" class="imgWidth" name="stickerImgList[].imgWidth" value=""/>
                                        <input type="hidden" class="imgHeight" name="stickerImgList[].imgHeight" value=""/>
                                        <input type ="hidden" class="device" name="stickerImgList[].device" value="PC"/>
                                        <input type="hidden" class="useYn" name="stickerImgList[].useYn" value="Y"/>
                                    </div>
                                </div>
                                <div id="MOBILE" class="uploadType" style="display: none">
                                    <input type="hidden" class="imgNo" name="stickerImgList[].imgNo" value=""/>
                                    <input type="hidden" class="imgUrl" name="stickerImgList[].imgUrl" value=""/>
                                    <input type="hidden" class="imgWidth" name="stickerImgList[].imgWidth" value=""/>
                                    <input type="hidden" class="imgHeight" name="stickerImgList[].imgHeight" value=""/>
                                    <input type ="hidden" class="device" name="stickerImgList[].device" value="MOBILE"/>
                                    <input type="hidden" class="useYn" name="stickerImgList[].useYn" value="Y"/>
                                </div>

                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id=imgRegBtn0" class="ui button medium" onclick="sticker.img.clickFile('PC');">등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br>
                                최적화 가이드<br>
                                90 * 90 <br>
                                파일 : JPG, JPEG, PNG<br>

                                용량 : 2MB 이하<br>
                                파일명 : 45자 이내
                            </div>
                        </div>
                </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                    </td>
                </tr>


                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setStickerBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="stickerImgFile" style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/stickerMain.js?${fileVersion}"></script>

<script>
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
    //스티커 리스트 리얼그리드 기본 정보
    ${stickerListGridBaseInfo}

</script>
