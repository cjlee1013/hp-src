<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">공지사항 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="noticeSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>공지사항 검색</caption>
                    <colgroup>
                        <col width="100px;">
                        <col width="*">
                        <col width="100px;">
                        <col width="*">
                        <col width="100px;">
                        <col width="*">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="7">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="notice.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="notice.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="notice.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="notice.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>노출영역</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDispArea" style="width: 120px;" name="searchDispArea" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchDispArea}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchClassCd" style="width: 120px;" name="searchClassCd" class="ui input medium"  disabled="disabled">
                                    <option value="">전체</option>
                                </select>
                            </div>
                        </td>
                        <th>노출사이트</th>
                        <td>
                        <div class="ui form inline">
                                <select id="searchDispSite" style="width: 120px;" name="searchDispSite" class="ui input medium mg-r-10" disabled="disabled">
                                    <option value="">전체</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 120px;" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchUseYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>중요공지</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchTopYn" style="width: 120px;" name="searchTopYn" class="ui input medium" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchTopYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="7">
                            <select class="ui input medium mg-r-5"  id="searchType" name="searchType" style="width: 120px; float: left;">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 400px; float: left;">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="noticeTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="noticeListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="noticeSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>공지사항 등록/수정</caption>
                <colgroup>
                    <col width="110px">
                    <col width="200px">
                    <col width="110px">
                    <col width="200px">
                    <col width="120px">
                    <col >
                </colgroup>
                <tbody>
                <tr>
                    <th>번호</th>
                    <td>
                        <div class="ui form inline">
                            <input id="noticeNo" name="noticeNo" type="text" class="ui input medium mg-r-5"  readonly>
                        </div>
                    </td>
                    <th>노출영역 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="dispArea" style="width: 100px" name="dispArea" class="ui input medium mg-r-10" >
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${searchDispArea}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>


                    <th>구분 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="noticeKind" style="width: 100px" name="noticeKind" class="ui input medium"  disabled="disabled">
                                <option value="">선택</option>
                            </select>
                        </div>
                    </td>
                </tr>


                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="useYn" style="width: 100px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${searchUseYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>중요공지 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="topYn" style="width: 80px" name="topYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${searchTopYn}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>노출사이트 <span class="text-red">*</span></th>
                    <td>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox"  name="dispHomeYn"  id="dispHomeYn" value="Y"  disabled="disabled" checked>
                            <span>홈플러스</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block" >
                            <input type="checkbox"  name="dispClubYn"  id="dispClubYn" value="Y"  disabled="disabled" checked>
                            <span>더클럽</span>
                        </label>
                    </td>
                </tr>

                <tr>
                    <th>제목 <span class="text-red">*</span></th>
                    <td colspan="5">
                            <span class="ui form inline">
                                <input id="noticeTitle" name="noticeTitle" style="float:left;width: 500px;" type="text" class="ui input medium mg-r-5" maxlength="40">
                                <span class="text">( <span style="color:red;" id="textCountNoticeTitle">0</span> / 40 )</span>
                            </span>
                    </td>

                </tr>
                <tr>
                    <th>내용 <span class="text-red">*</span></th>
                    <td colspan="5">
                            <span class="ui form inline">
                                <!--<textarea id="faqBody" name="faqBody" class="ui input mg-r-5" style="float:left;width: 100%; height: 200px;" ></textarea>-->
                                <input type="hidden" id="noticeDesc" name="noticeDesc"/>
                                <div id="noticeDescEdit" name="noticeDescEdit"></div>
                            </span>
                    </td>

                </tr>
                <tr>
                    <th>첨부파일</th>
                    <td colspan="4">
                            <span class="ui form inline">
                                <button type="button" id="addFile" class="ui button medium gray-dark font-malgun mg-r-5" onclick="notice.file.clickFile($(this));" style="float:left">파일선택</button>
                                <div id="" name=""class="ui input medium mg-r-5" style="width:50%;background-color: #f9f9f9;"></div>
                            </span>
                            <div style="margin-top: 5px;">
                                첨부파일은 1개당 최대 25MB까지만 등록 가능합니다.
                            </div>
                    </td>
                    <td>
                        <ul id="fileList" name="fileList">

                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="ui wrap-table horizontal mg-t-10" id="partnerList" >
                <div class="com wrap-title sub">
                    <h3 class="title">노출대상</h3>
                    <span class="inner">
                        <button type="button" id="addPartner" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">판매자 등록</button>
                        <button type="button" id="deletePartner" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">삭제</button>
                    </span>

                </div>
                <div id="noticePartnerListGrid" style="width: 100%; height: 320px;"></div>
            </div>


        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setNoticeBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/noticeMain.js?${fileVersion}"></script>

<script>
    // 판매업체 리스트 리얼그리드 기본 정보
    ${noticeListGridBaseInfo}
    ${noticePartnerListGridBaseInfo}
    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
