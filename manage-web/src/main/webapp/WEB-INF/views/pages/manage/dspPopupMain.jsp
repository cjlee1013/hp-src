<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            파트너 팝업 관리
        </h2>
    </div>
    <div class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspPopupSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  dspPopup.search();">
                <table class="ui table">
                    <caption>파트너 팝업 관리 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <select id="searchPeriodType" name="searchPeriodType" class="ui input medium mg-r-10" style="width: 100px">
                                <c:forEach var="codeDto" items="${dspPopupPeriodTyp}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPopup.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPopup.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspPopup.initSearchDate('-30d');">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 90px;" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 120px">
                                <option value="popupNm">팝업명</option>
                                <option value="regNm">등록자</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 350px;" minlength="2" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="dspPopupTotalCount">0</span>건</h3>
            </div>

            <div class="topline"></div>

        </div>

        <div class="mg-t-20 mg-b-20" id="dspPopupListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspPopupSetForm" onsubmit="return false;">
            <input type="hidden" id="popupNo" name="popupNo">
            <input type="hidden" id="linkInfo" name="linkInfo">
            <table class="ui table">
                <caption>팝업 정보</caption>
                <colgroup>
                    <col width="130px">
                    <col />
                </colgroup>
                <tbody>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작시각" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료시각" style="width:150px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>팝업명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="popupNm" name="popupNm" maxlength="40" class="ui input medium mg-r-5" style="width: 350px; float: left;">
                            <span class="text pull-left">( <span style="color:red;" id="popupNmCnt">0</span> / 40자 )</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>숨김 기능&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dspPopupBtnPeriod}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="closeBtnPeriod" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="closeBtnPeriod" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="text-red-star">이미지</span>
                    </th>
                    <td>
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" id="imgUrl" name="imgUrl" value=""/>
                                        <input type="hidden" class="imgNo" name="imgNo" value=""/>
                                        <input type="hidden" class="imgNm" name="imgNm" value=""/>
                                        <input type="hidden" class="width" name="imgWidth" value=""/>
                                        <input type="hidden" class="height" name="imgHeight" value=""/>
                                        <input type="hidden" class="changeYn" name="changeYn" value="N"/>
                                        <input type="hidden" class="imgType" name="imgType" value=""/>
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" id="imgRegBtn" class="ui button medium addImgBtn" onclick="dspPopup.img.clickFile($(this));">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDivImgSize">358*N</span><br>
                                용량 : 2MB 이하<br>
                                파일 : JPG,JPEG,GIF<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>링크 유형&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="linkType" name="linkType" style="width: 150px; float:left;">
                            <c:forEach var="codeDto" items="${partnerPopMenuId}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        <c:forEach var="codeDto" items="${dspPopupLinkType}" varStatus="status">
                            <c:if test="${codeDto.mcCd eq 'URL' || codeDto.mcCd eq 'NONE'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                            </select>
                            <div id="selectTextBox" style="float: left;">
                                <input id="linkInfo" name="linkInfo" style="width: 130px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="15" placeholder="seq 입력(input box)">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>노출 대상&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dspPopupTarget}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="dispTarget" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="dispTarget" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="topline"></div>

            <div class="ui wrap-table horizontal mg-t-10" id="partnerList" >
                <div class="com wrap-title sub">
                    <h3 class="title">팝업 노출 판매자 : <span id="partnerTotalCount">0</span>건</h3>
                    <span class="inner">
                        <button type="button" id="addPartner" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">판매업체 조회</button>
                        <button type="button" id="deletePartner" class="ui button medium gray-dark font-malgun mg-r-5" style="float: right">삭제</button>
                    </span>
                </div>
                <div id="dspPopupPartnerListGrid" style="width: 100%; height: 320px;"></div>
            </div>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setDspPopupBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<div id="menuIdList" style="display: none">
    <select class="ui input medium mg-r-10" id="linkInfo" name="linkInfo" style="width: 150px; float:left;">
    <c:forEach var="codeDto" items="${partnerPopMenuId}" varStatus="status">
        <c:set var="selected" value="" />
        <c:if test="${statis.index eq 0}">
            <c:set var="selected" value="selected" />
        </c:if>
        <option value="${codeDto.ref1}" ${selected}>${codeDto.mcNm}</option>
    </c:forEach>
    </select>
</div>

<div id="linkNotice" style="display: none">
    <input id="linkInfo" name="linkInfo" style="width: 130px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="15" placeholder="seq 입력(input box)">
</div>

<div id="linkUrl" style="display: none">
    <input id="linkInfo" name="linkInfo" style="width: 950px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" value="http://">
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspPopupMain.js?${fileVersion}"></script>

<script>
    // 파트너공지 팝업 관리 리스트 리얼그리드 기본 정보
    ${dspPopupListGridBaseInfo}
    ${dspPopupPartnerListGridBaseInfo}

    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>