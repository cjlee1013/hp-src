<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title sub">
    <h3 class="title">• 빅배너</h3>
</div>
<div class="ui wrap-table horizontal mg-t-10" id="BIG">
    <form id="bigBannerSetForm" onsubmit="return false;" >
        <table class="ui table" >
            <caption>메인빅배너</caption>
            <colgroup>
                <col width="100px">
                <col width="1000px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>노출점포 <span class="text-red">*</span></th>
                <td colspan="2">

                    <c:forEach var="codeDto" items="${dspDispStoreType}" varStatus="status">
                        <c:choose>
                            <c:when test="${ status.index eq 0 }">
                                <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                            </c:when>
                            <c:otherwise>
                                <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>

                    <div id ="dispStore" style="display: none;">
                        <input type="hidden" id="storeList" name="dispStoreList">
                        <button type="button" class="ui button small gray-dark font-malgun mg-r-10 storeInfoBtn">점포선택</button>
                        <span class="text">(적용 점포수 : <span id="dispStoreCnt">0</span>개)</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>배너설정 <span class="text-red">*</span></th>
                <td colspan="2">
                    <select id="template" style="width: 100px;float: left" name="template" class="ui input medium mg-r-10" >
                        <c:forEach var="codeDto" items="${dspMainTemplate}" varStatus="status">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                    <select id="templateDetail" style="width: 100px;float: left" name="templateDetail" class="ui input medium mg-r-10"  disabled>

                    </select>
                </td>
            </tr>

            <tr class ="bigBannerDiv1" >
                <input type ="hidden" name="linkList[].linkNo" value="1"/>
                <th rowspan="3">이미지1 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">

                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv1TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv1TitleCnt" class="cnt">0</span> /  <span id="bigDiv1TitleBaseCnt" >20</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv1SubBaseCnt" style="width: 200px;float: left" maxlength="28"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv1SubCnt" class="cnt" >0</span> /  <span id="bigDiv1SubBaseCnt" >28</span> )
                        </span>
                    </div>

                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('1')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv1" >
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                        <select id="bigBannerLinktype1"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                        </select>
                        <div id="bigBannerLinkInfo1">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                        <%--<input type ="hidden" name="linkList[].linkNo" value="1"/>--%>
                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv1" >
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="0">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:170px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv1ImgSize">1200*450</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 2단 입력 창 시작--%>
            <tr class ="bigBannerDiv2" style="display: none;">
                <input type ="hidden" name="linkList[].linkNo" value="2"/>
                <th rowspan="3">이미지2 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv2TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv2TitleCnt" class="cnt" >0</span> /  <span id="bigDiv2TitleBaseCnt">40</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv2SubBaseCnt" style="width: 200px;float: left" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv2SubCnt" class="cnt" >0</span> /  <span id="bigDiv2SubBaseCnt">40</span> )
                        </span>
                    </div>
                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('2')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv2" style="display: none;">
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype2"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo2">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv2" style="display: none;">
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="1">

                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:150px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv2ImgSize">1920*80</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 2단 입력 창 끝--%>
            <%--빅배너 3단 입력 창 시작--%>
            <tr class ="bigBannerDiv3" style="display: none;">
                <input type ="hidden" name="linkList[].linkNo" value="3"/>
                <th rowspan="3">이미지3 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv3TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv3TitleCnt" class="cnt" >0</span> /  <span id="bigDiv3TitleBaseCnt">40</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv3SubBaseCnt" style="width: 200px;float: left" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv3SubCnt" class="cnt" >0</span> /  <span id="bigDiv3SubBaseCnt">40</span> )
                        </span>
                    </div>

                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('3')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv3" style="display: none;">
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype3"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo3">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv3" style="display: none;">
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;" >
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="2">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:150px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv3ImgSize">1920*80</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 3단 입력 창 끝--%>

            <%--빅배너 4단 입력 창 시작--%>
            <tr class ="bigBannerDiv4" style="display: none;">
                <input type ="hidden" name="linkList[].linkNo" value="4"/>
                <th rowspan="3">이미지4 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv4TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv4TitleCnt" class="cnt" >0</span> /  <span id="bigDiv4TitleBaseCnt">40</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv4SubBaseCnt" style="width: 200px;float: left" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv4SubCnt" class="cnt" >0</span> /  <span id="bigDiv4SubBaseCnt">40</span> )
                        </span>
                    </div>

                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('4')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv4" style="display: none;">
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype4"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo4">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                     <%--  <input type ="hidden" name="linkList[].linkNo" value="4"/>--%>
                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv4" style="display: none;">
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="3">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:150px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv4ImgSize">1920*80</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 4단 입력 창 끝--%>



            <%--빅배너 5단 입력 창 시작--%>
            <tr class ="bigBannerDiv5" style="display: none;">
                <input type ="hidden" name="linkList[].linkNo" value="5"/>
                <th rowspan="3">이미지5 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv5TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv5TitleCnt" class="cnt" >0</span> /  <span id="bigDiv5TitleBaseCnt">40</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv5SubBaseCnt" style="width: 200px;float: left" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv5SubCnt" class="cnt">0</span> /  <span id="bigDiv5SubBaseCnt">40</span> )
                        </span>
                    </div>

                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('5')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv5" style="display: none;">
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype5"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo5">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="5"/>--%>
                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv5" style="display: none;">
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;" division="4">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" >
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="4">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:150px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv5ImgSize">1920*80</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 5단 입력 창 끝--%>
            <%--빅배너 6단 입력 창 시작--%>
            <tr class ="bigBannerDiv6" style="display: none;">
                <input type ="hidden" name="linkList[].linkNo" value="6"/>
                <th rowspan="3">이미지6 <span class="text-red">*</span></th>
                <td>

                    <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                        <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                        <span>TEXT</span>
                    </label>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                        <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle bigDiv6TitleBaseCnt" style="width: 200px;float: left;margin-right: 20px;" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv6TitleCnt" class="cnt">0</span> /  <span id="bigDiv6TitleBaseCnt">40</span> )
                        </span>
                    </div>
                    <div style="float: left">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                        <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle bigDiv6SubBaseCnt" style="width: 200px;float: left" maxlength="20"  readonly >
                        <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">
                            ( <span style="color:red;" id="bigDiv6SubCnt" class="cnt">0</span> /  <span id="bigDiv6SubBaseCnt">40</span> )
                        </span>
                    </div>

                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainBigBannerDetail.resetLinkInfo('6')">초기화</button>
                </td>
            </tr>
            <tr class ="bigBannerDiv6" style="display: none;">
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype6"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo6">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>

                    </span>
                </td>
            </tr>
            <tr class ="bigBannerDiv6" style="display: none;">
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;" >
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    <input type="hidden" class="division"  value="5">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:150px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="bigBannerDiv6ImgSize">1920*80</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            <%--빅배너 6단 입력 창 끝--%>
            </tbody>
        </table>
    </form>
</div><%--&lt;%&ndash;//  <div class="ui wrap-table horizontal mg-t-10" id="BIG">&ndash;%&gt;--%>

<%--배너 템플릿 상세 셀렉트 박스 --%>
<div id = "templateDetail2" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate2}" varStatus="status">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<div id = "templateDetail3" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate3}" varStatus="status">
        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<div id = "templateDetail5" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate5}" varStatus="status">
        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<%--배너 템플릿 상세 셀렉트 박스 --%>