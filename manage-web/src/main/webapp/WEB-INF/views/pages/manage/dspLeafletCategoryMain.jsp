<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            마트전단 카테고리 관리
        </h2>
    </div>
    <div class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspLeafletCategorySearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  dspLeafletCategory.search();">
                <table class="ui table">
                    <caption>마트전단 카테고리 관리 검색</caption>
                    <colgroup>
                        <col width="100px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리명</th>
                        <td colspan="4">
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 60%;" minlength="2" >
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan mg-b-5">검색</button><br />
                                <button type="button" id="searchResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="4">
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 90px;" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="dspLeafletCategoryTotalCount">0</span>건</h3>
            </div>

            <div class="topline"></div>

        </div>

        <div class="mg-t-20 mg-b-20" id="dspLeafletCategoryListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspLeafletCategorySetForm" onsubmit="return false;">
            <input type="hidden" id="cateNo" name="cateNo">
            <table class="ui table">
                <caption>카테고리 등록/수정</caption>
                <colgroup>
                    <col width="170px;">
                    <col width="*">
                    <col width="170px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>카테고리명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="cateNm" name="cateNm" maxlength="40" class="ui input medium mg-r-5" style="width: 250px;">
                        </div>
                    </td>
                    <th>우선순위&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="number" id="priority" name="priority" class="ui input medium mg-r-5" style="width: 250px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <span class="text-red-star">카테고리 이미지(PC)</span>
                    </th>
                    <td>
                        <div id="categoryPcImgDiv">
                            <div style="position:relative; display:inline-block;">
                                <div class="text-center preview-image" style="height:132px;">
                                    <div class="categoryPcDisplayView" style="display:none;">
                                        <button type="button" id="pcDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                        <img id="categoryPcImgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" id="pcImg" name="pcImg" value=""/>
                                        <input type="hidden" id="pcImgWidth" name="pcImgWidth" value=""/>
                                        <input type="hidden" id="pcImgHeight" name="pcImgHeight" value=""/>
                                        <input type="hidden" id="pcImgChangeYn" name="pcImgChangeYn" value="N"/>
                                    </div>
                                    <div class="pcDisplayReg" style="padding-top: 50px;">
                                        <button type="button" id="pcImgRegBtn" class="ui button medium" onclick="dspLeafletCategory.img.clickFile('pc');">등록</button>
                                    </div>
                                </div>
                                <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                    <br><br><br>
                                    최적화 가이드<br>
                                    사이즈 : 80*80<br>
                                    용량 : 2MB 이하<br>
                                    파일 : PNG<br>
                                </div>
                            </div>
                        </div>
                    </td>
                    <th>
                        <span class="text-red-star">카테고리 이미지(MOBILE)</span>
                    </th>
                    <td>
                        <div id="categoryMobileImgDiv">
                            <div style="position:relative; display:inline-block;">
                                <div class="text-center preview-image" style="height:132px;">
                                    <div class="categoryMobileDisplayView" style="display:none;">
                                        <button type="button" id="mobileDelBtn" class="ui button small delete-thumb deleteImgBtn text-size-sm">삭제</button>
                                        <img id="categoryMobileImgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" id="mobileImg" name="mobileImg" value=""/>
                                        <input type="hidden" id="mobileImgWidth" name="mobileImgWidth" value=""/>
                                        <input type="hidden" id="mobileImgHeight" name="mobileImgHeight" value=""/>
                                        <input type="hidden" id="mobileImgChangeYn" name="mobileImgYn" value="N"/>
                                    </div>
                                    <div class="mobileDisplayReg" style="padding-top: 50px;">
                                        <button type="button" id="mobileImgRegBtn" class="ui button medium" onclick="dspLeafletCategory.img.clickFile('mobile');">등록</button>
                                    </div>
                                </div>
                                <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                    <br><br><br>
                                    최적화 가이드<br>
                                    사이즈 : 120*120<br>
                                    용량 : 2MB 이하<br>
                                    파일 : PNG<br>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <select id="useYn" style="width: 180px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="imageFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspLeafletCategoryMain.js?${fileVersion}"></script>
<script>
    ${dspLeafletCategoryListGridBaseInfo}
    const hmpImgUrl = '${hmpImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>