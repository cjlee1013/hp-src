<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            메인 테마 관리
        </h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspMainThemeSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) dspLeaflet.search();">
                <table class="ui table">
                    <caption>메인 테마 관리 검색</caption>
                    <colgroup>
                        <col width="100px;">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <c:forEach var="codeDto" items="${dspMainPeriodType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainThemePeriod.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainThemePeriod.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMainThemePeriod.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="dspMainThemePeriod.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchLocType" style="width: 180px" name="searchLocType" class="ui input medium mg-r-10" >
                                    <c:forEach var="codeDto" items="${dspMainLocType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 180px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="themeNm">전시 테마명</option>
                                    <option value="themeMngNm">관리 테마명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="dspMainThemePeriodTotalCount">0</span>건</h3>
            </div>

            <div class="topline"></div>

        </div>

        <div class="mg-t-20 mg-b-20" id="dspMainThemePeriodListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">테마 & 상품 등록</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspMainThemePeriodSetForm" onsubmit="return false;">
            <input type="hidden" id="themeId" name="themeId">

            <table class="ui table">
                <caption>메인테마 등록/수정</caption>
                <colgroup>
                    <col width="170px;">
                    <col width="*">
                    <col width="170px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>전시위치&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="locType" name="locType" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${dspMainLocType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>우선순위&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input id="priority" name="priority" style="width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>관리 테마명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="themeMngNm" name="themeMngNm" class="ui input medium mg-r-5" maxlength="20" style="width: 250px;">
                            <span class="text">
                                 ( <span style="color:red;" id="themeMngNmCnt" class="cnt">0</span>/20 자)
                            </span>
                        </div>
                    </td>
                    <th>전시 테마명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="themeNm" name="themeNm" class="ui input medium mg-r-5" maxlength="6" style="width: 150px;">
                            <span class="text">
                                 ( <span style="color:red;" id="themeNmCnt" class="cnt">0</span>/6 자)
                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간&nbsp;<span class="text-red"> *</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:140px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:140px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <select id="useYn" style="width: 180px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="linkTr" style="display: none;">
                    <th>링크&nbsp;<span class="text-red"> *</span></th>
                    <td colspan="4">
                        <select id="linkType" class="ui input medium mg-r-10" style="width: 100px; float: left;">
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd eq 'EXH'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <input name="linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                        <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                        <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainThemePeriod.popLink();">검색</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div style="width: 100%; float: left; margin-top: 15px;">

        <div class="com wrap-title sub">
            <h3 class="title">상품 등록/수정</h3>
            <h4 class="text" style="font-weight: normal;">&nbsp;&nbsp; • 상품 등록 시, 저장하지 않은 순서 변경내역은 초기화됩니다.</h4>
        </div>

        <span class="text pull-left" style="margin-top: 5px;">* 상품건수 : <span id="dspMainThemePeriodItemTotalCount">0</span>건</span>

        <span class="ui form inline mg-l-10 pull-right" style="margin-bottom: 5px;">
            <span class="text ">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
            <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
        </span>

        <div class="mg-t-20 mg-b-20" id="dspMainThemePeriodItemListGrid" style="width: 100%; height: 350px;"></div>

        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="dspMainThemePeriodItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="dspMainThemePeriodItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="dspMainThemePeriodItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="dspMainThemePeriodItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="dspMainThemePeriodItem.openItemReadPopup();">일괄등록</button>
            <button type="button" class="ui button medium" id="addItemPopUp">상품조회</button>

            <div id="dispStore" style="float: right;">
                <select style="width: 100px;" id="itemStoreType" name="itemStoreType" class="ui input medium mg-r-10 storeType">
                    <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
                        <c:if test="${codeDto.ref1 eq 'HOME' && codeDto.mcCd eq 'HYPER'}">
                            <option value="${codeDto.mcCd}">${codeDto.mcCd}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>

            <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 100px; float: right;">
                <option value="TD">점포상품</option>
            </select>

            <li style="margin-left : 40px; margin-right: 5px; margin-top: 3px; float: right;">* 상품 전시영역</li>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="dspMainThemePeriodItem.chgDispYn('N');">전시안함</button>
            <button type="button" class="ui button medium" onclick="dspMainThemePeriodItem.chgDispYn('Y');">전시</button>
            <li style="margin-right: 5px; margin-top: 3px; float: right;">* 전시여부 변경</li>
        </span>
    </div>

</div>

<div class="ui center-button-group mg-t-15">
    <span class="inner">
        <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
        <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
    </span>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMainThemePeriodMain.js?${fileVersion}"></script>
<script>
    ${dspMainThemePeriodGridBaseInfo}
    ${dspMainThemePeriodItemGridBaseInfo}
</script>