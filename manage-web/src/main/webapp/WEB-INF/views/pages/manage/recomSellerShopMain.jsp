<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">추천 셀러샵 전시관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="recomSellerShopSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>셀러샵 검색</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="30%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomSellerShop.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomSellerShop.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomSellerShop.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="recomSellerShop.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px;float: left">
                                <c:forEach var="codeDto" items="${recomshopSchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 250px;" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="recomSellerShopTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="recomSellerShopListGrid" style="width: 100%; height: 320px;"></div>
    </div>


    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 추천 셀러샵 기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="sellerItemSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>상품정보</caption>
                <colgroup>
                    <col width="200px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>판매업체ID <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="partnerId" name="partnerId" class="ui input medium mg-r-5" style="width: 200px;" readonly>
                            <button type="button" class="ui button medium gray-dark font-malgun mg-r-5 mg-l-10" onclick="recomSellerShop.getPartnerPop();">조회</button>
                        </div>
                    </td>
                </tr>
                <tr >
                    <th>셀러샵 이름(닉네임) <span class="text-red">*</span></th>
                    <td id ="shopNm">
                    </td>
                </tr>
                <tr >
                    <th>셀러샵 소개 <span class="text-red">*</span></th>
                    <td id ="shopInfo">
                    </td>
                </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td >
                        <c:forEach var="recomshopUseYn" items="${recomshopUseYn}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${recomshopUseYn.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="recommendUseYn" class="ui input" value="${recomshopUseYn.mcCd}" ${checked}/><span>${recomshopUseYn.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>상품 추천 <span class="text-red">*</span></th>
                    <td>
                        <c:forEach var="recomshopType" items="${recomshopType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${recomshopType.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="recommendType" class="ui input" value="${recomshopType.mcCd}" ${checked}/><span>${recomshopType.mcNm}</span></label>
                        </c:forEach>
                    </td>
                </tr>
                <tr id ="manual">
                    <th>상품 수동추천 <span class="text-red">*</span></th>
                    <td>

                        <div id="recomSellerShopItemListGrid" style="width: 100%; height: 150px;margin-top: 10px;"></div>
                        <button class="ui button medium gray-dark mg-r-5 " id="addItemPopUp" style="float: right;margin-bottom: 10px;margin-top: 10px;">등록</button>
                        <%--<div class="ui wrap-table horizontal mg-t-10">--%>
                            <table class="ui table" style="border: 1px solid #ddd">
                                <colgroup>
                                    <col width="200px">
                                    <col width="200px">
                                    <col width="200px">
                                    <col width="*">
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th>상품번호</th>
                                    <td id ="itemNo"></td>
                                    <th>사용여부</th>
                                    <td>

                                        <select id="itemUseYn" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                            <c:forEach var="useYn" items="${recomshopUseYn}" varStatus="status">
                                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>우선순위</th>
                                    <td colspan="3">
                                        <input type="text" id="itemPriority"  class="ui input medium mg-r-5" style="width: 250px;" >
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        <button class="ui button medium white mg-r-5 " id="setItem" style="float: right;margin-top: 10px;">저장</button>
                        <%--</div>--%>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>


    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setSellerShopItmBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


</div>
<!-- 이미지정보 업로드 폼 -->
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/recomSellerShopMain.js?${fileVersion}"></script>

<script>
    ${recomSellerShopListGridBaseInfo};
    ${recomSellerShopItemListGridBaseInfo};
</script>
