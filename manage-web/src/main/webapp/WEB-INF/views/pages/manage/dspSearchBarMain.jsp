<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp"/>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">검색창 키워드 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspSearchBarSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색창 키워드 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="*">
                        <col width="100px">
                        <col width="*">
                        <col width="100px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="6">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:120px;float:left;" data-default="등록일">
                                    <option value="regDt">등록일</option>
                                    <option value="dispDt">전시일</option>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspSearchBarMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspSearchBarMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspSearchBarMain.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspSearchBarMain.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>노출위치</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchLocCd" style="width: 120px;" name="searchLocCd" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspSearchLocCd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>디바이스</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select id="searchDevice" style="width: 100px" name="searchDevice" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 80px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="6">
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px; float: left;">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="dspSearchBarTotalCount">0</span>건</h3>
        </div>

        <div class="topline"></div>

        <div id="dspSearchBarListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 검색창 키워드 정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspSearchBarSetForm" onsubmit="return false;">
            <input type="hidden" id="keywordNo" name="keywordNo">
            <input type="hidden" id="linkInfo" name="linkInfo">
            <input type="hidden" id="linkType" name="linkType">
            <input type="hidden" id="linkOptions" name="linkOptions">
            <input type="hidden" id="priority" name="priority">
            <input type="hidden" id="weight" name="weight">
            <table class="ui table">
                <caption>검색창 키워드 등록/수정</caption>
                <colgroup>
                    <col width="140px">
                    <col width="*">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>키워드&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="keyword" name="keyword" maxlength="30" class="ui input medium mg-r-5" style="width: 230px;">
                            <span class="text">
                                ( <span style="color:red;" id="keywordCnt" class="cnt">0</span>/30 자)
                            </span>
                        </div>
                    </td>
                    <th>노출위치&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="locCd" name="locCd" style="width: 150px;">
                                <c:forEach var="codeDto" items="${dspSearchLocCd}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width: 150px;">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>디바이스&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox" id ="pcUseYn" name="pcUseYn" value="N">
                            <span>PC</span>
                        </label>
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox" id ="mobileUseYn" name="mobileUseYn" value="N">
                            <span>모바일</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>전시기간&nbsp;<span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작시각" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료시각" style="width:150px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>가중치/우선순위&nbsp;<span class="text-red">*</span></th>
                    <td colspan="4">
                        <div class="ui form inline">
                            <input type="number" id="setWeightOrPriority" name="setWeightOrPriority" maxlength="1" min="1" max="9" class="ui input medium mg-r-5" style="width: 80px;">
                        </div>
                    </td>
                </tr>
                <tr id ="linkDiv">
                    <th>링크 유형&nbsp;<span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span style="float: left;">링크</span>
                            <select id="bannerLinkType1" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <option value="KEYWORD">검색어</option>
                                <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                    <c:if test="${codeDto.mcCd ne 'GNB'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <div id="bannerLinkInfo1">
                                <input name="linkList[].linkInfo" style="width: 400px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<%--링크 관련 셀렉트 옵션 및 입력창 관리--%>
<div id="linkCommon" style="display: none">
    <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
    <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
    <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspSearchBarMain.popLink(this);">검색</button>
</div>

<div id="linkUrl" style="display: none">
    <input name="linkList[].linkInfo" style="width: 600px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" value="http://">
</div>
<div id="linkKeyword" style="display: none">
    <input name="linkList[].linkInfo" style="width: 400px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" >
</div>
<div id="linkGnbDevice" style="display: none">
    <select style="width: 100px;float: left"  name="linkList[].gnbDeviceType" class="ui input medium mg-r-10 gnbDeviceType">
        <c:forEach var="codeDto" items="${dspMainGnbType}" varStatus="status">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:forEach>
    </select>
</div>

<div id="storeTypeHome" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="storeTypeClub" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="dlvStoreTypeClub" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">점포선택</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'CLUB'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>
<div id="dlvStoreTypeHome" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'HYPER'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>

<div id="linkTypeHome" style="display: none">
    <option value="KEYWORD">검색어</option>
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 eq 'HOME'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>
<div id="linkTypeClub" style="display: none">
    <option value="KEYWORD">검색어</option>
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref2 eq 'CLUB'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspSearchBarMain.js?${fileVersion}"></script>

<script>
    // GNB 리스트 리얼그리드 기본 정보
    ${dspSearchBarListGridBaseInfo}
</script>