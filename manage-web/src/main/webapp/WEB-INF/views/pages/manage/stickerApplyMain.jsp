<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">스티커 대상상품 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="stickerApplySearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>스티커대상상품관리</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="400px">
                        <col width="120px">
                        <col width="200px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" style="width: 120px" name="schDateType" class="ui input medium mg-r-10" data-default="REGDT">
                                    <c:forEach var="codeDto" items="${stickerPeriodType}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected" />
                                        </c:if>
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="stickerApply.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="stickerApply.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="stickerApply.initSearchDate('-30d');">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" style="width: 120px" name="schType" class="ui input medium mg-r-10" >
                                    <option value="itemNm" selected>상품명</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="stickerNm">스티커명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5" style="width:150px"; maxlength="20" autocomplete="off">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" style="width: 120px" name="schUseYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="stickerApplyTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="stickerApplyListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 대상상품 등록/수정 </h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="stickerApplySetForm" onsubmit="return false;">

            <input type="hidden" id="stickerApplyNo" name="stickerApplyNo">
            <table class="ui table">
                <caption>대상상품 등록/수정 </caption>
                <colgroup>
                    <col width="10%">
                    <col width="30%">
                    <col width="10%">
                    <col width="30%">
                </colgroup>
                <tbody>
                <tr>
                    <th>관리번호</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <span class="text" id ="stickerApplyNoTxt"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>관리명 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="stickerApplyNm" name="stickerApplyNm" class="ui input medium mg-r-5" maxlength="15" style="width:200px">
                            <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 15 )</span>
                        </div>
                    </td>
                    <th>스티커명 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="stickerNo" style="width: 180px" name="stickerNo" class="ui input medium mg-r-10">
                                <option value="">선택하세요</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
               <tr>
                   <th>점포유형 <span class="text-red">*</span></th>
                   <td colspan="3">
                       <label class="ui checkbox mg-r-10" style="display: inline-block">
                           <input type="checkbox" id ="hyperDispYn" name="hyperDispYn" class="chkStickerApplyStoreType" value="Y"><span>Hyper</span>
                       </label>
                       <label class="ui checkbox" style="display: inline-block">
                           <input type="checkbox"   id ="expDispYn" name="expDispYn" class="chkStickerApplyStoreType" value="Y"><span>Express</span>
                       </label>
                   </td>
               </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useY" value="Y" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useYn" id="useN" value="N"><span>사용안함</span></label>
                    </td>
                </tr>
                <tr>
                    <th>대상상품 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="pull-left mg-b-10 ">
                            * 상품 건수 : <span id="stickerApplyItemTotalCount">0</span>건
                            <button type="button" id="deleteItemBtn" class="ui button medium gray mg-l-5">선택삭제</button>
                        </span>

                        <span class="ui form inline mg-l-10">
                            <span class="text ">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
                            <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
                        </span>

                        </span>
                        <span class="pull-right mg-b-10">
                            <button type="submit" id="addItemBtn" class="ui button medium gray-dark ">추가</button>
                            <button type="submit" id="excelUploadBtn" class="ui button medium gray">엑셀일괄등록</button>
                            <button type="submit" id="excelDownloadBtn" class="ui button medium gray">대상상품 엑셀다운</button>
                        </span>
                        <div class="mg-t-10 mg-b-20" id="stickerApplyItemListGrid" style="width: 100%; height: 350px;"></div>

                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
        <div class="ui center-button-group mg-t-15">
            <span class="inner">
                <button class="ui button xlarge cyan font-malgun" id="setStickerApplyBtn">저장</button>
                <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            </span>
        </div>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/stickerApplyMain.js?${fileVersion}"></script>
<script>

    //스티커 대상상품 리스트 리얼그리드 기본 정보
    ${stickerApplyListGridBaseInfo}
    ${stickerApplyItemListGridBaseInfo}

</script>
