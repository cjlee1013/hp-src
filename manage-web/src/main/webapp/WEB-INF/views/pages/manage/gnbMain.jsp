<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">GNB 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="gnbSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>GNB 검색</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="250px">
                        <col width="150px">
                        <col width="250px">
                        <col width="150px">
                        <col width="150px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>전시기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSite" style="width: 100px" name="searchSite" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchSite}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>디바이스</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDevice" style="width: 100px" name="searchDevice" class="ui input medium" >
                                    <c:forEach var="codeDto" items="${searchDevice}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 100px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${searchUseYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 80px;float: left;">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <div class="ui form inline" style="float: left">
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 400px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="gnbTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="gnbListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 배너정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="gnbSetForm" onsubmit="return false;">
            <input type="hidden" id="gnbNo" name="gnbNo" value="">
            <table class="ui table">
                <caption>GNB 등록/수정</caption>
                <colgroup>
                    <col width="120px">
                    <col width="350px">
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트구분</th>
                    <td colspan="3">
                        <label class="ui radio inline" style="margin-right: 10px">
                            <c:forEach var="codeDto" items="${searchSite}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="siteType" class="ui input medium" value="${codeDto.mcCd}"  ${checked} ><span>${codeDto.mcNm}</span>
                                </label>
                            </c:forEach>
                        </label>
                    </td>
                </tr>

                <tr >
                    <th>GNB명 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input  id= "dispNm" name="dispNm" style="width: 150px;" type="text" class="ui input medium mg-r-5" maxlength="6">
                            <span class="text">최대 6자</span>
                        </div>
                    </td>
                </tr>


                <tr>
                    <th>디바이스 <span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio inline" style="margin-right: 10px">
                            <c:forEach var="codeDto" items="${searchDevice}" varStatus="status">
                                <c:set var="checked" value="" />
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="checked" value="checked" />
                                </c:if>
                                <label class="ui radio inline" style="margin-right: 10px">
                                    <input type="radio" name="device" class="ui input medium" value="${codeDto.mcCd}"  ${checked} ><span>${codeDto.mcNm}</span>
                                </label>
                            </c:forEach>
                        </label>
                    </td>
                    <th id="iconTxt">아이콘 <span class="text-red">*</span></th>
                    <td id="iconInput">
                        <label class="ui checkbox" style="display: inline-block">
                            <input type="checkbox" name="iconKind" id="iconKind" value="NEW" >
                            <span>NEW</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <th>사용여부 <span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <select id="useYn" style="width: 100px" name="useYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${searchUseYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </span>
                    </td>
                    <th>전시순서 <span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input id="priority" name="priority" style="float:left;width: 100px;" type="text" class="ui input medium mg-r-5" maxlength="40">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td>
                        <span class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>링크 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <select id="linkType" style="width: 120px;float: left;" name="linkType" class="ui input medium mg-r-10"  >
                                <c:forEach var="codeDto" items="${searchLinkType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input name="linkInfo" id="linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" readonly="">
                            <div id ="linkCommon" style="display: none">
                                <input name="linkInfoNm" id="linkInfoNm" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="gnb.popLink();">검색</button>
                            </div>
                        </span>
                    </td>
                </tr>

                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setGnbBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/gnbMain.js?${fileVersion}"></script>

<script>
    // GNB 리스트 리얼그리드 기본 정보
    ${gnbListGridBaseInfo}
</script>

