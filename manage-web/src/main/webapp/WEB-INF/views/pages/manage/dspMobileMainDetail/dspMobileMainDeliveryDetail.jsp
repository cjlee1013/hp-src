<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title sub">
    <h3 class="title" id="deliveryBannerTxt">• 배송배너</h3>
</div>
<div class="ui wrap-table horizontal mg-t-10" >
    <form id="deliveryBannerSetForm" onsubmit="return false;" >
        <table class="ui table" >
            <caption>배송배너</caption>
            <colgroup>
                <col width="100px">
                <col width="1000px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>점포설정 <span class="text-red">*</span></th>
                <td colspan="2">

                    <c:forEach var="codeDto" items="${dspDispStoreType}" varStatus="status">
                        <c:choose>
                            <c:when test="${ status.index eq 0 }">
                                <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                            </c:when>
                            <c:otherwise>
                                <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>

                    <div id ="dispStore" style="display: none;">
                        <input type="hidden" id="storeList" name="dispStoreList">
                        <button type="button" class="ui button small gray-dark font-malgun mg-r-10 storeInfoBtn">점포선택</button>
                        <span class="text">(적용 점포수 : <span id="dispStoreCnt">0</span>개)</span>
                    </div>
                </td>
            </tr>

            <tr class ="deliveryBannerDiv" >
                <input type ="hidden" name="linkList[].linkNo" value="1"/>
                <th rowspan="2">이미지 <span class="text-red">*</span></th>
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                        <select id="deliveryBannerLinktype"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                        </select>
                        <div id="deliveryBannerLinkInfo">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>

                    </span>
                </td>
                </td>
                <td  rowspan="3">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainDeliveryBannerDetail.resetInfo()">초기화</button>
                </td>
            </tr>
            <tr class ="deliveryBannerDiv" >
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : <span id="deliveryImg">750*N</span><br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG<br>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div><%--&lt;%&ndash;//  <div class="ui wrap-table horizontal mg-t-10" id="BIG">&ndash;%&gt;--%>
