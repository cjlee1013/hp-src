<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<style>
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        border-style: solid;
        border-width: 0 1px 1px 1px;
        text-align: center;
    }
    .on{
        position: relative;
        z-index: 1;
        background-color: #f6f6f6;
        color: #111;
    }
    li {
        float: left;
        width:50%;
        background-color:  #fff;
        border-bottom: 1px solid;

    }
    li a {
        display: block;
        color: black;
        padding: 14px 16px;
        text-decoration: none;
    }
    .text{

        color: rgb(102, 102, 102);
        display: block;
        width: 100%;
        height:200px;
        border: 0;
    }
</style>

<div class="com wrap-popup medium popup-request">
    <input type="hidden" id="partnerCnt"  value="${partnerCnt}"/>

    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원상태 변경</h2>
    </div>

    <ul>
        <li><a href="#home" class="on" >판매자 추가</a></li>
        <li><a class="" href="#news">액셀 일관 업로드</a></li>
        <div id ="addTxt"><input type ="textarea" id="partnerIdList" class="text" placeholder="(판매자 ID를 입력하며, ',' 로 분류 : aaaa,bbbb,cccc)"/></div>
        <div id ="addExel"><input class="text"type ="textarea" placeholder="(판매자 ID를 입력하며, ',' 로 분류 : aaaa,bbbb,cccc)"/></div>
    </ul>

    <div class="ui center-button-group">
        <span class="inner">
            <button type="button" class="ui button midium btn-danger font-malgun" id="setBtn">저장</button>
            <button type="button" class="ui button midium font-malgun" id="cancelBtn">취소</button>
        </span>
    </div>

</div>

<script>
$(document).ready(function() {
    $('#addExel').hide();
    $('#setBtn').click(function() {
    var partnerId = $('#partnerIdList').val();
    var partnerIdCnt  = partnerId.split(",").length;
        if(partnerId != null && partnerId != '') {
            // var partnerSize  =  partnerId.split(",").length+ parseInt($('#partnerCnt').val());
            // if(partnerSize > 10){
            //     alert("판매자 공지 등록은 최대 5명까지 가능합니다.");
            //     return false;
            // }
            CommonAjax.basic({url:'/manage/dspPopup/getPartnerList.json', data : { partnerIdArr : $('#partnerIdList').val()}, method : "GET", successMsg : null, callbackFunc : function(res) {
                if(partnerIdCnt == res.length) {
                    alert($.jUtil.comma(partnerIdCnt) + "개의 아이디가 등록 되었습니다.");
                } else {
                    alert("총 "+ $.jUtil.comma(partnerIdCnt) + "개 중 " + $.jUtil.comma(res.length) + "개가 등록, " + $.jUtil.comma((parseInt(partnerIdCnt)-res.length)) + "개가 실패하였습니다.");
                }
                opener.dspPopup.addPartner(res);
                self.close();
            }});
        } else {
            alert("파트너 아이디를 입력해 주세요.");
        }
    });

    $('#cancelBtn').click(function() {
        self.close();
    });
});
</script>