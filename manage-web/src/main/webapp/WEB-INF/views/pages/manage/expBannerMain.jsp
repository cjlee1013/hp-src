<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">EXPRESS 배너관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="expBannerSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>EXPRESS 배너관리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <option value="regDt">등록일</option>
                                    <option value="dispDt">전시일자</option>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expBanner.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expBanner.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="expBanner.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="expBanner.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>디바이스</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="searchDevice" style="width: 180px" name="searchDevice" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchLoc" style="width: 120px; float: left;" name="searchLoc" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${expBannerLoc}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>전시여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDispYn" style="width: 180px" name="searchDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.ref2}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="bannerNm">배너명</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="expBannerTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="expBannerListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 배너기본정보</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="bannerSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>베너 등록/수정</caption>
                <colgroup>
                    <col width="150px">
                    <col width="350px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>배너번호</th>
                    <td>
                        <input id="bannerNo" name="bannerNo"type=" text" class="ui input medium mg-r-5" style="width: 200px;" readonly >
                    </td>
                </tr>
                <tr>
                    <th>전시위치 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <select id="loc" style="width: 180px; float: left;" name="loc" class="ui input medium mg-r-10" >
                            <option value="">선택</option>
                            <c:forEach var="codeDto" items="${expBannerLoc}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>디바이스&nbsp;<span class="text-red">*</span></th>
                    <td colspan="3">
                        <select id="device" name="device" style="width: 180px"  class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dskDeviceGubun}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>전시여부 <span class="text-red">*</span></th>
                    <td>
                        <select id="dispYn" style="width: 180px" name="dispYn" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dispYn}" varStatus="status">
                            <option value="${codeDto.mcCd}">${codeDto.ref2}</option>
                        </c:forEach>
                        </select>
                    </td>
                    <th>전시순서 <span class="text-red">*</span></th>
                    <td>
                        <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="6" >
                    </td>
                </tr>
                <tr>
                    <th>전시기간 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                        </span>
                    </td>
                </tr>
                <tr>
                    <th>배너명 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="bannerNm" name="bannerNm" type="text" class="ui input medium mg-r-5" style="width: 300px;" maxlength="20" >
                            <span class="text">( <span style="color:red;" id="bannerNmCnt">0</span> / 20자 )</span>
                        </div>
                    </td>
                </tr>
                <tr id="displayStore" style="display: none;">
                    <th>노출점포 <span class="text-red">*</span></th>
                    <td colspan="2">

                        <c:forEach var="codeDto" items="${dspDispStoreType}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline" style="float: left"><input type="radio" name="dispStoreType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                        <div id ="dispStore" style="display: none;">
                            <input type="hidden" id="storeList" name="storeList">
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-10 storeInfoBtn">점포선택</button>
                            <span class="text">(적용 점포수 : <span id="dispStoreCnt">0</span>개)</span>
                        </div>
                    </td>
                </tr>
                <tr id="bannerTr" style="display: none;">
                    <th>배너설정 <span class="text-red">*</span></th>
                    <td colspan="2">
                        <select id="template" style="width: 100px;float: left" name="template" class="ui input medium mg-r-10" >
                            <c:forEach var="codeDto" items="${dspMainTemplate}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <select id="templateDetail" style="width: 100px;float: left" name="templateDetail" class="ui input medium mg-r-10"  disabled>

                        </select>
                    </td>
                </tr>
                <%--빅배너 1단 입력 창 시작--%>
                <tr class ="bigBannerDiv1" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="1"/>
                    <th rowspan="3">이미지1 <span class="text-red">*</span></th>
                    <td colspan="3" id="bannerTdTxt1">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N" class="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt1">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt1">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td rowspan="3" id="bannerTdTxt2">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('1')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv1" style="display: none;">
                    <td colspan="3">
                        <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype1"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo1">
                            <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                            <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                        <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                        <%--<input type ="hidden" name="linkList[].linkNo" value="1"/>--%>
                        </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv1" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="0">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv1ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv1ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--빅배너 1단 입력 창 끝--%>
                <%--빅배너 2단 입력 창 시작--%>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="2"/>
                    <th rowspan="3">이미지2 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt2">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt2">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('2')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype2"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo2">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv2" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="1">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv2ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv2ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%--빅배너 2단 입력 창 끝--%>
                <%--빅배너 3단 입력 창 시작--%>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="3"/>
                    <th rowspan="3">이미지3 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt3">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt3">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td  rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('3')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype3"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo3">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv3" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="2">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv3ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv3ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너3단 입력 창 끝--%>
                <%--빅배너 4단 입력 창 시작--%>
                <tr class ="bigBannerDiv4" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="4"/>
                    <th rowspan="3">이미지4 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt4">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt4">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td  rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('4')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv4" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype4"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo4">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv4" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="3">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv4ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv4ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너4단 입력 창 끝--%>
                <%--빅배너 5단 입력 창 시작--%>
                <tr class ="bigBannerDiv5" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="5"/>
                    <th rowspan="3">이미지5 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt5">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt5">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td  rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('5')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv5" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype5"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo5">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv5" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="4">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv5ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv5ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너5단 입력 창 끝--%>
                <%--빅배너 6단 입력 창 시작--%>
                <tr class ="bigBannerDiv6" style="display: none;">
                    <input type ="hidden" name="linkList[].linkNo" value="6"/>
                    <th rowspan="3">이미지6 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <label class="ui checkbox " style="display: inline-block;float: left;margin-right: 20px;padding-top: 3px;">
                            <input type="checkbox" name="linkList[].textYn" value="N"  class ="textYn">
                            <span>TEXT</span>
                        </label>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Title</span>
                            <input name="linkList[].title" type="text" class="ui input medium mg-r-5 bigBannerTitle" style="width: 250px;float: left;margin-right: 20px;" maxlength="20" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerTitleCnt6">0</span> / 20자 )</span>
                        </div>
                        <div style="float: left">
                            <span style="float: left;line-height: 25px;margin-right: 20px;">Sub</span>
                            <input name="linkList[].subTitle" type="text" class="ui input medium mg-r-5 bigBannerSubTitle" style="width: 300px;float: left" maxlength="28" readonly >
                            <span class="text" style="float: left;line-height: 25px;margin-right: 20px;">( <span class="cnt" style="color:red;" id="bannerSubTitleCnt6">0</span> / 28자 )</span>
                        </div>
                    </td>
                    <td  rowspan="3">
                        <button class="ui button medium white font-malgun" style="float: right" onclick="expBannerCommon.resetLinkInfo('6')">초기화</button>
                    </td>
                </tr>
                <tr class ="bigBannerDiv6" style="display: none;">
                    <td colspan="3">
                    <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                            <select id="bigBannerLinktype6"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <c:if test="${codeDto.mcCd ne 'ITEM_DS'}">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                        <div id="bigBannerLinkInfo6">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);" >검색</button>
                        </div>
                       <%--<input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                    </span>
                    </td>
                </tr>
                <tr class ="bigBannerDiv6" style="display: none;">
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                        <input type="hidden" class="division" value="5">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:180px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : <span id="bannerDiv6ImgSize">750*140</span><br>
                                용량 : 2MB 이하<br>
                                확장자 : <span id="bannerDiv6ImgType">JPG / JPEG / PNG</span><br>
                            </div>
                        </div>
                    </td>
                </tr>
                <%-- 배너6단 입력 창 끝--%>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setPcMainBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<%--링크 관련 셀렉트 옵션 및 입력창 관리 끝--%>

<div style="display:none;" class="storePop" >
    <form id="storeSelectPopForm" name="storeSelectPopForm" method="post" action="/common/popup/storeSelectPop">
        <input type="text" name="storeTypePop" value="EXP"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" name="setYn" value="Y"/>
        <input type="text" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="expBannerCommon.storePopupCallback"/>
    </form>
</div>

<%--배너 템플릿 상세 셀렉트 박스 --%>
<div id = "templateDetail2" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate2}" varStatus="status">
        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<div id = "templateDetail3" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate3}" varStatus="status">
        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<div id = "templateDetail5" style="display: none">
    <c:forEach var="codeDto" items="${dspMainTemplate5}" varStatus="status">
        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<%--배너 템플릿 상세 셀렉트 박스 --%>

<%--링크 관련 셀렉트 옵션 및 입력창 관리--%>
<div id ="linkCommon" style="display: none">
    <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
    <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
    <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="expBannerCommon.popLink(this);">검색</button>
</div>

<div id ="linkUrl" style="display: none">
    <input name="linkList[].linkInfo" style="width: 600px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="90" value="http://">
</div>

<div id ="linkGnbDevice" style="display: none">
    <select style="width: 100px;float: left"  name="linkList[].gnbDeviceType" class="ui input medium mg-r-10 gnbDeviceType">
        <c:forEach var="codeDto" items="${dspMainGnbType}" varStatus="status">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeHome" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id = "linkTypeHome" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 eq 'HOME'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>
<div id = "linkTypeClub" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref2 eq 'CLUB'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>

<%--링크 관련 셀렉트 옵션 및 입력창 관리 끝--%>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/expBannerMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>

<script>
    ${expBannerListGridBaseInfo}
    var hmpImgUrl = '${homeImgUrl}';
</script>