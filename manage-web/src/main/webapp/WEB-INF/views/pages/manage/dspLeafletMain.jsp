<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            마트전단 전시 관리
        </h2>
    </div>
    <div class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspLeafletSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13) dspLeaflet.search();">
                <table class="ui table">
                    <caption>마트전단 전시 관리 검색</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="100px">
                        <col width="100px">
                        <col width="100px">
                        <col width="100px">
                        <col width="100px">
                        <col width="100px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <select id="searchPeriodType" name="searchPeriodType" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="regDt">등록일</option>
                                <option value="dispDt">전시</option>
                            </select>
                        </td>
                        <td colspan="4">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp; &nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspLeaflet.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspLeaflet.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspLeaflet.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspLeaflet.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <div class="ui form inline ">
                            <td colspan="4">
                                <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input small" value="" checked/><span>전체</span></label>
                                <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input small" value="HOME"/><span>홈플러스</span></label>
                                <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input small" value="CLUB"/><span>더 클럽</span></label>
                            </td>
                        </div>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="martLeafletNm">마트전단명</option>
                                <option value="leafletNo">마트전단ID</option>
                            </select>
                        </td>
                        <td colspan="4">
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 300px;" minlength="2" >
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색결과 : <span id="dspLeafletTotalCount">0</span>건</h3>
            </div>

            <div class="topline"></div>

        </div>

        <div class="mg-t-20 mg-b-20" id="dspLeafletListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspLeafletSetForm" onsubmit="return false;">
            <input type="hidden" id="leafletNo" name="leafletNo">

            <table class="ui table">
                <caption>마트전단 등록/수정</caption>
                <colgroup>
                    <col width="170px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="siteType" name="siteType" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${dskSiteGubun}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>마트전단 명&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="martLeafletNm" name="martLeafletNm" class="ui input medium mg-r-5" style="width: 350px; float: left;" maxlength="20">
                            <span class="text" style="float: left;">
                                 ( <span style="color:red;" id="martLeafletNmCnt" class="cnt">0</span>/20)
                            </span>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th>행사기간<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="com wrap-title sub">
        <h3 class="text">• 전시 일자가 중복될 경우 최근 등록된 마트전단이 노출됩니다. 전시 시작 및 종료일을 정확히 설정해 주세요.</h3>
    </div>

    <br/>

    <div style="width: 43%; height: 350px; float: left;">
        <div class="com wrap-title sub">
            <h3 class="title">• 카테고리 정보</h3>
        </div>

        <div class="topline"></div>

        <br/>

        <div class="mg-t-20 mg-b-20" id="dspLeafletItemCategoryListGrid" style="width: 100%; height: 375px;"></div>

        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="dspLeafletItemCategory.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="dspLeafletItemCategory.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="dspLeafletItemCategory.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="dspLeafletItemCategory.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>

        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="100px;">
                    <col width="*">
                    <col width="100px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>카테고리명</th>
                    <td>
                        <input type="text" id="dispCateNm" name="dispCateNm" class="ui input medium mg-r-5" style="width: 200px;" readonly/>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <div class="ui form inline">
                            <select id="setDspLeafletItemCategoryUseYn" name="setDspLeafletItemCategoryUseYn" class="ui input medium mg-r-10" style="width: 130px">
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub" style="float: left;">
            <h3 class="text">• 대표상품 카테고리 상품 미등록 시 마트전단이 저장되지 않습니다.</h3>
        </div>

        <div class="ui form inline" style="float: right; margin-top: 20px;">
            <button type="button" class="ui button large mg-r-5 gray-dark pull-left" id="setDspLeafletItemCategory" >변경</button>
        </div>
    </div>

    <div style="width: 55%; height: 350px; float: right;">
        <div class="com wrap-title sub">
            <h3 class="title">• 상품 등록/수정</h3>
        </div>

        <div class="topline"></div>

        <br/>

        <span class="text pull-left" style="margin-top: 5px;">* 상품건수 : <span id="itemTotalCount">0</span>건</span>

        <span class="ui form inline mg-l-10 pull-left" style="margin-bottom: 5px;">
            <span class="text ">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
            <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
        </span>

        <span class="ui form inline mg-l-10 pull-right">
            <button type="button" class="ui button medium" onclick="dspLeafletItem.openItemReadPopup();">일괄등록</button>
        </span>

        <br/>

        <div class="mg-t-20 mg-b-20" id="dspLeafletItemListGrid" style="width: 100%; height: 350px;"></div>

        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="dspLeafletItem.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="dspLeafletItem.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="dspLeafletItem.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="dspLeafletItem.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button class="ui button medium" id="addItemPopUp">상품조회</button>
            <li style="margin-left : 15px; margin-right: 5px; margin-top: 3px; float: right;">* 전시영역</li>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="dspLeafletItem.chgUseYn('N');">사용안함</button>
            <button type="button" class="ui button medium" onclick="dspLeafletItem.chgUseYn('Y');">사용</button>
            <li style="margin-right: 5px; margin-top: 3px; float: right;">* 사용여부</li>
        </span>

        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="text">• 상품등록, 사용여부 변경 후 반드시 저장버튼을 클릭하셔야 반영이 됩니다.</h3>
        </div>
    </div>

</div>

<div class="ui center-button-group mg-t-15" style="margin-top: 360px;">
    <span class="inner">
        <button class="ui button xlarge cyan font-malgun" id="setDspLeafletItemBtn">저장</button>
        <button class="ui button xlarge white font-malgun" id="resetDspLeafletItemBtn">초기화</button>
    </span>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspLeafletMain.js?${fileVersion}"></script>
<script>
    ${dspLeafletGridBaseInfo}
    ${dspLeafletItemCategoryListGridBaseInfo}
    ${dspLeafletItemListGridBaseInfo}
</script>