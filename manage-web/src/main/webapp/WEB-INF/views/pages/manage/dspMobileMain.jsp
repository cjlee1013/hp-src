<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">MOBILE 메인관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspMainSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>MOBILE 메인관리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="20%">
                        <col width="10%">
                        <col width="20%">
                        <col width="15%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="searchPeriodType" name="searchPeriodType" style="width:180px;float:left;" data-default="등록일">
                                    <c:forEach var="codeDto" items="${dspMainPeriodType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;margin-left: 10px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMobileMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMobileMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dspMobileMain.initSearchDate('-1m');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="dspMobileMain.initSearchDate('-3m');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchSiteType" style="width: 180px" name="searchSiteType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspMainSiteType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>전시여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchDispYn" style="width: 180px" name="searchDispYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${displayYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchLoc1Depth" style="width: 180px" name="searchLoc1Depth" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dspMainLoc}" varStatus="status">
                                        <c:if test="${codeDto.ref1 eq 'M' }">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <c:forEach var="codeDto" items="${dspMainSchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="dspMobileMainTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="dspMobileMainListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 배너기본정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="dspMobileMainSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>mobile 메인 등록/수정</caption>
                <colgroup>
                    <col width="150px">
                    <col width="350px">
                    <col width="150px">
                    <col width="*">
                </colgroup>
                <tbody>
                    <tr>
                        <th>번호</th>
                        <td>
                            <input id="mainNo" name="mainNo" type="text" class="ui input medium mg-r-5" style="width: 200px;" readonly >
                        </td>
                        <th>사이트구분 <span class="text-red">*</span></th>
                        <td>
                            <select id="siteType" style="width: 180px" name="siteType" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${dspMainSiteType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>전시위치 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <select id="loc1Depth" style="width: 180px;float: left;" name="loc1Depth" class="ui input medium mg-r-10" >
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${dspMainLoc}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'M' }">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>전시여부 <span class="text-red">*</span></th>
                        <td>
                            <select id="dispYn" style="width: 180px" name="dispYn" class="ui input medium mg-r-10" >
                                <c:forEach var="codeDto" items="${displayYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th> <div id ="divType" style="float: left;margin-right: 5px;">전시순서</div> <span class="text-red">*</span></th>
                        <td>
                            <div id = "priorityDiv" style="">
                                <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="6" >
                            </div>
                            <div id ="splashDiv" style="display:none ">
                                <select id="splash" style="width: 180px" name="splash" class="ui input medium mg-r-10" >
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${dspMainSplType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>

                    <tr id ="dispPeriod">
                        <th>전시기간 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <span class="ui form inline">
                                <input type="text" id="dispStartDt" name="dispStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:150px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="dispEndDt" name="dispEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:150px;">
                            </span>
                        </td>
                    </tr>
                    <tr id="displayTimeTr" style="display: none;">
                        <th>전시구간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="dispTimeYn" name="dispTimeYn" class="ui input medium mg-r-5" style="width: 100px;" >
                                    <c:forEach var="dispTimeYn" items="${useYn}" varStatus="status">
                                        <option value="${dispTimeYn.mcCd}" <c:if test="${dispTimeYn.mcCd eq 'N'}">selected</c:if>>${dispTimeYn.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <span id="displayTimeSpan" style="display: none;">
                                <input type="text" id="dispStartTime" name="dispStartTime" class="ui input medium mg-r-5" style="width: 60px;" maxlength="5"><span class="text">~</span>
                                <input type="text" id="dispEndTime" name="dispEndTime" class="ui input medium mg-l-5 mg-r-5" style="width: 60px;" maxlength="5"><span class="text">(시:분)</span>
                            </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>배너명 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input id="bannerNm" name="bannerNm" type="text" class="ui input medium mg-r-5" style="width: 200px;" maxlength="20" >
                                <span class="text">
                                     ( <span style="color:red;" id="bannerNmCnt" class="cnt">0</span>/20)
                                </span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>


 <%--   <div id ="bannerDesc" ></div>--%>
    <%--/배너 상세 정보 역영/--%>

    <div id ="bannerTemplateLogo" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainLogoDetail.jsp"/>

    </div>
    <div id ="bannerTemplateBig" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainBigDetail.jsp"/>

    </div>
    <div id ="bannerTemplateQuick" style="display: none" class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainQuickDetail.jsp"/>

    </div>
    <div id ="bannerTemplateNoti" style="display:none " class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainNotiDetail.jsp"/>

    </div>
    <div id ="bannerTemplateSplash" style="display:none " class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainSplashDetail.jsp"/>

    </div>
    <div id ="bannerTemplateMkt" style="display:none " class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainMktDetail.jsp"/>

    </div>

    <div id ="bannerTemplateDelivery" style="display:none " class="bannerTemplate">
        <jsp:include page="/WEB-INF/views/pages/manage/dspMobileMainDetail/dspMobileMainDeliveryDetail.jsp"/>

    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setMobileMainBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


<%--링크 관련 셀렉트 옵션 및 입력창 관리--%>
<div id ="linkCommon" style="display: none">
    <input name="linkList[].linkInfo" style="width: 150px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
    <input style="width: 150px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
    <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);">검색</button>
</div>

<div id ="linkUrl" style="display: none">
    <input name="linkList[].linkInfo" style="width: 600px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="100" value="http://">
</div>
<div id ="linkGnbDevice" style="display: none">
    <select style="width: 100px;float: left"  name="linkList[].gnbDeviceType" class="ui input medium mg-r-10 gnbDeviceType">
        <c:forEach var="codeDto" items="${dspMainGnbType}" varStatus="status">
             <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeHome" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'HOME'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id = "storeTypeClub" style="display: none">
    <select style="width: 100px;float: left" name="linkList[].storeType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>



<div id = "dlvStoreTypeClub" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'CLUB'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>
<div id = "dlvStoreTypeHome" style="display: none">
    <label class="ui checkbox" style="display: inline-block;float: left;margin-left: 20px;margin-right: 20px;">
        <input type="checkbox" name="linkList[].dlvStoreYn" class="dlvStoreYn" value ="N" >
        <span>택배점 설정</span>
        <select  style="width: 100px;float: right;margin-left: 10px;" name="linkList[].dlvStoreId" class="ui input medium mg-r-10 dlvStoreList" disabled>
            <option value="">택배점</option>
            <c:forEach var="codeDto" items="${dlvStoreList}" varStatus="status">
                <c:if test="${codeDto.storeType eq 'HYPER'}">
                    <option value="${codeDto.storeId}">${codeDto.storeNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="hidden" name="linkList[].linkOptions" class="linkOptions" value ="">
    </label>
</div>




<div id = "linkTypeHome" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
    </c:forEach>
</div>
<div id = "linkTypeClub" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.ref1 ne 'HOME' }">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>

<div id = "linkTypeHyper" style="display: none">
    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
        <c:if test="${codeDto.mcCd == 'EXH' or codeDto.mcCd == 'EVENT' or codeDto.mcCd == 'ITEM_TD' or codeDto.mcCd == 'URL' or codeDto.mcCd == 'NONE'}">
            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
        </c:if>
    </c:forEach>
</div>



<%--링크 관련 셀렉트 옵션 및 입력창 관리 끝--%>

<div style="display:none;" class="storePop" >
    <form id="storeSelectPopForm" name="storeSelectPopForm" method="post" action="/common/popup/storeSelectPop">
        <input type="text" name="storeTypePop" value="HYPER"/>
        <input type="text" name="storeTypeRadioYn" value="N"/>
        <input type="text" name="setYn" value="Y"/>
        <input type="text" name="storeList" value=""/>
        <input type="text" name="callBackScript" value="dspMainCommon.storePopupCallback"/>
    </form>
</div>



<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainBig.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainQuick.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainNoti.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainLogo.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainSplash.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainMkt.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMobileMainDetail/dspMobileMainDelivery.js?${fileVersion}"></script>


<script>
    // 판매업체 리스트 리얼그리드 기본 정보
    ${dspMobileMainListGridBaseInfo}

    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>
