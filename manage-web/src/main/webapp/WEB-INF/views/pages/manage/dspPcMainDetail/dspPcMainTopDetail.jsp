<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title sub">
    <h3 class="title">• 최상단 띠배너</h3>
</div>
<%--메임 탑 베너 상세 입력 폼--%>
<div class="ui wrap-table horizontal mg-t-10" id="TOP">
    <form id="topBannerSetForm" onsubmit="return false;" >
        <table class="ui table" >
            <caption>최상단 띠배너</caption>
            <colgroup>
                <col width="150px">
                <col width="500px">
                <col width="150px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th>이미지 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="imgUrl" id ="imgUrl" value="">
                                        <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                        <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn" >등록</button>
                                </div>
                            </div>
                            <div style="width:170px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br>
                                사이즈 : 1200*80<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG / JPEG / PNG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>배경 색상 <span class="text-red">*</span></th>
                    <td>
                        <input type="text" id="bgColor" name="bgColor" class="ui input medium mg-r-5" style="width: 150px;" placeholder="색상코드 입력(ex : #ffffff)"/>
                    </td>
                    <th>닫기버튼 선택 <span class="text-red">*</span></th>
                    <td>
                        <c:forEach var="codeDto" items="${dspMainCloseType}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="closeType" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="closeType" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>링크 분할 설정 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <c:forEach var="codeDto" items="${dspMainDivision}" varStatus="status">
                            <c:choose>
                                <c:when test="${ status.index eq 0 }">
                                    <label class="ui radio inline"><input type="radio" name="division" class="ui input medium" value="${codeDto.mcCd}" checked><span>${codeDto.mcNm}</span></label>
                                </c:when>
                                <c:otherwise>
                                    <label class="ui radio inline"><input type="radio" name="division" class="ui input medium" value="${codeDto.mcCd}"><span>${codeDto.mcNm}</span></label>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </td>
                </tr>


                <tr>
                    <input type ="hidden" name="linkList[].linkNo" value="1"/>
                    <th>링크 1 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form">
                            <select id="topBannerLinktype1"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                                <%--<c:choose>
                                    <c:when test="${siteType eq 'HOME'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref1 eq 'HOME'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${siteType eq 'CLUB'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref2 eq 'CLUB'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>--%>
                            </select>
                            <div id="topBannerLinkInfo1">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);">검색</button>
                                <button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);">초기화</button>
                            </div>
                             <%--<input type ="hidden" name="linkList[].linkNo" value="1"/>--%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <input type ="hidden" name="linkList[].linkNo" value="2"/>
                    <th>링크 2 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form">
                            <select id="topBannerLinktype2"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" disabled >
                                <%--<c:choose>
                                    <c:when test="${siteType eq 'HOME'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref1 eq 'HOME'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${siteType eq 'CLUB'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref2 eq 'CLUB'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>--%>
                            </select>
                            <div id="topBannerLinkInfo2">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" disabled>검색</button>
                                <button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);" >초기화</button>
                            </div>
                             <%--<input type ="hidden" name="linkList[].linkNo" value="2"/>--%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <input type ="hidden" name="linkList[].linkNo" value="3"/>
                    <th>링크 3 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form">
                            <select id="topBannerLinktype3" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" disabled>
                                <%--<c:choose>
                                    <c:when test="${siteType eq 'HOME'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref1 eq 'HOME'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${siteType eq 'CLUB'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref2 eq 'CLUB'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>--%>
                            </select>
                            <div id="topBannerLinkInfo3">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" disabled>검색</button>
                                <button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);" >초기화</button>
                            </div>
                           <%-- <input type ="hidden" name="linkList[].linkNo" value="3"/>--%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <input type ="hidden" name="linkList[].linkNo" value="4"/>
                    <th>링크 4 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <span class="ui form">
                            <select id="topBannerLinktype4" data-division="1" style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" disabled >
                               <%-- <c:choose>
                                    <c:when test="${siteType eq 'HOME'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref1 eq 'HOME'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${siteType eq 'CLUB'}">
                                        <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                            <c:if test="${codeDto.ref2 eq 'CLUB'}">
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>--%>
                            </select>
                            <div id="topBannerLinkInfo4">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" disabled>검색</button>
                                <button class="ui button medium white font-malgun resetLinkInfoBtn" style="float: right" onclick="dspMainTopBannerDetail.resetLinkInfo(this);" >초기화</button>
                            </div>
                           <%--  <input type ="hidden" name="linkList[].linkNo" value="4"/>--%>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </form><%--메임 탑 베너 상세 입력 폼--%>
</div><%--//  <div class="ui wrap-table horizontal mg-t-10" id="TOP">--%>


