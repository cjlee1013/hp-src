<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title sub">
    <h3 class="title">• 퀵메뉴</h3>
</div>
<div class="ui wrap-table horizontal mg-t-10" id="BIG">
    <form id="quickBannerSetForm" onsubmit="return false;" >
        <table class="ui table" >
            <caption>GNB상단배너</caption>
            <colgroup>
                <col width="100px">
                <col width="1000px">
                <col width="*">
            </colgroup>
            <tbody>

            <tr class ="quickBannerDiv1" >
                <input type ="hidden" name="linkList[].linkNo" value="1"/>
                <th rowspan="2">이미지 <span class="text-red">*</span></th>
                <td>
                   <span class="ui form">
                        <span style="float: left;line-height: 25px;margin-right: 20px;">링크</span>
                        <select id="quickBannerLinktype1"  style="width: 100px;float: left" name="linkList[].linkType" class="ui input medium mg-r-10 linkeType" >
                            <%--<c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>--%>
                        </select>
                        <div id="quickBannerLinkInfo1">
                                <input name="linkList[].linkInfo" style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
                                <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
                                <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainCommon.popLink(this);" >검색</button>
                        </div>
                       <%-- <input type ="hidden" name="linkList[].linkNo" value="1"/>--%>
                    </span>
                </td>
                <td  rowspan="2">
                    <button class="ui button medium white font-malgun" style="float: right" onclick="dspMainQuickBannerDetail.resetLinkInfo()">초기화</button>
                </td>
            </tr>
            <tr class ="quickBannerDiv1" >
                <td>
                    <div style="position:relative; display:inline-block;">
                        <div class="text-center preview-image" style="height:132px;">
                            <div class="imgDisplayView" style="display:none;">
                                <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                    <img class="imgUrlTag" width="132" height="132" src="">
                                    <input type="hidden" class="imgUrl" name="linkList[].imgUrl" value="">
                                    <input type="hidden" class="imgHeight" name="linkList[].imgHeight">
                                    <input type="hidden" class="imgWidth" name="linkList[].imgWidth">
                                </div>
                            </div>
                            <div class="imgDisplayReg" style="padding-top: 50px;">
                                <button type="button" class="ui button medium addImgBtn" >등록</button>
                            </div>
                        </div>
                        <div style="width:190px; height:132px;display: inline-block;padding-left: 10px;">
                            <br><br><br>
                            최적화 가이드<br>
                            사이즈 : 100*100<br>
                            용량 : 2MB 이하<br>
                            확장자 : JPG / JPEG / PNG / GIF<br>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
