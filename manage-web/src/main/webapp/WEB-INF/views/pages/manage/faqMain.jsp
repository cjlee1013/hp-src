<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">
            <c:if test="${dispArea eq 'FRONT'}">
                사이트 FAQ
            </c:if>
            <c:if test="${dispArea eq 'PARTNER'}">
                파트너 FAQ
            </c:if>
        </h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="faqSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  faq.search();">
                <input type="hidden" id="searchDispArea" name="searchDispArea" value="${dispArea}">
                <table class="ui table">
                    <caption>FAQ 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="150px">
                        <col width="120px">
                        <col width="150px">
                        <col width="120px">
                        <col width="150px">
                        <col >
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>분류</th>
                        <td >
                            <select class="ui input medium mg-r-5" id="searchClassCd" name="searchClassCd" style="width:150px;float:left;" data-default="전체">
                                <option value="" selected>전체</option>
                                <c:forEach var="codeDto" items="${faqClass}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>사용여부</th>
                        <td >
                            <select class="ui input medium mg-r-5" id="searchUseYn" name="searchUseYn" style="width:150px;float:left;" data-default="전체">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>

                        <c:if test="${dispArea eq 'FRONT'}">
                            <th>
                                사이트구분
                            </th>
                            <td>
                                <select class="ui input medium mg-r-5" id="searchSiteType" name="searchSiteType" style="width:150px;float:left;" data-default="전체">
                                    <option value="ALL" selected>전체</option>
                                    <option value="HOME">홈플러스</option>
                                    <option value="CLUB">더클럽</option>
                                </select>
                            </td>
                        </c:if>
                        <c:if test="${dispArea eq 'PARTNER'}">
                            <td></td><td></td>
                        </c:if>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left" id="searchBtn"><i class="fa fa-search"></i> 검색</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchType" name="searchType" style="width:130px;float:left;" data-default="질문">
                                <c:forEach var="codeDto" items="${searchType}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>

                        <td colspan="4" >
                            <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" style="width: 500px;" minlength="2">
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 white pull-left" id="searchResetBtn">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="faqTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="faqListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="faqSetForm" onsubmit="return false;">
            <input type="hidden" id="dispArea" name="dispArea" value="${dispArea}">
            <table class="ui table">
                <caption>FAQ 등록/수정</caption>
                <colgroup>
                    <col width="100px">
                    <col width="100px">
                    <col width="100px">
                    <col width="100px">
                    <col width="100px">
                    <col width="120px">
                    <col width="150px">
                    <col >
                </colgroup>
                <tbody>
                    <tr>
                        <th>번호</th>
                        <td>
                            <div class="ui form inline">
                                <input id="faqNo" name="faqNo" type="text" class="ui input medium mg-r-5" style="width:80px" readonly>
                            </div>
                        </td>
                        <th>분류 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="classCd" name="classCd" style="float:left;width:150px;" data-default="전체">
                                    <option value="" selected>선택</option>
                                    <c:forEach var="codeDto" items="${faqClass}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>


                        <th>사용여부 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="useYn" name="useYn" style="width:150px" >
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>전시순서 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="priority" name="priority" type="text" class="ui input medium mg-r-5" maxlength="3" style="width: 150px;">
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <th>제목  <span class="text-red">*</span></th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input id="faqSubject" name="faqSubject" style="float:left;width: 600px;" type="text" class="ui input medium mg-r-5" maxlength="100">
                                <span class="text">( <span style="color:red;" id="textCountFaqSubject">0</span> / 100 )</span>
                            </div>
                        </td>
                        <c:if test="${dispArea eq 'FRONT'}">
                            <th>사이트구분 <span class="text-red">*</span></th>
                            <td>
                                <label class="ui checkbox" style="display: inline-block">
                                    <input type="checkbox"  name="dispHomeYn"  id="dispHomeYn" value="Y" checked>
                                    <span>홈플러스</span>
                                </label>
                                <label class="ui checkbox" style="display: inline-block">
                                    <input type="checkbox"  name="dispClubYn"  id="dispClubYn" value="Y" checked>
                                    <span>더클럽</span>
                                </label>
                            </td>
                        </c:if>
                    </tr>
                    <tr>
                        <th>내용 <span class="text-red">*</span></th>
                        <td colspan="8">
                            <span class="ui form inline">
                                <!--<textarea id="faqBody" name="faqBody" class="ui input mg-r-5" style="float:left;width: 100%; height: 200px;" ></textarea>-->
                                <input type="hidden" id="faqBody" name="faqBody"/>
                                <div id="faqBodyEdit" name="faqBodyEdit"></div>
                            </span>
                        </td>

                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setFaqBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>


<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/faqMain.js?${fileVersion}"></script>

<script>
	// 브랜드 리스트 리얼그리드 기본 정보
    const hmpImgUrl = '${homeImgUrl}';
    const hmpImgFrontUrl = '${hmpImgFrontUrl}';
    ${faqListGridBaseInfo}
</script>
