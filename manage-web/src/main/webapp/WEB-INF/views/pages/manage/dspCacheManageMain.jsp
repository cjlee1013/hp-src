<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">Cache 강제 적용 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="dspCacheManageSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>Cache 강제 적용 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>캐쉬명</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <input type="text" class="ui input medium" name="searchCacheNm" id="searchCacheNm">
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchUseYn" style="width: 180px" name="searchUseYn" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan mg-b-5">검색</button><br />
                                <button type="button" id="searchResetBtn" class="ui button large">초기화</button>
                            </div>
                        </td>
                   </tr>
                   </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="dspCacheManageTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="dspCacheManageListGrid" style="width: 100%; height: 600px;"></div>
    </div>

    <div class="topline"></div>

    <!-- 캐쉬 정보 -->
    <div class="com wrap-title sub"></div>
    <div class=""></div>
    <div class="ui wrap-table horizontal">
        <form id="dspCacheManageSetForm" onsubmit="return false;">
            <input type="hidden" id="manageSeq" name="manageSeq">
            <table class="ui table" id="cacheInformation">
                <caption>메뉴 정보</caption>
                <colgroup>
                    <col width="150px;">
                    <col width="250px;">
                    <col width="150px;">
                    <col width="*;">
                </colgroup>
                <tbody>
                    <tr>
                        <th>캐쉬명</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium" name="cacheNm" id="cacheNm">
                            </div>
                        </td>
                        <th>캐쉬 URL</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium" name="cacheUrl" id="cacheUrl">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="useYn" style="width: 100%" name="useYn" class="ui input medium mg-r-10" >
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>캐쉬 강제 적용 <br> ( 임시 )</th>
                        <td>
                            <div class="ui form inline">
                                <button class="ui button large mg-r-5 dark-blue" id="btnForceUpdate">강제적용</button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspCacheManageMain.js?${fileVersion}"></script>
<script>
    // GNB 리스트 리얼그리드 기본 정보
    ${dspCacheManageListGridBaseInfo}
</script>