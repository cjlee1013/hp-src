<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    /* 캘린더모드 ui-kit.css 맨 아래에 추가할 것 */
    .ui.table.calendar-mode th,td {
        border-left: solid 1px #eee!important;
        border-right: solid 1px #eee!important;
        border-bottom: solid 1px #eee!important;
    }
    .ui.table.calendar-mode td:hover {
        background-color: #f9f9f9;
        cursor: pointer;
    }
    .ui.table.calendar-mode {
        height: 20px;
        position: relative;
    }
    .ui.table.calendar-mode {
        position: relative; top: 0; right: 0; font-size: 11px;
    }
</style>

<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">홈테마 상품 관리</h2>
    </div>

    <br/>
    <th>스토어 타입</th>
    <td>
        <label class="ui radio small inline"><input type="radio" name="siteType" id="siteHome" value="HOME" checked><span>HOME</span></label>
        <label class="ui radio small inline"><input type="radio" name="siteType" id="siteClub" value="CLUB"><span>CLUB</span></label>
    </td>
    <br/>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <div style="text-align: center;">
            <div class="ui form inline" style="display: inline-block;">
                <button id="prevYear" class="ui button small mint mg-r-5">이전 년도</button>
                <button id="prevMonth" class="ui button small mint mg-r-5">이전 달</button>
            </div>
            <span style="position: relative;top: 6px;margin: 2px 20px;font-size: 18px;font-weight: bold;">
            <span id="curYear"></span>년 <span id="curMonth"> </span>월
        </span>
            <div class="ui form inline" style="display: inline-block;">
                <button id="nextMonth" class="ui button small mint mg-r-5">다음 달</button>
                <button id="nextYear" class="ui button small mint mg-r-5">다음 년도</button>
            </div>
        </div>

        <div class="com wrap-title">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table calendar-mode" id="mainThemeItemCalendar">
                    <caption>영업일 관리</caption>
                    <colgroup>
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                        <col width="*">
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="text-center">월</th>
                        <th class="text-center">화</th>
                        <th class="text-center">수</th>
                        <th class="text-center">목</th>
                        <th class="text-center">금</th>
                        <th class="text-center"><span class="text-red">토</span></th>
                        <th class="text-center"><span class="text-red">일</span></th>
                    </tr>
                    </thead>
                    <tbody><!-- 캘린더 동적 생성영역... --></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- 홈테마 정보 -->
    <div style="width: 34%; height: 350px; float: left;">
        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">테마 정보</h3>
            </div>
            <div class="topline"></div>
        </div>
        <br/>
        <br/><br/>
        <br/>
        <div class="mg-t-20 mg-b-20" id="dspMainThemeListGrid" style="width: 100%; height: 350px;"></div>

    </div>

    <!-- 홈테마 상품정보 -->
    <div style="width: 63%; height: 750px; float: right;">
        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">상품 등록/수정</h3>
                <h4 class="text" style="font-weight: normal;">&nbsp;&nbsp; • 상품 등록 시, 저장하지 않은 순서 변경내역은 초기화됩니다.</h4>
            </div>
            <div class="topline"></div>
            <br/>

            <span class="text pull-left" style="margin-top: 5px;">* 상품 수 : <span id="mainThemeItemTotalCount">0</span>건</span>

            <span class="ui form inline mg-l-10" style="margin-left: 15px;">
                <span class="text">등록상품 검색 : </span> <input type="text" id="schGridItemNo" name="schGridItemNo" class="ui input medium mg-l-5" maxlength="15" style="width:150px">
                <button type="submit" id="schGridItemBtn" class="ui button medium gray mg-l-10">검색</button>
            </span>

            <button type="button" class="ui button small" style="float: right;" onclick="dspMainThemeItemMng.openItemReadPopup();">일괄등록</button>
            <br/><br/>
        </div>

        <input type="hidden" id="themeId" name="themeId">
        <input type="hidden" id="itemSeq" name="itemSeq">

        <div class="mg-t-20 mg-b-20" id="dspMainThemeItemListGrid" style="width: 100%; height: 350px;"></div>

        <br/><br/><br/><br/>

        <span class="ui left-button-group">
            <button type="button" key="top" class="ui button small mg-r-5" onclick="dspMainThemeItemMng.moveRow(this);"><i class="fa fa-play fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button type="button" key="bottom" class="ui button small mg-r-5" onclick="dspMainThemeItemMng.moveRow(this);"><i class="fa fa-play fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="firstTop" class="ui button small mg-r-5" onclick="dspMainThemeItemMng.moveRow(this);"><i class="fa fa-backward fa-rotate-90" style="margin-top:-3px;"></i></button>
            <button type="button" key="lastBottom" class="ui button small mg-r-5" onclick="dspMainThemeItemMng.moveRow(this);"><i class="fa fa-backward fa-rotate-270" style="margin-top:-3px;"></i></button>
            <button class="ui button medium gray-light font-malgun" id="sortPriority">순서변경</button>
        </span>

        <span class="ui right-button-group" style="font-size: 13px;">
            <button type="button" class="ui button medium" onclick="dspMainThemeItemMng.chgDispYn('N');">전시안함</button>
            <button type="button" class="ui button medium" onclick="dspMainThemeItemMng.chgDispYn('Y');">전시</button>
            <li style="margin-right: 5px; margin-top: 3px; float: right;">* 전시여부 변경</li>
        </span>

        <br/>

        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="80px">
                    <col width="200px">
                    <col width="170px">
                    <col width="200px">
                </colgroup>
                <tbody>
                <tr>
                    <th>구분</th>
                    <td colspan="2">
                        <label class="ui radio inline">
                            <input type="radio" name="themeItemType" class="ui input" value="ITEM" checked/><span>상품</span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="themeItemType" class="ui input" value="BANNER"/><span>모바일 배너</span>
                        </label>
                    </td>
                </tr>
                <tr id="itemTr">
                    <th>상품등록</th>
                    <td colspan="2">
                        <select id="storeType" name="storeType" class="ui input medium mg-r-10" style="width: 100px; float: left;">
                            <option value="TD">점포상품</option>
                            <option value="DS">셀러상품</option>
                        </select>
                        <div id="dispStore">
                            <select style="width: 100px;float: left;" id="itemStoreType" name="itemStoreType" class="ui input medium mg-r-10 storeType">
                                <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
                                    <c:if test="${codeDto.mcCd eq 'HYPER'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                        <button class="ui button medium gray-light font-malgun" id="addItemPopUp">상품등록</button>
                    </td>
                </tr>
                <tr id="linkTr" style="display: none;">
                    <th>링크</th>
                    <td colspan="2">
                        <select id="linkType" class="ui input medium mg-r-10" style="width: 100px; float: left;">
                    <c:forEach var="codeDto" items="${dspMainLinkType}" varStatus="status">
                        <c:if test="${codeDto.ref2 eq 'THEME'}">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:if>
                    </c:forEach>
                        </select>
                        <div id="linkInfoDiv">

                        </div>
                    </td>
                </tr>
                <tr class ="bannerDiv" style="display: none;">
                    <th>배너</th>
                    <td colspan="2">
                        <div style="position:relative; display:inline-block;">
                            <div class="text-center preview-image" style="height:132px;">
                                <div class="imgDisplayView" style="display:none;">
                                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm" >삭제</button>
                                    <div class="uploadType itemImg" inputid="uploadMNIds" style="cursor:hand;">
                                        <img class="imgUrlTag" width="132" height="132" src="">
                                        <input type="hidden" class="imgUrl" name="imgUrl">
                                        <input type="hidden" class="imgHeight" name="imgHeight">
                                        <input type="hidden" class="imgWidth" name="imgWidth">
                                    </div>
                                </div>
                                <div class="imgDisplayReg" style="padding-top: 50px;">
                                    <button type="button" class="ui button medium addImgBtn">등록</button>
                                </div>
                            </div>
                            <div style="width:132px; height:132px;display: inline-block;padding-left: 10px;">
                                <br><br><br>
                                최적화 가이드<br><br>
                                사이즈 : 670*N<br>
                                용량 : 2MB 이하<br>
                                확장자 : JPG<br>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="bannerRegDiv" class="ui form inline" style="margin-top: 20px; margin-bottom:20px; float: right;">
            <button type="button" class="ui button large mg-r-5 cyan pull-left" id="setDspMainThemeItem" style="display: none;">등록</button>
            <span style="margin:4px;"></span>
            <button type="button" class="ui button large mg-r-5 white pull-left" id="resetDspMainThemeItem">초기화</button>
        </div>
    </div>

</div>

<div id="storeTypeHome" style="display: none">
    <select style="width: 100px;float: left;" name="itemStoreType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.mcCd eq 'HYPER'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="storeTypeClub" style="display: none">
    <select style="width: 100px;float: left;" name="itemStoreType" class="ui input medium mg-r-10 storeType">
        <c:forEach var="codeDto" items="${dspMainStoreType}" varStatus="status">
            <c:if test="${codeDto.ref1 eq 'CLUB'}">
                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
            </c:if>
        </c:forEach>
    </select>
</div>

<div id="storeTypeNone" style="display: none">
    <select style="width: 100px;float: left;" name="itemStoreType" class="ui input medium mg-r-10 storeType" readonly>
        <option value="DS">-</option>
    </select>
</div>

<div id="linkCommon" style="display: none">
    <td style="float: left;">
        <input name="linkInfo" style="width: 100px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" readonly="">
        <input style="width: 200px;float: left" type="text" class="ui input medium mg-r-5 linkInfoNm" maxlength="40" readonly="">
        <button class="ui button medium gray-light font-malgun addLinkInfoBtn" style="float: left" onclick="dspMainThemeItemMng.popLink();">검색</button>
    </td>
</div>

<div id="linkUrl" style="display: none">
    <td style="float: left;">
        <input name="linkInfo" style="width: 400px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" maxlength="40" value="http://">
    </td>
</div>

<div id="linkNone" style="display: none">
    <td style="float: left;">
        <input name="linkInfo" style="width: 400px;float: left" type="text" class="ui input medium mg-r-5 linkInfo" value="" readonly>
    </td>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/manage/dspMainThemeItemMain.js?${fileVersion}"></script>

<script>
    ${dspMainThemeGridBaseInfo}
    ${dspMainThemeItemGridBaseInfo}

    var hmpImgUrl = '${homeImgUrl}';
</script>