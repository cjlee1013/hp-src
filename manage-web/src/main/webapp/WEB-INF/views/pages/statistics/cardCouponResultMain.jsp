<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="cardCouponResultSearchForm" name="cardCouponResultSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">카드사별쿠폰실적조회</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="descriptionBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
                <span class="text">* 장바구니 쿠폰만 집계합니다.</span>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>카드사별쿠폰실적조회</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="40%">
                        <col width="8%">
                        <col width="35%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>주문일</th>
                            <td class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">이번주</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                            </td>
                            <th>점포유형</th>
                            <td>
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="HYPER">Hyper</option>
                                    <option value="EXP">Express</option>
                                </select>
                            </td>
                            <td rowspan="2">
                                <div class="ui form">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>검색어</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                        <option value="couponNo">쿠폰번호</option>
                                        <option value="couponNm">쿠폰명</option>
                                    </select>
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 150px">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
    <div style="margin-top: 30px;">
        <div id="cardCouponResultGrid" style="height: 250px;"></div>
    </div>

    <p>&nbsp;</p>

    <div style="margin-top: 10px;">
        <div id="cardCouponResultDetailGrid" style="width: 100%;height: 400px;"></div>
    </div>
</div>

<script>
    ${cardCouponResultGridBaseInfo}
    ${cardCouponResultDetailGridBaseInfo}

    $(document).ready(function() {
        cardCouponResultMain.init();
        cardCouponResultGrid.init();
        cardCouponResultDetailGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/cardCouponResultMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>