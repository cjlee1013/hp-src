<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [기간별 점포 판매 현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [기간별 점포 판매 현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        주문건수
                    </td>
                    <td>
                        당일 전체 주문취소 반영
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        주문금액
                    </td>
                    <td>
                        총상품금액(상품금액 x 수량) – 상품할인액 (행사할인,상품쿠폰,임직원할인)<br/>
                        ㄴ 결제금액이 아님, 장바구니 / 중복 쿠폰 / 마일리지할인 반영 안함<br/>
                        ㄴ 합배송 주문금액 포함
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        취소건수
                    </td>
                    <td>
                        취소 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합 (대체주문 취소건수 포함 안함)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        취소금액
                    </td>
                    <td>
                        취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합 (대체주문 취소건수 포함 안함)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        반품건수
                    </td>
                    <td>
                        반품 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        반품금액
                    </td>
                    <td>
                        반품 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        대체주문건수
                    </td>
                    <td>
                        대체 주문번호 건수 총합
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        대체주문금액
                    </td>
                    <td>
                        대체된 상품 금액 총합
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>