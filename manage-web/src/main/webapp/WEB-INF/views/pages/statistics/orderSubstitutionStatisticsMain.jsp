<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/orderSubstitutionStatisticsMain.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <!-- 검색영역 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">대체공제 쿠폰 통계</h2>
    </div>
    <div class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="orderSubstitutionStatisticsSearchForm" name="orderSubstitutionStatisticsSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>대체공제 쿠폰 통계</caption>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <div class="ui form inline">
                                    <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:120px;" placeholder="시작일">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:120px;" placeholder="종료일">
                                </div>
                            </div>
                        </td>
                        <th>점포</th>
                        <td class="ui form inline">
                            <c:choose>
                            <c:when test="${empty userStoreId}">
                            <select id="schStoreType" name="schStoreType" style="width: 110px;" class="ui input medium mg-r-10">
                                </c:when>
                                <c:otherwise>
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" readonly>
                                    </c:otherwise>
                                    </c:choose>
                                        <option value="HYPER">HYPER</option>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" style="width: 150px;" readonly value="${userStoreId}">
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" style="width: 150px;" readonly value="${userStoreNm}">
                                <c:if test="${empty userStoreId}"><button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button></c:if>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과</h3>
        </div>
        <div class="topline"></div>
        <div id="orderSubstitutionStatisticsGrid" style="width: 100%; height: 330px;"></div>
    </div>
</div>

<script>
    ${orderSubstitutionStatisticsGridBaseInfo}
    var orderSubstitutionStatisticsGrid = GridUtil.initializeGrid("orderSubstitutionStatisticsGrid", orderSubstitutionStatisticsGridBaseInfo);
</script>


