<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="zipcodeOrderStatisticsSearchForm" name="zipcodeOrderStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">우편번호별 주문현황</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="helpBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>우편번호별 주문현황</caption>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-5">
                                    <option value="ORDER">주문기간</option>
                                    <option value="SHIP">배송기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="조회시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="조회종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <td width="100px" rowspan="2">
                            <div>
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large mg-t-5">초기화</button>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포</th>
                        <td class="ui form inline">
                        <c:choose>
                        <c:when test="${empty userStoreId}">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                        </c:when>
                        <c:otherwise>
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" readonly>
                        </c:otherwise>
                        </c:choose>
                            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'home'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly value="${userStoreId}">
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly value="${userStoreNm}">
                            <c:if test="${empty userStoreId}"><button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button></c:if>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="zipcodeOrderStatisticsSearchCount">0</span>건</h3>
        </div>
        <div id="zipcodeOrderStatisticsGrid" style="height: 350px;"></div>
    </div>
</div>

<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>

<script>
    ${zipcodeOrderStatisticsBaseInfo}

    $(document).ready(function() {
        zipcodeOrderStatisticsMain.init();
        zipcodeOrderStatisticsGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/zipcodeOrderStatisticsMain.js?v=${fileVersion}"></script>
