<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [카테고리별 매출(E-KPI)] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [카테고리별 매출(E-KPI)] 집계 기준</caption>
            <colgroup>
                <col width="160px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        <b>TD 매출금액(E-KPI)</b>
                    </td>
                    <td>
                        - 결제 마감 기준<br/>
                        - 매출금액 = 상품금액 – 상품할인(업체/자사) – 카드할인(카드사/자사) – 임직원할인 – 행사할인
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        <b>DC 매출금액(E-KPI)</b>
                    </td>
                    <td>
                        - 구매 확정 기준<br/>
                        - 매출금액 = 상품금액 – 상품할인(업체/자사) – 카드할인(카드사/자사) – 임직원할인 – 행사할인
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        <b>DS 매출금액(E-KPI)</b>
                    </td>
                    <td>
                        - 구매 확정 기준<br/>
                        - 매출금액 = 상품금액
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        <b>전체 매출금액(E-KPI)</b>
                    </td>
                    <td>
                        PC 매출금액(E-KPI) + MOBILE 매출금액(E-KPI)
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    * 클레임(취소/반품) 마이너스 처리 / 합배송 금액 합산 / 대체주문 금액 합산

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>