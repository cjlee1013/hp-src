<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">기간별 ${getName} 판매 현황</h2>
        <span class="text">&nbsp;&nbsp;</span>
        <button type="button" id="descriptionBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>기간별 ${getName} 판매 현황</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td<c:if test="${getType.equals(\"Store\")}"> colspan="3"</c:if>>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="DailyOrderInfoForm.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                    </select>
                                    <span class="text">~</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="DailyOrderInfoForm.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                    </select>
                                </span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-t-15">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운로드</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>${getName}</th>
                        <td class="ui form inline">
                            <c:choose>
                            <c:when test="${empty storeType}">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 90px;" readonly>
                                    <option value="DS">DS</option>
                                </select>
                            </c:when>
                            <c:otherwise>
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 90px;">
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            </c:otherwise>
                            </c:choose>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 140px;" readonly value="${getStoreId}">
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" readonly value="${getStoreNm}">
                            <c:if test="${empty getStoreId}"><button id="schStoreBtn" type="button" class="ui button medium" onclick="DailyOrderInfoForm.openStorePopup();">조회</button></c:if>
                        </td>
                        <c:if test="${getType.equals(\"Store\")}">
                        <th>마켓</th>
                        <td>
                            <select class="ui input inline medium mg-r-10" id="schMarketType" name="schMarketType" style="width: 100px;">
                                <option value="">전체</option>
                                <option value="R">제휴제외</option>
                                <option value="NAVER">N마트</option>
                                <option value="ELEVEN">11번가</option>
                            </select>
                        </td>
                        </c:if>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">검색결과</h3>
            </div>
        </div>
        <div class="mg-b-20 " id="DailyOrderInfoGrid" style="height: 400px;"></div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/dailyOrderInfo.js?v=${fileVersion}"></script>
<script>
    ${DailyOrderInfoBaseInfo}

    var getName = "${getName}";
    var getType = "${getType}";
    var getKind = "${getKind}";
    $(document).ready(function() {
          DailyOrderInfoForm.init();
          DailyOrderInfoGrid.init();
    });

    //도움말 팝업
    $("#descriptionBtn").on('click', function(){
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _width  = 780;
        if (getName == "점포") {
            var _url = '/statistics/popup/dailyStorePaymentPop';
            var _height = 490;
        } else {
            var _url = '/statistics/popup/dailyPartnerPaymentPop';
            var _height = 390;
        }
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);

        window.open(_url, '_blank',
            'toolbar=no, location=no, menubar=no, scrollbars=no, resizable=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top);
    });
</script>