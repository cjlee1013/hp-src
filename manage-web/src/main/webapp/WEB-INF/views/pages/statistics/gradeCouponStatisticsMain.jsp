<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">등급별 쿠폰 통계</h2>
        <span class="text">&nbsp;&nbsp;</span>
        <button type="button" id="descriptionBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
        <span class="text">* 장바구니, 배송비 쿠폰만 집계합니다.</span>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>등급별 쿠폰 통계</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="40%">
                        <col width="8%">
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th >조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="gradeCouponStatistics.initSearchDate('-1w');">1주일</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="gradeCouponStatistics.initSearchDate('-1m');">1개월</button>

                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="gradeCouponStatisticsListTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="gradeCouponStatisticsListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/gradeCouponStatisticsMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>


<script>
    ${gradeCouponStatisticsListGridBaseInfo}
</script>