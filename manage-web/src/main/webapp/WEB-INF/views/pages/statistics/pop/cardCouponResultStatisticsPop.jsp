<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [카드사별쿠폰실적] 집계 기준</h2>
    </div>

    <div class="com wrap-title" style="margin-top: 5px; margin-bottom: 5px">
        <span class="text">* 장바구니 쿠폰만 집계합니다.</span>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [카드사별쿠폰실적] 집계 기준</caption>
            <colgroup>
                <col width="160px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>내용</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        카드결제금액
                    </td>
                    <td>
                        - 승인금액<br/>
                        - 전체 반품완료/주문취소 포함(주문한 것만 추출_PG사에서 카드사 접수되는 건)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        원매출금액
                    </td>
                    <td>
                        - 전체 반품완료/주문취소 포함(주문한 것만 추출_PG사에서 카드사 접수되는 건)<br/>
                        - 결제수단 적용 전 금액으로 할인정보(임직원할인, 행사/할인, 상품/배송비쿠폰, 장바구니쿠폰)<br/>
                        및 포인트(마이 홈플러스 포인트, 마일리지, 모바일 상품권, OK캐시백), 상품권 모두 포함된 금액<br/>
                        - 배송비 제외
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        장바구니쿠폰금액
                    </td>
                    <td>
                        - 쿠폰사용금액
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>
<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>