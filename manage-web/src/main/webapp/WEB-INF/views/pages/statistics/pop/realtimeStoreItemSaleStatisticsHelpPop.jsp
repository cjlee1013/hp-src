<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [실시간 점포 상품 판매 현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [실시간 점포 상품 판매 현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    항목
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    판매수량
                </td>
                <td>
                    결제완료된 주문 중 동일 상품번호의 건수 총합<br>
                    ㄴ 합배송, 대체주문은 건수 포함안함 / 마켓연동주문 포함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소수량
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 동일 상품번호 건수 총합<br>
                    ㄴ 부분취소/마켓연동주문 포함 (반품/교환 클레임 건수 포함안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    판매금액
                </td>
                <td>
                    결제완료된 주문 중 동일 상품번호의 결제금액 총합<br>
                    ㄴ 합배송 / 대체주문은 금액 포함안함 / 마켓연동주문 포함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소금액
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 동일 상품번호 금액 총합<br>
                    ㄴ 부분취소/마켓연동주문 포함 (반품/교환 클레임 금액 포함안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    순판매수량
                </td>
                <td>
                    판매수량 – 취소수량
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    순판매금액
                </td>
                <td>
                    판매금액 – 취소금액
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>