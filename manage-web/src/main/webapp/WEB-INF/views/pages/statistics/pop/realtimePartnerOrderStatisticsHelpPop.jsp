<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [실시간 판매업체별 판매 현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [실시간 판매업체별 판매 현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    구분
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문건수
                </td>
                <td>
                    결제완료된 주문번호 건수 총합
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문금액
                </td>
                <td>
                    상품금액(상품금액 x 수량) - 상품할인액(행사할인,상품쿠폰,임직원할인,카드즉시할인)<br>
                    ㄴ 결제금액 아님, 배송비 제외, 장바구니 / 마일리지 할인 반영 안함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소건수
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합<br>
                    ㄴ 부분/전체 취소 합산 건수
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소금액
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합<br>
                    ㄴ 부분/전체 취소 합산 금액
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    반품건수
                </td>
                <td>
                    반품 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합<br>
                    ㄴ 부분/전체 반품 합산 건수
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    반품금액
                </td>
                <td>
                    반품 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합<br>
                    ㄴ 부분/전체 반품 합산 금액
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
