<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div  class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="cartCouponStatisticsSearchForm" name="cartCouponStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">장바구니쿠폰통계</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>장바구니쿠폰통계 검색</caption>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                    <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="cartCouponStatisticsMain.setSearchDate('W');">이번주</button>
                                    <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="cartCouponStatisticsMain.setSearchDate('2W');">2주</button>
                                </div>
                            </td>
                            <th>점포유형</th>
                            <td>
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" >
                                    <option value="HYPER">Hyper</option>
                                    <option value="EXP">Express</option>
                                </select>
                            </td>
                            <td rowspan="2">
                                <div style="text-align: center;" class="ui form">
                                    <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                    <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>쿠폰유형</th>
                            <td colspan="3">
                                <label id="cartLabel" class="ui radio small inline"><input type="radio" name="schCouponKind" id="schCouponKindCart" value="CART" checked><span>장바구니쿠폰</span></label>
                                <label id="addTdLabel" class="ui radio small inline"><input type="radio" name="schCouponKind" id="schCouponKindAddTd" value="ADDTD"><span>중복쿠폰</span></label>
                                <label id="addDsLabel" class="ui radio small inline"><input type="radio" name="schCouponKind" id="schCouponKindAddDs" value="ADDDS"><span>DS 중복쿠폰</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div>
        <div class="com wrap-title sub" style="width: 50%; float: left;">
            <h3 class="title">&nbsp;</h3>
        </div>
        <div style="width: 50%; float: right;">
            <button type="button" id="storeExcelDownBtn" class="ui button large white mg-t-5" style="float: right;">엑셀다운</button><br/>
        </div>
        <div id="cartCouponStatisticsGrid" style="height: 250px;"></div>
    </div>

    <p>&nbsp;</p>

    <div style="margin-top: 30px; width: 100%;">
        <div class="com wrap-title sub" style="width: 50%; float: left;">
            <h3 class="title">&nbsp;</h3>
        </div>
        <div style="width: 50%; float: right;">
            <button type="button" id="ordDtExcelDownBtn" class="ui button large white mg-t-5" style="float: right;">엑셀다운</button><br/>
        </div>
        <div id="cartCouponStatisticsOrdDtGrid" style="height: 400px;"></div>
    </div>

</div>

<script>
    ${cartCouponStatisticsBaseInfo}
    ${cartCouponStatisticsOrdDtBaseInfo}
    $(document).ready(function () {
        cartCouponStatisticsMain.init();
        cartCouponStatisticsGrid.init();
        cartCouponStatisticsOrdDtGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/cartCouponStatisticsMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>