<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">기간별 상품 판매현황</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>기기간별 상품 판매현황</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="600px">
                        <col width="*">
                        <col width="80px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색 기간</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-10">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>마켓 조회</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="siteType" name="siteType" style="width:150px;float:left;">
                                    <option value="">전체</option>
                                    <option value="HOME">제휴제외</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">검색 결과</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
            </div>
            <div class="mg-b-20 " id="dailySettleMarketItemGrid" style="height: 700px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/dailySettleMarketItem.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${dailySettleMarketItemBaseInfo}

    $(document).ready(function() {
        dailySettleMarketItemForm.init();
        dailySettleMarketItemForm.bindingEvent();
        dailySettleMarketItemGrid.init();
    });
</script>