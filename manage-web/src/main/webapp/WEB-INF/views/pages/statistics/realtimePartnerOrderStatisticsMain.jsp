<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="realtimePartnerOrderStatisticsSearchForm" name="realtimePartnerOrderStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">실시간 판매업체별 판매 현황</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="helpBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>실시간 판매업체별 판매 현황</caption>
                    <tbody>
                    <tr>
                        <th>주문일</th>
                        <td>
                            <div class="ui form inline" colspan="3">
                                <input type="text" id="schBasicDt" name="schBasicDt" class="ui input medium mg-r-5" placeholder="주문일" readonly>
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>판매업체</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" readonly>
                                <option value="DS">DS</option>
                            </select>
                            <input type="text" id="schPartnerId" name="schPartnerId" class="ui input mg-r-5" placeholder="판매업체ID" readonly>
                            <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input mg-r-5" placeholder="판매업체명" readonly>
                            <button type="button" class="ui button medium" onclick="realtimePartnerOrderStatisticsMng.openPartnerPop('realtimePartnerOrderStatisticsMng.openPartnerPopCallback');" >조회</button>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="realtimePartnerSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="realtimePartnerOrderStatisticsGrid" style="width: 100%; height: 330px;"></div>
    </div>
</div>

<script>
    ${realtimePartnerOrderStatisticsGridBaseInfo}
    $(document).ready(function() {
        realtimePartnerOrderStatisticsMng.init();
        realtimePartnerOrderStatisticsGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/realtimePartnerOrderStatisticsMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
