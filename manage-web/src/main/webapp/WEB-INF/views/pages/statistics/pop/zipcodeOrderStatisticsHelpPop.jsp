<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [우편번호별 주문현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [우편번호별 주문현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    항목
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문건수
                </td>
                <td>
                    결제완료된 주문번호 건수 총합 (합배송 / 대체주문은 주문건수에 포함 안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문금액
                </td>
                <td>
                    상품금액(상품금액 x 수량) - 상품할인(즉시할인,상품쿠폰)-행사할인-임직원할인<br>
                    ㄴ 결제금액 아님, 배송비 제외, 장바구니 / 마일리지 할인 반영 안함<br>
                    ㄴ 합배송 주문금액 포함 / 대체주문은 주문금액에 포함 안함
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>