<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">주문당 평균 주문 상품수</h2>
        <span class="text">&nbsp;&nbsp;</span>
        <button type="button" id="descriptionBtn" class="ui button medium white mg-t-5" style="margin-top:-4px">도움말</button>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>주문당 평균 주문 상품수</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="30%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th >주문일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="averageOrderItemQty.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="text">~</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="averageOrderItemQty.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>점포유형</th>
                        <td>
                            <div class="ui form inline">
                                <select style="width: 150px" name="schStoreType" id="schStoreType" class="ui input medium mg-r-10" >
                                    <option value="HYPER">HYPER</option>
                                    <option value="EXP">EXPRESS</option>
                                </select>
                            </div>
                        </td>

                        <th>상품구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schMallType" name="schMallType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ALL">전체</option>
                                    <option value="TD">TD</option>
                                    <option value="DS">DS</option>
                                    <option value="DC">DC</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="averageItemQtyListTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="averageItemQtyListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/averageOrderItemQtyMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>


<script>
    ${averageItemQtyListGridBaseInfo}

    //도움말 팝업
    $("#descriptionBtn").on('click', function(){
        var _url = '/statistics/popup/averageOrderItemQtyPop';
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _width  = 780;
        var _height = 370;
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);

        window.open(_url, '_blank',
            'toolbar=no, location=no, menubar=no, scrollbars=no, resizable=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top);
    });
</script>