<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="realtimePartnerItemSaleStatisticsSearchForm" name="realtimePartnerItemSaleStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">실시간 판매업체 상품 판매 현황</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="helpBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>실시간 판매업체 상품 판매 현황</caption>
                    <tbody>
                    <tr>
                        <th>주문일</th>
                        <td colspan="3">
                            <div class="ui form inline" colspan="3">
                                <input type="text" id="schBasicDt" name="schBasicDt" class="ui input medium mg-r-5" placeholder="주문일" readonly>
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                            <input type="hidden" id="schTodayYn" name="schTodayYn" value="">
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>판매업체</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" readonly>
                                <option value="DS">DS</option>
                            </select>
                            <input type="text" id="schPartnerId" name="schPartnerId" class="ui input mg-r-5" placeholder="판매업체ID" readonly>
                            <input type="text" id="schPartnerNm" name="schPartnerNm" class="ui input mg-r-5" placeholder="판매업체명" readonly>
                            <button type="button" class="ui button medium" onclick="realtimePartnerItemSaleStatisticsMng.openPartnerPop('realtimePartnerItemSaleStatisticsMng.openPartnerPopCallback');" >조회</button>
                        </td>
                        <th>상품번호</th>
                        <td>
                            <textarea id="schItemNoList" name="schItemNoList" class="ui input medium mg-r-5" style="width: 70%; height: 60px;" minlength="2" placeholder="상품번호 복수 검색 시 쉼표(,)로 구분 &#13;&#10;(최대 50개 까지 가능)" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>온라인 카테고리</th>
                        <td class="ui form inline" colspan="3">
                            <select id="schCateCd1" name="schLcateCd" class="ui input medium mg-r-5" style="float:left;" data-default="대분류" onchange="commonCategory.changeCategorySelectBox(1,'schCateCd');"></select>
                            <select id="schCateCd2" name="schMcateCd" class="ui input medium mg-r-5" style="float:left;" data-default="중분류" onchange="commonCategory.changeCategorySelectBox(2,'schCateCd');"></select>
                            <select id="schCateCd3" name="schScateCd" class="ui input medium mg-r-5" style="float:left;" data-default="소분류" onchange="commonCategory.changeCategorySelectBox(3,'schCateCd');"></select>
                            <select id="schCateCd4" name="schDcateCd" class="ui input medium mg-r-5" data-default="세분류"></select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과</h3><span class="text">&nbsp;&nbsp;* 전체 검색시 TOP 5,000순으로 노출 (순판매금액 기준)</span>
        </div>
        <div class="topline"></div>
        <div id="realtimePartnerItemSaleStatisticsGrid" style="width: 100%; height: 330px;"></div>
    </div>
</div>

<script>
    ${realtimePartnerItemSaleStatisticsGridBaseInfo}
    var taxYnJson = ${taxYnJson};
    var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
    $(document).ready(function() {
        realtimePartnerItemSaleStatisticsMng.init();
        realtimePartnerItemSaleStatisticsGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/realtimePartnerItemSaleStatisticsMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
