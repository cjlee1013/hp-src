<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [실시간 판매업체 상품 판매 현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [실시간 판매업체 상품 판매 현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    구분
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    판매수량
                </td>
                <td>
                    결제완료된 주문 중 동일 상품번호의 건수 총합
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소수량
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 동일 상품번호 건수 총합<br>
                    ㄴ 부분취소 포함 (반품/교환 클레임 건수 포함안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    판매금액
                </td>
                <td>
                    결제완료된 주문 중 동일 상품번호의 결제금액 총합
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소금액
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 동일 상품번호 금액 총합<br>
                    ㄴ 부분취소 포함 (반품/교환 클레임 금액 포함안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    순판매수량
                </td>
                <td>
                    판매수량 – 취소수량
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    순판매금액
                </td>
                <td>
                    판매금액 – 취소금액
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    매출(In VAT)
                </td>
                <td>
                    상품금액*수량 - 상품금액*클레임수량
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    매출(Ex VAT)
                </td>
                <td>
                    과세에 한하여 =(상품금액*수량 - 상품금액*클레임수량)/1.1 (소수점 1자리 반올림)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    정산(In VAT)
                </td>
                <td>
                    DS에 과세에 한하여 =매출-판매수수료, 이외 0
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    정산(Ex VAT)
                </td>
                <td>
                    DS에 과세에 한하여 =정산(Iv VAT) / 1.1 (소수점 1자리 반올림)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    판매수수료
                </td>
                <td>
                    판매수수료 조건에 따라 계산식 상이함<br>
                    - 정액 : 수량 * 판매수수료율<br>
                    - 정률 : 상품금액 * 판매수수료율 / 100 (*소수점 1자리 버림)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    총 할인금액
                </td>
                <td>
                    상품할인+카드할인+임직원할인+행사할인+장바구니할인
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    결제금액합계
                </td>
                <td>
                    순 결제금액<br>
                    = PG결제금액 + DGV + OCB + MHC + 마일리지
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>