<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [등급별 쿠폰 통계] 집계 기준</h2>
    </div>

    <div class="com wrap-title" style="margin-top: 5px; margin-bottom: 5px">
        <span class="text">* 장바구니, 배송비 쿠폰만 집계합니다.</span>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [등급별 쿠폰 통계] 집계 기준</caption>
            <colgroup>
                <col width="160px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        주문고객수
                    </td>
                    <td>
                        * 산정된 고객중 주문 고객 수 (전체반품완료/주문취소 제외)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        주문건수
                    </td>
                    <td>
                        합배송 주문건수 주문제외 / 픽업주문건수 포함/대체주문건 제외<br/>
                        주문번호 기준으로 집계<br/>
                        ㄴ DS의 주문건수는 결제 번호 기준으로 셀러가 다수인 경우 1건으로 함<br/>
                        ㄴ 1개의 결제번호에 Hyper 1건, DS 2건 인경우 집계시 Hyper 1건, DS 1건으로 집계<br/>
                        ㄴ DS 클레임 건수는 결제 번호 기준으로 셀러가 모두 취소 또는 반품 한 경우에만 마이너스 처리<br/>
                        ㄴ 원배송 + 합배송인 경우 1건으로 간주하며, 금액에 포함됨<br/>
                        ㄴ 대체 주문건수는 주문건수에 제외<br/>
                        ㄴ 0원 결제 주문건수 포함. 렌탈 상품은 0원 상품
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        매출
                    </td>
                    <td>
                        배송비제외 / 반품, 주문취소 제외 / 마일리지 사용 건 제외 / 장바구니 쿠폰 COST 포함 / MHC 포인트, 카드사 포인트 포함 / 합배송,픽업,대체주문 매출포함
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        주문횟수
                    </td>
                    <td>
                        =주문건수/주문고객수
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        객단가
                    </td>
                    <td>
                        =매출/주문횟수
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        장바구니쿠폰사용금액
                    </td>
                    <td>
                        해당 월의 장바구니쿠폰 사용금액=쿠폰 사용 cost
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>
<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>