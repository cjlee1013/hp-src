<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <c:if test="${mallType eq 'TD'}">
            <h2 class="title font-malgun">기간별 점포 판매 현황(E-KPI)</h2>
            <span class="text">&nbsp;&nbsp;</span>
            <button type="button" id="descriptionBtn" class="ui button medium white mg-t-5" style="margin-top:-4px" onclick="windowOpen('/statistics/popup/storePeriodEKPIPop', 520)">도움말</button>
        </c:if>
        <c:if test="${mallType eq 'DS'}">
            <h2 class="title font-malgun">기간별 판매업체 현황(E-KPI)</h2>
            <span class="text">&nbsp;&nbsp;</span>
            <button type="button" id="descriptionBtn" class="ui button medium white mg-t-5" style="margin-top:-4px" onclick="windowOpen('/statistics/popup/DSPeriodEKPIPop', 430)">도움말</button>
        </c:if>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>기간별<c:if test="${mallType eq 'TD'}"> 점포</c:if>
                        <c:if test="${mallType eq 'DS'}">판매업체</c:if> 판매 현황(E-KPI)</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="550px">
                        <col width="120px">
                        <col width="*">
                        <col width="80px">
                    </colgroup>
                    <tbody>
                    <c:if test="${mallType eq 'TD'}">
                    <tr>
                        <th>구분</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="searchType" value="D" checked><span>전점합계</span>
                                </label>
                                <label class="ui radio inline">
                                    <input type="radio" name="searchType" value="A"><span>점포별</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    </c:if>
                    <tr style="height:56px;">
                        <th>검색 기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="text">~&nbsp;&nbsp;</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: right;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-10">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <c:if test="${mallType eq 'TD'}">
                    <tr>
                        <th>점포 유형</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:120px;float:left;">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.ref3 eq 'REAL'}">
                                            <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                                <input type="text" id="originStoreId" name="originStoreId" class="ui input mg-r-5" style="width: 120px;" readonly value="${getStoreId}" placeholder="점포ID">
                                <input type="text" id="storeNm" name="storeNm" class="ui input mg-r-5" style="width: 120px;" readonly value="${getStoreNm}" placeholder="점포명">
                                <button id="schStoreBtn" type="button" class="ui button medium" onclick="storePeriodListForm.openStorePopup();">조회</button>
                            </div>
                        </td>
                        <th>마켓</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="marketType" name="marketType" style="width:120px;float:left;">
                                    <option value="">전체</option>
                                    <option value="HOME">제휴제외</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </c:if>
                    </tbody>
                </table>
                <input type="hidden" id="mallType" name="mallType" value="${mallType}">
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">검색 결과</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
            </div>
            <div class="mg-b-20 " id="storePeriodListGrid" style="height: 600px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/storePeriodEKPIMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${storePeriodListBaseInfo}

    $(document).ready(function() {
        storePeriodListForm.init();
        storePeriodListForm.bindingEvent();
        storePeriodListGrid.init();
    });

    //도움말 팝업
    function windowOpen(_url, _height){
        // 팝업을 가운데 위치시키기 위해 아래와 같이 값 구하기
        var _width  = 780;
        var _left = ($(window).width()/2)-(_width/2);
        var _top = ($(window).height()/2)-(_height/2);

        window.open(_url, '_blank',
            'toolbar=no, location=no, menubar=no, scrollbars=no, resizable=no, width='+ _width +', height='+ _height +', left=' + _left + ', top='+ _top);
    }
</script>