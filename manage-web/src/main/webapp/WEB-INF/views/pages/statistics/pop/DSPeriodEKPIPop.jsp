<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [기간별 판매업체 판매 현황(E-KPI)] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [기간별 판매업체 판매 현황(E-KPI)] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        전체주문
                    </td>
                    <td>
                        * 주문건수 : 전체 클레임 건수가 마이너스 처리가 되지 않은 주문 데이터<br/>
                        * 매출금액(E-KPI) : 당일 전체취소 신청금액/부분취소 신청금액/반품 신청금액이 반영되지 않은 금액<br/><br/>
                        DS = 상품금액 (할인금액 마이너스 처리없이 상품매출 금액만 집계)<br/>
                        * 상품개수 : 판매수량(unit)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        유효주문
                    </td>
                    <td>
                        * 유효주문건수 = 전체주문 – 전체 클레임 건수<br/>
                        * 매출금액(E-KPI) = 전체매출금액(E-KPI) – 당일 전체취소 신청금액 – 부분취소 신청금액  – 부분반품 신청금액<br/>
                        * 상품개수 : 판매수량(unit)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        당일 전체취소
                    </td>
                    <td>
                        * 당일 기준 전체취소된 주문 데이터
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        부분취소
                    </td>
                    <td>
                        * 취소 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수/금액/개수 총합 (대체주문 취소건수 포함 안함)
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        부분반품
                    </td>
                    <td>
                        * 반품 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수/금액/개수 총합
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>