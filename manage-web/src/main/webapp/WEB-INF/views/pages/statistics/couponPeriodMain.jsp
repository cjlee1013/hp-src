<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="couponPeriodSearchForm" name="couponPeriodSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">기간별 쿠폰통계</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>기간별 쿠폰통계</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="40%">
                        <col width="8%">
                        <col width="35%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">이번주</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                        </td>
                        <th>점포유형</th>
                        <td>
                            <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10" style="width: 100px">
                                <option value="Hyper">Hyper</option>
                                <option value="Express">Express</option>
                                <option value="DS">DS</option>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>쿠폰유형</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schDiscountKind" name="schDiscountKind" class="ui input medium mg-r-10" style="width: 100px">
                                    </select>
                                    <select id="schMarketType" name="schMarketType" class="ui input medium mg-r-10" style="width: 100px; display: none;">
                                        <option value="">전체</option>
                                        <option value="HOME">HOME</option>
                                        <option value="NAVER">N마트</option>
                                        <option value="ELEVEN">11번가</option>
                                    </select>
                                </div>
                            </td>
                            <th>검색어</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                        <option value="couponNm">쿠폰명</option>
                                        <option value="couponNo">쿠폰번호</option>
                                    </select>
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 150px">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div class="com wrap-title sub" style="margin-top: 30px;">
        <div style="float:right;">
            <button type="button" class="ui button medium dark-blue" id="excelDownloadMainBtn">엑셀 다운</button>
        </div>
    </div>

    <div class="topline"></div>
    <div id="couponPeriodGrid" style="width: 100%; height: 300px;"></div>

    <div class="com wrap-title sub" style="margin-top: 30px;">
        <div style="float:right;">
            <button type="button" class="ui button medium dark-blue" id="excelDownloadDetailBtn">엑셀 다운</button>
        </div>
    </div>

    <div class="topline"></div>
    <div class="mg-b-20 " id="couponPeriodDetailGrid" style="height: 400px;"></div>
</div>

<script>
    ${couponPeriodGridBaseInfo}
    ${couponPeriodDetailGridBaseInfo}

    $(document).ready(function() {
        couponPeriodMain.init();
        couponPeriodGrid.init();
        couponPeriodDetailGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/couponPeriodMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>