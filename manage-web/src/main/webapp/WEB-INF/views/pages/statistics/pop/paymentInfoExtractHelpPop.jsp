<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [지불 수단별 판매현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [지불 수단별 판매현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    구분
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    결제건수
                </td>
                <td>
                    - 결제수단으로 결제된 건수(취소 건수 불포함)<br>
                    - 신용카드+간편결제+100%+0원결제 건수의 합과 동일<br>
                    - 100% 포인트 결제 건수는 하위 MHC/마일리지/DGV/OCB 건수의 합과 상이 할 수 있음<br>
                    - 보조결제수단의 건수는 하위 MHC/마일리지/DGV/OCB 건수의 합과 동일함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    결제금액
                </td>
                <td>
                    결제수단으로 결제된 금액(취소 금액 불포함)<br>
                    - 신용카드+간편결제+100% 금액의 합산금액과 동일<br>
                    - 100% 포인트 결제금액은 하위 MHC/마일리지/DGV/OCB 금액의 합과 동일함<br>
                    - 보조결제수단의 결제금액은 하위 MHC/마일리지/DGV/OCB 금액의 합과 동일함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    객단가
                </td>
                <td>
                    결제금액/결제건수 = 객단가 (소수점 절사)
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>