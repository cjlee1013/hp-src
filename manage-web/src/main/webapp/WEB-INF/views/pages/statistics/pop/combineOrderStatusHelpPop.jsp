<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .ui.table td {
        background-color: white;
    }
</style>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">︎■ [합배송 주문 현황] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>︎■ [합배송 주문 현황] 집계 기준</caption>
            <colgroup>
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th style="border-right: 1px solid #eee; text-align: center; background-color: #f9f9f9;">
                    구분
                </th>
                <th style="text-align: center; background-color: #f9f9f9;">
                    설명
                </th>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문건수
                </td>
                <td>
                    결제완료된 주문번호 건수 총합 (대체주문은 주문건수에 포함 안함)
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    주문금액
                </td>
                <td>
                    상품금액(상품금액 x 수량) – 상품할인-행사할인-임직원할인<br>
                    ㄴ 결제금액 아님, 배송비 제외, 장바구니 / 마일리지 할인 반영 안함<br>
                    ㄴ 대체주문은 주문금액에 포함 안함
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소건수
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합 (대체주문 취소시 취소건수에 포함 안함)<br>
                    ㄴ 부분/전체 취소 합산 건수<br>
                    클레임의 경우 주문일에 카운트 되는것이 아니라 클레임이 발생한날에 카운트되고 주문일보다 검색 기간이 길기 때문에 클레임 건수/금액이 주문 건수/금액보다 커질수 있음
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    취소금액
                </td>
                <td>
                    취소 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합 (대체주문 취소시 취소금액에 포함 안함)<br>
                    ㄴ 부분/전체 취소 합산 금액
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    반품건수
                </td>
                <td>
                    반품 클레임이고 처리상태가 ‘완료’인 클레임 번호 건수 총합<br>
                    ㄴ 부분/전체 반품 합산 건수
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    반품금액
                </td>
                <td>
                    반품 클레임이고 처리상태가 ‘완료’인 클레임 번호의 금액 총합<br>
                    ㄴ 부분/전체 반품 합산 금액
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    대체주문건수
                </td>
                <td>
                    대체 주문번호 건수 총합
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #eee; text-align: center;">
                    대체주문금액
                </td>
                <td>
                    대체된 상품 금액 총합
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>