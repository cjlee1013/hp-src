<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="exhibitionSaleStatisticsSearchForm" name="exhibitionSaleStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">기획전별 판매현황</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="helpBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>기획전별 판매현황</caption>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schApplyStartDt" name="schApplyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schApplyEndDt" name="schApplyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            </div>
                        </td>
                        <th>기획전명 조회</th>
                        <td>
                            <input type="text" id="schExhibitionNm" name="schExhibitionNm" class="ui input mg-r-5">
                        </td>
                        <td width="100px">
                            <div>
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <!-- 전시 조회 결과 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="exhibitionSaleStatisticsExhSearchCount">0</span>건</h3>
            <span class="ui pull-right mg-b-5">
                <button type="button" id="ExhibitionExcelDownBtn" class="ui button small" onclick="">엑셀다운</button>
            </span>
        </div>
        <div id="exhibitionSaleStatisticsExhGrid" style="height: 350px;"></div>
        <input type="hidden" id="exhSearchStartDt" value=""/>
        <input type="hidden" id="exhSearchEndDt" value=""/>
    </div>

    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">상세내역 : <span id="exhibitionSaleStatisticsItemSearchCount">0</span>건</h3>
            <span class="ui pull-right mg-b-5">
                <button type="button" id="ExhibitionItemExcelDownBtn" class="ui button small" onclick="">엑셀다운</button>
            </span>
        </div>
        <div id="exhibitionSaleStatisticsItemGrid" style="height: 350px;"></div>
    </div>

    <!-- 전시 > 상품 조회 결과 -->
</div>

<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>

<script>
    ${exhibitionSaleStatisticsExhBaseInfo}
    ${exhibitionSaleStatisticsItemBaseInfo}

    $(document).ready(function() {
        exhibitionSaleStatisticsMain.init();
        exhibitionGrid.init();
        exhibitionItemGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/exhibitionSaleStatisticsMain.js?v=${fileVersion}"></script>
