<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="combineOrderStatusSearchForm" name="combineOrderStatusSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">합배송 주문 현황</h2>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" id="helpBtn" style="margin-top:-4px" class="ui button medium white mg-t-5">도움말</button>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>합배송 주문 현황</caption>
                    <tbody>
                    <tr>
                        <th>주문일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schOrderStartDt" name="schOrderStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schOrderEndDt" name="schOrderEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            </div>
                        </td>
                        <th>배송요청일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schShipReqStartDt" name="schShipReqStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schShipReqEndDt" name="schShipReqEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                            </div>
                        </td>
                        <td width="100px" rowspan="2">
                            <div>
                                <button type="button" id="schBtn" class="ui button large cyan mg-r-5">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large mg-t-5">초기화</button>
                                <button type="button" id="excelDownBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <c:choose>
                                <c:when test="${empty userStoreId}">
                                    <select style="width: 200px" id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                                        <option value="HYPER">HYPER</option>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <select style="width: 200px" id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" readonly>
                                        <option value="${userStoreType}">${userStoreType}</option>
                                    </select>
                                </c:otherwise>
                                </c:choose>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly value="${userStoreId}">
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly value="${userStoreNm}">
                                <c:if test="${empty userStoreId}"><button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button></c:if>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과</h3>
        </div>
        <div id="combineOrderStatusGridView" style="height: 500px;"></div>
    </div>
</div>

<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
<script>
    ${combineOrderStatusBaseInfo}

    $(document).ready(function() {
        combineOrderStatusMain.init();
        combineOrderStatusGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/combineOrderStatusMain.js?v=${fileVersion}"></script>
