<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<style>
    .ui.table.calendar-mode td {
        background-color: white;
    }
</style>
<div id="divPop" class="com wrap-popup large">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">■ [주문당 평균 주문 상품수] 집계 기준</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table calendar-mode">
            <caption>■ [주문당 평균 주문 상품수] 집계 기준</caption>
            <colgroup>
                <col width="160px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th style="border-right: 1px solid #333;">구분</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        평균 주문 상품수
                    </td>
                    <td>
                        * 주문 상품수 : 결제 완료 된 주문번호 내 상품 개수<br/>
                        ㄴ a상품 1개 + b상품 2개 = 주문 상품수 (2개)<br/>
                        ㄴ 합배송, 대체주문 건수 포함안함<br/>
                        ㄴ 주문취소, 부분취소 포함안함<br/><br/>
                        * 평균 주문 상품수 : 주문상품수 합산의 평균 값
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid #333;">
                        평균 주문 상품 ITEM수
                    </td>
                    <td>
                        * 주문 상품 ITEM수 : 결제 완료 된 주문번호 내 상품 수량<br/>
                        ㄴ a상품 1개 + b상품 2개 = 주문 상품 ITEM수(3개)<br/>
                        ㄴ 합배송, 대체주문은 건수 포함안함<br/>
                        ㄴ 주문취소, 부분취소 포함안함<br/><br/>
                        * 평균 주문 상품 ITEM수 : 주문상품 ITEM수 합산의 평균 값
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button large font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>
<script>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>