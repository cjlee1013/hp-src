<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="counselStatisticsSearchForm" name="counselStatisticsSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">상담통계</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>상담통계</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="40%">
                        <col width="8%">
                        <col width="35%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td class="ui form inline">
                                <input type="text" id="schStartDt" name="SCH_S_YMD" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="SCH_E_YMD" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">이번주</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                        </td>
                        <th>점포유형</th>
                        <td colspan="2" class="ui form inline">
                            <select id="hChannel" name="H_CHANNEL" class="ui input medium mg-r-10" style="width: 110px;">
                                <option value="H_CHANNEL_1">Hyper</option>
                                <option value="H_CHANNEL_4">Express</option>
                            </select>
                            <input type="text" id="schStoreId" name="STORE_ID" class="ui input mg-r-5" style="width: 140px; display: none;" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" readonly>
                            <button id="schStoreBtn" type="button" class="ui button medium">조회</button>
                        </td>
                        <td rowspan="3">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>상담구분</th>
                            <td>
                                <select id="advisorType1" name="ADVISOR_TYPE_1" style="width: 150px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_advisor_type_1}" varStatus="status">
                                        <option value="${codeDto.mcCd}" gmcCd="${codeDto.ref1}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>채널구분</th>
                            <td>
                                <select id="channelCd" name="CHANNEL_TYPE" class="ui input medium mg-r-10" style="width: 150px">
                                    <c:forEach var="codeDto" items="${cs_channel_cd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 최대 1개월까지 조회가 가능합니다.</h3>
        </div>
    </div>
    <!-- 그리드 영역 -->
    <div>
        <div class="topline"></div>
        <div id="counselStatisticsGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${counselStatisticsGridBaseInfo}

    $(document).ready(function() {
        counselStatisticsMain.init();
        counselStatisticsGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/counselStatisticsMain.js?v=${fileVersion}"></script>
