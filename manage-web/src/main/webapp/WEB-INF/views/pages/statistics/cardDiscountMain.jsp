<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="cardDiscountSearchForm" name="cardDiscountSearchForm" onsubmit="return false;">
            <input type="hidden" id="schDiscountNo" name="schDiscountNo">
            <input type="hidden" id="schDiscountNm" name="schDiscountNm">
            <input type="hidden" id="schDetailDiscountNo" name="schDetailDiscountNo">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">카드즉시할인통계</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>카드즉시할인통계</caption>
                    <tbody>
                        <tr>
                        <th>주문일</th>
                        <td class="ui form inline">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">이번주</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </div>
                        </td>
                        <th>점포유형</th>
                        <td>
                            <select id="schSiteTypeExtend" name="schSiteTypeExtend" class="ui input medium mg-r-10">
                                <option value="HOME">Hyper</option>
                                <option value="EXP">Express</option>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>검색어</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schKeywordKind" name="schKeywordKind" class="ui input medium mg-r-10">
                                        <option value="schDiscountNo">카드할인번호</option>
                                        <option value="schDiscountNm">카드할인명</option>
                                    </select>
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5">
                                </div>
                            </td>
                            <th>상품유형</th>
                            <td>
                                <select id="schMallType" name="schMallType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <option value="TD">TD</option>
                                    <option value="DS">DS</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
        </div>
        <div class="topline"></div>
        <div id="cardDiscountGrid" style="width: 100%; height: 300px;"></div>
    </div>

    <div class="com wrap-title sub" style="margin-top: 30px;">
<%--        <h3 class="title">검색결과 : 0건</h3>--%>
        <div style="float:right;">
            <button type="button" class="ui button medium dark-blue" id="excelDownloadBtn">엑셀 다운</button>
        </div>
    </div>

    <div class="topline"></div>
    <div class="mg-b-20 " id="cardDiscountDetailGrid" style="height: 400px;"></div>
</div>

<script>
    ${cardDiscountGridBaseInfo}
    ${cardDiscountDetailGridBaseInfo}

    $(document).ready(function() {
        cardDiscountMain.init();
        cardDiscountGrid.init();
        cardDiscountDetailGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/statistics/cardDiscountMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>