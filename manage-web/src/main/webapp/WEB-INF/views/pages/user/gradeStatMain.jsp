<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원등급통계</h2>
    </div>

    <div class="com wrap-title sub">

        <form id="searchForm" name="searchForm" onsubmit="return true;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원등급통계조회</caption>
                    <colgroup>
                        <col style="width:120px;"/>
                        <col style="width:30%;" />
                        <col style="width:120px;"/>
                        <col style="width:20%;"/>
                        <col style="width:30%;"/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">조회기간</th>
                        <td>
                            <select id="gradeApplyYm" style="width: 120px;display: inline;" name='gradeApplyYm' class="ui input medium mg-r-5">
                            <c:forEach var="gradeApplyYm" items="${gradeApplyYmList}" varStatus="i">
                                <c:choose>
                                    <c:when test="${gradeApplyYm eq param.gradeApplyYm}">
                                <option value="<c:out value="${gradeApplyYm}" />" selected><c:out value="${gradeApplyYm}" /></option>
                                    </c:when>
                                    <c:otherwise>
                                <option value="<c:out value="${gradeApplyYm}" />" <c:if test="${i.last and empty param.gradeApplyYm}">selected</c:if>><c:out value="${gradeApplyYm}" /></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            </select>
                        </td>
                        <th scope="col">조회구분</th>
                        <td><input type="checkbox" id="isBlackType" name="isBlackType" value="1" <c:if test="${param.isBlackType eq '1'}">checked</c:if>> 연속Black등급</td>
                        <%-- 검색 버튼 --%>
                        <td>
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 회원관리 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="gradeStatListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>

        <div id="gradeStatGrid" style="background-color:white;width:100%;height:530px;"></div>
        <!-- // 회원관리 검색 그리드 -->
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/gradeStatMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${gradeStatGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        gradeStatMain.init();
        gradeStatGrid.init();

        gradeStatGrid.dataProvider.setRows(${gradeStatChageListJson});
        $('#gradeStatListCount').html($.jUtil.comma(gradeStatGrid.gridView.getItemCount()));
    });
</script>
