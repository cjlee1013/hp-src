<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">컨슈머관리</h2>
    </div>

    <div class="com wrap-title sub">
        <%-- 검색 영역 --%>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>컨슈머관리</caption>
                <colgroup>
                    <col style="width:120px;"/>
                    <col style="width:60%;"/>
                    <col/>
                </colgroup>
                <tbody>
                <tr>
                    <th>회원검색</th>
                    <td class="ui form inline">
                        <input type="text" id="userNm" name="userNm" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                        <input type="hidden" id="searchUserNo" name="searchUserNo" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                        <button type="button" class="ui button medium mg-r-5" id="memberSchButton"
                                onclick="memberSearchPopup('callbackMemberSearchPop', true); return false;">
                            조회
                        </button>
                    </td>
                    <%--버튼--%>
                    <td>
                        <div style="text-align: center;" class="ui form mg-t-10">
                            <button type="submit" class="ui button large cyan" id="searchBtn">검색</button><br/>
                            <button type="button" class="ui button large white mg-t-5" id="initBtn">초기화</button><br/>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- 블랙컨슈머 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="blackConsumerMainGridCount">0</span>건</h3>
        </div>
        <div class="topline"></div>

        <div id="blackConsumerDetailGrid" style="background-color:white;width:100%;height:180px;"></div>
    </div>

    <%--기본 정보--%>
    <div class="com wrap-title sub">
        <h3 class="title font-malgun">기본정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="inputForm" name="inputForm" onsubmit="return false;">
            <table class="ui table">
                <caption>기본정보</caption>
                <colgroup>
                    <col style="width:8%;"/>
                    <col />
                    <col style="width:8%;"/>
                    <col />
                </colgroup>
                <tbody>
                <tr>
                    <th>회원명</th>
                    <td>
                        <div class="ui form inline">
                            <span id="userInfo" class="text text-gray-Lightest">회원을 검색해주세요.</span>
                        </div>
                    </td>
                    <th>구분 <span class="text-red">*</span></th>
                    <td class="ui form inline">
                        <div class="ui form inline">
                            <select id="isRelease" style="width: 120px" name='isRelease' class="ui input medium mg-r-10">
                                <option value="true">등록</option>
                                <option value="false">해제</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="reasonDetailAreaSelectBox">
                    <th>사유 <span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="reasonType" style="width: 180px" name='reasonType' class="ui input medium mg-r-10">
                                <option value="">선택</option>
                                <c:forEach var="reasonType" items="${reasonType}" varStatus="status">
                                    <option value="${reasonType.mcCd}">${reasonType.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <th>권한정지범위 <span class="text-red">*</span></th>
                    <td >
                        <div class="ui form inline">
                            <select id="consumerType" style="width: 150px" name='consumerType' class="ui input medium mg-r-10">
                                <option value="">선택</option>
                                <option value="PURCHASE_RESTRICTION">구매제한</option>
                                <option value="COMMUNITY_RESTRICTION">커뮤니티 이용제한</option>
                                <option value="BLACK_CONSUMER">블랙컨슈머</option>
                            </select>
                            <span id="reasonDetailAreaDate" class="ui form inline dateControl">
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5" placeholder="제한 시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5" placeholder="제한 종료일" style="width:100px;">
                            </span>
                        </div>
                    </td>
                </tr>
                <tr id="reasonDetailAreaInput">
                    <th>상세사유</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <textarea id="reasonDetail" name="reasonDetail" class="ui input medium mg-r-5" style="width: 800px;" maxlength="100"></textarea>
                        </div>
                    </td>
                </tr>
                <%-- 해제 사유 --%>
                <tr id="releaseDetailArea" style="display: none;">
                    <th>해제사유 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <textarea id="releaseDetail" name="releaseDetail" class="ui input medium mg-r-5" style="width: 800px;" maxlength="100"></textarea>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <input type="hidden" id="seq" name="seq">
            <input type="hidden" id="userNo" name="userNo">
            <input type="hidden" id="releaseDt" name="releaseDt">
        </form>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button large cyan font-malgun" id="saveBtn">저장</button>
            <button class="ui button large white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${consumerHistoryGridBaseInfo}
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/consumerMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>