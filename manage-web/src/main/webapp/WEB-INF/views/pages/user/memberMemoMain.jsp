<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원메모 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <form id="searchForm" name="searchForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원메모 관리</caption>
                    <colgroup>
                        <col style="width:120px;">
                        <col style="width:60%;">
                        <col/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">회원검색</th>
                        <td class="ui form inline">
                            <input type="text" id="userNm" name="userNm" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                            <input type="hidden" id="searchUserNo" name="searchUserNo">
                            <button type="button" class="ui button medium mg-r-5" id="userSchButton"
                                    onclick="memberSearchPopup('callbackMemberSearchPop', true); return false;">
                                조회
                            </button>
                        </td>
                        <td>
                            <div style="text-align: center;" class="ui form mg-t-10">
                                <button type="submit" class="ui button large cyan" id="searchBtn">검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="initBtn">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 회원메모관리 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="memberMemoListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="memberMemoGrid" style="background-color:white;width:100%;height:400px;"></div>

        <%--회원메모관리 상세--%>
        <div class="com wrap-title sub">
            <h3 class="title font-malgun">상세정보</h3>
        </div>

        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="inputForm" name="inputForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>상세정보</caption>
                    <colgroup>
                        <col style="width:120px;">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>회원명</th>
                        <td>
                            <div class="ui form inline">
                                <span id="userInfo" class="text text-gray-Lightest">회원을 검색해주세요.</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>내용</th>
                        <td><textarea id="content" class="ui input medium mg-r-5" style="width: 850px;" maxlength="100"></textarea></td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="seq" name="seq">
                <input type="hidden" id="userNo" name="userNo">
            </form>
        </div>
        <%--버튼영역--%>
        <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button large cyan font-malgun" id="saveBtn">저장</button>
            <button class="ui button large white font-malgun" id="resetBtn">초기화</button>
        </span>
        </div>

    </div>
</div>

<script type="text/javascript" src="/static/js/user/memberMemoMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${memberMemoGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        memberMemoMain.init();
        memberMemoGrid.init();
    });
</script>