<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">환불계좌 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <form id="searchForm" name="searchForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>환불계좌 관리</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="30%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">회원정보</th>
                        <td class="ui form inline">
                            <select id="searchType" style="width: 120px" name='searchType' class="ui input medium mg-r-10">
                                <option value="USER_NM">회원명</option>
                                <option value="USER_NO">회원번호</option>
                                <option value="USER_ID">회원ID</option>
                                <option value="MOBILE">휴대폰번호</option>
                            </select>
                            <span id="searchKeywordSpan">
                                <input type="text" id="keyword" name="keyword" class="ui input medium mg-r-5" style="width: 245px;">
                            </span>
                            <span id="mobileAreaSpan" class="text" style="display:none;">
                                <input type="text" id="mobile1" name="mobile1" class="ui input small mg-r-5" maxlength="3" style="width: 73px;">&#45;
                                <input type="text" id="mobile2" name="mobile2" class="ui input small mg-r-5" maxlength="4" style="width: 73px;">&#45;
                                <input type="text" id="mobile3" name="mobile3" class="ui input small mg-r-5" maxlength="4" style="width: 73px;">
                            </span>
                        </td>
                        <td>
                            <div class="ui form inline mg-b-10">
                                <button type="button" class="ui button large cyan" id="searchBtn">검색</button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large" id="initBtn">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 환불계좌관리 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="refundAccountListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="refundAccountGrid" style="background-color:white;width:100%;height:500px;"></div>

        <%--환불계좌관리 상세--%>
        <div class="com wrap-title sub">
            <h3 class="title font-malgun">상세정보</h3>
            <span class="inner">
                <button type="button" id="rmRefundAccountBtn" class="ui button medium red font-malgun"
                        style="float: right">삭제</button>
            </span>
        </div>

        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="inputForm" name="inputForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>환불계좌 상세</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="24%">
                        <col width="10%">
                        <col width="23%">
                        <col width="10%">
                        <col width="23%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>회원번호</th>
                        <td id="userNo"></td>
                        <th>회원명</th>
                        <td id="userNm"></td>
                        <th>회원ID</th>
                        <td id="userId"></td>
                    </tr>
                    <tr>
                        <th>은행명</th>
                        <td id="bankNm"></td>
                        <th>계좌번호</th>
                        <td id="bankAccount"></td>
                        <th>예금주</th>
                        <td id="bankOwner"></td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" id="seq" name="seq">
            </form>
        </div>

    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/refundAccountMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${refundAccountGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        refundAccountMain.init();
        refundAccountGrid.init();
    });
</script>