<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">회원등급 변경</h2>
    </div>

    <%--기본정보--%>
    <form id="updateForm" name="updateForm" onsubmit="return false;">
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>기본정보</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:40%">
                <col style="width:10%">
                <col style="width:40%">
            </colgroup>
            <tbody>
            <tr>
                <th>회원번호</th>
                <td id="userNo">${userGradeInfo.userNo}</td>
                <th>회원명</th>
                <td id="userNm">${userGradeInfo.userNm}</td>
            </tr>
            <tr>
                <th>아이디</th>
                <td id="userId">${userGradeInfo.userId}</td>
                <th>성별</th>
                <td id="gender">${userGradeInfo.gender}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <%--회원등급변경--%>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>회원등급변경</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:40%">
                <col style="width:10%">
                <col style="width:40%">
            </colgroup>
            <tbody>
            <tr>
                <th>등급<span class="text-red"> *</span></th>
                <td colspan="3">
                    <select id="userGrade" name="userGrade" class="ui input medium mg-r-10" style="width: 100px">
                        <option value="">선택하세요</option>
                        <c:forEach var="userGradeInfo" items="${userGradeSelectBoxInfo}" varStatus="status">
                            <option value="${userGradeInfo.gradeSeq}">${userGradeInfo.gradeNm}</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" id="gradeSeq" name="gradeSeq">
                    <input type="hidden" id="gradeNm" name="gradeNm">
                </td>
            </tr>
            <tr>
                <th>구매건수<span class="text-red"> *</span></th>
                <td>
                    <div class="ui form inline">
                        <input type="text" id="orderCnt" name="orderCnt" class="ui input medium" style="width: 100px;">
                        <span class="text">건</span>
                    </div>
                </td>
                <th>구매금액<span class="text-red"> *</span></th>
                <td>
                    <div class="ui form inline">
                        <input type="text" id="orderAmount" name="orderAmount" class="ui input medium" style="width: 100px;">
                        <span class="text">원</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>사유<span class="text-red"> *</span></th>
                <td colspan="3">
                    <div class="ui form inline">
                        <input type="text" id="reason" name="reason" class="ui input medium" style="width: 390px;" maxlength="20">
                    </div>
                </td>
            </tr>
            <tr>
                <th>문의번호<span class="text-red"> *</span></th>
                <td colspan="3">
                    <div class="ui form inline">
                        <input type="text" id="callCenterReadOnly" class="ui input medium mg-r-5" style="width: 100px;" readonly>
                        <input type="hidden" id="callCenterSeq" name="callCenterSeq">
                        <button type="button" id="getCallCenterInquiriesBtn" name="getCallCenterInquiriesBtn" class="ui button medium font-malgun">조회</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- 등급내역 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 문의번호 선택</h3>
    </div>
    <div class="topline"></div>
    <div id="userGradeUpdateGrid" style="width: 100%; height: 350px;"></div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="updateUserGradeBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
    </form>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/pop/userGradeUpdatePop.js?v=${fileVersion}"></script>
<script>
    ${userGradeUpdateGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        userGradeUpdate.init();
        userGradeUpdateGrid.init();
    });
</script>
