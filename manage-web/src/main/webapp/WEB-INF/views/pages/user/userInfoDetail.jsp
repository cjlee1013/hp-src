<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<input type="hidden" name="userNo" id="userNo" value="${userNo}"/>
<input type="hidden" name="mhcCardnoHmac" id="mhcCardnoHmac"/>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원정보 상세</h2>
    </div>

    <form id="searchDetailForm" name="searchDetailForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">• 기본정보</h3>
        <span class="inner" style="float:right;">
            <button type="button" id="loginLogBtn"
                    class="ui button medium font-malgun mg-r-5" style="float: left;">로그인LOG</button>
            <button type="button" id="vocCallCenterMainBtn"
                    class="ui button medium font-malgun mg-r-5" style="float: left;">문의내역 조회하기</button>
       </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>* 기본정보</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:24%">
                <col style="width:10%">
                <col style="width:23%">
                <col style="width:10%">
                <col style="width:23%">
            </colgroup>
            <tbody>
            <tr>
                <th scope="col">회원번호</th>
                <td id="detailUserNo"></td>
                <th scope="col">회원명</th>
                <td id="userNm"></td>
                <th scope="col">아이디</th>
                <td id="userId"></td>
            </tr>
            <tr>
                <th scope="col">생년월일</th>
                <td id="birth"></td>
                <th scope="col">성별</th>
                <td colspan="3" id="gender"></td>
            </tr>
            <tr>
                <th scope="col">휴대폰</th>
                <td id="mobile"></td>
                <th scope="col">이메일</th>
                <td colspan="3" id="email"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 회원정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>회원정보</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:24%">
                <col style="width:10%">
                <col style="width:23%">
                <col style="width:10%">
                <col style="width:23%">
            </colgroup>
            <tbody>
            <tr>
                <th scope="col">회원구분</th>
                <td id="isCompany"></td>
                <th scope="col">통합회원여부</th>
                <td id="isUnion"></td>
                <th scope="col">회원등급</th>
                <td>
                    <span id="gradeNm" class="text">
                    </span>
                    <span class="inner" id="gradeHistBtnArea">
                        <%--회원 등급변경 권한여부--%>
                        <c:if test="${MEMBER_GRADE eq true}">
                        <button type="button" id="userGradeUpdateBtn"
                                class="ui button medium font-malgun mg-r-5" style="float: right">등급변경</button>
                        </c:if>
                        <button type="button" id="gradeHistBtn"
                                class="ui button medium gray-dark font-malgun mg-r-5"
                                style="float: right">등급내역</button>
                    </span>
                </td>
            </tr>
            <tr>
                <th scope="col">본인인증여부</th>
                <td id="isCert"></td>
                <th scope="col">성인인증여부</th>
                <td id="isAdultCertificate"></td>
                <th scope="col">블랙컨슈머</th>
                <td id="isBlackConsumer"></td>
            </tr>
            <tr>
                <%--<th scope="col">SNS 로그인연결</th>
                <td id="sns"></td>--%>
                <th scope="col">개인통관부호</th>
                <td id="personalOverseaNo"></td>
                <th scope="col">임직원여부</th>
                <td colspan="3" id="empId"></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <th scope="col">가입일시</th>
                <td id="regDt"></td>
                <th scope="col">최종접속일시</th>
                <td colspan="3" id="lastLoginDt"></td>
                <%--<th scope="col">최초주문여부</th>--%>
                <%--<td></td>--%>
            </tr>
            <tr>
                <th scope="col">Hyper 최초주문</th>
                <td id="hyperFirstOrder"></td>
                <th scope="col">Express 최초주문</th>
                <td id="expFirstOrder"></td>
                <th scope="col">Club 최초주문</th>
                <td id="clubFirstOrder"></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 제휴정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>제휴정보</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:24%">
                <col style="width:10%">
                <col style="width:23%">
                <col style="width:10%">
                <col style="width:23%">
            </colgroup>
            <tbody>
            <tr>
                <th scope="col">MHC<br/>발급여부</th>
                <td id="isMhcCard"></td>
                <th scope="col">MHC<br/>카드유형</th>
                <td id="mhcCardType"></td>
                <th  scope="col">대체카드번호</th>
                <td id="mhcCardno"></td>
            </tr>
            <tr>
                <th  scope="col">MHC<br/>가입일시</th>
                <td id="mhcCardIssueDt"></td>
                <th  scope="col">MHC<br/>보유포인트</th>
                <%--MHC 보유포인트 영역--%>
                <td id="mhcPointArea"></td>
                <th  scope="col">OCB<br/>제휴여부</th>
                <td id="ocbYn"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <%-- 비노출 처리
    <div class="com wrap-title sub">
        <h3 class="title">• 계좌 환불 정보</h3>
        <span class="inner">
            <button type="button" id="delBankAccountBtn" class="ui button medium red font-malgun"
                    style="float: right">계좌삭제</button>
        </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>계좌 환불 정보</caption>
            <colgroup>
                <col style="width:10%">
                <col style="width:24%">
                <col style="width:10%">
                <col style="width:23%">
                <col style="width:10%">
                <col style="width:23%">
            </colgroup>
            <tbody>
            <tr>
                <th scope="col">은행명</th>
                <td id="bankNm"></td>
                <th scope="col">계좌번호</th>
                <td id="bankAccount"></td>
                <th scope="col">예금주</th>
                <td id="bankOwner"></td>
                <input type="hidden" id="bankSeq"/>
            </tr>
            </tbody>
        </table>
    </div>--%>

    <div class="com wrap-title sub">
        <h3 class="title">• 마케팅 정보</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>마케팅 정보</caption>
            <colgroup>
                <col style="width: 10%;"/>
                <col style="width: 22%;">
                <col style="width: 22%;">
                <col style="width: 23%;">
                <col style="width: 23%;">
            </colgroup>
            <tbody>
            <tr>
                <th scope="col">마케팅<br/>활용동의</th>
                <td colspan="4">
                    <span class="inner" id="marketingAgreeArea"></span>
                </td>
            </tr>
            <tr>
                <th scope="col">마케팅<br/>정보수신</th>
                <td>SMS 수신동의 : <span id="smsAgreeYn"></span></td>
                <td style="border-left:3px solid #f9f9f9;">이메일 수신동의 : <span id="emailAgreeYn"></span></td>
                <td style="border-left:3px solid #f9f9f9;">우편 수신동의 : <span id="dmAgreeYn"></span></td>
                <td style="border-left:3px solid #f9f9f9;">영수증쿠폰 수신동의 : <span id="receiptAgreeYn"></span></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 배송지 정보</h3>
        <span class="inner">
            <button type="button" id="delAddressBtn" class="ui button medium red font-malgun"
                    style="float: right">주소삭제</button>
        </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
    </div>

    <div class="com wrap-title sub"></div>
    <div class="topline"></div>

    <div id="shippingGrid" style="background-color:white;width:100%;height:125px;"></div>
    <!-- //배송지 정보 그리드 -->
    </form>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/userInfoDetail.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${shippingAddressGridBaseInfo}

    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        userInfoDetail.init();
        shippingGrid.init();
    });
</script>