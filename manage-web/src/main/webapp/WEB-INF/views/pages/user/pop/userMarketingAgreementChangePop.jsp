<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<input type="hidden" name="userNo" id="userNo" value="${userNo}"/>
<div class="com wrap-popup xlarge" style="background-color: #FFFFFF">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">마케팅 동의 변경</h2>
    </div>

    <%--기본정보--%>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>기본정보</caption>
            <colgroup>
                <col width="10%">
                <col width="24%">
                <col width="10%">
                <col width="23%">
                <col width="10%">
                <col width="23%">
            </colgroup>
            <tbody>
            <tr>
                <th>아이디</th>
                <td id="userId"></td>
                <th>회원명</th>
                <td id="userNm"></td>
                <th>상태</th>
                <td id="userStatus"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <%--상태변경--%>
    <div class="com wrap-title sub">
        <h3 class="title">상태변경 <span
                style="font-weight:normal;font-size:13px;">( 저장 시점에 바로 반영됩니다. )</span></h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>상태변경</caption>
            <colgroup>
                <col width="10%">
                <col width="40%">
                <col width="10%">
                <col width="40%">
            </colgroup>
            <tbody>
            <tr>
                <th>활용동의</th>
                <td>
                    <label class="ui checkbox mg-r-20" style="display: inline-block">
                        <input type="checkbox" id ="targetrepYn" name="targetrepYn"><span>마케팅 동의함</span>
                    </label>
                </td>
                <th>정보수신</th>
                <td>
                    <label class="ui checkbox mg-r-20" style="display: inline-block">
                        <input type="checkbox" id ="smsrepYn" name="smsrepYn"><span>SMS 수신 동의함</span>
                    </label>
                    <label class="ui checkbox mg-r-20" style="display: inline-block">
                        <input type="checkbox" id ="emailrepYn" name="emailrepYn"><span>이메일 수신 동의함</span>
                    </label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="saveBtn">저장</button>
            <button class="ui button xlarge dark-blue font-malgun" id="resetBtn">초기화</button>
            <button class="ui button xlarge font-malgun" id="closeBtn"
                    onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/pop/userMarketingAgreementChangePop.js?v=${fileVersion}"></script>
<script>
    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        userMarketingAgreementChange.init();
    });
</script>
