<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<input type="hidden" name="userNo" id="userNo" value="${userNo}"/>
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">로그인LOG</h2>
    </div>
    <!-- 로그인로그 그리드 -->
    <div class="com wrap-title sub">
    </div>
    <div class="topline"></div>
    <div id="userLoginLogGrid" style="width: 100%; height: 350px;"></div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="closeBtn"
                    onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript"
        src="/static/js/user/pop/userLoginLogPop.js?v=${fileVersion}"></script>
<script>
    ${userLoginLogGridBaseInfo}

    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        userLoginLog.init();
        userLoginLogGrid.init();
    });
</script>
