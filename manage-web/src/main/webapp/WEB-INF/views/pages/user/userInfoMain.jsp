<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">회원정보 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <%--회원정보 관리 메뉴 탭 아이디--%>
        <input type="hidden" id="userInfoMainTabId">
        <form id="searchForm" name="searchForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원관리</caption>
                    <colgroup>
                        <col style="width:120px;"/>
                        <col style="width:60%;"/>
                        <col/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">회원정보</th>
                        <td>
                            <select id="searchType" style="width: 120px;display: inline;" name='searchType'
                                    class="ui input medium mg-r-5">
                                <option value="USER_NM">회원명</option>
                                <option value="USER_NO">회원번호</option>
                                <option value="USER_ID">회원ID</option>
                                <option value="MOBILE">휴대폰번호</option>
                            </select>
                            <span id="searchKeywordSpan">
                                <input type="text" id="keyword" name="keyword" class="ui input medium mg-r-5"
                                       style="width: 245px;display: inline;">
                            </span>
                            <span id="mobileSpan" class="text" style="display:none;">
                                <input type="text" id="mobile1" name="mobile1" class="ui input small mg-r-5"
                                       maxlength="3" style="width: 73px;display: inline;">&#45;
                                <input type="text" id="mobile2" name="mobile2" class="ui input small mg-r-5"
                                       maxlength="4" style="width: 73px;display: inline;">&#45;
                                <input type="text" id="mobile3" name="mobile3" class="ui input small mg-r-5"
                                       maxlength="4" style="width: 73px;display: inline;">
                            </span>
                        </td>
                        <%-- 검색 버튼 --%>
                        <td>
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="initBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 회원관리 검색 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="searchUserMainListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>

        <div id="userInfoGrid" style="background-color:white;width:100%;height:700px;"></div>
        <!-- // 회원관리 검색 그리드 -->
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/user/userInfoMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${userMainGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        userInfoMain.init();
        userInfoGrid.init();
    });
</script>