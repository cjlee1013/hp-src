<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<!-- 기본 프레임 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 등록/수정</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>카테고리 검색</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="60%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lcateCd"
                                    style="width:150px;float:left;" data-default="1depth"
                                    onchange="javascript:categoryMng.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="mcateCd"
                                    style="width:150px;float:left;" data-default="2depth"
                                    onchange="javascript:categoryMng.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="scateCd"
                                    style="width:150px;float:left;" data-default="3depth"
                                    onchange="javascript:categoryMng.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="dcateCd"
                                    style="width:150px; display: none;" data-default="4depth">
                            </select>
                        </td>
                        <td>
                            <div class="ui form inline">
                                <button type="button" class="ui button large mg-r-5 cyan pull-left"
                                        id="searchBtn"><i class="fa fa-search"></i> 검색
                                </button>
                                <span style="margin-left:4px;"></span>
                                <button type="button" class="ui button large mg-r-5 white pull-left"
                                        id="searchResetBtn">초기화
                                </button>
                                <span style="margin-left:4px;"></span>
                                <button type="button"
                                        class="ui button large mg-r-5 dark-blue pull-left"
                                        id="excelDownloadBtn">엑셀다운
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="categoryGridCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="categoryGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">카테고리 등록/수정</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <caption>카테고리 등록/수정</caption>
            <colgroup>
                <col width="138px">
                <col width="115px">
                <col width="115px">
                <col width="310px">
                <col width="88px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>카테고리 ID</th>
                <td>
                    <div class="ui form inline">
                        <input id="cateCd" type="text" class="ui input medium mg-r-5" readonly style="width:100px">
                        <input type="hidden" id="orgLcateCd">
                    </div>
                </td>
                <th>카테고리명 <span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <input id="cateNm" type="text" class="ui input medium mg-r-5" style="width:300px"
                               maxlength="16">
                    </div>
                </td>
                <th>우선순위<span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <input id="priority" type="text" class="ui input medium mg-r-5"
                               maxlength="4" style="width:60px;">
                    </div>
                </td>
            </tr>
            <tr>
                <th>단계 (Depth) <span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="depth" style="width:80px">
                            <option selected="" value="">선택</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                </td>
                <th>부모카테고리 <span class="text-red">*</span></th>
                <td colspan="3">
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-5" id="formCateCd1" name="lcateCd"
                                style="width:150px;float:left;" data-default="1depth"
                                onchange="javascript:categoryMng.changeCategorySelectBox(1,'formCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="formCateCd2" name="mcateCd"
                                style="width:150px;float:left;" data-default="2depth"
                                onchange="javascript:categoryMng.changeCategorySelectBox(2,'formCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="formCateCd3" name="scateCd"
                                style="width:150px;float:left;" data-default="3depth"
                                onchange="javascript:categoryMng.changeCategorySelectBox(3,'formCateCd');">
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>오프라인 매핑 <span class="text-red">*</span></th>
                <td colspan="5">
                    <select class="ui input medium mg-r-5" id="formMappingCateCd1" name="division"
                            style="width:150px;float:left;" data-default="1depth" onchange="javascript:categoryMng.changeCategoryMappingSelectBox(1,'formMappingCateCd');"
                            >
                    </select>
                    <select class="ui input medium mg-r-5" id="formMappingCateCd2" name="groupNo"
                            style="width:150px;float:left;" data-default="2depth" onchange="javascript:categoryMng.changeCategoryMappingSelectBox(2,'formMappingCateCd');"
                    >
                    </select>
                    <select class="ui input medium mg-r-5" id="formMappingCateCd3" name="dept"
                            style="width:150px;float:left;" data-default="3depth" onchange="javascript:categoryMng.changeCategoryMappingSelectBox(3,'formMappingCateCd');"
                    >
                    </select>
                    <select class="ui input medium mg-r-5" id="formMappingCateCd4" name="classCd"
                            style="width:150px;float:left;" data-default="4depth" onchange="javascript:categoryMng.changeCategoryMappingSelectBox(4,'formMappingCateCd');"
                    >
                    </select>
                    <select class="ui input medium mg-r-5" id="formMappingCateCd5" name="subclass"
                            style="width:150px;float:left;" data-default="5depth"
                    >
                    </select>

                </td>
            </tr>
            <tr>
                <th>장바구니 담기 제한</th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="cartYn" name="cartYn"
                                disabled="disabled" style="width:100px;">
                            <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
                <th>19금 제한</th>
                <td>
                    <c:set var="dfAdultLimitYn" value=""/>
                    <c:forEach var="codeDto" items="${limitYn}" varStatus="status">
                        <c:set var="checked" value=""/>
                        <c:if test="${codeDto.ref1 eq 'df'}">
                            <c:set var="checked" value="checked"/>
                            <c:set var="dfAdultLimitYn" value="${codeDto.mcCd}"/>
                        </c:if>
                        <label class="ui radio inline">
                            <input type="radio" disabled="disabled"
                                   id="adultLimitYn_${codeDto.mcCd}" name="adultLimitYn"
                                   class="ui input"
                                   value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span>
                        </label>
                    </c:forEach>
                </td>
                <th>노출범위 <span class="text-red">*</span></th>
                <td>
                    <div class="ui form inline">
                        <select class="ui input medium mg-r-10" id="dispYn" style="width:120px;">
                            <c:forEach var="codeDto" items="${categoryDispYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <th>도서카테고리</th>
                <td>
                    <div class="ui form inline">
                        <select id="isbnYn" style="width: 100px" name="isbnYn" class="ui input medium mg-r-10">
                            <c:forEach var="codeDto" items="${isbnYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
                <th>리뷰노출여부</th>
                <td>
                    <div class="ui form inline">
                        <select id="reviewDisplay" style="width: 120px" name="reviewDisplay" class="ui input medium mg-r-10">
                            <c:forEach var="codeDto" items="${reviewDisplay}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </div>
                </td>
                <th>사용여부 <span class="text-red">*</span></th>
                <td>
                    <span class="ui form inline">
                        <select class="ui input medium mg-r-10" id="useYn" style="width:90px;">
                            <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                                <c:set var="selected" value=""/>
                                <c:if test="${codeDto.ref1 eq 'df'}">
                                    <c:set var="selected" value="selected"/>
                                </c:if>
                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </span>
                </td>
            </tr>
            <tr>
                <th>API연동여부</th>
                <td colspan="5">
                    <select class="ui input medium mg-r-10" id="apiUseYn" style="width:100px;">
                        <c:forEach var="codeDto" items="${useYn}" varStatus="status">
                            <c:set var="selected" value=""/>
                            <c:if test="${codeDto.ref1 eq 'df'}">
                                <c:set var="selected" value="selected"/>
                            </c:if>
                            <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <c:if test="${CATEGORY_VIEW eq 'N'}">
    ※ 카테고리 정보 수정 후 '저장' 버튼 클릭 시 어드민에 저장됩니다. <BR>
    ※ 어드민에 저장 후 '프론트 반영' 버튼 클릭 시 프론트에 최종 반영됩니다. (단, 시스템속도 저하로 인해 '프론트 반영'은 전체 저장 후 마지막에 한번 해주세요.)
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="setCategoryBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            <button type="button" class="ui button xlarge dark-blue font-malgun" id="setStaticCategoryBtn">프론트 반영</button>
        </span>
    </div>
    </c:if>
</div>




<script>
    var categoryGrid = {
        treeView: new RealGridJS.TreeView("categoryGrid"),
        treeDataProviders: new RealGridJS.LocalTreeDataProvider(),
        init: function () {
            categoryGrid.initGrid();
            categoryGrid.initDataProvider();
            categoryGrid.event();
        },
        initGrid: function () {
            categoryGrid.treeView.setDataSource(categoryGrid.treeDataProviders);

            categoryGrid.treeView.setStyles(categoryGridBaseInfo.realgrid.styles);
            categoryGrid.treeView.setDisplayOptions(categoryGridBaseInfo.realgrid.displayOptions);
            categoryGrid.treeView.setColumns(categoryGridBaseInfo.realgrid.columns);
            categoryGrid.treeView.setOptions(categoryGridBaseInfo.realgrid.options);
        },
        initDataProvider: function () {
            categoryGrid.treeDataProviders.setFields(categoryGridBaseInfo.dataProvider.fields);
            categoryGrid.treeDataProviders.setOptions(categoryGridBaseInfo.dataProvider.options);
        },
        event: function () {
            // 그리드 선택
            categoryGrid.treeView.onDataCellClicked = function (gridView, index) {
                categoryMng.getDetail(index.dataRow);
            };
        },
        setData: function (dataList) {
            categoryGrid.treeDataProviders.clearRows();
            categoryGrid.treeDataProviders.fillJsonData({"rows": dataList},
                    {rows: "rows", icon: "lcateCd"});
        },
        excelDownload: function () {
            if (categoryGrid.treeView.getItemCount() == 0) {
                alert("검색 결과가 없습니다.");
                return false;
            }

            var _date = new Date();
            var fileName = "카테고리관리_" + _date.getFullYear() + (_date.getMonth() + 1)
                    + _date.getDate();

            categoryGrid.treeView.exportGrid({
                type: "excel",
                target: "local",
                showColumns : ['division', 'groupNo', 'dept', 'classCd', 'subclass', 'divisionNm', 'groupNoNm', 'deptNm', 'classCdNm', 'subclassNm'],
                fileName: fileName + ".xlsx",
                showProgress: true,
                progressMessage: "엑셀 Export중입니다."
            });
        }
    };

    var categoryMng = {
        categoryCnt: 4,
        init: function () {
            this.event();
            categoryMng.setCategorySelectBox(1, '', '', $('#searchCateCd1'));
            categoryMng.setCategorySelectBox(2, '', '', $('#searchCateCd2'));
            categoryMng.setCategorySelectBox(3, '', '', $('#searchCateCd3'));
            categoryMng.setCategorySelectBox(4, '', '', $('#searchCateCd4'));

            categoryMng.setCategorySelectBox(2, '', '', $('#formCateCd2'));
            categoryMng.setCategorySelectBox(3, '', '', $('#formCateCd3'));

            categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
            categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd2'), true);
            categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd3'), true);
            categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd4'), true);
            categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd5'), true);

            categoryMng.changeCategorySelectItem('1');
            categoryMng.formReset();
        },
        event: function () {
            //카테고리 검색 버튼 클릭
            $('#searchBtn').click(function () {
                categoryMng.search();
            });

            //카테고리 저장 버튼 클릭
            $('#setCategoryBtn').click(function () {
                categoryMng.setCategory();
            });

            //카테고리 리셋 버튼 클릭
            $('#resetBtn').click(function () {
                categoryMng.formReset();
            });

            //카테고리 엑셀 다운로드 버튼 클릭
            $('#excelDownloadBtn').click(function () {
                categoryGrid.excelDownload();
            });

            //카테고리 검색 리셋 버튼 클릭
            $('#searchResetBtn').click(function () {
                categoryMng.searchFormReset();
            });

            //정적 카테고리 생성
            $('#setStaticCategoryBtn').click(function () {
                categoryMng.setStaticCategory();
            });

            //정적 카테고리 생성
            $('#chkClubDispYn').change(function () {
                categoryMng.chkClubDispYn();
            });

            //뎁스 변경시
            $('#depth').change(function () {
                $('#formCateCd1,#formCateCd2,#formCateCd3').prop('disabled', false);
                $('select[id^="formMappingCateCd"]').prop('disabled', true);
                $('#chkClubDispYn').prop('disabled', true);

                switch ($(this).val()) {
                    case '1' :
                        $('#formCateCd1').prop('disabled', true);
                        $('#chkClubDispYn').prop('disabled', false);
                    case '2' :
                        $('#formCateCd2').prop('disabled', true);
                    case '3' :
                        $('#formCateCd3').prop('disabled', true);
                        $('#cartYn').val('N').prop('disabled', true);
                        $('input:radio[name="adultLimitYn"]:input[value="N"]').trigger('click');
                        $('input:radio[name="adultLimitYn"]').prop('disabled', true);
                        $('#apiUseYn').val('Y').prop('disabled', true);
                        break;
                    case '4' :
                        $('#cartYn').prop('disabled', false);
                        $('input:radio[name="adultLimitYn"], select[id^="formMappingCateCd"]').prop('disabled', false);
                        $('#apiUseYn').prop('disabled', false);
                        break;
                }
            });

            //사용여부 변경시
            $('#useYn').change(function () {
                if ($(this).val() == 'N') {
                    $('#dispYn').val('N');
                    $('#dispYn  option[value=N]').prop('disabled', false);
                    $('select[id=dispYn]').prop('disabled', true);
                    $('#priority').val('').prop('readonly', true);

                    if ( $('#depth').val() == "1" ) {
                        $("input[name=chkClubDispYn]:checkbox").prop("checked", true);
                    }
                } else {
                    $('select[id=dispYn]').prop('disabled', false);
                    $('#priority').val('').prop('readonly', false);

                    if ( $('#depth').val() == "1" ) {
                        $("input[name=chkClubDispYn]:checkbox").prop("checked", false);
                    }
                }
            });

            $("#priority").on({
                keydown: function (e) {
                    if (e.which === 32) return false;
                },
                keyup: function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                },
                change: function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }
            });

            $('#setSchBrandBtn').on('click', function() {
                categoryMng.getSchLgcMapping();
            });

        },
        search: function (cateArr) {
            var url = $('#searchForm').serialize();
            if (typeof cateArr == 'object') {
                url = $.param(cateArr)
            }

            CommonAjax.basic({
                url: '/common/category/getCategory.json?' + url,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    categoryGrid.setData(res);
                    var searchVal;
                    for (var i = categoryMng.categoryCnt; i > 0; i--) {
                        if ($('#searchCateCd' + i).val() != "") {
                            searchVal = $('#searchCateCd' + i + ' option:selected').text();
                            break;
                        }
                    }

                    var ret = categoryGrid.treeDataProviders.searchData(
                            {fields: ["cateNm"], value: searchVal, wrap: true});
                    if (ret) {
                        var rowId = ret.dataRow;
                        var parents = categoryGrid.treeDataProviders.getAncestors(rowId);
                        if (parents) {
                            for (var i = parents.length - 1; i >= 0; i--) {
                                categoryGrid.treeView.expand(
                                        categoryGrid.treeView.getItemIndex(parents[i]));
                            }
                            categoryGrid.treeView.setCurrent({
                                itemIndex: categoryGrid.treeView.getItemIndex(rowId),
                                fieldIndex: ret.fieldIndex
                            })
                            categoryGrid.treeView.expand(categoryGrid.treeView.getItemIndex(rowId));
                        }
                    }

                    $('#categoryGridCnt').html($.jUtil.comma(categoryGrid.treeView.getItemCount()));
                }
            });
        },
        searchFormReset: function () {
            $('#searchCateCd1').val('').change();
        },
        formReset: function (option) {
            $('#cateCd').val('');
            $('#cateNm').val('');
            $('#cartYn').val('N');
            $('input:radio[name="adultLimitYn"]:input[value="N"]').trigger('click');
            $('#useYn').val('Y').change();
            $('#dispYn').val('Y').change();
            $('#isbnYn').val('Y').change();
            $('#reviewDisplay').val('Y').change();
            $('#priority').val('');
            $('#depth').val('').change();
            $("#depth[id=depth]").prop("disabled", false);
            $('#parentCateCd').val('');
            $('#apiUseYn').val('Y');
            $("input[name=chkClubDispYn]:checkbox").prop("checked", false);

            categoryMng.changeCategorySelectItem('1');

            if (option != 'grid') {
                categoryMng.setCategorySelectBox(1, '', '', $('#formCateCd1'));
                $('#formCateCd1').val('').change();
                // 오프라인 매핑 초기화
                categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'));
                $('#formMappingCateCd1 option:first').prop('selected', true).change();
            }

            // 비활성 처리
            $('input:radio[name="adultLimitYn"], select[id^="formMappingCateCd"], #cartYn, #apiUseYn').prop('disabled', true);
        },
        getDetail: function (selectRowId) {
            const rowDataJson = categoryGrid.treeDataProviders.getJsonRow(selectRowId);
            categoryMng.formReset('grid');

            const cateCd = rowDataJson.cateCd;
            const cateNm = rowDataJson.cateNm;
            const cartYn = rowDataJson.cartYn;
            const useYn = rowDataJson.useYn;
            const dispYn = rowDataJson.dispYn;
            const isbnYn = rowDataJson.isbnYn;
            const reviewDisplay = rowDataJson.reviewDisplay;
            const priority = rowDataJson.priority;
            const depth = rowDataJson.depth;
            const lcateCd = rowDataJson.lcateCd;
            const mcateCd = rowDataJson.mcateCd;
            const scateCd = rowDataJson.scateCd;
            const adultLimitYn = rowDataJson.adultLimitYn;
            const apiUseYn = rowDataJson.apiUseYn;

            $('#cateCd').val(cateCd);
            $('#orgLcateCd').val(lcateCd);
            $('#cateNm').val(cateNm);
            // $('#apiUseYn').val(apiUseYn);
            $('#depth').val(depth).change();
            $('#depth  option[value=' + depth + ']').prop('disabled', false);
            $("select[id=depth]").prop("disabled", true);

            categoryMng.setCategorySelectBox(1, '', lcateCd, $('#formCateCd1'));
            categoryMng.setCategorySelectBox(2, lcateCd, mcateCd, $('#formCateCd2'));
            categoryMng.setCategorySelectBox(3, mcateCd, scateCd, $('#formCateCd3'));

            categoryMng.changeCategorySelectItem(depth);

            if (depth == 4) {
                categoryMng.setCategoryMappingSelectBox(1, $('#formMappingCateCd1'), false, rowDataJson);
                categoryMng.setCategoryMappingSelectBox(2, $('#formMappingCateCd2'), false, rowDataJson);
                categoryMng.setCategoryMappingSelectBox(3, $('#formMappingCateCd3'), false, rowDataJson);
                categoryMng.setCategoryMappingSelectBox(4, $('#formMappingCateCd4'), false, rowDataJson);
                categoryMng.setCategoryMappingSelectBox(5, $('#formMappingCateCd5'), false, rowDataJson);

                $('#isbnYn').val(isbnYn).change();
                $('#reviewDisplay').val(reviewDisplay).change();
            } else {
                $('[id^="formMappingCateCd"]').val(0);
            }

            $('#useYn').val(useYn).change();
            $('#dispYn').val(dispYn).change();
            $('#priority').val(priority);

            if (depth == 4) {
                $('#cartYn').val(cartYn).prop('disabled', false);
                $('input:radio[name="adultLimitYn"]').prop('disabled', false);
                $('input:radio[name="adultLimitYn"]:input[value="' + adultLimitYn + '"]').trigger(
                        'click');
                $('#apiUseYn').val(apiUseYn).prop('disabled', false);
            }

            if ( rowDataJson.clubDispYn == 'N' ) {
                $("input[name=chkClubDispYn]:checkbox").prop("checked", true);
            } else {
                $("input[name=chkClubDispYn]:checkbox").prop("checked", false);
            }

        },
        setCategory: function () {
            var depth = $('#depth').val();
            var isEdit = $('#cateCd').val() == '' ? false : true;

            if (isEdit == true) {
                if ($('#cateCd').val() == "") {
                    alert("등록 버튼을 클릭하여 카테고리 신규 등록하세요.");
                    return false;
                }
            }

            if ($('#cateNm').val() == '') {
                alert('카테고리명을 입력해주세요.');
                $('#cateNm').focus();
                return false;
            }

            if ($.jUtil.isNotAllowInput($('#cateNm').val(), ['SPC_SCH']) || $('#cateNm').val().length > 16) {
                alert('카테고리명 최대 16자까지 가능하며, 입력가능 문자 : 숫자, 영문, 한글'
                        + ' 입니다.');
                $('#cateNm').focus();
                return false;
            }

            if ($('#depth').val() == '') {
                alert('Depth를 선택해주세요.');
                $('#depth').focus();
                return false;
            }

            if ($('#useYn').val() == 'Y') {
                if ($('#priority').val() == '') {
                    alert('우선순위를 입력해주세요.');
                    $('#priority').focus();
                    return false;
                }

                var pattern_number = /[0-9]+$/;
                if (!pattern_number.test($('#priority').val())) {
                    alert('숫자만 입력 가능하며, 최대 9999까지만 입력하실 수 있습니다.');
                    $('#priority').focus();
                    return false;
                }
            }

            var parentCateCd    = 0;
            var lcateCd         = '';
            var mcateCd         = '';
            var scateCd         = '';
            var dcateCd         = '';

            switch (depth) {
                case '4':
                    lcateCd = $('#formCateCd1').val();
                    mcateCd = $('#formCateCd2').val();
                    scateCd = $('#formCateCd3').val();
                    parentCateCd = $('#formCateCd3').val();
                    break;
                case '3':
                    lcateCd = $('#formCateCd1').val();
                    mcateCd = $('#formCateCd2').val();
                    parentCateCd = $('#formCateCd2').val();
                    break;
                case '2':
                    lcateCd = $('#formCateCd1').val();
                    parentCateCd = $('#formCateCd1').val();
                    break;
            }

            if ($('#depth').val() > 1 && parentCateCd == 0) {
                alert('부모카테고리를 선택해주세요.');
                return false;
            }

            if($('#depth').val() == 4 ) {
                if ( ($('#formMappingCateCd3').val() == '0' || $('#formMappingCateCd4').val() == '0' || $('#formMappingCateCd5').val() == '0') ) {
                    alert('오프라인 매핑 카테고리를 선택해주세요.');
                    return false;
                }
            }

            var sucessMsg = (isEdit == true) ? "수정 완료되었습니다." : "등록 완료되었습니다.";

            var tmpCateCd = $('#cateCd').val();
            if (!isEdit || tmpCateCd == "") {
                tmpCateCd = 0;
            }

            var data = Array();
            data.push({name: "cateCd", value: tmpCateCd});
            data.push({name: "cateNm", value: $('#cateNm').val()});
            data.push({name: "depth", value: $('#depth').val()});
            data.push({name: "parentCateCd", value: parentCateCd});
            data.push({name: "priority", value: $('#priority').val()});
            data.push({name: "cartYn", value: $('#cartYn').val()});
            data.push({name: "useYn", value: $('#useYn').val()});
            data.push({name: "dispYn", value: $('#dispYn').val()});
            data.push({name: "isbnYn", value: $('#isbnYn').val()});
            data.push({name: "reviewDisplay", value: $('#reviewDisplay').val()});
            data.push({
                name: "adultLimitYn",
                value: $('input:radio[name="adultLimitYn"]:checked').val()
            });
            data.push({name: "dept", value: $('select[name="dept"]').val()});
            data.push({name: "classCd", value: $('select[name="classCd"]').val()});
            data.push({name: "subclass", value: $('select[name="subclass"]').val()});
            data.push({name: "apiUseYn", value: $('#apiUseYn').val()});
            data.push({name: "clubDispYn", value: ($("input:checkbox[name=chkClubDispYn]").is(":checked") ? "N" : "Y")});

            CommonAjax.basic({
                url: '/common/category/setCategory.json',
                data: data,
                method: 'get',
                successMsg: sucessMsg,
                callbackFunc: function () {
                    var cateObj = {
                        lcateCd: lcateCd,
                        mcateCd: mcateCd,
                        scateCd: scateCd,
                        dcateCd: dcateCd
                    };

                    categoryMng.formReset();
                    categoryMng.search(cateObj);

                    categoryMng.setCategorySelectBox(1, '', lcateCd, $('#searchCateCd1'));
                    categoryMng.setCategorySelectBox(2, lcateCd, mcateCd, $('#searchCateCd2'));
                    categoryMng.setCategorySelectBox(3, mcateCd, scateCd, $('#searchCateCd3'));
                    categoryMng.setCategorySelectBox(4, scateCd, dcateCd, $('#searchCateCd4'));
                }
            });
        },
        setCategorySelectBox: function (depth, parentCateCd, cateCd, selector) {
            $(selector).html('<option value="">' + $(selector).data('default') + '</option>');
            if ((!parentCateCd && depth == 1) || (parentCateCd && depth > 1)) {
                CommonAjax.basic({
                    url: '/common/category/getAllCategoryForSelectBox.json',
                    data: {
                        depth: depth,
                        parentCateCd: parentCateCd
                    },
                    method: 'get',
                    callbackFunc: function (resData) {
                        $.each(resData, function (idx, val) {
                            var selected = (cateCd == val.cateCd) ? 'selected="selected"' : '';
                            $(selector).append(
                                    '<option value="' + val.cateCd + '" ' + selected + '>'
                                    + val.cateNm
                                    + '</option>');
                        });
                        $(selector).css('-webkit-padding-end', '30px');
                        if (depth == 1) {
                            $('#formCateCd1').html($(selector).html());
                        }
                    }
                });
            }
        },
        setCategoryMappingSelectBox: function (depth, selector, isInit, rowDataJson) {

            $(selector).html('<option value="0">' + $(selector).data('default') + '</option>');

            if (!isInit) {
                var fieldNm     = '';
                var division    = $('select[name=division]').val();
                var groupNo     = $('select[name=groupNo]').val();
                var dept        = $('select[name=dept]').val();
                var classCd     = $('select[name=classCd]').val();

                switch (depth) {
                    case 1 :
                        fieldNm     = 'division';
                        break;
                    case 2 :
                        fieldNm     = 'groupNo';
                        division    = rowDataJson ? rowDataJson.division : division;
                        break;
                    case 3 :
                        fieldNm     = 'dept';
                        groupNo     = rowDataJson ? rowDataJson.groupNo : groupNo;
                        break;
                    case 4 :
                        fieldNm     = 'classCd';
                        dept        = rowDataJson ? rowDataJson.dept    : dept;
                        break;
                    case 5 :
                        fieldNm     = 'subclass';
                        if (rowDataJson) {
                            dept        = rowDataJson.dept;
                            classCd     = rowDataJson.classCd;
                        }
                        break;
                }

                CommonAjax.basic({
                    url: '/common/category/getCategoryForMappingSelectBox.json',
                    data: {
                        depth: depth,
                        division: division,
                        groupNo: groupNo,
                        dept: dept,
                        classCd: classCd,
                    },
                    method: 'get',
                    callbackFunc: function (resData) {
                        $.each(resData, function (idx, val) {
                            var selected = '';

                            if (rowDataJson) {
                                selected = (rowDataJson[fieldNm] == val[fieldNm]) ? 'selected="selected"' : '';
                            }

                            $(selector).append(
                                    '<option value="' + val[fieldNm] + '" ' + selected + '>'
                                    + val.cateNm
                                    + '</option>');
                        });
                        $(selector).css('-webkit-padding-end', '30px');
                    }
                });
            }
        },
        changeCategorySelectBox: function (depth, prefix) {
            var cateCd = $('#' + prefix + depth).val();
            this.setCategorySelectBox(depth + 1, cateCd, '', $('#' + prefix + (depth + 1)));
            $('#' + prefix + (depth + 1)).change();
        },
        changeCategoryMappingSelectBox: function (depth, prefix) {
            var cateCd = $('#' + prefix + depth).val();

            this.setCategoryMappingSelectBox(depth + 1, $('#' + prefix + (depth + 1)), cateCd == '' ? true : false);
            $('#' + prefix + (depth + 1)).change();
        },
        changeCategorySelectItem: function (depth) {
            if(depth === '4') {
                $('#reviewDisplay').prop('disabled', false);
            } else {
                $('#reviewDisplay').prop('disabled', true);
            }
        },
        setStaticCategory: function () {
            CommonAjax.basic({
                url: '/common/category/setStaticCategory.json',
                data: { },
                method: 'get',
                callbackFunc: function (resData) {
                    alert(resData.returnMsg);
                }
            });
        },
        chkClubDispYn: function () {
            if ( $('#useYn').val() == "N" ) {
                $("input[name=chkClubDispYn]:checkbox").prop("checked", true);
            }
        }
    }

    $(document).ready(function () {
        categoryMng.init();
        categoryGrid.init();
        CommonAjaxBlockUI.targetId('searchBtn');
    });

    ${categoryGridBaseInfo}
</script>