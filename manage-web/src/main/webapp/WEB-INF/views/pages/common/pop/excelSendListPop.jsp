<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">일괄 발송처리</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="excelSendListPopForm" onsubmit="return false;">
                <div class="ui wrap-table horizontal mg-t-1 0">
                    <table class="ui table">
                        <caption>일괄 발송처리</caption>
                        <colgroup>
                            <col width="20%">
                            <col width="80%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th>파일 등록</th>
                            <td>
                                <input type="file" id="excels" name="excels" accept=".xlsx, .xls" style="width: 100%">
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <div class="ui form inline">
                                    <button class="ui button small white font-malgun" id="sampleBtn"><a href="/static/templates/uploadSendList_template.xlsx" download>엑셀양식 다운로드</a></button>
                                </div>
                            </th>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="save" onclick="excelSendListPop.save();">등록</button>
            <button class="ui button xlarge white font-malgun" id="cancel" onclick="excelSendListPop.cancel();">취소</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/excelSendListPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script>
    let callBackScript = "${callBackScript}";
</script>