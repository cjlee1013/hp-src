<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup medium">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" id="popTitle">일괄등록 결과</h2>
        <ul class="mg-t-10">
        </ul>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption>일괄등록 결과</caption>
            <colgroup>
                <col width="70px">
                <col width="20%">
                <col width="70px">
                <col width="20%">
                <col width="70px">
                <col width="20%">
            </colgroup>
            <tbody>
                <tr>
                    <th>등록</th>
                    <td id="uploadCount">${uploadCount}</td>
                    <th>성공</th>
                    <td id="successCount">${successCount}</td>
                    <th>실패</th>
                    <td id="failCount">${failCount}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <c:choose>
        <c:when test="${succssYn ne 'Y'}">
            <div class="mg-t-10 mg-b-10">
                <h2>등록에 실패하였습니다.</h2>
                <h2>업로드할 엑셀파일에 오류가 없는지 확인하시고 다시 한 번 등록해주세요.</h2>
            </div>
        </c:when>
        <c:otherwise>등록 성공하였습니다.</c:otherwise>
    </c:choose>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script>
    var callBackScript = "${callBackScript}";
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
