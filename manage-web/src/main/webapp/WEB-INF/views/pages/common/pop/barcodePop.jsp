<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<c:choose>
    <c:when test="${setYn eq 'Y'}">
        <c:set var="width" value="37%" />
        <c:set var="display" value="inline-block" />
        <c:set var="closeTxt" value="취소" />
    </c:when>
    <c:otherwise>
        <c:set var="width" value="45%" />
        <c:set var="display" value="none" />
        <c:set var="closeTxt" value="확인" />
    </c:otherwise>
</c:choose>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">쿠폰바코드 조회</h2>
    </div>
    <form id="barcodePopForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal">
                <table class="ui table">
                    <caption>행사번호 검색</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="gridSearchType" name="gridSearchType" class="ui input medium mg-r-10" style="width: 120px; ">
                                    <option value="couponCd" selected>쿠폰바코드</option>
                                    <option value="trgtSeq">행사일련번호</option>
                                </select>
                                <input type="text" id="gridSearchValue" name="applyGridSearchValue" class="ui input medium mg-r-5" style="width: 250px;">
                                <button type="button" id="gridSearchBtn" class="ui button medium cyan" style="margin-right : 5px">검색</button>
                                <button type="button" id="gridSearchResetBtn" class="ui button medium">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
    </form>
    <!-- 리스트 그리드 -->
    <div class="com wrap-title sub" style="display: none;">
        <h3 class="title">• 검색결과: <span id="searchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="barcodePopGrid" style="width: 100%; height: 250px;"></div>
    <!-- 리스트 그리드 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn" style="display: ${display};">확인</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">${closeTxt}</button>
        </span>
    </div>
</div>

<script>
    // 카드 코드 조회 그리드 정보
    ${barcodePopGridBaseInfo}

    var incUnitCdJson   = ${incUnitCdJson};
    var setYn           = "${setYn}";
    var callBackScript  = '${callBackScript}';
    var multiYn         = "${multiYn}";
    var storeType       = "${storeType}";
    var getBarcode      = '${getBarcode}';
    var allBarcodeList  = '${couponBarcodeList}';

</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/barcodePop.js?v=${fileVersion}"></script>