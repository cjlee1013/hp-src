<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">판매업체조회</h2>
    </div>
    <form id="partnerPopForm" onsubmit="return false;">
        <input type="hidden" id="schPartnerType" name="schPartnerType" value="${partnerType}">
        <div class="ui wrap-table horizontal mg-t-1 0">
                <table class="ui table">
                    <caption>판매자 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="20%">
                        <col width="7%">
                        <col width="20%">
                        <col width="7%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schRegStartDate" name="schRegStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schRegEndDate" name="schRegEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사업자유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schOperatorType" style="width: 180px" name="schOperatorType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${operatorType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="businessNm">판매업체명</option>
                                    <option value="partnerId">판매업체ID</option>
                                    <option value="partnerNo">사업자등록번호</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </form>

    <!-- 판매업체 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="partnerPopSearchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="partnerPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 판매업체 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript  = "${callBackScript}";
    var isMulti         = "${isMulti}";
    var partnerId       = "${partnerId}";
    var partnerType     = "${partnerType}";
    var mallType        = "${mallType}";

    // 판매업체 조회 그리드 정보
    ${partnerPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/partnerPop.js?v=${fileVersion}"></script>