<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">점포조회</h2>
    </div>
    <form id="storePopForm" onsubmit="return false;">
        <input type="hidden" name="storeRange" id="storeRange" value="${storeRange}" >
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>점포조회</caption>
                <colgroup>
                    <col width="250px">
                    <col width="*">
                    <col width="200px">
                </colgroup>
                <tbody>
                <tr>
                    <th>점포유형</th>
                    <td>
                        <div class="ui form inline">
                            <select id="searchStoreType" style="width: 180px" name="searchStoreType" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:choose>
                                        <c:when test="${storeTypeParam ne ''}">
                                            <c:if test="${codeDto.mcCd eq storeTypeParam}">
                                                <option value="${codeDto.mcCd}" selected>${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${codeDto.ref3 eq 'REAL'}">
                                                <c:if test="${codeDto.ref2 eq 'df'}">
                                                    <c:set var="selected" value="selected" />
                                                </c:if>
                                                <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <!-- 검색 버튼 -->
                    <td rowspan="3">
                        <div style="text-align: center;" class="ui form mg-t-15">
                            <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                            <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            <!-- button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/-->
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                <option value="STORENM">점포명</option>
                                <option value="STOREID">점포코드</option>
                            </select>
                            <input type="text" id="searchValue" name="searchValue" class="ui input medium mg-r-5" style="width: 250px;" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 점포 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="storePopSearchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="storePopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 점포 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript      = "${callBackScript}";
    // 점포 조회 그리드 정보
    ${storePopGridBaseInfo}

</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/storePop.js?v=${fileVersion}"></script>