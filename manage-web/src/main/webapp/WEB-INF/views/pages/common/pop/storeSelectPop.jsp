<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<c:choose>
    <c:when test="${setYn eq 'Y'}">
        <c:set var="width" value="40%" />
        <c:set var="display" value="inline-block" />
        <c:set var="closeTxt" value="취소" />
    </c:when>
    <c:otherwise>
        <c:set var="width" value="45%" />
        <c:set var="display" value="none" />
        <c:set var="closeTxt" value="확인" />
    </c:otherwise>
</c:choose>
<div class="com wrap-popup large popup-request">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            <i class="fa fa-clone mg-r-5"></i><span id="storeTypeTitle">${storeTypeTitle}</span> 점포설정
        </h3>
            <span class="pull-right"
            <c:if test="${storeTypeRadioYn eq 'N'}">
                style="display: none;"
            </c:if>
            >
            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                <c:set var="checked" value="" />
                <c:if test="${codeDto.ref2 eq 'df'}">
                    <c:set var="checked" value="checked" />
                </c:if>
                <c:if test="${codeDto.ref1 eq 'hmp'}">
                    <label class="ui radio inline" style="width:35px;"><input type="radio" name="storeType" class="ui input medium" data-store-type-nm="${codeDto.mcNm}" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                </c:if>
            </c:forEach>
            </span>
    </div>
    <div class="tabui-wrap pd-t-20">
        <div class="tabui medium col-02">
            <div class="mg-b-20" style="height: 320px;">

                <div class="pull-left" id="prStoreListSetGrid" style="width: ${width}; height: 300px;">
                    <div class="mg-b-5 chkStoreAttr" <c:if test="${storeTypePop eq 'EXP'}">style="display: none;" </c:if>>
                    <label class="ui checkbox mg-r-10" style="display: inline-block;"><input type="checkbox" id="prNorCheck" class="prStoreCheck" value="NOR"><span class="text">일반점만 선택</span></label>
                    <label class="ui checkbox mg-r-10" style="display: inline-block;"><input type="checkbox" id="prSpcCheck" class="prStoreCheck" value="SPC"><span class="text">스페셜만 선택</span></label>
                    </div>
                </div>

                <div class="pull-left" style="margin-top: 112px;margin-left: 50px;display: ${display};">
                    <button type="button" class="ui button medium " style="height: 40px;" onclick="storePop.deletePrStoreList();">▶ <br />제거</button><br />
                    <button type="button" class="ui button medium mg-t-5" style="height: 40px;" onclick="storePop.addPrStoreList();">◀ <br />추가</button>
                </div>

                <div class="pull-right" id="storeListGrid" style="width: ${width}; height: 300px;">
                    <div class="mg-b-5 chkStoreAttr" <c:if test="${storeTypePop eq 'EXP'}">style="display: none;" </c:if>>
                        <label class="ui checkbox mg-r-10" style="display: inline-block;"><input type="checkbox" id="norCheck" class="storeCheck" value="NOR"><span class="text">일반점만 선택</span></label>
                        <label class="ui checkbox mg-r-10" style="display: inline-block;"><input type="checkbox" id="spcCheck" class="storeCheck" value="SPC"><span class="text">스페셜만 선택</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui form inline mg-t-15 text-center">
        <button class="ui button large cyan mg-r-5" style="display: ${display};" onclick="storePop.save();">저장</button>
        <button class="ui button large" onclick="storePop.close();">${closeTxt}</button>
    </div>
</div>

<script>
    ${storeListGridBaseInfo}

    ${storeListSetGridBaseInfo}
    let getStoreList = '${storeList}';	// 파라미터 적용점포
    let allStoreList = '${allStoreList}';
    let callBackScript  = "${callBackScript}";

    var storeListGrid = {
        selectedRowId : null,
        gridView : new RealGridJS.GridView("storeListGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            storeListGrid.initGrid();
            storeListGrid.initDataProvider();
            storeListGrid.event();
        },
        initGrid : function() {
            storeListGrid.gridView.setDataSource(storeListGrid.dataProvider);

            storeListGrid.gridView.setStyles(storeListGridBaseInfo.realgrid.styles);
            storeListGrid.gridView.setDisplayOptions(storeListGridBaseInfo.realgrid.displayOptions);
            storeListGrid.gridView.setColumns(storeListGridBaseInfo.realgrid.columns);
            storeListGrid.gridView.setOptions(storeListGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            storeListGrid.dataProvider.setFields(storeListGridBaseInfo.dataProvider.fields);
            storeListGrid.dataProvider.setOptions(storeListGridBaseInfo.dataProvider.options);
        },
        setData : function(dataList) {
            storeListGrid.dataProvider.clearRows();
            storeListGrid.dataProvider.setRows(dataList);
        },
        event : function () {
            storeListGrid.gridView.onItemChecked = function (grid, itemIndex, checked) {
                let checkedRows = storeListGrid.gridView.getCheckedRows(true);
                if (checkedRows.length == 0) {
                    $('.storeCheck').prop('checked', false);
                }
            };
        }
        
    };

    var prStoreListSetGrid = {
        selectedRowId : null,
        gridView : new RealGridJS.GridView("prStoreListSetGrid"),
        dataProvider : new RealGridJS.LocalDataProvider(),
        init : function() {
            prStoreListSetGrid.initGrid();
            prStoreListSetGrid.initDataProvider();
            prStoreListSetGrid.event();
            let storeNmColumn = prStoreListSetGrid.gridView.columnByName("storeNm");
            storeNmColumn.header.text = '적용 점포';
            prStoreListSetGrid.gridView.setColumn(storeNmColumn);
        },
        initGrid : function() {
            prStoreListSetGrid.gridView.setDataSource(prStoreListSetGrid.dataProvider);

            prStoreListSetGrid.gridView.setStyles(storeListSetGridBaseInfo.realgrid.styles);
            prStoreListSetGrid.gridView.setDisplayOptions(storeListSetGridBaseInfo.realgrid.displayOptions);
            prStoreListSetGrid.gridView.setColumns(storeListSetGridBaseInfo.realgrid.columns);
            prStoreListSetGrid.gridView.setOptions(storeListSetGridBaseInfo.realgrid.options);
        },
        initDataProvider : function() {
            prStoreListSetGrid.dataProvider.setFields(storeListSetGridBaseInfo.dataProvider.fields);
            prStoreListSetGrid.dataProvider.setOptions(storeListSetGridBaseInfo.dataProvider.options);
        },
        setData : function(dataList) {
            prStoreListSetGrid.dataProvider.clearRows();
            prStoreListSetGrid.dataProvider.setRows(dataList);
        },
        event : function () {
            prStoreListSetGrid.gridView.onItemChecked = function (grid, itemIndex, checked) {
                let checkedRows = prStoreListSetGrid.gridView.getCheckedRows(true);
                if (checkedRows.length == 0) {
                    $('.prStoreCheck').prop('checked', false);
                }
            };
        }
    };

    $(document).ready(function() {
        resizeTo(830, 500)
        storeListGrid.init();
        prStoreListSetGrid.init();
    });

    let storePop = {
        init : function() {
            this.getPrStoreList();
            this.event();
        },
        event : function() {
            $('input[name="storeType"]').on('change', function() {

                CommonAjax.basic({
                    url: '/common/getStoreSelectPopList.json',
                    data: {storeType: $('input[name="storeType"]:checked').val()},
                    method: 'GET',
                    callbackFunc: function (data) {
                        $('input[name="storeType"]:checked').val() == 'HYPER' ? $('.chkStoreAttr').show() : $('.chkStoreAttr').hide();
                        $('#storeTypeTitle').html($('input[name="storeType"]:checked').data('store-type-nm'));
                        storePop.getPrStoreList(data);
                    }
                });
            });

            $('.prStoreCheck').on('click', function() {
                let value = $(this).val();
                if ($(this).is(':checked')) {
                    storePop.applyCheckRow(prStoreListSetGrid, value, true);
                }else {
                    storePop.applyCheckRow(prStoreListSetGrid, value, false);
                }
            });

            $('.storeCheck').on('click', function() {
                let value = $(this).val();
                if ($(this).is(':checked')) {
                    storePop.applyCheckRow( storeListGrid, value, true);
                }else {
                    storePop.applyCheckRow(storeListGrid, value, false)
                }
            });
        },
        /**
         * 점포 구분 체크 처리
         * @param grid
         * @param value
         * @param checked
         */
        applyCheckRow : function (grid, value, checked) {
            if(!$.jUtil.isEmpty(value)) {
                let gridList = grid.dataProvider.getJsonRows();
                gridList.forEach(function (item, index) {
                    if (item.storeAttr == value) {
                        grid.gridView.checkRow(index, checked);
                    }
                });
            }
        },
        getPrStoreList : function(_allStoreList) {
            if (_allStoreList) {
                allStoreList = _allStoreList;
            } else {
                allStoreList = JSON.parse(allStoreList);
            }

            const uniqueAllStoreList = allStoreList.reduce((prev, now) => {
                if (!prev.some(obj => obj.storeNm === now.storeNm )) {
                    prev.push(now);
                }
                return prev;
            }, []);


            if (getStoreList != "" && !_allStoreList) {
                getStoreList = JSON.parse(getStoreList);

                let exclStoreList = allStoreList;
                let _getStoreList = new Array();

                getStoreList.some(existingItem => {
                    let result = exclStoreList.filter(item => {
                        if (existingItem.storeId != item.storeId) {
                            return item;
                        }else {
                            _getStoreList.push(item);
                        }
                    });
                    exclStoreList = result;
                });

                storeListGrid.setData(exclStoreList);
                prStoreListSetGrid.setData(_getStoreList);
            } else {
                prStoreListSetGrid.setData(allStoreList);
            }

            //필터링 옵션 설정
            storeListGrid.gridView.setFilteringOptions({
                clearWhenSearchCheck:true,
                selector: {
                    showSearchInput: true,
                    showButtons:true,
                    acceptText: "확인",
                    cancelText: "취소"
                }
            });

            prStoreListSetGrid.gridView.setFilteringOptions({
                clearWhenSearchCheck:true,
                selector: {
                    showSearchInput: true,
                    showButtons:true,
                    acceptText: "확인",
                    cancelText: "취소"
                }
            });

            //데이터로 필터 설정
            let filters = [];
            for (let i in uniqueAllStoreList) {

                filters.push({name:uniqueAllStoreList[i].storeNm, criteria:"value = " + "'" + uniqueAllStoreList[i].storeNm + "'"});
            }

            storeListGrid.gridView.setColumnFilters("storeNm",filters);
            prStoreListSetGrid.gridView.setColumnFilters("storeNm",filters);

        },
        addPrStoreList : function() {
            let rowArr = storeListGrid.gridView.getCheckedRows(false);

            if (rowArr.length == 0) {
                alert('추가하려는 점포를 선택해주세요.');
                return false;
            }

            for (let i in rowArr) {
                prStoreListSetGrid.dataProvider.addRow(storeListGrid.dataProvider.getJsonRow(rowArr[i]));
            }

            for (let i in rowArr.reverse()) {
                storeListGrid.dataProvider.removeRow(rowArr[i]);
            }
        },
        deletePrStoreList : function() {
            let rowArr = prStoreListSetGrid.gridView.getCheckedRows(false);

            if (rowArr.length == 0) {
                alert('제거하려는 점포를 선택해주세요.');
                return false;
            }

            for (let i in rowArr) {
                storeListGrid.dataProvider.addRow(prStoreListSetGrid.dataProvider.getJsonRow(rowArr[i]));
            }

            for (let i in rowArr.reverse()) {
                prStoreListSetGrid.dataProvider.removeRow(rowArr[i]);
            }
        },
        save : function() {
            if (storePop.validSave()) {
                eval("opener." + callBackScript + "(storePop.getStoreData());");
                storePop.close();
            }
        },
        validSave : function() {

            return true;
        },
        getStoreData : function() {
            let res = {
                storeType : $('input[name="storeType"]:checked').val(),
                storeList : prStoreListSetGrid.dataProvider.getJsonRows()
            };

            return res;
        },
        close : function() {
            window.close();
        }
    };

    $(document).ready(function() {
        storePop.init();
    });
</script>