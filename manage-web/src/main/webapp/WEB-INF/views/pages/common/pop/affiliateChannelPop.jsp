<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="affiliateChannelPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 id="titlePop" class="title font-malgun">제휴채널 조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="150px">
                    <col width="700px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>사이트구분</th>
                    <td >
                        <div class="ui form inline">
                            <select id="searchSiteType" style="width: 150px" name="searchSiteType" class="ui input medium" >
                                <c:forEach var="codeDto" items="${dskSiteGugun}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <td rowspan="2">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 150px;float: left;">
                                <c:forEach var="codeDto" items="${affliSearchType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 ne 'POP'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <div class="ui form inline" style="float: left">
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 300px;" >
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 쿠폰 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="affiliateChannelCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="affiliateChannelPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>



<script>
    var callBackScript  = "${callBackScript}";
    var isMulti         = "${isMulti}";

    ${affiliateChannelPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/affiliateChannelPop.js?v=${fileVersion}"></script>