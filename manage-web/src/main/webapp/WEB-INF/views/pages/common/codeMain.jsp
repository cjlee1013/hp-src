<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp"/>
<div class="">
    <form id="codeSchFrom" name="codeSchFrom" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">공통코드관리</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>공통코드 검색</caption>
                <colgroup>
                    <col width="10%">
                    <col width="60%">
                    <col width="30%">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id='schType' style="width: 180px" name='schType'
                                    class="ui input medium mg-r-10">
                                <option value="gmcNm">유형명</option>
                                <option value="gmcCd">유형코드</option>
                            </select>
                            <input type="text" id="schValue" name="schValue"
                                   class="ui input medium mg-r-5" style="width: 350px;">
                        </div>
                    </td>
                    <td>
                        <!-- 우측 검색 버튼 -->
                        <div class="ui form inline">
                            <button type="subject" class="ui button large mg-r-5 cyan pull-left"
                                    id="codeSch"><i class="fa fa-search"></i> 검색
                            </button>
                            <span style="margin-left:4px;"></span>
                            <button type="button" class="ui button large mg-r-5 white pull-left"
                                    id="codeInit">초기화
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="com wrap-title">
        <!-- 좌측 상세 정보 -->
        <div id="section1" style="float: left; width:19%;">
            <!-- 유형목록 -->
            <div class="com wrap-title sub">
                <h3 class="title">유형목록 : <span id="codeGCnt">0</span>건</h3>
            </div>
            <div class="topline"></div>
            <div id="codeGGrid" style="background-color:white;width:100%;height:650px;"></div>
            <!-- 유형목록 -->
            <form id="codeGForm" name="codeGForm" onsubmit="return false;">
                <div class="ui wrap-table horizontal mg-t-10">
                    <table class="ui table">
                        <caption>유형입력</caption>
                        <colgroup>
                            <col width="49%">
                            <col width="49%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th><span class="text-red">*</span> 유형코드</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="typeCode" name="gmcCd"
                                           class="ui input medium mg-r-5" maxlength="20">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><span class="text-red">*</span> 유형명</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="typeCodeName" name="gmcNm"
                                           class="ui input medium mg-r-5" maxlength="100">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>이전 시스템 상위코드</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="lgcUpcodeCdG" name="lgcUpcodeCd"
                                           class="ui input medium mg-r-5" maxlength="5">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="ui right-button-group mg-t-15">
				<span class="inner">
					<button type="button" class="ui button medium white font-malgun"
                            onclick="javascript:codeMng.codeGInit();">초기화</button>
					<button type="button" class="ui button medium gray-dark font-malgun"
                            onclick="javascript:codeMng.setCodeG();">등록/수정</button>
				</span>
                </div>
            </form>
        </div>
        <!-- 좌측 상세 정보 -->
        <!-- 우측 상세 정보 -->
        <div id="section2" style="float: right; width: 79%;">
            <!-- 코드목록 -->
            <div class="com wrap-title sub">
                <h3 class="title">코드목록 : <span id="codeDCnt">0</span>건</h3>
            </div>
            <div class="topline"></div>
            <div id="codeDGrid" style="background-color:white;width:100%;height:650px;"></div>

            <form id="codeDForm" name="codeDForm" onsubmit="return false;">
                <div class="ui wrap-table horizontal mg-t-10">
                    <table class="ui table">
                        <caption>코드입력</caption>
                        <colgroup>
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                            <col width="12%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <th><span class="text-red">*</span> 코드</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="code" name="mcCd"
                                           class="ui input medium mg-r-5" maxlength="20">
                                </div>
                            </td>
                            <th><span class="text-red">*</span> 코드명</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="codeName" name="mcNm"
                                           class="ui input medium mg-r-5" maxlength="100">
                                </div>
                            </td>
                            <th><span class="text-red">*</span> 우선순위</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="priority" name="priority"
                                           class="ui input medium mg-r-5" maxlength="5">
                                </div>
                            </td>
                            <th>사용여부</th>
                            <td>
                                <div class="ui form inline">
                                    <select id='useYn' style="width: 80px" name='useYn'
                                            class="ui input medium mg-r-10">
                                        <option value="Y">사용</option>
                                        <option value="N">미사용</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>참조1</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="reference1" name="ref1"
                                           class="ui input medium mg-r-5" maxlength="45">
                                </div>
                            </td>
                            <th>참조2</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="reference2" name="ref2"
                                           class="ui input medium mg-r-5" maxlength="45">
                                </div>
                            </td>
                            <th>참조3</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="reference3" name="ref3"
                                           class="ui input medium mg-r-5" maxlength="45">
                                </div>
                            </td>
                            <th>참조4</th>
                            <td>
                                <div class="ui form inline">
                                    <input type="text" id="reference4" name="ref4"
                                           class="ui input medium mg-r-5" maxlength="45">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>이전 시스템 상세코드</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <input type="text" id="lgcCodeCd" name="lgcCodeCd"
                                           class="ui input medium mg-r-5" maxlength="5">
                                </div>
                            </td>
                            <th>이전 시스템 상위코드</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <input type="text" id="lgcUpcodeCdD" name="lgcUpcodeCd"
                                           class="ui input medium mg-r-5" maxlength="5">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="ui right-button-group mg-t-15">
				<span class="inner">
					<button type="button" class="ui button medium white font-malgun"
                            onclick="javascript:codeMng.codeDInit();">초기화</button>
					<button type="button" class="ui button medium gray-dark font-malgun"
                            onclick="javascript:codeMng.setCodeD();">등록/수정</button>
				</span>
                </div>
            </form>
            <!-- 코드목록 -->
        </div>
        <!-- 우측 상세 정보 -->
    </div>
</div>

<script type="text/javascript">
    var codeGGrid = {
        gridView: new RealGridJS.GridView("codeGGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            codeGGrid.initGrid();
            codeGGrid.initDataProvider();
            codeGGrid.event();
        },
        initGrid: function () {
            codeGGrid.gridView.setDataSource(codeGGrid.dataProvider);
            codeGGrid.gridView.setStyles(codeGGridBaseInfo.realgrid.styles);
            codeGGrid.gridView.setDisplayOptions(codeGGridBaseInfo.realgrid.displayOptions);
            codeGGrid.gridView.setColumns(codeGGridBaseInfo.realgrid.columns);
            codeGGrid.gridView.setOptions(codeGGridBaseInfo.realgrid.options);
        },
        initDataProvider: function () {
            codeGGrid.dataProvider.setFields(codeGGridBaseInfo.dataProvider.fields);
            codeGGrid.dataProvider.setOptions(codeGGridBaseInfo.dataProvider.options);
        },
        event: function () {

            codeGGrid.gridView.onDataCellClicked = function (gridView, index) {
                var data = codeGGrid.dataProvider.getJsonRow(index.dataRow);
                codeMng.codeGSelect(data);
            };
        },
        setData: function (dataList) {
            codeGGrid.dataProvider.clearRows();
            codeGGrid.dataProvider.setRows(dataList);
        }
    };

    var codeDGrid = {
        gridView: new RealGridJS.GridView("codeDGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            codeDGrid.initGrid();
            codeDGrid.initDataProvider();
            codeDGrid.event();
        },
        initGrid: function () {
            codeDGrid.gridView.setDataSource(codeDGrid.dataProvider);
            codeDGrid.gridView.setStyles(codeDGridBaseInfo.realgrid.styles);
            codeDGrid.gridView.setDisplayOptions(codeDGridBaseInfo.realgrid.displayOptions);
            codeDGrid.gridView.setColumns(codeDGridBaseInfo.realgrid.columns);
            codeDGrid.gridView.setOptions(codeDGridBaseInfo.realgrid.options);
        },
        initDataProvider: function () {
            codeDGrid.dataProvider.setFields(codeDGridBaseInfo.dataProvider.fields);
            codeDGrid.dataProvider.setOptions(codeDGridBaseInfo.dataProvider.options);
        },
        event: function () {

            codeDGrid.gridView.onDataCellClicked = function (gridView, index) {
                var data = codeDGrid.dataProvider.getJsonRow(index.dataRow);
                codeMng.codeDSelect(data);
            };
        },
        setData: function (dataList) {
            codeDGrid.dataProvider.clearRows();
            codeDGrid.dataProvider.setRows(dataList);
        }
    };

    ${codeGGridBaseInfo}
    ${codeDGridBaseInfo}

    $(document).ready(function () {
        codeGGrid.init();
        codeDGrid.init();
    });

    codeMng = {
        init: function () {

        },
        event: function () {

            $('#codeSch').click(function () {
                codeMng.search();
            });
            $('#codeInit').click(function () {
                codeMng.searchInit();
            });
        },
        searchInit: function (formInitYn) {
            if (typeof formInitYn == "undefined" || formInitYn != 'N') {
                $('#codeSchFrom').each(function () {
                    this.reset();
                });
            }
            codeMng.codeGInit();
            codeMng.codeDInit();
            codeGGrid.dataProvider.clearRows();
            $('#codeGCnt').html('0');
            codeDGrid.dataProvider.clearRows();
            $('#codeDCnt').html('0');
        },
        codeGInit: function () {

            $('#codeGForm').each(function () {
                this.reset();
            });

            $('#typeCode').attr("readonly", false);
        },
        codeDInit: function () {

            $('#codeDForm').each(function () {
                this.reset();
            });

            $('#code').attr("readonly", false);
        },
        search: function () {

            codeMng.searchInit("N");

            var infoData = $('form[name=codeSchFrom]').serialize();

            CommonAjax.basic({
                url: '/common/code/getTypeCodeList.json?' + infoData,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    codeGGrid.setData(res);
                    $('#codeGCnt').html($.jUtil.comma(codeGGrid.gridView.getItemCount()));
                }
            });

        },
        codeGSelect: function (data) {

            codeMng.codeDInit();
            $('#codeDCnt').html('0');

            var typeCode = data.gmcCd;

            $('#typeCode').val(typeCode).attr("readonly", true);
            $('#typeCodeName').val(data.gmcNm);
            $('#lgcUpcodeCdG').val(data.lgcUpcodeCd);

            var infoData = 'typeCode=' + typeCode;

            CommonAjax.basic({
                url: '/common/code/getCodeList.json?' + infoData,
                data: null,
                method: "GET",
                successMsg: null,
                callbackFunc: function (res) {
                    codeDGrid.setData(res);
                    $('#codeDCnt').html($.jUtil.comma(codeDGrid.gridView.getItemCount()));
                }
            });
        },
        codeDSelect: function (data) {

            $('#code').val(data.mcCd).attr("readonly", true);
            $('#codeName').val(data.mcNm);
            $('#reference1').val(data.ref1);
            $('#reference2').val(data.ref2);
            $('#reference3').val(data.ref3);
            $('#reference4').val(data.ref4);
            $('#priority').val(data.priority);
            $('#useYn').val(data.useYn);
            $('#lgcCodeCd').val(data.lgcCodeCd);
            $('#lgcUpcodeCdD').val(data.lgcUpcodeCd);
        },
        setCodeG: function () {

            if (codeMng.validate(codeMng.validateMObj)) {
                var params = $('#codeGForm').serialize();

                CommonAjax.basic({
                    url: '/common/code/setTypeCode.json',
                    data: params,
                    method: 'POST',
                    callbackFunc: function (res) {
                        var msg = '등록';
                        if (res.returnCnt > 1) {
                            msg = '수정';
                        }
                        alert(msg + ' 되었습니다.');
                        codeMng.codeGInit();
                        codeMng.search();
                    }
                });
            }
        },
        setCodeD: function () {

            if (codeMng.validate(codeMng.validateDObj)) {
                var selectedTypeCode = codeGGrid.dataProvider.getJsonRow(
                        codeGGrid.gridView.getSelectedRows()).gmcCd;

                var params = $('#codeDForm').serialize() + "&gmcCd=" + selectedTypeCode;

                CommonAjax.basic({
                    url: '/common/code/setCode.json',
                    data: params,
                    method: 'POST',
                    callbackFunc: function (res) {
                        var msg = '등록';
                        if (res.returnCnt > 1) {
                            msg = '수정';
                        }

                        alert(msg + ' 되었습니다.');
                        codeMng.codeGSelect(codeGGrid.dataProvider.getJsonRow(
                                codeGGrid.gridView.getSelectedRows()));
                    }
                });
            }
        },
        validateMObj: {
            "#typeCode": {"msg": "유형코드를 입력해주십시오."},
            "#typeCodeName": {"msg": "유형명을 입력해주십시오."}
        },
        validateDObj: {
            // "codeGGrid.getSelectedRowId()"	:	{"msg" : "유형을 선택해주십시오.", "isEvalVal" : true },
            "#code": {"msg": "코드를 입력해주십시오."},
            "#codeName": {"msg": "코드명을 입력해주십시오."},
            "input[name=priority]": {"msg": "우선순위를 입력해주십시오."},
            "#priority": {"msg": "숫자를 입력해주십시오.", "test": /^[0-9]*$/},
        },
        validate: function (validateObj) {
			var checkValidate = true;

			for (var id in validateObj) {
				if (validateObj[id].hasOwnProperty("isEvalVal")) {
					var checkVal = eval(id);
					if (checkVal == "" || checkVal == null) {
						checkValidate = validateObj[id].msg;
						break;
					}
				} else if (validateObj[id].hasOwnProperty("test")) {
					var num_check = validateObj[id].test;
					if (!num_check.test($(id).val())) {
						checkValidate = validateObj[id].msg;
						break;
					}
				} else if ($(id).val() == "") {
					checkValidate = validateObj[id].msg;
					break;
				}
			}

			if (checkValidate === true) {
				return true;
			} else {
				alert(checkValidate);
				return false;
			}
		}
    };

    $(document).ready(function () {
        codeMng.init();
        codeMng.event();
    });

</script>