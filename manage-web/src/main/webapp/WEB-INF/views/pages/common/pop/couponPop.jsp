<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="couponPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">쿠폰조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>쿠폰 검색</caption>
                <colgroup>
                    <col width="10%">
                    <col width="30%">
                    <col width="10%">
                    <col width="30%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>발급기간</th>
                    <td colspan="3">
                        <div style="float:left" class="ui form inline">
                            <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                        </div>
                    </td>
                    <td rowspan="4">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>점포유형</th>
                    <td>
                        <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 100px;">
                            <option value="">전체</option>
                            <c:forEach var="storeTypeCd" items="${storeTypeCd}" varStatus="status">
                                <option value="${storeTypeCd.mcCd}">${storeTypeCd.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>쿠폰상태</th>
                    <td>
                        <select id="schCouponStatus" name="schCouponStatus" class="ui input medium" style="width: 120px;">
                            <option value="" selected>전체</option>
                            <option value="READY">발급대기</option>
                            <option value="ISSUED">발급중</option>
                            <option value="STOP">발급중단</option>
                            <option value="COMP">발급종료</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 120px;">
                                <option value="COUPONNO" selected>쿠폰번호</option>
                                <option value="COUPONNM">관리쿠폰명</option>
                            </select>
                            <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 쿠폰 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="couponCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="couponPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    let callBackScript  = "${callBackScript}";
    let isMulti         = "${isMulti}";
    let storeType       = "${storeType}";
    let couponNo        = "${couponNo}";
    let storeTypeJson   = ${storeTypeJson};
    let couponTypeJson  = ${couponTypeJson};
    let couponStatusJson= ${couponStatusJson};

    // 쿠폰 조회 그리드 정보
    ${couponPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/couponPop.js?v=${fileVersion}"></script>