<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="itemPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">카테고리조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>상품 검색</caption>
                <colgroup>
                    <col width="120px">
                    <col width="*">
                    <col width="100px">
                </colgroup>
                <tbody>
                <tr>
                    <th>카테고리</th>
                    <td>
                        <select id="schCateCd1" name="schLcateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="대분류" onchange="javascript:commonCategory.changeCategorySelectBox(1,'schCateCd');"></select>
                        <select id="schCateCd2" name="schMcateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="중분류" onchange="javascript:commonCategory.changeCategorySelectBox(2,'schCateCd');"></select>
                        <select id="schCateCd3" name="schScateCd" class="ui input medium mg-r-5" style="width:170px;float:left;" data-default="소분류" onchange="javascript:commonCategory.changeCategorySelectBox(3,'schCateCd');"></select>
                        <select id="schCateCd4" name="schDcateCd" class="ui input medium mg-r-5" style="width:170px;" data-default="세분류"></select>
                    </td>
                    <td rowspan="4">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>거래유형</th>
                    <td>
                        <c:forEach var="code" items="${itemMallType}" varStatus="status">
                            <label class="ui radio inline">
                                <input type="radio" id="schMallType${code.mcCd}" name="schMallType" class="ui input medium" value="${code.mcCd}"><span>${code.mcNm}</span>
                            </label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <th>판매업체</th>
                    <td>
                        <div class="ui form inline">
                            <select id="schPartnerType" name="schPartnerType" class="ui input medium mg-r-5" style="width: 120px;">
                                <option value="BUSINESSNM">판매업체명</option>
                                <option value="PARTNERID">판매업체ID</option>
                                <option value="PARTNERNO">사업자등록번호</option>
                            </select>
                            <input type="text" id="schPartnerValue" name="schPartnerValue" class="ui input medium mg-r-5" style="width: 150px;" minlength="2" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 120px;">
                                <option value="ITEMNM">상품명</option>
                                <option value="ITEMNO">상품번호</option>
                                <option value="REGNM">등록자</option>
                            </select>
                            <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 상품 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="itemPopSearchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="itemPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript  = "${callBackScript}";
    var isMulti         = "${isMulti}";
    var partnerId       = "${partnerId}";
    var itemType        = "${itemType}";
    var mallType        = "${mallType}";
    var storeType       = "${storeType}";

    // 상품 조회 그리드 정보
    ${itemPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/itemPop.js?v=${fileVersion}"></script>