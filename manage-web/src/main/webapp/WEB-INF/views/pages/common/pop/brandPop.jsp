<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">브랜드 조회 </h2>
        <span class="text">* 브랜드는 복수 등록이 불가능 합니다. </span>
    </div>
    <form id="brandPopForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>브랜드 검색</caption>
                <colgroup>
                    <col width="150px">
                    <col width="*">
                    <col width="200px">
                </colgroup>
                <tbody>
                <tr>
                    <th>등록일</th>
                    <td colspan="5">
                        <div class="ui form inline">
                            <input type="text" id="schRegStartDate" name="schRegStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="schRegEndDate" name="schRegEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                    <!-- 검색 버튼 -->
                    <td rowspan="3">
                        <div style="text-align: center;" class="ui form mg-t-15">
                            <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                            <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>검색어<span class="text-red"> *</span></th>
                    <td>
                        <div class="ui form inline">
                            <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 180px">
                                <option value="brandNm">브랜드명</option>
                                <option value="brandNo">브랜드코드</option>
                                <option value="regNm">등록자</option>
                            </select>
                            <input type="text" id="searchValue" name="searchValue" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 점포 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="brandPopSearchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="brandPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 점포 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript      = "${callBackScript}";
    // 점포 조회 그리드 정보
    ${brandPopGridBaseInfo}

</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/brandPop.js?v=${fileVersion}"></script>