<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">회원 조회</h2>
    </div>
    <form id="memberPopForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal mg-t-1 0">
                <table class="ui table">
                    <caption>회원 조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="35%">
                        <col width="10%">
                        <col width="35%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>회원번호</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="userNo" name="userNo" class="ui input medium mg-r-5" style="width: 245px;" >
                            </div>
                        </td>
                        <th>회원명</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="userNm" name="userNm" class="ui input medium mg-r-5" style="width: 245px;" minlength="2" >
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="initBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>회원ID</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="userId" name="userId" class="ui input medium mg-r-5" style="width: 245px;" minlength="2" >
                            </div>
                        </td>
                        <th>휴대폰</th>
                        <td>
                            <div class="ui form inline">
                                <span class="text">
                                    <input type="text" id="mobile1" name="mobile1" class="ui input medium mg-r-5" style="width: 73px;" maxlength="3">&#45;
                                    <input type="text" id="mobile2" name="mobile2" class="ui input medium mg-r-5" style="width: 73px;" maxlength="4">&#45;
                                    <input type="text" id="mobile3" name="mobile3" class="ui input medium mg-r-5" style="width: 73px;" maxlength="4">
                                </span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </form>

    <!-- 판매업체 그리드 -->
    <div class="com wrap-title sub">
<%--        <h3 class="title">• 검색결과: <span id="partnerPopSearchCnt">0</span>건</h3>--%>
    </div>
    <div class="topline"></div>
    <div id="memberSearchGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 판매업체 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">확인</button>
            <button class="ui button xlarge font-malgun" id="closeBtn">취소</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/memberSearchPop.js?v=${fileVersion}"></script>
<script>
    let callBackScript = "${callBackScript}";
    let isIncludeUnmasking = ${isIncludeUnmasking};

    ${memberSearchGridBaseInfo}

    $(document).ready(function() {
        memberSearchMain.init();
        memberSearchGrid.init();

        CommonAjaxBlockUI.global();
    });
</script>
