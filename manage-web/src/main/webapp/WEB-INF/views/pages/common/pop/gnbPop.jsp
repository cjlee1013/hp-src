<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="gnbPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 id="titlePop" class="title font-malgun">GNB 조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="35%">
                    <col width="10%">
                    <col width="35%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>전시기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                        </div>
                    </td>
                    <td rowspan="2">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사이트구분</th>
                    <td >
                        <c:forEach var="item" items="${gnbSiteType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${item.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="searchSiteType" class="ui input" value="${item.mcCd}" ${checked}/><span>${item.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <th>디바이스</th>
                    <td>
                        <div class="ui form inline">
                            <select id="searchDeviceType" style="width: 180px" name="searchDeviceType" class="ui input medium" >
                                <c:forEach var="codeDto" items="${gnbDevice}" varStatus="status">
                                    <c:set var="selected" value="" />
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected" />
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 쿠폰 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="gnbCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="gnbPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>



<script>
    var callBackScript  = "${callBackScript}";
    var isMulti         = "${isMulti}";
    var siteType        = "${siteType}";
    var deviceType       = "${deviceType}";
    var  gnbDeviceTypeJson  = ${gnbDeviceTypeJson};
    ${gnbPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/gnbPop.js?v=${fileVersion}"></script>