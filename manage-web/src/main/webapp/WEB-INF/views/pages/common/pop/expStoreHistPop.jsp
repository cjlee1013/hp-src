<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .popup-request {margin: 20px;}
    .tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
    .tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
    .tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
    .tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
    .wrap-table td {background-color: white; word-break:break-all;}
    .vertical-table td {text-align: left !important;}
</style>
<div class="com popup-request">
    <div class="com popup-wrap-title">
        <h3 class="title pull-left">
            <i class="fa fa-clone mg-r-5"></i>  변경이력
        </h3>
        <span class="text mg-l-10">최대 30일 DATA만 노출됩니다. </span>
    </div>
    <div class="tabui-wrap pd-t-20">
        <div class="tabui medium col-02">
            <div id="expStoreHistListPopGrid" style="width: 100%; height: 350px;"></div>

        </div>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
            <button class="ui button xlarge font-malgun" id="excelDownloadBtn">엑셀다운</button>
        </span>
    </div>
</div>

<script>
    //hist
    var  expStoreHist = ${expStoreHist};

    //grid
    ${expStoreHistListPopGridBaseInfo}

</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/expStoreHistPop.js?v=${fileVersion}"></script>