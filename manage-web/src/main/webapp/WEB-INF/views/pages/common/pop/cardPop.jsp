<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup small popup-request">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" id="popTitle">
            <c:choose>
                <c:when test="${paymentMethodType eq 'CARD'}">카드선택</c:when>
                <c:when test="${paymentMethodType eq 'EASYPAY'}">간편결제 선택</c:when>
                <c:otherwise>결제수단 선택</c:otherwise>
            </c:choose>
        </h2>
    </div>
    <form id="cardPopForm" onsubmit="return false;">
        <input type="hidden" id="schPaymentMethodType" name="schPaymentMethodType" value="${paymentMethodType}">
        <div class="ui wrap-table horizontal mg-t-1 0" style="display: none;">
            <table class="ui table">
                <caption>카드 검색</caption>
                <colgroup>
                    <col width="7%">
                    <col width="*">
                    <col width="10%">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 180px">
                                <option value="cardCd">코드</option>
                                <option value="cardNm">카드명</option>
                            </select>
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                        </div>
                    </td>
                    <!-- 검색 버튼 -->
                    <td>
                        <div style="text-align: center;" class="ui form mg-t-15">
                            <button type="submit" id="schBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                            <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <div id="divGroupCd" class="ui wrap-table horizontal mg-t-1 0" style="display: none;">
        <table class="ui table">
            <caption>카드 검색</caption>
            <colgroup>
                <col width="80px">
                <col width="*">
                <col width="200px">
            </colgroup>
            <tbody>
            <tr>
                <th>그룹코드</th>
                <td>
                    <input type="text" id="groupCd" name="groupCd" class="ui input medium mg-r-5" style="width: 100px;" maxlength="2">
                </td>
                <td>
                    <div class="ui form">
                        <button type="button" id="setGroupCdBtn" class="ui button large cyan "><i class="fa fa-search"></i>입력</button>
                        <button type="button" id="delGroupCdBtn" class="ui button large white">삭제</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="text-align: right" class="ui form mg-l-30 mg-b-5">
            <input type="checkbox" id ="checkYn" name="checkYn"  checked><span> 그룹코드 입력된 카드 선택</span>
        </div>
    </div>

    <!-- 카드리스트 그리드 -->
    <div class="com wrap-title sub" style="display: none;">
        <h3 class="title">• 검색결과: <span id="searchCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="cardPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 카드리스트 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    // 카드 코드 조회 그리드 정보
    ${cardPopGridBaseInfo}

    var callBackScript  = "${callBackScript}";
    var multiYn         = "${multiYn}";
    var groupCdNeedYn         = "${groupCdNeedYn}";
    var paymentMethodType     = "${paymentMethodType}";
    var allPaymentList = '${allPaymentList}';
    var getPaymentList = '${paymentList}';
    var setYn = "${setYn}";
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/cardPop.js?v=${fileVersion}"></script>