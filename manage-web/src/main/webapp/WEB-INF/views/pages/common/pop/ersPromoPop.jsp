<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="ersPromoPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">사은행사 조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="35%">
                    <col width="10%">
                    <col width="35%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>조회기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                <option value="EVENTDT">행사일</option>
                                <option value="REGDT">등록일</option>
                            </select>
                            <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                        </div>
                    </td>
                    <td rowspan="3">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>구매기준유형</th>
                    <td>
                        <select id="schEventTargetType" name="schEventTargetType" class="ui input medium" style="width: 180px;">
                            <option value="">전체</option>
                            <c:forEach var="eventTargetType" items="${eventTargetType}" varStatus="status">
                                <option value="${eventTargetType.mcCd}">${eventTargetType.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>온라인<br/>사용여부</th>
                    <td>
                        <select id="schUseYn" name="schUseYn" class="ui input medium" style="width: 180px;">
                            <option value="">전체</option>
                            <c:forEach var="useYn" items="${useYn}" varStatus="status">
                                <option value="${useYn.mcCd}">${useYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>확정여부</th>
                    <td colspan="3">
                        <select id="schConfirmYn" name="schConfirmYn" class="ui input medium" style="width: 180px;">
                            <option value="">전체</option>
                            <c:forEach var="confirmYn" items="${confirmYn}" varStatus="status">
                                <option value="${confirmYn.mcCd}">${confirmYn.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 180px;">
                                <option value="EVENTNM">행사명</option>
                                <option value="EVENTCD" selected>행사번호</option>
                                <option value="ITEMNO">상품번호</option>
                            </select>
                            <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 쿠폰 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="ersPromoCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="ersPromoPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var isMulti         = "${isMulti}";
    var callBackScript  = "${callBackScript}";

    // 사은행사 조회 그리드 정보
    ${ersPromoPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/ersPromoPop.js?v=${fileVersion}"></script>