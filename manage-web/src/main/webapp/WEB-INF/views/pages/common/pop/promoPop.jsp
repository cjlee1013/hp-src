<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge">
    <form id="promoPopForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 id="titlePop" class="title font-malgun"></h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="35%">
                    <col width="10%">
                    <col width="35%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>조회기간</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="schDateType" name="schDateType" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                <option value="REGDT">등록일</option>
                                <option value="DISPDT">전시일</option>
                            </select>
                            <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                            <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                            <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-l-10" onclick="promoPop.initCalendarDate('0')">오늘</button>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="promoPop.initCalendarDate('-1w')">1주일전</button>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="promoPop.initCalendarDate('-1m')">1개월전</button>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="promoPop.initCalendarDate('-3m')">3개월전</button>
                        </div>
                    </td>
                    <td rowspan="3">
                        <div style="text-align: left;" class="ui form mg-l-30">
                            <button type="button" id="schBtn" class="ui button large cyan">검색</button><br/>
                            <button type="button" id="schResetBtn" class="ui button large mg-t-10">초기화</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사이트구분</th>
                    <td <c:choose><c:when test="${promoType eq 'EVENT'}">colspan="3" </c:when></c:choose>>
                        <c:forEach var="siteType" items="${siteType}" varStatus="status">
                            <c:set var="checked" value="" />
                            <c:if test="${siteType.ref1 eq 'df'}">
                                <c:set var="checked" value="checked" />
                            </c:if>
                            <label class="ui radio inline"><input type="radio" name="schSiteType" class="ui input" value="${siteType.mcCd}" ${checked}/><span>${siteType.mcNm}</span></label>
                        </c:forEach>
                    </td>
                    <c:choose>
                        <c:when test="${promoType eq 'EXH'}">
                            <th>점포유형</th>
                            <td>
                                <select id="schStoreType" name="schStoreType" class="ui input medium" style="width: 100px;">
                                    <c:forEach var="storeType" items="${storeType}" varStatus="status">
                                        <option value="${storeType.mcCd}">${storeType.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </c:when>
                    </c:choose>
                </tr>
                <tr>
                    <th>검색어</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select id="schType" name="schType" class="ui input medium mg-r-5" style="width: 100px;">
                                <c:choose>
                                    <c:when test="${promoType eq 'EXH'}">
                                    <option value="PROMONO" selected="selected">기획전번호</option>
                                    <option value="MNGPROMONM">관리기획전명</option>
                                    </c:when>
                                    <c:when test="${promoType eq 'EVENT'}">
                                        <option value="PROMONO" selected="selected">이벤트번호</option>
                                        <option value="MNGPROMONM">관리이벤트명</option>
                                    </c:when>
                                </c:choose>
                                <option value="REGNM">등록자</option>
                            </select>
                            <input type="text" id="schValue" name="schValue" class="ui input medium mg-r-5" style="width: 250px;" >
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 쿠폰 그리드 -->
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="promoCnt">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div id="promoPopGrid" style="width: 100%; height: 250px;"></div>
    <!-- // 상품 그리드 -->

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="selectBtn">선택</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript  = "${callBackScript}";
    var promoNo         = "${promoNo}";
    var isMulti         = "${isMulti}";
    var promoType       = "${promoType}";
    var siteType        = "${promositeType}";
    var storeType       = "${promoStoreType}";
    var useYnJson       = ${useYnJson};
    var dispYnJson      = ${dispYnJson};
    var siteTypeJson    = ${siteTypeJson};
    var storeTypeJson   = ${storeTypeJson};

    // 기획전, 이벤트 조회 그리드 정보
    ${promoPopGridBaseInfo}
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/promoPop.js?v=${fileVersion}"></script>