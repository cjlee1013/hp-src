<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>
    .tab_vicinity {height: 30px;padding: 0;border-bottom: 1px solid #ccc;margin-top: 20px;}
    .tab_vicinity li {padding: 0 10px;overflow: hidden;text-align: center;float: left;position: relative;z-index: 10;border: 1px solid #ccc;line-height: 28px;font-weight: bold;font-size: 12px;background-color: #dfdfdf;bottom: -1px;margin-right: 2px;}
    .tab_vicinity li:hover, .tab_vicinity li.on {background-color: #dc2626;}
    .tab_vicinity li:hover a, .tab_vicinity li.on a {color: #fff;text-decoration: none;}
    .num-box .link{color: #5886d1 !important; font-weight: bold !important;}
    .num-box td {border-left: 1px #eee solid !important;}
    .num-box tr {border-left: 1px #eee solid !important; border-right: 1px #eee solid !important;}
    .default-width {width:90%;}
</style>
<div class="com wrap-title">
    <c:if test="${isMd eq true}">
        <div id="mdDashBoard" class="ui wrap-table horizontal default-width">
            <table class="ui table num-box">
                <colgroup>
                    <col width="*">
                    <col width="15%">
                    <col width="15%">
                    <col width="15%">
                    <col width="15%">
                    <col width="15%">
                    <col width="15%">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품상태</th>
                    <c:forEach var="codeDto" items="${prodSchStatusPolicy}" varStatus="status">
                        <td>${codeDto.statusNm} (<a href="#" id="${codeDto.status}" class="link prodStatusCnt" data-sch-type="schProdStatus">-</a>)</td>
                    </c:forEach>
                </tr>
                <tr>
                    <th>정보변경 요청상태</th>
                    <c:forEach var="codeDto" items="${prodInfoChgStatusSch}" varStatus="status">
                        <td>${codeDto.statusNm} (<a href="#" id="${codeDto.status}" class="link prodStatusCnt" data-sch-type="schProdActiveStatus">-</a>)</td>
                    </c:forEach>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title default-width mg-b-20 mg-t-5 ">
            <span class="pull-right">(최근 7일 / 상품수정일 기준)</span>
        </div>
    </c:if>


    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">공지사항 (ADMIN) </h2>
    </div>

    <div  class="com wrap-title sub default-width" >

        <ul class="tab_vicinity">
            <li class="on" data-type="GENERAL"><a class="tab_cnt">일반공지</a></li>
            <li data-type="HARM"><a class="tab_cnt">위해상품공지</a></li>
        </ul>

        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
            <table class="ui table">
                <caption>검색</caption>
                <colgroup>
                    <col width="100px">
                    <col width="*">
                    <col width="230px">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <select id='searchType' name='searchType' style="width: 120px" class="ui input"></select>
                            <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-l-5" style="width: 300px;">
                        </div>
                    </td>
                    <td>
                        <div class="ui form" style="float:right;">
                            <button type="button" id="searchBtn" class="ui button large cyan">검색</button>
                            <button type="button" id="searchInitBtn" class="ui button large mg-l-10">초기화</button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            </form>
        </div>
        <div class="mg-t-5">※ 최근 6개월 공지가 노출됩니다.</div>
        <!-- 여기까진 공통으로.. -->

        <!-- 일반공지 그리드 -->
        <div id="cnoticeGridTotalDiv">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="cnoticeSearchCnt">0</span> 건</h3>
            </div>
            <div class="topline"></div>
            <div id="cnoticeGrid" style="background-color:white;width:100%;height:350px;"></div>
        </div>

        <!-- 일반공지 상세 영역 -->
        <div class="ui wrap-table horizontal mg-t-10" id="cnoticeDetail" style="display: none;">
            <table class="ui table">
                <caption>공지사항 등록/수정</caption>
                <colgroup>
                    <col width="100px">
                    <col width="300px">
                    <col width="100px">
                    <col width="300px">
                </colgroup>
                <tbody>
                <tr>
                    <th>제목</th>
                    <td id="noticeTitle"></td>
                    <th>첨부파일</th>
                    <td id ="fileList">
                    </td>
                </tr>
                <tr style="height: 300px">
                    <th>내용</th>
                    <td colspan="3" id="noticeDesc">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- 위해상품 검색 그리드 -->
        <div id="harmGridTotalDiv"  style="display: none;">
            <div class="com wrap-title sub">
                <h3 class="title">• 검색결과 : <span id="harmListCount">0</span> 건</h3>
            </div>
            <div class="topline"></div>
            <!-- // 위해상품 검색 그리드 -->
            <div id="harmGrid" style="background-color:white;width:100%;height:350px;"></div>
        </div>

        <!-- 위해상품 상세정보 -->

        <!-- //위해상품 상세정보 -->
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/home.js?v=${fileVersion}"></script>
<script type="text/javascript">
    var loginEmpNo  = '${loginEmpNo}';
    var tabDiv;

    ${cnoticeGridHeadBaseInfo}
    ${harmGridHeadBaseInfo}
    hmpImgUrl = '${homeImgUrl}';

    $(document).ready(function() {
        home.init();
        //위해상품공지
        harmManage.init();
        harmManage.search();
        //일반공지
        cnotice.init();
        cnotice.search();

        $('#searchKeyword').focus();
    });
</script>
