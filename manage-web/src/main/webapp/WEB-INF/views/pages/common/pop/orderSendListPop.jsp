<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup xlarge2">
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun">주문 검색</h2>
    </div>
    <form id="orderSendListPopForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>주문 검색</caption>
                <colgroup>
                    <col width="10%">
                    <col width="35%">
                    <col width="10%">
                    <col width="35%">
                    <col width="10%">
                </colgroup>
                <tbody>
                <tr>
                    <th>일자</th>
                    <td>
                        <div class="ui form inline">
                            <select id="orderType" name="orderType" class="ui input medium mg-r-10" style="width: 40%">
                                <option value="order">주문일</option>
                                <option value="ship">배송일</option>
                            </select>
                            <input type="text" id="orderDt" name="orderDt" class="ui input medium mg-r-5" style="width: 50%" >
                        </div>
                    </td>
                    <th>점포명</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="storeId" name="storeId" class="ui input medium mg-r-5" placeholder="점포코드" style="width: 30%" readonly>
                            <input type="text" id="storeNm" name="storeNm" class="ui input medium mg-r-5" placeholder="점포명" style="width: 30%" readonly>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="orderSendListPop.openStorePopup();">조회</button>
                        </div>
                    </td>
                    <td rowspan="3">
                        <div style="text-align: center;" class="ui form mg-t-15">
                            <button type="submit" id="schBtn" class="ui button large cyan" onclick="orderSendListPop.search();"><i class="fa fa-search"></i>검색</button><br/>
                            <button type="button" id="initBtn"	class="ui button large white mg-t-5" onclick="orderSendListPop.reset();">초기화</button><br/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>주문자</th>
                    <td>
                        <div class="ui form inline">
                            <label class="ui radio small inline"><input type="radio" name="isMember" value="true" checked><span>회원</span></label>
                            <label class="ui radio small inline"><input type="radio" name="isMember" value="false"><span>비회원</span></label>
                            <input type="hidden" id="userNo" name="userNo">
                            <input type="text" id="buyerNm" name="buyerNm" class="ui input medium mg-r-5" style="width: 40%" readonly>
                            <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="orderSendListPop.openMemberPopup();">조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Shift/Slot</th>
                    <td>
                        <div class="ui form inline">
                            <select id="shiftId" name="shiftId" class="ui input medium mg-r-10" style="width: 45%"></select>
                            <select id="slotId" name="slotId" class="ui input medium mg-r-10" style="width: 45%"></select>
                        </div>
                    </td>
                    <th>배송기사</th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="driverNm" name="driverNm" class="ui input medium mg-r-5" style="width: 80%">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <div class="topline"></div>
    <div id="orderSendListGrid" style="width: 100%; height: 250px;"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="saveAsBuyerBtn" onclick="orderSendListPop.saveAsBuyer();">주문자 번호 저장</button>
            <button class="ui button xlarge btn-danger font-malgun" id="saveAsShipBtn" onclick="orderSendListPop.saveAsShip();">수취인 번호 저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="orderSendListPop.close();">취소</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/pop/orderSendListPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/core/common.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script>
    let buyerCallback = "${buyerCallback}";
    let shipCallback = "${shipCallback}";
    ${orderSendListGridBaseInfo}
</script>
