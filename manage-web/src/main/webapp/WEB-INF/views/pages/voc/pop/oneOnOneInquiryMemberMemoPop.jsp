<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup large">
    <!-- 검색영역 -->
    <div>
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">회원 메모</h2>
        </div>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="topline"></div>
        <div id="oneOnOneInquiryMemberMemoGrid" style="width: 100%; height: 500px;"></div>
    </div>

    <!-- 메모등록 영역 (type ADD일 경우) -->
    <c:if test="${type eq 'ADD'}">
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>내용</th>
                    <td>
                        <textarea class="ui input mg-t-5" style="width: 100%; height: 60px;" maxlength="100" id="content"></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- 하단 버튼영역 -->
        <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button xlarge btn-danger font-malgun" id="addMemoBtn">저장</button>
            </span>
        </div>
    </c:if>
</div>

<script>
    ${oneOnOneInquiryMemberMemoGridBaseInfo}

    var userNo = '${userNo}';

    $(document).ready(function() {
        oneOnOneInquiryMemberMemoPop.init();
        oneOnOneInquiryMemberMemoGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/pop/oneOnOneInquiryMemberMemoPop.js?v=${fileVersion}"></script>