<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp"/>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">상품Q&A문의 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="csQnaSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>Q&A관리 검색</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="20%">
                        <col width="5%">
                        <col width="20%">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="csQna.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="csQna.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="csQna.initSearchDate('-3d');">3일</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan ">
                                    <i class="fa fa-search"></i>검색
                                </button>
                                <br/>
                                <button type="button" id="searchResetBtn"
                                        class="ui button large white mg-t-5">초기화
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                    style="width:150px;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>판매업체
                        </th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="searchPartnerId" name="searchPartnerId" class="ui input medium mg-r-5" style="width:100px;" readonly>
                                <input type="text" id="searchPartnerNm" class="ui input medium mg-r-5" style="width:100px;" readonly="">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="csQna.getPartnerName();">조회</button>
                            </div>
                        </td>
                        <th>답변여부</th>
                        <td>
                            <select id="searchQnaStatus" style="width: 180px" name="searchQnaStatus" class="ui input medium mg-r-10" >
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${qnaStatus}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10" style="width: 100px">
                                    <c:forEach var="codeDto" items="${qnaSearchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="searchKeyword" name="searchKeyword" class="ui input medium mg-r-5" style="width: 200px;" >
                            </div>
                        </td>
                        <th>노출여부</th>
                        <td>
                            <select id="searchQnaDispStatus" style="width: 180px" name="searchQnaDispStatus" class="ui input medium mg-r-10" >
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${qnaDispStatus}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="csQnaTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="csQnaListGrid" style="width: 100%; height: 320px;"></div>
    </div>
    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 상품 Q&A 문의 상세</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <colgroup>
                <col width="10%">
                <col width="15%">
                <col width="10%">
                <col width="15%">
                <col width="10%">
                <col width="15%">
                <col width="10%">
                <col width="15%">
            </colgroup>
            <tbody>
            <tr>
                <th>문의번호</th>
                <td id="qnaNo"></td>
                <th>문의일시</th>
                <td id="regDt"></td>
                <th>상품번호</th>
                <td id="itemNo"></td>
                <th>상품명</th>
                <td id="itemNm"></td>
            </tr>
            <tr>
                <th>답변여부</th>
                <td  id="qnaStatusTxt"></td>
                <th>노출여부</th>
                <td id="displayStatusTxt"></td>
                <th>비밀글여부</th>
                <td id="secretYn"></td>
                <th>구매여부<br>(주문번호)</th>
                <td id="purchaseYnTxt"></td>
            </tr>
            <tr>
                <th>작성자ID</th>
                <td id="userId"></td>
                <th>작성자</th>
                <td id="userNm"></td>
                <th>판매업체ID</th>
                <td id="partnerId"></td>
                <th>판매업체명</th>
                <td id="partnerNm"></td>
            </tr>
            <tr>
                <th>문의내용</th>
                <td colspan="7">
                    <table class="ui table">
                        <colgroup>
                            <col width="90%">
                            <col width="10%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <td>
                                <textarea name="qnaContents" id="qnaContents" class="ui input" style="width: 100%; height: 70px;" disabled></textarea>
                                <div id ="setQnaStatusButton" hidden>
                                    <button type="button" id="setQnaStatus"
                                            class="ui button large middle mg-t-5" >노출중지
                                    </button>

                                </div>

                            </td>
<%--
                            <td style="text-align: center" id ="setQnaStatusButton" hidden>
                                <button type="button" id="setQnaStatus"
                                        class="ui button large middle mg-t-5">노출중지
                                </button>
                            </td>--%>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th>답변내용</th>
                <td colspan="7" style="height: 100px;">
                    <table class="ui table">
                        <tbody id="replyList">
                        </tbody>

                    </table>



                </td>
            </tr>
            <tr id ="setReplyArea">
                <th>답변등록</th>
                <td colspan="7">
                    <table class="ui table">
                        <tbody>
                        <tr>
                            <td>
                                <textarea name="reply" id="reply" class="ui input" style="width: 100%; height: 70px;" maxlength="1500"></textarea>
                                <button type="button" id="addReply"
                                        class="ui button large middle mg-t-5">등록
                                </button>
                                <span style="float: right;margin-top: 10px;" class="text">( <span style="color:red;" id="textCountReply">0</span> / 1500자 )</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/csQnaMain.js?${fileVersion}"></script>


<script>
    ${csQnaListGridBaseInfo}
</script>
