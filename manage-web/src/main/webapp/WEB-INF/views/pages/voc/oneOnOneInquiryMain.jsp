<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="oneOnOneInquirySearchForm" name="oneOnOneInquirySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">1:1문의관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>1:1문의관리</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="35%">
                        <col width="10%">
                        <col width="40%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>문의종류</th>
                            <td class="ui form inline">
                                <select id="schInqryKind" name="schInqryKind" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="N">신규문의</option>
                                    <option value="R">재문의</option>
                                </select>
                            </td>
                            <th>검색조건</th>
                            <td class="ui form inline" colspan="2">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="inqryNo">문의번호</option>
                                    <option value="purchaseOrderNo">주문번호</option>
                                    <option value="inqryTitle">제목</option>
                                    <option value="inqryAnswrRegNm">처리자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px">
                            </td>
                            <td rowspan="5">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>사이트</th>
                            <td class="ui form inline" >
                                <select id="schSiteType" name="schSiteType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>처리상태</th>
                            <td colspan="2" class="ui form inline">
                                <select id="schInqryStatus" name="schInqryStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${inqryStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>판매자</th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 110px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'AURORA'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                    <option value="NONE">주문내역없음</option>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 140px; display: none;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" readonly>
                                <button id="schStoreBtn" type="button" class="ui button medium" disabled>조회</button>
                            </td>
                            <th>작성자(회원)</th>
                            <td class="ui form inline" colspan="2">
                                <input type="text" id="schUserNo" name="schUserNo" class="ui input mg-r-5" style="width: 100px; display: none">
                                <input type="text" id="schUserNm" name="schUserNm" class="ui input mg-r-5" style="width: 120px;" readonly>
                                <button id="schBuyer" type="button" class="ui button medium">조회</button>
                            </td>
                        </tr>
                        <tr>
                            <th>문의유형</th>
                            <td class="ui form inline" colspan="4">
                                <select id="schInqryCategory" name="schInqryCategory" style="width: 150px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">구분 전체</option>
                                    <c:forEach var="codeDto" items="${inqryCategory}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select id="schInqryType" name="schInqryType" style="width: 200px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">문의구분 전체</option>
                                    <c:forEach var="codeDto" items="${inqryType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select id="schInqryDetailType" name="schInqryDetailType" style="width: 200px; float: left;"  class="ui input medium mg-r-10">
                                    <option value="">문의항목 전체</option>
                                    <c:forEach var="codeDto" items="${inqryDetailType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" data-ref1="${codeDto.ref1}" data-ref2="${codeDto.ref2}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>일자</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schDtType" name="schDtType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="inqryDt">문의일</option>
                                    <option value="inqryAnswrDt">처리일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="oneOnOneInquirySearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="oneOnOneInquiryGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${oneOnOneInquiryGridBaseInfo}

    var so_storeId = '${so_storeId}';
    var so_storeNm = '${so_storeNm}';
    var so_storeType = '${so_storeType}';

    $(document).ready(function() {
        oneOnOneInquiryMain.init();
        oneOnOneInquiryGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/oneOnOneInquiryMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>