<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="marketInquirySearchForm" name="marketInquirySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">제휴사 고객문의</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>제휴사 고객문의</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="35%">
                        <col width="10%">
                        <col width="40%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>일자</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schDtType" name="schDtType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="receiveDate">문의일</option>
                                    <option value="isresponseDate">처리일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>점포</th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 110px;">
                                    <option value="HYPER">Hyper</option>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 140px; display: none;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" readonly>
                                <button id="schStoreBtn" type="button" class="ui button medium">조회</button>
                            </td>
                            <th>검색어</th>
                            <td class="ui form inline" colspan="2">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ordNo">주문번호</option>
                                    <option value="copOrdNo">마켓주문번호</option>
                                    <option value="messageNo">문의번호</option>
                                    <option value="inquirerName">문의자명</option>
                                    <option value="inquirerPhone">문의자연락처</option>
                                    <option value="goodId">상품번호</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px">
                            </td>
                        </tr>
                        <tr>
                            <th>마켓연동</th>
                            <td class="ui form inline" >
                                <select id="schMarketType" name="schMarketType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </td>
                            <th>처리상태</th>
                            <td colspan="2" class="ui form inline">
                                <select id="schIsresponseStatus" name="schIsresponseStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="Y">답변완료</option>
                                    <option value="N">미답변</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>등록구분</th>
                            <td class="ui form inline" >
                                <select id="schMessageType" name="schMessageType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                </select>
                            </td>
                            <th>문의타입</th>
                            <td class="ui form inline" >
                                <select id="schContactType" name="schContactType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="marketInquirySearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="marketInquiryGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${marketInquiryGridBaseInfo}

    var homeplusFrontUrl = '${homeplusFrontUrl}'; // 홈플러스 프론트 페이지 URL

    $(document).ready(function() {
        marketInquiryMain.init();
        marketInquiryGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/marketInquiryMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>