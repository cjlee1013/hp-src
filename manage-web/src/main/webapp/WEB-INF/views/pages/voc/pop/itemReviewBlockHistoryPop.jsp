<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-popup xlarge">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">노출여부 이력조회</h2>
    </div>
    <div class="com wrap-title mg-t-10" >
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tr>
                    <td valign="top">
                        <div class="mg-t-20" id="itemReviewBlockHistoryGrid" style="overflow: scroll; width:100%; height:300px;"></div>
                    </td>
                </tr>
            </table>
            * 최근 6개월의 이력입니다.
        </div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    // 변경이력 팝업 그리드 기본정보
    ${itemReviewBlockHistoryPopBaseInfo};

    $(document).ready(function () {
        itemReviewBlockHistoryGrid.init();
        CommonAjaxBlockUI.global();
    });

    // 변경이력 팝업 그리드
    var itemReviewBlockHistoryGrid = {
        realGrid: new RealGridJS.GridView("itemReviewBlockHistoryGrid"),
        dataProvider: new RealGridJS.LocalDataProvider(),
        init: function () {
            itemReviewBlockHistoryGrid.initGrid();
            itemReviewBlockHistoryGrid.initDataProvider();
            itemReviewBlockHistoryMng.search();
        },
        initGrid: function () {
            itemReviewBlockHistoryGrid.realGrid.setDataSource(itemReviewBlockHistoryGrid.dataProvider);
            itemReviewBlockHistoryGrid.realGrid.setStyles(itemReviewBlockHistoryPopBaseInfo.realgrid.styles);
            itemReviewBlockHistoryGrid.realGrid.setDisplayOptions(itemReviewBlockHistoryPopBaseInfo.realgrid.displayOptions);
            itemReviewBlockHistoryGrid.realGrid.setColumns(itemReviewBlockHistoryPopBaseInfo.realgrid.columns);
            itemReviewBlockHistoryGrid.realGrid.setOptions(itemReviewBlockHistoryPopBaseInfo.realgrid.options);
            itemReviewBlockHistoryGrid.realGrid.setEditOptions()
        },
        initDataProvider: function () {
            itemReviewBlockHistoryGrid.dataProvider.setFields(itemReviewBlockHistoryPopBaseInfo.dataProvider.fields);
            itemReviewBlockHistoryGrid.dataProvider.setOptions(itemReviewBlockHistoryPopBaseInfo.dataProvider.options);
        },
        setData: function (dataList) {
            itemReviewBlockHistoryGrid.dataProvider.clearRows();
            itemReviewBlockHistoryGrid.dataProvider.setRows(dataList);
        }
    };

    // 변경이력 팝업 관리
    var itemReviewBlockHistoryMng = {
        /** 조회 */
        search : function() {
            CommonAjax.basic({
                url: "/voc/itemReview/getItemReviewHist.json?reviewNo=${reviewNo}"
                , method: "GET"
                , callbackFunc: function (res) {
                    console.log(res);
                    itemReviewBlockHistoryGrid.setData(res);
                }
            });
        },
    };
</script>