<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup xlarge"
style="margin:0 0 7px 0;padding:10px 20px 0 20px; height:500px;max-height: 500px; overflow:hidden;position:relative;overflow-y:scroll;background-color:#f7f7f7;">
    <div>
        <form id="oneOnOneInquiryDetailPopForm" name="oneOnOneInquiryDetailPopForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">고객문의 상세</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="57%">
                        <col width="8%">
                        <col width="25%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>문의번호</th>
                            <td>
                                <span class="text">${inqryNo}</span>
                            </td>
                            <th>처리상태</th>
                            <td>
                                <select id="schInqryStatus" name="schInqryStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <c:forEach var="codeDto" items="${inqryStatus}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${codeDto.mcCd eq 'I4'}">
                                                <option value="${codeDto.mcCd}" style="display: none">${codeDto.mcNm}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>문의유형</th>
                            <td>
                                <select id="schInqryCategory" name="schInqryCategory" style="width: 120px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">구분 전체</option>
                                    <c:forEach var="codeDto" items="${inqryCategory}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select id="schInqryType" name="schInqryType" style="width: 150px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">문의구분 전체</option>
                                    <c:forEach var="codeDto" items="${inqryType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select id="schInqryDetailType" name="schInqryDetailType" style="width: 200px; float: left;"  class="ui input medium mg-r-10">
                                    <option value="">문의항목 전체</option>
                                    <c:forEach var="codeDto" items="${inqryDetailType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" data-ref1="${codeDto.ref1}" data-ref2="${codeDto.ref2}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>판매자</th>
                            <td>
                                <span class="text" id="storeId"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>사이트구분</th>
                            <td>
                                <span class="text" id="site"></span>
                            </td>
                            <th>회원정보</th>
                            <td>
                                <span class="text" id="userInfo"></span>
                                <button type="button" id="memberMemoBtn" class="ui button small gray-dark font-malgun mg-l-10">회원메모</button>
                            </td>
                        </tr>
                        <tr>
                            <th>주문번호</th>
                            <td>
                                <span class="text" id="purchaseOrderNo"></span>
                            </td>
                            <th>문의일</th>
                            <td>
                                <span class="text" id="inqryDt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>상품번호</th>
                            <td colspan="3">
                                <span class="text" id="itemNo"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <colgroup>
                        <col width="12%">
                        <col width="38%">
                        <col width="10%">
                        <col width="15%">
                        <col width="10%">
                        <col width="15%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>제목</th>
                            <td>
                                <span class="text" id="inqryTitle"></span>
                            </td>
                            <th rowspan="3">답변내용</th>
                            <td rowspan="3" colspan="3">
                                <textarea id="inqryAnswrCntnt" class="ui input mg-t-5" style="width: 100%; height: 100px;" maxlength="10000"></textarea>
                                <span class="words align-l"><i>0</i>/10000자</span>
                            </td>
                        </tr>
                        <tr>
                            <th>문의내용</th>
                            <td>
                                <textarea id="inqryCntnt" class="ui input mg-t-5" style="width: 100%; height: 110px;" maxlength="10000" readonly="readonly"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>첨부파일</th>
                            <td id="inqryImg">
                            </td>
                        </tr>
                        <tr>
                            <th>SMS발송 여부</th>
                            <td>
                                <span class="text" id="smsAlamYn"></span>
                            </td>
                            <th>답변작성일</th>
                            <td>
                                <span class="text" id="inqryAnswrDt"></span>
                            </td>
                            <th>답변작성자</th>
                            <td>
                                <span class="text" id="inqryAnswrRegNm"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <c:if test="${inqryKind eq 'R'}">

                <div class="ui wrap-table horizontal mg-t-20">
                    <table class="ui table">
                        <colgroup>
                            <col width="12%">
                            <col width="38%">
                            <col width="11%">
                            <col width="14%">
                            <col width="11%">
                            <col width="14%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th rowspan="2">재문의내용</th>
                                <td rowspan="2">
                                    <textarea id="reInqryCntnt" class="ui input mg-t-5" style="width: 100%; height: 110px;" maxlength="10000" readonly="readonly"></textarea>
                                </td>
                                <th>재문의답변</th>
                                <td colspan="3">
                                    <textarea id="reInqryAnswrCntnt" class="ui input mg-t-5" style="width: 100%; height: 100px;" maxlength="10000"></textarea>
                                    <span class="words align-l"><i>0</i>/10000자</span>
                                </td>
                            </tr>
                            <tr>
                                <th>재문의 답변작성일</th>
                                <td>
                                    <span class="text" id="reInqryAnswrDt"></span>
                                </td>
                                <th>재문의 답변작성자</th>
                                <td>
                                    <span class="text" id="reInqryAnswrRegNm"></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </c:if>

            <div class="ui wrap-table horizontal mg-t-20">
                <table class="ui table">
                    <colgroup>
                        <col width="12%">
                        <col width="38%">
                        <col width="8%">
                        <col width="42%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>직원메모</th>
                        <td colspan="3">
                            <textarea id="empMemo" class="ui input mg-t-5" style="width: 100%; height: 60px;" maxlength="100"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>작성일</th>
                        <td>
                            <span class="text" id="empMemoDt"></span>
                        </td>
                        <th>작성자</th>
                        <td>
                                <span class="text" id="empMemoNm"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>

</div>

<script>
    var inqryNo = '${inqryNo}'; //문의번호
    var inqryKind = '${inqryKind}'; //문의종류
    var callBackScript = "${callBackScript}";
    var hmpImgUrl = '${hmpImgUrl}';
    var so_storeId = '${so_storeId}';
    var so_storeNm = '${so_storeNm}';
    var so_storeType = '${so_storeType}';

    $(document).ready(function() {
        window.resizeTo(1130,620);
        oneOnOneInquiryDetailPop.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/pop/oneOnOneInquiryDetailPop.js?v=${fileVersion}"></script>