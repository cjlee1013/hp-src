<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup xlarge" style="margin:0 0 7px 0;padding:10px 20px 0 20px; height:500px;max-height: 500px; overflow:hidden;position:relative;overflow-y:scroll;background-color:#f7f7f7;">
    <div>
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">콜센터문의상세</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="8%">
                    <col width="37%">
                    <col width="12%">
                    <col width="43%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>문의번호</th>
                        <td>
                            <span class="text" id="KEY"></span>
                        </td>
                        <th>처리상태</th>
                        <td>
                            <span class="text" id="advisor_ing_nm"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>상담유형</th>
                        <td>
                            <span class="text" id="advisor_type"></span>
                        </td>
                        <th>판매업체</th>
                        <td>
                            <span class="text" id="store_name"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>문의자</th>
                        <td>
                            <span class="text" id="cust_name"></span>
                            <button type="button" id="memberMemoBtn" class="ui button small gray-dark font-malgun mg-l-10" style="display: none">회원메모</button>
                        </td>
                        <th>주문번호</th>
                        <td>
                            <a id="linkOrderNo" target="_blank" style="text-decoration: underline;" ><span class="text" id="h_order_no"></span><a/>
                        </td>
                    </tr>
                    <tr>
                        <th>상담구분</th>
                        <td>
                            <span class="text" id="voc_yn_nm"></span>
                        </td>
                        <th>상담주문상품</th>
                        <td>
                            <span class="text" id="h_item_no"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>마켓연동</th>
                        <td>
                            <span class="text" id="market_type_nm"></span>
                        </td>
                        <th>처리방법</th>
                        <td>
                            <span class="text" id="completWay"></span>

                            <label id="lblWay" class="small" style="float: left; display: none;">
                                <input type="checkbox" id="chkWay" class="mg-r-5">
                                <span class="text mg-r-10">변경</span>
                            </label>

                            <select id="hBranchCd" name="H_BRANCH_CD" class="ui input small mg-r-10" style="width: 150px; float: left; display: none;">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${cs_h_branch_cd}" varStatus="status">
                                    <c:if test="${codeDto.mcCd ne 'H_BRANCH_3'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <select id="advisorIngType" name="ADVISOR_ING_TYPE" class="ui input small mg-r-10" style="width: 150px; float: left; display: none;">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${cs_advisor_ing_type}" varStatus="status">
                                    <c:if test="${!(isSo and codeDto.mcCd eq 'ADVISOR_ING_END')}">
                                        <option value="${codeDto.mcCd}" gmcCd1="${codeDto.ref1}" gmcCd2="${codeDto.ref2}" style="display:none;">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title mg-t-20">
            <h2 class="title font-malgun">문의내용</h2>
            <span style="float: right;">
                <button type="button" id="addContentBtn" class="ui button large btn-outline-info" style="min-width: auto">문의추가</button>
            </span>
        </div>
        <div id="divInquiryContent">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="40%">
                    </colgroup>
                    <tbody >
                        <tr>
                            <th>접수일</th>
                            <td>
                                <span class="text" id="start_time"></span>
                            </td>
                            <th>접수자</th>
                            <td>
                                <span class="text" id="account_nm"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>제목</th>
                            <td colspan="3">
                                <span class="text" id="history_title"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>접수내용</th>
                            <td colspan="3">
                                <textarea class="ui input mg-t-5" style="width: 100%; height: 120px;" maxlength="1000" id="rmks" readonly></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="saveAdd">저장</button>
        </span>
        </div>

        <div class="com wrap-title mg-t-20">
            <h2 class="title font-malgun">처리내용</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody>
                    <tr>
                        <th><span class="text-red-star">처리사유</span></th>
                        <td colspan="3">
                            <span class="text" id="txt_complet_reason" style="display: none"></span>

                            <select id="complet_reason" name="complet_reason" class="ui input small mg-r-10" style="width: 120px; float: left;display: none;">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${cs_complet_reason}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <select id="complet_detail" name="complet_detail" class="ui input small mg-r-10" style="width: 200px; float: left;display: none;">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${cs_reason_detail}" varStatus="status">
                                    <option value="${codeDto.mcCd}" gmcCd="${codeDto.ref1}" style="display: none">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>발생자</th>
                        <td>
                            <input type="text" class="ui input" maxlength="50" id="issue_generator">
                        </td>
                        <th>발생원인</th>
                        <td>
                            <input type="text" class="ui input" maxlength="500" id="issue_reason">
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">처리내용</span></th>
                        <td colspan="3">
                            <textarea class="ui input mg-t-5" style="width: 100%; height: 60px;" maxlength="1000" id="complet_txt"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>처리완료일시</th>
                        <td>
                            <span class="text" id="complet_dt"></span>
                        </td>
                        <th>처리자</th>
                        <td>
                            <span class="text" id="complet_account_nm"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="saveComplete">저장</button>
        </span>
    </div>

    <div class="com wrap-title mg-t-20">
        <h2 class="title font-malgun">첨부파일</h2>
        <span style="float: left; padding-left: 10px;">
            *png, jpg 파일을 총 3개까지 업로드할 수 있습니다. 1개당 최대 용량은 5MB입니다. 파일제목은 최대 20자까지 허용합니다.
        </span>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="callCenterFileForm" onsubmit="return false;">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="40%">
                    <col width="10%">
                    <col width="40%">
                </colgroup>
                <tbody >
                <tr>
                    <th><button type="button" id="addFile" class="ui button medium gray-dark font-malgun mg-r-5" onclick="callCenterDetailPop.file.clickFile($(this));" style="float:left">파일선택</button></th>
                    <td colspan="3">
                        <ul id="fileList" name="fileList">

                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    
</div>

<input type="file" name="fileArr" id="itemFile" multiple style="display: none;"/>

<script>
    var master_seq = '${master_seq}'; //문의번호
    var channel_type = '${channel_type}'; //채널종류
    var callBackScript = "${callBackScript}";
    var callbackLinkScript = "${callbackLinkScript}";
    var hmpImgUrl = '${homeImgUrl}';
    var isSo = ${isSo};

    $(document).ready(function() {
        window.resizeTo(1130,620);
        callCenterDetailPop.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/pop/callCenterDetailPop.js?v=${fileVersion}"></script>