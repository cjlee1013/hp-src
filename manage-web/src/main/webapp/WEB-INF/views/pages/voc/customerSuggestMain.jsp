<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">제안하기</h2>
    </div>
    <div  class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="customerSuggestSearchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  customerSuggestMain.search();">
                <table class="ui table">
                    <caption>컨텐츠 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="30%">
                        <col width="7%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="customerSuggestMain.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="customerSuggestMain.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="customerSuggestMain.initSearchDate('-30d');">1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun" onclick="customerSuggestMain.initSearchDate('-90d');">3개월</button>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div class="ui form inline">
                                <button type="button" class="ui button large cyan" id="searchBtn"><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" class="ui button large white mg-t-5" id="searchResetBtn">초기화</button><br/>
                                <button type="button" class="ui button large dark-blue mg-t-5" id="excelDownloadBtn">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <!--
                        <th>사이트 구분</th>
                        <td>
                            <div class="ui form inline">
                                <select id="siteType" name="siteType" class="ui input medium mg-r-10" style="width: 250px" >
                                    <c:forEach var="siteType" items="${siteType}">
                                        <option value=${siteType.key}>${siteType.value}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        -->
                        <th>단말 OS</th>
                        <td>
                            <div class="ui form inline">
                                <select id="osType" name="osType" class="ui input medium mg-r-10" style="width: 250px" >
                                    <c:forEach var="osType" items="${osType}">
                                        <option value=${osType.key}>${osType.value}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div style="float:right; width:100%">
            <div class="com wrap-title sub">
                <h3 class="title">검색 결과 : <span id="customerSuggestTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-t-20 mg-b-20" id="customerSuggestListGrid" style="width: 100%; height: 335px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">문의 내용</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="customerSuggestForm" onsubmit="return false;">
            <table class="ui table">
                <colgroup>

                </colgroup>
                <tbody>
                    <tr>
                        <th style="width: 100px">회원번호</th>
                        <td colspan="1" style="width: 450px">
                            <div class="ui form inline">
                                <span class="text" id="userNo" name="userNo"></span>
                            </div>
                        </td>
                        <th style="width: 100px">앱 구분</th>
                        <td colspan="1" style="width: 450px">
                            <div class="ui form inline">
                                <span class="text" id="platformTypeData" name="platformTypeData"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 100px">디바이스 OS</th>
                        <td colspan="1" style="width: 450px">
                            <div class="ui form inline">
                                <span class="text" id="osTypeData" name="osTypeData"></span>
                            </div>
                        </td>
                        <th style="width: 100px">단말 모델명</th>
                        <td colspan="1" style="width: 450px">
                            <div class="ui form inline">
                                <span class="text" id="deviceModel" name="deviceModel"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 100px">내용</th>
                        <td colspan="3" style="width: 900px">
                            <div class="ui form inline">
                                <textarea id="customerSuggestMessage" name="customerSuggestMessage" rows="10" type="text" class="ui input medium mg-r-5" style="width: 100%;border: none;"></textarea>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <input type="hidden" id="osTypeMap" name="osTypeMap" value="${osType}">
            <input type="hidden" id="siteTypeMap" name="siteTypeMap" value="${siteType}">
        </span>
    </div>
</div>

<iframe style="display:none;" id="customerSuggestExcelDownloadFrame"></iframe>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/customerSuggestMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>

<script>
    ${customerSuggestGridBaseInfo}
</script>
