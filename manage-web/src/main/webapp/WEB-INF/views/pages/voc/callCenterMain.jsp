<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="callCenterSearchForm" name="callCenterSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">콜센터문의관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>콜센터문의관리</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="38%">
                        <col width="10%">
                        <col width="42%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schDtType" name="SCH_D_TYPE" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="RCV">접수일</option>
                                    <option value="PROC">처리완료일</option>
                                </select>
                                <input type="text" id="schStartDt" name="SCH_S_YMD" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="SCH_E_YMD" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
<%--                                        <input type="text" id="schStartDt" name="SCH_S_YMD" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">--%>
<%--                                        <input type="hidden" id="schEndDt" name="SCH_E_YMD">--%>
<%--                                        <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>--%>
<%--                                        <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>--%>
<%--                                        <button type="button" id="setYesterDayBtn" class="ui button small gray-dark font-malgun mg-r-5">어제</button>--%>
                            </td>
                            <td rowspan="5">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>판매업체</th>
                            <td class="ui form inline">
                                <select id="hChannel" name="H_CHANNEL" class="ui input medium mg-r-10" style="width: 110px;">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_h_channel}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStoreId" name="STORE_ID" class="ui input mg-r-5" style="width: 140px; display: none;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" readonly>
                                <button id="schStoreBtn" type="button" class="ui button medium" disabled>조회</button>
                            </td>
                            <th>접수구분</th>
                            <td colspan="2" class="ui form inline">
                                <select id="channelCd" name="CHANNEL_CD" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_channel_cd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>검색조건</th>
                            <td class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="H_ORDER_NO">주문번호</option>
                                    <option value="HISTORY_TITLE">제목</option>
                                    <option value="COMPLET_ACCOUNT_NM">처리자</option>
                                    <option value="MASTER_SEQ">문의번호</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px">
                                <input type="hidden" id="H_ORDER_NO" name="H_ORDER_NO" >
                                <input type="hidden" id="HISTORY_TITLE" name="HISTORY_TITLE" >
                                <input type="hidden" id="COMPLET_ACCOUNT_NM" name="COMPLET_ACCOUNT_NM" >
                                <input type="hidden" id="MASTER_SEQ" name="MASTER_SEQ" >
                            </td>
                            <th>작성자(회원)</th>
                            <td class="ui form inline" colspan="2">
                                <select id="schNomemOrder" name="schNomemOrder" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="회원">회원</option>
                                    <option value="비식별">비회원</option>
                                    <option value="99999998">N마트</option>
                                    <option value="99999993">11번가</option>
                                </select>

                                <input type="hidden" id="schUserNo" name="CUST_ID">
                                <input type="text" id="schUserNm" name="schUserNm" class="ui input mg-r-5" style="width: 100px;" readonly>
                                <input type="hidden" id="hidUserNo">
                                <button id="schBuyer" type="button" class="ui button medium">조회</button>
                            </td>
                        </tr>
                        <tr>
                            <th>상담유형</th>
                            <td class="ui form inline">
                                <select id="advisorType1" name="ADVISOR_TYPE_1" style="width: 150px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">상담구분 전체</option>
                                    <c:forEach var="codeDto" items="${cs_advisor_type_1}" varStatus="status">
                                        <option value="${codeDto.mcCd}" gmcCd="${codeDto.ref1}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select id="advisorType2" name="ADVISOR_TYPE_2" style="width: 150px; float: left;" class="ui input medium mg-r-10">
                                    <option value="">상담항목 전체</option>
                                    <c:forEach var="codeDto" items="${cs_advisor_type_2}" varStatus="status">
                                        <option value="${codeDto.mcCd}" gmcCd="${codeDto.ref1}" style="display: none">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>상담구분</th>
                            <td colspan="2" class="ui form inline">
                                <select id="vocYn" name="VOC_YN" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_voc_yn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>처리상태</th>
                            <td class="ui form inline">
                                <select id="advisorIngType" name="ADVISOR_ING_TYPE" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_advisor_ing_type}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>처리부서</th>
                            <td class="ui form inline">
                                <select id="hBranchCd" name="H_BRANCH_CD" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${cs_h_branch_cd}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>마켓연동</th>
                            <td class="ui form inline" colspan="3">
                                <select id="marketType" name="MARKET_TYPE" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="callCenterSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="callCenterGrid" style="width: 100%; height: 500px;"></div>
    </div>

    <div id="callCenterExcelGrid" style="display: none;"></div>
</div>

<script>
    ${callCenterGridBaseInfo}
    ${callCenterExcelGridBaseInfo}

    var so_storeId = '${so_storeId}';
    var so_storeNm = '${so_storeNm}';
    var so_storeType = '${so_storeType}';
    var isSo = ${isSo};
    var userNo = '${userNo}';
    var userNm = '${userNm}';

    $(document).ready(function() {
        callCenterMain.init();
        callCenterGrid.init();
        callCenterExcelGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/callCenterMain.js?v=${fileVersion}"></script>