<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">상품평 마일리지 지급 제외</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="itemReviewSearchForm" onsubmit="return false;">
                <input type="hidden" id="searchCateCd" name="searchCateCd">
                <table class="ui table">
                    <caption>상품평검색</caption>
                    <colgroup>
                        <col width="150px;">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="4">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="lcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="mcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="scateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="dcateCd"
                                    style="width:150px;float:left;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="itemReviewTotalCount">0</span>건</h3>
        </div>

        <div class="topline"></div>

        <div id="itemReviewExMileageListGrid" style="width: 100%; height: 320px;"></div>

        <br/>

        <td>
            <button type="button" class="ui button small mg-r-5 white pull-left" id="delItemReviewExMileageBtn">선택삭제</button>
        </td>
    </div>

    <br/>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 상품평 마일리지 지급 제외 등록</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="itemReviewSetForm" onsubmit="return false;">
            <input type="hidden" id="cateKind" name="cateKind">
            <input type="hidden" id="cateCd" name="cateCd">
            <table class="ui table">
                <caption>미게시사유 입력</caption>
                <colgroup>
                    <col width="150px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>구분&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="excKind" id="excKindItem" value="ITEM" checked><span>상품번호</span></label>
                        <label class="ui radio small inline"><input type="radio" name="excKind" id="excKindCate" value="CATE"><span>카테고리</span></label>
                    </td>
                </tr>
                <tr>
                    <th>카테고리&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <select class="ui input medium mg-r-5" id="setCateCd1" name="lcateCd"
                                style="width:200px;float:left;" data-default="대분류"
                                disabled="disabled"
                                onchange="javascript:commonCategory.changeCategorySelectBox(1,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd2" name="mcateCd"
                                style="width:200px;float:left;" data-default="중분류"
                                disabled="disabled"
                                onchange="javascript:commonCategory.changeCategorySelectBox(2,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd3" name="scateCd"
                                style="width:200px;float:left;" data-default="소분류"
                                disabled="disabled"
                                onchange="javascript:commonCategory.changeCategorySelectBox(3,'setCateCd');">
                        </select>
                        <select class="ui input medium mg-r-5" id="setCateCd4" name="dcateCd"
                                disabled="disabled"
                                style="width:200px;float:left;" data-default="세분류">
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>상품번호&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <div id="selectItemPopup" style="float: left;">
                                <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" readonly style="width: 150px;">
                                <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" readonly style="width: 250px;">
                                <button class="ui button small gray-light font-malgun" id="addItemPopUp">조회</button>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/itemReviewExMileageMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script>
    ${itemReviewExMileageListGridBaseInfo}
</script>