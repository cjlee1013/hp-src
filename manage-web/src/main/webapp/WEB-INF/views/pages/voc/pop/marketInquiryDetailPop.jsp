<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup xlarge"
style="margin:0 0 7px 0;padding:10px 20px 0 20px; height:500px;max-height: 500px; overflow:hidden;position:relative;overflow-y:scroll;background-color:#f7f7f7;">
    <div>
        <form id="marketInquiryDetailPopForm" name="marketInquiryDetailPopForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">제휴사 고객문의 상세</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="40%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>문의번호</th>
                            <td>
                            <span class="text" id="messageNo"></span>
                            </td>
                            <th>점포명</th>
                            <td>
                                <span class="text" id="storeNm"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>제휴사</th>
                            <td>
                                <span class="text" id="marketNm"></span>
                            </td>
                            <th>처리상태</th>
                            <td>
                                <span class="text" id="isresponseStatus"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>문의유형</th>
                            <td>
                                <span class="text" id="contactNm"></span>
                            </td>
                            <th>주문번호</th>
                            <td>
                                <span class="text" id="ordNo"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>상품번호</th>
                            <td>
                                <span class="text" id="goodId"></span>
                            </td>
                            <th>제휴사상품번호</th>
                            <td>
                                <span class="text" id="copGoodId"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>접수일</th>
                            <td>
                                <span class="text" id="receiveDate"></span>
                            </td>
                            <th>회원명</th>
                            <td>
                                <span class="text" id="inquirerName"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>문의제목</th>
                            <td colspan="3">
                                <span class="text" id="requestTitle"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>문의내용</th>
                            <td colspan="3">
                                <textarea id="requestComments" class="ui input mg-t-5" style="width: 100%; height: 110px;" maxlength="2000" readonly="readonly"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>답변작성자</th>
                            <td>
                                <span class="text" id="isresponseNm"></span>
                            </td>
                            <th>답변일</th>
                            <td>
                                <span class="text" id="isresponseDate"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>답변제목</th>
                            <td colspan="3">
                                <input type="text" id="responseTitle" name="responseTitle" class="ui input medium mg-r-5" style="width: 100%;" minlength="30">
                            </td>
                        </tr>
                        <tr>
                            <th>답변내용</th>
                            <td colspan="3">
                                <textarea id="responseContents" class="ui input mg-t-5" style="width: 100%; height: 110px;" maxlength="2000"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>

</div>

<script>
    var messageNo = '${messageNo}';
    var marketType = '${marketType}';
    var messageType = '${messageType}';
    var callBackScript = "${callBackScript}";

    $(document).ready(function() {
        window.resizeTo(1130,620);
        marketInquiryDetailPop.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/pop/marketInquiryDetailPop.js?v=${fileVersion}"></script>