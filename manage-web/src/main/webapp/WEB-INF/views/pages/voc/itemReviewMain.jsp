<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">상품평관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <form id="itemReviewSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>상품평검색</caption>
                    <colgroup>
                        <col width="9%">
                        <col width="17%">
                        <col width="9%">
                        <col width="17%">
                        <col width="9%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>등록일</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemReview.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemReview.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="itemReview.initSearchDate('-3d');">3일전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="5">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                    style="width:150px;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>스토어유형</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schStoreType" style="width: 100px" name="schStoreType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>적립여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schReserveYn" style="width: 90px" name="schReserveYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${reserveYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>노출여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schBlockYn" style="width: 100px" name="schBlockYn" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${blockYn}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="itemNo">상품번호</option>
                                    <option value="userNo">회원번호</option>
                                    <option value="purchaseOrderNo">주문번호</option>
                                    <option value="businessNm">판매업체명</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2">
                            </div>
                        </td>
                        <th>만족도</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schGrade" name="schGrade" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="">전체</option>
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="itemReviewTotalCount">0</span>건</h3>
        </div>

        <div class="topline"></div>

        <div id="itemReviewListGrid" style="width: 100%; height: 320px;"></div>
    </div>

    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 노출여부</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <form id="itemReviewSetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>노출여부</caption>
                <colgroup>
                    <col width="150px;">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>노출여부</th>
                    <td>
                        <label class="ui radio inline"><input type="radio" name="blockYn" class="ui input medium" value="N" checked><span>노출</span></label>
                        <label class="ui radio inline"><input type="radio" name="blockYn" class="ui input medium" value="Y"><span>미노출</span></label>
                    </td>
                </tr>
                <tr>
                    <th>사유선택</th>
                    <td>
                        <label class="ui radio inline"><input type="radio" name="blockType" class="ui input medium" value="B" disabled><span>욕설/비방/음란</span></label>
                        <label class="ui radio inline"><input type="radio" name="blockType" class="ui input medium" value="C" disabled><span>동일내용반복입력(도배)</span></label>
                        <label class="ui radio inline"><input type="radio" name="blockType" class="ui input medium" value="D" disabled><span>개인정보유출</span></label>
                        <label class="ui radio inline"><input type="radio" name="blockType" class="ui input medium" value="E" disabled><span>광고/홍보성</span></label>
                        <label class="ui radio inline"><input type="radio" name="blockType" class="ui input medium" value="A" disabled><span>직접입력</span></label>
                    </td>
                </tr>
                <tr>
                    <th>미게시상세사유</th>
                    <td>
                        <textarea id="blockContents" name="blockContents" class="ui input medium mg-r-5" cols="50" rows="10" minlength="10" maxlength="50" style="width: 300px; height: 100px;" readonly></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>

    <br/>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <h3 class="title">• 상세정보</h3>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <input type="hidden" id="reviewNo"/>
        <table class="ui table">
            <colgroup>
                <col width="12%">
                <col width="10%">
                <col width="12%">
                <col width="13%">
                <col width="8%">
                <col width="*">
                <col width="9%">
                <col width="13%">
            </colgroup>
            <tbody>
            <tr>
                <th>점포유형</th>
                <td id="storeType"></td>
                <th>상품번호</th>
                <td id="itemNo"></td>
                <th>상품명</th>
                <td id="itemNm1"></td>
                <th>노출여부</th>
                <td id="blockYn"></td>
            </tr>
            <tr>
                <th>적립여부</th>
                <td id="reserveYn"></td>
                <th>적립일</th>
                <td id="reserveDt"></td>
                <th>작성자 / 작성자ID</th>
                <td id="regId"></td>
                <th>등록일</th>
                <td id="regDt"></td>
            </tr>
            <tr>
                <th>만족도</th>
                <td id="grade"></td>
                <th>제품상태 만족도</th>
                <td id="gradeStatus"></td>
                <th>상품일치 만족도</th>
                <td id="gradeAccuracy"></td>
                <th>가격 만족도</th>
                <td id="gradePrice"></td>
            </tr>
            <tr>
                <th>상품평내용</th>
                <td colspan="7">
                    <table class="ui table">
                        <colgroup>
                            <col width="90%">
                            <col width="10%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <td id="contents"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th>상품평 이미지</th>
                <td colspan="7">
                    <table class="ui table">
                        <colgroup>
                            <col width="90%">
                            <col width="10%">
                        </colgroup>
                        <tbody>
                        <tr>
                            <td id="imgList"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/voc/itemReviewMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>

<script>
    ${itemReviewListGridBaseInfo}

    var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
    var hmpImgUrl = '${hmpImgUrl}';
    var hmpImgFrontUrl = '${hmpImgFrontUrl}';
</script>