<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--

<!-- 기본 프레임 -->
<div class="content min-w1280">
    <div class="">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">
                <i class="fa fa-bullhorn"></i> 알려드립니다 NO IFRAME
            </h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tbody>
                <tr>
                    <th class="table-banner">
                        <div class="banner-title">
                            해당 메뉴에 대한 이용권한이 없습니다. 이용권한이 필요하신 경우, 전자결재를 통해 신청하여 주십시오.
                        </div>
                        <p class="mg-t-20">권한부여 담당자: 홍길동</p>
                        <p class="mg-t-10">연락처: 010-123-4567</p>
                        <p class="mg-t-10">이메일: hongs@homeplus.co.kr</p>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <tbody>
                <tr>
                    <th class="table-banner">
                        <div class="banner-title">권한관리 하위 메뉴 권한 관련 문의사항은 아래 문의처로 연락 주십시오.</div>
                        <p class="mg-t-20">담당자: 홍길동</p>
                        <p class="mg-t-10">연락처: 010-123-4567</p>
                        <p class="mg-t-10">이메일: hongs@homeplus.co.kr</p>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
--%>
