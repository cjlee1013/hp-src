<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">${claimTypeName}배송</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
    <!-- 취소/반품/교환 사유 입력 -->
    <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
        <div class="ui form inline mg-l-10" >
            <span class="text mg-r-5" style="font-size: 15px">· 선택한 클레임 건의</span>
            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold">상품을 배송</span>
            <span class="text" style="font-size: 15px">합니다.</span>
        </div>
        <input type="hidden" id="claimNo" name="claimNo" value="${claimNo}">
        <input type="hidden" id="claimBundleNo" name="claimBundleNo" value="${claimBundleNo}">
        <input type="hidden" id="claimType" name="claimType" value="${claimType}">
        <input type="hidden" id="claimReqType" name="claimReqType" value="CA">
    </div>
    <!-- 취소/반품/교환 사유 입력 end -->
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="claimReqBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<script>
    var claimTypeName = '${claimTypeName}';
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/claimDeliveryPop.js?v=${fileVersion}"></script>
