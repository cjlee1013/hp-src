<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup xlarge" style="height: 100%; background-color: #FFFFFF; width: 100%; overflow: auto">
    <div class="com popup-wrap-title mg-t-20 mg-l-20">
        <c:choose>
            <c:when test="${reqType eq '1'}">
                <h5 class="title font-malgun" style="font-size: 20px">수거정보 변경</h5>
            </c:when>
            <c:otherwise>
                <h5 class="title font-malgun" style="font-size: 20px">배송정보 변경</h5>
            </c:otherwise>
        </c:choose>
    </div>
    <form id="claimApproveForm" onsubmit="return false;" class="mg-l-20 mg-r-20">
        <div class="ui wrap-table mg-t-15">
            <table class="ui table" id="changeDeliveryTable">
                <colgroup>
                    <col width="10%">
                    <col width="*">
                </colgroup>
                <tr style="height: 50px;">
                    <th>
                        *수거방법
                    </th>
                    <td id="pickWayDS" hidden>
                        <!-- 택배사 정보 입력 테이블 -->
                        <table id="courierInfoTable">
                            <caption>신청정보 테이블</caption>
                            <colgroup>
                                <col width="20%">
                                <col width="20%">
                                <col width="*%">
                            </colgroup>
                            <tr>
                                <td style="text-align: left">
                                    <select id="selectDeliveryType" name="selectDeliveryType" class="ui input medium mg-r-20">
                                        <option value="C">택배</option>
                                        <option value="D">직접수거</option>
                                        <option value="P">우편</option>
                                        <option value="N">수거안함</option>
                                    </select>
                                </td>
                                <td colspan="2" style="text-align: left">
                                    <div class="ui form inline"  id="parcelDiv" style="display: none">
                                        <select id="selectDlvCd" name="selectDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                            <option value="">택배사선택</option>
                                                <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                                </c:forEach>
                                        </select>
                                        <input type="text" id="invoice" name="invoice" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                                    </div>
                                    <div class="ui form inline"  id="pickDiv" style="display: none">
                                        <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="pickDateText">수거예정일 : </span>
                                        <input type="text" id="deliveryDt" name="deliveryDt" class="ui input medium mg-r-5" placeholder="수거예정일" style="width:180px;">

                                    </div>
                                    <div class="ui form inline"  id="postDiv" style="display: none">
                                        <input type="text" id="pickMemo" name="pickMemo" class="ui input medium mg-r-5" maxlength="50" style="width: 100%" placeholder="메모">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                               <td colspan="3" style="text-align: left">
                                   <span class="text mg-r-5" style="font-size: 13px" id="text1"></span>
                                   <span class="text mg-r-5" style="font-size: 13px" id="text2"></span>
                                   <span class="text mg-r-5" style="font-size: 13px" id="text3"></span>
                               </td>
                            </tr>
                        </table>
                        <!-- 택배사 정보 입력 테이블 end -->
                    </td>
                    <td id="pickWayTD" hidden style="text-align: left">
                        <div class="ui form inline mg-b-10">
                            <span class="text" style="font-size: 12px;">- 배송점포 : 홈플러스 </span>
                            <span class="text" id="storeNm" name="storeNm" style="font-size: 12px;"></span>
                            <span class="pull-right" id="exchInfoText" style="font-size: 14px; display: none">&#8251; 배송방법 변경 시 발생하는 배송비는 미차감됩니다.</span>

                        </div>
                        <c:choose>
                            <c:when test="${reqType eq '1'}">
                                <span class="text" style="font-size: 12px;">- 수거방법 : </span>
                            </c:when>
                            <c:otherwise>
                                <span class="text" style="font-size: 12px;">- 배송방법 : </span>
                            </c:otherwise>
                        </c:choose>





                        <label class="ui radio inline mg-l-10" id="tdRadioLabel" style="display: none">
                            <input type="radio" id="tdRadio" name="selectDeliveryType" class="ui input small" value="D">
                            <c:choose>
                                <c:when test="${reqType eq '1'}">
                                    <span>자차수거</span>
                                </c:when>
                                <c:otherwise>
                                    <span>자차</span>
                                </c:otherwise>
                            </c:choose>
                        </label>
                        <label class="ui radio inline" id="pickRadioLabel" style="display: none">
                            <input type="radio" id="pickRadio" name="selectDeliveryType" class="ui input small" value="D">
                            <c:choose>
                                <c:when test="${reqType eq '1'}">
                                    <span>점포방문</span>
                                </c:when>
                                <c:otherwise>
                                    <span>픽업</span>
                                </c:otherwise>
                            </c:choose>
                        </label>
                        <label class="ui radio inline mg-l-10" id="quickRadioLabel" style="display: none">
                            <input type="radio" id="quickRadio" name="selectDeliveryType" class="ui input small" value="D">
                            <c:choose>
                                <c:when test="${reqType eq '1'}">
                                    <span>퀵수거</span>
                                </c:when>
                                <c:otherwise>
                                    <span>퀵</span>
                                </c:otherwise>
                            </c:choose>
                        </label>

                        <c:if test="${reqType eq '1'}">
                            <label class="ui radio inline">
                                <input type="radio" id="noneRadio" name="selectDeliveryType" class="ui input small" value="N"><span>수거안함</span>
                            </label>
                        </c:if>

                        <div class="ui form inline mg-t-10" id="slotDiv">
                            <div class="ui form inline mg-b-5" id="pickPlace" style="display: none">
                                <span class="text" id="placeNm" name="placeNm"></span>
                            </div>

                            <c:choose>
                                <c:when test="${reqType eq '1'}">
                                    <span class="text" style="font-size: 12px;">- 수거시간 선택</span>
                                </c:when>
                                <c:otherwise>
                                    <span class="text" style="font-size: 12px;">- 배송시간 선택</span>
                                </c:otherwise>
                            </c:choose>

                            <div class="tbl-time mg-t-10" id="slotChangeArea"/>
                            <input type="hidden" id="selectSlot" name="selectSlot">
                        </div>
                    </td>
                </tr>

                <tr style="height: 50px;" id="pickShippingRow">
                    <th>
                        <c:choose>
                            <c:when test="${reqType eq '1'}">
                                *반품수거지
                            </c:when>
                            <c:otherwise>
                                *교환배송지
                            </c:otherwise>
                        </c:choose>
                    </th>
                    <td colspan="2">
                        <table class="ui table" id="pickShippingTable">
                            <div style="text-align: left; display: none" class="mg-t-10 mg-b-20" id="tdWayTitle">
                                <span class="text" id="" style="font-size: 14px;">&#8251; 원 주문의 점포지 기준으로 수거가 진행됩니다.</span>
                            </div>
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="name" name="name" class="ui input medium mg-r-5" style="width: 40%;" placeholder="이름을 입력해주세요" value="${param.name}" maxlength="20">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                        <input type="hidden" name="mobileNo" id="mobileNo">
                                        <input type="text" name="mobileNo_1" id="mobileNo_1" class="ui input medium" style="width: 65px;" title="연락처" maxlength="3">
                                        <span class="text">-</span>
                                        <input type="text" name="mobileNo_2" id="mobileNo_2" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                                        <span class="text">-</span>
                                        <input type="text" name="mobileNo_3" id="mobileNo_3" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                    <input type="text" name="zipCode" id="zipCode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="ui button small" onclick="javascript:zipCodePopup('chgDeliveryType.setZipcode'); return false;">우편번호 찾기</button>
                                    <br>
                                    <input type="text" name="addr" id="addr" class="ui input medium mg-t-5 mg-r-5" style="width:300px;" readonly>
                                    <input type="hidden" name="gibunAddr" id="gibunAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                    <input type="text" name="addrDetail" id="addrDetail" class="ui input medium mg-t-5" style="width:240px;" placeholder="상세주소를 입력해주세요" maxlength="100">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="bundleNo" name="bundleNo">
        <input type="hidden" id="purchaseOrderNo" name="purchaseOrderNo">
        <input type="hidden" id="claimPickShippingNo" name="claimPickShippingNo">
        <input type="hidden" id="claimExchShippingNo" name="claimExchShippingNo">
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="chgDeliveryBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>

    var reqType = '${reqType}';
    var claimNo = '${claimNo}';
    var claimBundleNo = '${claimBundleNo}';
    var claimType = '${claimType}';
    var shipType = '';
    var shipMethod = '';
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/changeDeliveryTypePop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
