<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">

    <!-- 클레임 정보 Layout -->
    <jsp:include page="layout/claimDetailPopLayout.jsp"></jsp:include>
    <!-- 클레임정보 Layout end -->

    <!-- 상세정보 -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 상세정보</h3>
        <span class="pull-right">
            <button type="button" class="ui button medium" id="chgPickAddrBtn" onclick="claimCommonUtil.chgDeliveryType('1')" style="display: none">수거정보변경</button>
            <button type="button" class="ui button medium" id="sellerInfoBtn" onclick="claimCommonUtil.sellerInfo() ">판매업체정보</button>
        </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table" id="returnDetailInfoTable">
            <caption>취소상세정보</caption>
            <colgroup>
                <col width="10%">
                <col width="*">
            </colgroup>
            <div class="ui form inline">
                <tbody>
                <tr>
                    <th>반품사유</th>
                    <td>
                        <span class="text" style="color: #f90000">[</span>
                        <span class="text" id="claimReasonType" name="claimReasonType" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" style="color: #f90000">]</span>
                        <span class="text" style="color: #f90000">  </span>
                        <span class="text" id="deductPromoYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductDiscountYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductShipYn" style="color: #f90000; display: none"></span>
                        <br>
                        <span class="text" id="claimReasonDetail" name="claimReasonTypeDetail"></span>
                        <span class="text" id="claimReasonTypeCode" name="claimReasonTypeCode" hidden></span>
                    </td>
                </tr>

                <tr id="pendingReason" hidden>
                    <th>반품보류사유</th>
                    <td>
                        <span class="text" style="color: #f90000">[</span>
                        <span class="text" id="pendingReasonType" name="pendingReasonType" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" style="color: #f90000">]</span><br>
                        <span class="text" id="pendingReasonDetail" name="pendingReasonDetail"></span>
                    </td>
                </tr>
                <tr id="rejectReason" hidden>
                    <th>반품거부사유</th>
                    <td>
                        <span class="text" style="color: #f90000">[</span>
                        <span class="text" id="rejectReasonType" name="rejectReasonType" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" style="color: #f90000">]</span><br>
                        <span class="text" id="rejectReasonDetail" name="rejectReasonTypeDetail"></span>
                    </td>
                </tr>

                 <tr>
                    <th>반품배송비</th>
                    <td>
                        <span class="text" id="returnShipAmt" name="returnShipAmt" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" id="returnShipAmtText" name="returnShipAmtText" style="color: #f90000"></span>
                        <span class="text" style="color: #f90000">&nbsp|&nbsp</span>
                        <span class="text" id="whoReason" name="whoReason" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" id="claimShipFeeEnclose" name="claimShipFeeEnclose" style="color: #f90000"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td>
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr id="pickAddrRow1">
                    <th rowspan="2">반품수거지</th>
                    <td>
                        <span class="text" id="pickReceiverNm" name="pickReceiverNm"></span>
                        <span class="text">  &#10072;  </span>
                        <span class="text" id="pickMobileNo" name="pickMobileNo"></span>
                        <span class="text" id="claimPickShippingNo" name="claimPickShippingNo" hidden></span>
                        <span class="text" id="pickStatusCode" name="pickStatusCode" hidden></span>
                    </td>
                </tr>
                <tr id="pickAddrRow2">
                    <td>
                        <div class="ui form inline">
                            <span class="text">(</span>
                            <span class="text" id="pickZipcode" name="pickZipcode"></span>
                            <span class="text">) </span>
                            <span class="text" id="pickAddr" name="pickAddr"></span>&nbsp;
                            <span class="text">&nbsp</span>
                            <span class="text" id="pickAddrDetail" name="pickAddrDetail"></span>&nbsp;
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>수거방법</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="pickStatusText" name="pickStatusText"></span>
                        </div>
                        <div class="ui form inline">
                            <span class="text" id="pickShipText" name="pickShipText"></span>
                            <span class="text">&nbsp&nbsp</span>
                            <input type="hidden" id="pickInvoiceNo" name="pickInvoiceNo"/>
                            <input type="hidden" id="pickDlv" name="pickDlv"/>
                            <button type="button" class="ui button medium gray-light" id="shipSearchBtn" onclick="claimCommonUtil.shipHistoryPop()" style="display: none">배송조회</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>환불수단</th>
                    <td>
                        <span class="text" id="refundFailReason" name="refundFailReason" style="width: 100%; font-weight: bold" hidden></span>
                        <span class="text" id="paymentType" name="paymentType" style="width: 100%" ></span>
                        <span class="text" id="refundType" name="refundType" style="width: 100%" ></span>
                    </td>
                </tr>
                </tbody>
            </div>
        </table>
    </div>
    <!-- 상세정보 end -->

    <div  class="com wrap-title sub" >
        <h3 class="title">• 환불금액 상세정보</h3>
        <span class="pull-right">
             <button type="button" class="ui button medium" id="discountInfoBtn" onclick="claimCommonUtil.discountInfoPopOpen()">차감 할인정보</button>
             <button type="button" class="ui button medium" id="deductPromoBtn" onclick="claimCommonUtil.promoInfoPopOpen()">차감 행사정보</button>
             <button type="button" class="ui button medium" id="cancelPromoBtn" onclick="claimCommonUtil.claimGiftInfoPop()">취소사은행사</button>
        </span>
    </div>
    <!-- 환불금액 상세정보 Layout -->
    <jsp:include page="layout/claimRefundInfoPopLayout.jsp"></jsp:include>
    <!-- 환불금액 상세정보 Layout end -->


    <!-- 처리내역 리스트 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 처리내역</h3>
    </div>
    <div class="topline"></div>
    <div id="claimProcessGrid" style="width: 100%; height: 200px;"></div>
    <!-- 처리내역 리스트 RealGrid end -->


    <!-- 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner" id="pickBtnGroup">
            <button class="ui button xlarge btn-danger font-malgun" id="reqPick" onclick="claimCommonUtil.pickReqPopOpen('Request')" style="display: none">수거요청</button>
            <button class="ui button xlarge btn-danger font-malgun" id="completePick" onclick="claimCommonUtil.pickPopOpen('Complete')" style="display: none">수거완료</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqApprove" onclick="claimCommonUtil.claimPopOpen('R','Approve')" style="display: none">반품승인</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqPending" onclick="claimCommonUtil.claimPopOpen('R','Pending')" style="display: none">반품보류</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqWithDraw" onclick="claimCommonUtil.claimPopOpen('R','Withdraw')" style="display: none">반품철회</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqReject" onclick="claimCommonUtil.claimPopOpen('R','Reject')" style="display: none">반품거부</button>
            <button class="ui button xlarge font-malgun" id="closeBtn">닫기</button>
        </span>

    </div>
    <!-- 버튼영역 end -->

    <!-- 히스토리 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 히스토리</h3>
    </div>
    <div class="topline"></div>
    <div id="claimHistoryGrid" style="width: 100%; height: 200px;"></div>
    <!-- 히스토리 RealGrid end-->

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimReturnDetail.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimCommonUtil.js?${fileVersion}"></script>

<script>
  ${claimDetailInfoListGridBaseInfo}
  ${claimHistoryGridBaseInfo}
  ${claimProcessGridBaseInfo}

  var clubUrl = '${clubUrl}'; // 클럽 페이지 URL
  var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
  var purchaseOrderNo = '${purchaseOrderNo}';
  var claimNo = '${claimNo}';
  var claimBundleNo = '${claimBundleNo}';
  var claimType = '${claimType}';
</script>
