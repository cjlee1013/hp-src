<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">교환완료</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
    <!-- 취소/반품/교환 사유 입력 -->
    <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
        <div class="ui form inline mg-l-10" >
            <span class="text mg-r-5" style="font-size: 15px">· 교환배송 중인 건을 강제로</span>
            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold">교환완료</span>
            <span class="text" style="font-size: 15px">처리 합니다.</span>
        </div>
        <div class="ui form inline mg-t-5 mg-l-10">
            <span class="text" style="font-size: 15px">· 배송방법이 업체직배송이거나, 택배사 트래킹이 정상적으로 동작하지 않을 경우 사용해 주세요.</span>
        </div>

        <input type="hidden" id="claimNo" name="claimNo" value="${claimNo}">
        <input type="hidden" id="claimBundleNo" name="claimBundleNo" value="${claimBundleNo}">
        <input type="hidden" id="claimReqType" name="claimReqType" value="CA">
        <input type="hidden" id="claimType" name="claimType" value="X">

    </div>
    <!-- 취소/반품/교환 사유 입력 end -->
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="claimReqBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<script>
    var claimNo = '${claimNo}';
    var claimBundleNo = '${claimBundleNo}';
    var claimExchShippingNo = '${claimExchShippingNo}';
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/exchCompletePop.js?v=${fileVersion}"></script>
