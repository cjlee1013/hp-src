<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-title has-border">
    <h2 class="title font-malgun">클레임정보 상세</h2>
</div>
<div  class="com wrap-title sub" >
    <h3 class="title">• 클레임정보</h3>
</div>

<!-- 클레임 기본정보 테이블 -->
<div class="ui wrap-table horizontal mg-t-10">
    <table class="ui table" id="claimDetailinfoTable">
        <caption>클레임 상세정보</caption>
        <colgroup>
            <col width="5%">
            <col width="10%">
            <col width="5%">
            <col width="10%">
            <col width="5%">
            <col width="12%">
            <col width="5%">
            <col width="12%">
            <col width="7%">
            <col width="10%">
            <col width="5%">
            <col width="*">
        </colgroup>
            <tbody>
            <tr>
                <th colspan="2">클레임 구분</th>
                <td colspan="2">
                    <span class="text" id="claimType" name="claimType" class="ui input border-none medium mg-r-5" style="width: 100%;"/>
                </td>
                <th colspan="2">그룹 클레임번호</th>
                <td colspan="2">
                    <span class="text" id="claimNo" name="claimNo" class="ui input border-none medium mg-r-5" style="width: 100%" />
                </td>
                <th colspan="2" id="claimBundleNoText">클레임번호</th>
                <td colspan="2">
                    <span class="text" id="claimBundleNo" name="claimBundleNo" class="ui input border-none medium mg-r-5" style="width: 100%" />
                </td>
            </tr>
            <tr>
                <th>주문번호</th>
                <td>
                    <span class="text" id="purchaseOrderNo" name="purchaseOrderNo" class="ui input border-none medium mg-r-5" style="width: 100%; cursor:pointer" />
                </td>
                <th>처리상태</th>
                <td>
                    <span class="text" id="paymentStatus" name="paymentStatus" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>주문일시</th>
                <td>
                    <span class="text" id="orderDt" name="orderDt" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>결제일시</th>
                <td>
                    <span class="text" id="paymentFshDt" name="paymentFshDt" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>주문디바이스</th>
                <td>
                    <span class="text" id="platform" name="platform" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>마켓연동</th>
                <td>
                    <span class="text" id="marketType" name="marketType" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
            </tr>
            <tr>
                <th>회원번호</th>
                <td>
                    <span class="text" id="userNo" name="userNo" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>구매자</th>
                <td>
                    <span class="text" id="buyerNm" name="buyerNm" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>구매자ID</th>
                <td>
                    <span class="text" id="buyerEmail" name="buyerEmail" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>휴대폰번호</th>
                <td>
                    <span class="text" id="buyerMobileNo" name="buyerMobileNo" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>합배송주문번호</th>
                <td>
                    <span class="text" id="orgPurchaseOrderNo" name="orgPurchaseOrderNo" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <th>마켓주문번호</th>
                <td>
                    <span class="text" id="marketOrderNo" name="marketOrderNo" class="ui input border-none medium mg-r-5" style="width: 100%;" />
                </td>
                <td hidden>
                    <span class="text" id="claimStatusCode" name="claimStatusCode"/>
                </td>
            </tr>
            </tbody>
    </table>
</div>
<!-- 클레임 상품 리스트 RealGrid -->
<div class="topline mg-t-15"></div>
<div id="claimDetailInfoListGrid" style="width: 100%; height: 300px;"></div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>