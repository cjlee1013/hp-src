<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">수거완료</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
    <!-- 취소/반품/교환 사유 입력 -->
    <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
        <div class="ui form inline mg-l-10" >
            <span class="text mg-r-5" style="font-size: 15px">· 선택한 클레임 건을</span>
            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold">수거를 완료처리</span>
            <span class="text" style="font-size: 15px">합니다.</span>
        </div>
    </div>
    <!-- 취소/반품/교환 사유 입력 end -->
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="reqBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<script>

    var param = {
        "claimNo" : ${claimNo},
        "claimPickShippingNo" : '${claimPickShippingNo}',
        "claimBundleNo" : '${claimBundleNo}',
        "bundleNo" : '${bundleNo}',
        "pickStatus" : 'P3'
    };

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/pickCompletePop.js?v=${fileVersion}"></script>
