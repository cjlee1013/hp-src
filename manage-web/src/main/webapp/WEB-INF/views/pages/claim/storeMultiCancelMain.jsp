<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">점포 일괄주문취소</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>점포일괄주문취소 검색</caption>
                    <colgroup>
                        <col width="8%">
                        <col width="10%">
                        <col width="5%">
                        <col width="8%">
                        <col width="15%">
                        <col width="8%">
                        <col width="15%">
                        <col width="7%">
                        <col width="10">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td colspan="4">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schDateType" name="schDateType" style="width:180px;">
                                    <option value="DELIVERY">배송 요청일</option>
                                    <option value="ORDER">주문일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="storeMultiCancel.initSearchDate('-1w');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="storeMultiCancel.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="storeMultiCancel.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                        <th>마켓연동</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium" style="width: 180px" id="schPartnerCd" name="schPartnerCd">
                                    <option value="ALL" selected>전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>점포유형</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select style="width: 150px" name="schStoreType" id="schStoreType" class="ui input medium mg-r-10" >
                                    <option value="HYPER">HYPER</option>
                                    <option value="CLUB">CLUB</option>
                                    <option value="EXP">EXPRESS</option>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input medium mg-r-10" style="width: 120px;" placeholder="점포ID" readonly>
                                <input type="text" id="schStoreName" name="schStoreName" class="ui input medium mg-r-10 mg-t-5" style="width:150px;" placeholder="점포명" readonly>
                                <button type="button" class="ui button medium mg-r-5 mg-t-5" id="schStoreBtn">조회</button>
                            </div>
                        </td>

                        <th>배송 shift</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schShiftId" name="schShiftId" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ALL">전체</option>
                                </select>
                            </div>
                        </td>
                        <th>배송상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schShipStatus" name="schShipStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="D1">신규주문</option>
                                    <option value="D2">상품준비중</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색조건</th>
                        <td colspan="6">
                            <div class="ui form inline">
                                <select id="schSearchType" name="schSearchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="PURCHASEORDERNO" selected>주문번호</option>
                                    <option value="BUNDLENO">배송번호</option>
                                    <option value="BUYERNM">구매자</option>
                                    <option value="RECEIVERNM">수령인</option>
                                    <option value="MARKETORDERNO">마켓주문번호</option>
                                </select>
                                <input type="text" id="schSearch" name="schSearch" class="ui input medium mg-r-5 mg-t-5" style="width: 200px;" >
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="storeMultiCancelTotalCount">0</span>건</h3>

            <span class="pull-right">
                <button class="ui button large blue font-malgun mg-r-30" id="reqMultiCancel" onclick="">일괄취소</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="storeMultiCancelListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/storeMultiCancelMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<script>
    ${storeMultiCancelListGridBaseInfo}

    var reqStoreMultiCancelList = [];
    var storeId = '${storeId}';
    var storeNm = '${storeNm}';
    var storeType = '${storeType}';
</script>