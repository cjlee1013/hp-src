<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">환불실패대상 조회</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="refundFailSearchForm" name="refundFailSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>환불실패대상 검색</caption>
                    <colgroup>
                        <col width="5%">
                        <col width="30%">
                        <col width="5%">
                        <col width="40%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="refundDateType" name="refundDateType" style="width:180px;">
                                    <option value="REQUEST">환불요청일</option>
                                    <option value="COMPLETE">환불완료일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="refundFail.initSearchDate('-3d');">3일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="refundFail.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="refundFail.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색조건</th>
                        <td>
                            <div class="ui form inline">
                                <select style="width: 180px" id="schClaimSearchType"  name="schClaimSearchType" class="ui input medium mg-r-10">
                                    <option value="PURCHASEORDERNO">주문번호</option>
                                    <option value="CLAIMNO">그룹클레임번호</option>
                                    <option value="CLAIMBUNDLENO">클레임번호</option>
                                </select>

                                <input type="text" id="schClaimSearch" name="schClaimSearch" class="ui input medium mg-r-5" style="width: 200px;" >
                            </div>
                        </td>
                        <th>구매자</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="schUserYn" value="Y" checked><span>회원</span>
                                </label>
                                <label class="ui radio inline">
                                    <input type="radio" name="schUserYn" value="N"><span>비회원</span>
                                </label>
                                <select style="width: 110px; display: none" name="schBuyerInfo" id="schBuyerInfo" class="ui input medium mg-r-10">
                                    <option value="BUYERNM">구매자 이름</option>
                                    <option value="BUYERMOBILE">구매자 연락처</option>
                                </select>
                                <input type="text" id="schUserSearch" name="schUserSearch" class="ui input medium mg-r-10" style="width: 150px;" readonly>
                                <button type="button" class="ui button medium mg-r-5" id="userSchButton" onclick="memberSearchPopup('refundFail.callbackMemberSearchPop'); return false;">조회</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>결제수단</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-10">
                                    <input type="checkbox" name="parentMethodCd" value="ALL" checked><span class="text text-guide mg-r-10 mg-t-5">전체</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="CARD" checked><span class="text text-guide mg-r-10 mg-t-5">신용카드</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="RBANK" checked><span class="text text-guide mg-r-10 mg-t-5">실시간계좌이체</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="KAKAOPAY" checked><span class="text text-guide mg-r-10 mg-t-5">카카오카드</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="NAVERPAY" checked><span class="text text-guide mg-r-10 mg-t-5">네이버페이</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="SAMSUNGPAY" checked><span class="text text-guide mg-r-10 mg-t-5">삼성페이</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="MHC" checked><span class="text text-guide mg-r-10 mg-t-5">마이홈플러스포인트</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="DIGITAL" checked><span class="text text-guide mg-r-10 mg-t-5">디지털상품권</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="MILEAGE" checked><span class="text text-guide mg-r-10 mg-t-5">마일리지</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="OCB" checked><span class="text text-guide mg-r-10 mg-t-5">오케이캐쉬백</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="PAYCO" checked><span class="text text-guide mg-r-10 mg-t-5">페이코</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="KAKAOMONEY" checked><span class="text text-guide mg-r-10 mg-t-5">카카오머니</span>
                                </label>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="refundFailTotalCount">0</span>건</h3>

            <span class="pull-right">
                <button class="ui button large blue font-malgun mg-r-30" id="reqRefundComplete" onclick="">환불완료</button>
            </span>
        </span>
        </div>
        <div class="topline"></div>
        <div id="refundFailListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/refundFailMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<script>
   ${refundFailListGridBaseInfo}

   var reqRefundCompleteList = [];
</script>