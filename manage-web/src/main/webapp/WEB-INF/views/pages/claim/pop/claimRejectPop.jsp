<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">${claimTypeName}거부</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
    <div class="ui form inline mg-t-10 mg-l-10 mg-b-10">
        <select id="reasonType" name="reasonType" class="ui input medium mg-r-20" style="width: 30%;">
            <option value="">거부사유 선택</option>
            <c:forEach var="codeDto" items="${claimReasonType}" varStatus="status">
                <c:if test="${codeDto.ref1 eq 'D'}">
                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                </c:if>
            </c:forEach>
        </select>
        <input type="text" id="reasonTypeDetail" name="reasonTypeDetail" class="ui input medium mg-r-5" style="width: 65%;" maxlength="100" placeholder="상세사유를 입력 (최대 100자)">
    </div>
    <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
        <div class="ui form inline mg-l-10" >
            <span class="text mg-r-5" style="font-size: 15px">· 선택한 클레임 건을</span>
            <span class="text mg-r-5" style="font-size: 15px; font-weight: bold">거부처리</span>
            <span class="text" style="font-size: 15px">합니다.</span>
        </div>
        <div class="ui form inline mg-t-5 mg-l-10">
            <span class="text" style="font-size: 15px">· 거부사유를 상세하게 작성해 주세요.</span>
        </div>
        <input type="hidden" id="claimNo" name="claimNo" value="${claimNo}">
        <input type="hidden" id="claimBundleNo" name="claimBundleNo" value="${claimBundleNo}">
        <input type="hidden" id="claimType" name="claimType" value="${claimType}">
        <input type="hidden" id="claimReqType" name="claimReqType" value="CD">
        <input type="hidden" id="regId" name="regId" value="ADMIN">
    </div>

    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="claimReqBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<script>
    var claimTypeName = '${claimTypeName}';

    var claimReasonTypeList = new Array();

    <c:forEach var="codeDto" items="${claimReasonType}">
        <c:if test="${codeDto.ref1 eq 'D'}">
            claimReasonTypeList.push({
                mcCd : "${codeDto.mcCd}",
                mcNm : "${codeDto.mcNm}",
                whoReason : "${codeDto.ref2}",
                detailReasonRequired : "${codeDto.ref4}"
            });
        </c:if>
    </c:forEach>

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/claimRejectPop.js?v=${fileVersion}"></script>
