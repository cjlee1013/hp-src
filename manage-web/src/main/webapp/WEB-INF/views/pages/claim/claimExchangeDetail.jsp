<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">

    <!-- 클레임 정보 Layout -->
    <jsp:include page="layout/claimDetailPopLayout.jsp"></jsp:include>
    <!-- 클레임정보 Layout end -->

    <!-- 상세정보 -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 상세정보</h3>
        <span class="pull-right">
            <button type="button" class="ui button medium" id="chgPickShipTypeBtn" onclick="claimCommonUtil.chgDeliveryType('1')" style="display: none">수거방법변경</button>
            <button type="button" class="ui button medium" id="chgShipTypeBtn" onclick="claimCommonUtil.chgDeliveryType('2')" style="display: none">배송방법변경</button>
            <button type="button" class="ui button medium" id="sellerInfoBtn" onclick="claimCommonUtil.sellerInfo() ">판매업체정보</button>
        </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table" id="pickDetailInfoTable">
            <caption>교환상세정보</caption>
            <colgroup>
                <col width="10%">
                <col width="*">
            </colgroup>
            <div class="ui form inline">
                <tbody>
                <tr>
                    <th>교환사유</th>
                    <td>
                        <span class="text" style="color: #f90000">[</span>
                        <span class="text" id="claimReasonType" name="claimReasonType" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" style="color: #f90000">]</span>
                        <span class="text" style="color: #f90000">  </span>
                        <span class="text" id="deductPromoYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductDiscountYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductShipYn" style="color: #f90000; display: none"></span>
                        <br>
                        <span class="text" id="claimReasonDetail" name="claimReasonTypeDetail" style="color: #f90000"></span>
                        <span class="text" id="claimReasonTypeCode" name="claimReasonTypeCode" hidden></span>
                    </td>
                </tr>
                <tr>
                    <th>교환배송비</th>
                    <td>
                        <span class="text" id="returnShipAmt" name="returnShipAmt" style="color: #f90000"></span>
                        <span class="text" id="returnShipAmtText" name="returnShipAmtText" style="color: #f90000"></span>
                        <span class="text" style="color: #f90000">&nbsp|&nbsp</span>
                        <span class="text" id="whoReason" name="whoReason" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" id="claimShipFeeEnclose" name="claimShipFeeEnclose" style="color: #f90000"></span>
                    </td>
                </tr>
                <tr id="uploadRow">
                    <th>첨부파일</th>
                    <td>
                        <a id="uploadFileName" target="_blank"><span>첨부파일1</span><input type="hidden" id="uploadFileNameUrl"/><input type="hidden" id="uploadFileNameType"/></a>&nbsp
                        <a id="uploadFileName2" target="_blank"><span>첨부파일2</span><input type="hidden" id="uploadFileNameUrl2"/><input type="hidden" id="uploadFileNameType2"/></a>&nbsp
                        <a id="uploadFileName3" target="_blank"><span>첨부파일3</span><input type="hidden" id="uploadFileNameUrl3"/><input type="hidden" id="uploadFileNameType3"/></a>
                    </td>
                </tr>
                <tr id="pickAddrRow1">
                    <th rowspan="2">교환수거지</th>
                    <td>
                        <span class="text" id="pickReceiverNm" name="pickReceiverNm"></span>
                        <span class="text">  &#10072;  </span>
                        <span class="text" id="pickMobileNo" name="pickMobileNo"></span>
                        <span class="text" id="claimPickShippingNo" name="claimPickShippingNo" hidden></span>
                        <span class="text" id="pickStatus" name="pickStatus" hidden></span>
                    </td>
                </tr>
                <tr id="pickAddrRow2">
                    <td>
                        <div class="ui form inline">
                            <span class="text">(</span>
                            <span class="text" id="pickZipcode" name="pickZipcode"></span>
                            <span class="text">) </span>
                            <span class="text" id="pickAddr" name="pickAddr"></span>&nbsp;
                            <span class="text">&nbsp</span>
                            <span class="text" id="pickAddrDetail" name="pickAddrDetail"></span>&nbsp;
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>수거방법</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="pickStatusText" name="pickStatusText"></span>
                        </div>

                        <div class="ui form inline">
                            <span class="text" id="pickShipText" name="pickShipText"></span>
                            <span class="text">&nbsp&nbsp</span>
                            <input type="hidden" id="pickInvoiceNo" name="pickInvoiceNo"/>
                            <input type="hidden" id="pickDlv" name="pickDlv"/>
                            <button type="button" class="ui button medium gray-light" id="shipSearchBtn" onclick="claimCommonUtil.shipHistoryPop()" style="display: none">배송조회</button>
                        </div>
                    </td>
                </tr>
            </tbody>
            </div>
        </table>
        <table class="ui table" id="exchangeDetailInfoTable">
            <caption>교환상세정보</caption>
            <colgroup>
                <col width="10%">
                <col width="*">
            </colgroup>
            <div class="ui form inline">
                <tbody>
                <tr style="border-top: 1px solid #eee;" id="exchAddrRow1">
                    <th rowspan="2">교환배송지</th>
                    <td>
                        <span class="text" id="exchReceiverNm" name="receiveNm"></span>
                        <span class="text">  |  </span>
                        <span class="text" id="exchMobileNo" name="receiveMobile"></span>
                        <span class="text" id="claimExchShippingNo" name="claimExchShippingNo" hidden></span>
                        <span class="text" id="exchStatusCode" name="exchStatusCode" hidden></span>
                    </td>
                </tr>
                <tr id="exchAddrRow2">
                    <td>
                        <div class="ui form inline">
                            <span class="text">(</span>
                            <span class="text" id="exchZipcode" name="exchZipcode"></span>
                            <span class="text">) </span>
                            <span class="text" id="exchAddr" name="exchAddr"></span>&nbsp;
                            <span class="text">&nbsp</span>
                            <span class="text" id="exchAddrDetail" name="exchAddrDetail"></span>&nbsp;
                        </div>
                    </td>
                </tr>
                <tr id="exchShipTextRow">
                    <th>배송방법</th>
                    <td>
                        <div class="ui form inline">
                            <span class="text" id="exchStatusText" name="exchStatusText"></span>
                        </div>

                        <div class="ui form inline">
                            <span class="text" id="exchShipText" name="exchShipText"></span>
                            <span class="text">&nbsp&nbsp</span>
                            <input type="hidden" id="exchInvoiceNo" name="exchInvoiceNo"/>
                            <input type="hidden" id="exchDlv" name="exchDlv"/>
                            <button type="button" class="ui button medium gray-light" id="exchShipSearchBtn" onclick="claimCommonUtil.exchShipHistoryPop()" style="display: none">배송조회</button>
                        </div>
                    </td>
                </tr>
                <tr id="pendingReason" hidden>
                    <th>교환보류사유</th>
                    <td>
                        <span class="text">[</span>
                        <span class="text" id="pendingReasonType" name="pendingReasonType" style="font-weight: bold;"></span>
                        <span class="text">]</span><br>
                        <span class="text" id="pendingReasonDetail" name="pendingReasonDetail"></span>
                    </td>
                </tr>
                <tr id="rejectReason" hidden>
                    <th>교환거부사유</th>
                    <td>
                        <span class="text">[</span>
                        <span class="text" id="rejectReasonType" name="rejectReasonType" style="font-weight: bold;"></span>
                        <span class="text">]</span><br>
                        <span class="text" id="rejectReasonDetail" name="rejectReasonTypeDetail"></span>
                    </td>
                </tr>
                </tbody>
            </div>
        </table>
    </div>
    <!-- 상세정보 end -->

    <!-- 처리내역 리스트 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 처리내역</h3>
    </div>
    <div class="topline"></div>
    <div id="claimProcessGrid" style="width: 100%; height: 200px;"></div>
    <!-- 처리내역 리스트 RealGrid end -->


    <!-- 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="reqPick" onclick="claimCommonUtil.pickReqPopOpen('Request')" style="display: none">수거요청</button>
            <button class="ui button xlarge btn-danger font-malgun" id="completePick" onclick="claimCommonUtil.pickPopOpen('Complete')" style="display: none">수거완료</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqApprove" onclick="claimCommonUtil.exchShippingPop()" style="display: none">교환배송</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqComplete" onclick="claimCommonUtil.exchCompletePop()" style="display: none">교환완료</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqPending" onclick="claimCommonUtil.claimPopOpen('X','Pending')" style="display: none">교환보류</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqWithDraw" onclick="claimCommonUtil.claimPopOpen('X','Withdraw')" style="display: none">교환철회</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqReject" onclick="claimCommonUtil.claimPopOpen('X','Reject')" style="display: none">교환거부</button>
            <button class="ui button xlarge font-malgun" id="closeBtn">닫기</button>
        </span>
    </div>
    <!-- 버튼영역 end -->

    <!-- 히스토리 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 히스토리</h3>
    </div>
    <div class="topline"></div>
    <div id="claimHistoryGrid" style="width: 100%; height: 200px;"></div>
    <!-- 히스토리 RealGrid end-->

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimExchangeDetail.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimCommonUtil.js?${fileVersion}"></script>

<script>
  ${claimDetailInfoListGridBaseInfo}
  ${claimHistoryGridBaseInfo}
  ${claimProcessGridBaseInfo}

  var clubUrl = '${clubUrl}'; // 클럽 페이지 URL
  var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
  var purchaseOrderNo = '${purchaseOrderNo}';
  var claimNo = '${claimNo}';
  var claimBundleNo = '${claimBundleNo}';
  var claimType = '${claimType}';
</script>
