<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">클레임 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="claimSearchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>클레임 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="20%">
                        <col width="13%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>구분</th>
                        <td colspan="4">
                            <div class="ui form inline">
                                <select style="width: 180px" id="schClaimType" name="schClaimType" class="ui input medium  mg-r-10 mg-t-5">
                                    <c:forEach var="codeDto" items="${claimType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <select style="width: 180px"  id="schClaimStatus" name="schClaimStatus" class="ui input medium mg-r-10 mg-t-5">
                                    <option value="ALL">처리상태전체</option>
                                </select>
                                <select style="width: 180px" id="schPickupStatus"  name="schPickupStatus" class="ui input medium mg-r-10 mg-t-5" disabled>
                                    <option value="ALL">수거배송상태전체</option>
                                    <option value="NN">수거대기</option>
                                    <option value="P0">수거요청 완료</option>
                                    <option value="P1">수거예정</option>
                                    <option value="P2">수거 중</option>
                                    <option value="P3">수거완료</option>
                                    <option value="P8">수거실패</option>
                                </select>
                                <select style="width: 180px" id="schExchStatus"  name="schExchStatus" class="ui input medium mg-r-10 mg-t-5" disabled>
                                    <option value="ALL">교환배송상태전체</option>
                                    <c:forEach var="codeDto" items="${exchShippingStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>마켓연동</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium" style="width: 180px" id="partnerCd" name="partnerCd">
                                    <option value="ALL" selected>전체</option>
                                    <option value="naver">N마트</option>
                                    <option value="eleven">11번가</option>
                                </select>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색기간</th>
                        <td colspan="6">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="claimDateType" name="claimDateType" style="width:180px;">
                                    <c:forEach var="codeDto" items="${claimDateType}" varStatus="status">
                                        <c:set var="selected" value=""/>
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected"/>
                                        </c:if>
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="claim.initSearchDate('-1w');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="claim.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="claim.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>구매자</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="schUserYn" value="Y" checked><span>회원</span>
                                </label>
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="schUserYn" value="N"><span>비회원</span>
                                </label>
                                <select style="width: 110px; display: none" name="schBuyerInfo" id="schBuyerInfo" class="ui input medium mg-r-10 mg-t-5">
                                    <option value="BUYERNM">구매자 이름</option>
                                    <option value="BUYERMOBILE">구매자 연락처</option>
                                    <option value="BUYERMAIL">구매자 메일</option>
                                </select>
                                <input type="text" id="schUserSearch" name="schUserSearch" class="ui input medium mg-r-10 mg-t-5" style="width: 150px;" readonly>
                                <button type="button" class="ui button medium mg-r-5" id="userSchButton" onclick="memberSearchPopup('claim.callbackMemberSearchPop'); return false;">조회</button>
                            </div>
                        </td>

                        <th>점포/판매자</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select style="width: 150px" name="storeType" id="storeType" class="ui input medium mg-r-10" >
                                    <option value="ALL">전체</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schPartnerId" name="schPartnerId" class="ui input medium mg-r-10" style="width: 150px;" placeholder="판매업체ID" readonly>
                                <input type="text" id="schPartnerName" name="schPartnerName" class="ui input medium mg-r-10 mg-t-5" style="width:180px;" placeholder="판매업체명" readonly>
                                <button type="button" class="ui button medium mg-r-5 mg-t-5" id="schPartnerBtn" disabled>조회</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="claimSearchType" name="claimSearchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <c:forEach var="codeDto" items="${claimSearchType}" varStatus="status">
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schClaimSearch" name="schClaimSearch" class="ui input medium mg-r-5 mg-t-5" style="width: 200px;" >
                            </div>
                        </td>
                        <th>신청채널</th>
                        <td>
                            <div class="ui form inline">
                                <select style="width: 120px" name="schClaimChannel" id="schClaimChannel" class="ui input medium mg-r-10" >
                                    <option value="ALL">전체</option>
                                    <option value="MYPAGE">구매자</option>
                                    <option value="ADMIN">ADMIN</option>
                                    <option value="STORE">STORE</option>
                                    <option value="PARTNER">PARTNER</option>
                                    <option value="OMNI">OMNI</option>
                                    <option value="SYSTEM">SYSTEM</option>
                                    <option value="M-ADMIN">M-ADMIN</option>
                                    <option value="M-PARTNER">M-PARTNER</option>
                                    <option value="M-FRONT">M-FRONT</option>
                                </select>
                            </div>
                        </td>
                        <th>배송유형</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-5">
                                    <input type="checkbox" name="schReserveShipMethod" id="shipTypeDS" value="RESERVE_DRCT_DLV"><span class="text text-guide mg-l-5">명절선물세트</span>
                                </label>
                                <label class="ui checkbox mg-l-5">
                                    <input type="checkbox" name="schReserveShipMethod" id="shipTypeTD" value="RESERVE_TD_DRCT"><span class="text text-guide mg-l-5">절임배추</span>
                                </label>
                                <label class="ui checkbox mg-l-5">
                                    <input type="checkbox" name="schReserveShipMethod" id="shipTypeItem" value="RESERVE_DRCT_NOR"><span class="text text-guide mg-l-5">일반상품</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="claimTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="claimListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<script>
  // 클레임 리스트 리얼그리드 기본 정보
  ${claimListGridBaseInfo}
  var claimStatisList = new Array();
  var storeId = '${storeId}';
  var storeNm = '${storeNm}';
  var soStoreType = '${soStoreType}';

  <c:forEach var="codeDto" items="${claimStatus}">
      claimStatisList.push({
          mcCd : "${codeDto.mcCd}",
          mcNm : "${codeDto.mcNm}",
          ref1 : "${codeDto.ref1}"
          }
      );
  </c:forEach>
</script>