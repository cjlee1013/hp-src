<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">환불수단</h5>
    </div>
    <form id="claimApproveForm" onsubmit="return false;">
        <div class="ui wrap-table mg-t-15" style="padding-top: 20px; padding-bottom: 20px; padding-left: 10px">
            <span class="text mg-r-5" style="font-size: 15px">&#8251 환불처리가 실패하였습니다. 환불수단을 변경해주세요.</span><br>
            <span class="text mg-r-5" style="font-size: 15px">&#8226 계좌입금</span>
        </div>
        <div class="ui wrap-table mg-t-15">
            <table>
                <colgroup>
                    <col width="15%">
                    <col width="20%">
                    <col width="*">
                    <col width="15%">
                </colgroup>
                <tr style="height: 50px;">
                    <th style="border: 0; border-right: 1px solid #ddd; font-size: 14px">계좌정보</th>
                    <td>
                        <div class="ui form inline">
                            <select id="selectBankType" name="selectBankType" class="ui input medium mg-l-20 mg-r-20" style="width: 80%">
                                <option value="">은행선택</option>
                                <c:forEach var="codeDto" items="${bankCode}" varStatus="status">
                                    <c:set var="selected" value=""/>
                                    <c:if test="${codeDto.ref1 eq 'df'}">
                                        <c:set var="selected" value="selected"/>
                                    </c:if>
                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="bankAccountNo" name="bankAccountNo" class="ui input medium mg-r-5" style="width: 55%" placeholder="환불계좌번호">
                            <input type="text" id="bankAccountName" name="bankAccountName" class="ui input medium mg-r-5" style="width: 35%;" value="${buyerNm}" disabled>
                        </div>
                    </td>
                    <td>
                        <button class="ui button large btn-gray-light font-malgun" id="bankAccountAuth">계좌인증</button>
                    </td>
                </tr>
            </table>

            <input type="hidden" id="claimNo" name="claimNo" value="${claimNo}">
            <input type="hidden" id="claimBundleNo" name="claimBundleNo" value="${claimBundleNo}">
            <input type="hidden" id="bundleNo" name="bundleNo" value="${bundleNo}">
            <input type="hidden" id="claimType" name="claimType" value="${claimType}">
            <input type="hidden" id="claimStatus" name="claimStatus" value="C2">
        </div>
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="claimApproveBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/claimApprovePop.js?v=${fileVersion}"></script>
