<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">

    <!-- 클레임 정보 Layout -->
    <jsp:include page="layout/claimDetailPopLayout.jsp"></jsp:include>
    <!-- 클레임정보 Layout end -->

    <!-- 상세정보 -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 상세정보</h3>
        <span class="pull-right">
            <button type="button" class="ui button medium" id="sellerInfoBtn" onclick="claimCommonUtil.sellerInfo()">판매업체정보</button>
        </span>
    </div>
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table" id="cancelDetailInfoTable">
            <caption>취소상세정보</caption>
            <colgroup>
                <col width="10%">
                <col width="*">
            </colgroup>
            <div class="ui form inline">
                <tbody>
                <tr>
                    <th rowspan="2">취소사유</th>
                    <td>
                        <span class="text" style="color: #f90000">[</span>
                        <span class="text" id="claimReasonType" name="claimReasonType" style="font-weight: bold; color: #f90000"></span>
                        <span class="text" style="color: #f90000">]</span>
                        <span class="text" style="color: #f90000">  </span>
                        <span class="text" id="deductPromoYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductDiscountYn" style="color: #f90000; display: none"></span>
                        <span class="text" id="deductShipYn" style="color: #f90000; display: none"></span>
                        <br>
                        <span class="text" id="claimReasonDetail" name="claimReasonTypeDetail"></span>
                    </td>
                </tr>
                <tr>
                    <td id="deductRow" hidden>
                        <label class="ui checkbox inline">
                            <input type="checkbox" id="searchClaimReason" name="searchClaimReason" disabled/>
                            <span class="text mg-r-20">할인 미차감</span>
                        </label>
                        <label class="ui checkbox inline">
                            <input type="checkbox" id="searchClaimDeduct" name="searchClaimDeduct" disabled/>
                            <span class="text mg-r-20">배송비 미차감</span>
                        </label>
                    </td>
                </tr>
                <tr id="pendingReason" hidden>
                    <th>취소보류사유</th>
                    <td>
                        [<span class="text" id="pendingReasonType" name="claimReasonType" style="font-weight: bold;"></span>]<br>
                        <span class="text" id="pendingReasonDetail" name="pendingReasonDetail"></span>
                    </td>
                </tr>
                <tr id="rejectReason" hidden>
                    <th>취소거절사유</th>
                    <td>
                        <span class="text" id="refundFailReason" name="refundFailReason" style="width: 100%; font-weight: bold" hidden></span>
                        <span class="text" id="paymentType" name="paymentType" style="width: 100%" ></span>
                        <span class="text" id="refundType" name="refundType" style="width: 100%" ></span>
                    </td>
                </tr>
                </tbody>
            </div>
        </table>
    </div>
    <!-- 상세정보 end -->

    <div  class="com wrap-title sub" >
        <h3 class="title">• 환불금액 상세정보</h3>
        <span class="pull-right">
             <button type="button" class="ui button medium" id="discountInfoBtn" onclick="claimCommonUtil.discountInfoPopOpen()">차감 할인정보</button>
             <button type="button" class="ui button medium" id="deductPromoBtn" onclick="claimCommonUtil.promoInfoPopOpen()">차감 행사정보</button>
             <button type="button" class="ui button medium" id="cancelPromoBtn" onclick="claimCommonUtil.claimGiftInfoPop()">취소사은행사</button>
        </span>
    </div>
    <!-- 환불금액 상세정보 Layout -->
    <jsp:include page="layout/claimRefundInfoPopLayout.jsp"></jsp:include>
    <!-- 환불금액 상세정보 Layout end -->


    <!-- 처리내역 리스트 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 처리내역</h3>
    </div>
    <div class="topline"></div>
    <div id="claimProcessGrid" style="width: 100%; height: 200px;"></div>
    <!-- 처리내역 리스트 RealGrid end -->


    <!-- 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="reqApprove" onclick="claimCommonUtil.claimPopOpen('C','Approve')" style="display: none">취소승인</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqPending" onclick="claimCommonUtil.claimPopOpen('C','Pending')" style="display: none">취소보류</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqWithDraw" onclick="claimCommonUtil.claimPopOpen('C','Withdraw')" style="display: none">취소철회</button>
            <button class="ui button xlarge btn-danger font-malgun" id="reqReject" onclick="claimCommonUtil.claimPopOpen('C','Reject')" style="display: none">취소거부</button>
            <button class="ui button xlarge font-malgun" id="closeBtn">닫기</button>
        </span>
    </div>
    <!-- 버튼영역 end -->

    <!-- 히스토리 RealGrid -->
    <div  class="com wrap-title sub" >
        <h3 class="title">• 히스토리</h3>
    </div>
    <div class="topline"></div>
    <div id="claimHistoryGrid" style="width: 100%; height: 200px;"></div>
    <!-- 히스토리 RealGrid end-->

</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimCancelDetail.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/claimCommonUtil.js?${fileVersion}"></script>

<script>
  ${claimDetailInfoListGridBaseInfo}
  ${claimHistoryGridBaseInfo}
  ${claimProcessGridBaseInfo}

  const clubUrl = '${clubUrl}'; // 클럽 페이지 URL
  const frontUrl = '${frontUrl}'; // 프론트 페이지 URL
  const purchaseOrderNo = '${purchaseOrderNo}';
  const claimNo = '${claimNo}';
  const claimBundleNo = '${claimBundleNo}';
  const claimType = '${claimType}';
</script>
