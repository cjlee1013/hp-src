<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">결품/대체상품 조회</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>결품/대체상품 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="20%">
                        <col width="13%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td colspan="5">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="schDateType" name="schDateType" style="width:180px;">
                                    <option value="DELIVERY" selected>배송요청일</option>
                                    <option value="ORDER">주문일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:180px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="outOfStockItem.initSearchDate('-3d');">3일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="outOfStockItem.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="outOfStockItem.initSearchDate('0');">오늘</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="resetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>점포유형</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select style="width: 150px" name="schStoreType" id="schStoreType" class="ui input medium mg-r-10" >
                                    <option value="ALL">전체</option>
                                    <option value="HYPER">HYPER</option>
                                    <option value="AURORA">AURORA</option>
                                    <option value="EXP">EXPRESS</option>
                                    <option value="CLUB">CLUB</option>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input medium mg-r-10" style="width: 150px;" placeholder="점포ID" readonly>
                                <input type="text" id="schStoreName" name="schStoreName" class="ui input medium mg-r-10 mg-t-5" style="width:180px;" placeholder="점포명" readonly>
                                <button type="button" class="ui button medium mg-r-5 mg-t-5" id="schStoreBtn" disabled>조회</button>
                            </div>
                        </td>

                        <th>배송 shift</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schShiftId" name="schShiftId" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ALL">전체</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>대체여부</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select class="ui input medium" style="width: 180px" id="schSubstitutionYn" name="schSubstitutionYn">
                                    <option value="ALL" selected>전체</option>
                                    <option value="N">대체허용</option>
                                    <option value="Y">대체안함</option>
                                </select>
                            </div>
                        </td>
                        <th>대체/취소상태</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select id="schSubStatus" name="schSubStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ALL" selected>전체</option>
                                    <option value="P4">대체없음</option>
                                    <option value="P5">대체완료</option>
                                </select>
                                <select id="schRefundStatus" name="schRefundStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="ALL" selected>전체</option>
                                    <option value="C4">취소실패</option>
                                    <option value="C9">취소거부</option>
                                    <option value="C3">취소완료</option>
                                    <option value="C0">-</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상세검색</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <select id="schSearchType" name="schSearchType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="PURCHASEORDERNO" selected>주문번호</option>
                                    <option value="ITEMNO">주문상품번호</option>
                                    <option value="SUBITEMNO">대체상품번호</option>
                                </select>
                                <input type="text" id="schSearch" name="schSearch" class="ui input medium mg-r-5 mg-t-5" style="width: 200px;" >
                            </div>
                        </td>
                        <th>단축번호</th>
                        <td colspan="2">
                            <div class="ui form inline">
                                <input type="text" id="schStartSordNo" name="schStartSordNo" class="ui input medium mg-r-5" style="width:100px;" maxlength="5">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndSordNo" name="schEndSordNo" class="ui input medium mg-r-5" style="width:100px;" maxlength="5">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="outOfStockItemTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="outOfStockItemListGrid" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="topline"></div>
</div>


</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/outOfStockItemMain.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>

<script>
  ${outOfStockItemListGridBaseInfo}
  var storeId = '${storeId}';
  var storeNm = '${storeNm}';
  var storeType = '${storeType}';
</script>