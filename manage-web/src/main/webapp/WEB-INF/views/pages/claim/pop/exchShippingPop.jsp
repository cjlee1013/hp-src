<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup xlarge" style="height: 100%; background-color: #FFFFFF; width: 100%; overflow: auto">
    <div class="com popup-wrap-title mg-t-20 mg-l-20">
        <h5 class="title font-malgun" style="font-size: 20px">교환배송</h5>
    </div>
    <form id="exchShippingForm" onsubmit="return false;" class="mg-l-20 mg-r-20">
        <div class="ui wrap-table mg-t-15">
            <table class="ui table" id="exchShippingTable">
                <colgroup>
                    <col width="15%">
                    <col width="*">
                </colgroup>
                <tr style="height: 50px;">
                    <th>
                        *배송방법
                    </th>
                    <td id="pickWayDS" hidden>
                        <!-- 택배사 정보 입력 테이블 -->
                        <table id="courierInfoTable">
                            <caption>신청정보 테이블</caption>
                            <colgroup>
                                <col width="20%">
                                <col width="20%">
                                <col width="*%">
                            </colgroup>
                            <tr>
                                <td style="text-align: left">
                                    <select id="exchShipType" name="exchShipType" class="ui input medium mg-r-20">
                                        <option value="C">택배</option>
                                        <option value="D">직접배송</option>
                                        <option value="P">우편배송</option>
                                        <option value="N">배송없음</option>
                                    </select>
                                </td>
                                <td colspan="2" style="text-align: left">
                                    <div class="ui form inline"  id="parcelDiv" style="display: none">
                                        <select id="exchDlvCd" name="exchDlvCd" class="ui input medium mg-r-5" style="width: 30%">
                                            <option value="">택배사선택</option>
                                                <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                                    <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                                </c:forEach>
                                        </select>
                                        <input type="text" id="exchInvoiceNo" name="exchInvoiceNo" class="ui input medium mg-r-5" maxlength="30" style="width: 60%" placeholder="송장번호 입력">
                                    </div>
                                    <div class="ui form inline"  id="pickDiv" style="display: none">
                                        <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="pickDateText">배송예정일 : </span>
                                        <input type="text" id="exchD0Dt" name="exchD0Dt" class="ui input medium mg-r-5" placeholder="배송예정일" style="width:180px;">

                                    </div>
                                    <div class="ui form inline"  id="postDiv" style="display: none">
                                        <input type="text" id="exchMemo" name="exchMemo" class="ui input medium mg-r-5" maxlength="50" style="width: 100%" placeholder="메모">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                               <td colspan="3" style="text-align: left">
                                   <span class="text mg-r-5" style="font-size: 13px" id="text1"></span>
                                   <span class="text mg-r-5" style="font-size: 13px" id="text2"></span>
                                   <span class="text mg-r-5" style="font-size: 13px" id="text3"></span>
                               </td>
                            </tr>
                        </table>
                        <!-- 택배사 정보 입력 테이블 end -->
                    </td>
                    <td id="pickWayTD" hidden style="text-align: left">
                        <div class="ui form inline mg-b-10">
                            <span class="text" style="font-size: 12px;">- 배송점포 : 홈플러스 </span>
                            <span class="text" id="storeNm" name="storeNm" style="font-size: 12px;"></span>
                        </div>
                        <span class="text" style="font-size: 12px;">- 배송방법 : </span>
                        <label class="ui radio inline mg-l-10" id="tdRadioLabel" style="display: none">
                            <input type="radio" id="tdRadio" name="selectDeliveryType" class="ui input small" value="D"><span>자차</span>
                        </label>
                        <label class="ui radio inline" id="pickRadioLabel" style="display: none">
                            <input type="radio" id="pickRadio" name="selectDeliveryType" class="ui input small" value="D"><span>픽업</span>
                        </label>
                        <label class="ui radio inline" id="quickRadioLabel" style="display: none">
                            <input type="radio" id="quickRadio" name="selectDeliveryType" class="ui input small" value="D"><span>퀵</span>
                        </label>
                        <div class="ui form inline mg-t-10" id="pickPlace" style="display: none">
                            <span class="text" id="placeNm" name="placeNm"></span>
                        </div>
                        <div class="ui form inline mg-t-10" id="slotDiv">

                            <span class="text" style="font-size: 12px;">- 방문시간 선택</span>
                            <div class="tbl-time mg-t-10" id="slotChangeArea"/>
                        </div>
                    </td>
                </tr>

                <tr style="height: 50px;" id="exchShippingRow">
                    <th>*교환배송지</th>
                    <td colspan="2">
                        <table class="ui table">
                            <div style="text-align: left; display: none" class="mg-t-10 mg-b-20" id="tdWayTitle">
                                <span class="text" id="" style="font-size: 14px;">&#8251; 원 주문의 점포지 기준으로 배송이 진행됩니다.</span>
                            </div>
                            <tr>
                                <th style="width: 100px;">*이름</th>
                                <td>
                                    <input type="text" id="exchReceiverNm" name="exchReceiverNm" class="ui input medium mg-r-5" style="width: 40%;" placeholder="이름을 입력해주세요" value="${param.name}" maxlength="20">
                                </td>
                            </tr>
                            <tr>
                                <th>*연락처</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                        <input type="hidden" name="exchMobileNo" id="exchMobileNo">
                                        <input type="text" name="exchMobileNo_1" id="exchMobileNo_1" class="ui input medium" style="width: 65px;" title="연락처" maxlength="3">
                                        <span class="text">-</span>
                                        <input type="text" name="exchMobileNo_2" id="exchMobileNo_2" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                                        <span class="text">-</span>
                                        <input type="text" name="exchMobileNo_3" id="exchMobileNo_3" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>*주소</th>
                                <td style="text-align: left">
                                    <div class="ui form inline">
                                    <input type="text" name="exchZipcode" id="exchZipcode" class="ui input medium mg-r-5" style="width:100px;" readonly><button type="button" class="ui button small" onclick="javascript:zipCodePopup('exchShipping.setZipcode'); return false;">우편번호 찾기</button>
                                    <br>
                                    <input type="text" name="exchAddr" id="exchAddr" class="ui input medium mg-t-5 mg-r-5" style="width:300px;" readonly>
                                    <input type="hidden" name="exchBaseAddr" id="exchBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;">
                                    <input type="text" name="exchAddrDetail" id="exchAddrDetail" class="ui input medium mg-t-5" style="width:240px;" placeholder="상세주소를 입력해주세요" maxlength="100">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="bundleNo" name="bundleNo">
        <input type="hidden" id="purchaseOrderNo" name="purchaseOrderNo">
        <input type="hidden" id="claimExchShippingNo" name="claimExchShippingNo">

    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge btn-danger font-malgun" id="chgDeliveryBtn">저장</button>
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>

    var claimNo = '${claimNo}';
    var claimBundleNo = '${claimBundleNo}';
    var shipType = '';
    var shipMethod = '';
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/exchShippingPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
