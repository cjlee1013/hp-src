<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%; background-color: #FFFFFF; overflow: auto">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">판매업체 정보</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
    <!-- 취소/반품/교환 사유 입력 -->
    <div class="tabui-wrap mg-t-10">
        <!-- tab-cont -->
        <table class="ui table mg-b-20" id="partnerTable">
            <tbody>
            <tr>
                <th style="width: 80px; text-align: left;">판매업체명</th>
                <td style="text-align: left">
                    <span class="text" id="partnerNm"></span>
                </td>
                <th style="width: 80px; text-align: left;">판매업체ID</th>
                <td style="text-align: left">
                    <span class="text" id="partnerId"></span>
                </td>
            </tr>
            <tr>
                <th style="width: 80px; text-align: left;">담당자</th>
                <td style="text-align: left">
                    <span class="text" id="mngNm"></span>
                </td>
                <th style="width: 80px; text-align: left;">담당자 연락처</th>
                <td style="text-align: left">
                    <span class="text" id="mngMobile"></span>
                </td>
            </tr>
            <tr>
                <th style="text-align: left">업체출고지</th>
                <td style="text-align: left" colspan="3">
                    <div class="ui form inline">
                        <span class="text" id="zipcode" name="zipcode"></span>
                        <span class="text" id="addr1" name="addr1"></span>
                        <span class="text" id="addr2" name="addr2"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <th style="text-align: left; border-bottom: solid; border-width: 1px; border-color:#eee;">업체회수지</th>
                <td style="text-align: left; border-bottom: solid; border-width: 1px; border-color:#eee;" colspan="3">
                    <div class="ui form inline" >
                        <span class="text" name="zipcode"></span>
                        <span class="text" name="addr1"></span>
                        <span class="text" name="addr2"></span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- //bottom -->
    </div>
    <!-- 취소/반품/교환 사유 입력 end -->
    </form>
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<script>
    var purchaseOrderNo = '${purchaseOrderNo}';
    var claimNo = '${claimNo}';

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/sellerInfoPop.js?v=${fileVersion}"></script>

