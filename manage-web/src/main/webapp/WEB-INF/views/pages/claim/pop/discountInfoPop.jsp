<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup xlarge" style="height: 100%;">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">차감할인정보</h5>
    </div>
    <form id="claimReqForm" onsubmit="return false;">
        <!-- 취소/반품/교환 사유 입력 -->
        <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
            <div class="ui form inline mg-l-10" >
                <span class="text mg-r-5" style="font-size: 15px">총 할인금액 : </span>
                <span class="text mg-r-5" style="font-size: 15px; font-weight: bold" id="totalDiscountAmt"></span>
                <span class="text mg-r-5" style="font-size: 15px; display: none" id="leftBrc">(</span>
                <span class="text" style="font-size: 15px" id="discountText"></span>
                <span class="text mg-r-5" style="font-size: 15px; display: none" id="rightBrc"> )</span>
            </div>
        </div>
    </form>
    <div id="discountInfoListGrid" style="width: 100%; height: 200px;"></div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script>
    ${discountInfoListGridBaseInfo};
    var claimNo = '${claimNo}';
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/discountInfoPop.js?v=${fileVersion}"></script>
