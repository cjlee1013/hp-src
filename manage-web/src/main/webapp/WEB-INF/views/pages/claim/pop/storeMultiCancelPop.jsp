<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup large" style="height: 100%; background-color: #FFFFFF">

    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">일괄취소</h5>
    </div>

    <div id="requestTag">
        <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
            <div class="ui form inline mg-l-10" >
                <span class="text mg-r-5" style="font-size: 15px;">· 일괄 취소 사유는 상품 품절로 처리 됩니다.</span>
            </div>
            <div class="ui form inline mg-t-5 mg-l-10">
                <span class="text mg-r-5" style="font-size: 15px;">· 취소 처리시 구매자에게 추가배송비가 청구되지 않습니다.</span>
            </div>

            <div class="ui form inline mg-t-20 mg-l-10">
                <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-20" style="width: 30%;">
                    <option value="C007">주문상품의 품절/재고없음</option>
                </select>
                <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 65%;" maxlength="100" placeholder="상세사유를 입력 (최대 100자)">
            </div>
        </div>

        <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button xlarge btn-danger font-malgun" id="reqMultiCancelBtn">환불완료</button>
                <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
            </span>
        </div>
    </div>

    <div id="resultTag" style="display: none">
        <div class="ui wrap-table mg-t-15" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px">
            <div style="text-align: center">
                <span style="font-size: 25px;" class="mg-r-15">등록</span>
                <span id="totalCnt" style="font-size: 25px;" class="mg-r-60">0</span>
                <span style="font-size: 25px; text-align: center;" class="mg-r-15">성공</span>
                <span id="successCnt" style="font-size: 25px;" class="mg-r-60">0</span>
                <span style="font-size: 25px; text-align: center;" class="mg-r-15">실패</span>
                <span id="failCnt" id="failCnt" style="font-size: 25px;" >0</span>
            </div>

            <div class="ui form inline mg-t-20 mg-l-20" >
                <span class="text mg-r-5" style="font-size: 15px;">· 성공내역은 클레임 관리에서 확인 가능합니다.</span>
            </div>
        </div>



        <div class="ui center-button-group">
            <span class="inner">
                <button class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
            </span>
        </div>

    </div>

</div>
<script>
    var callBackScript = '${callBackScript}';
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/storeMultiCancelPop.js?v=${fileVersion}"></script>


