<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup xlarge" style="height: 100%;">
    <div class="com popup-wrap-title mg-t-20">
        <h5 class="title font-malgun" style="font-size: 20px">차감 행사정보</h5>
    </div>
    <div id="promoInfoListGrid" style="width: 100%; height: 200px;" class="mg-t-20"></div>

    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script>
    ${promoInfoListGridBaseInfo};
    var claimNo = '${claimNo}';
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/claim/pop/promoInfoPop.js?v=${fileVersion}"></script>
