<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">${getKind} 실적조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>${getKind} 실적조회</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPaymentForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPaymentForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="etcPaymentForm.initSearchDate('-31d');">1개월전</button>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="etcPaymentForm.setMonth(this.value,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                    </select>
                                </span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-t-15">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large white mg-t-5">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:150px;float:left;">
                                <option value="" selected>전체</option>
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 전 일자 결제완료 또는 환불완료를을 기준으로 제공됩니다.</h3>
            </div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">• 점별 합계</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadStoreBtn">엑셀다운</button>
            </div>
            <div class="mg-b-20 " id="etcPaymentStoreGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">• 일별 상세내역</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadDailyBtn">엑셀다운</button>
            </div>
            <div class="mg-b-20 " id="etcPaymentDailyGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">• 건별 상세내역</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadListBtn">엑셀다운</button>
            </div>
            <div class="mg-b-20 " id="etcPaymentListGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/etcPayment.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${etcPaymentStoreBaseInfo}
    ${etcPaymentDailyBaseInfo}
    ${etcPaymentListBaseInfo}
    var getKind = "${getKind}";

    $(document).ready(function() {
        etcPaymentForm.init();
        etcPaymentStoreGrid.init();
        etcPaymentDailyGrid.init();
        etcPaymentListGrid.init();
    });
</script>