<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="marginSearchForm" name="marginSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">DC/DS 마진조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>DC/DS 마진조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="80%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt','-1d','0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt',-(new Date()).getDay()+1,'0');">이번주</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt',-(new Date()).getDate()+1,'0');">이번달</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt','-3m','0');">3개월</button>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><span class="text-red-star">상품구분</span></th>
                            <td class="ui form inline">
                                <select id="mallType" name="mallType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="DC">DC</option>
                                    <option value="DS">DS</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 전 일자 배송완료 또는 구매확정을 기준으로 제공됩니다.</h3>
        </div>
    </div>
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 팀별 합계</h3>
        </div>
        <div class="ui form inline mg-t-10 text-right">
            <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadTeamBtn">엑셀다운로드</button>
        </div>
    </div>
    <div class="mg-b-20 " id="marginTeamGrid" style="height: 347px;"></div>
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 파트별 합계</h3>
        </div>
        <div class="ui form inline mg-t-10 text-right">
            <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadPartBtn">엑셀다운로드</button>
        </div>
        <div class="mg-b-20 " id="marginPartGrid" style="height: 289px;"></div>
    </div>
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 유형별 합계</h3>
        </div>
        <div class="ui form inline mg-t-10 text-right">
            <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadHeadBtn">엑셀다운로드</button>
        </div>
        <div class="mg-b-20 " id="marginHeadGrid" style="height: 115px;"></div>
    </div>
</div>

<script>
    ${marginTeamGridBaseInfo}
    ${marginPartGridBaseInfo}
    ${marginHeadGridBaseInfo}

    $(document).ready(function() {
        marginMain.init();
        marginTeamGrid.init();
        marginPartGrid.init();
        marginHeadGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/marginMain.js?v=${fileVersion}"></script>