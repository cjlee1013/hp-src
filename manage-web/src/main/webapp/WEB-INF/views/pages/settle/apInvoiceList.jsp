<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">AP전표조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>AP전표조회</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="apInvoiceListForm.initSearchDate('0d');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="apInvoiceListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="apInvoiceListForm.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="apInvoiceListForm.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>전표유형</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="slipType" name="slipType" style="width:190px;float:left;">
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <span>* 전표유형별 회계일을 기준으로 제공됩니다.<br /></span>
                <span>* 확정된 전표는 매시간 정각 OF로 전송됩니다.<br /><br /></span>
                <h3 class="title">검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
        </div>
        <div class="ui inline mg-t-10">
            <div class="ui form inline mg-t-10 text-left">
                <button class="ui button medium mint mg-r-10" id="confirmBtn">확정</button>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-top:-28px;" id="excelDownloadAllBtn">일괄 상세 다운로드</button>
                <button class="ui button medium dark-blue mg-l-10" style="margin-top:-28px;margin-right:1%;" id="excelDownloadBtn">엑셀 다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="apInvoiceListGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top:20px;">
                <h3 class="title">* 상세 내역</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-top:-33px;margin-right:1%;" id="detailExcelDownloadBtn">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20 " id="apBalanceSheetGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/apInvoiceList.js?v=${fileVersion}"></script>
<script>
    ${apInvoiceListGridBaseInfo}
    ${apBalanceSheetGridBaseInfo}

    $(document).ready(function() {
        apInvoiceListForm.init();
        apInvoiceListForm.bindingEvent();
        apInvoiceListGrid.init();
        apBalanceSheetGrid.init();
    });
</script>