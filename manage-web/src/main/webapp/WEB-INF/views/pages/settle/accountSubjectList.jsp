<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:12px;}
    #searchPartnerLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div>
    <form id="searchForm" name="searchForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">계정과목별 데이터 조회</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>계정과목별 데이터 조회</caption>
                <colgroup>
                    <col width="150px">
                    <col width="*">
                    <col width="150px">
                </colgroup>
                <tbody>
                    <tr>
                        <th><span class="text-red">* </span>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="startDt" name="startDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="endDt" name="endDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="accountSubjectListForm.initSearchDate('0d');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="accountSubjectListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="accountSubjectListForm.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="accountSubjectListForm.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td>
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button class="ui button large cyan" id="searchBtn">검색</button><br>
                                <button class="ui button large mg-t-10" id="clearBtn">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 회계일을 기준으로 제공됩니다.</h3>
            </div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left; width:49%;">
                <h3 class="title">* 합계내역</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-7px" id="excelDownload">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20" id="accountSubjectListGrid" style="height:980px;"></div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/accountSubjectList.js?v=${fileVersion}"></script>
<script>
    ${accountSubjectListBaseInfo}

    $(document).ready(function() {
        accountSubjectListForm.init();
        accountSubjectListGrid.init();
    });
</script>