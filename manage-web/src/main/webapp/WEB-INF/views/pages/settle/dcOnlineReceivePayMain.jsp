<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="dcOnlineReceivePaySearchForm" name="dcOnlineReceivePaySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">DC온라인수불 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>DC온라인수불 조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="80%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td class="ui form inline">
                                <select class="ui input inline medium mg-r-10" id="schStartYear" name="schStartYear" onchange="SettleCommon.setMonth(this,'schStartMonth')" style="width: 100px;">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${years.dataValue}" selected>${years.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${years.dataValue}">${years.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="schStartMonth" name="schStartMonth" style="width: 80px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${months.dataValue}" selected>${months.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${months.dataValue}">${months.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <span class="text">~</span>
                                <select class="ui input inline medium mg-r-10" id="schEndYear" name="schEndYear" onchange="SettleCommon.setMonth(this,'schEndMonth')" style="width: 100px;">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${years.dataValue}" selected>${years.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${years.dataValue}">${years.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="schEndMonth" name="schEndMonth" style="width: 80px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${months.dataValue}" selected>${months.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${months.dataValue}">${months.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>검색조건</th>
                            <td class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="" selected>선택</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemName">상품명</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px" disabled>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="dcOnlineReceivePaySearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button class="ui button medium dark-blue mg-l-10" style="min-width: auto" id="schExcelDownloadDetailBtn">주문상세 다운</button>
                <button class="ui button medium dark-blue mg-l-10" style="min-width: auto" id="schExcelDownloadBtn">엑셀 다운로드</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="dcOnlineReceivePayGrid" style="width: 100%; height: 500px;"></div>
        <div id="dcOnlineOrderDetailGrid" style="display: none"></div>
    </div>
</div>

<script>
    ${dcOnlineReceivePayGridBaseInfo}
    ${dcOnlineOrderDetailGridBaseInfo}

    $(document).ready(function() {
        dcOnlineReceivePayMain.init();
        dcOnlineReceivePayGrid.init();
        dcOnlineOrderDetailGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/dcOnlineReceivePayMain.js?v=${fileVersion}"></script>