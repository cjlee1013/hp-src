<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="tdUnshipSlipSearchForm" name="tdUnshipSlipSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">TD미배송 전표조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>TD미배송 전표조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="80%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td class="ui form inline">
                                <select class="ui input inline medium mg-r-10" id="schYear" name="schYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'schMonth')">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${years.dataValue}" selected>${years.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${years.dataValue}">${years.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="schMonth" name="schMonth" style="width: 80px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.count == fn:length(getMonth)-1}">
                                                <option value="${months.dataValue}" selected>${months.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${months.dataValue}">${months.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><span class="text-red-star">점포유형</span></th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 120px;">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.mcCd eq 'HYPER' or codeDto.mcCd eq 'CLUB'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="tdUnshipSlipSearchCnt">0</span>건</h3>
        </div>
        <div class="ui inline mg-t-10 mg-b-10">
            <div class="ui form inline mg-t-10 text-left">
                <button class="ui button medium mint mg-r-10" id="createSlip" disabled>생성</button>
                <span style="float: right;">
                    <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;" id="schExcelDownloadBtn">엑셀 다운로드</button>
                </span>
            </div>
        </div>
        <div class="topline"></div>
        <div id="tdUnshipSlipGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${tdUnshipSlipGridBaseInfo}

    $(document).ready(function() {
        tdUnshipSlipMain.init();
        tdUnshipSlipGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/tdUnshipSlipMain.js?v=${fileVersion}"></script>
