<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">일마감 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>일마감 조회</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="gubun" name="gubun" style="width: 120px;">
                                    <option value="paymentInfo">결제일</option>
                                    <option value="settleInfo">매출일</option>
                                </select>
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="startDt" name="startDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="endDt" name="endDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="paymentInfoListForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="paymentInfoListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="paymentInfoListForm.initSearchDate('-31d');">1개월전</button>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:150px;float:left;">
                                <option value="" selected>전체</option>
                                <option value="HYPER">HYPER</option>
                                <option value="CLUB">CLUB</option>
                                <option value="EXP">EXPRESS</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>상품구분</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="mallType" name="mallType" style="width:150px;float:left;">
                                <option value="" selected>전체</option>
                                <option value="TD">TD</option>
                                <option value="DC">DC</option>
                                <option value="DS">DS</option>
                                <option value="제휴">제휴</option>
                                <option value="PG">PG</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색 조건</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 120px;">
                                <option value="" selected>선택</option>
                                <option value="partnerId">판매업체ID</option>
                                <option value="ofVendorCd">업체코드</option>
                                <option value="partnerName">판매업체명</option>
                                <option value="itemNo">상품번호</option>
                                <option value="itemNm">상품명</option>
                                <option value="purchaseOrderNo">주문번호</option>
                                <option value="bundleNo">배송번호</option>
                                <option value="claimNo">클레임그룹번호</option>
                                <option value="orderItemNo">상품주문번호</option>
                            </select>
                            <input type="text" name="searchValue" id="searchValue" style="width:200px" class="ui input medium mg-r-5" placeholder="검색 조건을 입력하세요." readonly>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 전 일자 결제일 또는 매출일을 기준으로 제공됩니다.</h3>
            </div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">• 점포별 합계</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadSumBtn">엑셀다운</button>
            </div>
            <div class="mg-b-20 " id="paymentInfoSumGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">• 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadBtn">엑셀다운</button>
            </div>
            <div class="mg-b-20 " id="paymentInfoListGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/paymentInfoList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${paymentInfoSumBaseInfo}
    ${paymentInfoListBaseInfo}

    $(document).ready(function() {
        paymentInfoListForm.init();
        paymentInfoListForm.bindingEvent();
        paymentInfoListGrid.init();
        paymentInfoSumGrid.init();
    });
</script>