<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">마켓연동 수취내역 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>마켓연동 수취내역 관리</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" style="width:120px;float:left;">
                                    <option value="REG_DT" selected>발행일</option>
                                </select>

                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="taxBillInvoice.initSearchDate('0d');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="taxBillInvoice.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="taxBillInvoice.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="taxBillInvoice.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td>
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>수취유형</th>
                        <td colspan="2">
                            <select class="ui input medium mg-r-5" id="schTaxBillReceiveType" name="schTaxBillReceiveType" style="width:120px;float:left;">
                                <option value="MK" selected>제휴수수료</option>
                                <c:forEach var="codeDto" items="${schTaxBillReceiveType}" varStatus="status">
                                    <c:if test="${codeDto.ref2 eq 'MARKET'}">
                                        <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <span>* 전 월 말일까지 수령한 내역에 한하여 등록 가능합니다.</span><br /><br />
                <h3 class="title">* 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
        </div>
        <div class="ui inline mg-t-10">
            <div class="ui form inline mg-t-10 text-left">
                <button class="ui button medium mint mg-r-10" id="confirmBtn">확정</button>
                <button class="ui button medium mint mg-r-10" id="deleteBtn">삭제</button>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-35px" id="excelDownloadBtn">엑셀 다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="taxBillInvoiceListGrid" style="height: 400px;"></div>
        </div>
    <form name="registerForm" onsubmit="return false;">
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <colgroup>
                <col width="8%">
                <col width="10%">
                <col width="10%">
                <col width="12%">
                <col width="9%">
                <col width="9%">
                <col width="10%">
                <col width="12%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <tbody>
            <tr>
                <th>수취ID</th>
                <th>유형</th>
                <th>공급처</th>
                <th>상호</th>
                <th>대표자명</th>
                <th>사업자등록번호</th>
                <th>발행일</th>
                <th>합계금액</th>
                <th>공급가</th>
                <th>부가세</th>
            </tr>
            <tr>
                <td>
                    <span id="taxReceiveNo"></span>
                </td>
                <td>
                    <select class="ui input medium mg-r-10" id="taxBillReceiveType" name="taxBillReceiveType" style="width: 140px;">
                        <option selected value="MK">제휴수수료</option>
                        <c:forEach var="codeDto" items="${schTaxBillReceiveType}" varStatus="status">
                            <c:if test="${codeDto.ref2 eq 'MARKET'}">
                                <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </td>
                <td>
                    <select class="ui input medium mg-r-10" id="supplier" name="supplier" style="width: 120px;">
                    </select>
                    <select class="ui input medium mg-r-10" id="supplierInfo" name="supplierInfo" style="width: 120px;display:none">
                    </select>
                </td>
                <td>
                    <span id="partnerNm"></span>
                </td>
                <td>
                    <span id="partnerOwner"></span>
                </td>
                <td>
                    <span id="partnerNo"></span>
                </td>
                <td>
                    <input type="text" id="issueDt" name="issueDt" class="ui input medium mg-r-5" style="width: 120px;" maxlength="10" placeholder="YYYY-MM-DD" >
                </td>
                <td>
                    <span id="sumAmt" name="sumAmt">0</span>
                </td>
                <td>
                    <input type="text" id="supplyAmt" name="supplyAmt" class="ui input medium mg-r-5" style="width: 120px;" placeholder="0">
                </td>
                <td>
                    <input type="text" id="vatAmt" name="vatAmt" class="ui input medium mg-r-5" style="width: 120px;" placeholder="0">
                </td>
            </tr>
            <tr>
                <td colspan="10" class="com wrap-title" style="text-align:center;height:45px;">
                    <button class="ui button medium dark-blue mg-l-10" id="regSubmitBtn">저장</button>
                    <button class="ui button medium white mg-l-10" id="regResetBtn">초기화</button>
                </td>
            </tr>
            </tbody>
        </table>
        <input type="hidden" id="partnerId" name="partnerId" value="" />
    </div>
    </form>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/taxBillReceiveMarket.js?v=${fileVersion}"></script>
<script>
    ${taxBillInvoiceListBaseInfo}

    $(document).ready(function() {
        taxBillInvoice.init();
        taxBillInvoice.bindingEvent();
        taxBillInvoiceListGrid.init();
    });
</script>