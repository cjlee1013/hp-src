<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">매출현황</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>매출현황</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="500px">
                        <col width="150px">
                        <col width="*">
                        <col width="80px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색 기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailySettleListForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailySettleListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailySettleListForm.initSearchDate('-1m');">1개월전</button>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                        <td rowspan="4">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상품 구분</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="mallType" name="mallType" style="width:150px;float:left;">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${mallType}" varStatus="status">
                                        <option value="${codeDto.code}">${codeDto.code}</option>
                                    </c:forEach>
                                    <option value="MARKET">제휴</option>
                                    <option value="HOME">PG</option>
                                </select>
                            </div>
                        </td>
                        <th>점포 유형</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="storeType" name="storeType" style="width:150px;float:left;">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.ref3 eq 'REAL'}">
                                            <option value="${codeDto.mcCd}" >${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색 조건</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 150px;">
                            </select>
                            <input type="text" name="searchField" id="searchField" style="width:200px" class="ui input medium mg-r-5" placeholder="검색 조건을 입력하세요." readonly="true">
                        </td>
                        <th>제휴사</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="siteType" name="siteType" style="width:150px;float:left;">
                                    <option value="">전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <span>* 전 일자 배송완료 또는 구매확정을 기준으로 제공됩니다.</span><br /><br />
                <h3 class="title">점포별 합계</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:35px" id="sumExcelDownloadBtn">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20 " id="dailySettleSumGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top:20px;">
                <h3 class="title">상품별 합계</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-35px" id="excelDownloadBtn">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20 " id="dailySettleListGrid" style="height: 400px;"></div>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="margin-top:20px;">
                <h3 class="title">주문 상세 내역</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-35px" id="detailExcelDownloadBtn">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20 " id="dailySettleInfoGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/dailySettleList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${dailySettleListBaseInfo}
    ${dailySettleDetailBaseInfo}
    ${dailySettleSumBaseInfo}

    $(document).ready(function() {
        dailySettleListForm.init();
        dailySettleListForm.bindingEvent();
        dailySettleListGrid.init();
        dailySettleInfoGrid.init();
        dailySettleSumGrid.init();
    });
</script>