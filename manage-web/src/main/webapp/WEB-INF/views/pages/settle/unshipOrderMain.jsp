<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="unshipOrderSearchForm" name="unshipOrderSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">미배송 주문조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>미배송 주문조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="10%">
                        <col width="30%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td colspan="4" class="ui form inline">
                                <select class="ui input inline medium mg-r-10" id="schYear" name="schYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'schMonth')">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${years.dataValue}" selected>${years.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${years.dataValue}">${years.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="schMonth" name="schMonth" style="width: 80px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${months.dataValue}" selected>${months.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${months.dataValue}">${months.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="3">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>상품구분</th>
                            <td class="ui form inline">
                                <select class="ui input medium mg-r-5" id="schMallType" name="schMallType" style="width:120px">
                                    <option value="" selected>전체</option>
                                    <c:forEach var="codeDto" items="${mallType}" varStatus="status">
                                        <c:if test="${codeDto.code ne 'TD'}">
                                            <option value="${codeDto.code}">${codeDto.code}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>배송상태</th>
                            <td colspan="2" class="ui form inline">
                                <select id="schShipStatus" name="schShipStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'NN'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>검색조건</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="" selected>선택</option>
                                    <option value="partnerId">판매업체ID</option>
                                    <option value="supplier">업체코드</option>
                                    <option value="partnerName">판매업체명</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="itemNm">상품명</option>
                                    <option value="purchaseOrderNo">주문번호</option>
                                    <option value="bundleNo">배송번호</option>
                                    <option value="claimNo">클레임그룹번호</option>
                                    <option value="orderItemNo">상품주문번호</option>
                                    <option value="orderTicketNo">티켓번호</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px" disabled>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="unshipOrderSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-7px" id="schExcelDownloadBtn">엑셀 다운로드</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="unshipOrderGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${unshipOrderGridBaseInfo}

    $(document).ready(function() {
        unshipOrderMain.init();
        unshipOrderGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/unshipOrderMain.js?v=${fileVersion}"></script>
