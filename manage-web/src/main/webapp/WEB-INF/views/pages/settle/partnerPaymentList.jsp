<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<style>
    #searchPartnerIdLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">DS지급관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <form name="searchForm" onsubmit="return false;">
        <div class="ui wrap-table horizontal mg-t-1 0">
                <table class="ui table">
                    <caption>DS지급관리</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색 기간</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerPayment.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerPayment.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerPayment.initSearchDate('-31d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerPayment.initSearchDate('-91d');">3개월전</button>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="text">~</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>지급상태</th>
                        <td>
                            <select id="settlePayState" style="width: 180px" name="settlePayState" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${settlePayState}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>지급방법</th>
                        <td>
                            <select id="payMethod" style="width: 180px" name="payMethod" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="O">지급대행</option>
                                <option value="D">직접지급</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색 조건</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 150px;">
                                <option value="">선택</option>
                                <option value="partnerId">판매업체ID</option>
                                <option value="partnerName">판매업체명</option>
                                <option value="partnerNo">사업자번호</option>
                                <option value="bankAccountNo">계좌번호</option>
                                <option value="bankAccountHolder">예금주</option>
                                <option value="settleSrl">지급SRL</option>
                            </select>
                            <input type="text" name="searchField" id="searchField" style="width:200px" class="ui input medium mg-r-5" placeholder="검색 조건을 입력하세요." readonly="true">
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 정산완료 된 지급SRL에 한하여 지급예정일 기준으로 제공됩니다.</h3>
            </div>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="searchCnt">0</span>건</h3>
        </div>
        <div class="ui inline mg-t-10">
            <div class="ui form inline mg-t-10 text-left">
                <button class="ui button medium mint mg-r-10" onclick="partnerPayment.setPayState(2,true)">일괄 지급요청</button>
                <button class="ui button medium mint mg-r-10" onclick="partnerPayment.setPayState(4,true)">일괄 지급중지</button>
                <button class="ui button medium mint mg-r-10" onclick="partnerPayment.setPayState(3,true)">일괄 지급보류</button>
                <button class="ui button medium mint mg-r-10" onclick="partnerPayment.setPayState(1,true)">일괄 보류해제</button>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-top:-33px;margin-right:1%;" id="excelDownloadBtn">엑셀 다운로드</button>
            </div>
        </div>
        <div id="partnerPaymentMngGrid" style="height: 320px;"></div>
        <input type="hidden" id="CheckedData" name="CheckedData" value="" />
        </form>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/partnerPaymentMng.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${partnerPaymentMngBaseInfo}

    $(document).ready(function() {
        partnerPayment.init();
        partnerPayment.bindingEvent();
        partnerPaymentGrid.init();
    });

    settlePayStateList = new Array();
    <c:forEach var="codeDto" items="${settlePayState}" varStatus="status">
    settlePayStateList[${codeDto.mcCd}] = '${codeDto.mcNm}';
    </c:forEach>
</script>
