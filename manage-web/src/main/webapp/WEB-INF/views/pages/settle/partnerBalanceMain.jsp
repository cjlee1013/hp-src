<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="partnerBalanceSearchForm" name="partnerBalanceSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">파트너 잔액 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>파트너 잔액 조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="80%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><span class="text-red-star">검색 기간</span></th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="startDt" name="startDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="endDt" name="endDt" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerBalanceMain.initSearchDateCustom('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerBalanceMain.initSearchDateCustom('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerBalanceMain.initSearchDateCustom('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerBalanceMain.initSearchDateCustom('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색조건</th>
                        <td class="ui form inline">
                            <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 120px">
                                <option value="" selected>선택</option>
                                <option value="partnerId">판매업체ID</option>
                                <option value="partnerName">판매업체명</option>
                                <option value="vendorCd">업체코드</option>
                            </select>
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 150px" disabled>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="partnerBalanceSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-7px" id="schExcelDownloadBtn">엑셀 다운로드</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="partnerBalanceGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
  ${partnerBalanceGridBaseInfo}

  $(document).ready(function() {
    partnerBalanceMain.init();
    partnerBalanceGrid.init();
    CommonAjaxBlockUI.global();
  });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/partnerBalanceMain.js?v=${fileVersion}"></script>
