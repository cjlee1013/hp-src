<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">제휴실적조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>제휴실적조회</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="720px">
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5" id="dateType" name="dateType" style="width:150px;float:left;">
                                    <option value="">집계기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliateSalesForm.initSearchDate('-1m',0);">최근 1개월</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="affiliateSalesForm.initSearchDate(-(new Date()).getDate()+1,0);">이번달</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="var now = new Date();var prevDay=(new Date(now.getFullYear(),now.getMonth()-1,1));affiliateSalesForm.initSearchDate(prevDay.getFullYear()+'-'+(prevDay.getMonth()+1)+'-'+prevDay.getDate(),-(new Date()).getDate());">지난달</button>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사이트구분</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="siteType" name="siteType" style="width:150px;float:left;">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${siteType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th>집계기준</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-5" id="gubun" name="gubun" style="width:150px;float:left;">
                                <option value="">전체</option>
                                <option value="1">구매</option>
                                <option value="2">취소</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색 조건</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 120px;">
                                    <option value="channelNm">제휴채널명</option>
                                    <option value="affiliateCd">제휴채널ID</option>
                                    <option value="affiliateNm">제휴업체명</option>
                                </select>
                                <input type="text" name="searchValue" id="searchValue" style="width:200px" class="ui input medium mg-r-5" placeholder="검색 조건을 입력하세요.">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadBtn">다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="affiliateSalesGrid" style="height: 400px;"></div>
        <div class="com wrap-title" style="padding-bottom: 10px;">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title" id="descriptionDetail"></h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadDetailBtn">다운로드</button>
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadDetailAllBtn">조회기간 전체 다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="affiliateDetailGrid" style="height: 400px;"></div>
        <div class="mg-b-20 " id="affiliateDetailAllGrid" style="height: 400px; display: none"></div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/affiliateList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
  ${affiliateSalesBaseInfo}
  ${affiliateDetailBaseInfo}
  ${affiliateDetailAllBaseInfo}

  $(document).ready(function() {
    affiliateSalesForm.init();
    affiliateSalesForm.bindingEvent();
    affiliateSalesGrid.init();
    affiliateDetailGrid.init();
    affiliateDetailAllGrid.init();
  });
</script>