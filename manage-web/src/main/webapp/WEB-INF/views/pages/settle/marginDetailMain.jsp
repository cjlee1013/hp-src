<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="marginDetailSearchForm" name="marginDetailSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">DC/DS 마진상세조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>DC/DS 마진상세조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="80%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt','-1d','0');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt',-(new Date()).getDay()+1,'0');">이번주</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt',-(new Date()).getDate()+1,'0');">이번달</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="shippingUtil.initSearchDateCustom('schStartDt','schEndDt','-3m','0');">3개월</button>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><span class="text-red-star">상품구분</span></th>
                            <td class="ui form inline">
                                <select id="mallType" name="mallType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="DC">DC</option>
                                    <option value="DS">DS</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>기준카테고리</th>
                            <td class="ui form inline">
                                <select class="ui input medium mg-r-5" id="formMappingCateCd1" name="division"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(1,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd2" name="groupNo"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(2,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd3" name="dept"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(3,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd4" name="classCd"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(4,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd5" name="subclass"
                                        style="width:150px;float:left;" data-default="전체"
                                >
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* 전 일자 배송완료 또는 구매확정을 기준으로 제공됩니다.</h3>
        </div>
    </div>
    <div class="com wrap-title sub" style="margin-top: 30px;">
        <h3 class="title">검색결과 : <span id="marginDetailSearchCnt">0</span>건</h3>
        <span style="float: right;">
            <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-7px" id="schExcelDownloadBtn">엑셀 다운로드</button>
        </span>
    </div>
    <div class="topline"></div>
    <div id="marginDetailGrid" style="width: 100%; height: 500px;"></div>
</div>

<script>
    ${marginDetailGridBaseInfo}

    $(document).ready(function() {
        marginDetailMain.init();
        marginDetailGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/marginDetailMain.js?v=${fileVersion}"></script>