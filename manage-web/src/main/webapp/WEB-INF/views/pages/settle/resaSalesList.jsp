<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">SALES 자료조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>SALES 자료조회</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="120px">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="D" checked><span>일별</span>
                                </label>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="resaSalesListForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="resaSalesListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="resaSalesListForm.initSearchDate('-31d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="resaSalesListForm.initSearchDate('-91d');">3개월전</button>
                            </div>
                            <br />
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="dateType" value="M"><span>월별</span>
                                </label>
                                <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="text">~</span>
                                    <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.dataValue}">${years.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                        <c:forEach var="months" items="${getMonth}" varStatus="status">
                                            <option value="${months.dataValue}">${months.dataStr}</option>
                                        </c:forEach>
                                    </select>
                                </span>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="clearBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상세검색</th>
                        <td class="ui form inline">
                            <label class="ui inline">
                                <input type="checkbox" id="diffOnly" name="diffOnly" value="" class="mg-r-5">
                                <span class="text mg-r-10">차이내역</span>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">* 검색결과</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10"  id="excelDownloadBtn">엑셀다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="resaSalesListGrid" style="height: 400px;"></div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left; width:49%;">
                <h3 class="title">* 자료등록</h3>
            </div>
        </div>
        <form name="salesUploadForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <tbody>
                    <tr>
                        <th>파일 선택</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium mg-r-10" style="display:inline; width:200px;" readonly="true">
                            <button class="ui button medium white mg-l-10" id="salesUpload">파일 선택</button>
                            <input type="file" name="uploadFile" id="uploadFile" accept=".csv" style="display:none;">
                            &nbsp;<span>* 정해진 양식만 등록 가능합니다.</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align:center;height:35px;">
                                <button class="ui button medium dark-blue mg-l-10" id="salesSubmit">저장</button>
                                <button class="ui button medium white mg-l-10" id="salesReset">초기화</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/resaSalesList.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${resaSalesListBaseInfo}

    $(document).ready(function() {
        resaSalesListForm.init();
        resaSalesListForm.bindingEvent();
        resaSalesListGrid.init();
    });
</script>