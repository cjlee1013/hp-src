<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">판매자 정산 정보</h2>
    </div>
    <div class="com wrap-title sub">
        <form name="searchForm" onsubmit="return false;">
            <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>판매자 정보</caption>
                    <colgroup>
                        <col width="100px">
                        <col width="9%">
                        <col width="100px">
                        <col width="12%">
                        <col width="100px">
                        <col width="10%">
                        <col width="100px">
                        <col width="9%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th> * 판매업체ID</th>
                        <td>
                            <div class="ui form inline"><span class="text" id="partnerId">${partnerId}</span></div>
                        </td>
                        <th> * 판매업체명</th>
                        <td>
                            <div class="ui form inline"><span class="text" id="partnerNm">${partnerNm}</span></div>
                        </td>
                        <th> * 대표자명</th>
                        <td>
                            <div class="ui form inline"><span class="text" id="partnerOwner">${partnerOwner}</span></div>
                        </td>
                        <th> * 사업자유형</th>
                        <td>
                            <div class="ui form inline"><span class="text" id="operatorType">${operatorType}</span></div>
                        </td>
                        </tr>
                        <tr>
                            <th> * 사업자번호</th>
                            <td colspan="3">
                                <div class="ui form inline"><span class="text" id="partnerNo">${partnerNo}</span></div>
                            </td>
                            <th> * 계좌번호</th>
                            <td colspan="3">
                                <div class="ui form inline"><span class="text" id="bankAccountNo">${bankAccountNo}</span></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="com wrap-title sub" style="margin-top: 30px;">
                <h3 class="title">정산 내역 </h3>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>판매자 정산내역</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="schStartDt" name="schStartDt" style="width:100px;" value="${schStartDt}">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="schEndDt" name="schEndDt" style="width:100px;" value="${schEndDt}">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerSettleForm.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerSettleForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerSettleForm.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="partnerSettleForm.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>정산주기</th>
                        <td>
                            <div class="ui form inline">
                                <div class="ui form inline">
                                    <label class="ui radio">
                                        <input type="radio" name="settleCycleType" value="" checked><span>전체</span>
                                    </label>
                                    <c:forEach var="codeDto" items="${settleCycleType}" varStatus="status">
                                        <label class="ui radio">
                                            <input type="radio" name="settleCycleType" value="${codeDto.code}"><span>${codeDto.name}</span>
                                        </label>
                                    </c:forEach>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="com wrap-title">
                <div class="com wrap-title sub" style="float: left;">
                    <h3 class="title">• 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
                </div>
                <div class="ui form inline mg-t-10 text-right">
                    <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;" id="excelDownloadBtn">엑셀 다운로드</button>
                </div>
                <div class="mg-b-20 " id="partnerSettleGrid" style="height: 400px;"></div>
                <input type="hidden" name="settleSrl" id="settleSrl" value="${settleSrl}" />
            </div>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <h3 class="title">상품별 정산 상세 내역</h3><br /><br />
                <h3 class="title">• 검색결과: <span id="partnerSettleDetailCnt">0</span>건</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;;margin-top:35px" id="detailExcelDownloadBtn">엑셀 다운로드</button>
            </div>
            <div class="mg-b-20 " id="partnerSettleDetailGrid" style="height: 400px;"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/partnerSettleDetail.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script>
    ${partnerSettleGridBaseInfo}
    ${partnerSettleDetailBaseInfo}

    $(document).ready(function() {
        partnerSettleForm.init();
        partnerSettleForm.bindingEvent();
        partnerSettleGrid.init();
        partnerSettleDetailGrid.init();
    });
</script>