<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<style>
    .ui-autocomplete {max-height: 200px; overflow-y: auto; overflow-x: hidden;}
    .ui-menu-item-wrapper {font-size:12px;}
    #searchPartnerLayer li:hover{background:#f7f7f7;cursor:pointer;}
</style>
<div>
    <form id="adjustSearchForm" name="adjustSearchForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">정산조정 관리</h2>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>정산조정 관리</caption>
                <colgroup>
                    <col width="150px">
                    <col width="*">
                    <col width="150px">
                </colgroup>
                <tbody>
                    <tr>
                        <th><span class="text-red">* </span>입력일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium mg-r-10" placeholder="시작일" id="startDt_D" name="startDt_D" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;</span>
                                <input type="text" class="ui input medium mg-l-10 mg-r-10" placeholder="종료일" id="endDt_D" name="endDt_D" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="adjustListForm.initSearchDate('0d');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="adjustListForm.initSearchDate('-7d');">1주일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="adjustListForm.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="adjustListForm.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button class="ui button large cyan" id="searchBtn">검색</button><br>
                                <button class="ui button large mg-t-10" id="clearBtn">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>유형 구분</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="schAdjustType" name="schAdjustType" style="width: 150px;">
                                <option selected value="">전체</option>
                                <c:forEach var="item" items="${schAdjustType}">
                                    <option value="${item.key}">${item.value}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 120px;">
                                <option selected value="">선택</option>
                            </select>
                            <input type="text" name="searchValue" id="searchValue" style="width:200px" class="ui input medium mg-r-5">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left; width:49%;">
                <span>* DS 상품에 한하여 등록 가능합니다.</span><br /><br />
                <h3 class="title">• 검색결과 : <span id="searchCnt">0</span>건</h3>
            </div>
        </div>
        <div class="ui inline mg-t-10">
            <div class="ui form inline mg-t-10 text-left">
                <button class="ui button medium mint mg-r-10" id="deleteBtn">삭제</button>
            </div>
            <div class="ui form inline mg-t-20 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-40px" id="excelDownload">엑셀 다운로드</button>
            </div>
        </div>
        <div class="mg-b-20" id="adjustListGrid" style="height: 400px;"></div>
    </form>
</div>
<br />
<div class="com wrap-title">
    <div class="com wrap-title sub" style="float: left; width:49%;">
        <h3 class="title">* 정산조정 등록</h3>
    </div>
</div>
<div>
    <form id="adjustRegForm" name="adjustRegForm" onsubmit="return false;" onkeydown="if(event.keyCode==13) adjustListForm.insertAdjust();">
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table" style="text-align: center">
                <colgroup>
                    <col width="10%">
                    <col width="11%">
                    <col width="9%">
                    <col width="8%">
                    <col width="9%">
                    <col width="9%">
                    <col width="5%">
                    <col width="11%">
                    <col width="8%">
                    <col width="7%">
                    <col width="9%">
                    <col width="3%">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품번호</th>
                    <th>상품명</th>
                    <th>판매업체ID</th>
                    <th>업체코드</th>
                    <th>판매업체명</th>
                    <th>유형</th>
                    <th>구분</th>
                    <th>유형 설명</th>
                    <th>조정금액</th>
                    <th>조정수수료</th>
                    <th>조정사유</th>
                    <th>별도<br />정산</th>
                </tr>
                <tr>
                    <td style="alignment: center">
                        <div class="ui form inline mg-t-10">
                            <input type="text" id="regItemNo"  class="ui input medium mg-r-5" style="width:95px; margin-left:1px;">
                            <button type="button" class="partnerInfo" style="margin-left:-6px;border: 1px solid #ccc;background-color: #fff;" id="btnItemSearch">
                                <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                            </button>
                        </div>
                    </td>
                    <td>
                        <span id="regItemNm"></span>
                    </td>
                    <td>
                        <span id="regPartnerId"></span>
                    </td>
                    <td>
                        <span id="regVendorCd"></span>
                    </td>
                    <td>
                        <span id="regPartnerNm"></span>
                    </td>
                    <td>
                        <select class="ui input medium mg-r-10" id="adjustType" name="adjustType" style="min-width:85px;width:100%;">
                            <option value="">선택</option>
                            <c:forEach var="item" items="${adjustType}">
                                <option value="${item.key}">${item.value}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <select class="ui input medium mg-r-5" id="regGubun" name="regGubun" style="min-width:60px;width:100%">
                            <option value="" selected>선택</option>
                            <option value="1">가산</option>
                            <option value="2">차감</option>
                        </select>
                    </td>
                    <td>
                        <span id="regTypeDesc"></span>
                    </td>
                    <td>
                        <input type="text" id="regAmt" class="ui input medium mg-r-5" style="width:100%;" placeholder="0" >

                    </td>
                    <td>
                        <input type="text" id="regFee" class="ui input medium mg-r-5" style="width:100%" placeholder="0">
                    <td>
                        <input type="text" id="regComment" class="ui input medium mg-r-5" style="width:100%;" maxlength="100" placeholder="최대 100자">
                    </td>
                    <td>
                        <label class="ui checkbox">
                            <input type="checkbox" id="separateYn" name="separateYn" value="Y" checked/><span class="text"> </span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="11">
                        <div class="ui form inline mg-t-10" style="text-align:center;height:25px;">
                            <button class="ui button medium dark-blue mg-l-10" id="regSubmit">등록</button>
                            <button class="ui button medium white mg-l-10" id="regClear">초기화</button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left; width:49%;">
            <h3 class="title">* 엑셀 업로드</h3>
        </div>
    </div>
    <form method="POST" id="excelUploadForm" name="excelUploadForm" onsubmit="return false;" enctype="multipart/form-data">
    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <colgroup>
                <col width="90%">
                <col width="10%">
            </colgroup>
            <tbody>
            <tr>
                <th>파일 선택</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td>
                    <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium mg-r-10" style="display:inline; width:200px;" readonly="true">
                    <button class="ui button medium white mg-l-10" id="excelUpload">파일 선택</button>
                    <input type="file" name="uploadFile" id="uploadFile" accept=".xlsx, .xls" style="display:none;">
                    &nbsp;<span>* 양식에 작성한 파일만 업로드 할 수 있습니다.</span>
                </td>
                <td>
                    <button class="ui button medium white mg-l-10" id="excelSample">양식 다운로드</button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="text-align:center;height:35px;">
                        <button class="ui button medium dark-blue mg-l-10" id="excelSubmit">등록</button>
                        <button class="ui button medium white mg-l-10" id="excelReset">초기화</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="mg-b-20" id="adjustUploadGrid" style="height: 200px;display:none"></div>
    </form>
</div>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/adjustList.js?v=${fileVersion}"></script>
<script>
    ${adjustListBaseInfo}
    ${adjustUploadBaseInfo}

    $(document).ready(function() {
        adjustListForm.init();
        adjustListGrid.init();
        adjustUploadGrid.init();
    });
</script>