<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">부가세신고내역</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>부가세신고내역 검색</caption>
                    <colgroup>
                        <col width="150px">
                        <col width="*">
                        <col width="150px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색기간</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input inline medium mg-r-10" id="startYear" name="startYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'startMonth')">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <option value="${years.dataValue}">${years.dataStr}</option>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="startMonth" name="startMonth" style="width: 100px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <option value="${months.dataValue}">${months.dataStr}</option>
                                    </c:forEach>
                                </select>
                                <span class="text">~</span>
                                <select class="ui input inline medium mg-r-10" id="endYear" name="endYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'endMonth')">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <option value="${years.dataValue}">${years.dataStr}</option>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="endMonth" name="endMonth" style="width: 100px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <option value="${months.dataValue}">${months.dataStr}</option>
                                    </c:forEach>
                                </select>
                                </select>
                            </div>
                        </td>
                        <td rowspan="2">
                            <div style="text-align: left;" class="ui form mg-l-30">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-10">초기화</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색 조건</th>
                        <td class="ui form inline">
                            <select class="ui input medium mg-r-10" id="searchType" name="searchType" style="width: 150px;">
                            </select>
                            <input type="text" name="searchField" id="searchField" style="width:200px" class="ui input medium mg-r-5" placeholder="검색 조건을 입력하세요." readonly="true">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title">
            <div class="com wrap-title sub" style="float: left;">
                <span>* 위탁 판매업체에 한하여 매월 1일 제공됩니다.</span><br /><br />
                <h3 class="title">• 검색결과 : 총 <span id="searchCnt">0</span>건</h3>
            </div>
            <div class="ui form inline mg-t-10 text-right">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:35px" id="excelDownloadBtn">엑셀 다운로드</button>
            </div>
        </div>
        <div class="mg-b-20 " id="monthlySalesVATListGrid" style="height: 400px;"></div>
        <div class="mg-b-20 " id="monthlySalesVATDetailGrid" style="display: none"></div>
        <div class="mg-b-20" id="adjustListGrid" style="display: none"></div>
        </div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/monthlySalesVATList.js?v=${fileVersion}"></script>
<script>
    ${monthlySalesVATListBaseInfo}
    ${monthlySalesVATDetailBaseInfo}
    ${adjustListBaseInfo}

    $(document).ready(function() {
        monthlySalesVAT.init();
        monthlySalesVAT.bindingEvent();
        monthlySalesVATListGrid.init();
        monthlySalesVATDetailGrid.init();
        adjustListGrid.init();
    });
</script>