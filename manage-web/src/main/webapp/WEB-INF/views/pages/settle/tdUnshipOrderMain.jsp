<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <div>
        <form id="tdUnshipOrderSearchForm" name="tdUnshipOrderSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">TD미배송 주문조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>TD미배송 주문조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="30%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th><span class="text-red-star">검색 기간</span></th>
                            <td colspan="3" class="ui form inline">
                                <select class="ui input inline medium mg-r-10" id="schYear" name="schYear" style="width: 100px;" onchange="SettleCommon.setMonth(this,'schMonth')">
                                    <c:forEach var="years" items="${getYear}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.last}">
                                                <option value="${years.dataValue}" selected>${years.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${years.dataValue}">${years.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <select class="ui input inline medium mg-r-10" id="schMonth" name="schMonth" style="width: 80px;">
                                    <c:forEach var="months" items="${getMonth}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${status.count == fn:length(getMonth)-1}">
                                                <option value="${months.dataValue}" selected>${months.dataStr}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${months.dataValue}">${months.dataStr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="4">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>상품 구분</th>
                            <td>
                                <div class="ui form inline">
                                    <select class="ui input medium mg-r-5" id="marketType" name="marketType" style="width:150px;">
                                        <option value="HOME">TD</option>
                                        <option value="NAVER">제휴</option>
                                    </select>
                                </div>
                            </td>
                            <th><span class="text-red-star">점포유형</span></th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 120px;">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:if test="${codeDto.mcCd eq 'HYPER'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                                <select id="schStoreId" name="schStoreId" class="ui input medium mg-r-10" style="width: 120px;">
                                    <option value="">전체</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>기준카테고리</th>
                            <td colspan="3" class="ui form inline">
                                <select class="ui input medium mg-r-5" id="formMappingCateCd1" name="division"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(1,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd2" name="groupNo"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(2,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd3" name="dept"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(3,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd4" name="classCd"
                                        style="width:150px;float:left;" data-default="전체" onchange="javascript:SettleCommon.changeCategoryMappingSelectBox(4,'formMappingCateCd');"
                                >
                                </select>
                                <select class="ui input medium mg-r-5" id="formMappingCateCd5" name="subclass"
                                        style="width:150px;float:left;" data-default="전체"
                                >
                                </select>
                            </td>
                         </tr>
                        <tr>
                            <th>추가정보</th>
                            <td class="ui form inline">
                                <label class="ui inline">
                                    <input type="checkbox" id="schShipInfo" name="schShipInfo" value="" class="mg-r-5">
                                    <span class="text mg-r-10">배송정보</span>
                                </label>
                            </td>
                            <th>현 배송상태</th>
                            <td class="ui form inline">
                                <select id="schShipStatus" name="schShipStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'NN'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                          </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div class="com wrap-title">
        <div class="com wrap-title sub" style="float: left;">
            <h3 class="title">* TD 상품에 한하여 매월 1일 제공됩니다.</h3>
        </div>
    </div>
    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="tdUnshipOrderSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button class="ui button medium dark-blue mg-l-10" style="margin-right:1%;margin-top:-7px" id="schExcelDownloadBtn">엑셀 다운로드</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="tdUnshipOrderGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${tdUnshipOrderGridBaseInfo}

    $(document).ready(function() {
        tdUnshipOrderMain.init();
        tdUnshipOrderGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/settleCommon.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/settle/tdUnshipOrderMain.js?v=${fileVersion}"></script>
