<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<!-- 기본 프레임 -->
<form name="searchForm" method="POST" onsubmit="return false;">
    <div class="com wrap-popup medium popup-request">
        <div class="com popup-wrap-title">
            <div class="com wrap-title has-border">
                <h3 class="title pull-left">
                    지급예정일 변경
                </h3>
            </div>
        </div>
        <div class="pd-t-20">
            <div class="ui form inline mg-t-10 text-center">
                <span class="text" style="text-align:center;">지급예정일 변경 시 변경하실 지급예정일과 사유를 입력해주세요.<br /><br /></span>
            </div>
            <table class="ui table">
                <colgroup>
                    <col width="120px">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <td  style="text-align:left">판매업체명</td>
                    <td style="text-align:left">${partnerNm}</td>
                </tr>
                <tr>
                    <td  style="text-align:left">지급SRL</td>
                    <td style="text-align:left">${settleSrl}</td>
                </tr>
                <tr>
                    <td  style="text-align:left">지급예정일</td>
                    <td style="text-align:left">${settlePreDt}</td>
                </tr>
                <tr>
                    <td  style="text-align:left">변경 지급예정일</td>
                    <td style="text-align:left">
                        <div class="ui form inline">
                            <input type="text" class="ui input medium mg-r-5" id="changeSettlePreDt" name="changeSettlePreDt" value="${settlePreDt}" style="width:200px;" maxlength="10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left">변경사유</td>
                    <td style="text-align:left">
                        <div class="ui form inline">
                            <span id="holdClearMemoLength" style="width:40px"/>
                            <input type="text" class="ui input medium mg-r-10" id="changeMemo" name="changeMemo" style="width:350px" maxlength="200">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="ui form inline mg-t-15 text-center">
                <button type="button" class="ui button large btn-danger" onclick="javascript:register()"> 등록 </button>
                <span class="text">&nbsp;&nbsp;</span>
                <button type="button" class="ui button large" onclick="javascript:self.close()"> 닫기 </button>
            </div>
        </div>
    </div>
    <input type="hidden" id="settleSrl" name="settleSrl" value="${settleSrl}" />
    <input type="hidden" id="partnerId" name="partnerId" value="${partnerId}" />
    <input type="hidden" id="partnerNm" name="partnerNm" value="${partnerNm}" />
    <input type="hidden" id="settlePreDt" name="settlePreDt" value="${settlePreDt}" />
    <input type="hidden" id="settleCycleType" name="settleCycleType" value="${settleCycleType}" />
</form>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script>
    function register() {
        if ($("#changeMemo").val() == null || $("#changeMemo").val() == "") {
            alert("변경 사유를 입력해주세요.");
            $("#changeMemo").focus();
            return false;
        }
        if ($("#settlePreDt").val() == $("#changeSettlePreDt").val()) {
            alert("동일한 날짜로 변경 불가능합니다.");
            $("#changeSettlePreDt").focus();
            return false;
        }

        if(confirm("정산예정일 변경하시겠습니까?")) {
            $("#settlePreDt").val($("#changeSettlePreDt").val().split('-').join(''));
            var searchForm = $("form[name=searchForm]").serialize();
            console.log(searchForm);

            CommonAjax.basic({
                url: "/settle/partnerSettle/setSettlePreDt.json?"+ searchForm,
                data: null,
                method: "GET",
                callbackFunc: function (resData) {
                    alert(resData.returnMessage);
                    if (resDate.returnCode == '0') {
                      window.opener.document.getElementById('searchBtn').click();
                      self.close();
                    }
                }
            });
        }
    }
</script>