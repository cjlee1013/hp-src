<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    td.close_day_img span {
        text-align: center;
        display: block;
    }
</style>

<div class="com wrap-popup xlarge">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun" style="float: none">이미지변경</h2>
        <ul class="mg-t-10">
            <li>- 해당이미지는 배송 옵션 선택 화면에 노출되며 이미지 변경 시 기 등록된 모든 행사의 휴무 이미지가 변경되니 주의바랍니다.</li>
            <li>- 하기 PC 5개 이미지는 항상 등록되어 있어야 합니다.</li>
        </ul>
    </div>

    <!-- 상단 노출 Tab -->
    <c:set var="nowSiteType" value="Hyper" />
    <div>
        <ul id="storeType" name="storeType" class="tabtab" style="background-color: #ececec">
            <li id="HYPER" name="HYPER" class="col-2 tab-on" style="text-align: center; width: 34%;">홈플러스(일반)</li>
            <li id="AURORA" name="AURORA" class="col-2" style="text-align: center; width: 33%;">홈플러스(새벽)</li>
            <li id="CLUB" name="CLUB" class="col-2" style="text-align: center; width: 33%;">더 클럽</li>
        </ul>
    </div>

    <!-- 휴무일 이미지 정보 form -->
    <form id="closeDayImagePopForm" onsubmit="return false;" style="height: 410px; overflow-y: auto">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>Hyper</caption>
                <colgroup>
                    <col width="10%">
                    <col width="20%">
                    <col width="20%">
                    <col width="10%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <tbody>
                    <tr>
                        <!-- 최종수정일(자유휴무일) -->
                        <th>최종 수정일</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="F_chgDt"></span>
                        </td>
                        <!-- 최종수정일(정기휴무일) -->
                        <th>최종 수정일</th>
                        <td data-tagType="dynamic_content" colspan="2" >
                            <span id="R_chgDt"></span>
                        </td>
                    </tr>
                    <tr>
                        <!-- 최종등록자(자유휴무일) -->
                        <th>최종 등록자</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="F_chgId"></span>
                        </td>
                        <!-- 최종등록자(정기휴무일) -->
                        <th>최종 등록자</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="R_chgId"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>자유휴무일<br/>이미지</th>
                        <!-- PC 이미지(자유휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="F_PC">
                            <span class="mg-b-5">[PC] 사이즈 132X132</span>
                            <span id="F_pcImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <!-- Mobile 이미지(자유휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="F_MOBILE">
                            <span class="mg-b-5">[MO] 사이즈 132X132</span>
                            <span id="F_mobileImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <div id="closeDayTypeF" style="display:none;">
                            <input type="hidden" class="storeType" name="imageList[].storeType" value=""/>
                            <input type="hidden" class="closeDayType" name="imageList[].closeDayType" value="F"/>
                            <input type="hidden" class="pcImgUrl" name="imageList[].pcImgUrl" value=""/>
                            <input type="hidden" class="mobileImgUrl" name="imageList[].mobileImgUrl" value=""/>
                        </div>

                        <th>정기휴무일<br/>이미지</th>
                        <!-- PC 이미지(정기휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="R_PC">
                            <span class="mg-b-5">[PC] 사이즈 132X132</span>
                            <span id="R_pcImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <!-- Mobile 이미지(정기휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="R_MOBILE">
                            <span class="mg-b-5">[MO] 사이즈 132X132</span>
                            <span id="R_mobileImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <div id="closeDayTypeR" style="display:none;">
                            <input type="hidden" class="storeType" name="imageList[].storeType" value=""/>
                            <input type="hidden" class="closeDayType" name="imageList[].closeDayType" value="R"/>
                            <input type="hidden" class="pcImgUrl" name="imageList[].pcImgUrl" value=""/>
                            <input type="hidden" class="mobileImgUrl" name="imageList[].mobileImgUrl" value=""/>
                        </div>
                    </tr>
                    <tr>
                        <!-- 최종수정일(설휴무일) -->
                        <th>최종 수정일</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="S_chgDt"></span>
                        </td>
                        <!-- 최종수정일(추석휴무일) -->
                        <th>최종 수정일</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="C_chgDt"></span>
                        </td>
                    </tr>
                    <tr>
                        <!-- 최종등록자(설휴무일) -->
                        <th>최종 등록자</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="S_chgId"></span>
                        </td>
                        <!-- 최종등록자(추석휴무일) -->
                        <th>최종 등록자</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="C_chgId"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>설휴무일<br/>이미지</th>
                        <!-- PC 이미지(설휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="S_PC">
                            <span class="mg-b-5">[PC] 사이즈 132X132</span>
                            <span id="S_pcImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <!-- Mobile 이미지(설휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="S_MOBILE">
                            <span class="mg-b-5">[MO] 사이즈 132X132</span>
                            <span id="S_mobileImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <div id="closeDayTypeS" style="display:none;">
                            <input type="hidden" class="storeType" name="imageList[].storeType" value=""/>
                            <input type="hidden" class="closeDayType" name="imageList[].closeDayType" value="S"/>
                            <input type="hidden" class="pcImgUrl" name="imageList[].pcImgUrl" value=""/>
                            <input type="hidden" class="mobileImgUrl" name="imageList[].mobileImgUrl" value=""/>
                        </div>

                        <th>추석휴무일<br/>이미지</th>
                        <!-- PC 이미지(추석휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="C_PC">
                            <span class="mg-b-5">[PC] 사이즈 132X132</span>
                            <span id="C_pcImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <!-- Mobile 이미지(추석휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="C_MOBILE">
                            <span class="mg-b-5">[MO] 사이즈 132X132</span>
                            <span id="C_mobileImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <div id="closeDayTypeC" style="display:none;">
                            <input type="hidden" class="storeType" name="imageList[].storeType" value=""/>
                            <input type="hidden" class="closeDayType" name="imageList[].closeDayType" value="C"/>
                            <input type="hidden" class="pcImgUrl" name="imageList[].pcImgUrl" value=""/>
                            <input type="hidden" class="mobileImgUrl" name="imageList[].mobileImgUrl" value=""/>
                        </div>
                    </tr>
                    <tr>
                        <!-- 최종수정일(기타휴무일) -->
                        <th>최종 수정일</th>
                        <td data-tagType="dynamic_content" colspan="2" >
                            <span id="E_chgDt"></span>
                        </td>
                    </tr>
                    <tr>
                        <!-- 최종등록자(기타휴무일) -->
                        <th>최종 등록자</th>
                        <td data-tagType="dynamic_content" colspan="2">
                            <span id="E_chgId"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>기타휴무일<br/>이미지</th>
                        <!-- PC 이미지(기타휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="E_PC">
                            <span class="mg-b-5">[PC] 사이즈 132X132</span>
                            <span id="E_pcImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <!-- Mobile 이미지(기타휴무일) -->
                        <td data-tagType="dynamic_content" class="close_day_img" id="E_MOBILE">
                            <span class="mg-b-5">[MO] 사이즈 132X132</span>
                            <span id="E_mobileImgUrl">
                                <img width="150" height="280" src="">
                            </span>
                            <span><button type="button" class="ui button medium mg-t-5">등록</button></span>
                        </td>
                        <div id="closeDayTypeE" style="display:none;">
                            <input type="hidden" class="storeType" name="imageList[].storeType" value=""/>
                            <input type="hidden" class="closeDayType" name="imageList[].closeDayType" value="E"/>
                            <input type="hidden" class="pcImgUrl" name="imageList[].pcImgUrl" value=""/>
                            <input type="hidden" class="mobileImgUrl" name="imageList[].mobileImgUrl" value=""/>
                        </div>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<!-- 이미지정보 업로드 폼 -->
<input type="file" name="fileArr" id="closeDayImgFile" style="display: none;"/>

<script>
    const apiImgUrl = '${apiImgUrl}';

    var hyperImageList = JSON.parse('${closeDayImageListByHyperJson}');
    var auroraImageList = JSON.parse('${closeDayImageListByAuroraJson}');
    var clubImageList = JSON.parse('${closeDayImageListByClubJson}');

    $(document).ready(function() {
        closeDayImagePopMain.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/closeDayImagePopMain.js?v=${fileVersion}"></script>