<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">일괄발송처리/수정</h2>
        <ul class="mg-t-10">
            <li>- 원하시는 배송방법을 선택하시고 택배배송을 선택한 경우 해당하는 택배사를 선택해주세요.</li>
        </ul>
    </div>

    <!-- 파일업로드 form -->
    <form id="fileUploadForm">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>파일등록</caption>
                <colgroup>
                    <col width="100px">
                    <col width="400px">
                    <col width="*">
                </colgroup>
                <tbody>
                    <tr>
                        <th>배송방법</th>
                        <td class="ui form inline">
                            <label class="ui radio inline" style="margin-right: 5px"><input type="radio" id="shipMethod_DLV" name="shipMethod" value="DLV"><span>택배배송</span></label>
                            <select id="dlvCd" name="dlvCd" class="ui input inline medium mg-r-5" style="width: 150px" disabled>
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <label class="ui radio inline" style="margin-right: 5px"><input type="radio" id="shipMethod_DRCT" name="shipMethod" value="DRCT"><span>직접배송</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>파일등록</th>
                        <td>
                            <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="shipManageShipAllExcelPop.getFile($(this))" style="display: none">
                            <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly>
                        </td>
                        <td>
                            <button type="button" id="searchUploadFile" class="ui button medium">파일찾기</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- 엑셀양식 다운로드 -->
        <div class="mg-t-5">
            <a href="${templateUrlDlv}" class="text-underline" style="color: #5a569a">택배배송 일괄처리 엑셀양식 다운로드</a>
        </div>
        <div class="mg-t-5">
            <a href="${templateUrlDrct}" class="text-underline" style="color: #5a569a">직접배송 일괄처리 엑셀양식 다운로드</a>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<div id="shipManageShipAllExcelPopGrid" style="display: none;"></div>

<script>
    ${shipManageShipAllExcelPopGridBaseInfo}

    var callBackScript = '${callBackScript}';
    $(document).ready(function() {
        shipManageShipAllExcelPop.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipManageShipAllExcelPop.js?v=${fileVersion}"></script>
