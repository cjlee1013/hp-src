<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="itemShiftManageSearchForm" name="itemShiftManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">상품별 Shift 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>상품별 Shift 관리</caption>
                    <tbody>
                        <tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'home'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
                        </td>
                        <th>상품</th>
                        <td class="ui form inline">
                            <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10">
                                <option value="ITEM_NO">상품번호</option>
                                <option value="ITEM_NM">상품명</option>
                            </select>
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input">
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>카테고리</th>
                            <td colspan="3">
                                <select id="schCateCd1" name="schLcateCd" class="ui input medium mg-r-5" style="float:left;" data-default="대분류" onchange="commonCategory.changeCategorySelectBox(1,'schCateCd');"></select>
                                <select id="schCateCd2" name="schMcateCd" class="ui input medium mg-r-5" style="float:left;" data-default="중분류" onchange="commonCategory.changeCategorySelectBox(2,'schCateCd');"></select>
                                <select id="schCateCd3" name="schScateCd" class="ui input medium mg-r-5" style="float:left;" data-default="소분류" onchange="commonCategory.changeCategorySelectBox(3,'schCateCd');"></select>
                                <select id="schCateCd4" name="schDcateCd" class="ui input medium mg-r-5" data-default="세분류"></select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="itemShiftManageSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button type="button" id="excelUploadInsert" class="ui button large btn-outline-info" style="min-width: auto">엑셀일괄등록</button>
                <button type="button" id="excelUploadUpdate" class="ui button large btn-outline-info" style="min-width: auto">엑셀일괄수정</button>
                <button type="button" id="removeBtn" class="ui button large btn-outline-info" style="min-width: auto">삭제</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="itemShiftManageGrid" style="width: 100%; height: 500px;"></div>
    </div>

    <!-- 하단 적용 점포 정보 -->
    <form id="applyItemShiftManageForm" name="applyItemShiftManageForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3 class="title">상세정보</h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>적용점포</caption>
                <tbody>
                <tr>
                    <th>점포</th>
                    <td>
                        <div class="form ui inline">
                            <label>
                                <select name="storeType" id="storeType" class="ui input medium mg-r-10" >
                                    <option value="HYPER">HYPER</option>
<%--                                    [ESCROW-343, CLUB 노출제외]--%>
<%--                                    <option value="CLUB">CLUB</option>--%>
                                </select>
                            </label>

                            <button type="button" id="selectStorePopBtn" class="ui button medium gray-dark" onclick="deliveryCore_popup.openStoreSelectPopup();">점포선택</button>
                            <span id="selectStorePopResult" class="text mg-l-5">( 적용 점포수 : <span id="applyStoreListCount">0</span> 개 )</span>
                            <span id="applyStoreType"></span>   <!-- 그리드에서 선택/팝업창에서 return 받은 store 의 type -->
                            <span id="applyStoreId"></span>     <!-- 그리드에서 선택한 1건의 store id -->
                            <span id="applyStoreNm" class="text mg-l-5"></span> <!-- 그리드에서 선택한 1건의 store name -->
                            <span id="applyStoreList"></span>   <!-- 점포선택 팝업창에서 return 받은 다건의 store list (json) -->
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>상품</th>
                    <td>
                        <div class="form ui inline">
                            <button type="button" id="selectItemPopBtn" class="ui button medium gray-dark">상품검색</button>
                            <span id="applyItemNo" class="text mg-l-5"></span>
                            <span class="text mg-l-5">|</span>
                            <span id="applyItemNm" class="text mg-l-5"></span>
                        </div>
                    </td>
                    <th>사용 Shift</th>
                    <td>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift1" name="shift1" checked>
                            <span>Shift1</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift2" name="shift2" checked>
                            <span>Shift2</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift3" name="shift3" checked>
                            <span>Shift3</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift4" name="shift4" checked>
                            <span>Shift4</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift5" name="shift5" checked>
                            <span>Shift5</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift6" name="shift6" checked>
                            <span>Shift6</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift7" name="shift7" checked>
                            <span>Shift7</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift8" name="shift8" checked>
                            <span>Shift8</span>
                        </label>
                        <label class="ui checkbox mg-r-20" style="display: inline-block">
                            <input type="checkbox" id="shift9" name="shift9" checked>
                            <span>Shift9</span>
                        </label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 하단 버튼영역 (저장/초기화) -->
    <div class="ui center-button-group mg-t-15 ">
        <span class="inner">
            <button id="saveBtn" style="width:60px;" class="ui button large btn-danger">저장</button>
            <button id="resetBtn" style="width:60px;" class="ui button large">초기화</button>
        </span>
    </div>

    <!-- 점포선택 팝업창을 호출하기 위한 hidden form -->
    <span style="display: none">
        <form id="storeSelectPopForm" name="storeSelectPopForm">
            <input type="text" name="storeTypeRadioYn" value="Y">   <!-- store type 을 팝업에서 선택할 수 있도록 함 (Y) -->
            <input type="text" name="storeTypePop" value="HYPER">      <!-- df store type 지정 (HYPER) -->
            <input type="text" name="storeList" value="">           <!-- 여기에 담은 store 만 노출시켜주는 듯 함 (이 화면에선 사용하지 않아서 빈 값) -->
            <input type="text" name="callBackScript" value="deliveryCore.callBack.setStoreNoList"> <!-- 팝업 닫히면서 부를 call back script 지정 -->
        </form>
    </span>
</div>

<script>
    ${itemShiftManageGridBaseInfo}
    $(document).ready(function() {
        itemShiftManageMain.init();
        itemShiftManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/itemShiftManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>