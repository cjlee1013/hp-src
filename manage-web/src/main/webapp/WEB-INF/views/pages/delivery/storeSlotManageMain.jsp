<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<div class="escrow-main">
    <div>
        <form id="storeSlotSearchForm" name="storeSlotSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">Slot 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>Shift</caption>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'home'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>일자</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="storeSlotSearchCnt">0</span>건</h3>
            <span class="ui form inline" style="float: right;">
                <select id="saveUseYn" name="saveUseYn" class="ui input medium mg-r-10">
                    <option value="Y">사용</option>
                    <option value="N">사용안함</option>
                </select>
                <button type="button" id="chgUseYnBtn" class="ui button medium sky-blue">변경</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="storeSlotGrid" style="height: 500px;"></div>
    </div>
</div>
<script>
    ${storeSlotGridBaseInfo}
    $(document).ready(function() {
        storeSlotManageMain.init();
        storeSlotGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/storeSlotManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>

<script>
    var weekdayJson = ${weekdayJson};
</script>