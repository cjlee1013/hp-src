<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="pickupStoreManageSearchForm" name="pickupStoreManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">픽업 점포 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>픽업 점포 관리</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="40%">
                        <col width="10%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td class="ui form inline" colspan="3">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="searchBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="resetBtn" class="ui button large white mg-t-5">초기화</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>장소사용여부</th>
                        <td class="ui form inline">
                            <select id="schPlaceUseYn" name="schPlaceUseYn" class="ui input medium mg-r-10" style="float:left;">
                                <option value="">전체</option>
                                <option value="N">N</option>
                                <option value="Y">Y</option>
                            </select>
                        </td>
                        <th>락커운영여부</th>
                        <td>
                            <select id="schLockerUseYn" name="schLockerUseYn" class="ui input medium mg-r-10" style="float:left;">
                                <option value="">전체</option>
                                <option value="N">N</option>
                                <option value="Y">Y</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="pickupStoreManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="pickupStoreManageGrid" style="width: 100%; height: 330px;"></div>
    </div>

    <!-- 하단 상세 정보 -->
    <form id="pickupStoreDetailForm" name="pickupStoreDetailForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3 class="title">상세정보</h3>
        </div>
        <div class="mg-t-10">
            * 픽업장소 및 데스트 전화번호 작성 안내<br>
            - 첫번째 입력 란 : 배송옵션페이지 라디오버튼에 노출되는 장소    e.g. 주차장 B1 A45<br>
            - 두번째 입력 란 : 주문/결제 페이지 "픽업서비스 수령고객정보"의 "수령장소"에 노출되는 상세주소    e.g. 기둥번호 H05 맞은편<br>
            - 세번째 입력 란 : 배송옵션페이지의 장소보기 안내 레이어에 노출되는 전화번호    e.g. 02-890-8167
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <colgroup>
                    <col width="10%">
                    <col width="90%">
                </colgroup>
                <tbody>
                <tr>
                    <th>점포</th>
                    <td>
                        <span class="text" id="storeId"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="drawDiv"></div>
    </form>
</div>

<script>
    ${pickupStoreManageGridBaseInfo}
    $(document).ready(function() {
        pickupStoreManageMng.init();
        pickupStoreManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pickupStoreManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
