<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="dailyShiftManageSearchForm" name="dailyShiftManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">특정일 배송관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>특정일 배송관리</caption>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" disabled="true">
                                    <option value="HYPER">Hyper</option>
                                </select>
                            </div>
                        </td>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10">
                                    <option value="NAME">점포명</option>
                                    <option value="ID">점포코드</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" value="">
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="button" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀 다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상품</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schItemType" name="schItemType" class="ui input medium mg-r-10">
                                    <option value="NO">상품번호</option>
                                    <option value="NAME">상품명</option>
                                </select>
                                <input type="text" id="schItemKeyword" name="schItemKeyword" class="ui input medium mg-r-5" value="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>일자</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-10">
                                    <option value="ORDER">주문기간</option>
                                    <option value="SHIP">배송기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="dailyShiftMng.setSearchDate('T');">오늘</button>
                                <button type="button" class="ui button small white font-malgun mg-r-5" onclick="dailyShiftMng.setSearchDate('W');">1주일</button>
                                <button type="button" class="ui button small white font-malgun mg-r-5" onclick="dailyShiftMng.setSearchDate('M');">1개월</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 메인 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="dailyShiftManageSearchCnt">0</span> 건</h3>
            <span style="float: right;">
                <button type="button" id="removeBtn" class="ui button large btn-outline-info">삭제</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="dailyShiftManageGrid" style="width: 100%; height: 290px;"></div>
    </div>

    <!-- 상세 그리드 영역 -->
    <div class="mg-t-50"></div>
    <div>
        <form id="dailyShiftInputForm" name="dailyShiftInputForm" onsubmit="return false;">
            <input type="hidden" id="dailyShiftMngSeq" name="dailyShiftMngSeq" value="">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>특정일 배송관리 상세내역</caption>
                    <tbody>
                    <tr>
                        <th>상품</th>
                        <td>
                            <div class="ui form inline">
                                <button type="button" id="itemSelectBtn" class="ui button medium gray-dark">상품검색</button>
                                <span class="text mg-l-5">[</span>
                                <span id="applyItemNo" class="text"></span>
                                <span class="text">]</span>
                                <span id="applyItemNm" class="text mg-l-5"></span>
                            </div>
                        </td>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <button type="button" id="storeSelectBtn" class="ui button medium gray-dark" onclick="deliveryCore_popup.openStoreSelectPopup();">점포선택</button>
                                <span class="text mg-l-5">(적용 점포 수 : <span id="applyStoreListCount">0</span>개)</span>
                                <span id="applyStoreList"></span>   <!-- 점포선택 팝업창에서 return 받은 선택된 store 리스트 (json) -->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>주문기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="orderStartDt" name="orderStartDt" class="ui input medium mg-r-5">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="orderEndDt" name="orderEndDt" class="ui input medium mg-r-5"">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <th>배송기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="shiftShipStartDt" name="shiftShipStartDt" class="ui input medium mg-r-5">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="shiftShipEndDt" name="shiftShipEndDt" class="ui input medium mg-r-5">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge cyan" id="saveBtn">저장</button>
            <button class="ui button xlarge" id="resetBtn">초기화</button>
        </span>
    </div>

    <!-- 점포선택 팝업창을 호출하기 위한 hidden form -->
    <span style="display: none">
        <form id="storeSelectPopForm" name="storeSelectPopForm">
            <input type="text" name="storeTypePop" value="HYPER">
            <input type="text" name="storeTypeRadioYn" value="N">
            <input type="text" id="storeList" name="storeList" value="">
            <input type="text" name="callBackScript" value="deliveryCore.callBack.setStoreNoList">
        </form>
    </span>
</div>

<script>
    ${dailyShiftManageGridBaseInfo}
    $(document).ready(function() {
        dailyShiftMng.init();
        dailyShiftManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/dailyShiftManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>
