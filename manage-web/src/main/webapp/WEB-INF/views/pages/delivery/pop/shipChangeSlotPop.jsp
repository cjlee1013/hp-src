<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="com wrap-popup system" id="divPop">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 배송시간 수정
        </h2>
    </div>
    <div class="com wrap-title sub" >
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>배송시간 수정</caption>
                <colgroup>
                    <col width="100px">
                    <col width="*">
                </colgroup>
                <div class="ui form inline">
                    <tbody>
                        <tr>
                            <th>배송점포</th>
                            <td><span id="storeNm"></span></td>
                        </tr>
                    </tbody>
                </div>
            </table>
        </div>
    </div>
    <div class="ui wrap-table" style="max-width: 800px; overflow:hidden;position:relative;overflow-x:scroll;" id="slotChangeArea">
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipChangeSlotPop.js?v=${fileVersion}"></script>
<script>
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        shipChangeSlotPop.init();
    });

</script>