<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">연동업체설정</h2>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="shipLinkCompanySetPopSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="shipLinkCompanySetPopGrid" style="width: 100%; height: 200px;"></div>
    </div>

    <!-- 상세정보 -->
    <form id="shipLinkCompanySetForm" name="shipLinkCompanySetForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption></caption>
                <colgroup>
                    <col width="20%">
                    <col width="30%">
                    <col width="15%">
                    <col width="35%">
                </colgroup>
                <tbody>
                <tr>
                    <th>연동업체</th>
                    <td>
                        <select id="dlvLinkCompany" name="dlvLinkCompany" class="ui input medium mg-r-10" style="width: 150px">
                            <option value="">선택</option>
                            <c:forEach var="codeDto" items="${dlvLinkCompany}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <th>연동여부</th>
                    <td>
                        <label class="ui radio inline" ><input type="radio" name="linkYn" class="ui input medium" value="Y"><span>Y</span></label>
                        <label class="ui radio inline" ><input type="radio" name="linkYn" class="ui input medium" value="N" checked><span>N</span></label>
                    </td>
                </tr>
                <tr>
                    <th>연동중지기간</th>
                    <td class="ui form inline" colspan="3">
                        <label class="ui inline">
                            <input type="checkbox" id="chkStopDt" name="chkStopDt" class="mg-r-5">
                            <span class="text mg-r-10">설정</span>
                        </label>
                        <input type="text" id="stopStartDt" name="stopStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" disabled>
                        <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                        <input type="text" id="stopEndDt" name="stopEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" disabled>
                        <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 하단 버튼영역 (저장/초기화) -->
    <div class="ui center-button-group mg-t-15 ">
        <span class="inner">
            <button id="saveBtn" style="width:60px;" class="ui button large btn-danger">저장</button>
            <button id="resetBtn" style="width:60px;" class="ui button large">초기화</button>
        </span>
    </div>
</div>

<script>
    ${shipLinkCompanySetPopGridBaseInfo}
    $(document).ready(function() {
        shipLinkCompanySetPop.init();
        shipLinkCompanySetPopGrid.init();
        shipLinkCompanySetPop.search();

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipLinkCompanySetPop.js?v=${fileVersion}"></script>