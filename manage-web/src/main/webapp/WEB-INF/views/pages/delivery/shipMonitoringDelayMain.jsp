<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="shipMonitoringDelaySearchForm" name="shipMonitoringDelaySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">발송지연조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>발송지연조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="45%">
                        <col width="10%">
                        <col width="20%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schShipDtType" name="schShipDtType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="orderDt">주문일</option>
                                    <option value="confirmDt">주문확인일</option>
                                    <option value="orgShipDt">발송기한</option>
                                    <option value="delayShipDt">2차발송기한</option>
                                </select>
                                <input type="text" id="schStartDt" name="schShipStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schShipEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun mg-r-5">1개월</button>
                            </td>
                            <td rowspan="5">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>점포/판매업체</th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 100px;">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 130px;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 130px;" readonly>
                                <button id="schStoreBtn" type="button" class="ui button medium">조회</button>
                            </td>
                            <th>배송방법</th>
                            <td class="ui form inline">
                                <select id="schShipMethod" name="schShipMethod" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${shipMethod}" varStatus="status">
                                        <c:choose>
                                            <c:when test="${codeDto.ref1 eq 'DS'}">
                                                <option value="${codeDto.mcCd}" title="DS" style="display: none;">${codeDto.mcNm}</option>
                                            </c:when>
                                            <c:otherwise>
<%--                                                <option value="${codeDto.mcCd}" title="TD" style="display: none;">${codeDto.mcNm}</option>--%>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                    <option value="TD_DRCT" title="TD" style="display: none;">자차</option>
                                    <option value="TD_DLV_HDS" title="TD" style="display: none;">택배-업체배송</option>
                                    <option value="TD_DLV_GIFT" title="TD" style="display: none;">택배-명절선물</option>
                                    <option value="TD_DLV_RESERVE" title="TD" style="display: none;">택배-예약일반</option>
                                    <option value="TD_PICK" title="TD" style="display: none;">픽업</option>
                                    <option value="TD_QUICK" title="TD" style="display: none;">퀵</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>배송상태</th>
                            <td class="ui form inline">
                                <select id="schShipStatus" name="schShipStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                        <c:if test="${codeDto.mcCd eq 'D1' or codeDto.mcCd eq 'D2'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>발송지연안내</th>
                            <td class="ui form inline">
                                <select id="schShipDelay" name="schShipDelay" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="Y">처리</option>
                                    <option value="N">미처리</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>검색조건</th>
                            <td class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="purchaseOrderNo">주문번호</option>
                                    <option value="bundleNo">배송번호</option>
                                    <option value="receiverNm">수령인</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 200px">
                            </td>
                            <th>지연일수</th>
                            <td class="ui form inline">
                                <select id="schDelayType" name="schDelayType" class="ui input medium mg-r-10" style="width: 110px">
                                    <option value="delayConfirmDay">주문확인지연</option>
                                    <option value="delayShipDay">발송지연</option>
                                </select>
                                <input type="text" id="schDelayDay" name="schDelayDay" class="ui input" style="width: 40px"><span class="text">일</span>
                            </td>
                        </tr>
                        <tr>
                            <th>마켓연동</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schMarketType" name="schMarketType" class="ui input medium mg-r-10" style="width: 150px;">
                                    <option value="">전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="shipMonitoringDelaySearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="shipMonitoringDelayGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${shipMonitoringDelayGridBaseInfo}
    var theclubFrontUrl = '${theclubFrontUrl}'; // 더클럽 프론트 페이지 URL
    var homeplusFrontUrl = '${homeplusFrontUrl}'; // 홈플러스 프론트 페이지 URL
    var so_storeId = '${so_storeId}';
    var so_storeNm = '${so_storeNm}';
    var so_storeType = '${so_storeType}';

    $(document).ready(function() {
        shipMonitoringDelayMain.init();
        shipMonitoringDelayGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shipMonitoringDelayMain.js?v=${fileVersion}"></script>