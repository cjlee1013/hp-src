<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="storeZipcodeSearchForm" name="storeZipcodeSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">점포별 우편번호 조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>점포별 우편번호 조회</caption>
                    <tbody>
                        <tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'home'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
                        </td>
                        <th>원거리배송<br>여부</th>
                        <td>
                            <select id="schRemoteShipYn" name="schRemoteShipYn" class="ui input medium mg-r-10">
                                <option value="">전체</option>
                                <option value="Y">가능</option>
                                <option value="N">불가</option>
                            </select>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>우편번호</th>
                            <td>
                                <input type="text" id="schZipcode" name="schZipcode" class="ui input medium" maxlength="5">
                            </td>
                            <th>주소</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schSidoType" name="schSidoType" class="ui input medium mg-r-10">
                                        <option value="">시/도 전체</option>
                                        <c:forEach var="sido" items="${sidoList}">
                                            <option value="${sido}">${sido}</option>
                                        </c:forEach>
                                    </select>
                                    <select id="schSigunguType" name="schSigunguType" class="ui input medium">
                                        <option value="">시군구 전체</option>
                                    </select>

                                    <div id="schInputBox" name="schInputBox">
                                        <!-- 도로명/건물명 InputBox -->
                                        <input type="text" id="schAddrKeyword" name="schAddrKeyword" class="ui input mg-t-10 mg-r-10" maxlength="100" placeholder="도로명/건물명">
                                        <!-- 건물번호 InputBox -->
                                        <input type="text" id="schBuildNo" name="schBuildNo" class="ui input mg-t-10" maxlength="50" placeholder="건물번호">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="storeZipcodeSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="storeZipcodeGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${storeZipcodeGridBaseInfo}
    $(document).ready(function() {
        storeZipcodeMain.init();
        storeZipcodeGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/storeZipcodeMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>