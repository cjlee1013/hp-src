<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="storeCloseDaySearchForm" name="storeCloseDaySearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun" style="float: none">점포별 휴일 조회</h2>
                <div class="mg-t-10">
                    점포별 휴일이 등록되면 저장되는 즉시 해당 점포 배송옵션, 배송가능시간 안내 팝업에서 등록이레 세로 이미지가 노출되어 고객에게 주문 불가함을 알립니다.<br>
                    단, front 안내 노출에 관한 메뉴이므로 해당일에 주문 자체를 막기 위해서는 제휴사 및 모바일 주문 제어를 위해 해당일 slot들도 0처리 해주셔야 합니다. (배송관리>점포배송정보>Slot관리)
                </div>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>점포별 휴일 조회</caption>
                    <tbody>
                        <tr>
                            <th>점포유형</th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                                    <option value="">전체</option>
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'home'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                                </select>
                            </td>
                            <th>휴무유형</th>
                            <td>
                                <select id="schCloseDayType" name="schCloseDayType" class="ui input medium mg-r-10" >
                                    <option value="">전체</option>
                                    <c:forEach var="closeDayType" items="${closeDayTypeList}">
                                        <option value="${closeDayType.mcCd}">${closeDayType.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="3">
                                <div class="ui form">
                                    <button type="submit" id="schBtn" class="ui button large cyan">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>검색어</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schKeywordType" name="schKeywordType" class="ui input">
                                        <option value="SEQ">등록번호</option>
                                        <option value="TITLE">등록제목</option>
                                    </select>
                                    <input type="text" id="schKeyword" name="schKeyword" class="ui input mg-l-10">
                                </div>
                            </td>
                            <th>진행상태</th>
                            <td>
                                <select id="schUseYn" name="schUseYn" class="ui input">
                                    <option value="">전체</option>
                                    <option value="Y">정상</option>
                                    <option value="N">취소</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>진행기간</th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <input type="text" id="schApplyStartDt" name="schApplyStartDt" class="ui input medium mg-r-5" placeholder="시작일">
                                    <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                    <input type="text" id="schApplyEndDt" name="schApplyEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                    <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                                    <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일</button>
                                    <button type="button" id="setOneMonthBtn" class="ui button small gray-dark font-malgun">1개월</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="storeCloseDaySearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button type="button" id="chgImageBtn" class="ui button large btn-outline-info" onclick="storeCloseDayMain.openChgImagePopup();">이미지변경</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="storeCloseDayGrid" style="width: 100%; height: 500px;"></div>
    </div>

    <!-- 하단 적용 점포 정보 -->
    <form id="applyStoreCloseDayForm" name="applyStoreCloseDayForm" onsubmit="return false;" style="display: none">
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3 id="applyTitleNm" name="applyTitleNm" class="title"></h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>휴무점포</caption>
                <tbody>
                <tr>
                    <th>휴무점포</th>
                    <td id="applyStoreNm" name="applyStoreNm"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

<script>
    ${storeCloseDayGridBaseInfo}
    $(document).ready(function() {
        storeCloseDayMain.init();
        storeCloseDayGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/storeCloseDayMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>