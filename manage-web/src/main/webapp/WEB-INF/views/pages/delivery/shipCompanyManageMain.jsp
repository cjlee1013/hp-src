<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="shipCompanyManageSearchForm" name="shipCompanyManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">택배사코드 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>택배사코드 관리</caption>
                    <colgroup>
                        <col width="120px">
                        <col width="*">
                        <col width="200px">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>연동업체</th>
                            <td colspan="2" class="ui form inline">
                                <select id="schDlvLinkCompany" name="schDlvLinkCompany" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${dlvLinkCompany}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td rowspan="2">
                                <div class="ui form">
                                    <button type="submit" id="schBtn" class="ui button large cyan">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>검색어</th>
                            <td class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <option value="dlvNm">택배사명</option>
                                    <option value="dlvCd">내부코드</option>
                                    <option value="dlvLinkCompanyCd">연동업체코드</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 300px">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="shipCompanyManageSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button type="button" id="shipLinkCompanySetPop" class="ui button large btn-outline-info" style="min-width: auto">연동업체설정</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="shipCompanyManageGrid" style="width: 100%; height: 500px;"></div>
    </div>

    <!-- 상세정보 -->
    <form id="shipCompanyDetailForm" name="shipCompanyDetailForm" onsubmit="return false;">
        <div class="topline"></div>
        <div class="com wrap-title sub">
            <h3 class="title">상세정보</h3>
        </div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption></caption>
                <colgroup>
                    <col width="16%">
                    <col width="20%">
                    <col width="10%">
                    <col width="24%">
                    <col width="8%">
                    <col width="22%">
                </colgroup>
                <tbody>
                    <tr>
                        <th><span class="text-red-star">택배사명</span></th>
                        <td class="ui form inline">
                            <select id="dlvCd" name="dlvCd" class="ui input medium mg-r-10" style="width: 230px">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <th><span class="text-red-star">연동업체</span></th>
                        <td class="ui form inline" colspan="3">
                            <select id="dlvLinkCompany" name="dlvLinkCompany" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="">선택</option>
                                <c:forEach var="codeDto" items="${dlvLinkCompany}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:forEach>
                            </select>
                            <input type="text" id="dlvLinkCompanyCd" name="dlvLinkCompanyCd" class="ui input" style="width: 230px" placeholder="코드" maxlength="15">
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">자동반품접수</span></th>
                        <td>
                            <label class="ui radio inline" ><input type="radio" name="autoReturnYn" class="ui input medium" value="Y"><span>Y</span></label>
                            <label class="ui radio inline" ><input type="radio" name="autoReturnYn" class="ui input medium" value="N"><span>N</span></label>
                        </td>
                        <th><span class="text-red-star">자동반품지연</span></th>
                        <td>
                            <label class="ui radio inline" ><input type="radio" name="autoReturnDelayYn" class="ui input medium" value="Y"><span>Y</span></label>
                            <label class="ui radio inline" ><input type="radio" name="autoReturnDelayYn" class="ui input medium" value="N"><span>N</span></label>
                        </td>
                        <th><span class="text-red-star">노출여부</span></th>
                        <td>
                            <label class="ui radio inline" ><input type="radio" name="displayYn" class="ui input medium" value="Y"><span>Y</span></label>
                            <label class="ui radio inline" ><input type="radio" name="displayYn" class="ui input medium" value="N"><span>N</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>고객센터 전화번호</th>
                        <td class="ui form inline">
                            <input type="text" id="csPhone" name="csPhone" class="ui input" style="width: 200px" maxlength="15">
                        </td>
                        <th>홈페이지</th>
                        <td class="ui form inline" colspan="3">
                            <input type="text" id="homePage" name="homePage" class="ui input" style="width: 300px" maxlength="20">
                        </td>
                    </tr>
                    <tr>
                        <th>비고</th>
                        <td class="ui form inline" colspan="4">
                            <textarea id="note" name="note" class="ui input mg-t-5" style="width: 600px; height: 60px;" maxlength="100"></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>

    <!-- 하단 버튼영역 (저장/초기화) -->
    <div class="ui center-button-group mg-t-15 ">
        <span class="inner">
            <button id="saveBtn" style="width:60px;" class="ui button large btn-danger">저장</button>
            <button id="resetBtn" style="width:60px;" class="ui button large">초기화</button>
        </span>
    </div>
</div>

<script>
    ${shipCompanyManageGridBaseInfo}
    var linkCompanyCodeList = {};

    $(document).ready(function() {
        shipCompanyManageMain.init();
        shipCompanyManageGrid.init();
        shipCompanyManageMain.search();

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shipCompanyManageMain.js?v=${fileVersion}"></script>