<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">선택 발송처리/수정</h2>
        <ul class="mg-t-10">
            <li>- 선택한 주문에 대해 택배사와 송장번호를 입력해 주세요.</li>
            <li>- 우편과 직접배송은 송장번호 없이 처리할 수 있으며, 배송중으로 상태값이 변경됩니다.</li>
        </ul>
    </div>

    <div class="ui wrap-table horizontal mg-t-1 0">
        <table class="ui table">
            <caption></caption>
            <colgroup>
                <col width="140px">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <td class="ui form inline">
                        <select id="shipMethod" name="shipMethod" class="ui input medium mg-r-5" style="width: 120px">
                            <option value="">배송방법 선택</option>
                            <c:forEach var="codeDto" items="${shipMethod}" varStatus="status">
                                <c:choose>
                                    <c:when test="${mallType eq 'DS'}">
                                        <c:if test="${codeDto.ref1 eq 'DS'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${codeDto.ref1 eq 'TD' and codeDto.mcCd ne 'TD_DRCT' and codeDto.mcCd ne 'TD_PICK'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </td>
                    <td class="ui form inline">
                        <select id="dlvCd" name="dlvCd" class="ui input inline medium mg-r-10" style="width: 150px; display: none;">
                            <option value="">택배사 선택</option>
                            <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                            </c:forEach>
                        </select>
                        <input type="text" id="invoiceNo" name="invoiceNo" class="ui input" style="width: 150px; display: none;" placeholder="송장번호" maxlength="30">
                        <input type="text" id="scheduleShipDt" name="scheduleShipDt" class="ui input medium mg-r-5" placeholder="배송예정일" style="width:100px; display: none;" readonly>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="topline"></div>
        <div id="shipManageShipAllPopGrid" style="width: 100%; height: 250px;"></div>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    ${shipManageShipAllPopGridBaseInfo}
    var callBackScript = '${callBackScript}';
    var bundleNo = '${bundleNo}';
    $(document).ready(function() {
        shipManageShipAllPop.init();
        shipManageShipAllPopGrid.init();
        shipManageShipAllPop.search();

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipManageShipAllPop.js?v=${fileVersion}"></script>
