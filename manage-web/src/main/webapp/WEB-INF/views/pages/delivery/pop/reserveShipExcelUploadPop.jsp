<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 id="titlePop" class="title font-malgun" style="float: none">엑셀업로드</h2>
    </div>

    <!-- 파일업로드 form -->
    <form id="fileUploadForm">
        <div class="ui wrap-table horizontal mg-t-1 0">
            <table class="ui table">
                <caption>파일등록</caption>
                <colgroup>
                    <col width="20%">
                    <col width="70%">
                    <col width="10%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>파일등록</th>
                        <td>
                            <input type="file" id="uploadFile" name="uploadFile" accept=".xlsx" onchange="reserveShipExcelUploadPopMng.getFile($(this))" style="display: none">
                            <input type="text" id="uploadFileNm" name="uploadFileNm" class="ui input medium" readonly>
                        </td>
                        <td>
                            <button type="button" id="searchUploadFile" class="ui button medium">파일찾기</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- 엑셀양식 다운로드 -->
        <div class="mg-t-5">
            <a href="${templateUrl}" class="text-underline" style="color: #5a569a">엑셀 양식 다운로드</a>
        </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">등록</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript = '${callback}';
    $(document).ready(function() {
        reserveShipExcelUploadPopMng.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/reserveShipExcelUploadPop.js?v=${fileVersion}"></script>