<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="reserveShipPlaceSearchForm" name="reserveShipPlaceSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">선물세트 발송지 관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>선물세트 발송지 관리 검색</caption>
                    <tbody>
                    <tr>
                        <th>발송일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="schFromDt" name="schFromDt" class="ui input medium mg-r-5" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="reserveShipPlaceSearchCnt">0</span>건</h3>
            <span style="float: right;">
                <button type="button" id="excelUploadBtn" class="ui button large btn-outline-info">엑셀업로드</button>
                <button type="button" id="removeBtn" class="ui button large btn-outline-info">삭제</button>
            </span>
        </div>
        <div class="topline"></div>
        <div id="reserveShipPlaceManageGrid" style="height: 330px;"></div>
    </div>

    <!-- 상세영역 -->
    <form id="reserveShipPlaceInputForm" name="reserveShipPlaceInputForm" onsubmit="return false;">
    <div class="com wrap-title sub">
        <h3 class="title">상세정보</h3>
    </div>
    <div class="topline"></div>
    <div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table">
                <caption>선물세트 발송지 상세내역</caption>
                <tbody>
                <tr>
                    <th>발송일&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <div class="ui form inline">
                            <input type="text" id="shipDt" name="shipDt" class="ui input medium mg-r-5" placeholder="발송일" readonly>
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </td>
                    <th>발송지&nbsp;<span class="text-red">*</span></th>
                    <td>
                        <span class="text">점포&nbsp;</span>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceStoreYn" id="shipPlaceStoreY" value="Y" checked><span>Y</span></label>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceStoreYn" id="shipPlaceStoreN" value="N"><span>N</span></label>
                        <span class="text">&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;중앙(안성)&nbsp;</span>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceAnseongYn" id="shipPlaceAnseongY" value="Y" checked><span>Y</span></label>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceAnseongYn" id="shipPlaceAnseongN" value="N"><span>N</span></label>
                        <span class="text">&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;중앙(함안)&nbsp;</span>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceHamanYn" id="shipPlaceHamanY" value="Y" checked><span>Y</span></label>
                        <label class="ui radio small inline"><input type="radio" name="shipPlaceHamanYn" id="shipPlaceHamanN" value="N"><span>N</span></label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </form>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button xlarge cyan" id="saveBtn">저장</button>
            <button class="ui button xlarge" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script>
    ${reserveShipPlaceManageGridBaseInfo}
    $(document).ready(function () {
        reserveShipPlaceManageMng.init();
        reserveShipPlaceManageGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/reserveShipPlaceManageMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>