<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="shipInfoPrintPop" class="com wrap-popup medium"
     style="margin:0 0 7px 0;padding:10px 20px 0 20px; height:580px;max-height: 580px; width: 980px; overflow:hidden;overflow-y:scroll;background-color:#f7f7f7;">
    <div id="printArea" >
    </div>
    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group mg-b-10">
        <span class="inner">
            <button id="printBtn" class="ui button xlarge btn-danger font-malgun">출력</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>

<script>
    const bundleNo = '${bundleNo}';

    $(document).ready(function() {
      shipInfoPrintPop.init();
    });
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipInfoPrintPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>