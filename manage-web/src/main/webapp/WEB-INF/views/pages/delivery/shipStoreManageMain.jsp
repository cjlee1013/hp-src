<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="shipStoreManageSearchForm" name="shipStoreManageSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">점포배송관리</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>점포배송관리</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="46%">
                        <col width="9%">
                        <col width="26%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><span class="text-red-star">점포</span></th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 100px;">
                                <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                    <c:if test="${codeDto.mcCd eq 'HYPER' or codeDto.mcCd eq 'EXP'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 130px;" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 140px;" placeholder="점포명" readonly>
                            <button id="schStoreBtn" type="button" class="ui button medium">조회</button>
                        </td>
                        <th>주문상태</th>
                        <td colspan="2" class="ui form inline">
                            <select id="schShipStatus" name="schShipStatus" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${shipStatus}" varStatus="status">
                                    <c:if test="${codeDto.mcCd ne 'NN'}">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                        <td rowspan="4">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <c:if test="${packageExcel}">
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </c:if>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">조회기간</span></th>
                        <td class="ui form inline">
                            <select id="schShipDtType" name="schShipDtType" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="orderDt">주문일</option>
                                <option value="reserveShipDt">배송요청일</option>
                            </select>
                            <input type="text" id="schStartDt" name="schShipStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                            <input type="hidden" id="schEndDt" name="schShipEndDt">
                            <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">오늘</button>
                            <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">어제</button>
                        </td>
                        <th>배송방법</th>
                        <td colspan="2" class="ui form inline">
                            <select id="schShipMethod" name="schShipMethod" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="">전체</option>
                                <c:forEach var="codeDto" items="${shipMethod}" varStatus="status">
                                    <c:if test="${codeDto.ref1 eq 'TD' and codeDto.mcCd ne 'TD_DLV'}">
                                        <option value="${codeDto.mcCd}" style="display: none;">${codeDto.mcNm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>구매자</th>
                        <td class="ui form inline">
                            <label class="ui radio inline" ><input type="radio" name="schNomemOrderYn" value="N" checked><span>회원</span></label>
                            <label class="ui radio inline" ><input type="radio" name="schNomemOrderYn" value="Y"><span>비회원</span></label>
                            <select id="schNomemOrderType" name="schNomemOrderType" class="ui input medium mg-r-10" style="width: 120px; display: none">
                                <option value="buyerNm">구매자 이름</option>
                                <option value="buyerMobileNo">구매자 연락처</option>
                            </select>
                            <input type="text" id="schUserNo" name="schUserNo" class="ui input mg-r-5" style="width: 100px; display: none">
                            <input type="text" id="schNomemOrderWord" name="schNomemOrderWord" class="ui input mg-r-5" style="width: 120px;" readonly>
                            <button id="schBuyer" type="button" class="ui button medium">조회</button>
                        </td>
                        <th>단축번호</th>
                        <td colspan="2" class="ui form inline">
                            <label class="ui inline">
                                <input type="text" id="schSordNoMin" name="schSordNoMin" class="ui input" style="width: 60px" maxlength="3"><span class="text"> ~ </span>
                                <input type="text" id="schSordNoMax" name="schSordNoMax" class="ui input" style="width: 60px" maxlength="3">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>검색조건</th>
                        <td class="ui form inline">
                            <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                <option value="purchaseOrderNo">주문번호</option>
                                <option value="purchaseStoreInfoNo">옴니주문번호</option>
                                <option value="bundleNo">배송번호</option>
                                <option value="receiverNm">수령인</option>
                            </select>
                            <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 200px">
                        </td>
                        <th>배송shift</th>
                        <td colspan="2" class="ui form inline">
                            <select id="schShift" name="schShift" class="ui input medium mg-r-10" style="width: 150px;">
                                <option value="">전체</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>마켓연동</th>
                        <td colspan="4" class="ui form inline">
                            <select id="schMarketType" name="schMarketType" class="ui input medium mg-r-10" style="width: 150px;">
                                <option value="">전체</option>
                                <option value="NAVER">N마트</option>
                                <option value="ELEVEN">11번가</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="shipStoreManageSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="shipStoreManageGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
  ${shipStoreManageGridBaseInfo}
  var theclubFrontUrl = '${theclubFrontUrl}'; // 더클럽 프론트 페이지 URL
  var homeplusFrontUrl = '${homeplusFrontUrl}'; // 홈플러스 프론트 페이지 URL
  var so_storeId = '${so_storeId}';
  var so_storeNm = '${so_storeNm}';
  var so_storeType = '${so_storeType}';

  $(document).ready(function() {
    shipStoreManageMain.init();
    shipStoreManageGrid.init();
    CommonAjaxBlockUI.global();
  });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shipStoreManageMain.js?v=${fileVersion}"></script>