<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="shipManageShipHistoryPop" class="com wrap-popup medium"
     style="margin:0 0 7px 0;padding:10px 20px 0 20px; height:500px;max-height: 500px; overflow:hidden;overflow-y:scroll;background-color:#f7f7f7;">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">배송조회</h2>
    </div>

    <div>
        <div class="delivery-info">
            <span id="invoiceInfo"></span>
        </div>
        <div class="delivery-state">
            <ul id="ulList">
            </ul>
        </div>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group mg-b-10">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>

<script>
    var global = {
      dlvCd : '${dlvCd}'
      , dlvNm : '${dlvNm}'
      , invoiceNo : '${invoiceNo}'
    }

    $(document).ready(function() {
        shipManageShipHistoryPop.init();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipManageShipHistoryPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>