<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="escrow-main">
    <!-- 검색영역 -->
    <div>
        <form id="remoteShipSearchForm" name="remoteShipSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun" style="float: none">원거리 배송 관리</h2>
                <div class="mg-t-10">
                    일반점포에서 Slot selectbox에 "N" 값이 존재하는 우편번호 입니다.<br>
                    원거리 지역 배송 서비스를 하는 우편번호만 검색 됩니다.<br>
                    "점포우편번호등록" 메뉴에서 "원거리 지역 배송 서비스 여부"가 "Y"로 설정된 우편번호만 노출됩니다.<br>
                    "Slot 정보 template table data"에 있는 정보를 기준으로 노출 합니다. Slot은 기본 일별 10Slot을 기본 틀로 하여 데이터가 노출됩니다.
                </div>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>원거리 배송 관리</caption>
                    <tbody>
                        <tr>
                        <th>점포</th>
                        <td class="ui form inline">
                            <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10">
                            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                <c:if test="${codeDto.ref1 eq 'home'}">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                </c:if>
                            </c:forEach>
                            </select>
                            <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" placeholder="점포ID" readonly>
                            <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" placeholder="점포명" readonly>
                            <button type="button" class="ui button medium" onclick="deliveryCore_popup.openStorePop('deliveryCore.callBack.setStoreInfo');" >조회</button>
                        </td>
                        <th>요일</th>
                        <td>
                            <label class="ui checkbox mg-r-20" style="display: inline-block">
                                <input type="checkbox" id ="weekdayAll" name="weekdayAll" value="" checked="checked"><span>전체</span>
                            </label>
                            <c:forEach var="codeDto" items="${weekdayList}">
                            <label class="ui checkbox mg-r-10" style="display: inline-block">
                                <input type="checkbox" id ="weekday${codeDto.mcCd}" name="shipWeekdayList" value="${codeDto.mcCd}" checked="checked"><span>${codeDto.mcNm}</span>
                            </label>
                            </c:forEach>
                        </td>
                        <td rowspan="2">
                            <div class="ui form">
                                <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                        </tr>
                        <tr>
                            <th>우편번호</th>
                            <td>
                                <input type="text" id="schZipcode" name="schZipcode" class="ui input medium" maxlength="5">
                            </td>
                            <th>주소</th>
                            <td>
                                <div class="ui form inline">
                                    <select id="schSidoType" name="schSidoType" class="ui input medium mg-r-10">
                                        <option value="">시/도 전체</option>
                                        <c:forEach var="sido" items="${sidoList}">
                                            <option value="${sido}">${sido}</option>
                                        </c:forEach>
                                    </select>
                                    <select id="schSigunguType" name="schSigunguType" class="ui input medium">
                                        <option value="">시군구 전체</option>
                                    </select>

                                    <div id="schInputBox" name="schInputBox">
                                        <!-- 검색키워드 InputBox -->
                                        <input type="text" id="schAddrKeyword" name="schAddrKeyword" class="ui input mg-t-10 mg-r-10" maxlength="100" placeholder="도로명/건물명">
                                        <!-- 도로명주소 건물번호 InputBox -->
                                        <input type="text" id="schBuildNo" name="schBuildNo" class="ui input mg-t-10" maxlength="50" placeholder="건물번호">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="remoteShipSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="remoteShipGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    $(document).ready(function() {
        remoteShipMain.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/remoteShipMain.js?v=${fileVersion}"></script>
<!-- 주문개발TF 어드민 화면 basic style 적용 -->
<script type="text/javascript" src="/static/js/escrow/escrowBasicStyle.js?v=${fileVersion}"></script>