<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="divPop" class="com wrap-popup small">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">발송지연안내</h2>
    </div>

    <div class="ui wrap-table horizontal mg-t-10">
        <table class="ui table">
            <colgroup>
                <col width="25%">
                <col width="75%">
            </colgroup>
            <tbody>
            <tr>
                <th>발송기한 변경</th>
                <td class="ui form inline">
                    <input type="text" id="delayShipDt" name="delayShipDt" class="ui input medium mg-r-5" placeholder="발송기한" style="width:100px;" readonly>
                </td>
            </tr>
            <tr>
                <th>사유입력</th>
                <td>
                    <select id="delayShipCd" name="delayShipCd" class="ui input medium mg-r-10" style="width: 180px">
                        <option value="">선택</option>
                        <c:forEach var="codeDto" items="${delayShipCd}" varStatus="status">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:forEach>
                    </select>
                    <textarea id="delayShipMsg" class="ui input mg-t-5" style="width: 100%; height: 60px; display: none;" maxlength="100"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div>
        <ul class="mg-t-10">
            <li>- 발송기한안내는 최초 설정한 1차 발송기한 당일을 포함한 30일 이후까지 등록가능하며, 발송기한 경과 시 주문취소 처리될 수 있습니다.</li>
            <li>- 발송기한은 입력 후 수정할 수 없으며, 최초 1회에 한해 안내 가능합니다. 발송기한과 지연사유는 구매자에게 직접 안내되므로 신중히 작성해 주세요.(인사말과 사과문구는 자동으로 안내되므로 상세사유에 입력하지 않으셔도 됩니다.)</li>
        </ul>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    var callBackScript = "${callBackScript}";
    var bundleNo = "${bundleNo}";

    $(document).ready(function() {
        shipManageNotiDealyPop.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipManageNotiDelayPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/deliveryCore.js?v=${fileVersion}"></script>