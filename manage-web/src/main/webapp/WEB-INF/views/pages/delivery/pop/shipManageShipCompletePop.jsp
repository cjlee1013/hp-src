<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div id="divPop" class="com wrap-popup medium">
    <!-- 상단에 고정되는 팝업 타이틀 및 설명 -->
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun" style="float: none">배송완료</h2>
        <ul class="mg-t-10">
            <li>- 배송전 배송완료 처리시 판매자에게 불이익이 발생할 수 있습니다.</li>
        </ul>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="topline"></div>
        <div id="shipManageShipCompletePopGrid" style="width: 100%; height: 200px;"></div>
    </div>

    <!-- 하단 버튼영역 -->
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">배송완료</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>

<script>
    ${shipManageShipCompletePopGridBaseInfo}
    var callBackScript = '${callBackScript}';

    $(document).ready(function() {
        shipManageShipCompletePop.init();
        shipManageShipCompletePopGrid.init();
        shipManageShipCompletePop.search();

        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/pop/shipManageShipCompletePop.js?v=${fileVersion}"></script>