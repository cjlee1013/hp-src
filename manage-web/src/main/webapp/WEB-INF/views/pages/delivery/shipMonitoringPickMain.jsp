<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div>
    <!-- 검색영역 -->
    <div>
        <form id="shipMonitoringPickSearchForm" name="shipMonitoringPickSearchForm" onsubmit="return false;">
            <div class="com wrap-title has-border">
                <h2 class="title font-malgun">회수지연조회</h2>
            </div>
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회수지연조회</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="45%">
                        <col width="10%">
                        <col width="20%">
                        <col width="*%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>조회기간</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schShipDtType" name="schShipDtType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="requestDt">클레임신청일</option>
                                    <option value="orderDt">주문일</option>
                                </select>
                                <input type="text" id="schStartDt" name="schShipStartDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schShipEndDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;" maxlength="10">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
<%--                                <button type="button" id="setTodayBtn" class="ui button small gray-dark font-malgun mg-r-5">어제</button>--%>
                                <button type="button" id="setOneWeekBtn" class="ui button small gray-dark font-malgun mg-r-5">1주일전</button>
                            </td>
                            <td rowspan="5">
                                <div style="text-align: center;" class="ui form mg-t-15">
                                    <button type="submit" id="schBtn" class="ui button large cyan ">검색</button><br>
                                    <button type="button" id="schResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                    <button type="button" id="schExcelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>점포/판매업체</th>
                            <td class="ui form inline">
                                <select id="schStoreType" name="schStoreType" class="ui input medium mg-r-10" style="width: 100px;">
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input mg-r-5" style="width: 130px;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input mg-r-5" style="width: 130px;" readonly>
                                <button id="schStoreBtn" type="button" class="ui button medium">조회</button>
                            </td>
                            <th>수거방법</th>
                            <td class="ui form inline">
                                <select id="schPickShipType" name="schPickShipType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <option value="자차수거">자차수거</option>
                                    <option value="직접수거">직접수거</option>
                                    <option value="택배수거">택배수거</option>
                                    <option value="우편">우편</option>
                                    <option value="점포방문">점포방문</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>클레임구분</th>
                            <td class="ui form inline">
                                <select id="schClaimType" name="schClaimType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${claimType}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'C'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                            <th>수거상태</th>
                            <td class="ui form inline">
                                <select id="schPickStatus" name="schPickStatus" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${pickupStatus}" varStatus="status">
                                        <c:if test="${codeDto.mcCd ne 'N0' and codeDto.mcCd ne 'P3'}">
                                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>검색조건</th>
                            <td class="ui form inline">
                                <select id="schKeywordType" name="schKeywordType" class="ui input medium mg-r-10" style="width: 150px">
                                    <option value="claimBundleNo">클레임번호</option>
                                    <option value="claimNo">그룹클레임번호</option>
                                    <option value="purchaseOrderNo">주문번호</option>
                                    <option value="receiverNm">수령인</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input" style="width: 200px">
                            </td>
                            <th>수거지연일</th>
                            <td class="ui form inline">
                                <input type="text" id="schPickDelayDay" name="schPickDelayDay" class="ui input" style="width: 50px">
                            </td>
                        </tr>
                        <tr>
                            <th>마켓연동</th>
                            <td colspan="4" class="ui form inline">
                                <select id="schMarketType" name="schMarketType" class="ui input medium mg-r-10" style="width: 150px;">
                                    <option value="">전체</option>
                                    <option value="NAVER">N마트</option>
                                    <option value="ELEVEN">11번가</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <!-- 그리드 영역 -->
    <div>
        <div class="com wrap-title sub" style="margin-top: 30px;">
            <h3 class="title">검색결과 : <span id="shipMonitoringPickSearchCnt">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div id="shipMonitoringPickGrid" style="width: 100%; height: 500px;"></div>
    </div>
</div>

<script>
    ${shipMonitoringPickGridBaseInfo}
    var so_storeId = '${so_storeId}';
    var so_storeNm = '${so_storeNm}';
    var so_storeType = '${so_storeType}';

    $(document).ready(function() {
        shipMonitoringPickMain.init();
        shipMonitoringPickGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shippingUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/delivery/shipMonitoringPickMain.js?v=${fileVersion}"></script>