<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<style type="text/css">
    .flex-order {
        display: flex;
        align-items: center;
    }
    .flex-in-button {
        width: 90px !important;
        display: inline !important;
    }
    .refresh-contain-parent {
        display:flex;
        align-items:center;
    }
    .refresh-button {
        display: flex;
        justify-content: flex-end;
        flex: 1;
    }
</style>

<!-- 클럽정보 삭제로 hidden 처리 -->
<div style="display: none">
    <input type="radio" name="siteTypeRadio" value="HOME" checked/>
</div>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">인기검색어 조회</h2>
    </div>
    <div id="schArea" class="ui wrap-table horizontal" style="border-top: 0px;">
        <table class="ui table">
            <caption>인기검색어</caption>
            <colgroup>
                <col width="30%">
                <col width="30%">
                <col width="30%">
            </colgroup>
            <tbody>
            <tr>
                <th class="refresh-contain-parent">
                        <span>현재 인기검색어 </span>
                        <span>(</span>
                        <span id="nowDisplayGroup"></span>
                        <span>)</span>
                        <span class="refresh-button">
                            <button type="button" id="searchResetBtn" class="form small ui button large mg-t-5" onclick="popularHistoryJs.refreshEvent()">새로고침</button><br/>
                        </span>
                </th>
                <th>
                    <div class="flex-order">
                        <span>이전 인기검색어</span>
                        <span><input type="text" id="selDt" name="selDt" class="ui input medium mg-r-5 mg-l-5 flex-in-button" placeholder="조회일"></span>
                        <span class="mg-l-5">
                            <select id="selDisplayGroupOption"></select>
                        </span>
                    </div>
                </th>
                <th></th>
            </tr>
            <tr>
                <td>
                    <div class="mg-b-20" id="nowPopularGrid" style="width: 100%; height: 614px;"></div>
                </td>

                <td>
                    <div class="mg-b-20" id="selPopularGrid" style="width: 100%; height: 614px;"></div>
                </td>

                <td>
                    <div class="com sub">
                        <h3>• 현재 적용중인 확정어</h3>
                        <div class="mg-b-20" id="fixedGrid" style="width: 100%; height: 280px;"></div>
                    </div>
                    <div class="com sub">
                        <h3>• 현재 적용중인 제외어</h3>
                        <div class="mg-b-20" id="exceptGrid" style="width: 100%; height: 280px;"></div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="/static/js/search/popularCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/popularHistory.js?${fileVersion}"></script>
<script>
    ${basicPopularGridBaseInfo}
    ${nowPopularGridBaseInfo}
    ${fixedGridBaseInfo}
    ${exceptGridBaseInfo}
    $(document).ready(function() {

        nowPopularGrid.init();
        selPopularGrid.init();
        fixedGrid.init();
        exceptGrid.init();
        CommonAjaxBlockUI.global();

        popularHistoryJs.init(true);

        $('input:radio[name=siteTypeRadio]').click(function(){
            popularHistoryJs.init(false);
        });

        CommonAjaxBlockUI.global();

        $("#selDisplayGroupOption").change(function () {
            let regExp = /\d/;
            if(regExp.test(this.value)){
                let resultData = commonPopularJs.getPopularKeyword(this.value, popularType.POPULAR);
                selPopularGrid.setData(resultData);
            }else{
                alert('불러올 이전 인기검색어가 없습니다.');
            }
        });

        popularHistoryJs.getBeforePopularGroupNo();
    });
</script>
