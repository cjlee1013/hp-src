<%--
  Created by IntelliJ IDEA.
  User: hyeoncheol.shin
  Date: 03/02/2020
  Time: 10:09 오전
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<style type="text/css">
    body {
        overflow-x: hidden;
        overflow-y: hidden;
    }
</style>

<%--<html>
<head>
    <title>어휘 이력조회</title>
</head>
<body>

</body>
</html>--%>
<br>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <div class="ui form inline" style="margin-right:10px;">
            <h2 class="title font-malgun" style="margin-left:10px;">이력조회</h2> <span style="float:right;"><button type="button" class="ui button small gray-dark font-malgun " onclick="javascript:self.close();" style="height:26px;width:50px;">닫기</button></span>
        </div>
    </div>
    <div class="com wrap-title sub">
        <div class="mg-b-20 " id="dictionaryHistoryGrid" style="height: 350px;"></div>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/search/dictionaryHistory.js?${fileVersion}"></script>
<script>
    ${dictionaryHistoryGridBaseInfo}

    var wordId = "${wordId}";

    $(document).ready(function() {
        dictionaryHistory.init();
        dictionaryHistoryGrid.init();
        dictionaryHistory.onLoad();
    });
</script>

