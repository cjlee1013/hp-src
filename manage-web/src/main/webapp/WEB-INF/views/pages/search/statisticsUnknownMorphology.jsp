<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">미등록어 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr >
                        <th>검색어</th>
                        <td>
                            <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                        </td>
                        <!-- 검색 버튼 -->
                        <td>
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="unknownMorphologyGrid" style="height: 80%; width:100%;"></div>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/statisticsUnknownMorphology.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${unknownMorphologyGridBaseInfo}

    $(document).ready(function() {
        unknownMorphologyJs.init();
        unknownMorphologyJs.event();
        unknownMorphologyGrid.init();
        unknownMorphologyJs.onload();
        CommonAjaxBlockUI.global();
    });
</script>