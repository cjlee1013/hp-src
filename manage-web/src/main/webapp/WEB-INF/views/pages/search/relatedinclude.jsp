<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>




<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">연관검색어 추가노출</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  relatedKeywordJs.search();">
                <input type="hidden" id="schRelationType" name="schRelationType" value="INCLUDE"/>
                <div style="display: none">
                    <input type="radio" name="schSiteType" value="HOME" checked/>
                </div>
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-10 select-style">
                                    <option value="CREATEDDT" selected="selected">등록일</option>
                                    <option value="STATUSDT">운영기간</option>
                                </select>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5 date-input-style" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5 date-input-style" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="relatedKeywordJs.initSearchDate('0', this);">오늘</button>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="relatedKeywordJs.initSearchDate('-1d', this);">어제</button>
                                <button type="button" name="rangeBtn" class="ui button small cyan font-malgun mg-r-5" id="rangeDefault" onclick="relatedKeywordJs.initSearchDate('-30d', this);">1개월전</button>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun " onclick="relatedKeywordJs.initSearchDate('-90d', this);">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>운영상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schStatus" name="schStatus" class="ui input medium mg-r-10 select-style">
                                    <option value="ALL">전체</option>
                                    <option value="OPEN">운영중</option>
                                    <option value="STAND_BY">운영대기</option>
                                    <option value="CLOSE">운영종료</option>
                                </select>
                            </div>
                        </td>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10 select-style">
                                    <option value="" selected="selected">전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schSearchType" name="schSearchType" class="ui input medium mg-r-10 select-style">
                                    <option value="KEYWORD">검색어</option>
                                    <option value="RELATION_KEYWORD">연관검색어</option>
                                    <option value="CREATOR">등록자</option>
                                    <option value="MODIFIER">수정자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5 search-input-style-small" minlength="2">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="relatedKeywordListGrid" style="height: 350px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="relatedKeywordSetForm" onsubmit="return false;">
                <input type="hidden" id="relationType" name="relationType" value="INCLUDE"/>
                <input type="hidden" id="relationId" name="relationId"/>
                <div style="display: none">
                    <input type="radio" name="siteType" value="HOME" checked/>
                </div>
                <table class="ui table">
                    <caption>등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="50">
                                <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 50자 )</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">
                                <select id="useYn" name="useYn" class="ui input medium mg-r-10 select-style">
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                        <th id="dlv_siteTypeRadio" style="visibility:hidden;">사이트</th>
                        <td id="dlv_siteTypeText" style="visibility:hidden;">
                            <div class="ui form inline">
                                <span class="text" id="dlv_siteTypeText_siteName">홈플러스</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>운영기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="operationPeriodYn" value="Y"><span>설정 </span>
                                </label>
                                <label class="ui radio inline">
                                    <input type="radio" name="operationPeriodYn" value="N" checked><span>설정안함 </span>
                                </label>
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>추가 연관검색어</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input id="relationKeyword" name="relationKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="250" placeholder="※ 키워드가 여러개일때 , 으로 구분">
                                <span class="text">( <span style="color:red;" id="textCountKr_relationKeyword">0</span> / 250자 )</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/relatedInclude.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${relatedKeywordListGridBaseInfo}
    $(document).ready(function(){
        CommonAjaxBlockUI.global();
    });
</script>