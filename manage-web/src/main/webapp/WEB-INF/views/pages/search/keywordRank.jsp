<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<!-- 점포선택 폼 -->
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">키워드 랭킹 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-1">
            <form id="itemSearchForm" onsubmit="return false;">
                <input type="hidden" id="mallType" name="mallType"/>
                <input type="hidden" id="schStoreId" name="schStoreId"/>

                <table class="ui table">
                    <caption>상품 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="30%">
                        <col width="7%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select id="schDate" name="schDate" class="ui input medium mg-r-10" style="width: 100px" >
                                    <option value="regDt">등록일</option>
                                    <option value="saleDt">판매일</option>
                                </select>
                                <input type="text" id="schStartDate" name="schStartDate" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDate" name="schEndDate" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="keywordRankJs.initSearchDate('0');">오늘</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="keywordRankJs.initSearchDate('-1d');">어제</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5" onclick="keywordRankJs.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun " onclick="keywordRankJs.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form mg-t-15">
                                <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br/>
                                <button type="button" id="searchResetBtn"	class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5" style="display:none;">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td colspan="3">
                            <select class="ui input medium mg-r-5" id="searchCateCd1" name="schLcateCd"
                                    style="width:150px;float:left;" data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd2" name="schMcateCd"
                                    style="width:150px;float:left;" data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd3" name="schScateCd"
                                    style="width:150px;float:left;" data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5" id="searchCateCd4" name="schDcateCd"
                                    style="width:150px;" data-default="세분류">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                <c:if test="${codeDto.ref3 eq 'REAL'}">
                                    <c:set var="checked" value="" />
                                    <c:if test="${codeDto.mcCd eq 'HYPER'}">
                                        <c:set var="checked" value="checked" />
                                    </c:if>
                                    <label class="ui radio inline"><input type="radio" name="schStoreType" class="ui input" value="${codeDto.mcCd}" ${checked}/><span>${codeDto.mcNm}</span></label>
                                </c:if>
                            </c:forEach>
                            <label class="ui radio inline"><input type="radio" name="schStoreType" class="ui input" value="DS"/><span>DS</span></label>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schType" name="schType" class="ui input medium mg-r-10" style="width: 100px">
                                    <option value="itemNm">상품명</option>
                                    <option value="itemNo">상품번호</option>
                                    <option value="regNm">등록자</option>
                                </select>
                                <input type="text" id="schKeyword" name="schKeyword" class="ui input medium mg-r-5" style="width: 250px;" minlength="2" >
                            </div>
                        </td>
                        <th>상품상태</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schItemStatus" name="schItemStatus" class="ui input medium mg-r-10" style="width: 180px">
                                    <option value="">전체</option>
                                    <c:forEach var="codeDto" items="${itemStatus}" varStatus="status">
                                        <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div>
            <div class="com wrap-title sub">
                <h3 class="title pull-left">검색결과 : <span id="itemTotalCount">0</span>건</h3>
            </div>
            <div class="topline"></div>
        </div>

        <div class="mg-b-20" id="itemListGrid" style="width: 100%; height: 350px;"></div>
    </div>

    <form id="itemSetForm" onsubmit="return false;">
        <div class="com wrap-title sub">
            <h3 class="title">저장/관리</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10" style="width:100%;">
                <table class="ui table">
                    <caption>키워드랭킹 저장/관리</caption>
                    <colgroup>
                        <col width="100px">
                        <col>
                        <col width="100px">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>상품번호</th>
                        <td style="width:340px;">
                            <div class="ui form inline">
                                <input id="itemNo" name="itemNo" type="text" class="ui input medium mg-r-5" maxlength="22" style="width:170px;" readonly="readonly">
                            </div>
                        </td>
                        <th>상품명</th>
                        <td>
                            <div class="ui form inline">
                                <input id="itemNm" name="itemNm" type="text" class="ui input medium mg-r-5" style="width:170px;" readonly="readonly">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>적용기간 <span class="text-red">*</span></th>
                        <td colspan="10">
                            <div class="ui form inline">
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5" placeholder="시작일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5" placeholder="종료일" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>키워드 항목</th>
                        <td colspan="10">
                            <div class="ui form inline">
                                <input id="keywordList" name="keywordList" type="text" class="ui input medium mg-r-5" maxlength="2000" placeholder="'/' 구분으로 입력하세요." style="width:100%;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
        <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>&nbsp;<button type="button" class="ui button xlarge white font-malgun" id="deleteBtn">삭제</button>
        </span>
        </div>
    </form>

</div>
<!-- script -->
<script type="text/javascript" src="/static/js/search/keywordRank.js?${fileVersion}"></script>
<script>
    // 상품 리스트 리얼그리드 기본 정보
    ${itemListGridBaseInfo}

    $(document).ready(function() {
        keywordRankJs.init();
        CommonAjaxBlockUI.global();
        itemListGrid.init();
    });
</script>