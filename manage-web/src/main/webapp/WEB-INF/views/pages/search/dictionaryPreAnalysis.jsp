<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<style type="text/css">
    body {
        overflow-x: hidden;
        overflow-y: hidden;
    }
</style>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <div class="ui form inline" style="margin-right:10px;">
            <h2 class="title font-malgun" style="margin-left:10px;">기분석 설정 : ${wordName} (${wordId})</h2> <span style="float:right;"><button type="button" class="ui button small gray-dark font-malgun " onclick="javascript:self.close();" style="height:26px;width:50px;">닫기</button></span>
        </div>
    </div>

    <div class="com wrap-title sub">
        <div id="groupWordWrite" class="ui wrap-table horizontal mg-t-10">
            <form id="wordWriteForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>어휘</caption>
                    <colgroup>
                        <col width="400">
                        <col width="100">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <!-- 어휘 -->
                    <tr>
                        <td style="height:600px; vertical-align:top;">
                            <div class="ui form inline" style="width:400px;">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="16" style="height:35px;width:342px;">
                                <button type="submit" id="searchBtn" class="ui button white font-malgun"><i class="fa fa-search"></i> 검색</button>
                                <button type="submit" id="writeBtn" style="display: none; width:53px;" class="ui button cyan font-malgun">등록</button>
                            </div>

                            <div class="mg-b-2" id="preAnalysisSearchGrid" style="height: 63px;width:400px;height:510px;margin-top:20px;"></div>
                        </td>
                        <td style="vertical-align:top;">
                            <div class="ui form inline" style="width:100px;margin-bottom:15px;margin-top:80px;">
                                <button type="button" class="ui button xlarge white font-malgun mg-r-5" onclick="preAnalysisJs.move(preAnalysisResultGrid);" style="height:40px; width:100px;"> > > </button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:160px;">
                                <button type="button" class="ui button large blue font-malgun mg-r-5" onclick="preAnalysisJs.save(preAnalysisResultGrid)" style="height:80px; width:100px;">검색용 저장</button>
                            </div>
                        </td>
                        <td style="height:600px; vertical-align:top;">
                                <span class="text"><strong>전방 매칭 : </strong></span>

                                <c:forEach var="attr" items="${preAnalysisAttrCd}" varStatus="status">
                                    <c:set var="checked" value=""></c:set>
                                    <c:if test="${'ALWAYS' eq attr.code}">
                                        <c:set var="checked" value="checked"></c:set>
                                    </c:if>
                                    <label class="ui radio small inline">
                                        <input type="radio" name="preType" class="ui input small" value="${attr.code}" ${checked}><span>${attr.name}</span>
                                    </label>
                                </c:forEach>
                                <span class="text" style="margin-left:30px;">
                                    <strong>후방 매칭 : </strong>
                                </span>
                                <label class="ui radio inline">

                                <c:forEach var="attr" items="${preAnalysisAttrCd}" varStatus="status">
                                    <c:set var="checked" value=""></c:set>
                                    <c:if test="${'ALWAYS' eq attr.code}">
                                        <c:set var="checked" value="checked"></c:set>
                                    </c:if>
                                    <label class="ui radio small inline">
                                        <input type="radio" name="sufType" class="ui input small" value="${attr.code}" ${checked}><span>${attr.name}</span>
                                    </label>
                                </c:forEach>
                                </label>
                            <div class="ui form inline" style="width:500px; text-align: left;margin-top:13px;">
                                <span class="text"><strong>검색용</strong> : <span id="txtPreAnalysisResult"></span></span>
                            </div>
                            <div class="mg-b-2" id="preAnalysisResultGrid" style="height: 63px;width:500px;height:250px;margin-top:10px;"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="requestWordId" name="requestWordId" value="${wordId}"/>
<input type="hidden" id="wordId" name="wordId"/>
<input type="hidden" id="wordName" name="wordName"/>
<input type="hidden" id="senseTag" name="senseTag"/>
<input type="hidden" id="pos" name="pos"/>
<input type="hidden" id="dicType" name="dicType"/>
<input type="hidden" id="description" name="description"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<!-- script -->
<script type="text/javascript" src="/static/js/search/dictionaryPreAnalysis.js?v=${fileVersion}"></script>
<script>
    ${dictionaryPreAnalysisSearchBaseInfo}
    ${dictionaryPreAnalysisResultBaseInfo}

    var error = "${error}";
    var url = "${redirectUrl}";

    if(error != null && error != "")
    {
        alert(error);
        opener.parent.location.href = url;
        self.close();
    }

    $(document).ready(function() {
        preAnalysisJs.init();
        preAnalysisJs.event();
        CommonAjaxBlockUI.global();
    });
</script>
