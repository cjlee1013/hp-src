<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<!-- month picker -->
<script type="text/javascript" src="/static/js/core/jquery.ui.monthpicker.js"></script>


<style type="text/css">
    .flex-order {
        display: flex;
        align-items: center;
    }
    .flex-in-button {
        width: 70px !important;
        display: inline !important;
    }
</style>

<!-- 클럽정보 삭제로 hidden 처리 -->
<div style="display: none">
    <input type="radio" name="siteTypeRadio" value="HOME" checked/>
</div>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">기간별 인기검색어 조회</h2>
    </div>

    <div id="schArea" class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>인기검색어</caption>
            <colgroup>
                <col width="30%">
                <col width="30%">
                <col width="30%">
            </colgroup>
            <tbody>
            <tr>
                <th>
                    <div class="flex-order">
                        <span>일간 인기검색어 </span>
                        <span><input type="text" id="daySelDt" name="daySelDt" class="ui input medium mg-r-5 mg-l-5 flex-in-button" placeholder="조회일"></span>
                        <span class="mg-l-5">
                            <select id="dayStatisticsGroupOption">
                            </select>
                        </span>
                    </div>
                </th>
                <th>
                    <span>주간 인기검색어 </span>
                    <span><input type="text" id="weekSelDt" name="weekSelDt" class="ui input medium mg-r-5 mg-l-5 flex-in-button" placeholder="조회일"></span>
                    <span class="mg-l-5">
                        <select id="weekStatisticsGroupOption">
                        </select>
                    </span>
                </th>
                <th>
                    <span>월간 인기검색어 </span>
                    <span><input type="text" id="monthSelDt" name="monthSelDt" class="ui input medium mg-r-5 mg-l-5 flex-in-button" placeholder="조회일"></span>
                </th>
            </tr>
            <tr>
                <td>
                    <div class="mg-b-20" id="dayPopularGrid" style="width: 100%; height: 614px;"></div>
                </td>
                <td>
                    <div class="mg-b-20" id="weekPopularGrid" style="width: 100%; height: 614px;"></div>
                </td>
                <td>
                    <div class="mg-b-20" id="monthPopularGrid" style="width: 100%; height: 614px;"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="/static/js/search/popularCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/popularStatistics.js?${fileVersion}"></script>
<script>
    ${basicPopularGridBaseInfo}
    ${dayPopularGridBaseInfo}
    ${weekPopularGridBaseInfo}
    ${monthPopularGridBaseInfo}
    $(document).ready(function() {

        CommonAjaxBlockUI.global();
        dayPopularGrid.init();
        weekPopularGrid.init();
        monthPopularGrid.init();

        popularStatisticsJs.init();

        $('input:radio[name=siteTypeRadio]').click(function(){
            popularStatisticsJs.init();
        });

        $("#dayStatisticsGroupOption").change(function () {
            popularStatisticsJs.setGridData('day',this.value);
        });
        $("#weekStatisticsGroupOption").change(function () {
            popularStatisticsJs.setGridData('week',this.value);
        });
        $("#monthStatisticsGroupOption").change(function () {
            popularStatisticsJs.setGridData('month',this.value);
        });
    });
</script>
