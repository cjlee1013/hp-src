<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">대체검색어 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <select id="schDateType" name="schDateType" class="ui input medium mg-r-10 select-style">
                                    <option value="STATUSDT" selected="selected">사용기간</option>
                                    <option value="CREATEDDT">등록일</option>
                                </select>
                                <input type="text" id="searchSdate" name="searchSdate" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="searchEdate" name="searchEdate" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="replacedKeywordJs.initSearchDate('0', this);">오늘</button>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="replacedKeywordJs.initSearchDate('-1d', this);">어제</button>
                                <button type="button" name="rangeBtn" class="ui button small cyan font-malgun mg-r-5" id="rangeDefault" onclick="replacedKeywordJs.initSearchDate('-30d', this);">1개월전</button>
                                <button type="button" name="rangeBtn" class="ui button small gray-dark font-malgun " onclick="replacedKeywordJs.initSearchDate('-90d', this);">3개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>키워드</th>
                        <td>
                            <div class="ui form inline">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>어휘포함</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-5 pull-left">
                                    <input type="checkbox" name="inner" id="inner" value="Y" class="text-center"/> <span class="text">대체검색어 포함 모두 찾기</span>
                                </label>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="dictionaryTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="replacedKeywordGrid" style="height: 450px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="replacedKeywordSetForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>대체키워드 등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                                <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 15자 )</span>
                            </div>
                        </td>
                        <th>사용여부 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10 select-style" id="useFlag" name="useFlag">
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>대체검색어 <span class="text-red">*</span></th>
                        <td>
                        <span class="ui form inline">
                            <input id="replacedKeyword" name="replacedKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                            <span class="text">( <span style="color:red;" id="textCountKr_replacedKeyword">0</span> / 15자 )</span>
                        </span>
                        </td>
                        <th>사용기간</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio small inline"><input type="radio" name="operationPeriodYn" id="useY" value="Y" onclick="replacedKeywordJs.radioClickEvent(this.value)"><span>설정</span></label>
                                <label class="ui radio small inline"><input type="radio" name="operationPeriodYn" id="useN" value="N" onclick="replacedKeywordJs.radioClickEvent(this.value)" checked><span>설정안함</span></label>
                                <input type="text" id="sdate" name="sdate" class="ui input medium mg-r-5 date-input-style" disabled>
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="edate" name="edate" class="ui input medium mg-r-5 date-input-style" disabled>
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge cyan font-malgun" id="editBtn" style="display: none;">수정</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
            <button type="button" class="ui button xlarge white font-malgun" id="deleteBtn" style="display: none;">삭제</button>
        </span>
    </div>
</div>

<input type="hidden" id="replacedId" name="replacedId"/>
<input type="hidden" id="totalCount" name="totalCount"/>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/replacedKeyword.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${replacedKeywordGridBaseInfo}

    $(document).ready(function() {
        replacedKeywordJs.init();
        replacedKeywordJs.event();
        replacedKeywordGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>