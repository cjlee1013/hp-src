<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<style type="text/css">
    body {
        overflow-x: hidden;
        overflow-y: hidden;
    }
</style>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <div class="ui form inline" style="margin-right:10px;">
            <h2 class="title font-malgun" style="margin-left:10px;">관련어 설정 : ${wordName} (${wordId})</h2> <span style="float:right;"><button type="button" class="ui button small gray-dark font-malgun " onclick="javascript:self.close();" style="height:26px;width:50px;">닫기</button></span>
        </div>
    </div>

    <div class="com wrap-title sub">
        <div id="groupWordWrite" class="ui wrap-table horizontal mg-t-10">
            <form id="wordWriteForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>어휘</caption>
                    <colgroup>
                        <col width="400">
                        <col width="100">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <!-- 어휘 -->
                    <tr>
                        <td style="height:600px; vertical-align:top;">
                            <div class="ui form inline" style="width:400px;">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="16" style="height:35px;width:342px;">
                                <button type="submit" id="searchBtn" class="ui button white font-malgun"><i class="fa fa-search"></i> 검색</button>
                                <button type="submit" id="writeBtn" style="display: none; width:53px;" class="ui button cyan font-malgun">등록</button>
                            </div>

                            <div class="mg-b-2" id="relationSearchGrid" style="height: 63px;width:400px;height:510px;margin-top:20px;"></div>
                        </td>
                        <td style="vertical-align:top;">
                            <div class="ui form inline" style="width:100px;margin-bottom:15px;margin-top:80px;">
                                <button type="button" class="ui button xlarge white font-malgun mg-r-5" onclick="relationJs.move(relationSynonymGrid);" style="height:40px; width:100px;"> > > </button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:160px;">
                                <button type="button" class="ui button large blue font-malgun mg-r-5" onclick="relationJs.save(relationSynonymGrid)" style="height:80px; width:100px;">동의어 저장</button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:15px;">
                                <button type="button" class="ui button xlarge white font-malgun mg-r-5" onclick="relationJs.move(relationHyponymGrid);" style="height:40px; width:100px;"> > > </button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:160px;">
                                <button type="button" class="ui button large blue font-malgun mg-r-5" onclick="relationJs.save(relationHyponymGrid)" style="height:80px; width:100px;">하위어 저장</button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:15px;">
                                <button type="button" class="ui button xlarge white font-malgun mg-r-5" onclick="relationJs.move(relationRelatednymGrid);" style="height:40px; width:100px;"> > > </button>
                            </div>
                            <div class="ui form inline" style="width:100px;margin-bottom:160px;">
                                <button type="button" class="ui button large blue font-malgun mg-r-5" onclick="relationJs.save(relationRelatednymGrid)" style="height:80px; width:100px;">연관어 저장</button>
                            </div>
                        </td>
                        <td style="height:600px; vertical-align:top;">
                            <div class="ui form inline" style="width:500px; text-align: left;">
                                <span class="text"><strong>동의어</strong> </span>
                            </div>
                            <div class="mg-b-2" id="relationSynonymGrid" style="height: 63px;width:500px;height:250px;margin-top:10px;"></div>
                            <div class="ui form inline" style="width:500px; text-align: left; margin-top:30px;">
                                <span class="text"><strong>하위어</strong> </span>
                            </div>
                            <div class="mg-b-2" id="relationHyponymGrid" style="height: 63px;width:500px;height:250px;margin-top:10px;"></div>
                            <div class="ui form inline" style="width:500px; text-align: left; margin-top:30px;">
                                <span class="text"><strong>연관어</strong> </span>
                            </div>
                            <div class="mg-b-2" id="relationRelatednymGrid" style="height: 63px;width:500px;height:250px;margin-top:10px;"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<!-- hidden input -->
<input type="hidden" id="requestWordId" name="requestWordId" value="${wordId}"/>
<input type="hidden" id="wordId" name="wordId"/>
<input type="hidden" id="wordName" name="wordName"/>
<input type="hidden" id="senseTag" name="senseTag"/>
<input type="hidden" id="pos" name="pos"/>
<input type="hidden" id="description" name="description"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<!-- script -->
<script type="text/javascript" src="/static/js/search/dictionaryRelation.js?v=${fileVersion}"></script>

<script>
    ${dictionaryRelationSearchInfo}
    ${dictionaryRelationSynonymInfo}
    ${dictionaryRelationHyponymInfo}
    ${dictionaryRelationRelatednymInfo}

    var error = "${error}";
    var url = "${redirectUrl}";

    if(error != null && error != "")
    {
        alert(error);
        opener.parent.location.href = url;
        self.close();
    }

    $(document).ready(function() {
        relationJs.init();
        relationJs.event();
        CommonAjaxBlockUI.global();
    });
</script>
