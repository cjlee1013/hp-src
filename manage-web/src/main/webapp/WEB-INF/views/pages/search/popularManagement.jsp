<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<!-- 클럽정보 삭제로 hidden 처리 -->
<div style="display: none">
    <input type="radio" name="siteTypeRadio" value="HOME" checked/>
</div>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">인기검색어 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 확정 키워드 <span id="fixedTotalCount">0</span>건</h3>
    </div>
    <div class="mg-b-20" id="fixedGrid" style="width: 100%; height: 350px;"></div>
    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 제외 키워드 <span id="exceptTotalCount">0</span>건</h3>
    </div>
    <div class="mg-b-20" id="exceptGrid" style="width: 100%; height: 350px;"></div>
    <div class="topline"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="ui wrap-table horizontal mg-t-10" style="width:100%;">
        <form id="popularForm" onsubmit="return false;">
            <input type="hidden" id="managementNo" name="managementNo">
            <input type="hidden" id="regId" name="regId" value="${userInfo.getEmpId()}">
            <input type="hidden" id="chgId" name="chgId" value="${userInfo.getEmpId()}">
            <input type="hidden" id="siteType" name="siteType" />
            <input type="hidden" id="apply" name="apply" />

            <table class="ui table">
                <caption>인기검색 키워드 등록/수정</caption>
                <colgroup>
                    <col width="7%">
                    <col width="22%">
                    <col width="7%">
                    <col width="22%">
                    <col width="7%">
                    <col width="22%">
                </colgroup>
                <tbody>
                <tr>
                    <th>키워드</th>
                    <td>
                        <div class="ui form inline">
                            <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                            <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 15자 )</span>
                        </div>
                    </td>
                    <th>구분</th>
                    <td>
                        <select class="ui input medium mg-r-5 select-style" id="type" name="type" data-default="1">
                            <option value="">구분</option>
                            <option value="fixed">확정</option>
                            <option value="except">제외</option>
                        </select>
                    </td>
                    <th>순위</th>
                    <td>
                        <select class="ui input medium mg-r-5 select-style" id="rank" name="rank">
                            <option value="">순위</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <select class="ui input medium mg-r-5 select-style" id="useYn" name="useYn" data-default="1">
                            <option value="Y">사용</option>
                            <option value="N">미사용</option>
                        </select>
                    </td>
                    <th>적용 시작일</th>
                    <td class="ui form inline"><input id="startDt" name="startDt" type="text" class="ui input mg-r-5 date-input-style"></td>
                    <th>적용 종료일</th>
                    <td colspan="3" class="ui form inline"><input id="endDt" name="endDt" type="text" class="ui input mg-r-5 date-input-style"></td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/search/popularCommon.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/popularManagement.js?${fileVersion}"></script>
<script>
    ${fixedGridBaseInfo}
    ${exceptGridBaseInfo}
    $(document).ready(function() {

        fixedGrid.init();
        exceptGrid.init();
        CommonAjaxBlockUI.global();

        popularManagementJs.init(true);

        $('input:radio[name=siteTypeRadio]').click(function(){
            popularManagementJs.init(false);
        });

        CommonAjaxBlockUI.global();
    });
</script>