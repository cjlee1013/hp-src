<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">검색로그 필터링 설정</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <colgroup>
                        <col width="15%">
                        <col width="650">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>서비스</th>
                        <td>
                            <label for="searchBlackListType"></label>
                            <select class="ui input middle left-float mg-r-5" id="searchServiceType" name="searchServiceType" style="width:20%;">
                                <option value="">전체</option>
                                <option value="HOME">홈플러스</option>
                                <option value="EXP">익스프레스</option>
                            </select>
                        </td>
                        <td style="vertical-align: middle; text-align: left;" rowspan="2">
                            <div class="ui form inline mg-l-5">
                                <button type="submit" id="searchBtn" class="ui button large cyan"><i class="fa fa-search"></i> 검색</button>
                                <br/>
                                <button type="button" id="resetBtn" class="ui button large white font-malgun mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:56px;">
                        <th>블랙리스트 유형 </th>
                        <td>
                            <label for="searchBlackListType"></label>
                            <select class="ui input middle left-float mg-r-5" id="searchBlackListType" name="searchBlackListType" style="width:20%;">
                                <option value="">전체</option>
                                <option value="CRAWLING">크롤링</option>
                                <option value="ABUSING">어뷰징</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 블랙리스트 설정 정보 </h3>
    </div>
    <div class="topline"></div>
    <div class="mg-b-20 " id="blackListRuleInfoGrid" style="height: 200px; width:100%;"></div>
    <div class="com wrap-title sub">
        <h3 class="title">• 블랙리스트 설정 정보 수정 &nbsp;&nbsp;&nbsp;&nbsp; </h3>
        <h3 class="sub-title inline" align="right">
            <span class="text">** 블랙리스트 등록 가능 횟수는 동일한 서비스유형에 따라 값이 수정됩니다. ** </span>
        </h3>
    </div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>블랙리스트 설정 정보 *</caption>
            <colgroup>
                <col width="15%">
                <col width="15%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <tbody>
            <tr>
                <th class="text" style="text-align: center;">서비스 유형</th>
                <th class="text" style="text-align: center;">블랙리스트 유형</th>
                <th class="text" style="text-align: center;">배치 주기(시간)</th>
                <th class="text" style="text-align: center;">필터링 간격(분)</th>
               <th class="text" style="text-align: center;">필터링 검색 빈도(회)</th>
                <th class="text" style="text-align: center;">필터링 반복 횟수(회)</th>
                <th class="text" style="text-align: center;">블랙리스트 등록 횟수 <br>(배치 실행 횟수)</th>
            </tr>
            <tr>
                <input type="hidden" name="ruleNo" id="ruleNo">
                <td class="ui form inline" style="text-align: center;">
                    <input type="hidden" id="ruleServiceType" name="ruleServiceType">
                    <span class="text" name="serviceType"></span>
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <span class="text" name="blacklistType"></span>
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <span class="text" name="ruleHourInterval"></span>
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <span class="text" name="ruleMinuteInterval"></span>
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <input type="text" id="ruleMinuteFrequency" name="ruleMinuteFrequency" class="ui input" style="width:70px;text-align: center;" maxlength="4" >
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <input type="text" id="ruleRepeatCount" name="ruleRepeatCount" class="ui input" style="width:70px;text-align: center;" maxlength="4">
                </td>
                <td class="ui form inline" style="text-align: center;">
                    <input type="text" id="blacklistAddCount" name="blacklistAddCount" class="ui input" style="width:70px;text-align: center;" maxlength="4">
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui center-button-group mg-t-15" style="width:100%;" id="modifyDiv">
        <span class="inner">
            <button type="button" id="modifyRule" class="ui button xlarge cyan font-malgun"> 저장 </button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/operationFiltering.js?v=${fileVersion}"></script>
<script>
    ${blackListRuleInfoGridBaseInfo}
    $(document).ready(function() {
        filteringJs.init();
        blackListRuleInfoGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

