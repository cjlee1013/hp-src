<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사전 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>어휘</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>어휘</th>
                        <td>
                            <div class="ui form inline">
                                <select id="searchType" name="searchType" class="ui input medium mg-r-10 select-style">
                                    <option value="wordName" selected="selected">단어</option>
                                    <option value="wordId">번호</option>
                                </select>

                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" value="${requestWordName}">
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="wordInclude" id="wordInclude" value="1" class="text-center"/><span class="text">일부포함 모두 찾기</span>
                                </label>
                            </div>
                        </td>
                        <td rowspan="6">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="writeBtn" style="display: none;" class="ui button large white" >등록</button>
                                <button type="submit" id="searchBtn" class="ui button large cyan" >검색</button><br>
                                <button type="button" class="ui button large white mg-t-5" id="resetBtn" >초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>속성</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="useFlag" id="useFlag" value="N" class="text-center" /><span class="text">사전등록안함</span>
                                </label>
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="stopFlag" id="stopFlag" value="Y" class="text-center" /><span class="text">금칙어</span>
                                </label>
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="isGeneral" id="isGeneral" value="Y" class="text-center" /><span class="text">범용키워드</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>확장어</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="synonymInclude" id="synonymInclude" value="Y" class="text-center" checked="checked"/><span class="text">동의어</span>
                                </label>
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="hyponymInclude" id="hyponymInclude" value="Y" class="text-center" checked="checked"/><span class="text">하위어</span>
                                </label>
                                <label class="ui checkbox mg-r-5 mg-t-10">
                                    <input type="checkbox" name="relatednymInclude" id="relatednymInclude" value="Y" class="text-center" checked="checked"/><span class="text">연관어</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>품사</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5 select-style" id="pos" name="pos" data-default="1depth"
                                        onchange="javascript:;">
                                    <c:forEach var="posDto" items="${pos}" varStatus="status">
                                        <option value="${posDto.posCd}" ${selected}>${posDto.posNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사전</th>
                        <td>
                            <c:forEach var="dicTypeDto" items="${dicType}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <input type="checkbox" name="dicType" id="dicType" value="${dicTypeDto.dicTypeCd}" class="text-center"/> <span class="text">${dicTypeDto.dicTypeNm}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th>날짜</th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5 select-style" id="schDateType" name="schDateType">
                                    <option value="REG">등록일</option>
                                    <option value="MOD">수정일</option>
                                </select>
                                &nbsp;
                                <input type="text" id="sdate" name="sdate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="edate" name="edate" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="dictionaryTotalCount">0</span>건</h3>
            <div class="ui form inline" style="text-align:right;">
                <button type="button" class="ui button small mint font-malgun mg-r-5" onclick="dictionaryList.history();">이력</button>
                <button type="button" class="ui button small blue font-malgun mg-r-5" onclick="dictionaryList.modify();">수정</button>
                <button type="button" class="ui button small green font-malgun mg-r-5" onclick="dictionaryList.compounds();">복합</button>
                <button type="button" class="ui button small sky-blue font-malgun mg-r-5" onclick="dictionaryList.preAnalysis();">기분석</button>
                <button type="button" class="ui button small btn-danger font-malgun mg-r-5" onclick="dictionaryList.delete();">삭제</button>
            </div>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="dictionaryListGrid" style="height: 350px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 관련어(동의어,하위어,연관어) 정보</h3>
    </div>
    <div class="topline"></div>

    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>카테고리랭킹 등록/수정</caption>
            <colgroup>
                <col width="7%">
                <col width="*">
            </colgroup>
            <tbody>
            <tr> 
                <th >동의어</th>
                <td >
                    <div class="ui form inline">
                        <input id="synonym" name="synonym" type="text" class="ui input medium mg-r-5 search-input-style-middle" maxlength="500" placeholder="여러개의 키워드를 입력할 경우에는 ;(세미콜론) 기준으로 입력해주세요." onfocusout="javascript:$('#overview_analysis').hide();" onkeydown="dictionaryList.overviewAnalysis(35);" onkeyup="dictionaryList.overviewTokenizedResult(this)">
                        <span class="text">( <span style="color:red;" id="textCountKr_synonym">0</span> / 500자 )</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th >하위어</th>
                <td >
                    <div class="ui form inline">
                        <input id="hyponym" name="hyponym" type="text" class="ui input medium mg-r-5 search-input-style-middle" maxlength="500" placeholder="여러개의 키워드를 입력할 경우에는 ;(세미콜론) 기준으로 입력해주세요." onfocusout="javascript:$('#overview_analysis').hide();" onkeydown="dictionaryList.overviewAnalysis(80);" onkeyup="dictionaryList.overviewTokenizedResult(this)">
                        <span class="text">( <span style="color:red;" id="textCountKr_hyponym">0</span> / 500자 )</span>
                    </div>
                </td>
            </tr>
            <tr>
                <th >연관어</th>
                <td >
                    <div class="ui form inline">
                        <input id="relatednym" name="relatednym" type="text" class="ui input medium mg-r-5 search-input-style-middle" maxlength="500" placeholder="여러개의 키워드를 입력할 경우에는 ;(세미콜론) 기준으로 입력해주세요." onfocusout="javascript:$('#overview_analysis').hide();" onkeydown="dictionaryList.overviewAnalysis(125);" onkeyup="dictionaryList.overviewTokenizedResult(this)">
                        <span class="text">( <span style="color:red;" id="textCountKr_relatednym">0</span> / 500자 )</span>
                    </div>
                </td>
            </tr>
            </tbody>
            <div id="overview_analysis" style="display:none;position:absolute;z-index: 1;background-color:white;width:150px;height:150px;left:186px; border: 1px solid; overflow:scroll;">
                <span class="inner">
                    <textarea id="analysisResult" name="analysisResult" class="ui input mg-r-5" style="float:left;width: 150px; height: 150px;"></textarea>
                </span>
            </div>
        </table>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="morphExpansionSaveBtn">관련어 저장</button>
            <button class="ui button xlarge white font-malgun" id="morphExpansionDeleteBtn">관련어 삭제</button>
        </span>
    </div>

</div>
<input type="hidden" id="wordId" name="wordId"/>
<input type="hidden" id="wordName" name="wordName"/>
<input type="hidden" id="requestWordName" name="requestWordName" value="${requestWordName}"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<!-- script -->
<script type="text/javascript" src="/static/js/search/dictionaryList.js?v=${fileVersion}"></script>
<script>
    ${dictionaryListGridBaseInfo}

    $(document).ready(function() {
        dictionaryList.init();
        dictionaryList.bindingEvent();
        dictionaryListGrid.init();

        if($('#requestWordName').val() != '') {
            dictionaryList.search();
        }
        CommonAjaxBlockUI.global();
    });
</script>

