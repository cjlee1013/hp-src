<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 데이터 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <div style="display: none">
                    <input type="radio" name="siteType" value="HOME" checked/>
                </div>
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="DIRECT" checked><span>직접입력</span></label>
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="SEARCH"><span>조회</span></label>
                                <input type="text" id="storeIds" name="storeIds" value="37" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID">
                                <span class="text" id="directText">(점포ID는 (,) 쉼표로 구분해서 입력해주세요. 최대 3개까지만 입력 가능합니다.)</span>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;display:none;" placeholder="점포명" readonly>
                                <button type="button" id="schStoreBtn" class="ui button medium" style="display:none;" onclick="categorySearchJs.getStorePop('categorySearchJs.callBack');" >조회</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white font-malgun mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <label for="searchCateCd1"></label>
                            <select class="ui input middle left-float mg-r-5 select-style"
                                    id="searchCateCd1"
                                    name="schLcateCd"
                                    data-default="대분류"
                                    onchange="categorySearchJs.drawCategorySelect(1,'searchCateCd');">
                                <option value="">전체</option>
                            </select>
                            <label for="searchCateCd2"></label>
                            <select class="ui input middle left-float mg-r-5 select-style"
                                    id="searchCateCd2"
                                    name="schMcateCd"
                                    data-default="중분류"
                                    onchange="categorySearchJs.drawCategorySelect(2,'searchCateCd');">
                                <option value="">중분류</option>
                            </select>
                            <label for="searchCateCd3"></label>
                            <select class="ui input middle left-float mg-r-5 select-style"
                                    id="searchCateCd3"
                                    name="schScateCd"
                                    data-default="소분류"
                                    onchange="categorySearchJs.drawCategorySelect(3,'searchCateCd');">
                                <option value="">소분류</option>
                            </select>
                            <label for="searchCateCd4"></label>
                            <select class="ui input middle left-float mg-r-5 select-style"
                                    id="searchCateCd4"
                                    name="schDcateCd"
                                    data-default="세분류"
                                    onchange="categorySearchJs.drawCategorySelect(4,'searchCateCd');">
                                <option value="">세분류</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>정렬</th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="RANK" checked><span>추천순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="SALES_UP"><span>많이팔린순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="PRICE_DOWN"><span>낮은가격순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="PRICE_UP"><span>높은가격순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="REVIEW_UP"><span>리뷰많은순</span>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <!-- 검색 조건 필터 ∧ ∨ -->
        <button type="button" id="filterBtn" class="ui button middle gray-dark font-malgun mg-t-10" onclick="javascript:filterJs.toggle()">필터 검색 ∨</button>
        <div id="filter-box" style="display: none">
            <div class="attribute-box" >
                <div style="overflow: hidden;" id="attributeList"></div>
            </div>

            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <colgroup>
                        <col width="8%">
                        <col width="25%">
                        <col width="8%">
                        <col width="*">
                    </colgroup>
                    <tbody id="filter-content">
                    <tr>
                        <th>배송</th>
                        <td>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="delivery" value="HYPER_DRCT" attr="HYPER_DRCT" display="마트직송"><span>마트직송</span>
                            </label>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="delivery" value="DLV" attr="DLV" display="택배배송"><span>택배배송</span>
                            </label>
                        </td>
                        <th>혜택</th>
                        <td>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="benefit" value="BASIC" attr="BASIC" display="행사상품"><span>행사상품</span>
                            </label>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="benefit" value="PICK" attr="PICK" display="골라담기"><span>골라담기</span>
                            </label>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="benefit" value="TOGETHER:INTERVAL" attr="TOGETHERINTERVAL" display="함께할인"><span>함께할인</span>
                            </label>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="benefit" value="GIFT" attr="GIFT" display="사은품"><span>사은품</span>
                            </label>
                            <label class="ui checkbox mg-r-10 checkbox-style">
                                <input type="checkbox" name="benefit" value="FREE" attr="FREE" display="무료배송"><span>무료배송</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>브랜드</th>
                        <td id="brandList" colspan="2" ></td>
                        <td id="brandMore" style="visibility:hidden;">
                            <button type="button" id="brandButton" class="ui button small sky-blue font-malgun mg-l-20" onclick="filterJs.moreButton('brand')">+ 더보기</button>
                        </td>
                    </tr>
                    <tr>
                        <th>파트너</th>
                        <td id="partnerList" colspan="2" ></td>
                        <td id="partnerMore" style="visibility:hidden;">
                            <button type="button" id="partnerButton" class="ui button small sky-blue font-malgun mg-l-20" onclick="filterJs.moreButton('partner')">+ 더보기</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount" name="totalCount">0</span>건</h3> <button type="button" id="copyQuery" name="copyQuery" style="margin-left:10px;" class="ui button small dark-blue font-malgun">쿼리복사</button>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="categorySearchGrid" style="height: 50%; width:100%;"></div>

        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>가중치 매치정보</caption>
                <colgroup>
                    <col width="8%">
                    <col width="5%">
                    <col width="*%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text"></th>
                    <th class="text" style="text-align: center;">반영점수</th>
                    <th class="text" style="text-align: center;">매치정보</th>
                </tr>
                <tr>
                    <th class="text">DOCID</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexDocId"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">상품번호</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexItemNo"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">검색상품명(원형)</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexSearchItemNm"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">카테고리</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexCategory"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">브랜드 한글</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexBrandNmKor"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">브랜드 영문</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexBrandNmEng"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">파트너</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexShopNm"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">가중치(Weight) 수식</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexWeight">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightCalc"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">24시간 구매자수</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightDay"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="hidden_query" name="hidden_query"/>
<input type="hidden" id="profilesActive" name="profilesActive" value=${profilesActive} />
<input id="clip_target" type="text" value="" style="position:absolute;top:-9999em;"/>
<!-- script -->
<script type="text/javascript" src="/static/js/search/dataManagementFilter.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/dataManagementCategory.js?${fileVersion}"></script>
<script type="text/javascript">

    ${categorySearchGridBaseInfo}

    $(document).ready(function() {
        setSearchType(searchType.CATEGORY);
        currentSearchJs.init();
        categorySearchGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>
