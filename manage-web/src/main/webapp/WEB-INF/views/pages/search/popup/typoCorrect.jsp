<%--
  Created by IntelliJ IDEA.
  User: hyeoncheol.shin
  Date: 03/02/2020
  Time: 10:09 오전
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp" />
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<style type="text/css">
    body {
        overflow-x: hidden;
        overflow-y: hidden;
    }
</style>

<br>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <div class="ui form inline" style="margin-right:10px;">
            <h2 class="title font-malgun" style="margin-left:10px;">오탈자</h2> <span style="float:right;"><button type="button" class="ui button small gray-dark font-malgun " onclick="javascript:self.close();" style="height:26px;width:50px;">닫기</button></span>
        </div>
    </div>
<%--    <div class="com wrap-title sub">
        <div class="ui form inline" style="margin-left: 10px;">
            <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" maxlength="16" readonly="readonly" value="${wordName}" style="width:140px;height:30px;"> <input id="correctedTerms" name="correctedTerms" type="text" class="ui input medium mg-r-5" maxlength="16"  value="${model.correctedTerms}" style="width:600px;height:30px;">
        </div>
    </div>--%>
    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>어휘</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>표제어</th>
                        <td>
                            <div class="ui form inline" style="width:300px;">
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5" readonly="readonly" style="height:30px;" value="${wordName}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>오탈자</th>
                        <td>
                            <div class="ui form inline" style="width:300px;">
                                <textarea name="correctedTerms" id="correctedTerms" class="ui input" style="width: 600px; height: 100px;" maxlength="2000" placeholder="※ 키워드가 여러개일때 ; 으로 구분">${correctedTerms}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="ui center-button-group mg-t-15">
                                <span class="inner">
                                    <button type="button" class="ui button large cyan font-malgun" id="setItemBtn">등록</button>
                                    <button type="button" class="ui button large white font-malgun" id="resetBtn">취소</button>
                                </span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="wordId" name="wordId" value="${wordId}">
<!-- script -->
<script type="text/javascript" src="/static/js/search/dictionaryTypoCorrect.js?v=${fileVersion}"></script>


