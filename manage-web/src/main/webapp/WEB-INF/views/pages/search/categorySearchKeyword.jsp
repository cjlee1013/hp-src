<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 유사어</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline" >
                                <select id="schSearchType" name="schSearchType" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="KEYWORD" selected="selected">키워드</option>
                                    <option value="CATEGORYID">카테고리번호</option>
                                </select>
                                &nbsp;
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline" >
                                <select id="schUseYn" name="schUseYn" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="" selected="selected">전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                        <th>재설정대상</th>
                        <td>
                            <div class="ui form inline" >
                                <select id="schResetYn" name="schResetYn" class="ui input medium mg-r-10" style="width: 120px">
                                    <option value="" selected="selected">전체</option>
                                    <option value="Y">대상</option>
                                    <option value="N">대상아님</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="categorySearchKeywordGrid" style="height: 400px; width:100%"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="editForm" onsubmit="return false;">
                <input type="hidden" id="dcateSearchKeywordNo" name="dcateSearchKeywordNo">
                <input type="hidden" name="regId" value="${userInfo.getEmpId()}">
                <input type="hidden" name="chgId" value="${userInfo.getEmpId()}">
                <input type="hidden" name="confirmId" value="${userInfo.getEmpId()}">
                <table class="ui table">
                    <caption>카테고리랭킹 등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>카테고리 <span class="text-red">*</span></th>
                        <td class="saveArea">
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-5 select-style" id="searchCateCd1" name="lcateCd" data-default="대분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5 select-style" id="searchCateCd2" name="mcateCd" data-default="중분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5 select-style" id="searchCateCd3" name="scateCd" data-default="소분류"
                                        onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                                </select>
                                <select class="ui input medium mg-r-5 select-style" id="searchCateCd4" name="dcateCd" data-default="세분류">
                                </select>
                            </div>
                        </td>
                        <td class="modArea" id="cateViewArea" style="display: none;">
                        </td>
                    </tr>
                    <tr>
                        <th>검색어 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-middle" maxlength="500" placeholder="여러개의 키워드를 입력할 경우에는 ;(세미콜론) 기준으로 입력해주세요.">
                                <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 500자 )</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부 <span class="text-red">*</span></th>
                        <td>
                            <label class="ui radio small inline"><input type="radio" name="useYn" value="Y" checked><span>사용</span></label>
                            <label class="ui radio small inline"><input type="radio" name="useYn" value="N"><span>미사용</span></label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <br>
    <div class="topline"></div>
    <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun saveArea" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun modArea" id="confirmBtn" style="display: none;">확인</button>
            <button type="button" class="ui button xlarge white font-malgun modArea" id="modBtn" style="display: none;">수정</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/categorySearchKeyword.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${categorySearchKeywordGridBaseInfo}

    $(document).ready(function() {
        categorySearchKeywordJs.init();
        categorySearchKeywordJs.event();
        categorySearchKeywordGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>