<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">오타 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>수정일</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <input type="text" id="schSdate" name="schSdate" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEdate" name="schEdate" class="ui input medium mg-r-5 date-input-style">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <input id="schKeyword" name="schKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="typoGrid" style="height: 450px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10" >
            <form id="typodSetForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>오탈자 등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <div class="ui form inline">
                                <input id="keyword" readonly="readonly" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                                <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 15자 )</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>오탈자 후보 <span class="text-red">*</span></th>
                        <td>
                        <span class="ui form inline">
                            <input id="correctedTerms" name="correctedTerms" type="text" class="ui input medium mg-r-5 search-input-style-middle" maxlength="2000" placeholder="여러개의 오타를 입력할 경우에는 ;(세미콜론) 기준으로 입력해주세요.">
                            <span class="text">( <span style="color:red;" id="textCountKr_correctedTerms">0</span> / 2000자 )</span>
                        </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>

    <div class="com wrap-title sub">
        <div class="com wrap-title sub">
            <h3 class="title">• 추가후보: <span id="candidateCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="candidateGrid" style="height: 450px; width:100%;"></div>
    </div>

    <div class="ui center-button-group mg-t-15" >
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="deleteBtn">삭제</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<input type="hidden" id="typoId" name="typoId"/>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/typo.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${typoGridBaseInfo}

    ${candidateGridBaseInfo}

    $(document).ready(function() {
        typoJs.init();
        typoJs.event();
        typoGrid.init();
        candidateGrid.init();
        typoJs.onload();
        CommonAjaxBlockUI.global();
    });
</script>