<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사전 백업/복원 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="backupRecoveryForm" onsubmit="return false;" enctype="multipart/form-data">
                <table class="ui table">
                    <caption>백업/복원 구성</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>백업 대상 서버</th>
                        <td>
                            <select name="schServiceCd" class="ui input medium mg-r-10 select-style">
                                <option value="HOMEPLUS" selected="selected">통합검색</option>
                                <option value="ADDRESS">주소검색</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>복원 파일</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" class="ui input medium search-input-style-small" readonly="readonly"/>
                                <button class="ui button medium white mg-l-10" id="fileUpload">파일 선택</button>
                                <input multiple="multiple" type="file" name="uploadFile" id="uploadFile" style="display:none;">
                                <div id="uploadFileNm" name="uploadFileNm" class="text"></div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="backupBtn">백업</button>
            <button class="ui button xlarge white font-malgun" id="recoveryBtn">복원</button>
        </span>
    </div>
</div>
<!-- download iframe's -->
<iframe id="tb_dic_vocabulary" name="tb_dic_vocabulary" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_description" name="tb_dic_description" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_update_history" name="tb_dic_update_history" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_attribute" name="tb_dic_attribute" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_morphexpansion" name="tb_dic_morphexpansion" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_compounds" name="tb_dic_compounds" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_compounds_result" name="tb_dic_compounds_result" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_preanalysis_attribute" name="tb_dic_preanalysis_attribute" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_preanalysis" name="tb_dic_preanalysis" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_preanalysis_result" name="tb_dic_preanalysis_result" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_unit" name="tb_dic_unit" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_replaced_keyword" name="tb_dic_replaced_keyword" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_ranking_category" name="tb_ranking_category" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_ranking_category_relation" name="tb_ranking_category_relation" style="display:none;" frameborder="0"></iframe>

<iframe id="tb_dic_address_noun" name="tb_dic_address_noun" style="display:none;" frameborder="0"></iframe>
<iframe id="tb_dic_address_synonym" name="tb_dic_address_synonym" style="display:none;" frameborder="0"></iframe>
<!--// download iframe's -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/backupRecovery.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    CommonAjaxBlockUI.global();
    $(function() {
        backupRecovery.event();
    });
</script>