<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">익스프레스 검색 데이터 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <input type="hidden" name="storeType" id="storeType" value="EXP">
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="DIRECT" checked><span>직접입력</span></label>
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="SEARCH"><span>조회</span></label>
                                <input type="text" id="storeIds" name="storeIds" value="515" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID">
                                <span class="text" id="directText">(점포ID는 (,) 쉼표로 구분해서 입력해주세요. 최대 3개까지만 입력 가능합니다.)</span>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;display:none;" placeholder="점포명" readonly>
                                <button type="button" id="schStoreBtn" class="ui button medium" style="display:none;" onclick="expsearchJs.getStorePop('expsearchJs.callBack');" >조회</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white font-malgun mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small">
                        </td>
                    </tr>
                    <tr>
                        <th>정렬</th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="RANK" checked><span>추천순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="SALES_UP"><span>많이팔린순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="PRICE_DOWN"><span>낮은가격순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="PRICE_UP"><span>높은가격순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="REVIEW_UP"><span>리뷰많은순</span>
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <!-- 검색 조건 필터 ∧ ∨ -->
        <button type="button" id="filterBtn" class="ui button middle gray-dark font-malgun mg-t-10" onclick="javascript:filterJs.toggle()">필터 검색 ∨</button>
        <div id="filter-box" style="display: none">
            <div class="attribute-box" >
                <div style="overflow: hidden;" id="attributeList"></div>
            </div>

            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <colgroup>
                        <col width="8%">
                        <col width="36%">
                        <col width="*">
                    </colgroup>
                    <tbody id="filter-content">
                    <tr>
                        <th>카테고리</th>
                        <td colspan="2">
                            <label for="schCateCd1"></label>
                            <select class="ui input middle left-float mg-r-5" id="schCateCd1" name="schLcateCd" onchange="categoryChange(1,'schCateCd');">
                                <option value="">대분류</option>
                            </select>
                            <label for="schCateCd2"></label>
                            <select class="ui input middle left-float mg-r-5" id="schCateCd2" name="schMcateCd" data-default="중분류" onchange="categoryChange(2,'schCateCd');">
                                <option value="">중분류</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>혜택</th>
                        <td colspan="2">
                            <label class="ui checkbox mg-r-10" style="display: inline-block">
                                <input type="checkbox" name="benefit" value="BASIC" attr="BASIC" display="행사상품"><span>행사상품</span>
                            </label>
                            <label class="ui checkbox mg-r-10" style="display: inline-block">
                                <input type="checkbox" name="benefit" value="PICK" attr="PICK" display="골라담기"><span>골라담기</span>
                            </label>
                            <label class="ui checkbox mg-r-10" style="display: inline-block">
                                <input type="checkbox" name="benefit" value="TOGETHER:INTERVAL" attr="TOGETHERINTERVAL" display="함께할인"><span>함께할인</span>
                            </label>
                            <label class="ui checkbox mg-r-10" style="display: inline-block">
                                <input type="checkbox" name="benefit" value="GIFT" attr="GIFT" display="사은품"><span>사은품</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>브랜드</th>
                        <td id="brandList"></td>
                        <td id="brandMore" style="visibility:hidden;">
                            <button type="button" id="brandButton" class="ui button small sky-blue font-malgun mg-l-20" onclick="filterJs.moreButton('brand')">+ 더보기</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount" name="totalCount">0</span>건</h3> <button type="button" id="copyQuery" name="copyQuery" style="margin-left:10px;" class="ui button small dark-blue font-malgun">쿼리복사</button>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="expsearchGrid" style="height: 50%; width:100%;"></div>


        <div class="com wrap-title sub">
            <h3 class="title">• 키워드 정보</h3>
            <h3 class="sub-title" align="right">
                <span class="text">ㅁ대표어</span>&nbsp;
                <span class="text"><font color="#dc143c"> ㅁ금칙어</font></span>&nbsp;
                <span class="text"><font color="#6495ed"> ㅁ동의어</font></span>&nbsp;
                <span class="text"><font color="#6b8e23"> ㅁ연관어</font></span>&nbsp;
                <span class="text"><font color="#8a2be2"> ㅁ하위어</font></span>&nbsp;
                <span class="text"><font color="#f4a460"> ㅁ단위</font></span>&nbsp;
            </h3>
        </div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>키워드 정보</caption>
                <colgroup>
                    <col width="8%">
                    <col width="5%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th rowspan="3" class="text">검색어 분석 결과</th>
                    <th class="text">범용어</th>
                    <td>
                        <span class="text" name="generalTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">일반</th>
                    <td colspan="2">
                        <span class="text" name="analysisResultTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">원본검색</th>
                    <td colspan="2">
                        <span class="text" name="analysisOriginalResultTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text-red-star">금칙어</th>
                    <td colspan="2" class="ui form inline">
                        <span class="text" name="filteredTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">오타변환</th>
                    <td colspan="2">
                        <span class="text" name="typoTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">한/영변환</th>
                    <td colspan="2">
                        <span class="text" name="hanEngTxt"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">대체 검색어</th>
                    <td colspan="2">
                        <span class="text" name="subTxt"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <br>

        <div class="com wrap-title sub">
            <h3 class="title">• 색인어/가중치 매치정보</h3>
            <h3 class="sub-title" align="right">
                <span class="text"><font color="blue">* 검색어 매칭 SCORE 가중치 -</font> 대표어(KOREAN,WORD):10점, 단위:10점, 연관어:6점, 하위어:7점, 동의어:8점</span>&nbsp;&nbsp;&nbsp;
            </h3>
        </div>
        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>색인어/가중치 매치정보</caption>
                <colgroup>
                    <col width="8%">
                    <col width="5%">
                    <col width="5%">
                    <col width="*%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text"></th>
                    <th class="text" style="text-align: center;">단위점수</th>
                    <th class="text" style="text-align: center;">반영점수</th>
                    <th class="text" style="text-align: center;">매치정보</th>
                </tr>
                <tr>
                    <th class="text">DOCID</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexDocId"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">상품번호</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexItemNo"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">검색상품명 (원형)</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexSearchItemNmList"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">검색상품명 (분해정보)</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexSearchItemNmTokenList"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">검색상품명 <font color="#dc143c">(매칭정보)</font></th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexSearchItemNmMatchedListScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexSearchItemNmMatchedList"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">텀(Term)거리점수</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexPosDistance">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">검색키워드</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexSearchKeywordScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexSearchKeyword"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">온라인 카테고리</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexPhysicalCategoryInfo"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">온라인 카테고리 키워드</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexDcateNmScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexDcateNm"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">용도 옵션</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexItemOptionNm"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">온라인 카테고리 랭킹 - 부스트</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">1,000점</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexCategoryBoostScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexCategoryBoost"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">온라인 카테고리 랭킹 - 제외</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexCategoryExclusive"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">온라인 카테고리 유사어</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexCategorySearchKeyword"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">브랜드 한글</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexBrandNmKorScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexBrandNmKor"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">브랜드 영문</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexBrandNmEngScore">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexBrandNmEng"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">행사 키워드</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexEventKeywordList"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">가중치(Weight) 수식</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexWeight">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightCalc"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">24시간 구매자수</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightDay"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<input type="hidden" id="schKeyword" name="schKeyword"/>
<input type="hidden" id="searchType" name="searchType"/>
<input type="hidden" id="hidden_query" name="hidden_query"/>
<input type="hidden" id="profilesActive" name="profilesActive" value=${profilesActive} />
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/dataManagementFilter.js?${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/dataManagementExpsearch.js?v=${fileVersion}"></script>
<script type="text/javascript">
    ${expsearchGridBaseInfo}
    $(document).ready(function() {
        setSearchType(searchType.EXP_KEYWORD);
        currentSearchJs.init();
        expsearchGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>
<input id="clip_target" type="text" value="" style="position:absolute;top:-9999em;"/>
