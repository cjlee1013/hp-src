<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">검색로그 블랙리스트 IP 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <colgroup>
                        <col width="15%">
                        <col width="650">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr style="height:56px;">
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <label for="searchDateType">
                                    <select class="ui input middle left-float mg-r-5" id="searchDateType" name="searchDateType" style="width:15%;">
                                        <option value="mod">수정일</option>
                                        <option value="reg">등록일</option>
                                        <option value="valid">유효기간</option>
                                    </select>
                                </label>
                                <input type="text" id="searchStartDt" name="searchStartDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">~</span>
                                <input type="text" id="searchEndDt" name="searchEndDt" class="ui input medium mg-r-5" style="width:100px;">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="today" class="ui button small gray-dark font-malgun mg-r-5" onclick="blackListJs.initSearchDate('0d');">오늘</button>
                                <button type="button" id="oneday" class="ui button small gray-dark font-malgun mg-r-5" onclick="blackListJs.initSearchDate('-1d');">어제</button>
                                <button type="button" id="month" class="ui button small gray-dark font-malgun mg-r-5" onclick="blackListJs.initSearchDate('-30d');">1개월전</button>
                                <button type="button" id="3month" class="ui button small gray-dark font-malgun mg-r-5" onclick="blackListJs.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <td style="vertical-align: middle; text-align: left;" rowspan="4">
                            <div class="ui form inline mg-l-5">
                                <button type="submit" id="searchBtn" class="ui button large cyan"><i class="fa fa-search"></i> 검색</button>
                                <br/>
                                <button type="button" id="resetBtn" class="ui button large white font-malgun mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:56px;">
                        <th>서비스</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio inline">
                                    <input type="radio" name="searchServiceType" value="HOME" checked><span>홈플러스 </span>
                                </label>
                                <label class="ui radio inline">
                                    <input type="radio" name="searchServiceType" value="EXP"><span>익스프레스 </span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr style="height:56px;">
                        <th>블랙리스트 유형 </th>
                        <td>
                            <label for="searchBlackListType"></label>
                            <select class="ui input middle left-float mg-r-5" id="searchBlackListType" name="searchBlackListType" style="width:20%;">
                                <option value="">전체</option>
                                <option value="CRAWLING">크롤링</option>
                                <option value="ABUSING">어뷰징</option>
                            </select>
                        </td>
                    </tr>
                    <th>IP</th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <input id="searchIp" name="searchIp" value="" type="text" class="ui input medium mg-r-5" maxlength="30" style="width:20%;height:30px;">
                            <span class="text"> ** 입력 예시) 10.100.100.100</span>
                        </div>
                    </td>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="blackListGridTotalCount" name="blackListGridTotalCount">0</span>건</h3>
            <span class="pull-right" style="font-size: 14px;">• 상태 변경
                <button type="button" class="ui button medium" onclick="blackListJs.statusChange('Y','활성화')">활성화 </button>
                <button type="button" class="ui button medium" onclick="blackListJs.statusChange('N','비활성')">비활성화 </button>
            </span>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="blackListGrid" style="height: 350px; width:100%;"></div>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 블랙리스트 IP 추가 등록</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <colgroup>
                <col width="15%">
                <col width="300">
                <col width="15%">
                <col width="300">
            </colgroup>
            <tbody>
            <tr> 
                <th >IP</th>
                <td >
                    <div class="ui form inline">
                        <input id="regIp" name="regIp" value="" type="text" class="ui input medium mg-r-5" maxlength="15" style="width:100%;height:30px;">
                        <span class="text"> ** 입력 예시) 100.100.100.100</span>
                    </div>
                </td>
                <th> 서비스 </th>
                <td>
                    <div class="ui form inline">
                        <label class="ui radio inline">
                            <input type="radio" name="regServiceType" value="HOME" checked><span>홈플러스 </span>
                        </label>
                        <label class="ui radio inline">
                            <input type="radio" name="regServiceType" value="EXP"><span>익스프레스 </span>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <th >블랙리스트 유형 </th>
                <td >
                    <label for="regBlackType"></label>
                    <select class="ui input middle left-float mg-r-5" id="regBlackType" name="regBlackType" style="width:50%;" onchange="blackListJs.blackTypeChange();">
                        <option value="CRAWLING">크롤링</option>
                        <option value="ABUSING">어뷰징</option>
                    </select>
                </td>
                <th> 설정 기간  </th>
                <td>
                    <div class="ui form inline">
                        <input type="text" id="regStartDt" name="regStartDt" class="ui input medium mg-r-5" style="width:100px;">
                        <span class="text">~</span>
                        <input type="text" id="regEndDt" name="regEndDt" class="ui input medium mg-r-5" style="width:100px;">
                        <span class="text"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <th>비고</th>
                <td colspan="3">
                    <div class="ui form inline">
                        * 200자 이내로 작성하세요.
                        <textarea id="regMemo" name="regMemo" class="ui input medium mg-r-5" maxlength="200" style="width:100%;height:60px;"></textarea>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <input type="hidden" id="actionType" />
            <button type="button" id="regBtn" class="ui button xlarge cyan font-malgun"> 저장 </button>
            <button type="button" id="regReset" class="ui button xlarge white font-malgun">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/operationSearchLog.js?v=${fileVersion}"></script>
<script>
    ${blackListGridBaseInfo}
    $(document).ready(function() {
        blackListJs.init();
        blackListGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>

