<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun"><c:if test="${modify eq false}">어휘 등록</c:if><c:if test="${modify eq true}">어휘 수정</c:if></h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupWordWrite" class="ui wrap-table horizontal mg-t-10">
            <form id="wordWriteForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>어휘</caption>
                    <colgroup>
                        <col width="140">
                        <col width="500">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <!-- 어휘 -->
                    <tr>
                        <th>어휘</th>
                        <td>
                            <div class="ui form inline" style="width:300px;">

                                <input id="wordName" name="wordName" type="text" value="${wordName}" readonly="readonly" class="ui input medium mg-r-5" maxlength="16" style="height:30px;width:170px;">
                                <c:if test="${modify eq false}">
                                      <button type="submit" id="searchBtn"	class="ui button large cyan "><i class="fa fa-search"></i> 동형어조회</button>
                                </c:if>
                            </div>
                        </td>
                        <td colspan="1"></td>
                    </tr>
                    <tr <c:if test="${modify eq true}">style="display: none;"</c:if>>
                        <th>동형어내역</th>
                        <td>
                            <div class="mg-b-2" id="wordListGrid" style="height: 150px;width:750px;"></div>
                        </td>
                    </tr>
                    <!-- 의미 태그 -->
                    <tr>
                        <th>의미태그</th>
                        <td>
                            <div class="ui form inline"  style="width:170px;">
                                <input id="senseTag" name="senseTag" type="text" class="ui input medium mg-r-5" <c:if test="${modify eq true}">readonly="readonly"</c:if> maxlength="16" style="height:30px;" value="${detail.senseTag}">
                            </div>
                        </td>
                    </tr>
                    <!-- 사전 종류 -->
                    <tr>
                        <th>사전</th>
                        <td>
                            <c:forEach var="dicTypeDto" items="${dicType}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <c:set var="checked" value=""/>
                                        <c:if test="${modify eq true}">
                                            <c:forEach var="modifyDicTypeDto" items="${detail.dicType}" varStatus="status">
                                                <c:if test="${modifyDicTypeDto eq dicTypeDto.dicTypeCd}">
                                                    <c:set var="checked" value="checked"/>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${modify eq false}">
                                            <c:if test="${dicTypeDto.dicTypeCd eq 'EXTENSION'}">
                                                <c:set var="checked" value="checked"/>
                                            </c:if>
                                        </c:if>
                                        <input type="checkbox" name="dicType" id="dicType" value="${dicTypeDto.dicTypeCd}" class="text-center" ${checked}/> <span class="text">${dicTypeDto.dicTypeNm}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th>상태속성</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                    <c:choose>
                                        <c:when test="${useFlag eq true}">
                                            <input type="checkbox" name="useFlag" id="useFlag" value="Y" class="text-center" checked="checked"/> <span class="text">사전등록</span>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="useFlag" id="useFlag" value="N" class="text-center"/> <span class="text">사전등록</span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                    <c:choose>
                                        <c:when test="${stopFlag eq true}">
                                            <input type="checkbox" name="stopFlag" id="stopFlag" value="Y" class="text-center" checked="checked"/> <span class="text">금칙어</span>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="stopFlag" id="stopFlag" value="N" class="text-center"/> <span class="text">금칙어</span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                    <c:choose>
                                        <c:when test="${isGeneral eq 'Y'}">
                                            <input type="checkbox" name="isGeneral" id="isGeneral" value="Y" class="text-center" checked="checked"/> <span class="text">범용</span>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="isGeneral" id="isGeneral" value="N" class="text-center"/> <span class="text">범용</span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <!-- 품사 -->
                    <tr>
                        <th>품사</th>
                        <td>
                            <div class="ui form inline" style="width:170px;">
                                <select class="ui input medium mg-r-5" id="pos" name="pos"
                                        style="width:150px;float:left;" data-default="1depth"
                                        onchange="javascript:;">
                                    <c:forEach var="posDto" items="${pos}" varStatus="status">
                                        <c:set var="selected" value=""/>
                                        <c:if test="${modify eq true}">
                                            <c:if test="${posDto.posCd eq detail.pos}">
                                                <c:set var="selected" value="selected"/>
                                            </c:if>
                                        </c:if>
                                        <c:if test="${modify eq false}">
                                            <c:if test="${posDto.posCd eq 'NOUN'}">
                                                <c:set var="selected" value="selected"/>
                                            </c:if>
                                        </c:if>
                                        <option value="${posDto.posCd}" ${selected}>${posDto.posNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <!-- 명사 문법정보 -->
                    <tr>
                        <th>명사 문법정보</th>
                        <td>
                            <c:forEach var="nounAttrDto" items="${nounAttr}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <c:set var="checked" value=""></c:set>
                                        <c:forEach var="modifyNounAttr" items="${detail.nounAttr}" varStatus="status">
                                            <c:if test="${modifyNounAttr eq nounAttrDto.code}">
                                                <c:set var="checked" value="checked"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <input type="checkbox" name="nounAttr" id="nounAttr" value="${nounAttrDto.code}" class="text-center" ${checked}/> <span class="text">${nounAttrDto.name}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <!-- 동사 문법정보 -->
                    <tr>
                        <th>동사 문법정보</th>
                        <td>
                            <div class="ui form inline" style="width:170px;">
                                <select class="ui input medium mg-r-5" id="verbAttr" name="verbAttr"
                                        style="width:150px;float:left;" data-default="1depth"
                                        onchange="javascript:;">
                                    <c:forEach var="verbAttrDto" items="${verbAttr}" varStatus="status">
                                        <c:set var="selected" value=""></c:set>
                                        <c:if test="${detail.verbAttr eq verbAttrDto.code}">
                                            <c:set var="selected" value="selected"></c:set>
                                        </c:if>
                                        <option value="${verbAttrDto.code}" ${selected}>${verbAttrDto.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <!-- 개체명 유형 -->
                    <tr>
                        <th>개체명 유형</th>
                        <td>
                            <c:forEach var="neTypeDto" items="${neType}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <c:set var="checked" value=""></c:set>
                                        <c:forEach var="modifyNeTypeDto" items="${detail.neType}" varStatus="status">
                                            <c:if test="${modifyNeTypeDto eq neTypeDto.code}">
                                                <c:set var="checked" value="checked"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <input type="checkbox" name="neType" id="neType" value="${neTypeDto.code}" class="text-center" ${checked}/> <span class="text">${neTypeDto.name}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <!-- 분야 -->
                    <tr>
                        <th>분야</th>
                        <td>
                            <c:forEach var="domainDto" items="${domain}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <c:set var="checked" value=""></c:set>
                                        <c:forEach var="modifyDomainDto" items="${detail.domain}" varStatus="status">
                                            <c:if test="${modifyDomainDto eq domainDto.code}">
                                                <c:set var="checked" value="checked"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <input type="checkbox" name="domain" id="domain" value="${domainDto.code}" class="text-center" ${checked}/> <span class="text">${domainDto.name}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <!-- 언어 -->
                    <tr>
                        <th>언어</th>
                        <td>
                            <c:forEach var="languageDto" items="${language}" varStatus="status">
                                <div class="ui form inline">
                                    <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                        <c:set var="checked" value=""></c:set>
                                        <c:forEach var="modifyLanguageDto" items="${detail.language}" varStatus="status">
                                            <c:if test="${modifyLanguageDto eq languageDto.code}">
                                                <c:set var="checked" value="checked"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <input type="checkbox" name="language" id="language" value="${languageDto.code}" class="text-center" ${checked}/> <span class="text">${languageDto.name}</span>
                                    </label>
                                </div>
                            </c:forEach>
                        </td>
                    </tr>
                    <!-- 간략 설명 -->
                    <tr>
                        <th>간략 설명</th>
                        <td>
                            <div class="ui form inline">
                                <input id="note" name="note" type="text" class="ui input medium mg-r-5" maxlength="100" value="${detail.note}" style="height:30px;width:750px;">
                            </div>
                        </td>
                    </tr>
                    <!-- 뜻 풀이 -->
                    <tr>
                        <th>뜻풀이</th>
                        <td>
                            <div class="ui form inline">
                                <textarea name="description" id="description" class="ui input" style="width: 750px; height: 100px;" maxlength="1000">${detail.description}</textarea>
                            </div>
                        </td>
                    </tr>
                    <!-- 예시 -->
                    <tr>
                        <th>예시</th>
                        <td>
                            <div class="ui form inline">
                                <textarea name="example" id="example" class="ui input" style="width: 750px; height: 100px;" maxlength="1000">${detail.example}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="ui center-button-group mg-t-15">
                                <span class="inner">
                                    <c:if test="${modify eq false || modify eq null}">
                                        <button type="button" class="ui button xlarge cyan font-malgun" id="setItemBtn">등록</button>
                                    </c:if>
                                    <c:if test="${modify eq true}">
                                        <button type="button" class="ui button xlarge dark-blue font-malgun" id="editItemBtn">수정</button>
                                    </c:if>
                                    <c:if test="${popup eq true}">
                                    <button type="button" class="ui button xlarge white font-malgun" onclick="self.close();">닫기</button>
                                    </c:if>
                                    <c:if test="${popup eq false || popup eq null}">
                                        <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">목록으로</button>
                                    </c:if>
                                </span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="wordId" name="wordId" value="${wordId}"/>
<input type="hidden" id="popup" name="popup" value="${popup}"/>
<input type="hidden" id="direct" name="direct" value="${direct}"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/dictionaryWrite.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${wordListGridBaseInfo}
    $(document).ready(function() {
        dictionaryWrite.init();
        dictionaryWrite.event();
        CommonAjaxBlockUI.global();
    });
</script>