<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">키워드별 속성관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>키워드</th>
                        <td>
                            <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="10">
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <div class="ui form inline">+
                                <select class="ui input medium mg-r-10 select-style" id="searchUseYn" name="searchUseYn">
                                    <option value="">전체</option>
                                    <option value="Y">사용</option>
                                    <option value="N">사용안함</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="mg-b-20 " id="keywordAttrGrid" style="height: 330px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/조회</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="replacedKeywordSetForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>키워드 속성 등록/조회</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>키워드번호</th>
                        <td id="keywordAttrNohtml"></td>
                    </tr>
                    <tr>
                        <th>키워드 <span class="text-red">*</span></th>
                        <td>
                        <span class="ui form inline">
                            <input id="keyword" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="10">
                            <span class="text">( <span style="color:red;" id="textCount_keyword">0</span> / 10자 )</span>
                        </span>
                        </td>
                    </tr>
                    <tr>
                        <th>상품분류정보 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <input id="searchGAttr" name="searchGAttr" type="text" class="ui input medium mg-b-10 search-input-style-small" placeholder="분류명을 입력하세요.">
                                <ul id="searchGAttrLayer" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="top: 20px; left: 0px; width: 224px; display: none;">
                                    <li class="ui-menu-item-wrapper" id="gAttrSearchList_52">나이키 | NIKE | 52</li>
                                </ul>
                                <button type="button" class="mg-b-10" style="margin-right: 10px;border: 1px solid #ccc;background-color: #fff;" id="getGAttrBtn">
                                    <img src="/static/images/ui-kit/ui-select-search.png" alt="검색">
                                </button>
                            </div>
                            <table style="border-top: 1px solid #333333; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf;">
                                <caption>키워드 속성 조회/등록</caption>
                                <colgroup>
                                    <col width="80px">
                                    <col width="250px">
                                    <col width="100px">
                                    <col width="*">
                                    <col width="50px">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="text-align: center; color: #000000; background-color: #e0e0e0; border-bottom: 1px solid #53abe6; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf;"><strong>분류번호</strong></th>
                                        <th style="text-align: center; color: #000000; background-color: #e0e0e0; border-bottom: 1px solid #53abe6; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf;">관리분류명</th>
                                        <th style="text-align: center; color: #000000; background-color: #e0e0e0; border-bottom: 1px solid #53abe6; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf;">우선순위 <span class="text-red">*</span></th>
                                        <th style="text-align: center; color: #000000; background-color: #e0e0e0; border-bottom: 1px solid #53abe6; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf;">속성명 <span class="text-red">*</span></th>
                                        <th style="text-align: center; color: #000000; background-color: #e0e0e0; border-bottom: 1px solid #53abe6; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf;">삭제</th>
                                    </tr>
                                </thead>
                                <tbody id="gattrTable">
                                    <tr id="gattrEmptyTr">
                                        <td colspan="5" style="text-align: center">선택된 분류가 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부 <span class="text-red">*</span></th>
                        <td>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input medium" value="Y"><span>사용</span></label>
                            <label class="ui radio inline"><input type="radio" name="useYn" class="ui input medium" value="N"><span>사용안함</span></label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<input type="hidden" id="keywordAttrNo" name="keywordAttrNo"/>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/keywordAttr.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${keywordAttrListGridBaseInfo}

    $(document).ready(function() {
        $(document).on("click",function() {$('#searchGAttrLayer').hide()});
        keywordAttrGrid.init();
        keywordAttrJs.event();
        CommonAjaxBlockUI.global();
    });
</script>