<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<style type="text/css">
    #mindmapBody {
    }
    #debug1 {
        display:block;
    }
    .js-mindmap-active h1 {
        display:none;
    }
    .js-mindmap-active section h1 {
        display:block;
    }
    .js-mindmap-active .node {
        position:absolute;
        top:0;
        left:0;
        font-family:verdana;
        font-size:11px;
        color:#003258;
        opacity:0.9;
        padding:0 7px;
        cursor:pointer;
        cursor:hand;
        z-index:100;
        list-style:none;
    }
    .js-mindmap-active a.node {
        font: 30px/34px Arial, sans-serif;
        font-size:1em;
        letter-spacing: 0;
        display:block;
        color:blue;
        text-align:center;
        text-decoration:none;
    }
    .js-mindmap-active .node.active{
        font-size:1.5em;
    }
    .js-mindmap-active .node.active  a{
        color:#003258;
    }
    .js-mindmap-active .node.activeparent  a{
        color:#001228;
    }
    .js-mindmap-active img.line {
        position:absolute;
        width:200px;
        height:133px;
        top:0;
        left:0;
        display:block;
        z-index:0;
    }
    .ui-draggable {
        position:absolute;
    }
    .js-mindmap-active .node .node-action {
        position:absolute;
        right:-2em;
        bottom:-1px;
        text-align:center;
        vertical-align:super;
    }

</style>
<style type="text/css">
    #mindmapBody {
        background:white;
    }
    .js-mindmap-active a.node {
        background:white;
        /*border: 2px solid black; 이거가 각 어휘 테두리 */
        -webkit-border-top-left-radius: 20px;
        -webkit-border-bottom-right-radius: 20px;
        -moz-border-radius-topleft: 20px;
        -moz-border-radius-bottomright: 20px;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        border-bottom-left-radius: 20px;
        color:black;
    }
    .js-mindmap-active a.node.active {
        padding:5px 10px !important;
        border-width:5px !important;
    }
    .js-mindmap-active a.node.activeparent {
        padding:5px 10px !important;
        border-width:5px !important;
        background:#D8D8D8;
    }
</style>

<div class="com wrap-title sub" id="mindmapBody">
    <ul>
        <li><a href="http://kenneth.kufluk.com/blog/">${relation.info.wordName}</a>
            <ul>
                <li>
                    <a href="http://kenneth.kufluk.com/blog/">동의어 (${relation.synonyms.size()})</a>
                    <c:forEach var="entities" items="${relation.synonyms}">
                    <ul>
                        <li><a href="javascript:;" title="${entities.wordName}">${entities.wordName}</a></li>
                    </ul>
                    </c:forEach>
                </li>
                <li>
                    <a href="http://kenneth.kufluk.com/blog/">하위어 (${relation.hyponyms.size()})</a>
                    <c:forEach var="entities" items="${relation.hyponyms}">
                        <ul>
                            <li><a href="javascript:;" title="${entities.wordName}">${entities.wordName}</a></li>
                        </ul>
                    </c:forEach>
                </li>
                <li>
                    <a href="http://kenneth.kufluk.com/blog/">관련어 (${relation.relatednyms.size()})</a>
                    <c:forEach var="entities" items="${relation.relatednyms}">
                        <ul>
                            <li><a href="javascript:;" title="${entities.wordName}">${entities.wordName}</a></li>
                        </ul>
                    </c:forEach>
                </li>
                <li>
                    <a href="http://kenneth.kufluk.com/blog/">오탈자 (${relation.typonyms.size()})</a>
                    <c:forEach var="entities" items="${relation.typonyms}">
                        <ul>
                            <li><a href="javascript:;" title="${entities}">${entities}</a></li>
                        </ul>
                    </c:forEach>
                </li>
            </ul>
        </li>
    </ul>
</div>


<!-- Raphael for SVG support (won't work on android) -->
<script type="text/javascript" src="/static/js/search/raphael.js?v=${fileVersion}"></script>
<!-- Mindmap -->
<script type="text/javascript" src="/static/js/search/mindmap-script.js?v=${fileVersion}"></script>
<!-- Kick everything off -->
<script src="/static/js/search/dictionaryMindmap.js?v=${fileVersion}" type="text/javascript"></script>
