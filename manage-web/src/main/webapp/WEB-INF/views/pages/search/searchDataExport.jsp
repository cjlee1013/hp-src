<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">검색 데이터 추출</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <input type="hidden" name="searchUseYn" value="Y">
                <table class="ui table">
                    <caption>검색</caption>
                    <colgroup>
                        <col width="130px">
                        <col width="60%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포유형</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="searchStoreType" name="searchStoreType" style="width:120px;float:left;">
                                <option value="HYPER" selected="">HYPER</option>
                                <option value="EXP">EXPRESS</option>
                            </select>
                        </td>
                        <td rowspan="4">
                            <div class="ui form inline">
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포ID</th>
                        <td>
                            <select class="ui input medium mg-r-5" id="storeId" name="storeId" style="width:120px;float:left;">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>정렬</th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="ITEMNO" checked=""><span>상품번호순</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="sort" value="RANK"><span>추천순</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>필드</th>
                        <td>
                            <label class="ui checkbox-inline inline">
                                <input type="checkbox" name="fields" value="itemNo" checked=""><span>상품번호</span>
                            </label>
                            <label class="ui checkbox-inline inline">
                                <input type="checkbox" name="fields" value="itemNm" checked=""><span>상품명</span>
                            </label>
                            <label class="ui checkbox-inline inline">
                                <input type="checkbox" name="fields" value="soldOutYn"><span>품절여부</span>
                           </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<iframe style="display:none;" id="excelDownloadFrame"></iframe>
<!-- script -->
<script type="text/javascript" src="/static/js/search/searchDataExport.js?${fileVersion}"></script>
<script type="text/javascript">

    $(document).ready(function() {
        searchDataExportJs.init();
        CommonAjaxBlockUI.global();
    });
</script>