<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">신규상품 데이터 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <!-- 클럽정보 삭제로 hidden 처리 -->
                <div style="display: none">
                    <input type="radio" name="siteType" value="HOME" checked/>
                </div>
                <table class="ui table">
                    <caption>검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="DIRECT" checked><span>직접입력</span></label>
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="SEARCH"><span>조회</span></label>
                                <input type="text" id="storeIds" name="storeIds" value="37" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID">
                                <span class="text" id="directText">(점포ID는 (,) 쉼표로 구분해서 입력해주세요. 최대 3개까지만 입력 가능합니다.)</span>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;display:none;" placeholder="점포명" readonly>
                                <button type="button" id="schStoreBtn" class="ui button medium" style="display:none;" onclick="newItemJs.getStorePop('newItemJs.callBack');" >조회</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="resetBtn" class="ui button large white font-malgun mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <td>
                            <select class="ui input medium mg-r-5 select-style" id="schCategory" name="schCategory" data-default="대대분류">
                                <option value="0" selected="selected" name="전체">전체</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <br><br>
        <div class="topline"></div>
        <div class="mg-b-20 " id="newItemGrid" name="newItemGrid" style="height: 70%; width:100%;"></div>

        <div class="ui wrap-table horizontal">
            <table class="ui table">
                <caption>가중치 매치정보</caption>
                <colgroup>
                    <col width="8%">
                    <col width="5%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th class="text"></th>
                    <th class="text" style="text-align: center;">반영점수</th>
                    <th class="text" style="text-align: center;">매치정보</th>
                </tr>
                <tr>
                    <th class="text">DOCID</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexDocId"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">가중치(Weight) 수식</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="indexWeight">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightCalc"></span>
                    </td>
                </tr>
                <tr>
                    <th class="text">24시간 구매자수</th>
                    <td class="ui form inline" style="text-align: center;">
                        <span class="text" name="">-</span>
                    </td>
                    <td class="ui form inline">
                        <span class="text" name="indexWeightDay"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="hidden_query" name="hidden_query"/>
<input type="hidden" id="profilesActive" name="profilesActive" value=${profilesActive} />
<!-- script -->
<script type="text/javascript" src="/static/js/search/dataManagementNewItem.js?${fileVersion}"></script>
<script type="text/javascript">

    ${newItemGridBaseInfo}

    $(document).ready(function() {
        newItemJs.init();
        newItemGrid.init();
        CommonAjaxBlockUI.global();
    });
</script>