<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div style="display: none">
    <input type="radio" name="siteType" value="HOME" checked/>
</div>

<div class="com wrap-title">
    <input type="hidden" id="logType" value="${logType}">
    <div class="com wrap-title has-border">
        <c:if test="${logType eq 'success'}">
            <h2 class="title font-malgun">성공검색어</h2>
        </c:if>
        <c:if test="${logType eq 'failure'}">
            <h2 class="title font-malgun">실패검색어</h2>
        </c:if>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>성공검색 검색</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5 date-input-style" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5 date-input-style" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" name="rangeBtn" id="todayBtn"  class="ui button small cyan font-malgun mg-r-5" onclick="searchLogJs.initSearchDate('0', this);">오늘</button>
                                <button type="button" name="rangeBtn" id="onedayBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="searchLogJs.initSearchDate('-1d', this);">어제</button>
                                <button type="button" name="rangeBtn" id="weekBtn"   class="ui button small gray-dark font-malgun mg-r-5" onclick="searchLogJs.initSearchDate('-6d', this);">1주일전</button>
                                <button type="button" name="rangeBtn" id="monthBtn"   class="ui button small gray-dark font-malgun mg-r-5" onclick="searchLogJs.initSearchDate('-30d', this);">1개월전</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan ">검색</button><br>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                                <!-- 엑셀 파일명용 hidden 변수 -->
                                <input type="hidden" id="excelStartDt" value=""/>
                                <input type="hidden" id="excelEndDt" value=""/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색어</th>
                        <td>
                            <input type="text" class="ui input medium mg-r-5 search-input-style-small" id="searchKeyword">
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="searchLogGrid" style="height: 620px;"></div>
    </div>



    <c:if test="${logType eq 'failure'}">
    <div class="com wrap-title sub">
        <h3 class="title">• 검색어 로그 정보</h3>
    </div>
    <div class="ui wrap-table horizontal">
        <table class="ui table">
            <caption>검색어 로그 정보</caption>
            <colgroup>
                <col width="7%">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th>연관검색어</th>
                    <td>
                        <span class="text" name="indexRelateWord"></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </c:if>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/searchLog.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${searchLogGridBaseInfo}

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        searchLogJs.init();
        searchLogGrid.init();
    });
</script>