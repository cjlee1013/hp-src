<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사전 배포 이력</h2>
    </div>
    <div class="ui wrap-table horizontal mg-t-1">
        <table class="ui table">
            <caption>사전 배포 이력</caption>
            <colgroup>
                <col width="90px">
                <col width="120px">
                <col width="90px">
                <col width="120px">
                <col width="90px">
                <col width="120px">
                <col width="90px">
                <col width="120px">
                <col width="90px">
                <col width="120px">
                <col width="*">
            </colgroup>
            <tbody>
            <tr>
                <th>조회기간</th>
                <td colspan="9">
                    <div class="ui form inline">
                        <input type="text" id="startDeployDt" name="startDeployDt" class="ui input medium mg-r-5 picker-input" placeholder="시작일" style="width: 100px;" readonly="readonly">
                        <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                        <input type="text" id="endDeployDt" name="endDeployDt" class="ui input medium mg-r-5 picker-input" placeholder="종료일" style="width: 100px;" readonly="readonly">
                        <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <button type="button" id="todayBtn"  class="ui button small cyan font-malgun mg-r-5" onclick="historyJs.initSearchDate('0');">오늘</button>
                        <button type="button" id="onedayBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="historyJs.initSearchDate('-1d');">어제</button>
                        <button type="button" id="weekBtn"   class="ui button small gray-dark font-malgun mg-r-5" onclick="historyJs.initSearchDate('-6d');">1주일전</button>
                        <button type="button" id="monthBtn"   class="ui button small gray-dark font-malgun mg-r-5" onclick="historyJs.initSearchDate('-1m');">1개월전</button>
                    </div>
                </td>
                <!-- 검색 버튼 -->
                <td rowspan="2">
                    <div style="text-align: center;" class="ui form mg-t-15">
                        <button type="button" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i> 검색</button><br>
                        <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br>
                        <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button>
                        <!-- 엑셀 파일명용 hidden 변수 -->
                        <input type="hidden" id="excelStartDt" value=""/>
                        <input type="hidden" id="excelEndDt" value=""/>
                    </div>
                </td>
            </tr>
            <tr>
                <th>서비스</th>
                <td>
                    <select id="service" name="service" class="ui input medium mg-r-10" style="width: 150px">
                        <option value="HOMEPLUS" selected="selected">통합검색</option>
                        <option value="ADDRESS">주소검색</option>
                    </select>
                </td>
                <th>기능</th>
                <td>
                    <select id="actionType" name="actionType" class="ui input medium mg-r-10" style="width: 150px">
                        <option value="" selected="selected">전체</option>
                        <option value="DEPLOY">배포</option>
                        <option value="BUILD">빌드</option>
                    </select>
                </td>
                <th>구성</th>
                <td>
                    <select id="appType" name="appType" class="ui input medium mg-r-10" style="width: 150px">
                        <option value="" selected="selected">전체</option>
                        <option value="API">API</option>
                        <option value="NODE">NODE</option>
                    </select>
                </td>
                <th>사전유형</th>
                <td>
                    <select id="dictType" name="dictType" class="ui input medium mg-r-10" style="width: 150px">
                        <option value="" selected="selected">전체</option>
                        <option value="MORPH">어휘사전</option>
                        <option value="ISQ">검색품질사전</option>
                    </select>
                </td>
                <th>성공여부</th>
                <td>
                    <select id="successYn" name="successYn" class="ui input medium mg-r-10" style="width: 150px">
                        <option value="" selected="selected">전체</option>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="com wrap-title sub">
        <h3 class="title">• 검색결과: <span id="totalCount">0</span>건</h3>
    </div>
    <div class="topline"></div>
    <div class="mg-b-20 " id="deployHistoryGrid" style="height: 620px; width:100%;"></div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/deployHistory.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${deployHistoryGridBaseInfo}

    $(document).ready(function () {
        historyJs.init();
        CommonAjaxBlockUI.global();
        deployHistoryGrid.init();
        historyJs.search();
    });
</script>