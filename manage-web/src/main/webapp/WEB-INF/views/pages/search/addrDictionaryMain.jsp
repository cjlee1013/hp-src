<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">고유명사 조회</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10" >
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>고유명사</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>고유명사</th>
                        <td>
                             <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                        </td>
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="searchBtn" class="ui button large cyan">검색</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>고유명사포함</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-r-5 mg-t-10 pull-left">
                                    <input type="checkbox" name="includeNoun" id="includeNoun" value="Y" class="text-center"/> <span class="text">포함된 고유명사 모두 찾기</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="dictionaryTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="dictionaryListGrid" style="height: 400px; width:100%;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="replacedKeywordSetForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>고유명사 등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>단어</th>
                        <td>
                            <div class="ui form inline">
                                <input id="wordName" name="wordName" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                                <span class="text">( <span style="color:red;" id="textCountKr_wordName">0</span> / 15자 )</span>
                            </div>
                        </td>
                        <th>사용여부 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <select class="ui input medium mg-r-10 select-style" id="useFlag" name="useFlag">
                                    <option value="Y">사용</option>
                                    <option value="N">미사용</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>동의어 <span class="text-red">*</span></th>
                        <td colspan="3">
                            <span class="text-red">* (선택 된 행을 '더블클릭' 시 제거 할 수 있습니다)</span>
                            <div class="mg-b-20 " id="synonymListGrid" style="height: 360px;"></div>
                            <div class="ui form inline">
                                <input id="synonymName" name="synonymName" placeholder="추가하실 동의어를 입력해주세요." type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                                <span class="text" style="margin-right:10px;">( <span style="color:red;" id="textCountKr_synonymName">0</span> / 15자 )</span>
                                <button type="button" class="ui button medium white font-malgun" id="synonymSaveBtn">저장</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="editBtn">수정</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<input type="hidden" id="wordId" name="wordId"/>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<!-- script -->
<script type="text/javascript" src="/static/js/search/addrDictionaryList.js?v=${fileVersion}"></script>
<script type="text/javascript">
    ${dictionaryListGridBaseInfo}
    ${synonymListGridBaseInfo}

    $(document).ready(function() {
        dictionaryList.init();
        dictionaryList.bindingEvent();
        dictionaryListGrid.init();
        synonymListGrid.init();
        dictionaryList.search(0,dictionaryList.limitPageSize());
        CommonAjaxBlockUI.global();
    });
</script>